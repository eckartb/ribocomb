# parse output of nanotiler script. Assumes that last command
# of script was provided by nanotiler command opthelices
parse_nanoscript <- function(txt) {
 txt <- txt[grep("^\\s+#", txt, invert=TRUE)] # remove comments
 if (length(txt) > 1) {
  txt <- txt[length(txt)] # last line
 }
 txt <- as.character(txt)[1]
 s <- stringr::str_sub(txt,2,-2)
 sx <- stringr::str_split(s, pattern=",\\s*")[[1]]
 lst = list()
 for (s in sx) {
  words = stringr::str_split_fixed(s,pattern="=",n=2)
  testthat::expect_equal(length(words),2)
  y = words[2]
  if (!is.na(as.numeric(words[2]))) {
    y <- as.numeric(words[2]) # convert to number if possible!
  }
  lst[[words[1]]] <-  y
 }
 lst
}


