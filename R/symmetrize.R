
# symmetrize matrix using a specified function
symmetrize <- function(mtx, FUN=mean) {
  m = nrow(mtx)
  n = ncol(mtx)	   
  for (i in 1:(m-1)) {
   for (j in (i+1):m) {
    a = FUN(mtx[i,j],mtx[j,i])
    mtx[i,j] = a
    mtx[j,i] = a
   }
  }
  mtx
}