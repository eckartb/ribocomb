## Table of Commands



Command  |     Description  |               Issue         |                                  Priority  | Difficulty| Status
-------  |    -----------    |             -----           |                                --------  | ----------|------
addatom  |
alignxyz  |
bridgeall  |
bridgeit    |  Finds bridging fragments.|   NanoTiler implementation needs improvement.	|  Important|  Easy|       Done
jbridgeit   |  Finds bridging fragments for junctions
optrnagraph |
tracernagraph |
align|
characterize |
children |
clear |
clone |
closest |
check |
chosen |
controller |
define |
demo |
deselect |
dist |
echo |	     Print something |	 | Unimportant |	Easy |
ending |
env |
exit |
exportpdb |
exportpdbsym |
extend |
fit |
foreach |
fusestrands |	Fuses two specified strands | |	Important |	Easy | Done
fuseallstrands | Fuses all strands | | Easy |Done
=======
fuseallstrands | Fuses all strands | | | Easy |Done
genbpconstraint |
genlink |
gendistconstraint |
genjunctiondbconstraint |
genhelix |
genjunction |
genhelixconstraint |
genjunctionconstraint |
genshape |
gentorsionconstraint |
graphics |Rudimentary graphics - not useable for Galaxy | |Unimportant |Difficult
grow |	Take building blocks and connectivity rules to create nanostructures. | | Important |Easy |Done!
growgraph |
help |
history |
import |
links |
loadbasepairs |
loadjunctions |
loadnucleotides |
man |
missinglink |
move |
movetoclosest |
mutate |Mutate sequence  | |Important |Easy |Done!
"#" | Creates comment  | |Unimportant |Easy
optconstraints |
opthelices |Optimizes helix constraints. Consider combining with genhelixconstraint. | |	Important | Difficult |   Done |
optsystematic |
orient |
physics |Computes some properties (mass etc). Needs more testing. | |	Unimportant | Easy
place |
properties |
quit |
randomize |
remove |
rmlink |
rmdupseq |
rename |
renumber |
resproperty |
rgrow |
ringfixconstraints |
ringfuse |
rotate |
scorefit |
secondary |
seed |
select |
set |
shift |
signature |
source |
splitstrand |
=======
splitstrand | Splits strand before given position |  |  | Easy | Done
status |
steric |
superposable |
symadd |
symapply |
syminfo |
synth |
tile |
tracegraphscript |  Generates a nanotiler script that creates and RNA structure tracing a graph |  | Medium | Done
=======
tracegraphscript |  Generates a nanotiler script that creates and RNA structure tracing a graph | | | Medium | Done
tree |
trim |
twist |
unshift |
verbose |
