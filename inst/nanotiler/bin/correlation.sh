#!/bin/csh
if ($1 == "") then
    echo "Computes p-value of Spearman correlation for set of paired values. Usage: spearman.sh filename [kendall|spearman|pearson]";
    echo "The file contains two values in each line corresponding to value pairs.";
    exit
endif
R --no-save --no-restore --silent --vanilla --args $1 $2 < $NANOTILER_HOME/scripts/correlation.R
# |2 grep -E -v "^>"

