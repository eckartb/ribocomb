#!/bin/csh

if ($2 == "") then
    echo "Multi-sequence Secondary structure prediction based on hxmatch. Usage: hxmatch.sh inputfile outputfile";
    echo "Input: Seequence (alignment) in CLUSTAL format." 
    exit
endif
hxmatch -N < $1 > $2
