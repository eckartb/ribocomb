#!/bin/csh


if ($1 == "") then
 echo "junctioncompare is used to compare two junction scan output files (helix positions are randomized after reading - the output should be the same compared to the wrapper script junctioncompare.sh). Usage: junctioncompare_rand filenamefile | [file1 file2 file3 ...]"
 exit
endif

# The letter "n" stands for "normal". Alternative: "r" (in which case the helix positions and orientations are randomized):
java -ea -Xmx1500m -cp $NANOTILER_HOME/jar/nanotiler.jar rnadesign.rnamodel.JunctionCompare r $*
