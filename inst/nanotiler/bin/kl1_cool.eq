% NUPACK 2.0
% This is matchfold.eq, an output file generated for a "concentrations"
% calculation of equilibrium concentrations.
% For information on contents, see NUPACK manual.
% Time calculation was begun: Thu Apr 16 09:26:51 2009 PST
% Command: /home/knot/bindewae/programs/nupack2.0/bin/concentrations matchfold 
% Initial monomer concentrations:
%   1: 1.000000e-06 Molar
%   2: 1.000000e-06 Molar
%
% Following is the header from the input file (matchfold.cx):
%
%
% Do not change the comments below this line, as they may be read by other programs!
%
% Number of strands: 2
% id sequence
%  1 CAUGGUGAAGUCCACACGCCAUG
%  2 GAACGUGAAGUGGACACGCGUUC
% T = 37
2	0	1	-1.730210e+01	9.377132e-07	
1	1	0	-1.642900e+01	9.377132e-07	
3	1	1	-4.308560e+01	6.228679e-08	
