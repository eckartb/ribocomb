#!/usr/bin/perl

use strict;

if (scalar(@ARGV) != 2) {
    print "Wrapper script for matchfold secondary structure prediction. Usage: matchconc.pl FASTAFILE OUTPUTBASE [concentration]\n"; # STRUCTUREOUTPUTFILE CONCENTRATIONSOUTPUT\n";
    exit(0);
}

my $debugMode = 0;
my $SLASH = "/";
my $base = "matchfold";
my $conc = $ARGV[2];
if (length($conc) == 0) {
    $conc = 1e-6;
}
if (! ($conc > 0) ) {
    die "Concentration must be number greater zero: $conc\n";
}
my $tmpRoot = $ENV{"TMP"};
my $NUPACK = $ENV{"NUPACKHOME"};
if (length($NUPACK) == 0) {
    die "Could not find environment variable NUPACKHOME. Please install NUPACK software and set NUPACKHOME environment variable accordingly.\n";
}
if (length($tmpRoot) == 0) {
    $tmpRoot = "/tmp";
}

my $user = $ENV{"USER"};
if (length($user) == 0) {
    $user = "matchconc";
}

my $tmpDir = $tmpRoot . $SLASH . $user . ".matchconc" . $$ . ".tmp.dir";
# print("Creating directory: $tmpDir\n");
my $nupackcx = $tmpDir . $SLASH . "$base.cx";

# print("Nupack file: $nupackcx\n");
`mkdir -p $tmpDir`;
`matchfold --of 6 -v 0 -T 310.15 --noGU < $ARGV[0] -n 5 > $ARGV[1].ct`;
`matchfold --of 6 -v 0 -T 310.15 --noGU -a -u $nupackcx -c 1 -n 5 < $ARGV[0]`; # do not store all intermediate structures: > $ARGV[1].all`;
# creating sequence file:
chomp(my @seqs = `cat $ARGV[0] | grep -v ">"`);
my $seqFile = $tmpDir . $SLASH . "$base.in";
my $concFile = $tmpDir . $SLASH . "$base.con";
open(SEQFILE, ">$seqFile") or die "Error opening sequence output file: $seqFile\n";
print SEQFILE scalar(@seqs);
open(CONCFILE, ">$concFile") or die "Error opening concentrations output file: $concFile\n";
foreach (@seqs) {
    print SEQFILE "$_\n";
    print CONCFILE "$conc\n";
}
print SEQFILE scalar(@seqs);

close(CONCFILE);
close(SEQFILE);

`pushd $tmpDir; $NUPACK/bin/concentrations $base`;

`cp $tmpDir/$base.eq $ARGV[1].eq`;
# concentration of target complex; last complex corresonds to counter " 1 1 1 1 ... 1 1" : one of each strand
chomp(my $concTarget = `cat $ARGV[1].eq | sort -n | tail -n1 | tr "\t" " " | cut -f13 -d" "`);
my $yield = $concTarget / $conc; 
if ($yield > 1.0) {
    die "Internal error: yield fraction can never be greater than 1.0: $yield, $concTarget, $conc\n";
}

open(OUTFILE, ">$ARGV[1].out") or die "Error opening output file: $ARGV[1].out\n";
print OUTFILE ("# Start concentration: $conc Target concentration: $concTarget Yield fraction: $yield\n");
close(OUTFILE);

if (!$debugMode) {
    # clean up
    `rm $tmpDir/*; rmdir $tmpDir`; # avoid rm -rf
}

