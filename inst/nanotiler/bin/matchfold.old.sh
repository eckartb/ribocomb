#!/bin/csh -v
if ($2 == "") then
    echo "Wrapper script for matchfold secondary structure prediction."
    exit
endif
set tmpseq=matchfold.tmp.seq
$NANOTILER_HOME/bin/alignedit2 -i $1 --verbose 0 --of 34 -o $tmpseq
# Generate stem table:
set stemfile=matchfold.tmp.stemtable
$GA_HOME/scripts/genstemtable.pl $tmpseq > $stemfile

echo "matchfold --of 6 -v 0 -T 300 -s $stemfile < $1 > $2"
matchfold --of 6 -v 0 -T 300 -s $stemfile < $1 > $2
