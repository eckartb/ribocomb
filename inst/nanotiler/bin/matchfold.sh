#!/bin/csh
if ($2 == "") then
    echo "Wrapper script for matchfold secondary structure prediction."
    exit
endif
matchfold --stack $NANOTILER_HOME/prm/stack_r4.prm --of 6 -v 0 -T 310.15 --multi 1.0 < $1 > $2
