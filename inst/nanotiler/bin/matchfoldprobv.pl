#!/usr/bin/perl

# wrapper script for matchfold in probability mode. Expects CT file format with sequence start indices in first row.

use strict;

my $KNETFOLD_HOME = $ENV{"KNETFOLD_HOME"};

my ($ctfile,$outfile) = @ARGV;

chomp(my $startLine = `head -n1 $ctfile | cut -f3- -d" "`);
# chomp(my $numRes = `head -n1 $ctfile | cut -f1 -d" "`);
# print "# Number residues: $numRes\n";

my $tmpfasta = "matchfoldprob.tmp.$$.fa";
`stemconvert -i $ctfile --if 4 --of 22 --starts $startLine > $tmpfasta`;

chomp(my $result = `matchfold -a 5 -d $ctfile --df 4 --stack $KNETFOLD_HOME/prm/stack_r4.prm --multi 1.0 < $tmpfasta`);

`rm $tmpfasta`;

if (length($outfile) == 0) {
    print "$result\n";
} else {
    open(OFILE, ">$outfile") or die "Error opening output file: $outfile\n";
    print OFILE "$result\n";
    close(OFILE);
}

