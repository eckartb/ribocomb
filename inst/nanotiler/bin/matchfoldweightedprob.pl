#!/usr/bin/perl

# wrapper script for matchfold in probability mode. Expects CT file format with sequence start indices in first row.

use strict;

my $KNETFOLD_HOME = $ENV{"NANOTILER_HOME"}; # $ENV{"KNETFOLD_HOME"};

if (scalar(@ARGV) != 3) {
    die "Computes score of sequence given a target structure and a set of weights. Usage: matchfoldweightedprob.pl CTFILE OUTPUTFILE WEIGHTFILE\n";
}

my ($ctfile,$outfile, $weightfile) = @ARGV;

chomp(my $startLine = `head -n1 $ctfile | cut -f3- -d" "`);
# chomp(my $numRes = `head -n1 $ctfile | cut -f1 -d" "`);
# print "# Number residues: $numRes\n";

my $tmpfasta = "matchfoldprob.tmp.$$.fa";
# print "stemconvert -i $ctfile --if 4 --of 22 --starts $startLine > $tmpfasta\n";
`stemconvert -i $ctfile --if 4 --of 22 --starts $startLine | grep -v ">" > $tmpfasta`;
# print("Starting matchfold:\n");

my $debugMode = 0;
my $MATCHFOLD = "/home/bas/bindewae/spvs_projects/knetfold_spvs/develop/knetfold/bin/knot/matchfold_0.9.7";

my $command = "$MATCHFOLD -a 6 -d $ctfile --df 4 --stack $KNETFOLD_HOME/prm/stack_r4.prm --multi 1.0 --if 31 -w $weightfile < $tmpfasta | grep -v \"#\" | tail -n 1";
if ($debugMode) {
    print "$command\n";
}
chomp(my $result = `$MATCHFOLD -a 6 -d $ctfile --df 4 --stack $KNETFOLD_HOME/prm/stack_r4.prm --multi 1.0 --if 31 -w $weightfile < $tmpfasta | grep -v "#" | tail -n 1`);

if (! $debugMode) {
    `rm $tmpfasta`;
}

if (length($outfile) == 0) {
    print "$result\n";
} else {
    open(OFILE, ">$outfile") or die "Error opening output file: $outfile\n";
    print OFILE "$result\n";
    close(OFILE);
}

