#!/bin/csh
if ($2 == "") then
    echo "Launches pknotsRG for a give sequence. Usage: pknotsRG.sh inputfile outputfile"
    exit
endif
cat $1 | grep -v ">" | pknotsRG > $2

