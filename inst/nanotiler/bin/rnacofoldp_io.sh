#!/bin/csh
if ($2 == "") then
    echo "Launches RNAcofold for a give sequence combination (fused with & character). Usage: rnacofoldp_io.sh inputfile outputfile"
    exit
endif
cat $1 | grep -v ">" | RNAcofold -p
if (-e "dot.ps") then
    cp dot.ps $2
    rm dot.ps
else
    echo "Warning: Not probability matrix was generated when launching RNAcofold!"
endif
if (-e "rna.ps") then
    rm rna.ps
endif

