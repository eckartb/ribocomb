#!/bin/csh
if ($2 == "") then
    echo "Launches RNAfold for a give sequence combination (fused with & character). Usage: rnafold_io.sh inputfile outputfile"
    exit
endif
cat $1 | grep -v ">" | RNAfold > $2
rm -f rna.ps
