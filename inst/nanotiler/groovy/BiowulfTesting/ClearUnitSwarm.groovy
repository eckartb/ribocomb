class WriteSwarm{
static void main(String[] args){

/* Write swarm to run clear unit of all pdbs on list */

def nameFile = args[0]

def names = (new File(args[0])).readLines()

def file = new File("launch.swarm")
file.write("")

println("input: "+nameFile)
println("structures: "+names.size())

int total = names.size()
int rec = (total/1000)+1

println("\ntotal: "+total)
println("recommended bundle: "+rec)
println("swarm -f ../launch.swarm -g 4 -b "+rec)

for(int i=0; i<names.size(); i++){

	def infile=names[i]

	file.append("groovy /data/millerjk2/spvs_projects/develop/nanotiler/groovy/ClearUnits.groovy "+infile+"\n")
}

}
}
