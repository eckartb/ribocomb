class Main{
  static void main(String[] args){
  
	/* dictionary exclusion of motifs from test structure */    

def name = args[0]
    def id = args[1]
    def out = args[2]
    def path = "/data/millerjk2/spvs_projects/science/frabase_motifs"
    
    new File(path+"/bulges/2D").eachFile() { file->  
      moveTo(file, name);
    }
    new File(path+"/internalloops/2D").eachFile() { file->  
      moveTo(file, name);
    }
    new File(path+"/hairpins/2D").eachFile() { file->  
      moveTo(file, name);
    }
    
    def proc = ("groovy ../SecToPdb.groovy "+name+" 0.0 "+id+" "+out).execute();
    def sout = new StringBuffer(), serr = new StringBuffer()
    proc.consumeProcessOutput(sout,serr)
    proc.waitFor()
    println(serr)
    println(sout)
    
    new File(path+"/bulges/2D").eachFile() { file->  
      moveOut(file);
    }
    new File(path+"/internalloops/2D").eachFile() { file->  
      moveOut(file);
    }
    new File(path+"/hairpins/2D").eachFile() { file->  
      moveOut(file);
    }
  }
  
  static void moveTo(file, name){
    if(file.getName().contains(name.substring(0,5))){
      def dir = new File(file.getAbsolutePath()+"/temp/")
      file.renameTo(new File(dir,file.getName()))
    }
  }
  static void moveOut(file){
      def dir = new File(file.getAbsolutePath()+"/../")
      file.renameTo(new File(dir,file.getName()))
    }
  
}