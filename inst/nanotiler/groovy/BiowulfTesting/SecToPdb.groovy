import rnadesign.designapp.NanoTilerScripter
import rnadesign.rnacontrol.Object3DGraphController
import rnasecondary.SecondaryStructureTools
import rnasecondary.SecondaryStructure

/* Given a pdb file, it creates the script, runs it, and scores the prediction's rmsd */

class SecToPdb{
  static void main(String[] args){

    // ("sh pwd").execute();
    println("home: "+ System.getProperty("user.dir"))

    Object3DGraphController graph = new Object3DGraphController()
    NanoTilerScripter app = new NanoTilerScripter(graph)
    
    def guideName = args[0] //native
    def repuls = args[1] //constraint type
    def id = args[2] //id
    def outputFile = new File(args[3]) //output file
    def pdbname=(guideName.tokenize("/").last()).tokenize(".")[0]
    def secFileName=pdbname+"_nano_"+repuls+"_"+id+".sec"
    def secFile = new File(secFileName)
    app.runScriptLine("import "+guideName)
    app.runScriptLine("secondary file="+secFileName+" format=sec")
    def outputName = pdbname+"_pred_"+repuls+"_"+id
    def scriptName = outputName+".script"
    def pdbName = outputName+".pdb"
    
    app.runScriptLine("tracesecondary i=" + secFileName + " file=" + outputName + " repuls="+repuls + " steps=500000 datahome=/data/millerjk2/spvs_projects/science/frabase_motifs/")
    if(!new File(scriptName).exists()){
      println("Broken script: "+scriptName)
      (new File(outputName+"_Broken")).append("Broken script: "+scriptName)
      return
    }
    
    // app.runScriptLine("source "+scriptName)
    println("nanoscript "+scriptName)
    def proc = ("nanoscript "+scriptName).execute();
    def sout = new StringBuffer(), serr = new StringBuffer()
    	proc.consumeProcessOutput(sout,serr)
    proc.waitFor()
    println(serr)
    println(sout)
    println("created pdbs")
    // ("sh pwd").execute();
  
    // while(!new File(pdbName).exists())  {}
    if(!new File(pdbName).exists()){
      println("home: "+ System.getProperty("user.dir"))
      println("Broken pdb: "+pdbName)
      (new File(outputName+"_Broken")).append("Broken pdb: "+pdbName)
      return
    }
    
    app.runScriptLine("clear all")
    app.runScriptLine("import "+guideName)
    app.runScriptLine("import "+pdbName)
    def rms = graph.findRms("1.1","1.2")
    println("Done: "+pdbName+" "+repuls+" "+id+" "+rms)
    outputFile.append(pdbName+" "+repuls+" "+id+" "+rms+"\n")
  }
}