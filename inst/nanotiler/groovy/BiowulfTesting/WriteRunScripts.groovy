class WriteRunScripts{
static void main(String[] args){

/* write swarm file along with many nanotiler scripts.
Used for many runs on a single secondary structure input
(used to create triangle, rnapuzzle 17) */

def input = args[0]
def trials = Integer.parseInt(args[1])

def file = new File("launch2.swarm")
file.write("")

println("trials: "+trials)

int total = trials
int rec = (total/1000)+1

println("\ntotal: "+total)
println("recommended bundle: "+rec)
println("swarm -f ../launch.swarm -g 4 -b "+rec)

for(int i=0; i<trials; i++){

        def script = new File("run_"+i+".script")
        script.write("")
        script.append("tracesecondary i="+input+" file="+input+"_pred"+i+" datahome=/data/millerjk2/spvs_projects/science/frabase_motifs/\n")
        script.append("source "+input+"_pred"+i+".script\n")
        
        file.append("nanoscript run_"+i+".script\n")
        
}

}
}