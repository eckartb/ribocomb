class WriteSwarm{
static void main(String[] args){

/* write swarm using dictionary exclusion script */

def nameFile = args[0]
def trials = Integer.parseInt(args[1])
def repuls = Double.parseDouble(args[2])

def names = (new File(args[0])).readLines()

def file = new File("launch.swarm")
file.write("")

println("input: "+nameFile)
println("structures: "+names.size())
println("trials: "+trials)
println("repuls: "+repuls)
println("script: "+"launch.swarm")

int total = names.size()*trials
int rec = (total/1000)+1

println("\ntotal: "+total)
println("recommended bundle: "+rec)
println("swarm -f ../launch.swarm -g 4 -b "+rec)

for(int i=0; i<names.size(); i++){

	def infile=names[i]

	def pdbName = (infile.tokenize("/").last()).tokenize(".")[0]

	for(def j=0; j<trials; j++){
        	file.append("groovy ../MoveReplicates.groovy "+infile+" "+j+" "+pdbName+"_output_"+j+"\n")
	}
}

}
}
