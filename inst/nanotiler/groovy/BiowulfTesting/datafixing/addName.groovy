class WriteSwarm{
static void main(String[] args){

/* given a data file, adds a column with 4 letter pdb code */

def names = (new File(args[0])).readLines()

def file = new File("Result_fixed.txt")
file.write("")

for(int i=0; i<names.size(); i++){

	def line=names[i]
	def pdbName = line.substring(0,4)

        file.append(line+" "+pdbName+"\n")
}

}


}