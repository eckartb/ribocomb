import rnadesign.designapp.NanoTilerScripter
import rnadesign.rnacontrol.Object3DGraphController
import sequence.*;
import rnasecondary.*
import rnasecondary.substructures.*

/* given a data	file, adds a column with residue count */

class WriteSwarm{
static void main(String[] args){

def names = (new File(args[0])).readLines()

def file = new File("Result_fixed_sized.txt")
file.write("")

Map<String,Integer> done = new HashMap<String,Integer>()

for(int i=0; i<names.size(); i++){

	def line=names[i]
	def pdbName = line.substring(0,4)
	int rescount =0;
	if(done.containsKey(pdbName)){
		 rescount = done.get(pdbName);
	} else{
	 rescount = count("/Users/millerjk2/Data/NDBdataset/"+pdbName+".pdb")
	done.put(pdbName,rescount)
	}	
        file.append(line+" "+rescount+"\n")
}

}

public static int count(name){
	Object3DGraphController graph = new Object3DGraphController()
	NanoTilerScripter scripter = new NanoTilerScripter(graph)

	scripter.runScriptLine("import " + name)
	SecondaryStructure _structure = graph.generateSecondaryStructure();
	SecondaryStructure structure = SecondaryStructureTools.cleanCopy(_structure);
	
	int resCount = 0
	for(def i=0; i<structure.getSequenceCount(); i++){
		Sequence seq = structure.getSequence(i)
		resCount+=seq.size()
	}
	return resCount;
}


}