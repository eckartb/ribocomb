import rnadesign.designapp.NanoTilerScripter
import rnadesign.rnacontrol.Object3DGraphController
import sequence.*;
import rnasecondary.*
import rnasecondary.substructures.*

class Main{
  static void main(String[] args){
    clear(args[0])
  }
  
  public static void clear(name){
    
    Object3DGraphController graph = new Object3DGraphController()
    NanoTilerScripter scripter = new NanoTilerScripter(graph)

    scripter.runScriptLine("import " + name)
    SecondaryStructure structure = graph.generateSecondaryStructure()
    
    List<Residue> unitRes = SecondaryStructureTools.findConnectedResidues(structure)
    graph.removeResidues(unitRes,false,false)
    
    scripter.runScriptLine("exportpdb "+name.tokenize(".")[0]+"_unit.pdb")
    
  }
}