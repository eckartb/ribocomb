class ContstraintTestWrapper{
  static void main(String[] args){

    def minCon = 0; //lowest constraint
    def maxCon = 10; //highest constraint
    def inc = 1; //increment from min to max
    def trials = 10; //repeats for each constraint level
    
    def infile=args[0]
    def outfile=args[1]
    def wd = System.getProperty("user.dir")
    def pbsName = "/users/n2001762559/millerjk2/nanotiler_folding/motifs/runSecToPdb.pbs"
    def path = infile.tokenize(".")[0]
    def name = path.tokenize("/").last();

    for(def con=minCon; con<=maxCon; con+=inc){
      for(def t=0; t<trials; t++){
        def scriptName = name+"_"+con+"_"+t;
        println("qsub "+pbsName+" -v infile="+infile+",type="+con+",id="+t+",outfile="+scriptName+".txt,wd="+wd+" -N "+scriptName)
        def proc = ("qsub "+pbsName+" -v infile="+infile+",type="+con+",id="+t+",outfile="+scriptName+".txt,wd="+wd+" -N "+scriptName).execute()
        def sout = new StringBuffer(), serr = new StringBuffer()
    		proc.consumeProcessOutput(sout,serr)
        proc.waitFor()
        println(sout)
        println(serr)
      }
    }
  }
}