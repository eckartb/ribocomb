import rnadesign.designapp.NanoTilerScripter
import rnadesign.rnacontrol.Object3DGraphController
import sequence.*;
import rnasecondary.*
import rnasecondary.substructures.*

class Main{
  static void main(String[] args){
    println(count(args[0]))
  }
  
  public static int count(name){
    Object3DGraphController graph = new Object3DGraphController()
    NanoTilerScripter scripter = new NanoTilerScripter(graph)

    scripter.runScriptLine("import " + name)
    SecondaryStructure _structure = graph.generateSecondaryStructure();
    SecondaryStructure structure = SecondaryStructureTools.cleanCopy(_structure);
    
    int resCount = 0
    for(def i=0; i<structure.getSequenceCount(); i++){
      Sequence seq = structure.getSequence(i)
      resCount+=seq.size()
    }
    return resCount;
  }
}