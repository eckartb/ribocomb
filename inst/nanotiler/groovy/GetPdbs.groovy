class GetPdbs{
static void main(String[] args){
  
def pdbUrl = "http://www.rcsb.org/pdb/files/"

assert args.length ==  2
def idFile_ = args[0]
def dir_ = args[1]
def idFile = new File(idFile_)
assert idFile.exists()
def dir = new File(dir_)
assert dir.exists()

def pdbIds = idFile.readLines()

def pdbSize = pdbIds.size()
println ( pdbSize + "  pdb ids")

for ( def i=0; i<pdbSize; i++){ //download pdb's
  def pdbId = pdbIds[i]
  def url = pdbUrl + pdbId + ".pdb"
  def pdbFile = new File(dir_ + "/" + pdbId + ".pdb")
  if( pdbFile.createNewFile() ){
    println ( i + ": getting file: " + pdbId)
    
    def fout = new FileOutputStream(pdbFile)
    def out = new BufferedOutputStream(fout)
    out << new URL(url).openStream()
    out.close()
    
    println ( "recieved file: " + pdbId)
  } else{
    println (i + ": file already exists: " + pdbId)
  }
}

}
}