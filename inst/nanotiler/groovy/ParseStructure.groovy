import rnadesign.designapp.NanoTilerScripter
import rnadesign.rnacontrol.Object3DGraphController
import sequence.*;
import rnasecondary.*
import rnasecondary.substructures.*

class Main{
  static void main(String[] args){
    
    List<Substructure> finders = new ArrayList<Substructure>();
    List<String> names = new ArrayList<String>();
    // finders.add(new FindBulge());
    // names.add("bulge");
    // finders.add(new FindHairpin());
    // names.add("hairpin");
    // finders.add(new FindInternalLoop());
    // names.add("internalloops");
    // assert finders.size()==names.size();
    finders.add(new FindJunction());
    names.add("junction");
    
    for(int i=0; i<args.size(); i++){
      for(int j=0; j<names.size(); j++){
          get(args[i], finders.get(j), names.get(j));
        }
    }
    
    // if(args.length==1){
    //   for(int i=0; i<names.size(); i++){
    //     get(args[0], finders.get(i), names.get(i));
    //   }
    // } else if(args.length==2){
    //   int index = args[1].toInteger();
    //   println(index);
    //   println("finding "+names.get(index) +" in " + args[0])
    //   get(args[0], finders.get(index), names.get(index));
    // }

  }
  
  static void get(file, finder, type){
    Object3DGraphController graph = new Object3DGraphController()
    NanoTilerScripter scripter = new NanoTilerScripter(graph)
    println("importing "+file)
try{
    scripter.runScriptLine("import " + file)
} catch(Exception e){
  println(e)
}    
SecondaryStructure _structure = graph.generateSecondaryStructure();
    SecondaryStructure structure = SecondaryStructureTools.cleanCopy(_structure); 

    //def list = SecondaryStructureTools.FindHairpinResidues(structure);
    finder.run(structure)
    def list = finder.getResidues();
    // def secList = SecondaryStructureTools.FindHairpins(structure);
    println("Substructure count: "+list.size())
    for(int i=0; i<list.size(); i++){
      println("Residues: "+list[i].size())
      // SecondaryStructureWriter writer = new SecondaryStructureScriptFormatWriter();
      // System.out.println(writer.writeString(secList[i]));
      scripter.runScriptLine("clear all")
      try{
      scripter.runScriptLine("import " + file)
      } catch(Exception e){
println(e)}      
//println("check");
      structure = graph.generateSecondaryStructure()
      InteractionSet interactions = structure.getInteractions();
      HashMap<Residue,Residue> pairs = new HashMap<Residue,Residue>();
    	for (int j=0; j<interactions.size(); j++) {
    		Interaction inter = interactions.get(j);
    		pairs.put( inter.getResidue1(), inter.getResidue2() );
    		pairs.put( inter.getResidue2(), inter.getResidue1() );
    	}
      HashMap<Residue,Residue> clones = new HashMap<Residue,Residue>();

      int num = structure.getSequenceCount();
      UnevenAlignment ua = new SimpleUnevenAlignment();
      for(int k=0;k<num;k++){
        Sequence seq = structure.getSequence(k);
        Sequence newSeq = new SimpleSequence("strand_"+k, seq.getAlphabet());
        for(int j=0;j<seq.size();j++){
          Residue res = seq.getResidue(j);
          Residue newRes = new SimpleResidue(res.getSymbol(), newSeq, res.getPos());
          System.out.println(res.getClass());
          System.out.println(newRes.getClass());
          clones.put(res,newRes);
          clones.put(newRes,res);
          newSeq.addResidue(newRes);
        }
        try{
          ua.addSequence(newSeq);
        } catch (Exception e){
          System.out.println(e);
        }
      }
      InteractionSet newInteractions = new SimpleInteractionSet();
      for(Residue res : pairs.keySet()){
        newInteractions.add(new SimpleInteraction( clones.get(res), clones.get(pairs.get(res)) , new RnaInteractionType(RnaInteractionType.WATSON_CRICK)));
      }
      finder.run(new SimpleSecondaryStructure(ua,newInteractions));
      def smallList = finder.getResidues();
      for(int a=0; a<smallList[i].size(); a++){
        smallList[i][a]=clones.get(smallList[i][a])
      }
      graph.removeResidues( smallList[i], false, true );
      def name_type = type;
      if(type.equals("junction")){
        int count = finder.getStructures().get(i).getSequenceCount()
        name_type=count+"-way_"+type
      }
      SecondaryStructureWriter writer = new SecondaryStructureScriptFormatWriter();
      String out = writer.writeString(finder.getStructures().get(i));
      System.out.println(out);
      def name = file.tokenize('.')[0] + "_" + name_type + i +".pdb"
      scripter.runScriptLine("exportpdb " + name)
      ("pdbfilter " + name + " " + name).execute()
    }
  }
}