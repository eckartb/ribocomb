import rnadesign.designapp.NanoTilerScripter
import rnadesign.rnacontrol.Object3DGraphController
import rnasecondary.SecondaryStructureTools
import rnasecondary.SecondaryStructure

class SecToPdb{
  static void main(String[] args){
    
    int trials=10
    int inc=0.2

    // ("sh pwd").execute();
    println("home: "+ System.getProperty("user.dir"))

    Object3DGraphController graph = new Object3DGraphController()
    NanoTilerScripter app = new NanoTilerScripter(graph)
    
    def guideName = args[0] //native
    def index = args[1] //constraint type
    def outputFile = new File(args[2]) //output file
    def repuls=index*inc/trials
    def id=index%trials
    def secFileName=guideName.tokenize(".")[0]+"_nano_"+repuls+"_"+id+".sec"
    def secFile = new File(secFileName)
    app.runScriptLine("import "+guideName)
    app.runScriptLine("secondary file="+secFileName+" format=sec")
    def outputName = guideName.tokenize(".")[0]+"_pred_"+repuls+"_"+id
    def scriptName = outputName+".script"
    def pdbName = outputName+".pdb"
    
    app.runScriptLine("tracesecondary i=" + secFileName + " file=" + outputName + " repuls="+repuls + " steps=500000")
    if(!new File(scriptName).exists()){
      println("Broken script: "+scriptName)
      (new File(outputName+"_Broken")).append("Broken script: "+scriptName)
      return
    }
    
    // app.runScriptLine("source "+scriptName)
    println("nanoscript "+scriptName)
    def proc = ("nanoscript "+scriptName).execute();
    def sout = new StringBuffer(), serr = new StringBuffer()
		proc.consumeProcessOutput(sout,serr)
    proc.waitFor()
    println(serr)
    println(sout)
    println("created pdbs")
    // ("sh pwd").execute();
  
    // while(!new File(pdbName).exists())  {}
    if(!new File(pdbName).exists()){
      println("home: "+ System.getProperty("user.dir"))
      println("Broken pdb: "+pdbName)
      (new File(outputName+"_Broken")).append("Broken pdb: "+pdbName)
      return
    }
    
    app.runScriptLine("clear all")
    app.runScriptLine("import "+guideName)
    app.runScriptLine("import "+pdbName)
    def rms = graph.findRms("1.1","1.2")
    println("Done: "+pdbName+" "+repuls+" "+id+" "+rms)
    outputFile.append(pdbName+" "+repuls+" "+id+" "+rms+"\n")
  }
}