import rnadesign.designapp.NanoTilerScripter
import rnadesign.rnacontrol.Object3DGraphController
import rnasecondary.SecondaryStructureTools
import rnasecondary.SecondaryStructure

class Main{
  static void main(String[] args){
    
    // def file = new File("repeats.txt")
    // file.write("")
    def dir = new File("extras")
    def removed = new HashSet()
    
    for(int i=0; i<args.length; i++){
      if(!removed.contains(args[i])){
        println(args[i])
        ("pdbfilter "+args[i]+" "+args[i]).execute();
        Object3DGraphController graph = new Object3DGraphController()
        NanoTilerScripter scripter = new NanoTilerScripter(graph)
        // new File("dupCheck.sec").write("")
        scripter.runScriptLine("import " + args[i])
        scripter.runScriptLine("secondary file=dupCheck.sec format=sec")
        def size = graph.getLinks().getHydrogenBondInteractions().size();
        println("links: " + size);
        
        List struc1 = new ArrayList();
        String[] lines1 = new File("dupCheck.sec").readLines();
        for(String s : lines1){
          if(s.contains("STRUCTURE")){
            struc1.add(s);
          }
        }  
      
        for(int j=i+1; j<args.length; j++){
          if(!removed.contains(args[j])){
            Object3DGraphController graph2 = new Object3DGraphController()
            NanoTilerScripter scripter2 = new NanoTilerScripter(graph2)
            // new File("dupCheck2.sec").write("")
            scripter2.runScriptLine("import " + args[j])
            scripter2.runScriptLine("secondary file=dupCheck2.sec format=sec")
            def size2 = graph2.getLinks().getHydrogenBondInteractions().size();
            println("links: " + size2);
            
            List struc2 = new ArrayList();
            String[] lines2 = new File("dupCheck2.sec").readLines();
            for(String s : lines2){
              if(s.contains("STRUCTURE")){
                struc2.add(s);
              }
            }
            
            println(struc1)
            println(struc2)
            ("pdbfilter "+args[j]+" "+args[j]).execute();
            if(struc1.size() == struc2.size()){
              boolean same = true;
              for(int k=0; k<struc1.size(); k++){
                if(struc1.get(k)!=struc2.get(k)){
                  same = false;
                  break;
                }
              }
              if(same){
                println(args[i]+" and "+args[j]+" are identical");
                File file = new File(args[j]);
                file.renameTo(new File(dir, file.getName()))
                removed.add(args[j])
              }
            }
          }
        }
        
      }
  }}}