class parseCSV{
static void main(String[] args){
  
def pdbUrl = "http://www.rcsb.org/pdb/files/"
def motifAtlasPdbDir = "MotifAtlasPDB/"
def motifDir = "Motifs/"

assert args.length ==  1

def csvFileName = args[0]
def csvFile = new File(csvFileName)
assert csvFile.exists()

def csv = csvFile.text
def motifs = csv.tokenize(">")
def motifsSize = motifs.size()

println(motifsSize + " motifs found in " + csvFileName)

def pdbNames = []

for( def i=0; i<motifsSize; i++){ //find which pdb's are needed
  def motif = motifs[i].tokenize(" ,")
  def pdbCode = motif[1].substring(1,5)
  print (i + ": pdb for " + motif[0] + ": " + pdbCode)
  
  if(!pdbNames.contains(pdbCode)){
    pdbNames.add(pdbCode)
    println (" (new)")
  } else{
    println()
  }
}

def pdbsSize = pdbNames.size()
println ( pdbsSize + " distinct pdb files")

def pdbLocal = new File(motifAtlasPdbDir)
pdbLocal.mkdir()

for ( def i=0; i<pdbsSize; i++){ //download pdb's
  def pdbName = pdbNames[i]
  def url = pdbUrl + pdbName + ".pdb"
  def pdbFile = new File(motifAtlasPdbDir + pdbName + ".pdb")
  if( pdbFile.createNewFile() ){
    println ( i + ": getting file: " + pdbName)
    
    def fout = new FileOutputStream(pdbFile)
    def out = new BufferedOutputStream(fout)
    out << new URL(url).openStream()
    out.close()
    
    println ( "recieved file: " + pdbName)
  } else{
    println (i + ": file already exists: " + pdbName)
  }
}

def motifLocal = new File(motifDir)
motifLocal.mkdir()
def motifFiles = []

for( def i=0; i<motifsSize; i++){ //create motifs from pdb's
  def motif = motifs[i].tokenize(" ,")
  def motifName = motif[0]
  def pdbCode = motif[1].substring(1,5)
  println (i + ": Motif " + motifName + " from " + pdbCode)
  print ("Residues: ")
  def residues = []
  for( def j=1; j<motif.size(); j++){
    def identifier = (motif[j].substring( 1,(motif[j].size()-1) )) .split("\\|") //cut out "", split identifiers
    def res = identifier[4]
    def chain = identifier[2]
    def entry = [res,chain]
    residues.add( entry )
    print (res + "," + chain + " ")
  }
  println ()
  
  def pdbText = (new File(motifAtlasPdbDir + pdbCode + ".pdb")).readLines()
  def motifFileName = motifDir + motifName +"_raw.pdb"
  def motifFile = new File(motifFileName)
  motifFiles.add(motifFile)
  if( motifFile.createNewFile() ){
    for (def j=0; j<pdbText.size(); j++){
      def line = pdbText[j]
      def type = line.substring(0,6)
      if(type!="ATOM  "&&type!="TER   "){
        continue;
      }
      def res = line.substring(22,26).trim()
      def chain = line.substring(21,22)
      def entry = [res,chain]
      if(type == "ATOM  " && !residues.contains( entry ) ){
        continue;
      }
      motifFile.append(line + "\n")
    }
  } else{
    println ("Motif already exists")
  }
  
}


for( def i=0; i<motifFiles.size(); i++){ //filter motifs
  def rawMotifFile = motifFiles.get(i)
  def motif = motifs[i].tokenize(" ,")
  def motifName = motif[0]
  println (i + ": Motif file " + rawMotifFile.getName() + " to " + motifName + ".pdb")
  def pdbText = rawMotifFile.readLines()
  def motifFile = new File(motifDir + motifName +".pdb")
  if( motifFile.createNewFile() ){
    for (def j=0; j<pdbText.size(); j++){
      def line = pdbText[j]
      def type = line.substring(0,6)
      if(type=="TER   "){
        if(j<1){
          continue
        } else if(pdbText[j-1].substring(0,6)=="TER   "){ //second TER in row
          continue;
        }
      }
      // def res = line.substring(22,26).trim()
      // def chain = line.substring(21,22)
      // def entry = [res,chain]
      // if(type == "ATOM  " && !residues.contains( entry ) ){
      //   continue;
      // }
      motifFile.append(line + "\n")
    }
  } else{
    println ("Motif already exists")
  }
  
}



}
}
