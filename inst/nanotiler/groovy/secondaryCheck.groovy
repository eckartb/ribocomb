import rnadesign.designapp.NanoTilerScripter
import rnadesign.rnacontrol.Object3DGraphController
import rnasecondary.SecondaryStructureTools
import rnasecondary.SecondaryStructure

class Main{
  static void main(String[] args){
    
    def file = new File("loops.txt")
    file.write("")
    
    for(int i=0; i<args.length; i++){
      println(args[i])
      ("pdbfilter "+args[i]+" "+args[i]).execute()
      Object3DGraphController graph = new Object3DGraphController()
      NanoTilerScripter scripter = new NanoTilerScripter(graph)
      scripter.runScriptLine("import " + args[i])
      scripter.runScriptLine("secondary")
      def size = graph.getLinks().getHydrogenBondInteractions().size();
      println("links: " + size);
      if(size==2){
        file << args[i]+"\n"
      }
      
    }
  }
}