import rnadesign.designapp.NanoTilerScripter

class Main
{
static void main(String[] args){//tracesecondaryRunner.groovy filename id

	if(args.length!=2){
		print("Usage: tracesecondaryRunner.groovy inputfile [id]")
		assert args.length==1;
	}

	def inputFile = args[0] //sec file
	def id = (args.length==2?args[1]:"") //added to end of filename, use for multiple runs on same molecule
	def predFileName = inputFile.tokenize(".")[0] + "_pred_" + id

	//generate script
	NanoTilerScripter app = new NanoTilerScripter()
	app.runScriptLine("tracesecondary i=" + inputFile + " file=" + predFileName)
	
	def result = false
	def tries = 0
	
	while(! result){
		
		def proc = ("nanoscript " + predFileName + ".script").execute() 
		def sout = new StringBuffer(), serr = new StringBuffer()
		proc.consumeProcessOutput(sout,serr)
		proc.waitFor()

		if( new File(predFileName+".pdb").exists() && sout.contains("Good bye") ){
		    	result = true;
		}

		tries++
		println(sout)
		println(tries + " attempt(s)")
	}
	print("\n\n---------------Done---------------\n")

}
}
