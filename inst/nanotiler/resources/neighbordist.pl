#!/usr/bin/perl

@lines = <STDIN>;
chomp(@lines);
$nn = scalar(@lines);
print "$nn lines read!n";
for ($i = 0; $i < $nn; $i++) {
    $last = $lines[$i];
    for ($j = $i+1; $j < $nn; $j++) {
	$curr = $lines[$j];
	$molid = substr($curr, 23,3);
	$chain = substr($curr, 21, 1);
	$key = substr($curr, 0, 4);
	$atomName = substr($curr, 13, 3); # TODO check
	$x = substr($curr,30,8);
	$y = substr($curr,38,8);
	$z = substr($curr,46,8);
	$oldmolid = substr($last, 23,3);
	$oldchain = substr($last, 21, 1);
	$oldkey = substr($last, 0, 4);
	$oldAtomName = substr($last, 13, 3); # TODO check
	$oldx = substr($last,30,8);
	$oldy = substr($last,38,8);
	$oldz = substr($last,46,8);
	if (($molid - $oldmolid) == 1) {
	    next;
	}
	if ($molid eq $oldmolid) {
	    next;
	}
	if (($key eq "ATOM") && ($oldkey eq "ATOM")) {
	    $dist = sqrt(($oldx-$x)*($oldx-$x) + ($oldy-$y)*($oldy-$y) + ($oldz-$z)*($oldz-$z));
	    if ($dist < 10.0) {
		print "$dist $molid $oldmolid $chain $atomName $oldAtomName $x $y $z $oldx $oldy $oldz\n";
	    }
	}
	$last = $curr;
    }
}
