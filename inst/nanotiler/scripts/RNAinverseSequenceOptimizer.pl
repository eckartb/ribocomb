#!/usr/bin/perl

#################################################
#
# Author: Calvin Grunewald
#
# Date Last Modified: April 5, 2006
#
# Description: This script wraps around the RNAinverse program.  It takes
#   structures that have both secondary (interactions local to the RNA
#   strand) and tertiary (interactions between RNA strands) interactions
#   and generates sequences that could possible fold into the given
#   structure.
#
#################################################

#################################################
# File format information
#
# A file contains information pertaining to different strands.
# Each strand can be specified with a name, structure definition,
# and sequence pattern.
#
# The structure definition defines both the intra-strand and
# inter-strand interactions. The Sequence pattern defines any
# bases that are to remain constant throughout the script.
#
# The syntax for a perl script file is given below
#
# ~STRAND strand-name
# STRUCTURE = (((((..AAAA...)))))
# SEQUENCE  = aaaNNNNNNNNNNNNNuuu
#
# ~STRAND strand-name1
# STRUCTURE = (((((((....AAAA...)))))))
# SEQUENCE  = NNNNNNNNNNNNNNNNNNNNNNNNN
#
# ~STRAND strand-name2
# STRUCTURE = ((((..BBBBBBB..CCCC...))))
# SEQUENCE  = NNNNccNNNNNNNaaNNNNuuuNNNN
#
# ~STRAND strand-name3
# STRUCTURE = .....((((...)))).BBBBBBB..((((..CCCC...))))
# SEQUENCE  = cguacgNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNucga
#
#
# For each binding site that is local to the strand (ie, one part
# of the strand binds with another part of the same strand), the
# base pair should be represented by the character '(' and ')'
# respectively. For base pairs that connect two strands, each connection
# should be assigned its own letter value.  In each strand that shares
# that connection, the bases that bind should share the same letter.
#
# The sequence pattern should have 'N' where the bases do not matter
# and lower case 'c', 'g', 'u', and 'a' where the bases do matter.
#
#
# IMPORTANT NOTE
#
# It is up to the user or program that generates the input file to ensure
#   the file conforms to the correct format.  If the file does not,
#   the scripts response is undefined.
#
###################################################

#important variables
$inputFile = "";     #input file path
$outputFile = "";    #output file path

# run mode
# if $fullSearch is true, then every combination of pairs is tested
# if $fullSearch is false, then random pairs are tested
$fullSearch = 1;

$defaultRnaInversePath = "RNAinverse";    #defualt RNAinverse path
$defaultRnaFoldPath = "RNAfold";          #default RNAfold path

$numberRuns = 1;     #number of RNAinverse runs
$iterations = 1000;     #number of recalculations during each step

$maxHammingDistance = 0;        #maximum haming distance
$maxNumberReCalculations = 2;   #maximum number of recalculations during
                                # re-optimization
$minStrandDistance = 2;         #minimum number needed to do recalculations

$debugFlag = 1;  # print debug info

$tempFile = "temp";  #temporary file control

@secondaryStructures; # hold secondary structure info
@tertiaryStructures;  # hold tertiary structure info
@sequencePatterns;    # hold the sequence patterns
@finalSequences;      # hold the final sequences
@strandNames;         # hold the strand names
$strandCount = 0;     # number of strands

####################################################################################
#
# STEP 1: EVALUATE COMMAND LINE ARGUMENTS
#
# First Argument: Path to the Input File
# Second Argument: Path to the Output File
# Third Optional Argument: Path to the RNAinverse Program (if not set, a default is used)
# Fourth Optional Argument: Path to the RNAfold Program (if not set, a default is used)
#
####################################################################################
if(scalar(@ARGV) == 2) {
    $inputFile = $ARGV[0];
    $outputFile = $ARGV[1];
}

elsif(scalar(@ARGV) == 3) {
    $inputFile = $ARGV[0];
    $outputFile = $ARGV[1];
    $defaultRnaInversePath = $ARGV[2];
}

elsif(scalar(@ARGV) == 4) {
    $inputFile = $ARGV[0];
    $outputFile = $ARGV[1];
    $defaultRnaInversePath = $ARGV[2];
    $defaultRnaFoldPath = $ARGV[3];
}


else {
    print "Syntax Error! Usage: ./runRnaInverse.pl inputFilePath outputFilePath [RNAinverse_path] [RNAfold_path]\n";
    exit(0);
}

########################################################################################
#
# STEP 2: PARSE, FORMAT, AND STORE INPUT
#
########################################################################################


open DUMP, ">scriptDefug.log" or die "Couldn't open dump file\n";
print DUMP "script started\n";
print DUMP "Input file: $inputFile\n";
print DUMP "Output file: $outputFile\n";
close DUMP;




open INPUT, "$inputFile" or die "$inputFile cannot be opened\n";

@input = <INPUT>;


for ($i = 0; $i < scalar(@input); $i++) {
    if($input[$i]=~/~ *STRAND +(.*)/) {
	$strandCount++;
	$strandNames[$strandCount - 1] = $1;
    }

    elsif($input[$i]=~/ *STRUCTURE += +(.*)/) {
	$secondaryStructure = "";
	$tertiaryStructure = "";
	$structure = $1;
	while($structure=~/([\(\)\.]+)|([A-Za-z]+)/g) {
	    if($1) {
		$secondaryStructure = $secondaryStructure . $1;
		$tertiaryStructure = $tertiaryStructure . ("\." x length($1));
	    }

	    elsif($2) {
		$secondaryStructure = $secondaryStructure . ("\." x length($2));
		$tertiaryStructure = $tertiaryStructure . $2;
	    }
	}
	$secondaryStructures[$strandCount - 1] = $secondaryStructure;
	$tertiaryStructures[$strandCount - 1] = $tertiaryStructure;
	
    }

    elsif($input[$i]=~/ *SEQUENCE += +(.*)/) {
	$sequencePatterns[$strandCount - 1] = $1;
	$finalSequences[$strandCount - 1] = $1;
    }

}

&dumpCurrentState();

###################################################################
#
# STEP 3: GENERATE THE INITIAL SEQUENCES FOR EACH RNA STRAND
#
###################################################################
for ($i = 0; $i < $strandCount; $i++) {
    $finalSequences[$i] = &getSequenceRNAInverse(&runRNAInverse($secondaryStructures[$i], $finalSequences[$i]));
}

&dumpCurrentState();

##################################################################
#
# STEP 4: OPTIMIZE SEQUENCES SO UNSPECIFIED INTER-STRAND INTERACTIONS DO NOT OCCUR
#
##################################################################
for ($i = 0; $i < $strandCount; $i++) {

    
    $concatenatedStructure = $secondaryStructures[$i];
    $concatenatedSequence = $finalSequences[$i];
    
    $iters = 0;
    for ($j = 0; $j < $strandCount; $j++) {
	if($j != $i) {
	    $concatenatedStructure .= "." x length($secondaryStructures[$j]);
	    $concatenatedSequence .= lc($finalSequences[$j]);
	}
    }
    
    print "\nConcatenated Structure: $concatenatedStructure\n" if $debugFlag;
    print "Concatenated Sequence:  $concatenatedSequence\n\n" if $debugFlag;
    
    $results = &runRNAInverse($concatenatedStructure, $concatenatedSequence);
    $finalSequences[$i] = substr(&getSequenceRNAInverse($results), 0, length($finalSequences[$i]));

}
	
######################################################################
#
# STEP 5: OPTIMIZE SEQUENCES FOR SPECIFED INTER-STRAND INTERACTIONS
#
######################################################################
$seenInteractions = "";
for ($i = 0; $i < $strandCount; $i++) {

    while($tertiaryStructures[$i]=~/([A-Za-z])/g) {
	
	# make sure that the letter hasn't already been found
	if($seenInteractions=~/$1/) {
	    next;
	}
	
	print "The seen interactions are: ", $seenInteractions, "\n" if $debugFlag;
	
	$currentInteraction = $1;
	$seenInteractions = $seenInteractions . $currentInteraction;
	$firstStructure = $tertiaryStructures[$i];
	$secondStructure = "";
	$firstStructureIndex = $i;
	$secondStructureIndex = -1;
	
	#find the next structure
	for($j = 0; $j < $strandCount; $j++) {
	    
	    # make sure that the same structure isn't used
	    if($j == $i) {
		next;
	    }
	    elsif($tertiaryStructures[$j]=~/$currentInteraction/) {
		$secondStructure = $tertiaryStructures[$j];
		$secondStructureIndex = $j;
		last;
	    }
	    else {
		next;
	    }
	}
	
	if($debugFlag) {
	    print "Interacting Structures\n";
	    print "Current Interaction: ", $currentInteraction, "\n";
	    print "First Structure:  ", $firstStructure, "\n";
	    print "Second Structure: ", $secondStructure, "\n";
	    print "\n";
	}
	
	# format the structures so they can be input into RNAinverse
	$firstStructure=~s/[^\.$currentInteraction]/\./g;
	$secondStructure=~s/[^\.$currentInteraction]/\./g;
	$firstStructure=~s/$currentInteraction/\(/g;
	$secondStructure=~s/$currentInteraction/\)/g;
	$connectingSegment = lc(&createCombiningSequence(5));
	$combinedStructure= $firstStructure . ("\." x length($connectingSegment)) .  $secondStructure;
	
	if($debugFlag) {
	    print "Formatted Structures\n";
	    print "First Structure:    ", $firstStructure, "\n";
	    print "Second Structure:   ", $secondStructure, "\n";
	    print "Combined Structure: ", $combinedStructure, "\n";
	    print "\n";
	}
	
	$firstSequence = "";
	$secondSequence = "";
	
	# format sequence data
	$firstSequence = lc($finalSequences[$firstStructureIndex]);
	$secondSequence = lc($finalSequences[$secondStructureIndex]);
	
	if($debugFlag) {
	    print "First sequence: ", $finalSequences[$firstStructureIndex], "\n";
	    print "Second Sequence: ", $finalSequences[$secondStructureIndex], "\n";
	}
	
	# format first sequence
	$begIndex = index($firstStructure, "\(");
	$endIndex = rindex($firstStructure, "\(");
	$size = $endIndex - $begIndex + 1;
	$randomBase = substr($sequencePatterns[$firstStructureIndex], $begIndex, $size);
	print "Length of random base: ", length($randomBase), "\nSize: $size\n";
	print "Random base: $randomBase\n";
	
	substr($firstSequence, $begIndex, $size) = $randomBase;
	
	# format second sequence
	$begIndex = index($secondStructure, "\)");
	$endIndex = rindex($secondStructure, "\)");
	$size = $endIndex - $begIndex + 1;
	$randomBase = substr($sequencePatterns[$secondStructureIndex], $begIndex, $size);
	
	substr($secondSequence, $begIndex, $size) = $randomBase;
	
	# combine the sequences
	$combinedSequence = $firstSequence . $connectingSegment . $secondSequence;
	
	if($debugFlag) {
	    print "CombinedSequence:  ", $firstSequence . uc($connectingSegment) . $secondSequence, "\n";
	    print "CombinedStructure: ", $firstStructure . ("\." x length($connectingSegment)) . $secondStructure, "\n";
	}	    
	
	$iters = 0;
	$distanceChanged = 0;
	
	for ($k = 0; $k < $strandCount; $k++) {
	    if($k != $firstStructureIndex && $k != $secondStructureIndex) {
		$combinedStructure .= "." x length($finalSequences[$k]);
		$combinedSequence .= lc($finalSequences[$k]);
	    }
	}
	
	
	$result = &runRNAInverse($combinedStructure, $combinedSequence);
	$resultSequence = &getSequenceRNAInverse($result);
	
	$finalSequences[$firstStructureIndex] = uc(substr($resultSequence, 0, length($firstStructure)));
	$finalSequences[$secondStructureIndex] = uc(substr($resultSequence, length($firstStructure) + length($connectingSegment), length($secondStructure)));
	
	
	
    }
}
&dumpCurrentState();

##########################################################################
#
# STEP 6: TEST RESEULTS WITH RNA FOLD AND RE-OPTIMIZE IF SUB-OPTIMAL
#
##########################################################################
$totalScore = 0;

for ($i = 0; $i < $strandCount; $i++) {

    $hammingDistance = 0;

    for ($j = $i + 1; $j < $strandCount; $j++) {
	
	$hammingDistance = &validateSequence($finalSequences[$i] . $finalSequences[$j], $secondaryStructures[$i] . $secondaryStructures[$j], $tertiaryStructures[$i] . $tertiaryStructures[$j]);
	
	print "\n\nThe Hamming Distance is: $hammingDistance\n\n" if $debugFlag;
	
	
	$recalculations = 0;
	while($hammingDistance > $maxHammingDistance && $recalculations < $maxNumberReCalculations) {
	    print "Sequence needs to be re-optimized\n";
	    $seenInteractions  = "";
	    &computeSecondaryStructureSequence($i, $j);
	    $hammingDistance = &validateSequence($finalSequences[$i] . $finalSequences[$j], $secondaryStructures[$i] . $secondaryStructures[$j], $tertiaryStructures[$i] . $tertiaryStructures[$j]);
	    $recalculations++;
	}
	
	$totalScore += $hammingDistance;
    }
}

###########################################################################
#
# STEP 7: WRITE THE FINAL RESULTS TO A FILE
#
###########################################################################
print "GENERATING OUTPUT FILE\n";
open OUTFILE, ">$outputFile" or die "Could not open $outputFile for write.";
for($i = 0; $i < $strandCount; $i++) {
    print OUTFILE $finalSequences[$i], "\n";
    print OUTFILE $secondaryStructures[$i], "\n";
    print OUTFILE $tertiaryStructures[$i], "\n";
    print OUTFILE &compareProbabilities($secondaryStructures[$i], $finalSequences[$i]), "\n\n";
}

print OUTFILE "\n\nTotal Score for Set of Sequences: $totalScore\n"; 
close OUTFILE;

print "DONE\n";

##########################################################################
#
# END OF SCRIPT. BEGIN SUB-ROUTINES
#
#########################################################################




#########################################################################
#
# SUB-ROUTINE: COMPARE PROBABILITIES
#
#########################################################################
sub compareProbabilities {
    $structure = $_[0];
    $sequence = $_[1];

    open TEMPFILE, ">$tempFile" or die "Could not open temp file for write";
    print TEMPFILE $structure, "\n", lc($sequence);

    $rnaInverseResults = `$defaultRnaInversePath -Fp < $tempFile`;

    close TEMPFILE;

    open TEMPFILE, ">$tempFile" or die "Could not open temp file for write";
    print TEMPFILE $sequence, "\n";

    $rnaFoldResults = `$defaultRnaFoldPath -p < $tempFile`;

    close TEMPFILE;

    $rnaInverseProbability = 0;
    $rnaFoldProbability = 0;

    if($rnaInverseResults=~/(?:[ACGUacgu]+) +(?:\d+) +\(([0-9\.e\-]+)\)/) {
	$rnaInverseProbability = $1;
	#print "RNAinverse Probability: $rnaInverseProbability\n" if $debugFlag;
    }

    if($rnaFoldResults=~/ensemble ([0-9\.]+)/) {
	$rnaFoldProbability = $1;
	#print "RNAfold Probability: $rnaFoldProbability\n" if $debugFlag;
    }

    $probabilities = "RNAinverse Probability: " . $rnaInverseProbability . "\nRNAfold Probability: " . $rnaFoldProbability;
}
##########################################################################
#
# SUB-ROUTINE: RE-OPTIMIZE SECONDARY STRUCTURE
#
##########################################################################
sub computeSecondaryStructureSequence {
    $sequenceIndex1 = $_[0];
    $sequenceIndex2 = $_[1];

    $sequence1 = $finalSequences[$sequenceIndex1];
    $sequence2 = $finalSequences[$sequenceIndex2];
    $structure1 = $secondaryStructures[$sequenceIndex1];
    $structure2 = $secondaryStructures[$sequenceIndex2];

    $rnaFoldResults = &runRNAFold($sequence1 . $sequence2);

    print "RNA fold results are: $rnaFoldResults\n\n";

    $rnaFoldStructure1 = substr($rnaFoldResults, 0, length($sequence1));
    $rnaFoldStructure2 = substr($rnaFoldResults, length($sequence1), length($sequence2));

    $fixedSequence1 = "";
    $fixedSequence2 = "";

    for ($inde = 0; $inde < length($sequence1); $inde++) {
	if(substr($structure1, $inde, 1) eq substr($rnaFoldStructure1, $inde, 1)) {
	    $fixedSequence1 .= lc(substr($sequence1, $inde, 1));
	}

	else {
	    $fixedSequence1 .= "N";
	}
    }

    for ($inde = 0; $inde < length($sequence2); $inde++) {
	if(substr($structure2, $inde, 1) eq substr($rnaFoldStructure2, $inde, 1)) {
	    $fixedSequence2 .= lc(substr($sequence2, $inde, 1));
	}
	
	else {
	    $fixedSequence2 .= "N";
	}
    }
	
    if($debugFlag) {
	print "RNAfold Results for Structure 1: ", $rnaFoldStructure1, "\n";
	print "RNAfold Results for Structure 2: ", $rnaFoldStructure2, "\n";
	print "\n";
	print "Fixed Sequence for Structure 1:  ", $fixedSequence1, "\n";
	print "Fixed Sequence for Structure 2:  ", $fixedSequence2, "\n";
    }

    $concatenatedStructure = $structure1 . $structure2;
    $concatenatedSequence = $sequence1 . $sequence2;

    for ($oth = 0; $oth < $strandCount; $oth++) {
	if($oth != $sequenceIndex1 && $oth != $sequenceIndex2) {
	    $concatenatedStructure .= "." x length($secondaryStructures[$oth]);
	    $concatenatedSequence .= lc($finalSequences[$oth]);
	}
    }


    $rnaInverseResults = &runRNAInverse($concatenatedStructure, $concatenatedSequence);

    $finalSequences[$sequenceIndex1] = substr(&getSequenceRNAInverse($rnaInverseResults), 0, length($finalSequences[$sequenceIndex1]));
    $finalSequences[$sequenceIndex2] = substr(&getSequenceRNAInverse($rnaInverseResults), length($finalSequences[$sequenceIndex1]), length($finalSequences[$sequenceIndex2]));
}
###################################################################
#
# SUB-ROUTINE: RUN RNA FOLD
# Parameters: Sequence
#
###################################################################
sub runRNAFold {
    # open temporary file
    open TEMPFILE, ">$tempFile" or die "Could not open temporary file for write.";
    print TEMPFILE $_[0], "\n";

    $rnaFoldResults = `$defaultRnaFoldPath < $tempFile`;
    chomp($rnaFoldResults);

    $rnaStructure = "";

    if($rnaFoldResults=~/([\(\)\.]+)/) {
	$rnaStructure = $1;
    }

    close TEMPFILE;
    print "RNAfold Structure: ", $rnaStructure, "\n";
    $rnaStructure;
}
    

##################################################################
#
# SUB-ROUTINE: VALIDATE SEQUENCE USING RNA FOLD
#
##################################################################
sub validateSequence {
    $rnaFoldStructure = &runRNAFold($_[0]);

    if(length($rnaFoldStructure) == length($_[1])) {
	print "Lengths are the same\n\n" if $debugFlag;
	$hDistance = 0;
	for($ind = 0; $ind < length($rnaFoldStructure); $ind++) {
	    if(substr($rnaFoldStructure, $ind, 1) ne substr($_[1], $ind, 1) and not (substr($_[2], $ind, 1)=~/[A-Za-z]/)) {
		print "Found descrepency at position: $ind\.  The descrepency was: ", substr($rnaFoldStructure, $ind, 1), ' vs. ', substr($_[1], $ind, 1), "\n" if $debugFlag;
		$hDistance++;
	    }
	}
	$hDistance;
    }

    elsif(length($rnaFoldStructure) > length($_[1])) {
	print "Lengths are different\n\n" if $debugFlag;
	$hDistance = 0;
	for($ind = 0; $ind < length($_[1]); $ind++) {
	    if(substr($rnaFoldStructure, $ind, 1) ne substr($_[1], $ind, 1)) {
		print "Found descrepency at position: $ind\.  The descrepency was: ", substr($rnaFoldStructure, $ind, 1), ' vs. ', substr($_[1], $ind, 1), "\n" if $debugFlag;
		$hDistance++;
	    }
	}
	$hDistance += (length($rnaFoldStructure) - length($_[1]));
    }

    else {
	print "Lengths are different\n\n" if $debugFlag;
	$hDistance = 0;
	for($ind = 0; $ind < length($rnaFoldStructure); $ind++) {
	    if(substr($rnaFoldStructure, $ind, 1) ne substr($_[1], $ind, 1)) {
		print "Found descrepency at position: $ind\.  The descrepency was: ", substr($rnaFoldStructure, $ind, 1), ' vs. ', substr($_[1], $ind, 1), "\n" if $debugFlag;
		$hDistance++;
	    }
	}
	$hDistance += (length($_[1]) - length($rnaFoldStructure));
    }
}

###################################################################
#
# SUB-ROUTINE: RUN RNA INVERSE AND RETURN THE RESULTS
# Parameters: Structure, Sequence
#
##################################################################
sub runRNAInverse {
    open RNAINVERSEFILE, ">$tempFile" or die "Could not open temporary file for write";
    print RNAINVERSEFILE $_[0], "\n", $_[1];

    @rnaInverseResults; # hold results for the $numberRuns of RNAinverse

    for($numRun = 0; $numRun < $numberRuns; $numRun++) {
	$rnaInverseResults[$numRun] = `$defaultRnaInversePath -Fp < $tempFile`;
	chomp($rnaInverseResults[$numRun]);
	print "RNAinverse results: ", $rnaInverseResults[$numRun], "\n" if $debugFlag;
    }

    $bestStructure = $rnaInverseResults[0];
    for($numRun = 0; $numRun < ($numberRuns - 1); $numRun++) {
	$bestStructure = &compareRNAInverseResults($bestStructure, $rnaInverseResults[$numRun + 1]);
    }

    $bestStructure;
}

####################################################################
#
# SUB-ROUTINE: COMPARE RNA INVERSE RESULTS
# Parameters: RNAinverse result 1, RNAinverse result 2
#
####################################################################
sub compareRNAInverseResults {
    if(&getProbabilityRNAInverse($_[0]) > &getProbabilityRNAInverse($_[1])) {
	$_[0];
    }
    else {
	$_[1];
    }
}

#####################################################################
#
# SUB-ROUTINE: GET SEQUENCE FROM RNA INVERSE RESULTS
# Parameter: RNAinverse results
#
#####################################################################
sub getSequenceRNAInverse {
    if($_[0]=~/([ACGUacgu]+) +(?:\d+) +\((?:[0-9\.e\-]+)\)/) {
	$1;
    }

}

#####################################################################
#
# SUB-ROUTINE: GET DISTANCE FROM START SEQUENCE FROM RNA INVERSE RESULTS
# Parameter: RNAinverse results
#
#####################################################################
sub getDistanceRNAInverse {
    if($_[0]=~/(?:[ACGUacgu]+) +(\d+) +\((?:[0-9\.e\-]+)\)/) {
	$1;
    }
}

#####################################################################
#
# SUB-ROUTINE: GET PROBABILITY FROM RNA INVERSE RESULTS
# Parameter: RNAinverse results
#
#####################################################################
sub getProbabilityRNAInverse {
    if($_[0]=~/(?:[ACGUacgu]+) +(?:\d+) +\(([0-9\.e\-]+)\)/) {
	$1;
    }
}


####################################################################
#
# SUB-ROUTINE: PRINT STATE INFORMATION TO SCREEN (DEBUG)
#
####################################################################	
sub dumpCurrentState {
    for($i = 0; $i < $strandCount; $i++) {
	print "Strand Name:         ", $strandNames[$i], "\n";
	print "Secondary Structure: ", $secondaryStructures[$i], "\n";
	print "Inter-Strand Inter:  ", $tertiaryStructures[$i], "\n";
	print "Sequence:            ", $finalSequences[$i], "\n";
	print "Sequence Pattern:    ", $sequencePatterns[$i], "\n";
	print "\n\n";
    }
}

####################################################################
#
# SUB-ROUTINE: CREATE A RANDOM SEQUENCE OF SPECIFIED LENGTH
#
####################################################################
sub createCombiningSequence {
    $return = "";
    for($counter = 0; $counter < $_[0]; $counter++) {
	$random = int(rand(100));

	if($random >= 0 && $random < 50) {
	    $return .= "U";
	}

	elsif($random >= 50 && $random < 75) {
	    $return .= "A";
	}

	elsif($random >= 75 && $random < 87) {
	    $return .= "C";
	}

	else {
	    $return .= "G";
	}

    }

    $return;
}
