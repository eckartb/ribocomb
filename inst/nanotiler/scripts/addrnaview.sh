#!/bin/csh

set filename=$1
set mode = $2
if ($filename == "") then
    echo "Script adds RNAview information to PDB file. Usage: addrnaview.sh pdbfile [n]"
    echo "With mode n the script does not repeat the PDB file content"
    exit
endif
if (! -e $filename) then
    echo "Cannot find filename $filename"
    exit
endif

set intermfile="$filename.out"

# call rnaview
rnaview $filename

if (! -e $intermfile) then
  set intermfile="${filename}_nmr.pdb.out"
endif

if (! -e $intermfile) then
    echo "Could not file output file: $intermfile"
    exit
endif

echo "REMARK  Begin of output RNAVIEW"
cat $intermfile | $PERL_GOODIES/string2front.pl "REMARK  "
echo "REMARK  End of output RNAVIEW"
# if mode == "n", only print rnaview lines
if ($mode != "n") then
    cat $filename
endif


