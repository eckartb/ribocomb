#!/usr/bin/perl

# computes mean sequence similarity between two junctions (attempts best one-one one assignment of sequences)

use strict;

if (scalar(@ARGV) != 2) {
    die "Usage: junction2meansimilarity.pl junction1 junction2\n";
}

my ($junctionFileName1, $junctionFileName2) = @ARGV;

my $goodies = $ENV{"PERL_GOODIES"};
my $FASTA2MEAN = "$goodies/fasta2meansimilarity.pl";

if (! -e $FASTA2MEAN) {
    die "Could not find mean similary script: $FASTA2MEAN\n";
}

# convert to uneven alignments:

my @sequences1 = &generateJunctionSequences($junctionFileName1);
my @sequences2 = &generateJunctionSequences($junctionFileName2);

my $n1 = scalar(@sequences1);
my $n2 = scalar(@sequences2);

print "Number of sequences $n1 $n2\n";

if ($n1 != $n2) {
    print "Junctions belong to different types! Sequence similarity meaningless.\n";
    exit(0);
}

my $aliFile1 = "ali1.tmp.fasta";
my $aliFile2 = "ali2.tmp.fasta";

&writeSequences($aliFile1, @sequences1);
&writeSequences($aliFile2, @sequences2);

my $result = `$FASTA2MEAN $aliFile1 $aliFile2`;

print "$FASTA2MEAN $aliFile1 $aliFile2\n";

# clean up
`rm $aliFile1`;
`rm $aliFile2`;

print "$result\n";

############################################################################################################
#                              SUBROUTINES
############################################################################################################

sub generateJunctionSequences {
    my ($fileName) = @_;
    chomp(my @sequences = `grep SQ $fileName | grep -v SQLEN | grep -v SQ2 | tr "=" " " | tr -s " " | cut -f2 -d" "`);
    print "grep SQ $fileName | grep -v SQLEN | grep -v SQ2 | tr \"=\" \" \" | tr -s \" \" | cut -f2 -d\" \"\n";
    return @sequences;
}

sub writeSequences {
    my ($fileName, @sequences) = @_;
    open(FILE, ">$fileName") or die "Error opening file: $fileName\n";
    foreach (@sequences) {
	print FILE "$_\n";
    }
    close(FILE);
}
