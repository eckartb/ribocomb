#!/usr/bin/perl

use strict;

my ($perc, @filenames) = @ARGV;

if (scalar(@ARGV) < 3) {
    die "Generate non-redundant list of junctions. Usage: junction2nonredundant.pl percentage filename1 filename2 ...\n";
}
my @sofar;

my $NANOTILER_HOME = $ENV{"NANOTILER_HOME"};
my $SIMSCRIPT = "$NANOTILER_HOME/scripts/junction2meansimilarity.pl";

if (! -e $SIMSCRIPT) {
    die "Could not find similarity script: $SIMSCRIPT\n";
}

print "$filenames[0]\n"; # first is always part of solution
push(@sofar, $filenames[0]);

for (my $i = 1; $i < scalar(@filenames); $i++) {
    if (&isNonRedundant($perc, $filenames[$i], @sofar) == 1) {
	print "$filenames[$i]\n";
	push(@sofar, $filenames[$i]);
    }

}


#####################################################


sub isNonRedundant {
    my ($perc, $filename, @sofar) = @_;

    foreach my $currname (@sofar) {
	if (&isNonRedundantPair($perc, $filename, $currname) != 1) {
	    return 0; # hit found
	}
    }
    return 1;
}


sub isNonRedundantPair {
    my ($perc, $filename1, $filename2) = @_;
#    print "$SIMSCRIPT $filename1 $filename2 | grep Averager | cut -f4 -d\" \"\n";
    chomp(my $result = `$SIMSCRIPT $filename1 $filename2 | grep Average | cut -f5 -d" "`);
    if (length($result) == 0) {
#	print "oops!\n";
#	print "$SIMSCRIPT $filename1 $filename2 | grep Average | cut -f5 -d\" \"\n";
	return 1; # not computation possible
    }
    if ($result >= $perc) {
#	print "Hit found: $filename1 $filename2 $result $perc\n";
	return 0;
    }
#    print "Everything ok: between $filename1 filename2: $result $perc\n";
    return 1;
}
