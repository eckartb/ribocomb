#!/usr/bin/perl

#################################################
#
# Author: Calvin Grunewald, Eckart Bindewald
#
# History: initial version April 5, 2006 (Calvin)
#          redesign :      Feb 2006 (Eckart)
#
# Description: This script wraps around the RNAinverse program.  It takes
#   structures that have both secondary (interactions local to the RNA
#   strand) and tertiary (interactions between RNA strands) interactions
#   and generates sequences that could possible fold into the given
#   structure.
#
#################################################

#################################################
# File format information
#
# A file contains information pertaining to different strands.
# Each strand can be specified with a name, structure definition,
# and sequence pattern.
#
# The structure definition defines both the intra-strand and
# inter-strand interactions. The Sequence pattern defines any
# bases that are to remain constant throughout the script.
#
# The syntax for a perl script file is given below
#
# ~STRAND strand-name
# STRUCTURE = (((((..AAAA...)))))
# SEQUENCE  = aaaNNNNNNNNNNNNNuuu
#
# ~STRAND strand-name1
# STRUCTURE = (((((((....AAAA...)))))))
# SEQUENCE  = NNNNNNNNNNNNNNNNNNNNNNNNN
#
# ~STRAND strand-name2
# STRUCTURE = ((((..BBBBBBB..CCCC...))))
# SEQUENCE  = NNNNccNNNNNNNaaNNNNuuuNNNN
#
# ~STRAND strand-name3
# STRUCTURE = .....((((...)))).BBBBBBB..((((..CCCC...))))
# SEQUENCE  = cguacgNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNucga
#
#
# For each binding site that is local to the strand (ie, one part
# of the strand binds with another part of the same strand), the
# base pair should be represented by the character '(' and ')'
# respectively. For base pairs that connect two strands, each connection
# should be assigned its own letter value.  In each strand that shares
# that connection, the bases that bind should share the same letter.
#
# The sequence pattern should have 'N' where the bases do not matter
# and lower case 'c', 'g', 'u', and 'a' where the bases do matter.
#
#
# IMPORTANT NOTE
#
# It is up to the user or program that generates the input file to ensure
#   the file conforms to the correct format.  If the file does not,
#   the scripts response is undefined.
#
###################################################

use strict;

my $RNA_ALPHABET = "ACGU";
my $DNA_ALPHABET = "ACGT";

#important variables
my $inputFile = "";     #input file path
my $outputFile = "";    #output file path
my $alphabet = $RNA_ALPHABET;
# run mode
# if $fullSearch is true, then every combination of pairs is tested
# if $fullSearch is false, then random pairs are tested
my $fullSearch = 1;
my $keepFixedProb = 0.9;
my $mismatchLimit = 0; # currently: do not allow any mismatches
my $defaultRnaInversePath = "RNAinverse";    #defualt RNAinverse path
my $defaultRnaFoldPath = "RNAfold";          #default RNAfold path

my $numberRuns = 1;     #number of RNAinverse runs
my $iterations = 100000;     #number of recalculations during each step

my $maxHammingDistance = 0;        #maximum haming distance
my $maxNumberReCalculations = 2;   #maximum number of recalculations during
                                # re-optimization
my $minStrandDistance = 2;         #minimum number needed to do recalculations

my $debugFlag = 1;  # print debug info

my $tempFile = "temp";  #temporary file control

my @secondaryStructures; # hold secondary structure info
my @tertiaryStructures;  # hold tertiary structure info
my @sequencePatterns;    # hold the sequence patterns
my @finalSequences;      # hold the final sequences
my @strandNames;         # hold the strand names
my $strandCount = 0;     # number of strands

####################################################################################
#
# STEP 1: EVALUATE COMMAND LINE ARGUMENTS
#
# First Argument: Path to the Input File
# Second Argument: Path to the Output File
# Third Optional Argument: Path to the RNAinverse Program (if not set, a default is used)
# Fourth Optional Argument: Path to the RNAfold Program (if not set, a default is used)
#
####################################################################################
if(scalar(@ARGV) == 2) {
    $inputFile = $ARGV[0];
    $outputFile = $ARGV[1];
}

elsif(scalar(@ARGV) == 3) {
    $inputFile = $ARGV[0];
    $outputFile = $ARGV[1];
    $defaultRnaInversePath = $ARGV[2];
}

elsif(scalar(@ARGV) == 4) {
    $inputFile = $ARGV[0];
    $outputFile = $ARGV[1];
    $defaultRnaInversePath = $ARGV[2];
    $defaultRnaFoldPath = $ARGV[3];
}
else {
    print "Syntax Error! Usage: ./runRnaInverse.pl inputFilePath outputFilePath [RNAinverse_path] [RNAfold_path]\n";
    exit(0);
}

########################################################################################
#
# STEP 2: PARSE, FORMAT, AND STORE INPUT
#
########################################################################################

open DUMP, ">scriptDefug.log" or die "Couldn't open dump file\n";
print DUMP "script started\n";
print DUMP "Input file: $inputFile\n";
print DUMP "Output file: $outputFile\n";
close DUMP;

open INPUT, "$inputFile" or die "$inputFile cannot be opened\n";

my @input = <INPUT>;

for (my $i = 0; $i < scalar(@input); $i++) {
    if($input[$i]=~/~ *STRAND +(.*)/) {
	$strandCount++;
	$strandNames[$strandCount - 1] = $1;
    }

    elsif($input[$i]=~/ *STRUCTURE += +(.*)/) {
	my $secondaryStructure = "";
	my $tertiaryStructure = "";
	my $structure = $1;
	while($structure=~/([\(\)\.]+)|([A-Za-z]+)/g) {
	    if($1) {
		$secondaryStructure = $secondaryStructure . $1;
		$tertiaryStructure = $tertiaryStructure . ("\." x length($1));
	    }

	    elsif($2) {
		$secondaryStructure = $secondaryStructure . ("\." x length($2));
		$tertiaryStructure = $tertiaryStructure . $2;
	    }
	}
	$secondaryStructures[$strandCount - 1] = $secondaryStructure;
	$tertiaryStructures[$strandCount - 1] = $tertiaryStructure;
	
    }

    elsif($input[$i]=~/ *SEQUENCE += +(.*)/) {
	$sequencePatterns[$strandCount - 1] = $1;
	$finalSequences[$strandCount - 1] = $1;
    }

}

&dumpCurrentState();

if ($strandCount < 1) {
    print "No sequences are specified, quitting script after initial optimization!\n";
    exit(0);
}

###################################################################
#
# STEP 2b: RANDOMIZE THE INITIAL SEQUENCES FOR EACH RNA STRAND WHERE APPROPRIATE
#
###################################################################
print "Randomizing unspecified parts of sequences:\n";
for (my $i = 0; $i < $strandCount; $i++) {
#    $finalSequences[$i] = &randomizeUnspecified($finalSequences[$i], $alphabet, $keepFixedProb);
}

print "Result of step 2b: \n";

&dumpCurrentState();


###################################################################
#
# STEP 3: GENERATE THE INITIAL SEQUENCES FOR EACH RNA STRAND
#
###################################################################

# print "Starting step 3: generating initial sequences for each RNA strand\n";

# for (my $i = 0; $i < $strandCount; $i++) {
#    $finalSequences[$i] = &getSequenceRNAInverse(&runRNAInverse($secondaryStructures[$i], $finalSequences[$i]));
# }

# print "Result of step 3: \n";

# &dumpCurrentState();

if ($strandCount < 2) {
    print "Only one sequence is specified, stopping here because there are no inter-strand interactions to be optimized.\n";
    exit(0);
}


##################################################################
#
# STEP 4: OPTIMIZE SEQUENCES SO UNSPECIFIED INTER-STRAND INTERACTIONS DO NOT OCCUR
#
##################################################################
# for ($i = 0; $i < $strandCount; $i++) {

    
#    $concatenatedStructure = $secondaryStructures[$i];
#    $concatenatedSequence = $finalSequences[$i];
#    
#    $iters = 0;
##    for ($j = 0; $j < $strandCount; $j++) {
###	if($j != $i) {
#	    $concatenatedStructure .= "." x length($secondaryStructures[$j]);
#	    $concatenatedSequence .= lc($finalSequences[$j]);
#	}
#    }
#    
#    print "\nConcatenated Structure: $concatenatedStructure\n" if $debugFlag;
#    print "Concatenated Sequence:  $concatenatedSequence\n\n" if $debugFlag;
#    
#    $results = &runRNAInverse($concatenatedStructure, $concatenatedSequence);
#    $finalSequences[$i] = substr(&getSequenceRNAInverse($results), 0, length($finalSequences[$i]));
#
# }



######################################################################
#
# STEP 4: OPTIMIZE SEQUENCE PAIRS
#
######################################################################

my $bestMismatches = &computeMismatchScore();

print "Initial mismatch score: $bestMismatches\n";

exit;

my $lastId1 = -1;
my $lastId2 = -1;
for (my $i = 0; $i < $iterations; $i++) {
    my $id1 = 0; # int(rand $strandCount);
    my $id2 = 0; # int(rand $strandCount);
    do {
	$id1 = int(rand $strandCount);
	$id2 = int(rand $strandCount);
    }
    while (($id1 == $id2));

    my $firstSequence = $finalSequences[$id1];
    my $firstSecStructure = $secondaryStructures[$id1];
    my $firstTertStructure = $tertiaryStructures[$id1];
    my $secondSequence = $finalSequences[$id2];
    my $secondSecStructure = $secondaryStructures[$id2];
    my $secondTertStructure = $tertiaryStructures[$id2];
    my $commInt = &findInteractionLetters($firstTertStructure, $secondTertStructure);
    my $firstPattern = $sequencePatterns[$id1];
    my $secondPattern = $sequencePatterns[$id2];
    if (length($commInt) == 0) {
	next;
    }
    print "Sequence pair before randomizing constantness:\n$firstSequence\n$secondSequence\n";

    $firstSequence = &randomizeConstantness($firstSequence, $firstPattern, $keepFixedProb);
    $secondSequence = &randomizeConstantness($secondSequence, $secondPattern, $keepFixedProb);
    my $connectingSegment = lc(&createCombiningSequence(5));
    print "Iteration $i : Optimizing sequence pair: $id1 $id2\n";
    print "Optimizing common interaction $commInt in sequence structure pair:\n$firstSequence\n$firstSecStructure\n$firstTertStructure\n$firstPattern\nand\n$secondSequence\n$secondSecStructure\n$secondTertStructure\n$secondPattern\n";
#    my @result = &optimizeSequencePair($commInt,
##				       $connectingSegment,
#				       $firstSequence, 
#				       $firstSecStructure,
#				       $firstTertStructure,
#				       $secondSequence,
#				       $secondSecStructure,
#				       $secondTertStructure);
#    print "Result of optimizing sequence structure pair:\n$result[0]\n$result[1]\n";

    my $saveSeq1 = $finalSequences[$id1];
    my $saveSeq2 = $finalSequences[$id2];
    my $combinedSeq = $firstSequence . $connectingSegment . $secondSequence;
    my $rnafoldTestStruct = &runRNAFold($combinedSeq);
    print "Test structure:\n$rnafoldTestStruct\n";
    $finalSequences[$id1] = $firstSequence;
    $finalSequences[$id2] = $secondSequence;

    # check overall matching score: 
    my $newMismatches = &computeMismatchScore();
    print "New mismatches: $newMismatches\n";
    if ($newMismatches > $bestMismatches) {
	print "Reverting to previous sequence pair because number of mismatches has increased!\n";
	# revert: 
	$finalSequences[$id1] = $saveSeq1;
	$finalSequences[$id2] = $saveSeq2;
    }
    else {
	print "Accepting step!\n";
	$bestMismatches = $newMismatches;
    }
    if ($newMismatches <= $mismatchLimit) {
	print "Limit for RNAfold mismatches reached! Quitting\n";
	last;
    }
}


# taken out step 5!	

##########################################################################
#
# STEP 6: TEST RESEULTS WITH RNA FOLD AND RE-OPTIMIZE IF SUB-OPTIMAL
#
##########################################################################
# my $totalScore = 0;

# for (my $i = 0; $i < $strandCount; $i++) {

 #   my $hammingDistance = 0;

#    for (my $j = $i + 1; $j < $strandCount; $j++) {
	
#	$hammingDistance = &validateSequence($finalSequences[$i] . $finalSequences[$j], $secondaryStructures[$i] . $secondaryStructures[$j], $tertiaryStructures[$i] . $tertiaryStructures[$j]);
#	
#	print "\n\nThe Hamming Distance is: $hammingDistance\n\n" if $debugFlag;
	
	
#	my $recalculations = 0;
#	while($hammingDistance > $maxHammingDistance && $recalculations < $maxNumberReCalculations) {
#	    print "Sequence needs to be re-optimized\n";
#	    my $seenInteractions  = "";
#	    &computeSecondaryStructureSequence($i, $j);
#	    $hammingDistance = &validateSequence($finalSequences[$i] . $finalSequences[$j], $secondaryStructures[$i] . $secondaryStructures[$j], $tertiaryStructures[$i] . $tertiaryStructures[$j]);
#	    $recalculations++;
#	}
	
#	$totalScore += $hammingDistance;
#    }
# }

###########################################################################
#
# STEP 7: WRITE THE FINAL RESULTS TO A FILE
#
###########################################################################
print "GENERATING OUTPUT FILE\n";
open OUTFILE, ">$outputFile" or die "Could not open $outputFile for write.";
for(my $i = 0; $i < $strandCount; $i++) {
    print OUTFILE $finalSequences[$i], "\n";
    print OUTFILE $secondaryStructures[$i], "\n";
    print OUTFILE $tertiaryStructures[$i], "\n";
    print OUTFILE &compareProbabilities($secondaryStructures[$i], $finalSequences[$i]), "\n\n";
}

print OUTFILE "\n\nTotal Score for Set of Sequences: $bestMismatches\n"; 
close OUTFILE;

print "DONE\n";

##########################################################################
#
# END OF SCRIPT. BEGIN SUB-ROUTINES
#
#########################################################################




#########################################################################
#
# SUB-ROUTINE: COMPARE PROBABILITIES
#
#########################################################################
sub compareProbabilities {
    my $structure = $_[0];
    my $sequence = $_[1];

    open TEMPFILE, ">$tempFile" or die "Could not open temp file for write";
    print TEMPFILE $structure, "\n", lc($sequence);

    my $rnaInverseResults = `$defaultRnaInversePath -Fp < $tempFile`;

    close TEMPFILE;

    open TEMPFILE, ">$tempFile" or die "Could not open temp file for write";
    print TEMPFILE $sequence, "\n";

    my $rnaFoldResults = `$defaultRnaFoldPath -p < $tempFile`;

    close TEMPFILE;

    my $rnaInverseProbability = 0;
    my $rnaFoldProbability = 0;

    if($rnaInverseResults=~/(?:[ACGUacgu]+) +(?:\d+) +\(([0-9\.e\-]+)\)/) {
	$rnaInverseProbability = $1;
	#print "RNAinverse Probability: $rnaInverseProbability\n" if $debugFlag;
    }

    if($rnaFoldResults=~/ensemble ([0-9\.]+)/) {
	$rnaFoldProbability = $1;
	#print "RNAfold Probability: $rnaFoldProbability\n" if $debugFlag;
    }

    my $probabilities = "RNAinverse Probability: " . $rnaInverseProbability . "\nRNAfold Probability: " . $rnaFoldProbability;
    return $probabilities;
}


##########################################################################
#
# SUB-ROUTINE: RE-OPTIMIZE SECONDARY STRUCTURE
#
##########################################################################
sub computeSecondaryStructureSequence {
    my $sequenceIndex1 = $_[0];
    my $sequenceIndex2 = $_[1];

    my $sequence1 = $finalSequences[$sequenceIndex1];
    my $sequence2 = $finalSequences[$sequenceIndex2];
    my $structure1 = $secondaryStructures[$sequenceIndex1];
    my $structure2 = $secondaryStructures[$sequenceIndex2];

    my $rnaFoldResults = &runRNAFold($sequence1 . $sequence2);

    print "RNA fold results are: $rnaFoldResults\n\n";

    my $rnaFoldStructure1 = substr($rnaFoldResults, 0, length($sequence1));
    my $rnaFoldStructure2 = substr($rnaFoldResults, length($sequence1), length($sequence2));

    my $fixedSequence1 = "";
    my $fixedSequence2 = "";

    for (my $inde = 0; $inde < length($sequence1); $inde++) {
	if(substr($structure1, $inde, 1) eq substr($rnaFoldStructure1, $inde, 1)) {
	    $fixedSequence1 .= lc(substr($sequence1, $inde, 1));
	}

	else {
	    $fixedSequence1 .= "N";
	}
    }

    for (my $inde = 0; $inde < length($sequence2); $inde++) {
	if(substr($structure2, $inde, 1) eq substr($rnaFoldStructure2, $inde, 1)) {
	    $fixedSequence2 .= lc(substr($sequence2, $inde, 1));
	}
	
	else {
	    $fixedSequence2 .= "N";
	}
    }
	
    if($debugFlag) {
	print "RNAfold Results for Structure 1: ", $rnaFoldStructure1, "\n";
	print "RNAfold Results for Structure 2: ", $rnaFoldStructure2, "\n";
	print "\n";
	print "Fixed Sequence for Structure 1:  ", $fixedSequence1, "\n";
	print "Fixed Sequence for Structure 2:  ", $fixedSequence2, "\n";
    }

    my $concatenatedStructure = $structure1 . $structure2;
    my $concatenatedSequence = $sequence1 . $sequence2;

#    for ($oth = 0; $oth < $strandCount; $oth++) {
#	if($oth != $sequenceIndex1 && $oth != $sequenceIndex2) {
#	    $concatenatedStructure .= "." x length($secondaryStructures[$oth]);
#	    $concatenatedSequence .= lc($finalSequences[$oth]);
#	}
#    }

    my $rnaInverseResults = &runRNAInverse($concatenatedStructure, $concatenatedSequence);

    $finalSequences[$sequenceIndex1] = substr(&getSequenceRNAInverse($rnaInverseResults), 0, length($finalSequences[$sequenceIndex1]));
    $finalSequences[$sequenceIndex2] = substr(&getSequenceRNAInverse($rnaInverseResults), length($finalSequences[$sequenceIndex1]), length($finalSequences[$sequenceIndex2]));
}

###################################################################
#
# SUB-ROUTINE: RUN RNA FOLD
# Parameters: Sequence
#
###################################################################
sub runRNAFold {
    # open temporary file
    open TEMPFILE, ">$tempFile" or die "Could not open temporary file for write.";
    print TEMPFILE $_[0], "\n";

    my $rnaFoldResults = `$defaultRnaFoldPath < $tempFile`;
    chomp($rnaFoldResults);

    my $rnaStructure = "";

    if($rnaFoldResults=~/([\(\)\.]+)/) {
	$rnaStructure = $1;
    }

    close TEMPFILE;
    # print "RNAfold Structure: ", $rnaStructure, "\n";
    $rnaStructure;
}
 
sub optimizeSequencePair   
{
    if (scalar(@_) != 8) {
	die "Internal error in optimizeSequencePair!\n";
    }
    my ($commonInt, $connectingSegment,
	$firstSequence, $firstSec, $firstTert,
	$secondSequence, $secondSec, $secondTert) = @_;
    print "Called optimizeSequencepair with:\n$commonInt\n$firstSequence\n$firstSec\n$firstTert\n$secondSequence\n$secondSec\n$secondTert\n";

    # format the structures so they can be input into RNAinverse
    my $firstStructure = $firstSec;
    my $secondStructure = $secondSec;
    # my $connectingSegment = lc(&createCombiningSequence(5));

    my $interactions = &findInteractionLetters($firstTert, $secondTert);
    for (my $i = 0; $i < length($interactions); $i++) {
	my $currentInteraction = substr($interactions, $i, 1);
	if (length($currentInteraction) != 1) {
	    die "Internal error in optimizeSequencePair(2)\n";
	}
	$firstTert =~s/$currentInteraction/\(/g; # global replace "A" or "B" with "("
	$secondTert =~s/$currentInteraction/\)/g; # global replace "A" or "B" with ")"
    }
    for (my $i = 0; $i < length($firstStructure); $i++) {
	if (substr($firstTert, $i, 1) eq "(") {
	    substr($firstStructure, $i, 1) = "(";
	}
	if (substr($secondTert, $i, 1) eq ")") {
	    substr($secondStructure, $i, 1) = ")";
	}
    }
    for (my $i = 0; $i < length($secondStructure); $i++) {
	if (substr($secondTert, $i, 1) eq ")") {
	    substr($secondStructure, $i, 1) = ")";
	}
    }
    my $combinedStructure= $firstStructure . ("\." x length($connectingSegment)) .  $secondStructure;
    my $combinedSequence = $firstSequence . $connectingSegment . $secondSequence;

    my $result = &runRNAInverse($combinedStructure, $combinedSequence);
    my $resultSequence = &getSequenceRNAInverse($result);

#    $finalSequences[$id1] = uc(substr($resultSequence, 0, length($firstStructure)));
#    $finalSequences[$id2] = uc(substr($resultSequence, length($firstStructure) + length($connectingSegment), length($secondStructure)));    
    my @result;
    my $res1 = substr($resultSequence, 0, length($firstStructure));
    my $res2 = substr($resultSequence, length($firstStructure) + length($connectingSegment), length($secondStructure));    
    push(@result, $res1);
    push(@result, $res2);
    return @result;
}

# returns the interaction characters of two structures. For example ...AAA....BBB....CC....  and ..BBB....AAA... returns AB;
sub findInteractionLetters {
    my ($struct1, $struct2) = @_;
    my $result = "";
    my $offs = ord("A");
    for (my $cid = 0; $cid < 26; $cid++) {
	my $c = chr($cid + $offs); 
	if (($struct1 =~ /$c/) && ($struct2 =~ /$c/)) {
	    # found!
	    $result = $result . $c;
	}
    }
    return $result;
}

##################################################################
#
# SUB-ROUTINE: VALIDATE SEQUENCE USING RNA FOLD
#
##################################################################
sub validateSequence {
    my $rnaFoldStructure = &runRNAFold($_[0]);
    my $hDistance = 0;
    if(length($rnaFoldStructure) == length($_[1])) {
#	print "Lengths are the same\n\n" if $debugFlag;
	my $hDistance = 0;
	for(my $ind = 0; $ind < length($rnaFoldStructure); $ind++) {
	    if(substr($rnaFoldStructure, $ind, 1) ne substr($_[1], $ind, 1) and not (substr($_[2], $ind, 1)=~/[A-Za-z]/)) {
		# print "Found descrepency at position: $ind\.  The descrepency was: ", substr($rnaFoldStructure, $ind, 1), ' vs. ', substr($_[1], $ind, 1), "\n" if $debugFlag;
		$hDistance++;
	    }
	}
	$hDistance;
    }

    elsif(length($rnaFoldStructure) > length($_[1])) {
	print "Lengths are different\n\n" if $debugFlag;
	$hDistance = 0;
	for(my $ind = 0; $ind < length($_[1]); $ind++) {
	    if(substr($rnaFoldStructure, $ind, 1) ne substr($_[1], $ind, 1)) {
		print "Found descrepency at position: $ind\.  The descrepency was: ", substr($rnaFoldStructure, $ind, 1), ' vs. ', substr($_[1], $ind, 1), "\n" if $debugFlag;
		$hDistance++;
	    }
	}
	$hDistance += (length($rnaFoldStructure) - length($_[1]));
    }

    else {
	print "Lengths are different\n\n" if $debugFlag;
	$hDistance = 0;
	for(my $ind = 0; $ind < length($rnaFoldStructure); $ind++) {
	    if(substr($rnaFoldStructure, $ind, 1) ne substr($_[1], $ind, 1)) {
		print "Found descrepency at position: $ind\.  The descrepency was: ", substr($rnaFoldStructure, $ind, 1), ' vs. ', substr($_[1], $ind, 1), "\n" if $debugFlag;
		$hDistance++;
	    }
	}
	$hDistance += (length($_[1]) - length($rnaFoldStructure));
    }
}


###################################################################
#
# SUB-ROUTINE: RUN RNA INVERSE AND RETURN THE RESULTS
# Parameters: Structure, Sequence
# must already be in bracket notation
##################################################################
sub runRNAInverse {
    open RNAINVERSEFILE, ">$tempFile" or die "Could not open temporary file for write";
    print RNAINVERSEFILE $_[0], "\n", $_[1];
    close(RNAINVERSEFILE);
    my @rnaInverseResults; # hold results for the $numberRuns of RNAinverse
    print "Running RNAinverse with\n$_[0]\n$_[1]\n";
    for(my $numRun = 0; $numRun < $numberRuns; $numRun++) {
	if ($debugFlag == 1) {
	    print "$defaultRnaInversePath -Fp < $tempFile\n";
	}
	$rnaInverseResults[$numRun] = `$defaultRnaInversePath -Fp < $tempFile`;
	chomp($rnaInverseResults[$numRun]);
	print "RNAinverse results:\n", $rnaInverseResults[$numRun], "\n" if $debugFlag;
    }

    my $bestStructure = $rnaInverseResults[0];
    for(my $numRun = 0; $numRun < ($numberRuns - 1); $numRun++) {
	$bestStructure = &compareRNAInverseResults($bestStructure, $rnaInverseResults[$numRun + 1]);
    }

    $bestStructure;
}

####################################################################
#
# SUB-ROUTINE: COMPARE RNA INVERSE RESULTS
# Parameters: RNAinverse result 1, RNAinverse result 2
#
####################################################################
sub compareRNAInverseResults {
    if(&getProbabilityRNAInverse($_[0]) > &getProbabilityRNAInverse($_[1])) {
	$_[0];
    }
    else {
	$_[1];
    }
}

#####################################################################
#
# SUB-ROUTINE: GET SEQUENCE FROM RNA INVERSE RESULTS
# Parameter: RNAinverse results
#
#####################################################################
sub getSequenceRNAInverse {
    if($_[0]=~/([ACGUacgu]+) +(?:\d+) +\((?:[0-9\.e\-]+)\)/) {
	$1;
    }

}

#####################################################################
#
# SUB-ROUTINE: GET DISTANCE FROM START SEQUENCE FROM RNA INVERSE RESULTS
# Parameter: RNAinverse results
#
#####################################################################
sub getDistanceRNAInverse {
    if($_[0]=~/(?:[ACGUacgu]+) +(\d+) +\((?:[0-9\.e\-]+)\)/) {
	$1;
    }
}

#####################################################################
#
# SUB-ROUTINE: GET PROBABILITY FROM RNA INVERSE RESULTS
# Parameter: RNAinverse results
#
#####################################################################
sub getProbabilityRNAInverse {
    if($_[0]=~/(?:[ACGUacgu]+) +(?:\d+) +\(([0-9\.e\-]+)\)/) {
	$1;
    }
}


####################################################################
#
# SUB-ROUTINE: PRINT STATE INFORMATION TO SCREEN (DEBUG)
#
####################################################################	
sub dumpCurrentState {
    for(my $i = 0; $i < $strandCount; $i++) {
	print "Strand Name:         ", $strandNames[$i], "\n";
	print "Secondary Structure: ", $secondaryStructures[$i], "\n";
	print "Inter-Strand Inter:  ", $tertiaryStructures[$i], "\n";
	print "Sequence:            ", $finalSequences[$i], "\n";
	print "Sequence Pattern:    ", $sequencePatterns[$i], "\n";
	print "\n\n";
    }
}

####################################################################
#
# SUB-ROUTINE: CREATE A RANDOM SEQUENCE OF SPECIFIED LENGTH
#
####################################################################
sub createCombiningSequence {
    my $return = "";
    for(my $counter = 0; $counter < $_[0]; $counter++) {
	my $random = int(rand(100));

	if($random >= 0 && $random < 50) {
	    $return .= "U";
	}

	elsif($random >= 50 && $random < 75) {
	    $return .= "A";
	}

	elsif($random >= 75 && $random < 87) {
	    $return .= "C";
	}

	else {
	    $return .= "G";
	}

    }

    $return;
}

sub randomizeUnspecified {
    if (scalar(@_) != 3) {
	die "Internal error in randomizeUnspecified!\n";
    }
    my ($seq, $alphabet, $keepFixedProb) = @_;
    print "Randomizing: $seq with alphabet $alphabet\n";
    if (length($alphabet) < 1) {
	die "Internal error in randomizeUnspecified: no alphabet specified!\n";
    }
    my $result = $seq;
    if (length($result) != length($seq)) {
	die "Internal error in line randomizeUnspecified!\n";
    }
    for (my $i = 0; $i < length($result); $i++) {
	my $base = substr($result, $i, 1);
	if ($base eq uc($base)) {
	    if (length($alphabet) < 1) {
		die "Internal error in randomizeUnspecified: no alphabet specified (3)!\n";
	    }
	    my $c = &generateRandomBase($alphabet);
	    if (length($c) != 1) {
		die "Internal error in randomizeUnspecified\n";
	    }
	    substr($result, $i, 1) = $c;
	}
    }
    if (length($result) != length($seq)) {
	die "Internal error in line randomizeUnspecified!\n";
    }
    print "Result of randomizing: $result\n";
    return $result;
}

#sub randomBaseChar() {
#    my $r = int((rand 1) * 4.0);
###    switch ($r) {
	##case 0: return "A";
	#case 1: return "C";
	#case 2: return "G";
	#case 3: return "U";
   # }
   # die "Internal error!";
   # return "N";
# }

# shuffle what bases of "N" section are to be kept constant
sub randomizeConstantness {
    if (scalar(@_) != 3) {
	die "Internal error in randomizeConstantness!\n";
    }
    my ($seq, $pattern, $keepFixedProb) = @_;
    my $seqOrig = $seq;
    my $mutCount = 0;
    my $mutProb = 1.0-$keepFixedProb;
    my $mutMax = length($seq) * $mutProb;
    for (my $i = 0; $i < length($seq); $i++) {
	my $c = substr($pattern, $i, 1);
	if ($c eq uc($c)) { # check if upper case
	    if ((rand(1) < $keepFixedProb) || ($mutCount >= $mutMax)) {
#		substr($seq, $i, 1) = lc(substr($seq, $i, 1)); # keep fixed, do nothing!
	    }
	    else { # mutate
		$mutCount++;
		substr($seq, $i, 1) = &generateRandomBase($alphabet); # uc(substr($seq, $i, 1));
	    }
	}
    }
    if (($mutCount > 0) && ($seq eq $seqOrig)) {
	print "Warning: no bases mutated in randomizeConstantnes!\n";
    }
    print "original sequence: " . $seqOrig;
    print "result of mutated sequence: " . $seq;
    return $seq;
}

sub generateRandomBase {
    if (scalar(@_) == 0) {
	die "Expected alphabet in generateRandomBase!\n";
    }
    my $alphabet = $_[0];
    if (length($alphabet) < 1) {
	die "Internal error in generateRandomBase: no alphabet specified!\n";
    }
    my $r = int(rand(length($alphabet)));
    print "Random number: $r\n";
    my $result = substr($alphabet, $r, 1);
    if (length($result) != 1) {
	die "Internal error in generateRandomBase(2) $result with alphabet $alphabet\n";
    }
    return $result;
}


##########################################################################
#
# Computes number of mismatches base pairs
#
##########################################################################
sub computeMismatchScore {
    die "Buggy version of computeMismatchScore\n";
    my $totalScore = 0;
    for (my $i = 0; $i < $strandCount; $i++) {	
	my $hammingDistance = 0;
	for (my $j = $i + 1; $j < $strandCount; $j++) {
	    $hammingDistance = &validateSequence($finalSequences[$i] . $finalSequences[$j], $secondaryStructures[$i] . $secondaryStructures[$j], $tertiaryStructures[$i] . $tertiaryStructures[$j]);
	    
	}
	$totalScore += $hammingDistance;
    }
    return $totalScore;
}


sub computeMismatchScore2 {
    my $totalScore = 0;
    for (my $i = 0; $i < $strandCount; $i++) {	
	my $hammingDistance = 0;
	for (my $j = $i + 1; $j < $strandCount; $j++) {
	    $totalScore += &coumtMismatches($finalSequences[$i] . $finalSequences[$j], $secondaryStructures[$i] . $secondaryStructures[$j], $tertiaryStructures[$i] . $tertiaryStructures[$j]);
	    
	}
    }
    return $totalScore;
}

sub countMismatches {

    my ($finalSequence1, $finalSequence2,
	$secondaryStructures1, $secondaryStructure2,
	$tertiaryStructure1, $tertiaryStructure2) = @_;
    
}
