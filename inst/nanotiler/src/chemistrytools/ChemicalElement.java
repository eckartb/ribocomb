package chemistrytools;

/** represents a chemical element.
 * Two elements are the same, if their name, protein and neutron count
 * are identical 
 */
public interface ChemicalElement {

    public Object clone();

    /* return name like "Helium" for helium */
    public String getName();

    /** return string like "He" for helium */
    public String getShortName();

    /** returns mass */
    public double getMass();

    /** would be 6 for Carbon */
    public int getProtonCount();

    /** would be 6 for Carbon */
    public int getNeutronCount();

    /** overwrites the Object.equals method */
    public boolean equals(Object other);

}
