package chemistrytools;

/** Describes chemical composition of a compound, equivalent to "H2O"
 * for water. The interface defines the amounts of the atoms 
 * as double type, so that alloeys like Fe0.9Ni0.1 are possible
 */
public interface Composition {

    /** returns number of different elements */
    int getElementCount();

    /** returns n'th chemical element */
    ChemicalElement getElement(int n);

    /** adds chemical elements with certain amount */
    void addElement(ChemicalElement element, double amount);

    /** returns amount of n'th chemical element, like "2" for "H" in H2O */
    double getElementAmount(int n);

    /** returns number of atoms in compound, like "3" for "H" in H2O */
    double getElemenTotalAmount();

    /** returns total molecular mass of compound, like "18" for H2O */
    double getTotalMass();

}
