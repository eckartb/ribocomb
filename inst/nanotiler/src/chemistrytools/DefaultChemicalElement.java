package chemistrytools;

/** represents a chemical element.
 * Two elements are the same, if their name, protein and neutron count
 * are identical 
 *
 * TODO Class only supports short name property right now
 * TODO Need to fully implement class
 */
public class DefaultChemicalElement implements ChemicalElement {


    private String shortName;

    /** uses element name according to official periodic table of elements including upper case;
     * for example pass "He" for Helium
     */
    public DefaultChemicalElement(String shortName) {
	this.shortName = shortName;
    }

    public Object clone() {
	return new DefaultChemicalElement(shortName);
    }

    /* return name like "Helium" */
    public String getName() {
	return shortName;
    }

    /** return string like "He" */
    public String getShortName() {
	return shortName;
    }

    /** returns mass */
    public double getMass() {
	return 0.0;
    }

    /** would be 6 for Carbon */
    public int getProtonCount() {
	return 0;
    }

    /** would be 6 for Carbon */
    public int getNeutronCount() {
	return 0;
    }

    /** overwrites the Object.equals method */
    public boolean equals(Object other) {
	if(other instanceof ChemicalElement) {
	    ChemicalElement e = (ChemicalElement) other;
	    return e.getProtonCount() == getProtonCount() &&
		e.getName().equals(getName()) && 
		e.getShortName().equals(getShortName());

	}

	return false;

    }

}
