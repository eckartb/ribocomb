package chemistrytools;

import tools3d.Vector3D;

public interface LatticeDescriptor {

    /** returns one of the 230 different space group */
    SpaceGroup getSpaceGroup();

    /** defines space group */
    void setSpaceGroup(SpaceGroup group);

    /** returns number of atoms defined in unit cell */
    int getAtomCount();

    /** returns n'th atom. Returns null if point is just abstract point */
    ChemicalElement getAtomElement(int n);
    
    /** returns position of n'th atom */
    Vector3D getAtomPosition(int n);

    /** adds a certain chemical element at certain position. 
     * Null is possible also for the chemical element.
     */
    void addAtom(ChemicalElement element, Vector3D position);

    /** returns dimensions of unit cells. */
    Vector3D getUnitCellDimensionX();

    /** returns dimensions of unit cells. */
    Vector3D getUnitCellDimensionY();

    /** returns dimensions of unit cells. */
    Vector3D getUnitCellDimensionZ();

    /** sets unit cell dimensions */
    void setUnitCellDimensions(Vector3D x, Vector3D y, Vector3D z);
    
}
