package chemistrytools;

public interface MaterialDescriptor {
    
    Composition getComposition();

    void setComposition(Composition comp);

    LatticeDescriptor getLattice();

    void setLattice(LatticeDescriptor lattice);

}
