package chemistrytools;

/** acts as "database" for chemical elements.
 * Allows to generate element object (representing for example Carbon 
 * for a given full name (Carbon, carbon, CARBON) or short name ("C")
 */
public interface PeriodicTable {

    /** adds a chemical element to the table */
    public void add(ChemicalElement element);

    /** return Helium object for given "He" */
    public ChemicalElement findElementShortName(String shortName);

    /** return Helium object for given "Helium" */
    public ChemicalElement findElementName(String shortName);

    /** returns number of defined elements */
    public int size();
}
