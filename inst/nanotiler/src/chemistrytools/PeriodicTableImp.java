package chemistrytools;

/** acts as "database" for chemical elements.
 * Allows to generate element object (representing for example Carbon 
 * for a given full name (Carbon, carbon, CARBON) or short name ("C")
 * TODO : not fully implemented
 */
public class PeriodicTableImp implements PeriodicTable {

    /** adds a chemical element to the table */
    public void add(ChemicalElement element) { }

    /** return Helium object for given "He" */
    public ChemicalElement findElementShortName(String shortName) { return null; }

    /** return Helium object for given "Helium" */
    public ChemicalElement findElementName(String shortName) { return null; }

    /** returns number of defined elements */
    public int size() { return 0; }

    /** generates a chemical element according to valency */
    public static ChemicalElement generateDefaultChemicalElementByValency(int valency) {
	switch (valency) {
	case 1: return new DefaultChemicalElement("H");
	case 2: return new DefaultChemicalElement("O");
	case 3: return new DefaultChemicalElement("N");
	case 4: return new DefaultChemicalElement("C");
	case 5: return new DefaultChemicalElement("P");
	default:
	    System.out.println("Chemical element with valency " + valency + " is not supported!");
	    assert false;
	}
	return null;
    }

    /** finds valency of atom */
    public static int findDefaultValency(ChemicalElement element) {
	if (element.getShortName().equals("H")) {
	    return 1;
	}
	else if (element.getShortName().equals("O")) {
	    return 2;
	}
	else if (element.getShortName().equals("S")) {
	    return 2;
	}
	else if (element.getShortName().equals("N")) {
	    return 3;
	}
	else if (element.getShortName().equals("C")) {
	    return 4;
	}
	else if (element.getShortName().equals("P")) {
	    return 5;
	}
	assert false; 
	return -1; // element not found
    }
}
