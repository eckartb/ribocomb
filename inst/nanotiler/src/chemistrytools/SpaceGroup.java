package chemistrytools;

/** Defines on of 230 space groups. 
 * TODO : incomplete definition !
 */
public interface SpaceGroup {

    /** returns index of space group */
    int getIndex();

    /** returns name of space group */
    String getName();

    /** returns true if one of the 14 Bravais lattices */
    boolean isBravais();

}
