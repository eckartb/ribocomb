package commandtools;

import java.util.Iterator;
import java.util.Vector;
import java.util.Properties;
import java.util.logging.Logger;

/** Handles interpreteting of any language! Only necessary to add known commands properly. Actual text parsing has to be done by sub-class */
public abstract class AbstractInterpreter implements Interpreter {

    protected static Logger log;

    protected static Properties variables = new Properties();

    private Vector<Command> knownCommands = new Vector<Command>();

    public AbstractInterpreter(Logger log) {
	this.log = log;
    }

    public void addKnownCommand(Command command) {
	knownCommands.add(command);
    }

    public void clear() {
	knownCommands.clear();
    }

    /** returns number of known commands */
    public int countKnownCommands() { return knownCommands.size(); }

    /** returns list of command names */
    public String getKnownCommandNames() {
	StringBuffer buf = new StringBuffer();
	for (int i = 0; i < knownCommands.size(); ++i) {
	    if (i > 0) {
		buf.append(" ");
	    }
	    buf.append(knownCommands.get(i).getName());
	}
	return buf.toString();
    }

    /** returns command from a given name, returns null if not found.
     * TODO : use HashMap instead of linear search! */
    protected Command findCommand(String commandName) {
	Iterator<Command> i = knownCommands.iterator();
	while (i.hasNext()){
	    Command currCommand = i.next();
	    if (currCommand.getName().equals(commandName)) {
		return currCommand;
	    }
	}
	return null; // not found
    }

    /** Returns the command that begins with the given string. If no commands or multiple commands match, returns only the input string */
    public String autoComplete(String s) {
	if (s == null || s.length() == 0) return "";
	String completed = null;
	int len = s.length();

	Iterator<Command> i = knownCommands.iterator();
	while(i.hasNext()) {
	    String command = i.next().getName();

	    if (command.length() >= len && command.substring(0,len).equals(s)) {
		if (completed == null) completed = command;
		else return s;
	    }
	}
	if (completed == null) return s;
	else return completed;
    }

    /** returns all variables */
    public Properties getVariables() { return variables; }

    /** returns value of variable */
    public String getVariable(String name) {
	return variables.getProperty(name);
    }

    /** sets variable to a value */
    public void setVariable(String name, String value) {
	variables.setProperty(name, value);
    }

    /** Returns help string */
    public String getCommandNames() {
	String result = "";
	Iterator<Command> iter = knownCommands.iterator();
	while (iter.hasNext()) {
	    result = result + " " + iter.next().getName();
	}
	return result;
    }

}
