package commandtools;

/** represents command that consists of a set of other commands. 
 * @see AtomicCommand
 * @see Command
 */
public abstract class AbstractAtomicCommand extends AbstractCommand implements AtomicCommand {

    public AbstractAtomicCommand(String name) {
	super(name);
    }

    public void addParameter(Command c) { assert false; }

    /** overwrites getParameter method from abstract command */
    public Command getParameter(int n) { assert false; return null; }

    public int getParameterCount() { return 0; }

}
