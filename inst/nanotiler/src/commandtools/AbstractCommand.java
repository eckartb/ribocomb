package commandtools;

import generaltools.ParsingException;
import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.*;
import java.util.List;
import java.util.Properties;
import static commandtools.PackageConstants.*;

/** represents command that consists of a set of other commands. 
 * @see AtomicCommand
 * @see Command
 */
public abstract class AbstractCommand implements Command {

    protected String name;
    
    protected static Logger log = Logger.getLogger("general_debug");

    protected Properties resultProperties = new Properties();

    private List<Command> parameters = new ArrayList<Command>();

    public AbstractCommand(String name) {
	this.name = name;
    }

    public void addParameter(Command command) {
	this.parameters.add(command);
    }

    /** returns true if all parameter names are ok */
    public boolean checkParameterNames() {
	for (int i = 0; i < getParameterCount(); ++i) {
	    if (! isAllowedParameterName(getParameterName(i))) {
		log.warning("Bad parameter name found: " + getParameterName(i));
		return false;
	    }
	}
	return true;
    }

    public String getShortHelpText() { return "Sorry, short help of this command not yet implemented"; }

    public String getLongHelpText() { return "Sorry, long help of this command not yet implemented"; }

    /** returns name of command like "move", or "exit" */
    public String getName() { return name; }

    /** returns number of parameters */
    public int getParameterCount() { return parameters.size(); }

    /** returns n'th parameter */
    public Command getParameter(int n) {
	assert n < getParameterCount();
	return parameters.get(n); 
    }

    /** returns n'th parameter */
    public Command getParameter(String name) {
	for (int i = 0; i < getParameterCount(); ++i) {
	    if (getParameterName(i).equals(name)) {
		return getParameter(i);
	    }
	}
	return null;
    }

    /** returns result properties. Its values are potentially filled with parameters that depend on the implementation of individual commands */
    public Properties getResultProperties() { return this.resultProperties; }

    /** gets name of n't parameter (use for named parameters like "x=5") */
    public String getParameterName(int n) { return parameters.get(n).getName(); }

    /** returns true if parameter name is allowed; this default implementation always returns true! */
    public boolean isAllowedParameterName(String name) {
	return true;
    }

    /** convinience method for returning parsed parameter */
    public int parse(String paramName, int defaultValue) throws NumberFormatException {
	StringParameter param = (StringParameter)(getParameter(paramName));
	if (param == null) {
	    return defaultValue;
	}
	return Integer.parseInt(param.getValue());
    }

    /** convinience method for returning parsed parameter */
    public double parse(String paramName, double defaultValue) throws NumberFormatException {
	StringParameter param = (StringParameter)(getParameter(paramName));
	if (param == null) {
	    return defaultValue;
	}
	return Double.parseDouble(param.getValue());
    }

    /** convinience method for returning parsed parameter */
    public boolean parse(String paramName, boolean defaultValue) throws ParsingException {
	StringParameter param = (StringParameter)(getParameter(paramName));
	if (param == null) {
	    return defaultValue;
	}
	return param.parseBoolean();
    }

    /** convinience method for returning parsed parameter */
    public String parse(String paramName, String defaultValue) throws NumberFormatException {
	StringParameter param = (StringParameter)(getParameter(paramName));
	if (param == null) {
	    return defaultValue;
	}
	return param.getValue();
    }

    public void setName(String name) { this.name = name; }

    /** returns string */
    public String toString() {
	StringBuffer buf = new StringBuffer();
	buf.append(getName());
	if (getParameterCount() > 0) {
	    buf.append(" ");
	}
	for (int i = 0; i < getParameterCount(); ++i) {
	    Command subCommand = getParameter(i);
	    if (subCommand instanceof Parameter) {
		buf.append(subCommand.toString());
		buf.append(" ");
	    }
	    else {
		buf.append(subCommand.toString());
		buf.append(NEWLINE);
	    }
	}
	return buf.toString();
    }

    public Vector<String> toStringVector() {
	Vector<String> result = new Vector<String>();
	String toAdd = getName();
	if (getParameterCount() > 0) {
	    toAdd += " ";
	}
	if (toAdd.equals("composite ")) {
	    toAdd = "";
	}
	for (int i = 0; i < getParameterCount(); i++) {
	    Command subCommand = getParameter(i);
	    toAdd += subCommand.toString();
	    if (subCommand instanceof Parameter) {
		toAdd += " ";
	    }
	    else {
		result.add(toAdd);
		toAdd = new String();
	    }
	}
	return result;
    }

}
