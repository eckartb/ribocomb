package commandtools; 

import java.util.Vector;

public abstract class AbstractParameter extends AbstractAtomicCommand implements Parameter {

    public AbstractParameter(String name) {
	super(name);
    }

    public void addParameter(Command c) { assert false; }
    
        /** puts command into action. Implementation depends on appliation.
     * Return corresponding undo command ! */
    public Command execute() { 
	return null;
    }

    /** puts command into action. Implementation depends on appliation. */
    public void executeWithoutUndo() {
    }

    public Command getParameter(int n) { assert false; return null; }

    public Command getParameter(String name) { return null; }

    public int getParameterCount() { return 0; }

    public Vector<String> toStringVector() { assert false; return null; }

}
