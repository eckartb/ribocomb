package commandtools;

/** Indicates commands that do not have parameters */
public interface AtomicCommand extends Command {

}
