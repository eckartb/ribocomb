package commandtools;

public class BadSyntaxException extends CommandException 
{

    public BadSyntaxException() {
    }

    public BadSyntaxException(String source) {
	super(source);
    }

}
