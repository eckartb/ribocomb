package commandtools;

import java.util.Vector;
import java.util.Properties;

public interface Command {

    public Object cloneDeep();

    public void addParameter(Command c);

    /** returns true if all parameter names are ok */
    public boolean checkParameterNames();

    /** returns number of parameters */
    public int getParameterCount();

    /** Returns short help text */
    public String getShortHelpText();

    /** Returns long help text */
    public String getLongHelpText();

    /** returns n'th parameter */
    public Command getParameter(int n);

    /** returns parameter with specified name or null if not found */
    public Command getParameter(String name);

    /** gets name of n't parameter (use for named parameters like "x=5") */
    public String getParameterName(int n);

    /** returns true if parameter name is allowed. The default implementation returns always true. */
    public boolean isAllowedParameterName(String name);

    // public Object clone();

    /** puts command into action. Implementation depends on application.
     * Return corresponding undo command ! */
    public Command execute() throws CommandExecutionException;

    /** puts command into action. Implementation depends on application. */
    public void executeWithoutUndo() throws CommandExecutionException;

    /** returns property class potentially containing result values */
    public Properties getResultProperties();

    /** returns name of command like "move", or "exit" */
    public String getName();

    /** returns string */
    public String toString();

    /** returns a vector containing string */
    public Vector<String> toStringVector();

}
