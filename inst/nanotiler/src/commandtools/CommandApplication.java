package commandtools; 

import java.util.Properties;
import java.io.InputStream;

/** base class that defines a framework for a "command base" application: that is an 
 * application that can read and interpret scripts, and handles all events (like from
 * a potential gui) as an issued and excecuted command.
 */
public interface CommandApplication extends CommandHistoryChanger {

    public Interpreter getInterpreter();

    public Command getLastCommand();

    public Command getCommandHistory();

    public void runCommand(Command command) throws CommandException;

    public void runScript(InputStream is) throws CommandException;

    public Properties runScriptLine(String line) throws CommandException;

    /** if false, commands are excecuted without undo */
    public void setUndoMode(boolean mode); 

    public void undo();

}
