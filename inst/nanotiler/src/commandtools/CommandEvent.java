package commandtools;

/** describes event that involves a command
 * TODO : make class subclass of java.util.Event
 */
public class CommandEvent {

    private Command command;

    public CommandEvent(Command command) {
	this.command = command;
    }

    public Command getCommand() { return this.command; }

}
