package commandtools;

public class CommandExecutionException extends CommandException {

    public CommandExecutionException() {
	super();
    }

    public CommandExecutionException(String msg) {
	super(msg);
    }

}
