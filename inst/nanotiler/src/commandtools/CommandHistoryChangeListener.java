package commandtools;

public interface CommandHistoryChangeListener {

    public void commandHistoryChanged(CommandEvent event);

}
