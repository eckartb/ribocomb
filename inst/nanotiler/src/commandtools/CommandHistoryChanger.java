package commandtools;

public interface  CommandHistoryChanger {

    public void addCommandHistoryChangeListener(CommandHistoryChangeListener listener);

    public void fireCommandHistoryChanged(CommandEvent command);

}
