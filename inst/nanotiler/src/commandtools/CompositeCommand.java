package commandtools;

/** represents command that consists of a set of other commands. 
 * @see AtomicCommand
 * @see Command
 */
public interface CompositeCommand extends Command {

}
