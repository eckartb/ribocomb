package commandtools;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.IOException;

import static commandtools.PackageConstants.NEWLINE;

public class DefineCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "define";

    private String definedName;
    private String fileName;
    private CommandApplication application;
    
    public DefineCommand(CommandApplication application) {
	super(COMMAND_NAME);
	this.application = application;
    }

    public Object cloneDeep() {
	DefineCommand command = new DefineCommand(application);
	command.definedName = this.definedName;
	command.fileName = this.fileName;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    public String helpOutput() {
	return "Reads and executes script file. Correct usage: source filename";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME + " COMMANDNAME " 
	    + " FILENAME" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Defines a comand by a command name and a script file name." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	assert (this.application != null);
	prepareReadout();
	Command newCommand = new DefinedCommand(definedName, fileName, "", application);
	application.getInterpreter().addKnownCommand(newCommand);
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() != 2) {
	    throw new CommandExecutionException(helpOutput());
	}
	assert getParameter(0) instanceof StringParameter;
	assert getParameter(1) instanceof StringParameter;
	StringParameter stringParameter0 = (StringParameter)(getParameter(0));
	definedName = stringParameter0.getValue();
	StringParameter stringParameter1 = (StringParameter)(getParameter(1));
	fileName = stringParameter1.getValue();

    }
    
}
