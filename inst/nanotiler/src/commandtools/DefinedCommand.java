package commandtools;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.IOException;

import static commandtools.PackageConstants.NEWLINE;

class DefinedCommand extends AbstractCommand {
    
    // private String COMMAND_NAME = "";

    private String definedName;
    private String fileName;
    private String helpText;
    private CommandApplication application;
    
    public DefinedCommand(String definedName, String fileName, String helpText, CommandApplication application) {
	super(definedName);
	this.definedName = definedName;
	this.fileName = fileName;
	this.helpText = helpText;
	this.application = application;
    }

    public Object cloneDeep() {
	DefinedCommand command = new DefinedCommand(definedName, fileName, helpText, application);
	command.definedName = this.definedName;
	command.fileName = this.fileName;
	command.helpText = this.helpText;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return definedName; }

    public String helpOutput() {
	return helpText;
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	assert (this.application != null);
	// prepareReadout();
	String commandText = "source " + fileName;
	for (int i = 0; i < getParameterCount(); ++i) {
	    commandText = commandText + " " + ((StringParameter)(getParameter(i))).getValue();
	}
	System.out.println("Issuing command: " + commandText);
	try {
	    application.runScriptLine(commandText);
	} catch (CommandException ce) {
	    throw new CommandExecutionException("Error issuing command: " + commandText + " : " + ce.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }
    
}
