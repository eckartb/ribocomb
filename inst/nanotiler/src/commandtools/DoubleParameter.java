package commandtools;

/** concept of double constant or variable */
public class DoubleParameter extends AbstractParameter implements Parameter  {

    private double value;

    public static final String DOUBLE_NAME = "double";

    public DoubleParameter(double d) { 
	super(DOUBLE_NAME);
	this.value = d; setName(DOUBLE_NAME); 
    }

    public DoubleParameter(double d, String name) { 
	super(name);
	this.value = d; setName(name); 
    }

    public double getValue() { return value; }

    public void setValue(double d) { this.value = d; }

    public String toString() { 
	return "" + value;
    }

    public Object cloneDeep() {
	DoubleParameter result = new DoubleParameter(this.getValue(), this.getName());
	return result;
    }

}
