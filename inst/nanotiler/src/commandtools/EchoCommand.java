package commandtools;

import static commandtools.PackageConstants.NEWLINE;

public class EchoCommand extends AbstractCommand implements Command {
    
    public static final String COMMAND_NAME = "echo";

    public EchoCommand() {
	super(COMMAND_NAME);
    }

    public EchoCommand(String message) { 
	super(COMMAND_NAME);
	addParameter(new StringParameter(message));
    }

    public EchoCommand(StringParameter messageParameter) { 
	super(COMMAND_NAME);
	addParameter(messageParameter);
    }

    public Object cloneDeep() {
	EchoCommand result = new EchoCommand();
	for (int i = 0; i < getParameterCount(); ++i) {
	    result.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return result;
    }

    public Command execute() {
	executeWithoutUndo(); // no undo possible
	return null;
    }

    public void executeWithoutUndo() {
	for (int i = 0; i < getParameterCount(); ++i) {
	    if (getParameter(i) instanceof StringParameter) {
		StringParameter sp = (StringParameter)(getParameter(i));
		System.out.print(sp.getValue() + " ");
	    }
	}
	System.out.println();
    }

   public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " [options]" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Echo command echoes strings and values." + NEWLINE + NEWLINE;
	return helpText;
    }

   private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " [options]";
    }

    /** returns name of command like "move", or "exit" */
    public String getName() { return COMMAND_NAME; }

    /** returns string */
    public String toString() {
	StringBuffer result = new StringBuffer("echo ");
	for (int i = 0; i < getParameterCount(); ++i) {
	    if (getParameter(i) instanceof StringParameter) {
		StringParameter sp = (StringParameter)(getParameter(i));
		result.append("" + sp.getValue() + " ");
	    }
	}
	return result.toString();
    }

}
