package commandtools;

import java.util.Properties;
import java.util.Enumeration;
import java.io.PrintStream;

import static commandtools.PackageConstants.NEWLINE;

/** setting of script variable! Syntax: set variablename value */
public class EnvCommand extends AbstractCommand implements Command {
    
    public static final String COMMAND_NAME = "env";

    private Properties properties;
    private PrintStream ps;

    public EnvCommand(Properties properties, PrintStream ps) {
	super(COMMAND_NAME);
	this.properties = properties;
	this.ps = ps;
    }


    public Object cloneDeep() {
	EnvCommand result = new EnvCommand(properties, ps);
	for (int i = 0; i < getParameterCount(); ++i) {
	    result.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return result;
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo(); // no undo possible
	return null;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	Enumeration names = properties.propertyNames();
	while (names.hasMoreElements()) {
	    String name = (String)(names.nextElement());
	    ps.println(name + "=" + properties.getProperty(name));
	}
    }

    /** returns name of command like "move", or "exit" */
    public String getName() { return COMMAND_NAME; }

    /** returns string */
    public String toString() {
	StringBuffer result = new StringBuffer(COMMAND_NAME + " ");
	for (int i = 0; i < getParameterCount(); ++i) {
	    if (getParameter(i) instanceof StringParameter) {
		StringParameter sp = (StringParameter)(getParameter(i));
		result.append("" + sp.getValue() + " ");
	    }
	}
	return result.toString();
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
 	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Env command gives a list of the set variables." + NEWLINE + NEWLINE;
	return helpText;
    }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME ;
    }

}
