package commandtools; 

import static commandtools.PackageConstants.NEWLINE;

public class ExitCommand extends AbstractCommand implements Command {

    public static final String COMMAND_NAME = "exit";

    public ExitCommand() {
	super(COMMAND_NAME);
    }

    public Object cloneDeep() {
	ExitCommand result = new ExitCommand();
	for (int i = 0; i < getParameterCount(); ++i) {
	    result.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return result;
    }

    public void executeWithoutUndo() { 
	System.exit(0);
    }

    public Command execute() {
	System.exit(0);
	return null;
    }

    public String toString() { return COMMAND_NAME; }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Exit command shuts down the program." + NEWLINE + NEWLINE;
	return helpText;
    }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME;
    }

    
}
