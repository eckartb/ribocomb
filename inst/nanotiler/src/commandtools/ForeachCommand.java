package commandtools;

import java.io.*;
import java.util.Properties;

import static commandtools.PackageConstants.*;

public class ForeachCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "foreach";
    public static final String BRACKET_OPEN = "(";
    public static final String BRACKET_CLOSE = ")";
    private Properties properties;
    private String varName;
    private String[] varValues;
    private String[] bodyLines;
    private CommandApplication application;

    public ForeachCommand(Properties props, CommandApplication app) {
	super(COMMAND_NAME);
	assert props != null;
	assert app != null;
	this.properties = props;
	this.application = app;
	assert this.properties != null;
	assert this.application != null;
    }

    public Object cloneDeep() {
	ForeachCommand command = new ForeachCommand(properties, application);
	command.varName = this.varName;
	command.varValues = this.varValues;	
	command.bodyLines = this.bodyLines;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    public String helpOutput() {
	return "Performs for loop. Example syntax: foreach varname ( 1 5 10 15 ) echo ${varname} end" + NEWLINE;
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " VARIABLE ( VAL VAL VAL ) COMMAND end" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Foreach command allows loops." + NEWLINE +
	    "Example syntax: foreach varname ( 1 5 10 15 ) echo ${varname} end" +
	    NEWLINE + NEWLINE;
	return helpText;
    }

    /** generates reader stream from set of lines */
    private static BufferedReader generateReader(String[] lines) {
	StringBuffer buf = new StringBuffer();
	for (int i = 0; i < lines.length; ++i) {
	    buf.append(lines[i]);
	    buf.append(NEWLINE);
	}
	String resultString = buf.toString();
	return new BufferedReader(new StringReader(resultString));
    }

    /** generates reader stream from set of lines */
    private static StringBufferInputStream generateInputStream(String[] lines) {
	StringBuffer buf = new StringBuffer();
	for (int i = 0; i < lines.length; ++i) {
	    buf.append(lines[i]);
	    buf.append(NEWLINE);
	}
	String resultString = buf.toString();
	return new StringBufferInputStream(resultString);
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	assert (this.application != null);
	prepareReadout();
	if (varValues == null) {
	    throw new CommandExecutionException("No loop parameters defined!");
	}
	assert bodyLines != null;
	for (int i = 0; i < varValues.length; ++i) {
	    properties.setProperty(varName, varValues[i]);
	    log.finest("Set " + varName + " to " + varValues[i]);
	    InputStream is = generateInputStream(bodyLines);
	    try {
		application.runScript(is);
	    }
	    catch (CommandException ce) {
		throw new CommandExecutionException(ce.getMessage());
	    }
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() < 3) {
	    throw new CommandExecutionException(helpOutput());
	}
	assert getParameter(0) instanceof StringParameter;
	varName = ((StringParameter)(getParameter(0))).getValue();
	int numValues = getParameterCount() - 3; // example: for a ( 1 2 3 )    : 7 values, subtract "a", "(", ")"
	varValues = new String[numValues];
	String openBracketName = ((StringParameter)(getParameter(1))).getValue();
	if (! BRACKET_OPEN.equals(openBracketName)) {
	    throw new CommandExecutionException("Syntax error in for loop. Found " + openBracketName + " instead of \"(\". Example of correct usage: for varname in ( 1 2 3 ) echo end");
	}
	String closingName = ((StringParameter)(getParameter(getParameterCount()-1))).getValue();
	if (! BRACKET_CLOSE.equals(closingName)) {
	    throw new CommandExecutionException("Syntax error in for loop. Found " + closingName + " instead of \")\". Example of correct usage: for varname in ( 1 2 3 ) echo end");
	}
	for (int i = 0; i < varValues.length; ++i) {
	    varValues[i] = ((StringParameter)(getParameter(i+2))).getValue();
	    log.fine("Set : " + varValues[i]);
	}
    }

    /** sets lines of body of for loop to be executed */
    public void setBodyLines(String[] bodyLines) {
	assert bodyLines != null;
	this.bodyLines = bodyLines;
    }
    
}
