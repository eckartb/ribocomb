package commandtools;

import java.io.*;

import static commandtools.PackageConstants.NEWLINE;

public class HistoryCommand extends AbstractCommand implements Command {
    
    public static final String COMMAND_NAME = "history";

    private CommandApplication application;

    private String fileName = null;

    public HistoryCommand(CommandApplication app) {
	super(COMMAND_NAME);
	this.application = app;
    }

    public Object cloneDeep() {
	HistoryCommand result = new HistoryCommand(application);
	result.application = application;
	result.fileName = fileName;
	for (int i = 0; i < getParameterCount(); ++i) {
	    result.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return result;
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo(); // no undo possible
	return null;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	if ((fileName == null) || (fileName.length() == 0)) {
	    System.out.println(application.getCommandHistory().toString());
	}
	else {
	    FileOutputStream fos = null;
	    try {
		fos = new FileOutputStream(fileName);
		PrintStream ps = new PrintStream(fos);
		ps.println(application.getCommandHistory().toString());
		fos.close();
	    }
	    catch (IOException ioe) {
		throw new CommandExecutionException("Error writing to file: " + fileName + " " + ioe.getMessage());
	    }
	}
    }

    /** returns name of command like "move", or "exit" */
    public String getName() { return COMMAND_NAME; }

    /** returns string */
    public String toString() {
	StringBuffer result = new StringBuffer(COMMAND_NAME);
	StringParameter filePar = (StringParameter)(getParameter("file"));
	if (filePar != null) {
	    this.fileName = filePar.getValue();
	}
	
	return result.toString();
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " [options]" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     History command prints history or writes history to a file." + NEWLINE + NEWLINE;
	helpText += "OPTIONS" + NEWLINE;
	helpText += "     file=FILENAME" + NEWLINE + "          Write history to FILENAME." + NEWLINE + NEWLINE;
	return helpText;
    }

   private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " [file=FILENAME]";
    }

}
