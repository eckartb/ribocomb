package commandtools;

/** concept of double constant or variable */
public class IntParameter extends AbstractParameter implements Parameter  {

    private int value;

    public static final String COMMAND_NAME = "int";

    public IntParameter(int d) { 
	super(COMMAND_NAME);
	this.value = d; setName(COMMAND_NAME); 
    }

    public IntParameter(int d, String name) { 
	super(name);
	this.value = d; setName(name); 
    }

    public Object cloneDeep() {
	return new IntParameter(this.getValue(), this.getName());
    }

    public int getValue() { return value; }

    public void setValue(int d) { this.value = d; }

    public String toString() { 
	return "" + value;
    }

}
