package commandtools; 

import java.io.InputStream;
import java.io.IOException;
import java.io.BufferedReader;

public interface Interpreter {

    public void addKnownCommand(Command command);

    /** Returns help string */
    public String getCommandNames();

    /** reads and parses next command */
    public Command interpret(BufferedReader reader) throws IOException, UnknownCommandException, BadSyntaxException;

    /** reads and parses next command */
    public Command interpretLine(String s) throws UnknownCommandException, BadSyntaxException;

    /** returns value of variable */
    public String getVariable(String name);

    /** sets variable to a value */
    public void setVariable(String name, String value);

    /** Find the command that completes the substring s */
    public String autoComplete(String s);

}
