package commandtools;

import java.util.Properties;
import generaltools.ParsingException;
import java.util.logging.*;

public class InterpreterTools {

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    /** takes a text, and replaces all occurences of ${VARNAME} to its content set by environment variables */
    public static String replaceEnvironmentVariables(String text, Properties properties) throws ParsingException {
	StringBuffer buf = new StringBuffer(text);
	int pc = 0;
	while ((pc+1) < buf.length()) {
	    if (buf.charAt(pc) == PackageConstants.COMMENT_CHAR) {
		break; // primitive parsing: any occurrency of "#" char is interpreted as comment TODO: Make more flexible (allow strings etc)
	    }
	    int pc2 = pc;
	    boolean found = false;
	    if ((buf.charAt(pc) == '$') && (buf.charAt(pc+1) == '{')) {
		// search for closing bracket:
		for (pc2 = pc+2; pc2 < buf.length(); ++pc2) {
		    if (buf.charAt(pc2) == '}') {
			found = true;
			break;
		    }
		}
		if (!found) {
		    throw new ParsingException("Could not find closing bracket for environment variable at position " + (pc+1) + " in text " + text);
		}
		pc2++; // make exclusive, one higher than closing bracket
		String varName = buf.substring(pc+2 , pc2-1); // instead of ${HOME}, pass on HOME  for example.
		String varContent = null;
		// check in local property object first
		if (properties != null) {
		    varContent = properties.getProperty(varName);
		}
		// if not found, check environment variables
		if (varContent == null) {
		    varContent = System.getenv(varName);
		}
		if (varContent == null) {
		    throw new ParsingException("Undefined variable: " + varName);
		}
		buf.replace(pc, pc2, varContent);
		log.finest("Replaced " + pc + " " + pc2 + " " + varContent);
	    }

	    pc++;
	}
	return buf.toString();
    }

}
