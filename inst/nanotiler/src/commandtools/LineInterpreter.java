package commandtools;

import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.IOException;
import java.io.EOFException;
import java.util.StringTokenizer;
import java.util.logging.Logger;
import java.util.Properties;
import java.util.List;
import java.util.ArrayList;

import generaltools.ParsingException;
import generaltools.StringTools;

 /** Interprets any line-based script language! Only necessary to add known commands properly. */
 public class LineInterpreter extends AbstractInterpreter implements Interpreter {


     public static final char PARAM_DELIMITER = '='; // delimiter

     public static final String CLOSING_DELIM = "end";

     public LineInterpreter(Logger log) {
	 super(log);
     }

     /** reads and parses next command */
     public Command interpret(BufferedReader bufReader) throws BadSyntaxException, IOException, UnknownCommandException {
	 Command command;
	 // BufferedReader bufReader = new BufferedReader(new InputStreamReader(is));	 
	 String line = null;
	 do {
	     if (!bufReader.ready()) {
		 return null;
	     }
	     line = bufReader.readLine();
	     //	 log.fine("line: " + line + "line2: " + line2 + " line3: " + line3);
	 }
	 while ((line == null) || (line.length() == 0));
	 log.fine("Interpreting scanned line: " + line);
	 command = interpretLine(line);
	 // special handling of "for" command:
	 if ((command != null) && (command instanceof ForeachCommand)) {
	     log.fine("Foreach command detected!");
	     // read until "end" statement reached:
	     List<String> bodyLinesList = new ArrayList<String>();
	     boolean isOk = false;
	     int loopCount = 1; // keeps track of how many loops were opened
	     do {
		 do {
		     if (! bufReader.ready()) {
			 throw new BadSyntaxException("Could not find end statement for the for loop");
		     }
		     line = bufReader.readLine();
		 }
		 while ((line == null) || (line.length() == 0));
		 line = line.trim();
		 log.fine("body line: " + line);
		 if (line.indexOf("foreach") == 0) {
		     ++loopCount;
		 }
		 else if (line.equals(CLOSING_DELIM)) {
		     --loopCount;
		 }
		 if ((line.equals(CLOSING_DELIM)) && (loopCount == 0)) {
		     String[] bodyLines = new String[bodyLinesList.size()];
		     for (int i = 0; i < bodyLines.length; ++i) {
			 bodyLines[i] = bodyLinesList.get(i);
		     }
		     ForeachCommand foreachCommand = (ForeachCommand)command;
		     assert bodyLines != null;
		     foreachCommand.setBodyLines(bodyLines);
		     break; // end of loop
		 }
		 else {
		     bodyLinesList.add(line);
		 }
	     }
	     while (!isOk);
	 }	 
	 return command;
     }

     /** converts text like file=hi.dat into StringParameter with name "file" and value "hi.dat" */
     public static StringParameter generateStringParameter(String text) {
	 assert text != null;
	 // scan for "=" character:
	 int pos = text.indexOf(PARAM_DELIMITER);
	 if (pos < 0) {
	     return new StringParameter(text);
	 }
	 String sValue = "";
	 if (pos < text.length()) {
	     sValue = text.substring(pos + 1);
	 }
	 String name = "";
	 if (pos > 0) {
	     name = text.substring(0, pos);
	 }
	 log.fine("Parameter pair: " + name + " " + sValue + " from: " + text);
	 return new StringParameter(sValue, name);
     }



     /** reads and parses next command */
     public Command interpretLine(String line) throws BadSyntaxException, UnknownCommandException {

	 if (line == null) {
	     log.warning("Null line cannot be interpreted!");
	     return null;
	 }
	 log.fine("Interpreting line: " + line);
	 
	 // preprocess: replace potential environment variables:
	 try {
	     line = InterpreterTools.replaceEnvironmentVariables(line, variables);
	 }
	 catch (ParsingException pe) {
	     throw new BadSyntaxException(pe.getMessage()); // TODO : define UndefinedVariableException !
	}

	StringTokenizer tokenizer = new StringTokenizer(line);
	if (!tokenizer.hasMoreTokens()) {
	    return null;
	}
	String commandName = tokenizer.nextToken();
	Command commandOrig = findCommand(commandName);
	if (commandOrig == null) {
	    throw new UnknownCommandException("Unknown command: " + commandName);
	}
	log.fine("Found command: " + commandOrig);
	Command command = (Command)(commandOrig.cloneDeep()); 
	if (! (command instanceof AtomicCommand) ) {
	    while (tokenizer.hasMoreTokens()) {
		String nextToken = tokenizer.nextToken();
		log.fine("Adding parameter (or sub-command): " + nextToken);
		command.addParameter(generateStringParameter(nextToken));
	    }
	}
	log.fine("Finished interpretLine: " + command.toString());
	return command;
    }

}
