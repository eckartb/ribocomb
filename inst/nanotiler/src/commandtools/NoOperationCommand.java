package commandtools;

import static commandtools.PackageConstants.NEWLINE;

/** concept of double constant or variable */
public class NoOperationCommand extends AbstractAtomicCommand implements AtomicCommand  {

    public static final String COMMAND_NAME = "#";

    public NoOperationCommand() { super(COMMAND_NAME); }

    public Object cloneDeep() {
	NoOperationCommand result = new NoOperationCommand();
	return result;
    }

    /** puts command into action. Implementation depends on appliation.
     * Return corresponding undo command ! */
    public Command execute() { 
	return null;
    }

    /** puts command into action. Implementation depends on appliation. */
    public void executeWithoutUndo() {
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " COMMENT" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     # command allows user to make a comment." +
	    NEWLINE + NEWLINE;
	return helpText;
    }

   private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " COMMENT";
    }

}
