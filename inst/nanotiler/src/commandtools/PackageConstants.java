package commandtools;

import java.io.File;

public class PackageConstants {

    public static final String NEWLINE = System.getProperty("line.separator");

    public static final String ENDL = NEWLINE;

    public static String SLASH = File.separator; // either "/" or "\"  depending on Unix of Windows

    public static char COMMENT_CHAR = '#'; // used for indicating comments

}
