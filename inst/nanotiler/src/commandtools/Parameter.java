package commandtools;

/** represents simple values like string or double values. 
 *  Please note, that Command.getParameter(n) returns a Command and not necessarily a Paramter object !
 *  This potentially ensures a lot of flexibility, because a command can be a parameter. 
 */
public interface Parameter extends AtomicCommand {

}
