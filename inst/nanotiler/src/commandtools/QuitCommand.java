package commandtools; 

import static commandtools.PackageConstants.NEWLINE;

public class QuitCommand extends AbstractCommand implements Command {

    public static final String COMMAND_NAME = "quit";

    public QuitCommand() {
	super(COMMAND_NAME);
    }

    public Object cloneDeep() {
	QuitCommand result = new QuitCommand();
	for (int i = 0; i < getParameterCount(); ++i) {
	    result.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return result;
    }

    public void executeWithoutUndo() { 
	System.exit(0);
    }

    public Command execute() {
	System.exit(0);
	return null;
    }

    public String toString() { return COMMAND_NAME; }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Quit command leaves script mode and returns (if possible) to interactive mode. This is mostly useful in conjunction with the source command." + NEWLINE + NEWLINE;
	return helpText;
    }

    private String helpOutput() {
	return "Leaves script mode and returns (if possible) to interactive mode. Correct usage: " + COMMAND_NAME;
    }

    
}
