package commandtools;

/** represent recoverable exception within an application. Try to use more specific exception */
public class ScriptException extends CommandException {

    public ScriptException() {
	super();
    }

    public ScriptException(String s) {
	super(s);
    }

}
