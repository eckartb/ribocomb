package commandtools;

import java.util.Properties;

import static commandtools.PackageConstants.NEWLINE;

/** setting of script variable! Syntax: set variablename value */
public class SetCommand extends AbstractCommand implements Command {
    
    public static final String COMMAND_NAME = "set";

    private Properties properties;

    public SetCommand(Properties properties) {
	super(COMMAND_NAME);
	this.properties = properties;
    }


    public Object cloneDeep() {
	SetCommand result = new SetCommand(properties);
	result.properties = properties; // no cloning!
	for (int i = 0; i < getParameterCount(); ++i) {
	    result.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return result;
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo(); // no undo possible
	return null;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	String varName = null;
	String varValue = null;
	if (getParameterCount() != 2) {
	    throw new CommandExecutionException("Correct usage of set command: set variablename value");
	}
	if (getParameter(0) instanceof StringParameter) {
	    varName = ((StringParameter)(getParameter(0))).getValue();
	}
	else {
	    assert false;
	}
	if (getParameter(1) instanceof StringParameter) {
	    varValue = ((StringParameter)(getParameter(1))).getValue();
	}
	else {
	    assert false; // should never happen
	}
	properties.setProperty(varName, varValue);
    }

   private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " variablename value";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " VARIABLENAME VALUE" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Set command allows user to set a variable to a value." + NEWLINE + NEWLINE;
	return helpText;
    }

    /** returns name of command like "move", or "exit" */
    public String getName() { return COMMAND_NAME; }

    /** returns string */
    public String toString() {
	StringBuffer result = new StringBuffer(COMMAND_NAME + " ");
	for (int i = 0; i < getParameterCount(); ++i) {
	    if (getParameter(i) instanceof StringParameter) {
		StringParameter sp = (StringParameter)(getParameter(i));
		result.append("" + sp.getValue() + " ");
	    }
	}
	return result.toString();
    }

}
