package commandtools;

import static commandtools.PackageConstants.*;

/** represents command that consists of a set of other commands. 
 * @see AtomicCommand
 * @see Command
 */
public class SimpleCommand extends AbstractCommand implements Command {
    
    public static final String COMMAND_NAME = "composite";

    public SimpleCommand() {
	super(COMMAND_NAME);
    }

    public Object cloneDeep() {
	SimpleCommand result = new SimpleCommand();
	for (int i = 0; i < getParameterCount(); ++i) {
	    result.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return result;
    }

    /** puts command into action. Implementation depends on appliation.
     * Return corresponding undo command ! */
    public Command execute() throws CommandExecutionException { 
	Command undoCommand = new SimpleCommand();
	for (int i = 0; i < getParameterCount(); ++i) {
	    undoCommand.addParameter(getParameter(i).execute()); // TODO : what about undo!?
	}
	// TODO : implement reversing of order of undo commands!!!
	return undoCommand;
    }

    /** puts command into action. Implementation depends on appliation. */
    public void executeWithoutUndo() throws CommandExecutionException {
	for (int i = 0; i < getParameterCount(); ++i) {
	    getParameter(i).executeWithoutUndo();
	}
    }

    /** returns string */
    public String toString() {
	StringBuffer buf = new StringBuffer();
	for (int i = 0; i < getParameterCount(); ++i) {
	    Command subCommand = getParameter(i);
	    buf.append(subCommand.toString());
	    if (subCommand instanceof Parameter) {
		buf.append(" ");
	    }
	    else {
		buf.append(NEWLINE);
	    }
	}
	return buf.toString();
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " [options]" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Composite command represents command that stands for list of other commands." + NEWLINE + NEWLINE;
	return helpText;
    }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " TODO";
    }

}
