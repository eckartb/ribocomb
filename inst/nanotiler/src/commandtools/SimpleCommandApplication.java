package commandtools; 

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.EOFException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.Properties;

import generaltools.StringTools;

/** base class that defines a framework for a "command base" application: that is an 
 * application that can read and interpret scripts, and handles all events (like from
 * a potential gui) as an issued and excecuted command.
 */
public class SimpleCommandApplication implements CommandApplication {

    protected static Logger log;

    private Command commandHistory;

    private Command lastCommand;

    protected boolean undoMode = true;
    
    protected Interpreter interpreter;

    private List<CommandHistoryChangeListener> historyListeners = new ArrayList<CommandHistoryChangeListener>();

    private List<Command> undoStack = new ArrayList<Command>();

    public SimpleCommandApplication(Interpreter interpreter, Logger log) {
	this.interpreter = interpreter;
	this.commandHistory = new SimpleCommand();
	this.log = log;
    }

    public void addCommandHistoryChangeListener(CommandHistoryChangeListener listener) {
	this.historyListeners.add(listener);
    }

    public void fireCommandHistoryChanged(CommandEvent event) {
	for (int i = 0;  i < historyListeners.size(); ++i) {
	    CommandHistoryChangeListener listener = (CommandHistoryChangeListener)(historyListeners.get(i));
	    listener.commandHistoryChanged(event);
	}
    }

    public Interpreter getInterpreter() { return interpreter; }

    public Command getLastCommand() { return lastCommand; }

    public Command getCommandHistory() { return commandHistory; }

    private void runCommandInternal(Command command) throws UnknownCommandException {
	assert commandHistory != null;
	if (command == null) {
	    throw new UnknownCommandException("Null pointer instead of runnable command encountered!");
	}
	commandHistory.addParameter(command);
	lastCommand = command;
	fireCommandHistoryChanged(new CommandEvent(command));
    }


    public void runCommand(Command command) throws CommandException {
	runCommandInternal(command);
	Command undoCommand = command.execute();
	if (undoCommand != null) {
	    undoStack.add(undoCommand);
	}
	else {
	    // cannot be undone! clear undo stack:
	    log.fine("Command cannot be undone, clearing undo stack: " + command.getName());
	    undoStack.clear();
	}
    }

    public void runCommandWithoutUndo(Command command) throws CommandException {
	runCommandInternal(command);
	command.executeWithoutUndo();
    }
    
    /** runs command defined in this line. Returns properties class potentially containing result values */
    public Properties runScriptLine(String line) throws CommandException {
	assert line != null;
	assert interpreter != null;
	Command command = interpreter.interpretLine(line);
	if (command == null) {
	    log.fine("Null command encountered in runScript!");
	    return null; // nothing to do
	}
	if (command instanceof ForeachCommand) {
	    throw new CommandException("Sorry, foreach command is currently only available in non-interactive script mode.");
	}
	if (undoMode) {
	    runCommand(command);
	}
	else {
	    runCommandWithoutUndo(command);
	}
	return command.getResultProperties();
    }

    public void runScript(InputStream is) throws CommandException {
	try {
	    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	    Command command = null;
	    do {
		command = interpreter.interpret(reader);
		if (command != null) {
		    if (command.getName().equals("quit")) {
			break; // special case: quit leaves script mode. Back to interactive mode if possible
		    }
		    if (undoMode) {
			runCommand(command);
		    }
		    else {
			runCommandWithoutUndo(command);
		    }
		}
	    }
	    while (command != null);
	}
	catch (EOFException e) { // do not throw error for end of file
	    log.info("# End of script.");
	    return;
	}
	catch(IOException ioe) {
	    throw new CommandException(ioe.getMessage());
	}
	catch (UnknownCommandException uce) {
	    throw new CommandException(uce.getMessage());
	}
	catch (BadSyntaxException bse) {
	    throw new CommandException(bse.getMessage());
	}
// 	/** reads all lines from a stream */
// 	String[] allLines = null;
// 	try {
// 	    allLines = StringTools.readAllLines(is);
// 	}
// 	catch (IOException e) {
// 	    throw new CommandException("Error reading input stream!");
// 	}
// 	assert allLines != null;
// 	assert allLines.length != 0;
// 	for (int i = 0; i < allLines.length; ++i) {
// 	    assert (allLines[i] != null);
// 	    if (allLines[i].length() > 0) {
// 		runScriptLine(allLines[i]);
// 	    }
// 	}
    }

    public void setUndoMode(boolean b) { this.undoMode = b; }

    /** undo last command
     * TODO */
    public void undo() {
	assert false; // NOT YET IMPLEMENTED
    }

}
