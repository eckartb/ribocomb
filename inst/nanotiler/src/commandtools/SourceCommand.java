package commandtools;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.IOException;

import static commandtools.PackageConstants.NEWLINE;

public class SourceCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "source";

    private String fileName;
    private CommandApplication application;
    
    public SourceCommand(CommandApplication application) {
	super(COMMAND_NAME);
	this.application = application;
    }

    public Object cloneDeep() {
	SourceCommand command = new SourceCommand(application);
	command.fileName = this.fileName;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    public String helpOutput() {
	return "Reads and executes script file. Correct usage: source filename";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " FILENAME" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Source command reads and executes script file." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	assert (this.application != null);
	prepareReadout();
	try {
	    InputStream is = new FileInputStream(fileName);    
	    application.runScript(is);
	}
	catch (IOException exc) {
	    // replace with better error window
	    throw new CommandExecutionException("Error opening file " + fileName);
	}
	catch (CommandException ce) {
	    throw new CommandExecutionException(ce.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() < 1) {
	    throw new CommandExecutionException(helpOutput());
	}
	assert getParameter(0) instanceof StringParameter;
	StringParameter stringParameter = (StringParameter)(getParameter(0));
	fileName = stringParameter.getValue();
	Interpreter interpreter = application.getInterpreter();
	for (int i = 1; i < getParameterCount(); ++i) {
	    String varName = "" + i;
	    String value = ((StringParameter)(getParameter(i))).getValue();
	    System.out.println("# Setting variable " + varName + " to " + value);
	    interpreter.setVariable(varName, value);
	}

    }
    
}
