package commandtools;

import generaltools.ParsingException;

public class StringParameter extends AbstractParameter implements Parameter  {

    private String value;

    public static final String STRING_NAME = "string";

    public StringParameter(String s) { 
	super(STRING_NAME);
	this.value = s;
    }

    public StringParameter(String s, String name) { 
	super(name);
	this.value = s; 
    }

    public Object cloneDeep() {
	return new StringParameter(this.getValue(), this.getName());
    }

    public String getValue() { return value; }

    public boolean parseBoolean() throws ParsingException {
	return parseBoolean(this.value);
    }
    
    /** returns true of false for given "true" or "false" Strings. Ignores case. */
    public static boolean parseBoolean(String value) throws ParsingException {
	if (value == null) {
	    throw new ParsingException("Could not parse null boolean value string");
	}
	String lc = value.toLowerCase();
	if (lc.equals("true")) {
	    return true;
	}
	else if (!lc.equals("false")) {
	    throw new ParsingException("Could not parse boolean value: " + value);
	}
	return false;
    }

    public void setValue(String s) { this.value = s; }

    public String toString() {
	String name = getName();
	if ((name == null) || (name.length() == 0) || (name.equals("string"))) {
	    return value;
	}
	return name + "=" + value;
    }

}
