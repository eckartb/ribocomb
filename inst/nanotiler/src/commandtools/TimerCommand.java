package commandtools;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static commandtools.PackageConstants.NEWLINE;

public class TimerCommand extends AbstractCommand implements Command {

    private static Map<String, Date> map = new HashMap<String, Date>();

    private static String COMMAND_NAME = "timer";

    public TimerCommand(String name) {
	super(name);
    }

    public TimerCommand() {
	super(COMMAND_NAME);
    }

    public Object cloneDeep() {
	return new TimerCommand(); // only static information, no copy needed so far
    }
    
    public void startTimer(String label) {
	assert map.get(label) == null; // not allowed to exist yet
	map.put(label, new Date());
    }
    
    /** returns milisecond of labeled timer */
    public long getTimerTime(String label) {
	return (new Date()).getTime() - map.get(label).getTime();
    }

    public void removeTimer(String label) {
	map.remove(label);
    }

    public void executeWithoutUndo() {
	assert false; // TODO: NOT YET IMPLEMENTED
    }

    public Command execute() {
	assert false; // TODO: NOT YET IMPLEMENTED
	return null;
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " [options]" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Timer command TODO." + NEWLINE + NEWLINE;
	return helpText;
    }
    
    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " TODO";
    }

}
