package commandtools;

public class UnknownCommandException extends CommandException 
{

    public UnknownCommandException() {
	super();
    }

    public UnknownCommandException(String source) {
	super(source);
    }

}
