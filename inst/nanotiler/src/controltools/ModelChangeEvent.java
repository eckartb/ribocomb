package controltools;

/** this class indicates that the underlying model has changed */
public class ModelChangeEvent extends ControlEvent {

    private String eventDescription = "undefined";

    private int eventId = 0;
    
    public ModelChangeEvent(Object source) {
	super(source);
	this.eventId = 0;
    }

    public ModelChangeEvent(Object source, int id) {
	super(source);
	this.eventId = id;
    }

    public ModelChangeEvent(Object source, String eventDescription) {
	super(source);
	this.eventDescription = eventDescription;
    }

    public ModelChangeEvent(Object source, String eventDescription, int id) {
	super(source);
	this.eventDescription = eventDescription;
	this.eventId = id;
    }

    public String getEventDescription() { return this.eventDescription; }

    public int getEventId() { return this.eventId; }

    public void setEventDescription(String s) { this.eventDescription = s; }

    public void setEventId(int id) { this.eventId = id; }

    public String toString() { return "ModelChangeEvent: " + eventId + " : " + eventDescription; }

}
