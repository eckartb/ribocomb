package controltools;

/** Listener class that helps to implements a model-view-controller 
 * design pattern. A model change listener (registered by a controller) 
 * should be notified whenever a model change event occurred 
*/
public interface ModelChangeListener {

    public void modelChanged(ModelChangeEvent e);


}
