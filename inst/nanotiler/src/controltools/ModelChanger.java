package controltools;

public interface ModelChanger {

    public void addModelChangeListener(ModelChangeListener listener);

    public void fireModelChanged(ModelChangeEvent event);

    /*public void removeModelChangeListener(ModelChangeListener listener);*/

}
