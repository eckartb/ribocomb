package generaltools;

public class AlgorithmFailureException extends Exception {

    public AlgorithmFailureException(String message) {
	super(message);
    }

    public AlgorithmFailureException(Exception e) {
	super(e);
    }

}
