package generaltools;

/** reprents a bug in the application that is non-recoverable */
public class ApplicationBugException extends RuntimeException {
    public ApplicationBugException() {
	super();
    }
    public ApplicationBugException(String msg) {
	super(msg);
    }
}
