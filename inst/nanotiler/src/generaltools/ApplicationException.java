package generaltools;

/** represent recoverable exception within an application. Try to use more specific exception */
public class ApplicationException extends Exception {

    public ApplicationException() {
	super();
    }

    public ApplicationException(String s) {
	super(s);
    }

}
