package generaltools;

import java.util.*;

public class CollectionTools {

    /** My contains method: iterator through member, return set instance if equals method is true. Returns null if not found */
    public static Object contains(Collection c, Object member) {
	for (Object obj : c) {
	    if (obj.equals(member)) {
		return obj;
	    }
	}
	return null;
    }

    /** My contains method: iterator through member, return set instance if equals method is true. Returns null if not found */
    public static Object containsIdentical(Collection c, Object member) {
	for (Object obj : c) {
	    if (obj == member) {
		return obj;
	    }
	}
	return null;
    }

    /** My contains method: iterator through member, return set instance if equals method is true. Returns null if not found */
    public static int indexOf(Collection c, Object member) {
	int count = 0;
	for (Object obj : c) {
	    if (obj.equals(member)) {
		return count;
	    }
	    ++count;
	}
	return -1;
    }

    /** My contains method: iterator through member, return index (iterator steps) if == operator is true. Returns -1 if not found */
    public static int indexOfIdentical(Collection c, Object member) {
	int count = 0;
	for (Object obj : c) {
	    if (obj == member) {
		return count;
	    }
	    ++count;
	}
	return -1;
    }

}
