package generaltools;

import java.util.Properties;

public class CommandLineTools {

    /** returns parsed command line as properties object. A command line programname file1 out=b.txt in=a.txt will be
     * stored as: {arg0=file; arg1=out=b.txt; arg2=in=a.txt; out=b.txt; in=a.txt}
     * @param args Command line parameters that are passed to main method.
     */
    public static Properties parseCommandLine(String[] args) {
	Properties result = new Properties();
	for (int i = 0; i < args.length; ++i) {
	    result.setProperty("arg" + i, args[i]); // set to arg0=firstarg ; arg1=secondarg etc
	    String[] words = args[i].split("=");
	    if (words.length == 2) {
		result.setProperty(words[0], words[1]);
	    }
	}
	return result;
    }

    public static double getDoubleArg(String name, Properties cli, double defVal) throws NumberFormatException {
	String s = cli.getProperty(name);
	if (s == null) {
	    return defVal;
	}
	return Double.parseDouble(s);
    }

    public static double getDoubleArg(String name, Properties cli, Double defVal) throws NumberFormatException {
	return getDoubleArg(name, cli, defVal.doubleValue());
    }

    public static int getIntArg(String name, Properties cli, int defVal) throws NumberFormatException {
	String s = cli.getProperty(name);
	if (s == null) {
	    return defVal;
	}
	return Integer.parseInt(s);
    }

    public static int getIntArg(String name, Properties cli, Integer defVal) throws NumberFormatException {
	return getIntArg(name, cli, defVal.intValue());
    }

    public static String getStringArg(String name, Properties cli, String defVal) {
	String s = cli.getProperty(name);
	if (s == null) {
	    return defVal;
	}
	return s;
    }

}
