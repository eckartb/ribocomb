
package generaltools;

/** describes constraint */
public interface ConstraintDouble {

    /** returns force constant that applies if constraints are violated */
    public double getForceConstant();
    
    /** get minimum distance */
    public double getMin();

    /** get maximum distance */
    public double getMax();

    /** returns zero if within range, pos - max if higher, pos-min if lower */
    public double getDiff(double pos);

}
