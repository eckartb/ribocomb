package generaltools;

import java.util.logging.ConsoleHandler;
import java.util.logging.Logger;

/** class that contains logger used for debugging as a static member */
public class DebugTools {

    private int LEVEL_SILENT = 0;
    private int LEVEL_DEFAULT = 1;
    private int LEVEL_VERBOSE = 2;
    
    private static Logger logger = Logger.getLogger("NanoTiler_debug");
    private static boolean usedBefore = false;
    
    /** returns logger used for debugging message */
    public static Logger getLogger() { 
	if (!usedBefore) {
	    logger.addHandler(new ConsoleHandler());
	    usedBefore = true;
	}
	return logger; 
    }

}
