package generaltools;

/** defines class providing real valued function */
public interface DoubleFunctor {

    /** function that maps real value to another real value */
    double doubleFunc(double value);

}
