package generaltools;

/** interface representing an object in a tree */
public interface HierarchyLeafObject {
    
    Object getParent();

    /** returns number of children (always zero for leaf objects) */
    int size();

}
