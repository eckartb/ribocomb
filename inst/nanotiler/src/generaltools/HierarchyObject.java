package generaltools;

/** interface representing an object in a tree */
public interface HierarchyObject extends HierarchyLeafObject {

    Object getChild(int n);
    
    void insertChild(Object child);

}
