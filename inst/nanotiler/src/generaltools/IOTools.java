package generaltools;

import java.io.*;

public class IOTools {

    public static String generatePath(String[] names) {
	return null;
    }

    /** Extracts relative name from absolute name */
    public static String extractRelativeName(String absoluteName) {
	assert (absoluteName.length() > 0);
	String[] words = absoluteName.split(File.separator);
	String lastWord = "";
	if (words.length > 0) {
	    lastWord = words[words.length-1];
	}
	return lastWord;
    }

}
