package generaltools;

public enum JobStatus {
    INITIAL, RUNNING, FINISHED, PENDING;
}
