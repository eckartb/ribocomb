package generaltools;

/** represent recoverable exception within an application. Try to use more specific exception */
public class MalformedInputException extends ApplicationException {

    public MalformedInputException() {
	super("Malformed input format encountered!");
    }

    public MalformedInputException(String s) {
	super(s);
    }

}
