package generaltools;

/** interface for objects with a name */
public interface Namable {

    public String getName();

    public void setName(String name);

}
