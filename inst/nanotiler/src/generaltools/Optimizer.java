package generaltools;

import java.util.Properties;

/** Interface for many optimization routines */
public interface Optimizer {

    public static final String NUMBER_STEPS = "number_steps"; // number of optimization steps
    public static final String FINAL_SCORE = "final_score"; // final score
    public static final String FINAL_STATUS = "final_status"; // final score
    public static final String START_SCORE = "start_score"; // starting score
    public static final String MESSAGE = "message"; // general comments
    public static final String ERROR = "error"; // contains error message
   
    Properties optimize();

}
