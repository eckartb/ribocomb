package generaltools;

public class ParsingException extends java.text.ParseException {

    public ParsingException(String message) {
	super(message,1);
    }

}
