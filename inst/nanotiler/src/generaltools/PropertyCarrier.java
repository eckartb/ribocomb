package generaltools;

import java.util.Properties;

public interface PropertyCarrier {

    Properties getProperties();

    String getProperty(String key);

    void setProperty(String key, String value);

    void setProperties(Properties properties);

}
