package generaltools; 

import java.io.*;
import java.text.ParseException;
import java.util.Properties;
import java.util.Enumeration;
import java.util.ResourceBundle;
import java.util.zip.DataFormatException;

import org.testng.annotations.*;

public class PropertyTools {

    public static final String DELIMITER = ";";

    public static void printProperties(PrintStream ps, Properties properties) {
	for (Enumeration keys = properties.keys(); keys.hasMoreElements(); ) {
	    String key = (String)(keys.nextElement());
	    ps.println(key + " = " + properties.getProperty(key));
	}
    }

    public static void printProperties(PrintStream ps, ResourceBundle properties) {
	for (Enumeration keys = properties.getKeys(); keys.hasMoreElements(); ) {
	    String key = (String)(keys.nextElement());
	    ps.println(key + " = " + properties.getString(key));
	}
    }

    /** Merges properties from pNew into pOrig with an optional prefix */
    public static void mergeProperties(Properties pOrig, Properties pNew, String prefix) {
	if (pNew == null) {
	    return;
	}
	if (prefix==null) {
	    prefix="";
	}
	for (Enumeration keys = pNew.keys(); keys.hasMoreElements(); ) {
	    String key = (String)(keys.nextElement());
	    pOrig.setProperty(prefix + key, pNew.getProperty(key));
	}
    }

    /** merges content of pNew into pOrig, possibly concatenating properties with same name using delimiter */
    public static void merge(Properties pOrig, Properties pNew) {
	if (pNew == null) {
	    return;
	}
	for (Enumeration keys = pNew.keys(); keys.hasMoreElements(); ) {
	    String key = (String)(keys.nextElement());
	    String newValue = pNew.getProperty(key);
	    addProperty(pOrig, key, newValue); // possibly concatenate with delimiter
	}
    }

    /** concatenated property values using semicolons */
    public static void addProperty(Properties p, String key, String newValue) {
	assert p != null;
	String val = p.getProperty(key);
	if (val == null) {
	    val = new String(newValue);
	}
	else {
	    val = val + DELIMITER + newValue;
	}
	p.setProperty(key, val);
    }

    /** returns concatenated property values using semicolons */
    public static String[] getIndividualProperties(Properties p, String key) {
	assert p != null;
	String val = p.getProperty(key);
	if (val == null) {
	    return null;
	}
	return val.split(DELIMITER);
    }

    /** Converts string of form a:something;b:somethingelse;anotheroption:1,67,3  into Properties
     * a=something
     * b=somethingelse
     * anotheroption=1,67,3
     */
    public static Properties generatePropertiesFromSingleParameter(String s) throws ParseException {
	assert s != null;
	Properties properties = new Properties();
	String[] words = s.split(";");
	for (String word : words) {
	    String[] tokens = word.split(":");
	    if (tokens.length != 2) {
		throw new ParseException("Excpected colon (:) character in word " + word, 0);
	    }
	    properties.setProperty(tokens[0], tokens[1]);
	}
	return properties;
    }

    @Test(groups={"new"})
    public void testGeneratePropertiesFromSingleParameter() {
	String s1 = "hey:you;car:jeep";
	try {
	    Properties prop1 = generatePropertiesFromSingleParameter(s1);
	    assert "you".equals(prop1.getProperty("hey"));
	    assert "jeep".equals(prop1.getProperty("car"));
	    assert prop1.size() == 2;
	}
	catch(ParseException pe) {
	    System.out.println(pe.getMessage());
	    assert false;
	}
    }


    /** returns parsed integer value or zero if not found. Potentially throws NumberFormatException */
    public static int parseInt(Properties p, String key) {
	if (p.getProperty(key) == null) {
	    return 0;
	}
	return Integer.parseInt(p.getProperty(key));
    }

    /** returns parsed integer value or zero if not found. Potentially throws NumberFormatException */
    public static double parseDouble(Properties p, String key) throws DataFormatException {
	if (p.getProperty(key) == null) {
	    throw new DataFormatException("Property " + key + " undefined.");
	}
	return Double.parseDouble(p.getProperty(key));
    }

    /** Adds property values in numeric sense, not concatenation ("3.0" and 4.0 is stored as "7.0" */
    public static void plusProperty(Properties p, String key, double value) {
	if (p.getProperty(key) == null) {
	    p.setProperty(key, "" + value);
	}
	else {
	    p.setProperty(key, "" + (Double.parseDouble(p.getProperty(key)) + value));
	}
    }

    /** Adds property values in numeric sense, not concatenation ("3.0" and 4.0 is stored as "7.0" */
    public static void plusProperty(Properties p, String key, int value) {
	assert p != null;
	if (p.getProperty(key) == null) {
	    p.setProperty(key, "" + value);
	}
	else {
	    p.setProperty(key, "" + (Integer.parseInt(p.getProperty(key)) + value));
	}
    }

    /** Writes property file to PDB remark section  */
    public static void writeToPdbRemarks(OutputStream os, Properties p) {
	PrintStream ps = new PrintStream(os);
	for (Enumeration keys = p.keys(); keys.hasMoreElements(); ) {
	    String key = (String)(keys.nextElement());
	    ps.println("REMARK   " + key + "=" + p.getProperty(key));
	}
    }

}
