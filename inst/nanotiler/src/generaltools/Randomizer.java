package generaltools;

import java.util.Date;
import java.util.Random;

public class Randomizer {

    /** should not be negative: if so, different instances
     * of random objects are generated, and the NanoTiler seed command
     * will stop functioning */
    public static long SEED = generateNewSeed(); 

    private static Random random = new Random(SEED);

    public static Random getInstance() {
	if (SEED <= 0) {
	    assert false; // currently not allowed
	    return new Random();
	}
	else {
	    assert random != null;
	    return random;
	}
    }

    /** sets seed of random number generator */
    public static long getSeed() {
	return SEED;
    }

    /** sets seed of random number generator */
    public static void setSeed(long seed) {
	SEED = seed;
	random.setSeed(seed);
    }

    /** Generates new seed */
    public static long generateNewSeed() {
	int result = new Date().hashCode();
	String pid =  System.getProperty("pid");
	if (pid != null) {
	    result += pid.hashCode();
	}
	result = Math.abs(result) + 1; // must be positive
	return (long)result;
    }

}
