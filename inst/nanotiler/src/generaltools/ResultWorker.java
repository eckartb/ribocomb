package generaltools;

/** Abstract class for classes computing a result using expensive "run" method */
public abstract class ResultWorker implements Runnable {

    // protected JobStatus jobStatus = JobStatus.INITIAL;
    protected Object result = null;
    private boolean valid = true;
    private Exception exception = null;

    // public JobStatus getJobStatus() { return jobStatus; }

    public Exception getException() { return exception; }

    public Object getResult() {
	if (result == null) {
	    run();
// 	    assert jobStatus == JobStatus.FINISHED;
// 	    assert jobStatus != JobStatus.RUNNING;
	}
// 	assert jobStatus != JobStatus.INITIAL;
// 	assert jobStatus != JobStatus.RUNNING;
// 	assert jobStatus == JobStatus.FINISHED;
	return result;
    }

    public void run() {
	runInternal();
// 	jobStatus = JobStatus.FINISHED;
// 	assert jobStatus == JobStatus.FINISHED;
// 	assert jobStatus != JobStatus.RUNNING;
    }

    protected abstract void runInternal();

    public void setException(Exception exception) { this.exception = exception; }

    public void setResult(Object other) { this.result = other; }

    public boolean validate() { return valid; }

    public void setValid(boolean flag) { valid = flag; }

}
