package generaltools;

import java.util.*;

/** Wrapper class used to sort wrapped class */
public class ScoreWrapper implements Comparable<ScoreWrapper> {

    private Object object;
    private double score;

    public ScoreWrapper(Object object) {
	this.object = object;
    }

    public ScoreWrapper(Object object, double score) {
	this.object = object;
	this.score = score;
    }

    public int compareTo(ScoreWrapper otherWrapper) {
	// assert other instanceof ScoreWrapper;
	// ScoreWrapper otherWrapper = (ScoreWrapper) other;
	if (getScore() < otherWrapper.getScore()) {
	    return -1;
	}
	else if (getScore() > otherWrapper.getScore()) {
	    return 1;
	}
	return 0;
    }

    public Object getObject() {
	return object;
    }

    public double getScore() {
	return score;
    }

    public void setScore(double score) {
	this.score = score;
    }

    /** sort vector of elements (fast!)
	but also return the old rank order as Vec<unsigned int> 
    */
    int[] documentedSort(List<? extends Comparable> v) {
	assert(v.size() > 0);
	List<ScoreWrapper> order = new ArrayList<ScoreWrapper>();
	for (int i = 0; i < v.size(); ++i) {
	    order.add(new ScoreWrapper(v.get(i), (double)i));
	}
	Collections.sort(order);
	int[] resultOrder = new int[v.size()];
	for (int i = 0; i < v.size(); ++i) {
	    resultOrder[i] = (int)(order.get(i).getScore());
	}
	return resultOrder;
    }


}
