package generaltools;

/** describes constraint */
public class SimpleConstraintDouble implements  ConstraintDouble {

    double min;
    double max;
    double forceConstant;
    boolean squareMode = true; // false;

    public SimpleConstraintDouble(double min, double max) {
	this.min = min;
	this.max = max;
	this.forceConstant = 1.0;
    }

    public SimpleConstraintDouble(double min, double max, double force) {
	this.min = min;
	this.max = max;
	this.forceConstant = force;
    }

    public double getForceConstant() { return forceConstant; }

    public double getMin() { return min; }

    public double getMax() { return max; }

    /** returns zero if within range, pos - max if higher, pos-min if lower */
    public double getDiff(double pos) {
	double result = 0.0;
	if (pos > max) {
	    result = pos - max;
	}
	else if (pos < min) {
	    result = min - pos; // fixed bug!
	}
	if (squareMode) {
	    result *= result;
	}
	assert result >= 0.0;
	return result;
    }

    public void setSquareMode(boolean b) { this.squareMode = b; }

    public String toString() { return "(Constraint " + min + " " + max + " " + forceConstant + " )"; }

}
