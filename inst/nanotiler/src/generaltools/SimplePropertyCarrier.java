package generaltools;

import java.util.Properties;

public class SimplePropertyCarrier implements PropertyCarrier {

    protected Properties properties;
    
    public SimplePropertyCarrier() {
	this.properties = new Properties();
    }

    public SimplePropertyCarrier(Properties properties) {
	this.properties = properties;
    }
    
    public String getProperty(String key) {
	return properties.getProperty(key);
    }

    public Properties getProperties() {
	return properties;
    }

    public void setProperties(Properties properties) {
	this.properties = properties;
    }

    /** Adds set of properties to current properties with an added prefix. */
    public void mergeProperties(Properties other, String prefix) {
	PropertyTools.mergeProperties(properties, other, prefix);
    }
    
    public void setProperty(String key, String value) {
	properties.setProperty(key, value);
    }

    public boolean validate() {
	return properties != null;
    }

}
