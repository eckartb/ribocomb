package generaltools;

import java.io.PrintStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class StringTools {

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    /** system independent newline
     * @see http://www.leepoint.net/notes-java/GUI/components/40textarea/40newline.html
     */
    public static final String ENDL = System.getProperty("line.separator");

    /** Returns concatenated words */
    public static String paste(String[] words, String separator) {
	if ((words == null) || (words.length == 0)) {
	    return "";
	}
	StringBuffer buf = new StringBuffer();
	buf.append(words[0]);
	for (int i = 1; i < words.length; ++i) {
	    buf.append(separator);
	    buf.append(words[i]);
	}
	return buf.toString();
    }

    /** Returns index of first found digit or -1 if no digit found. */
    public static int indexOfDigit(String word) {
	for (int i = 0; i < word.length(); ++i) {
	    if (Character.isDigit(word.charAt(i))) {
		return i;
	    }
	}
	return -1; // not found!
    }

    /** Return set of string consisting only of digits from right */
    public static String getRightDigits(String s) {
	StringBuffer buf = new StringBuffer();
	for (int i = s.length()-1; i >= 0; --i) {
	    if (Character.isDigit(s.charAt(i))) {
		buf.append(s.charAt(i));
	    }
	}
	return buf.reverse().toString();
    }

    /** Return set of string consisting only of digits from left */
    public static String getLeftDigits(String s) {
	StringBuffer buf = new StringBuffer();
	for (int i = 0; i < s.length(); ++i) {
	    if (Character.isDigit(s.charAt(i))) {
		buf.append(s.charAt(i));
	    }
	}
	return buf.toString();
    }

    /** returns ids for text string, example: the string "3-5" is parsed to 3,4,5. Comparable with unix cut command,
    ** except trailing minus is not allowed, also no commas allowed (use command parseNumbers instead for comma handling) */
    public static List<Integer> parseSequence(String s) throws ParsingException {
	String[] words = s.split("-");
	List<Integer> result = new ArrayList<Integer>();
	if (words.length == 1) {
	    result.add(new Integer(Integer.parseInt(words[0])));
	    return result;
	}
	if (words.length == 2) {
	    int min = Integer.parseInt(words[0]);
	    int max = Integer.parseInt(words[1]);
	    for (int i = min; i <= max; ++i) {
		result.add(new Integer(i));
	    }
	}
	else {
	    throw new ParsingException("Expected string of form 3-5 or 7 or 18-9 to specify uninterrupted sequence of numbers.");
	}
	return result;
    }

    /** returns ids for text string, example: the string "3-5,7,9" is parsed to 3,4,5,7,9. Comparable with unix cut command,
    ** except trailing minus is not allowed */
    public static List<Integer> parseNumbers(String s) throws ParsingException {
	List<Integer> result = new ArrayList<Integer>();
	String[] words= s.split(",");
	for (int i = 0; i < words.length; ++i) {
	    result.addAll(parseSequence(words[i]));
	}
	return result;
    }

    /** generates entirely new set of lines with same content */
    public static String[] cloneLines(String[] lines) {
	if (lines == null) {
	    return null;
	}
	String[] result = new String[lines.length];
	for (int i = 0; i < lines.length; ++i) {
	    if (lines[i] != null) {
		result[i] = (String)(lines[i].substring(0, lines[i].length()));
	    }
	}
	return result;
    }

    /** returns number of characters equal to c in string */
    public static int countChar(String s, char c) {
	int count = 0;
	for (int i = 0; i < s.length(); ++i) {
	    if (s.charAt(i) == c) {
		++count;
	    }
	}
	return count;
    }

    /** fill right side of string until s has length len */
    public static String fillRight(String s, int len, String fill) {
	while (s.length() < len) {
	    s = s + fill;
	}
	return s;
    }

    /** fill left side of string until s has length len */
    public static String fillLeft(String s, int len, String fill) {
	while (s.length() < len) {
	    s = fill + s;
	}
	return s;
    }

    /** fill right side of string until s has length len */
    public static String stringWithLength(String s, int len, String fill) {
	if (s.length() > len) {
	    return s.substring(0, len);
	}
	else if (s.length() == len) {
	    return s;
	}
	while (s.length() < len) {
	    s = s + fill;
	}
	if (s.length() > len) {
	    return s.substring(0, len);
	}
	return s;
    }

    public static void printStride(PrintStream ps, String s, int stride) {
	for (int i = 0; i < s.length(); ++i) {
	    if ((i > 0) && ((i % stride) == 0)) {
		ps.print(" ");
	    }
	    ps.print(s.substring(i,i+1));
	}
    }

    /** TODO */
    public String  streamToString(InputStream is) {
	assert false;
	return null;
    }

    /** TODO */
    public InputStream  stringToStream(String s) {
	assert false;
	return null;
    }

    /** generates a string from a single character */
    public static String stringFromChar(char c) {
	char[] chars = new char[1];
	chars[0] = c;
	return new String(chars);
    }

    /** generates a string of length n from a single character */
    public static String stringFromChar(char c, int n) {
	char[] chars = new char[n];
	for (int i = 0; i < n; ++i) {
	    chars[i] = c;
	}
	return new String(chars);
    }

    /** reads a single word from data stream */
    public static String readWord(InputStream is) {
	DataInputStream dis = new DataInputStream(is);
	String s = new String("");
	char c = ' ';
	
	// first skip white space
	do {
	    try {
		c = (char)dis.readByte();
	    }
	    catch (IOException e) {
		break; // end of file reached
	    }
	}
	while (Character.isWhitespace(c));
		
	if (!Character.isWhitespace(c)) {
	    s = s + c;
	}
	else {
	    log.finest("Found word: " + s);
	    return s;
	}
	
	while (true) {
	    try {
		c = (char)dis.readByte();
		if (!Character.isWhitespace(c)) {
		    s = s + c;
		}
		else {
		    break;
		}
	    }
	    catch (IOException e) {
		break; // end of file reached
	    }
	}
	log.finest("found word: " + s);
	return s;
    }

    /** reads one line of text */
    public static String readLine(InputStream is) throws IOException {
	BufferedReader bufReader = new BufferedReader(new InputStreamReader(is));
	return bufReader.readLine();
    }

    /** converts array of string to list of string */
    public static List<String> convertArrayToList(String[] linesOrig) {
	List<String> lines = new ArrayList<String>();
	for (int i = 0; i < linesOrig.length; ++i) {
	    lines.add(linesOrig[i]);
	}
	return lines;
    }

    /** converts array of string single string seperated by NEWLINE */
    public static String convertArrayToString(String[] linesOrig) {
	StringBuffer buf = new StringBuffer();
	for (String s: linesOrig) {
	    buf.append( s + ENDL);
	}
	return buf.toString();
    }

    /** Returns index of line for which equals method is true */
    public static int indexOfString(String[] lines, String word) {
	assert word != null && lines != null;
	for (int i = 0;i < lines.length; ++i) {
	    if (word.equals(lines[i])) {
		return i;
	    }
	}
	return -1;
    }
    
    /** reads all lines from a stream */
    public static String[] readAllLines(InputStream is) throws IOException {
	// DataInputStream dis = new DataInputStream(is);
	BufferedReader d
	    = new BufferedReader(new InputStreamReader(is));
	List<String> lines = new ArrayList<String>();
	String line;
	do {
	    line = d.readLine();
	    if (line != null) {
		lines.add(line);
	    }
	}
	while (line != null);
	String[] result = new String[lines.size()];
	for (int i = 0; i < result.length; ++i) {
	    result[i] = (String)(lines.get(i));
	}
	return result;
    }
    
}
