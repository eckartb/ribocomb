package generaltools;

public class TestTools {

    public static final String WRAP_TOKEN = "#####";

    public static String generateMethodHeader(String methodName) {
	return WRAP_TOKEN + " Testing " + methodName + " " + WRAP_TOKEN;
    }

    public static String generateMethodFooter(String methodName) {
	return WRAP_TOKEN + " Finished testing " + methodName + " " + WRAP_TOKEN;
    }


}
