package generaltools;

public interface Validatable {

    boolean validate();

}
