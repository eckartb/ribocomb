package graphtools;

import java.io.PrintStream;
import java.util.List;
import java.util.ArrayList;

public class AdjacencyTools {

    /** generates one set of connected nodes containing node n. Not equivalent to clique problem: in a group, there has to be only a path from one group member to any other, but each member does not have to be connected to all other members of the same group */
    private static List<Integer> findConnectedSet(boolean[][] adjacencyMatrix, int n) {
	List<Integer> result = new ArrayList<Integer>();
	result.add(new Integer(n));
	boolean isOk = true;
	do {
	    isOk = true;
	    for (int i = 0; i < adjacencyMatrix.length; ++i) {
		if (i == n) {
		    continue;
		}
		Integer newI = new Integer(i);
		if (!result.contains(newI)) {
		    // check if connected to some existing group members:
		    for (int j = 0; j < result.size(); ++j) {
			int testNode = result.get(j).intValue();
			if (adjacencyMatrix[testNode][i]) {
			    result.add(newI);
			    isOk = false;
			    break;
			}
		    }
		}
	    }
	}
	while (!isOk);
	return result;
    }

    /** generates sets of connected nodes. Not equivalent to clique problem: in a group, there has to be only a path from one group member to any other, but each member does not have to be connected to all other members of the same group */
    public static List<List<Integer> > findConnectedSets(boolean[][] adjacencyMatrix) {
	List<List<Integer> > result = new ArrayList<List<Integer> >();
	for (int i = 0; i < adjacencyMatrix.length; ++i) {
	    Integer newN = new Integer(i);
	    if (!contains(result, newN)) {
		List<Integer> newClique = findConnectedSet(adjacencyMatrix, i);
		result.add(newClique);
	    }
	}
	return result;
    }

    private static boolean contains(List<List<Integer> > lists, Integer n) {
	for (int i = 0; i < lists.size(); ++i) {
	    if (lists.get(i).contains(n)) {
		return true;
	    }
	}
	return false;
    }

    public static void writeMatrix(PrintStream ps, boolean[][] matrix) {
	for (int i = 0; i < matrix.length; ++i) {
	    for (int j = 0; j < matrix[i].length; ++j) {
		if (matrix[i][j]) {
		    ps.print("1 ");
		}
		else {
		    ps.print("0 ");
		}
	    }
	    ps.println(""); // new line
	}
    }

    public static void writeGroup(PrintStream ps, List<Integer> group) {
	for (int i = 0; i < group.size(); ++i) {
	    ps.print("" + group.get(i) + " ");
	}
    }

    public static void writeGroups(PrintStream ps, List<List<Integer> > groups) {
	for (int i = 0; i < groups.size(); ++i) {
	    writeGroup(ps, groups.get(i));
	    ps.println();
	}
    }


}
