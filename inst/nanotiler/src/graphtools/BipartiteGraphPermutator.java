package graphtools;

import java.io.*;
import java.util.ArrayList;
import java.math.BigInteger;
import java.util.List;
import java.util.logging.*;
import generaltools.TestTools;

import org.testng.annotations.*;

/**  Iterating through all combinations in which there is at most this many different nummbers.
*/
public class BipartiteGraphPermutator implements IntegerPermutator {

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    private IntegerPermutator posGenerator;
    private IntegerPermutator valueGenerator;
    private IntegerPermutatorList permutatorList;
    private List<IntegerArrayConstraint> constraints = new ArrayList<IntegerArrayConstraint>();
    private int toLen;
    private int numConn;
    private boolean uptodate = false;

    /**  @param toLen number of vertices (one half on bipartite graph)
     *  @param numConn number of connections in bipartite graph
     */
    public BipartiteGraphPermutator(int toLen, int numConn) {
  	assert numConn > 0;
	assert toLen >= (2*numConn);
	this.numConn = numConn;
	this.toLen = toLen;
	this.posGenerator = new IntegerArrayIncreasingGenerator(2*numConn, toLen);
	this.valueGenerator = new PermutationGenerator(2*numConn);
	this.permutatorList = new IntegerPermutatorList();
	this.permutatorList.add(posGenerator);
	this.permutatorList.add(valueGenerator);
	reset();
	assert validate();
    }

    public void addConstraint(IntegerArrayConstraint constraint) {
	assert false; // not yet implemented
	constraints.add(constraint);
	reset();
	assert validate();
    }

    public Object clone() { throw new RuntimeException("clone method not supported."); } // FIXIT

    /** Returns total number of instances (not counting constraints) */
    public BigInteger getTotal() {
	return permutatorList.getTotal();
    }

    /** returns true if permutation can be increased. Example: max mapping from len 3 to len 7 is: 6 5 4 */
    public boolean hasNext() {
	return permutatorList.hasNext();
    }

    public boolean inc() {
	assert hasNext();
	do {
	    permutatorList.inc();
	}
	while (permutatorList.hasNext() && (!validate()));
	if (!validate()) {
	    return false;
	}
	return true;
    }

//     /** return described end position of edge */
//     int computeEndValue(int[] stopBinaries, int n) {
// 	int count = 0;
// 	for (int i = 0; i < stopBinaries.length; ++i) {
// 	    if (stopBinaries[i] > 0) {
// 		if (count == n) {
// 		    return i; 
// 		}
// 		++count;
// 	    }
// 	}
// 	assert false;
// 	return -1;
//     }

//     /** Update cached result value */
//     private boolean update() {
//  	if ((result == null) || (result.length != size())) {
//  	    result = new int[size()];
//  	}
// 	assert result.length / 2 == numConn;
// 	int[] startValues = startGenerator.get();
// 	int[] stopBinaries = endposGenerator.get();
// 	int[] values = valueGenerator.get();
// 	for (int i = 0; i < numConn; ++i) {
// 	    result[2*i] = startValues[i];
// 	    int endValue = computeEndValue(stopBinaries, values[i]);
// 	    result[2*i+1] = endValue;
// 	}
// 	uptodate = true;
// 	return true;
//     }

    private void printVec(PrintStream ps, int[] positions) {	
	for (int i = 0; i < positions.length; ++i) {
	    ps.print(" " + positions[i]); 
	}
	ps.println();
    }

    /** returns current set of values */
    public int[] get() {
	int[] values = valueGenerator.get();
	int[] result = new int[values.length];
	int[] positions = posGenerator.get();
	assert values.length == positions.length;
	for (int i = 0; i < result.length; ++i) {
	    assert values[i] < positions.length;
	    result[i] = positions[values[i]];
	}
	return result;
    }
    
    /** Returns next array of integers */
    public int[] next() { inc(); return get(); }

    /** Resets to first state that validates */
    public void reset() {
	System.out.println("Starting reset");
	posGenerator.reset();
	valueGenerator.reset();
	permutatorList.reset();
	while ((!validate()) && hasNext()) {
	    System.out.println("Increasing vector ");
	    printVec(System.out, get());
	    inc();
	}
	System.out.println("Finished reset:");
	printVec(System.out, get());
	assert validate();
    }

    /** returns number of elements of returned arrays */
    public int size() { return 2*numConn; }

    public boolean validate() {
	int[] data = get();
	// each connection has to go in increasing order, like 1->4 but not the other way around
	for (int i = 0; i < numConn; ++i) {
	    if (data[2*i] >= data[2*i+1]) {
		return false;
	    }
	}
	// consecutive connections have to have increasing starting points
	for (int i = 1; i < numConn; ++i) {
	    if (data[2*i] <= data[2*(i-1)]) {
		return false;
	    }
	}
	return true;
    }

    public String toString() {
	int[] result = get();
	if (result == null) {
	    return "null";
	}
	String s = "";
	for (int i = 0; i < result.length; ++i) {
	    s = s + result[i] + " ";
	}
	return s;	
    }

}
