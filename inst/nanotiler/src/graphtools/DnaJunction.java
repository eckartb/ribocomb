package graphtools;

public interface DnaJunction {

    public int getEdgeCount();

    /** returns the 5' connection (the index of that edge) with respect to the n'th edge */
    public int get5PrimeConnection(int n);

    /** returns the 5' connection (the index of that edge) with respect to the n'th edge */
    public int get3PrimeConnection(int n);

    /** returns true if there are more permutations */
    boolean hasNext();

    /** returns index of current permutation */
    int getPermutationId();

    /** sets to next permutation */
    void next();

    /** sets to a random status */
    void setRandom();

    /** returns total number of permutations */
    int permutationCount();

    /** returns to first permutation */
    void reset();
}
