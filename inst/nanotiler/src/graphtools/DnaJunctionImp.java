package graphtools;

import java.util.Random;
import java.util.logging.Logger;
import generaltools.Randomizer;

/** defines connectivity of n stems in an n'way junction */
public class DnaJunctionImp implements DnaJunction {

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    // this array has length (n-1)  ! The n'th element is thought to be constant and equal to n
    private int permutation[];
    private int permutationId;
    private PermutationGenerator permGenerator;
    private static Random rnd = Randomizer.getInstance();

    /** supply with n for n-way junction. n>=2 required */
    public DnaJunctionImp(int n) {
	if (n < 2) {
	    throw new IllegalArgumentException("Minimum argument for DnaJunctionImp(n): 2");
	}
	permGenerator = new PermutationGenerator(n-1);
	reset();
    }

    public int getEdgeCount() { return permutation.length+1; }

    /** returns the 5' connection (the index of that edge (==stem), whose 3'strand-seqgment is connected to the 5' strand-segment of the n'th stem) 
	with respect to the n'th edge */
    public int get5PrimeConnection(int n) { 
	if (n == permutation.length) {
	    // special case for highest index:
	    return permutation[0];
	}
	for (int i = 0; i < permutation.length; ++i) {
	    if (permutation[i] == n) {
		if ((i+1) < permutation.length) {
		    return (permutation[i+1]); // should never give array out of bound error, otherwise internal error
		}
		else {
		    return permutation.length;
		}
	    }
	}
	log.warning("Could not find " + n + " " + toString()); 
	return permutation[-1]; // ring. returns right neighbor
    }

    /** returns the 3' connection (the index of that edge) with respect to the n'th edge */
    public int get3PrimeConnection(int n) {
	if (n == permutation.length) {
	    // special case for highest index:
	    return permutation[permutation.length-1];
	}
	for (int i = 0; i < permutation.length; ++i) {
	    if (permutation[i] == n) {
		if (i > 0) {
		    return (permutation[i-1]); // should never give array out of bound error, otherwise internal error
		}
		else {
		    return permutation.length;
		}
	    }
	}
	log.warning("Could not find " + n + " " + toString()); 
	return permutation[-1]; // ring. returns right neighbor
    }

    public int[] getPermutation() { return permutation; }

    /** returns true if there are more permutations */
    public boolean hasNext() { return permGenerator.hasMore(); }

    /** returns index of current permutation */
    public int getPermutationId() { return permutationId; }

    /** gets next permutation */
    public void next() { 
	permutation = permGenerator.getNext(); 
	++permutationId; 
    }

    /** returns total number of permutations */
    public int permutationCount() { return permGenerator.getTotal().intValue(); }

    /** returns to first permutation */
    public void reset() { 
	permGenerator.reset(); 
	permutation = permGenerator.getNext(); 
	permutationId = 0;
    }

    /** sets to a random status */
    public void setRandom() {
	int r = rnd.nextInt(permutationCount());
	reset();
	for (int i = 0; i < r; ++i) {
	    next();
	}
    }

    /** returns status of permutation generator */
    public String toString() {
	String result = "";
	result = result + permGenerator.size() + " " + permutationId + " " + hasNext();
	return result;
    }

}
