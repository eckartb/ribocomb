package graphtools;

/** defines set of n DnaJunctions. Provides iteration over all combinations */
public interface DnaJunctionSet {

    /** adds a junction */
    public void add(DnaJunction junction);

    public DnaJunction getJunction(int n);

    /** returns true if there are more permutations */
    public boolean hasNext();

    /** gets next permutation */
    public void next();

    /** returns to first permutation */
    public void reset();

    /** returns number of defined junctions */
    public int size();

    // public int total(); // total number of combinations

}
