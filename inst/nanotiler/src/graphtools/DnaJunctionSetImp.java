package graphtools;

import java.util.ArrayList;
import java.util.List;

/** defines connectivity of n stems in an n'way junction */
public class DnaJunctionSetImp implements DnaJunctionSet {

    private List<DnaJunction> junctions = new ArrayList<DnaJunction>();
    private int permutationCount;

    /** supply with n for n-way junction. n>=2 required */
    public DnaJunctionSetImp() {
	reset();
    }

    public void add(DnaJunction junction) {
	junctions.add(junction);
    }

    public DnaJunction getJunction(int n) {
	return (DnaJunction)(junctions.get(n));
    }

    /** returns true if there are more permutations */
    public boolean hasNext() { 
	for (int i = 0; i < junctions.size(); ++i) {
	    DnaJunction junction = getJunction(i);
	    if (junction.hasNext()) {
		return true;
	    }
	}
	return false;
    }

    /** gets next permutation */
    public void next() { 
	for (int i = 0; i < junctions.size(); ++i) {
	    DnaJunction junction = getJunction(i);
	    if (junction.hasNext()) {
		junction.next();
		return;
	    }
	    else {
		junction.reset();
	    }
	}
    }

    /** returns to first permutation */
    public void reset() { 
	for (int i = 0; i < junctions.size(); ++i) {
	    DnaJunction junction = getJunction(i);
	    junction.reset();
	    permutationCount = 0;
	}
    }

    public int size() { return junctions.size(); }

    public String toString() {
	String result = "";
	result = result + size() + " " + hasNext() + "  ";
	for (int i = 0; i < size(); ++i) {
	    result = result + getJunction(i) + "  ";
	}
	return result;
    }

}
