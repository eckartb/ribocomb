package graphtools;

import java.util.logging.Logger;

import generaltools.ApplicationBugException;

public class DnaJunctionSetTools {

    public static int verboseLevel = 0;

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    /** generates a n-way junction for node in graph */
    public static DnaJunction generateJunction(GraphBase graph,
					       int nodeId) {
	return new DnaJunctionImp(graph.getConnections(nodeId).size());
    }

    /** generates a n-way junction for node in graph */
    public static DnaJunction generateRandomJunction(GraphBase graph,
					       int nodeId) {
	DnaJunction junction = new DnaJunctionImp(graph.getConnections(nodeId).size());
	junction.setRandom(); // set to random status
	return junction;
    }

    /** generates a n-way junction for each node in graph */
    public static DnaJunctionSet generateJunctionSet(GraphBase graph) {
	DnaJunctionSet junctionSet = new DnaJunctionSetImp();
	for (int i = 0; i < graph.getNodeCount(); ++i) {
	    int numConn = graph.getConnections(i).size();
	    if (numConn < 2) {
		log.warning("graph node with " + numConn + " connections detected: "
				   + i + "  " + graph.getConnections(i)); 
		
	    }
	    else {
		junctionSet.add(generateJunction(graph, i));
	    }
	}
	return junctionSet;
    }

    /** generates a n-way junction for each node in graph */
    public static DnaJunctionSet generateRandomJunctionSet(GraphBase graph) {
	DnaJunctionSet junctionSet = new DnaJunctionSetImp();
	for (int i = 0; i < graph.getNodeCount(); ++i) {
	    int numConn = graph.getConnections(i).size();
	    if (numConn < 2) {
		log.warning("graph node with " + numConn + " connections detected: "
				   + i + "  " + graph.getConnections(i)); 
		
	    }
	    else {
		junctionSet.add(generateRandomJunction(graph, i));
	    }
	}
	return junctionSet;
    }

    /** Important method that generates one circular path in 5' direction going from startEdge.getFirst to startEdge.getSecond. 
     * Assumes one-to-one correspondence between junctions and nodes in the graph 
     */
    public static IntegerList generateEdgePath(IntPair startEdge,
						   GraphBase graph,
						   DnaJunctionSet junctions) 
    {
	IntegerList result = new IntegerList();
	result.add(startEdge.getFirst());
	result.add(startEdge.getSecond());
	int newNode;
	int oldNode = startEdge.getFirst();
	int currentNode = startEdge.getSecond();
	if (verboseLevel > 1) {
	    log.fine("Starting generateEdgePath!");
	    log.fine("starting from nodes " + oldNode + " " + currentNode);
	}
	IntPair currentEdge = new IntPair(0,0);
	while (result.size() > 0) { // endless loop
	    // find out edge id: what is the edge id from oldNode with respect to currentNode
	    currentEdge.setFirst(oldNode);
	    currentEdge.setSecond(currentNode);
	    int edgeId = graph.nodeIndexOf(currentEdge, currentNode);
	    if (edgeId == -1) {
		throw new ApplicationBugException("Internal error (1) in generateEdgePath");
	    }
	    int newEdgeId = junctions.getJunction(currentNode).get3PrimeConnection(edgeId);
	    IntPair newEdge = graph.getNodeEdge(newEdgeId, currentNode);
	    newNode = newEdge.getFirst();
	    if (newNode == currentNode) {
		newNode = newEdge.getSecond();
	    }
	    if (verboseLevel > 1) {
		log.fine("Current Node, oldNode, newNode, edgeId: " + currentNode + " "
				   + oldNode + " " + newNode + " " + edgeId);
	    }
	    if (result.contains(newNode)) {
		// if ((result.size() > 2) && (result.get(0) == newNode)) {
		if (result.size() > 2) {
		    result.add(newNode);
		    if (verboseLevel > 1) {
			log.fine("Returned path: " + result);
		    }
		    return result;
		}
		else {
		    log.warning("bad path detected! " 
				       + " path so far: " + result
				       + " new node: " + newNode);
		    
		    return result; // bad path! bad circle!
		}
	    }
	    result.add(newNode);
	    oldNode = currentNode;
	    currentNode = newNode;
	}
	if (verboseLevel > 1) {
	    log.fine("Finished generateEdgePath: " + result);
	}
	return result;
    }

    /** returns true if path contains edge. Directionalist matters! */
    public static boolean containsEdge(IntegerList path, IntPair edge) {
	if ((path.get(path.size()-1) == edge.getFirst())
	    && (path.get(0) == edge.getSecond())) {
	    return true;
	}
	for (int i = 1; i < path.size(); ++i) {
	    if ((path.get(i-1) == edge.getFirst())
		&& (path.get(i) == edge.getSecond())) {
		return true;
	    }
	}
	return false;
    }

    public static boolean isEdgeFree(IntPair edge, IntegerListList paths) {
	int occurence = 0;
	for (int i = 0; i < paths.size(); ++i) {
	    if (containsEdge(paths.get(i), edge)) {
		++occurence;
		if (occurence > 1) {
		    return false;
		}
	    }
	}
	return true;
    }

    public static int countEdgeOccurence(IntPair edge, IntegerListList paths) {
	int occurence = 0;
	for (int i = 0; i < paths.size(); ++i) {
	    if (containsEdge(paths.get(i), edge)) {
		++occurence;
	    }
	}
	return occurence;
    }

    public static boolean isCoveringPathSet(IntegerListList paths, GraphBase graph) {
	for (int i = 0; i < graph.getEdgeCount(); ++i) {
	    if ((countEdgeOccurence(graph.getEdge(i), paths) != 1) 
		|| (countEdgeOccurence(graph.getEdge(i).reverse(), paths) != 1)) {
		return false;
	    }
	}
	return true;
    }

    /** returns list of paths (list of strands) corresponding to graph and DnaJunctionSet connectivity */
    public static IntegerListList generatePaths(GraphBase graph, DnaJunctionSet junctions) {
	IntegerListList result = new IntegerListList();
	for (int i = 0; i < graph.getEdgeCount(); ++i) {
	    if (verboseLevel > 1) {
		log.fine("generatePaths iteration " + i);
		log.fine("working on edge " + i);
		log.fine("Result so far: " + result);
	    }
	    IntPair edge = graph.getEdge(i);
	    if (isEdgeFree(edge, result)) {
		// generate path that is as long as possible:		
		IntegerList path = generateEdgePath(edge, graph, junctions);
		if ((path != null) && (path.size() > 1)) {
		    int currLen = path.size();
		    boolean found = false; // no collision found so far
		    do {
			found = false;
			// check if path already exists in other form:
			IntegerList subPath = path.subset(0, currLen);
			for (int j  = 0; j < result.size(); ++j) {
			    // if (PathTools.isSameCircular(path, result.get(j))) {
			    if (PathTools.isOverlapping(subPath, result.get(j))) {
				found = true;
				if (verboseLevel > 1) {
				    log.fine("sub path is overlapping: " + subPath + " ; " + result.get(j));
				}
				break;
			    }
			}
		    }
		    while (found && (currLen-- > 1));
		    if ((!found) && (currLen > 1)) {
			if (verboseLevel > 1) {
			    log.fine("found total path: " + path + " length: " + currLen);
			    log.fine("adding path: " + path.subset(0, currLen));
			}
			result.add(path.subset(0, currLen));
		    }
		    else {
			if (verboseLevel > 1) {
			    log.fine("not adding path because it already exists: " + path);
			}
		    }
		}
	    }
	    edge = edge.reverse(); // path in opposite direction
	    if (isEdgeFree(edge, result)) {
		IntegerList path = generateEdgePath(edge, graph, junctions);
// 		if (path != null) {
// 		    boolean found = false; 
// 		    // check if path already exists in other form:
// 		    for (int j  = 0; j < result.size(); ++j) {
// 			// if (PathTools.isSameCircular(path, result.get(j))) {
// 			if (PathTools.isOverlapping(path, result.get(j))) {
// 			    found = true;
// 			    break;
// 			}
// 		    }
// 		    if (!found) {
// 			result.add(path);
// 		    }
// 		    else {
// 		    }
// 		}
		if (path != null) {
		    int currLen = path.size();
		    boolean found = false; // no collision found so far
		    do {
			found = false;
			// check if path already exists in other form:
			IntegerList subPath = path.subset(0, currLen);
			for (int j  = 0; j < result.size(); ++j) {
			    // if (PathTools.isSameCircular(path, result.get(j))) {
			    if (PathTools.isOverlapping(subPath, result.get(j))) {
				found = true;
				if (verboseLevel > 1) {
				    log.fine("sub path 2 is overlapping: " + subPath + " ; " + result.get(j));
				}
				break;
			    }
			}
		    }
		    while (found && (currLen-- > 1));
		    if ((!found) && (currLen > 1)) {
			if (verboseLevel > 1) {
			    log.fine("found total path: " + path + " length: " + currLen);
			    log.fine("adding path: " + path.subset(0, currLen));
			}
			result.add(path.subset(0, currLen));
		    }
		    else {
			if (verboseLevel > 1) {
			    log.fine("not adding path because it already exists: " + path);
			}
		    }
		}

	    }
	}
	return result;
    }

}
