package graphtools;

public interface GraphBase {

    /** returns number of edges */
    public int getEdgeCount();

    /** returns n'th edge */
    public IntPair getEdge(int n);

    /** returns n'th edge */
    public IntPair getNodeEdge(int edgeNum, int nodeId);

    /** returns number of nodes */
    public int getNodeCount();

    /** returns all connections of node n */
    public IntegerList getConnections(int n);

    /** returns index of edge, -1 if not found */
    public int indexOf(IntPair edge);

    /** returns index of edge, -1 if not found */
    public int nodeIndexOf(IntPair edge, int node);

    /** is there a connection from node n1 to node n2 ? */
    public boolean isConnected(int n1, int n2);

    /** sets or deletes a connection from node n1 to node n2 */
    public void setConnected(int n1, int n2, boolean status);

    public String toString();

}
