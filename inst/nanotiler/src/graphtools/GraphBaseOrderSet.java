package graphtools;

public interface GraphBaseOrderSet {

    /** generates next order graph. For example, the connections in a first order
     * graph represent simple links. The connections in a second order graph
     * represent two-link paths. The paths of an n'th order graph are not allowed to contain duplicates */
    public void generateNextOrderGraph();

    /** returns number of nodes */
    public int size();
    
    /** returns all paths from node n with certain length that contain no cycles */
    // public IntegerPathSet getPaths(int node, int length);

    /** returns all cyclical paths from node n with certain length (returns non-cyclical path of length n-1,
     * the connection from the last node (n-1) to the first one is not included in the path but excists).
     */
    public IntegerListList getCyclicalPaths(int node);
    
    /** how many orders are defined (maximum path lengths) ? */
    public int getHighestOrder();

}
