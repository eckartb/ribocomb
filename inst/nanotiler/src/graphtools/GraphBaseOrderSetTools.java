package graphtools;

public class GraphBaseOrderSetTools {

    /** returns all cyclical paths from node n with certain length (returns non-cyclical path of length n-1,
     * the connection from the last node (n-1) to the first one is not included in the path but excists).
     */
    public static IntegerListList getUniqueCyclicalPaths(GraphBaseOrderSet orderSet) {
	IntegerListList paths = new IntegerListList();
	for (int i = 0; i < orderSet.size(); ++i) {
	    IntegerListList newPaths = orderSet.getCyclicalPaths(i);
	    if ((newPaths != null)) {
		for (int j = 0; j < newPaths.size(); ++i) {
		    // TODO : equivalent paths are also paths 1 2 3 4 and 2 3 4 1 ! 
		    int id = PathTools.findEquivalentPath(paths, newPaths.get(j));
		    if (id >= paths.size()) {
			paths.add(newPaths.get(j));
		    }
		}
	    }
	}
	return paths;
    }
}
