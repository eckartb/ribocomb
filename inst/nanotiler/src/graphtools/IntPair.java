package graphtools;

public class IntPair {

    private int first;
    private int second;
    
    public IntPair(int first, int second) {
	this.first = first;
	this.second = second;
    }

    public boolean equals(Object obj) {
	if (obj instanceof IntPair) {
	    IntPair other = (IntPair)obj;
	    return (first == other.getFirst()) && (second == other.getSecond());
	}
	return false;
    }
    
    public int getFirst() { return first; }
    
    public int getSecond() { return second; }

    public IntPair reverse() { return new IntPair(second, first); }

    public void setFirst(int first) { this.first = first; }

    public void setSecond(int second) { this.second = second; }
	
    public String toString() { return ("" + first + " " + second); }
}
