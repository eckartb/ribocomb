package graphtools;

/** Interface for concept on a constraint for an array of integers */
public interface IntegerArrayConstraint {

    /** Returns true if constraint fullfilled */
    boolean validate(int[] data);

}
