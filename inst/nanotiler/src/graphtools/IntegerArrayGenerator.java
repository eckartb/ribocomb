package graphtools;

import generaltools.TestTools;
import java.io.PrintStream;
import java.util.ArrayList;
import java.math.BigInteger;
import java.math.BigDecimal;
import java.util.List;
import org.testng.*;
import org.testng.annotations.*;

/** Implements concept of a counter with a position specific base */
public class IntegerArrayGenerator implements IntegerPermutator {

    private int[] maxNumbers;
    private int[] numbers;
    private List<IntegerArrayConstraint> constraints = new ArrayList<IntegerArrayConstraint>();
    private boolean initial = false; // true;

    /** Default constructor is not very useful. */
    public IntegerArrayGenerator() { 
	//	assert !validate(); 
    }
    
    public IntegerArrayGenerator(IntegerArrayGenerator other) {
	copy(other);
    }

    public void copy(IntegerArrayGenerator other) {
	if ((this.maxNumbers == null) || (this.maxNumbers.length != other.maxNumbers.length)) {
	    this.maxNumbers = new int[other.maxNumbers.length];
	}
	if ((this.numbers == null) || (this.numbers.length != other.numbers.length)) {
	    this.numbers = new int[other.numbers.length];
	}
	for (int i = 0; i < size(); ++i) {
	    maxNumbers[i] = other.maxNumbers[i];
	    numbers[i] = other.numbers[i];
	}
	this.initial = other.initial;
	constraints = new ArrayList<IntegerArrayConstraint>();
	for (int i = 0; i < other.constraints.size(); ++i) {
	    constraints.add(other.constraints.get(i)); // FIXIT deep copy?
	}
    }

    public Object clone() {
	IntegerArrayGenerator result = new IntegerArrayGenerator(this);
	return result;
    }

    /** integer array corresponding to numbers. Smallest indices (left-most!) vary the fastest, represent right-most digits in most representations.
     * The maxiumum number reflect the number of possibilities at each positions, meaning the numbers 0..9 at one position corresponds to the max number 10 at that pos */
    public IntegerArrayGenerator(int[] maxNumbers) {
	assert maxNumbers != null && maxNumbers.length > 0;
	this.maxNumbers = maxNumbers;
	numbers = new int[maxNumbers.length];
	reset();
	// assert validate();
    }

    /** integer array corresponding to numbers. Smallest indices (left-most!) vary the fastest, represent right-most digits in most representations.
     * The maxiumum number reflect the number of possibilities at each positions, meaning the numbers 0..9 at one position corresponds to the max number 10 at that pos */
    public IntegerArrayGenerator(int length, int base) {
	assert length > 0;
	assert base > 0;
	this.maxNumbers = new int[length];
	for (int i = 0; i < length; ++i) {
	    maxNumbers[i] = base;
	}
	numbers = new int[maxNumbers.length];
	reset();
	assert numbers.length == length;
	assert validate();
    }

    public void addConstraint(IntegerArrayConstraint constraint) {
	this.constraints.add(constraint);
	reset();
	assert validate();
    }
    
    public int[] getMaxNumbers() { return maxNumbers; }

    /** Returns total number of instances (not counting constraints) */
    public BigInteger getTotal() {
	BigDecimal result = BigDecimal.ONE;
	for (int i = 0; i < numbers.length; ++i) {
	    result = result.multiply(new BigDecimal((double)(maxNumbers[i])));
	}
	return result.toBigInteger();
    }

    @Test(groups={"new"})
    public void testGetTotal() {
	IntegerArrayGenerator gen = new IntegerArrayGenerator(2,3); // should be 9 : length 2, base 3
	BigInteger nine = new BigDecimal("9").toBigInteger();
	assert gen.getTotal().equals(nine);
	gen.next();
	assert gen.getTotal().equals(nine); // should not change
    }

    private void printVec(PrintStream ps, int[] x) {
	for (int n : x) {
	    ps.print("" + n + " ");
	}
    }


    public boolean hasNext() {
	for (int i = 0; i < maxNumbers.length; ++i) {
	    if ((numbers[i] + 1) < maxNumbers[i]) {
		return true;
	    }
	}
	return false; // corresponds to 99999... to base 10 example
    }

    /** Increases values if possible, otherwise return false */
    public boolean inc() {
// 	if (initial) {
// 	    assert false;
// 	    initial = false;
// 	}	
// 	else {
	assert hasNext();
	OUTER:
	while(hasNext()) {
	    int pc = 0;
	    INNER:
	    while (pc < numbers.length) {
		++numbers[pc];
		if (numbers[pc] >= maxNumbers[pc]) {
		    numbers[pc] = 0;
		    ++pc;
		}
		else {
		    if (validate()) {
			return true; // successfully increased
		    }
		    else {
			break;
		    }
		}
	    }
	    // }
	}
	// reset();
	return false;
    }

    /** Returns reference to numbers */
    public int[] getNumbers() { return numbers; }

    public int[] get() { 
	if (!validate()) {
	    System.out.println("IntegerArrayGenerator: Numbers do not validate: ");
	    printVec(System.out, numbers);
	    return null;
	}
	int[] result = new int[numbers.length];
	for (int i = 0; i < result.length; ++i) {
	    result[i] = numbers[i];
	}
	return result; // make sure deep copy is returned, not reference
    }

    public int[] next() {
	boolean check = inc();
	if (!check) { 
	    return null; // could not increase!
	}
	// assert validate();
	return get();
    }	

    /** Resets to first position that validates */
    public void reset() {
	for (int i = 0; i < numbers.length; ++i) {
	    numbers[i] = 0;
	}
	while ((!validate()) && hasNext()) {
	    System.out.println("IntegerArrayGenerator.result: Sofar could not find validating reset position:");
	    printVec(System.out,numbers);	    
	    inc();
	}
	if (!validate()) {
	    System.out.println("Could not find validating reset position:");
	    printVec(System.out,numbers);
	    assert false;
	}
    }

    /** Returns number of elements */
    public int size() { return numbers.length; }

    /** Returns total number of elements */
    public long totalNumber() {
	long result = 1;
	for (int a : maxNumbers) {
	    result *= a;
	}
	return result;
    }

    public boolean validate() {
	if (numbers.length == 0) {
	    return true;
	}
	boolean check1 = maxNumbers != null && numbers!=null && maxNumbers.length > 0 && maxNumbers.length == numbers.length
	    && numbers[0] >= 0;
	if (!check1) {
	    return false;
	}
// 	String s = toString();
// 	if ((s == null) || (s.equals("null"))) {
// 	    return false;
// 	}
	if (constraints == null) {
	    return false;
	}
	for (IntegerArrayConstraint constraint : constraints) {
	    if (!constraint.validate(numbers)) {
		return false;
	    }
	}
	return true;
    }

    public String toString() {
	StringBuffer buf = new StringBuffer();
	for (int i = 0; i < numbers.length; ++i) {
	    buf.append(""+ numbers[i] + "(" + maxNumbers[i] + ") ");
	}
	return buf.toString();
    }

    @Test(groups={"new"})
    public void testIntegerArrayGenerator() {
	String methodName = "testIntegerArrayGenerator";
	System.out.println(TestTools.generateMethodHeader(methodName));
	IntegerArrayGenerator gen = new IntegerArrayGenerator(2,3);
	int count = 0;
	System.out.println("Results of integer Array generator");
	do {
	    int[] x = gen.get();
	    assert x != null;
	    printVec(System.out,x);
	    System.out.println();
	    ++count;
	}
	while (gen.hasNext() && (gen.next() != null));
	System.out.println("gen has no successor: " + gen);
	assert count == 9;
	System.out.println(TestTools.generateMethodFooter(methodName));	
    }

    @Test(groups={"new"})
    public void testIntegerArrayGenerator2() {
	String methodName = "testIntegerArrayGenerator2";
	System.out.println(TestTools.generateMethodHeader(methodName));
	int[] maxNumbers = new int[4];
	maxNumbers[0] = 3;
	maxNumbers[1] = 1;
	maxNumbers[2] = 1;
	maxNumbers[3] = 2;
	IntegerArrayGenerator gen = new IntegerArrayGenerator(maxNumbers);
	int count = 0;
	System.out.println("Results of integer Array generator");
	do {
	    int[] x = gen.get();
	    assert x != null;
	    printVec(System.out,x);
	    System.out.println();
	    ++count;
	}
	while (gen.hasNext() && (gen.next() != null));
	System.out.println("gen has no successor: " + gen);
	assert count == 6;
	System.out.println(TestTools.generateMethodFooter(methodName));	
    }

    @Test(groups={"new"})
    public void testIntegerArrayGenerator3() {
	String methodName = "testIntegerArrayGenerator3";
	System.out.println(TestTools.generateMethodHeader(methodName));
	IntegerArrayGenerator gen = new IntegerArrayGenerator(4,3); // length, base
	gen.addConstraint(new IntegerArrayMaxDifferentConstraint(2)); // not more than this many diff ones
	int count = 0;
	System.out.println("Results of integer Array generator");
	while (gen.hasNext()) {
	    ++count;
	    int[] x = gen.next();
	    System.out.print("" + count + " : ");
	    printVec(System.out,x);
	    System.out.println();

	}; //  while (gen.hasNext() && (gen.next() != null));
	assert count == 44; // less than 81 without constraint
	System.out.println("gen has no successor: " + gen);
	System.out.println(TestTools.generateMethodFooter(methodName));	
    }

    private int computeSum(int[] a) {
	int s = 0;
	for (int i = 0; i < a.length; ++i) {
	    s += a[i];
	}
	return s;
    }

    @Test(groups={"new"})
    public void testIntegerArrayGenerator4() {
	String methodName = "testIntegerArrayGenerator4";
	System.out.println(TestTools.generateMethodHeader(methodName));
	int sum = 3;
	IntegerArrayGenerator gen = new IntegerArrayGenerator(4,3); // length, base
	gen.addConstraint(new IntegerArraySumConstraint(sum)); // sum of elements must be equal to this number
	int count = 0;
	System.out.println("Results of integer Array generator, sum must always be " + sum);
	while (gen.hasNext()) {
	    ++count;
	    int[] x = gen.next();
	    if (x == null) {
		System.out.println("Could not generate further solutions! Quitting loop.");
		break;
	    }
	    System.out.print("" + count + " : ");
	    int computedSum = computeSum(x);
	    printVec(System.out,x);
	    System.out.print(" sum: " + computedSum);
	    assert computedSum == sum;
	    System.out.println();

	}; //  while (gen.hasNext() && (gen.next() != null));
	// assert count == 44; // less than 81 without constraint
	System.out.println("gen has no successor: " + gen);
	System.out.println(TestTools.generateMethodFooter(methodName));	
    }
    
}
