package graphtools;

/** validates array if all elements are stricly monotonic increasing */
public class IntegerArrayIncreasingConstraint implements IntegerArrayConstraint {

    /** Returns true if constraint fullfilled */
    public boolean validate(int[] data) {
	if (data.length < 2) {
	    return true;
	}
	for (int i = 1; i < data.length; ++i) {
	    if (data[i] <= data[i-1]) {
		return false;
	    }
	}
	return true;
    }

}
