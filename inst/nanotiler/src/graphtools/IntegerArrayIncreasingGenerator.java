package graphtools;

import generaltools.TestTools;
import java.io.PrintStream;
import java.util.ArrayList;
import java.math.BigInteger;
import java.math.BigDecimal;
import java.util.List;
import org.testng.*;
import org.testng.annotations.*;

/** integer array corresponding to numbers. Smallest indices (left-most!) vary the fastest, represent right-most digits in most representations.
 * The maxiumum number reflect the number of possibilities at each positions, meaning the numbers 0..9 at one position corresponds
 * to the max number 10 at that pos 
 */
public class IntegerArrayIncreasingGenerator implements IntegerPermutator {

    private int[] numbers;
    private int base;
    private List<IntegerArrayConstraint> constraints = new ArrayList<IntegerArrayConstraint>();
    private boolean initial = false; // true;
    private BigInteger total;

    /** Default constructor is not very useful. */
    public IntegerArrayIncreasingGenerator() { 
	this(1,1); // dummy just for test suite
    }

    /** integer array corresponding to numbers. Smallest indices (left-most!) vary the fastest, represent right-most digits in most representations.
     * The maxiumum number reflect the number of possibilities at each positions, meaning the numbers 0..9 at one position corresponds to the max number 10 at that pos */
    public IntegerArrayIncreasingGenerator(int length, int base) {
	assert length > 0;
	assert base > 0;
	assert base >= length;
	this.numbers = new int[length];
	this.base = base;
	reset();
	assert numbers.length == length;
	assert validate();
    }

    public void addConstraint(IntegerArrayConstraint constraint) {
	this.constraints.add(constraint);
	reset();
	assert validate();
    }

    public Object clone() { throw new RuntimeException("clone method not supported."); } // FIXIT

    private void printVec(PrintStream ps, int[] x) {
	for (int n : x) {
	    ps.print("" + n + " ");
	}
    }

    private void printVec(PrintStream ps) {
	printVec(ps, numbers);
    }

    private void printVec() {
	printVec(System.out, numbers);
    }

    /** Returns total number of instances (not counting constraints) */
    private BigInteger getTotal(int len, int max) {
	assert len > 0;
	assert max >= len; // otherwise impossible
	if (len == 1) {
	    assert max >= 1;
	    BigInteger result = new BigInteger("" + max); // this many different values for length one
	    // System.out.println("getTotal(): result of len " + len + " max: " + max + " : " + result);
	    return result;
	}
	BigInteger result = new BigInteger("0");
	int newLen = len-1;
	for (int newMax = max-1; newMax >= newLen; --newMax) {
	    result = result.add(getTotal(newLen, newMax));
	}
	// System.out.println("getTotal(): result of len " + len + " max: " + max + " : " + result);
	return result;
    }

    /** Returns total number of instances (not counting constraints) */
    public BigInteger getTotal() {
	if (total != null) {
	    return total; // caching of results
	}
	total = getTotal(numbers.length, base);
	return total;
// 	BigDecimal result = new BigDecimal("1");
// 	for (int i = 0; i < numbers.length; ++i) {
// 	    BigDecimal mult = new BigDecimal("" + (base - i));
// 	    System.out.println("Applying " + mult);
// 	    result = result.multiply(mult);
// 	}
// 	System.out.println("Result ot IntegerArrayIncreasingGenerator.getTotal: " + result);
// 	return result.toBigInteger();
    }

    @Test(groups={"new"})
    public void testGetTotal() {
	String methodName = "IntegerArrayIncreasingGenerator.testGetTotal";
	System.out.println(TestTools.generateMethodHeader(methodName));
	IntegerArrayIncreasingGenerator gen = new IntegerArrayIncreasingGenerator(2,3); // should be 9 : length 2, base 3
	BigInteger expected = new BigDecimal("3").toBigInteger();
	BigInteger firstCount = gen.getTotal();
	long count = 0;
	do {
	    System.out.println("" + ++count + " : " +  gen);
	    assert gen.getTotal().equals(firstCount); // should not change
	}
	while (gen.hasNext() && gen.inc());
	System.out.println("Expected this many iterations: " + expected + " computed total: " + firstCount + " " + gen.getTotal());
	assert gen.getTotal().equals(expected);
	System.out.println(TestTools.generateMethodHeader(methodName));
    }

    public void testGetTotal(int len, int b) {
	IntegerArrayIncreasingGenerator gen = new IntegerArrayIncreasingGenerator(len,b); // should be 9 : length 2, base 3
	BigInteger firstCount = gen.getTotal();
	long count = 0;
	do {
	    System.out.println("" + ++count + " : " +  gen);
	    assert gen.getTotal().equals(firstCount); // should not change
	}
	while (gen.hasNext() && gen.inc());
	System.out.println("Expected this many iterations: computed total: " + firstCount + " " + gen.getTotal());
    }

    @Test(groups={"new"})
    public void testGetTotal3_4() {
	String methodName = "IntegerArrayIncreasingGenerator.testGetTotal";
	System.out.println(TestTools.generateMethodHeader(methodName));
	testGetTotal(3,4);
	System.out.println(TestTools.generateMethodHeader(methodName));
    }

    public boolean hasNext() {
	for (int i = 1; i < numbers.length; ++i) {
	    if ((numbers[i-1] + 1) < numbers[i]) {
		return true;
	    }
	}
	return numbers[numbers.length-1] + 1 < base; // special case: highest order element
    }

    /** Increases values if possible between indices 0 and (inclusive) id */
    public boolean inc(int pc) {
	assert(pc >= 0);
	assert(pc < numbers.length);
// 	if (initial) {
// 	    assert false;
// 	    initial = false;
// 	}	
// 	else {
	// System.out.println("Starting IntegerArrayIncreasingGenerator.inc " + pc);
	// printVec();
	int n = numbers.length;
	assert(numbers[pc] < base);
	numbers[pc] = numbers[pc] + 1;
	if (numbers[pc] == base) {
	    if (pc > 0) {
		inc(pc-1);
		// numbers[pc] = numbers[pc-1]+1;
		if (numbers[pc] >= base) {
		    reset();
		    assert(false);
		    return false;
		}
	    } else {
		reset();
		return false;
	    }
	}
	for (int k = pc + 1; k < n; ++k) {
	    numbers[k] = numbers[k-1]+1;
	    if (numbers[k] >= base) {
		inc(pc-1);
	    }
	}
	// numbers[pc
	// System.out.println("Finished IntegerArrayIncreasingGenerator.inc");
	return true;
    }


    /** Increases values if possible, otherwise return false */
    public boolean inc() {
	assert hasNext();
	boolean result = inc(numbers.length-1);
	assert(validate());
	return result;
    }

    public int[] getNumbers() { return numbers; }

    public int[] get() { 
	if (!validate()) {
	    System.out.println("IntegerArrayIncreasingGenerator: Numbers do not validate: ");
	    printVec(System.out, numbers);
	    return null;
	}
	return numbers;
    }

    public int[] next() {
	boolean check = inc();
	if (!check) { 
	    return null; // could not increase!
	}
	// assert validate();
	return numbers;
    }	

    /** Resets to first position that validates */
    public void reset() {
	for (int i = 0; i < numbers.length; ++i) {
	    assert i < base;
	    numbers[i] = i;
	}
	while ((!validate()) && hasNext()) {
	    System.out.println("IntegerArrayGenerator.result: Sofar could not find validating reset position:");
	    printVec(System.out,numbers);	    
	    inc();
	}
	if (!validate()) {
	    System.out.println("Could not find validating reset position:");
	    printVec(System.out,numbers);
	    assert false;
	}
	// System.out.println("Result of reset IntegerArrayIncreasingGenerator:");
	// printVec(System.out);
    }

    /** Returns number of elements */
    public int size() { return numbers.length; }

    /** Returns total number of elements */
    public long totalNumber() {
	assert base >= numbers.length;
	long result = 1;
	for (int i = 0; i < numbers.length; ++i) {
	    assert base > i;
	    result *= base-i;
	}
	return result;
    }

    public boolean validate() {
	if ((numbers.length == 0) && (numbers[0] < base) && (numbers[0] >= 0)) {
	    return true;
	}
	for (int i = 1; i < numbers.length; ++i) {
	    if (numbers[i-1] >= numbers[i]) {
		return false;  // not increasing
	    }
	    if (numbers[i] < 0) {
		return false;
	    }
	}
	String s = toString();
	if ((s == null) || (s.equals("null"))) {
	    return false;
	}
	for (IntegerArrayConstraint constraint : constraints) {
	    if (!constraint.validate(numbers)) {
		return false;
	    }
	}
	return true;
    }

    public String toString() {
	StringBuffer buf = new StringBuffer();
	for (int i = 0; i < numbers.length; ++i) {
	    buf.append(""+ numbers[i] + " ");
	}
	return buf.toString();
    }

    @Test(groups={"newest"})
    public void testIntegerArrayIncreasingGenerator() {
	String methodName = "testIntegerArrayIncreasingGenerator";
	System.out.println(TestTools.generateMethodHeader(methodName));
	IntegerArrayIncreasingGenerator gen = new IntegerArrayIncreasingGenerator(2,3);
	int count = 0;
	System.out.println("Results of integer Array generator");
	do {
	    int[] x = gen.get();
	    assert x != null;
	    printVec(System.out,x);
	    System.out.println();
	    ++count;
	}
	while (gen.hasNext() && (gen.next() != null));
	System.out.println("gen has no successor: " + gen);
	assert count == 3;
	System.out.println(TestTools.generateMethodFooter(methodName));	
    }

    @Test(groups={"newest"})
    public void testIntegerArrayIncreasingGenerator2() {
	String methodName = "testIntegerArrayIncreasingGenerator2";
	System.out.println(TestTools.generateMethodHeader(methodName));
	IntegerArrayIncreasingGenerator gen = new IntegerArrayIncreasingGenerator(3,5);
	int count = 0;
	System.out.println("Results of integer Array generator");
	do {
	    int[] x = gen.get();
	    assert x != null;
	    printVec(System.out,x);
	    System.out.println();
	    ++count;
	}
	while (gen.hasNext() && (gen.next() != null));
	System.out.println("gen has no successor: " + gen);
	System.out.println(TestTools.generateMethodFooter(methodName));	
    }

    private int computeSum(int[] a) {
	int s = 0;
	for (int i = 0; i < a.length; ++i) {
	    s += a[i];
	}
	return s;
    }

    @Test(groups={"new"})
    public void testIntegerArrayIncreasingGenerator4() {
	String methodName = "testIntegerArrayIncreasingGenerator4";
	System.out.println(TestTools.generateMethodHeader(methodName));
	int sum = 6;
	IntegerArrayIncreasingGenerator gen = new IntegerArrayIncreasingGenerator(3,4); // length, base
	gen.addConstraint(new IntegerArraySumConstraint(sum)); // sum of elements must be equal to this number
	int count = 0;
	System.out.println("Results of integer Array generator, sum must always be " + sum);
	while (gen.hasNext()) {
	    ++count;
	    int[] x = gen.next();
	    if (x == null) {
		System.out.println("Could not generate further solutions! Quitting loop.");
		break;
	    }
	    System.out.print("" + count + " : ");
	    int computedSum = computeSum(x);
	    printVec(System.out,x);
	    System.out.print(" sum: " + computedSum);
	    assert computedSum == sum;
	    System.out.println();

	}; //  while (gen.hasNext() && (gen.next() != null));
	// assert count == 44; // less than 81 without constraint
	System.out.println("gen has no successor: " + gen);
	System.out.println(TestTools.generateMethodFooter(methodName));	
    }

    @Test(groups={"new"})
    public void testIntegerArrayIncreasingGenerator5() {
	String methodName = "testIntegerArrayIncreasingGenerator5";
	System.out.println(TestTools.generateMethodHeader(methodName));
	IntegerArrayIncreasingGenerator gen = new IntegerArrayIncreasingGenerator(4,6); // length, base
	int n = IntegerPermutatorTools.testIntegerPermutatorRunToEnd(gen);
	System.out.println(TestTools.generateMethodFooter(methodName));
    }
    
}
