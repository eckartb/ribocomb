package graphtools;

import java.util.HashSet;
import java.util.Set;

import org.testng.annotations.*;

/** Returns true if not mare than specified number of different integers in list */
public class IntegerArrayMaxDifferentConstraint implements IntegerArrayConstraint {

    private int max;

    public IntegerArrayMaxDifferentConstraint() {
	this(1); // default: allow only one type of different values
    }

    public IntegerArrayMaxDifferentConstraint(int max) {
	this.max = max;
    }

    /** Returns true if all elemets are larger or equal than minimum */
    public boolean validate(int[] data) {
	Set<Integer> set = new HashSet<Integer>();
	for (int a : data) {
	    set.add(a);
	}
	return set.size() <= max;
    }

    public String toString() {
	return "" + max;
    }

    @Test(groups={"new"})
    public void testData1() {
	int[] data1 = new int[4];
	data1[0] = 1;
	data1[1] = 1;
	data1[2] = 3;
	data1[3] = 3;
	IntegerArrayConstraint constraint1 = new IntegerArrayMaxDifferentConstraint(1);
	IntegerArrayConstraint constraint2 = new IntegerArrayMaxDifferentConstraint(2);
	assert constraint2.validate(data1);
	assert !constraint1.validate(data1);
    }

}
