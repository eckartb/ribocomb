package graphtools;

/** Returns true if all elemets are larger or equal than minimum */
public class IntegerArrayMinConstraint implements IntegerArrayConstraint {

    private int min;

    public IntegerArrayMinConstraint(int min) {
	this.min = min;
    }

    /** Returns true if all elemets are larger or equal than minimum */
    public boolean validate(int[] data) {
	for (int a : data) {
	    if (a < min) {
		return false;
	    }
	}
	return true;
    }

}
