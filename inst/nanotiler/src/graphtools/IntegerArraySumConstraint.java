package graphtools;

/** Returns true if all elemets are larger or equal than minimum */
public class IntegerArraySumConstraint implements IntegerArrayConstraint {

    private int sum;

    public IntegerArraySumConstraint(int sum) {
	this.sum = sum;
    }

    /** Returns true if all elemets are larger or equal than minimum */
    public boolean validate(int[] data) {
	int s = 0;
	for (int a : data) {
	    s += a;
	}
	return s == sum;
    }

}
