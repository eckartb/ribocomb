package graphtools;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class IntegerList implements Comparable {
    
    private List<Integer> nodes = new ArrayList<Integer>();
    private Set<Integer> nodeSet = new HashSet<Integer>();

    public IntegerList() { }

    public IntegerList(IntegerList orig) {
	for (int i = 0; i < orig.size(); ++i) {
	    add(orig.get(i));
	}
    }

    public IntegerList(int[] orig) {
	for (int i = 0; i < orig.length; ++i) {
	    add(orig[i]);
	}
    }

    public void add(int n) {
	nodes.add(new Integer(n));
	if (!contains(n)) {
	    nodeSet.add(new Integer(n));
	}
    }

    public void clear() {
	nodes = new ArrayList<Integer>();
	nodeSet = new HashSet<Integer>();
    }

    /** returns true if this list contains all elements (and possibly more) of the other list */
    public boolean containsAll(IntegerList other) {
	for (int i = 0; i < other.size(); ++i) {
	    if (!contains(other.get(i))) {
		return false;
	    }
	}
	return true;
    }

    /** returns n'th integer value */
    public int get(int n) {
	return ((Integer)(nodes.get(n))).intValue();
    }

    public int compareTo(Object obj) {
	if (obj instanceof IntegerList) {
	    IntegerList other = (IntegerList)obj;
	    if (size() < other.size()) {
		return -1;
	    }
	    else if (size() > other.size()) {
		return +1;
	    }
	    for (int i = 0; i < size(); ++i) {
		if (get(i) < other.get(i)) {
		    return -1;
		}
		else if (get(i) > other.get(i)) {
		    return +1;
		}
	    }
	    return 0; // objects are the same!
	}
	return -1; // should never happen, called with object that is not IntegerList
    }

    /** returns true if a node has the value n */
    public boolean contains(int n) {
	return nodeSet.contains(new Integer(n));
    }

    /** returns index of number n. Returns -1 if not found */
    public int indexOf(int n) {
	for (int i = 0; i < size(); ++i) {
	    if (get(i) == n) {
		return i;
	    }
	}
	return -1;
    }

    /** returns index of number n. Returns -1 if not found */
    public int indexOfSmallest() {
	int smallest = get(0);
	int smallestId = 0;
	for (int i = 1; i < size(); ++i) {
	    if (get(i) < smallest) {
		smallest = get(i);
		smallestId = i;
	    }
	}
	return smallestId;
    }

    /** rotate until smallest member is at get(0) */
    public IntegerList rotateToSmallest() {
	assert isUnique(); // does not work properly is not unique
	int smallest = get(indexOfSmallest());
	for (int i = 0; i < size(); ++i) {
	    IntegerList path = rotate(i);
	    if (path.get(0) == smallest) {
		return path;
	    }
	}
	assert false; // should never be here
	return null;
    }


    /** returns true if all elements are equal */
    public boolean equals(Object obj) {
	if (obj instanceof IntegerList) {
	    IntegerList other = (IntegerList)obj;
	    if (other.size() != size()) {
		return false;
	    }
	    for (int i = 0; i < size(); ++i) {
		if (get(i) != other.get(i)) {
		    return false;
		}
	    }
	    return true;
	}
	return false;
    }

    public IntegerList reverse() {
	IntegerList result = new IntegerList();
	for (int i = size()-1; i >= 0; --i) {
	    result.add(get(i));
	}
	return result;
    }

    /** rot : cyclical permutations of rot step towards LEFT */
    public IntegerList rotate(int rot) {
	IntegerList result = new IntegerList();
	if (size() == 0) {
	    return result;
	}
	while (rot < 0) {
	    rot += size(); 
	}
	rot = rot % size();
	for (int i = 0; i < size(); ++i) {
	    result.add(get((i+rot)%size()));
	}
	return result;
    }

    /** returns number of defined nodes */
    public int size() {
	return nodes.size(); 
    }

    /** returns subset of length starting at index start */
    public IntegerList subset(int start, int length) {
	IntegerList result = new IntegerList();
	for (int i = 0; i < length; ++i) {
	    result.add(get(start + i));
	}
	return result;
    }

    /** returns true if path has no node twice */
    public boolean isUnique() {
	return (nodes.size() == nodeSet.size());
    }

    public int[] toArray() {
	int[] result = new int[size()];
	for (int i = 0; i < size(); ++i) {
	    result[i] = get(i);
	}
	return result;
    }

    public String toString() {
	String result = "" + size() + "  ";
	for (int i = 0; i < size(); ++i) {
	    result = result + get(i) + " ";
	}
	return result;
    }

}
