package graphtools;

import java.util.ArrayList;
import java.util.List;

public class IntegerListList {
    
    private List<IntegerList> lists = new ArrayList<IntegerList>();

    public void add(IntegerList list) {
	lists.add(list);
    }

    /** returns n'th integer value */
    public IntegerList get(int n) {
	return ((IntegerList)(lists.get(n)));
    }

    /** returns number of defined nodes */
    public int size() {
	return lists.size(); 
    }

    public String toString() {
	String result = "" + size() + "  ";
	for (int i = 0; i < size(); ++i) {
	    result = result + get(i).toString() + " ";
	}
	return result;
    }

}
