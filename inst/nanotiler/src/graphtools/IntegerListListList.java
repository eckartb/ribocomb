package graphtools;

import java.util.ArrayList;
import java.util.List;

public class IntegerListListList {
    
    private List<IntegerListList> lists = new ArrayList<IntegerListList>();

    public void add(IntegerListList list) {
	lists.add(list);
    }

    /** returns n'th integer value */
    public IntegerListList get(int n) {
	return ((IntegerListList)(lists.get(n)));
    }

    /** returns number of defined nodes */
    public int size() {
	return lists.size(); 
    }

}
