package graphtools;

import java.math.BigInteger;
import java.io.OutputStream;

/** Interface for all classes iterating through integer array. 
 * Implementing subclasses: PermutationGenerator and IntegerArrayGenerator
 */
public interface IntegerPermutator {

    void addConstraint(IntegerArrayConstraint constraint);

    Object clone();

    /** returns true if permutation can be increased */
    boolean hasNext();

    /** Increase counters, return true if not starting over but really increasing values */
    boolean inc();

    /** returns current set of values */
    int[] get();
    
    /** Returns next array of integers */
    int[] next();

    /** Set all integers to  zero */
    void reset();

    /** returns number of elements of returned arrays */
    int size();

    boolean validate();

    /** Returns total number of instances (not counting constraints) */
    BigInteger getTotal();

}
