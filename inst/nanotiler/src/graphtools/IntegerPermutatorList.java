package graphtools;

// import java.util.logging.*;
import generaltools.TestTools;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;
import org.testng.*;
import org.testng.annotations.*;

/** This class allows for chaining of permutators to one big permutator */
public class IntegerPermutatorList extends ArrayList<IntegerPermutator> implements List<IntegerPermutator>, IntegerPermutator {

    // public static Logger log = Logger.getLogger("dummy");

    private boolean initial = false; // true;
    private List<IntegerArrayConstraint> constraints = new ArrayList<IntegerArrayConstraint>();

    public IntegerPermutatorList() {
	super();
    }

    public IntegerPermutatorList(IntegerPermutatorList other) {
	copy(other);
    }

    /** Adds a constraint validator to list of constraints */
    public void addConstraint(IntegerArrayConstraint constraint) {
	constraints.add(constraint);
    }

    /** Adds permutator to list and validates it */
    public boolean add(IntegerPermutator permutator) {
	assert validate(); // pre-condition
	assert permutator.validate();
	super.add(permutator);
	assert validate(); // post condition
	return true; // see java Collection 
    }

    public Object clone() {
	return new IntegerPermutatorList(this);
    }

    public void copy(IntegerPermutatorList other) {
	clear();
	this.initial = other.initial;
	this.constraints = other.constraints; // FIXIT : deep copy needed
	for (IntegerPermutator perm : other) {
	    add((IntegerPermutator)(perm.clone()));
	}
    }

    /** Returns total number of instances (not counting constraints) */
    public BigInteger getTotal() {
	BigDecimal result = BigDecimal.ONE;
	for (IntegerPermutator perm : this) {
	    result = result.multiply(new BigDecimal(perm.getTotal()));
	}
	return result.toBigInteger();
    }

    /** Counts current iterations and compares with getTotal value */
    public void testCurrentTotal() {
	String methodName = "IntegerPermutatorList.testCurrentTotal";
	System.out.println(TestTools.generateMethodHeader(methodName));
	assert hasNext();
	BigInteger firstCount = getTotal();
	long count = 0;
	do {
	    System.out.println("" + ++count + " : " +  this);
	    assert getTotal().equals(firstCount); // should not change
	}
	while (hasNext() && inc());
	System.out.println("Expected this many iterations: " + firstCount);
	assert getTotal().equals(new BigDecimal("" + count).toBigInteger());
	assert !hasNext();
	System.out.println(TestTools.generateMethodHeader(methodName));
    }

    /** Returns array being a concatenation of all element arrays */
    public int[] get() { assert false; return null; }

    public boolean hasNext() {
	for (IntegerPermutator p : this) {
	    if (p.hasNext()) {
		return true;
	    }
	}
	return false;
    }

    /** check if more counts available with respect to calls skip(n) (ignoring status of permutators 0..n-1). Careful: a sub-permutator can itself consist of several digits. */
    public boolean hasNext(int n) {
	for (int i = n; i < size(); ++i) {
	    IntegerPermutator p = get(i);
	    if (p.hasNext()) {
		return true;
	    }
	}
	return false;
    }

    /** Increases counters by one */
    public boolean inc() {
//   	if (initial) {
// 	    assert false; // outdated, should never be here
//  	    for (IntegerPermutator p : this) {
//  		p.inc();
//  	    }
//  	    initial = false;
//  	}
//  	else {
	while (hasNext()) {
	    for (IntegerPermutator p : this) {
		if (p.hasNext()) {
		    p.inc();
		    break;
		} 
		else {
		    p.reset();
		}
	    }
	    // check constraints
	    if (validate()) {
		return true;
	    }
	}
	// }
	// reset();
	return false;
    }

    /** Increases counter n by one, resetting all subcounters from 0..n-1. Careful: a subcounter can itself consist of several digits. */
    public boolean skip(int n) {
	// log.info("Called skip("+ n + ") for size " + size() + " and: " + toString());
	if (initial) {
	    assert false;
	    inc();
	}
	assert !initial;
	for (int i = 0; i < n; ++i) {
	    IntegerPermutator p = get(i);
	    p.reset();
	    // log.info("resetting permutator: " + i);
	}
	for (int i = n; i < size(); ++i) {
	    IntegerPermutator p = get(i);
	    if (p.hasNext()) {
		p.inc();
		// log.info("increasing permutator: " + i);
		return true;
	    } 
	    else {
		p.reset();
		// log.info("resetted permutator: " + i);
	    }
	}
	return true;
    }

    public int[] next() {
	inc();
	return get();
    }

    /** Resets all counters */
    public void reset() {
	for (IntegerPermutator p : this) {
	    p.reset();
	}
    }

    public String toString() {
	assert validate();
	StringBuffer buf = new StringBuffer();
	for (IntegerPermutator p : this) {
	    assert p.validate();
	    String newString = p.toString();
	    assert newString != null;
	    assert !newString.equals("null");
	    buf.append(newString + " | ");
	}
	return buf.toString();
    }

    public boolean validate() {
	for (IntegerPermutator p : this) {
	    if (!p.validate()) {
		return false;
	    }
	}
	for (IntegerArrayConstraint constraint : constraints) {
	    if (!constraint.validate(get())) { // currently not working!
		return false;
	    }
	}
	return true;
    }

    @Test(groups={"new"})
    public void testIntegerPermutatorListBasics() {
	String methodName = "testIntegerPermutatorListBasics";
	System.out.println(TestTools.generateMethodHeader(methodName));
	IntegerPermutatorList pList = new IntegerPermutatorList();
	pList.add(new IntegerArrayGenerator(3,2)); // 2*2*2 = 8
	pList.add(new IntegerArrayGenerator(2,3)); //  3*3 = 9 possibilities
	int count = 0;
	do {
	    // pList.next();
	    ++count;
	    System.out.println("" + count + " " + pList.toString());
	}
	while (pList.hasNext() && pList.inc());
	assert count == 72;
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new"})
    public void testIntegerPermutatorListBasics2() {
	String methodName = "testIntegerPermutatorListBasics2";
	System.out.println(TestTools.generateMethodHeader(methodName));
	IntegerPermutatorList pList = new IntegerPermutatorList();
	pList.add(new IntegerArrayGenerator(3,2)); // length 3, base 2 == 2 * 2 * 2 == 8
	pList.add(new PermutationGenerator(3)); // permutations of 3 elements == 3! (6 possibilities)
	int count = 0;
	//	System.out.println("" + count + " " +pList.toString());
	do {
	    ++count;
	    System.out.println("" + count + " " + pList.toString());
	}
	while (pList.hasNext() && pList.inc());
	assert count == 8 * 6;
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new"})
    public void testIntegerPermutatorListBasics3() {
	String methodName = "testIntegerPermutatorListBasics3";
	System.out.println(TestTools.generateMethodHeader(methodName));
	IntegerPermutatorList pList = new IntegerPermutatorList();
	pList.add(new IntegerArrayGenerator(2,2)); // length 2, base 2 == 2*2 == 4
	pList.add(new PermutationGenerator(2)); // permutations of 2 elements (2 possibilities)
	pList.add(new PermutationGenerator(2)); // permutations of 2 elements (2 possibilities)
	pList.add(new IntegerArrayGenerator(2,2)); // length 2, base 2
	int count = 0;
	//	System.out.println("" + count + " " +pList.toString());
	do {
	    ++count;
	    System.out.println("Counter status: " + count + " : " + pList.toString());
	}
	while (pList.hasNext() && pList.inc());
	assert count == 4 * 2 * 2 * 4;
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new"})
	public void testIntegerPermutatorListSkip() {
	String methodName = "testIntegerPermutatorListSkip";
	System.out.println(TestTools.generateMethodHeader(methodName));
	IntegerPermutatorList pList = new IntegerPermutatorList();
	pList.add(new IntegerArrayGenerator(3,2)); // 2*2*2 = 8
	pList.add(new IntegerArrayGenerator(2,3)); // 3*3 = 9 possibilities
	assert pList.size() == 2;
	int count = 0;
	do {
	    // pList.inc();
	    ++count;
	    System.out.println("" + count + " " + pList.toString());
	}
	while (pList.hasNext(1) && pList.skip(1));
	assert count == 9;
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new"})
	public void testIntegerPermutatorListSkip2() {
	String methodName = "testIntegerPermutatorListSkip2";
	System.out.println(TestTools.generateMethodHeader(methodName));
	IntegerPermutatorList pList = new IntegerPermutatorList();
	pList.add(new IntegerArrayGenerator(3,2)); // 2*2*2 = 8
	pList.add(new IntegerArrayGenerator(2,3)); // 3*3 = 9 possibilities
	assert pList.size() == 2;
	for (int i = 0; i < 3; ++i) {
	    System.out.println(pList.toString());
	    pList.inc();
	}
	int count = 0;
	do {
	    ++count;
	    System.out.println("" + count + " " + pList.toString());
	}
	while (pList.hasNext(1) && pList.skip(1));
	assert count == 9;
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

}
