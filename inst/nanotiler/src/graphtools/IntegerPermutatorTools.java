package graphtools;

import java.io.OutputStream;
import java.io.PrintStream;

public class IntegerPermutatorTools {

    /** Dry-run until last element
     * @return number of generated values */
    public static int runToEndNoReset(OutputStream os, IntegerPermutator perm) {
	PrintStream ps = new PrintStream(os);
	int count = 0;
	do {
	    ps.println("" + ++count + " " + perm.toString());
	}
	while (perm.hasNext() && perm.inc());
	return count;
    }

    /** Dry-run until last element
     * @return number of generated values */
    public static int runToEndAndReset(OutputStream os, IntegerPermutator perm) {
	int count = runToEndNoReset(os, perm);
	perm.reset();
	return count;
    }

    /** Tests if running permutator twice results in same number of calls. Only passes test if initial state is the same as the reset state. 
     * @return number of function calls */
    static int testIntegerPermutatorRunToEnd(IntegerPermutator gen) {
	int count1 = IntegerPermutatorTools.runToEndAndReset(System.out, gen);
	int count2 = IntegerPermutatorTools.runToEndAndReset(System.out, gen);
	assert count1 > 0;
	assert count1 == count2;
	return count1;
    }

}
