package graphtools;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.*;
import generaltools.TestTools;
import java.math.BigInteger;
import org.testng.annotations.*;

/** Interface for all classes iterating through integer array. 
 * Implementing subclasses: PermutationGenerator and IntegerArrayGenerator
 */
public class MappedIntegerPermutator implements IntegerPermutator {

    private int[] positions;
    private int max;
    private List<IntegerArrayConstraint> constraints = new ArrayList<IntegerArrayConstraint>();
    
    public MappedIntegerPermutator() {
	this(1,1);
    }

    /** @param fromLen Length to be mapped 
     *  @param toLen Maximum value. Example: 10 allows values between 0 and 9
     */
    public MappedIntegerPermutator(int fromLen, int toLen) {
	// log.fine("Called MappedIntegerPermutator: " + fromLen + " : " + toLen);
	assert fromLen > 0;
	assert toLen >= fromLen;
	positions = new int[fromLen];
	this.max = toLen;
	reset();
	assert validate();
    }

    public void addConstraint(IntegerArrayConstraint constraint) {
	assert false; // not yet implemented
	constraints.add(constraint);
    }

    public Object clone() { throw new RuntimeException("clone method not supported."); } // FIXIT

    /** returns true if permutation can be increased. Example: max mapping from len 3 to len 7 is: 6 5 4 */
    public boolean hasNext() {
	for (int i = 0; i < positions.length; ++i) {
	    if (positions[i] < (max - i - 1)) {
		return true;
	    }
	}
	return false;
    }

    /** Increase counters, return true if not starting over but really increasing values */
    public boolean incAt(int pos) {
	if (pos >= positions.length) {
	    return false;
	}
	int pc = pos;
	++positions[pc];
	// || ((pc > 0) && (positions[pc] >= positions[pc-1]))) {
	if (positions[pc] >= (max-pc)) {
	    if (pos+1 < positions.length) {
		boolean tmpResult = incAt(pos+1);
		if (!tmpResult) {
		    return false;
		}
		for (int i = pc; i >= 0; --i) {
		    positions[i] = positions[i+1]+1; // reset; instead of 990->001 we have 980 -> 321
		}
	    }
	}
	return true;
    }

    /** Increase counters, return true if not starting over but really increasing values */
    public boolean inc() {
	assert validate();
	boolean expNext = hasNext();
	if (!expNext) {
	    return false;
	}
	// System.out.println("Vector before increase: ");
	// printVec(System.out, positions);
	boolean result = incAt(0);
	// System.out.println("After increase: " + result + " : ");
	// printVec(System.out, positions);
	return result;
    }


    public void setPositions(int[] positions) {
	assert this.positions.length == positions.length;
	this.positions = positions;
	assert validate();
    }

    private void printVec(PrintStream ps, int[] a) {
	for (int i = 0; i < positions.length; ++i) {
	    ps.print(" " + positions[i]); 
	}
	ps.println();
    }

    /** returns current set of values */
    public int[] get() { return positions; }
    
    public BigInteger getTotal() { assert false; return null; }

    /** Returns next array of integers */
    public int[] next() { inc(); return get(); }

    /** Set all integers to  zero */
    public void reset() {
	for (int i = 0; i < positions.length; ++i) {
	    positions[positions.length - i - 1] = i;
	} 
	// for (int i = 0; i < positions.length; ++i) {
	// System.out.print(" " + positions[i]); 
	// }
	// System.out.println();
	assert validate();
    }

    /** returns number of elements of returned arrays */
    public int size() { return positions.length; }

    public boolean validate() {
	for (int i = 0; i < positions.length; ++i) {
	    if ((positions[i] < 0) || (positions[i] >= max)) {
		return false;
	    }
	    if ((i > 0) && (positions[i] >= positions[i-1])) {
		return false;
	    }
	}
	return true;
    }

    @Test(groups={"new"})
    public void testMappedIntegerPermutator() {
	String methodName = "testMappedIntegerPermutator";
	System.out.println(TestTools.generateMethodHeader(methodName));
	MappedIntegerPermutator gen = new MappedIntegerPermutator(4,6); // length, base
	// 	gen.addConstraint(new IntegerArrayMaxDifferentConstraint(2)); // not more than this many diff ones
	int count = 0;
	System.out.println("Results of MappedIntegerPermutator");

	do {
	    ++count;
	    int[] x = gen.get();
	    assert gen.validate();
	    System.out.print("" + count + " : ");
	    for (int i = 0; i < x.length; ++i) {
		System.out.print(" " + x[i]); 
	    }
	    System.out.println();

	}
	while (gen.hasNext() && (gen.next() != null));
    }


}
