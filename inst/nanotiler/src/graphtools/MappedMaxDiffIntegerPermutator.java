package graphtools;

import java.io.*;
import java.util.ArrayList;
import java.math.BigInteger;
import java.util.List;
import java.util.logging.*;
import generaltools.TestTools;

import org.testng.annotations.*;

/**  Iterating through all combinations in which there is at most this many different nummbers.
*/
public class MappedMaxDiffIntegerPermutator implements IntegerPermutator {

    private static Logger log = Logger.getLogger("NanoTiler_debug");
    
    private IntegerPermutator quantityGenerator;
    private IntegerPermutator posGenerator;
    // private IntegerArrayGenerator valueGenerator = new IntegerArrayGenerator();
    private List<IntegerArrayConstraint> constraints = new ArrayList<IntegerArrayConstraint>();
    private int toLen;
    private int[] result;
    private int base;
    private int minValue;
    private int maxDiff;
    public MappedMaxDiffIntegerPermutator() {
	this(2,0,2,2);
    }

    /**  @param toLen new length of result vector
     *  @param minValue minimum value of elements of result vector
     *  @param maxValue one higher than maximum allowed value of elements of result vector
     *  @param maxDiff maximum number of different elements of result vector
     */
    public MappedMaxDiffIntegerPermutator(int toLen, int minValue, int maxValue, int maxDiff) {
 	assert maxDiff > 0;
 	assert maxValue >= minValue;
	this.base = maxValue - minValue;
	assert base >= maxDiff;
	this.maxDiff = maxDiff;
	this.minValue = minValue;
	// assert validate();
	this.toLen = toLen;
	this.quantityGenerator = new IntegerArrayIncreasingGenerator(maxDiff,base);
	this.posGenerator = new IntegerArrayGenerator(toLen,maxDiff);
	assert quantityGenerator.size() == maxDiff;
	assert posGenerator.size() == toLen;
	// quantityGenerator.addConstraint(new IntegerArrayIncreasingConstraint()); // no duplicate position value counting
	reset();
	assert validate();
    }

    public void addConstraint(IntegerArrayConstraint constraint) {
	// assert false; // not yet implemented
	constraints.add(constraint);
	reset();
	assert validate();
    }

    public Object clone() { throw new RuntimeException("clone method not supported."); } // FIXIT

    /** Returns total number of instances (not counting constraints) */
    public BigInteger getTotal() {
	return quantityGenerator.getTotal().multiply(posGenerator.getTotal());
    }

    /** returns true if permutation can be increased. Example: max mapping from len 3 to len 7 is: 6 5 4 */
    public boolean hasNext() {
	return (quantityGenerator.hasNext() || posGenerator.hasNext());
    }

    public boolean inc() {
	assert hasNext();
	if (!hasNext()) {
	    return false;
	}
	boolean check = true;
	if (posGenerator.hasNext()) {
	    posGenerator.inc();
	}
	else {
	    posGenerator.reset();
	    assert posGenerator.validate();
	    check = quantityGenerator.inc();
	}
	update();
	if (!validate()) {
	    check = false;
	}
	return check;
    }

    /** Update cached result value */
    private boolean update() {
	if ((result == null) || (result.length != posGenerator.size())) {
	    result = new int[posGenerator.size()];
	}
	int[] posValues = posGenerator.get();
	if (posValues == null) {
	    return false;
	}
	int[] quantValues = quantityGenerator.get();
	if (quantValues == null) {
	    return false;
	}
	for (int i = 0; i < posValues.length; ++i) {
	    assert posValues[i] < quantValues.length;
	    result[i] = quantValues[posValues[i]] + minValue;
	}
	return true;
    }

    private void printVec(PrintStream ps, int[] positions) {
	
	for (int i = 0; i < positions.length; ++i) {
	    ps.print(" " + positions[i]); 
	}
	ps.println();
    }

    /** returns current set of values */
    public int[] get() {
	if (result == null) {
	    update();
	}
	return result;
    }
    
    /** Returns next array of integers */
    public int[] next() { inc(); return get(); }

    /** Resets to first state that validates */
    public void reset() {
	quantityGenerator.reset();
	assert quantityGenerator.validate();
	posGenerator.reset();
	assert posGenerator.validate();
	update();
	assert validate();
    }

    /** returns number of elements of returned arrays */
    public int size() { return posGenerator.size(); }

    public boolean validate() {
	if (result == null) {
	    return false;
	}
	if (!quantityGenerator.validate()) {
	    return false;
	}
	if (!posGenerator.validate()) {
	    return false;
	}
	return true;
    }

    @Test(groups={"new"})
    public void testMappedMaxDiffIntegerPermutator() {
	String methodName = "testMappedMaxDiffIntegerPermutator";
	System.out.println(TestTools.generateMethodHeader(methodName));
	MappedMaxDiffIntegerPermutator gen = new MappedMaxDiffIntegerPermutator(6,3,6,2); // toLen, minvalue, maxvalue, how many different
	IntegerArrayConstraint constraint = new IntegerArrayMaxDifferentConstraint(2); // not more than this many diff ones
	int count = 0;
	System.out.println("Results of MappedMaxDiffIntegerPermutator");

	do {
	    ++count;
	    int[] x = gen.get();
// 	    printVec
// 	    assert gen.validate();
	    System.out.print("" + count + " : ");
	    for (int i = 0; i < x.length; ++i) {
		System.out.print(" " + x[i]); 
	    }
	    System.out.println();
	    assert constraint.validate(x); // no more than two different connections
	}
	while (gen.hasNext() && (gen.next() != null));
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new"})
    public void testMappedMaxDiffIntegerPermutatorRunToEnd() {
	String methodName = "testMappedMaxDiffIntegerPermutator";
	System.out.println(TestTools.generateMethodHeader(methodName));
	MappedMaxDiffIntegerPermutator gen = new MappedMaxDiffIntegerPermutator(6,3,6,2); // toLen, minvalue, maxvalue, how many different
	IntegerPermutatorTools.testIntegerPermutatorRunToEnd(gen);
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    public String toString() {
	if (result == null) {
	    return "null";
	}
	String s = "";
	for (int i = 0; i < result.length; ++i) {
	    s = s + result[i] + " ";
	}
	return s;	
    }

}
