package graphtools;

import java.util.logging.Logger;

/** Bundles set of helper functions that interpret IntegerList as a path in a graph */
public class PathTools {

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    public static boolean isCyclical(IntegerList path) {
	return ((path != null) && (path.size() > 2) && (path.get(0) == path.get(path.size()-1)));
    }

    /** returns true if two paths are the same of if one path is the reverse of the other */
    public static boolean isEquivalent(IntegerList path1, IntegerList path2) {
	return (path1.equals(path2) || path1.equals(path2.reverse()));
    }

    /** returns true if two paths are the same of if one path is the reverse of the other including rotations */
    public static boolean isEquivalentCircular(IntegerList path1, IntegerList path2) {
	if (path1.size() != path2.size()) {
	    return false;
	}
	if (path1.size() == 0) {
	    return true;
	}
	if (path1.equals(path2) || path1.equals(path2.reverse())) {
	    return true;
	}
	// ignore last element because it repeats first element
	IntegerList path1Sub = path1;
	IntegerList path2Sub = path2;

	// if mode in which first element repeats the last:
	if ((path1.size() > 1) && (path1.get(0) == path1.get(path1.size()-1))) {
	    path1Sub = path1.subset(0, path1.size()-1);
	    path2Sub = path2.subset(0, path2.size()-1);
	}

	// check circular permutations:
	IntegerList path2Rev = path2.reverse();
	for (int i = 1; i < path1Sub.size(); ++i) {
	    IntegerList rotPath = path1Sub.rotate(i);
	    rotPath.add(rotPath.get(0)); 
	    if (rotPath.equals(path2) || rotPath.equals(path2Rev)) {
		return true;
	    }
	}

	log.info("The following paths are not equivalent: " + path1.toString() + " " 
			   + path2.toString());
	return false;
    }

    /** returns true if two paths are the same of if one path is the rotation of the other */
    public static boolean isSameCircular(IntegerList path1, IntegerList path2) {
	if (path1.size() != path2.size()) {
	    return false;
	}
	if (path1.size() == 0) {
	    return true;
	}
	if (path1.equals(path2) || path1.equals(path2.reverse())) {
	    return true;
	}
	// ignore last element because it repeats first element
	IntegerList path1Sub = path1;
	IntegerList path2Sub = path2;

	// if mode in which first element repeats the last:
	if ((path1.size() > 1) && (path1.get(0) == path1.get(path1.size()-1))) {
	    path1Sub = path1.subset(0, path1.size()-1);
	    path2Sub = path2.subset(0, path2.size()-1);
	}

	// check circular permutations:
	for (int i = 1; i < path1Sub.size(); ++i) {
	    IntegerList rotPath = path1Sub.rotate(i);
	    rotPath.add(rotPath.get(0)); 
	    if (rotPath.equals(path2)) {
		return true;
	    }
	}

	log.info("The following paths are not same (with rotations): " + path1.toString() + " " 
			   + path2.toString());
	return false;
    }

    /** return true if integer path contains consecutive pair of values */ 
    public static boolean containsPair(IntegerList path, int id1, int id2) {
	for (int i = 1; i < path.size(); ++i) {
	    if ((path.get(i-1) == id1) && (path.get(i) == id2)) {
		return true;
	    }
	}
// 	if ((path.get(path.size()-1) == id1) && (path.get(0) == id2)) {
// 	    return true;
// 	}
	return false;
    }

    /** returns true if two paths are the same of if one path is the rotation of the other */
    public static boolean isOverlapping(IntegerList path1, IntegerList path2) {
	for (int i = 1; i < path1.size(); ++i) {
	    int id1 = path1.get(i-1);
	    int id2 = path1.get(i);
	    if (containsPair(path2, id1, id2)) {
		return true;
	    }
	}
	// TODO : do not check for circular ends
	// 	int id1 = path1.get(path1.size()-1);
	// 	int id2 = path1.get(0);
	// 	if (containsPair(path2, id1, id2)) {
	// 	    return true;
	// 	}

	return false;
    }

    /** returns index of first equivalent path found. returns size of paths otherwise */
    public static int findEquivalentPath(IntegerListList paths, IntegerList path) {
	for (int i = 0; i < paths.size(); ++i) {
	    if (isEquivalent(path, paths.get(i))) {
		return i;
	    }
	}
	return paths.size();
    }

    /** returns index of first equivalent path found. returns size of paths otherwise */
    public static int findEquivalentCircularPath(IntegerListList paths, IntegerList path) {
	for (int i = 0; i < paths.size(); ++i) {
	    if (isEquivalentCircular(path, paths.get(i))) {
		return i;
	    }
	}
	return paths.size();
    }

    public static IntegerListList getUniquePaths(IntegerListList paths) {
	IntegerListList result = new IntegerListList();
	if ((paths == null) || (paths.size() == 0)) {
	    return result;
	}
	result.add(paths.get(0));
	for (int i = 1; i < paths.size(); ++i) {
	    int id = findEquivalentPath(result, paths.get(i));
	    if (id >= result.size()) { // if no duplicate found
		result.add(paths.get(i));
	    }
	}
	return result;
    }

    public static IntegerListList getUniqueCircularPaths(IntegerListList paths) {
	IntegerListList result = new IntegerListList();
	if ((paths == null) || (paths.size() == 0)) {
	    return result;
	}
	result.add(paths.get(0));
	for (int i = 1; i < paths.size(); ++i) {
	    int id = findEquivalentCircularPath(result, paths.get(i));
	    if (id >= result.size()) { // if no duplicate found
		result.add(paths.get(i));
	    }
	}
	return result;
    }
    
}
