//--------------------------------------
// Systematically generate permutations. 
// taken from: http://www.merriampark.com/perm.htm
// "The source code is free for you to use in whatever way you wish."
//--------------------------------------
package graphtools;

import java.math.BigInteger;

import java.util.ArrayList;
import java.util.List;
import generaltools.TestTools;
import org.testng.*;
import org.testng.annotations.*;

public class PermutationGenerator implements IntegerPermutator {

    private int[] a;
    private BigInteger numLeft;
    private BigInteger total;
    private List<IntegerArrayConstraint> constraints = new ArrayList<IntegerArrayConstraint>();

    //-----------------------------------------------------------
    // Constructor. WARNING: Don't make n too large.
    // Recall that the number of permutations is n!
    // which can be very large, even when n is as small as 20 --
    // 20! = 2,432,902,008,176,640,000 and
    // 21! is too big to fit into a Java long, which is
    // why we use BigInteger instead.
    //----------------------------------------------------------

    /** Constructor of permutation object with 1 place */
    public PermutationGenerator () {
	this(1);
    }

    /** Constructor of permutation object with n places (n numbers ranging from 0 to n-1 */
    public PermutationGenerator (int n) {
	if (n < 1) {
	    throw new IllegalArgumentException ("Minimum argument for PermutationGenerator: 1");
	}
	a = new int[n];
	total = getFactorial (n);
	reset ();
    }

    public void addConstraint(IntegerArrayConstraint constraint) {
	this.constraints.add(constraint);
    }

    public Object clone() { throw new RuntimeException("clone method not supported."); } // FIXIT

    //------
    // Reset
    //------

    public void reset () {
	for (int i = 0; i < a.length; i++) {
	    a[i] = i;
	}
	numLeft = new BigInteger (total.toString ());
	next();
    }

    //------------------------------------------------
    // Return number of permutations not yet generated
    //------------------------------------------------

    public BigInteger getNumLeft () {
	return numLeft;
    }

    //------------------------------------
    // Return total number of permutations
    //------------------------------------

    public BigInteger getTotal () {
	return total;
    }

    //-----------------------------
    // Are there more permutations?
    //-----------------------------

    public boolean hasNext () {
	return hasMore();
    }

    /** @deprecated use hasNext instead */
    public boolean hasMore () {
	return numLeft.compareTo (BigInteger.ZERO) == 1;
    }

    //------------------
    // Compute factorial
    //------------------

    private static BigInteger getFactorial (int n) {
	BigInteger fact = BigInteger.ONE;
	for (int i = n; i > 1; i--) {
	    fact = fact.multiply (new BigInteger (Integer.toString (i)));
	}
	return fact;
    }

    //--------------------------------------------------------
    // Generate next permutation (algorithm from Rosen p. 284)
    //--------------------------------------------------------

    public int[] next () {
	return getNext();
    }

    /** returns current status of integers */
    public int[] get() {
	return a;
    }

    /** increase permutation */
    public boolean inc () {
	while (hasNext()) {
	    if (numLeft.equals (total)) {
		numLeft = numLeft.subtract (BigInteger.ONE);
		if (validate()) {
		    return true;
		}
		else {
		    continue;
		}
	    }
	    
	    int temp;
	    
	    // Find largest index j with a[j] < a[j+1]
	    
	    int j = a.length - 2;
	    while (a[j] > a[j+1]) {
		j--;
	    }
	    
	    // Find index k such that a[k] is smallest integer
	    // greater than a[j] to the right of a[j]
	    
	    int k = a.length - 1;
	    while (a[j] > a[k]) {
		k--;
	    }
	    
	    // Interchange a[j] and a[k]
	    
	    temp = a[k];
	    a[k] = a[j];
	    a[j] = temp;
	    
	    // Put tail end of permutation after jth position in increasing order
	    
	    int r = a.length - 1;
	    int s = j + 1;
	    
	    while (r > s) {
		temp = a[s];
		a[s] = a[r];
		a[r] = temp;
		r--;
		s++;
	    }
	    
	    numLeft = numLeft.subtract (BigInteger.ONE);
	    if (validate()) {
		return true;
	    }
	}
	// no solution found
	assert (!hasNext());
	reset();
	return false;
    }

    public int[] getNext () {
	inc();
	return a;
    }

    /** returns number of items being permuted */
    public int size() { return a.length; }

    public String toString() {
	StringBuffer buf = new StringBuffer();
	for (int n : a) {
	    buf.append("" + n + " ");
	}
	return buf.toString();
    }

    public boolean validate() {
	boolean check1 =  a != null && a.length > 0;
	if (!check1) {
	    return false;
	}
	for (IntegerArrayConstraint constraint : constraints) {
	    if (!constraint.validate(get())) {
		return false;
	    }
	}
	return true;
    }

    @Test(groups={"new"})
    public void testPermutationGeneratorBasics() {
	String methodName = "testPermutationGeneratorBasics";
	System.out.println(TestTools.generateMethodHeader(methodName));
	PermutationGenerator pg = new PermutationGenerator(3);
//  	while (pg.hasNext()) {
// 	    int a[] = pg.next();
// 	    for (int n : a) {
// 		System.out.print("" + n + " ");
// 	    }
// 	    System.out.println();
// 	}
	int count = 0;
	do {
	    int a[] = pg.get();
	    ++count;
	    System.out.print("" + count + " : ");
	    for (int n : a) {
		System.out.print("" + n + " ");
	    }
	    System.out.println();
	}
	while (pg.hasNext() && pg.inc());
	assert count == 6;
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

}
