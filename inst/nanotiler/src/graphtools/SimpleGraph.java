package graphtools;

import java.util.ArrayList;
import java.util.List;

/** implements simple connection graph as matrix of booleans. */
public class SimpleGraph implements GraphBase {

    public static final String NEWLINE = System.getProperty("line.separator");

    boolean[][] matrix;
    int nodeCount;
    int edgeCount;
    List<IntPair> edgeList = new ArrayList<IntPair>();
    List<IntPair>[] edgeArray; // separate list of edges for each node

    @SuppressWarnings(value="unchecked")
    public SimpleGraph(int n) {
	nodeCount = n;
	edgeCount = 0;
	matrix = new boolean[n][n];
	edgeArray = new ArrayList[n];
	for (int i = 0; i < n; ++i) {
	    for (int j = 0; j < n; ++j) {
		matrix[i][j] = false; // no connection by default
	    }
	    edgeArray[i] = new ArrayList<IntPair>(i);
	}
    }

    /** returns all connections of node n */
    public IntegerList getConnections(int n) {
	IntegerList result = new IntegerList();
	for (int i = 0; i < matrix.length; ++i) {
	    if (matrix[n][i]) {
		result.add(i);
	    }
	}
	return result;
    }

    /** returns number of nodes */
    public int getNodeCount() { return nodeCount; }

    /** returns number of edges */
    public int getEdgeCount() { return edgeCount; }

    /** returns number of edges involved with n'th node */
    public int getNodeEdgeCount(int n) { return edgeArray[n].size(); }

    /** returns n'th edge of graph */
    public IntPair getEdge(int n) { return (IntPair)(edgeList.get(n)); }

    /** returns edge of node nodeId. EdgeNum is a number between zero and getNodeEdgeCount(nodeId) */
    public IntPair getNodeEdge(int edgeNum, int nodeId) { return (IntPair)(edgeArray[nodeId].get(edgeNum)); }

    /** is there a connection from node n1 to node n2 ? */
    public boolean isConnected(int n1, int n2) {
	return matrix[n1][n2];
    }

    public static boolean isEquivalent(IntPair edge1, IntPair edge2) {
	return (((edge1.getFirst() == edge2.getFirst()) && (edge1.getSecond() == edge2.getSecond()))
	    || ((edge1.getFirst() == edge2.getSecond()) && (edge1.getSecond() == edge2.getFirst())) );
    }

    /** returns index of edge, -1 if not found */
    public int indexOf(IntPair edge) {
	for (int i = 0; i < getEdgeCount(); ++i) {
	    if (isEquivalent(getEdge(i), edge)) {
		return i;
	    }
	}
	return -1;
    }

    /** returns index of edge, -1 if not found */
    public int nodeIndexOf(IntPair edge, int node) {
	for (int i = 0; i < getNodeEdgeCount(node); ++i) {
	    if (isEquivalent(getNodeEdge(i, node), edge)) {
		return i;
	    }
	}
	return -1;
    }

    /** sets or deletes a connection from node n1 to node n2 */
    public void setConnected(int i, int j, boolean status) {
	if (status) {
	    if (!matrix[i][j]) {
		IntPair newEdge = new IntPair(i,j);
		edgeList.add(newEdge);
		edgeArray[i].add(newEdge);
		edgeArray[j].add(newEdge);
		++edgeCount;
	    }
	}
	else if (matrix[i][j]) {
	    --edgeCount;
	    IntPair newEdge = new IntPair(i,j);
	    edgeList.remove(newEdge);
	    if (edgeArray[i].contains(newEdge)) {
		edgeArray[i].remove(newEdge);
	    }
	    else {
		edgeArray[i].remove(newEdge.reverse());
	    }
	    if (edgeArray[j].contains(newEdge)) {
		edgeArray[j].remove(newEdge);
	    }
	    else {
		edgeArray[j].remove(newEdge.reverse());
	    }
	}
	matrix[i][j] = status;
	matrix[j][i] = status;
    }

    public String toString() {
	String result = "" + getNodeCount() + " " + getEdgeCount() + NEWLINE;
	for (int i = 0; i < getNodeCount(); ++i) {
	    for (int j = 0; j < getNodeCount(); ++j) {
		if (matrix[i][j]) {
		    result = result + "1 ";
		}
		else {
		    result = result + "0 ";
		}
	    }
	    result = result + NEWLINE;
	}
	return result;
    }
}
