package graphtools;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class SimpleGraphBaseOrderSet implements GraphBaseOrderSet {

    public static final String NEWLINE = System.getProperty("line.separator");

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    int nodeCount;
    List graphs = new ArrayList();
    IntegerListList[][] pathMatrix; // all paths so far found leading from node i to node j
    // IntegerListList[] cyclicalPaths; // all cyclical paths founds so far
    int highestOrder;
    GraphBase graph;

    public SimpleGraphBaseOrderSet(GraphBase graph) {
	this.graph = graph;
	nodeCount = graph.getNodeCount();
	highestOrder = 1; // paths of this length considered so far
	log.fine("Starting SimpleGraphBaseOrderSet(GraphBase graph)! " 
			   + highestOrder + " " + nodeCount);
	pathMatrix = new IntegerListList[nodeCount][nodeCount];
	// cyclicalPaths = new IntegerListList[nodeCount];
	int countConnected = 0;
	for (int i = 0; i < graph.getNodeCount(); ++i) {
	    // cyclicalPaths[i] = new IntegerListList();
	    for (int j = 0; j < graph.getNodeCount(); ++j) {
		pathMatrix[i][j] = new IntegerListList();
		if (graph.isConnected(i,j)) {
		    ++countConnected;
		    IntegerList newPath = new IntegerList();
		    newPath.add(i);
		    newPath.add(j); // not really necessary
		    pathMatrix[i][j].add(newPath);
		    log.fine("Added path to " + i + " " + j + " " + pathMatrix[i][j].size());
		}
	    }
	}
	log.fine("Overall " + countConnected + " connections defined as paths!");
    }

    /** defines datastructure up to path lengths of length "order" */
    public SimpleGraphBaseOrderSet(GraphBase graph, int order) {
	this(graph);
	for (int i = 0; i < (order-1); ++i) {
	    generateNextOrderGraph();
	}
    }

    /** generates next order graph. For example, the connections in a first order
     * graph represent simple links. The connections in a second order graph
     * represent two-link paths. The paths of an n'th order graph are not allowed to contain duplicates */
    public void generateNextOrderGraph() {
	GraphBase newGraph = new SimpleGraph(size());
	for (int i = 0; i < size(); ++i) {
	    for (int j = 0; j < size(); ++j) {
		if (i == j) {
		    continue; // do not extend already cyclical graphs
		}
		IntegerListList paths = getPaths(i,j);
		// loop over paths going from i to j :
		if (paths == null) {
		    continue;
		}
		for (int m = 0; m < paths.size(); ++m) {
		    IntegerList path = paths.get(m); // get m'th path defined so far
		    if ((path != null) && ((path.size()-1) == highestOrder)) {
			int endNode = path.get(path.size()-1);
			// get all connections from last node:
			IntegerList endConnections = graph.getConnections(endNode);
			if (endConnections == null) {
			    continue;
			}
			// check if cyclical:
			if (path.size() > 2) {
			    for (int k = 0; k < endConnections.size(); ++k) {
				int conn = endConnections.get(k);
				if ((conn == i) || (! path.contains(conn))) { // cyclical path found:
				    // new path of higher order found
				    IntegerList newPath = new IntegerList(path);
				    newPath.add(conn);
				    pathMatrix[i][conn].add(newPath);
				    if (conn == i) {
					log.fine("Cyclical path found for node " + i );
				    }
				}
			    }
			    
			}
			else { // cannot be cyclical because current path has length of only 2 nodes
			    for (int k = 0; k < endConnections.size(); ++k) {
				int conn = endConnections.get(k);
				if ((conn == i) || (conn == j)) {
				    continue;
				}
				else {
				    // new path of higher order found
				    IntegerList newPath = new IntegerList(path);
				    newPath.add(conn);
				    pathMatrix[i][conn].add(newPath);
				    // pathMatrix[conn][i].add(newPath.reverse());
				}
			    }
			}
		    }
		}
	    } 
	}
	++highestOrder;
    }

    /** returns longest path lengths considered so far */
    public int getHighestOrder() { return highestOrder; }

    /** returns number of nodes */
    public int size() { return nodeCount; }
    
    /** returns all paths from node n with certain length that contain no cycles */
    // public IntegerListSet getPaths(int node, int length);

    public IntegerListList getPaths(int n1, int n2) {
	return pathMatrix[n1][n2];
    }

    /** returns all cyclical paths from node n with certain length (returns non-cyclical path of length n-1,
     * the connection from the last node (n-1) to the first one is not included in the path but excists).
     */
    public IntegerListList getCyclicalPaths(int node) { return pathMatrix[node][node]; }
    
    public String toString() {
	String result = "";
	/*
	for (int i = 0; i < nodeCount; ++i) {
	    for (int j = i+1; j < nodeCount; ++j) {
		IntegerListList paths = pathMatrix[i][j];
		if ((paths != null) && (paths.size() > 0)) {
		    int n = paths.size(); 
		    result = result + i + " " + j + " " + n + " : ";
		    for (int k = 0; k < n; ++k) {
			IntegerList path = paths.get(k);
			result = result + " " + k + ":";
			for (int m = 0; m < path.size(); ++m) {
			    result = result + path.get(m) + " ";
			}
		    }
		    result = result + NEWLINE;
		}
	    }
	}
	*/
	result = result + "Cyclical paths:" + NEWLINE;
	for (int i = 0; i < pathMatrix.length; ++i) {
	    if (pathMatrix[i][i].size() > 0) { 
		for (int j = 0; j < pathMatrix[i][i].size(); ++j) {
		    IntegerList path = pathMatrix[i][i].get(j);
		    result = result + " " + i + ": ";
		    for (int k = 0; k < path.size(); ++k) {
			result = result + path.get(k) + " ";
		    }
		    result = result + NEWLINE;
		}
	    }
	}
	return result;
    }
}
