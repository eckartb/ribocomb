package guitools;

import java.awt.Component;
import java.awt.event.ActionListener;

public interface BeanEditor {

    public void addActionListener(ActionListener listener);

    /** general property editor */
    public void launchEdit(Object o, Component parentFrame);

    public boolean isFinished();

    public Object getObject();

}
