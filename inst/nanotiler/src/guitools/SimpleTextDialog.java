package guitools;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class SimpleTextDialog implements TextDialog {

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    // external action to perform if "Done" is pressed 
    ActionListener externalDoneActionListener; 
    JFrame frame;
    Component parentFrame; // used for disabling
    JTextArea textArea;
    String originalText; // used for reset
    boolean cancelled = false;
    boolean finished = false;

    private class DoneActionListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    frame.setVisible(false);
	    finished = true;
	    log.fine("SimpleTextDialog finished!");
	    // perform external action if defined:
	    if (externalDoneActionListener != null) {
		externalDoneActionListener.actionPerformed(e);
	    }
	    if (parentFrame != null) {
		parentFrame.setEnabled(true);
		parentFrame.repaint();
	    }
	}
    }

    private class CancelActionListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    frame.setVisible(false);
	    log.fine("SimpleTextDialog cancelled!");
	    // perform external action if defined:
	    if (parentFrame != null) {
		parentFrame.setEnabled(true);
		parentFrame.repaint();
	    }
	    cancelled = true;
	    finished = true;
	}
    }


    private class ClearActionListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    textArea.setText("");
	}
    }

    private class ResetActionListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    textArea.setText(originalText);
	}
    }


    private class BottomPanel extends JPanel {

	BottomPanel() {
	    setLayout(new FlowLayout());
	    JButton button = new JButton("Reset");
	    button.addActionListener(new ResetActionListener());
	    add(button);
	    button = new JButton("Clear");
	    button.addActionListener(new ClearActionListener());
	    add(button);
	    button = new JButton("Cancel");
	    button.addActionListener(new CancelActionListener());
	    add(button);
	    button = new JButton("Done");
	    button.addActionListener(new DoneActionListener());
	    add(button);
	}

    }

    public SimpleTextDialog(String s, ActionListener externalDoneActionListener,
			    Component parentFrame) {
	this.externalDoneActionListener = externalDoneActionListener;
	this.originalText = s;
	this.parentFrame = parentFrame;
	frame = new JFrame();

	Container f = frame.getContentPane();

	f.setLayout(new BorderLayout());

	textArea = new JTextArea(10, 8);
	if (s != null) {
	    textArea.setText(s);
	}
	f.add(textArea, BorderLayout.CENTER);
	f.add(new BottomPanel(), BorderLayout.SOUTH);
	frame.pack();
	frame.setVisible(true);
	if (parentFrame != null) {
	    parentFrame.setEnabled(false);
	}
	frame.requestFocusInWindow();
    }

    public String getText() { return textArea.getText(); }

    public boolean isCancelled() { return cancelled; }

    public boolean isFinished() { return finished; }

}
