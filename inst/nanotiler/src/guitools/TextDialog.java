package guitools;

import javax.swing.JFrame;

public interface TextDialog {

    /** starts the dialog, returns text */
    public String getText();

    public boolean isCancelled();

    public boolean isFinished();

}
