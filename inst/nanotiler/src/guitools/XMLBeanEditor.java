package guitools;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.ByteArrayOutputStream;
import java.io.StringBufferInputStream;
import java.io.StringWriter;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Logger;
import javax.swing.JFrame;

public class XMLBeanEditor implements BeanEditor {

    public static final String NEWLINE = System.getProperty("line.separator");

    public static Logger log = Logger.getLogger("NanoTiler_debug");
    
    private boolean finished = false;
    private Object object;
    private TextDialog textDialog;
    Set<ActionListener> actionListeners = new HashSet<ActionListener>();

    private class LocalDoneActionListener implements ActionListener {

	public void actionPerformed(ActionEvent e) {
	    log.fine("Starting LocalDoneActionListener.actionPerformed!");
	    String s = textDialog.getText();
	    // translate string into object
	    StringBufferInputStream stringStream = new StringBufferInputStream(s);
	    XMLDecoder d = new XMLDecoder(stringStream);
	    object = d.readObject();
	    d.close();
	    finished = true;

	    if (object != null) {
		// debug info:
		StringWriter stringWriter = new StringWriter();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		// convert strand to Xml:
		XMLEncoder enc = new XMLEncoder(baos);
		enc.writeObject(object);
		enc.close();
		String xmlDebugString = baos.toString();
		log.fine("Debug XML output of gerator object: " + NEWLINE
				   + xmlDebugString);
	    }
	    else {
		log.warning("Debug XML output of gerator object not possible: object was null! " + NEWLINE);
	    }

	    Iterator iterator = actionListeners.iterator();
	    int i = 0;
	    while (iterator.hasNext()) {
		log.fine("calling listener " + (i++) + " of XMLBeanEditor in LocalDoneActionListener.actionPerformed!");
		ActionListener listener = (ActionListener)(iterator.next());
		listener.actionPerformed(e);
	    }
	    log.fine("Ending LocalDoneActionListener.actionPerformed!");
	}
    }

    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }


    /** general property editor */
    public void launchEdit(Object o, Component parentFrame) {
	// first translate object into xml string:
	finished = false;
	StringWriter stringWriter = new StringWriter();
	ByteArrayOutputStream baos = new ByteArrayOutputStream();
	// convert strand to Xml:
	XMLEncoder e = new XMLEncoder(baos);
	e.writeObject(o);
	e.close();
	String xmlString = baos.toString();
	// xmlString = "hihi!";
	// call editor:
// 	CustomDialog customDialog = new CustomDialog(rootFrame, xmlString);
// 	customDialog.pack();
// 	customDialog.setLocationRelativeTo(rootFrame);
// 	customDialog.setVisible(true);
	textDialog = new SimpleTextDialog(xmlString, new LocalDoneActionListener(), parentFrame);
	// request result string
	// String s = customDialog.getValidatedText();
	String s = textDialog.getText();
	// translate string into object
	StringBufferInputStream stringStream = new StringBufferInputStream(s);
	XMLDecoder d = new XMLDecoder(stringStream);
	Object result = d.readObject();
	d.close();
    }

    /** returns true if dialog was cancelled */
    public boolean isCancelled() {
	return textDialog.isCancelled(); // was dialog cancelled?
    }

    /** returns true iff editing has been launched and is done */
    public boolean isFinished() {
	return textDialog.isFinished();
    }

    /** returns object */
    public Object getObject() {
	return object;
    }


}
