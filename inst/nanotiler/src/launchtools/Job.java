package launchtools;

public interface Job {

    /** adds exceptions that might have occurred during runtime */
    public void addException(Exception e);

    public void addJobFinishedListener(JobFinishedListener listener);

    /** notifies all listeners that the job is finished */
    void fireJobFinished();

    /** returns job id */
    public int getJobId();

    /** returns run command */
    public RunCommand getCommand();

    /** contains start and stop time, and possibly run results and run command and submission parameters */
    public ProcessRunInfo getRunInfo();

    /** returns true if job is finished */
    public boolean isFinished();

    /** returns true if job is running */
    public boolean isRunning();

    public void removeJobFinishedListener(JobFinishedListener listener);

    /** sets to true after job has finished */
    public void setFinished();

    /** sets job id */
    void setJobId(int n);

    void setRunInfo(ProcessRunInfo runInfo);

    public void start();

    public void stop();

}
