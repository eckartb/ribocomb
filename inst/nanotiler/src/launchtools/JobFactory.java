package launchtools;

/** Factory design pattern for creating Job objects */
public interface JobFactory {

    /** creates new job object for given command */
    public Job createJob(RunCommand command);

}
