package launchtools;

public interface JobFinishedListener {

    public void jobFinished(JobFinishedEvent event);

}
