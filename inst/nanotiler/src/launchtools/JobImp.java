package launchtools;

import java.util.ArrayList;
import java.util.List;

public class JobImp implements Job {

    private List<JobFinishedListener> listeners = new ArrayList<JobFinishedListener>();
    private boolean started = false;
    private boolean running = false;
    private boolean finished = false;
    private int jobId = 0;
    private List<Exception> exceptions = new ArrayList<Exception>();
    private RunCommand command = null;
    private ProcessRunInfo runInfo = null;

    /** constructure is not public. Instead use QueueManager as factory */
    JobImp(RunCommand command) { this.command = command; }

    /** adds exceptions that might have occurred during runtime */
    public void addException(Exception e) {
	exceptions.add(e);
    }

    public void addJobFinishedListener(JobFinishedListener listener) { listeners.add(listener); }

    /** notifies all listeners that the job is finished */
    public void fireJobFinished() {
	JobFinishedEvent event = new JobFinishedEvent(this);
	for (int i = 0; i < listeners.size(); ++i) {
	    JobFinishedListener listener = getJobFinishedListener(i);
	    listener.jobFinished(event);
	}
    }

    public JobFinishedListener getJobFinishedListener(int n) {
	return (JobFinishedListener)(listeners.get(n));
    }

    /** returns job id */
    public int getJobId() { return jobId; }

    /** returns run command */
    public RunCommand getCommand() { return command; }

    /** contains start and stop time, and possibly run results and run command and submission parameters */
    public ProcessRunInfo getRunInfo() { return runInfo; }

    /** returns true if job is finished */
    public boolean isFinished() { return finished; }

    /** returns true if job is running */
    public boolean isRunning() { return running; }

    public boolean isStarted() { return started; }

    public void removeJobFinishedListener(JobFinishedListener listener) { listeners.remove(listener); }

    /** sets to true after job has finished */
    public void setFinished(boolean b) { finished = b; }

    /** sets job id */
    public void setJobId(int n) { jobId = n; }

    public void setRunInfo(ProcessRunInfo runInfo) { this.runInfo = runInfo; }

    public void setFinished() { finished = true; running = false; }

    public void start() { started = true; running = true; }

    public void stop() { running = false; finished = true; }

    public String toString() {
	String result = "job " + getJobId() + " : "
	    + getCommand();
	return result;
    }

}
