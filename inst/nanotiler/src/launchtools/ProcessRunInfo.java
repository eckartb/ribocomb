package launchtools;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProcessRunInfo {
    Date startTime = null;
    Date stopTime = null;
    List<String> lines = new ArrayList<String>();

    ProcessRunInfo() {
    }
    
    public Date getStartTime() {
	return startTime;
    }

    public Date getStopTime() {
	return stopTime;
    }

    public void setStartTime(Date d) {
	startTime = d;
    }

    public void setStopTime(Date d) {
	stopTime = d;
    }

    public void start() {
	startTime = new Date();
    }

    public void stop() {
	stopTime = new Date();
    }

    public boolean isRunning() {
	return (startTime != null) && (stopTime == null);
    }

    public boolean isFinished() {
	return (startTime != null) && (stopTime != null);
    }

    /** how long did job run so far? */
    public long getRunTime() {
	if (isFinished()) {
	    return (stopTime.getTime() - startTime.getTime());
	}
	else if (isRunning()) {
	    Date current = new Date();
	    return (current.getTime() - startTime.getTime());
	}
	return 0; // job was not started so far
    }

    public void addLine(String line) {
	lines.add(line);
    }

    public List<String> getLines() { return lines; }

}
