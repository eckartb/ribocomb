package launchtools;

public interface Queue {

    /** returns status of queue */
    public QueueStatus getStatus();

    /** returns job status */
    public Job getJobStatus(int jobId) throws UnknownJobException;

    /** returns name of queue */
    public String getName();

    /** deletes job from queue */
    public void deleteJob(int jobId) throws UnknownJobException;

    /** submits a job. The results of the job are in Job.getResults() at end.
     * If one wants to get notified of a finished job, one has to add a listener
     * to the job itself with job.addJobFinishedListener(listener) prior to submission.
     */
    public void submit(Job job);
    
    /** resumes job that was suspended before. If it was already running and not suspended,
     * do nothing */
    public void resumeJob(int jobId) throws UnknownJobException;

    /** sets name of queue */
    public void setName(String name);

    /** suspends job.  Restart with resumeJob */
    public void suspendJob(int jobId) throws UnknownJobException;

}
