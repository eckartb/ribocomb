package launchtools;

public interface QueueManager extends JobFactory {

    public void addQueueManagerChangeListener(QueueManagerChangeListener listener);

    public void fireChangeEvent(QueueManagerChangeEvent event);

    /** returns status of queue */
    public QueueManagerStatus getStatus();

    /** returns true if generation of queue was successful */
    public boolean generateQueue(String queueName, QueuePolicy policy);

    /** returns job status */
    public Job getJobStatus(int jobId) throws UnknownJobException;

    /** returns new job id */
    public int generateNewJobId();

    public int getNumberQueues();

    public String getQueueName(int n) throws IndexOutOfBoundsException;

    public QueueStatus getQueueStatus(String queueName) throws UnknownQueueException;

    /** deletes job from queue */
    public void deleteJob(int jobId) throws UnknownJobException;

    /** submits a job. The results of the job are in Job.getResults() at end.
     * If one wants to get notified of a finished job, one has to add a listener
     * to the job itself with job.addJobFinishedListener(listener) prior to submission.
     */
    public void submit(Job job);

    /** submits a job. The results of the job are in Job.getResults() at end.
     * If one wants to get notified of a finished job, one has to add a listener
     * to the job itself with job.addJobFinishedListener(listener) prior to submission.
     */
    public void submit(Job job, String queueName) throws UnknownQueueException;
    
    /** resumes job that was suspended before. If it was already running and not suspended,
     * do nothing */
    public void resumeJob(int jobId) throws UnknownJobException;

    /** suspends job.  Restart with resumeJob */
    public void suspendJob(int jobId) throws UnknownJobException;

}
