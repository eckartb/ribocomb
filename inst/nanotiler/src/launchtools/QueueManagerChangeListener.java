package launchtools;

public interface QueueManagerChangeListener {

    public void statusChanged(QueueManagerChangeEvent event);

}
