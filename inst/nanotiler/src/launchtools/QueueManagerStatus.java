package launchtools;

/** status of queue manager and queues as of interest to user */
public interface QueueManagerStatus {

    /** writes status to string */
    public String toString();

}
