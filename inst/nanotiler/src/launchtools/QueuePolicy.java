package launchtools;

/** status of queue manager and queues as of interest to user */
public interface QueuePolicy {

    /** gets maximum number of jobs allowed to run simultaneously */
    public int getMaxRunning(int n);

    /** gets the maximum run-time of a job */
    public long getMaxRuntime();

    /** sets how many jobs may run in parallel */
    public void setMaxRunning(int n);

    /** sets the maximum run-time of a job */
    public void setMaxRuntime(long milliseconds);

}
