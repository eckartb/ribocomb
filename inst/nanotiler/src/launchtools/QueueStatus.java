package launchtools;

/** status of queue as of interest to user */
public interface QueueStatus {

    /** writes status to string */
    public String toString();

}
