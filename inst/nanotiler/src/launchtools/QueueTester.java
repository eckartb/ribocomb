package launchtools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.testng.*;
import org.testng.annotations.*;

/** Class used for testing queues and jobs, because many classes in this package do not have default constructor */
public class QueueTester {

    public QueueTester() {
    }

    @Test(groups={"verynew"})
    public void testShell() {
	System.out.println("Starting testShell");
	String testString = "hello world";
	List<String> helloWorld = SimpleQueueManager.shell("echo " + testString); // launch shell command in one line!
	System.out.println("Output of hello world test:");
	for (int i = 0; i < helloWorld.size(); ++i) {
	    System.out.println(helloWorld.get(i));
	}
	assert helloWorld.get(0).equals(testString);
    }


}
