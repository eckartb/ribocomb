package launchtools;

public interface RunCommand {

    public String[] getCommand();

    public String[] getStandardInput();

}
