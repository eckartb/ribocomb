package launchtools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.testng.*;
import org.testng.annotations.*;

import generaltools.ApplicationBugException;

public class SimpleQueueManager implements QueueManager {

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    private Map<String, Queue> queueNames = new HashMap<String, Queue>();

    private List<Queue> queues = new ArrayList<Queue>();

    private Map jobs = new HashMap();

    private static final String defaultQueueName = "default";

    private List<QueueManagerChangeListener> changeListeners = new ArrayList<QueueManagerChangeListener>();

    private static SimpleQueueManager instance = null;

    private int jobIdCounter = 1;

    /** constructor is private because of singleton patter */
    private SimpleQueueManager() {
	generateQueue(defaultQueueName, new SimpleQueuePolicy());
    }

    /** singleton pattern: use getInstance instead of constructor */
    public static synchronized SimpleQueueManager getInstance() {
	if (instance == null) {
	    instance = new SimpleQueueManager();
	}
	return instance;
    }

    public void addQueueManagerChangeListener(QueueManagerChangeListener listener) {
	this.changeListeners.add(listener);
    }

    /** Factory design pattern for generating job objects */
    public synchronized Job createJob(RunCommand command) {
	int jobId = generateNewJobId();
	Job job = new JobImp(command);
	job.setJobId(jobId);
	return job;
    }

    public void fireChangeEvent(QueueManagerChangeEvent event) {
	for (int i = 0; i < changeListeners.size(); ++i) {
	    QueueManagerChangeListener listener =
		(QueueManagerChangeListener)(changeListeners.get(i));
	    listener.statusChanged(event);
	}
    }

    /** returns status of queue */
    public QueueManagerStatus getStatus() { 
	return new SimpleQueueManagerStatus(this); 
    }

    /** returns true if generation of queue was successful */
    public boolean generateQueue(String queueName, QueuePolicy policy) {
	Queue queue = new SingleQueue(policy);
	queue.setName(queueName);
	queueNames.put(queueName, queue);
	queues.add(queue);
	return true; // TODO not totally clean
    }

    /** returns job status */
    public Job getJobStatus(int jobId) throws UnknownJobException {
	Job job = (Job)(jobs.get(new Integer(jobId)));
	if (job == null) {
	    throw new UnknownJobException();
	}
	return job;
    }

    public int getNumberQueues() { return queues.size(); }

    /** returns queue for job whose queue was not specified */
    public Queue getQueue(String name) throws UnknownQueueException {
	Queue queue = (Queue)(queueNames.get(name));
	if (queue == null) {
	    throw new UnknownQueueException();
	}
	return queue;
    }

    /** returns queue for job whose queue was not specified */
    public Queue getQueue(int n) {
	return (Queue)(queues.get(n));
    }

    /** returns queue for job whose queue was not specified */
    public Queue getQueue() {
	Queue queue = null;
	try {
	    queue = getQueue(defaultQueueName); // TODO: better policy
	}
	catch (UnknownQueueException e) {
	    throw new ApplicationBugException("Could not find default queue: " + defaultQueueName);
	}
	return queue;
    }

    public String getQueueName(int n) throws IndexOutOfBoundsException {
	return getQueue(n).getName(); // TODO not yet implemented
    }

    public QueueStatus getQueueStatus(String queueName) throws UnknownQueueException {
	Queue queue = getQueue(queueName);
	return queue.getStatus();
    }

    /** deletes job from queue */
    public void deleteJob(int jobId) throws UnknownJobException {
	Queue queue = findJobQueue(jobId);
	queue.deleteJob(jobId);
    }

    private Queue findJobQueue(int jobId) throws UnknownJobException {
	// TODO : not yet implemented
	return getQueue();
    }

    private void registerJob(Job job, Queue queue) {
	// TODO
    }

    /** submits a job. The results of the job are in Job.getResults() at end.
     * If one wants to get notified of a finished job, one has to add a listener
     * to the job itself with job.addJobFinishedListener(listener) prior to submission.
     */
    public synchronized void submit(Job job) {
	Queue queue = getQueue();
	submit(job, queue);
    }

    /** convinience method for performing shell command */
    public static synchronized List<String> shell(String shellCommand) {
	SimpleQueueManager queueManager = SimpleQueueManager.getInstance(); // singleton pattern
	String[] commandWords = shellCommand.split(" ");
	// generate command
	RunCommand command = new SimpleRunCommand(commandWords);
	Job job = queueManager.createJob(command);	
	queueManager.submit(job);	    
	List<String> lines = job.getRunInfo().getLines();
	return lines;
    }

    public synchronized int generateNewJobId() {
	return ++jobIdCounter;
    }

    /** submits a job. The results of the job are in Job.getResults() at end.
     * If one wants to get notified of a finished job, one has to add a listener
     * to the job itself with job.addJobFinishedListener(listener) prior to submission.
     */
    public synchronized void submit(Job job, String queueName) throws UnknownQueueException {
	Queue queue = getQueue(queueName);
	submit(job, queue);
    }

    private synchronized void submit(Job job, Queue queue) { 
	log.fine("Submitted job: " + job.getJobId() + " " + job.getCommand() 
			   + " to queue " + queue.getName());
	// int jobId = generateNewJobId();
	// job.setJobId(jobId);
	registerJob(job, queue); // TODO
	queue.submit(job);
    }

    /** resumes job that was suspended before. If it was already running and not suspended,
     * do nothing */
    public void resumeJob(int jobId) throws UnknownJobException {
	Queue queue = findJobQueue(jobId);
	queue.resumeJob(jobId);
    }

    /** suspends job.  Restart with resumeJob */
    public void suspendJob(int jobId) throws UnknownJobException {
	Queue queue = findJobQueue(jobId);
	queue.suspendJob(jobId);
    }

}
