package launchtools;

/** status of queue manager and queues as of interest to user */
public class SimpleQueuePolicy implements QueuePolicy {

    private int maxRunning = 1;

    private long maxRuntime = -1; // negative: no restriction

    /** gets maximum number of jobs allowed to run simultaneously */
    public int getMaxRunning(int n) { return maxRunning; }

    /** gets the maximum run-time of a job */
    public long getMaxRuntime() { return maxRuntime; }

    /** sets how many jobs may run in parallel */
    public void setMaxRunning(int n) { this.maxRunning = n; }

    /** sets the maximum run-time of a job */
    public void setMaxRuntime(long milliseconds) { this.maxRuntime = milliseconds; }

}
