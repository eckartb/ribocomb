package launchtools;

public class SimpleRunCommand implements RunCommand {

    private String[] command;
    private String[] standardInput;

    public SimpleRunCommand(String[] command) { 
	this.command = command; 
    }

    public SimpleRunCommand(String[] command, String[] standardInput) { 
	this.command = command; 
	this.standardInput = standardInput;
    }

    public String[] getCommand() { return command; }

    public String[] getStandardInput() { return standardInput; }

    public void setCommand(String[] strings) { this.command = strings; }

    public String toString() {
	String result = "";
	for (int i = 0; i < command.length; ++i) {
	    result = result + command[i] + " ";
	}
	return result;
    }

}
