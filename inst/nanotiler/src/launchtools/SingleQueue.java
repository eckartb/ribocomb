package launchtools;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.PrintStream;
import java.io.IOException;
import java.util.logging.Logger;

public class SingleQueue implements Queue {

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    QueuePolicy policy;
    String name;

    /** constructor takes queue policy */
    public SingleQueue(QueuePolicy policy) { this.policy = policy; }

    /** default constructor */
    public SingleQueue() { this(new SimpleQueuePolicy()); }

    public String getName() { return name; }

    /** returns status of queue */
    public QueueStatus getStatus() { return new SimpleQueueStatus(this); }

    /** returns job status */
    public Job getJobStatus(int jobId) throws UnknownJobException {
	return null; // TODO
    }

    /** deletes job from queue */
    public void deleteJob(int jobId) throws UnknownJobException {
	// TODO
    }

    public void setName(String name) { this.name = name; }

    /** starts next job in queue */
    synchronized void startNextJob() {
	// TODO
    }

    /** resumes job that was suspended before. If it was already running and not suspended,
     * do nothing */
    public void resumeJob(int jobId) throws UnknownJobException {
	// TODO
    }

    /** suspends job.  Restart with resumeJob */
    public void suspendJob(int jobId) throws UnknownJobException { 
	// TODO
    }

    /** submits a job. The results of the job are in Job.getResults() at end.
     * If one wants to get notified of a finished job, one has to add a listener
     * to the job itself with job.addJobFinishedListener(listener) prior to submission.
     */
    public synchronized void submit(Job job) {
	log.fine("Started job on SingeQueue: " + job);
	String[] command = job.getCommand().getCommand();
	Runtime runtime = Runtime.getRuntime();
	Process process = null;
	DataInputStream in = null;
	PrintStream out = null;
	try {
	    ProcessRunInfo runInfo = new ProcessRunInfo();
	    job.setRunInfo(runInfo);
	    job.start();
	    process = runtime.exec(command);
	    out = new PrintStream(process.getOutputStream());		
	    String[] standardInput = job.getCommand().getStandardInput();
	    if (standardInput != null) {
		for (int i = 0; i < standardInput.length; ++i) {
		    out.println(standardInput[i]);
		}
		out.close();
	    }
	    in = new DataInputStream(process.getInputStream());		
	    // Read and print the output
	    String line = null;
	    while ((line = in.readLine()) != null) {
		// out.println(line);
		runInfo.addLine(line); // store output of launched program in runinfo (EB 2007)
	    }
	    in.close();
	}
	catch (IOException e) {
	    job.addException(e);
	}
	job.stop();
	log.fine("Finished job on SingleQueue!");
    }

}
