package launchtools;

import generaltools.ApplicationException;

public class UnknownJobException extends ApplicationException {
    
    public UnknownJobException() {
	super("Unknown job");
    }

    public UnknownJobException(String msg) {
	super(msg);
    }

}
