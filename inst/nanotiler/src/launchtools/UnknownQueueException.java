package launchtools;

import generaltools.ApplicationException;

public class UnknownQueueException extends ApplicationException {
    
    public UnknownQueueException() {
	super("Unknown job");
    }

    public UnknownQueueException(String msg) {
	super(msg);
    }

}
