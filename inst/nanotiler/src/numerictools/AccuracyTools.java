package numerictools;

public class AccuracyTools {


    /* compute true/false positives/negatives of scores, which if greater
       than cutOff, are predicted to belong to class classNum */
    
    public static double computeMatthews(  int truePos,  int falsePos,
					   int trueNeg,  int falseNeg)
    {
	double head = (double)((truePos * trueNeg))
	    - (double)((falsePos * falseNeg));
	double denom = (double)(trueNeg + falseNeg)
	    * (double)(trueNeg + falsePos)
	    * (double)(truePos + falseNeg)
	    * (double)(truePos + falsePos);
	if (denom <= 0.0) {
	    return 0.0;
	}
	return head / Math.sqrt(denom);
    }


    
}
