package numerictools;

/** converts number M to string representation with respect to base N
 * adapted from: http://www.cut-the-knot.org/recurrence/conversion.shtml
 */
public class BaseConversion {
 
    // return string, accept two integers
    public static String convert(int M, int N)	
    {	
	if (M < N)	{ // see if it's time to return
	    return new String(""+M);	// ""+M makes a string out of a digit
	}
	// else	// the time is not yet ripe
	return convert(M/N, N) + new String(""+(M % N));	// continue
    }

}
