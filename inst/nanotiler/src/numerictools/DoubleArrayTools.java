package numerictools;

import generaltools.ParsingException;
import generaltools.StringTools;
import java.io.*;
import java.util.*;
import generaltools.Randomizer;

import org.testng.*;
import org.testng.annotations.*;

/** collection of static methods that work with datatype double[][] */
public class DoubleArrayTools {

    /**
     * chooses an element indix from vector pVec with probability given in pVec
     * imagine probabilities like pieces of "pie" 
     */
    public static int chooseRouletteWheel(double[] pVec) {
	assert(pVec.length > 0);
	Random rnd = Randomizer.getInstance();
	if (pVec.length == 1) {
	    return 0;
	}
	double totalSum = computeSum(pVec);
	assert(totalSum > 0.0);
	double y = rnd.nextDouble() * totalSum;
	double sum = 0.0;
	int result = 0;
	boolean found = false;
	for (int i = 0; i < pVec.length; ++i) {
	    assert(pVec[i] >= 0.0);
	    if ((y >= sum) && (y <= (sum + pVec[i]))) {
		result = i;
		found = true;
		break;
	    }
	    sum += pVec[i];
	}
	assert(found);
	assert(result < pVec.length);
	return result;
    }


    /** writes 1D array in square format */
    public static void writeArray(PrintStream ps, double[] ary) {
	ps.print("" + ary.length + " ");
	for (int i = 0; i < ary.length; ++i) {
	    ps.print(" " + ary[i]);
	}
	ps.println("");
    }

    private static double[] parseVector(String line) throws ParsingException {
	String[] words = line.trim().split(" ");
	double[] result = new double[words.length];
	for (int i = 0; i < result.length; ++i) {
	    try {
		result[i] = Double.parseDouble(words[i]);
	    }
	    catch (NumberFormatException nfe) {
		throw new ParsingException("Error parsing word " + (i+1) + " " + nfe.getMessage());
	    }
	}
	return result;
    }

    public static double[][] readMatrix(InputStream is) throws IOException, ParsingException {
	String[] lines = StringTools.readAllLines(is);
	double[][] result = new double[lines.length][];
	for (int i =0; i < result.length; ++i) {
	    double[] v = parseVector(lines[i]);
	    result[i] = v;
	}
	return result;
    }

    /** writes 2D array in square format */
    public static void writeMatrix(PrintStream ps, double[][] ary) {
	for (int i = 0; i < ary.length; ++i) {
	    for (int j = 0; j < ary[i].length; ++j) {
		ps.print(""+ary[i][j] + " ");
	    }
	    ps.println("");
	}

    }

    /** computes transpose of matrix */
    public static double[][] transpose(double[][] mtx) {
	int rows = mtx.length;
	int cols = mtx[0].length;
	double[][] result = new double[cols][rows];
	for (int i = 0; i < rows; ++i) {
	    for (int j = 0; j < cols; ++j) {
		result[j][i] = mtx[i][j];
	    }
	}
	return result;
    }

    /** returns sum of array */
    public static double computeSum(double[] ary) {
	double result = 0; 
	for (int i = 0; i < ary.length; ++i) {
	    result += ary[i];
	}
	return result;
    }

    /** returns n'th column of matrix */
    public static double[] getColumn(double[][] matrix, int n) {
	double[] col = new double[matrix.length];
	for (int i = 0; i < col.length; ++i) {
	    col[i] = matrix[i][n];
	}
	return col;
    }

    /** returns index corresponding to highest element value */
    public static int findHighestElement(double[] ary) {
	assert(ary.length > 0);
	int bestId = 0;
	for (int i = 1; i < ary.length; ++i) {
	    if (ary[i] > ary[bestId]) {
		bestId = i;
	    }
	}
	assert(bestId < ary.length);
	return bestId;
    }

    /** Returns score describing overlap between two sorted lists. Is zero if lowest from highScores is higher than highest from low scores. */
    public static double scoreOverlap(List<Double> highScores, List<Double> lowScores) {
	assert(highScores != null);
	assert(highScores.size() > 0);
	assert(lowScores != null);
	assert(lowScores.size() > 0);
	Collections.sort(highScores);
	Collections.sort(lowScores);
	double lowestFromHigh = highScores.get(0);
	double highestFromLow = lowScores.get(lowScores.size()-1);
	double result = 0.0;
	for (int i = 0; i < highScores.size(); ++i) {
	    if (highScores.get(i) < highestFromLow) {
		result += highestFromLow - highScores.get(i);
	    } else {
		break; // is sorted, subsequent values will not fulfill the inequality either
	    }
	}
	for (int i = lowScores.size() - 1; i >= 0; --i) {
	    if (lowScores.get(i) > lowestFromHigh) {
		result += lowScores.get(i) - lowestFromHigh; 
	    } else {
		break; // is sorted, subsequent values will not fulfill the inequality either
	    }
	}
	return result;	
    }

    @Test(groups={"new"})
    public void testScoreOverlap() {
	List<Double> highScores = new ArrayList<Double>();
	List<Double> lowScores = new ArrayList<Double>();
	List<Double> vlowScores = new ArrayList<Double>();
	highScores.add(0.0);
	highScores.add(1.0);
	highScores.add(2.0);
	highScores.add(3.0);
	highScores.add(4.0);

	lowScores.add(2.0);
	lowScores.add(1.0);
	lowScores.add(0.0);
	lowScores.add(-1.0);
	lowScores.add(-2.0);

	vlowScores.add(0.0);
	vlowScores.add(-1.0);
	vlowScores.add(-2.0);
	vlowScores.add(-3.0);
	vlowScores.add(-3.0);
	// should yield: 2 + 1 + 2 + 1 = 6
	assert(Math.abs(scoreOverlap(highScores, lowScores) - 6.0) < 0.01);
	assert(Math.abs(scoreOverlap(highScores, vlowScores)) < 0.01);
    }

}
