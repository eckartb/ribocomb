package numerictools;

public class DoubleTools {

    public static final double REASONABLE_LIMIT = 1E10;

    public static boolean isReasonable(double d) {
	return isValid(d) && (Math.abs(d) < REASONABLE_LIMIT);
    }

    public static boolean isValid(double d) {
	return ! (Double.isNaN(d) || Double.isInfinite(d));
    }

    public static boolean isValidAndPositive(double d) {
	return isValid(d) && (d > 0.0);
    }

    public static boolean isValidAndNotNegative(double d) {
	return isValid(d) && (d >= 0.0);
    }

}
