package numerictools;

import java.io.PrintStream;

/** collection of static methods that work with datatype double[][] */
public class IntegerArrayTools {

    /** Generates cloned deep copy */
    public static int[] clone(int[] ary) {
	int[] result = new int[ary.length];
	for (int i = 0; i < ary.length; ++i) {
	    result[i] = ary[i];
	}
	return result;
    }

    /** Returns true iff all elements of ary1 are identical to the corresponding elements in ary2 */
    public static boolean equals(int[] ary1, int[] ary2) {
	if ((ary1 == null) && (ary2 == null)) {
	    return true;
	}
	if ((ary1 == null) || (ary2 == null)) {
	    return false; // only one is null;
	}
	if (ary1.length != ary2.length) {
	    return false;
	}
	for (int i = 0; i < ary1.length; ++i) {
	    if (ary1[i] != ary2[i]) {
		return false;
	    }
	}
	return true;
    }

    /** writes 1D array in square format */
    public static void writeArray(PrintStream ps, int[] ary) {
	ps.print("" + ary.length + " ");
	for (int i = 0; i < ary.length; ++i) {
	    ps.print(" " + ary[i]);
	}
	ps.println("");
    }

    /** writes 2D array in square format */
    public static void writeMatrix(PrintStream ps, int[][] ary) {
	for (int i = 0; i < ary.length; ++i) {
	    for (int j = 0; j < ary[i].length; ++j) {
		ps.print(""+ary[i][j] + " ");
	    }
	    ps.println("");
	}

    }

    /** Computes the transpose of an integer matrix */
    public static int[][] transpose(int[][] matrix) {
	int[][] result = new int[matrix[0].length][matrix.length];
	for (int i = 0; i < matrix.length; ++i) {
	    for (int j = 0; j < matrix[0].length; ++j) {
		result[j][i] = matrix[i][j];
	    }
	}
	return result;
    }

    /** Sets all values of integer matrix */
    public static void fill(int[][] matrix, int value) {
	for (int i = 0; i < matrix.length; ++i) {
	    for (int j = 0; j < matrix[0].length; ++j) {
		matrix[i][j] = value;
	    }
	}
    }

    /** Returns the submatrix of an integer matrix */
    public static int[][] subMatrix(int[][] matrix, int rowMin, int colMin, int rowMax,  int colMax) {
	assert rowMin >= 0 && rowMax >= rowMin;
	assert colMin >= 0 && colMax >= colMin;
	assert rowMax <= matrix.length;
	assert colMax <= matrix[0].length;
	int rowLen = rowMax - rowMin;
	int colLen = colMax - colMin;
	assert rowLen >= 0;
	assert colLen >= 0;
	int[][] result = new int[rowLen][colLen];
	for (int i = 0; i < rowLen; ++i) {
		assert i + rowMin < rowMax;
	    for (int j = 0; j < colLen; ++j) {
		assert j + colMin < colMax;
		result[i][j] = matrix[i + rowMin][j + colMin];
	    }
	}
	return result;
    }

}
