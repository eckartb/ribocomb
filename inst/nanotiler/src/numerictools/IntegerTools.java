package numerictools;

import java.util.HashSet;
import java.util.Iterator;

public class IntegerTools {

    /** returns factorial of value */
    public static long factorial(long n) {
	if (n <= 1) {
	    return 1;
	}
	return n * factorial(n-1);
    }

    /** Returns indices to maximum numbers */
    public static int[] whichMax(int[] v) {

	int maxSoFar = v[0];
	for (int i = 0; i < v.length;++i) {
	    if (v[i] >= maxSoFar) {
		maxSoFar = v[i];
	    }
	}
	HashSet<Integer> ids = new HashSet<Integer>();
	for (int i = 0; i < v.length;++i) {
	    if (v[i] >= maxSoFar) {
		ids.add(i);
	    }
	}
	int[] result = new int[ids.size()];
	Iterator<Integer> it = ids.iterator();
	int pc = 0;
	while (it.hasNext()) {
	    assert pc < result.length;
	    result[pc++] = it.next();
	}
	return result;
    }

}
