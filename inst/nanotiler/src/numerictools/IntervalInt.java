// --*- C++ -*------x---------------------------------------------------------
// $Id:
//
// Class:           IntervallInt
// 
// Base class:      -
//
// Derived classes: - 
//
// Author:          Eckart Bindewald
//
// Project name:    -
//
// Date:            06/2004
//
// Description:     Implements concepts of intervall.
//                  Upper and lower bounds are integers.
// 
// Reviewed by:     -
// -----------------x-------------------x-------------------x-----------------

package numerictools;

/** Intervall class, stores upper and lower limits (both are inclusive)
    @author Eckart Bindewald
    @see    -
    @review - */
public class IntervalInt 
{

    private int lower;
    
    private int upper;
    
    /** default: start with empty interval */
    public IntervalInt() {
	lower = 0;
	upper = -1; 
    }
    
    public IntervalInt(int _lower, int _upper) {
	this.lower = _lower;
	this.upper = _upper;
    }
    
    public IntervalInt(IntervalInt orig) { 
	copy(orig); 
    }
    
    public int getLength()  { 
	if (!isValid()) {
	    return 0;
	}
	return getUpper() - getLower() + 1; 
    }
    
    public int getLower()  { return lower; }
    
    public int getUpper()  { return upper; }
    
    public double getMiddle()  { return 0.5 * (getLower() + getUpper()); }
    
    /** returns true if intervalls are overlapping */
    public boolean isOverlapping( IntervalInt other) {
	if ((getUpper() < other.getLower())
	    || (other.getUpper() < getLower())) {
	    return false;
	}
	return true;
    }
    
    public boolean isValid() { return getUpper() >= getLower(); }
	
    /** precondition: must be overlapping in the first place! */
    public void consolidate( IntervalInt other) {
	// assert(isOverlapping(other));
	if (other.getLower() < lower) {
	    lower = other.getLower();
	}
	if (other.getUpper() > upper) {
	    upper = other.getUpper();
	}
    }

    /* TODO !
       inline
       IntervalInt intersection( IntervalInt intervallA, 
       IntervalInt intervallB)
       {
       IntervalInt result(intervallA.getLower(), intervallA.getLower());
       if (!intervallIsOverlapping(intervallB)) {
       return result; // return empty intervall
       }
       // todo
       }
    */


  /* MODIFIERS */
    
    public void setLower(int n) { this.lower = n; }
    
    public void setUpper(int m) { this.upper = m; }
    
    public void copy( IntervalInt other) { 
	lower = other.lower;
	upper = other.upper;
    }

    public String toString() { 
	return ("" + getLower() + " - " + getUpper());
    }
    
}



