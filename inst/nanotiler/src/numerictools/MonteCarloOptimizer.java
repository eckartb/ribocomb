package numerictools;

import java.util.Random;
import generaltools.Randomizer;
import java.util.logging.Logger;

public class MonteCarloOptimizer implements PotentialNDOptimizer {

    private int iterMax = 10000;
    private double stepWidth = 1.0;
    private double kt = 10.0;
    private boolean minimize = true;
    private Random rand = Randomizer.getInstance();
    public int verboseLevel = 1;
    private Logger log = Logger.getLogger("NanoTiler_debug");
    
    /** returns random term [-stepWidth,stepWidth[ added to x */
    double computeRandomPosition(double x) {
	return x + (2.0*(rand.nextDouble()-0.5) * stepWidth);
    }

    /** modifies vector with random component */
    double[] generateNewPosition(double[] position) {
	double[] newPos = new double[position.length];
	for (int i = 0; i < position.length; ++i) {
	    newPos[i] = computeRandomPosition(position[i]);
	}
	return newPos;
    }

    /** Return Temperature * Boltzmann Constant kt (= fluctuation energy) */
    public double getKt() { return this.kt; }

    /** returns verbose level (0: silent, 1: normal, 2: verbose) */
    public int getVerboseLevel() { return verboseLevel; }

    /** sets maximum number of iterations */
    public void setIterMax(int n) { this.iterMax = n; }

    /** Sets Temperature * Boltzmann Constant kt (= fluctuation energy) */
    public void setKt(double kt) { this.kt = kt; }

    /** sets step width of random step */
    public void setStepWidth(double x) { this.stepWidth = x; }

    /** sets verbose level (0: silent, 1: normal, 2: verbose) */
    public void setVerboseLevel(int level) { this.verboseLevel = level; }

    /** starts optimization */
    public OptimizationNDResult optimize(PotentialND potential, double[] startPos) {
	int dim = startPos.length;
	SimpleOptimizationNDResult result = new SimpleOptimizationNDResult(dim);
	result.bestValue = potential.getValue(startPos);
	result.bestPosition = startPos;
	double[] currentPos = new double[startPos.length];
	System.arraycopy(startPos, 0, currentPos, 0, dim); // copy array
	double currentVal = result.bestValue;
	if (verboseLevel == 1) {
	    log.fine("Starting optimization at value and position: " + result.bestValue);
	    // DoubleArrayTools.writeArray(System.out, startPos);
	}
	else if (verboseLevel > 1) {
	    log.info("Starting optimization at value: " + result.bestValue);
	    DoubleArrayTools.writeArray(System.out, startPos);
	}

	for (int iter = 0; iter < iterMax; ++iter) {
	    // compute new position:
	    double[] newPosition = generateNewPosition(currentPos);
	    double newVal = potential.getValue(newPosition);
	    if ((minimize && (newVal < result.bestValue)) || ((!minimize) && (newVal > result.bestValue))) {
		result.bestValue = newVal;
		result.bestPosition = newPosition;
		result.stepCount = iter;
		if (verboseLevel > 1) {
			log.fine("New best value and position: " + newVal);
			DoubleArrayTools.writeArray(System.out, newPosition);
			log.fine("Iteration: " + iter + " currently best: " + result);
		}
	    }
	    double delta = newVal - currentVal;
	    if (!minimize) {
		delta = - delta;
	    }
	    if (Math.exp(-(delta)/kt) > rand.nextDouble()) {
		// accept step
		System.arraycopy(newPosition, 0, currentPos, 0, dim); // copy array
		currentVal = newVal;
	    }
	}

	if (verboseLevel > 0) {
	    log.fine("Finishing optimization at value and position: " + result.bestValue);
	    DoubleArrayTools.writeArray(System.out, result.bestPosition);
	}

	return result;
    }


}
