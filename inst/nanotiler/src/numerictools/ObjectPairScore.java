package numerictools;

/** Class for rank-ordeing any objects */
public class ObjectPairScore implements Comparable<Scorable>, Scorable {

    private double score;
    private Object object1;
    private Object object2;
    
    public ObjectPairScore(double score, Object object1, Object object2) {
	this.score = score;
	this.object1 = object1;
	this.object2 = object2;
    }
    
    public double getScore() { return score; }
    
    public Object getObject1() { return object1; }

    public Object getObject2() { return object2; }
        
    public int compareTo(Scorable other) { 
	// assert other instanceof Scorable;
	double otherScore = other.getScore();
	if (score < otherScore) {
	    return -1;
	}
	else if (score > otherScore) {
	    return 1;
	}
	return 0;
    }

    public String toString() { return "" + score + " " + object1.toString() + " " + object2.toString(); }
    
}

