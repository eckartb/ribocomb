package numerictools;

/** Class for rank-ordeing any objects */
public class ObjectScore implements Comparable, Scorable {

    private double score = 0.0;
    private Object object;
    
    public ObjectScore(double score, Object object) {
	this.score = score;
	this.object = object;
    }
    
    public double getScore() { return score; }
    
    public Object getObject() { return object; }
    
    public void setObject(Object o) { this.object = o; }
    
    public int compareTo(Object other) { 
	assert other instanceof Scorable;
	double otherScore = ((Scorable)other).getScore();
	if (score < otherScore) {
	    return -1;
	}
	else if (score > otherScore) {
	    return 1;
	}
	return 0;
    }
    
    
}

