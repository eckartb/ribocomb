package numerictools;

public interface OptimizationNDResult {

    double[] getBestPosition();

    double getBestValue();

    double getError();

    double getStepCount();
    

}
