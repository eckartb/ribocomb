package numerictools;

public class PolarTools {

    /* Convert x and y to polar phi angle. result is between 0 and 2PI.
    * TODO : verify !!!*/
    public static double phiFromXY(double x, double y)
    {
	if (y == 0.0) {
	    if (x >= 0.0) {
		return 0.0;
	    }
	    else {
		return Math.PI;
	    }
	}
	// programming bug !?? was atan(fabs(x) / fabs(y)) EB 2006
	double phi = Math.atan(Math.abs(y) / Math.abs(x)); // treat as if quadrant I
	// phi is now in [0.. Math.PI/2]
	// choose correct quadrant
	if (x < 0.0) { // quadrant II or III
	    if (y < 0.0) { // quadrant III
		phi += Math.PI;
	    }
	    else { // quadrant II
		phi = Math.PI - phi;
	    }
	}
	else if (y < 0.0) { // quadrant IV
	    phi = (2 * Math.PI) - phi;
	}
	assert((phi >= 0.0) && (phi <= 2*Math.PI));
	return phi;
    }

}
