package numerictools;

/** potential in n dimensions */
public interface PotentialND {

    double[] generateLowPosition();

    double[] generateHighPosition();

    int getDimension();

    double getValue(double[] position);
}
