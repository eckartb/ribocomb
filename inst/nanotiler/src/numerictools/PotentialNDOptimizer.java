package numerictools;

public interface PotentialNDOptimizer {

    OptimizationNDResult optimize(PotentialND potential, double[] startPos);

    int getVerboseLevel();

    void setVerboseLevel(int level);

}
