package numerictools;

public interface Scorable {

    double getScore();
    
}
