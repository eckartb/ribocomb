package numerictools;

import java.util.Random;
import java.util.logging.Logger;
import generaltools.Randomizer;
public class SimulatedAnnealingOptimizer implements PotentialNDOptimizer {

    private int iterMax = 1000;
    private double stepWidth = 1.0;
    private boolean minimize = true;
    private Random rand = Randomizer.getInstance();
    public int verboseLevel = 1;
    private Logger log = Logger.getLogger("NanoTiler_debug");
    public double decay = 0.999;
    public double[] stepScalings = null;

    /** returns random term [-stepWidth,stepWidth[ added to x */
    double computeRandomPosition(double x, double step) {
	return x + (rand.nextGaussian() * step);
    }

    /** modifies vector with random component */
    double[] generateNewPosition(double[] position) {
	double[] newPos = new double[position.length];
	double step;
	for (int i = 0; i < position.length; ++i) {
	    step = stepWidth;
	    if (stepScalings != null) {
		step *= stepScalings[i];
	    }
	    newPos[i] = computeRandomPosition(position[i], step);
	}
	return newPos;
    }

    /** returns verbose level (0: silent, 1: normal, 2: verbose) */
    public int getVerboseLevel() { return verboseLevel; }

    /** sets maximum number of iterations */
    public void setIterMax(int n) { this.iterMax = n; }

    /** sets step width of random step */
    public void setStepWidth(double x) { this.stepWidth = x; }

    /** sets verbose level (0: silent, 1: normal, 2: verbose) */
    public void setVerboseLevel(int level) { this.verboseLevel = level; }

    /** starts optimization */
    public OptimizationNDResult optimize(PotentialND potential, double[] startPos) {
	int dim = startPos.length;
	SimpleOptimizationNDResult result = new SimpleOptimizationNDResult(dim);
	result.bestValue = potential.getValue(startPos);
	result.bestPosition = startPos;
	if (verboseLevel > 0) {
	    log.fine("Starting Simulated annealing optimization at value and position: " + result.bestValue);
	    //	    DoubleArrayTools.writeArray(System.out, startPos); //TODO: Change to different printstream: makes too many print statements that mean nothing
	}
	for (int iter = 0; iter < iterMax; ++iter) {
	    // compute new position:
	    double[] newPosition = generateNewPosition(result.bestPosition);
	    double newVal = potential.getValue(newPosition);
	    if (minimize) {
		if (newVal < result.bestValue) {
		    result.bestValue = newVal;
		    result.bestPosition = newPosition;
		    result.stepCount = iter;
		    if (verboseLevel > 1) {
			log.fine("New best value and position: " + newVal);
			//			DoubleArrayTools.writeArray(System.out, newPosition); //TODO: Change to different printstream: makes too many print statements that mean nothing
			log.fine("Iteration: " + iter + " currently best: " + result);
		    }
		}
	    }
	    else {
		if (newVal > result.bestValue) {
		    result.bestValue = newVal;
		    result.bestPosition = newPosition;
		    result.stepCount = iter;
		    if (verboseLevel > 1) {
			log.fine("New best value and position: " + newVal);
			//			DoubleArrayTools.writeArray(System.out, newPosition); //TODO: Change to different printstream: makes too many print statements that mean nothing
			log.fine("Iteration: " + iter + " currently best: " + result);
		    }
		}
	    }
	    stepWidth *= decay;
	}
	if (verboseLevel > 0) {
	    log.fine("SimulatedAnnealingOptimizer: Finishing optimization at value and position: " + result.bestValue);
	    //	    DoubleArrayTools.writeArray(System.out, result.bestPosition); //TODO: Change to different printstream: makes too many print statements that mean nothing.
	}

	return result;
    }


}
