package rnadesign.designapp;

import java.io.PrintStream;

import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import rnasecondary.RnaInteractionType;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class AddBasePairCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "genbpconstraint";

    private Object3DGraphController controller;
    private String name1;
    private String name2;
    private String interactionName;
    private int length = 1;
    private int symId1 = 0;
    private int symId2 = 0;

    public AddBasePairCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	AddBasePairCommand command = new AddBasePairCommand(this.controller);
	command.name1 = this.name1;
	command.name2 = this.name2;
	command.interactionName = this.interactionName;
	command.length = this.length;
	command.symId1 = this.symId1;
	command.symId2 = this.symId2;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " nucleotide1 nucleotide2 [wc [length]]";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " ORIGINALNAME nucleotide1 nucleotide2 [length]" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Adds a base pair between two residues." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	try {
	    int interactionId = RnaInteractionType.WATSON_CRICK; // TODO : use interactionName
	    for (int offset = 0; offset < length; ++offset) {
		controller.addBasePair(name1, name2, interactionId, offset, symId1, symId2);
	    }
	}
	catch (Object3DGraphControllerException e) {
	    throw new CommandExecutionException(e.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if ((getParameterCount() < 2) || (getParameterCount() > 4)) {
	    throw new CommandExecutionException(helpOutput());
	}
	StringParameter p0 = (StringParameter)(getParameter(0));
	name1 = p0.getValue();
	StringParameter p1 = (StringParameter)(getParameter(1));
	name2 = p1.getValue();
// 	if (getParameterCount() > 2) {
// 	    StringParameter p2 = (StringParameter)(getParameter(2));
// 	    interactionName = p2.getValue();
// 	}
	if (getParameterCount() > 2) {
	    StringParameter p2 = (StringParameter)(getParameter(2));
	    length = Integer.parseInt(p2.getValue());
	}
// 	if (interactionName.indexOf(".") >= 0) { // TODO : check for "." characters
// 	    throw new CommandExecutionException("No . character allowed in individual object name.");
// 	}
	try {
	    StringParameter p = (StringParameter)(getParameter("sym1"));
	    if (p != null) {
		symId1 = Integer.parseInt(p.getValue());
	    }
	    p = (StringParameter)(getParameter("sym2"));
	    if (p != null)  {
		symId2 = Integer.parseInt(p.getValue());
	    }
	}
	catch (NumberFormatException nfe) {
	    throw new CommandExecutionException(nfe.getMessage());
	}

    }

}
