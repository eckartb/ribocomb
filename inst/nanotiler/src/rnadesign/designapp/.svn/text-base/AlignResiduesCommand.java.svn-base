package rnadesign.designapp;

import java.util.*;

import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DSet;
import tools3d.objects3d.SimpleObject3DSet;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import rnadesign.rnacontrol.Mutation;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import sequence.UnknownSymbolException;
import generaltools.StringTools;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class AlignResiduesCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "align";

    private Object3DGraphController controller;
    private String residueDescriptor1;
    private String residueDescriptor2;
    private Object3DSet objectBlocks1;
    private Object3DSet objectBlocks2;
    private int numberSteps=1000000;
    private String[] atomNames = {"C4*", "C1*", "O3*" };

    public AlignResiduesCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	AlignResiduesCommand command = new AlignResiduesCommand(this.controller);
	command.controller = this.controller;
	command.numberSteps = this.numberSteps;
	command.objectBlocks1 = this.objectBlocks1;
	command.objectBlocks2 = this.objectBlocks2;
	command.residueDescriptor1 = this.residueDescriptor1;
	command.residueDescriptor2 = this.residueDescriptor2;
	command.atomNames = this.atomNames;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String synopsis() { return "" + COMMAND_NAME 
				    + " RES-DESCRIPTOR1 RES-DESCRIPTOR2 ROOT1 ROOT2 . Example: align A:3-8 B:1-6 root.structure1 root.structure2 [steps=NUMBER]";
    }

    private String helpOutput() {
	return "Aligns second set of specified residues onto first set of residues. Correct usage: " + synopsis();
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + synopsis() + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Aligns two sets of residues such that the root mean square deviation is minimal" + NEWLINE + NEWLINE;
	helpText += "OPTIONS" + NEWLINE;
	return helpText;
    }

    /** Returns true if of type A24,C36 ; returns false if of type ACGUUGUGA */
    private static boolean isSinglePositionString(String s) {
	for (int i = 0; i < s.length(); ++i) {
	    if (Character.isDigit(s.charAt(i))) {
		return true;
	    }
	}
	return false;
    }

    /** parses string of form name1,name2,...;name3,name4,... into List of object sets */
    Object3DSet parseObjectBlocks(String blocks) throws CommandExecutionException {
	if ((blocks == null) || (blocks.length() == 0)) {
	    throw new CommandExecutionException("blocks option has to be defined! See usage.");
	}
	List<Object3DSet> result = new ArrayList<Object3DSet>();
	String setWords[] = blocks.split(";");
	for (int i = 0; i < setWords.length; ++i) {
	    String objWords[] = setWords[i].split(",");
	    if (objWords.length < 1) {
		throw new CommandExecutionException("Error in optbasepairs: expected at least one object in " + setWords[i]);
	    }
	    Object3DSet set = new SimpleObject3DSet();
	    for (int j = 0; j < objWords.length; ++j) {
		String objName = objWords[j];
		Object3D obj = controller.getGraph().findByFullName(objName);
		if (obj == null) {
		    throw new CommandExecutionException("Could not find object with name: " + objName);
		}
		set.add(obj);
	    }
	    result.add(set);
	}
	if (result.size() != 1) {
	    throw new CommandExecutionException("Expected comma separated list of tree nodes as one object block!");
	}
	return result.get(0);
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	if ((objectBlocks1 == null) || (objectBlocks1.size() == 0) ) {
	    throw new CommandExecutionException("No object blocks (1) defined!");
	}
	if ((objectBlocks2 == null) || (objectBlocks2.size() == 0) ) {
	    throw new CommandExecutionException("No object blocks (2) defined!");
	}
	try {
	    System.out.println("Aligning residues with atoms: " + StringTools.paste(atomNames, ","));
	    resultProperties = controller.alignStructures(objectBlocks1, objectBlocks2, residueDescriptor1, residueDescriptor2, atomNames,
							  numberSteps);
	    System.out.println(resultProperties);
	}
	catch (Object3DGraphControllerException e) {
	    throw new CommandExecutionException(e.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	int numPar = getParameterCount();
	if (numPar < 4) {
	    throw new CommandExecutionException("Insufficient number of parameters: " + helpOutput());
	}
	this.residueDescriptor1 = ((StringParameter)(getParameter(0))).getValue();
	this.residueDescriptor2 = ((StringParameter)(getParameter(1))).getValue();
	this.objectBlocks1 = parseObjectBlocks(((StringParameter)(getParameter(2))).getValue());
	this.objectBlocks2 = parseObjectBlocks(((StringParameter)(getParameter(3))).getValue());
	StringParameter p = (StringParameter)(getParameter("steps"));
	if (p != null) {
	    try {
		this.numberSteps = Integer.parseInt(p.getValue());
	    } catch ( NumberFormatException nfe) {
		throw new CommandExecutionException("Error parsing number of stems in align command: " + nfe.getMessage());
	    }
	}

	p = (StringParameter)(getParameter("atoms"));
	if (p != null) {
	    this.atomNames = p.getValue().split(",");
	}
    }
}
    
