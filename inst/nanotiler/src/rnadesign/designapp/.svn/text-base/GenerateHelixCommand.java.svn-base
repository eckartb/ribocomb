package rnadesign.designapp;

import java.util.logging.Logger;
import java.util.Properties;
import java.io.PrintStream;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.IntParameter;
import commandtools.StringParameter;
import rnadesign.rnamodel.FitParameters;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import rnadesign.rnacontrol.Object3DGraphControllerConstants;
import rnadesign.rnacontrol.Object3DGraphControllerEventConstants;
import rnadesign.rnamodel.FittingException;
import rnadesign.rnamodel.HelixParameters;
import controltools.*;
import rnadesign.rnamodel.HelixOptimizer;
import tools3d.*;
import tools3d.objects3d.CoordinateSystem3D;

import static rnadesign.designapp.PackageConstants.*;

public class GenerateHelixCommand extends AbstractCommand {

    private static Logger log = Logger.getLogger(LOGGER_NAME);
    
    public static final String COMMAND_NAME = "genhelix";

    private String helixRootName = "root";

    private String name1;
    private String name2;
    private boolean propagate = true;
    private String bases="GC";

    // determines which algorithm is used for optimization
    // private int optimizerAlgorithm = Object3DGraphController.SEQUENCE_OPTIMIZER_SCRIPT;

    private int iterMax = 10000; // number of optimization steps for fitting helix

    private double rmsLimit = 5.0; // rms limit for placement of mutated residue

    private double angleLimit = Math.PI / 4;
    
    private int numBasePairs = 10; // if smaller zero: estimate best value

    private Object3DGraphController controller;
    
    private Vector3D position = new Vector3D(0.0,0.0,0.0);
    private Vector3D vx = new Vector3D(1.0,0.0,0.0);
    private Vector3D vy = new Vector3D(0.0,1.0,0.0);
    private String name = "helix";
    private HelixParameters helixParameters = new HelixParameters();

    public GenerateHelixCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	GenerateHelixCommand command = new GenerateHelixCommand(this.controller);
	command.name1 = this.name1;
	command.name2 = this.name2;
	command.name = this.name;
	command.helixRootName = this.helixRootName;
	command.rmsLimit = this.rmsLimit;
	command.angleLimit = this.angleLimit;
        command.propagate = this.propagate;
	command.helixParameters = new HelixParameters(this.helixParameters);
	return command;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	assert (this.controller != null);
	prepareReadout();
	FitParameters stemFitParameters = new FitParameters(rmsLimit, angleLimit);
	stemFitParameters.setIterMax(this.iterMax);
	Properties properties = new Properties();
	try {
	    char c1 = bases.charAt(0);
	    char c2 = bases.charAt(1);
	    if ((name1 != null) && (name1.length() > 0)) {
		if ((name2 != null) && (name2.length() > 0)) {
		    if (getParameter("bp") == null) {
			numBasePairs = -1; // if no number of base pairs specified: estimate best number
		    }
		    controller.generateHelix(name1, name2, c1, c2, stemFitParameters, helixRootName, numBasePairs, name);
		}
		else { 
		    controller.generateHelix(name1, c1, c2, helixRootName, numBasePairs, name, propagate);
		}
	    }
	    else {
		CoordinateSystem3D cs = new CoordinateSystem3D(position, vx, vy);
		controller.generateIsolatedHelix(cs, helixParameters, c1, c2, helixRootName, name,numBasePairs);
	    }
	}
	catch (FittingException fe) {
	    throw new CommandExecutionException("Bad helix fit! " + fe.getMessage());
	}
	catch (Object3DGraphControllerException gce) {
	    throw new CommandExecutionException(gce.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private boolean checkBase(char c) {
	switch (c) {
	case 'A': return true;
	case 'C': return true;
	case 'G': return true;
	case 'T': return true;
	case 'U': return true;
	case 'N': return false; // currently not allowed
	case 'X': return false;
	}
	return false;
    }

    /** Returns true if bases is of the form AU, GC or the like */
    private boolean checkBases(String bases) {
	if ((bases == null) || (bases.length() != 2)) {
	    return false;
	}
	return (checkBase(bases.charAt(0)) && checkBase(bases.charAt(1)));
    }

    private CoordinateSystem parseDeformation(String def) throws CommandExecutionException, NumberFormatException {
	String[] words = def.split(",");
	if ((words.length != 9) && (words.length != 3)) {
	    throw new CommandExecutionException("Error parsing deformation matrix parameters! Format: def=px,py,pz or def=px,py,pz,yx,yy,yz,zx,zy,zz");
	}
	Vector3D ex = new Vector3D(1.0,0.0,0.0);
	Vector3D ey = new Vector3D(0.0,1.0,0.0);
	Vector3D pos = new Vector3D(0.0,0.0,0.0);
	pos.setX(Double.parseDouble(words[0]));
	pos.setY(Double.parseDouble(words[1]));
	pos.setZ(Double.parseDouble(words[2]));
	if (words.length == 9) {
	    ex.setX(Double.parseDouble(words[3]));
	    ex.setY(Double.parseDouble(words[4]));
	    ex.setZ(Double.parseDouble(words[5]));
	    ey.setX(Double.parseDouble(words[6]));
	    ey.setY(Double.parseDouble(words[7]));
	    ey.setZ(Double.parseDouble(words[8]));
	}
	return new CoordinateSystem3D(pos, ex, ey);
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameter("bp1") != null) { // kept for backwards compatibility; should be bd1
	    name1 = ((StringParameter)getParameter("bp1")).getValue();
	}
	if (getParameter("bp2") != null) { // kept for backwards compatibility; should be bd2
	    name2 = ((StringParameter)getParameter("bp2")).getValue();
	}
	if (getParameter("bd1") != null) {
	    name1 = ((StringParameter)getParameter("bd1")).getValue();
	}
	if (getParameter("bd2") != null) {
	    name2 = ((StringParameter)getParameter("bd2")).getValue();
	}
	try {
	    Command bpParameter = getParameter("bp");
	    if (bpParameter != null) {
		numBasePairs = Integer.parseInt(((StringParameter)bpParameter).getValue());
	    }
	    Command rmsParameter = getParameter("rms");
	    if (rmsParameter != null) {
		rmsLimit = Double.parseDouble(((StringParameter)rmsParameter).getValue());
	    }
	    Command angleLimitParameter = getParameter("angle");
	    if (angleLimitParameter != null) {
		angleLimit = DEG2RAD*Double.parseDouble(((StringParameter)angleLimitParameter).getValue());
	    }
	    Command helixRootParameter = getParameter("root");
	    if (helixRootParameter != null) {
		helixRootName = ((StringParameter)(helixRootParameter)).getValue();
	    }
	    Command nameParameter = getParameter("name"); // a link can have a name!
	    if (nameParameter != null) {
		name = ((StringParameter)(nameParameter)).getValue();
	    }
	    Command defParameter = getParameter("def");
	    if (defParameter != null) {
		String defString = ((StringParameter)(defParameter)).getValue();
		helixParameters.setDeformation(parseDeformation(defString));
	    }
	    Command basesParameter = getParameter("bases");
	    if (basesParameter != null) {
		bases = ((StringParameter)(basesParameter)).getValue();
		if (!checkBases(bases)) {
		    throw new CommandExecutionException("Illegel base name definition. Must be to characters out of A,C,G,U. Typical example: bases=AU or bases=GC");
		}
	    }
	}
	catch(NumberFormatException nfe) {
	    throw new CommandExecutionException("Bad number format detected: " + nfe.getMessage());
	}
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + helpOutput() + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE + 
	    "    " + helpOutput() + NEWLINE + NEWLINE;
	helpText += "OPTIONS" + NEWLINE;
	return helpText;
    }

   private String helpOutput() {
       return "Generates helix between one, two or zero helix descriptors (\"BranchDescriptor3D\"). Correct usage: " + COMMAND_NAME + " [bd1=helixdescriptor1 [bd2=helixdescriptor2] [rms=value] [angle=value] [root=name] [bp=number] [def=px,py,pz[,xx,xy,xz,yx,yy,yz]]";
    }


}
    
