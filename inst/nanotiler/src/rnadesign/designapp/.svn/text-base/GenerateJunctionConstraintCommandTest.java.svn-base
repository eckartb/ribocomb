package rnadesign.designapp;

import org.testng.*;
import org.testng.annotations.*;
import generaltools.TestTools;
import commandtools.*;

public class GenerateJunctionConstraintCommandTest {

    @Test(groups={"new"})
    public void testGenerateJunctionConstraintCommandTwoHelices() {
	NanoTilerScripter app = new NanoTilerScripter();
	String methodName = "testGenerateJunctionConstraintCommandTwoHelices";
	System.out.println(TestTools.generateMethodHeader(methodName));
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("set distMin 2.5");
	    app.runScriptLine("set distMax 3");
	    app.runScriptLine("synth simple square root 0 0 0");
	    app.runScriptLine("genhelix bp=12 root=root.square name=helix0");
	    app.runScriptLine("genhelix bp=12 root=root.square name=helix1");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("echo Links before genjunctionconstraint " + methodName);
	    app.runScriptLine("links");
	    app.runScriptLine("genjunctionconstraint ends=root.square.helix0_root.helix0_forw.1,root.square.helix0_root.helix0_back.12,root.square.helix1_root.helix1_forw.1,root.square.helix1_root.helix1_back.12 min=${distMin} max=${distMax}");
	    app.runScriptLine("echo Links after genjunctionconstraint " + methodName);
	    app.runScriptLine("links");
	    app.runScriptLine("echo Still running " + methodName);
	    app.runScriptLine("optconstraints vdw=1 blocks=root.square.helix0_root;root.square.helix1_root");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + ".script");
	} catch (CommandException ce) {
	    System.out.println("Exception thrown in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new"})
    public void testGenerateJunctionConstraintCommandSquare() {
	NanoTilerScripter app = new NanoTilerScripter();
	String methodName = "testGenerateJunctionConstraintCommandSquare";
	System.out.println(TestTools.generateMethodHeader(methodName));
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("set distMin 3.0");
	    app.runScriptLine("set distMax 9.0");
	    app.runScriptLine("set bp 10");
	    app.runScriptLine("synth simple square root 0 0 0");
	    app.runScriptLine("genhelix bp=${bp} root=root.square name=helix0");
	    app.runScriptLine("genhelix bp=${bp} root=root.square name=helix1");
	    app.runScriptLine("genhelix bp=${bp} root=root.square name=helix2");
	    app.runScriptLine("genhelix bp=${bp} root=root.square name=helix3");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("echo Links before genjunctionconstraint " + methodName);
	    app.runScriptLine("links");
	    app.runScriptLine("genjunctionconstraint ends=root.square.helix0_root.helix0_forw.1,root.square.helix0_root.helix0_back.${bp},root.square.helix1_root.helix1_back.1,root.square.helix1_root.helix1_forw.${bp} min=${distMin} max=${distMax}");
	    app.runScriptLine("genjunctionconstraint ends=root.square.helix1_root.helix1_forw.1,root.square.helix1_root.helix1_back.${bp},root.square.helix2_root.helix2_back.1,root.square.helix2_root.helix2_forw.${bp} min=${distMin} max=${distMax}");
	    app.runScriptLine("genjunctionconstraint ends=root.square.helix2_root.helix2_forw.1,root.square.helix2_root.helix2_back.${bp},root.square.helix3_root.helix3_back.1,root.square.helix3_root.helix3_forw.${bp} min=${distMin} max=${distMax}");
	    app.runScriptLine("genjunctionconstraint ends=root.square.helix3_root.helix3_forw.1,root.square.helix3_root.helix3_back.${bp},root.square.helix0_root.helix0_back.1,root.square.helix0_root.helix0_forw.${bp} min=${distMin} max=${distMax}");
	    app.runScriptLine("echo Links after genjunctionconstraint " + methodName);
	    app.runScriptLine("links");
	    app.runScriptLine("echo Still running " + methodName);
	    app.runScriptLine("optconstraints vdw=1 blocks=root.square.helix0_root;root.square.helix1_root;root.square.helix2_root;root.square.helix3_root");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + ".script");
	} catch (CommandException ce) {
	    System.out.println("Exception thrown in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }
    
}
