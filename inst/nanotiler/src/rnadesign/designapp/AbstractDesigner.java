package rnadesign.designapp;

import java.util.logging.Logger;

import commandtools.Interpreter;
import commandtools.SimpleCommandApplication;
import rnadesign.rnacontrol.Object3DGraphController;

public class AbstractDesigner extends SimpleCommandApplication {

    protected Object3DGraphController graphController;

    protected AbstractDesigner(Object3DGraphController controller, Interpreter interpreter) {
	super(interpreter, Logger.getLogger("NanoTiler_debug"));
	this.graphController = controller;
    }

    public Object3DGraphController getGraphController() { 
	return graphController; 
    }

}
