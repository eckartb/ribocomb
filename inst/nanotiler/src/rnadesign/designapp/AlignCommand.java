package rnadesign.designapp;

import java.io.PrintStream;

import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import tools3d.*;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class AlignCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "alignxyz";

    private Object3DGraphController controller;
    private String name1;
    private String name2;
    private double x = 0;
    private double y = 0;
    private double z = 0;

    public AlignCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	AlignCommand command = new AlignCommand(this.controller);
	command.name1 = this.name1;
	command.name2 = this.name2;
	command.x = this.x;
	command.y = this.y;
	command.z = this.z;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " obj1-name obj2-name x y z";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " Object1Name Object2Name x y z" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Align command rotates the selected subtree such that objects 1 and 2 point in direction x,y,z." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	try {
	    if ((name2 != null) && (name2.length() > 2)) {
		controller.getGraph().alignObject(name1, name2, new Vector3D(x, y, z));
	    }
	    else {
		controller.getGraph().alignOrientation(name1);
	    }
	}
	catch (Object3DGraphControllerException e) {
	    throw new CommandExecutionException(e.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if ((getParameterCount() != 5) && (getParameterCount() != 1)) {
	    throw new CommandExecutionException(helpOutput());
	}
	try {
	    StringParameter p0 = (StringParameter)(getParameter(0));
	    name1 = p0.getValue();
	    if (getParameterCount() < 2) {
		return;
	    }
	    StringParameter p1 = (StringParameter)(getParameter(1));
	    name2 = p1.getValue();
	    StringParameter p2 = (StringParameter)(getParameter(2));
	    x = Double.parseDouble(p2.getValue());
	    StringParameter p3 = (StringParameter)(getParameter(3));
	    y = Double.parseDouble(p3.getValue());
	    StringParameter p4 = (StringParameter)(getParameter(4));
	    z = Double.parseDouble(p4.getValue());
	}
	catch (NumberFormatException nfe) {
	    throw new CommandExecutionException("Bad number syntax in command align : " 
						+ nfe.getMessage());
	}
    }

}
