package rnadesign.designapp;

import java.io.PrintStream;

import rnadesign.rnamodel.FittingException;
import rnadesign.rnacontrol.*;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import generaltools.ParsingException;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class BridgeAllCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "bridgeall";

    private Object3DGraphController controller;
    private double angleWeight = BridgeItController.DEFAULT_ANGLE_WEIGHT;
    private String origName;
    private String filePrefix = "bridge_";
    private String rootName = "root";
    private int solutionId = 10; // top n solutions
    private int helixAlgorithm = BridgeItController.SIMPLE_HELIX;
    private PrintStream ps;
    private double collisionDistance = 2.0;
    private double rms = 3.0;
    private int lenMax = 3;

    public BridgeAllCommand(PrintStream ps, Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.ps = ps;
	this.controller = controller;
    }

    public Object cloneDeep() {
	BridgeAllCommand command = new BridgeAllCommand(this.ps, this.controller);
	command.angleWeight = this.angleWeight;
	command.collisionDistance = this.collisionDistance;
	command.origName = this.origName;
	command.filePrefix = this.filePrefix;
	command.rootName = this.rootName;
	command.solutionId = this.solutionId;
	command.helixAlgorithm = this.helixAlgorithm;
	command.rms = this.rms;
	command.lenMax = this.lenMax;

	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " name1 [angle=WEIGHT] [coll=VALUE] [combined=false|true][l=NUMBER] [n=NUMBER] [prefix=NAME] [root=NAME] [rms=VALUE]";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " OBJECTNAME1 OBJECTNAME2 [angle=WEIGHT] [l=NUMBER] [n=NUMBER] [prefix=NAME] [root=NAME] [rms=VALUE]" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     bridgeall command attempts to find bridging fragments between all strands of a substree";
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	ps.println("Calling controller.bridgeIt with " + origName);
	try {
	    int numBridges = controller.bridgeAllStrands(origName, rootName);
	    // filePrefix, solutionId, rms, angleWeight, helixAlgorithm, lenMax,
	    // collisionDistance);
	    ps.println("Number of placed bridges: " + numBridges);
	}
	catch (Object3DGraphControllerException oce) {
	    throw new CommandExecutionException("Error in bridgeall command: " + oce.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() > 0) {
	    StringParameter p0 = (StringParameter)(getParameter(0));
	    origName = p0.getValue();
	}
	StringParameter pref = (StringParameter)(getParameter("prefix"));
	if (pref != null) {
	    filePrefix = pref.getValue();
	}
	StringParameter rootPar = (StringParameter)(getParameter("root"));
	if (rootPar != null) {
	    rootName = rootPar.getValue();
	}
	try {
	    StringParameter apar = (StringParameter)(getParameter("a"));
	    if (apar != null) {
		this.helixAlgorithm = Integer.parseInt(apar.getValue()); // convert to internal counting
		if (!BridgeItController.validateHelixAlgorithm(this.helixAlgorithm)) {
		    throw new CommandExecutionException("Unknown helix algorithm id: " + this.helixAlgorithm);
		}
	    }
	    // read maximum length of helices
	    StringParameter lpar = (StringParameter)(getParameter("max"));
	    if (lpar != null) {
		this.lenMax = Integer.parseInt(lpar.getValue()); 
	    }
	    StringParameter npar = (StringParameter)(getParameter("n"));
	    if (npar != null) {
		this.solutionId = Integer.parseInt(npar.getValue()); 
	    }
	    StringParameter rpar = (StringParameter)(getParameter("rms"));
	    if (rpar != null) {
		this.rms = Double.parseDouble(rpar.getValue());
	    }
	    StringParameter collpar = (StringParameter)(getParameter("coll"));
	    if (collpar != null) {
		this.collisionDistance = Double.parseDouble(collpar.getValue());
	    }
	    StringParameter angpar = (StringParameter)(getParameter("angle"));
	    if (angpar != null) {
		this.angleWeight = Double.parseDouble(angpar.getValue());
	    }
	}
	catch (NumberFormatException nfe) {
	    throw new CommandExecutionException("Could not parse number: " + nfe.getMessage());
	}
    }

}
