package rnadesign.designapp;

import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;

import static rnadesign.designapp.PackageConstants.*;

public class CellCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "cell";

    private double a = 0;
    private double b = 0;
    private double c = 0;
    private double alpha = 0;
    private double beta = 0;
    private double gamma = 0;

    private Object3DGraphController controller;
    
    public CellCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	CellCommand command = new CellCommand(controller);
	command.a = a;
	command.b = b;
	command.c = c;
	command.alpha = alpha;
	command.beta = beta;
	command.gamma = gamma;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " a b c alpha beta gamma";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " a b c alpha beta gamma" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Currently not supported." + NEWLINE + NEWLINE;
	// helpText += "OPTIONS" + NEWLINE;
	// helpText += "     name=NAME" + NEWLINE + "          Set the name of the imported file." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	assert (this.controller != null);
	prepareReadout();
	this.controller.setCell(a,b,c,alpha, beta, gamma);

    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() != 6) {
	    throw new CommandExecutionException(helpOutput()); 
	}
	StringParameter p0 = (StringParameter)(getParameter(0));
	StringParameter p1 = (StringParameter)(getParameter(1));
	StringParameter p2 = (StringParameter)(getParameter(2));
	StringParameter p3 = (StringParameter)(getParameter(3));
	StringParameter p4 = (StringParameter)(getParameter(4));
	StringParameter p5 = (StringParameter)(getParameter(5));
	a = Double.parseDouble(p0.getValue());
	b = Double.parseDouble(p1.getValue());
	c = Double.parseDouble(p2.getValue());
	alpha = DEG2RAD * Double.parseDouble(p3.getValue());
	beta = DEG2RAD * Double.parseDouble(p4.getValue());
	gamma = DEG2RAD * Double.parseDouble(p5.getValue());
    }
    
}
