package rnadesign.designapp;

import java.io.PrintStream;
import java.util.Properties;
import tools3d.objects3d.Object3D;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnamodel.MoleculeTools;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class CharacterizeCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "characterize";

    private Object3DGraphController controller;
    private PrintStream ps;
    private String name;
    private String firstValue;

    public CharacterizeCommand(PrintStream ps, Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	assert ps != null;
	this.ps = ps;
	this.controller = controller;
    }

    public Object cloneDeep() {
	CharacterizeCommand command = new CharacterizeCommand(this.ps, this.controller);
	command.name = this.name;
	command.firstValue = this.firstValue;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME 
	    + " [strands|atoms|helixends|residues][allowed=<CLASSNAME>][forbidden=<CLASSNAME>][name=<OBJECTNAME>]" + NEWLINE
	    + " with typical CLASSNAME values of Atom3D, RnaStrand3D and object names of root.A or 1.1.5";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + helpOutput();
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Tree command displays list of objects in tree." + NEWLINE + NEWLINE;
	helpText += "OPTIONS" + NEWLINE;
	helpText += "     allowed=OBJECT" + NEWLINE + "          Atom3D, Nucleotide3D. Show only this type." + NEWLINE + NEWLINE;
	helpText += "     forbidden=OBJECT" + NEWLINE + "          RnaStrand3D. Do not show this type." + NEWLINE + NEWLINE;
	helpText += "     name=OBJECT" + NEWLINE + "          Only print the sub-tree from node with this name." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	Properties prop = new Properties();
	Object3D obj = controller.getGraph().getGraph();
	if (name != null) {
	    obj = controller.getGraph().findByFullName(name);
	    if (obj == null) {
		throw new CommandExecutionException("Could not find object: " + name);
	    }
	}
	prop = MoleculeTools.characterize(obj);
	ps.println("Result of characterization: ");
	ps.println(prop.toString());
	this.resultProperties = prop;
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() > 0) {
	    StringParameter firstParameter = (StringParameter)(getParameter(0));
	    if (firstParameter != null) {
		firstValue = firstParameter.getValue();
	    }
	}
	StringParameter nameParameter = (StringParameter)(getParameter("name"));
	if (nameParameter != null) {
	    name = nameParameter.getValue();
	}

    }
}
    
