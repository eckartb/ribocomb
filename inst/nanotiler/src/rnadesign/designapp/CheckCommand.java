package rnadesign.designapp;

import java.io.PrintStream;

import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DTools;
import rnadesign.rnacontrol.Object3DGraphController;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class CheckCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "check";

    private Object3DGraphController controller;
    private PrintStream ps;
    
    public CheckCommand(PrintStream ps, Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	assert ps != null;
	this.ps = ps;
	this.controller = controller;
    }

    public Object cloneDeep() {
	CheckCommand command = new CheckCommand(this.ps, this.controller);
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME;
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Checks for structural problems. Currently implemented: missing backbone atoms in nucleotides." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	controller.getGraph().checkStructure(ps);
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

}
