package rnadesign.designapp;

import java.io.PrintStream;

import rnadesign.rnacontrol.Object3DGraphController;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class ClearCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "clear";

    private Object3DGraphController controller;
    private String name;

    public ClearCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	ClearCommand command = new ClearCommand(this.controller);
	command.name = this.name;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " (all|trash)";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " (all|trash)" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Clear command clears the workspace of all objects. Building block loaded with loadjunctions are not affected." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	if (name != null && name.equals("all")) {
		controller.clear();
		System.gc(); // suggest running garbage collection
	} else if (name != null && name.equals("trash")) {
	    controller.emptyTrash();
	    System.gc(); // suggest running garbage collection
	}
	else {
	    throw new CommandExecutionException("Bad command syntax! " + helpOutput());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() == 1) {
	    StringParameter p0 = (StringParameter)(getParameter(0));
	    name = p0.getValue();
	}
	else {
	    throw new CommandExecutionException(helpOutput()); 
	}
    }

}
