package rnadesign.designapp;

import java.io.PrintStream;

import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class CloneCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "clone";

    private Object3DGraphController controller;
    private String origName;
    private String newParentName;
    private String newIndividualName;
    private int symId = 0;

    public CloneCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	CloneCommand command = new CloneCommand(this.controller);
	command.origName = this.origName;
	command.newParentName = this.newParentName;
	command.newIndividualName = this.newIndividualName;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " originalname newparentname newindividualname";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " ORIGINALNAME NEWPARENTNAME NEWINDIVIDUALNAME" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Clone command clones an object to a new name and place." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	try {
	    controller.cloneObject(origName, newParentName, newIndividualName, symId);
	}
	catch (Object3DGraphControllerException e) {
	    throw new CommandExecutionException(e.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() < 3) {
	    throw new CommandExecutionException(helpOutput());
	}
	StringParameter p0 = (StringParameter)(getParameter(0));
	origName = p0.getValue();
	StringParameter p1 = (StringParameter)(getParameter(1));
	newParentName = p1.getValue();
	StringParameter p2 = (StringParameter)(getParameter(2));
	newIndividualName = p2.getValue();
	StringParameter sp = (StringParameter)(getParameter("sym"));
	try {
	    if (sp != null) {
		symId = Integer.parseInt(sp.getValue());
	    }
	} catch (NumberFormatException nfe) {
	    throw new CommandExecutionException(nfe.getMessage());
	}
// 	if (newIndividualName.indexOf(".") >= 0) { // TODO : check for "." characters
// 	    throw new CommandExecutionException("No . character allowed in individual object name.");
// 	}
    }

}
