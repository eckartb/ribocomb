package rnadesign.designapp;

import java.io.PrintStream;

import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DTools;
import rnadesign.rnacontrol.Object3DGraphController;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import rnadesign.rnamodel.RnaTools;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class ClosestCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "closest";

    private Object3DGraphController controller;
    private PrintStream ps;
    
    public ClosestCommand(PrintStream ps, Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	assert ps != null;
	this.ps = ps;
	this.controller = controller;
    }

    public Object cloneDeep() {
	ClosestCommand command = new ClosestCommand(this.ps, this.controller);
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME;
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Closest command shows the closest objects for each structure class." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	Object3D obj = controller.getGraph().getSelectionRoot();
	if (obj == null) {
	    ps.println("undefined");
	}
	else {
	    ps.println(RnaTools.reportClosestRnaObjects(Object3DTools.findRoot(obj),obj.getPosition()));
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

}
