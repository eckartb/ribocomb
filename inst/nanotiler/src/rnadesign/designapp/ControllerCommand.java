package rnadesign.designapp;

import java.io.PrintStream;
import java.util.*;
import generaltools.ParsingException;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DController;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class ControllerCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "controller";

    private Object3DGraphController controller;
    private Properties properties = new Properties();
    public static final String MIN_LENGTH_STRING = "import-stem-length-min";
    public static final String RENAME_STRAND_STRING = "rename-strands";

    public ControllerCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	ControllerCommand command = new ControllerCommand(this.controller);
	this.properties = properties;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Direct setting and querying of controller variables and status. Correct usage: " + COMMAND_NAME 
	    + " [" + MIN_LENGTH_STRING + "=value]  [" + RENAME_STRAND_STRING + "=true|false]";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + helpOutput();
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	Enumeration keys = properties.propertyNames();
	System.out.println("Using controller directives: " + properties);
	while (keys.hasMoreElements()) {
	    String key = (String)(keys.nextElement());
	    try {
		if (key.equals(MIN_LENGTH_STRING)) {
		    controller.setImportStemLengthMin(Integer.parseInt((String)(properties.getProperty(key))));
		}
		else if (key.equals(RENAME_STRAND_STRING) ) {
		    boolean flag = StringParameter.parseBoolean(properties.getProperty(key));
		    if (flag) {
			controller.getGraph().setRenameSequenceMode(Object3DController.SEQUENCE_RENAMING_COUNTER);
		    } else {
			controller.getGraph().setRenameSequenceMode(Object3DController.NO_SEQUENCE_RENAMING);
		    } 
		}
		else {
		    throw new CommandExecutionException("Unknown controller directive : " + key);
		}
	    }
	    catch (NumberFormatException nfe) {
		throw new CommandExecutionException("Could not parse number for : " + key + " : " + properties.getProperty(key)
						    + " : " + nfe.getMessage());
	    }
	    catch (ParsingException pe) {
		throw new CommandExecutionException("Parsing exception in controller command: " + pe.getMessage());
	    }
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	for (int i = 0; i < getParameterCount(); ++i) {
	    StringParameter parameter = (StringParameter)(getParameter(i));
	    assert (parameter != null);
	    String value = parameter.getValue();
	    String name = parameter.getName();
	    if ((name == null) || (name.length() == 0)) {
		throw new CommandExecutionException("Only named parameters excepted in controller command: " + getParameter(i));
	    }
	    properties.setProperty(name, value);
	}
    }
}
    
