package rnadesign.designapp;

import java.io.*;
import java.text.ParseException;
import java.util.*;
import tools3d.symmetry2.*;
import tools3d.objects3d.*;
import rnadesign.rnacontrol.*;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.StringParameter;
import commandtools.CommandExecutionException;
import generaltools.ParsingException;
import rnasecondary.*;

import static rnadesign.designapp.PackageConstants.NEWLINE;

/** For a give secondary structure, create a NanoTiler script, that if exectuted, creates a 3D structure corresponding to the RNA secondary structure. */
public class ConvertSecondaryFormatCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "convertformat";
    private Object3DGraphController controller;
    private PrintStream ps; //to write to file and terminal
    private int out; //int flag for out 
    private int inType;
    private String inputFileName;  //input file name
    private String outputFileName; //outputfile name
    private boolean writeToFile = false;
    private Properties params = new Properties(); //easy parsing of input parameters

    public ConvertSecondaryFormatCommand(PrintStream ps, Object3DGraphController controller) {
    	super(COMMAND_NAME);
    	assert controller != null;
    	this.controller = controller;
      this.ps = ps;
    }

    public Object cloneDeep() {
    	ConvertSecondaryFormatCommand command = new ConvertSecondaryFormatCommand(this.ps, this.controller);
    	command.inputFileName = this.inputFileName;
    	command.params=this.params;
    	for (int i = 0; i < getParameterCount(); ++i) {
    	    command.addParameter((Command)(getParameter(i).cloneDeep())); 
    	}
    	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	      return "Correct usage: " + COMMAND_NAME + " in=INPUT_SECONDARY_FILE_NAME out=[sec|ct|server|fasta] file=OUTPUT_SECONDARY_FILE_NAME";
    }
    
    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
    	String helpText = getShortHelpText() + NEWLINE + "\"" + COMMAND_NAME + "\" Command Manual" +
    	    NEWLINE + NEWLINE;
    	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
    	    NEWLINE + NEWLINE;
    	helpText += "in=INPUT_SECONDARY_FILE_NAME : secondary structure to be traced (only ct and sec)" + NEWLINE;
      helpText += "out=[sec|ct|server|fasta]  : out file format " + NEWLINE; 
      helpText += "file=OUTPUT_SECONDARY_FILE_NAME : name of the output file with extention" + NEWLINE;   	
      helpText += "DESCRIPTION" + NEWLINE +
    	    "     Converts to and from supported secondary structure file formats " + NEWLINE + NEWLINE;
    	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
    	prepareReadout();
    	try {
         SecondaryStructureParser parser = null;
    	    if (inType == Object3DGraphControllerConstants.CT_FORMAT) {
    	      parser = new CTFileParser();
          } else if (inType == Object3DGraphControllerConstants.SECONDARY_FORMAT) {
            parser = new ImprovedSecondaryStructureParser();
          } else {
            throw new CommandExecutionException("Unknown out (only accepts sec and ct as input):" + out);
          }
        
          SecondaryStructure secondaryStructure = parser.parse(inputFileName);
          if (!writeToFile) { //prints out to console
            controller.writeSecondaryStructure (ps, out, secondaryStructure);
          } else if (writeToFile) { //will not print to console if output file exists
            FileOutputStream fos = new FileOutputStream(outputFileName);
          	PrintStream ps2 = new PrintStream(fos);
          	controller.writeSecondaryStructure(ps2, out, secondaryStructure);
          	fos.close();
          }
        
          
    	}
    	catch (IOException ioe) {
    	    throw new CommandExecutionException("IOException in " + COMMAND_NAME + " : " + ioe.getMessage());
    	}
    	catch (Object3DGraphControllerException gce) {
    	    throw new CommandExecutionException("Error retrieving objects: " + gce.getMessage());
    	}
    	catch (ParseException pe) {
    	    throw new CommandExecutionException("Error parsing secondary structure: " + pe.getMessage());
    	}
    }

    public Command execute() throws CommandExecutionException {
    	executeWithoutUndo();
    	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
    	StringParameter p0 = (StringParameter)(getParameter("in"));
    	if (p0 == null) {
    	    throw new CommandExecutionException(helpOutput()); 
    	}
      StringParameter egg = (StringParameter)(getParameter("eggin")); //this is a hidden parameter that is only meant to
      //be used by devs. This parameter adds support for Galaxy, which outputs .dat files
      //eggin can be used to specify the input file format when the file extention does not provide the info
      //eggin in for Easter egg
      if ((egg == null) || (egg.getValue ().isEmpty ())) {
      	inputFileName = p0.getValue();
        if (inputFileName != null) {
          if (inputFileName.toLowerCase().contains(".sec")) { //gets format of input file depending upon extention
            inType = Object3DGraphControllerConstants.SECONDARY_FORMAT;
          } else if (inputFileName.toLowerCase().contains(".ct")) {
            inType = Object3DGraphControllerConstants.CT_FORMAT;
          }
        }
      } else { //only if inegg paramater is specified
        String tmp = egg.getValue (); //temporary string
        inputFileName = p0.getValue ();  
        if (tmp.toLowerCase().contains("sec")) {
          inType = Object3DGraphControllerConstants.SECONDARY_FORMAT;
        } else if (tmp.toLowerCase().contains("ct")) {
          inType = Object3DGraphControllerConstants.CT_FORMAT;
        }
      }
      
      StringParameter p1 = (StringParameter)(getParameter("out")); //format to convert to assigned
      if (p1 != null) { // expect "sec" [default] or "ct";
        String value = p1.getValue();
          if (value.equals("ct")) {
        		out = Object3DGraphControllerConstants.CT_FORMAT;
          } else if (value.equals("sec")) {
        		out = Object3DGraphControllerConstants.SECONDARY_FORMAT;
          } else if (value.equals("fasta")) {
        		out = Object3DGraphControllerConstants.FASTA_FORMAT;
          } else if (value.equals("server")) { // use for web server
        		out = Object3DGraphControllerConstants.SERVER_FORMAT;
        	} else {
        		throw new CommandExecutionException("Unknown format string: " + value);
        	}
      } 
      
      StringParameter p2 = (StringParameter)(getParameter("file"));
      if (p2 != null) {
        outputFileName = p2.getValue();
        writeToFile = true; //if no output file is given, only print to screen, do not write to file
      } 

    } 
      

}