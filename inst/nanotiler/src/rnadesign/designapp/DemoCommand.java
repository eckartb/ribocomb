package rnadesign.designapp;

import commandtools.*;
import java.io.File;
import java.io.FilenameFilter;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.IOException;

import static rnadesign.designapp.PackageConstants.*;

public class DemoCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "demo";

    private String fileName;
    private CommandApplication application;
    private File[] files;

    private class ScriptNameFilter implements FilenameFilter {
	
	public boolean accept(File dir, String name) {
	    return name != null && name.endsWith(".script");
	}

    }

    public DemoCommand(CommandApplication application) {
	super(COMMAND_NAME);
	this.application = application;
    }

    public Object cloneDeep() {
	DemoCommand command = new DemoCommand(application);
	command.fileName = this.fileName;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    public String helpOutput() {
	return "Reads and executes demo scripts available in ${NANOTILER_HOME}/demo/scripts. Correct usage: demo relativefilename";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " FILENAME" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Demo command reads and executes script file located in ${NANOTILER_HOME}/demo/scripts." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	assert (this.application != null);
	prepareReadout();
	if (fileName == null) {
	    assert files != null;
	    System.out.println("Available demos: ");
	    for (File file : files) {
		String name = file.getName();
		if (name.endsWith(".script")) {
		    System.out.println(name.substring(0, name.length()-7));
		}
		else {
		    System.out.println(name);
		}
	    }
	    System.out.println("Run individual demo with command : demo NAME");
	    System.out.println("Run all demos with command : demo all");
	}
	else if (fileName.equals("all")) {
	    System.out.println("demo all not yet implemented!");
	    for (File file : files) {
		try {
		    application.runScriptLine("echo #############################################################################");
		    application.runScriptLine("echo Running all available demos!");
		    application.runScriptLine("clear all"); // remove all entries from workspace to avoid collisions
		    String scriptName = NANOTILER_HOME + "/demo/scripts/" + file.getName();
		    String command = "source " + scriptName;
		    application.runScriptLine("echo Executing demo script with command: " + command);
		    application.runScriptLine("echo #############################################################################");
		    application.runScriptLine(command);
		    application.runScriptLine("echo #############################################################################");
		    application.runScriptLine("echo Finished running all demos !");
		    application.runScriptLine("echo #############################################################################");
		}
		catch (CommandException ce) {
		    throw new CommandExecutionException(ce.getMessage());
		}		
	    }
	}
	else {
	    if (!fileName.endsWith(".script")) {
		fileName = fileName + ".script";
	    }
	    try {

		String command = "source " + NANOTILER_HOME + "/demo/scripts/" + fileName;
		application.runScriptLine("echo #############################################################################");
		application.runScriptLine("echo Running demo " + fileName);
		application.runScriptLine("echo Executing demo script with command: " + command);
		application.runScriptLine("echo #############################################################################");
		application.runScriptLine("clear all");
		application.runScriptLine(command);
		application.runScriptLine("echo #############################################################################");
		application.runScriptLine("echo Finished demo " + fileName);
		application.runScriptLine("echo #############################################################################");
	    }
	    catch (CommandException ce) {
		throw new CommandExecutionException(ce.getMessage());
	    }
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	String dirName = NANOTILER_HOME + SLASH + "demo" + SLASH + "scripts";
	File dirFile = new File(dirName);
	if (!dirFile.exists()) {
	    throw new CommandExecutionException("Could not find directory: " + dirName);
	}
	this.files = dirFile.listFiles(new ScriptNameFilter());
	if (getParameterCount() < 1) {
	    return;
	}
	assert getParameter(0) instanceof StringParameter;
	StringParameter stringParameter = (StringParameter)(getParameter(0));
	fileName = stringParameter.getValue();

    }

    
}
