package rnadesign.designapp;

import java.io.PrintStream;

import tools3d.objects3d.Object3D;
import tools3d.Vector3D;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class DistanceCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "dist";

    private Object3DGraphController controller;
    private String origName;
    private String newParentName;
    private PrintStream ps;
    private static double distance;

    public DistanceCommand(PrintStream ps, Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.ps = ps;
	this.controller = controller;
    }

    public Object cloneDeep() {
	DistanceCommand command = new DistanceCommand(this.ps, this.controller);
	command.origName = this.origName;
	command.newParentName = this.newParentName;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " name1 name2";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " OBJECTNAME1 OBJECTNAME2" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Distance command gives the distance between two objects." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	try {
	    distance = controller.getGraph().computeDistance(origName, newParentName);
	    Object3D obj1 = controller.getGraph().findByFullName(origName);
	    Object3D obj2 = controller.getGraph().findByFullName(newParentName);
	    Vector3D distVec = obj2.getPosition().minus(obj1.getPosition());
	    ps.println(distance);
	    ps.println(" Position 1: " + obj1.getPosition() + " Position 2: " + obj2.getPosition() + " Distance vector: " + distVec);
	    resultProperties.setProperty("dist", "" + distance);
	}
	catch (Object3DGraphControllerException e) {
	    throw new CommandExecutionException(e.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() != 2) {
	    throw new CommandExecutionException(helpOutput());
	}
	StringParameter p0 = (StringParameter)(getParameter(0));
	origName = p0.getValue();
	StringParameter p1 = (StringParameter)(getParameter(1));
	newParentName = p1.getValue();
    }
    public double getDistance(){
	return distance;
    }

}
