package rnadesign.designapp;

import java.io.PrintStream;
import java.util.HashSet;
import java.util.Set;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class EndingCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "ending";

    private Object3DGraphController controller;
    private String name;
    private String ending;
    private Set<String> allowedNames;
    private Set<String> forbiddenNames;

    public EndingCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	EndingCommand command = new EndingCommand(this.controller);
	command.name = this.name;
	command.ending = this.ending;
	command.allowedNames = this.allowedNames;
	command.forbiddenNames = this.forbiddenNames;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " [name=NAME] [ending=ENDING]"
	    + " [ allowed=Atom3D,Nucleotide3D | forbidden=RnaStrand]";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " [options]" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Adds an ending to the name of all objects of a subtree. The options allowed and forbidden can specify the required object classes to which command is applied." + NEWLINE + NEWLINE;
	helpText += "OPTIONS" + NEWLINE;
	helpText += "     name=value     : specify name of subtree for which ending is to be added." + NEWLINE + NEWLINE;
	helpText += "     ending=value   : value of ending to be added" + NEWLINE;
	helpText += "     allowed=Classname[,Classname[...]]" + NEWLINE + "          example: Atom3D,Nucleotide3D" + NEWLINE + NEWLINE;
	helpText += "     forbidden=Classname[,Classname[...]]" + NEWLINE + "          example: RnaStrand" + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	if (name != null) {
	    try {
		controller.getGraph().addEnding(name, ending, allowedNames, forbiddenNames);
	    }
	    catch (Object3DGraphControllerException gce) {
		throw new CommandExecutionException(gce.getMessage());
	    }
	}
	else {
	    throw new CommandExecutionException("No name specified!");
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	StringParameter nameParameter = (StringParameter)(getParameter("name"));
	if (nameParameter != null) {
	    name = nameParameter.getValue();
	}
	StringParameter endingParameter = (StringParameter)(getParameter("ending"));
	if (endingParameter != null) {
	    ending = endingParameter.getValue();
	}
	StringParameter allowedParameter = (StringParameter)(getParameter("allowed"));
	if (allowedParameter != null) {
	    String[] allowedNamesRaw = allowedParameter.getValue().split(",");
	    allowedNames = new HashSet<String>();
	    for (String s : allowedNamesRaw) {
		allowedNames.add(s);
	    }
	}
	StringParameter forbiddenParameter = (StringParameter)(getParameter("forbidden"));
	if (forbiddenParameter != null) {
	    String[] forbiddenNamesRaw = forbiddenParameter.getValue().split(",");
	    for (String s : forbiddenNamesRaw) {
		forbiddenNames.add(s);
	    }
	}
    }
}
    
