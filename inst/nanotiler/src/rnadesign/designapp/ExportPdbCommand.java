package rnadesign.designapp;

import java.io.FileOutputStream;
import java.io.IOException;

import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import generaltools.ParsingException;
import tools3d.objects3d.*;
import rnadesign.rnacontrol.Object3DGraphController; 
import rnadesign.rnacontrol.Object3DGraphControllerException;
import rnadesign.rnamodel.GeneralPdbWriter;
import rnadesign.rnamodel.AmberPdbWriter;
import rnadesign.rnamodel.AbstractPdbWriter;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class ExportPdbCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "exportpdb";

    private String fileName;
    private boolean helicesMode = true; // iff true, write pure helical segments
    private boolean junctionMode = true;
    private int renameMode = GeneralPdbWriter.RESIDUE_RECOUNTED_NUMBER;
    private String variant = "default"; // also: "amber"
    private Object3DGraphController controller;
    private String[] subtreeNames = null;

    public ExportPdbCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	ExportPdbCommand command = new ExportPdbCommand(controller);
	command.fileName = this.fileName;
	command.helicesMode = this.helicesMode;
	command.junctionMode = this.junctionMode;
	command.renameMode = this.renameMode;
	command.variant = this.variant;
	command.subtreeNames = this.subtreeNames;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    public String getShortHelpText() { return helpOutput(); }

    public String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " FILENAME [renumber=count|name|pdb] [helices=true|false] [junction=false|true] [name=subtree1[,subtree2[,...]]][variant=default|amber";
    }

    public String getLongHelpText() {
	String helpText = "\"exportpdb\" Command Manual" + NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     exportpdb" + NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + helpOutput() + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE + "     The command writes a file in PDB format with atom coordinates.";
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	assert (this.controller != null);
	prepareReadout();
	try {
	    if (!fileName.endsWith(".pdb")) {
		fileName += ".pdb";
	    }
	    FileOutputStream fos = new FileOutputStream(fileName);    
	    int format = 0;
	    AbstractPdbWriter writer = null;
	    if (variant.equals("default")) {
		writer = new GeneralPdbWriter();
	    } else if (variant.equals("amber")) {
		writer = new AmberPdbWriter();
	    }
	    writer.setOriginalMode(renameMode);
	    writer.setJunctionMode(junctionMode);
	    if (!helicesMode) {
		// obtain a list of purely helical strands:
		Object3DSet helixStrands = controller.findPureHelicalStrands();
		if (helixStrands.size() > 0) {
		    log.info("Exporting PDB structure ignoring " + helixStrands.size() + " helical strands.");
		    Object3DTester helixTester = new ForbiddenObject3DTester(helixStrands.getAsList());		
		    writer.setObjectTester(helixTester);
		}
	    }
	    controller.write(fos, writer, subtreeNames);
	}
	catch (IOException exc) {
	    // replace with better error window
	    throw new CommandExecutionException("Error opening file " + fileName);
	}
	catch (Object3DGraphControllerException ex) {
	    throw new CommandExecutionException("Controller error in exportpdb command: " + ex.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() < 1) {
	    throw new CommandExecutionException(helpOutput());
	}
	assert getParameter(0) instanceof StringParameter;
	try {
	StringParameter stringParameter = (StringParameter)(getParameter(0));
	fileName = stringParameter.getValue();
	StringParameter renumberParameter = (StringParameter)(getParameter("renumber"));
	if (renumberParameter != null) {
	    String renumberString = renumberParameter.getValue();
	    if (renumberString != null) {
		if (renumberString.equals("pdb")) {
		    renameMode = GeneralPdbWriter.RESIDUE_PDB_NUMBER;
		}
		else if (renumberString.equals("count")) {
		    renameMode = GeneralPdbWriter.RESIDUE_RECOUNTED_NUMBER;
		}
		else if (renumberString.equals("name")) {
		    renameMode = GeneralPdbWriter.RESIDUE_NAMED_NUMBER;
		}
		else {
		    throw new CommandExecutionException("Unknown value of parameter renumber! Allowed: pdb|count|name");
		}
	    }
	}
	StringParameter junctionParameter = (StringParameter)(getParameter("junction"));
	if (junctionParameter != null) {
	    junctionMode = junctionParameter.parseBoolean(); // set to true if value "true" or "TRUE"
	}
	StringParameter helicesParameter = (StringParameter)(getParameter("helices"));
	if (helicesParameter != null) {
	    helicesMode = helicesParameter.parseBoolean(); // set to true if value "true" or "TRUE"
	}
	}
	catch (ParsingException pe) {
	    throw new CommandExecutionException(pe.getMessage());
	}
	StringParameter nameParameter = (StringParameter)(getParameter("name"));
	if (nameParameter!=null) {
	    this.subtreeNames = nameParameter.getValue().split(",");
	}
	StringParameter variantParameter = (StringParameter)(getParameter("variant"));
	if (variantParameter!=null) {
	    this.variant = variantParameter.getValue();
	}
    }
    
}
