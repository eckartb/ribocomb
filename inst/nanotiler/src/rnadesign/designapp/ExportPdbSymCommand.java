package rnadesign.designapp;

import java.io.FileOutputStream;
import java.io.IOException;

import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import rnadesign.rnacontrol.Object3DGraphController; 
import rnadesign.rnacontrol.Object3DGraphControllerException;
import rnadesign.rnamodel.GeneralPdbWriter;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class ExportPdbSymCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "exportpdbsym";

    private String fileName;
    private Object3DGraphController controller;
    
    public ExportPdbSymCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	ExportPdbSymCommand command = new ExportPdbSymCommand(controller);
	command.fileName = this.fileName;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    public void executeWithoutUndo() throws CommandExecutionException {
	assert (this.controller != null);
	prepareReadout();
	try {
	    controller.writePdbSymmetric(fileName);
	}
	catch (Object3DGraphControllerException e) {
	    // replace with better error window
	    throw new CommandExecutionException("Generating PDB file including symmetries: " + e.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() {
	assert (getParameterCount() > 0);
	assert getParameter(0) instanceof StringParameter;
	StringParameter stringParameter = (StringParameter)(getParameter(0));
	fileName = stringParameter.getValue();
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " FILENAME" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     ExportPdbSym command TODO." + NEWLINE + NEWLINE;
	return helpText;
    }

   private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " FILENAME";
    }

    
}
