package rnadesign.designapp;

import java.io.FileOutputStream;
import java.io.IOException;

import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import generaltools.ParsingException;
import tools3d.objects3d.*;
import rnadesign.rnacontrol.Object3DGraphController; 
import rnadesign.rnacontrol.Object3DGraphControllerException;
import rnadesign.rnamodel.GeneralPdbWriter;
import rnadesign.rnamodel.AmberPdbWriter;
import rnadesign.rnamodel.AbstractPdbWriter;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class ExportPointsCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "exportpoints";

    private String className = "Object3D";
    private String fileName;
    private boolean removeRoot = true;
    private String rootName = "root";
    private Object3DGraphController controller;

    public ExportPointsCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	ExportPointsCommand command = new ExportPointsCommand(controller);
	command.className = this.className;
	command.controller = this.controller;
	command.fileName = this.fileName;
	command.removeRoot = this.removeRoot;
	command.rootName = this.rootName;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    public String getShortHelpText() { return helpOutput(); }

    public String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " FILENAME [root=ROOTNAME] [class=CLASSNAME]";
    }

    public String getLongHelpText() {
	String helpText = "\"exportpdb\" Command Manual" + NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     exportpdb" + NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + helpOutput() + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE + "     The command writes a file in PDB format with atom coordinates.";
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	assert (this.controller != null);
	prepareReadout();
	try {
	    FileOutputStream fos = new FileOutputStream(fileName);    
	    controller.writePoints(fos, rootName, className, removeRoot);
	}
	catch (IOException exc) {
	    // replace with better error window
	    throw new CommandExecutionException("Error opening file " + fileName);
	}
	catch (Object3DGraphControllerException ex) {
	    throw new CommandExecutionException("Controller error in exportpdb command: " + ex.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() < 1) {
	    throw new CommandExecutionException(helpOutput());
	}
	assert getParameter(0) instanceof StringParameter;
	StringParameter stringParameter = (StringParameter)(getParameter(0));
	fileName = stringParameter.getValue();

	StringParameter nameParameter = (StringParameter)(getParameter("root"));
	if (nameParameter!=null) {
	    this.rootName = nameParameter.getValue();
	}
	StringParameter classParameter = (StringParameter)(getParameter("class"));
	if (classParameter!=null) {
	    this.className = classParameter.getValue();
	}

    }
    
}
