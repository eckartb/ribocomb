package rnadesign.designapp;

import generaltools.ParsingException;
import rnadesign.rnamodel.FittingException;
import java.io.PrintStream;

import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DTools;
import rnadesign.rnamodel.BranchDescriptor3D;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import java.util.List;
import java.util.Properties;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class FitCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "fit";
    private PrintStream ps;
    private Object3DGraphController controller;
    private boolean placeMode = true;
    private String mode;
    private String root = null;
    private String b1Name;
    private String b2Name;
    private double rms = 5.0;
    private int n1Min = 0;
    private int n1Max = 10;
    private int n2Min = 0;
    private int n2Max = 10;

    public FitCommand(PrintStream ps, Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
	this.ps = ps;
    }

    public Object cloneDeep() {
	FitCommand command = new FitCommand(this.ps, this.controller);
	command.name = this.name;
	command.placeMode = this.placeMode;
	command.mode = this.mode;
	command.root = this.root;
	command.b1Name = this.b1Name;
	command.b2Name = this.b2Name;
	command.rms = this.rms;
	command.n1Min = this.n1Min;
	command.n1Max = this.n1Max;
	command.n2Min = this.n2Min;
	command.n2Max = this.n2Max;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " (j|k) branchdescriporName1 branchdescriptorName2 [place=true|false][h1min=val][h1max=val][h2min=val][h2max=val][rms=val]";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + helpOutput() +
	    NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Rank orders 2-way junctions into gap." + NEWLINE + NEWLINE;
	return helpText;
    }

    private void printEntry(PrintStream ps, Properties result) {
	ps.println("" + result);
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	Object3D obj1 = controller.getGraph().findByFullName(b1Name);
	Object3D obj2 = controller.getGraph().findByFullName(b2Name);
	if (obj1 == null) {
	    throw new CommandExecutionException("Could not find object 1 : " + b1Name);
	}
	if (obj2 == null) {
	    throw new CommandExecutionException("Could not find object 2 : " + b2Name);
	}
	if (!(obj1 instanceof BranchDescriptor3D)) {
	    throw new CommandExecutionException("Object 1 is not of type BranchDescriptor3D : " + b1Name);
	}
	if (!(obj2 instanceof BranchDescriptor3D)) {
	    throw new CommandExecutionException("Object 2 is not of type BranchDescriptor3D : " + b2Name);
	}
	if (mode == null) {
	    throw new CommandExecutionException("Undfined fit mode! Allowed: (j|k)");
	}
	BranchDescriptor3D b1 = (BranchDescriptor3D)obj1;
	BranchDescriptor3D b2 = (BranchDescriptor3D)obj2;
	List<Properties> result = null;
	double placeRms = rms;
	if (!placeMode) {
	    placeRms = - rms;
	}
	if (root == null) {
	    root = Object3DTools.findRoot(b1).getFullName();
	}
	try {
	    if (mode.equals("j")) {
		result = controller.rankJunctionFits(b1, b2, n1Min, n1Max, n2Min, n2Max, placeRms, root);
	    }
	    else  if (mode.equals("k")) {
		result = controller.rankKissingLoopFits(b1, b2, n1Min, n1Max, n2Min, n2Max, placeRms, root);
	    }
	    else {
		throw new CommandExecutionException("Unknown fit mode! Allowed: (j|k)");
	    }
	    if (result == null) {
		throw new CommandExecutionException("No fitting structural elements found!");
	    }
	}
	catch (Object3DGraphControllerException ogce) {
	    throw new CommandExecutionException(ogce.getMessage());
	}
	catch (FittingException fe) {
	    throw new CommandExecutionException(fe.getMessage());
	}
	for (int i = 0; i < result.size(); ++i) {
	    printEntry(ps, result.get(i));
	    ps.println();
	}
	
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() < 3) {
	    throw new CommandExecutionException(helpOutput());
	}
	this.mode = ((StringParameter)(getParameter(0))).getValue();
	this.b1Name = ((StringParameter)(getParameter(1))).getValue();
	this.b2Name = ((StringParameter)(getParameter(2))).getValue();

	try {
	    this.placeMode = parse("place", this.placeMode);
	    this.rms = parse("rms", this.rms);
	    this.n1Min = parse("h1min", this.n1Min);
	    this.n1Max = parse("h1min", this.n1Max-1) + 1; // user counts inclusive, internally non-inclusive
	    this.n2Min = parse("h2min", this.n2Min);
	    this.n2Max = parse("h2max", this.n2Max - 1) + 1;
	}
	catch (NumberFormatException nfe) {
	    throw new CommandExecutionException(nfe.getMessage());
	}
	catch (ParsingException pe) {
	    throw new CommandExecutionException(pe.getMessage());
	}

	StringParameter rootParam = (StringParameter)(getParameter("root"));
	if (rootParam != null) {
	    root = rootParam.getValue();
	}
    }

}
