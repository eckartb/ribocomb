package rnadesign.designapp;

import java.io.PrintStream;
import java.util.Properties;

import generaltools.ParsingException;
import generaltools.PropertyTools;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class FuseAllStrandsCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "fuseallstrands";

    private Object3DGraphController controller;

    private String root = "root";
    private String resultRoot = "root";
    private String name = "fused";
    private double bridgeRms = 3.0;
    private double cutoff = 5; // previously: 3.0;
    private int lengthMin = 1;
    private int lengthMax = 5;
    private boolean stepMode = false;

    public FuseAllStrandsCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	FuseAllStrandsCommand command = new FuseAllStrandsCommand(this.controller);
	command.root = this.root;
	command.resultRoot = this.resultRoot;
	command.name = this.name;
	command.bridgeRms = this.bridgeRms;
	command.cutoff = this.cutoff;
        command.lengthMin = this.lengthMin;
	command.lengthMax = this.lengthMax;
	command.stepMode = this.stepMode;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Fuses strands corresponding to a ring with as high symmetry as possible. Correct usage: " + COMMAND_NAME 
	    + " [cutoff=VALUE][max=LENGTH][min=LENGTH][name=RESULTNAME][root=RESULTSUBTREENAME][rms=VALUE][step=false|true]";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + helpOutput();
	// helpText += "DESCRIPTION" + NEWLINE  + helpOutput();
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	try {
	    // controller.fuseAllStrands(root, name, resultRoot, cutoff, bridgeRms, lengthMin, lengthMax);
	    if (stepMode) {
		Properties properties = controller.fuseClosestStrands(root, name, resultRoot, cutoff, bridgeRms, lengthMin, lengthMax);
		PropertyTools.printProperties(System.out, properties);
	    } else {
		int count = controller.fuseAllClosestStrands(root, name, resultRoot, cutoff);
		System.out.println("Performed " + count + " strand fusions!");
	    }
	}
	catch (Object3DGraphControllerException e) {
	    throw new CommandExecutionException("Error while performing automated strand fusing: " + e.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	StringParameter p = (StringParameter)(getParameter("name"));
	if (p != null) {
	    name = p.getValue();
	}
	p = (StringParameter)(getParameter("root"));
	if (p != null) {
	    root = p.getValue();
	    resultRoot = root;
	}
	p = (StringParameter)(getParameter("name"));
	if (p != null) {
	    name = p.getValue();
	}
	try {
	    p = (StringParameter)(getParameter("rms"));
	    if (p != null) {
		this.bridgeRms = Double.parseDouble(p.getValue());
	    }
	    p = (StringParameter)(getParameter("cutoff"));
	    if (p != null) {
		this.cutoff = Double.parseDouble(p.getValue());
	    }
	    p = (StringParameter)(getParameter("min"));
	    if (p != null) {
		this.lengthMin = Integer.parseInt(p.getValue());
	    }
	    p = (StringParameter)(getParameter("max"));
	    if (p != null) {
		this.lengthMax = Integer.parseInt(p.getValue());
	    }
	    p = (StringParameter)(getParameter("step"));
	    if (p != null) {
		this.stepMode = p.parseBoolean(p.getValue());
	    }
	}
	catch (NumberFormatException nfe) {
	    throw new CommandExecutionException("Could not interpret number: " + nfe.getMessage());
	}
	catch (ParsingException pe) {
	    throw new CommandExecutionException("Could parse parameter: " + pe.getMessage());
	}

	if (!validate()) {
	    throw new CommandExecutionException("Invalid parameters for fuseallstrands command!");
	}
    }

    /** Returns true if all parameters make sense */
    public boolean validate() {
	return cutoff > 0.0 && lengthMin >= 0 && lengthMax >= lengthMin && name != null && name.length() > 0 
	    && root != null && root.length() > 0 && resultRoot != null && resultRoot.length() > 0;
    }
}
    
