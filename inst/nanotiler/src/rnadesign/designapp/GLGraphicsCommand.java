package rnadesign.designapp;

import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import rnadesign.rnacontrol.*;
import viewer.display.DisplayFactory;
import viewer.display.DisplayGridLayout;
import viewer.display.DisplayLayoutRequest;
import viewer.display.MultiDisplay;
import viewer.view.ViewOrientation;

public class GLGraphicsCommand extends AbstractCommand {
	
	
	public static final String COMMAND_NAME = "glgraphics";
	
	private Object3DGraphController controller;
	
	public GLGraphicsCommand(Object3DGraphController controller) {
		super(COMMAND_NAME);
		this.controller = controller;
	}

	public Object cloneDeep() {
		return new GLGraphicsCommand(controller);
	}

	public Command execute() throws CommandExecutionException {
		// TODO Auto-generated method stub
		return null;
	}

	public void executeWithoutUndo() throws CommandExecutionException {
		int[][] grid = { { 1, 2}, {3, 4} };
		ViewOrientation[] orientations = { ViewOrientation.Top,
				ViewOrientation.Left,
				ViewOrientation.Back,
				ViewOrientation.PerspectiveTop };
		Dimension size = new Dimension(800, 600);
		DisplayLayoutRequest request = new DisplayGridLayout(grid, orientations, size);
		final MultiDisplay display = DisplayFactory.createDisplay(request);
		//Object3DControllerRenderer renderer = null; // TODO !!! new Object3DControllerRenderer(controller.getGraph());
		//		Object3DControllerRenderer renderer = new Object3DControllerRenderer(controller);
		//display.setModel(renderer);
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				JFrame frame = new JFrame("Display Tester");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.add(display);
				
				frame.pack();
				frame.setVisible(true);
			}
		});
		

	}

}
