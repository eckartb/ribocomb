package rnadesign.designapp;

import java.io.PrintStream;
import generaltools.PropertyTools;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandException;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;

import org.testng.annotations.*; // for testing

import static rnadesign.designapp.PackageConstants.NEWLINE;

/** With this command a Strand junction is generated at the vertex of a graph */
public class GenJunctionCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "genjunction";

    private Object3DGraphController controller;

    private String origName;
    private int angleDiv = 120;
    private String rootName;
    private double offset = 15.0;
    private double distMin = 3.0;
    private double distMax = 9.0;

    public GenJunctionCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	GenJunctionCommand command = new GenJunctionCommand(this.controller);
	command.origName = this.origName;
	command.angleDiv = this.angleDiv;
	command.rootName = this.rootName;
	command.offset = this.offset;
	command.distMin = this.distMin;
	command.distMax = this.distMax;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " CORNERNAME [ROOTNAME] [offset=VALUE][max=VALUE][min=VALUE][n=VALUE]";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " ORIGINALNAME" + NEWLINE + NEWLINE;
	helpText += "max=VALUE       maximum distance between connected 3' and 5' ends of helicas strands. Default: " + this.distMax + NEWLINE;
	helpText += "min=VALUE       minimum distance between connected 3' and 5' ends of helicas strands. Default:" + this.distMin + NEWLINE;
	helpText += "n=VALUE         Number of angles steps to explore full circe. Default: 120 (corresponds to 360/120=3 degrees)." + NEWLINE;
	helpText += "offset=VALUE    distance from helices to curner point (in Angstroem). Default: " + offset + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Generates 3D junction for a graph corner" + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
       	try{
	    resultProperties = controller.generateJunction(origName, rootName, offset, distMin, distMax, angleDiv);
	    PropertyTools.printProperties(System.out, resultProperties);
      	} catch(Object3DGraphControllerException e) {
	    System.out.println("Command error: " + e.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() < 1) {
	    throw new CommandExecutionException(helpOutput());
	}
	StringParameter p0 = (StringParameter)(getParameter(0));
	origName = p0.getValue();
	try {
	    StringParameter poff = (StringParameter)(getParameter("offset"));
	    if (poff != null) {
		this.offset = Double.parseDouble(poff.getValue());
	    }
	    poff = (StringParameter)(getParameter("max"));
	    if (poff != null) {
		this.distMax = Double.parseDouble(poff.getValue());
	    }
	    poff = (StringParameter)(getParameter("min"));
	    if (poff != null) {
		this.distMin = Double.parseDouble(poff.getValue());
	    }
	    poff = (StringParameter)(getParameter("n"));
	    if (poff != null) {
		this.angleDiv = Integer.parseInt(poff.getValue());
	    }
	}
	catch (NumberFormatException nfe) {
	    throw new CommandExecutionException("Error parsing offset value: " + nfe.getMessage());
	}
    }

}
