package rnadesign.designapp;

import java.io.PrintStream;

import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandException;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;

import org.testng.annotations.*; // for testing

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class GenLinkCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "genlink";

    private Object3DGraphController controller;
    private String name1;
    private String name2;

    public GenLinkCommand(Object3DGraphController controller){
    	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	GenLinkCommand command = new GenLinkCommand(this.controller);
	command.name1 = this.name1;
	command.name2 = this.name2;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " name1 name2";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " name1 name2" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "  genlink command generates a link between object 1 and 2." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
       	try{
	    controller.generateLink(name1,name2,"null");
       	}catch(Object3DGraphControllerException e){System.out.println("Command error");}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() != 2) {
	    throw new CommandExecutionException(helpOutput());
	}
	StringParameter p0 = (StringParameter)(getParameter(0));
	name1 = p0.getValue();
	StringParameter p1 = (StringParameter)(getParameter(1));
	name2 = p1.getValue();
    }
}
