package rnadesign.designapp;

import java.io.PrintStream;

import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import tools3d.Vector3D;
import generaltools.ParsingException;

import static rnadesign.rnacontrol.Object3DGraphControllerConstants.*;
import static rnadesign.designapp.PackageConstants.NEWLINE;

public class GenShapeCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "genshape";

    private Object3DGraphController controller;
    private String name;
    private String geomName;
    private double length = 100;
    // currently unused variables:
    private Vector3D startPos = new Vector3D(0,0,0);
    private boolean addStemsFlag = false;
    private char sequenceChar = 'N';
    private boolean onlyFirstPathMode = false;
    private int geometryCode = 103;
    private int tilerAlgorithm = -1;
    private String childNameBase = "p"; 

    public GenShapeCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	GenShapeCommand command = new GenShapeCommand(this.controller);
	command.name = this.name;
	command.geomName = this.geomName;
	command.length = this.length;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME 
	    + " ringN|prismN|tetrahedron|cube|dodecahedron|icosahedron|octahedron [name=objectname][length=value]" + NEWLINE;
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + helpOutput();
	helpText += "DESCRIPTION" + NEWLINE +
	    "     genshape command generates 3D object of specified type." + NEWLINE + NEWLINE;
	helpText += "OPTIONS" + NEWLINE;
	helpText += "     name=OBJECT" + NEWLINE + "          Name of generated object." + NEWLINE + NEWLINE;
	helpText += "     length=VALUE" + NEWLINE + "          Side length of 3D object." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	try {
	    controller.addGeometry(startPos, length, name, childNameBase, addStemsFlag, sequenceChar, onlyFirstPathMode, geometryCode, tilerAlgorithm);
	}
	catch (Object3DGraphControllerException e) {
	    throw new CommandExecutionException("Error in genshape command: " + e.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    /** Returns geometry code as used by Object3DGraphController.addGeometry method */
    private int parseGeometryCode(String geomName) throws ParsingException {
	if (geomName == null) {
	    throw new ParsingException("No geometry name specified!");
	}
	try {
	    if (geomName.equals("cube")) {
		return CUBE;
	    }
	    else if (geomName.equals("tetrahedron")) {
		return TETRAHEDRON;
	    }
	    else if (geomName.equals("dodecahedron")) {
		return DODECAHEDRON;
	    }
	    else if (geomName.equals("octahedron")) {
		return OCTAHEDRON;
	    }
	    else if (geomName.equals("icosahedron")) {
		return ICOSAHEDRON;
	    }
	    else if (geomName.startsWith("ring")) {
		int geomCode = 100 + Integer.parseInt(geomName.substring("ring".length()));
		return geomCode;
	    }
	    else if (geomName.startsWith("prism")) {
		int geomCode = 200 + Integer.parseInt(geomName.substring("prism".length()));
		return geomCode;
	    }
	    else if (!geomName.equals("pink_polarbear")) { // workaround, otherwise java complains about "unreachable statements"!
		throw new ParsingException("Error in parsing geometry code: unknown geometry name " + geomName);
	    }
	}
	catch (NumberFormatException e) {
	    throw new ParsingException("Error parsing geometry code number: " + e.getMessage());
	}
	return -1; // nothing found
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() == 0) {
	    throw new CommandExecutionException("genshape command has to have at least one parameter! Try help genshape or man genshape.");
	}
	StringParameter p0 = (StringParameter)(getParameter(0));
	assert p0 != null;
	this.geomName = p0.getValue();
	try {
	    this.geometryCode = parseGeometryCode(this.geomName);
	    StringParameter nameParameter = (StringParameter)(getParameter("name"));
	    if (nameParameter != null) {
		this.name = nameParameter.getValue();
	    }
	    else {
		this.name = geomName;
	    }
	    StringParameter lengthParameter = (StringParameter)(getParameter("length"));
	    if (lengthParameter != null) {
		this.length = Double.parseDouble(lengthParameter.getValue());
	    }
	}
	catch (NumberFormatException nfe) {
	    throw new CommandExecutionException(nfe.getMessage());
	}
	catch (ParsingException pe) {
	    throw new CommandExecutionException(pe.getMessage());
	}
    }
}
    
