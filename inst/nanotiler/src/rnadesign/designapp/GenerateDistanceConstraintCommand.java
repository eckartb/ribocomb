package rnadesign.designapp;

import java.io.PrintStream;

import tools3d.objects3d.ConstraintLink;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class GenerateDistanceConstraintCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "gendistconstraint";

    private Object3DGraphController controller;
    private String name1;
    private String name2;
    private double min = 0.0;
    private double max = 0.0;
    private int symId1 = 0;
    private int symId2 = 0;

    public GenerateDistanceConstraintCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	GenerateDistanceConstraintCommand command = new GenerateDistanceConstraintCommand(this.controller);
	command.name1 = this.name1;
	command.name2 = this.name2;
	command.min = min;
	command.max = max;
	command.symId1 = symId1;
	command.symId2 = symId2;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " name1 name2 distmin distmax [sym1=ID][sym2=ID]";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " objectname1 objectname2 distmin distmax [sym1=ID][sym2=ID]" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Adds a distance constraint between two object descriptors." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	try {
	    ConstraintLink newLink = controller.addDistanceConstraint(name1, name2, min, max, symId1, symId2);
	    System.out.println("Added constraint for parameters:" + name1 + " " + name2 + " " + min + " " + max + " : " 
			       + newLink.toString());
	}
	catch (Object3DGraphControllerException e) {
	    throw new CommandExecutionException(e.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if ((getParameterCount() < 4)) {
	    throw new CommandExecutionException(helpOutput());
	}
	StringParameter p0 = (StringParameter)(getParameter(0));
	name1 = p0.getValue();
	StringParameter p1 = (StringParameter)(getParameter(1));
	name2 = p1.getValue();
	try {
	    StringParameter p2 = (StringParameter)(getParameter(2));
	    min = Double.parseDouble(p2.getValue());
	    StringParameter p3 = (StringParameter)(getParameter(3));
	    max = Double.parseDouble(p3.getValue());
	    StringParameter sp1 = (StringParameter)(getParameter("sym1"));
	    if (sp1 != null) {
		symId1 = Integer.parseInt(sp1.getValue());
	    }
	    StringParameter sp2 = (StringParameter)(getParameter("sym2"));
	    if (sp2 != null) {
		symId2 = Integer.parseInt(sp2.getValue());
	    }
	}
	catch (NumberFormatException nfe) {
	    throw new CommandExecutionException("Error parsing number value: " + nfe.getMessage());
	}
    }
    
}
