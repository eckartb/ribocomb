package rnadesign.designapp;

import java.io.PrintStream;

import java.util.*;
import tools3d.objects3d.MultiConstraintLink;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class GenerateJunctionConstraintCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "genjunctionconstraint";

    private Object3DGraphController controller;
    private List<String> fivePrimeNames = new ArrayList<String>();
    private List<String> threePrimeNames = new ArrayList<String>();
    private double min = 4.0;
    private double max = 12.0;
    private String name = "jlink";
    private double minBridgable = 2.3;
    private double maxBridgable = 1e30;
    private List<Integer> symIds;

    public GenerateJunctionConstraintCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	GenerateJunctionConstraintCommand command = new GenerateJunctionConstraintCommand(this.controller);
	command.fivePrimeNames = this.fivePrimeNames;
	command.threePrimeNames = this.threePrimeNames;
	command.min = this.min;
	command.max = this.max;
	command.name = this.name;
	command.symIds = this.symIds;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " ends=res1-5,res1-3,res2-5,res2-3[,res3-5,res3-3] min=distmin max=distmax [sym=ID1,ID2,...,IDN]";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " ends=res1-5,res1-3,res2-5,res2-3[,res3-5,res3-3] min=DISTMIN max=DISTMAX minb=DISTMIN maxb=DISTMAX name=NAME [sym=ID1,ID2,...,IDN]";
	helpText += "Generates a junction-constraint by specifying alternating 5' and 3' residues of helix ends pointing towards a junction";
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	try {
	    MultiConstraintLink newLink = controller.addJunctionMultiConstraintLink(name, fivePrimeNames, threePrimeNames, min, max,
										    minBridgable, maxBridgable, symIds);
	}
	catch (Object3DGraphControllerException e) {
	    throw new CommandExecutionException(e.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void parseEndResidues(String s) throws CommandExecutionException {
	String[] words = s.split(","); // split by comma
	fivePrimeNames.clear();
	threePrimeNames.clear();
	if ((words.length == 0) || ((words.length % 2) != 0)) {
	    throw new CommandExecutionException("Even number of residue descriptors expected (alternating 5' and 3' residue descriptors): " + s);
	}
	for (int i = 0; (i+1) < words.length; i+=2) {
	    fivePrimeNames.add(words[i]);
	    threePrimeNames.add(words[i+1]);
	}
    }

    private void parseSymIds(String s) throws CommandExecutionException {
	String[] words = s.split(","); // split by comma
	this.symIds = new ArrayList<Integer>();
	for (int i = 0; i < words.length; ++i) {
	    symIds.add(Integer.parseInt(words[i]));
	}
    }

    private void prepareReadout() throws CommandExecutionException {
	StringParameter pEnd = (StringParameter)(getParameter("ends"));
	if (pEnd == null) {
	    throw new CommandExecutionException(helpOutput());
	} else { // mandatory parameter
	    parseEndResidues(pEnd.getValue());
	}
	StringParameter pSym = (StringParameter)(getParameter("sym"));
	if (pSym != null) {
	    parseSymIds(pSym.getValue());
	}
	StringParameter pName = (StringParameter)(getParameter("name"));
	if (pName != null) {
	    name = pName.getValue();
	}
	try {
	    StringParameter pMin = (StringParameter)(getParameter("min"));
	    if (pMin != null) {
		min = Double.parseDouble(pMin.getValue());
	    }
	    StringParameter pMax = (StringParameter)(getParameter("max"));
	    if (pMax != null) {
		max = Double.parseDouble(pMax.getValue());
	    }
	    StringParameter pMinB = (StringParameter)(getParameter("minb"));
	    if (pMinB != null) {
		minBridgable = Double.parseDouble(pMinB.getValue());
	    }
	    StringParameter pMaxB = (StringParameter)(getParameter("maxb"));
	    if (pMaxB != null) {
		maxBridgable = Double.parseDouble(pMaxB.getValue());
	    }
	}
	catch (NumberFormatException nfe) {
	    throw new CommandExecutionException("Error parsing number value: " + nfe.getMessage());
	}
    }
    
}
