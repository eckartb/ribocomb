package rnadesign.designapp;

import generaltools.ParsingException;
import java.io.PrintStream;
import java.util.*;
import tools3d.objects3d.MultiConstraintLink;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class GenerateJunctionDBConstraintCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "genjunctiondbconstraint";

    private Object3DGraphController controller;
    private List<String> branchNames = new ArrayList<String>();
    private String name = "dbjunction";
    private boolean kissingLoopMode = false;

    public GenerateJunctionDBConstraintCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	GenerateJunctionDBConstraintCommand command = new GenerateJunctionDBConstraintCommand(this.controller);
	command.branchNames.addAll(this.branchNames);
	command.kissingLoopMode = this.kissingLoopMode;
	command.name = this.name;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " name hd=branch1,branch2,[branch3 ...] kl=false|true";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME;
	helpText += "name : name of constraint to be generated" + NEWLINE;
	helpText += "hd=branch1,branch2,[branch3 ...]   : specifies helix ends (class BranchDescriptor3D)." + NEWLINE;
	helpText += "kl=false|true      : specifies if this is a junction (false) or a kissing-loop (true). Default: false." + NEWLINE;
	helpText += "Generates a junction-constraint by specifying helix ends (class BranchDescriptor3D) pointing away from junction" + NEWLINE;
	helpText += getShortHelpText();
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	try {
	    controller.addJunctionDBConstraintLink(name, branchNames, kissingLoopMode);
	}
	catch (Object3DGraphControllerException e) {
	    throw new CommandExecutionException(e.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() < 3) {
	    throw new CommandExecutionException("Syntax error in command " + COMMAND_NAME + " . At least three parameter have to be specified (name, helix1, helix2). See help text.");
	}
	StringParameter p = (StringParameter)(getParameter(0));
	name = p.getValue();
	p = (StringParameter)(getParameter("kl"));
	if (p != null) {
	    try {
		kissingLoopMode = p.parseBoolean();
	    } catch (ParsingException pe) {
		throw new CommandExecutionException("Error parsing boolean (true|false) in parameter kl: " + pe.getMessage());
	    }
	}
	p = (StringParameter)(getParameter("hd"));
	if (p != null) {
	    String[] words = p.getValue().split(",");
	    if (words.length < 2) {
		throw new CommandExecutionException("At least two helix descriptors have to be specified in opion: hd");
	    }
	    for (String word : words) {
		branchNames.add(word);	
	    }
	} else {
	    throw new CommandExecutionException("Missing mandatory parameter: hd");
	}
    }
    
}
