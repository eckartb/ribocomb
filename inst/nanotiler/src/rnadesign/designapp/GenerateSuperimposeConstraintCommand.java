package rnadesign.designapp;

import java.io.PrintStream;

import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import rnadesign.rnamodel.SuperimposeConstraintLink;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class GenerateSuperimposeConstraintCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "gensuperimposeconstraint";

    private Object3DGraphController controller;
    private String name1;
    private String name2;

    public GenerateSuperimposeConstraintCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	GenerateSuperimposeConstraintCommand command = new GenerateSuperimposeConstraintCommand(this.controller);
	command.name1 = this.name1;
	command.name2 = this.name2;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " name1 name2";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " objectname1 objectname2" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Adds a constraint between two objects that tries to superimpose them." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	try {
	    SuperimposeConstraintLink newLink = controller.addSuperimposeConstraint(name1, name2);
	    System.out.println("Added constraint for parameters: " + name1 + " " + name2 + " : " 
			       + newLink.toString());
	}
	catch (Object3DGraphControllerException e) {
	    throw new CommandExecutionException(e.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if ((getParameterCount() < 2)) {
	    throw new CommandExecutionException(helpOutput());
	}
	StringParameter p0 = (StringParameter)(getParameter(0));
	name1 = p0.getValue();
	StringParameter p1 = (StringParameter)(getParameter(1));
	name2 = p1.getValue();
	}
    
}
