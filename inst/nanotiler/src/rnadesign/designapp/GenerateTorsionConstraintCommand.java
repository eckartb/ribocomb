package rnadesign.designapp;

import java.io.PrintStream;

import tools3d.objects3d.TorsionLink;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class GenerateTorsionConstraintCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "gentorsionconstraint";

    private Object3DGraphController controller;
    private String name1;
    private String name2;
    private String name3;
    private String name4;
    private double min = 0.0;
    private double max = 0.0;

    public GenerateTorsionConstraintCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	GenerateTorsionConstraintCommand command = new GenerateTorsionConstraintCommand(this.controller);
	command.name1 = this.name1;
	command.name2 = this.name2;
	command.name3 = this.name3;
	command.name4 = this.name4;
	command.min = min;
	command.max = max;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " name1 name2 name3 name4 distmin distmax";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " objectname1 objectname2 objectname3 objectname4 distmin distmax" + NEWLINE + NEWLINE;
	helpText += "Computes torsion angle between v1-v2 and v4-v3 , with v1,..,v4 being the position vectors of specified objects 1 to 4." + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Adds a torsion constraint between four object descriptors. Angle is measured in degree." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	try {
	    TorsionLink newLink = controller.addTorsionConstraint(name1, name2, name3, name4, min, max);
	    System.out.println("Added constraint for parameters:" + name1 + " " + name2 + " " + name3 + " " + name4 + " "
			       + min + " " + max + " : " + newLink.toString());
	}
	catch (Object3DGraphControllerException e) {
	    throw new CommandExecutionException(e.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if ((getParameterCount() < 6)) {
	    throw new CommandExecutionException(helpOutput());
	}
	StringParameter p0 = (StringParameter)(getParameter(0));
	StringParameter p1 = (StringParameter)(getParameter(1));
	StringParameter p2 = (StringParameter)(getParameter(2));
	StringParameter p3 = (StringParameter)(getParameter(3));
	StringParameter p4 = (StringParameter)(getParameter(4));
	StringParameter p5 = (StringParameter)(getParameter(5));
	name1 = p0.getValue();
	name2 = p1.getValue();
	name3 = p2.getValue();
	name4 = p3.getValue();
	try {
	    min = Math.toRadians(Double.parseDouble(p4.getValue()));
	    max = Math.toRadians(Double.parseDouble(p5.getValue()));
	}
	catch (NumberFormatException nfe) {
	    throw new CommandExecutionException("Error parsing number value: " + nfe.getMessage());
	}
    }
    
}
