package rnadesign.designapp;

import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerConstants;
import rnadesign.rnacontrol.Object3DGraphControllerException;

import static rnadesign.designapp.PackageConstants.*;

public class GridShapeCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "grid";

    private int mode = Object3DGraphControllerConstants.GRID_SHAPE_DEFAULT;

    private Object3DGraphController controller;
    
    public GridShapeCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	GridShapeCommand command = new GridShapeCommand(controller);
	command.mode = mode;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " none|unit";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " none|unit" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Change the grid shape mode." + NEWLINE + NEWLINE;
	// helpText += "OPTIONS" + NEWLINE;
	// helpText += "     name=NAME" + NEWLINE + "          Set the name of the imported file." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	assert (this.controller != null);
	prepareReadout();
	this.controller.setGridShapeMode(mode);
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() != 1) {
	    throw new CommandExecutionException(helpOutput()); 
	}
	StringParameter p0 = (StringParameter)(getParameter(0));
	String s = p0.getValue();
	s = s.toLowerCase();
	if (s.equals("none")) {
	    mode = Object3DGraphControllerConstants.GRID_SHAPE_NONE;
	}
	else if (s.equals("unit")) {
	    mode = Object3DGraphControllerConstants.GRID_SHAPE_UNIT;
	}
	else {
	    throw new CommandExecutionException("Command " + COMMAND_NAME + " : unknown parameter " + s + " " + helpOutput());
	}

    }
    
}
