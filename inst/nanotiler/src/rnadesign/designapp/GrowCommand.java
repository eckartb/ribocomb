package rnadesign.designapp;

import java.io.PrintStream;
import java.util.*;

import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import rnadesign.rnamodel.DBElementDescriptor;
import rnadesign.rnamodel.DBElementConnectionDescriptor;
import rnadesign.rnamodel.GrowConnectivity;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandException;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import generaltools.ParsingException;
import tools3d.Vector3D;

import org.testng.annotations.*; // for testing

import static rnadesign.designapp.PackageConstants.NEWLINE;

/** implements "grow command": given a set of building blocks and connection rules,
 * assemble large structure by "growing" it over several generations.
 */
public class GrowCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "grow";

    private Object3DGraphController controller;

    private List<DBElementDescriptor> blockList = new ArrayList<DBElementDescriptor>();
    private List<DBElementConnectionDescriptor> connections = new ArrayList<DBElementConnectionDescriptor>();
    private boolean graphFlag = false;
    private String rootName = "root";
    private String nameBase = "g_";
    private Vector3D startPosition = new Vector3D(0.0, 0.0, 0.0);
    private int generationCount = 3;
    private boolean addHelicesFlag = true;
    private boolean avoidCollisionsFlag = true;
    private double ringClosureExportLimit = 15.0; // save all structures with this or better ring closure score to pdb file
    private String ringClosureExportFileNameBase = "grow_ring";
    private double stemRmsLimit = 5.0;
    private Set<String> topologies = new HashSet<String>();
    private int sizeMax = -1;
    private int minLen = 0;
    private int maxLen = 7; 
    private boolean randomMode = false;

    public GrowCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    private String getBlockString(DBElementDescriptor dbe) {
	char c = 'j';
	if (dbe.getType() == DBElementDescriptor.JUNCTION_TYPE) {
	    c = 'j';
	}
	else if (dbe.getType() == DBElementDescriptor.KISSING_LOOP_TYPE) {
	    c = 'k';
	}
	String result = "" + c + "," + dbe.getOrder() + "," + (dbe.getId()+1);
	log.finest("toString of " + dbe + " : " + result);
	return result;
    }

    public boolean isValid() {
	return (blockList.size() > 0) && ((connections.size() > 0) || randomMode);
    }

    private String getConnectionString(DBElementConnectionDescriptor dbc) {
	int id1 = blockList.indexOf(dbc.getDescriptor1()) + 1;
	int id2 = blockList.indexOf(dbc.getDescriptor2()) + 1;
	String helixendName1 = dbc.getHelixendName1();
	String helixendName2 = dbc.getHelixendName2();
	int len = dbc.getBasePairCount();
	String result = "" + id1 + "," + id2 + "," + helixendName1 + "," + helixendName2 + "," + len;
	log.finest("toString of " + dbc + " : " + result);
	return result;
    }

    private String getBlocksString(List<DBElementDescriptor> dbes) {
	if (dbes.size() == 0) {
	    return "";
	}
	String result = getBlockString(dbes.get(0));
	assert result != null;
	if (result.equals("")) {
	    log.warning("Weird block descriptor: " + dbes.get(0));
	}
	for (int i = 1; i < dbes.size(); ++i) {
	    result = result + ";" + getBlockString(dbes.get(i));
	}
	return result;
    }

    private String getConnectionsString(List<DBElementConnectionDescriptor> dbes) {
	if (dbes.size() == 0) {
	    return "";
	}
	String result = getConnectionString(dbes.get(0));
	for (int i = 1; i < dbes.size(); ++i) {
	    result = result + ";" + getConnectionString(dbes.get(i));
	}
	return result;
    }

    private List<DBElementDescriptor> cloneBlockList() {
	List<DBElementDescriptor> result = new ArrayList<DBElementDescriptor>();
	for (int i = 0; i < blockList.size(); ++i) {
	    // result.add((DBElementDescriptor)(blockList.get(i).clone()));
	    result.add((DBElementDescriptor)(blockList.get(i)));
	}
	return result;
    }

    private List<DBElementConnectionDescriptor> cloneConnections() {
	List<DBElementConnectionDescriptor> result = new ArrayList<DBElementConnectionDescriptor>();
	for (int i = 0; i < connections.size(); ++i) {
	    // result.add((DBElementConnectionDescriptor)(connections.get(i).clone()));
	    result.add((DBElementConnectionDescriptor)(connections.get(i)));
	}
	return result;
    }

    public Object cloneDeep() {
	GrowCommand command = new GrowCommand(this.controller);
	command.blockList = cloneBlockList();
	command.connections = cloneConnections();
	command.graphFlag = this.graphFlag;
	command.rootName = new String(this.rootName);
	command.nameBase = new String(this.nameBase);
	command.startPosition = this.startPosition;
	command.generationCount = this.generationCount;
	command.addHelicesFlag = this.addHelicesFlag;
	command.avoidCollisionsFlag = this.avoidCollisionsFlag;
	command.ringClosureExportLimit = this.ringClosureExportLimit;
	command.ringClosureExportFileNameBase = this.ringClosureExportFileNameBase;
	command.stemRmsLimit = this.stemRmsLimit;
	command.topologies = this.topologies;
	command.minLen = this.minLen;
	command.maxLen = this.maxLen;
	command.randomMode = this.randomMode;

	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Simulated self-assembly of RNA building blocks. Correct usage: " + COMMAND_NAME + " root=graphobjectname blocks=j|k|m,order,index[;j|k|m,order,index] connect=BLOCK_ID1,BLOCK_ID2,HELIX_ID1,HELIX_ID2,BASEPAIRCOUNT[;BLOCK_ID3,BLOCK_ID4,HELIX_ID3,HELIX_ID4,BASEPAIRCOUNT ...] [pos=x,y,z] [gen=number][graph=false|true] [helices=true|false][ring-export filename][ring-export-limit][size-max=value][steric=true|false][topologies=top1|top2|...][rand=false|true]";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + helpOutput() + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     places specified junction at certain position in space." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	log.info("Command " + COMMAND_NAME + " called with parameters: " + toString());
	if (!isValid()) {
	    throw new CommandExecutionException("Grow command parameters are not valid for execution: " + toString());
	}

	try {
	    GrowConnectivity connectivity = new GrowConnectivity(blockList, connections, topologies, generationCount);
	    if (randomMode) {
		connectivity.synthesizeAllConnections(minLen, maxLen);
	    }
	    connectivity.setGraphFlag(graphFlag);
	    connectivity.setSizeMax(sizeMax);
	    assert topologies == null || topologies.size() == 0;
	    assert connectivity.getTopologies() == null || connectivity.getTopologies().size() == 0;
	    this.resultProperties = controller.growBuildingBlocks(connectivity, rootName, nameBase, startPosition,
								  addHelicesFlag, stemRmsLimit, avoidCollisionsFlag, ringClosureExportLimit, ringClosureExportFileNameBase,
								  randomMode);
	}
	catch (Object3DGraphControllerException e) {
	    throw new CommandExecutionException(e.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void readoutBlock(String block, int descriptorId) throws CommandExecutionException {
	if ((block == null) || (block.length() == 0)) {
	    throw new CommandExecutionException("Error parsing building blocks option. Insufficient block description.");
	}
	String[] words = block.split(","); // split in different building blocks
	if (words.length != 3) {
	    throw new CommandExecutionException("Error parsing building block: 4 elements expected for each block descriptor.");
	}
	int blockType = 0;
	int id = 0;
	int order = 0;
	if (words[0].equals("j")) {
	    blockType = DBElementDescriptor.JUNCTION_TYPE;
	}
	else if (words[0].equals("k")) {
	    blockType = DBElementDescriptor.KISSING_LOOP_TYPE;
	}
	else if (words[0].equals("m")) {
	    blockType = DBElementDescriptor.MOTIF_TYPE;
	}

	else {
	    throw new CommandExecutionException("Unknown building block type: " + words[0]);
	}
	try {
	    order = Integer.parseInt(words[1]);
	    id = Integer.parseInt(words[2]) -1; // convert to internal counting
	}
	catch (NumberFormatException nfe) {
	    throw new CommandExecutionException("Grow command: Error parsing number: " + nfe.getMessage());
	}
	DBElementDescriptor element = new DBElementDescriptor(order, id, blockType, descriptorId);
	blockList.add(element);
    }

    private void readoutBlocks(String blocks) throws CommandExecutionException {
	if ((blocks == null) || (blocks.length() == 0)) {
	    throw new CommandExecutionException("Error parsing building blocks option.");
	}
	String[] words = blocks.split(";"); // split in different building blocks
	for (int i = 0; i < words.length; ++i) {
	    readoutBlock(words[i], i);
	}
    }

    private void readoutConnection(String connection) throws CommandExecutionException {
	assert connection != null;
	String[] words = connection.split(","); // split in different building blocks
	if ((words.length != 5) && (words.length != 6)) {
	    throw new CommandExecutionException("5 or 6 elements expected in connect descriptor");
	}
	try {
	    int id1 = Integer.parseInt(words[0])-1;
	    int id2 = Integer.parseInt(words[1])-1;
	    String helixendName1 = words[2];
	    String helixendName2 = words[3];
	    if ((helixendName1.length() == 0) || (helixendName2.length() == 0)) {
		throw new CommandExecutionException("Undefined helix index in " + connection + " : " + helixendName1 + " " + helixendName2);
	    }
	    if (Character.isDigit(helixendName1.charAt(0))) {
		helixendName1 = "(hxend)" + helixendName1; // for convinience add (hxend) if missing
	    }
	    if (Character.isDigit(helixendName2.charAt(0))) {
		helixendName2 = "(hxend)" + helixendName2; // for convinience add (hxend) if missing
	    }
	    if ((id1  < 0) || (id1 >= blockList.size())) {
		throw new CommandExecutionException("Undefined block index in " + connection + " : " + id1);
	    }
	    if ((id2  < 0) || (id2 >= blockList.size())) {
		throw new CommandExecutionException("Undefined block index in " + connection + " : " + id2);
	    }
	    DBElementDescriptor descriptor1 = blockList.get(id1);
	    DBElementDescriptor descriptor2 = blockList.get(id2);
	    int basePairCount = Integer.parseInt(words[4]);
	    int delayStage = 0;
	    if (words.length > 5) {
		delayStage = Integer.parseInt(words[5]);
	    }
	    log.fine("Added descriptor " + connections.size() + " for ids: " + (id1 + 1) + " and " + (id2 + 1) + " "
		     + helixendName1 + " " + helixendName2 + " bp: " + basePairCount + " delay: " + delayStage);
	    DBElementConnectionDescriptor conn = new DBElementConnectionDescriptor(descriptor1, descriptor2, 
								   helixendName1, helixendName2, basePairCount);
	    conn.setDelayStage(delayStage);
	    connections.add(conn);
	}
	catch (NumberFormatException nfe) {
	    throw new CommandExecutionException("Number format exception while parsing connect option in grow command: " + nfe.getMessage());
	}
	catch (ArrayIndexOutOfBoundsException oobe) {
	    throw new CommandExecutionException("Index of out bounds exception while parsing connect option in grow command: " + oobe.getMessage());
	}
    }

    private void readoutConnections(String connections) throws CommandExecutionException {
	if ((connections == null) || (connections.length() == 0)) {
	    throw new CommandExecutionException("Error parsing connect option in grow command.");
	}
	String[] words = connections.split(";"); // split in different building blocks
	for (int i = 0; i < words.length; ++i) {
	    readoutConnection(words[i]);
	}
    }

    /** generates set of topology strings from "|" delimited list of topology strings */
    private Set<String> readoutTopologies(String value) {
	assert false; // currently not implemented
	assert value != null;
	String[] topwords = value.split("|");
	Set<String> result = new HashSet<String>();
	for (String s : topwords) {
	    if (s.length() > 0) {
		result.add(s);
	    }
	}
	return result;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() == 0) {
	    throw new CommandExecutionException(helpOutput());
	}
	try {
	    StringParameter randPar = (StringParameter)(getParameter("rand"));
	    if (randPar != null) { // true if "true", false if "false"
		randomMode = randPar.parseBoolean(); 
	    } 
	    StringParameter pBlocks = (StringParameter)(getParameter("blocks"));
	    if (pBlocks != null) {
		readoutBlocks(pBlocks.getValue());
	    }
	    else {
		    throw new CommandExecutionException("Building blocks have to be specified using the blocks option unless the rand=true option is specified.");
	    }

	    if (!randomMode) {
		StringParameter pConnect = (StringParameter)(getParameter("connect"));
		if (pConnect != null) {
		    readoutConnections(pConnect.getValue());
		}
		else {
		    throw new CommandExecutionException("Building block connections have to be specified using the connect option.");
		}
	    }
	StringParameter graphPar = (StringParameter)(getParameter("graph"));
	if (graphPar != null) { // true if "true", false if "false"
	    graphFlag = graphPar.parseBoolean(); 
	}
	StringParameter pRoot = (StringParameter)(getParameter("root"));
	if (pRoot !=  null) {
	    this.rootName = pRoot.getValue();
	}
	StringParameter pName = (StringParameter)(getParameter("name"));
	if (pName !=  null) {
	    this.nameBase = pName.getValue();
	}
	StringParameter ringPdbName = (StringParameter)(getParameter("ring-export"));
	log.info("ringPdbName = " + ringPdbName);
	if (ringPdbName !=  null) {
	    this.ringClosureExportFileNameBase = ringPdbName.getValue();
	}
	StringParameter ringPdbLimitName = (StringParameter)(getParameter("ring-export-limit"));
	if (ringPdbLimitName !=  null) {
	    try {
		this.ringClosureExportLimit = Double.parseDouble(ringPdbLimitName.getValue());
	    }
	    catch (NumberFormatException nfe) {
		throw new CommandExecutionException("Number format exception while parsing ring closure export limit: " + nfe.getMessage());
	    }
	}
	StringParameter stemRmsLimitName = (StringParameter)(getParameter("stem-rms"));
	if (stemRmsLimitName !=  null) {
	    try {
		this.stemRmsLimit = Double.parseDouble(stemRmsLimitName.getValue());
	    }
	    catch (NumberFormatException nfe) {
		throw new CommandExecutionException("Number format exception while parsing stem RMS limit: " + nfe.getMessage());
	    }
	}
	StringParameter sizeMaxString = (StringParameter)(getParameter("size-max"));
	if (sizeMaxString !=  null) {
	    try {
		this.sizeMax = Integer.parseInt(sizeMaxString.getValue());
	    }
	    catch (NumberFormatException nfe) {
		throw new CommandExecutionException("Number format exception while parsing size-max parameter: " + nfe.getMessage());
	    }
	}
	StringParameter topologyParameter = (StringParameter)(getParameter("topologies"));
	if (topologyParameter != null) {
	    this.topologies = readoutTopologies(topologyParameter.getValue());
	}
	StringParameter hName = (StringParameter)(getParameter("helices"));
	if (hName !=  null) {
	    addHelicesFlag = hName.parseBoolean();
	}
	hName = (StringParameter)(getParameter("steric"));
	if (hName !=  null) {
	    avoidCollisionsFlag = hName.parseBoolean();
	}
	StringParameter genName = (StringParameter)(getParameter("gen"));
	if (genName != null) {
	    generationCount = Integer.parseInt(genName.getValue());
	}
	}
	catch (NumberFormatException nfe) {
	    throw new CommandExecutionException("Number format exception while parsing number of base pairs (option bp): " + nfe.getMessage());
	}
	catch (ParsingException pe) {
	    throw new CommandExecutionException("Parsing exception in grow command: " + pe.getMessage());
	}
    }

    private String boolString(boolean b) {
	if (b) {
	    return "true";
	}
	return "false";
    }

    /*
    public String toString() {
	String result = COMMAND_NAME;
	if (blockList.size() > 0) {
	    result = result + " blocks=" + getBlocksString(blockList);
	}
	if (connections.size() > 0) {
	    result = result + " connect=" + getConnectionsString(connections);
	}
	result = result + " helices=" + boolString(addHelicesFlag) + " steric=" + boolString(avoidCollisionsFlag)
	    + " ring-export=" + ringClosureExportFileNameBase + " ring-export-limit=" + ringClosureExportLimit
	    + " gen=" + generationCount + " name=" + nameBase + " root=" + rootName;
	return result;
    }
    */

}
