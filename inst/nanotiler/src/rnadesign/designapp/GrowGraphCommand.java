package rnadesign.designapp;

import java.io.PrintStream;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.Properties;
import generaltools.ParsingException; // replace with ParseException 
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import rnadesign.rnamodel.DBElementDescriptor;
import rnadesign.rnamodel.DBElementConnectionDescriptor;
import rnadesign.rnamodel.GrowConnectivity;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import tools3d.Vector3D;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class GrowGraphCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "growgraph";

    private Object3DGraphController controller;
    private List<DBElementDescriptor> blockList  = new ArrayList<DBElementDescriptor>();
    private boolean buildMode = true;
    private String rootName = "root";
    private String nameBase = "g_";
    private int generationCount = 5;
    private int maxBlocks = 3; // maxmum number of non-equivalent building blocks
    private int maxConnections = 4; // maximum number of non-equivalent connections to non-equivalent building blocks
    private int helixVariation = 4; // how much to vary helix length (+-, 2n+1 lengths are tried per helix for helixVariation=n)
    private double ringClosureLimit = 15.0;

    public GrowGraphCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	GrowGraphCommand command = new GrowGraphCommand(this.controller);
	command.buildMode = this.buildMode;
	command.rootName = new String(this.rootName);
	command.nameBase = new String(this.nameBase);
	command.blockList = cloneBlockList();
	command.generationCount = this.generationCount;
	command.maxBlocks = this.maxBlocks;
	command.maxConnections = this.maxConnections;
	command.helixVariation = this.helixVariation;
	command.ringClosureLimit = this.ringClosureLimit;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    private List<DBElementDescriptor> cloneBlockList() {
	if (blockList == null) {
	    return null;
	}
	List<DBElementDescriptor> result = new ArrayList<DBElementDescriptor>();
	for (int i = 0; i < blockList.size(); ++i) {
	    // result.add((DBElementDescriptor)(blockList.get(i).clone()));
	    result.add((DBElementDescriptor)(blockList.get(i)));
	}
	return result;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " blocks=j|k,order,index[;j|k,order,index] [block-max=number][build=true|false][conn-max=number][pos=x,y,z] [gen=number][name=name][ring-limit=value][root=name][var=number]";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + helpOutput();
	helpText += "DESCRIPTION" + NEWLINE +
	    "     This command generates a set of simulated self-assembly structures that are compatible with a supplied graph structure. See also at options for grow command." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	try {
	    this.resultProperties = controller.growBuildingBlocksFramework(rootName, nameBase, generationCount, blockList, maxBlocks, maxConnections, helixVariation,
									   buildMode,ringClosureLimit);
	    log.info("Result of grow graph command: " + this.resultProperties);
	}
	catch (Object3DGraphControllerException e) {
	    throw new CommandExecutionException(e.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() == 0) {
	    throw new CommandExecutionException(helpOutput());
	}
	try {
	StringParameter pBlocks = (StringParameter)(getParameter("blocks"));
	if (pBlocks != null) {
	    readoutBlocks(pBlocks.getValue());
	}
	StringParameter pRoot = (StringParameter)(getParameter("root"));
	if (pRoot !=  null) {
	    this.rootName = pRoot.getValue();
	}
	StringParameter pName = (StringParameter)(getParameter("name"));
	if (pName !=  null) {
	    this.nameBase = pName.getValue();
	}
	StringParameter buildName = (StringParameter)(getParameter("build"));
	if (buildName !=  null) {
	    this.buildMode = buildName.parseBoolean();
	}
	    StringParameter genName = (StringParameter)(getParameter("gen"));
	    if (genName != null) {
		this.generationCount = Integer.parseInt(genName.getValue());
	    }
	    StringParameter maxBlockName = (StringParameter)(getParameter("block-max"));
	    if (maxBlockName != null) {
		this.maxBlocks = Integer.parseInt(maxBlockName.getValue());
	    }
	    StringParameter varName = (StringParameter)(getParameter("var"));
	    if (varName != null) {
		this.helixVariation = Integer.parseInt(varName.getValue());
	    }
	    StringParameter maxConnectionsName = (StringParameter)(getParameter("conn-max"));
	    if (maxConnectionsName != null) {
		this.maxConnections = Integer.parseInt(maxConnectionsName.getValue());
	    }
	    StringParameter ringPdbLimitName = (StringParameter)(getParameter("ring-limit"));
	    if (ringPdbLimitName !=  null) {
		this.ringClosureLimit = Double.parseDouble(ringPdbLimitName.getValue());
	    }
	}
	catch (ParsingException pe) {
	    throw new CommandExecutionException("Parsing exception in " + COMMAND_NAME + " command: " + pe.getMessage());
	}
	catch (NumberFormatException nfe) {
	    throw new CommandExecutionException("Number format exception while parsing number of base pairs (option bp): " + nfe.getMessage());
	}
    }


    private void readoutBlock(String block, int descriptorId) throws CommandExecutionException {
	if ((block == null) || (block.length() == 0)) {
	    throw new CommandExecutionException("Error parsing building blocks option. Insufficient block description.");
	}
	String[] words = block.split(","); // split in different building blocks
	if (words.length != 3) {
	    throw new CommandExecutionException("Error parsing building block: 4 elements expected for each block descriptor.");
	}
	int blockType = 0;
	int id = 0;
	int order = 0;
	if (words[0].equals("j")) {
	    blockType = DBElementDescriptor.JUNCTION_TYPE;
	}
	else if (words[0].equals("k")) {
	    blockType = DBElementDescriptor.KISSING_LOOP_TYPE;
	}
	else {
	    throw new CommandExecutionException("Unknown building block type: " + words[0]);
	}
	try {
	    order = Integer.parseInt(words[1]);
	    id = Integer.parseInt(words[2]) -1; // convert to internal counting
	}
	catch (NumberFormatException nfe) {
	    throw new CommandExecutionException("Grow command: Error parsing number: " + nfe.getMessage());
	}
	DBElementDescriptor element = new DBElementDescriptor(order, id, blockType, descriptorId);
	blockList.add(element);
    }

    private void readoutBlocks(String blocks) throws CommandExecutionException {
	if ((blocks == null) || (blocks.length() == 0)) {
	    throw new CommandExecutionException("Error parsing building blocks option.");
	}
	String[] words = blocks.split(";"); // split in different building blocks
	for (int i = 0; i < words.length; ++i) {
	    readoutBlock(words[i], i);
	}
    }

}
