package rnadesign.designapp;

import java.io.PrintStream;
import java.util.List;
import java.util.ArrayList;

import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import rnadesign.rnamodel.DBElementDescriptor;
import rnadesign.rnamodel.DBElementConnectionDescriptor;
import rnadesign.rnamodel.GrowConnectivity;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import tools3d.Vector3D;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class GrowLoopCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "growloop";

    private Object3DGraphController controller;

    private List<DBElementDescriptor> blockList = new ArrayList<DBElementDescriptor>();
    private List<DBElementConnectionDescriptor> connections = new ArrayList<DBElementConnectionDescriptor>();
    private String rootName = "root";
    private String nameBase = "g_";
    private Vector3D startPosition = new Vector3D(0.0, 0.0, 0.0);
    private int generationCount = 1;
    private double stemRmsLimit = 10.0;
    private boolean addHelicesFlag = false;
    private boolean avoidCollisionsFlag = false;
    private boolean randomMode = false; // TODO: implement "true" case

    public GrowLoopCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    private List<DBElementDescriptor> cloneBlockList() {
	List<DBElementDescriptor> result = new ArrayList<DBElementDescriptor>();
	for (int i = 0; i < blockList.size(); ++i) {
	    result.add((DBElementDescriptor)(blockList.get(i).clone()));
	}
	return result;
    }

    private List<DBElementConnectionDescriptor> cloneConnections() {
	List<DBElementConnectionDescriptor> result = new ArrayList<DBElementConnectionDescriptor>();
	for (int i = 0; i < connections.size(); ++i) {
	    result.add((DBElementConnectionDescriptor)(connections.get(i).clone()));
	}
	return result;
    }

    public Object cloneDeep() {
	GrowLoopCommand command = new GrowLoopCommand(this.controller);
	command.blockList = cloneBlockList();
	command.connections = cloneConnections();
	command.rootName = new String(this.rootName);
	command.nameBase = new String(this.nameBase);
	command.startPosition = this.startPosition;
	command.generationCount = this.generationCount;
	command.addHelicesFlag = this.addHelicesFlag;
	command.avoidCollisionsFlag = this.avoidCollisionsFlag;
	command.randomMode = this.randomMode;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " block=j|k,order,index[;j|k,order,index] connect=1.1,1.2,3;1.3,2.1,4 [pos=x,y,z] [gen=number][steric=true|false] [helices=true|false]";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " junction newparentname newname order id x y z [strand-ending]" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     places specified junction at certain position in space." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	try {
	    GrowConnectivity connectivity = new GrowConnectivity(blockList, connections, null, generationCount);
	    connectivity.setNumGenerations(generationCount);
	    controller.growBuildingBlocks(connectivity, rootName, nameBase, startPosition, 
					  addHelicesFlag, stemRmsLimit, avoidCollisionsFlag, 10.0, "found_ring", randomMode );
	}
	catch (Object3DGraphControllerException e) {
	    throw new CommandExecutionException(e.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void readoutBlock(String block, int descriptorId) throws CommandExecutionException {
	if ((block == null) || (block.length() == 0)) {
	    throw new CommandExecutionException("Error parsing building blocks option. Insufficient block description.");
	}
	String[] words = block.split(","); // split in different building blocks
	if (words.length != 3) {
	    throw new CommandExecutionException("Error parsing building block: 4 elements expected for each block descriptor.");
	}
	int blockType = 0;
	int id = 0;
	int order = 0;
	if (words[0].equals("j")) {
	    blockType = DBElementDescriptor.JUNCTION_TYPE;
	}
	else if (words[0].equals("k")) {
	    blockType = DBElementDescriptor.KISSING_LOOP_TYPE;
	}
	else {
	    throw new CommandExecutionException("Unknown building block type: " + words[0]);
	}
	try {
	    order = Integer.parseInt(words[1]);
	    id = Integer.parseInt(words[2]) -1; // convert to internal counting
	}
	catch (NumberFormatException nfe) {
	    throw new CommandExecutionException("Grow command: Error parsing number: " + nfe.getMessage());
	}
	DBElementDescriptor element = new DBElementDescriptor(order, id, blockType, descriptorId);
	blockList.add(element);
    }

    private void readoutBlocks(String blocks) throws CommandExecutionException {
	if ((blocks == null) || (blocks.length() == 0)) {
	    throw new CommandExecutionException("Error parsing building blocks option.");
	}
	String[] words = blocks.split(";"); // split in different building blocks
	for (int i = 0; i < words.length; ++i) {
	    readoutBlock(words[i], i);
	}
    }

    private void readoutConnection(String connection) throws CommandExecutionException {
	assert connection != null;
	String[] words = connection.split(","); // split in different building blocks
	if (words.length != 5) {
	    throw new CommandExecutionException("5 elements expected in connect descriptor");
	}
	try {
	    int id1 = Integer.parseInt(words[0])-1;
	    int id2 = Integer.parseInt(words[1])-1;
	    String helixendName1 = words[2];
	    String helixendName2 = words[3];
	    DBElementDescriptor descriptor1 = blockList.get(id1);
	    DBElementDescriptor descriptor2 = blockList.get(id2);
	    int basePairCount = Integer.parseInt(words[4]);
	    connections.add(new DBElementConnectionDescriptor(descriptor1, descriptor2, 
							      helixendName1, helixendName2, basePairCount));
	}
	catch (NumberFormatException nfe) {
	    throw new CommandExecutionException("Number format exception while parsing connect option in grow command: " + nfe.getMessage());
	}
	catch (ArrayIndexOutOfBoundsException oobe) {
	    throw new CommandExecutionException("Index of out bounds exception while parsing connect option in grow command: " + oobe.getMessage());
	}
    }

    private void readoutConnections(String connections) throws CommandExecutionException {
	if ((connections == null) || (connections.length() == 0)) {
	    throw new CommandExecutionException("Error parsing building blocks option.");
	}
	String[] words = connections.split(";"); // split in different building blocks
	for (int i = 0; i < words.length; ++i) {
	    readoutConnection(words[i]);
	}
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() == 0) {
	    throw new CommandExecutionException(helpOutput());
	}
	StringParameter pBlocks = (StringParameter)(getParameter("blocks"));
	if (pBlocks != null) {
	    readoutBlocks(pBlocks.getValue());
	}
	else {
	    throw new CommandExecutionException("Building blocks have to be specified using the blocks option.");
	}
	StringParameter pConnect = (StringParameter)(getParameter("connect"));
	if (pConnect != null) {
	    readoutConnections(pConnect.getValue());
	}
 	else {
 	    throw new CommandExecutionException("Building block connections have to be specified using the connect option.");
 	}
	StringParameter pRoot = (StringParameter)(getParameter("root"));
	if (pRoot !=  null) {
	    this.rootName = pRoot.getValue();
	}
	StringParameter pName = (StringParameter)(getParameter("name"));
	if (pName !=  null) {
	    this.nameBase = pName.getValue();
	}
	StringParameter hName = (StringParameter)(getParameter("helices"));
	if (hName !=  null) {
	    if ("true".equals(hName.getValue())) {
		addHelicesFlag = true;
	    }
	    else if ("false".equals(hName.getValue())) {
		addHelicesFlag = false;
	    }
	    else {
		throw new CommandExecutionException("helices option has values of either true of false");
	    }
	}
	hName = (StringParameter)(getParameter("steric"));
	if (hName !=  null) {
	    if ("true".equals(hName.getValue())) {
		avoidCollisionsFlag = true;
	    }
	    else if ("false".equals(hName.getValue())) {
		avoidCollisionsFlag = false;
	    }
	    else {
		throw new CommandExecutionException("steric option has values of either true of false");
	    }
	}
	try {
	    StringParameter genName = (StringParameter)(getParameter("gen"));
	    if (genName != null) {
		generationCount = Integer.parseInt(genName.getValue());
	    }
	}
	catch (NumberFormatException nfe) {
	    throw new CommandExecutionException("Number format exception while parsing number of base pairs (option bp): " + nfe.getMessage());
	}
    }

}
