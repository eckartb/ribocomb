package rnadesign.designapp;

import java.io.PrintStream;

import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class HelixConstraintCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "genhelixconstraint";

    private Object3DGraphController controller;
    private String name1;
    private String name2;
    private String name;
    private double rms = 3.0;
    private int symId1 = 0;
    private int symId2 = 0;
    private int basePairMin = 0;
    private int basePairMax = 0;

    public HelixConstraintCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	HelixConstraintCommand command = new HelixConstraintCommand(this.controller);
	command.name1 = this.name1;
	command.name2 = this.name2;
	command.name = this.name;
	command.rms = rms;
	command.basePairMin = basePairMin;
	command.basePairMax = basePairMax;
	command.symId1 = symId1;
	command.symId2 = symId2;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " name1 name2 [rms=<value>][bp=<value>][name=<name>]";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " helixdescriptor-1 helixdescriptor-2 [name=NAME][rms=value][bp=value][sym1=ID][sym2=ID]" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Adds a helical constraint between two helix descriptors (BranchDescriptor3D)." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	try {
	    controller.addHelixConstraint(name1, name2, basePairMin, basePairMax, rms, symId1, symId2, name);
	}
	catch (Object3DGraphControllerException e) {
	    throw new CommandExecutionException(e.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if ((getParameterCount() < 2)) {
	    throw new CommandExecutionException(helpOutput());
	}
	StringParameter p0 = (StringParameter)(getParameter(0));
	name1 = p0.getValue();
	StringParameter p1 = (StringParameter)(getParameter(1));
	name2 = p1.getValue();
	try {
	    StringParameter rmsParameter = (StringParameter)(getParameter("rms"));
	    if (rmsParameter != null) {
		this.rms = Double.parseDouble(rmsParameter.getValue());
	    }
	    StringParameter bpMinParameter = (StringParameter)(getParameter("bp"));
	    if (bpMinParameter != null) {
		this.basePairMin = Integer.parseInt(bpMinParameter.getValue());
		this.basePairMax = this.basePairMin;
	    }
	    StringParameter sp1 = (StringParameter)(getParameter("sym1"));
	    if (sp1 != null) {
		symId1 = Integer.parseInt(sp1.getValue());
	    }
	    StringParameter sp2 = (StringParameter)(getParameter("sym2"));
	    if (sp2 != null) {
		symId2 = Integer.parseInt(sp2.getValue());
	    }
	    StringParameter namePar = (StringParameter)(getParameter("name"));
	    if (namePar != null) {
		this.name = namePar.getValue();
	    }
	}
	catch (NumberFormatException nfe) {
	    throw new CommandExecutionException("Error parsing number value: " + nfe.getMessage());
	}
    }
    
}
