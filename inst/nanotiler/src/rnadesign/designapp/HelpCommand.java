
package rnadesign.designapp;

import java.io.PrintStream;

import commandtools.AbstractCommand;
import commandtools.BadSyntaxException;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import commandtools.UnknownCommandException;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class HelpCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "help";

    private NanoTilerInterpreter interpreter;
    private PrintStream ps;
    private String helpCommand;
    
    public HelpCommand(PrintStream ps, NanoTilerInterpreter interpreter) {
	super(COMMAND_NAME);
	assert interpreter != null;
	assert ps != null;
	this.ps = ps;
	this.interpreter = interpreter;
    }

    public Object cloneDeep() {
	HelpCommand command = new HelpCommand(this.ps, this.interpreter);
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "User help for individual commands. Correct usage: " + COMMAND_NAME + " [COMMAND-NAME] . Try also commands \"man\" and \"demo\"";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " [COMMAND]" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Help command gives list of known commands or short help for a specific command.";
	helpText += NEWLINE + "     Currently known commands: " + interpreter.getKnownCommandNames() + NEWLINE;
	helpText += "Consult the NanoTiler user manual located in $NANOTILER_HOME/doc for more information." + NEWLINE;
	helpText += "Commands related to help: demo [all|NAME], help [COMMAND], man [COMMAND]";

	return helpText;
    }

    public String getMessage() {
	return "Known commands: " + interpreter.getKnownCommandNames() + NEWLINE +
	    "Consult the NanoTiler user manual located in $NANOTILER_HOME/doc for more information." + NEWLINE +
	    "Commands related to help: demo [all|NAME], help [COMMAND], man [COMMAND]";
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	if (helpCommand == null) {
	    ps.println(getMessage());
	}
	else {
	    ps.println(getOutput(helpCommand));
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private String getOutput(String helpCommand) {
	Command command = new HelpCommand(ps, interpreter);
	try {
	    try {
		command = interpreter.interpretLine(helpCommand);
	    }
	    catch (UnknownCommandException ue) {
		log.severe("Unknown command!: " + ue.getMessage());
		return null;
	    }
	}
	catch (BadSyntaxException e) {
	    log.severe("Bad syntax!: " + e.getMessage());
	    return null;
	}
	return command.getShortHelpText();
    }

    private void prepareReadout() throws CommandExecutionException {
	// if (getParameterCount() == 0) {
	// throw new CommandExecutionException(helpOutput()); 
	// }
	if (getParameterCount() == 0) {
	    return;
	}
	else if (getParameterCount() == 1) {
	    StringParameter p0 = (StringParameter)getParameter(0);
	    helpCommand = p0.toString();
	}
	else {
	    throw new CommandExecutionException(helpOutput());
	}
    }

}
