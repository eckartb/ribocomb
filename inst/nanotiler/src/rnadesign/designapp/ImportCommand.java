package rnadesign.designapp;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;
import generaltools.ParsingException;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandException;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerConstants;
import rnadesign.rnamodel.FitParameters;
import rnadesign.rnamodel.FragmentGridTiler; // TODO : avoid
import tools3d.Vector3D;
import tools3d.objects3d.Object3DIOException; // TODO : bad style! should only deal with exceptions defined in controller package !
import org.testng.annotations.*; // for testing

import static rnadesign.designapp.PackageConstants.DEG2RAD;
import static rnadesign.designapp.PackageConstants.LOGFILE_DEFAULT;
import static rnadesign.designapp.PackageConstants.NEWLINE;

public class ImportCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "import";
    public static final String POINTS_EXTENSION = ".points";
    public static final String POINTS2_EXTENSION = ".points2";
    public static final String PDB_EXTENSION = ".pdb";

    private String fileName;
    private boolean findStemsFlag = false; // true;
    private boolean addJunctionsFlag = true;
    private String importName;
    private int format = -1;
    private char sequenceChar = 'N';
    private int rerunMax = -1;
    private double scaleX = 1.0;
    private double scaleY = 1.0;
    private double scaleZ = 1.0;
    private double sx = 0.0;
    private double sy = 0.0;
    private double sz = 0.0;
    private boolean addStemsFlag = false; // true;
    private boolean onlyFirstPathMode = false;
    private Object3DGraphController controller;
    private double stemAngleLimit = -1.0;
    private double junctionAngleLimit = -1.0;
    private double junctionAngleWeight = -1.0;
    private double stemRmsLimit = 3.0;
    private double junctionRmsLimit = 15.0;
    private static Set allowedParameterNames = generateAllowedParameterNameSet();
    private Logger log = Logger.getLogger(LOGFILE_DEFAULT);
    private boolean residueRenumberMode = false;

    public ImportCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }
    
    /** generates set with allowed parameter names */
    private static Set generateAllowedParameterNameSet() {
	Set<String> set = new HashSet<String>();
	set.add(""); // allow unnamed parameters
	set.add("name");
	set.add("addjunctions");
	set.add("addstems");
	set.add("findstems");
	set.add("format");
	set.add("stemrms");
	set.add("junctionrms");
	set.add("stemangle");
	set.add("junctionangle");
	set.add("junctionangleweight");
	set.add("renumber");
	set.add("rerun");
	set.add("scale");
	set.add("scalex");
	set.add("scaley");
	set.add("scalez");
	return set;
    }

    public Object cloneDeep() {
	ImportCommand command = new ImportCommand(controller);
	command.fileName = this.fileName;
	command.findStemsFlag = this.findStemsFlag;
	command.addJunctionsFlag = this.addJunctionsFlag;
	command.importName = this.importName;
	command.sequenceChar = this.sequenceChar;
	command.residueRenumberMode = this.residueRenumberMode;
	command.rerunMax = this.rerunMax;
	command.scaleX = this.scaleX;
	command.scaleY = this.scaleY;
	command.scaleZ = this.scaleZ;
	command.sx = this.sx;
	command.sy = this.sy;
	command.sz = this.sz;
	command.addStemsFlag = this.addStemsFlag;
	command.onlyFirstPathMode = this.onlyFirstPathMode;
	command.stemAngleLimit = this.stemAngleLimit;
	command.junctionAngleLimit = this.junctionAngleLimit;
	command.junctionAngleWeight = this.junctionAngleWeight;
	command.stemRmsLimit = this.stemRmsLimit;
	command.junctionRmsLimit = this.junctionRmsLimit;
	command.residueRenumberMode = this.residueRenumberMode;
	command.allowedParameterNames = this.allowedParameterNames;
	log.fine("Copying " + getParameterCount() + " parameters!");
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	assert (this.controller != null);
	prepareReadout();
	log.info("Started ImportCommand.executeWithoutUndo");
	FragmentGridTiler fgTiler = controller.getFragmentGridTiler();
	try {
	    FileInputStream fis;
	    fis = new FileInputStream(fileName);    
	    if (format < 0) {
		if( fileName.toLowerCase().endsWith(POINTS_EXTENSION) ) {
		    format = Object3DGraphControllerConstants.POINTSET_FORMAT;
		}
		else if( fileName.toLowerCase().endsWith(POINTS2_EXTENSION) ) {
		    format = Object3DGraphControllerConstants.POINTSET2_FORMAT;
		}
		else if( fileName.toLowerCase().endsWith(PDB_EXTENSION) ) {
		    format = Object3DGraphControllerConstants.PDB_RNAVIEW_FORMAT;
		    if (residueRenumberMode) {
			format = Object3DGraphControllerConstants.PDB_RNAVIEW_RENUMBER_FORMAT;
		    }
		}
		
		else {
		    throw new CommandExecutionException("Unknown import format extension: " + fileName);
		}
	    }
	    FitParameters stemFitParameters = fgTiler.getStemFitParameters();
	    if (stemRmsLimit > 0.0) {
		stemFitParameters.setRmsLimit(stemRmsLimit); 		// TODO : avoid use of quasi-global variable
	    }
	    if (stemAngleLimit > 0.0) {
		stemFitParameters.setAngleLimit(stemAngleLimit); 	     
	    }
	    controller.getFragmentGridTiler().setStemFitParameters(stemFitParameters);
	    FitParameters junctionFitParameters = fgTiler.getJunctionFitParameters();
	    if (junctionAngleLimit > 0.0) {
		junctionFitParameters.setAngleLimit(junctionAngleLimit);  // TODO : avoid use of quasi-global variable
	    }
	    if (junctionAngleWeight >= 0.0) {
		junctionFitParameters.setAngleWeight(junctionAngleWeight);
		assert junctionFitParameters.getAngleWeight() == junctionAngleWeight;
	    }
	    else {
		log.fine("Junction angle weight was not set: " + junctionAngleWeight);
	    }
	    if (junctionRmsLimit > 0.0) {
		junctionFitParameters.setRmsLimit(junctionRmsLimit);  // TODO : avoid use of quasi-global variable
		assert junctionFitParameters.getRmsLimit() == junctionRmsLimit;
	    }
	    fgTiler.setJunctionFitParameters(junctionFitParameters);
	    if (this.rerunMax > 0) {
		fgTiler.setRerunMax(this.rerunMax);
	    }
	    assert fgTiler.getJunctionFitParameters() == junctionFitParameters;
	    assert controller.getFragmentGridTiler() == fgTiler; // check if references are equal
	    FitParameters juncFitTestPrm = controller.getFragmentGridTiler().getJunctionFitParameters();

	    assert ((junctionRmsLimit <= 0.0) || (juncFitTestPrm.getRmsLimit() == junctionRmsLimit));
	    if (getParameterCount() > 3) {
		log.info("Effective junction fit parameters: " + juncFitTestPrm + " should be: " + junctionFitParameters);
	    }
	    else {
		log.fine("Effective junction fit parameters: " + juncFitTestPrm + " should be: " + junctionFitParameters);
	    }
	    // assert (juncFitTestPrm.getAngleWeight() == junctionAngleWeight);
	    controller.readAndAdd(fis, importName, new Vector3D(scaleX, scaleY, scaleZ), 
				  new Vector3D(sx, sy, sz), 
				  format,
				  addStemsFlag, sequenceChar, onlyFirstPathMode,
				  findStemsFlag, addJunctionsFlag);
	}
	catch (Object3DIOException exc) { // TODO : should only deal with exceptions defined in controller package !
	    throw new CommandExecutionException("Error reading file: " + exc.getMessage());
	}
	catch (IOException exc) {
	    // replace with better error window
	    throw new CommandExecutionException("Error opening file " + fileName);
	}
	log.info("Finished ImportCommand.executeWithoutUndo");
    }

    /** returns true if parameter name is allowed */
    public boolean isAllowedParameterName(String name) {
	if (name.equals("string")) {
	    return true; // allow unnames parameters
	}
	return allowedParameterNames.contains(name);
    }
    
    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }
    
    public String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " fileName [addstems=true|false][name=newname][addjunctions=true|false][findstems=false|true][format=rnaview|pdb|points|points2|renumber][junctionangle=value][junctionangleweight=value][junctionrms=value][renumber=true|false][rerun=number][scale=value][stemangle][stemrms=value]";
    }
    
    public String getShortHelpText() { return helpOutput(); }
    
    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " FILENAME [options]" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Import command allows user to import a file." + NEWLINE;
	helpText += "     TODO: Also, unnamed options are allowed.";
	helpText += "OPTIONS" + NEWLINE;
	helpText += "     name=NAME" + NEWLINE + "          Set the name of the imported file." + NEWLINE + NEWLINE;
	helpText += "     addstems=BOOLEAN" + NEWLINE + "          Synthesize stems for graph structure." + NEWLINE + NEWLINE;

	helpText += "     findstems=BOOLEAN" + NEWLINE + "          Find stems using atom coordinates. Default: false" + NEWLINE + NEWLINE;

	helpText += "     addjunctions=BOOLEAN" + NEWLINE + "       Add junctions using stems generated from atom coordinates." + NEWLINE + NEWLINE;

	helpText += "     format=rnaview|pdb|points|points2|renumber" + NEWLINE
	    + "        Specificif format specification. rnaview: add RNAview info before PDB file (default), pdb: read normal pdb file without base pair info, points: read points file point2: read points2 file (not supported anymore), renumber: read Rnaview + PDB file, rename residues" + NEWLINE;

	helpText += "     renumber=BOOLEAN" + NEWLINE + "          if true, renumber residues while reading file." + NEWLINE + NEWLINE;

	helpText += "     stemrms=NUM" + NEWLINE + "          Set the stem RMS limit to NUM." + NEWLINE + NEWLINE;

	helpText += "     junctionrms=NUM" + NEWLINE + "          Set the junction RMS limit to NUM." + NEWLINE + NEWLINE;

	helpText += "     stemangle=NUM" + NEWLINE + "          Set the stem angle limit to NUM." + NEWLINE + NEWLINE;

	helpText += "     junctionangle=NUM" + NEWLINE + "          Set the junction angle limit to NUM." + NEWLINE + NEWLINE;
	helpText += "     junctionangleweight=NUM" + NEWLINE + "          Set the weight junction angle to NUM." + NEWLINE + NEWLINE;
	helpText += "     rerun=NUM" + NEWLINE + "          Set the number of reruns to try until entire graph is covered." + NEWLINE + NEWLINE;
	helpText += "     scale=NUM" + NEWLINE + "          Scale the entire imported file by NUM." + NEWLINE + NEWLINE;
	helpText += "     scalex=NUM" + NEWLINE + "          X scale factor." + NEWLINE + NEWLINE;
	helpText += "     scaley=NUM" + NEWLINE + "          Y scale factor." + NEWLINE + NEWLINE;
	helpText += "     scalez=NUM" + NEWLINE + "          Z scale factor." + NEWLINE + NEWLINE;
	return helpText;
    }

    /** Converts strings "rnaview", "pdb", "points", "points2", "renumber"
     * into corresponding format constants from Object3DGraphControllerConstants */
    private int parseFormat(String s) throws ParsingException {
	assert s != null;
	if (s.length() == 0) {
	    throw new ParsingException("Error parsing empty string as import format string.");
	}
	if (Character.isDigit(s.charAt(0))) {
	    try {
		int result = Integer.parseInt(s);
		return result;
	    }
	    catch (NumberFormatException nfe) {
		throw new ParsingException("Error parsing numerical import format: " + nfe.getMessage());
	    }
	}
	s = s.toLowerCase();	
	if (s.equals("rnaview")) {
	    return Object3DGraphControllerConstants.PDB_RNAVIEW_FORMAT;
	}
	else if (s.equals("pdb")) {
	    return Object3DGraphControllerConstants.PDB_FORMAT;
	}
	else if (s.equals("points")) {
	    return Object3DGraphControllerConstants.POINTSET_FORMAT;
	}
	else if (s.equals("points2")) {
	    return Object3DGraphControllerConstants.PDB_RNAVIEW_FORMAT;
	}
	else if (s.equals("renumber")) {
	    return Object3DGraphControllerConstants.PDB_RNAVIEW_RENUMBER_FORMAT;
	}
	else if (!s.equals("Some_rediciulus_value_as_workaround")) {
	    throw new ParsingException("Could not interpret format idenfifier: " + s 
				       + " allowed: rnaview|pdb|points|points2|renumber");
	}
	assert false; // should never be here
	return -1;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() == 0) {
	    throw new CommandExecutionException(helpOutput());
	}
	log.fine("Number of parameters: " + getParameterCount());
	for (int i = 0; i < getParameterCount(); ++i) {
	    log.fine("Parameter: " + (i+1) + getParameter(i));
	}
 	if (!checkParameterNames()) {
 	    throw new CommandExecutionException("bad parameter names detected in import!");
 	}
	assert (getParameterCount() > 0);
	assert getParameter(0) instanceof StringParameter;
	try {
	    StringParameter stringParameter = (StringParameter)(getParameter(0));
	    fileName = stringParameter.getValue();
	    Command nameParameter = getParameter("name");
	    if (nameParameter != null) {
		assert (nameParameter instanceof StringParameter);
		importName = ((StringParameter)nameParameter).getValue();
	    }
	    else {
		importName = "import";
	    }
	    StringParameter addStemsParameter = (StringParameter)(getParameter("addstems"));
	    if (addStemsParameter != null) {
		String value = addStemsParameter.getValue().toLowerCase();
		if (value.equals("true")) {
		    addStemsFlag = true;
		}
		else if (value.equals("false")) {
		    addStemsFlag = false;
		}
		else {
		    throw new CommandExecutionException("bad syntax, expected addstems=true|false");
		}
		log.fine("Setting addstems parameter from " 
			 + addStemsParameter.getValue() + " to " + addStemsFlag);
	    }
	    else {
		log.fine("addstems parameter was not specified.");
	    }
	    Command addJunctionsParameter = getParameter("addjunctions");
	    if (addJunctionsParameter != null) {
		addJunctionsFlag = ((StringParameter)addJunctionsParameter).parseBoolean();
	    }
	    Command findStemsParameter = getParameter("findstems");
	    if (findStemsParameter != null) {
		findStemsFlag = ((StringParameter)findStemsParameter).parseBoolean();
	    }
	    Command formatParameter = getParameter("format");
	    if (formatParameter != null) {
		format = parseFormat(((StringParameter)formatParameter).getValue());
		//	throw new CommandExecutionException("Unknown import format extension: " + formatParameter);
	    }
	    Command stemRmsLimitParameter = getParameter("stemrms");
	    if (stemRmsLimitParameter != null) {
		stemRmsLimit = Double.parseDouble(((StringParameter)stemRmsLimitParameter).getValue());
		log.fine("stemrms is part of import command: " + stemRmsLimit);
	    }
	    else {
		log.fine("stemrms not part of import command");
	    }
	    
	    Command renumberParameter = getParameter("renumber");
	    if (renumberParameter != null) {
		String value = ((StringParameter)renumberParameter).getValue().toLowerCase();
		if (value.equals("true")) {
		    residueRenumberMode = true;
		}
		else if (value.equals("false")) {
		    residueRenumberMode = false;
		}
		else {
		    throw new CommandExecutionException("bad syntax, expected renumber=true|false");
		}
	    }
	    
	    Command junctionRmsLimitParameter = getParameter("junctionrms");
	    if (junctionRmsLimitParameter != null) {
		junctionRmsLimit = Double.parseDouble(((StringParameter)junctionRmsLimitParameter).getValue());
		log.fine("junctionrms is part of import command: " + junctionRmsLimit);
	    }
	    else {
		log.fine("Using default junction rms limit: " + junctionRmsLimit);
	    }
	    Command stemAngleLimitParameter = getParameter("stemangle");
	    if (stemAngleLimitParameter != null) {
		stemAngleLimit = DEG2RAD * Double.parseDouble(((StringParameter)stemAngleLimitParameter).getValue());
		log.fine("stemangle is part of command: " + stemAngleLimit);
	    }
	    Command junctionAngleLimitParameter = getParameter("junctionangle");
	    if (junctionAngleLimitParameter != null) {
		junctionAngleLimit = DEG2RAD * Double.parseDouble(((StringParameter)junctionAngleLimitParameter).getValue());
		log.fine("junctionangle is part of command: " + junctionAngleLimit);
	    }
	    else {
		log.fine("import : junctionangle not part of command.");
	    }
	    Command junctionAngleWeightParameter = getParameter("junctionangleweight");
	    if (junctionAngleWeightParameter != null) {
	    junctionAngleWeight = Double.parseDouble(((StringParameter)junctionAngleWeightParameter).getValue());
	    log.fine("Setting junction angle weight to : " + junctionAngleWeight);
	    }
	    else {
		log.fine("import : junctionangleweight not part of command.");
	    }
	    Command scaleParameter = getParameter("scale");
	    if (scaleParameter != null) {
		double scale = Double.parseDouble(((StringParameter)scaleParameter).getValue());
		if (scale <= 0.0) {
		throw new CommandExecutionException("scale parameter must be greater zero!");
		}
		scaleX = scale;
		scaleY = scale;
	    scaleZ = scale;
	    log.fine("Import scale was set to: " + scale);
	    }
	    else {
		log.fine("scale is not part of command!");
	    }
	    scaleParameter = getParameter("scalex");
	    if (scaleParameter != null) {
		scaleX = Double.parseDouble(((StringParameter)scaleParameter).getValue());
		if (scaleX <= 0.0) {
		    throw new CommandExecutionException("scale parameter must be greater zero!");
		}
	    }
	    scaleParameter = getParameter("scaley");
	    if (scaleParameter != null) {
		scaleY = Double.parseDouble(((StringParameter)scaleParameter).getValue());
		if (scaleY <= 0.0) {
		    throw new CommandExecutionException("scale parameter must be greater zero!");
	    }
	    }
	    scaleParameter = getParameter("scalez");
	    if (scaleParameter != null) {
		scaleZ = Double.parseDouble(((StringParameter)scaleParameter).getValue());
		if (scaleZ <= 0.0) {
		    throw new CommandExecutionException("scale parameter must be greater zero!");
		}
	    }
	    
	    Command rerunParameter = getParameter("rerun");
	    if (rerunParameter != null) {
		this.rerunMax = Integer.parseInt(((StringParameter)rerunParameter).getValue());
	    }
	}
	catch (NumberFormatException nfe) {
	    throw new CommandExecutionException("Number format exception! " + nfe.getMessage());
	}
	catch (ParsingException pe) {
	    throw new CommandExecutionException("Parsing exception in import command: " + pe.getMessage());
	}
    }
    
}
