package rnadesign.designapp;

import java.io.PrintStream;

import rnadesign.rnacontrol.Object3DGraphController;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class LinksCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "links";

    private Object3DGraphController controller;
    private PrintStream ps;
    private String name;
    private String linkType = null;

    public LinksCommand(PrintStream ps, Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	assert ps != null;
	this.ps = ps;
	this.controller = controller;
    }

    public Object cloneDeep() {
	LinksCommand command = new LinksCommand(this.ps, this.controller);
	command.name = this.name;
	command.linkType = this.linkType;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME;
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " [options]" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Links command lists all of the existing links." + NEWLINE + NEWLINE;
	helpText += "OPTIONS" + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	ps.println(controller.getLinks().toPrettyString(linkType));
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	StringParameter nameParameter = (StringParameter)(getParameter("name"));
	if (nameParameter != null) {
	    name = nameParameter.getValue();
	}
	nameParameter = (StringParameter)(getParameter("type"));
	if (nameParameter != null) {
	    linkType = nameParameter.getValue();
	}
    }
}
    
