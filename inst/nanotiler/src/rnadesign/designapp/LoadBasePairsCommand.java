package rnadesign.designapp;

import java.io.*;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class LoadBasePairsCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "loadbasepairs";

    private String fileName;
    
    private Object3DGraphController controller;
    
    public LoadBasePairsCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	LoadBasePairsCommand command = new LoadBasePairsCommand(controller);
	command.fileName = this.fileName;
	command.controller = this.controller;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " filename";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " FILENAME" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     LoadBasePairs command TODO." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	assert (this.controller != null);
	prepareReadout();
	if ((fileName == null) || (fileName.length() == 0)) {
	    throw new CommandExecutionException(helpOutput());
	}
	try {
	    FileInputStream fis = new FileInputStream(fileName);
	    this.controller.getBasePairDB().read(fis);
	}
	catch(IOException ioe) {
	    throw new CommandExecutionException("Error reading base pair file: " + ioe.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() == 0) {
	    throw new CommandExecutionException(helpOutput()); 
	}
	assert (getParameterCount() > 0);
	assert getParameter(0) instanceof StringParameter;
	StringParameter stringParameter = (StringParameter)(getParameter(0));
	fileName = stringParameter.getValue();
    }
    
}
