package rnadesign.designapp;

import java.util.logging.Logger;
import java.io.*;
import java.lang.Double;
import java.lang.Boolean;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.IntParameter;
import commandtools.StringParameter;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerConstants;
import rnadesign.rnacontrol.PdbJunctionControllerParameters;
import rnadesign.rnamodel.CorridorDescriptor;
import rnadesign.rnamodel.JunctionScanner;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class LoadJunctionsCommand extends AbstractCommand {

    private static Logger log = Logger.getLogger("NanoTiler_debug");
    
    public static final String COMMAND_NAME = "loadjunctions";

    private double corridorMotifRadius = 3.0;
    private int orderMax = 5; // currently fixed
    private int stemLengthMin = 3;
    private String fileName;
    private String usedDirectory;
    private int loopLengthSumMax = JunctionScanner.LOOP_LENGTH_SUM_MAX;
    private String writeName;
    private String writeDir;
    private boolean noWriteFlag = false;
    private boolean intHelixCheck = true;
    private boolean motifMode = false;

    boolean rnaMode = true; // currently fixed: expect RNA and RNAView output in pdb file

    int branchDescriptorOffset = 2; // if nothing specified, extend helices by this many base pairs

    private CorridorDescriptor corridorDescriptor = new CorridorDescriptor(Object3DGraphControllerConstants.CORRIDOR_DEFAULT_RADIUS,
									   Object3DGraphControllerConstants.CORRIDOR_DEFAULT_START);

    private Object3DGraphController controller;
    
    public LoadJunctionsCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	LoadJunctionsCommand command = new LoadJunctionsCommand(controller);
	command.corridorMotifRadius = this.corridorMotifRadius;
	command.orderMax = this.orderMax;
	command.fileName = this.fileName;
	command.usedDirectory = this.usedDirectory;
	command.writeName = this.writeName;
	command.writeDir = this.writeDir;
	command.noWriteFlag = this.noWriteFlag;
	command.rnaMode = this.rnaMode;
	command.branchDescriptorOffset = this.branchDescriptorOffset;
	command.stemLengthMin = this.stemLengthMin;
	command.corridorDescriptor = new CorridorDescriptor(this.corridorDescriptor);
	command.intHelixCheck = this.intHelixCheck;
	command.motifMode = this.motifMode;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	assert (this.controller != null);
	prepareReadout();
	if (motifMode) {
	    log.info("Starting to read motifs using a minimum stem-length cutoff of " + stemLengthMin + " Setting corridor radius to " + corridorMotifRadius);
	    corridorDescriptor.radius = corridorMotifRadius;
	} else {
	    log.info("Starting to read junctions and kissing loops using a minimum stem-length cutoff of " + stemLengthMin);
	}
	PdbJunctionControllerParameters prm = 
	    new PdbJunctionControllerParameters(orderMax,
						fileName,
						usedDirectory,
						//writeName, 
						writeDir,
						noWriteFlag,
						rnaMode,
						branchDescriptorOffset,
						stemLengthMin,
						corridorDescriptor,
						loopLengthSumMax, intHelixCheck, motifMode);
	// PdbJunctionController junctionController = (PdbJunctionController)(graphController.getJunctionController());
	assert prm != null;
	this.controller.resetJunctionController(prm); 
	this.controller.setLastReadDirectory(usedDirectory);
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() == 0) {
	    throw new CommandExecutionException(helpOutput());
	}
	assert (getParameterCount() > 0);
	assert getParameter(0) instanceof StringParameter;
	StringParameter stringParameter = (StringParameter)(getParameter(0));
	fileName = stringParameter.getValue();
	boolean tempFileMode = false;
	String pdbFileName = null;
	if (fileName.endsWith(".pdb")) {
	    // no names file, but instead a PDB file is directly specified. Create temporary file:
	    try {
		File tempFile = File.createTempFile("nanotiler_junctions_", ".names");
		pdbFileName = fileName;
		File pdbFile = new File(pdbFileName);
		usedDirectory = pdbFile.getParent(); // directory for reading PDB file
		fileName = tempFile.getAbsolutePath();
		tempFileMode = true;
		FileOutputStream fos = new FileOutputStream(tempFile);
		PrintStream ps = new PrintStream(fos);
		ps.println(pdbFile.getName());
		log.info("Created temporary file with junction names: " + fileName);
	    } catch (IOException ioe) {
		throw new CommandExecutionException("Error creating temporary file with building block names: " + ioe.getMessage());
	    }
	}
	// TODO : don't know what this code is for ... EB
	log.fine("Nanotiler should write something here. LoadJunctionsCommand line 72");
	//writeName = "something"; //TODO christine
	if (getParameterCount() > 1) {
	    assert (getParameter(1) instanceof StringParameter);
	    StringParameter stringParameter2 = (StringParameter)(getParameter(1));
	    usedDirectory = stringParameter2.getValue();
	}
	else {
	    if (!tempFileMode) {
		usedDirectory = (new File(fileName)).getParent(); // use directory for reading
	    }
	}
	try {
	    if (getParameterCount() > 2) {
		Command parameter3 = getParameter(2);
		if (parameter3 instanceof StringParameter) {
		    branchDescriptorOffset = Integer.parseInt(((StringParameter)(parameter3)).getValue());
		}
		else if (parameter3 instanceof IntParameter) {
		    branchDescriptorOffset = ((IntParameter)(parameter3)).getValue();
		}
	    }
	    StringParameter stemMinParameter = (StringParameter)(getParameter("stem-min"));
	    if (stemMinParameter != null) {
		this.stemLengthMin = Integer.parseInt(stemMinParameter.getValue());
	    }
	    StringParameter modeParameter = (StringParameter)(getParameter("mode"));
	    if (modeParameter != null) {
		if (modeParameter.getValue().equals("g")) { // general motif mode
		    this.motifMode = true;
		} else if (modeParameter.getValue().equals("d")) { // general motif mode
		    this.motifMode = true;
		} else {
		    throw new CommandExecutionException("Defined values for mode option are d or g: [mode=d|g]");
		}		
	    }
	    StringParameter corridorWidthParameter = (StringParameter) (getParameter("corridor-width"));
	    if(corridorWidthParameter != null) {
		double corridorWidth = Double.parseDouble(corridorWidthParameter.getValue());
		this.corridorDescriptor = new CorridorDescriptor (corridorWidth, Object3DGraphControllerConstants.CORRIDOR_DEFAULT_START);
	    }
	    StringParameter intHelixCheckParameter = (StringParameter) (getParameter("inthelixcheck"));
	    if(intHelixCheckParameter != null)
		this.intHelixCheck = Boolean.parseBoolean(intHelixCheckParameter.getValue());
	}
	
    
	catch (NumberFormatException nfe) {
	    throw new CommandExecutionException("Error parsing value: " + nfe.getMessage());
	}
    }

    
    public String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " fileName [directory [offset]][stem-min=1|2|3|4|...][corridor-width=DOUBLE][inthelixcheck=BOOLEAN] [mode=d|g]";
    }
    
    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " [options]" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     LoadJunctions command TODO.";
	helpText += "OPTIONS" + NEWLINE;
	helpText += "     stringparameter  : name of file with relative filenames of junctions" + NEWLINE;
	helpText += "     stringparameter2 : name of directory of junctions" + NEWLINE ;
	helpText += "     branchDescriptorOffset : 0|1|2|... : Length of connector helices. Default: 2. Often used: 3" + NEWLINE + NEWLINE;
	helpText += "     stem-min=1|2|3|...    : Minimum length of considered stems." + NEWLINE;
	helpText += "     corridor-width=DOUBLE : Sets the radius of the corridor in front of helices in which collisions will be checked for. Default: 0. Often used: 3." + NEWLINE;
	helpText += "     inthelixcheck=BOOLEAN : Indicates whether or not the program should check for internal helices in the parsed pdb files. Default: True."+ NEWLINE;
	helpText += "     mode=d|g              : if set to g, all PDB files are interpreted such that general helix motifs are parsed, as opposed to junctions and kissing loops in the default mode d";
	return helpText;
    }
    
}
