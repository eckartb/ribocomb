package rnadesign.designapp;

import java.io.PrintStream;

import commandtools.AbstractCommand;
import commandtools.BadSyntaxException;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import commandtools.UnknownCommandException;

import static rnadesign.designapp.PackageConstants.NEWLINE;

/**
 * @author Christine Viets
 */
public class ManCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "man";

    private NanoTilerInterpreter interpreter;
    private PrintStream ps;
    private String helpCommand;
    
    public ManCommand(PrintStream ps, NanoTilerInterpreter interpreter) {
	super(COMMAND_NAME);
	assert interpreter != null;
	assert ps != null;
	this.ps = ps;
	this.interpreter = interpreter;
    }

    public Object cloneDeep() {
	ManCommand command = new ManCommand(this.ps, this.interpreter);
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " command";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"man\" Command Manual" + NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     man" + NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     man COMMAND" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE + "     Man command provides detailed text explanation for COMMAND.";
	helpText += NEWLINE + "     By default, man prints the available commands.";
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	ps.println(getOutput(helpCommand));
    }

    private String getOutput(String helpCommand) {
	Command command = new ManCommand(ps, interpreter);
	try {
	    try {
		command = interpreter.interpretLine(helpCommand);
	    }
	    catch (UnknownCommandException ue) {
		log.severe("Unknown command!: " + ue.getMessage());
		return null;
	    }
	}
	catch (BadSyntaxException e) {
	    log.severe("Bad syntax! : " + e.getMessage());
	    return null;
	}
	return command.getLongHelpText();
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() == 0) {
	    throw new CommandExecutionException(helpOutput()); 
	}
	else if (getParameterCount() == 1) {
	    StringParameter p0 = (StringParameter)getParameter(0);
	    helpCommand = p0.toString();
	}
	else {
	    throw new CommandExecutionException(helpOutput());
	}
    }

}
