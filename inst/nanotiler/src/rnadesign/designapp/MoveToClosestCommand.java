package rnadesign.designapp;

import java.io.PrintStream;

import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class MoveToClosestCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "movetoclosest";

    private Object3DGraphController controller;
    private String origName;
    private String newParentName;
    private String distClassName;
    private String assignClassName;
    double distCutoff = 3.0;

    public MoveToClosestCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	MoveToClosestCommand command = new MoveToClosestCommand(this.controller);
	command.origName = this.origName;
	command.newParentName = this.newParentName;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " originalname newparentname";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " ORIGINALNAME NEWPARENTNAME" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Move command moves an object within a tree." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	log.info("Starting " + COMMAND_NAME + " with " + origName + " " + newParentName
		 + " " + distClassName + " " + assignClassName + " " + distCutoff);
	try {
	    controller.getGraph().moveToClosestObject(origName, newParentName, distClassName, assignClassName, distCutoff);
	}
	catch (Object3DGraphControllerException e) {
	    throw new CommandExecutionException(e.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() != 5) {
	    throw new CommandExecutionException(helpOutput());
	}
	StringParameter p0 = (StringParameter)(getParameter(0));
	origName = p0.getValue();
	StringParameter p1 = (StringParameter)(getParameter(1));
	newParentName = p1.getValue();
	StringParameter p2 = (StringParameter)(getParameter(2));
	distClassName = p2.getValue();
	StringParameter p3 = (StringParameter)(getParameter(3));
	assignClassName = p3.getValue();
	StringParameter p4 = (StringParameter)(getParameter(4));
	distCutoff = Double.parseDouble(p4.getValue());
    }

}
