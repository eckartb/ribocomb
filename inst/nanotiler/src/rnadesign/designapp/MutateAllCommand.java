package rnadesign.designapp;

import java.util.Set;
import java.util.HashSet;

import java.io.*;
import java.text.ParseException;
import sequence.DuplicateNameException;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import rnadesign.rnacontrol.Mutation;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import commandtools.CommandApplication;
import sequence.UnknownSymbolException;
import generaltools.ParsingException;
import rnasecondary.ImprovedSecondaryStructureParser;
import rnasecondary.SecondaryStructure;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class MutateAllCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "mutateall";

    private Object3DGraphController controller;
    private String[] mutationDescriptors;
    private double rmsLimit = 3.0;
    private boolean forceMode = false;
    private String fileName;
    private CommandApplication interpreter;
    ///
    
    public MutateAllCommand(Object3DGraphController controller, CommandApplication interpreter) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
	this.interpreter = interpreter;
    }

    public Object cloneDeep() {
	MutateAllCommand command = new MutateAllCommand(this.controller, interpreter);
	command.controller = this.controller;
	command.mutationDescriptors = this.mutationDescriptors;
	command.interpreter=this.interpreter;
	command.rmsLimit = this.rmsLimit;
	command.forceMode = this.forceMode;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String synopsis() { return "" + COMMAND_NAME + " filename=SECONDARYSTRUCTURE_FILENAME";}
				    // + " [force=false|true][rms=VALUE]" 
				    // + " chainname:<symbol><pos>[,<symbol><pos> ...] . Example: mutate A:C5,G15,A2 B:U1,G2,A15 D:CGAUCAAUGUA\n";
    

    private String helpOutput() {
	return "Mutates bases or base pairs to match a given secondary structure. Correct usage: " + synopsis();
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + synopsis() + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Mutates nucleotides in current model to match a given secondary structure." + NEWLINE + NEWLINE;
	helpText += "OPTIONS" + NEWLINE;
	return helpText;
    }

    /** Returns true if of type A24,C36 ; returns false if of type ACGUUGUGA */
    private static boolean isSinglePositionString(String s) {
	for (int i = 0; i < s.length(); ++i) {
	    if (Character.isDigit(s.charAt(i))) {
		return true;
	    }
	}
	return false;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
		prepareReadout();
	
		ImprovedSecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
	 	SecondaryStructure structure = null;
		try{
			structure = parser.parse(fileName);
		} catch( ParseException pe ){
			System.out.println("Error parsing file");
		} catch( IOException ioe ){
			System.out.println(ioe);
			System.out.println("File not found");
		}
	
		assert structure != null;
		String script = null;
	
		try {
			script = controller.mutatePermutatedSequences(structure);
		}
		catch (Object3DGraphControllerException e) {
			throw new CommandExecutionException(e.getMessage());
		}
		catch (DuplicateNameException dpe){
			System.out.println("internal error: duplicate naming");
		}
	
		try{
			interpreter.runScriptLine("tree strand");
			String[] lines = script.split(NEWLINE);
			for (int i=0; i<lines.length; i++){
				interpreter.runScriptLine(lines[i]); //TODO: make flag for writing to file
			}
		} catch (Exception e){ //TODO import command exception??
			System.out.println("Command Exception:" + e);
		}
	
    }
	
    


    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	int numPar = getParameterCount();
	this.mutationDescriptors = new String[numPar];
	try {
		StringParameter parameter = (StringParameter)(getParameter("filename"));
	    if (parameter != null) {
		this.fileName = parameter.getValue();
		System.out.println(this.fileName);
	    }
	    parameter = (StringParameter)(getParameter("force"));
	    if (parameter != null) {
		this.forceMode = parameter.parseBoolean();
	    }
	    parameter = (StringParameter)(getParameter("rms"));
	    if (parameter != null) {
		this.rmsLimit = Double.parseDouble(parameter.getValue());
	    }
	}
	catch (ParsingException pe) {
	    throw new CommandExecutionException(pe.getMessage());
	}
	catch (NumberFormatException nfe) {
	    throw new CommandExecutionException(nfe.getMessage());
	}
    }
}
    
