package rnadesign.designapp;

import java.util.Set;
import java.util.HashSet;

import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import rnadesign.rnacontrol.Mutation;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import sequence.UnknownSymbolException;
import generaltools.ParsingException;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class MutateCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "mutate";

    private Object3DGraphController controller;
    private String[] mutationDescriptors;
    private double rmsLimit = 3.0;
    private boolean forceMode = false;

    public MutateCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	MutateCommand command = new MutateCommand(this.controller);
	command.controller = this.controller;
	command.mutationDescriptors = this.mutationDescriptors;
	command.rmsLimit = this.rmsLimit;
	command.forceMode = this.forceMode;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String synopsis() { return "" + COMMAND_NAME 
				    + " [force=false|true][rms=VALUE]" 
				    + " chainname:<symbol><pos>[,<symbol><pos> ...] . Example: mutate A:C5,G15,A2 B:U1,G2,A15 D:CGAUCAAUGUA\n";
    }

    private String helpOutput() {
	return "Mutates bases or base pairs. Correct usage: " + synopsis();
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + synopsis() + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Mutates base pairs and nucleotides in single or double strands." + NEWLINE + NEWLINE;
	helpText += "OPTIONS" + NEWLINE;
	return helpText;
    }

    /** Returns true if of type A24,C36 ; returns false if of type ACGUUGUGA */
    private static boolean isSinglePositionString(String s) {
	for (int i = 0; i < s.length(); ++i) {
	    if (Character.isDigit(s.charAt(i))) {
		return true;
	    }
	}
	return false;
    }

    /** Parses mutation descritporos of type type I:A24,C36 or Q:ACGUUGUGA */
    public Set<Mutation> parseMutationDescriptors(String[] mutationDescriptors) throws CommandExecutionException {
	Set<Mutation> mutations = new HashSet<Mutation>();
	for (int i = 0; i < mutationDescriptors.length; ++i) {
	    if (mutationDescriptors[i].indexOf("=") >= 0) {
		continue; // ignore items that contain "="
	    }
	    String[] words = mutationDescriptors[i].split(":");
	    if (words.length != 2) {
		throw new CommandExecutionException("Syntax for mutation descriptor: chainname:<symbol><pos>[,<symbol><pos> ...] . Example: mutate A:C5,G15,A2 B:U1,G2,A15 \n");
	    }
	    String chainName = words[0];
	    String posDesc = words[1];
	    if (isSinglePositionString(posDesc)) {
		String[] muts = posDesc.split(",");
		for (int j = 0; j < muts.length; ++j) {
		    char symbolChar = muts[j].toUpperCase().charAt(0);
		    String numberString = muts[j].substring(1);
		    try {
			int pos = Integer.parseInt(numberString) - 1; // internalCounting
			mutations.add(new Mutation(chainName, pos, symbolChar));
		    }
		    catch (NumberFormatException nfe) {
			throw new CommandExecutionException("Could not interpret position number: " + numberString 
							    + " . Syntax for mutation descriptor: chainname:<symbol><pos>[,<symbol><pos> ...] . Example: mutate A:C5,G15,A2 B:U1,G2,A15 \n");
		    }
		    catch (UnknownSymbolException use) {
			throw new CommandExecutionException("Unknown sequence symbol in " + posDesc + " Currently allowed symbols are: " 
							    + sequence.DnaTools.RNA_ALPHABET);
		    }
		}
	    }
	    else {
		// whole sequence is supplied:
		posDesc = posDesc.toUpperCase();
		for (int j = 0; j < posDesc.length(); ++j) {
		    char symbolChar = posDesc.charAt(j);
		    try {
			int pos = j;
			mutations.add(new Mutation(chainName, pos, symbolChar));
		    }
		    catch (UnknownSymbolException use) {
			throw new CommandExecutionException("Unknown sequence symbol in " + posDesc + " Currently allowed symbols are: " 
							    + sequence.DnaTools.RNA_ALPHABET);
		    }
		}		
	    }
	}
	return mutations;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	Set<Mutation> mutations = parseMutationDescriptors(mutationDescriptors);
	System.out.println("Applying the following mutations:");
	for (Mutation mutation : mutations) {
	    System.out.print("" + mutation + "; ");
	}
	System.out.println();
	try {
	    controller.mutateSequences(mutations, rmsLimit, forceMode);
	}
	catch (Object3DGraphControllerException e) {
	    throw new CommandExecutionException(e.getMessage());
	}
    }


    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	int numPar = getParameterCount();
	this.mutationDescriptors = new String[numPar];
	for (int i = 0; i < numPar; ++i) {
	    StringParameter parameter = (StringParameter)(getParameter(i));
	    this.mutationDescriptors[i] = parameter.toString();
	}
	try {
	    StringParameter parameter = (StringParameter)(getParameter("force"));
	    if (parameter != null) {
		this.forceMode = parameter.parseBoolean();
	    }
	    parameter = (StringParameter)(getParameter("rms"));
	    if (parameter != null) {
		this.rmsLimit = Double.parseDouble(parameter.getValue());
	    }
	}
	catch (ParsingException pe) {
	    throw new CommandExecutionException(pe.getMessage());
	}
	catch (NumberFormatException nfe) {
	    throw new CommandExecutionException(nfe.getMessage());
	}
    }
}
    
