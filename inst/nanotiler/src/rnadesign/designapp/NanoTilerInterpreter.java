package rnadesign.designapp; 

import java.io.PrintStream;
import java.util.logging.Logger;

import commandtools.*;
import commandtools.DefineCommand;
import rnadesign.designapp.rnagui.GraphicsCommand;
import rnadesign.rnacontrol.Object3DGraphController;

/** Interprets NanoTiler scripts */
public class NanoTilerInterpreter extends LineInterpreter implements Interpreter {

    private Object3DGraphController controller;
    private CommandApplication application;
    
    private static PrintStream ps;
    public static boolean developmentVersion = true; // if true, activate commands that are in development
    public NanoTilerInterpreter(PrintStream ps,
				Logger log) {
	super(log);
	this.ps = ps;
    }

    /** initialize all known commands */
    private void init() {
	clear();
	assert(application != null);
	if (developmentVersion) {
	    this.addKnownCommand(new AddAtomCommand(controller));
	    this.addKnownCommand(new AlignCommand(controller));
	    // this.addKnownCommand(new CellCommand(controller));
	    this.addKnownCommand(new OptimizeRnaGraphCommand(controller, this.ps));
	    this.addKnownCommand(new TraceRnaGraphCommand(controller, ps));
	}
	this.addKnownCommand(new AlignResiduesCommand(controller));
	this.addKnownCommand(new AliasCommand(controller));
	this.addKnownCommand(new BridgeAllCommand(this.ps, controller));
	this.addKnownCommand(new BridgeItCommand(this.ps, controller));
	this.addKnownCommand(new BridgeIt2Command(this.ps, controller));
	this.addKnownCommand(new CharacterizeCommand(this.ps, controller));
	this.addKnownCommand(new ChildrenCommand(this.ps, controller));
	this.addKnownCommand(new ClearCommand(controller));
	this.addKnownCommand(new CloneCommand(controller));
	this.addKnownCommand(new ClosestCommand(this.ps, controller));
	this.addKnownCommand(new CheckCommand(this.ps, controller));
	this.addKnownCommand(new ChosenCommand(this.ps, controller));
	this.addKnownCommand(new ControllerCommand(this.controller));
  this.addKnownCommand(new ConvertSecondaryFormatCommand(this.ps, controller));
	this.addKnownCommand(new DefineCommand(application));
	this.addKnownCommand(new DemoCommand(application));
	this.addKnownCommand(new DeselectCommand(this.ps,controller));
	this.addKnownCommand(new DistanceCommand(this.ps, controller));
	this.addKnownCommand(new EchoCommand());
	this.addKnownCommand(new EndingCommand(controller));
	this.addKnownCommand(new EnvCommand(getVariables(), ps)); // add Properties object containing defined variables
	this.addKnownCommand(new ExitCommand());
	this.addKnownCommand(new ExportPdbCommand(controller));
	this.addKnownCommand(new ExportPdbSymCommand(controller));
	assert getVariables() != null;
	this.addKnownCommand(new ExtendCommand(ps, controller));
	this.addKnownCommand(new ExportPointsCommand(controller));
	this.addKnownCommand(new FitCommand(this.ps, controller));
	this.addKnownCommand(new ForeachCommand(getVariables(), application));
	this.addKnownCommand(new FuseStrandsCommand(controller));
	this.addKnownCommand(new FuseAllStrandsCommand(controller));
	this.addKnownCommand(new FuseJStrandsCommand(controller));
	this.addKnownCommand(new AddBasePairCommand(controller)); // command  name: genbpconstraint
	this.addKnownCommand(new GenLinkCommand(controller));
	this.addKnownCommand(new GenerateDistanceConstraintCommand(controller));
	this.addKnownCommand(new GenerateJunctionDBConstraintCommand(controller));
  this.addKnownCommand(new GenerateMotifConstraintCommand(controller));
  this.addKnownCommand(new GenerateSuperimposeConstraintCommand(controller));
  this.addKnownCommand(new GenerateBlockCollisionConstraintCommand(controller));
	this.addKnownCommand(new GenerateHelixCommand(controller));
	this.addKnownCommand(new GenJunctionCommand(controller));
	this.addKnownCommand(new HelixConstraintCommand(controller)); // command name: genhelixconstraint
	this.addKnownCommand(new GenerateJunctionConstraintCommand(controller));
	this.addKnownCommand(new GenShapeCommand(this.controller));
	this.addKnownCommand(new GenerateTorsionConstraintCommand(controller));
	this.addKnownCommand(new GraphicsCommand(controller));
	this.addKnownCommand(new GrowCommand(controller));
	this.addKnownCommand(new GrowGraphCommand(controller));
	this.addKnownCommand(new GrowScanCommand(controller));
	// this.addKnownCommand(new GridShapeCommand(controller));
	this.addKnownCommand(new HelpCommand(this.ps, this));
	this.addKnownCommand(new HistoryCommand(application));
	this.addKnownCommand(new ImportCommand(controller));
	this.addKnownCommand(new JBridgeItCommand(this.ps, controller));
	this.addKnownCommand(new LinksCommand(this.ps, controller));
	this.addKnownCommand(new LoadBasePairsCommand(controller));
	this.addKnownCommand(new LoadJunctionsCommand(controller));
	this.addKnownCommand(new LoadNucleotidesCommand(controller));
	this.addKnownCommand(new ManCommand(this.ps, this));
	this.addKnownCommand(new MissingLinkCommand(this.ps, controller));
	this.addKnownCommand(new MoveCommand(controller));
	this.addKnownCommand(new MoveToClosestCommand(controller));
	this.addKnownCommand(new MutateCommand(controller));
	this.addKnownCommand(new MutateAllCommand(controller, this.application));
	this.addKnownCommand(new NoOperationCommand());
	this.addKnownCommand(new OptimizeBasepairsCommand(controller, this.ps)); // FIXIT: verify
	this.addKnownCommand(new OptimizeHelicesCommand(controller, this.ps));
	this.addKnownCommand(new OptimizeSystematicCommand(controller, this.ps));
	this.addKnownCommand(new OrientCommand(controller));
	this.addKnownCommand(new PhysicsCommand(controller));
	this.addKnownCommand(new PlaceCommand(controller));
	this.addKnownCommand(new PropertiesCommand(controller));
	// this.addKnownCommand(new QuaderCommand(controller));
       	this.addKnownCommand(new QuitCommand());
	this.addKnownCommand(new RandomizeCommand(controller));
  this.addKnownCommand(new RandomizeBlocksCommand(controller));
	this.addKnownCommand(new RemoveCommand(controller));
	this.addKnownCommand(new RemoveDupSequences(controller));
	this.addKnownCommand(new RemoveLinkCommand(controller));
	this.addKnownCommand(new RemovePhosphatesCommand(controller));
	this.addKnownCommand(new RenameCommand(controller));
	this.addKnownCommand(new RenumberCommand(controller));
	this.addKnownCommand(new ResiduePropertyCommand(controller));
	this.addKnownCommand(new RGrowCommand(controller));
	this.addKnownCommand(new RingFixConstraintsCommand(controller));
	this.addKnownCommand(new RingFuseCommand(controller));
	this.addKnownCommand(new RotateCommand(controller));
	this.addKnownCommand(new RmsCommand(controller));
	this.addKnownCommand(new ScoreConstraintsCommand(controller, this.application));
	this.addKnownCommand(new ScoreFitCommand(ps, controller));
	this.addKnownCommand(new SecondaryCommand(ps, controller));
	this.addKnownCommand(new SeedCommand());
	this.addKnownCommand(new SelectCommand(controller));
	this.addKnownCommand(new SetCommand(getVariables())); // add Properties object containing defined variables
	this.addKnownCommand(new ShiftCommand(controller));
        this.addKnownCommand(new SignatureCommand(controller));
	this.addKnownCommand(new SourceCommand(application));
	// this.addKnownCommand(new SpaceGroupCommand(controller));
	this.addKnownCommand(new SplitStrandCommand(controller));
	this.addKnownCommand(new StatusCommand(ps, controller));
	this.addKnownCommand(new StericCommand(ps, controller));
	this.addKnownCommand(new SuperposableCommand(ps, controller));
	// this.addKnownCommand(new SymmetryCommand(controller));
	this.addKnownCommand(new SymAddCommand(controller));
	this.addKnownCommand(new SymApplyCommand(controller));
	this.addKnownCommand(new SymInfoCommand(ps, controller));
	this.addKnownCommand(new SynthCommand(controller));
	this.addKnownCommand(new TileCommand(controller));
	this.addKnownCommand(new ToeholdCommand(controller));
	this.addKnownCommand(new TraceRnaGraphScriptCommand(controller));
  this.addKnownCommand(new TracePRSwSecScriptCommand(controller));
	this.addKnownCommand(new TraceSecondaryRnaScriptCommand(controller));
	this.addKnownCommand(new TreeCommand(ps, controller));
	this.addKnownCommand(new TrimCommand(controller));
	this.addKnownCommand(new TwistCommand(controller));
	// this.addKnownCommand(new UnfoldCommand(controller));
	this.addKnownCommand(new UnShiftCommand(controller));
	this.addKnownCommand(new VerboseCommand());
	// this.addKnownCommand(new GLGraphicsCommand(controller));
    }

    public void setApplication(AbstractDesigner app) {
	this.application = app;
	this.controller = app.getGraphController();
	init();
    }

}
