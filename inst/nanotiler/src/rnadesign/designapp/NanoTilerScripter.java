package rnadesign.designapp;

import java.util.logging.*;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.logging.Logger;

import commandtools.BadSyntaxException;
import commandtools.CommandException;
import commandtools.CommandExecutionException;
import commandtools.UnknownCommandException;
import generaltools.StringTools;
import rnadesign.rnacontrol.Object3DGraphController;

import static rnadesign.designapp.PackageConstants.*;

public class NanoTilerScripter extends AbstractDesigner
{

    public static Level logLevel = Level.OFF;

    public static final String PROMPT = "nano>";

    public static final int SUCCESS_CODE = 0;

    public static final int FAIL_CODE = 1;

    public static final String LOGFILE_DEFAULT = "nanotilerscripter";

    public static String nanotilerHome = System.getenv(NANOTILER_HOME_VAR);

    public NanoTilerScripter() {
	super(new Object3DGraphController(), new NanoTilerInterpreter(System.out,
						   Logger.getLogger(LOGFILE_DEFAULT) 
						   ) );
	NanoTilerInterpreter interpreter = (NanoTilerInterpreter)getInterpreter();
	interpreter.setApplication(this); // workaround, to give "source" command reference to this runScript method of this application
	log.setLevel(logLevel);
    }

    public NanoTilerScripter(Object3DGraphController controller) {
	super(controller, new NanoTilerInterpreter(System.out,
						   Logger.getLogger(LOGFILE_DEFAULT) 
						   ) );
	NanoTilerInterpreter interpreter = (NanoTilerInterpreter)getInterpreter();
	interpreter.setApplication(this); // workaround, to give "source" command reference to this runScript method of this application
	log.setLevel(logLevel);
    }

	//ADDED BY RISHABH SHARAN for Galaxy purposes
	public NanoTilerScripter(PrintStream ps){
		super(new Object3DGraphController(), new NanoTilerInterpreter(ps,Logger.getLogger(LOGFILE_DEFAULT)));
		NanoTilerInterpreter interpreter = (NanoTilerInterpreter)getInterpreter();
		interpreter.setApplication(this); // workaround, to give "source" command reference to this runScript method of this application
		log.setLevel(logLevel);
	}

    /** Handles quitting of program. "code" is the id passed on to the exit system command. */
    private static void fail(int code) {
	System.exit(code);
    }

    private void printHelp(PrintStream ps) {
	HelpCommand helpCommand = new HelpCommand(System.out, (NanoTilerInterpreter)(getInterpreter()));
	ps.println(helpCommand.getMessage());
    }

    /** performs default script if it exists */
    protected void runDefaultScript() throws CommandException, IOException {
	// running "defaults.script" script at startup:
	String defaultScriptName = nanotilerHome + SLASH + STARTUP_SCRIPT;
	File defaultScriptFile = new File(defaultScriptName);
	if (defaultScriptFile.exists()) {
	    System.out.println("Running default script file: " + defaultScriptName);
	    InputStream is = new FileInputStream(defaultScriptFile);
	    runScript(is);
	}
	else {
	    System.out.println("No default script found at " + defaultScriptName);
	}
    }

    public static void main(String[] args) {
	
	try {

	    Object3DGraphController controller = new Object3DGraphController();
	    NanoTilerScripter scripter = new NanoTilerScripter(controller);
	    scripter.setUndoMode(false); // no undo 
	    if ((args == null) || ((args.length > 0) && (args[0].equals("help")))) {
		scripter.printHelp(System.out);
		System.exit(0);
	    }
	    if ((nanotilerHome == null) || (nanotilerHome.equals(""))) {
		System.out.println("NANOTILER_HOME variable is not defined!");
		System.exit(0);
	    }
	    else {
		System.out.println("The NANOTILER_HOME variable is set to: " + nanotilerHome);
	    }
	    InputStream is = null;
	    // set variables if necessary
	    // System.out.println("Number of command line arguments: " + args.length);
	    if (args.length > 1) {
		for (int i = 1; i < args.length; ++i) {
		    String varName = "" + i;
		    System.out.println("Setting variable " + varName + " to " + args[i]);
		    scripter.getInterpreter().setVariable(varName, args[i]);
		}
	    }
	    // execute default script unless the user provides the option "no-default"
	    if (StringTools.indexOfString(args, "no-default") < 0) {
		scripter.runDefaultScript(); // run default script if it exists
	    }
	    if (args.length > 0) {
		String scriptFileName = args[0];
		System.out.println("Running script: " + scriptFileName);
		is = new FileInputStream(scriptFileName);
		scripter.runScript(is);
	    }
	    else { // read from standard input!
		System.out.println("Welcome to NanoTiler! (script mode)");
		scripter.printHelp(System.out);
		is = System.in; 
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		do {
		    System.out.print(PROMPT);
		    try {
			String line = reader.readLine();
			if (line == null) {
			    break;
			}
			scripter.runScriptLine(line);
		    }
		    catch (CommandExecutionException ce) {
			System.out.println("Command exception encountered: " + ce.getMessage());
		    }
		    catch (UnknownCommandException uce) {
			System.out.println("Unknown command " + uce.getMessage());
		    }
		    catch (BadSyntaxException bse) {
			System.out.println("Bad syntax : " + bse.getMessage());
		    }
		    catch (CommandException ce) {
		    System.out.println("Command exception: " + ce);
		    }	
		}
		while (true);
	    }
	}
	catch (IOException ioe) {
	    System.out.println("IO error reading file: " + args[0]);
	    fail(FAIL_CODE);
	}
	catch (CommandExecutionException ce) {
	    System.out.println("Command exception encountered: " + ce.getMessage());
	    fail(FAIL_CODE);
	}
	catch (UnknownCommandException uce) {
	    System.out.println("Error excecuting script: unknown command in " + args[0] + " : " + uce.getMessage());
	    fail(FAIL_CODE);
	}
	catch (BadSyntaxException bse) {
	    System.out.println("Error excecuting script: bad syntax in " + args[0] + " : " + bse.getMessage());
	    fail(FAIL_CODE);
	}
	catch (CommandException ce) {
	    System.out.println("Script command exception: " + ce);
	    fail(FAIL_CODE);
	}	

	System.out.println("Good bye!");

    }

}
