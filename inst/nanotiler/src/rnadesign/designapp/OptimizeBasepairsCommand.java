package rnadesign.designapp;

import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;
import java.util.Properties;
import java.io.PrintStream;
import generaltools.ParsingException;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.IntParameter;
import commandtools.StringParameter;
import rnadesign.rnamodel.FitParameters;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import rnadesign.rnacontrol.Object3DGraphControllerConstants;
import rnadesign.rnacontrol.Object3DGraphControllerEventConstants;
import controltools.*;
import rnadesign.rnamodel.HelixOptimizer;
import tools3d.Vector3D;
import tools3d.objects3d.Object3DSet;
import tools3d.objects3d.SimpleObject3DSet;
import tools3d.objects3d.Object3D;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class OptimizeBasepairsCommand extends AbstractCommand {

    private static Logger log = Logger.getLogger("NanoTiler_debug");
    
    public static final String COMMAND_NAME = "optconstraints";

    private boolean addHelicesFlag = true; // import sequences after optimization

    private double electrostaticWeight = 0.0;

    private boolean keepFirstFixed = false;

    private double vdwWeight = 0.0;

    private String helixRootName = "root";

    private double kt = 2.0;

    private int numSteps = 1000000; // number of steps of optimization algorithm

    private List<Object3DSet> objectBlocks;

    // determines which algorithm is used for optimization
    // private int optimizerAlgorithm = Object3DGraphController.SEQUENCE_OPTIMIZER_SCRIPT;
    private int optimizerAlgorithm = 0;

    private boolean placeBestJunctions = true;

    private double rmsLimit = 3.0; // rms limit for placement of mutated residue

    private double stemRmsLimit = 10.0; // 5.0;

    private double stemAngleLimit = Math.PI; // Math.PI / 4;

    private double errorScoreLimit = 0.5; // score below this limit leads to termination before end

    private Object3DGraphController controller;

    private String movableNames = null;
    
    private PrintStream ps;

    private List<Vector3D> fixedRotationAxisList;

    private int[] symVdwActive;

    public OptimizeBasepairsCommand(Object3DGraphController controller, PrintStream ps) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
	this.ps = ps;
    }

    public Object cloneDeep() {
	OptimizeBasepairsCommand command = new OptimizeBasepairsCommand(this.controller, this.ps);
	command.addHelicesFlag = this.addHelicesFlag;
	command.electrostaticWeight = this.electrostaticWeight;
	command.fixedRotationAxisList = this.fixedRotationAxisList;
	command.helixRootName = this.helixRootName;
	command.keepFirstFixed = this.keepFirstFixed;
	command.kt = this.kt;
	command.movableNames = this.movableNames;
	command.numSteps = this.numSteps;
	command.objectBlocks = this.objectBlocks;
	command.placeBestJunctions = this.placeBestJunctions;
	command.optimizerAlgorithm = this.optimizerAlgorithm;
	command.rmsLimit = this.rmsLimit;
	command.stemAngleLimit = this.stemAngleLimit;
	command.stemRmsLimit = this.stemRmsLimit;
	command.errorScoreLimit = this.errorScoreLimit;
	command.vdwWeight = this.vdwWeight;
	command.symVdwActive = this.symVdwActive; // shallow copy ok
	return command;
    }

    private Object3DSet generateMovableObjects(String movableNames) throws CommandExecutionException {
	if (movableNames == null) {
	    return null;
	}
	String[] words = movableNames.split(",");
	if ((words == null) || (words.length == 0)) {
	    return null;
	}
	Object3DSet oset = new SimpleObject3DSet();
	for (int i = 0; i < words.length; ++i) {
	    Object3D obj = controller.getGraph().findByFullName(words[i]);
	    if (obj != null) {
		oset.add(obj);
	    }
	    else {
		throw new CommandExecutionException("Could not find object: " + words[i]);
	    }
	}
	return oset;
    }

    private int[] parseIntIds(String symIds) throws CommandExecutionException {
	int[] result = null;
	try {
	    String[] words = symIds.split(",");
	    result = new int[words.length];
	    for (int i = 0; i < result.length; ++i) {
		result[i] = Integer.parseInt(words[i]);
	    }
	} catch (NumberFormatException nfe) {
	    throw new CommandExecutionException("Error parsing symmetry ids: " + nfe.getMessage());
	}
	return result;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	assert (this.controller != null);
	prepareReadout();
	ps.println("Starting to optimize helices!");
	FitParameters stemFitParameters = null;
	Properties properties = new Properties();
	if (addHelicesFlag) {
	    stemFitParameters = new FitParameters(stemRmsLimit, stemAngleLimit);
	    // TODO : specify stem rms, stem angle weight, stem angle error
	}
	if (objectBlocks == null) {
	    throw new CommandExecutionException("No object blocks defined!");
	}
	if (objectBlocks.size() == 0) {
	    throw new CommandExecutionException("Zero object blocks defined!");
	}
	try {
	    Object3DSet movableObjects = generateMovableObjects(movableNames);
	    if (movableObjects != null) {
		ps.println("Number of movable objects: " + movableObjects.size());
	    }
	    else {
		ps.println("All objects with constraints are considered movable.");
	    }
	    ps.println("Number of optimization steps: " + numSteps);
	    ps.println("Electrostatic repulsion term weight: " + electrostaticWeight);
	    ps.println("Van de Waals repulsion term weight: " + vdwWeight);
	    ps.println("Tolerance: " + kt);
	    ps.println("Number of object blocks: " + objectBlocks.size());
	    properties = controller.optimizeBasepairs(numSteps, errorScoreLimit, electrostaticWeight,
						      vdwWeight, kt, objectBlocks,
						      fixedRotationAxisList, symVdwActive, keepFirstFixed, placeBestJunctions, helixRootName);
	}
	catch (Object3DGraphControllerException gce) {
	    throw new CommandExecutionException(gce.getMessage());
	}
	// 	HelixOptimizer optimizer = new HelixOptimizer(controller.getGraph().getGraph(), controller.getLinks(), numSteps, 
	// 						      errorScoreLimit);
	// 	Properties properties = optimizer.optimize();
	controller.refresh(new ModelChangeEvent(controller, Object3DGraphControllerEventConstants.MODEL_MODIFIED));
	ps.println("Helix optimization finished! Result:");
	ps.println(properties.toString());
	this.resultProperties = properties;
    }

    /** parses string of form name1,name2,...;name3,name4,... into List of object sets */
    List<Object3DSet> parseObjectBlocks(String blocks) throws CommandExecutionException {
	if ((blocks == null) || (blocks.length() == 0)) {
	    throw new CommandExecutionException("blocks option has to be defined! See usage.");
	}
	List<Object3DSet> result = new ArrayList<Object3DSet>();
	String setWords[] = blocks.split(";");
	for (int i = 0; i < setWords.length; ++i) {
	    String objWords[] = setWords[i].split(",");
	    if (objWords.length < 1) {
		throw new CommandExecutionException("Error in optbasepairs: expected at least one object in " + setWords[i]);
	    }
	    Object3DSet set = new SimpleObject3DSet();
	    for (int j = 0; j < objWords.length; ++j) {
		String objName = objWords[j];
		Object3D obj = controller.getGraph().findByFullName(objName);
		if (obj == null) {
		    throw new CommandExecutionException("Could not find object with name: " + objName);
		}
		set.add(obj);
	    }
	    result.add(set);
	}
	return result;
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private Vector3D parseVector(String s) throws CommandExecutionException {
	if ((s == null) || (s.equals(""))) {
	    return null;
	}
	String[] words = s.split(",");
	if (words.length != 3) {
	    throw new CommandExecutionException("Expected 3 words in vector descriptor: " + s);
	}
	Vector3D result = new Vector3D();
	try {
	    result.setX(Double.parseDouble(words[0]));
	    result.setY(Double.parseDouble(words[1]));
	    result.setZ(Double.parseDouble(words[2]));
	}
	catch (NumberFormatException nfe) {
	    throw new CommandExecutionException("Error parsing item of vector descriptor: " + s);
	}
	if (result.length() == 0.0) {
	    throw new CommandExecutionException("Zero vector length found in vector descriptor: " + s);
	}
	return result;
    }

    private List<Vector3D> parseAxisList(String s) throws CommandExecutionException {
	String[] words = s.split(";");
	List<Vector3D> result = new ArrayList<Vector3D>();
	for (int i = 0; i < words.length; ++i) {
	    result.add(parseVector(words[i]));
	}
	return result;
    }

    private void prepareReadout() throws CommandExecutionException {
	Command importParameter = getParameter("helices");
	if (importParameter != null) {
	    String value = ((StringParameter)importParameter).getValue().toLowerCase();
	    if (value.equals("true")) {
		addHelicesFlag = true;
	    }
	    else if (value.equals("false")) {
		addHelicesFlag = false;
	    }
	    else {
		throw new CommandExecutionException(helpOutput());
	    }
	}
	try {
	    Command rmsParameter = getParameter("rms");
	    if (rmsParameter != null) {
		rmsLimit = Double.parseDouble(((StringParameter)rmsParameter).getValue());
	    }
	    Command stepsParameter = getParameter("steps");
	    if (stepsParameter != null) {
		numSteps = Integer.parseInt(((StringParameter)stepsParameter).getValue());
	    }
	    Command repulsParameter = getParameter("repuls");
	    if (repulsParameter != null) {
		electrostaticWeight = Double.parseDouble(((StringParameter)repulsParameter).getValue());
	    }
	    Command ktParameter = getParameter("kt");
	    if (ktParameter != null) {
		kt = Double.parseDouble(((StringParameter)ktParameter).getValue());
	    }
	    Command firstFixedParameter = getParameter("firstfixed");
	    if (firstFixedParameter != null) {
		keepFirstFixed = ((StringParameter)firstFixedParameter).parseBoolean();
	    }
	    Command errorLimitParameter = getParameter("error");
	    if (errorLimitParameter != null) {
		errorScoreLimit = Double.parseDouble(((StringParameter)errorLimitParameter).getValue());
	    }
	    Command helixRootParameter = getParameter("helixroot");
	    if (helixRootParameter != null) {
		helixRootName = ((StringParameter)(helixRootParameter)).getValue();
	    }
	    Command movableParameter = getParameter("movable");
	    if (movableParameter != null) {
		movableNames = ((StringParameter)(movableParameter)).getValue();
	    }
	    String movableParameterValue = "";
	    movableParameterValue = parse("blocks", movableParameterValue);
	    if (movableParameterValue != null) {
		this.objectBlocks = parseObjectBlocks(movableParameterValue);
	    }
	    Command fixedParameter = getParameter("fixed");
	    if (fixedParameter != null) {
		fixedRotationAxisList = parseAxisList(((StringParameter)(fixedParameter)).getValue());
		if (fixedRotationAxisList.size() != this.objectBlocks.size()) {
		    throw new CommandExecutionException("Number of rotation constraint vectors has to be equal to number of blocks.");
		}
	    }
	    Command placeParameter = getParameter("place");
	    if (placeParameter != null) {
		placeBestJunctions = ((StringParameter)placeParameter).parseBoolean();
	    }
	    Command vdwParameter = getParameter("vdw");
	    if (vdwParameter != null) {
		vdwWeight = Double.parseDouble(((StringParameter)vdwParameter).getValue());
	    }
	    Command symParameter = getParameter("symvdw");;;
	    if (symParameter != null) {
		this.symVdwActive = parseIntIds(((StringParameter)symParameter).getValue());
	    }
	}
	catch(ParsingException pe) {
	    throw new CommandExecutionException("Parsing exception: " + pe.getMessage());
	}
	catch(NumberFormatException nfe) {
	    throw new CommandExecutionException("Bad number format detected: " + nfe.getMessage());
	}
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " [options]" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Optimize distance and/or basepair constraints." + NEWLINE + NEWLINE;
	helpText += "OPTIONS" + NEWLINE;
	helpText += "     blocks=name1[;name2[...]][;name3[;name4[...]]]] : defines object blocks" + NEWLINE;
	helpText += "     error=DOUBLE" + NEWLINE + "          Set the error limit parameter." + NEWLINE;
	helpText += "     firstfixed=false|true   : Keep first block unchanged. Default: false." + NEWLINE;
	helpText += "     fixed=a,b,c[;d,e,f[;...]]   : constrain rotation axis of movable blocks." + NEWLINE;
	helpText += "     kt=DOUBLE : value of kT for optimization. Default: 2.0" + NEWLINE;
	helpText += "     movable=name1[,name2,[...]]" + NEWLINE;
	helpText += "     repuls=DOUBLE : weight of residue repulsion term. Default: 0.0" + NEWLINE;
	helpText += "     rms=DOUBLE" + NEWLINE + "          TODO" + NEWLINE + NEWLINE;
	helpText += "     steps=NUMBER : number of optimization steps." + NEWLINE;
	helpText += "     symvdw=ID1,ID2,...  : ids of symmetry copies that are used for repulsion term computation." + NEWLINE;
	helpText += "     vdw=DOUBLE   : weight of residue repulsion term. Default: 0.0" + NEWLINE;
	return helpText;
    }

    private String helpOutput() {
       return "Correct usage: " + COMMAND_NAME + " [blocks=name1[;name2[...]][;name3[;name4[...]]]] [error=value][firstfixed=false|true][kt=value][fixed=a,b,c[;d,e,f[...]]][place=true|false][repuls=<value>][rms=<value>] [steps=<value>][symvdw=ID1,ID2,...][vdw=WEIGHT]";
    }

}
    
