package rnadesign.designapp;

import java.util.logging.Logger;
import java.util.Properties;
import java.io.PrintStream;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.IntParameter;
import commandtools.StringParameter;
import generaltools.ParsingException;
import rnadesign.rnamodel.FitParameters;
import rnadesign.rnamodel.BranchDescriptorOptimizerFactory;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import rnadesign.rnacontrol.Object3DGraphControllerConstants;
import rnadesign.rnacontrol.Object3DGraphControllerEventConstants;
import controltools.*;
import rnadesign.rnamodel.HelixOptimizerTools;
import tools3d.objects3d.Object3DSet;
import tools3d.objects3d.SimpleObject3DSet;
import tools3d.objects3d.Object3D;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class OptimizeHelicesCommand extends AbstractCommand {

    private static Logger log = Logger.getLogger("NanoTiler_debug");
    
    public static final String COMMAND_NAME = "opthelices";

    private boolean addHelicesFlag = true; // import sequences after optimization

    private String helixRootName = "root";

    private boolean keepFirstFixed = true;

    private double kt = 30;

    private String movableNames = null;

    private String parentNames = null;

    private int connectionAlgorithm = BranchDescriptorOptimizerFactory.MONTE_CARLO_OPTIMIZER;
    
    private int helixMutInterval = 0; // if greater zero: every this many optimization steps make one helix-length-mutation move
    
    private int numSteps = 100000; // number of steps of optimization algorithm

    // determines which algorithm is used for optimization
    // private int optimizerAlgorithm = Object3DGraphController.SEQUENCE_OPTIMIZER_SCRIPT;
    private int optimizerAlgorithm = 0;

    // private double rmsLimit = 3.0; // rms limit for placement of mutated residue

    private double stemRmsLimit = 30.0; // 10.0; // 5.0;

    private double stemAngleLimit = Math.PI; // Math.PI / 4;

    private double errorScoreLimit = 0.1; // only very good score leads to termination before end

    private double errorScoreInitialLimit = 1000000000000000.0; // only very good score leads to termination before end

    private int fuseStrandsMode = HelixOptimizerTools.NO_FUSE;

    private Object3DGraphController controller;
    
    private PrintStream ps;

    public OptimizeHelicesCommand(Object3DGraphController controller, PrintStream ps) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
	this.ps = ps;
    }

    public Object cloneDeep() {
	OptimizeHelicesCommand command = new OptimizeHelicesCommand(this.controller, this.ps);
	command.addHelicesFlag = this.addHelicesFlag;
	command.helixRootName = this.helixRootName;
	command.keepFirstFixed = this.keepFirstFixed;
	command.kt = this.kt;
	command.movableNames = this.movableNames;
	command.parentNames = this.parentNames;
	command.connectionAlgorithm = this.connectionAlgorithm;
	command.numSteps = this.numSteps;
	command.optimizerAlgorithm = this.optimizerAlgorithm;
	// command.rmsLimit = this.rmsLimit;
	command.stemRmsLimit = this.stemRmsLimit;
	command.stemAngleLimit = this.stemAngleLimit;
	command.errorScoreLimit = this.errorScoreLimit;
	command.errorScoreInitialLimit = this.errorScoreInitialLimit;
	command.fuseStrandsMode = this.fuseStrandsMode;

	return command;
    }

    private Object3DSet generateMovableObjects(String movableNames) throws CommandExecutionException {
	if (movableNames == null) {
	    return null;
	}
	String[] words = movableNames.split(",");
	if ((words == null) || (words.length == 0)) {
	    return null;
	}
	Object3DSet oset = new SimpleObject3DSet();
	for (int i = 0; i < words.length; ++i) {
	    Object3D obj = controller.getGraph().findByFullName(words[i]);
	    if (obj != null) {
		oset.add(obj);
	    }
	    else {
		throw new CommandExecutionException("Could not find object: " + words[i]);
	    }
	}
	return oset;
    }

    private Object3DSet generateParentObjects(String parentNames) throws CommandExecutionException {
	Object3DSet oset = new SimpleObject3DSet();
 	if (parentNames == null) {
	    log.info("Warning: no parent objects have been specified with the " + COMMAND_NAME + " option parents=");
 	    // oset.add(controller.getGraph().getGraph()); // set to root of graph
	    return oset;
 	}
	String[] words = parentNames.split(",");
	if ((words == null) || (words.length == 0)) {
	    return null;
	}

	for (int i = 0; i < words.length; ++i) {
	    Object3D obj = controller.getGraph().findByFullName(words[i]);
	    if (obj != null) {
		oset.add(obj);
	    }
	    else {
		throw new CommandExecutionException("Could not find object: " + words[i]);
	    }
	}
	return oset;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	assert (this.controller != null);
	prepareReadout();
	ps.println("Starting to optimize helices!");
	FitParameters stemFitParameters = null;
	Properties properties = new Properties();
	if (addHelicesFlag) {
	    stemFitParameters = new FitParameters(stemRmsLimit, stemAngleLimit);
	    // TODO : specify stem rms, stem angle weight, stem angle error
	}
	try {
	    Object3DSet movableObjects = generateMovableObjects(movableNames);
	    Object3DSet parentObjects = generateParentObjects(parentNames);
	    if (movableObjects != null) {
		ps.println("Number of movable objects: " + movableObjects.size());
	    }
	    else {
		ps.println("All objects with constraints are considered movable.");
	    }
	    if (parentObjects != null) {
		ps.println("Number of parent objects that are moved as one block: " + parentObjects.size());
	    }
	    else {
		ps.println("No parent objects defined.");
	    }
	    ps.println("Starting controller.optimizeHelices!");
	    properties = controller.optimizeHelices(numSteps, errorScoreLimit, errorScoreInitialLimit, kt, stemFitParameters, helixRootName,
						    movableObjects, parentObjects,
						    connectionAlgorithm, fuseStrandsMode, keepFirstFixed, helixMutInterval);
	    ps.println("Finished controller.optimizeHelices!");
	}
	catch (Object3DGraphControllerException gce) {
	    throw new CommandExecutionException(gce.getMessage());
	}
	// 	HelixOptimizer optimizer = new HelixOptimizer(controller.getGraph().getGraph(), controller.getLinks(), numSteps, 
	// 						      errorScoreLimit);
	// 	Properties properties = optimizer.optimize();
	controller.refresh(new ModelChangeEvent(controller, Object3DGraphControllerEventConstants.MODEL_MODIFIED));
	ps.println("Helix optimization finished! Result:");
	ps.println(properties.toString());
	this.resultProperties = properties;
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	Command importParameter = getParameter("helices");
	if (importParameter != null) {
	    String value = ((StringParameter)importParameter).getValue().toLowerCase();
	    if (value.equals("true")) {
		addHelicesFlag = true;
	    }
	    else if (value.equals("false")) {
		addHelicesFlag = false;
	    }
	    else {
		throw new CommandExecutionException(helpOutput());
	    }
	}
	try {
	    Command ktParameter = getParameter("kt");
	    if (ktParameter != null) {
		this.kt = Double.parseDouble(((StringParameter)ktParameter).getValue());
		log.finest("Parsing kt and setting to : " + this.kt);
	    }
	    Command rmsParameter = getParameter("rms");
	    if (rmsParameter != null) {
		stemRmsLimit = Double.parseDouble(((StringParameter)rmsParameter).getValue());
	    }
	    Command stepsParameter = getParameter("steps");
	    if (stepsParameter != null) {
		numSteps = Integer.parseInt(((StringParameter)stepsParameter).getValue());
	    }
	    Command helixIntervalParameter = getParameter("bpinterval");
	    if (helixIntervalParameter != null) {
		helixMutInterval = Integer.parseInt(((StringParameter)helixIntervalParameter).getValue());
	    }
	    Command errorLimitParameter = getParameter("error");
	    if (errorLimitParameter != null) {
		errorScoreLimit = Double.parseDouble(((StringParameter)errorLimitParameter).getValue());
	    }
	    Command fuseParameter = getParameter("fuse");
	    if (fuseParameter != null) {
		String s = ((StringParameter)fuseParameter).getValue();
		if (s.equals("false")) {
		    fuseStrandsMode = HelixOptimizerTools.NO_FUSE;
		} else if (s.equals("append")) {
		    fuseStrandsMode = HelixOptimizerTools.FUSE_APPEND;
		} else if (s.equals("prepend")) {
		    fuseStrandsMode = HelixOptimizerTools.FUSE_PREPEND;
		}
		else {
		    throw new CommandExecutionException("Error parsing strand fuse mode (allowd: false|append|prepend):"
							+ s);
		}
	    }
	    Command errorLimitParameter2 = getParameter("initial-error");
	    if (errorLimitParameter2 != null) {
		errorScoreInitialLimit = Double.parseDouble(((StringParameter)errorLimitParameter2).getValue());
	    }
	    Command firstFixedParameter = getParameter("firstfixed");
	    if (firstFixedParameter != null) {
		keepFirstFixed = ((StringParameter)firstFixedParameter).parseBoolean();
	    }
	    Command helixRootParameter = getParameter("helixroot");
	    if (helixRootParameter != null) {
		helixRootName = ((StringParameter)(helixRootParameter)).getValue();
	    }
	    Command movableParameter = getParameter("movable");
	    if (movableParameter != null) {
		movableNames = ((StringParameter)(movableParameter)).getValue();
	    }
	    Command parentParameter = getParameter("parent");
	    if (parentParameter == null) {
		parentParameter = getParameter("blocks"); // alternative name
	    }
	    if (parentParameter != null) {
		parentNames = ((StringParameter)(parentParameter)).getValue();
	    }
	}
	catch (NumberFormatException nfe) {
	    throw new CommandExecutionException("Bad number format detected: " + nfe.getMessage());
	}
	catch (ParsingException pe) {
	    throw new CommandExecutionException("Bad format detected: " + pe.getMessage());
	}
// 	catch (ParsingException pe) {
// 	    throw new CommandExecutionException("Error parsing boolean value in opthelices command: " + pe.getMessage());
// 	}
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " [options]" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Optimize sequences command TODO." + NEWLINE + NEWLINE;
	helpText += "OPTIONS" + NEWLINE;
	helpText += "     blocks=name1,name2,..." + NEWLINE + "        Defines which blocks are considered a group in the optimization. Alternative option name: parent" + NEWLINE;
	helpText += "     helices=true|false" + NEWLINE + "          if true, add helices after optimization" + NEWLINE + NEWLINE;
	helpText += "     error=DOUBLE" + NEWLINE + "          Set the error limit parameter." + NEWLINE + NEWLINE;
	helpText += "     firstfixed=true|false    : iff true, keeps first block fixed during optimization (default:true)" + NEWLINE;
	helpText += "     fuse=false|append|prepend" + NEWLINE;
	helpText += "     bpinterval=INTEGER    : an integer that is greater or equal zero. If greater zero: every this many steps the helix lengths of the helix constrataints will be mutated." + NEWLINE;
	helpText += "     kt=DOUBLE" + NEWLINE + "           Sets the temperature tolerance parameter kt for simulated annealing." + NEWLINE;
	helpText += "     movable=name1,name2,..." + NEWLINE + "        Defines which blocks can be moved in the optimization." + NEWLINE;
	helpText += "     parent=name1,name2,..." + NEWLINE + "        Deprecated; use blocks=... option. Defines which blocks are considered a group in the optimization." + NEWLINE;
	helpText += "     rms=DOUBLE" + NEWLINE + "          TODO" + NEWLINE + NEWLINE;
	helpText += "     import=true|false" + NEWLINE + "          TODO" + NEWLINE + NEWLINE;
	helpText += "     helixroot=name  : insert generated helices at subtree with this name" + NEWLINE;
	helpText += "     steps=NUMBER    : number of optimization steps." + NEWLINE;
	return helpText;
    }

   private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " [firstfixed=true|false][fuse=false|append|prepend][helices=true|false][bpinterval=value][kt=value][movable=name1,name2,...][block|parent=name1,name2,...][rms=<value>][steps=<value>]";
    }


}
    
