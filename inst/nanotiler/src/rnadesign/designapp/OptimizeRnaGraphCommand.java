package rnadesign.designapp;

import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;
import java.util.Properties;
import java.io.PrintStream;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.IntParameter;
import commandtools.StringParameter;
import rnadesign.rnamodel.*;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import rnadesign.rnacontrol.Object3DGraphControllerConstants;
import rnadesign.rnacontrol.Object3DGraphControllerEventConstants;
import controltools.*;
import rnadesign.rnamodel.HelixOptimizer;
import tools3d.objects3d.Object3DSet;
import tools3d.objects3d.SimpleObject3DSet;
import tools3d.objects3d.Object3D;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class OptimizeRnaGraphCommand extends AbstractCommand {

    private static Logger log = Logger.getLogger("NanoTiler_debug");
    
    public static final String COMMAND_NAME = "optrnagraph";

    private String rootName = "root";

    private double offset = RnaConstants.JUNCTION_OFFSET;

    private int numSteps = 100000; // number of steps of optimization algorithm

    private double errorScoreLimit = 3.0; // only perfect score leads to termination before end

    private Object3DGraphController controller;

    private PrintStream ps;

    private int verboseLevel = 1;

    public OptimizeRnaGraphCommand(Object3DGraphController controller, PrintStream ps) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
	this.ps = ps;
    }

    public Object cloneDeep() {
	OptimizeRnaGraphCommand command = new OptimizeRnaGraphCommand(this.controller, this.ps);
	command.rootName = this.rootName;
	command.offset = this.offset;
	command.numSteps = this.numSteps;
	command.errorScoreLimit = this.errorScoreLimit;
	command.verboseLevel = this.verboseLevel;
	return command;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	assert (this.controller != null);
	prepareReadout();
	if (verboseLevel > 1) {
	    ps.println("Starting to optimize graph edges with parameters:" + rootName + " " + numSteps + " " + errorScoreLimit + " " + verboseLevel);
	}
	FitParameters stemFitParameters = null;
	Properties properties = new Properties();
	try {
	    properties = controller.optimizeRnaIntegerGraph(rootName, numSteps, errorScoreLimit, offset, verboseLevel);
	}
	catch (Object3DGraphControllerException gce) {
	    throw new CommandExecutionException(gce.getMessage());
	}
	// 	HelixOptimizer optimizer = new HelixOptimizer(controller.getGraph().getGraph(), controller.getLinks(), numSteps, 
	// 						      errorScoreLimit);
	// 	Properties properties = optimizer.optimize();
	controller.refresh(new ModelChangeEvent(controller, Object3DGraphControllerEventConstants.MODEL_MODIFIED));
	ps.println("Helix optimization finished! Result:");
	ps.println(properties.toString());
	this.resultProperties = properties;
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() < 1) {
	    helpOutput();
	    throw new CommandExecutionException("Missing parameter: root");
	}
	try {
	    Command stepsParameter = getParameter("steps");
	    if (stepsParameter != null) {
		numSteps = Integer.parseInt(((StringParameter)stepsParameter).getValue());
	    }
	    Command verboseParameter = getParameter("verbose");
	    if (verboseParameter != null) {
		verboseLevel = Integer.parseInt(((StringParameter)verboseParameter).getValue());
	    }
	    Command errorLimitParameter = getParameter("error");
	    if (errorLimitParameter != null) {
		errorScoreLimit = Double.parseDouble(((StringParameter)errorLimitParameter).getValue());
	    }
	    Command offsetParameter = getParameter("offset");
	    if (offsetParameter != null) {
		offset = Double.parseDouble(((StringParameter)offsetParameter).getValue());
	    }
	    Command rootParameter = getParameter("root");
	    if (rootParameter != null) {
		rootName = ((StringParameter)(rootParameter)).getValue();
	    }
	}
	catch(NumberFormatException nfe) {
	    throw new CommandExecutionException("Bad number format detected: " + nfe.getMessage());
	}
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " [options]" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Optimize graph edge lengths." + NEWLINE + NEWLINE;
	helpText += "OPTIONS" + NEWLINE;
	helpText += "     error=DOUBLE" + NEWLINE + "          Set the error limit parameter." + NEWLINE + NEWLINE;
	helpText += "     root=name  : use graph at subtree with this name" + NEWLINE;
	helpText += "     offset=value  : Length of uncovered helices." + NEWLINE;
	return helpText;
    }

    private String helpOutput() {
       return "Correct usage: " + COMMAND_NAME + " root=NAME [error=value] [steps=<value>] [offset=<value>]";
    }

}
    
