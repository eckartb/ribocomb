package rnadesign.designapp;

import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;
import java.util.Properties;
import java.io.PrintStream;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.IntParameter;
import commandtools.StringParameter;
import rnadesign.rnamodel.FitParameters;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import rnadesign.rnacontrol.Object3DGraphControllerConstants;
import rnadesign.rnacontrol.Object3DGraphControllerEventConstants;
import controltools.*;
import rnadesign.rnamodel.HelixOptimizer;
import tools3d.*;
import tools3d.objects3d.*;
import graphtools.IntegerPermutatorList;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class OptimizeSystematicCommand extends AbstractCommand {

    private static Logger log = Logger.getLogger("NanoTiler_debug");
    
    public static final String COMMAND_NAME = "optsystematic";

    private boolean addHelicesFlag = true; // import sequences after optimization

    private double electrostaticWeight = 0.0;

    private String helixRootName = "root";

    private List<Object3DSet> objectBlocks;

    private int outputInterval = 10000;

    private IntegerPermutatorList permutators;

    private double errorScoreLimit = 0.0; // only perfect score leads to termination before end

    private Object3DGraphController controller;

    private PrintStream ps;

    private double vdwWeight = 0.0;

    public OptimizeSystematicCommand(Object3DGraphController controller, PrintStream ps) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
	this.ps = ps;
    }

    public Object cloneDeep() {
	OptimizeSystematicCommand command = new OptimizeSystematicCommand(this.controller, this.ps);
	command.addHelicesFlag = this.addHelicesFlag;
	command.electrostaticWeight = this.electrostaticWeight;
	command.helixRootName = this.helixRootName;
	command.objectBlocks = this.objectBlocks;
	command.outputInterval = this.outputInterval;
	command.permutators = this.permutators;
	command.errorScoreLimit = this.errorScoreLimit;
	command.vdwWeight = this.vdwWeight;
	return command;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	assert (this.controller != null);
	prepareReadout();
	ps.println("Starting to optimize helices!");
	Properties properties = new Properties();
	if (objectBlocks == null) {
	    throw new CommandExecutionException("No object blocks defined!");
	}
	if (objectBlocks.size() == 0) {
	    throw new CommandExecutionException("Zero object blocks defined!");
	}
	try {
	    ps.println("Repulsion term weight: " + electrostaticWeight);
	    ps.println("Number of object blocks: " + objectBlocks.size());
	    properties = controller.optimizeSystematic(electrostaticWeight, vdwWeight, objectBlocks,
						       permutators, outputInterval);
	}
	catch (Object3DGraphControllerException gce) {
	    throw new CommandExecutionException(gce.getMessage());
	}
	// 	HelixOptimizer optimizer = new HelixOptimizer(controller.getGraph().getGraph(), controller.getLinks(), numSteps, 
	// 						      errorScoreLimit);
	// 	Properties properties = optimizer.optimize();
	controller.refresh(new ModelChangeEvent(controller, Object3DGraphControllerEventConstants.MODEL_MODIFIED));
	ps.println("Helix optimization finished! Result:");
	ps.println(properties.toString());
	this.resultProperties = properties;
    }

    /** parses string of form name1,name2,...;name3,name4,... into List of object sets */
    List<Object3DSet> parseObjectBlocks(String blocks) throws CommandExecutionException {
	List<Object3DSet> result = new ArrayList<Object3DSet>();
	String setWords[] = blocks.split(";");
	for (int i = 0; i < setWords.length; ++i) {
	    String objWords[] = setWords[i].split(",");
	    if (objWords.length < 1) {
		throw new CommandExecutionException("Error in optbasepairs: expected at least one object in " + setWords[i]);
	    }
	    Object3DSet set = new SimpleObject3DSet();
	    for (int j = 0; j < objWords.length; ++j) {
		String objName = objWords[j];
		Object3D obj = controller.getGraph().findByFullName(objName);
		if (obj == null) {
		    throw new CommandExecutionException("Could not find object with name: " + objName);
		}
		set.add(obj);
	    }
	    result.add(set);
	}
	return result;
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private Vector3D parseVector(String s) throws CommandExecutionException {
	if ((s == null) || (s.equals(""))) {
	    return null;
	}
	String[] words = s.split(",");
	if (words.length != 3) {
	    throw new CommandExecutionException("Expected 3 words in vector descriptor: " + s);
	}
	Vector3D result = new Vector3D();
	try {
	    result.setX(Double.parseDouble(words[0]));
	    result.setY(Double.parseDouble(words[1]));
	    result.setZ(Double.parseDouble(words[2]));
	}
	catch (NumberFormatException nfe) {
	    throw new CommandExecutionException("Error parsing item of vector descriptor: " + s);
	}
	if (result.length() == 0.0) {
	    throw new CommandExecutionException("Zero vector length found in vector descriptor: " + s);
	}
	return result;
    }

    private List<Vector3D> parseAxisList(String s) throws CommandExecutionException {
	String[] words = s.split(";");
	List<Vector3D> result = new ArrayList<Vector3D>();
	for (int i = 0; i < words.length; ++i) {
	    result.add(parseVector(words[i]));
	}
	return result;
    }

    private IntegerPermutatorList parseSystematicMutators(String s) throws CommandExecutionException {
	IntegerPermutatorList result = new IntegerPermutatorList();
	String[] words = s.split(";");
	for (String word : words) {
	    result.add(parseSystematicMutator(word));
	}
	return result;
    }

    private SystematicMutator2 parseSystematicMutator(String s) throws CommandExecutionException {
	String[] words = s.split(",");
	if ((words.length != 9) && (words.length != 6)) {
	    throw new CommandExecutionException("Error parseing systematic mutator, expected 9 comma-separated items (minx,miny,minz,n,dx,na,axisx,axisy,axisz): " + s);
	}
	Vector3D minPos = new Vector3D();
	Vector3D axis = null;
	int n = 0;
	double dx = 1.0;
	int na = 0;
	try {
	    minPos.setX(Double.parseDouble(words[0]));
	    minPos.setY(Double.parseDouble(words[1]));
	    minPos.setZ(Double.parseDouble(words[2]));
	    n = Integer.parseInt(words[3]);
	    if (n <= 0) {
		throw new CommandExecutionException("Number of grid elements must be greater zero: " + words[5]);
	    }
	    dx = Double.parseDouble(words[4]);
	    if (dx <= 0.0) {
		throw new CommandExecutionException("Size of grid spacing must be greater zero: " + words[5]);
	    }
	    na = Integer.parseInt(words[5]);
	    if (na <= 0) {
		throw new CommandExecutionException("Number of angle divisions must be greater zero: " + words[5]);
	    }
	    if (words.length >= 9) {
		axis = new Vector3D(Double.parseDouble(words[6]),Double.parseDouble(words[7]),Double.parseDouble(words[8]));
		if (axis.length() == 0.0) {
		    throw new CommandExecutionException("Rotation axis must have length greater zero: " 
							+ words[6] + " " + words[7] + " " + words[8]);
		}
	    }
	} catch (NumberFormatException nfe) {
	    throw new CommandExecutionException("Error parseing number in systematic mutator: " + s);
	}
	int[] maxNumbers = new int[SystematicMutator2.DEFAULT_SIZE];
	maxNumbers[SystematicMutator2.X_ID] = n;
	maxNumbers[SystematicMutator2.Y_ID] = n;
	maxNumbers[SystematicMutator2.Z_ID] = n;
	maxNumbers[SystematicMutator2.ANGLE_ID] = na;
	if (axis == null) {
	    maxNumbers[SystematicMutator2.AXIS_ID] = na;
	} else {
	    maxNumbers[SystematicMutator2.AXIS_ID] = 1;
	}
	SystematicMutator2 result = null;
	if (axis == null) {
	    result = new SystematicMutator2(dx, minPos, maxNumbers);
	} else {
	    result = new SystematicMutator2(dx, minPos, maxNumbers, axis);
	}
	return result;
    }

    private void prepareReadout() throws CommandExecutionException {
	Command importParameter = getParameter("helices");
	if (importParameter != null) {
	    String value = ((StringParameter)importParameter).getValue().toLowerCase();
	    if (value.equals("true")) {
		addHelicesFlag = true;
	    }
	    else if (value.equals("false")) {
		addHelicesFlag = false;
	    }
	    else {
		throw new CommandExecutionException(helpOutput());
	    }
	}
	try {
	    Command repulsParameter = getParameter("repuls");
	    if (repulsParameter != null) {
		electrostaticWeight = Double.parseDouble(((StringParameter)repulsParameter).getValue());
	    }
	    Command errorLimitParameter = getParameter("error");
	    if (errorLimitParameter != null) {
		errorScoreLimit = Double.parseDouble(((StringParameter)errorLimitParameter).getValue());
	    }
	    Command helixRootParameter = getParameter("helixroot");
	    if (helixRootParameter != null) {
		helixRootName = ((StringParameter)(helixRootParameter)).getValue();
	    }
	    String movableParameterValue = "";
	    movableParameterValue = parse("blocks", movableParameterValue);
	    if (movableParameterValue != null) {
		this.objectBlocks = parseObjectBlocks(movableParameterValue);
	    } else {
		throw new CommandExecutionException("Option blocks=... has to be specified.");
	    }
	    Command fixedParameter = getParameter("mut");
	    if (fixedParameter != null) {
		permutators = parseSystematicMutators(((StringParameter)(fixedParameter)).getValue());
		assert permutators != null;
		
		if (permutators.size() != this.objectBlocks.size()) {
		    throw new CommandExecutionException("Number of permutation descriptors has to be equal to number of blocks.");
		}
	    }
	    Command tParameter = getParameter("t");
	    if (tParameter != null) {
		this.outputInterval = Integer.parseInt(((StringParameter)tParameter).getValue());
	    }
	    Command vdwParameter = getParameter("vdw");
	    if (vdwParameter != null) {
		vdwWeight = Double.parseDouble(((StringParameter)vdwParameter).getValue());
	    }
	}
	catch(NumberFormatException nfe) {
	    throw new CommandExecutionException("Bad number format detected: " + nfe.getMessage());
	}
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " [options]" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Optimize distance and/or basepair constraints." + NEWLINE + NEWLINE;
	helpText += "OPTIONS" + NEWLINE;
	helpText += "     blocks=name1[,name2[...]][;name3[,name4[...]]]] : defines object blocks";
	helpText += "     error=DOUBLE" + NEWLINE + "          Set the error limit parameter." + NEWLINE + NEWLINE;
	helpText += "     mut=x,y,z,n,dx,na[,ax,ay,zy][;...]   : constrain space (defined by minimum positions x,y,z, grid width dx, number of grid elements per dimension n, number of rotational steps na, rotation axis ax,ay,az." + NEWLINE;
	helpText += "     repuls=DOUBLE : weight of residue repulsion term. Default: 0.0" + NEWLINE;
	helpText += "     t=OUTPUTINTERVAL : status output at specified intervals. Default: 10000" + NEWLINE;
	helpText += "     vdw=DOUBLE    : weight of Van de Waals term. Default: 0.0." + NEWLINE;
	return helpText;
    }

    private String helpOutput() {
       return "Correct usage: " + COMMAND_NAME + " [blocks=name1[,name2[...]][;name3[,name4[...]]]] [error=value][fixed=a,b,c[;d,e,f[...]]][mut=x,y,z,n,dx,na[,ax,ay,zy][;...]][repuls=<value>][vdw=<value>][rms=<value>] [steps=<value>] [t=OUTPUTINTERVALL]";
    }

}
    
