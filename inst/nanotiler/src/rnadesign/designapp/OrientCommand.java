package rnadesign.designapp;

import java.io.PrintStream;

import tools3d.objects3d.CoordinateSystem3D;
import rnadesign.rnacontrol.Object3DGraphController;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class OrientCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "orient";

    private Object3DGraphController controller;
    private String name;
    private String className = "Atom3D"; // ony orient using these names

    public OrientCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	OrientCommand command = new OrientCommand(this.controller);
	command.name = this.name;
	command.className = this.className;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME
	    + " [name=<OBJECTNAME>] [class=CLASSNAME]" + NEWLINE
	    + " with typical CLASSNAME values of Atom3D, RnaStrand3D and object names of root.A or 1.1.5";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + helpOutput();
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Normalized orientation along principal axis of moments of intertia tensor." + NEWLINE + NEWLINE;
	helpText += "OPTIONS" + NEWLINE;
	helpText += "     name=OBJECT" + NEWLINE + "          Only print the sub-tree from node with this name." + NEWLINE + NEWLINE;
	helpText += "     class=OBJECT" + NEWLINE + "          Atom3D, Nucleotide3D. Show only this type." + NEWLINE + NEWLINE;	
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	CoordinateSystem3D cs = controller.getGraph().applyNormalizedOrientation(name, className);
	System.out.println("Applied coordinate system transformation: " + cs);
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	StringParameter nameParameter = (StringParameter)(getParameter("name"));
	if (nameParameter != null) {
	    name = nameParameter.getValue();
	}
	StringParameter classNameParameter = (StringParameter)(getParameter("class"));
	if (classNameParameter != null) {
	    className = classNameParameter.getValue();
	}
    }
}
    
