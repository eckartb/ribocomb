package rnadesign.designapp;

import java.io.PrintStream;
import java.util.Properties;
import tools3d.objects3d.Object3D;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandException;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;

import org.testng.annotations.*; // for testing

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class PhysicsCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "physics";

    private Object3DGraphController controller;
    private String origName;

    public PhysicsCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	PhysicsCommand command = new PhysicsCommand(this.controller);
	command.origName = this.origName;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " [objectname]";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " OBJECTNAME" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     physics command returns some semi-physical properties of an object" + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	Object3D obj = null;
	if (origName != null) {
	    obj = controller.getGraph().findByFullName(origName);
	}
	System.out.println("Average position of atoms:");
	System.out.println(controller.getGraph().computeCenterOfMass(obj, "Atom3D"));
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() > 0) {
	    StringParameter p0 = (StringParameter)(getParameter(0));
	    origName = p0.getValue();
	}
    }

}
