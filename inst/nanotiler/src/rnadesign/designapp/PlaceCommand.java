package rnadesign.designapp;

import java.io.PrintStream;

import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import tools3d.Vector3D;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class PlaceCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "place";

    private Object3DGraphController controller;

    private String newParentName;
    private String newName;
    private String ending;
    private char subCommandChar;
    private Vector3D newPos;
    private int order;
    private int id;

    public PlaceCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	PlaceCommand command = new PlaceCommand(this.controller);
	command.newParentName = this.newParentName;
	command.newName = this.newName;
	command.newPos = this.newPos;
	command.order = this.order;
	command.id = this.id;

	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " j|k|m parentname name order index x y z strandending";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " j|k|m newparentname newname order id x y z strand-ending" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     places specified junction at certain position in space." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	try {
	    switch (subCommandChar) {
	    case 'j': controller.placeJunction(order, id, newParentName, newName, newPos, ending);
		break;
	    case 'k':
		controller.placeKissingLoop(order, id, newParentName, newName, newPos, ending);
		break;
	    case 'm':
		controller.placeMotif(order, id, newParentName, newName, newPos, ending);
		break;
	    default:
		throw new CommandExecutionException("Unknown subcommand: " + subCommandChar);
	    }
	}
	catch (Object3DGraphControllerException e) {
	    throw new CommandExecutionException(e.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() != 9) {
	    throw new CommandExecutionException(helpOutput());
	}
	StringParameter p0 = (StringParameter)(getParameter(0));
	if (!((p0.getValue().equals("j") || (p0.getValue().equals("k") || (p0.getValue().equals("m"))))) ) {
	    throw new CommandExecutionException("Expected j or k or m keyword");
	}
	subCommandChar = p0.getValue().charAt(0);
	StringParameter p1 = (StringParameter)(getParameter(1));
	newParentName = p1.getValue();
	StringParameter p2 = (StringParameter)(getParameter(2));
	newName = p2.getValue();
	StringParameter p3 = (StringParameter)(getParameter(3));
	order = Integer.parseInt(p3.getValue());
	StringParameter p4 = (StringParameter)(getParameter(4));
	id = Integer.parseInt(p4.getValue());
	id--; // convert to internal counting
	StringParameter p5 = (StringParameter)(getParameter(5));
	double x = Double.parseDouble(p5.getValue());
	StringParameter p6 = (StringParameter)(getParameter(6));
	double y = Double.parseDouble(p6.getValue());
	StringParameter p7 = (StringParameter)(getParameter(7));
	double z = Double.parseDouble(p7.getValue());
	this.newPos = new Vector3D(x,y,z);
	StringParameter endingParameter = (StringParameter)(getParameter(8));
	if (endingParameter != null) {
	    this.ending = endingParameter.getValue();
	}
    }

}
