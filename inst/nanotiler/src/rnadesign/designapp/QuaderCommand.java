package rnadesign.designapp;

import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;

import static rnadesign.designapp.PackageConstants.*;

public class QuaderCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "quader";

    private int xmin = 0;
    private int ymin = 0;
    private int zmin = 0;
    private int xmax = 0;
    private int ymax = 0;
    private int zmax = 0;

    private Object3DGraphController controller;
    
    public QuaderCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	QuaderCommand command = new QuaderCommand(controller);
	command.xmin = xmin;
	command.ymin = ymin;
	command.zmin = zmin;
	command.xmax = xmax;
	command.ymax = ymax;
	command.zmax = zmax;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " xmin ymin zmin xmax ymax zmax";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " xMin yMin zMin xMax yMax zMax" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Quader command TODO." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	assert (this.controller != null);
	prepareReadout();
	this.controller.setLatticeSection(xmin, ymin, zmin, xmax, ymax, zmax);

    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() != 6) {
	    throw new CommandExecutionException(helpOutput()); 
	}
	StringParameter p0 = (StringParameter)(getParameter(0));
	StringParameter p1 = (StringParameter)(getParameter(1));
	StringParameter p2 = (StringParameter)(getParameter(2));
	StringParameter p3 = (StringParameter)(getParameter(3));
	StringParameter p4 = (StringParameter)(getParameter(4));
	StringParameter p5 = (StringParameter)(getParameter(5));
	xmin = Integer.parseInt(p0.getValue());
	ymin = Integer.parseInt(p1.getValue());
	zmin = Integer.parseInt(p2.getValue());
	xmax = Integer.parseInt(p3.getValue());
	ymax = Integer.parseInt(p4.getValue());
	zmax = Integer.parseInt(p5.getValue());
	if ((xmax <= xmin) || (ymax <= ymin) || (zmax <= zmin)) {
	    throw new CommandExecutionException("Lattice section volume must be greater than zero!");
	}

    }
    
}
