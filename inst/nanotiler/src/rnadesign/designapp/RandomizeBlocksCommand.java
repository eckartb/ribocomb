package rnadesign.designapp;

import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;
import java.util.Properties;
import java.io.PrintStream;
import generaltools.ParsingException;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.IntParameter;
import commandtools.StringParameter;
import rnadesign.rnamodel.FitParameters;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import rnadesign.rnacontrol.Object3DGraphControllerConstants;
import rnadesign.rnacontrol.Object3DGraphControllerEventConstants;
import controltools.*;
import rnadesign.rnamodel.HelixOptimizer;
import tools3d.Vector3D;
import tools3d.objects3d.Object3DSet;
import tools3d.objects3d.SimpleObject3DSet;
import tools3d.objects3d.Object3D;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class RandomizeBlocksCommand extends AbstractCommand {

    private static Logger log = Logger.getLogger("NanoTiler_debug");
    
    public static final String COMMAND_NAME = "randomizeblocks";

    private double radius = 100.0;
    private List<Object3DSet> objectBlocks;
    private Object3DGraphController controller;
    private boolean orient = true;


    public RandomizeBlocksCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	RandomizeBlocksCommand command = new RandomizeBlocksCommand(this.controller);
	command.radius = this.radius;
  command.objectBlocks = this.objectBlocks;
  command.orient = this.orient;
	return command;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	assert (this.controller != null);
	prepareReadout();

  controller.randomizeBlocks(radius,objectBlocks, orient);
    }
    
    public Command execute() throws CommandExecutionException {
  executeWithoutUndo();
  return null;
    }

    /** parses string of form name1,name2,...;name3,name4,... into List of object sets */
    List<Object3DSet> parseObjectBlocks(String blocks) throws CommandExecutionException {
	if ((blocks == null) || (blocks.length() == 0)) {
	    throw new CommandExecutionException("blocks option has to be defined! See usage.");
	}
	List<Object3DSet> result = new ArrayList<Object3DSet>();
	String setWords[] = blocks.split(";");
	for (int i = 0; i < setWords.length; ++i) {
	    String objWords[] = setWords[i].split(",");
	    if (objWords.length < 1) {
		throw new CommandExecutionException("Error in optbasepairs: expected at least one object in " + setWords[i]);
	    }
	    Object3DSet set = new SimpleObject3DSet();
	    for (int j = 0; j < objWords.length; ++j) {
		String objName = objWords[j];
		Object3D obj = controller.getGraph().findByFullName(objName);
		if (obj == null) {
		    throw new CommandExecutionException("Could not find object with name: " + objName);
		}
		set.add(obj);
	    }
	    result.add(set);
	}
	return result;
    }


    private void prepareReadout() throws CommandExecutionException {

	try {
    Command radiusParameter = getParameter("r");
    if (radiusParameter != null) {
      radius = Double.parseDouble(((StringParameter)radiusParameter).getValue());
    }
    
    String movableParameterValue = "";
    movableParameterValue = parse("blocks", movableParameterValue);
    if (movableParameterValue != null) {
  this.objectBlocks = parseObjectBlocks(movableParameterValue);
    }
    
    Command orientParameter = getParameter("orient");
    if (orientParameter != null) {
  orient = ((StringParameter)orientParameter).parseBoolean();
    }
    
	}
  catch(ParsingException pe) {
      throw new CommandExecutionException("Parsing exception: " + pe.getMessage());
  }
	catch(NumberFormatException nfe) {
	    throw new CommandExecutionException("Bad number format detected: " + nfe.getMessage());
	}
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " [options]" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Move blocks to random points on sphere." + NEWLINE + NEWLINE;
	helpText += "OPTIONS" + NEWLINE;
	helpText += "     blocks=name1[;name2[...]][;name3[;name4[...]]]] : defines object blocks" + NEWLINE;
	helpText += "     radius=DOUBLE" + NEWLINE + "          Set the radius of the sphere. DEFUALT: 100" + NEWLINE;
  helpText += "     orient=boolean" + NEWLINE + "          Randomize orientations as well. DEFUALT: true" + NEWLINE;

	return helpText;
    }

    private String helpOutput() {
       return "Correct usage: " + COMMAND_NAME + " [blocks=name1[;name2[...]][;name3[;name4[...]]]] [radius=value][orient=true|false]";
    }

}
    
