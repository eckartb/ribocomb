package rnadesign.designapp;

import java.io.PrintStream;

import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class RandomizeCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "randomize";

    private Object3DGraphController controller;
    private String name;
    private String mode;
    private double scale = 10.0; // for translation mode

    public RandomizeCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	RandomizeCommand command = new RandomizeCommand(this.controller);
	command.name = this.name;
	command.mode = this.mode;
	command.scale = this.scale;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " name [mode=rotate|translate][scale=VALUE]";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " NAME" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Randomize command randomizes orientation of an object." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	if (name != null) {
	    try {
		controller.getGraph().randomize(name, mode,scale);
		controller.refreshLinks(); // remove bad links
	    }
	    catch(Object3DGraphControllerException e) {
		throw new CommandExecutionException(e.getMessage());
	    }
	}
	else {
	    throw new CommandExecutionException(helpOutput());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() >0 ) {
	    StringParameter p0 = (StringParameter)(getParameter(0));
	    name = p0.getValue();
	}
	else {
	    throw new CommandExecutionException(helpOutput()); 
	}
	StringParameter pmode = (StringParameter)(getParameter("mode"));
	if (pmode != null) {
	    mode = pmode.getValue();
	}
	try {
	    StringParameter pScale = (StringParameter)(getParameter("scale"));
	    if (pScale != null) {
		scale = Double.parseDouble(pScale.getValue());
	    }
	} catch (NumberFormatException nfe) {
	    throw new CommandExecutionException("Error parsing command line option for command randomize: " + nfe.getMessage());
	}
	    
    }

}
