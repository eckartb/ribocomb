package rnadesign.designapp;

import java.io.PrintStream;

import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class RemoveCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "remove";

    private Object3DGraphController controller;
    private String name;

    public RemoveCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	RemoveCommand command = new RemoveCommand(this.controller);
	command.name = this.name;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " name";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " NAME" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Remove command removes an object specified by its absolute name." + NEWLINE;
	helpText += "EXAMPLES: " + NEWLINE;
	helpText += "remove root.import.C   : removes imported strand C and its child nodes";
	helpText += "remove 1.1.3   : removes object with absolute numerical name 1.1.3 and its child nodes";
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	if (name != null) {
	    try {
		controller.remove(name);
		// controller.getGraph().remove(name);
		// controller.refreshLinks(); // remove bad links // BAD: should not contain controller code
	    }
	    catch(Object3DGraphControllerException e) {
		throw new CommandExecutionException(e.getMessage());
	    }
	}
	else {
	    throw new CommandExecutionException(helpOutput());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() == 1) {
	    StringParameter p0 = (StringParameter)(getParameter(0));
	    name = p0.getValue();
	}
	else {
	    throw new CommandExecutionException(helpOutput()); 
	}
    }

}
