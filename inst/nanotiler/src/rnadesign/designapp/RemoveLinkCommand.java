package rnadesign.designapp;

import java.io.PrintStream;

import rnadesign.rnacontrol.*;
import commandtools.AbstractCommand;
import commandtools.BadSyntaxException;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import commandtools.UnknownCommandException;

import static rnadesign.designapp.PackageConstants.NEWLINE;

/**
 * @author Christine Viets
 */
public class RemoveLinkCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "rmlink";

    private Object3DGraphController controller;
    private int linkId;
    private String linkName;
    
    public RemoveLinkCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	this.linkId = 0;
	this.controller = controller;
    }

    public Object cloneDeep() {
	RemoveLinkCommand command = new RemoveLinkCommand(this.controller);
	command.linkId = this.linkId;
        command.linkName = this.linkName;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " link-id|name=<name>";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "";
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME + NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "      " + helpOutput() + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE + "     " + COMMAND_NAME +
	    " removes a link given a link number. Find link number with command \"links\"";
	helpText += NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	LinkController linkController = controller.getLinks();
	if ((linkName == null) && ((linkId < 0) || (linkId >= linkController.size()))) {
	    throw new CommandExecutionException("Undefined link number: " + (linkId+1));
	}
	try {
	    if (linkName == null) {
		linkController.remove(linkId);
	    } else {
		linkController.remove(linkName);
	    }
	}
	catch (Object3DGraphControllerException gce) {
	    throw new CommandExecutionException(gce.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() == 0) {
	    throw new CommandExecutionException(helpOutput()); 
	}
	else if (getParameterCount() == 1) {
	    StringParameter pName = (StringParameter)getParameter("name");
            if (pName != null) {
		this.linkName = pName.getValue();
	    } else {
		StringParameter p0 = (StringParameter)getParameter(0);
		try {
		    linkId = Integer.parseInt(p0.toString()) - 1; // convert to internal counting
		}
		catch (NumberFormatException nfe) {
		    throw new CommandExecutionException("Error parsing integer value: " + p0.toString());
		}
	    }
	}
	else {
	    throw new CommandExecutionException(helpOutput());
	}
    }

}
