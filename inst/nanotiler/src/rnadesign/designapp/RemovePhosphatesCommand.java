package rnadesign.designapp;

import java.io.*;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import tools3d.objects3d.*;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class RemovePhosphatesCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "removephosphates";

    private Object3DGraphController controller;
    private String subcom = "";

    public RemovePhosphatesCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	RemovePhosphatesCommand command = new RemovePhosphatesCommand(controller);
	command.controller = this.controller;
	command.subcom = this.subcom;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME;
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " FILENAME" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     removephosphates command removes phosphate groups from 5' end of all RNA strands. " + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	assert (this.controller != null);
	prepareReadout();
	int result = controller.removeFivePrimePhosphates();
	System.out.println("# Removed phosphate groups from 5' ends of " + result + " sequences.");
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() > 0) {
	    StringParameter par1 = (StringParameter)(getParameter(0));
	    subcom = par1.getValue();
	}
    }
    
}
