package rnadesign.designapp;

import java.io.PrintStream;

import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class RenameCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "rename";

    private Object3DGraphController controller;
    private String originalName;
    private String newName;

    public RenameCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	RenameCommand command = new RenameCommand(this.controller);
	command.originalName = this.originalName;
	command.newName = this.newName;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " originalName newName " + PackageConstants.NEWLINE + "or rename selected: " + COMMAND_NAME + " newName";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " ORIGINALNAME NEWNAME" + NEWLINE +
	    "     or rename selected: " + COMMAND_NAME + " NEWNAME"+ NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Rename command allows user to rename an object." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	if (originalName != null) {
	    try {
		String oldSelection = null;
		try {
		    oldSelection = controller.getGraph().getSelectionRoot().getName();
		}
		catch (NullPointerException e) {
		    log.info("Nothing selected");
		}
		controller.getGraph().selectByFullName(originalName);
		controller.getGraph().rename(newName);
		if (oldSelection != null) {
		    controller.getGraph().selectByFullName(oldSelection);
		}
		else {
		    controller.getGraph().deselectAll();
		}
	    }
	    catch (Object3DGraphControllerException ce) {
		throw new CommandExecutionException(ce.getMessage());
	    }
	}
	else if (newName != null) {
	    try {
		controller.getGraph().rename(newName);
	    }
	    catch (Object3DGraphControllerException ce) {
		throw new CommandExecutionException(ce.getMessage());
	    }
	}
	else {
	    throw new CommandExecutionException("No name specified!");
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() == 1) {
	    StringParameter p0 = (StringParameter)(getParameter(0));
	    newName = p0.getValue();
	}
	else if (getParameterCount() == 2) {
	    StringParameter p0 = (StringParameter)(getParameter(0));
	    originalName = p0.getValue();
	    StringParameter p1 = (StringParameter)(getParameter(1));
	    newName = p1.getValue();
	}
	else {
	    throw new CommandExecutionException(helpOutput()); 
	}
    }

}
