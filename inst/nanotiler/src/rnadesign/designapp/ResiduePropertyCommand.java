package rnadesign.designapp;

import java.io.PrintStream;
import generaltools.ParsingException;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import sequence.SequenceStatus;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class ResiduePropertyCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "resproperty";

    private Object3DGraphController controller;
    private String residueNames;
    private String value;

    public ResiduePropertyCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	ResiduePropertyCommand command = new ResiduePropertyCommand(this.controller);
	command.residueNames = this.residueNames;
	command.value = this.value;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " residueNames residuenames status";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " residuenames status" + NEWLINE +
	    "     or rename selected: " + COMMAND_NAME + " NEWNAME"+ NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Changes status of residues. Example: resproperty A:3-5;B:9,11-15 adhoc . Instead of adhoc, also fragment or optimized can be used." 
	    + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	if (residueNames != null) {
	    try {
		controller.getSequences().setResiduesProperty(residueNames, SequenceStatus.name, value); // TODO : make more general 
	    }
	    catch (ParsingException pe) {
		throw new CommandExecutionException(pe.getMessage());
	    }
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() == 1) {
	    StringParameter p0 = (StringParameter)(getParameter(0));
	    value = p0.getValue();
	}
	else if (getParameterCount() == 2) {
	    StringParameter p0 = (StringParameter)(getParameter(0));
	    residueNames = p0.getValue();
	    StringParameter p1 = (StringParameter)(getParameter(1));
	    value = p1.getValue();
	}
	else {
	    throw new CommandExecutionException(helpOutput()); 
	}
    }

}
