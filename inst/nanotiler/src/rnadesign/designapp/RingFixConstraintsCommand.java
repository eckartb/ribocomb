package rnadesign.designapp;

import java.io.PrintStream;

import rnadesign.rnacontrol.Object3DGraphController;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class RingFixConstraintsCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "ringfixconstraints";

    private Object3DGraphController controller;

    private String[] junctionNames;

    public RingFixConstraintsCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	RingFixConstraintsCommand command = new RingFixConstraintsCommand(this.controller);
	command.junctionNames = this.junctionNames;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Generates helix constraints corresponding to a ring with as high symmetry as possible. Correct usage: " + COMMAND_NAME 
	    + " [junctions=subtree1,subtree2,...]";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + helpOutput();
	// helpText += "DESCRIPTION" + NEWLINE  + helpOutput();
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	if ((junctionNames == null) || (junctionNames.length == 0)) {
	    junctionNames = new String[1];
	    junctionNames[0] = controller.getGraph().getGraph().getFullName(); // typically set to "root"
	}
	controller.generateRingFixConstraints(junctionNames);
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	StringParameter p = (StringParameter)(getParameter("junctions"));
	if (p != null) {
	    junctionNames = p.getValue().split(",");
	}
    }
}
    
