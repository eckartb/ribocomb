package rnadesign.designapp;

import java.io.PrintStream;

import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class RingFuseCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "ringfuse";

    private Object3DGraphController controller;

    private String[] junctionNames;
    private String root = "root";
    private String name = "fused";

    public RingFuseCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	RingFuseCommand command = new RingFuseCommand(this.controller);
	command.junctionNames = this.junctionNames;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Fuses strands corresponding to a ring with as high symmetry as possible. Correct usage: " + COMMAND_NAME 
	    + " [junctions=subtree1,subtree2,...][root=SUBTREENAME][name=RESULTNAME]";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + helpOutput();
	// helpText += "DESCRIPTION" + NEWLINE  + helpOutput();
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	try {
	    controller.ringFuseStrands(junctionNames, root, name);
	}
	catch (Object3DGraphControllerException e) {
	    throw new CommandExecutionException("Error while performing automated ring fusing: " + e.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	StringParameter p = (StringParameter)(getParameter("junctions"));
	if (p != null) {
	    junctionNames = p.getValue().split(",");
	}
	p = (StringParameter)(getParameter("name"));
	if (p != null) {
	    name = p.getValue();
	}
	p = (StringParameter)(getParameter("root"));
	if (p != null) {
	    root = p.getValue();
	}
    }
}
    
