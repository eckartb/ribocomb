package rnadesign.designapp;

import java.util.Set;
import java.util.HashSet;
import java.util.Properties;

import java.io.*;
import java.text.ParseException;
import sequence.DuplicateNameException;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import rnadesign.rnacontrol.Mutation;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import commandtools.CommandApplication;
import sequence.UnknownSymbolException;
import generaltools.ParsingException;
import generaltools.Optimizer;
import rnasecondary.ImprovedSecondaryStructureParser;
import rnasecondary.SecondaryStructure;
import tools3d.objects3d.Object3DSet;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class ScoreConstraintsCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "scoreconstraints";

    private Object3DGraphController controller;
    private CommandApplication interpreter;
    private String modelFilename;
    
    public ScoreConstraintsCommand(Object3DGraphController controller, CommandApplication interpreter) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
	this.interpreter = interpreter;
	this.modelFilename = modelFilename;
    }

    public Object cloneDeep() {
	ScoreConstraintsCommand command = new ScoreConstraintsCommand(this.controller, interpreter);
	command.controller = this.controller;
	command.interpreter=this.interpreter;
	this.modelFilename = modelFilename;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String synopsis() { return "" + COMMAND_NAME + " [native=FILENAME]";}
				    // + " [force=false|true][rms=VALUE]" 
				    // + " chainname:<symbol><pos>[,<symbol><pos> ...] . Example: mutate A:C5,G15,A2 B:U1,G2,A15 D:CGAUCAAUGUA\n";
    

    private String helpOutput() {
	return "Scores the likelyhood of current structure to be the native state. Correct usage: " + synopsis();
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + synopsis() + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Scores the likelyhood of current structure to be a native state." + NEWLINE + NEWLINE;
	helpText += "OPTIONS" + NEWLINE + "native : define a different structure to get secondary structure from" + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	Properties properties = null;
	
	try{
		
		SecondaryStructure structure = null;
		if(modelFilename == null){
			structure = controller.generateSecondaryStructure();
		} else {
			Object3DGraphController newcontroller = new Object3DGraphController();
			NanoTilerScripter app = new NanoTilerScripter(newcontroller);
			app.runScriptLine("import "+modelFilename);
			structure = newcontroller.generateSecondaryStructure();
		}
		controller.prepareScoreConstraints(structure);
		String blocksString = "";
		Object3DSet blocks = controller.getGraph().findAllByFullName("root.import.*");
		for(int i=0; i<blocks.size(); i++){
			blocksString = blocksString + "root.import." + blocks.get(i).getName() + ";";
		}
		properties = interpreter.runScriptLine("optconstraints steps=0 kt=100 vdw=1 repuls=100 blocks=" + blocksString);
		double score = Double.parseDouble(properties.getProperty(Optimizer.FINAL_SCORE));
		System.out.println("Score: "+NEWLINE+score);
	}catch(Exception e){
		System.out.println(e);
	}
	
    }
	
    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	int numPar = getParameterCount();

		StringParameter p2 = (StringParameter)(getParameter("native"));
		if (p2 != null) {
	    	modelFilename = p2.getValue();
		} else{
			modelFilename = null;		
		}
	}
    
}
    
