package rnadesign.designapp;

import generaltools.ParsingException;
import rnadesign.rnamodel.FittingException;
import java.io.PrintStream;

import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DTools;
import rnadesign.rnamodel.BranchDescriptor3D;
import rnadesign.rnamodel.SimpleStrandJunctionDB;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import java.util.List;
import java.util.Properties;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class ScoreFitCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "scorefit";
    private PrintStream ps;
    private Object3DGraphController controller;
    private String bScaff1Name;
    private String bScaff2Name;
    private String bLoop1Name;
    private String bLoop2Name;
    private int n1 = 0;
    private int n2 = 0;
    private double angleWeight = 1.0; // weight of rotation term

    public ScoreFitCommand(PrintStream ps, Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
	this.ps = ps;
    }

    public Object cloneDeep() {
	ScoreFitCommand command = new ScoreFitCommand(this.ps, this.controller);
	command.name = this.name;
	command.bScaff1Name = bScaff1Name;
	command.bScaff2Name = bScaff2Name;
	command.bLoop1Name = bLoop1Name;
	command.bLoop2Name = bLoop2Name;
	command.n1 = n1;
	command.n2 = n2;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " (j|k) branchdescriporScaffoldName1 branchdescriptorScaffoldName2 branchdescriptorLoopName1 branchdescriptorLoopName2 [n1=val][n2=val]";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + helpOutput() +
	    NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Returns score of 2-way junctions in gap." + NEWLINE + NEWLINE;
	return helpText;
    }

    private void printEntry(PrintStream ps, Properties result) {
	ps.println("" + result);
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	Object3D obj1 = controller.getGraph().findByFullName(bScaff1Name);
	Object3D obj2 = controller.getGraph().findByFullName(bScaff2Name);
	Object3D obj3 = controller.getGraph().findByFullName(bLoop1Name);
	Object3D obj4 = controller.getGraph().findByFullName(bLoop2Name);
	if (obj1 == null) {
	    throw new CommandExecutionException("Could not find object 1 : " + bScaff1Name);
	}
	if (obj2 == null) {
	    throw new CommandExecutionException("Could not find object 2 : " + bScaff2Name);
	}
	if (obj3 == null) {
	    throw new CommandExecutionException("Could not find object 3 : " + bLoop1Name);
	}
	if (obj4 == null) {
	    throw new CommandExecutionException("Could not find object 4 : " + bLoop2Name);
	}
	if (!(obj1 instanceof BranchDescriptor3D)) {
	    throw new CommandExecutionException("Object 1 is not of type BranchDescriptor3D : " + bScaff1Name);
	}
	if (!(obj2 instanceof BranchDescriptor3D)) {
	    throw new CommandExecutionException("Object 2 is not of type BranchDescriptor3D : " + bScaff2Name);
	}
	if (!(obj3 instanceof BranchDescriptor3D)) {
	    throw new CommandExecutionException("Object 3 is not of type BranchDescriptor3D : " + bLoop1Name);
	}
	if (!(obj4 instanceof BranchDescriptor3D)) {
	    throw new CommandExecutionException("Object 4 is not of type BranchDescriptor3D : " + bLoop2Name);
	}

	BranchDescriptor3D b1 = (BranchDescriptor3D)obj1;
	BranchDescriptor3D b2 = (BranchDescriptor3D)obj2;
	BranchDescriptor3D b3 = (BranchDescriptor3D)obj3;
	BranchDescriptor3D b4 = (BranchDescriptor3D)obj4;
	
	this.resultProperties = SimpleStrandJunctionDB.scoreJunctionLoopFit(b1, b2, 
									    b3, b4, n1, n2, angleWeight);
	this.ps.println(this.resultProperties);
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() < 4) {
	    throw new CommandExecutionException(helpOutput());
	}
	this.bScaff1Name = ((StringParameter)(getParameter(0))).getValue();
	this.bScaff2Name = ((StringParameter)(getParameter(1))).getValue();
	this.bLoop1Name = ((StringParameter)(getParameter(2))).getValue();
	this.bLoop2Name = ((StringParameter)(getParameter(3))).getValue();
	
	try {
	    this.n1 = parse("n1", this.n1);
	    this.n2 = parse("n2", this.n2);
	}
	catch (NumberFormatException nfe) {
	    throw new CommandExecutionException(nfe.getMessage());
	}

    }

}
