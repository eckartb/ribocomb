package rnadesign.designapp;

import java.io.*;

import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerConstants;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class SecondaryCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "secondary";

    private Object3DGraphController controller;
    private PrintStream ps;
    private String fileName;
    private int format = Object3DGraphControllerConstants.SECONDARY_FORMAT;

    public SecondaryCommand(PrintStream ps, Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	assert ps != null;
	this.ps = ps;
	this.controller = controller;
    }

    public Object cloneDeep() {
	SecondaryCommand command = new SecondaryCommand(this.ps, this.controller);
	command.fileName = this.fileName;
	command.format = this.format;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + "[file=FILENAME][format=ct|fasta|sec|server]";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME + " [file=FILENAME] [format=ct|fasta|sec|server]"
	    + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Secondary command writes the secondary structure to a file." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	try {
	    if (fileName == null) { 
		controller.writeSecondaryStructure(ps, format);
	    } else {
		FileOutputStream fos = new FileOutputStream(fileName);
		PrintStream ps2 = new PrintStream(fos);
		controller.writeSecondaryStructure(ps2, format);
		fos.close();
	    }
	}
	catch (IOException ioe) {
	    throw new CommandExecutionException(ioe.getMessage());
	}
	catch (Object3DGraphControllerException e) {
	    throw new CommandExecutionException(e.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	StringParameter fileParameter = (StringParameter)(getParameter("file"));
	if (fileParameter != null) {
	    fileName = fileParameter.getValue();
	}
	StringParameter formatParameter = (StringParameter)(getParameter("format"));
	if (formatParameter != null) {
	    String formatString = formatParameter.getValue();
	    if (formatString.equals("ct")) {
		format = Object3DGraphControllerConstants.CT_FORMAT;
	    } else if (formatString.equals("sec")) {
		format = Object3DGraphControllerConstants.SECONDARY_FORMAT;
	    } else if (formatString.equals("fasta")) {
		format = Object3DGraphControllerConstants.FASTA_FORMAT;
	    } else if (formatString.equals("server")) { // use for web server
		format = Object3DGraphControllerConstants.SERVER_FORMAT;
	    } else {
		throw new CommandExecutionException("Unknown format string: " + formatString);
	    }
	}
    }

}
