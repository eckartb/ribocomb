package rnadesign.designapp;

import generaltools.Randomizer;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class SeedCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "seed";

    private long seed;

    public SeedCommand() {
	super(COMMAND_NAME);
    }

    public Object cloneDeep() {
	SeedCommand command = new SeedCommand();
	command.seed = this.seed;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Resets random number generator with specified seed value. Correct usage: " + COMMAND_NAME + " SEEDVALUE";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " [options]" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Seed command resets random number generator with specified seed value." + NEWLINE + NEWLINE;
	helpText += "Example: seed 11" + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	Randomizer.setSeed(seed);
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() == 1) {
	    StringParameter p0 = (StringParameter)(getParameter(0));
	    try {
		seed = Long.parseLong(p0.getValue());
	    }
	    catch (NumberFormatException e) {
		throw new CommandExecutionException("Bad number format: " + e.getMessage());
	    }
	}
	else {
	    throw new CommandExecutionException(helpOutput()); 
	}
    }

}
