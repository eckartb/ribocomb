package rnadesign.designapp;

import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import tools3d.Vector3D;

import static rnadesign.designapp.PackageConstants.*;

public class ShiftCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "shift";

    private double a = 0;
    private double b = 0;
    private double c = 0;
    private double scalex = 1.0;
    private double scaley = 1.0;
    private double scalez = 1.0;

    private Object3DGraphController controller;
    
    public ShiftCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	ShiftCommand command = new ShiftCommand(controller);
	command.a = a;
	command.b = b;
	command.c = c;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    private String helpOutput() {
	return "Shift selected objects. Correct usage: " + COMMAND_NAME + " x y z [scalex=value][scaley=value][scalez=value]";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " x y z [options]" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Shift command moves an object x y z." + NEWLINE + NEWLINE;
	helpText += "OPTIONS" + NEWLINE;
	helpText += "     scalex=NUM" + NEWLINE + "          X scale value." + NEWLINE + NEWLINE;
	helpText += "     scaley=NUM" + NEWLINE + "          Y scale value." + NEWLINE + NEWLINE;
	helpText += "     scalez=NUM" + NEWLINE + "          Z scale value." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	assert (this.controller != null);
	prepareReadout();
	Object3DController gControl = controller.getGraph();
	Vector3D shift = new Vector3D(scalex*a, scaley*b, scalez*c);
	if (gControl.getSelectionRoot() == null) {
	    gControl.translate(shift);
	}
	else {
	    gControl.translateSelected(shift);
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() < 3) {
	    throw new CommandExecutionException(helpOutput()); 
	}
	StringParameter p0 = (StringParameter)(getParameter(0));
	StringParameter p1 = (StringParameter)(getParameter(1));
	StringParameter p2 = (StringParameter)(getParameter(2));
	StringParameter scaleXPar = (StringParameter)(getParameter("scalex"));
	StringParameter scaleYPar = (StringParameter)(getParameter("scaley"));
	StringParameter scaleZPar = (StringParameter)(getParameter("scalez"));

	try {
	    a = Double.parseDouble(p0.getValue());
	    b = Double.parseDouble(p1.getValue());
	    c = Double.parseDouble(p2.getValue());
	    if (scaleXPar != null) {
		scalex = Double.parseDouble((scaleXPar.getValue()));
	    }
	    if (scaleYPar != null) {
		scaley = Double.parseDouble((scaleYPar.getValue()));
	    }
	    if (scaleZPar != null) {
		scalez = Double.parseDouble((scaleZPar.getValue()));
	    }
	}
	catch (NumberFormatException nfe) {
	    throw new CommandExecutionException("Bad number syntax in command shift : " 
						+ nfe.getMessage());
	}

    }
    
}
