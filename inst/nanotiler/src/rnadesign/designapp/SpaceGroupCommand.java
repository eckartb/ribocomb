package rnadesign.designapp;

import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import java.util.logging.*;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import static rnadesign.designapp.PackageConstants.*;

/** lets controller reads specified file with space group definition. 
 */
public class SpaceGroupCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "spacegroup";

    private String name;
    private int number;
    private String convention;
    private Logger log = Logger.getLogger(LOGFILE_DEFAULT);
    private Object3DGraphController controller;
    
    public SpaceGroupCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	SpaceGroupCommand command = new SpaceGroupCommand(controller);
	command.name = name;
	if (name != null) {
	    command.name = name.substring(0, name.length());
	}
	command.number = number;
	if (convention != null) {
	    command.convention = convention.substring(0, convention.length());;
	}
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " name=<NAME> number=id convention=ccp4|Hall|xHM";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " [options]" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Space group command TODO." + NEWLINE + NEWLINE;
	helpText += "OPTIONS" + NEWLINE;
	helpText += "     name=NAME" + NEWLINE + "          Set the name." + NEWLINE + NEWLINE;
	helpText += "     number=INT" + NEWLINE + "          TODO" + NEWLINE + NEWLINE;
	helpText += "     convention=ccp4|Hall|xHM" + NEWLINE + "          TODO" + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	assert (this.controller != null);
	prepareReadout();
	log.info("Readout values: " + number + " " + name + " " + convention );
	try {
	    if (name != null) {
		if (convention != null) {
		    log.info("Calling: controller.setSpaceGroup(" + name + ","
			     + convention + ")");
		    this.controller.setSpaceGroup(name, convention);
		}
		else {
		    log.info("Calling: controller.setSpaceGroup(" + name + "\")");
		    this.controller.setSpaceGroup(name);
		}
	    }
	    else {
		log.info("Calling: controller.setSpaceGroup(" + number + ")");
		this.controller.setSpaceGroup(number);
	    }
	}
	catch (Object3DGraphControllerException e) {
	    throw new CommandExecutionException("Error in command: " + toString() + " "
						+ e.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() != 1) {
	    throw new CommandExecutionException(helpOutput()); 
	}
	StringParameter namePar = (StringParameter)(getParameter("name"));
	StringParameter numberPar = ((StringParameter)(getParameter("number")));
	StringParameter conventionPar = ((StringParameter)(getParameter("convention")));
	if ((namePar != null) && (numberPar != null)) {
	    throw new CommandExecutionException("Specify either space group name or number, but not both.");
	}
	else if ((namePar == null) && (numberPar == null)) {
	    throw new CommandExecutionException("Specify either space group name or number with name=\"name\" or number=id");
	}
	if (namePar != null) {
	    name = namePar.getValue();
	}
	if (numberPar != null) {
	    number = Integer.parseInt(numberPar.getValue());
	}
	if (conventionPar != null) {
	    convention = conventionPar.getValue();
	}
    }
    
}
