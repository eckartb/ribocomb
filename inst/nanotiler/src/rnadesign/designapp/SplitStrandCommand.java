package rnadesign.designapp;

import java.io.PrintStream;

import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class SplitStrandCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "splitstrand";

    private Object3DGraphController controller;
    private String name;
    private int position = -1;

    public SplitStrandCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	SplitStrandCommand command = new SplitStrandCommand(this.controller);
	command.name = this.name;
	command.position = this.position;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " strandfullname position";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME 
	    + " strandfullname position" + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     splitstrand command splits a strand before specified position." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	if (position < 0) {
	    throw new CommandExecutionException("No position specified. Usage: splitstrand strandfullname position");
	}
	try {
	    controller.splitStrand(name, position);
	}
	catch (Object3DGraphControllerException e) {
	    throw new CommandExecutionException(e.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() != 2) {
	    throw new CommandExecutionException(helpOutput());
	}
	try {
	    StringParameter p0 = (StringParameter)(getParameter(0));
	    this.name = p0.getValue();
	    StringParameter p1 = (StringParameter)(getParameter(1));
	    this.position = Integer.parseInt(p1.getValue()) - 1; // convert to internal counting
	}
	catch (NumberFormatException nfe) {
	    throw new CommandExecutionException("Error parsing position in splitstrand: " + nfe.getMessage());
	}
    }

}
