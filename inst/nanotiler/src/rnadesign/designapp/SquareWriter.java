package rnadesign.designapp;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Random;
import generaltools.Randomizer;
import tools3d.Vector3D;

public class SquareWriter {

    public static final String ENDING = ".points";

    private static Random rand = Randomizer.getInstance();

    private static double distortValue(double x, double distort) {
	double d = 2.0 * (rand.nextDouble() - 0.5); // between -1 and 1
	return x  + (distort * d); 
    }

    private static Vector3D distortPosition(Vector3D pos, 
			     double distort) {
	return new Vector3D(distortValue(pos.getX(), distort),
			    distortValue(pos.getY(), distort),
			    distortValue(pos.getZ(), distort));
    }

    /** writes point */
    private static void writePoint(PrintStream pw,
			    Vector3D p) {
	pw.print("" + p.getX() + " " + p.getY() + " " + p.getZ());
    }

    /** writes square, connects 1-2, 2-3, 3-4, 4-1 */
    private static void writeSquare(PrintStream pw, 
			     Vector3D[] points) {
	assert (points.length == 4);
	pw.println("4");
	for (int i = 0; i < points.length; ++i) {
	    pw.print("" + (i+1) + " ");
	    writePoint(pw, points[i]);
	    pw.println();
	}
	pw.println("4"); // number of links
	pw.println("1 2");
	pw.println("2 3");
	pw.println("3 4");
	pw.println("1 4");
    }

    private static void writeSquare(PrintStream pw,
			     double width,
			     double distort) {
	Vector3D[] p = new Vector3D[4];
	p[0] = new Vector3D(0, 0, 0);
	p[1] = new Vector3D(width, 0, 0);
	p[2] = new Vector3D(width, width, 0);
	p[3] = new Vector3D(0, width, 0);
	for (int i = 0; i < p.length; ++i) {
	    p[i] = distortPosition(p[i], distort);
	}
	writeSquare(pw, p);
    }


    public static void helpOut(PrintStream pw) {
	pw.println("Usage: SquareWriter filenamebase");
    }

    public static void main(String[] args) {
	
	double width = 100.0;
	double distort = 30.0;
	if (args.length == 0) {
	    helpOut(System.out);
	    System.exit(0);
	}
	String fileNameBase = args[0];
	for (int i = 0; i < 100; ++i) {
	    String fileName = fileNameBase + "_" + (i + 1) + ENDING;
	    try {
		FileOutputStream fos = new FileOutputStream(fileName);
		PrintStream pw = new PrintStream(fos);
		writeSquare(pw, width, distort);
	    }
	    catch (IOException ioe) {
		System.out.println("Error writing to file!");
	    }

	}

    }

}
