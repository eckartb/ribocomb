package rnadesign.designapp;

import java.io.*;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import tools3d.objects3d.*;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class StatusCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "status";

    private Object3DGraphController controller;
    private PrintStream ps;
    private String subcom = "";

    public StatusCommand(PrintStream ps, Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
	this.ps = ps;
    }

    public Object cloneDeep() {
	StatusCommand command = new StatusCommand(ps, controller);
	command.ps = this.ps;
	command.controller = this.controller;
	command.subcom = this.subcom;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " [basepairs]";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " FILENAME" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Status command displays the defined base pairs, junctions and kissing loop data. " + NEWLINE + NEWLINE;
	return helpText;
    }

    private void executeSingleBaseStatus() {
	if (this.controller.getNucleotideDB() == null) {
	    System.out.println("No reference nucleotides are defined. Consider command loadnucleotides.");
	}
	else {
	    Object3DSet nucs = Object3DTools.collectByClassName(this.controller.getNucleotideDB(), "Nucleotide3D");
	    System.out.println("" + nucs.size());
	    for (int i = 0; i < nucs.size(); ++i) {
		System.out.print(nucs.get(i).getName() + " ");
	    }
	    System.out.println();
	}
    }

    private void executeBasePairStatus() {
	ps.println(this.controller.getBasePairDB().toInfoString());
    }

    private void executeJunctionStatus() {
	ps.println(this.controller.getJunctionController().infoString());
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	assert (this.controller != null);
	prepareReadout();
	if (subcom.equals("basepairs")) {
	    executeBasePairStatus();
	}
	else if (subcom.equals("")) {
	    ps.println("Status of read reference bases: " + NEWLINE);
	    executeSingleBaseStatus();
	    ps.println("Status of read reference base pairs: " + NEWLINE);
	    executeBasePairStatus();
	    ps.println("Status of read reference junctions and kissing loops: " + NEWLINE);
	    executeJunctionStatus();
	}
	else {
	    throw new CommandExecutionException("Unknown command modifier: status " + subcom);
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() > 0) {
	    StringParameter par1 = (StringParameter)(getParameter(0));
	    subcom = par1.getValue();
	}
    }
    
}
