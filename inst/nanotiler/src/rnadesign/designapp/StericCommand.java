package rnadesign.designapp;

import java.io.*;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import java.util.logging.*;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class StericCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "steric";

    private Object3DGraphController controller;
    private PrintStream ps;
    private String subcom = "";
    private Level verboseLevel = Level.INFO;
    private double distance = 0.5;

    public StericCommand(PrintStream ps, Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
	this.ps = ps;
    }

    public Object cloneDeep() {
	StericCommand command = new StericCommand(ps, controller);
	command.ps = this.ps;
	command.controller = this.controller;
	command.subcom = this.subcom;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    private String helpOutput() {
	return "Returns number of non-hydrogen atom collisions. Correct usage: " + COMMAND_NAME + " [d=COLLISION_DISTANCE]";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " d=COLLISION_DISTANCE" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Returns number of non-hydrogen atom collisions." + NEWLINE + NEWLINE;
	return helpText;
    }

    /** prints number of atom collisions. Also returns it as property */
    public void executeWithoutUndo() throws CommandExecutionException {
	assert (this.controller != null);
	prepareReadout();
	int numCollisions = controller.getGraph().countAtomCollisions(distance, verboseLevel);
	ps.println("Number of collisions: " + numCollisions);
	resultProperties.setProperty("atom_collisions", "" + numCollisions);
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
// 	if (getParameterCount() > 0) {
// 	    StringParameter par1 = (StringParameter)(getParameter(0));
// 	    subcom = par1.getValue();
// 	}
        StringParameter distPar = (StringParameter)(getParameter("d"));
	try {
	    if (distPar != null) {
		distance = Double.parseDouble(distPar.getValue());
	    }
	} catch (NumberFormatException nfe) {
	    throw new CommandExecutionException("Error parsing distance value: " + nfe.getMessage());
	}
    }
    
}
