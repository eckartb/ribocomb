package rnadesign.designapp;

import java.io.*;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import java.util.Properties;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class SuperposableCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "superposable";

    private Object3DGraphController controller;
    private PrintStream ps;
    private double rms = 3.0;
    private String subcom = "";

    public SuperposableCommand(PrintStream ps, Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
	this.ps = ps;
    }

    public Object cloneDeep() {
	SuperposableCommand command = new SuperposableCommand(ps, controller);
	command.ps = this.ps;
	command.controller = this.controller;
	command.rms = this.rms;
	command.subcom = this.subcom;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " [junctions|kl|strands]";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " FILENAME" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     superposable command list properties of all versus all superposition of strands, junctions or kissing loops." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	assert (this.controller != null);
	prepareReadout();
	if (subcom.equals("junctions")) {
	    controller.getJunctionController().printSuperposableJunctions(rms, ps);
	}
	else if (subcom.equals("kl")) {
	    controller.getJunctionController().printSuperposableKissingLoops(rms, ps);
	}
	else if (subcom.equals("strands")) {
	    if (getParameterCount() < 3) {
		throw new CommandExecutionException("Missing object names! Usage: superposable strands name1 name2");
	    }
	    try {
		for (int i = 1; i < getParameterCount(); ++i) {
		    for (int j = 1; j < i; ++j) {
			Properties properties = controller.getGraph().superposeStrands(((StringParameter)getParameter(i)).getValue(),
										       ((StringParameter)getParameter(j)).getValue());
			this.resultProperties = properties;
			ps.println(properties.toString());
		    }
		}
	    }
	    catch (Object3DGraphControllerException gce) {
		throw new CommandExecutionException(gce.getMessage());
	    }
	}
	else {
	    throw new CommandExecutionException("Could not generate superpositions. Use superposable junctions|kl");
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() > 0) {
	    StringParameter par1 = (StringParameter)(getParameter(0));
	    subcom = par1.getValue();
	}
    }
    
}
