package rnadesign.designapp;

import java.io.PrintStream;
import java.util.Properties;
import tools3d.objects3d.Object3D;
import tools3d.CoordinateSystem;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandException;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;

import org.testng.annotations.*; // for testing

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class SymApplyCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "symapply";

    private Object3DGraphController controller;
    private String origName;
    private int symId = 0;

    public SymApplyCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	SymApplyCommand command = new SymApplyCommand(this.controller);
	command.origName = this.origName;
	command.symId = this.symId;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " rootname + symmetryid";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " OBJECTNAME SYMMETRYID" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     symapply applies symmetry transformation to subtree" + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	try {
	    controller.getGraph().applySymmetry(origName, symId);
	} catch (Object3DGraphControllerException oce) {
	    throw new CommandExecutionException(oce.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() != 2) {
	    throw new CommandExecutionException("Two parameters expected in " + COMMAND_NAME + " : " + helpOutput());
	}
	StringParameter p0 = (StringParameter)(getParameter(0));
	origName = p0.getValue();
	StringParameter p1 = (StringParameter)(getParameter(1));
	try {
	    symId = Integer.parseInt(p1.getValue());
	} catch (NumberFormatException nfe) {
	    throw new CommandExecutionException("Error parsing symmetry id in symapply command: " + nfe.getMessage());
	}
    }

}
