package rnadesign.designapp;

import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerConstants;
import rnadesign.rnacontrol.Object3DGraphControllerException;

import static rnadesign.designapp.PackageConstants.*;

public class SymmetryCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "symmetry";

    private int mode = Object3DGraphControllerConstants.GRID_SHAPE_DEFAULT;

    private Object3DGraphController controller;
    
    public SymmetryCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	SymmetryCommand command = new SymmetryCommand(controller);
	command.mode = mode;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " none|cell|all";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " none|cell|all" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Symmetry command TODO." + NEWLINE + NEWLINE;
	// helpText += "OPTIONS" + NEWLINE;
	// helpText += "     name=NAME" + NEWLINE + "          Set the name of the imported file." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	assert (this.controller != null);
	prepareReadout();
	try {
	    this.controller.setSymmetryMode(mode);
	}
	catch (Object3DGraphControllerException e) {
	    throw new CommandExecutionException("Error in symmetry command: " + e.getMessage());
	}

    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() != 1) {
	    throw new CommandExecutionException(helpOutput()); 
	}
	StringParameter p0 = (StringParameter)(getParameter(0));
	String s = p0.getValue();
	s = s.toLowerCase();
	if (s.equals("none")) {
	    mode = Object3DGraphControllerConstants.SYMMETRY_NONE;
	}
	else if (s.equals("cell")) {
	    mode = Object3DGraphControllerConstants.SYMMETRY_CELL;
	}
	else if (s.equals("all")) {
	    mode = Object3DGraphControllerConstants.SYMMETRY_ALL;
	}
	else {
	    throw new CommandExecutionException("Command symmetry: unknown parameter " + s);
	}

    }
    
}
