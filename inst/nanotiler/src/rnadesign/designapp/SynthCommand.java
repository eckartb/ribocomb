package rnadesign.designapp;

import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import tools3d.Vector3D;
import tools3d.objects3d.Object3DTools;

import static rnadesign.designapp.PackageConstants.*;

public class SynthCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "synth";

    private double x = 0;
    private double y = 0;
    private double z = 0;
    private String className;
    private String name;
    private String parentName;
    private double scalex = 1.0;
    private double scaley = 1.0;
    private double scalez = 1.0;
    
    private Object3DGraphController controller;
    
    public SynthCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	SynthCommand command = new SynthCommand(controller);
	command.x = x;
	command.y = y;
	command.z = z;
	command.className = className;
	command.name = name;
	command.parentName = parentName;
	command.scalex = scalex;
	command.scaley = scaley;
	command.scalez = scalez;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " classname|simple name parentfullname x y z [scalex=val][scaley=val][scalez=val]";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " classname|simple name parentfullname x y z [scalex=val] [scaley=val] [scalez=val]" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Synth command synthesizes an object of specified type at a specified location." + NEWLINE + NEWLINE;
	// helpText += "OPTIONS" + NEWLINE;
	// helpText += "     name=NAME" + NEWLINE + "          Set the name of the imported file." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	assert (this.controller != null);
	prepareReadout();
	try {
	    this.controller.getGraph().synthesizeObject(className, 
							new Vector3D(scalex*x,scaley*y,scalez*z), 
							name, parentName);
	}
	catch (Object3DGraphControllerException ce) {
	    throw new CommandExecutionException("Error in synth command: " + ce.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if ((getParameterCount() < 6)) {
	    throw new CommandExecutionException(helpOutput()); 
	}
	StringParameter p0 = (StringParameter)(getParameter(0));
	StringParameter p1 = (StringParameter)(getParameter(1));
	StringParameter p2 = (StringParameter)(getParameter(2));
	StringParameter p3 = (StringParameter)(getParameter(3));
	StringParameter p4 = (StringParameter)(getParameter(4));
	StringParameter p5 = (StringParameter)(getParameter(5));
	className = p0.getValue();
	name = p1.getValue();
	if (!Object3DTools.isAllowedName(name)) {
	    throw new CommandExecutionException("Illegal object name: " + name);
	}
	parentName = p2.getValue();
	try {
	    x = Double.parseDouble(p3.getValue());
	    y = Double.parseDouble(p4.getValue());
	    z = Double.parseDouble(p5.getValue())
;	    StringParameter scaleXPar = (StringParameter)(getParameter("scalex"));
	    StringParameter scaleYPar = (StringParameter)(getParameter("scaley"));
	    StringParameter scaleZPar = (StringParameter)(getParameter("scalez"));
	    if (scaleXPar != null) {
		scalex = Double.parseDouble(scaleXPar.getValue());
	    }
	    if (scaleYPar != null) {
		scaley = Double.parseDouble(scaleYPar.getValue());
	    }
	    if (scaleZPar != null) {
		scalez = Double.parseDouble(scaleZPar.getValue());
	    }

	}
	catch (NumberFormatException nfe) {
	    throw new CommandExecutionException("Bad number syntax in command rotate : " 
						+ nfe.getMessage());
	}
    }
    
}
