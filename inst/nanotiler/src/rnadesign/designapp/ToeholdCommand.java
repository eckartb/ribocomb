package rnadesign.designapp;

import java.io.PrintStream;
import java.util.Properties;

import generaltools.ParsingException;
import generaltools.PropertyTools;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class ToeholdCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "toehold";

    private Object3DGraphController controller;

    private String root = "root";
    private String resultRoot = "root";
    private String name = "fused";
    private double bridgeRms = 3.0;
    private double cutoff = 5; // previously: 3.0;
    private int lengthMin = 5;
    private boolean stepMode = false;
    private String fivePrimeAtom = "P";
    private String threePrimeAtom = "O3*";

    public ToeholdCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	ToeholdCommand command = new ToeholdCommand(this.controller);
	command.root = this.root;
	command.resultRoot = this.resultRoot;
	command.name = this.name;
	command.bridgeRms = this.bridgeRms;
	command.cutoff = this.cutoff;
        command.lengthMin = this.lengthMin;
	command.stepMode = this.stepMode;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Generates toeholds for sequence pairs that correspond to blunt helical ends. Correct usage: " + COMMAND_NAME 
	    + " [cutoff=VALUE]";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + helpOutput();
	// helpText += "DESCRIPTION" + NEWLINE  + helpOutput();
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	try {
	    int count = controller.createToeholds(root, lengthMin, fivePrimeAtom, threePrimeAtom, cutoff);
	    System.out.println("# Performed " + count + " strand fusions!");
	} catch (Object3DGraphControllerException gce) {
	    throw new CommandExecutionException(gce.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	StringParameter p = (StringParameter)(getParameter("name"));
	if (p != null) {
	    name = p.getValue();
	}
	p = (StringParameter)(getParameter("root"));
	if (p != null) {
	    root = p.getValue();
	    resultRoot = root;
	}
	p = (StringParameter)(getParameter("name"));
	if (p != null) {
	    name = p.getValue();
	}
	try {
	    p = (StringParameter)(getParameter("rms"));
	    if (p != null) {
		this.bridgeRms = Double.parseDouble(p.getValue());
	    }
	    p = (StringParameter)(getParameter("cutoff"));
	    if (p != null) {
		this.cutoff = Double.parseDouble(p.getValue());
	    }
	    p = (StringParameter)(getParameter("length"));
	    if (p != null) {
		this.lengthMin = Integer.parseInt(p.getValue());
	    }
	    p = (StringParameter)(getParameter("step"));
	    if (p != null) {
		this.stepMode = p.parseBoolean(p.getValue());
	    }
	}
	catch (NumberFormatException nfe) {
	    throw new CommandExecutionException("Could not interpret number: " + nfe.getMessage());
	}
	catch (ParsingException pe) {
	    throw new CommandExecutionException("Could parse parameter: " + pe.getMessage());
	}

	if (!validate()) {
	    throw new CommandExecutionException("Invalid parameters for fuseallstrands command!");
	}
    }

    /** Returns true if all parameters make sense */
    public boolean validate() {
	return cutoff > 0.0 && lengthMin >= 0 && lengthMin > 0 && name != null && name.length() > 0 
	    && root != null && root.length() > 0 && resultRoot != null && resultRoot.length() > 0;
    }
}
    
