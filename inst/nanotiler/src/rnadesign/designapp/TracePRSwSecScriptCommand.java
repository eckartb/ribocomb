package rnadesign.designapp;

import java.io.*;
import java.text.ParseException;
import java.util.*;
import tools3d.symmetry2.*;
import tools3d.objects3d.*;
import rnadesign.rnacontrol.*;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.StringParameter;
import commandtools.CommandExecutionException;
import generaltools.ParsingException;
import rnasecondary.*;

import static rnadesign.designapp.PackageConstants.NEWLINE;

/** For a give secondary structure, create a NanoTiler script, that if exectuted, creates a 3D structure corresponding to the RNA secondary structure. */
public class TracePRSwSecScriptCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "traceprswsec";

    private Object3DGraphController controller;
    private String format = "sec";
    private String algorithm = "helix";
    private String name = "traced";
    private String fileName;
    private String prsfileName;
    private boolean unique = false;

    private String inputFile; 
    private List<SymEdgeConstraint> symConstraints;
    private Properties params = new Properties();

    public TracePRSwSecScriptCommand(Object3DGraphController controller) {
    	super(COMMAND_NAME);
    	assert controller != null;
    	this.controller = controller;
    }

    public Object cloneDeep() {
	TracePRSwSecScriptCommand command = new TracePRSwSecScriptCommand(this.controller);
	command.algorithm = this.algorithm;
	command.name = this.name;
	command.fileName = this.fileName;
  command.prsfileName = this.prsfileName;
	command.inputFile = this.inputFile;
	command.params=this.params;
	command.symConstraints = this.symConstraints;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " in=INPUT_SECONDARY_FILE prs=INPUT_PRS_FILE file=SCRIPT_OUTPUT_FILENAME [datahome=path] [format=sec|ct]";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
    	String helpText = getShortHelpText() + NEWLINE + "\"" + COMMAND_NAME + "\" Command Manual" +
    	    NEWLINE + NEWLINE;
    	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
    	    NEWLINE + NEWLINE;
    	helpText += "in=INPUT_SECONDARY_FILE : secondary structure to be traced (currently NanoTiler sec notation and CT format)";
    	helpText += "file=FILENAME  : file name of script that is being generated." + NEWLINE;
      helpText += "prs=INPUT_PRS_FILE : input PARSYS file containing carbon prime tertiary information." + NEWLINE;
      helpText += "datahome=PATH  : path to motif directory." + NEWLINE;
      helpText += "format=ct|sec : specify the input secondary structure file format. sec format by default " + NEWLINE;
    	helpText += "DESCRIPTION" + NEWLINE +
    	    "Generates a script for the prediction of RNA structures with both secondary and tertiary information" + NEWLINE + NEWLINE;
    	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
    	prepareReadout();
    	try {
         SecondaryStructureParser parser = null;
    	    if ("sec".equals(format)) {
    	    // open input file
    	      parser = new ImprovedSecondaryStructureParser();
          } else if ("ct".equals(format)) {
            parser = new CTFileParser();
          } else {
            throw new CommandExecutionException("Unknown format (only sec and CT are implmeneted):" + format);
          }
    	    SecondaryStructure secondaryStructure = parser.parse(inputFile);
    	    // read condary structure
    	    
    	    System.out.println("# Generating secondary structure trace script from file " + inputFile);
          
    		String scriptFileName = (fileName + ".script");
        TertiaryMotifBlockController motifs;
        try{
        if(this.params.get("datahome")!=null){
          motifs = this.controller.initTertiaryMotifBlockController(this.params.getProperty("datahome"));
        } else{
          motifs = this.controller.initTertiaryMotifBlockController();
        }
      } catch(IOException e){
        throw new CommandExecutionException("Error reading motif database: " + e.getMessage());
      }
        
          if(motifs==null){
            throw new CommandExecutionException("Motif database not found at: " + this.params.getProperty("datahome"));
          }
          String pdbName;
          if(unique){
            pdbName = "${1}";
          } else{
            pdbName = fileName;
          }
          
          String result = ScriptController.generatePRSwSecScript(secondaryStructure, inputFile, prsfileName, pdbName, motifs.getDatabase(),  params, algorithm);
    	    
    	    log.info("Writing script to file " + scriptFileName);
    	    FileOutputStream fos = new FileOutputStream(scriptFileName);
    	    PrintStream ps = new PrintStream(fos);
    	    ps.println(result);
    	    fos.close();
    	    log.info("Closing generated script file " + scriptFileName);
    	    resultProperties.setProperty("output", result);
    	}
    	catch (IOException ioe) {
    	    throw new CommandExecutionException("IOException in " + COMMAND_NAME + " : " + ioe.getMessage());
    	}
    	catch (Object3DGraphControllerException gce) {
    	    throw new CommandExecutionException("Error retrieving objects: " + gce.getMessage());
    	}
    	catch (ParseException pe) {
    	    throw new CommandExecutionException("Error parsing secondary structure: " + pe.getMessage());
    	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	StringParameter secFile = (StringParameter)(getParameter("in"));
	if (secFile == null) {
	    throw new CommandExecutionException(helpOutput()); 
	}
	inputFile = secFile.getValue();
	StringParameter scriptFile = (StringParameter)(getParameter("file"));
	if (scriptFile == null) {
    throw new CommandExecutionException(helpOutput()); 
	}
	fileName = scriptFile.getValue();
  StringParameter prsFile = (StringParameter)(getParameter("prs"));
  if (prsFile == null) {
    throw new CommandExecutionException(helpOutput());
  } 
  prsfileName = prsFile.getValue();
  
  
  try{
    Command uniqueNamesParameter = getParameter("unique");
     if (uniqueNamesParameter != null) {
   unique = ((StringParameter)uniqueNamesParameter).parseBoolean();
     }
   }catch(ParsingException pe) {
         throw new CommandExecutionException("Parsing exception: " + pe.getMessage());
  }

  StringParameter repuls = (StringParameter)(getParameter("repuls"));
  if (repuls != null) {
      this.params.setProperty("repuls", repuls.getValue());
  }
  StringParameter steps = (StringParameter)(getParameter("steps"));
  if (steps != null) {
      this.params.setProperty("steps", steps.getValue());
  }
  StringParameter datahome = (StringParameter)(getParameter("datahome"));
  if (datahome != null) {
      this.params.setProperty("datahome", datahome.getValue());
  }
  StringParameter formatParam = (StringParameter)(getParameter("format"));
  if (formatParam != null) { // expect "sec" [default] or "ct";
      this.format=formatParam.getValue();
  }
    }

}
