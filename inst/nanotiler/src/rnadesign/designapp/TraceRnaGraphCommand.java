package rnadesign.designapp;

import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;
import java.util.Properties;
import java.io.PrintStream;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.IntParameter;
import commandtools.StringParameter;
import rnadesign.rnamodel.*;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import rnadesign.rnacontrol.Object3DGraphControllerConstants;
import rnadesign.rnacontrol.Object3DGraphControllerEventConstants;
import controltools.*;
import rnadesign.rnamodel.HelixOptimizer;
import tools3d.objects3d.Object3DSet;
import tools3d.objects3d.SimpleObject3DSet;
import tools3d.objects3d.Object3D;
import generaltools.ParsingException;

import static rnadesign.designapp.PackageConstants.*;

public class TraceRnaGraphCommand extends AbstractCommand {

    private static Logger log = Logger.getLogger(LOGFILE_DEFAULT);
    
    public static final String COMMAND_NAME = "tracernagraph";

    private double kt = 10.0;

    private int helixLengthMax = 1000;

    private String name = "traced";

    private int numSteps = 100000; // number of steps of optimization algorithm

    private double errorScoreLimit = 5.0; // only perfect score leads to termination before end

    private Object3DGraphController controller;

    private PrintStream ps;

    private double offset = RnaConstants.JUNCTION_OFFSET;

    private double distMax = 7.0;
    
    private double distMin = 3.0;

    private boolean generateBridges = false;

    private String rootName = "root";

    private int verboseLevel = 1;

    private boolean modify = false;

    public TraceRnaGraphCommand(Object3DGraphController controller, PrintStream ps) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
	this.ps = ps;
    }

    public Object cloneDeep() {
	TraceRnaGraphCommand command = new TraceRnaGraphCommand(this.controller, this.ps);
	command.kt = this.kt;
        command.helixLengthMax = this.helixLengthMax;
	command.name = this.name;
	command.numSteps = this.numSteps;
	command.errorScoreLimit = this.errorScoreLimit;
	command.offset = this.offset;
	command.distMax = this.distMax;
	command.distMin = this.distMin;
	command.rootName = this.rootName;
	command.verboseLevel = this.verboseLevel;
	return command;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	assert (this.controller != null);
	prepareReadout();
	ps.println("Starting to optimize helices!");
	FitParameters stemFitParameters = null;
	Properties properties = new Properties();
	try {
	    if (modify) {
		properties = controller.traceAndModifyRnaGraph(rootName, numSteps, kt, errorScoreLimit, offset, distMax, distMin, name, verboseLevel);
	    }
	    else {
		properties = controller.traceRnaGraph(rootName, numSteps, kt, errorScoreLimit, offset, distMax, distMin, name, verboseLevel,
						      generateBridges, helixLengthMax);
	    }
	}
	catch (Object3DGraphControllerException gce) {
	    throw new CommandExecutionException(gce.getMessage());
	}
	// 	HelixOptimizer optimizer = new HelixOptimizer(controller.getGraph().getGraph(), controller.getLinks(), numSteps, 
	// 						      errorScoreLimit);
	// 	Properties properties = optimizer.optimize();
	controller.refresh(new ModelChangeEvent(controller, Object3DGraphControllerEventConstants.MODEL_MODIFIED));
	ps.println("Helix optimization finished! Result:");
	ps.println(properties.toString());
	this.resultProperties = properties;
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	try {
	    Command ktParameter = getParameter("kt");
	    if (ktParameter != null) {
		kt = Double.parseDouble(((StringParameter)ktParameter).getValue());
	    }
	    Command bridgeParameter = getParameter("bridge");
	    if (bridgeParameter != null) {
		generateBridges = ((StringParameter)bridgeParameter).parseBoolean();
	    }
	    Command stepsParameter = getParameter("steps");
	    if (stepsParameter != null) {
		numSteps = Integer.parseInt(((StringParameter)stepsParameter).getValue());
	    }
	    Command verboseParameter = getParameter("verbose");
	    if (verboseParameter != null) {
		verboseLevel = Integer.parseInt(((StringParameter)verboseParameter).getValue());
	    }
	    Command errorLimitParameter = getParameter("error");
	    if (errorLimitParameter != null) {
		errorScoreLimit = Double.parseDouble(((StringParameter)errorLimitParameter).getValue());
	    }
	    Command offsetParameter = getParameter("offset");
	    if (offsetParameter != null) {
		offset = Double.parseDouble(((StringParameter)offsetParameter).getValue());
	    }
	    Command maxParameter = getParameter("max");
	    if (maxParameter != null) {
		distMax = Double.parseDouble(((StringParameter)maxParameter).getValue());
	    }
	    Command bpmaxParameter = getParameter("bpmax");
	    if (bpmaxParameter != null) {
		helixLengthMax = Integer.parseInt(((StringParameter)bpmaxParameter).getValue());
	    }
	    Command rootParameter = getParameter("root");
	    if (rootParameter != null) {
		rootName = ((StringParameter)(rootParameter)).getValue();
	    }
	    Command nameParameter = getParameter("name");
	    if (nameParameter != null) {
		name = ((StringParameter)(nameParameter)).getValue();
	    }
	    Command modParameter = getParameter("mod");
	    if (modParameter != null) {
		modify = ((StringParameter)(modParameter)).parseBoolean();
	    }
	}
	catch(NumberFormatException nfe) {
	    throw new CommandExecutionException("Bad number format detected: " + nfe.getMessage());
	}
	catch (ParsingException pe) {
	    throw new CommandExecutionException("Parsing error encountered: " + pe.getMessage());
	}
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " [options]" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Generate RNA helices that trace a 3D graph." + NEWLINE + NEWLINE;
	helpText += "OPTIONS" + NEWLINE;
	helpText += "     bpmax=1..N   : maximum number of base pairs per helix." + NEWLINE; 
	helpText += "     error=DOUBLE" + NEWLINE + "          Set the error limit parameter." + NEWLINE + NEWLINE;
        helpText += "     bridge=false|true : generate single-stranded connections between generated RNA helices." + NEWLINE;
	helpText += "     root=STRING  : use graph at subtree with this name" + NEWLINE;
	helpText += "     max=VALUE  : maximum allowed distance between helical residues of strands that are to be connected." + NEWLINE;
	helpText += "     name=STRING  : base name for generated RNA structure" + NEWLINE;
	helpText += "     offset=VALUE  : Length of ends not traced with helices" + NEWLINE;
	return helpText;
    }

    private String helpOutput() {
       return "Correct usage: " + COMMAND_NAME + " root=NAME [bridge=false|true][error=value] [steps=<value>] [offset=<value>] [max=<value>] [mod=false|true]";
    }

}
    
