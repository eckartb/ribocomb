package rnadesign.designapp;

import java.io.*;
import java.util.*;
import tools3d.symmetry2.*;
import tools3d.objects3d.*;
import rnadesign.rnacontrol.*;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.StringParameter;
import commandtools.CommandExecutionException;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class TraceRnaGraphScriptCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "tracegraphscript";

    private Object3DGraphController controller;
    private String algorithm = "helix";
    private String objectNames;
    private boolean kissingLoopMode = false;
    private String name = "traced";
    private String fileName;
    private List<SymEdgeConstraint> symConstraints;
    private Properties params = new Properties();

    public TraceRnaGraphScriptCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	TraceRnaGraphScriptCommand command = new TraceRnaGraphScriptCommand(this.controller);
	command.algorithm = this.algorithm;
	command.name = this.name;
	command.fileName = this.fileName;
        command.kissingLoopMode = this.kissingLoopMode;
	command.objectNames = this.objectNames;
	command.params=this.params;
	command.symConstraints = this.symConstraints;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " graph=NAME file=FILENAME [alg=db|helix] [kl=false|true]";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	
	String helpText = getShortHelpText() + NEWLINE + "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "graph=NAME  : full name of root node of graph to be traced." + NEWLINE;
	helpText += "file=FILENAME  : file name of script that is being generated." + NEWLINE;
        helpText += "alg=db|helix   : chose algorithm; db: tries to identify matching junctions/kissing loops from a library(previously loaded with loadjunctions); helix: places helices along graph such that helix ends can be connected by single-stranded linker regions." + NEWLINE; 
	helpText += "kl=false|true  : Specifies whether generated junction constraint correspond to junctions (false) or kissing loops. Default: false." + NEWLINE; 
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Generates a script for tracing a graph with RNA helices." + NEWLINE + NEWLINE;
	return helpText;
    }

    String[] parseNames(String name) {
	return name.split(";");
    }

    SymEdgeConstraint parseSymEdgeConstraint(String s) throws CommandExecutionException {
	String[] words = s.split(",");
	if (words.length != 5) {
	    throw new CommandExecutionException("Expected 5 ids in edge symmetry constraint: id1, id2, origId1, origId2, symId");
	}
	SymEdgeConstraint result = null;
	try {
	    result = new SymEdgeConstraint(Integer.parseInt(words[0]) - 1,
					   Integer.parseInt(words[1]) - 1,
					   Integer.parseInt(words[2]) - 1,
					   Integer.parseInt(words[3]) - 1,
					   Integer.parseInt(words[4])); // sym id counts start at zero, object id counts start at 1
	} catch (NumberFormatException nfe) {
	    throw new CommandExecutionException("Error parsing integer value in symmetry edge constraint descriptor: " + s);
	}
	return result;
    }

    List<SymEdgeConstraint> parseSymEdgeConstraints(String s) throws CommandExecutionException {
	List<SymEdgeConstraint> result = new ArrayList<SymEdgeConstraint>();
	String[] words = s.split(";");
	for (int i = 0; i < words.length; ++i) {
	    result.add(parseSymEdgeConstraint(words[i]));
	}
	return result;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	if ((name == null) || (name.equals(""))) {
	    throw new CommandExecutionException("Command select: Undefined object name. " + helpOutput());
	}
	try {
	    Object3DController graphController = controller.getGraph();
	    String[] names = parseNames(objectNames);
	    Object3DSet objSet = new SimpleObject3DSet();
	    for (String s : names) {
		Object3DSet objs = graphController.findAllByFullName(s);
		if (objs == null) {
		    throw new CommandExecutionException("Could not find object with name " + s);
		}
		objSet.merge(objs);
	    }
	    System.out.println("Generating graph trace script with " + objSet.size() + " vertices.");
// 	    if ((symConstraints != null) && (symConstraints.size() > 0)) {
// 		if (symConstraints.size() != (objSet.size()/2)) {
// 		    throw new CommandExecutionException("Unequal number of object blocks and symmetry operations for this junction: "
// 							+ objSet.size() + " : " + symConstraints.size());
// 		}
// 	    }
	    String result = ScriptController.generateGraphTraceScript(objSet.get(0), controller.getLinks(), name, symConstraints,
								      params, algorithm, kissingLoopMode);
	    System.out.println(result);
	    FileOutputStream fos = new FileOutputStream(fileName);
	    PrintStream ps = new PrintStream(fos);
	    ps.println(result);
	    fos.close();
	    resultProperties.setProperty("output", result);
	}
	catch (IOException ioe) {
	    throw new CommandExecutionException("IOException in " + COMMAND_NAME + " : " + ioe.getMessage());
	}
	catch (Object3DGraphControllerException gce) {
	    throw new CommandExecutionException("Error retrieving objects: " + gce.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	StringParameter p0 = (StringParameter)(getParameter("graph"));
	if (p0 == null) {
	    throw new CommandExecutionException(helpOutput()); 
	}
	objectNames = p0.getValue();
	StringParameter pa = (StringParameter)(getParameter("alg"));
	if (pa != null) {
	    this.algorithm = pa.getValue();
	}
	StringParameter p1 = (StringParameter)(getParameter("file"));
	if (p1 == null) {
	    throw new CommandExecutionException(helpOutput()); 
	}
	fileName = p1.getValue();
	StringParameter p2 = (StringParameter)(getParameter("name"));
	if (p2 != null) {
	    name = p2.getValue();
	}
	StringParameter symP = (StringParameter)(getParameter("sym"));
	if (symP != null) {
	    this.symConstraints = parseSymEdgeConstraints(symP.getValue());
	}
	StringParameter maxP = (StringParameter)(getParameter("max"));
	if (maxP != null) {
	    this.params.setProperty("max", maxP.getValue());
	    log.info("changed max parameter to: " + params.getProperty("max"));
	}
	StringParameter minP = (StringParameter)(getParameter("min"));
	if (minP != null) {
	    this.params.setProperty("min", minP.getValue());
	}
	StringParameter klp = (StringParameter)(getParameter("kl"));
	if (klp != null) {
	    try {
		kissingLoopMode = klp.parseBoolean();
	    } catch (generaltools.ParsingException pe) {
		throw new CommandExecutionException(pe.getMessage());
	    }
	}

    }

}
