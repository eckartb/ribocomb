package rnadesign.designapp;

import java.io.*;
import java.text.ParseException;
import java.util.*;
import tools3d.symmetry2.*;
import tools3d.objects3d.*;
import rnadesign.rnacontrol.*;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.StringParameter;
import commandtools.CommandExecutionException;
import generaltools.ParsingException;
import rnasecondary.*;

import static rnadesign.designapp.PackageConstants.NEWLINE;

/** For a give secondary structure, create a NanoTiler script, that if exectuted, creates a 3D structure corresponding to the RNA secondary structure. */
public class TraceSecondaryRnaScriptCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "tracesecondary";

    private Object3DGraphController controller;
    private String format = "sec";
    private String algorithm = "helix";
    // private String objectNames;
    // private boolean kissingLoopMode = false;
    private String name = "traced";
    private String fileName;
    private boolean unique = false
    ;
    /** Filename corresponding to input secondary structure. */
    private String inputFile; 
    private List<SymEdgeConstraint> symConstraints;
    private Properties params = new Properties();

    public TraceSecondaryRnaScriptCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	TraceSecondaryRnaScriptCommand command = new TraceSecondaryRnaScriptCommand(this.controller);
	command.algorithm = this.algorithm;
	command.name = this.name;
	command.fileName = this.fileName;
	command.inputFile = this.inputFile;
        // command.kissingLoopMode = this.kissingLoopMode;
	// command.objectNames = this.objectNames;
	command.params=this.params;
	command.symConstraints = this.symConstraints;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " i=INPUT_SECONDARY_FILE file=SCRIPT_OUTPUT_FILENAME [alg=db|helix] [datahome=path] [format=sec|ct]";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	
	String helpText = getShortHelpText() + NEWLINE + "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "i=INPUT_SECONDARY_FILE : secondary structure to be traced (currently NanoTiler sec notation and CT format)";
	helpText += "file=FILENAME  : file name of script that is being generated." + NEWLINE;
  helpText += "datahome=PATH  : path to motif directory." + NEWLINE;
  helpText += "format= ct|sec : specify the input secondary structure file format. sec format by default " + NEWLINE;

        // helpText += "alg=db|helix   : chose algorithm; db: tries to identify matching junctions/kissing loops from a library(previously loaded with loadjunctions); helix: places helices along graph such that helix ends can be connected by single-stranded linker regions." + NEWLINE; 
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Generates a script for tracing a graph with RNA helices." + NEWLINE + NEWLINE;
	return helpText;
    }

  //   String[] parseNames(String name) {
	// return name.split(";");
  //   }

  //   SymEdgeConstraint parseSymEdgeConstraint(String s) throws CommandExecutionException {
	// String[] words = s.split(",");
	// if (words.length != 5) {
	//     throw new CommandExecutionException("Expected 5 ids in edge symmetry constraint: id1, id2, origId1, origId2, symId");
	// }
	// SymEdgeConstraint result = null;
	// try {
	//     result = new SymEdgeConstraint(Integer.parseInt(words[0]) - 1,
	// 				   Integer.parseInt(words[1]) - 1,
	// 				   Integer.parseInt(words[2]) - 1,
	// 				   Integer.parseInt(words[3]) - 1,
	// 				   Integer.parseInt(words[4])); // sym id counts start at zero, object id counts start at 1
	// } catch (NumberFormatException nfe) {
	//     throw new CommandExecutionException("Error parsing integer value in symmetry edge constraint descriptor: " + s);
	// }
	// return result;
  //   }
  // 
  //   List<SymEdgeConstraint> parseSymEdgeConstraints(String s) throws CommandExecutionException {
	// List<SymEdgeConstraint> result = new ArrayList<SymEdgeConstraint>();
	// String[] words = s.split(";");
	// for (int i = 0; i < words.length; ++i) {
	//     result.add(parseSymEdgeConstraint(words[i]));
	// }
	// return result;
  //   }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	// if ((name == null) || (name.equals(""))) {
	//     throw new CommandExecutionException("Command select: Undefined object name. " + helpOutput());
	// }
	try {
     SecondaryStructureParser parser = null;
	    if ("sec".equals(format)) {
	    // open input file
	      parser = new ImprovedSecondaryStructureParser();
      } else if ("ct".equals(format)) {
        parser = new CTFileParser();
      } else {
        throw new CommandExecutionException("Unknown format (only sec and CT are implmeneted):" + format);
      }
	    SecondaryStructure secondaryStructure = parser.parse(inputFile);
	    // read condary structure
	    
	    System.out.println("# Generating secondary structure trace script from file " + inputFile);
// 	    if ((symConstraints != null) && (symConstraints.size() > 0)) 
// 		if (symConstraints.size() != (objSet.size()/2)) 
// 		    throw new CommandExecutionException("Unequal number of object blocks and symmetry operations for this junction: "
// 							+ objSet.size() + " : " + symConstraints.size());
// 		
		String scriptFileName = (fileName + ".script");
    MotifBlockController motifs;
    try{
    if(this.params.get("datahome")!=null){
      motifs = this.controller.initMotifBlockController(this.params.getProperty("datahome"));
    } else{
      motifs = this.controller.initMotifBlockController();
    }
  } catch(IOException e){
    throw new CommandExecutionException("Error reading motif database: " + e.getMessage());
  }
    
      if(motifs==null){
        throw new CommandExecutionException("Motif database not found at: " + this.params.getProperty("datahome"));
      }
      String pdbName;
      if(unique){
        pdbName = "${1}";
      } else{
        pdbName = fileName;
      }
// 	
	    //String result = ScriptController.generateSecondaryTraceScript(secondaryStructure, inputFile, fileName, params, algorithm);
      String result = ScriptController.generateSecondaryInternalLoopTraceScript(secondaryStructure, inputFile, pdbName, motifs.getDatabase(),  params, algorithm);

	    //System.out.println(result);
	    
	    log.info("Writing script to file " + scriptFileName);
	    FileOutputStream fos = new FileOutputStream(scriptFileName);
	    PrintStream ps = new PrintStream(fos);
	    ps.println(result);
	    fos.close();
	    log.info("Closing generated script file " + scriptFileName);
	    resultProperties.setProperty("output", result);
	}
	catch (IOException ioe) {
	    throw new CommandExecutionException("IOException in " + COMMAND_NAME + " : " + ioe.getMessage());
	}
	catch (Object3DGraphControllerException gce) {
	    throw new CommandExecutionException("Error retrieving objects: " + gce.getMessage());
	}
	catch (ParseException pe) {
	    throw new CommandExecutionException("Error parsing secondary structure: " + pe.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	StringParameter p0 = (StringParameter)(getParameter("i"));
	if (p0 == null) {
	    throw new CommandExecutionException(helpOutput()); 
	}
	inputFile = p0.getValue();
	// StringParameter pa = (StringParameter)(getParameter("alg"));
	// if (pa != null) {
	//     this.algorithm = pa.getValue();
	// }
	StringParameter p1 = (StringParameter)(getParameter("file"));
	if (p1 == null) {
    throw new CommandExecutionException(helpOutput()); 
	}
	fileName = p1.getValue();
  
  try{
    Command uniqueNamesParameter = getParameter("unique");
     if (uniqueNamesParameter != null) {
   unique = ((StringParameter)uniqueNamesParameter).parseBoolean();
     }
   }catch(ParsingException pe) {
         throw new CommandExecutionException("Parsing exception: " + pe.getMessage());
       }
	// StringParameter p2 = (StringParameter)(getParameter("name"));
	// if (p2 != null) {
	//     name = p2.getValue();
	// }
	// StringParameter symP = (StringParameter)(getParameter("sym"));
	// if (symP != null) {
	//     this.symConstraints = parseSymEdgeConstraints(symP.getValue());
	// }
	// StringParameter maxP = (StringParameter)(getParameter("max"));
	// if (maxP != null) {
	//     this.params.setProperty("max", maxP.getValue());
	//     log.info("changed max parameter to: " + params.getProperty("max"));
	// }
	// StringParameter minP = (StringParameter)(getParameter("min"));
	// if (minP != null) {
	//     this.params.setProperty("min", minP.getValue());
	// }
  StringParameter repuls = (StringParameter)(getParameter("repuls"));
  if (repuls != null) {
      this.params.setProperty("repuls", repuls.getValue());
  }
  StringParameter steps = (StringParameter)(getParameter("steps"));
  if (steps != null) {
      this.params.setProperty("steps", steps.getValue());
  }
  StringParameter datahome = (StringParameter)(getParameter("datahome"));
  if (datahome != null) {
      this.params.setProperty("datahome", datahome.getValue());
  }
  StringParameter formatParam = (StringParameter)(getParameter("format"));
  if (formatParam != null) { // expect "sec" [default] or "ct";
      this.format=formatParam.getValue();
  }
    }

}
