package rnadesign.designapp;

import java.io.PrintStream;
import java.util.*;
import rnadesign.rnacontrol.Object3DGraphController;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class TreeCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "tree";

    private Object3DGraphController controller;
    private PrintStream ps;
    private String name;
    private Set<String> allowedNames;
    private Set<String> forbiddenNames;
    private String firstValue;

    public TreeCommand(PrintStream ps, Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	assert ps != null;
	this.ps = ps;
	this.controller = controller;
    }

    public Object cloneDeep() {
	TreeCommand command = new TreeCommand(this.ps, this.controller);
	command.name = this.name;
	command.allowedNames = this.allowedNames;
	command.forbiddenNames = this.forbiddenNames;
	command.firstValue = this.firstValue;
	log.finest("Copying " + getParameterCount() + " parameters!");
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME 
	    + " [strand|atom|helixend|residue][allowed=[<CLASSNAME>[,<CLASSNAME>][...]][forbidden=<CLASSNAME>[,<CLASSNAME>[...]][name=<OBJECTNAME>]" + NEWLINE
	    + " with typical CLASSNAME values of Atom3D, RnaStrand3D, Nucleotide3D, BranchDescriptor3D and object names of root.A or 1.1.5";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + helpOutput() + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Tree command displays list of objects in tree." + NEWLINE + NEWLINE;
	helpText += "OPTIONS" + NEWLINE;
	helpText += "     allowed=<CLASSNAME>[,<CLASSNAME>[,...]]" + NEWLINE + "      Show only objects that belong to the specified class or classes." + NEWLINE + NEWLINE;
	helpText += "     forbidden=<CLASSNAME[,<CLASSNAME[,...]]" + NEWLINE + "          Do not show objects of this class; also do not show child nodes of objects that belong to this class. Default: forbidden=Atom3D" + NEWLINE + NEWLINE;
	helpText += "     name=OBJECT" + NEWLINE + "          Only print the sub-tree from node with this name." + NEWLINE + NEWLINE;
        helpText += "Examples:" + NEWLINE +
	    "tree strand    : only show RNA strand objects." + NEWLINE +
	    "tree atom      : only show atoms." + NEWLINE +
            "tree forbidden= : show all objects (including atoms)" + NEWLINE +
            "tree forbidden=Nucleotide3D : show all objects except nucleotides and their child nodes." + NEWLINE +
	    "tree allowed=Nucleotide3D,Atom3D    : only show nucleotide or atom objects." + NEWLINE +
	    "tree name=root.import.A             : show all objects that are part of imported strand A." + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	if (firstValue != null) {
	    if (firstValue.equals("atom")) {
		allowedNames = new HashSet<String>();
		allowedNames.add("Atom3D");
	    }
	    else if (firstValue.equals("strand")) {
		allowedNames = new HashSet<String>();
		allowedNames.add("RnaStrand");
		forbiddenNames.add("Nucleotide3D"); // shortens search
	    }
	    else if (firstValue.equals("helixend")) {
		allowedNames = new HashSet<String>();
		allowedNames.add("BranchDescriptor3D");
	    }
	    else if (firstValue.equals("hxend")) {
		allowedNames = new HashSet<String>();
		allowedNames.add("BranchDescriptor3D");
	    }
	    else if (firstValue.equals("residue")) {
		allowedNames = new HashSet<String>();
		allowedNames.add("Nucleotide3D"); // TODO : add AminoAcid3D
		// forbiddenNames.add("Atom3D");
	    }
	}
	if (name != null) {
	    // System.out.println("Using tree root: " + name);
	    controller.getGraph().printTree(ps, name, allowedNames, forbiddenNames);
	}
	else {
	    // System.out.println("Using tree root: " + name);
	    controller.getGraph().printTree(ps, allowedNames, forbiddenNames);
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	forbiddenNames = new HashSet<String>();
	if (getParameterCount() > 0) {
	    StringParameter firstParameter = (StringParameter)(getParameter(0));
	    if (firstParameter != null) {
		firstValue = firstParameter.getValue(); // tree name=...
		if (firstValue.indexOf('=') >= 0) {
		    firstValue = null;
		}
	    }
	}
	else {
	    forbiddenNames.add("Atom3D"); // if no parameters are given: do not show atoms
	}
	StringParameter nameParameter = (StringParameter)(getParameter("name"));
	if (nameParameter != null) {
	    name = nameParameter.getValue();
	}
	StringParameter allowedParameter = (StringParameter)(getParameter("allowed"));
	if (allowedParameter != null) {
	    String[] allowedNamesRaw = allowedParameter.getValue().split(",");
	    this.allowedNames = new HashSet<String>();
	    for (String s : allowedNamesRaw) {
		allowedNames.add(s);
	    }
	}
	StringParameter forbiddenParameter = (StringParameter)(getParameter("forbidden"));
	if (forbiddenParameter != null) {
	    String[] forbiddenNamesRaw = forbiddenParameter.getValue().split(",");
	    forbiddenNames = new HashSet<String>();
	    for (String s : forbiddenNamesRaw) {
		forbiddenNames.add(s);
	    }
	}
    }
}
    
