package rnadesign.designapp;

import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import tools3d.Vector3D;

import static rnadesign.designapp.PackageConstants.*;

public class TwistCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "twist";

    private double a = 0;
    private double b = 0;
    private double c = 1; // rotation axis: a,b,c
    private double x = 0;
    private double y = 0;
    private double z = 0;
    private double alpha = 0;
    private String name;

    private Object3DGraphController controller;
    
    public TwistCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	TwistCommand command = new TwistCommand(controller);
	command.a = a;
	command.b = b;
	command.c = c;
	command.x = x;
	command.y = y;
	command.z = z;
	command.alpha = alpha;
	command.name = name;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " alpha rx ry rz [px py pz]  : rotate around axis (rx,ry,rz) and center (px, py, pz) angle angle alpha";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " alpha rx ry rz [px py pz]" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "        rotate around axis (rx, ry, rz) and center (px, py, pz) angle angle alpha" + NEWLINE + NEWLINE;
	// helpText += "OPTIONS" + NEWLINE;
	// helpText += "     name=NAME" + NEWLINE + "          Set the name of the imported file." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	assert (this.controller != null);
	prepareReadout();
	Vector3D axis = new Vector3D(a,b,c);
	Vector3D center = new Vector3D(x,y,z);
	if (axis.length() == 0.0) {
	    throw new CommandExecutionException("Length of rotation axis must be greater zero!");
	}
	try {
	    if (getParameterCount() == 7) {
		this.controller.getGraph().twistSelected(axis, center, alpha);
	    }
	    else if (getParameterCount() == 4) {
		this.controller.getGraph().twistSelected(a, b, c, alpha);
	    }
	    else if (getParameterCount() == 5) {
		this.controller.getGraph().twistSelected(axis, name, alpha);
	    }
	    else {
		throw new CommandExecutionException(helpOutput());
	    }
	}
	catch (Object3DGraphControllerException ce) {
	    throw new CommandExecutionException("Error in twist command: " + ce.getMessage());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if ((getParameterCount() != 4) && (getParameterCount() != 7)) {
	    throw new CommandExecutionException(helpOutput()); 
	}
	StringParameter p0 = (StringParameter)(getParameter(0));
	StringParameter p1 = (StringParameter)(getParameter(1));
	StringParameter p2 = (StringParameter)(getParameter(2));
	StringParameter p3 = (StringParameter)(getParameter(3));
	try {
	    alpha = DEG2RAD * Double.parseDouble(p0.getValue());
	    a = Double.parseDouble(p1.getValue());
	    b = Double.parseDouble(p2.getValue());
	    c = Double.parseDouble(p3.getValue());
	    if (getParameterCount() == 5) {
		StringParameter p4 = (StringParameter)(getParameter(4));
		name = p4.getValue();
	    }
	    else if (getParameterCount() == 7) {
		StringParameter p4 = (StringParameter)(getParameter(4));
		StringParameter p5 = (StringParameter)(getParameter(5));
		StringParameter p6 = (StringParameter)(getParameter(6));
		x = Double.parseDouble(p4.getValue());
		y = Double.parseDouble(p5.getValue());
		z = Double.parseDouble(p6.getValue());
	    }
	}
	catch (NumberFormatException nfe) {
	    throw new CommandExecutionException("Bad number syntax in command rotate : " 
						+ nfe.getMessage());
	}
    }
    
}
