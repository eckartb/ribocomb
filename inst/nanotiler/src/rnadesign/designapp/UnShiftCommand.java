package rnadesign.designapp;

import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import tools3d.Vector3D;

import static rnadesign.designapp.PackageConstants.*;

public class UnShiftCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "unshift";

    private double a = 0;
    private double b = 0;
    private double c = 0;

    private Object3DGraphController controller;
    
    public UnShiftCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	UnShiftCommand command = new UnShiftCommand(controller);
	command.a = a;
	command.b = b;
	command.c = c;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"unshift\" Command Manual" + NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     unshift" + NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     unshift x y z" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE + "     Unshift command shifts selection opposite x y z." + NEWLINE + NEWLINE;
	return helpText;
    }

    private String helpOutput() {
	return "Shift selected objects in reverse direction. Correct usage: " + COMMAND_NAME + " x y z";
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	assert (this.controller != null);
	prepareReadout();
	Object3DController gControl = controller.getGraph();
	Vector3D shift = new Vector3D(-a, -b, -c); // notice minus sign!
	if (gControl.getSelectionRoot() == null) {
	    gControl.translate(shift);
	}
	else {
	    gControl.translateSelected(shift);
	}

    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() != 3) {
	    throw new CommandExecutionException(helpOutput()); 
	}
	StringParameter p0 = (StringParameter)(getParameter(0));
	StringParameter p1 = (StringParameter)(getParameter(1));
	StringParameter p2 = (StringParameter)(getParameter(2));
	try {
	    a = Double.parseDouble(p0.getValue());
	    b = Double.parseDouble(p1.getValue());
	    c = Double.parseDouble(p2.getValue());
	}
	catch (NumberFormatException nfe) {
	    throw new CommandExecutionException("Bad number syntax in command shift : " 
						+ nfe.getMessage());
	}

    }
    
}
