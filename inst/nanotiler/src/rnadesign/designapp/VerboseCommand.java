package rnadesign.designapp;

import java.io.PrintStream;
import java.util.logging.*;
import rnadesign.rnacontrol.Object3DGraphController;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;

import static rnadesign.designapp.PackageConstants.*;

public class VerboseCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "verbose";

    private static Logger log = Logger.getLogger(LOGGER_NAME);

    private String name;

    public VerboseCommand() {
	super(COMMAND_NAME);
    }

    public Object cloneDeep() {
	VerboseCommand command = new VerboseCommand();
	command.name = this.name;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    public String getName() { return COMMAND_NAME; }

    private String helpOutput() {
	return "Correct usage: " + COMMAND_NAME + " all|finest|fine|info|warning|severe|off";
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    " all|finest|fine|info|warning|severe|off" + NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Verbose command TODO" + NEWLINE + NEWLINE;
	// helpText += "OPTIONS" + NEWLINE;
	// helpText += "     name=NAME" + NEWLINE + "          Set the name of the imported file." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	prepareReadout();
	if (name != null) {
	    if (name.equals("all")) {
		log.setLevel(Level.ALL);
	    }
	    else if (name.equals("fine")) {
		log.setLevel(Level.FINE);
	    }
	    else if (name.equals("finest")) {
		log.setLevel(Level.FINEST);
	    }
	    else if (name.equals("info")) {
		log.setLevel(Level.INFO);
	    }
	    else if (name.equals("warning")) {
		log.setLevel(Level.WARNING);
	    }
	    else if (name.equals("severe")) {
		log.setLevel(Level.SEVERE);
	    }
	    else if (name.equals("off")) {
		log.setLevel(Level.OFF);
	    }
	    else {
		throw new CommandExecutionException("Unknown command directive " + name + " " + helpOutput());
	    }
	}
	else {
	    throw new CommandExecutionException("Bad command syntax! " + helpOutput());
	}
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
	if (getParameterCount() == 1) {
	    StringParameter p0 = (StringParameter)(getParameter(0));
	    name = p0.getValue();
	}
	else if (getParameterCount() != 0) {
	    throw new CommandExecutionException(helpOutput()); 
	}
    }

}
