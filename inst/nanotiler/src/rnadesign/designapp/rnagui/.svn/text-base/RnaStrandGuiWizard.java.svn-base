package rnadesign.designapp.rnagui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Container;
import java.awt.Component;
import java.awt.event.*;
import javax.swing.*;
import rnadesign.rnacontrol.*;
import sequence.*;
import tools3d.objects3d.*;
import tools3d.*;
import rnadesign.rnamodel.Rna3DTools;
import rnadesign.rnamodel.RnaStrand;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import rnadesign.rnacontrol.Object3DGraphController;
import sequence.UnknownSymbolException;
import tools3d.Vector3D;

/** lets user choose to partner object, inserts correspnding link into controller */
public class RnaStrandGuiWizard implements GeneralWizard {

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    private boolean finished = false;
    private JFrame frame = null;
    private JComboBox typeComboBox;
    private JTextField sequenceField;
    private JTextField nameField;
    private JTextField xField;
    private JTextField yField;
    private JTextField zField;
    private JTextField xDirField;
    private JTextField yDirField;
    private JTextField zDirField;

    private List<ActionListener> actionListeners = new ArrayList<ActionListener>();
    private Object3DGraphController graphController;
    public static final String FRAME_TITLE = "RNA Strand Wizard";

    private class DoneListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    Vector3D direction = generateDirection();
	    if (direction.length()==0) {
		JOptionPane.showMessageDialog(frame, "Direction vector has to be non-zero!");
		return;
	    }
	    try {
		int typeNumber = typeComboBox.getSelectedIndex();
		int typeId = Object3DGraphControllerConstants.RNA;
		switch (typeNumber) {
		case 0: typeId = Object3DGraphControllerConstants.RNA;
		    break;
		case 1: typeId = Object3DGraphControllerConstants.DNA;
		    break;
		case 2: typeId = Object3DGraphControllerConstants.PROTEIN;
		    break;
		}
		graphController.addStrand(nameField.getText().trim(),
					  sequenceField.getText().trim(),
					  generatePosition(),
					  direction, typeId);
		frame.setVisible(false);
		frame = null;
		finished = true;
	    }
	    catch (UnknownSymbolException exception) {
		JOptionPane.showMessageDialog(frame, "Unknown sequence character found! Use only ACGU");
	    }
	}
    }

    private class CancelListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    frame.setVisible(false);
	    frame = null;
	    finished = true;
	}
    }

    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    public boolean isFinished() {
	return finished;
    }

    /** generates and inserts new Object3D according to user dialog */
    public void launchWizard(Object3DGraphController graphController,
			     Component parentFrame) {
	if (graphController == null) {
	    log.info("GraphController received by launchWizard is null!");
	}
	this.graphController = graphController;
	frame = new JFrame(FRAME_TITLE);
	addComponents(frame);
	frame.pack();
	frame.setVisible(true);
    }

    private String getInitName() {
	char c = (char)((int)('A') + this.graphController.getSequences().getSequenceCount());
	char[] chars = new char[1];
	chars[0] = c;
	return new String(chars);
    }

    public void addComponents(JFrame _frame) {
	Container f = _frame.getContentPane();
	f.setLayout(new BorderLayout());
	JPanel top = new JPanel();
	JPanel center = new JPanel();
	nameField = new JTextField(getInitName());
	sequenceField = new JTextField("NNNNNNNNNN", 50);
	xField = new JTextField("0.0", 6);
	yField = new JTextField("0.0", 6);
	zField = new JTextField("0.0", 6);
	xDirField = new JTextField("0.0", 6);
	yDirField = new JTextField("0.0", 6);
	zDirField = new JTextField("1.0", 6);
	JPanel xyzPanel = new JPanel();
	xyzPanel.setLayout(new FlowLayout());
	xyzPanel.add(new JLabel("Position:"));
	xyzPanel.add(xField);
	xyzPanel.add(yField);
	xyzPanel.add(zField);
	JPanel xyzDirPanel = new JPanel();
	xyzDirPanel.setLayout(new FlowLayout());
	xyzDirPanel.add(new JLabel("Direction:"));
	xyzDirPanel.add(xDirField);
	xyzDirPanel.add(yDirField);
	xyzDirPanel.add(zDirField);
	top.add(new JLabel("Name:"));
	top.add(nameField);
	top.add(xyzPanel);
	top.add(xyzDirPanel);
	center.add(new JLabel("Sequence:"));
	center.add(sequenceField);
	f.add(top, BorderLayout.NORTH);
	f.add(center, BorderLayout.CENTER);

	String[] typeStrings = { "RNA", "DNA", "Protein" };
	typeComboBox = new JComboBox(typeStrings);
	JPanel bottomPanel = new JPanel();
	JButton button = new JButton("Cancel");
	button.addActionListener(new CancelListener());
	bottomPanel.add(typeComboBox);
	bottomPanel.add(button);
  	button = new JButton("Done");
	button.addActionListener(new DoneListener());
	bottomPanel.add(button);
	f.add(bottomPanel, BorderLayout.SOUTH);
    }

    Vector3D generatePosition() {
	Vector3D result = null;
	double x = Double.parseDouble(xField.getText().trim());
	double y = Double.parseDouble(yField.getText().trim());
	double z = Double.parseDouble(zField.getText().trim());
	result = new Vector3D(x,y,z);
	return result;
    }

    Vector3D generateDirection() {
	Vector3D result = null;
	double x = Double.parseDouble(xDirField.getText().trim());
	double y = Double.parseDouble(yDirField.getText().trim());
	double z = Double.parseDouble(zDirField.getText().trim());
	result = new Vector3D(x,y,z);
	if (result.length() > 0.0) {
	    result.normalize();
	}
	return result;
    }
    
}
