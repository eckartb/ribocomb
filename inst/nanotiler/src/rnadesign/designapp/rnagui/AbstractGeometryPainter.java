package rnadesign.designapp.rnagui;

import java.awt.Color;
import java.awt.Graphics;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;

import tools3d.Appearance;
import tools3d.Character3D;
import tools3d.Drawable;
import tools3d.Edge3D;
import tools3d.Face3D;
import tools3d.Geometry;
import tools3d.GeometryColorModel;
import tools3d.Point3D;
import tools3d.Positionable3D;
import tools3d.Vector3D;
import tools3d.ZBuffer;

/** base class for all painters */
public abstract class AbstractGeometryPainter extends AbstractPainter implements GeometryPainter {

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    ZBuffer zBuf = new ZBuffer();
    Set<String> forbiddenClassNames = new HashSet<String>();
    
    private boolean paintLinks = true;
    private boolean depthCueing = true;

    public boolean getDepthCueing() {
	return depthCueing;
    }

    public void setDepthCueing(boolean depthCueing) {
	this.depthCueing = depthCueing;
    }

    public boolean getPaintLinks() {
	return paintLinks;
    }

    public void setPaintLinks(boolean b) {
	paintLinks = b;
    }

    protected GeometryColorModel geometryColorModel 
	// = new SimpleGeometryColorModel();
	= new RnaStrandColorModel();
    // = new RainbowColorModel();

    protected boolean paintCharacterMode = false;

    /** used for hiding classes */
    public void addForbidden(String className) {
	forbiddenClassNames.add(className);
    }

    /** removes all forbidden class names */
    public void clearForbidden() { }
    
    /* removes a forbidden class name */
    public void removeForbidden(String className) {
 	forbiddenClassNames.remove(className);
    }
    
    public GeometryColorModel getGeometryColorModel() {
	return geometryColorModel;
    }

    /** tries to find depth dependend default paint color */
    public Color getDefaultPaintColor(Drawable drawable) {
	Properties properties = drawable.getProperties();
	Appearance appearance = drawable.getAppearance();
	Vector3D normal = null;
	if (drawable instanceof Face3D) {
	    normal = ((Face3D)drawable).getNormal();
	}

	return geometryColorModel.computeColor(getAmbiente(),
					       appearance,
					       properties,
					       normal,
					       getCameraController().getCamera());
    }

    public boolean getPaintCharacterMode() { 
 	log.info("GOT HERE, LINE 43, ABSTRACTGEOMETRYPAINTER!");
 	return this.paintCharacterMode; 
    } /** method getPaintCharacterMode is nto called **/

    public boolean isPaintable(Drawable p) {
	Properties prop = p.getProperties();
	if (prop != null) {
	    String className = prop.getProperty("class_name");
	    if (forbiddenClassNames.contains(className)) {
		return false;
	    }
	    else {
		return true;
	    }
	}
	return true;
    }

    public void paint(Graphics g, Geometry geometry) {
	// log.fine("Calling paint of AbstractGeometryPainter!");
	zBuf.reset();
	for (int i = 0; i < geometry.getNumberPoints(); ++i) {
	    zBuf.add(geometry.getPoint(i));
	}
	for (int i = 0; i < geometry.getNumberEdges(); ++i) {
	    zBuf.add(geometry.getEdge(i));
	}
	for (int i = 0; i < geometry.getNumberFaces(); ++i) {
	    zBuf.add(geometry.getFace(i));
	}
	zBuf.sort();
	for (int i = zBuf.size()-1; i >= 0; --i) {
	    Positionable3D p = zBuf.get(i);
	    if ((p instanceof Character3D) && paintCharacterMode) {
		paintCharacter(g, (Character3D)p);
	    }
	    else if (p instanceof Point3D) {
		paintPoint(g, (Point3D)p);
	    }
	    else if (p instanceof Edge3D) {
		// log.fine("read paintEdge in AbstractGeometryPainter, LINE 64!");
		paintEdge(g, (Edge3D)p);
	    }
	    else if (p instanceof Face3D) {
		paintFace(g, (Face3D)p);
	    }
	}
    }      /** method paint not called **/

    public void paint(Graphics g, Drawable p) {
	if (p instanceof Character3D) {
	    paintCharacter(g, (Character3D)p);
	}
	else if (p instanceof Point3D) {
	    paintPoint(g, (Point3D)p);
	}
	else if (p instanceof Edge3D) {
	    paintEdge(g, (Edge3D)p);
	}
	else if (p instanceof Face3D) {
	    paintFace(g, (Face3D)p);
	}
    }

    public void setGeometryColorModel(GeometryColorModel m) {
	this.geometryColorModel = m;
    }

    public void setPaintCharacterMode(boolean b) { this.paintCharacterMode = b; }

}
