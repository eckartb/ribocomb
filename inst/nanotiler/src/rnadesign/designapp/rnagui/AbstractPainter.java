package rnadesign.designapp.rnagui;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Properties;

import generaltools.Letter;
import rnadesign.rnacontrol.CameraController;
import rnadesign.rnacontrol.SimpleCameraController;
import tools3d.Ambiente;
import tools3d.Drawable;

/** paints a single Shape3D 
 * this can be a base class to paint cubes, spheres, cylinders etc
 * @see Strategy pattern in Gamma et al: Design patterns
 */
public abstract class AbstractPainter implements Painter {

    Ambiente ambiente = null; // no implementation exists yet!
    
    CameraController cameraController = new SimpleCameraController();

    PaintStyle paintStyle = new PaintStyle();
    
    Color defaultPaintColor = Color.green;
    
    // depth dependent default colors:
    Color[] defaultPaintColors = { Color.red, Color.yellow, Color.green, Color.blue};

    Color defaultCharacterColor = Color.black;

    /** returns ambiente (descriptor for light sources, diffuse light etc) */
    public Ambiente getAmbiente() { return ambiente; }

    /** returns camera controller */
    public CameraController getCameraController() {
	return cameraController;
    } 

    /** returns default color */
    public Color getDefaultPaintColor() {
	return this.defaultPaintColor;
    }

    public Color getDefaultCharacterColor() {
	return defaultCharacterColor;
    }

    /** returns depth dependend default color */
    public Color getDefaultPaintColor(int depth) {
	if ((depth >= 0) && (depth < defaultPaintColors.length)) {
	    return defaultPaintColors[depth];
	}
	return getDefaultPaintColor();
    }

    /** tries to find depth dependend default paint color */
    public Color getDefaultPaintColor(Drawable drawable) {
	Properties properties = drawable.getProperties();
	Color col = null;
	if (drawable instanceof Letter) {
	    col = getDefaultCharacterColor();
	}
	else if (properties != null) {
	    String depthString = properties.getProperty("depth");
	    if (depthString != null) {
		int depth = Integer.parseInt(depthString);
		col = getDefaultPaintColor(depth);
	    }
	}
	if (col == null) {
	    col = getDefaultPaintColor();
	}
	// modify color according to selected status:
	if (drawable.isSelected()) {
	    col = col.brighter();
	}
	else {
	    col = col.darker();
	}
	return col; 
    }


    public PaintStyle getPaintStyle() { return paintStyle; }

    /** sets ambiente (descriptor of light sources, background etc) */
    public void setAmbiente(Ambiente ambiente) { this.ambiente = ambiente; }

    /** sets camera controller */
    public void setCameraController(CameraController camera) {
	this.cameraController = camera;
    }

    public void setDefaultPaintColor(Color color) {
	this.defaultPaintColor = color;
    }

    /** info about color, solid and wire frame mode */
    public void setPaintStyle(PaintStyle style) { this.paintStyle = style; }

}
