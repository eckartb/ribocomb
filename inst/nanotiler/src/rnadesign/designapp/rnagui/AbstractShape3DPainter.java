package rnadesign.designapp.rnagui;

import java.awt.Color;
import java.awt.Graphics;

import rnadesign.rnacontrol.CameraController;
import rnadesign.rnacontrol.SimpleCameraController;
import tools3d.Ambiente;

/** paints a single Shape3D 
 * this can be a base class to paint cubes, spheres, cylinders etc
 * @see Strategy pattern in Gamma et al: Design patterns
 */
public abstract class AbstractShape3DPainter implements Shape3DPainter {

    Ambiente ambiente = null; // no implementation exists yet!
    
    CameraController cameraController = new SimpleCameraController();

    PaintStyle paintStyle = new PaintStyle();

    Color defaultPaintColor = Color.GRAY;

    /** returns ambiente (descriptor for light sources, diffuse light etc) */
    public Ambiente getAmbiente() { return ambiente; }

    /** returns camera controller */
    public CameraController getCameraController() {
	return cameraController;
    } 

    public Color getDefaultPaintColor() {
	return this.defaultPaintColor;
    }

    public PaintStyle getPaintStyle() { return paintStyle; }

    /** sets ambiente (descriptor of light sources, background etc) */
    public void setAmbiente(Ambiente ambiente) { this.ambiente = ambiente; }

    /** sets camera controller */
    public void setCameraController(CameraController camera) {
	this.cameraController = camera;
    }

    public void setDefaultPaintColor(Color color) {
	this.defaultPaintColor = color;
    }

    /** info about color, solid and wire frame mode */
    public void setPaintStyle(PaintStyle style) { this.paintStyle = style; }

}
