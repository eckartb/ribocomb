package rnadesign.designapp.rnagui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.event.*;
import javax.swing.*;
import javax.swing.text.*;
import javax.swing.table.*;
import java.text.ParseException;
import java.io.*;
import java.util.*;
import commandtools.*;
import controltools.*;
import rnadesign.rnamodel.*;

import tools3d.objects3d.*;

import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerConstants;

/** lets user choose to partner object, inserts correspnding link into controller */
public class AddHelixWizard implements GeneralWizard {

    public static final String FRAME_TITLE = "Add Helix";

    private static final Dimension TABLE_DIM = new Dimension(200, 200);

    private CommandApplication application;

    private ArrayList<BranchDescriptor3D> junctions;

    private JList branchSelector1;
    private JList branchSelector2;

    private JDialog frame;

    private boolean finished;

    private JButton add;

    private Object3DGraphController graphController;

    private int selectedIndex1 = -1;
    private int selectedIndex2 = -1;

    private JTextField bpmin;
    private JTextField bpmax;


    private ArrayList<ActionListener> actionListeners = new ArrayList<ActionListener>();

    public AddHelixWizard(CommandApplication application) {
	this.application = application;
    }

    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    public boolean isFinished() {
	return finished;
    }

    /** generates and inserts new Object3D according to user dialog */
    public void launchWizard(Object3DGraphController graphController,
			     Component parentFrame) {

	this.graphController = graphController;

	
	frame = new JDialog(parentFrame instanceof Frame ? (Frame) parentFrame : null, FRAME_TITLE, true);
	frame.addWindowListener(new WindowAdapter() {
		
		public void windowClosed(WindowEvent e) {
		    finished = true;
	       
		}

	    });

	junctions = new ArrayList<BranchDescriptor3D>();
	Object3DActionVisitor visitor = new Object3DActionVisitor(graphController.getGraph().getGraph(), new Object3DAction() {
		public void act(Object3D obj) {
		    if(obj instanceof BranchDescriptor3D)
			junctions.add((BranchDescriptor3D)obj);

		}

	    });


	visitor.nextToEnd();

	if(junctions.size() <= 1) {
	    JOptionPane.showMessageDialog(null, "No branch descriptors available to add helices to.");
	    return;

	}

	JPanel panel = new JPanel();
	panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));


	JLabel label = new JLabel("<html><h2>Select first branch");
	label.setAlignmentX(JComponent.LEFT_ALIGNMENT);
	panel.add(label);

	branchSelector1 = new JList(new JunctionListModel());
	branchSelector1.addListSelectionListener(new AddListener());
	JScrollPane pane = new JScrollPane(branchSelector1);
	pane.setPreferredSize(TABLE_DIM);
	panel.add(pane);

	label = new JLabel("<html><h2>Select second branch");
	label.setAlignmentX(JComponent.LEFT_ALIGNMENT);
	panel.add(label);

	branchSelector2 = new JList(new JunctionListModel());
	branchSelector2.addListSelectionListener(new AddListener());
	pane = new JScrollPane(branchSelector2);
	pane.setPreferredSize(TABLE_DIM);
	panel.add(pane);

	panel.add(Box.createVerticalStrut(15));

	JPanel bottomPanel = new JPanel();
	
	JButton close = new JButton("Close");
	close.addActionListener(new CloseWindowListener());
	bottomPanel.add(close);

	add = new JButton("Add");
	add.addActionListener(new AddListener());
	add.setEnabled(false);
	bottomPanel.add(add);

	panel.add(bottomPanel);

	frame.add(panel);

	frame.pack();
	frame.setVisible(true);

    }

    private class CloseWindowListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    frame.setVisible(false);
	    frame.dispose();

	}

    }

    private class AddListener implements ActionListener, ListSelectionListener {
	public void valueChanged(ListSelectionEvent e) {
	    if(branchSelector1.getSelectedIndex() != -1 && branchSelector2.getSelectedIndex() != -1)
		add.setEnabled(true);
	    else
		add.setEnabled(false);


	}

	public void actionPerformed(ActionEvent e) {
	    if(branchSelector1.getSelectedIndex() == branchSelector2.getSelectedIndex()) {
		JOptionPane.showMessageDialog(frame, "Pleas select two different junctions");
		return;

	    }

	    BranchDescriptor3D branch1 = junctions.get(branchSelector1.getSelectedIndex());
	    BranchDescriptor3D branch2 = junctions.get(branchSelector2.getSelectedIndex());
	    String branch1Name = Object3DTools.getFullName(branch1);
	    String branch2Name = Object3DTools.getFullName(branch2);



	    try {
		application.runScriptLine("genhelix " + branch1Name + " " + branch2Name);

	    } catch(CommandException ex) {
		JOptionPane.showMessageDialog(frame, ex.getMessage());
		return;

	    }

	    frame.setVisible(false);
	    frame.dispose();

	}



    }

    private class JunctionListModel extends AbstractListModel {

	public int getSize() {
	    return junctions.size();

	}


	public Object getElementAt(int index) {
	    return Object3DTools.getFullName(junctions.get(index));

	}


    }

     
}
