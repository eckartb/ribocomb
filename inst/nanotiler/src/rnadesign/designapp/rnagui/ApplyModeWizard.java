package rnadesign.designapp.rnagui;

import java.awt.Font;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import commandtools.CommandApplication;
import commandtools.CommandException;
import commandtools.BadSyntaxException;
import commandtools.Command;
import rnadesign.designapp.NanoTilerInterpreter;
import rnadesign.designapp.AbstractDesigner;
import rnadesign.designapp.DistanceCommand;
import rnadesign.rnacontrol.Object3DController;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnamodel.RnaTools;
import tools3d.objects3d.Object3D;
import java.util.logging.Logger;

/**
 * GUI Implementation of elastic command.
 *
 * @author Brett Boyle
 */
public class ApplyModeWizard implements GeneralWizard {

    private static final int COL_SIZE_NAME = 25; //TODO: size?
    private static final String FRAME_TITLE = "Apply Mode Wizard";

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    private boolean finished = false;
    private JFrame frame = null;
    private List<ActionListener> actionListeners = new ArrayList<ActionListener>();
    private CommandApplication application;
    private Object3DGraphController graphController;
    private NanoTilerInterpreter interpreter;
    private JTextField fileField;
    private JTextField modeField;
    private JTextField scaleField;
    private JTextField stepsField;
    private JLabel label;
    private Vector<String> tree;
    private String[] allowedNames = RnaTools.getRnaClassNames();
    private String[] forbiddenNames;
    private JPanel treePanel;
    private JButton treeButton;
    private JList treeList;
    private JScrollPane treeScroll;

    /**
     * Constructor.
     *
     * @param application NanoTiler
     */
    public ApplyModeWizard(CommandApplication application) {
	assert application != null;
	this.application = application;
	this.interpreter = (NanoTilerInterpreter)application.getInterpreter();
    }

    /**
     * Called when "Cancel" button is pressed.
     * Window disappears.
     *
     */
    private class CancelListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    frame.setVisible(false);
	    frame = null;
	    finished = true;
	}
    }
    private class ApplyModeListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    String command = "applymode"+" mode=" + modeField.getText().trim() + " scale=" + scaleField.getText().trim() + " steps=" + stepsField.getText().trim();
	    if(!fileField.getText().trim().equals("")){
		command += " file="+fileField.getText().trim();
	    }

	    try {
		application.runScriptLine(command);
	    }
	    catch(CommandException ce) {
		JOptionPane.showMessageDialog(frame, ce + ": " + ce.getMessage());
	    }
	}
    }

    /**
     * Adds an action listener.
     *
     * @param listener The ActionListener to add.
     */
    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    /** Adds the components to the window. */
    public void addComponents() {
	Container f = frame.getContentPane();
	f.setLayout(new BorderLayout());

	JPanel centerPanel = new JPanel();
	centerPanel.setLayout(new BoxLayout(centerPanel,BoxLayout.Y_AXIS));
	JPanel temp = new JPanel();
	temp.add(new JLabel("File:"));
	fileField = new JTextField(COL_SIZE_NAME);
	temp.add(fileField);
	centerPanel.add(temp);
	temp = new JPanel();
	temp.add(new JLabel("Mode:"));
	modeField = new JTextField(COL_SIZE_NAME);
	modeField.setText("1");
	temp.add(modeField);
	centerPanel.add(temp);
	temp = new JPanel();
	temp.add(new JLabel("Scale:"));
	scaleField = new JTextField(COL_SIZE_NAME);
	scaleField.setText("5");
	temp.add(scaleField);
	centerPanel.add(temp);
	temp = new JPanel();
	temp.add(new JLabel("Steps:"));
	stepsField = new JTextField(COL_SIZE_NAME);
	stepsField.setText("5");
	temp.add(stepsField);
	centerPanel.add(temp);


	JPanel buttonPanel = new JPanel(new FlowLayout());
	JButton button = new JButton("Close");
	button.addActionListener(new CancelListener());
	buttonPanel.add(button);
	button = new JButton("Done");
	button.addActionListener(new ApplyModeListener());
	buttonPanel.add(button);

	f.add(centerPanel, BorderLayout.CENTER);
	f.add(buttonPanel, BorderLayout.SOUTH);
    }

    /** Returns a String representation of the current selection. */
    /*private String getSelectionText() {
	Object3DGraphController graphController = ((AbstractDesigner)application).getGraphController();
	Object3DController gControl = graphController.getGraph();
	Object3D selection = gControl.getSelectionRoot();
	if (selection == null) {
	    return "";
	}
	String selectionName = selection.getName();
	Object3D curr = selection;
	while (curr.getParent() != null) {
	    curr = curr.getParent();
	    selectionName = curr.getName() + "." + selectionName;
	}
	return selectionName;
	}*/

    public boolean isFinished() {
	return finished;
    }

    /** Creates a window. */
    public void launchWizard(Object3DGraphController controller,Component parentFrame) {
	frame = new JFrame(FRAME_TITLE);
	if(controller==null){
	    log.info("GraphController received by lauchWizard is null!");
	}
	this.graphController = controller;
	addComponents();
	frame.pack();
	frame.setResizable(false);
	frame.setVisible(true);
    }

}
