package rnadesign.designapp.rnagui;

import java.awt.Color;
import java.util.Properties;

import tools3d.Ambiente;
import tools3d.Appearance;
import tools3d.Camera;
import tools3d.GeometryColorModel;
import tools3d.Vector3D;

public class AtomColorModel implements GeometryColorModel {
  

    public static final Color C_COLOR = Color.gray;
    public static final Color H_COLOR = Color.orange;
    public static final Color S_COLOR = Color.yellow;
    public static final Color N_COLOR = Color.blue;
    public static final Color O_COLOR = Color.red;
    public static final Color P_COLOR = Color.magenta;
    public static final Color DEFAULT_COLOR = Color.pink;

    public Color computeColor(Ambiente ambiente,
			      Appearance appearance,
			      Properties properties,
			      Vector3D faceNormal,
			      Camera camera) {

	if(properties == null)
	    return DEFAULT_COLOR;

	String element = properties.getProperty("element");
	if(element == null)
	    return DEFAULT_COLOR;

	if(element.equals("C"))
	    return C_COLOR;
	if(element.equals("H"))
	    return H_COLOR;
	if(element.equals("S"))
	    return S_COLOR;
	if(element.equals("N"))
	    return N_COLOR;
	if(element.equals("O"))
	    return O_COLOR;
	if(element.equals("P"))
	    return P_COLOR;

	return DEFAULT_COLOR;
    }
    
      
}
