package rnadesign.designapp.rnagui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class BackgroundColorChooser extends JPanel implements ChangeListener {
    private JColorChooser jcc;
    private JLabel newLabel;
    
    public BackgroundColorChooser() {
	super (new BorderLayout());
	
	newLabel = new JLabel("Background Color Chooser: ");
	newLabel.setBackground(Color.blue);
	newLabel.setOpaque(true);
	newLabel.setPreferredSize(new Dimension(600, 300));
     	
	JPanel banner = new JPanel(new BorderLayout());
   	banner.add(newLabel, BorderLayout.CENTER);
  	banner.setBorder(BorderFactory.createTitledBorder("Banner"));
	
	Color newColor = JColorChooser.showDialog(BackgroundColorChooser.this, "Choose Background Color", newLabel.getBackground());
	if (newColor != null) {
	    newLabel.setBackground(newColor);
	}
	
    	add (banner, BorderLayout.CENTER);
//  	this.graphController = graphController;
//  	this.graphController.addModelChangeListener(new LocalStateChangeListener());
	
    }
    
    public void stateChanged(ChangeEvent e) {
	Color newColor = jcc.getColor();
	newLabel.setForeground(newColor);
    }
    
}
