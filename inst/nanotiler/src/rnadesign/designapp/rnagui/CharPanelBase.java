package rnadesign.designapp.rnagui;

import java.awt.Graphics;
import javax.swing.JPanel;

public class CharPanelBase extends JPanel {

    int colWidth = 10;
    int offsetX = 10;
    int offsetY = 20;
    int rowWidth = 14;

    /** returns x position for character in window.
     * TODO : use parameter object instead of hard-coded constant
     * @param row : column of character
     * @return  : x position of character to be drawn
     */
    public int getCharPosX(int col) {
	return offsetX + col * colWidth;
    }
		
		/** returns y position for character in window.
		 * TODO : use parameter object instead of hard-coded constant
		 * @param row : row of character
		 * @return  : y position of character to be drawn
		 */
    public int getCharPosY(int row) {
	return offsetY + row * rowWidth; 
    }
		
    /** writes single character from character array for certain position */
    public void writeChar(Graphics g, char[] chars, int row, int col) {
	int xpos = getCharPosX(col);
	int ypos = getCharPosY(row);
	// char[] chars = new char[1];
	// chars[0] = c;
	g.drawChars(chars, col, 1, xpos, ypos);
    }
		
    /** writes sequence in n'th row of window */
    public void writeString(Graphics g, String s, int n) {
	char[] charArray = s.toCharArray();
	for (int i = 0; i < s.length(); ++i) {
	    writeChar(g, charArray, n + 1, i);
	}
    }

}
