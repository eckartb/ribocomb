package rnadesign.designapp.rnagui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import commandtools.CommandApplication;
import commandtools.CommandException;
import rnadesign.designapp.AbstractDesigner;
import rnadesign.rnacontrol.Object3DController;
import rnadesign.rnacontrol.Object3DGraphController;
import tools3d.objects3d.Object3D;

/**
 * GUI Implementation of chosen command.
 *
 * @author Christine Viets
 */
public class ChosenWizard implements Wizard {

    private static final int COL_SIZE_NAME = 20; //TODO: size?
    private static final String FRAME_TITLE = "Chosen Wizard";

    private boolean finished = false;
    private JFrame frame = null;
    private List<ActionListener> actionListeners = new ArrayList<ActionListener>();
    private CommandApplication application;

    /**
     * Constructor.
     *
     * @param application NanoTiler
     */
    public ChosenWizard(CommandApplication application) {
	assert application != null;
	this.application = application;
    }

    /**
     * Called when "Done" button is pressed.
     * Window disappears.
     *
     * @author Christine Viets
     */
    private class DoneListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    frame.setVisible(false);
	    frame = null;
	    finished = true;
	}
    }

    /**
     * Adds an action listener.
     *
     * @param listener The ActionListener to add.
     */
    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    /** Adds the components to the window. */
    public void addComponents() {
	Container f = frame.getContentPane();
	f.setLayout(new BorderLayout());
	JPanel panel = new JPanel();
	panel.add(new JLabel(getSelectionText()));
	f.add(panel, BorderLayout.NORTH);
	JPanel buttonPanel = new JPanel();
	JButton button = new JButton("Done");
	button.addActionListener(new DoneListener());
	buttonPanel.add(button);
	f.add(buttonPanel, BorderLayout.SOUTH);
    }

    /** Returns a String representation of the current selection. */
    private String getSelectionText() {
	try {
	    application.runScriptLine("chosen");
	}
	catch(CommandException e) {
	    JOptionPane.showMessageDialog(frame, e + ": " + e.getMessage());
	}
	Object3DGraphController graphController = ((AbstractDesigner)application).getGraphController();
	Object3DController gControl = graphController.getGraph();
	Object3D selection = gControl.getSelectionRoot();
	if (selection == null) {
	    return "undefined";
	}
	String selectionName = selection.getName();
	Object3D curr = selection;
	while (curr.getParent() != null) {
	    curr = curr.getParent();
	    selectionName = curr.getName() + "." + selectionName;
	}
	return selectionName;
    }

    public boolean isFinished() {
	return finished;
    }

    /** Creates a window. */
    public void launchWizard(Component parentFrame) {
	frame = new JFrame(FRAME_TITLE);
	addComponents();
	frame.pack();
	frame.setVisible(true);
    }

}
