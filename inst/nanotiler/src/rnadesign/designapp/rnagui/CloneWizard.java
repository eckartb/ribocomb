package rnadesign.designapp.rnagui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;
import java.util.*;
import javax.swing.SwingConstants;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.JCheckBox;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.JSeparator;

import commandtools.CommandApplication;
import commandtools.CommandException;
import rnadesign.designapp.AbstractDesigner;
import rnadesign.rnacontrol.Object3DController;
import rnadesign.rnacontrol.Object3DGraphController;
import tools3d.objects3d.Object3D;

public class CloneWizard implements Wizard {

    private static final int COL_SIZE_NAME = 20; //TODO: size?
    private static final String FRAME_TITLE = "Clone Wizard";

    private boolean finished = false;
    private JFrame frame = null;
    private List<ActionListener> actionListeners = new ArrayList<ActionListener>();
    private CommandApplication application;
    private Vector<String> tree;
    private Set<String> allowedNames;
    private Set<String> forbiddenNames;
    private JPanel treePanel;
    private JButton treeButton;
    private JList treeList;
    private JScrollPane treeScroll;
    private JTextField selectionField1;
    private JTextField selectionField2;
    private JTextField selectionField3;
    private JLabel label;

    /**
     * Constructor.
     *
     * @param application NanoTiler
     */
    public CloneWizard(CommandApplication application) {
	assert application != null;
	this.application = application;
	allowedNames = new HashSet<String>();
	String[] names = {"Object3D","RnaStrand","Atom3D","Nucleotide3D"};
	for (String s : names) {
	    allowedNames.add(s);
	}
	forbiddenNames = new HashSet<String>();
    }

    /**
     * Called when "Cancel" button is pressed.
     * Window disappears.
     *
     */
    private class CancelListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    frame.setVisible(false);
	    frame = null;
	    finished = true;
	}
    }

    /**
     * Called when "Clone" button is pressed.
     * clones the specified object into a new parent object and name
     *
     */
    private class CloneListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    String command = "clone " + selectionField1.getText().trim() +
		" " + selectionField2.getText().trim() + " " + selectionField3.getText().trim();
	    try {
		application.runScriptLine(command);
		frame.setVisible(false);
		frame = null;
		finished = true;
	    }
	    catch(CommandException ce) {
		JOptionPane.showMessageDialog(frame, ce + ": " + ce.getMessage());
	    }
	}
    }

    /**
     * Adds an action listener.
     *
     * @param listener The ActionListener to add.
     */
    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    /** Adds the components to the window. */
    public void addComponents() {
	Container f = frame.getContentPane();
	f.setLayout(new BorderLayout());

	//	JSeparator separator = new JSeparator(SwingConstants.VERTICAL);
	//	separator.setPreferredSize(new Dimension(20,2));

	JPanel centerPanel = new JPanel();
	centerPanel.setLayout(new BorderLayout());
	   JPanel westCenterPanel = new JPanel();
	   westCenterPanel.setLayout(new BorderLayout());
	   label = new JLabel("Current Object Name: ");
	   westCenterPanel.add(label,BorderLayout.NORTH);
	   selectionField1 = new JTextField(getSelectionText(), COL_SIZE_NAME);
	   westCenterPanel.add(selectionField1,BorderLayout.CENTER);
	   westCenterPanel.add(new TreePanel(selectionField1),BorderLayout.SOUTH);
	centerPanel.add(westCenterPanel,BorderLayout.WEST);
	   JPanel centerCenterPanel = new JPanel();
	   centerCenterPanel.setLayout(new BorderLayout());
	   label = new JLabel("New Parent Name: ");
	   centerCenterPanel.add(label,BorderLayout.NORTH);
	   selectionField2 = new JTextField(getSelectionText(), COL_SIZE_NAME);
	   centerCenterPanel.add(selectionField2,BorderLayout.CENTER);
	   centerCenterPanel.add(new TreePanel(selectionField2),BorderLayout.SOUTH);
	centerPanel.add(centerCenterPanel,BorderLayout.CENTER);
	   JPanel eastCenterPanel = new JPanel();
	   eastCenterPanel.setLayout(new BorderLayout());
	   label = new JLabel("New Object Name: ");
	   eastCenterPanel.add(label,BorderLayout.NORTH);
	   selectionField3 = new JTextField(getSelectionText(), COL_SIZE_NAME);
	   eastCenterPanel.add(selectionField3,BorderLayout.CENTER);
	   eastCenterPanel.add(new TreePanel(selectionField3),BorderLayout.SOUTH);
	centerPanel.add(eastCenterPanel,BorderLayout.EAST);


	JPanel buttonPanel = new JPanel(new FlowLayout());
	JButton button = new JButton("Cancel");
	button.addActionListener(new CancelListener());
	buttonPanel.add(button);
	button = new JButton("Clone");
	button.addActionListener(new CloneListener());
	buttonPanel.add(button);

	f.add(centerPanel, BorderLayout.NORTH);
	f.add(buttonPanel, BorderLayout.SOUTH);
    }

    public boolean isFinished() {
	return finished;
    }

    /** Creates a window. */
    public void launchWizard(Component parentFrame) {
	frame = new JFrame(FRAME_TITLE);
	addComponents();
	frame.pack();
	frame.setVisible(true);
    }
    private class DoneListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    frame.setVisible(false);
	    frame = null;
	    finished = true;
	}
    }
    private String getSelectionText() {
	Object3DGraphController graphController = ((AbstractDesigner)application).getGraphController();
	Object3DController gControl = graphController.getGraph();
	Object3D selection = gControl.getSelectionRoot();
	if (selection == null) {
	    return "";
	}
	String selectionName = selection.getName();
	Object3D curr = selection;
	while (curr.getParent() != null) {
	    curr = curr.getParent();
	    selectionName = curr.getName() + "." + selectionName;
	}
	return selectionName;
    }
    /**
     * Called when a new value is selected in the list.
     * Allows user to pick an object from a list that
     * he/she wants selected.
     */
    public class SelectionListener implements ListSelectionListener {
	JTextField field;
	public SelectionListener(JTextField field){
	    this.field = field;
	}

	public void valueChanged(ListSelectionEvent e) {
	    if (e.getValueIsAdjusting() == true) {
		String text = tree.get(e.getFirstIndex());
		text = text.substring(0, text.indexOf(" "));
		if (text.equals(field.getText())) {
		    text = tree.get(e.getLastIndex());
		    text = text.substring(0, text.indexOf(" "));
		}
		field.setText(text);
	    }
	}
    }

    private class TreePanel extends JPanel{
	public TreePanel(JTextField field){
		Object3DGraphController controller = ((AbstractDesigner)application).getGraphController();
		tree = controller.getGraph().getTree(allowedNames, forbiddenNames);
		this.setLayout(new BorderLayout());
		JPanel labelPanel = new JPanel();
		labelPanel.setLayout(new BoxLayout(labelPanel, BoxLayout.Y_AXIS));
		labelPanel.add(new JLabel("Tree:"));
		this.add(labelPanel);
	        
		treePanel = new JPanel();
		treeList = new JList(tree);
		treeList.addListSelectionListener(new SelectionListener(field));
		treeScroll = new JScrollPane(treeList);
		treeScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		treeScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		treeScroll.setPreferredSize(new Dimension(250,200));
		treePanel.add(treeScroll);
		this.add(treePanel, BorderLayout.SOUTH);
	}
    }
}


