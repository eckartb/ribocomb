package rnadesign.designapp.rnagui;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Logger;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
// import java.awt.datatransfer.Clipboard;
// import java.awt.datatransfer.StringSelection;
// import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JList;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
// import javax.swing.event.ListSelectionEvent;
// import javax.swing.event.ListSelectionListener;

import commandtools.CommandApplication;
import commandtools.CommandEvent;
import commandtools.CommandHistoryChangeListener;

public class CommandHistoryDisplay extends JPanel implements CommandHistoryChangeListener {

    private List<ActionListener> actionListeners = new ArrayList<ActionListener>();
    private final Dimension dimensions = new Dimension(259, 88);

    // private Clipboard clipboard;
    private CommandApplication app;

    private Logger log = Logger.getLogger("NanoTiler_debug");

    private JList list;
    private JScrollPane scrollPane;

    public static final int LINES_MAX = 5;

    public CommandHistoryDisplay(CommandApplication application) { 
	this.app = application;
	this.app.addCommandHistoryChangeListener(this); // add listener 
	addComponents();
    }

    /*
      private void generateActionEvent(Object source) {
      ActionEvent event = new ActionEvent(source, ActionEvent.ACTION_PERFORMED, "Some action event thing");
      for (int i = 0; i < actionListeners.size(); i++) {
      actionListeners.get(i).actionPerformed(event);
      }
      }
      
      public void setClipboard(String copyString) {
      System.out.println("Set clipboard: " + copyString);
      if (clipboard == null) {
      System.out.println("Clipboard is null!");
      clipboard = new Clipboard("historyClipboard");
      }
      else {
      System.out.println("Clipboard is not null!");
      }
      StringSelection historyCommand = new StringSelection(copyString);
      clipboard.setContents(historyCommand, historyCommand);
      }
      
      public Clipboard getClipboard() {
      System.out.println("Returning clipboard: " + clipboard);
      return clipboard;
      }
    */

    private void addComponents() {
	setPreferredSize(dimensions);
	setLayout(new BorderLayout());
	setBorder(BorderFactory.createLineBorder(Color.black));
	add(new JLabel("Command history"), BorderLayout.NORTH);
	list = new JList(new Vector<String>());
	// list.addListSelectionListener(new SelectionListener());
	scrollPane = new JScrollPane(list); //scrollPane = new JScrollPane(textPane);
	scrollPane.setPreferredSize(dimensions);
	add(scrollPane, BorderLayout.CENTER);
    }

    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    /*
      private final class SelectionListener implements ListSelectionListener {
      public void valueChanged(ListSelectionEvent e) {
      generateActionEvent(e);
      String selectedCommand = new String();
      clipboard = new Clipboard("historyClipboard");
      StringSelection historyCommand = new StringSelection(selectedCommand);
      clipboard.setContents(historyCommand, historyCommand); // rnadesign.designapp.rnagui.ScriptCommandDisplay.this);
      // copy
      }
      }
    */

    public void commandHistoryChanged(CommandEvent event) {
	// generateActionEvent(event);
	Vector<String> history = app.getCommandHistory().toStringVector();
	list.setListData(history);
	setPreferredSize(dimensions);
	scrollPane.setPreferredSize(dimensions);
	final int end = scrollPane.getVerticalScrollBar().getMaximum();
	Runnable scrollToEnd = new Runnable() {
		public void run() {
		    try {
			scrollPane.getVerticalScrollBar().setValue(end);
		    }
		    catch (Exception e) {
			log.warning(e.getMessage());
		    }
		}
	    };
	SwingUtilities.invokeLater(scrollToEnd);
    }

}

/*
  private String getCutHistory(String[] lines) {
  if (lines == null) {
  return "";
  }
  if (lines.length == 0) {
  return "";
  }
  String result = "";
  for (int i = lines.length-1; i >= 0; --i) {
  if (i >= LINES_MAX) {
  break;
  }
  result = lines[i] + NEWLINE + result;
  }
  return result;
  }
*/

