package rnadesign.designapp.rnagui;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import rnadesign.rnacontrol.CameraController;
import tools3d.objects3d.Object3D;

/** paints a single object3d (withouth child nodes) */
public class DefaultSinglePainter implements Object3DSinglePainter {

    CameraController cameraController;

    public void paint(Graphics g, Object3D obj3d) {
	Graphics2D g2 = (Graphics2D)g;
	Point2D point = cameraController.project(obj3d.getPosition());
	double posX = point.getX();
	double posY = point.getY(); // use transformation here instead!
	g2.fill(new Rectangle2D.Double(posX, posY,
				       10, 10));
    }

    public CameraController getCameraController() {
	return cameraController;
    }

    public void setCameraController(CameraController camera) {
	this.cameraController = camera;
    }

}
