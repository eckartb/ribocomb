package rnadesign.designapp.rnagui;

import java.awt.Font;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import commandtools.CommandApplication;
import commandtools.CommandException;
import commandtools.BadSyntaxException;
import commandtools.Command;
import rnadesign.designapp.NanoTilerInterpreter;
import rnadesign.designapp.AbstractDesigner;
import rnadesign.designapp.DistanceCommand;
import rnadesign.rnacontrol.Object3DController;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnamodel.RnaTools;
import tools3d.objects3d.Object3D;
import java.util.logging.Logger;

/**
 * GUI Implementation of elastic command.
 *
 * @author Brett Boyle
 */
public class ElasticWizard implements GeneralWizard {

    private static final int COL_SIZE_NAME = 25; //TODO: size?
    private static final String FRAME_TITLE = "Elastic Wizard";

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    private boolean finished = false;
    private JFrame frame = null;
    private List<ActionListener> actionListeners = new ArrayList<ActionListener>();
    private CommandApplication application;
    private Object3DGraphController graphController;
    private NanoTilerInterpreter interpreter;
    private JTextField rootField;
    private JTextField classField;
    private JTextField cutoffField;
    private JTextField maxField;
    private JLabel label;
    private Vector<String> tree;
    private String[] allowedNames = RnaTools.getRnaClassNames();
    private String[] forbiddenNames;
    private JPanel treePanel;
    private JButton treeButton;
    private JList treeList;
    private JScrollPane treeScroll;

    /**
     * Constructor.
     *
     * @param application NanoTiler
     */
    public ElasticWizard(CommandApplication application) {
	assert application != null;
	this.application = application;
	this.interpreter = (NanoTilerInterpreter)application.getInterpreter();
    }

    /**
     * Called when "Cancel" button is pressed.
     * Window disappears.
     *
     */
    private class CancelListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    frame.setVisible(false);
	    frame = null;
	    finished = true;
	}
    }
    private class ElasticListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    String command = "elastic root=" + rootField.getText().trim() +
		" class=" + classField.getText().trim() + " cutoff=" + cutoffField.getText().trim() + " max=" + maxField.getText().trim();

	    try {
		application.runScriptLine(command);
	    }
	    catch(CommandException ce) {
		JOptionPane.showMessageDialog(frame, ce + ": " + ce.getMessage());
	    }
	}
    }

    /**
     * Adds an action listener.
     *
     * @param listener The ActionListener to add.
     */
    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    /** Adds the components to the window. */
    public void addComponents() {
	Container f = frame.getContentPane();
	f.setLayout(new BorderLayout());

	JPanel centerPanel = new JPanel();
	centerPanel.setLayout(new BorderLayout());
	JPanel westCenterPanel = new JPanel();
	westCenterPanel.setLayout(new BorderLayout());
	label = new JLabel("Root: ");
	westCenterPanel.add(label,BorderLayout.NORTH);
	rootField = new JTextField(getSelectionText(), COL_SIZE_NAME);
	rootField.setText("root");
	westCenterPanel.add(rootField,BorderLayout.CENTER);
	westCenterPanel.add(new TreePanel(rootField),BorderLayout.SOUTH);
	centerPanel.add(westCenterPanel,BorderLayout.WEST);
	
	JPanel eastCenterPanel = new JPanel();
	eastCenterPanel.setLayout(new BoxLayout(eastCenterPanel,BoxLayout.Y_AXIS));
	JPanel temp = new JPanel();
	temp.add(new JLabel("Class:"));
	classField = new JTextField(COL_SIZE_NAME);
	classField.setText("Atom3D");
	temp.add(classField);
	eastCenterPanel.add(temp);
	JPanel temp2 = new JPanel();
	temp2.add(new JLabel("Cutoff:"));
	cutoffField = new JTextField(COL_SIZE_NAME);
	cutoffField.setText("15.0");
	temp2.add(cutoffField);
	eastCenterPanel.add(temp2);
	JPanel temp3 = new JPanel();
	temp3.add(new JLabel("Max:"));
	maxField = new JTextField(COL_SIZE_NAME);
	maxField.setText("20");
	temp3.add(maxField);
	eastCenterPanel.add(temp3);
	centerPanel.add(eastCenterPanel,BorderLayout.EAST);


	JPanel buttonPanel = new JPanel(new FlowLayout());
	JButton button = new JButton("Close");
	button.addActionListener(new CancelListener());
	buttonPanel.add(button);
	button = new JButton("Done");
	button.addActionListener(new ElasticListener());
	buttonPanel.add(button);

	f.add(centerPanel, BorderLayout.CENTER);
	f.add(buttonPanel, BorderLayout.SOUTH);
    }

    /** Returns a String representation of the current selection. */
    private String getSelectionText() {
	Object3DGraphController graphController = ((AbstractDesigner)application).getGraphController();
	Object3DController gControl = graphController.getGraph();
	Object3D selection = gControl.getSelectionRoot();
	if (selection == null) {
	    return "";
	}
	String selectionName = selection.getName();
	Object3D curr = selection;
	while (curr.getParent() != null) {
	    curr = curr.getParent();
	    selectionName = curr.getName() + "." + selectionName;
	}
	return selectionName;
    }

    public boolean isFinished() {
	return finished;
    }

    /** Creates a window. */
    public void launchWizard(Object3DGraphController controller,Component parentFrame) {
	frame = new JFrame(FRAME_TITLE);
	if(controller==null){
	    log.info("GraphController received by lauchWizard is null!");
	}
	this.graphController = controller;
	addComponents();
	frame.pack();
	frame.setResizable(false);
	frame.setVisible(true);
    }
    private class TreePanel extends JPanel{
	public TreePanel(JTextField field){
		Object3DGraphController controller = ((AbstractDesigner)application).getGraphController();
		tree = controller.getGraph().getTree(allowedNames, forbiddenNames);
		this.setLayout(new BorderLayout());
		JPanel labelPanel = new JPanel();
		labelPanel.setLayout(new BoxLayout(labelPanel, BoxLayout.Y_AXIS));
		labelPanel.add(new JLabel("Tree:"));
		this.add(labelPanel);
	        
		treePanel = new JPanel();
		treeList = new JList(tree);
		treeList.addListSelectionListener(new SelectionListener(field));
		treeScroll = new JScrollPane(treeList);
		treeScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		treeScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		treeScroll.setPreferredSize(new Dimension(300,200));
		treePanel.add(treeScroll);
		this.add(treePanel, BorderLayout.SOUTH);
	}
    }
    /* links a textfield to a tree list.*/
    public class SelectionListener implements ListSelectionListener {
	JTextField field;
	public SelectionListener(JTextField field){
	    this.field = field;
	}

	public void valueChanged(ListSelectionEvent e) {
	    if (e.getValueIsAdjusting() == true) {
		String text = tree.get(e.getFirstIndex());
		text = text.substring(0, text.indexOf(" "));
		if (text.equals(field.getText())) {
		    text = tree.get(e.getLastIndex());
		    text = text.substring(0, text.indexOf(" "));
		}
		field.setText(text);
	    }
	}
    }

}
