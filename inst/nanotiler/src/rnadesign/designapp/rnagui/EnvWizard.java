package rnadesign.designapp.rnagui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JList;
import javax.swing.JScrollPane;

import java.util.Enumeration;
import java.util.Properties;

import commandtools.CommandApplication;
import commandtools.Command;
import commandtools.AbstractCommand;
import commandtools.UnknownCommandException;
import commandtools.BadSyntaxException;
import commandtools.EnvCommand;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerConstants;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import tools3d.Vector3D;
import rnadesign.designapp.NanoTilerInterpreter;

import java.io.PrintStream;
import java.io.OutputStream;

import static rnadesign.designapp.PackageConstants.NEWLINE;

/** lets user choose to partner object, inserts correspnding link into controller */
public class EnvWizard implements GeneralWizard {
    private static final int COL_SIZE = 15; 
    private boolean finished = false;

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    private JFrame frame = null;
    private JList resultsField;
    private JButton button;

    private Properties properties;
    private CommandApplication application;
    private NanoTilerInterpreter interpreter;
    private List<ActionListener> actionListeners = new ArrayList<ActionListener>();
    private Object3DGraphController graphController;
    private String[] variables;
    private PrintStream ps;

    public static final String FRAME_TITLE = "Environment Variables";

    public EnvWizard(CommandApplication application){
	assert application != null;
	this.application = application;
	this.interpreter = (NanoTilerInterpreter)application.getInterpreter();
	this.properties = interpreter.getVariables();
    }

    private class DoneListener implements ActionListener {

	public void actionPerformed(ActionEvent e) {
		frame.setVisible(false);
		frame = null;
		finished = true;
	}
    }

    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    public boolean isFinished() {
	return finished;
    }  
    private void formatVariables(){
	Enumeration names = properties.propertyNames();
	int countElements=0;
	while(names.hasMoreElements()){
	    names.nextElement();
	    countElements++;
	}
	names = properties.propertyNames();
	variables = new String[countElements];
	for(int i=0;i<variables.length;i++){
	    String name = (String)(names.nextElement());
	    variables[i] = name+" = " + properties.getProperty(name);
	}
    }


    /** generates and inserts new Object3D according to user dialog */
    public void launchWizard(Object3DGraphController graphController,
			     Component parentFrame) {
	if (graphController == null) {
	    log.info("GraphController received by launchWizard is null!");
	}
	this.graphController = graphController;
	frame = new JFrame(FRAME_TITLE);
	formatVariables();
	addComponents(frame);
	frame.pack();
	frame.setVisible(true);
    }

    public void addComponents(JFrame _frame) {
	Container f = _frame.getContentPane();
	f.setLayout(new BorderLayout());

	JPanel centerPanel = new JPanel();
	centerPanel.setPreferredSize(new Dimension(500,200));
	centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));
	resultsField = new JList(variables);
	JScrollPane scroll = new JScrollPane(resultsField);
	scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
	centerPanel.add(scroll);

	JPanel buttonPanel = new JPanel();
  	button = new JButton("Done");
       	button.addActionListener(new DoneListener());
	buttonPanel.add(button);
	f.add(buttonPanel, BorderLayout.SOUTH);
	f.add(centerPanel, BorderLayout.CENTER);
    }

}
