package rnadesign.designapp.rnagui;

import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class FileExtensionException extends Exception {

    public static final String NAMES_EXTENSION = ".names";
    public static final String POINTS_EXTENSION = ".points";
    public static final String PDB_EXTENSION = ".pdb";

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    /** Generates an unspecified file extension error. */
    FileExtensionException() {

	JOptionPane.showMessageDialog(null, "That is not a correct file. Please review the FAQ and use the correct file format.", "File Extension Error", JOptionPane.WARNING_MESSAGE);

    }

    /** Generates a specified file extension error. */
    FileExtensionException(String extension) {

	if( extension == NAMES_EXTENSION ) {

	    JOptionPane.showMessageDialog(null, "That is not a '.names' file. Please review the FAQ and use the correct file format.", ".names File Error", JOptionPane.WARNING_MESSAGE);

	}

	else if( extension == POINTS_EXTENSION ) {

	    JOptionPane.showMessageDialog(null, "That is not a '.points' file. Please review the FAQ and use the correct file format.", ".points File Error", JOptionPane.WARNING_MESSAGE);

	}

	else if( extension == PDB_EXTENSION ) {

	    JOptionPane.showMessageDialog(null, "That is not a file with a pdb file extension. Please consult the FAQ and use the correct file format.", "PDB File Error", JOptionPane.WARNING_MESSAGE);

	}

	else {

	    JOptionPane.showMessageDialog(null, "That is not a correct file. Please review the FAQ and use the correct file format.", "File Extension Error", JOptionPane.WARNING_MESSAGE);
	    log.severe("Threw FileExtensionException: " + extension);

	}

    }

}
