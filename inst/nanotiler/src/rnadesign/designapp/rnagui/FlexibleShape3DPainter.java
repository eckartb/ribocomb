package rnadesign.designapp.rnagui;

import tools3d.Ambiente;
import tools3d.Shape3D;

/** paints a single Shape3D 
 * this can be a base class to paint cubes, spheres, cylinders etc
 * @see Strategy pattern in Gamma et al: Design patterns
 */
public interface FlexibleShape3DPainter extends Shape3DPainter {    

    /** adds a painter that can handle certain types of shapes */
    public void addPainter(Shape3DPainter painter);

}
