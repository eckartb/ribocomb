package rnadesign.designapp.rnagui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.logging.Logger;

import rnadesign.rnacontrol.CameraController;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.SimpleCameraController;
import rnadesign.rnacontrol.Kaleidoscope;
import tools3d.Drawable;
import tools3d.Geometry;
import tools3d.GeometryColorModel;
import tools3d.Positionable3D;
import tools3d.Shape3D;
import tools3d.Shape3DSet;
import tools3d.ZBuffer;
import tools3d.objects3d.Primitive3D;

public class GeneralGeometryPainter implements Object3DPainter {

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    int[] xTmpArray;
    int[] yTmpArray;
    
    double geometryAngleDelta = 30.0 * RnaGuiConstants.DEG2RAD;
    boolean originMode = false;
    boolean debugMode = false;
    Primitive3D[] originPrimitives;
    GeometryPainter geometryPainter = new WireGeometryPainter();
    GeometryColorModel colorModel = new RainbowColorModel();
    CameraController cameraController = new SimpleCameraController();
    Object3DGraphController graphController;
    private RnaGuiParameters params;
    private ZBuffer zBuf = new ZBuffer();

    GeneralGeometryPainter(RnaGuiParameters p, GeometryPainter geometryPainter, Object3DGraphController graphController) {
	params = p;
	OriginFactory originFactory = new SimpleOriginFactory();
	originPrimitives = originFactory.createOrigin();
	this.geometryPainter = geometryPainter;
	// log.fine("" + this.cameraController);
	this.geometryPainter.setCameraController(this.cameraController); //CV - this line has a problem ... find out what changed this.cameraController 
	this.graphController = graphController;
    }

    /** hides certain classes  */
    public void addForbidden(String forbiddenClassName) {
	geometryPainter.addForbidden(forbiddenClassName);
    }

    /** removes all forbidden class names */
    public void clearForbidden() {
	geometryPainter.clearForbidden();
    }

    /** removes a class name */
    public void removeForbidden(String forbiddenClassName) {
	geometryPainter.removeForbidden(forbiddenClassName);
    }

    public void copy(Object3DPainter other) {
	if (other instanceof GeneralGeometryPainter) {
	    GeneralGeometryPainter c = (GeneralGeometryPainter)other;
	    cameraController.copy(c.cameraController);
	    params.copy(c.params);
	    // TODO painter chooser
	}
    }
    
    public void drawPrimitive(Graphics2D g2, Primitive3D p3d) {
	if (p3d == null) {
	    log.severe("Called drawPrimitive with null object!");
	    return;
	}
	Color colorSave = g2.getColor();
	g2.setColor(p3d.getColor());
	if (p3d.size() <= 0) {
	    // do nothing
	}
	else if (p3d.size() == 1) { // draw point
	    Point2D point = cameraController.project(p3d.getPoint(0));
	    double posX = point.getX();
	    double posY = point.getY(); // use transformation here instead!
	    g2.fill(new Ellipse2D.Double(posX, posY,
					 params.circleRad, params.circleRad));	
	}
	else if (p3d.size() == 2) { // draw line
	    Point2D pointA = cameraController.project(p3d.getPoint(0));
	    Point2D pointB = cameraController.project(p3d.getPoint(1));
	    g2.draw(new Line2D.Double(pointA, pointB));	
	}
	else {
	    if ((xTmpArray == null) || (xTmpArray.length != p3d.size())) {
		xTmpArray = new int[p3d.size()];
		yTmpArray = new int[p3d.size()];
	    }
	    for (int i = 0; i < p3d.size(); ++i) {
		Point2D point = cameraController.project(p3d.getPoint(i));
		xTmpArray[i] = (int)point.getX();
		xTmpArray[i] = (int)point.getY();
	    }
	    g2.fill(new Polygon(xTmpArray, yTmpArray, xTmpArray.length));
	}
	g2.setColor(colorSave);
    }
    
    /** returns objecto that projects 3d data to 2d screen */
    public CameraController getCameraController() { return cameraController; }
    
    /** central paint method (not automatically used) */
    public void paint(Graphics g, Object3DGraphController graphController) {
	if (graphController.getGraph() == null) {
	    return; // do nothing, no objects defined
	}
// 	if (debugMode) {
// 	    log.fine("called GeneralGeometryPainter.paint!");
// 	}
	Graphics2D g2 = (Graphics2D)g;
	int counter = 0;
	zBuf.reset(); // empty z-Buffer
	zBuf.setDirection(cameraController.getViewDirection()); // sets direction for z-buffer
	Shape3DSet shapeSet = graphController.getShapeSet();
	if (shapeSet != null) {
	    for (int i = 0; i < shapeSet.size(); ++i) {
		Shape3D shape = shapeSet.get(i);
		Geometry geometry = shape.createGeometry(geometryAngleDelta);
		/** isPaintable method speeds up program by only painting what is seen. */
		if (geometry != null) {
		    for (int j = 0; j < geometry.getNumberPoints(); ++j) {
			Drawable posi = geometry.getPoint(j);
			if (geometryPainter.isPaintable(posi)) {
			    zBuf.add(posi);
			}
		    }
		    for (int j = 0; j < geometry.getNumberEdges(); ++j) {
			Drawable posi = geometry.getEdge(j);
			if (geometryPainter.isPaintable(posi)) {
			    zBuf.add(posi);
			}
		    }
		    for (int j = 0; j < geometry.getNumberFaces(); ++j) {
			Drawable posi = geometry.getFace(j);
			if (geometryPainter.isPaintable(posi)) {
			    zBuf.add(posi);
			}
		    }
		}
	    }
	}
	else {
	    if (debugMode) {
		log.warning("shape set was null in GeneralGeometryPainter!");
	    }
	}
	if (originMode) {
	    // add primitives that correspond to a coordinate system at the origin
	    for (int i = 0; i < originPrimitives.length; ++i) {
		zBuf.add(originPrimitives[i]);
	    }
	}
	// generate symmetries!
	Kaleidoscope kaleidoscope = graphController.getKaleidoscope();
	assert kaleidoscope != null;
	if ((kaleidoscope != null) && (kaleidoscope.getSymmetryCount() > 1)) {
	    int origSize = zBuf.size();
	    kaleidoscope.apply(zBuf);
	    // log.fine("Generated kaleidoscope symmetries: " + zBuf.size());
	}
	else {
	    // log.fine("No kaleidoscope defined or needed!");
	}
	zBuf.sort(); // highest z-values last : we need reverse!
	for (int i = zBuf.size()-1; i >= 0; --i) {
	    Positionable3D pobj = zBuf.get(i);
	    if (pobj instanceof Primitive3D) {
		drawPrimitive(g2, (Primitive3D)pobj);
	    }
	    else if (pobj instanceof Drawable) {
		geometryPainter.paint(g2, (Drawable)pobj);
	    }
	    else {
		log.warning("unpaintable object in zBuffer!");
	    }
	}
    }
    
    public void setCameraController(CameraController p) {
	cameraController = p;
	geometryPainter.setCameraController(p);
    }

}
