package rnadesign.designapp.rnagui;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.logging.Logger;

import rnadesign.rnacontrol.CameraController;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.SimpleCameraController;
import rnadesign.rnacontrol.Kaleidoscope;
import tools3d.Drawable;
import tools3d.Geometry;
import tools3d.Positionable3D;
import tools3d.Shape3D;
import tools3d.Shape3DSet;
import tools3d.ZBuffer;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Primitive3D;

import static rnadesign.designapp.rnagui.PackageConstants.*;

public class GeneralPainter implements Object3DPainter {

    public static Logger log = Logger.getLogger(LOGFILE_DEFAULT);

    int[] xTmpArray;
    int[] yTmpArray;
    
    double geometryAngleDelta = 10.0 * RnaGuiConstants.DEG2RAD;
    boolean originMode = true;
    Primitive3D[] originPrimitives;
    Object3DSinglePainterChooser painterChooser = new SimpleSinglePainterChooser();
    Shape3DPainter shapePainter = null; //new SimpleShape3DPainter();
    GeometryPainter geometryPainter = new WireGeometryPainter();
    CameraController cameraController = new SimpleCameraController();
    Kaleidoscope kaleidoscope;

    private RnaGuiParameters params;
    private ZBuffer zBuf = new ZBuffer();

    GeneralPainter(RnaGuiParameters p, Shape3DPainter shapePainter, Kaleidoscope kaleidoscope) {
	params = p;
	OriginFactory originFactory = new SimpleOriginFactory();
	originPrimitives = originFactory.createOrigin();
	this.shapePainter = shapePainter;
	this.kaleidoscope = kaleidoscope;
    }

    public void copy(Object3DPainter other) {
	if (other instanceof GeneralPainter) {
	    GeneralPainter c = (GeneralPainter)other;
	    cameraController.copy(c.cameraController);
	    params.copy(c.params);
	    // TODO painter chooser
	}
    }

    public void drawObject3D(Graphics2D g2, Object3D obj3d) {
	// log.fine("Called drawObject3D");
	// choose painter:
	Object3DSinglePainter singlePainter = painterChooser.choosePainter(obj3d);
	if (singlePainter != null) {
	    singlePainter.setCameraController(cameraController);
	    singlePainter.paint(g2, obj3d);
	}
	else {
	    Point2D point = cameraController.project(obj3d.getPosition());
	    double posX = point.getX();
	    double posY = point.getY(); // use transformation here instead!
	    g2.fill(new Rectangle2D.Double(posX, posY,
					   params.rectWidth, params.rectHeight));
	}
    }
    
    public void drawPrimitive(Graphics2D g2, Primitive3D p3d) {
	if (p3d == null) {
	    log.severe("Called drawPrimitive with null object!");
	    return;
	}
	if (p3d.size() <= 0) {
	    // do nothing
	}
	g2.setXORMode(p3d.getColor());
	if (p3d.size() == 1) { // draw point
	    Point2D point = cameraController.project(p3d.getPoint(0));
	    double posX = point.getX();
	    double posY = point.getY(); // use transformation here instead!
	    g2.fill(new Ellipse2D.Double(posX, posY,
					 params.circleRad, params.circleRad));	
	}
	else if (p3d.size() == 2) { // draw line
	    Point2D pointA = cameraController.project(p3d.getPoint(0));
	    Point2D pointB = cameraController.project(p3d.getPoint(1));
	    g2.draw(new Line2D.Double(pointA, pointB));	
	}
	else {
	    if ((xTmpArray == null) || (xTmpArray.length != p3d.size())) {
		xTmpArray = new int[p3d.size()];
		yTmpArray = new int[p3d.size()];
	    }
	    for (int i = 0; i < p3d.size(); ++i) {
		Point2D point = cameraController.project(p3d.getPoint(i));
		xTmpArray[i] = (int)point.getX();
		xTmpArray[i] = (int)point.getY();
	    }
	    g2.fill(new Polygon(xTmpArray, yTmpArray, xTmpArray.length));
	}
    }
    
    /** returns objecto that projects 3d data to 2d screen */
    public CameraController getCameraController() { return cameraController; }
    
    /** central paint method (not automatically used) */
    public void paint(Graphics g, Object3DGraphController graphController) {
// 	if (graphController.getGraph().getObjectCount() == 0) {
// 	    return; // do nothing, no objects defined
// 	}
	// log.fine("called GeneralPainter.paint!");
	Graphics2D g2 = (Graphics2D)g;
	// 	graphController.resetIterator();
	int counter = 0;
	zBuf.reset(); // empty z-Buffer
	zBuf.setDirection(cameraController.getViewDirection()); // sets direction for z-buffer
	Shape3DSet shapeSet = graphController.getShapeSet();
	if (shapeSet != null) {
	    for (int i = 0; i < shapeSet.size(); ++i) {
		Shape3D shape = shapeSet.get(i);
		Geometry geometry = shape.createGeometry(geometryAngleDelta);
		if (geometry != null) {
		    // geometry.addToZBuffer(zBuf);
		    for (int j = 0; j < geometry.getNumberPoints(); ++j) {
			Drawable posi = geometry.getPoint(j);
			if (geometryPainter.isPaintable(posi)) {
			    zBuf.add(posi);
			}
		    }
		    for (int j = 0; j < geometry.getNumberEdges(); ++j) {
			Drawable posi = geometry.getEdge(j);
			if (geometryPainter.isPaintable(posi)) {
			    zBuf.add(posi);
			}
		    }
		    for (int j = 0; j < geometry.getNumberFaces(); ++j) {
			Drawable posi = geometry.getFace(j);
			if (geometryPainter.isPaintable(posi)) {
			    zBuf.add(posi);
			}
		    }
		}
	    }
	}
	else {
	    log.warning("shape set was null in GeneralPainter!");
	}
	if (originMode) {
	    // add primitives that correspond to a coordinate system at the origin
	    for (int i = 0; i < originPrimitives.length; ++i) {
		zBuf.add(originPrimitives[i]);
	    }
	}
	// generate symmetries!
	if ((kaleidoscope != null) && (kaleidoscope.getSymmetryCount() > 1)) {
	    int origSize = zBuf.size();
	    kaleidoscope.apply(zBuf);
	    log.info("Generated kaleodoscope symmetries: " + zBuf.size());
	}
	
	zBuf.sort(); // highest z-values last : we need reverse!
	for (int i = zBuf.size()-1; i >= 0; --i) {
	    Positionable3D pobj = zBuf.get(i);
	    if (pobj instanceof Object3D) {
		drawObject3D(g2, (Object3D)pobj);
	    }
	    else if (pobj instanceof Primitive3D) {
		drawPrimitive(g2, (Primitive3D)pobj);
	    }
	    else if (pobj instanceof Shape3D) {
		shapePainter.paint(g2, (Shape3D)pobj);
	    }
	    else if (pobj instanceof Drawable) {
		geometryPainter.paint(g2, (Drawable)pobj);
	    }
	}
    }

    public Object3DSinglePainterChooser getPainterChooser() {
	return painterChooser;
    }

    public Shape3DPainter getShapePainter() {
	return shapePainter;
    }

    public void addForbidden(String className) {
	assert false;
    }

    public void clearForbidden() {
	assert false;
    }

    public void removeForbidden(String className) {
	assert false;
    }

    public void setCameraController(CameraController p) {
	cameraController = p;
    }

    public void setPainterChooser(Object3DSinglePainterChooser chooser) {
	this.painterChooser = chooser;
    }

    public void setShapePainter(Shape3DPainter painter) {
	this.shapePainter = painter;
    }

}
