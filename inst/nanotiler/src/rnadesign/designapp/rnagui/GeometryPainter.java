package rnadesign.designapp.rnagui;

import java.awt.Graphics;

import tools3d.Character3D;
import tools3d.Drawable;
import tools3d.Edge3D;
import tools3d.Face3D;
import tools3d.Geometry;
import tools3d.GeometryColorModel;
import tools3d.Point3D;

public interface GeometryPainter extends Painter {

    /** adds a forbidden class name */
    public void addForbidden(String className);

    /** removes all forbidden class names */
    public void clearForbidden();

    /** removes a forbidden class name */
    public void removeForbidden(String className);

    public GeometryColorModel getGeometryColorModel();

    public boolean getPaintCharacterMode();

    public void paintPoint(Graphics g, Point3D point);

    public void paintCharacter(Graphics g, Character3D point);
    
    public void paintEdge(Graphics g, Edge3D edge);

    public void paintFace(Graphics g, Face3D face);

    public void paint(Graphics g, Geometry geometry);

    public void paint(Graphics g, Drawable drawable);

    public void setGeometryColorModel(GeometryColorModel m);

    public void setPaintCharacterMode(boolean b);

    public boolean isPaintable(Drawable p);
}
