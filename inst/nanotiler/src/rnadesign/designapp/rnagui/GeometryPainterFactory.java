package rnadesign.designapp.rnagui;

public interface GeometryPainterFactory {
    
    GeometryPainter createPainter(int styleId);
    GeometryPainter createPainter(int styleId, boolean paintLinks);

}
