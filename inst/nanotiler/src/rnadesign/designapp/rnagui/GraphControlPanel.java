package rnadesign.designapp.rnagui;

import controltools.ModelChangeEvent;
import controltools.ModelChangeListener;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.event.*;
import tools3d.GeometryColorModel;


import rnadesign.rnacontrol.CameraController;
import rnadesign.rnacontrol.Object3DGraphController;
// import rnadesign.rnacontrol.Kaleidoscope;
import tools3d.Matrix3D;
import tools3d.Vector3D;

import static rnadesign.designapp.rnagui.RnaGuiConstants.*;

class GraphControlPanel extends JPanel implements HierarchyListener, ModelChangeListener  {

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    JSplitPane splitPane;

    GraphPanel graphPanel;

    RnaGuiParameters params;

    private boolean paintLinks = true;

    /** this factory object creates shape painters typical for this application */
    public GeometryPainterFactory geometryPainterFactory = 
	new NanoTilerGeometryPainterFactory();

//     public GeometryColorModelFactory geometryColorModelFactory = 
//  	new NanoTilerGeometryColorModelFactory();

    YSliderPanel yslider;
    XSliderPanel xslider;
    JSlider phiSlider;
    JSlider thetaSlider;
    JSlider zoomSlider;
    JTextArea selected;

    double yValue = 0.0;
    double xValue = 0.0;
    double phiValue = 0.0;
    double thetaValue = 0.0;

    double oldYValue = 0.0;
    double oldXValue = 0.0;
    double oldPhiValue = 0.0;
    double oldThetaValue = 0.0;

    //    double angleEpsilon = 2.0 * RnaGuiConstants.DEG2RAD;

    //    double moveEpsilon = 2.0;

    double zoomFactor = 2.0;

    double dist = 10.0;

    double xDiff = 0.0;
    double yDiff = 0.0;
    double phiDiff = 0.0;
    double thetaDiff = 0.0;

    double phi = 0;

    double theta = 0;

    double zoom = 0;

    double psi = 0;

    private JCheckBox atomCheckBox;
    
    private JCheckBox nucleotideCheckBox;
    
    private JCheckBox strandCheckBox;

    public static enum PaintMode {
	Solid, Wire, Frame;
    }

    public static enum ColorModel {
	Rainbow, Strand, Atom, Nucleotide;
    }

    private ColorModel colorModel = ColorModel.Rainbow;
    private PaintMode paintMode = PaintMode.Solid;

    public ColorModel getPainterColorModel() {
	return colorModel;
    }

    public PaintMode getPaintMode() {
	return paintMode;
    }

    /**
     * Updates the graph panel's painter based
     * on the selected paint mode and color model.
     *
     * Added by Calvin Grunewald
     */
    public void updatePainter() {
	Object3DPainter painter = graphPanel.getPainter();
	CameraController projector = painter.getCameraController();
	
	GeometryPainter geometryPainter = null;
	switch(paintMode) {
	case Solid:
	    geometryPainter = geometryPainterFactory.createPainter(NanoTilerGeometryPainterFactory.SOLID_PAINTER, paintLinks);
	    break;
	case Wire:
	    geometryPainter = geometryPainterFactory.createPainter(NanoTilerGeometryPainterFactory.WIRE_PAINTER, paintLinks);
	    break;
	case Frame:
	    geometryPainter = geometryPainterFactory.createPainter(NanoTilerGeometryPainterFactory.WIREFRAME_PAINTER, paintLinks);
	    break;

	}

	GeometryColorModel geometryColorModel = null;
	switch(colorModel) {
	case Rainbow:
	    geometryColorModel = new RainbowColorModel();
	    break;
	case Strand:
	    geometryColorModel = new RnaStrandColorModel();
	    break;
	case Atom:
	    geometryColorModel = new AtomColorModel();
	    break;
	case Nucleotide:
	    geometryColorModel = new NucleotideColorModel();
	    break;
	}

	geometryPainter.setGeometryColorModel(geometryColorModel);

	GeneralGeometryPainter newPainter = new GeneralGeometryPainter(params, geometryPainter, graphPanel.getGraphController());
	newPainter.setCameraController(projector); // keep same camera settings
	graphPanel.setPainter(newPainter);

	repaintThis();
	
    }
   
    private class BottomPanel extends JPanel {
	BottomPanel(GraphControlPanel app) {
	    setBorder(BorderFactory.createLineBorder(Color.black));
	    //GridBagLayout bpgbl = new GridBagLayout();
	    //GridBagConstraints bpgbc = new GridBagConstraints();
	    //bpgbc.weightx = 1.0;
	    //bpgbc.weighty = 1.0;
	    //bpgbc.fill = GridBagConstraints.BOTH;
	    //bpgbc.anchor = GridBagConstraints.WEST;
	    BoxLayout layout = new BoxLayout(this, BoxLayout.Y_AXIS);
	    setLayout(layout);
	    //	    setLayout(new BorderLayout());
	    //setPreferredSize(new Dimension(params.cameraBottomPanelWidth, params.cameraBottomPanelHeight));
	    selected = new JTextArea(30, 2);
	    selected.setEditable(false);
	    selected.setLineWrap(true);
	    selected.setText(graphPanel.getGraphController().getGraph().getSelectedGraphText());
	    selected.setFont(new Font("roman", Font.PLAIN, 10));
	    // graphPanel.getGraphController().getGraph().addModelChangeListener(new LocalModelChangeListener());
	    graphPanel.getGraphController().addModelChangeListener(app);
	    JScrollPane scrollPane = new JScrollPane(selected, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
	    scrollPane.setPreferredSize(new Dimension(100, 50));
	    add(scrollPane);
	    // SelectedPanel selectedPanel = new SelectedPanel(graphPanel.getGraphController().getGraph());
	    //bpgbc.gridx = 0;
	    //bpgbl.setConstraints(selectedPanel,bpgbc);
	    // add(selectedPanel);
	    //	    add(new SelectedPanel(graphPanel.getGraphController().getGraph()), BorderLayout.WEST);
	    JPanel sliderPanel = new JPanel();
	    BoxLayout sliderPanelLayout = new BoxLayout(sliderPanel, BoxLayout.X_AXIS);
	    sliderPanel.setLayout(sliderPanelLayout);
	    // JPanel bottomMiddlePanel = new JPanel();
	    //GridBagLayout gbl = new GridBagLayout();
	    //GridBagConstraints gbc = new GridBagConstraints();
	    //gbc.weightx = 1.0;
	    //gbc.weighty = 1.0;
	    //int[] columnW = new int[1];
	    //columnW[0] = 212;
	    //gbl.columnWidths = columnW;
	    //bottomMiddlePanel.setLayout(gbl);
	    // CameraDisplayer cameradisplayer = new CameraDisplayer();
	    //gbc.gridx = 0;
	    //gbc.gridy = 1;
	    //gbc.fill = GridBagConstraints.BOTH;
	    // gbl.setConstraints(cameradisplayer,gbc);
	    // bottomMiddlePanel.add(cameradisplayer);
	    JButton resetButton = new JButton("Reset Position");
	    resetButton.addActionListener(new ResetButtonListener());
	    //gbc.fill = GridBagConstraints.NONE;
	    //gbc.gridy = 0;
	    //gbl.setConstraints(resetButton,gbc);
	    //bottomMiddlePanel.add(resetButton);
	    sliderPanel.add(resetButton);

	    zoomSlider = new JSlider(JSlider.HORIZONTAL,10 * (int)params.zoomMin,
				     10* (int)params.zoomMax, 10*(int)params.zoomInit);
 	    zoomSlider.addChangeListener(new ZoomChangedListener());
	    zoomSlider.setMajorTickSpacing(10);
	    //zoomSlider.setPaintTicks(true);
	    //zoomSlider.setPaintLabels(false);
	    //zoomSlider.setSnapToTicks(false);

	    zoomSlider.setPaintTicks(false);
	    zoomSlider.setPaintLabels(false);
	    zoomSlider.setSnapToTicks(false);

	    //gbc.gridy = 2;
	    //gbc.fill = GridBagConstraints.BOTH;
	    //gbl.setConstraints(zoomSlider,gbc);
	    
 	    //bottomMiddlePanel.add(zoomSlider);
	    
	    sliderPanel.add(new JLabel("Zoom"));
	    sliderPanel.add(zoomSlider);

	    //bpgbc.gridx = 1;	    
	    //bpgbl.setConstraints(bottomMiddlePanel, bpgbc);
	    //	    add(bottomMiddlePanel, BorderLayout.CENTER);
	    //add(bottomMiddlePanel);
	    //SliderPanel sliderPanel = new SliderPanel();
	    //bpgbc.gridx = 2;
	    //bpgbl.setConstraints(sliderPanel, bpgbc);
	    //add(sliderPanel);
	    //	    add(new SliderPanel(), BorderLayout.EAST);

	    GraphControlPanel.this.thetaSlider = new JSlider(JSlider.HORIZONTAL,(int)params.thetaMin,
				      (int)params.thetaMax, (int)params.thetaInit);
	    GraphControlPanel.this.thetaSlider.setMajorTickSpacing(30);
	    GraphControlPanel.this.thetaSlider.setPaintTicks(false);
	    GraphControlPanel.this.thetaSlider.setPaintLabels(false);
	    GraphControlPanel.this.thetaSlider.addChangeListener(new ThetaChangedListener());
	    
	    sliderPanel.add(new JLabel("Rotate Up/Down"));
	    sliderPanel.add(GraphControlPanel.this.thetaSlider);
	    
	    GraphControlPanel.this.phiSlider = new JSlider(JSlider.HORIZONTAL,(int)params.phiMin,
				    (int)params.phiMax, (int)params.phiInit);
	    GraphControlPanel.this.phiSlider.addChangeListener(new PhiChangedListener());
	    GraphControlPanel.this.phiSlider.setMajorTickSpacing(60);
 	    GraphControlPanel.this.phiSlider.setPaintTicks(false);
 	    GraphControlPanel.this.phiSlider.setPaintLabels(false);

	    sliderPanel.add(new JLabel("Rotate Left/Right"));
	    sliderPanel.add(GraphControlPanel.this.phiSlider);

	    add(sliderPanel);

       

	}
    }
    
    // private class LocalModelChangeListener implements ModelChangeListener {
    public void modelChanged(ModelChangeEvent e) {
	selected.setText(graphPanel.getGraphController().getGraph().getSelectedGraphText());
    }
    // }
    

    private class YSliderPanel extends JPanel {

	JSlider ySlider;

	YSliderPanel() {
	    setBorder(BorderFactory.createLineBorder(Color.black));
	    setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
	    ySlider = new JSlider(JSlider.VERTICAL, (int)params.yMin, (int)params.yMax, (int)params.yInit);
	    ySlider.setMajorTickSpacing(10);
	    ySlider.setPaintTicks(false);
	    ySlider.setPaintLabels(false);
	    ySlider.addChangeListener(new YChangedListener());
	    add(ySlider);
	}
    }

    private class XSliderPanel extends JPanel {

	JSlider xSlider;

	XSliderPanel() {
	    setBorder(BorderFactory.createLineBorder(Color.black));
	    setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
	    setSize(new Dimension(params.xSliderPanelWidth, params.xSliderPanelHeight));

	    xSlider = new JSlider(JSlider.HORIZONTAL, (int)params.xMin, (int)params.xMax, (int)params.xInit);
	    xSlider.setMajorTickSpacing(10);
	    xSlider.setPaintTicks(false);
	    xSlider.setPaintLabels(false);
	    xSlider.addChangeListener(new XChangedListener());
	    add(xSlider);
	}
	//TODO: create an algorithm so the sliders stay correctly aligned with window resizing and moving the image around
    }
    /*
    private class ThetaSliderPanel extends JPanel {
	JSlider thetaSlider;
	
	ThetaSliderPanel() {
	    setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
	    thetaSlider = new JSlider(JSlider.HORIZONTAL,(int)params.thetaMin,
				      (int)params.thetaMax, (int)params.thetaInit);
	    thetaSlider.setMajorTickSpacing(30);
	    thetaSlider.setPaintTicks(true);
	    thetaSlider.setPaintLabels(true);
	    JLabel sliderLabel = new JLabel("Rotate Up/Down", JLabel.CENTER);
	    add(sliderLabel);
	    thetaSlider.addChangeListener(new ThetaChangedListener());
	    add(thetaSlider);
	}
    }

    private class PhiSliderPanel extends JPanel {
	JSlider phiSlider;
 
	PhiSliderPanel() {
	    setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
	    JLabel sliderLabel = new JLabel("Rotate Left/Right", JLabel.CENTER);
	    add(sliderLabel);
	    phiSlider = new JSlider(JSlider.HORIZONTAL,(int)params.phiMin,
				    (int)params.phiMax, (int)params.phiInit);
	    phiSlider.addChangeListener(new PhiChangedListener());
	    phiSlider.setMajorTickSpacing(60);
 	    phiSlider.setPaintTicks(true);
 	    phiSlider.setPaintLabels(true);
	    add(phiSlider);

	}
    }
    */

    public void moveThetaSlider(double change) { setThetaSliderValue(getThetaSliderValue() + change); }
    
    public void movePhiSlider(double change) { setPhiSliderValue(getPhiSliderValue() + change); }

    private double getThetaSliderValue() { return thetaSlider.getValue(); }

    private void setThetaSliderValue(double d) {
	if (d < thetaSlider.getMinimum() || d > thetaSlider.getMaximum()) d = thetaSlider.getMaximum() + thetaSlider.getMinimum() - d;
	oldThetaValue = (int)(d) * DEG2RAD;
	thetaSlider.setValue((int)d); 
    }

    private double getPhiSliderValue() {return phiSlider.getValue(); }

    private void setPhiSliderValue(double d) {
	if (d < phiSlider.getMinimum() || d > phiSlider.getMaximum()) d = phiSlider.getMaximum() + phiSlider.getMinimum() - d;
	oldPhiValue = (int)(d) * DEG2RAD;
	phiSlider.setValue((int)d);
    }

    private void setZoomSliderValue(double d) { zoomSlider.setValue((int)d); }

    /*
    private class SliderPanel extends JPanel {

	//	JSlider thetaSlider;

	//	JSlider phiSlider;

	JSlider zoomSlider;

	SliderPanel() {
	    setBorder(BorderFactory.createLineBorder(Color.black));
	    GridBagLayout gbl = new GridBagLayout();
	    GridBagConstraints gbc = new GridBagConstraints();
	    setLayout(gbl);
	    //	    setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
	    setPreferredSize(new Dimension(params.cameraSliderPanelWidth, params.cameraSliderPanelHeight));

// 	    thetaSlider = new JSlider(JSlider.HORIZONTAL,(int)params.thetaMin,
// 				      (int)params.thetaMax, (int)params.thetaInit);
// 	    thetaSlider.setMajorTickSpacing(30);
//  	    thetaSlider.setPaintTicks(true);
//  	    thetaSlider.setPaintLabels(true);
// 	    JLabel sliderLabel = new JLabel("Theta", JLabel.CENTER);
// 	    add(sliderLabel);
// 	    thetaSlider.addChangeListener(new ThetaChangedListener());
// 	    add(thetaSlider);

// 	    sliderLabel = new JLabel("Phi", JLabel.CENTER);
// 	    add(sliderLabel);
// 	    phiSlider = new JSlider(JSlider.HORIZONTAL,(int)params.phiMin,
// 				    (int)params.phiMax, (int)params.phiInit);
// 	    phiSlider.addChangeListener(new PhiChangedListener());
// 	    phiSlider.setMajorTickSpacing(60);
//  	    phiSlider.setPaintTicks(true);
//  	    phiSlider.setPaintLabels(true);
// 	    add(phiSlider);
	    gbc.fill = GridBagConstraints.BOTH;
	    gbc.weightx = 1.0;
	    gbc.weighty = 1.0;
	    thetaslider = new ThetaSliderPanel();
	    gbc.gridy = 0;
	    gbl.setConstraints(thetaslider,gbc);
	    add(thetaslider);
	    phislider = new PhiSliderPanel();
	    gbc.gridy = 1;
	    gbl.setConstraints(phislider,gbc);
	    add(phislider);

 	    JLabel sliderLabel = new JLabel("Zoom", JLabel.CENTER);
 	    // add(sliderLabel);
//  	    zoomSlider = new JSlider(JSlider.HORIZONTAL,10 * (int)params.zoomMin,
//  				     10* (int)params.zoomMax, 10*(int)params.zoomInit);
//   	    zoomSlider.addChangeListener(new ZoomChangedListener());
//  	    zoomSlider.setMajorTickSpacing(10);
//  	    zoomSlider.setPaintTicks(true);
//  	    zoomSlider.setPaintLabels(false);
//  	    zoomSlider.setSnapToTicks(false);
//  	    add(zoomSlider);

	    //Turn on labels at major tick marks.
// 	    framesPerSecond.setMajorTickSpacing(10);
// 	    framesPerSecond.setMinorTickSpacing(1);
// 	    framesPerSecond.setPaintTicks(true);
// 	    framesPerSecond.setPaintLabels(true);
	}


    }

    */

    /** paints faces; not solid, 3D points */
    private class WireModeListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    log.info("Switching to WirePainter!");
	    paintMode = PaintMode.Wire;
	    updatePainter();
	    log.fine("End Switching to WirePainter!");
	}
    }

    /** returns array of form "{"Atom3D", "RnaStrand3D"} */
    private String[] getForbiddenNames() {
	List<String> list = new ArrayList<String>();

	if (!atomCheckBox.isSelected()) {
	    list.add("Atom3D");
	}
	if (!nucleotideCheckBox.isSelected()) {
	    list.add("Nucleotide3D");
	}
	if (!strandCheckBox.isSelected()) {
	    list.add("RnaStrand");
	}
	String[] result = new String[list.size()];
	for (int i = 0; i < result.length; ++i) {
	    result[i] = (String)(list.get(i));
	}
	return result;
    }

    /** changes the forbidden class name */
    private class ChangeForbiddenListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    String[] forbiddenNames = getForbiddenNames();
	    // get painter
	    Object3DPainter painter = graphPanel.getPainter();
	    painter.clearForbidden(); // clear all class names
// 	    GeometryPainter painter = // GraphPanel.getPainter();
	}
	
    }

    /** solid 3D points will be shown; unincludes edges */
    JCheckBox cBox;
    private class NoLinkGeometryListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    log.info("Switching to NoLinkGeometryPainter!");
	    Object3DPainter painter = graphPanel.getPainter();
	    CameraController projector = painter.getCameraController();
	    GeometryPainter geometryPainter;
	    if(cBox.isSelected()) {
		paintLinks = true;
	    }
	    else {
		paintLinks = false;
	    }

	    updatePainter();
	    
	    log.fine("End Switching to NoLinkGeometryPainter!");
	}
    }

    /** gets rid of all characters from WireGeometryPainter display. TODO: doesn't work... */
    JCheckBox checkBox;
    /*
    private class CharacterDisplayListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    log.info("Switching to CharacterDisplayPainter!");
	    Object3DPainter painter = graphPanel.getPainter();
	    CameraController projector = painter.getCameraController();
	    
	    // if checkBox is checked then pass class WireGeometryPainter, which has the characters defined;  if not then pass WireGeometryCharactersPainter, which does not define characters
	    GeometryPainter geometryPainter;
	    if(checkBox.isSelected()) {
		geometryPainter = geometryPainterFactory.createPainter(NanoTilerGeometryPainterFactory.WIRE_PAINTER);
	    }
	    else {
		geometryPainter = geometryPainterFactory.createPainter(NanoTilerGeometryPainterFactory.WIRE_CHARACTER_PAINTER); //I think this should go to a different class that repaints it without wiring. This class refers to 
	    }
	    //	    if (checkBox.getStateChange() == ItemEvent.DESELECTED) { //CV - this will change the wire form back to normal when it's deselected...
	    //		geometryPainter = 
		    //        }

	    GeneralGeometryPainter newPainter = new GeneralGeometryPainter(params, geometryPainter, graphPanel.getGraphController());	    
	    
	    newPainter.setCameraController(projector); // keep same camera settings
	    graphPanel.setPainter(newPainter);
	    repaintThis();
	    log.fine("End Switching to CharacterDisplayPainter!");
	}
    }

    */

    /** hides atoms */
    private class HideAtomListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    log.fine("called HideAtomListener.actionPerformed");
	    Object3DPainter painter = graphPanel.getPainter();
	    if (atomCheckBox.isSelected()) {
		painter.addForbidden("Atom3D");
	    }
	    else {
		painter.removeForbidden("Atom3D");
	    }
	    repaintThis();
	}
    }

    /** hides nucleotides */
    private class HideNucleotideListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    log.fine("called HideNucleotideListener.actionPerformed");
	    Object3DPainter painter = graphPanel.getPainter();
	    if (nucleotideCheckBox.isSelected()) {
		painter.addForbidden("Nucleotide3D");
	    }
	    else {
		painter.removeForbidden("Nucleotide3D");
	    }	    
	    repaintThis();
	}
    }

    /** hides strands */
    private class HideStrandListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    log.fine("called HideStrandListener.actionPerformed");
	    Object3DPainter painter = graphPanel.getPainter();
	    if (strandCheckBox.isSelected()) {
		painter.addForbidden("RnaStrand");
	    }
	    else {
		painter.removeForbidden("RnaStrand");
	    }
	    repaintThis();
	}
    }
    
    /** paints solid, 3D points */
    private class SolidModeListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    log.info("Switching to SolidGeometryPainter!");
	    paintMode = PaintMode.Solid;
	    updatePainter();
	    log.fine("End Switching to SolidGeometryPainter!");
	}
    }

    /** paints only edges; no faces, no points */
    private class WireFrameModeListener implements ActionListener {
	public void actionPerformed (ActionEvent e) {
	    paintMode = PaintMode.Frame;
	    log.info("Switching to WireFrameGeometryPainter");
	    updatePainter();
	    log.fine("End Switching to WireFrameGeometryPainter");
	}
    }


    private class RainbowModeListener implements ActionListener {
	public void actionPerformed (ActionEvent e) {
	    log.info("Switching to RainbowColormodel");
	    //Object3DPainter painter = graphPanel.getPainter();
	    //CameraController projector = painter.getCameraController();
// 	    GeometryColorModel colorModel = 
// 		geometryColorModelFactory.createPainter(NanoTilerGeometryColorModelFactory.RAINBOW_COLOR_MODEL);
 	    //GeometryPainter geometryPainter = 
	    //geometryPainterFactory.createPainter(NanoTilerGeometryPainterFactory.RAINBOW_PAINTER);
		//GeneralGeometryPainter newPainter = new GeneralGeometryPainter(params,geometryPainter, graphPanel.getGraphController());
		//newPainter.setCameraController(projector);
		//graphPanel.setPainter(newPainter);
	    
	    colorModel = ColorModel.Rainbow;
	    updatePainter();
	    log.fine("End Switching to RainbowColorModel");
	}
    }

    private class RnaStrandModeListener implements ActionListener {
	public void actionPerformed (ActionEvent e) {
	    log.info("Switching to RnaStrandColorModel");
	  
	    colorModel = ColorModel.Strand;
	    updatePainter();
	    log.fine("End Switching to RnaStrandColorModel");
	}
    }

    private class PointModeListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    Object3DPainter painter = graphPanel.getPainter();
	    CameraController projector = painter.getCameraController();
	    PointPainter pointPainter = new PointPainter(params);
	    pointPainter.setCameraController(projector); // keep same camera settings
	    graphPanel.setPainter(pointPainter);
	    repaintThis();
	}
    }

    private class DefaultModeListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    Object3DPainter painter = graphPanel.getPainter();
	    CameraController projector = painter.getCameraController();
	    SimplePainter newPainter = new SimplePainter(params);
	    newPainter.setCameraController(projector); // keep same camera settings
	    graphPanel.setPainter(newPainter);
	    repaintThis();
	}
    }

    /** TODO: improve */
    private class YChangedListener implements ChangeListener {
	public void stateChanged(ChangeEvent e) {
	    Object3DPainter painter = graphPanel.getPainter();
	    CameraController projector = painter.getCameraController();
	    yValue = getYSliderValue();
	    yDiff = yValue - oldYValue;
	    projector.moveCamera(0.0, yDiff);
	    repaintThis();
	    oldYValue = yValue;
	}
    }

    /** TODO: improve */
    private class XChangedListener implements ChangeListener {
	public void stateChanged(ChangeEvent e) {
	    Object3DPainter painter = graphPanel.getPainter();
	    CameraController projector = painter.getCameraController();
	    xValue = getXSliderValue();
	    xDiff = xValue - oldXValue;
	    projector.moveCamera(-xDiff, 0.0);
	    repaintThis();
	    oldXValue = xValue;
	}
    }

    private class PhiChangedListener implements ChangeListener {
	/** Listen to the slider. */
	public void stateChanged(ChangeEvent e) {
	    //	    JSlider source = (JSlider)e.getSource();
	    //	    phi = (double)source.getValue();
	    //	    updateCamera(theta * DEG2RAD, phi * DEG2RAD, psi * DEG2RAD, dist);

	    phiValue = getPhiSliderValue()*DEG2RAD;
	    phiDiff = phiValue - oldPhiValue;
	    Object3DPainter painter = graphPanel.getPainter();
	    CameraController projector = painter.getCameraController();
	    projector.rotateCameraWest(-phiDiff);
	    repaintThis();
	    oldPhiValue = phiValue;
	}
    }

    private class ThetaChangedListener implements ChangeListener {
	/** Listen to the slider. */
	public void stateChanged(ChangeEvent e) {
	    //	    JSlider source = (JSlider)e.getSource();
	    //	    theta = (double)source.getValue();
	    //	    updateCamera(theta * DEG2RAD, phi * DEG2RAD, psi * DEG2RAD, dist);

	    thetaValue = getThetaSliderValue()*DEG2RAD;
	    thetaDiff = thetaValue - oldThetaValue;
	    Object3DPainter painter = graphPanel.getPainter();
	    CameraController projector = painter.getCameraController();
	    projector.rotateCameraNorth(-thetaDiff);
	    repaintThis();

	    oldThetaValue = thetaValue;

	}
    }

    private void resetCamera() {
	for(int i = 0; i < 2; i++) //repeat because for some reason it won't reset all the way the first time.
	    {
		graphPanel.getCameraController().getCamera().setPosition(new Vector3D(0.0, -10.0, 0.0));
		graphPanel.getCameraController().getCamera().setOrientation(new Matrix3D(-1.0, 0.0, 0.0, 0.0, 1.0, -0.0, 0.0, 0.0, 1.0));
		graphPanel.getCameraController().getCamera().setZoom(1.0);
		setZoomSliderValue(1.0);
		setXSliderValue(0);
		setYSliderValue(0);
		setThetaSliderValue(0.0);
		setPhiSliderValue(0.0);
		repaintThis();
	    }

    }

    private class ResetButtonListener extends AbstractAction {
	public void actionPerformed(ActionEvent e) {
	    resetCamera();
	}
    }


    private class ZoomChangedListener implements ChangeListener {
	/** Listen to the slider. */
	public void stateChanged(ChangeEvent e) {
	    JSlider source = (JSlider)e.getSource();
	    Object3DPainter painter = graphPanel.getPainter();
	    CameraController projector = painter.getCameraController();
	    zoom = 0.1 * (double)source.getValue();
// 	    if(zoom < 0) {
// 		zoom = Math.abs(zoom);
// 		zoom = 1 / zoom;
// 	    }
	    zoom = Math.pow(2.0, zoom); // use 2**zoom instead
	    projector.setZoom(zoom);
	    repaintThis();
	}
    }

 //    private class RotateUpListener implements ActionListener {
// 	public void actionPerformed(ActionEvent e) {
// 	    Object3DPainter painter = graphPanel.getPainter();
// 	    CameraController projector = painter.getCameraController();
// 	    projector.rotateCameraNorth(angleEpsilon);
// 	    repaintThis();
// 	}
//     }
    
//     private class RotateDownListener implements ActionListener {
// 	public void actionPerformed(ActionEvent e) {
// 	    Object3DPainter painter = graphPanel.getPainter();
// 	    CameraController projector = painter.getCameraController();
// 	    projector.rotateCameraNorth(-angleEpsilon);
// 	    repaintThis();
// 	}
//     }
    
//     private class RotateLeftListener implements ActionListener {
// 	public void actionPerformed(ActionEvent e) {
// 	    Object3DPainter painter = graphPanel.getPainter();
// 	    CameraController projector = painter.getCameraController();
// 	    projector.rotateCameraWest(angleEpsilon);
// 	    repaintThis();
// 	}
//     }
    
//     private class RotateRightListener implements ActionListener {
// 	public void actionPerformed(ActionEvent e) {
// 	    Object3DPainter painter = graphPanel.getPainter();
// 	    CameraController projector = painter.getCameraController();
// 	    projector.rotateCameraWest(-angleEpsilon);
// 	    repaintThis();
// 	}
//     }

    /** zooms in */
    private class ZoomInListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    Object3DPainter painter = graphPanel.getPainter();
	    CameraController projector = painter.getCameraController();
	    zoomFactor *= 2;
	    projector.setZoom(zoomFactor);
	    repaintThis();
	}
    }

    /** zooms out */
    private class ZoomOutListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    Object3DPainter painter = graphPanel.getPainter();
	    CameraController projector = painter.getCameraController();
	    zoomFactor *= 0.5;
	    projector.setZoom(zoomFactor);
	    repaintThis();
	}
    }

//     /** resets to original camera position */  //doesn't work
//     private class ResetListener implements ActionListener {
// 	public void actionPerformed(ActionEvent e) {
// 	    graphPanel.resetPainter();
// 	    repaintThis();
// 	}
//     }

    private class PaintModeSelectedListener implements ItemListener {

	public void itemStateChanged(ItemEvent e) {
	    paintMode = (PaintMode) e.getItem();
	    updatePainter();
	}
    }

    private class ColorModelSelectedListener implements ItemListener {
	
	public void itemStateChanged(ItemEvent e) {
	    colorModel = (ColorModel) e.getItem();
	    updatePainter();
	}
    }

    
    GraphControlPanel(RnaGuiParameters params, Object3DGraphController controller) {
	this.params = params;
	setBackground(Color.WHITE);
	BoxLayout layout = new BoxLayout(this, BoxLayout.Y_AXIS);
	//GridBagLayout gbl = new GridBagLayout();
	//int[] columnW = new int[2];
	//columnW[0] = 800;
	//columnW[1] = params.ySliderPanelWidth;
	//int[] rowH = new int[4];
	//rowH[0] = 40;
	//rowH[1] = params.cameraLeftPanelHeight;
	//rowH[2] = params.xSliderPanelHeight;
	//rowH[3] = params.cameraBottomPanelHeight;
	//gbl.columnWidths = columnW;
	//gbl.rowHeights = rowH;
	//setLayout(gbl);//	setLayout(new BorderLayout());
	setLayout(layout);
	//GridBagConstraints gbc = new GridBagConstraints();
	//gbc.fill = GridBagConstraints.BOTH;
	//gbc.anchor = GridBagConstraints.WEST;

	setBorder(BorderFactory.createLineBorder(Color.black));
	

	

	JPanel topPanel = new JPanel();
	topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.X_AXIS));
	JButton button = new JButton("Solid");
	cBox = new JCheckBox("Links");
	cBox.setSelected(true);
	cBox.addActionListener(new NoLinkGeometryListener());
	
	//	checkBox = new JCheckBox("Characters");
//  	checkBox.setSelected(true);
// 	checkBox.setToolTipText("Add/Remove characters from Wire");
 	//checkBox.addActionListener(new CharacterDisplayListener());
	//topPanel.add(checkBox);
	// atomCheckBox = new JCheckBox("Atoms");
	// atomCheckBox.addActionListener(new HideAtomListener());
	// topPanel.add(atomCheckBox);
	// nucleotideCheckBox = new JCheckBox("Nucleotides");
	// nucleotideCheckBox.addActionListener(new HideNucleotideListener());
	// topPanel.add(nucleotideCheckBox);
	// strandCheckBox = new JCheckBox("Strands");
	// strandCheckBox.addActionListener(new HideStrandListener());
	// topPanel.add(strandCheckBox);
	JPanel painterPanel = new JPanel();
	painterPanel.setLayout(new BoxLayout(painterPanel, BoxLayout.X_AXIS));
	painterPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black), "Rendering Options"));
	JLabel boxLabel = new JLabel("Paint Mode");
	painterPanel.add(boxLabel);
	painterPanel.add(Box.createHorizontalStrut(5));
	JComboBox comboBox = new JComboBox(PaintMode.values());
	comboBox.setSelectedItem(paintMode);
	comboBox.setEditable(false);
	comboBox.addItemListener(new PaintModeSelectedListener());
	painterPanel.add(comboBox);
	painterPanel.add(Box.createHorizontalStrut(20));
	boxLabel = new JLabel("Color Model");
	painterPanel.add(boxLabel);
	painterPanel.add(Box.createHorizontalStrut(5));
	comboBox = new JComboBox(ColorModel.values());
	comboBox.setSelectedItem(colorModel);
	comboBox.setEditable(false);
	comboBox.addItemListener(new ColorModelSelectedListener());
	painterPanel.add(comboBox);
	painterPanel.add(Box.createHorizontalStrut(20));
	painterPanel.add(cBox);
	topPanel.add(painterPanel);
	/*
	button.addActionListener(new SolidModeListener());
	button.setToolTipText("View Solid Color Model");
	topPanel.add(button);
	button = new JButton("Wire");
	button.addActionListener(new WireModeListener());
	button.setToolTipText("View Wire Color Model");
	topPanel.add(button);
 	button = new JButton("Frame");
 	button.addActionListener(new WireFrameModeListener());
 	button.setToolTipText("View Wire Frame Color model");
 	topPanel.add(button);
	button = new JButton("Rainbow");
	button.addActionListener(new RainbowModeListener());
	button.setToolTipText("View Rainbow Color model");
	topPanel.add(button);
	button = new JButton("RnaStrand");
	button.addActionListener(new RnaStrandModeListener());
	button.setToolTipText("View RnaStrand Color model");
	topPanel.add(button);
	*/
// 	button = new JButton("reset");
// 	button.addActionListener(new ResetListener());
// 	topPanel.add(button);
	topPanel.setBorder(BorderFactory.createLineBorder(Color.black));
	topPanel.setMaximumSize(new Dimension(topPanel.getMaximumSize().width, topPanel.getPreferredSize().height));
	//gbc.gridx = 0;
	//gbc.gridy = 0;
	//gbc.weightx = 0.0;
	//gbc.weighty = 0.0;
	//gbc.gridwidth = GridBagConstraints.REMAINDER;
	//gbl.setConstraints(topPanel,gbc);
	add(topPanel);//add(topPanel, BorderLayout.NORTH);

	JPanel temp = new JPanel(new BorderLayout());


	graphPanel = new GraphPanel(this, params, controller);
	graphPanel.setBackground(Color.WHITE);

	
	//gbc.gridx = 0;
	//gbc.gridy = 1;
	//gbc.weightx = 1.0;
	//gbc.weighty = 1.0;
	//gbc.gridwidth = GridBagConstraints.RELATIVE;
	//gbl.setConstraints(graphPanel, gbc);
	temp.add(graphPanel, BorderLayout.CENTER);//       	add(graphPanel, BorderLayout.CENTER);


	yslider = new YSliderPanel();
	//gbc.gridx = 1;
	//gbc.gridy = 1;
	//gbc.gridwidth = GridBagConstraints.REMAINDER;
	//gbl.setConstraints(yslider, gbc);
	temp.add(yslider, BorderLayout.EAST);


	xslider = new XSliderPanel();
	//gbc.gridx = 0;
	//gbc.gridy = 2;
	//gbc.gridwidth = GridBagConstraints.RELATIVE;
	//gbl.setConstraints(xslider,gbc);
	temp.add(xslider, BorderLayout.SOUTH);



	// adds bottom panel with sliders:
	BottomPanel bottomPanel = new BottomPanel(this);//	add(new BottomPanel(), BorderLayout.SOUTH);
	//gbc.gridx = 0;
	//gbc.gridy = 3;
	//gbc.gridwidth = GridBagConstraints.REMAINDER;
	//gbc.gridheight = 1;
	//gbl.setConstraints(bottomPanel,gbc);
	bottomPanel.setMaximumSize(new Dimension(bottomPanel.getMaximumSize().width, bottomPanel.getPreferredSize().height));


	splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, temp, bottomPanel);
	splitPane.setOneTouchExpandable(true);
	splitPane.setResizeWeight(1.0);
	JPanel temp2 = new JPanel(new BorderLayout());
	temp2.add(splitPane);
	add(temp2);

	/* Uncomment to hide the sliders for controlling the graph panel */
	//addHierarchyListener(this);

	updatePainter();
    }
    

    /**
     * Set the split pane divider to the appropriate location when shown
     */
    public void hierarchyChanged(HierarchyEvent e) {
	if ( HierarchyEvent.SHOWING_CHANGED ==
	     ( e.getChangeFlags() & HierarchyEvent.SHOWING_CHANGED ) ) {
	    removeHierarchyListener( this );
	    splitPane.setDividerLocation( 1.0 );
	}
    }
    
    //    private Kaleidoscope getKaleidoscope() { return graphPanel.getGraphController().getKaleidoscope(); }

    private double getYSliderValue() { return yslider.ySlider.getValue(); }

    private void setYSliderValue(int i) { yslider.ySlider.setValue(i); }

    private double getXSliderValue() { return xslider.xSlider.getValue(); }

    private void setXSliderValue(int i) { xslider.xSlider.setValue(i); }

    public Object3DPainter getPainter() {
	return graphPanel.getPainter();
    }
        
    public void repaintThis() {
	repaint();
    }

    public void setPainter(Object3DPainter p) {
	graphPanel.setPainter(p);
    }
    
    public void setCameraController(CameraController p) {
	graphPanel.setCameraController(p);
    }    

    /** update camera position from phi, psi angles 
     * @TODO NOT YET IMPLEMENTED!
     */
    public void updateCamera(double theta, double phi, double psi, double dist) {
	graphPanel.getCameraController().updateCameraFromAngles(theta, phi, psi, dist);
    }
    
}

