package rnadesign.designapp.rnagui;

import java.awt.event.*;
import java.awt.*;
import javax.swing.*;
import java.awt.geom.*;
import java.util.*;


import rnadesign.rnacontrol.CameraController;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.SimpleCameraController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import tools3d.Camera;
import tools3d.SimpleCamera;
import tools3d.Matrix3D;
import tools3d.Vector3D;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DTools;
import controltools.ModelChangeListener;
import controltools.ModelChangeEvent;

/** center window: */
class GraphPanel extends JPanel implements KeyListener, MouseListener, MouseMotionListener, ModelChangeListener {

    Object3DGraphController graphController;
    
    GraphControlPanel controlPanel; // Added to allow access to sliders in control panel

    /**
     * 
     */
    private static final long serialVersionUID = -1285814574859329687L;

    public static final int ZOOM_BUTTON_MASK = MouseEvent.BUTTON1_DOWN_MASK |
	MouseEvent.BUTTON2_DOWN_MASK;
    public static final int MOVE_BUTTON_MASK = MouseEvent.BUTTON2_DOWN_MASK;
    public static final int ROTATE_BUTTON_MASK = MouseEvent.BUTTON1_DOWN_MASK;

    public static final int ROTATE_SELECTED_KEY = KeyEvent.VK_Y;
    public static final int TRANSLATE_SELECTED_KEY = KeyEvent.VK_T;
    public static final int CANCEL_SELECTED_KEY = KeyEvent.VK_C;

    public static final int X_AXIS_BUTTON_MASK = MouseEvent.BUTTON1_DOWN_MASK;
    public static final int Y_AXIS_BUTTON_MASK = MouseEvent.BUTTON2_DOWN_MASK;
    public static final int Z_AXIS_BUTTON_MASK = MouseEvent.BUTTON1_DOWN_MASK |
	MouseEvent.BUTTON2_DOWN_MASK;


    public static final int RESET_CAMERA = KeyEvent.VK_R;
    public static final int DESELECT_OBJECT = KeyEvent.VK_D;
    


    public enum SelectionMode {

	Translate, Rotate, None;
    }

    private SelectionMode selectionMode = SelectionMode.None;

    private static final double ZOOM_FACTOR = 0.01;
    private static final double MOVE_FACTOR = 1.0;
    private static final double ROTATE_FACTOR = 1.0;

  
    private double zoom = 1.0;

    private Point p;

    private final static double DISTANCE_THRESHOLD = 5;

    private Object3D selected;

    private Map<Point2D, Object3D> projectionMap = 
	new HashMap<Point2D, Object3D>();;

        
    Object3DPainter painter;

    Object3DPainter painterSave; // used when resetting
    
    GraphPanel(GraphControlPanel p, RnaGuiParameters params, Object3DGraphController graphController) {
	controlPanel = p;

	// setBackground(params.colorBackground);
	setBackground(Color.WHITE);
	setPreferredSize(new Dimension((int)params.xScreenMax, (int)params.yScreenMax));
	setBorder(BorderFactory.createLineBorder(Color.black));
	setFocusable(true);
	setFocusTraversalKeysEnabled(false);
	addKeyListener(this);
	addMouseListener(this);
	addMouseMotionListener(this);
	painter = new SimplePainter(params);
	painter.setCameraController(new SimpleCameraController(new SimpleCamera()));

	painterSave = new SimplePainter(params);
	SimpleCamera camera = new SimpleCamera();
	camera.setOrigin2D((int)(params.xScreenMax / 2), (int)(params.yScreenMax / 2));
	painterSave.setCameraController(new SimpleCameraController(camera));
	graphController.addModelChangeListener(this);
	
	this.graphController = graphController;


	getInputMap().put(KeyStroke.getKeyStroke(RESET_CAMERA, InputEvent.CTRL_MASK), "reset");
	getActionMap().put("reset", new ResetAction());
	getInputMap().put(KeyStroke.getKeyStroke(DESELECT_OBJECT, InputEvent.CTRL_MASK), "deselect");
	getActionMap().put("deselect", new DeselectAction());
	getInputMap().put(KeyStroke.getKeyStroke(ROTATE_SELECTED_KEY, InputEvent.CTRL_MASK), "rotate");
	getActionMap().put("rotate", new RotateAction());
	getInputMap().put(KeyStroke.getKeyStroke(TRANSLATE_SELECTED_KEY, InputEvent.CTRL_MASK), "translate");
	getActionMap().put("translate", new TranslateAction());
	getInputMap().put(KeyStroke.getKeyStroke(CANCEL_SELECTED_KEY, InputEvent.CTRL_MASK), "cancel");
	getActionMap().put("cancel", new CancelAction());
	getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "cancel");
	

       
	
    }

    private class RotateAction extends AbstractAction {
	public void actionPerformed(ActionEvent e) {
	    if(selectionMode == SelectionMode.Rotate)
		selectionMode = SelectionMode.None;
	    else
		selectionMode = SelectionMode.Rotate;

	    Object3D root = graphController.getGraph().getGraph();
	    projectionMap.clear();
	    buildProjectionMap(root, projectionMap);

	    setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));

	    
	}

    }

    private class TranslateAction extends AbstractAction {
	public void actionPerformed(ActionEvent e) {
	    if(selectionMode == SelectionMode.Translate)
		selectionMode = SelectionMode.None;
	    else
		selectionMode = SelectionMode.Translate;

	    Object3D root = graphController.getGraph().getGraph();
	    projectionMap.clear();
	    buildProjectionMap(root, projectionMap);

	    setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));

	}

    }


    private class CancelAction extends AbstractAction {
	public void actionPerformed(ActionEvent e) {
	    selectionMode = SelectionMode.None;
	    Object3D root = graphController.getGraph().getGraph();
	    projectionMap.clear();
	    buildProjectionMap(root, projectionMap);
	    setCursor(Cursor.getDefaultCursor());

	}

    }

    private class ResetAction extends AbstractAction {
	public void actionPerformed(ActionEvent e) {
	    getCameraController().getCamera().setPosition(new Vector3D(0.0, -10.0, 0.0));
	    getCameraController().getCamera().setOrientation(new Matrix3D(-1.0, 0.0, 0.0, 0.0, 1.0, -0.0, 0.0, 0.0, 1.0));
	    getCameraController().getCamera().setZoom(1.0);


	    Object3D root = graphController.getGraph().getGraph();
	    projectionMap.clear();
	    buildProjectionMap(root, projectionMap);

	    repaint();

	    
	}
	
    }


    private class DeselectAction extends AbstractAction {
	public void actionPerformed(ActionEvent e) {
	    graphController.getGraph().deselectCurrent();
	    selected = null;
	    
	}

    }


    public SelectionMode getSelectionMode() {

	return selectionMode;
    }

    public void modelChanged(ModelChangeEvent e) {
	Object3D root = graphController.getGraph().getGraph();
	projectionMap.clear();
	buildProjectionMap(root, projectionMap);
    }


    private void updateCamera(MouseEvent e) {

	double x = e.getX() - p.getX();
	double y = e.getY() - p.getY();
	y *= -1;
	this.p = e.getPoint();

	CameraController projector = painter.getCameraController();

	int masks = ZOOM_BUTTON_MASK | MOVE_BUTTON_MASK | ROTATE_BUTTON_MASK;

	if((e.getModifiersEx() & masks) == MOVE_BUTTON_MASK) { 
	    projector.moveCamera(x*MOVE_FACTOR, y*MOVE_FACTOR);
	}

	else if((e.getModifiersEx() & masks) == ROTATE_BUTTON_MASK) {
	    double phiChangeDeg = -y * ROTATE_FACTOR;
	    double thetaChangeDeg = -x * ROTATE_FACTOR;

	    projector.rotateCameraNorth(phiChangeDeg * RnaGuiConstants.DEG2RAD);
	    projector.rotateCameraWest(thetaChangeDeg * RnaGuiConstants.DEG2RAD);

	    controlPanel.movePhiSlider(-thetaChangeDeg);
	    controlPanel.moveThetaSlider(-phiChangeDeg);
	}

	else if((e.getModifiersEx() & masks) == ZOOM_BUTTON_MASK) {
	    //Vector3D position = projector.getCamera().getPosition();
	    //position.setZ(position.getZ() + y * ZOOM_FACTOR);
	    //projector.getCamera().setPosition(position);
	    
	    zoom += (y * ZOOM_FACTOR);
	    projector.setZoom(zoom);

	}

	repaint();

    }
    
    public void mouseDragged(MouseEvent e) {
	if(p == null) {
	    p = e.getPoint();
	    return;
	}
	
	if(selectionMode == SelectionMode.None) {
	    updateCamera(e);
	    return;
	}

	if(selectionMode == SelectionMode.Translate ||
	   selectionMode == SelectionMode.Rotate) {
	    updateObject(e);
	}


    }

    public void updateObject(MouseEvent e) {
        double xAxis = 0.0, yAxis = 0.0, zAxis = 0.0;
	double amount = 0.0;

	int masks = X_AXIS_BUTTON_MASK | Y_AXIS_BUTTON_MASK | Z_AXIS_BUTTON_MASK;

	if((e.getModifiersEx() & masks) == X_AXIS_BUTTON_MASK) {
	    xAxis = 1.0;
	}
	else if((e.getModifiersEx() & masks) == Y_AXIS_BUTTON_MASK) {
	    yAxis = 1.0;
	}
	else if((e.getModifiersEx() & masks) == Z_AXIS_BUTTON_MASK) {
	    zAxis = 1.0;
	}

	amount = e.getX() - p.getX();

	p = e.getPoint();

	if(graphController.getGraph().getSelectionRoot() != null) {
	    
	    if(selectionMode == SelectionMode.Translate) {
		if(xAxis == 1.0) {
		    xAxis = amount;
		}
		else if(yAxis == 1.0) {
		    yAxis = amount;
		}
		else if(zAxis == 1.0) {
		    zAxis = amount;
		}
		graphController.getGraph().translateSelected(new Vector3D(xAxis, yAxis, zAxis));
	    }
	    
	    else if(selectionMode == SelectionMode.Rotate) {
		graphController.getGraph().rotateSelected(xAxis, yAxis, zAxis, amount * Math.PI / 180);
		
	    }
	}

    }

    public void mouseMoved(MouseEvent e) {}


    public void mouseClicked(MouseEvent e) {

	

	if(selectionMode != SelectionMode.None) {
	    return;
	}

	// search through projection map to find the closest objects
	Set<Point2D> points = projectionMap.keySet();
	Point point = e.getPoint();
	Point2D closest = null;
	double distance = Double.MAX_VALUE;
	for(Point2D p : points) {
	    double newDistance = p.distance(point.x, point.y);
	    if(newDistance < distance && newDistance < DISTANCE_THRESHOLD) {
		closest = p;
		distance = newDistance;
	    }
	    
	}

	
	if(closest == null) {
	    graphController.getGraph().deselectCurrent();
	    selected = null;
	    return;
	}

	selected = projectionMap.get(closest);
	try {
	    String name = Object3DTools.getFullName(selected);
	    graphController.getGraph().selectByFullName(name);
	} catch(Object3DGraphControllerException ex) {
	    selected = null;
	    graphController.getGraph().deselectCurrent();
	}
	
	

	
    }


    private void buildProjectionMap(Object3D root, Map<Point2D, Object3D> map) {
	
	Point2D p = painter.getCameraController().project(root.getPosition());
	Object3D objectToAdd = root;
	if(map.get(p) != null) {
	    Object3D object = map.get(p);
	    Vector3D cameraPosition = painter.getCameraController().getCamera().getPosition();
	    double distanceOld = Math.abs(cameraPosition.getZ() - object.getPosition().getZ());
	    double distanceNew = Math.abs(cameraPosition.getZ() - root.getPosition().getZ());
	    
	    if(distanceOld < distanceNew)
		objectToAdd = object;
	    
	}
	
	map.put(p, objectToAdd);
	

	if(root.size() == 0)
	    return;

	for(int i = 0; i < root.size(); ++i)
	    buildProjectionMap(root.getChild(i), map);

    }

    


    public void mouseEntered(MouseEvent e) {
	requestFocusInWindow();
    }
    public void mouseExited(MouseEvent e) {
    }
    public void mousePressed(MouseEvent e) {
    
    }
    public void mouseReleased(MouseEvent e) {
	p = null;
	Object3D root = graphController.getGraph().getGraph();
	projectionMap.clear();
	buildProjectionMap(root, projectionMap);
    }

    public void keyReleased(KeyEvent e) {

    }

    public void keyTyped(KeyEvent e) {}

    public void keyPressed(KeyEvent e) {



    }

    public CameraController getCameraController() { 
	return painter.getCameraController();
    }

    public Object3DGraphController getGraphController() {
	return graphController;
    }

    public Object3DPainter getPainter() {
	return painter;
    }
    
    public void paint(Graphics g) {
	super.paint(g);
	painter.paint(g, graphController);
    } // public void paint
    

    /** resets to original camera position */
    public void resetPainter() {
	zoom = 1.0;
	painter.copy(painterSave);
    }

    public void setPainter(Object3DPainter p) {
	painter = p;
    }
    
    public void setCameraController(CameraController p) {
	painter.setCameraController(p);
    }
    
} // private class GraphPanel
