package rnadesign.designapp.rnagui;

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import rnadesign.rnacontrol.CameraController;
import rnadesign.rnacontrol.SimpleCameraController;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import tools3d.Camera;
import tools3d.SimpleCamera;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class GraphicsCommand extends AbstractCommand {
    
    public static final String COMMAND_NAME = "graphics";

    private Object3DGraphController controller;

    public GraphicsCommand(Object3DGraphController controller) {
	super(COMMAND_NAME);
	assert controller != null;
	this.controller = controller;
    }

    public Object cloneDeep() {
	GraphicsCommand command = new GraphicsCommand(controller);
	command.controller = this.controller;
	for (int i = 0; i < getParameterCount(); ++i) {
	    command.addParameter((Command)(getParameter(i).cloneDeep()));
	}
	return command;
    }

    private String helpOutput() {
	return "Launches graphics window. Correct usage: " + COMMAND_NAME;
    }

    public String getShortHelpText() { return helpOutput(); }

    public String getLongHelpText() {
	String helpText = "\"" + COMMAND_NAME + "\" Command Manual" +
	    NEWLINE + NEWLINE;
	helpText += "NAME" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "SYNOPSIS" + NEWLINE + "     " + COMMAND_NAME +
	    NEWLINE + NEWLINE;
	helpText += "DESCRIPTION" + NEWLINE +
	    "     Launches graphics in new window." + NEWLINE + NEWLINE;
	return helpText;
    }

    public void executeWithoutUndo() throws CommandExecutionException {
	assert (this.controller != null);
	prepareReadout();
        JFrame.setDefaultLookAndFeelDecorated(true);
	JFrame frame = new JFrame("NanoTiler");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	Container pane = frame.getContentPane();
	RnaGuiParameters params = new RnaGuiParameters();
        GraphControlPanel graphPanel = new GraphControlPanel(params, controller);
        CameraController xyProjector = new SimpleCameraController(new SimpleCamera());
	xyProjector.setOrigin2D((int)(params.xScreenMax / 2), (int)(params.yScreenMax / 2));
        graphPanel.setCameraController(xyProjector);
	GeometryPainterFactory geometryPainterFactory = new NanoTilerGeometryPainterFactory();
	GeometryPainter geometryPainter = 
	    geometryPainterFactory.createPainter(NanoTilerGeometryPainterFactory.SOLID_PAINTER);
	GeneralGeometryPainter newPainter = new GeneralGeometryPainter(params, geometryPainter, controller);
	newPainter.setCameraController(xyProjector); // keep same camera settings
	graphPanel.setPainter(newPainter);
	controller.addModelChangeListener(graphPanel.graphPanel);
	pane.add(graphPanel);
        frame.pack();
	frame.setVisible(true);
    }

    public Command execute() throws CommandExecutionException {
	executeWithoutUndo();
	return null;
    }

    private void prepareReadout() throws CommandExecutionException {
    }
    
}
