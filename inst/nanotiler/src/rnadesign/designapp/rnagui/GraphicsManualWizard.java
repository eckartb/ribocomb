package rnadesign.designapp.rnagui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import java.util.StringTokenizer;

import commandtools.CommandApplication;
import commandtools.Command;
import commandtools.AbstractCommand;
import commandtools.UnknownCommandException;
import commandtools.BadSyntaxException;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerConstants;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import tools3d.Vector3D;
import rnadesign.designapp.NanoTilerInterpreter;

import java.io.PrintStream;
import java.io.OutputStream;

import static rnadesign.designapp.PackageConstants.NEWLINE;

/* This class is for the Graphics help gui(this is hard coded
 * and not well organized in the addcomponents section).
 */
public class GraphicsManualWizard implements GeneralWizard {
    private static final int WIDTH = 500; 
    private static final int HEIGHT = 400;
    private boolean finished = false;

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    private JFrame frame = null;
    private JTextArea helpText;
    private JButton close;
    private JTabbedPane helpTabs;
    private JScrollPane scroller;

    private CommandApplication application;
    private NanoTilerInterpreter interpreter;
    private List<ActionListener> actionListeners = new ArrayList<ActionListener>();
    private Object3DGraphController graphController;

    public static final String FRAME_TITLE = "Graphics Manual";

    public GraphicsManualWizard(CommandApplication application){
	assert application != null;
	this.application = application;
	this.interpreter = (NanoTilerInterpreter)application.getInterpreter();
    }
    //used by the close button
    private class CloseListener implements ActionListener {

	public void actionPerformed(ActionEvent e) {
		frame.setVisible(false);
		frame = null;
		finished = true;
	}
    }

    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    public boolean isFinished() {
	return finished;
    }

    /** configures and launches the help gui frame*/
    public void launchWizard(Object3DGraphController graphController,
			     Component parentFrame) {
	if (graphController == null) {
	    log.info("GraphController received by launchWizard is null!");
	}
	this.graphController = graphController;
	frame = new JFrame(FRAME_TITLE);
	addComponents(frame);
	frame.pack();
	frame.setVisible(true);
    }
    //add the tabs and text to the gui
    public void addComponents(JFrame _frame) {
	Font bold = new Font("Bold",Font.BOLD,14);
	Font standard = new Font("Plain",Font.PLAIN,10);
	helpTabs = new JTabbedPane();
	helpTabs.setPreferredSize(new Dimension(WIDTH,HEIGHT));
	Container f = _frame.getContentPane();
	f.setLayout(new BorderLayout());

	//Help for the different types of views
	helpText = new JTextArea();
	helpText.setEditable(false);
	helpText.setPreferredSize(new Dimension(WIDTH,HEIGHT));
	helpText.append("Perspective:  \n");
	helpText.append("     View a molecule on screen and have the options\nof rotating, translating, and zooming in and out to change \nthe viewing perspective. The Perspective view is the most \nversatile of all the viewing options.\n\n");
	helpText.append("Top,Bottom,Right,Left,Front,Back:\n");
	helpText.append("     View a molecule from the chosen side with only \nthe options to zoom in and out and translate the image.\nThere is no rotating option in any like\nperspective mode.");        
	helpTabs.add("Views",helpText);

	//actions done using the mouse. Camera controls
	helpText = new JTextArea();
	helpText.setEditable(false);
	helpText.setPreferredSize(new Dimension(WIDTH,HEIGHT));
	helpText.append("Onscreen Mouse Clicking:\n");
	helpText.append("     If an object, such as an atom, is clicked on screen, \nthat object will become one of the selected objects. Click\nthe object again to deselect it.\n\n");
	helpText.append("Onscreen Mouse Dragging(left click):\n");
	helpText.append("     Dragging the mouse within the display area will translate\nthe molecule in the opposite direction to which the mouse\nwas dragged\n\n");
	helpText.append("Onscreen Mouse Dragging(right click):\n");
	helpText.append("     Dragging the mouse up or down within the display area \nwill rotate the camera clockwise and counterclockwise, \nrespectively.\n\n");
	helpText.append("Using the Buttons:\n");
	helpText.append("     In the upper right hand corner of the viewing panel(s), \nthere are three buttons which can be used to manipulate the camera\nview. The left button is used for basic translation.\nThe center button is used to rotate the camera.\nThe right button is used for zooming in and out.\nThe buttons are used by clicking the desired button \nand then dragging the mouse in a certain direction.\n\n"); 
	scroller = new JScrollPane(helpText);
	scroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
	helpTabs.add("Camera Controls",scroller);

	//Help for the different types of modes
	helpText = new JTextArea();
	helpText.setEditable(false);
	helpText.setPreferredSize(new Dimension(WIDTH,HEIGHT));
	helpText.append("Solid: \n");
	helpText.append("     Object are rendered with filled bodies.\n\n");
	helpText.append("Wireframe: \n");
	helpText.append("     Objects are rendered using wires to represent the body.\n\n");
	helpTabs.add("Modes",helpText);

	//Help for the different types of renderers
	helpText = new JTextArea();
	helpText.setEditable(false);
	helpText.setPreferredSize(new Dimension(WIDTH,HEIGHT));
	helpText.append("Ball: \n");
	helpText.append("     All objects within the current molecule are rendered using a\nspherical representation.\n\n");
	helpText.append("Ball and Stick: \n");
	helpText.append("     Typical ball and stick representation.\n\n");
	helpText.append("Cartoon: \n");
	helpText.append("     Renders the molecule as a cartoon model.\n\n");
	helpText.append("Ribbon: \n");
	helpText.append("     The backbone of the molecule is represented as a ribbon.\n\n");
	helpTabs.add("Renderers",helpText);

	//Help for the different types of color models
	helpText = new JTextArea();
	helpText.setEditable(false);
	helpText.setPreferredSize(new Dimension(WIDTH,HEIGHT));
	helpText.append("Strand: \n");
	helpText.append("     Each strand is colored an individual color. Strand 1=red.\nStrand n=blue.\n\n");
	helpText.append("Rainbow: \n");
	helpText.append("     The rainbow color model colors each strand a variety\nof viewing colors from red to blue.\n\n");
	helpText.append("Atom: \n");
	helpText.append("     The atom color model colors atoms based on their type. The colors\ncan be configured by the user to meet their needs \nand/or preferences.\n\n");
	helpText.append("White: \n");
	helpText.append("     Everthing is colored white.\n\n");
	helpTabs.add("Color Models",helpText);

	//help for the different shortcuts and hotkeys
	helpText = new JTextArea();
	helpText.setEditable(false);
	helpText.setPreferredSize(new Dimension(WIDTH,HEIGHT));
	helpText.append("Deselect                                     Ctrl + D\n");
	helpText.append("Reset Camera to Default                      Ctrl + R\n");
	helpText.append("Translation Mode(object must be selected)    Ctrl + T\n");
	helpText.append("Rotation Mode(object must be selected)       Ctrl + Y\n");
	helpText.append("Cancel                                       Ctrl + C or Esc\n");
	helpText.append("Find                                         Ctrl + F\n");
	helpText.append("Move Camera(translation)                     Left Click\n");                                  
	helpText.append("Rotate Camera                                Right Click\n");
	helpText.append("Zoom Camera                                  Left & Right Click\n");
	helpTabs.add("Shortcuts",helpText);

	//set up the close button for the help gui
	JPanel buttonPanel = new JPanel();
	close = new JButton("Close");
	close.addActionListener(new CloseListener());
	buttonPanel.add(close);

	f.add(buttonPanel,BorderLayout.SOUTH);
	f.add(helpTabs, BorderLayout.NORTH);
    }

}
