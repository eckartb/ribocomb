
package rnadesign.designapp.rnagui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerConstants;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import tools3d.Vector3D;
import tools3d.objects3d.Object3D;

/** lets user choose to partner object, inserts correspnding link into controller */
public class GraphitGridWizard implements GeneralWizard {
    private static final int COL_SIZE_DOUBLE = 8; 
    private static final int COL_SIZE_INT = 5; 
    private static final int COL_SIZE_NAME = 15; 
    private boolean finished = false;

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    private JFrame frame = null;
    private JTextField seqCharField;
    private JTextField axField;
    private JTextField ayField;
    private JTextField azField;
    private JTextField bxField;
    private JTextField byField;
    private JTextField bzField;
    private JTextField cxField;
    private JTextField cyField;
    private JTextField czField;

    private JTextField nameField;

    private JTextField nxField;
    private JTextField nyField;
    private JTextField nzField;
    // start position:
    private JTextField sxField;
    private JTextField syField;
    private JTextField szField;

    private JCheckBox addStemsBox;
    private JCheckBox onlyFirstPathBox;
    private JComboBox tilerChoice;

    private List<ActionListener> actionListeners = new ArrayList<ActionListener>();
    private Object3DGraphController graphController;
    private boolean addStemsFlag = false;
    private boolean onlyFirstPathMode = false;
    private double ax = 100.0;
    private double ay = 0.0;
    private double az = 0.0;
    private double bx = 0.0;
    private double by = 100.0;
    private double bz = 0.0;
    private double cx = 0.0;
    private double cy = 0.0;
    private double cz = 100.0;
    private double sx = 0.0;
    private double sy = 0.0;
    private double sz = 0.0;

    private int nx = 3;
    private int ny = 3;
    private int nz = 3;
    private int tilerAlgorithm = Object3DGraphControllerConstants.TILER_RANDOM;
    private String name = "grid";
    private String charString = "N"; // sequence character

    public static final String FRAME_TITLE = "Insert Cubic Grid Wizard";
    private static String[] tilerNames = {"random", "complete", 
					  "simple", "fragment"};

    private class DoneListener implements ActionListener {

	public void actionPerformed(ActionEvent e) {
		readOutValues();
		char sequenceChar = 'N';
		if (charString.length() > 0) {
		    sequenceChar = charString.charAt(0);
		}
		boolean addGridFlag = true; // TODO : make adding grid optional
		Object3D toBeClonedObject = null; // TODO avoid breaking of MVC layers, defined object
		try {
		    graphController.addGraphitGrid(new Vector3D(sx,sy,sz), ax, cz,
						   nx, ny, name, toBeClonedObject, addGridFlag, addStemsFlag, 
						   sequenceChar, onlyFirstPathMode,
						   tilerAlgorithm);
		}
		catch (Object3DGraphControllerException exc) {
		    JOptionPane.showMessageDialog(frame, "An error occured during the tiling algorithm: " + exc.getMessage());
		}
		frame.setVisible(false);
		frame = null;
		finished = true;
	}
    }

    private class CancelListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    frame.setVisible(false);
	    frame = null;
	    finished = true;
	}
    }

    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    public boolean isFinished() {
	return finished;
    }

    /** generates and inserts new Object3D according to user dialog */
    public void launchWizard(Object3DGraphController graphController,
			     Component parentFrame) {
	if (graphController == null) {
	    log.info("GraphController received by launchWizard is null!");
	}
	this.graphController = graphController;
	frame = new JFrame(FRAME_TITLE);
	addComponents(frame);
	frame.pack();
	frame.setVisible(true);
    }

    public void addComponents(JFrame _frame) {
	Container f = _frame.getContentPane();
	f.setLayout(new BorderLayout());
	JPanel center = new JPanel();
	center.setLayout(new BoxLayout(center, BoxLayout.Y_AXIS));

	JPanel topPanel = new JPanel();

	JPanel xPanel = new JPanel();
	JPanel yPanel = new JPanel();
	JPanel zPanel = new JPanel();
	JPanel sPanel = new JPanel();
	JPanel nPanel = new JPanel();
	JPanel checkPanel = new JPanel();
	
	addStemsBox = new JCheckBox("Add stems:",addStemsFlag);
	onlyFirstPathBox = new JCheckBox("Use first path:", onlyFirstPathMode);
	tilerChoice = new JComboBox(tilerNames);
	seqCharField = new JTextField(charString, 1);
	axField = new JTextField(""+ax, COL_SIZE_DOUBLE);
	ayField = new JTextField(""+ay, COL_SIZE_DOUBLE);
	azField = new JTextField(""+az, COL_SIZE_DOUBLE);
	bxField = new JTextField(""+bx, COL_SIZE_DOUBLE);
	byField = new JTextField(""+by, COL_SIZE_DOUBLE);
	bzField = new JTextField(""+bz, COL_SIZE_DOUBLE);
	cxField = new JTextField(""+cx, COL_SIZE_DOUBLE);
	cyField = new JTextField(""+cy, COL_SIZE_DOUBLE);
	czField = new JTextField(""+cz, COL_SIZE_DOUBLE);
	sxField = new JTextField(""+sx, COL_SIZE_DOUBLE);
	syField = new JTextField(""+sy, COL_SIZE_DOUBLE);
	szField = new JTextField(""+sz, COL_SIZE_DOUBLE);
	nxField = new JTextField(""+nx, COL_SIZE_INT);
	nyField = new JTextField(""+ny, COL_SIZE_INT);
	nzField = new JTextField(""+nz, COL_SIZE_INT);
	nameField = new JTextField(name, COL_SIZE_NAME);
	
	topPanel.add(new JLabel("Name:"));
	topPanel.add(nameField);
	topPanel.add(new JLabel("Sequence character:"));
	topPanel.add(seqCharField);
	xPanel.add(new JLabel("i: x:"));
	xPanel.add(axField);
	xPanel.add(new JLabel("y:"));
	xPanel.add(ayField);
	xPanel.add(new JLabel("z:"));
	xPanel.add(azField);

	yPanel.add(new JLabel("j: x:"));
	yPanel.add(bxField);
	yPanel.add(new JLabel("y:"));
	yPanel.add(byField);
	yPanel.add(new JLabel("z:"));
	yPanel.add(bzField);

	zPanel.add(new JLabel("k: x:"));
	zPanel.add(cxField);
	zPanel.add(new JLabel("y:"));
	zPanel.add(cyField);
	zPanel.add(new JLabel("z:"));
	zPanel.add(czField);

	sPanel.add(new JLabel("s: x:"));
	sPanel.add(sxField);
	sPanel.add(new JLabel("y:"));
	sPanel.add(syField);
	sPanel.add(new JLabel("z:"));
	sPanel.add(szField);

	nPanel.add(new JLabel("n: x:"));
	nPanel.add(nxField);
	nPanel.add(new JLabel("y:"));
	nPanel.add(nyField);
	nPanel.add(new JLabel("z:"));
	nPanel.add(nzField);

	checkPanel.add(addStemsBox);
	checkPanel.add(tilerChoice);
	checkPanel.add(onlyFirstPathBox);

	center.add(xPanel);
	center.add(yPanel);
	center.add(zPanel);
	center.add(sPanel);
	center.add(nPanel);
	center.add(checkPanel);

	JPanel bottomPanel = new JPanel();
	JButton button = new JButton("Cancel");
	button.addActionListener(new CancelListener());
	bottomPanel.add(button);
  	button = new JButton("Done");
	button.addActionListener(new DoneListener());
	bottomPanel.add(button);
	f.add(topPanel, BorderLayout.NORTH);
	f.add(bottomPanel, BorderLayout.SOUTH);
	f.add(center, BorderLayout.CENTER);
    }

    private void readOutValues() {
	addStemsFlag = addStemsBox.isSelected();
	onlyFirstPathMode = onlyFirstPathBox.isSelected();
	charString = seqCharField.getText().trim();
	ax = Double.parseDouble(axField.getText().trim());
	ay = Double.parseDouble(ayField.getText().trim());
	az = Double.parseDouble(azField.getText().trim());
	bx = Double.parseDouble(bxField.getText().trim());
	by = Double.parseDouble(byField.getText().trim());
	bz = Double.parseDouble(bzField.getText().trim());
	cx = Double.parseDouble(cxField.getText().trim());
	cy = Double.parseDouble(cyField.getText().trim());
	cz = Double.parseDouble(czField.getText().trim());
	sx = Double.parseDouble(sxField.getText().trim());
	sy = Double.parseDouble(syField.getText().trim());
	sz = Double.parseDouble(szField.getText().trim());
	nx = Integer.parseInt(nxField.getText().trim());
	ny = Integer.parseInt(nyField.getText().trim());
	nz = Integer.parseInt(nzField.getText().trim());
	name = nameField.getText().trim();
	tilerAlgorithm = translateTilerId(tilerChoice.getSelectedIndex());
    }

    private int translateTilerId(int id) {
	switch (id) {
	case 0: return Object3DGraphControllerConstants.TILER_RANDOM;
	case 1: return Object3DGraphControllerConstants.TILER_COMPLETE;
	case 2: return Object3DGraphControllerConstants.TILER_SIMPLE;
	case 3: return Object3DGraphControllerConstants.TILER_FRAGMENT;
	}
	log.severe("internal error in PlatonicSolidWizard!");
	return  Object3DGraphControllerConstants.TILER_RANDOM; // should never reach this point
    }

}
