package rnadesign.designapp.rnagui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.event.*;
import javax.swing.*;
import javax.swing.text.*;
import javax.swing.table.*;
import java.text.ParseException;
import java.io.*;
import java.util.*;
import commandtools.*;
import controltools.*;
import rnadesign.rnamodel.*;

import tools3d.objects3d.*;

import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DController;
import rnadesign.rnacontrol.Object3DGraphControllerConstants;
import rnadesign.designapp.AbstractDesigner;

/** lets user choose to partner object, inserts correspnding link into controller */
public class GrowGraphWizard implements GeneralWizard {

    public static final String FRAME_TITLE = "Grow Graph";

    private CommandApplication application;
    private static int version = 1;

    private final static Dimension PANEL_DIM = new Dimension(225, 225);
    private final static Dimension TABLE_DIM = new Dimension(200, 200);
    private static final int COL_SIZE_NAME = 25;

    private JTable junctionTable;
    private JTable addedJunctionTable;
    private JTable constraintTable;
    private AbstractTableModel junctionTableModel;
    private AbstractTableModel addedJunctionTableModel;
    private BranchSelector branchSelector1;
    private BranchSelector branchSelector2;
    private JButton generate;
    private JButton close;
    private JButton addConstraint;
    private JButton removeConstraint;
    private JFormattedTextField basePairs;

    private JLabel label;
    private JTextField rootField;
    private Vector<String> tree;
    private String[] allowedNames = RnaTools.getRnaClassNames();
    private String[] forbiddenNames;
    private JPanel treePanel;
    private JButton treeButton;
    private JList treeList;
    private JScrollPane treeScroll;

    private boolean steric = true, helices = true;
    private int generations = 1,maxConnections=1,maxBlocks=1;
    private double x = 0.0, y = 0.0, z = 0.0;
    private String root="root";

    private boolean useAdvanced = false;
    

    private ArrayList<Object3D> objects;
    private ArrayList<JunctionInfo> junctions;
    private ArrayList<JunctionInfo> addedJunctions;
    private ArrayList<JunctionInfo> availableJunctions;
    private ArrayList<ConstraintInfo> constraints;

    private class JunctionInfo {
	StrandJunction3D junction;
	int order;
	int index;
	String type;
    }

    private class ConstraintInfo {
	int j1, j2;
	int b1, b2;
	int bases;

    }


    private ArrayList<ActionListener> actionListeners = new ArrayList<ActionListener>();
    private boolean finished = false;
    private Object3DGraphController graphController;
    private JDialog frame;

    public GrowGraphWizard(CommandApplication application) {
	this.application = application;
    }

    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    public boolean isFinished() {
	return finished;
    }

    /** generates and inserts new Object3D according to user dialog */
    public void launchWizard(Object3DGraphController graphController,
			     Component parentFrame) {

	this.graphController = graphController;

	
	frame = new JDialog(parentFrame instanceof Frame ? (Frame) parentFrame : null, FRAME_TITLE, true);
	frame.addWindowListener(new WindowAdapter() {
		
		public void windowClosed(WindowEvent e) {
		    finished = true;
	       
		}

	    });

	getJunctionInfo();
	constraints = new ArrayList<ConstraintInfo>();
	availableJunctions = new ArrayList<JunctionInfo>();
	availableJunctions.addAll(junctions);

	frame.add(new RootChooser());

	frame.pack();
	frame.setVisible(true);

    }


    private void getJunctionInfo() {
	junctions = new ArrayList<JunctionInfo>();
	
	StrandJunctionDB db = graphController.getJunctionController().getKissingLoopDB();
	int numberJunctions = db.getJunctionCount();
	int junctionCount = 0;
	int order = 1;
	while(junctionCount != numberJunctions) {
	    StrandJunction3D[] j = db.getJunctions(order);
	    junctionCount += j.length;
	    for(int i = 0; i < j.length; ++i) {
		JunctionInfo info = new JunctionInfo();
		info.junction = j[i];
		info.order = order;
		info.index = i + 1;
		info.type = "k";
		junctions.add(info);
	    }

	    order++;

	}

	db = graphController.getJunctionController().getJunctionDB();
	numberJunctions = db.getJunctionCount();
	junctionCount = 0;
	order = 1;
	while(junctionCount != numberJunctions) {
	    StrandJunction3D[] j = db.getJunctions(order);
	    junctionCount += j.length;
	    for(int i = 0; i < j.length; ++i) {
		JunctionInfo info = new JunctionInfo();
		info.junction = j[i];
		info.order = order;
		info.index = i + 1;
		info.type = "j";
		junctions.add(info);
	    }

	    order++;

	}


    }

    private class RootChooser extends JPanel{

	RootChooser(){
	    JPanel centerPanel = new JPanel();
	    centerPanel.setLayout(new BorderLayout());
	    JPanel westCenterPanel = new JPanel();
	    westCenterPanel.setLayout(new BorderLayout());
	    label = new JLabel("Root: ");
	    westCenterPanel.add(label,BorderLayout.NORTH);
	    rootField = new JTextField(root, COL_SIZE_NAME);
	    westCenterPanel.add(rootField,BorderLayout.CENTER);
	    westCenterPanel.add(new TreePanel(rootField),BorderLayout.SOUTH);
	    centerPanel.add(westCenterPanel,BorderLayout.WEST);

	    JPanel buttonPanel = new JPanel();
	    JButton close = new JButton("Close");
	    close.addActionListener(new CloseWindowListener());
	    JButton next = new JButton("Next");
	    next.setEnabled(true);
	    next.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			root = rootField.getText().trim();

			frame.getContentPane().removeAll();
			frame.add(new BlockAdder());
			//frame.setVisible(false);
			frame.pack();
			//frame.setVisible(true);
			frame.repaint();

		    }
	     });
	    buttonPanel.add(close);
	    buttonPanel.add(next);
	    centerPanel.add(buttonPanel,BorderLayout.SOUTH);

	    add(centerPanel);
	}
    }
    private class TreePanel extends JPanel{
	public TreePanel(JTextField field){
		Object3DGraphController controller = ((AbstractDesigner)application).getGraphController();
		tree = controller.getGraph().getTree(allowedNames, forbiddenNames);
		this.setLayout(new BorderLayout());
		JPanel labelPanel = new JPanel();
		labelPanel.setLayout(new BoxLayout(labelPanel, BoxLayout.Y_AXIS));
		labelPanel.add(new JLabel("Tree:"));
		this.add(labelPanel);
	        
		treePanel = new JPanel();
		treeList = new JList(tree);
		treeList.addListSelectionListener(new SelectionListener(field));
		treeScroll = new JScrollPane(treeList);
		treeScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		treeScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		treeScroll.setPreferredSize(new Dimension(300,200));
		treePanel.add(treeScroll);
		this.add(treePanel, BorderLayout.SOUTH);
	}
    }
   /* links a textfield to a tree list.*/
    public class SelectionListener implements ListSelectionListener {
	JTextField field;
	public SelectionListener(JTextField field){
	    this.field = field;
	}

	public void valueChanged(ListSelectionEvent e) {
	    if (e.getValueIsAdjusting() == true) {
		String text = tree.get(e.getFirstIndex());
		text = text.substring(0, text.indexOf(" "));
		if (text.equals(field.getText())) {
		    text = tree.get(e.getLastIndex());
		    text = text.substring(0, text.indexOf(" "));
		}
		field.setText(text);
	    }
	}
    }
    private String getSelectionText() {
	Object3DGraphController graphController = ((AbstractDesigner)application).getGraphController();
	Object3DController gControl = graphController.getGraph();
	Object3D selection = gControl.getSelectionRoot();
	if (selection == null) {
	    return "";
	}
	String selectionName = selection.getName();
	Object3D curr = selection;
	while (curr.getParent() != null) {
	    curr = curr.getParent();
	    selectionName = curr.getName() + "." + selectionName;
	}
	return selectionName;
    }

    private class BlockAdder extends JPanel {

	BlockAdder() {

	    final JButton add = new JButton("Add");
	    final JButton remove = new JButton("Remove");
	    final JButton next = new JButton("Next");

	    JPanel leftRight = new JPanel();
	    leftRight.setLayout(new BoxLayout(leftRight, BoxLayout.X_AXIS));
	    
	    junctionTableModel = new JunctionTableModel(availableJunctions);
	    junctionTable = new JTable(junctionTableModel);
	    
	    junctionTable.setColumnSelectionAllowed(false);
	    junctionTable.getTableHeader().setReorderingAllowed(false);
	    ListSelectionModel lsm = junctionTable.getSelectionModel();
	    lsm.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	    lsm.addListSelectionListener(new ListSelectionListener() {
		    public void valueChanged(ListSelectionEvent e) {
			if(e.getValueIsAdjusting())
			    return;
			
			if(junctionTable.getSelectedRow() != -1) {
			    junctionTable.setRowSelectionInterval(junctionTable.getSelectedRow(), junctionTable.getSelectedRow());
			    add.setEnabled(true);
			}
			else {
			    add.setEnabled(false);
			}
			
		    }
		    
		});
	    
	    addedJunctions = new ArrayList<JunctionInfo>();
	    addedJunctionTableModel = new JunctionTableModel(addedJunctions);
	    addedJunctionTable = new JTable(addedJunctionTableModel);
	    addedJunctionTable.setColumnSelectionAllowed(false);
	    addedJunctionTable.getTableHeader().setReorderingAllowed(false);
	    lsm = addedJunctionTable.getSelectionModel();
	    lsm.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	    lsm.addListSelectionListener(new ListSelectionListener() {
		    public void valueChanged(ListSelectionEvent e) {
			if(e.getValueIsAdjusting())
			    return;
			
			if(addedJunctionTable.getSelectedRow() != -1) {
			    addedJunctionTable.setRowSelectionInterval(addedJunctionTable.getSelectedRow(), addedJunctionTable.getSelectedRow());
			    remove.setEnabled(true);
			}

			else
			    remove.setEnabled(false);
		    }
		});
	    

	    add.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			int row = junctionTable.getSelectedRow();
			if(row == -1)
			    return;
			
			JunctionInfo info = availableJunctions.remove(row);
			addedJunctions.add(info);

			if(availableJunctions.size() == 0)
			    add.setEnabled(false);
			
			junctionTableModel.fireTableRowsDeleted(row, row);
			addedJunctionTableModel.fireTableRowsInserted(addedJunctions.size() - 1, addedJunctions.size() -1);
			next.setEnabled(true);
			
		    }
		    
		});
	    
	    remove.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			int row = addedJunctionTable.getSelectedRow();
			if(row == -1)
			    return;
			
			JunctionInfo info = addedJunctions.remove(row);
			availableJunctions.add(info);

			if(addedJunctions.size() == 0) {
			    remove.setEnabled(false);
			    next.setEnabled(false);
			}
			
			addedJunctionTableModel.fireTableRowsDeleted(row, row);
			junctionTableModel.fireTableRowsInserted(addedJunctions.size() - 1, addedJunctions.size() -1);
			
			
		    }
		    
		});

	    remove.setEnabled(false);
	    add.setEnabled(false);
	    
	    
	    JPanel tempPanel = new JPanel(new BorderLayout());
	    tempPanel.add(new JLabel("Available"), BorderLayout.NORTH);
	    tempPanel.add(new JScrollPane(junctionTable), BorderLayout.CENTER);
	    tempPanel.add(add, BorderLayout.SOUTH);
	    tempPanel.setPreferredSize(PANEL_DIM);
	    
	    leftRight.add(tempPanel);
	    leftRight.add(new JSeparator(SwingConstants.VERTICAL));
	    
	    tempPanel = new JPanel(new BorderLayout());
	    tempPanel.add(new JLabel("Added"), BorderLayout.NORTH);
	    tempPanel.add(new JScrollPane(addedJunctionTable), BorderLayout.CENTER);
	    tempPanel.add(remove, BorderLayout.SOUTH);
	    tempPanel.setPreferredSize(PANEL_DIM);
	    
	    leftRight.add(tempPanel);
	    
	    setLayout(new BorderLayout());
	    add(leftRight, BorderLayout.CENTER);

	    JPanel temp = new JPanel();
	    temp.setLayout(new BoxLayout(temp, BoxLayout.X_AXIS));
	    temp.add(Box.createHorizontalGlue());
	    JButton close = new JButton("Close");
	    close.addActionListener(new CloseWindowListener());
	    temp.add(close);
	    temp.add(Box.createHorizontalStrut(10));
	    next.setEnabled(false);
	    next.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			frame.getContentPane().removeAll();
			frame.add(new AdvancedOptions());
			//frame.setVisible(false);
			frame.pack();
			//frame.setVisible(true);
			frame.repaint();

		    }

		});

	    temp.add(next);
	    add(temp, BorderLayout.SOUTH);
	    add(new JLabel("<html><h2>Add Junctions / Kissing Loops"), BorderLayout.NORTH);

	    
	    
	}


    }

    private class CloseWindowListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    frame.setVisible(false);
	    frame.dispose();

	}

    }

    private class GenerateListener implements ActionListener {
	 public void actionPerformed(ActionEvent e) {
	     StringBuffer command = new StringBuffer();
	     command.append("growgraph blocks=");
	     for(int i = 0; i < addedJunctions.size(); ++i) {
		 JunctionInfo j = addedJunctions.get(i);
		 command.append(j.type + "," + j.order + "," + j.index);
		 if(i < addedJunctions.size() - 1)
		     command.append(";");
		 
	     }
	     
	     /*command.append(" connect=");
	     for(int i = 0; i < constraints.size(); ++i) {
		 ConstraintInfo c = constraints.get(i);
		 command.append("" + (c.j1 + 1) + "," + (c.j2 + 1) + ",(hxend)" + c.b1 + ",(hxend)" + c.b2 + "," + c.bases);
		 if(i < constraints.size() - 1)
		     command.append(";");
		     }*/

	     if(useAdvanced) {
		 command.append(" root=" +root+ " ");
		 command.append("block-max=" + maxBlocks + " ");
		 command.append("conn-max=" +maxConnections+ " ");
		 command.append("pos=" + x + "," + y + "," + z + " ");
		 command.append("gen=" + generations + " ");

		 //command.append("steric=" + (steric ? "true" : "false") + " ");
		 //command.append("helices=" + (helices ? "true" : "false") + " ");

	     }
	     
	     System.out.println(command);
	     try {
		 application.runScriptLine(command.toString());
	     } catch(CommandException ex) {
		 JOptionPane.showMessageDialog(frame, ex.getMessage());
		 return;
	     }
	     
	     frame.setVisible(false);
	     frame.dispose();
	     
	 }
    }
	
    private class AdvancedOptions extends JPanel {

	JRadioButton helicesTrue, helicesFalse, stericTrue, stericFalse;
	JTextField xLocation, yLocation, zLocation, blockMax, connMax;
	JTextField generations;

	AdvancedOptions() {
	    
	    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

	    JPanel temp = new JPanel();
	    generations = new JTextField(10);
	    generations.setText("1");
	    temp.add(new JLabel("Generations:"));
	    temp.add(generations);

	    add(temp);

	    add(new JSeparator(SwingConstants.HORIZONTAL));
	    
	    temp = new JPanel();
	    helicesTrue = new JRadioButton("True");
	    helicesFalse = new JRadioButton("False");
	    ButtonGroup group = new ButtonGroup();
	    group.add(helicesTrue);
	    group.add(helicesFalse);
	    helicesTrue.setSelected(true);

	    helicesFalse.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			helices = false;
		    }
		});

	    helicesTrue.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			helices = true;
		    }
		    
		});

	    temp.add(new JLabel("Helices:"));
	    temp.add(helicesTrue);
	    temp.add(helicesFalse);

	    add(temp);
	    
	    add(new JSeparator(SwingConstants.HORIZONTAL));

	    temp = new JPanel();
	    temp.add(new JLabel("Connection Maximum: "));
	    connMax = new JTextField(10);
	    connMax.setText("1");
	    temp.add(connMax);

	    add(temp);

	    temp = new JPanel();
	    temp.add(new JLabel("Block Maximum: "));
	    blockMax = new JTextField(10);
	    blockMax.setText("1");
	    temp.add(blockMax);

	    add(temp);

	    add(new JSeparator(SwingConstants.HORIZONTAL));

	    temp = new JPanel();
	    temp.add(new JLabel("X:"));
	    xLocation = new JTextField(10);
	    xLocation.setText("0.0");
	    temp.add(xLocation);

	    add(temp);

	    temp = new JPanel();
	    temp.add(new JLabel("Y:"));
	    yLocation = new JTextField(10);
	    yLocation.setText("0.0");
	    temp.add(yLocation);

	    add(temp);

	    temp = new JPanel();
	    temp.add(new JLabel("Z:"));
	    zLocation = new JTextField(10);
	    zLocation.setText("0.0");
	    temp.add(zLocation);

	    add(temp);

	    add(new JSeparator(SwingConstants.HORIZONTAL));

	    temp = new JPanel();
	    stericTrue = new JRadioButton("True");
	    stericFalse = new JRadioButton("False");
	    group = new ButtonGroup();
	    group.add(stericTrue);
	    group.add(stericFalse);
	    stericTrue.setSelected(true);

	    stericFalse.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			steric = false;
		    }
		});

	    stericTrue.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			steric = true;
		    }
		    
		});

	    temp.add(new JLabel("Steric:"));
	    temp.add(stericTrue);
	    temp.add(stericFalse);

	    add(temp);

	    JPanel bottomPanel = new JPanel();
	    bottomPanel.setLayout(new BoxLayout(bottomPanel, BoxLayout.X_AXIS));
	    bottomPanel.add(Box.createHorizontalGlue());
	    JButton close = new JButton("Close");
	    close.addActionListener(new CloseWindowListener());
	    bottomPanel.add(close);

	    bottomPanel.add(Box.createHorizontalStrut(10));

	    JButton generate = new JButton("Generate");
	    generate.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			useAdvanced = true;
			try {
			    GrowGraphWizard.this.generations = Integer.parseInt(generations.getText());
			    if(GrowGraphWizard.this.generations < 1) {
				generations.setText("1");
				JOptionPane.showMessageDialog(frame, "Generations must be greater than or equal to 1");
				return;
			    }
			    maxConnections = Integer.parseInt(connMax.getText());
			    maxBlocks = Integer.parseInt(blockMax.getText());
			    x = Double.parseDouble(xLocation.getText());
			    y = Double.parseDouble(yLocation.getText());
			    z = Double.parseDouble(zLocation.getText());

			} catch(NumberFormatException ex) {
			    JOptionPane.showMessageDialog(frame, "Must enter a number");
			    return;

			}

			ActionListener listener = new GenerateListener();
			listener.actionPerformed(e);

		    }


		});

	    bottomPanel.add(generate);

	    add(bottomPanel);

	    
	    
	}

    }


    
    private class ConstraintTableModel extends AbstractTableModel {

	private String[] columns = {"Junction 1", "Branch 1", "Junction 2", "Branch 2", "Base Pairs"};
	public int getColumnCount() {
	    return columns.length;
	    
	}
	
	public int getRowCount() {
	    return constraints.size();
	    
	}
	
	
	public Object getValueAt(int rowIndex, int columnIndex) {
	    switch(columnIndex) {
	    case 0:
		return Object3DTools.getFullName(addedJunctions.get(constraints.get(rowIndex).j1).junction);
	    case 1:
		return constraints.get(rowIndex).b1;
	    case 2:
		return Object3DTools.getFullName(addedJunctions.get(constraints.get(rowIndex).j2).junction);
	    case 3:
		return constraints.get(rowIndex).b2;
	    case 4:
		return constraints.get(rowIndex).bases;
		
	    }
	    
	    return null;
	    
	}
	
	public String getColumnName(int columnIndex) {
	    return columns[columnIndex];
	    
	}

    }

    private class JunctionTableModel extends AbstractTableModel {

	private String[] columns = {"Type", "Name", "Order", "ID"};

	private ArrayList<JunctionInfo> list;

	public JunctionTableModel(ArrayList<JunctionInfo> list) {
	    this.list = list;
	}
	
	public int getColumnCount() {
	    return columns.length;
	    
	}
	
	public int getRowCount() {
	    return list.size();
	    
	}
	
	
	public Object getValueAt(int rowIndex, int columnIndex) {
	    switch(columnIndex) {
	    case 0:
		return list.get(rowIndex).type;
	    case 1:
		return Object3DTools.getFullName(list.get(rowIndex).junction);
	    case 2:
		return list.get(rowIndex).order;
	    case 3:
		return list.get(rowIndex).index;
		
	    }
	    
	    return null;
	    
	}
	
	public String getColumnName(int columnIndex) {
	    return columns[columnIndex];
	    
	}
	
    }

    private class BranchSelector extends JPanel {

	JTable junctionTable;
	JList branchTable;


	ListSelectionListener listener;

	BranchSelector(ListSelectionListener l) {
	    listener = l;
	    setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
	    junctionTable = new JTable(addedJunctionTableModel);
	    junctionTable.getTableHeader().setReorderingAllowed(false);
	    junctionTable.setColumnSelectionAllowed(false);
	    ListSelectionModel lsm = junctionTable.getSelectionModel();
	    lsm.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	    lsm.addListSelectionListener(new ListSelectionListener() {
		    public void valueChanged(ListSelectionEvent e) {
			if(e.getValueIsAdjusting())
			    return;

			addConstraint.setEnabled(false);

			if(junctionTable.getSelectedRow() != -1) {
			    junctionTable.setRowSelectionInterval(junctionTable.getSelectedRow(), junctionTable.getSelectedRow());
			    branchTable.setEnabled(true);
			    basePairs.setEnabled(true);
			    
			}

			else {
			    branchTable.setEnabled(false);
			    basePairs.setEnabled(false);
			    
			}

			StrandJunction3D j = addedJunctions.get(junctionTable.getSelectedRow()).junction;
			int size = j.getBranchCount();
			Object[] branches = new Object[size];
			for(int i = 0; i < size; ++i)
			    branches[i] = new Integer(i + 1);

			branchTable.setModel(new DefaultComboBoxModel(branches));
			branchTable.revalidate();



		    }

		});

	    JScrollPane pane = new JScrollPane(junctionTable);
	    pane.setPreferredSize(TABLE_DIM);
	    add(pane);
	    add(new JSeparator(SwingConstants.VERTICAL));
	    
	    branchTable = new JList();
	    branchTable.setPreferredSize(TABLE_DIM);
	    branchTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	    branchTable.addListSelectionListener(new ListSelectionListener() {
		    public void valueChanged(ListSelectionEvent e) {
			if(e.getValueIsAdjusting())
			    return;

			listener.valueChanged(e);

		    }

		});
	    branchTable.setEnabled(false);
	    
	    pane = new JScrollPane(branchTable);
	    pane.setPreferredSize(TABLE_DIM);
	    add(pane);

	   
	    


	    
	  


	}

	public void setEnabled(boolean b) {
	    super.setEnabled(b);
	    //junctionTable.setEnabled(b);
	    if(!b)
		junctionTable.removeRowSelectionInterval(0, addedJunctions.size());
	    branchTable.setEnabled(b);
	    basePairs.setEnabled(b);

	}

	public int getBasePairs() {

	    return (Integer) basePairs.getValue();
	}

	public int getJunction() {

	    if(junctionTable.getSelectedRow() == -1)
		return -1;

	    return junctionTable.getSelectedRow();
	}

	public int getBranch() {

	    if(branchTable.getSelectedIndex() == -1)
		return -1;

	    return branchTable.getSelectedIndex() + 1;
	}

    }
    
}
