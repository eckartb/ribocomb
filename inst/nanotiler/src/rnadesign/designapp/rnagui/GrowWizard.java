package rnadesign.designapp.rnagui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.event.*;
import javax.swing.*;
import javax.swing.text.*;
import javax.swing.table.*;
import java.text.ParseException;
import java.io.*;
import java.util.*;
import commandtools.*;
import controltools.*;
import rnadesign.rnamodel.*;

import tools3d.objects3d.*;

import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerConstants;

/** lets user choose to partner object, inserts correspnding link into controller */
public class GrowWizard implements GeneralWizard {

    public static final String FRAME_TITLE = "Grow";

    private CommandApplication application;
    private static int version = 1;

    private final static Dimension PANEL_DIM = new Dimension(225, 225);
    private final static Dimension TABLE_DIM = new Dimension(200, 200);

    private JTable junctionTable;
    private JTable addedJunctionTable;
    private JTable constraintTable;
    private AbstractTableModel junctionTableModel;
    private AbstractTableModel addedJunctionTableModel;
    private BranchSelector branchSelector1;
    private BranchSelector branchSelector2;
    private JButton generate;
    private JButton close;
    private JButton addConstraint;
    private JButton removeConstraint;
    private JFormattedTextField basePairs;

    private boolean steric = true, helices = true;
    private int generations = 1;
    private double x = 0.0, y = 0.0, z = 0.0;

    private boolean useAdvanced = false;
    

    private ArrayList<Object3D> objects;
    private ArrayList<JunctionInfo> junctions;
    private ArrayList<JunctionInfo> addedJunctions;
    private ArrayList<JunctionInfo> availableJunctions;
    private ArrayList<ConstraintInfo> constraints;

    private class JunctionInfo {
	StrandJunction3D junction;
	int order;
	int index;
	String type;
    }

    private class ConstraintInfo {
	int j1, j2;
	int b1, b2;
	int bases;

    }


    private ArrayList<ActionListener> actionListeners = new ArrayList<ActionListener>();
    private boolean finished = false;
    private Object3DGraphController graphController;
    private JDialog frame;

    public GrowWizard(CommandApplication application) {
	this.application = application;
    }

    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    public boolean isFinished() {
	return finished;
    }

    /** generates and inserts new Object3D according to user dialog */
    public void launchWizard(Object3DGraphController graphController,
			     Component parentFrame) {

	this.graphController = graphController;

	
	frame = new JDialog(parentFrame instanceof Frame ? (Frame) parentFrame : null, FRAME_TITLE, true);
	frame.addWindowListener(new WindowAdapter() {
		
		public void windowClosed(WindowEvent e) {
		    finished = true;
	       
		}

	    });

	getJunctionInfo();
	constraints = new ArrayList<ConstraintInfo>();
	availableJunctions = new ArrayList<JunctionInfo>();
	availableJunctions.addAll(junctions);

	frame.add(new BlockAdder());

	frame.pack();
	frame.setVisible(true);

    }


    private void getJunctionInfo() {
	junctions = new ArrayList<JunctionInfo>();
	
	StrandJunctionDB db = graphController.getJunctionController().getKissingLoopDB();
	int numberJunctions = db.getJunctionCount();
	int junctionCount = 0;
	int order = 1;
	while(junctionCount != numberJunctions) {
	    StrandJunction3D[] j = db.getJunctions(order);
	    junctionCount += j.length;
	    for(int i = 0; i < j.length; ++i) {
		JunctionInfo info = new JunctionInfo();
		info.junction = j[i];
		info.order = order;
		info.index = i + 1;
		info.type = "k";
		junctions.add(info);
	    }

	    order++;

	}

	db = graphController.getJunctionController().getJunctionDB();
	numberJunctions = db.getJunctionCount();
	junctionCount = 0;
	order = 1;
	while(junctionCount != numberJunctions) {
	    StrandJunction3D[] j = db.getJunctions(order);
	    junctionCount += j.length;
	    for(int i = 0; i < j.length; ++i) {
		JunctionInfo info = new JunctionInfo();
		info.junction = j[i];
		info.order = order;
		info.index = i + 1;
		info.type = "j";
		junctions.add(info);
	    }

	    order++;

	}


    }

    private class BlockAdder extends JPanel {

	BlockAdder() {

	    final JButton add = new JButton("Add");
	    final JButton remove = new JButton("Remove");
	    final JButton next = new JButton("Next");

	    JPanel leftRight = new JPanel();
	    leftRight.setLayout(new BoxLayout(leftRight, BoxLayout.X_AXIS));
	    
	    junctionTableModel = new JunctionTableModel(availableJunctions);
	    junctionTable = new JTable(junctionTableModel);
	    
	    junctionTable.setColumnSelectionAllowed(false);
	    junctionTable.getTableHeader().setReorderingAllowed(false);
	    ListSelectionModel lsm = junctionTable.getSelectionModel();
	    lsm.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	    lsm.addListSelectionListener(new ListSelectionListener() {
		    public void valueChanged(ListSelectionEvent e) {
			if(e.getValueIsAdjusting())
			    return;
			
			if(junctionTable.getSelectedRow() != -1) {
			    junctionTable.setRowSelectionInterval(junctionTable.getSelectedRow(), junctionTable.getSelectedRow());
			    add.setEnabled(true);
			}
			else {
			    add.setEnabled(false);
			}
			
		    }
		    
		});
	    
	    addedJunctions = new ArrayList<JunctionInfo>();
	    addedJunctionTableModel = new JunctionTableModel(addedJunctions);
	    addedJunctionTable = new JTable(addedJunctionTableModel);
	    addedJunctionTable.setColumnSelectionAllowed(false);
	    addedJunctionTable.getTableHeader().setReorderingAllowed(false);
	    lsm = addedJunctionTable.getSelectionModel();
	    lsm.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	    lsm.addListSelectionListener(new ListSelectionListener() {
		    public void valueChanged(ListSelectionEvent e) {
			if(e.getValueIsAdjusting())
			    return;
			
			if(addedJunctionTable.getSelectedRow() != -1) {
			    addedJunctionTable.setRowSelectionInterval(addedJunctionTable.getSelectedRow(), addedJunctionTable.getSelectedRow());
			    remove.setEnabled(true);
			}

			else
			    remove.setEnabled(false);
		    }
		});
	    

	    add.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			int row = junctionTable.getSelectedRow();
			if(row == -1)
			    return;
			
			JunctionInfo info = availableJunctions.remove(row);
			addedJunctions.add(info);

			if(availableJunctions.size() == 0)
			    add.setEnabled(false);
			
			junctionTableModel.fireTableRowsDeleted(row, row);
			addedJunctionTableModel.fireTableRowsInserted(addedJunctions.size() - 1, addedJunctions.size() -1);
			next.setEnabled(true);
			
		    }
		    
		});
	    
	    remove.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			int row = addedJunctionTable.getSelectedRow();
			if(row == -1)
			    return;
			
			JunctionInfo info = addedJunctions.remove(row);
			availableJunctions.add(info);

			if(addedJunctions.size() == 0) {
			    remove.setEnabled(false);
			    next.setEnabled(false);
			}
			
			addedJunctionTableModel.fireTableRowsDeleted(row, row);
			junctionTableModel.fireTableRowsInserted(addedJunctions.size() - 1, addedJunctions.size() -1);
			
			
		    }
		    
		});

	    remove.setEnabled(false);
	    add.setEnabled(false);
	    
	    
	    JPanel tempPanel = new JPanel(new BorderLayout());
	    tempPanel.add(new JLabel("Available"), BorderLayout.NORTH);
	    tempPanel.add(new JScrollPane(junctionTable), BorderLayout.CENTER);
	    tempPanel.add(add, BorderLayout.SOUTH);
	    tempPanel.setPreferredSize(PANEL_DIM);
	    
	    leftRight.add(tempPanel);
	    leftRight.add(new JSeparator(SwingConstants.VERTICAL));
	    
	    tempPanel = new JPanel(new BorderLayout());
	    tempPanel.add(new JLabel("Added"), BorderLayout.NORTH);
	    tempPanel.add(new JScrollPane(addedJunctionTable), BorderLayout.CENTER);
	    tempPanel.add(remove, BorderLayout.SOUTH);
	    tempPanel.setPreferredSize(PANEL_DIM);
	    
	    leftRight.add(tempPanel);
	    
	    setLayout(new BorderLayout());
	    add(leftRight, BorderLayout.CENTER);

	    JPanel temp = new JPanel();
	    temp.setLayout(new BoxLayout(temp, BoxLayout.X_AXIS));
	    temp.add(Box.createHorizontalGlue());
	    JButton close = new JButton("Close");
	    close.addActionListener(new CloseWindowListener());
	    temp.add(close);
	    temp.add(Box.createHorizontalStrut(10));
	    next.setEnabled(false);
	    next.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			frame.getContentPane().removeAll();
			frame.add(new ConstraintAdder());
			//frame.setVisible(false);
			frame.pack();
			//frame.setVisible(true);
			frame.repaint();

		    }

		});

	    temp.add(next);
	    add(temp, BorderLayout.SOUTH);
	    add(new JLabel("<html><h2>Add Junctions / Kissing Loops"), BorderLayout.NORTH);

	    
	    
	}


    }

    private class ConstraintAdder extends JPanel implements ListSelectionListener {
	public void valueChanged(ListSelectionEvent e) {

	    if(branchSelector1.getBranch() != -1 && branchSelector2.getBranch() != -1)
		addConstraint.setEnabled(true);

	}

	ConstraintAdder() {

	    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

	    branchSelector1 = new BranchSelector(this);
	    branchSelector2 = new BranchSelector(this);


	    
	    
	    constraintTable = new JTable(new ConstraintTableModel());
	    constraintTable.setColumnSelectionAllowed(false);
	    constraintTable.getTableHeader().setReorderingAllowed(false);
	    ListSelectionModel lsm = constraintTable.getSelectionModel();
	    lsm.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	    lsm.addListSelectionListener(new ListSelectionListener() {
		    public void valueChanged(ListSelectionEvent e) {
			if(e.getValueIsAdjusting())
			    return;

			if(constraintTable.getSelectedRow() != -1) {
			    constraintTable.setRowSelectionInterval(constraintTable.getSelectedRow(), constraintTable.getSelectedRow());
			    removeConstraint.setEnabled(true);
			}
			else {
			    removeConstraint.setEnabled(false);
			}
		    }

		});

	    JScrollPane pane = new JScrollPane(constraintTable);
	    pane.setPreferredSize(new Dimension(600, 200));

	    add(new JLabel("<html><h2 align=\"center\">Added Connections"));
	    add(Box.createVerticalStrut(10));
	    add(pane);
	    
	    
	    
	    JPanel selectorPanel = new JPanel();
	    selectorPanel.setLayout(new BoxLayout(selectorPanel, BoxLayout.X_AXIS));
	    
	    JPanel selectorPanelTemp = new JPanel();
	    selectorPanelTemp.setLayout(new BoxLayout(selectorPanelTemp, BoxLayout.Y_AXIS));
	    
	    
	    selectorPanelTemp.add(branchSelector1);
	    selectorPanelTemp.add(branchSelector2);
	    
	    selectorPanel.add(selectorPanelTemp);


	    basePairs = new JFormattedTextField(new JFormattedTextField.AbstractFormatter() {
		    public Object stringToValue(String s) throws ParseException {
			try {
			    return Integer.parseInt(s);
			} catch(NumberFormatException e) {
			    throw new ParseException(e.getMessage(), 0);
			}
		    }

		    public String valueToString(Object o) throws ParseException {
			if(o instanceof Number) {
			    Number n = (Number) o;
			    return "" + n.intValue();
			}

			throw new ParseException("Not a number", 0);
		    }

		    public DocumentFilter getDocumentFilter() {
			return new DocumentFilter() {
				public void insertString(DocumentFilter.FilterBypass fb,
							 int offset,
							 String string,
							 AttributeSet attr) throws BadLocationException {
				    try {
					Integer.parseInt(string);
					super.insertString(fb, offset, string, attr);

				    } catch(NumberFormatException e) {
					

				    }



				}
			    };

		    }


		});

	    basePairs.setValue(new Integer(0));
	    basePairs.setEnabled(false);
	    basePairs.setColumns(10);
	    JPanel basePairsPanel = new JPanel();
	    basePairsPanel.add(new JLabel("Base Pairs:"));
	    basePairsPanel.add(basePairs);

	    selectorPanel.add(Box.createHorizontalStrut(15));
	    selectorPanel.add(basePairsPanel);


	    selectorPanel.add(new JSeparator(SwingConstants.VERTICAL));
	    
	    JPanel buttonPanel = new JPanel();
	    buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));
	    
	    addConstraint = new JButton("Add");
	    addConstraint.setEnabled(false);
	    addConstraint.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			ConstraintInfo info = new ConstraintInfo();
			int j1 = branchSelector1.getJunction();
			int j2 = branchSelector2.getJunction();
			int b1 = branchSelector1.getBranch();
			int b2 = branchSelector2.getBranch();
			int bases = (Integer) basePairs.getValue();
			if(j1 == -1 || j2 == -1 || b1 == -1 || b2 == -1) {
			    JOptionPane.showMessageDialog(frame, "Cannot add constraint");
			    return;
			}

			info.j1 = j1;
			info.j2 = j2;
			info.b1 = b1;
			info.b2 = b2;
			info.bases = bases;
			constraints.add(info);
			AbstractTableModel model = (AbstractTableModel) constraintTable.getModel();
			model.fireTableRowsInserted(constraints.size() - 1, constraints.size() - 1);
			generate.setEnabled(true);

		    }
		});

	    removeConstraint = new JButton("Remove");
	    removeConstraint.setEnabled(false);
	    removeConstraint.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			int row = constraintTable.getSelectedRow();
			if(row == -1)
			    return;

			constraints.remove(row);
			if(constraints.size() == 0) {
			    removeConstraint.setEnabled(false);
			    generate.setEnabled(false);
			}
			
			AbstractTableModel model = (AbstractTableModel) constraintTable.getModel();
			model.fireTableRowsDeleted(row, row);

		    }

		});

	    buttonPanel.add(addConstraint);
	    buttonPanel.add(Box.createVerticalStrut(10));
	    buttonPanel.add(removeConstraint);

	    selectorPanel.add(buttonPanel);

	    add(Box.createVerticalStrut(10));
	    add(new JLabel("<html><h2 align=\"center\">Create / Add / Remove Connections"));
	    add(Box.createVerticalStrut(10));
	    add(selectorPanel);


	    JPanel bottomPanel = new JPanel();
	    bottomPanel.setLayout(new BoxLayout(bottomPanel, BoxLayout.X_AXIS));
	    bottomPanel.add(Box.createHorizontalGlue());
	    generate = new JButton("Generate");
	    generate.addActionListener(new GenerateListener());
	    JButton close = new JButton("Close");
	    close.addActionListener(new CloseWindowListener());
	    bottomPanel.add(close);
	    bottomPanel.add(Box.createHorizontalStrut(10));
	    bottomPanel.add(generate);
	    generate.setEnabled(false);
	    JButton next = new JButton("Next");
	    next.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			frame.getContentPane().removeAll();
			frame.add(new AdvancedOptions());
			frame.pack();
			frame.repaint();
		    }
		});

	    bottomPanel.add(Box.createHorizontalStrut(10));
	    bottomPanel.add(next);

	    add(bottomPanel);
	}

    }

    private class CloseWindowListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    frame.setVisible(false);
	    frame.dispose();

	}

    }

    private class GenerateListener implements ActionListener {
	 public void actionPerformed(ActionEvent e) {
	     StringBuffer command = new StringBuffer();
	     command.append("grow blocks=");
	     for(int i = 0; i < addedJunctions.size(); ++i) {
		 JunctionInfo j = addedJunctions.get(i);
		 command.append(j.type + "," + j.order + "," + j.index);
		 if(i < addedJunctions.size() - 1)
		     command.append(";");
		 
	     }
	     
	     command.append(" connect=");
	     for(int i = 0; i < constraints.size(); ++i) {
		 ConstraintInfo c = constraints.get(i);
		 command.append("" + (c.j1 + 1) + "," + (c.j2 + 1) + ",(hxend)" + c.b1 + ",(hxend)" + c.b2 + "," + c.bases);
		 if(i < constraints.size() - 1)
		     command.append(";");
	     }

	     if(useAdvanced) {
		 command.append(" pos=" + x + "," + y + "," + z + " ");
		 command.append("gen=" + generations + " ");
		 command.append("steric=" + (steric ? "true" : "false") + " ");
		 command.append("helices=" + (helices ? "true" : "false") + " ");

	     }
	     
	     System.out.println(command);
	     try {
		 application.runScriptLine(command.toString());
	     } catch(CommandException ex) {
		 JOptionPane.showMessageDialog(frame, ex.getMessage());
		 return;
	     }
	     
	     frame.setVisible(false);
	     frame.dispose();
	     
	 }
    }
	
    private class AdvancedOptions extends JPanel {

	JRadioButton helicesTrue, helicesFalse, stericTrue, stericFalse;
	JTextField xLocation, yLocation, zLocation;
	JTextField generations;

	AdvancedOptions() {
	    
	    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

	    JPanel temp = new JPanel();
	    generations = new JTextField(10);
	    generations.setText("1");
	    temp.add(new JLabel("Generations:"));
	    temp.add(generations);

	    add(temp);

	    add(new JSeparator(SwingConstants.HORIZONTAL));
	    
	    temp = new JPanel();
	    helicesTrue = new JRadioButton("True");
	    helicesFalse = new JRadioButton("False");
	    ButtonGroup group = new ButtonGroup();
	    group.add(helicesTrue);
	    group.add(helicesFalse);
	    helicesTrue.setSelected(true);

	    helicesFalse.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			helices = false;
		    }
		});

	    helicesTrue.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			helices = true;
		    }
		    
		});

	    temp.add(new JLabel("Helices:"));
	    temp.add(helicesTrue);
	    temp.add(helicesFalse);

	    add(temp);
	    
	    add(new JSeparator(SwingConstants.HORIZONTAL));

	    temp = new JPanel();
	    temp.add(new JLabel("X:"));
	    xLocation = new JTextField(10);
	    xLocation.setText("0.0");
	    temp.add(xLocation);

	    add(temp);

	    temp = new JPanel();
	    temp.add(new JLabel("Y:"));
	    yLocation = new JTextField(10);
	    yLocation.setText("0.0");
	    temp.add(yLocation);

	    add(temp);

	    temp = new JPanel();
	    temp.add(new JLabel("Z:"));
	    zLocation = new JTextField(10);
	    zLocation.setText("0.0");
	    temp.add(zLocation);

	    add(temp);

	    add(new JSeparator(SwingConstants.HORIZONTAL));

	    temp = new JPanel();
	    stericTrue = new JRadioButton("True");
	    stericFalse = new JRadioButton("False");
	    group = new ButtonGroup();
	    group.add(stericTrue);
	    group.add(stericFalse);
	    stericTrue.setSelected(true);

	    stericFalse.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			steric = false;
		    }
		});

	    stericTrue.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			steric = true;
		    }
		    
		});

	    temp.add(new JLabel("Steric:"));
	    temp.add(stericTrue);
	    temp.add(stericFalse);

	    add(temp);

	    JPanel bottomPanel = new JPanel();
	    bottomPanel.setLayout(new BoxLayout(bottomPanel, BoxLayout.X_AXIS));
	    bottomPanel.add(Box.createHorizontalGlue());
	    JButton close = new JButton("Close");
	    close.addActionListener(new CloseWindowListener());
	    bottomPanel.add(close);

	    bottomPanel.add(Box.createHorizontalStrut(10));

	    JButton generate = new JButton("Generate");
	    generate.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			useAdvanced = true;
			try {
			    GrowWizard.this.generations = Integer.parseInt(generations.getText());
			    if(GrowWizard.this.generations < 1) {
				generations.setText("1");
				JOptionPane.showMessageDialog(frame, "Generations must be greater than or equal to 1");
				return;
			    }

			    x = Double.parseDouble(xLocation.getText());
			    y = Double.parseDouble(yLocation.getText());
			    z = Double.parseDouble(zLocation.getText());

			} catch(NumberFormatException ex) {
			    JOptionPane.showMessageDialog(frame, "Must enter a number");
			    return;

			}

			ActionListener listener = new GenerateListener();
			listener.actionPerformed(e);

		    }


		});

	    bottomPanel.add(generate);

	    add(bottomPanel);

	    
	    
	}

    }


    
    private class ConstraintTableModel extends AbstractTableModel {

	private String[] columns = {"Junction 1", "Branch 1", "Junction 2", "Branch 2", "Base Pairs"};
	public int getColumnCount() {
	    return columns.length;
	    
	}
	
	public int getRowCount() {
	    return constraints.size();
	    
	}
	
	
	public Object getValueAt(int rowIndex, int columnIndex) {
	    switch(columnIndex) {
	    case 0:
		return Object3DTools.getFullName(addedJunctions.get(constraints.get(rowIndex).j1).junction);
	    case 1:
		return constraints.get(rowIndex).b1;
	    case 2:
		return Object3DTools.getFullName(addedJunctions.get(constraints.get(rowIndex).j2).junction);
	    case 3:
		return constraints.get(rowIndex).b2;
	    case 4:
		return constraints.get(rowIndex).bases;
		
	    }
	    
	    return null;
	    
	}
	
	public String getColumnName(int columnIndex) {
	    return columns[columnIndex];
	    
	}

    }

    private class JunctionTableModel extends AbstractTableModel {

	private String[] columns = {"Type", "Name", "Order", "ID"};

	private ArrayList<JunctionInfo> list;

	public JunctionTableModel(ArrayList<JunctionInfo> list) {
	    this.list = list;
	}
	
	public int getColumnCount() {
	    return columns.length;
	    
	}
	
	public int getRowCount() {
	    return list.size();
	    
	}
	
	
	public Object getValueAt(int rowIndex, int columnIndex) {
	    switch(columnIndex) {
	    case 0:
		return list.get(rowIndex).type;
	    case 1:
		return Object3DTools.getFullName(list.get(rowIndex).junction);
	    case 2:
		return list.get(rowIndex).order;
	    case 3:
		return list.get(rowIndex).index;
		
	    }
	    
	    return null;
	    
	}
	
	public String getColumnName(int columnIndex) {
	    return columns[columnIndex];
	    
	}
	
    }

    private class BranchSelector extends JPanel {

	JTable junctionTable;
	JList branchTable;


	ListSelectionListener listener;

	BranchSelector(ListSelectionListener l) {
	    listener = l;
	    setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
	    junctionTable = new JTable(addedJunctionTableModel);
	    junctionTable.getTableHeader().setReorderingAllowed(false);
	    junctionTable.setColumnSelectionAllowed(false);
	    ListSelectionModel lsm = junctionTable.getSelectionModel();
	    lsm.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	    lsm.addListSelectionListener(new ListSelectionListener() {
		    public void valueChanged(ListSelectionEvent e) {
			if(e.getValueIsAdjusting())
			    return;

			addConstraint.setEnabled(false);

			if(junctionTable.getSelectedRow() != -1) {
			    junctionTable.setRowSelectionInterval(junctionTable.getSelectedRow(), junctionTable.getSelectedRow());
			    branchTable.setEnabled(true);
			    basePairs.setEnabled(true);
			    
			}

			else {
			    branchTable.setEnabled(false);
			    basePairs.setEnabled(false);
			    
			}

			StrandJunction3D j = addedJunctions.get(junctionTable.getSelectedRow()).junction;
			int size = j.getBranchCount();
			Object[] branches = new Object[size];
			for(int i = 0; i < size; ++i)
			    branches[i] = new Integer(i + 1);

			branchTable.setModel(new DefaultComboBoxModel(branches));
			branchTable.revalidate();



		    }

		});

	    JScrollPane pane = new JScrollPane(junctionTable);
	    pane.setPreferredSize(TABLE_DIM);
	    add(pane);
	    add(new JSeparator(SwingConstants.VERTICAL));
	    
	    branchTable = new JList();
	    branchTable.setPreferredSize(TABLE_DIM);
	    branchTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	    branchTable.addListSelectionListener(new ListSelectionListener() {
		    public void valueChanged(ListSelectionEvent e) {
			if(e.getValueIsAdjusting())
			    return;

			listener.valueChanged(e);

		    }

		});
	    branchTable.setEnabled(false);
	    
	    pane = new JScrollPane(branchTable);
	    pane.setPreferredSize(TABLE_DIM);
	    add(pane);

	   
	    


	    
	  


	}

	public void setEnabled(boolean b) {
	    super.setEnabled(b);
	    //junctionTable.setEnabled(b);
	    if(!b)
		junctionTable.removeRowSelectionInterval(0, addedJunctions.size());
	    branchTable.setEnabled(b);
	    basePairs.setEnabled(b);

	}

	public int getBasePairs() {

	    return (Integer) basePairs.getValue();
	}

	public int getJunction() {

	    if(junctionTable.getSelectedRow() == -1)
		return -1;

	    return junctionTable.getSelectedRow();
	}

	public int getBranch() {

	    if(branchTable.getSelectedIndex() == -1)
		return -1;

	    return branchTable.getSelectedIndex() + 1;
	}

    }
    
}
