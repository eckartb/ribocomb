package rnadesign.designapp.rnagui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.event.*;
import javax.swing.*;
import javax.swing.text.*;
import javax.swing.table.*;
import java.text.ParseException;
import java.io.*;
import java.util.*;
import commandtools.*;
import controltools.*;
import rnadesign.rnamodel.*;

import tools3d.objects3d.*;

import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerConstants;

/** lets user choose to partner object, inserts correspnding link into controller */
public class HelixConstraintWizard implements GeneralWizard {

    public static final String FRAME_TITLE = "Add Helix Constraint";

    private static final Dimension TABLE_DIM = new Dimension(200, 200);

    private CommandApplication application;

    private ArrayList<StrandJunction3D> junctions;

    private BranchSelector branchSelector1;
    private BranchSelector branchSelector2;

    private JDialog frame;

    private boolean finished;

    private JButton add;

    private Object3DGraphController graphController;

    private int selectedIndex1 = -1;
    private int selectedIndex2 = -1;

    private JTextField bpmin;
    // private JTextField bpmax;


    private ArrayList<ActionListener> actionListeners = new ArrayList<ActionListener>();

    public HelixConstraintWizard(CommandApplication application) {
	this.application = application;
    }

    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    public boolean isFinished() {
	return finished;
    }

    /** generates and inserts new Object3D according to user dialog */
    public void launchWizard(Object3DGraphController graphController,
			     Component parentFrame) {

	this.graphController = graphController;

	
	frame = new JDialog(parentFrame instanceof Frame ? (Frame) parentFrame : null, FRAME_TITLE, true);
	frame.addWindowListener(new WindowAdapter() {
		
		public void windowClosed(WindowEvent e) {
		    finished = true;
	       
		}

	    });

	junctions = new ArrayList<StrandJunction3D>();
	Object3DActionVisitor visitor = new Object3DActionVisitor(graphController.getGraph().getGraph(), new Object3DAction() {
		public void act(Object3D obj) {
		    if(obj instanceof StrandJunction3D)
			junctions.add((StrandJunction3D)obj);

		}

	    });


	visitor.nextToEnd();

	if(junctions.size() <= 1) {
	    JOptionPane.showMessageDialog(null, "No junctions available to apply helix constraints to.\nPlease add junctions using the place junction wizard.");
	    return;

	}

	JPanel panel = new JPanel();
	panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));


	JLabel label = new JLabel("<html><h2>Select first branch");
	label.setAlignmentX(JComponent.LEFT_ALIGNMENT);
	panel.add(label);

	branchSelector1 = new BranchSelector(new AddListener());
	panel.add(branchSelector1);

	label = new JLabel("<html><h2>Select second branch");
	label.setAlignmentX(JComponent.LEFT_ALIGNMENT);
	panel.add(label);

	branchSelector2 = new BranchSelector(new AddListener());
	panel.add(branchSelector2);

	panel.add(Box.createVerticalStrut(15));

	JPanel temp = new JPanel();
	temp.add(new JLabel("Base Pairs:"));
	bpmin = new JTextField(10);
	bpmin.setText("0");
	temp.add(bpmin);

	panel.add(temp);

	panel.add(Box.createVerticalStrut(15));

	/*	temp = new JPanel();
	temp.add(new JLabel("Base Pair Max:"));
	bpmax = new JTextField(10);
	bpmax.setText("0");
	temp.add(bpmax);

	panel.add(temp);
	*/

	panel.add(Box.createVerticalStrut(15));

	JPanel bottomPanel = new JPanel();
	
	JButton close = new JButton("Close");
	close.addActionListener(new CloseWindowListener());
	bottomPanel.add(close);

	add = new JButton("Add");
	add.addActionListener(new AddListener());
	add.setEnabled(false);
	bottomPanel.add(add);

	panel.add(bottomPanel);

	frame.add(panel);

	frame.pack();
	frame.setVisible(true);

    }

    private class CloseWindowListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    frame.setVisible(false);
	    frame.dispose();

	}

    }

    private class AddListener implements ActionListener, ListSelectionListener {
	public void valueChanged(ListSelectionEvent e) {
	    if(branchSelector1.getBranch() != -1 && branchSelector2.getBranch() != -1)
		add.setEnabled(true);
	    else
		add.setEnabled(false);


	}

	public void actionPerformed(ActionEvent e) {
	    if(branchSelector1.getJunction() == branchSelector2.getJunction()) {
		JOptionPane.showMessageDialog(frame, "Pleas select two different junctions");
		return;

	    }

	    BranchDescriptor3D branch1 = junctions.get(branchSelector1.getJunction()).getBranch(branchSelector1.getBranch() - 1);
	    BranchDescriptor3D branch2 = junctions.get(branchSelector2.getJunction()).getBranch(branchSelector2.getBranch() - 1);
	    String branch1Name = Object3DTools.getFullName(branch1);
	    String branch2Name = Object3DTools.getFullName(branch2);

	    Integer bpmi = null;
	    // Integer bpma = null;

	    try {
		bpmi = Integer.parseInt(bpmin.getText());
		// bpma = Integer.parseInt(bpmax.getText());

		if(bpmi < 0) { // || bpma < 0) {
		    JOptionPane.showMessageDialog(frame, "Enter values above zero in the base pair fields");
		    return;
		}

	    } catch(NumberFormatException ex) {
		JOptionPane.showMessageDialog(frame, "Must enter numbers in fields");
		bpmin.setText("0");
		// bpmax.setText("0");
		return;

	    }
	    

	    try {
		application.runScriptLine("genhelixconstraint " + branch1Name + " " + branch2Name + " bp=" + bpmi); //  + " bpmax=" + bpma);

	    } catch(CommandException ex) {
		JOptionPane.showMessageDialog(frame, ex.getMessage());
		return;

	    }

	    frame.setVisible(false);
	    frame.dispose();

	}



    }

    private class JunctionListModel extends AbstractListModel {

	public int getSize() {
	    return junctions.size();

	}


	public Object getElementAt(int index) {
	    return Object3DTools.getFullName(junctions.get(index));

	}


    }

    
    private class BranchSelector extends JPanel {

	JList junctionList;
	JList branchList;


	ListSelectionListener listener;

	BranchSelector(ListSelectionListener l) {
	    listener = l;
	    setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
	    junctionList = new JList(new JunctionListModel());
	    junctionList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	    /*junctionList.setCellRenderer(new ListCellRenderer() {

		    public Component getListCellRendererComponent(JList list,
                                                   Object value,
                                                   int index,
                                                   boolean isSelected,
                                                   boolean cellHasFocus) {

			
			String string = value.toString();
			
			JLabel label = new JLabel(string);
			
			if(BranchSelector.this == branchSelector1) {
			    if(index == branchSelector2.getJunction()) {
				label.setForeground(Color.gray);
				return label;
			    }

			} else {

			    if(index == branchSelector1.getJunction()) {
				label.setForeground(Color.gray);
				return label;
			    }
			}



			if(isSelected || cellHasFocus) {
			    label.setBackground(new Color(240, 240, 255));
			    label.setOpaque(true);
			}

			return label;
		    }

		    });*/
	    junctionList.addListSelectionListener(new ListSelectionListener() {
		    public void valueChanged(ListSelectionEvent e) {
			if(e.getValueIsAdjusting())
			    return;


			add.setEnabled(false);
			if(junctionList.getSelectedIndex() != -1) {
			    /*if(BranchSelector.this == branchSelector1) {
				if(junctionList.getSelectedIndex() == branchSelector2.getJunction()) {
				    junctionList.setSelectedIndex(selectedIndex1);
				    junctionList.clearSelection();
				    branchList.clearSelection();
				    branchList.setEnabled(false);
				    branchSelector2.repaint();
				    return;
				}

				selectedIndex1 = junctionList.getSelectedIndex();

				branchSelector2.repaint();


			    }
			    else {
				if(junctionList.getSelectedIndex() == branchSelector1.getJunction()) {
				    junctionList.setSelectedIndex(selectedIndex2);
				    junctionList.clearSelection();
				    branchList.clearSelection();
				    branchList.setEnabled(false);
				    branchSelector2.repaint();
				    return;

				}

				selectedIndex2 = junctionList.getSelectedIndex();

				branchSelector1.repaint();

			    }

			    */
			    branchList.clearSelection();
			    branchList.setEnabled(true);
			    int branches = junctions.get(junctionList.getSelectedIndex()).getBranchCount();
			    Integer[] b = new Integer[branches];
			    for(int i = 0; i < branches; ++i)
				b[i] = new Integer(i + 1);
			    branchList.setModel(new DefaultComboBoxModel(b));
			    branchList.revalidate();

			}

			else {
			    branchList.setEnabled(false);

			    
			}

		    }

		});

	    JScrollPane pane = new JScrollPane(junctionList);
	    pane.setPreferredSize(TABLE_DIM);
	    add(pane);
	    add(new JSeparator(SwingConstants.VERTICAL));
	    
	    branchList = new JList();
	    branchList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	    branchList.addListSelectionListener(new ListSelectionListener() {
		    public void valueChanged(ListSelectionEvent e) {
			if(e.getValueIsAdjusting())
			    return;

			listener.valueChanged(e);

		    }

		});
	    branchList.setEnabled(false);
	    
	    pane = new JScrollPane(branchList);
	    pane.setPreferredSize(TABLE_DIM);
	    add(pane);

	   
	}

	public void setEnabled(boolean b) {
	    super.setEnabled(b);
	    //junctionTable.setEnabled(b);
	    if(!b)
		junctionList.clearSelection();

	    branchList .setEnabled(b);


	}


	public int getJunction() {

	    if(junctionList .getSelectedIndex() == -1)
		return -1;

	    return junctionList.getSelectedIndex();
	}

	public int getBranch() {

	    if(branchList.getSelectedIndex() == -1)
		return -1;

	    return branchList.getSelectedIndex() + 1;
	}

    }
    
}
