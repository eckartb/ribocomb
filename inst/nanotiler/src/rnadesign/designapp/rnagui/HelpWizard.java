package rnadesign.designapp.rnagui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;

import java.util.StringTokenizer;

import commandtools.CommandApplication;
import commandtools.Command;
import commandtools.AbstractCommand;
import commandtools.UnknownCommandException;
import commandtools.BadSyntaxException;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerConstants;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import tools3d.Vector3D;
import rnadesign.designapp.NanoTilerInterpreter;

import java.io.PrintStream;
import java.io.OutputStream;

import static rnadesign.designapp.PackageConstants.NEWLINE;

/** allows the user to view help for each implemented command*/
public class HelpWizard implements GeneralWizard {
    private static final int COL_SIZE = 15; 
    private boolean finished = false;

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    private JFrame frame = null;
    private JTextArea resultsField;
    private JButton button;
    private JComboBox commandChoice;

    private CommandApplication application;
    private NanoTilerInterpreter interpreter;
    private List<ActionListener> actionListeners = new ArrayList<ActionListener>();
    private Object3DGraphController graphController;
    private String[] commandNames;

    public static final String FRAME_TITLE = "Help";

    public HelpWizard(CommandApplication application){
	assert application != null;
	this.application = application;
	this.interpreter = (NanoTilerInterpreter)application.getInterpreter();
    }

    private class DoneListener implements ActionListener {

	public void actionPerformed(ActionEvent e) {
		frame.setVisible(false);
		frame = null;
		finished = true;
	}
    }
    private class MenuListener implements ActionListener{
	public void actionPerformed(ActionEvent e){
	    String selectedCommand = (String)(((JComboBox)e.getSource()).getSelectedItem());
	    Command command;
	    try{
		try{
		    command = interpreter.interpretLine(selectedCommand);
		    String helpText = command.getLongHelpText();
		    resultsField.setText(helpText);
		}catch(BadSyntaxException exc){command=null;}

	    }catch(UnknownCommandException ex){
		log.severe("Unknown Command");
	    }

	}
    }

    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    public boolean isFinished() {
	return finished;
    }
    private void initCommandNames(){
	commandNames = new String[interpreter.countKnownCommands()];
	StringTokenizer tokenizer = new StringTokenizer(interpreter.getKnownCommandNames());
	for(int k=0;k<commandNames.length;k++){
	    commandNames[k] = (String)tokenizer.nextToken();
	}
    }


    /** generates and inserts new Object3D according to user dialog */
    public void launchWizard(Object3DGraphController graphController,
			     Component parentFrame) {
	if (graphController == null) {
	    log.info("GraphController received by launchWizard is null!");
	}
	this.graphController = graphController;
	frame = new JFrame(FRAME_TITLE);
       	initCommandNames();
	addComponents(frame);
	frame.pack();
	frame.setVisible(true);
    }

    public void addComponents(JFrame _frame) {
	Container f = _frame.getContentPane();
	f.setLayout(new BorderLayout());
       	JPanel centerPanel = new JPanel();
	centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));
	commandChoice = new JComboBox(commandNames);
	commandChoice.addActionListener(new MenuListener());
	//	centerPanel.add(commandChoice);
	resultsField = new JTextArea(20,30);
        resultsField.setLineWrap(true);
	resultsField.setEditable(false);
	JScrollPane scroll = new JScrollPane(resultsField);
	scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
	scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
       	centerPanel.add(scroll);

	JPanel topPanel = new JPanel();
	topPanel.setLayout(new BoxLayout(topPanel,BoxLayout.Y_AXIS));		
	topPanel.add(new JLabel("Select the command that you would like to receive help for:              "));
	topPanel.add(commandChoice);

	JPanel buttonPanel = new JPanel();
  	button = new JButton("Done");
       	button.addActionListener(new DoneListener());
	buttonPanel.add(button);
	f.add(topPanel, BorderLayout.NORTH);
	f.add(buttonPanel, BorderLayout.SOUTH);
       	f.add(centerPanel, BorderLayout.CENTER);
	try{
	    try{
		Command command = interpreter.interpretLine(commandNames[0]);
		String helpText = command.getLongHelpText();
		resultsField.setText(helpText);
	    }catch(BadSyntaxException exc){}
	}
	catch(UnknownCommandException ex){
	}
    }

}
