package rnadesign.designapp.rnagui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

import rnadesign.rnacontrol.Object3DGraphController;


/** creates a new frame after "hide" is clicked with options to 
    turn off or on different graphics. 
*/

public class HideGraphicsWizard implements GeneralWizard {

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    private JFrame frame;
    private JCheckBox atomsBox;
    private JCheckBox moleculesBox;
    private JCheckBox nucleotidesBox;
    private JCheckBox strandsBox;

    private boolean addAtomsFlag = false;
    private boolean addMoleculesFlag = false;
    private boolean addNucleotidesFlag = false;
    private boolean addStrandsFlag = false;

    private String FRAME_TITLE = "Hide Graphics";
    private Object3DGraphController graphController;
    private List<ActionListener> actionListeners = new ArrayList<ActionListener>();

    private boolean finished = false;

    private class DoneListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    readOutValues();
	    
	    frame.setVisible(false);
	    frame = null;
	    finished = true;
	}
    }

    private class CancelListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
 	    frame.setVisible(false);
 	    frame = null;
 	    finished = true;
 	}
    }
    
    
    /** generates and inserts new Object3D according to user dialog */
    public void launchWizard(Object3DGraphController graphController,
			     Component parentFrame) {
	if (graphController == null) {
	    log.info("GraphController received by launchWizard is null!");
	}
	this.graphController = graphController;
	frame = new JFrame(FRAME_TITLE);
	addComponents(frame);
	frame.pack();
	frame.setVisible(true);
    }

    public void addComponents(JFrame _frame) {
	Container f = _frame.getContentPane();
	f.setLayout(new BorderLayout());
	JPanel center = new JPanel();
	center.setLayout(new BoxLayout(center, BoxLayout.Y_AXIS));
	
	JPanel topPanel = new JPanel();
	
	JPanel sPanel = new JPanel();
	JPanel checkPanel = new JPanel();
	
	atomsBox = new JCheckBox("hide atoms", addAtomsFlag);
	moleculesBox = new JCheckBox("hide molecules", addMoleculesFlag);
	nucleotidesBox = new JCheckBox("hide nucleotides", addNucleotidesFlag);
	strandsBox = new JCheckBox("hide strands", addStrandsFlag);

	topPanel.add(atomsBox);
	topPanel.add(moleculesBox);
	topPanel.add(nucleotidesBox);
	topPanel.add(strandsBox);
	
	JPanel bottomPanel = new JPanel();
	JButton button = new JButton("Done");
	button.addActionListener(new DoneListener());
	bottomPanel.add(button);
	button = new JButton("Cancel");
	button.addActionListener(new CancelListener());
	bottomPanel.add(button);

	f.add(topPanel, BorderLayout.NORTH);
	f.add(bottomPanel, BorderLayout.SOUTH);

    }

    private void readOutValues() {
	addAtomsFlag = atomsBox.isSelected();
	addMoleculesFlag = moleculesBox.isSelected();
	addNucleotidesFlag = nucleotidesBox.isSelected();
	addStrandsFlag = strandsBox.isSelected();
    }

    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    public boolean isFinished() {
	return finished;
    }

}
