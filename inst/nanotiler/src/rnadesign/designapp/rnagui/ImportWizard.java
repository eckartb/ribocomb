package rnadesign.designapp.rnagui;

import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import commandtools.CommandException;
import rnadesign.designapp.NanoTilerScripter;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerConstants;
import rnadesign.rnamodel.ConnectingStemGenerator;
import rnadesign.rnamodel.FitParameters;
import rnadesign.rnamodel.FragmentGridTiler;
import rnadesign.rnamodel.SimpleStrandJunctionDB;
import rnadesign.rnamodel.StrandJunctionDB;
import tools3d.Vector3D;
import tools3d.objects3d.Object3DIOException;

import static java.awt.Component.RIGHT_ALIGNMENT;
import static javax.swing.BoxLayout.Y_AXIS;
import static rnadesign.designapp.rnagui.PackageConstants.SLASH;

/** lets user choose to partner object, inserts correspnding link into controller */
public class ImportWizard implements GeneralWizard {

    // public static final int PDB_FORMAT = Object3DGraphControllerConstants.PDB_RNAVIEW_FORMAT;
    public static final int PDB_FORMAT = Object3DGraphControllerConstants.PDB_RNAVIEW_FORMAT; // TODO: add checkbox for potential renumbering! changed! EB 7/2007
    public static final int POINTSET_FORMAT = Object3DGraphControllerConstants.POINTSET_FORMAT;
    public static final int POINTSET2_FORMAT = Object3DGraphControllerConstants.POINTSET2_FORMAT;
    public static final int NTL_FORMAT = Object3DGraphControllerConstants.NTL_FORMAT;

    public static Logger log = Logger.getLogger("NanoTiler_debug");
    public static final double RAD2DEG = 180.0 / Math.PI;
    public static final double DEG2RAD = Math.PI / 180.0;
    private static final int COL_SIZE_DOUBLE = 8; 
    private static final int COL_SIZE_INT = 5; 
    private static final int COL_SIZE_NAME = 20; 
    private static final int COL_SIZE_FILENAME = 50; 
    private boolean finished = false;
    private JFrame frame = null;

    //TextFields:
    private JTextField nameField;
    private JTextField fileNameField;
    private JTextField seqCharField;
    private JTextField scaleField;
    private JTextField xStartField;
    private JTextField yStartField;
    private JTextField zStartField;
    private JTextField junctionMinOrderField;
    private JTextField junctionAngleLimitField;
    private JTextField junctionRmsLimitField;
    private JTextField stemAngleLimitField;
    private JTextField stemRmsLimitField;
    private JTextField rerunField;

    //CheckBoxes:
    private JCheckBox addJunctionsBox; // Pdb // Add junctions from the PDB into the DB.
    private JCheckBox addStemsBox; // Add stems to the graph.
    private JCheckBox connectingStemsBox; // Connect the stems in the graph.
    private JCheckBox findStemsBox; // Pdb 
    private JCheckBox forbiddenBox; // Place each junction only once.
    private JCheckBox fuseStrandsBox; // Fuse the strands together.
    private JCheckBox junctionPlaceBox; // Place junctions on the graph.
    private JCheckBox kissingLoopBox; // Place kissing loops in the connecting stems.
    private JCheckBox kissingLoopVerticeBox; // Place kissing loops on the vertices.
    private JCheckBox symmetryBox; // Use the same junction.

    //Flags:
    private boolean addJunctionsDefault = true;
    private boolean addJunctionsFlag;
    private boolean addStemsDefault = true;
    private boolean addStemsFlag;
    private boolean connectingStemsDefault; // fgTiler.getConnectJunctionsMode();
    private boolean findStemsDefault = false;
    private boolean findStemsFlag;
    private boolean forbiddenDefault; // fgTiler.getForbiddenMode();
    private boolean fuseStrandsDefault; // fgTiler.getFuseJunctionStrandsMode();
    private boolean junctionPlaceDefault = true;
    private boolean junctionPlaceFlag;
    private boolean kissingLoopDefault; // fgTiler.getKissingLoopMode();
    private boolean kissingLoopVerticeDefault = false;
    private boolean kissingLoopVerticeFlag;
    private boolean symmetryDefault; // fgTiler.getSymmetryMode();

    private List<ActionListener> actionListeners = new ArrayList<ActionListener>();
    private Object3DGraphController graphController;
    FragmentGridTiler fgTiler;
    private Vector3D scale = new Vector3D(1, 1, 1);
    private double xStart = 0;
    private double yStart = 0;
    private double zStart = 0;
    private String name = "";
    private String pointsName = "point";
    private String pdbName = "import"; //PdbImportWizard
    private String charString = "N"; // sequence character
    private String fileName;
    private String usedDirectory;
    private int junctionCount = 0;
    private int kissingLoopCount = 0;
    private int formatId;
    private NanoTilerScripter nanoScripter;

    public static final String FRAME_TITLE = "Import Wizard";
    public static final String NTL_EXTENSION = ".ntl";
    public static final String PDB_EXTENSION = ".pdb";
    public static final String POINTS_EXTENSION = ".points";
    public static final String POINTS2_EXTENSION = ".points2";
    
    public ImportWizard(NanoTilerScripter nanoScripter) {
	this.nanoScripter = nanoScripter;
    }

    private class DoneListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    try {
		readOutValues();
		char sequenceChar = 'N';
		if (charString.length() > 0) {
		    sequenceChar = charString.charAt(0);
		}
		// open file
		try {
		    // 		    FileInputStream fis;
		    // 		    fis = new FileInputStream(fileName);
		    // 		    if (formatId == PDB_FORMAT ||
		    // 			formatId == NTL_FORMAT) {
		    // 			addStemsFlag = false;
		    // 			sequenceChar = 'X';
		    // 		    }
		    String command = buildCommand(fileName, name, scale, xStart, yStart, zStart, formatId,
						  addStemsFlag, sequenceChar, findStemsFlag, addJunctionsFlag);
		    System.out.println("Calling import command: " + command);
		    nanoScripter.runScriptLine(command);
		    // 		    graphController.readAndAdd(fis, name,
		    // 					       scale, 
		    // 					       new Vector3D(xStart, yStart, zStart), 
		    // 					       formatId,
		    // 					       addStemsFlag, sequenceChar, false, //TODO: remove false: not used ever
		    // 					       findStemsFlag, addJunctionsFlag); //false, false
		}
		catch (CommandException exc) {
		    JOptionPane.showMessageDialog(frame, "Error importing file: " + fileName + " : " 
						  + exc.getMessage());
		    return;
		}
		frame.setVisible(false);
		frame = null;
		finished = true;
		graphController.setLastReadDirectory(usedDirectory);
	    }
	    catch (FileExtensionException fee) {
		    JOptionPane.showMessageDialog(frame, "Error importing file due to bad file extension: " + fileName + " : " 
						  + fee.getMessage());
		    return;		
	    }
	}
    }

    /** Converts boolean to "true" of "false" strings. */
    private String bool2String(boolean b) {
	return b ? "true" : "false";
    }

    /** Generates import command from numerical or string values */
    private String buildCommand(String fileName, String name, Vector3D scale, double xStart, double yStart, double zStart,
				int formatId, boolean addStemsFlag, char sequenceChar, 
				boolean findStemsFlag, boolean addJunctionsFlag) {
	// boolean onlyFirstPathMode,
	return "import " + fileName + " name=" + name + " format="+formatId + " addstems=" + bool2String(addStemsFlag) 
	    + " findstems="+bool2String(findStemsFlag) + " addjunctions=" + bool2String(addJunctionsFlag);
    }
    

    private int getFormat(String fileName) {
	int result;
	if (fileName.endsWith(POINTS_EXTENSION)) {
	    return POINTSET_FORMAT;
	}
	else if (fileName.endsWith(POINTS2_EXTENSION)) {
	    return POINTSET2_FORMAT;
	}
	else if (fileName.endsWith(PDB_EXTENSION)) {
	    return PDB_FORMAT;
	}
	else if (fileName.endsWith(NTL_EXTENSION)) {
	    return NTL_FORMAT;
	}
	else {
	    log.severe("File extension not found: " + fileName + " Assuming PDB file format.");
	    return PDB_FORMAT;
	}
    }
    
    private class CancelListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    frame.setVisible(false);
	    frame = null;
	    finished = true;
	}
    }
    
    class ImportFileFilter extends javax.swing.filechooser.FileFilter {
	public boolean accept(File f) {
	    boolean b = f.isDirectory() ||
		f.getName().toLowerCase().endsWith(POINTS_EXTENSION) ||
		f.getName().toLowerCase().endsWith(POINTS2_EXTENSION) ||
		// f.getName().toLowerCase().endsWith(KNOTPLOT_EXTENSION) ||
		f.getName().toLowerCase().endsWith(PDB_EXTENSION) ||
		f.getName().toLowerCase().endsWith(NTL_EXTENSION); //which files show up in window
	    return b;
	}
	public String getDescription() {
	    return "Importable files";
	}
    }

    private class BrowseActionListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
    	    JFileChooser chooser = new JFileChooser();
	    ImportFileFilter filter = new ImportFileFilter();
	    chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
	    chooser.setFileFilter(filter);
    	    String cwdName = graphController.getLastReadDirectory();
    	    File cwd = new File(cwdName);
	    chooser.setCurrentDirectory(cwd);
    	    int returnVal = chooser.showOpenDialog(frame);
	    if(returnVal == JFileChooser.APPROVE_OPTION) {
		fileName = chooser.getCurrentDirectory() + 
		    SLASH + chooser.getSelectedFile().getName();
		fileNameField.setText(fileName);
		usedDirectory = chooser.getCurrentDirectory().getAbsolutePath();
		if (fileName.endsWith(PDB_EXTENSION) ||
		    fileName.endsWith(NTL_EXTENSION)) {
		    name = pdbName;
		}
		else if (fileName.endsWith(POINTS_EXTENSION) ||
			 fileName.endsWith(POINTS2_EXTENSION)) {
		    name = pointsName;
		}
		else {
		    log.warning("Not PDB, NTL, or points file!!: " + fileName + " Assuming PDB format!");
		    name = pdbName;
		}
    	       	// TODO won't run on Windows! fix!
		log.info("You chose this file: " + fileName);
		formatId = getFormat(fileName);
		addComponents2(frame);
		frame.pack();
		frame.setVisible(true);
	    }
	}
    }

    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    public boolean isFinished() {
	return finished;
    }

    /** generates and inserts new Object3D according to user dialog */
    public void launchWizard(Object3DGraphController graphController,
			     Component parentFrame) {
	if (graphController == null) {
	    log.info("GraphController received by launchWizard is null!");
	}
	this.graphController = graphController;
	try {
	    if (this.graphController.getJunctionController().getJunctionDB() != null) {
		junctionCount = this.graphController.getJunctionController().getJunctionDB().getJunctionCount();
	    }
	    else {
		junctionCount = 0;
	    }
	}
	catch (AssertionError e) {
	    log.info("No junctions found in DB");
	}
	try {
	    if (this.graphController.getJunctionController().getKissingLoopDB() != null) {
		kissingLoopCount = this.graphController.getJunctionController().getKissingLoopDB().getJunctionCount();
	    }
	    else {
		kissingLoopCount = 0;
	    }
	}
	catch (AssertionError e) {
	    log.info("No kissing loops found in DB");
	}
	this.fgTiler = graphController.getFragmentGridTiler(); // TODO : avoid direction interaction with model
	setDefaultValues();
	frame = new JFrame(FRAME_TITLE);
	addComponents1(frame);
	frame.pack();
	frame.setVisible(true);
    }

    public void addComponents2(JFrame _frame) {
	Container container = _frame.getContentPane();
	try {
	    container.remove(1);
	} // if Browse is called a second time
	catch (ArrayIndexOutOfBoundsException e) { }
	container.setLayout(new BoxLayout(container, Y_AXIS));
	JPanel bigPanel = new JPanel();
	bigPanel.setLayout(new BoxLayout(bigPanel, Y_AXIS));
	bigPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 10));
	JPanel namePanel = new JPanel();
	nameField = new JTextField(name, COL_SIZE_NAME);
	namePanel.add(new JLabel("Name:"));
	namePanel.add(nameField);
	JPanel startPanel = new JPanel();
	xStartField = new JTextField("" + xStart, COL_SIZE_DOUBLE);
	yStartField = new JTextField("" + yStart, COL_SIZE_DOUBLE);
	zStartField = new JTextField("" + zStart, COL_SIZE_DOUBLE);
	startPanel.add(new JLabel("offset: x:"));
	startPanel.add(xStartField);
	startPanel.add(new JLabel("y:"));
	startPanel.add(yStartField);
	startPanel.add(new JLabel("z:"));
	startPanel.add(zStartField);
	JPanel scalePanel = new JPanel();
	scaleField = new JTextField("1.0", COL_SIZE_DOUBLE);
	scalePanel.add(new JLabel("Scale:"));
	scalePanel.add(scaleField);
	bigPanel.add(namePanel);
	bigPanel.add(startPanel);
	bigPanel.add(scalePanel);
	if (formatId == PDB_FORMAT ||
	    formatId == NTL_FORMAT) {
	    addComponentsPdb(bigPanel);
	}
	else if (formatId == POINTSET_FORMAT ||
		 formatId == POINTSET2_FORMAT) {
	    addComponentsPoints(bigPanel);
	}
	else {
	    log.severe("This format not recognized: " + formatId + " Assuming PDB file format.");
	    addComponentsPdb(bigPanel);
	}
	JPanel bottomPanel = new JPanel();
	JButton button = new JButton("Cancel");
	button.addActionListener(new CancelListener());
	bottomPanel.add(button);
  	button = new JButton("Done");
	button.addActionListener(new DoneListener());
	bottomPanel.add(button);
	bigPanel.add(bottomPanel);
	container.add(bigPanel);

    }

    public void addComponentsPdb(JPanel bigPanel) {
	findStemsBox = new JCheckBox("Find stems", findStemsFlag);
	addJunctionsBox = new JCheckBox("Find junctions", findStemsFlag);
	JPanel checkPanel = new JPanel();
	checkPanel.setLayout(new BoxLayout(checkPanel, Y_AXIS));
	checkPanel.add(findStemsBox);
	checkPanel.add(addJunctionsBox);
	bigPanel.add(checkPanel);
    }

    public void addComponentsPoints(JPanel bigPanel) {
 	seqCharField = new JTextField(charString, 1);
 	junctionMinOrderField = new JTextField("" + fgTiler.getJunctionMinOrder(), COL_SIZE_INT);
 	rerunField = new JTextField("" + fgTiler.getRerunMax(), COL_SIZE_INT);
 	String junctionAngleLimitText = "" + (fgTiler.getJunctionFitParameters().getAngleLimit() * RAD2DEG);
 	if (junctionAngleLimitText.length() > COL_SIZE_DOUBLE) {
 	    junctionAngleLimitText = junctionAngleLimitText.substring(0, COL_SIZE_DOUBLE);
 	}
 	junctionAngleLimitField = new JTextField(junctionAngleLimitText, COL_SIZE_DOUBLE);
	String junctionRmsLimitText = "" + (fgTiler.getJunctionFitParameters().getRmsLimit());
 	if (junctionRmsLimitText.length() > COL_SIZE_DOUBLE) {
 	    junctionRmsLimitText = junctionRmsLimitText.substring(0, COL_SIZE_DOUBLE);
 	}
 	junctionRmsLimitField = new JTextField( junctionRmsLimitText, COL_SIZE_DOUBLE);
 	String stemAngleLimitText = "" + (fgTiler.getStemFitParameters().getAngleLimit() * RAD2DEG);
 	if (stemAngleLimitText.length() > COL_SIZE_DOUBLE) {
 	    stemAngleLimitText = stemAngleLimitText.substring(0, COL_SIZE_DOUBLE);
 	}
 	stemAngleLimitField = new JTextField(stemAngleLimitText, COL_SIZE_DOUBLE);
 	String stemRmsLimitText = "" + (fgTiler.getStemFitParameters().getRmsLimit());
 	if (stemRmsLimitText.length() > COL_SIZE_DOUBLE) {
 	    stemRmsLimitText = stemRmsLimitText.substring(0, COL_SIZE_DOUBLE);
 	}
	stemRmsLimitField = new JTextField( stemRmsLimitText, COL_SIZE_DOUBLE);

	JPanel labelPanel = new JPanel();
	labelPanel.setLayout(new BoxLayout(labelPanel, Y_AXIS));
	JPanel fieldPanel = new JPanel();
	fieldPanel.setLayout(new BoxLayout(fieldPanel, Y_AXIS));
	// TODO: make left labels match up to right fields
	labelPanel.add(new JLabel("Sequence character:"));
	labelPanel.add(new JLabel("Minimum order to place:"));
	labelPanel.add(new JLabel("Reruns:"));
	labelPanel.add(new JLabel("Junction Angle Limit:"));
	labelPanel.add(new JLabel("Junction RMS Limit:"));
	labelPanel.add(new JLabel("Stem Angle Limit:"));
	labelPanel.add(new JLabel("Stem RMS Limit:"));
	fieldPanel.add(seqCharField);
	fieldPanel.add(junctionMinOrderField);
	fieldPanel.add(rerunField);
	fieldPanel.add(junctionAngleLimitField);
	fieldPanel.add(junctionRmsLimitField);
	fieldPanel.add(stemAngleLimitField);
	fieldPanel.add(stemRmsLimitField);
	JPanel paramPanel = new JPanel();
	paramPanel.setLayout(new FlowLayout());
	paramPanel.add(labelPanel);
	paramPanel.add(fieldPanel);

	JPanel checkPanel = new JPanel();
 	GridBagLayout gbl = new GridBagLayout();
 	int[] columnW = new int[3];
 	columnW[0] = 25;
 	columnW[1] = 25;
 	columnW[2] = 50;
 	gbl.columnWidths = columnW;
 	int[] rowH = new int[8];
 	for (int i = 0; i < rowH.length; i++)
 	    rowH[i] = 20;
 	gbl.rowHeights = rowH;
	checkPanel.setLayout(gbl);
 	GridBagConstraints gbc = new GridBagConstraints();
 	gbc.gridx = 0;
 	gbc.gridy = 0;
 	gbc.gridwidth = GridBagConstraints.REMAINDER;
 	gbc.anchor = GridBagConstraints.WEST;
 	addStemsBox = new JCheckBox("Structure generator", addStemsFlag);
	addStemsBox.addActionListener(new AddStemsBoxListener());
 	gbl.setConstraints(addStemsBox, gbc);
 	gbc.gridx++;
 	gbc.gridy++;
	junctionPlaceBox = new JCheckBox("Place junctions at vertices", junctionPlaceFlag);
	if (junctionCount > 0) {
	    junctionPlaceBox.setEnabled(true);
	}
	else {
	    junctionPlaceBox.setEnabled(false);
	    junctionPlaceBox.setSelected(false);
	    kissingLoopVerticeFlag = true;
	    junctionPlaceBox.setToolTipText("No junctions in database.");
	}
	gbl.setConstraints(junctionPlaceBox, gbc);
 	gbc.gridy++;
	kissingLoopVerticeBox = new JCheckBox("Place kissing loops at vertices", kissingLoopVerticeFlag);
	if (kissingLoopCount > 0) {
	    kissingLoopVerticeBox.setEnabled(true);
	}
	else {
	    kissingLoopVerticeBox.setEnabled(false);
	    kissingLoopVerticeBox.setSelected(false);
	    kissingLoopVerticeBox.setToolTipText("No kissing loops in database.");
	}
	gbl.setConstraints(kissingLoopVerticeBox, gbc);
 	gbc.gridy++;
	connectingStemsBox = new JCheckBox("Generate connecting stems", fgTiler.getConnectJunctionsMode());
	connectingStemsBox.addActionListener(new ConnectingStemsBoxListener());
	gbl.setConstraints(connectingStemsBox, gbc);
	gbc.gridy++;
	gbc.gridx++;
 	kissingLoopBox = new JCheckBox("Connect with kissing loops", fgTiler.getKissingLoopMode());
 	gbl.setConstraints(kissingLoopBox, gbc);
	gbc.gridy++;
	gbc.gridx--;
	forbiddenBox = new JCheckBox("Place each junction only once", fgTiler.getForbiddenMode());
	gbl.setConstraints(forbiddenBox, gbc);
	gbc.gridy++;
 	symmetryBox = new JCheckBox("Always use the same junction",fgTiler.getSymmetryMode());
 	gbl.setConstraints(symmetryBox, gbc);
	gbc.gridy++;
 	fuseStrandsBox = new JCheckBox("Fuse stem strands with junction strands", fgTiler.getFuseJunctionStrandsMode());
 	gbl.setConstraints(fuseStrandsBox, gbc);
	if ((junctionCount + kissingLoopCount) > 0) {
	    addStemsBox.setEnabled(true);
	}
	else {
	    addStemsBox.setEnabled(false);
	    addStemsBox.setSelected(false);
	    addStemsBox.setToolTipText("No junctions or kissing loops in DB");
	    connectingStemsBox.setEnabled(false);
	    connectingStemsBox.setSelected(false);
	    kissingLoopBox.setEnabled(false);
	    forbiddenBox.setEnabled(false);
	    symmetryBox.setEnabled(false);
	    fuseStrandsBox.setEnabled(false);
	    fuseStrandsBox.setSelected(false);
	}
	checkPanel.add(addStemsBox);
	checkPanel.add(junctionPlaceBox);
 	checkPanel.add(kissingLoopVerticeBox);
 	checkPanel.add(connectingStemsBox);
 	checkPanel.add(kissingLoopBox);
 	checkPanel.add(forbiddenBox);
 	checkPanel.add(symmetryBox);
 	checkPanel.add(fuseStrandsBox);
	bigPanel.add(paramPanel);
	bigPanel.add(checkPanel);
    }

    private void setDefaultValues() {
	findStemsDefault = false; // Pdb
	addJunctionsDefault = false; // Pdb
	if ((junctionCount + kissingLoopCount) > 0) {
	    addStemsDefault = true;
	    connectingStemsDefault = fgTiler.getConnectJunctionsMode();
	    forbiddenDefault = fgTiler.getForbiddenMode();
	    symmetryDefault = fgTiler.getSymmetryMode();
	    fuseStrandsDefault = fgTiler.getFuseJunctionStrandsMode();
	    if (junctionCount > 0) {
		junctionPlaceDefault = true;
		kissingLoopVerticeDefault = false;
		kissingLoopDefault = false;
	    }
	    else {
		junctionPlaceDefault = false;
		kissingLoopVerticeDefault = true;
		if (connectingStemsDefault = true) {
		    kissingLoopDefault = fgTiler.getKissingLoopMode();
		}
	    }
	}
	else {
	    addStemsDefault = false;
	    connectingStemsDefault = false;
	    junctionPlaceDefault = false;
	    kissingLoopVerticeDefault = false;
	    kissingLoopDefault = false;
	    forbiddenDefault = false;
	    symmetryDefault = false;
	    fuseStrandsDefault = false;
	}
	addStemsFlag = addStemsDefault;
	findStemsFlag = findStemsDefault; // Pdb
	addJunctionsFlag = addJunctionsDefault; // Pdb
	junctionPlaceFlag = junctionPlaceDefault;
	kissingLoopVerticeFlag = kissingLoopVerticeDefault;
    }
    
    private class ConnectingStemsBoxListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    if (connectingStemsBox.isSelected()) {
		kissingLoopBox.setEnabled(true);
	    }
	    else {
		kissingLoopBox.setEnabled(false);
		kissingLoopBox.setSelected(false);
		kissingLoopBox.setToolTipText("Generating stems mode not selected.");
	    }
	}
    }
    
    private class AddStemsBoxListener implements ActionListener {
	public void actionPerformed(ActionEvent e) { 
	    if (addStemsBox.isSelected()) {
		junctionPlaceBox.setEnabled(true);
		junctionPlaceBox.setSelected(junctionPlaceDefault);
		kissingLoopVerticeBox.setEnabled(true);
		kissingLoopVerticeBox.setSelected(kissingLoopVerticeDefault);
		connectingStemsBox.setEnabled(true);
		connectingStemsBox.setSelected(connectingStemsDefault);
		kissingLoopBox.setEnabled(true);
		kissingLoopBox.setSelected(kissingLoopDefault);
		forbiddenBox.setEnabled(true);
		forbiddenBox.setSelected(forbiddenDefault);
		symmetryBox.setEnabled(true);
		symmetryBox.setSelected(symmetryDefault);
		fuseStrandsBox.setEnabled(true);
		fuseStrandsBox.setSelected(fuseStrandsDefault);
	    }
	    else {
		junctionPlaceBox.setEnabled(false);
		junctionPlaceBox.setSelected(false);
		kissingLoopVerticeBox.setEnabled(false);
		kissingLoopVerticeBox.setSelected(false);
		connectingStemsBox.setEnabled(false);
		connectingStemsBox.setSelected(false);
		kissingLoopBox.setEnabled(false);
		kissingLoopBox.setSelected(false);
		forbiddenBox.setEnabled(false);
		forbiddenBox.setSelected(false);
		symmetryBox.setEnabled(false);
		symmetryBox.setSelected(false);
		fuseStrandsBox.setEnabled(false);
		fuseStrandsBox.setSelected(false);
	    }
	}
    }
     
    public void addComponents1(JFrame _frame) {
	Container container = _frame.getContentPane();
	JPanel bigPanel = new JPanel();
	bigPanel.setLayout(new BoxLayout(bigPanel, Y_AXIS));
	bigPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 10));
	JPanel filePanel = new JPanel();
	filePanel.add(new JLabel("Filename:"));
	fileNameField = new JTextField(fileName, COL_SIZE_FILENAME);
	filePanel.add(fileNameField);
	JButton button = new JButton("Browse");
	button.addActionListener(new BrowseActionListener());
	filePanel.add(button);
	bigPanel.add(filePanel);
	container.add(bigPanel);
    }


    private void readOutValues() throws FileExtensionException {
	try {
	    junctionPlaceFlag = junctionPlaceBox.isSelected();
	    kissingLoopVerticeFlag = kissingLoopVerticeBox.isSelected();
	    addStemsFlag = addStemsBox.isSelected();
	    FitParameters junctionFitParameters = fgTiler.getJunctionFitParameters();
	    junctionFitParameters.setAngleLimit(DEG2RAD * Double.parseDouble(junctionAngleLimitField.getText().trim()));
	    junctionFitParameters.setRmsLimit(Double.parseDouble(junctionRmsLimitField.getText().trim())); // TODO : mixing of junction and stem fit!
	    fgTiler.setJunctionFitParameters(junctionFitParameters);
	    
	    fgTiler.setForbiddenMode(forbiddenBox.isSelected());
	    fgTiler.setConnectJunctionsMode(connectingStemsBox.isSelected());
	    fgTiler.setKissingLoopMode(kissingLoopBox.isSelected());
	    fgTiler.setKissingLoopJunctionMode(kissingLoopVerticeFlag);
	    fgTiler.setRerunMax(Integer.parseInt(rerunField.getText().trim()));
	    fgTiler.setSymmetryMode(symmetryBox.isSelected());
	    if (fuseStrandsBox.isSelected()) {
		fgTiler.setAppendStrandsMode(ConnectingStemGenerator.APPEND_STICKY_ENDS); 
	    }
	    else {
		fgTiler.setAppendStrandsMode(ConnectingStemGenerator.APPEND_NONE); 
	    }
	    
	    FitParameters stemFitParameters = fgTiler.getStemFitParameters();
	    stemFitParameters.setAngleLimit(DEG2RAD * Double.parseDouble(stemAngleLimitField.getText().trim()));
	    stemFitParameters.setRmsLimit(Double.parseDouble(stemRmsLimitField.getText().trim())); // TODO : mixing of junction and stem fit!
	    fgTiler.setStemFitParameters(stemFitParameters);
	    
	    fgTiler.setJunctionMinOrder(Integer.parseInt(junctionMinOrderField.getText().trim()));
	    charString = seqCharField.getText().trim();
	}
	catch (NullPointerException npe) { //Boxes will be null if not initialized.
	    // PDBImportWizard
	    findStemsFlag = findStemsBox.isSelected();
	    addJunctionsFlag = addJunctionsBox.isSelected();
	}

	// TODO : avoid direction interaction with fragment grid tiler (part of "model") 

	xStart = Double.parseDouble(xStartField.getText().trim());
	yStart = Double.parseDouble(yStartField.getText().trim());
	zStart = Double.parseDouble(zStartField.getText().trim());
	double scaleFactor = Double.parseDouble(scaleField.getText().trim());
	if (scaleFactor != 1.0) {
	    scale = new Vector3D(scaleFactor, scaleFactor, scaleFactor);
	}
	fileName = fileNameField.getText().trim();
	if (!fileName.endsWith(POINTS_EXTENSION) &&
	    !fileName.endsWith(POINTS2_EXTENSION) &&
	    !fileName.endsWith(PDB_EXTENSION) &&
	    !fileName.endsWith(NTL_EXTENSION)) {
	    throw new FileExtensionException(fileName);
	}
	name = nameField.getText().trim();
    }

}

    /* //removed because only RNAVIEW format will be used
       private int translateFormatId(int rawIndex) {
       switch (rawIndex) {
       case 0: return Object3DGraphControllerConstants.PDB_RNAVIEW_FORMAT;
       case 1: return PDB_FORMAT;
       case 2: return Object3DGraphControllerConstants.PDB_DNA_FORMAT;
       default: assert false; // should never be here
       }
       return Object3DGraphControllerConstants.PDB_RNAVIEW_FORMAT;
       }
    */

    /*
      private class KissingLoopJunctionBoxListener implements ActionListener {
      public void actionPerformed(ActionEvent e) {
      // bothJunctionBox.setEnabled( kissingLoopJunctionBox.isEnabled() && kissingLoopJunctionBox.isSelected() );
      }
      }
    */

	    // 	    forbiddenBox.setEnabled( addStemsBox.isSelected() && addStemsBox.isEnabled() );
	    // 	    connectingStemsBox.setEnabled( addStemsBox.isSelected() && addStemsBox.isEnabled() );
	    // 	    kissingLoopBox.setEnabled( connectingStemsBox.isEnabled() && connectingStemsBox.isSelected() && NanoTiler.numberOfKissingLoops>0 );
	    // 	    kissingLoopJunctionBox.setEnabled( addStemsBox.isSelected() && addStemsBox.isEnabled() && NanoTiler.numberOfKissingLoops>0 );
	    // 	    bothJunctionBox.setEnabled( kissingLoopJunctionBox.isEnabled() && kissingLoopJunctionBox.isSelected() && NanoTiler.numberOfKissingLoops>0 );
	    // 	    symmetryBox.setEnabled( addStemsBox.isSelected() && addStemsBox.isEnabled() );
	    // 	    fuseStrandsBox.setEnabled( addStemsBox.isSelected() && addStemsBox.isEnabled() );
	    
	    // 	    if(forbiddenBox.isSelected()) {
	    // 		forbiddenBox.setSelected(forbiddenBox.isEnabled());
	    // 	    }
	    
	    // 	    if(connectingStemsBox.isSelected()) 
	    // 		connectingStemsBox.setSelected(connectingStemsBox.isEnabled());
	    // 	    if(kissingLoopBox.isSelected())
	    // 		kissingLoopBox.setSelected(kissingLoopBox.isEnabled());
	    // 	    if(kissingLoopJunctionBox.isSelected())
	    // 		kissingLoopJunctionBox.setSelected(kissingLoopJunctionBox.isEnabled());
	    // 	    if(bothJunctionBox.isSelected())
	    // 		bothJunctionBox.setSelected(bothJunctionBox.isEnabled());
	    // 	    if(symmetryBox.isSelected())
	    // 		symmetryBox.setSelected(symmetryBox.isEnabled());
	    // 	    if(fuseStrandsBox.isSelected())
	    // 		fuseStrandsBox.setSelected(fuseStrandsBox.isEnabled());
	    
    /**
     * Returns default selected value for each box.
     *
     * @param boxString The string representation of each box.
     */
    /*
      private boolean default(String boxString) {
      ImportWizard reference = new ImportWizard();
      if ((junctionDB.getJunctionCount() + kissingLoopDB.getJunctionCount()) > 0) {
      if (boxString.equals("addStemsBox")) {
      return reference.addStemsFlag;
      }
      else if (boxString.equals("junctionPlaceBox")) {
      if (junctionDB.getJunctionCount() > 0) {
      return reference.junctionPlaceFlag;
      }
      else {
      return false;
      }
      }
      else if (boxString.equals("kissingLoopVerticeBox")) {
      if (kissingLoopDB.getJunctionCount() > 0) {
      return reference.kissingLoopVerticeFlag;
      }
      else {
      return false;
      }
      }
      else if (boxString.equals("connectingStemsBox")) {
      if (junctionPlaceBox.isSelected() ||
      kissingLoopVerticeBox.isSelected()) {
      return reference.connectingStemsFlag;
      }
      else {
      return false;
      }
      }
      else if (boxString.equals("kissingLoopBox")) {
      if ((junctionPlaceBox.isSelected() ||
      kissingLoopVerticeBox.isSelected()) &&
      connectingStemsBox.isSelected()) {
      return reference.connectingStemsFlag;
      }
      else {
      return false;
      }
      }
      else if (boxString.equals("forbiddenBox")) {
      return reference.forbiddenFlag;
      }
      else if (boxString.equals("symmetryBox")) {
      return reference.symmetryFlag;
      }
      else if (boxString.equals("fuseStrandsBox")) {
      return reference.fuseStrandsFlag;
      }
      else {
      log.severe("ImportWizard does not recognize box: " + boxString);
      assert false;
      }
      }
      else {
      return false;
      }
      }
    */

