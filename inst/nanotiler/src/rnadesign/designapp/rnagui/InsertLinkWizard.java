package rnadesign.designapp.rnagui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import rnadesign.rnacontrol.Object3DGraphController;
import tools3d.objects3d.Object3D;

/** lets user choose to partner object, inserts correspnding link into controller */
public class InsertLinkWizard implements GeneralWizard {

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    private boolean finished = false;
    private JFrame frame = null;
    private Object3DTreePanel leftTreePanel;
    private Object3DTreePanel rightTreePanel;
    private List<ActionListener> actionListeners = new ArrayList<ActionListener>();
    private Object3DGraphController graphController;
    public static final String FRAME_TITLE = "Insert Link Wizard";

    private class DoneListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    Object3D o1 = leftTreePanel.getLastSelected();
	    Object3D o2 = rightTreePanel.getLastSelected();
	    if ((o1 != null) && (o2 != null)) {
 		graphController.getLinks().addLink(o1, o2);
		frame.setVisible(false);
		frame = null;
		finished = true;
	    }
	}
    }

    private class CancelListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    frame.setVisible(false);
	    frame = null;
	    finished = true;
	}
    }

    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    public boolean isFinished() {
	return finished;
    }

    /** generates and inserts new Object3D according to user dialog */
    public void launchWizard(Object3DGraphController graphController,
			     Component parentFrame) {
	if (graphController == null) {
	    log.info("GraphController received by launchWizard is null!");
	}
	this.graphController = graphController;
	frame = new JFrame(FRAME_TITLE);
	addComponents(frame);
	frame.pack();
	frame.setVisible(true);
    }

    public void addComponents(JFrame _frame) {
	Container f = _frame.getContentPane();
	f.setLayout(new BorderLayout());
	JPanel center = new JPanel();
	center.setLayout(new FlowLayout());
	if (graphController == null) {
	    log.info("GraphController is null before generating left and right tree panels!");
	}
	leftTreePanel = new Object3DTreePanel(graphController.getGraph());
	rightTreePanel = new Object3DTreePanel(graphController.getGraph());
	center.add(leftTreePanel);
	center.add(rightTreePanel);
	f.add(center, BorderLayout.CENTER);
	JPanel bottomPanel = new JPanel();
	JButton button = new JButton("Cancel");
	button.addActionListener(new CancelListener());
	bottomPanel.add(button);
  	button = new JButton("Done");
	button.addActionListener(new DoneListener());
	bottomPanel.add(button);
	f.add(bottomPanel, BorderLayout.SOUTH);
    }

}
