package rnadesign.designapp.rnagui;

import java.awt.*;
import javax.swing.JFrame;

import launchtools.QueueManager;
import launchtools.QueueManagerChangeEvent;
import launchtools.QueueManagerChangeListener;

public class JobViewer extends JFrame {

    private QueueManager queueManager;

    private class LocalStatusChangeListener 
	implements QueueManagerChangeListener {

	public void statusChanged(QueueManagerChangeEvent event) {
	    repaint();
	}

    }

    public JobViewer(QueueManager queueManger) {
	super("Status of Queue Manager");
	setPreferredSize(new Dimension(200,200));
	this.queueManager = queueManager;
	setBackground(Color.white);
	// setPreferredSize(new Dimension(300, 300));
	if (queueManager != null) {
	    this.queueManager.addQueueManagerChangeListener(new LocalStatusChangeListener());
	}
    }

}
