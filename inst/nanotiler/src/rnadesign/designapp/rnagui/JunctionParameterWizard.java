package rnadesign.designapp.rnagui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import rnadesign.designapp.rnagui.FileExtensionException;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.PdbJunctionControllerParameters;
import rnadesign.rnamodel.CorridorDescriptor;
import rnadesign.rnamodel.JunctionScanner;
import tools3d.Vector3D;

/**
 * Lets user choose to partner object,
 * inserts corresponding link into controller
 */
public class JunctionParameterWizard implements GeneralWizard {

    private static final String SLASH = File.separator;
    private static final int COL_SIZE_DOUBLE = 8; 
    private static final int COL_SIZE_INT = 5; 
    private static final int COL_SIZE_NAME = 20; 
    private static final int COL_SIZE_FILENAME = 50; 
    private boolean finished = false;

    public static Logger log = Logger.getLogger("NanoTiler_debug");
 
    private JFrame frame = null;
    private JTextField corridorRadiusField;
    private JTextField corridorStartField;
    private JTextField offsetField;
    private JTextField orderMaxField;
    private JTextField fileNameField;
    private JTextField writeNameField;
    // start position:
    private JCheckBox writeBox;
    private boolean noWriteFlag = true;
    private List<ActionListener> actionListeners = new ArrayList<ActionListener>();
    private Object3DGraphController graphController;
    private boolean rnaMode = true;
    private int branchDescriptorOffset = 2;
    private double corridorRadius = 5.0;
    private double corridorStart = 5.0;
    private int loopLengthSumMax = JunctionScanner.LOOP_LENGTH_SUM_MAX;
    private int orderMax = 5;
    private String name = "pdb";
    private String fileName;
    private int stemLengthMin = 3; // currently fixed
    private String writeName;
    private String usedDirectory;
    private String writeDir;
    private boolean intHelixCheck = true; // TODO: add to GUI
    private boolean motifMode = false; // TODO: add to GUI
    public static final String FRAME_TITLE = "Junction Parameter Wizard";
    public static final String NAMES_EXTENSION = ".names";
    public static final String NTL_EXTENSION = ".ntl";
    
    private class DoneListener implements ActionListener{
	public void actionPerformed(ActionEvent e) {
	    try {
		readOutValues(); 

		// open file
		PdbJunctionControllerParameters prm =
		    new PdbJunctionControllerParameters(orderMax,
							fileName,
							usedDirectory,
							writeDir,
							noWriteFlag,
							rnaMode,
							branchDescriptorOffset,
							stemLengthMin,
							new CorridorDescriptor(corridorRadius,
									       corridorStart),
							loopLengthSumMax, intHelixCheck, motifMode);
		// PdbJunctionController junctionController = 
		//(PdbJunctionController)(graphController.getJunctionController());
		graphController.resetJunctionController(prm); 
		frame.setVisible(false);
		frame = null;
		finished = true;
		graphController.setLastReadDirectory(usedDirectory);
	    }
	    catch (FileExtensionException fee) { log.severe("Threw a FileExtensionException"); }
	}
    }
    
    private class CancelListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    frame.setVisible(false);
	    frame = null;
	    finished = true;
	}
    }
    
    class NamesFileFilter extends javax.swing.filechooser.FileFilter {
	public boolean accept(File f) {
	    return f.isDirectory() || f.getName().toLowerCase().endsWith(NAMES_EXTENSION);
	}
	public String getDescription() {
	    return ".names files";
	}
    }

    class DirFileFilter extends javax.swing.filechooser.FileFilter {
	public boolean accept(File f) {
	    return f.isDirectory();
	}
	public String getDescription() {
	    return "directories";
	}
    }

    private class FileBrowseActionListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
    	    JFileChooser chooser = new JFileChooser();
	    NamesFileFilter filter = new NamesFileFilter();
	    chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
	    chooser.setFileFilter(filter);
    	    String cwdName = graphController.getLastReadDirectory();
    	    File cwd = new File(cwdName);
    	    
    	    chooser.setCurrentDirectory(cwd);
    	    int returnVal = chooser.showOpenDialog(frame); // parent);
    	    
    	    if (returnVal == JFileChooser.APPROVE_OPTION) {
		fileName = chooser.getCurrentDirectory()
		    + SLASH + chooser.getSelectedFile().getName();
		fileNameField.setText(fileName);
		try {
		    writeName = fileName.substring(0, fileName.indexOf("rnano4")) +
			"rnano4" + SLASH + "db" + SLASH + "nanotiler" + SLASH;
		}
		catch (StringIndexOutOfBoundsException se) { //fileName does not contain "rnano4"
		    writeName = "";
		}
		writeNameField.setText(writeName);
		try {
		    usedDirectory = chooser.getCurrentDirectory().getCanonicalPath();
		}
		catch (IOException ioe) {
		    log.severe("getCanonicalPath threw: " + ioe);
		}
		log.info("You chose this file: " + fileName);
		
    	    }
	}
    }

    private class WriteBrowseActionListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
    	    JFileChooser chooser = new JFileChooser();
	    DirFileFilter filter = new DirFileFilter();
	    chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
	    chooser.setFileFilter(filter);
    	    String cwdName = graphController.getLastReadDirectory();
	    File cwd = new File(cwdName);
    	    
	    chooser.setCurrentDirectory(cwd);
    	    int returnVal = chooser.showOpenDialog(frame); // parent);
    	    
    	    if (returnVal == JFileChooser.APPROVE_OPTION) {
		writeName = chooser.getCurrentDirectory() +
		    SLASH + chooser.getSelectedFile().getName();
		writeNameField.setText(writeName);
		try {
		    writeDir = chooser.getCurrentDirectory().getCanonicalPath();
		}
		catch (IOException ioe) {
		    log.severe("getCanonicalPath threw: " + ioe);
		}
		log.info("You chose this file: " + writeName);
    	    }
	}
    }
    
    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }
    
    public boolean isFinished() {
	return finished;
    }
    
    /** generates and inserts new Object3D according to user dialog */
    public void launchWizard(Object3DGraphController graphController,
			     Component parentFrame) {
	if (graphController == null) {
	    log.info("GraphController received by launchWizard is null!");
	}
	this.graphController = graphController;
	frame = new JFrame(FRAME_TITLE);
	addComponents(frame);
	frame.pack();
	frame.setVisible(true);
    }
    
    public void addComponents(JFrame _frame) {
	Container f = _frame.getContentPane();
	f.setLayout(new BorderLayout());
	JPanel center = new JPanel();
	center.setLayout(new BoxLayout(center, BoxLayout.Y_AXIS));

	JPanel topPanel = new JPanel();
	JPanel sPanel = new JPanel();
	JPanel checkPanel = new JPanel();
	
	orderMaxField = new JTextField("" + orderMax, 1);
	offsetField = new JTextField("" + branchDescriptorOffset, 1);
	corridorRadiusField = new JTextField("" + corridorRadius, COL_SIZE_DOUBLE);
	corridorStartField = new JTextField("" + corridorStart, COL_SIZE_DOUBLE);
	fileNameField = new JTextField(fileName, COL_SIZE_FILENAME);
	writeNameField = new JTextField(writeName, COL_SIZE_FILENAME);
	
	JPanel filePanel = new JPanel();
	filePanel.add(new JLabel("Filename:"));
	filePanel.add(fileNameField);
	JButton button = new JButton("Browse");
	button.addActionListener(new FileBrowseActionListener());
	filePanel.add(button);
	center.add(filePanel);

	JPanel writePanel = new JPanel();
	writePanel.add(new JLabel("Write to:"));
	writePanel.add(writeNameField);
	button = new JButton("Browse");
	button.addActionListener(new WriteBrowseActionListener());
	writePanel.add(button);
	center.add(writePanel);
	JPanel noWritePanel = new JPanel();
	writeBox = new JCheckBox("Write junctions to file", !noWriteFlag);
	noWritePanel.add(writeBox);
	center.add(noWritePanel);

	sPanel.add(new JLabel("Max. Order:"));
	sPanel.add(orderMaxField);
	sPanel.add(new JLabel(" Offset:"));
	sPanel.add(offsetField);
	sPanel.add(new JLabel(" Corridor radius:"));
	sPanel.add(corridorRadiusField);
	sPanel.add(new JLabel(" Corridor start:"));
	sPanel.add(corridorStartField);
	
	center.add(sPanel);
	center.add(checkPanel);
	
	JPanel bottomPanel = new JPanel();
	button = new JButton("Cancel");
	button.addActionListener(new CancelListener());
	bottomPanel.add(button);
  	button = new JButton("Done");
	button.addActionListener(new DoneListener());
	bottomPanel.add(button);
	f.add(topPanel, BorderLayout.NORTH);
	f.add(bottomPanel, BorderLayout.SOUTH);
	f.add(center, BorderLayout.CENTER);
    }
    
    private void readOutValues() throws FileExtensionException {
	orderMax = Integer.parseInt(orderMaxField.getText().trim()) + 1;
	branchDescriptorOffset = Integer.parseInt(offsetField.getText().trim());
	corridorRadius = Double.parseDouble(corridorRadiusField.getText().trim());
	corridorStart = Double.parseDouble(corridorStartField.getText().trim());
	fileName = fileNameField.getText().trim();
	writeDir = writeNameField.getText().trim();
	noWriteFlag = !(writeBox.isSelected());
	if (!fileName.endsWith(NAMES_EXTENSION)) {
	    throw new FileExtensionException(NAMES_EXTENSION);
	}
// 	if (!(new File(writeDir)).isDirectory() && noWriteFlag) {
// 	    throw new FileExtensionException(".dir");
// 	}
    }
    
}
