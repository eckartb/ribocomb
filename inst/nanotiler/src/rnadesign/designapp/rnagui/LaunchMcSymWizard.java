package rnadesign.designapp.rnagui;

import java.awt.Component;
import java.awt.event.*;
import java.io.*;
import java.util.logging.Logger;

import guitools.BeanEditor;
import guitools.XMLBeanEditor;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnamodel.RnaStrand;
import rnadesign.rnamodel.SimpleRnaStrand;
import sequence.DnaTools;
import sequence.Sequence;
import sequence.SimpleSequence;
import sequence.UnknownSymbolException;
import tools3d.Vector3D;

/** generate and insert new RNA strand according to user dialog */
public class LaunchMcSymWizard implements GeneralWizard {

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    Object3DGraphController controller;
    BeanEditor beanEditor = new XMLBeanEditor();
    // Set actionListeners = new HashSet();
    Component frame;

    /** if "Done" was pressed, insert Object3D into graph! */
    private class DoneActionListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    log.fine("Calling DoneActionListener in RnaStrandWizard!");
	    if (isFinished()) {
		log.fine("Calling DoneActionListener in LaunchMcSymWizard: text editor was finished!");
//  		Object3DHandle object3DHandle = getObject3DHandle();
// 		if ((object3DHandle != null) && object3DHandle.isValid()) {
// 		    controller.addGraph(object3DHandle);
// 		}
// 		else {
// 		}
		// 	    Iterator iterator = actionListeners.iterator();
		// 	    while (iterator.hasNext()) {
		// 		ActionListener listener = (ActionListener)(iterator.next());
		// 		listener.actionPerformed(e);
		// 	    }
	    }
	    else {
		log.fine("Calling DoneActionListener in LaunchMcSymWizard: text editor was NOT finished!");
	    }
	    if (frame != null) {
		frame.repaint();
	    }
	    log.fine("RnaStrandWizard finished!");
	}
    }

    public void addActionListener(ActionListener listener) {
	beanEditor.addActionListener(listener);
    }

    /** generates and inserts new Object3D according to user dialog */
    public void launchWizard(Object3DGraphController controller,
			     Component frame) {
	this.controller = controller;
	this.frame = frame;
 	RnaStrand strand = new SimpleRnaStrand();
 	initStrand(strand);
	
	// generate output stream that ends in string:
	// 	beanEditor = new XMLBeanEditor();
	// strand = (RnaStrand)beanEditor.launchEdit(strand, null, frame);
	beanEditor.addActionListener(new DoneActionListener());
	beanEditor.launchEdit(strand, frame);
    }

    private Sequence generateInitSequence() {
	String s = "NNNNNNNNNNNNNNNNNNNN";
	String name = "unnamed";
	Sequence result = null;
	try {
	    result = new SimpleSequence(s, name, 
					DnaTools.AMBIGUOUS_RNA_ALPHABET);
	}
	catch (UnknownSymbolException e) {
	    // nothing
	}
	return result;
    }

    /** initialize RNA strand: better move this code to model package or at least controller */
    private void initStrand(RnaStrand strand) {
	assert false; // currently not implemented
	// strand.setSequence(generateInitSequence());
	strand.setRelativePosition(new Vector3D(10.0, 20.0, 10.0));
    }

    public boolean isFinished() { 
	return beanEditor.isFinished(); 
    }

}
