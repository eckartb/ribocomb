package rnadesign.designapp.rnagui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JCheckBox;

// import rnadesign.rnacontrol.Object3DGraphController;
import sequence.DuplicateNameException;
import sequence.UnknownSymbolException;
import commandtools.CommandApplication;
import commandtools.CommandException;

/** lets user choose to partner object, inserts correspnding link into controller */
public class LaunchRnaInverseWizard implements Wizard {

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    private boolean finished = false;
    private JFrame frame = null;
    private JTextField stepField;
    private java.util.List<ActionListener> actionListeners = new ArrayList<ActionListener>();
    private static String[] algorithmNames = {"RNAinverseGroup v1"};
    private boolean importFlag = true;
    private JCheckBox importBox;
    private double rmsLimit = 3.0; // TODO : allow adjusting in window
    // private Object3DGraphController graphController;
    private CommandApplication application;
    public static final String FRAME_TITLE = "Sequence optimization Wizard";
    
    public LaunchRnaInverseWizard(CommandApplication application) {
	assert application != null;
	this.application = application;
    }

    private class DoneListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    readOutValues();
	    if (isValid()) {
		String command = "optimize_sequences import=" + importFlag + " rms=" + rmsLimit;
		try {
		    // graphController.optimizeSequences(importFlag, rmsLimit); // TODO in future make accepting optimized sequences optional
		    application.runScriptLine(command);
		    cleanUp(); // remove window
		}
		catch (CommandException ce) {
		    JOptionPane.showMessageDialog(frame, 
						  "Error executing command: " + command + " : " + ce.getMessage());
		}
	    }
	    else {
		JOptionPane.showMessageDialog(frame, "The current parameters are not ok!");
	    }
	}
    }

    private class CancelListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    cleanUp();
	}
    }

    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    public void cleanUp() {
	if (frame != null) {
	    frame.setVisible(false);
	    frame = null;
	}
	finished = true;
    }

    public boolean isFinished() {
	return finished;
    }

    /** generates and inserts new Object3D according to user dialog */
    public void launchWizard(Component parentFrame) {
	// this.graphController = graphController;
	// obtains current set of sequences:
	boolean result = checkValues();
	if (result) {
	    frame = new JFrame(FRAME_TITLE);
	    addComponents(frame);
	    frame.pack();
	    frame.setVisible(true);
	}
	else {
	    JOptionPane.showMessageDialog(parentFrame, "No objects defined so far!");
	}
    }

    public void addComponents(JFrame _frame) {
	Container f = _frame.getContentPane();
	f.setLayout(new BorderLayout());
	
	JPanel center = new JPanel();
	center.setLayout(new FlowLayout());
	center.setPreferredSize(new Dimension(400, 400));
	// f.add(center, BorderLayout.CENTER);
	// algorithmChoice = new JComboBox(algorithmNames);
	JPanel middlePanel = new JPanel();
	// stepField = new JTextField("" + numSteps,6);
	middlePanel.setPreferredSize(new Dimension(300, 200));
	middlePanel.setLayout(new FlowLayout());
	// middlePanel.add(algorithmChoice);
	middlePanel.add(new JLabel("Launch sequence optimization? "));
	// middlePanel.add(stepField);
	JPanel topPanel = new JPanel();
	topPanel.setLayout(new FlowLayout());
	importBox = new JCheckBox();
	topPanel.add(new JLabel("Import sequences:"));
	topPanel.add(importBox);
	JPanel bottomPanel = new JPanel();
	bottomPanel.setPreferredSize(new Dimension(300, 200));
	JButton button = new JButton("Cancel");
	button.addActionListener(new CancelListener());
	bottomPanel.add(button);
  	button = new JButton("Launch");
	button.addActionListener(new DoneListener());
	bottomPanel.add(button);
	JPanel southPanel = new JPanel();
	southPanel.add(middlePanel);
	southPanel.add(bottomPanel);
	f.add(topPanel, BorderLayout.NORTH);
	f.add(southPanel, BorderLayout.SOUTH);
    }

    /** check if values obtained from controller are ok */
    private boolean checkValues() {
	// return (graphController.getGraph().getObjectCount() > 0);
	return true; // TODO : activatve meaningful checks
    }

    /** reads values from mask */
    private void readOutValues() {
	importFlag = importBox.isSelected();
    }

    /** returns true if currently set values could correspond to a valid stem */
    private boolean isValid() {
	readOutValues();
	return true;
    }

}
