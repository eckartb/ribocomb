package rnadesign.designapp.rnagui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import commandtools.CommandApplication;
import commandtools.CommandException;
import rnadesign.designapp.AbstractDesigner;
import rnadesign.rnacontrol.Object3DController;
import rnadesign.rnacontrol.Object3DGraphController;
import tools3d.objects3d.Object3D;

/**
 * GUI Implementation of shift command.
 * Shifts selection, all, or new object x y z.
 *
 * @author Christine Viets
 */
public class ManualRotateWizard implements Wizard {

    public static Logger log = Logger.getLogger("NanoTiler_debug");
    private static final int COL_SIZE_SELECTION = 20; //TODO: size?
    private static final int COL_SIZE_DOUBLE = 8; //TODO: make public? One constants class?

    private boolean finished = false;
    private JFrame frame = null;
    private List<ActionListener> actionListeners = new ArrayList<ActionListener>();
    public static final String FRAME_TITLE = "Rotate Wizard";
    private JTextField selectionField;
    private JTextField angleField;
    private JTextField xField;
    private JTextField yField;
    private JTextField zField;
    private CommandApplication application;
    private List<String> rotateHistory; //commandHistory
    private List<String> selectHistory;

    /**
     * Constructor.
     *
     * @param application NanoTiler
     */
    public ManualRotateWizard(CommandApplication application) {
	assert application != null;
	this.application = application;
    }

    private class RotateButtonListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    String selectCommand = "select " + selectionField.getText().trim();
	    String command = "rotate " + angleField.getText().trim() + " " + xField.getText().trim() + " " + yField.getText().trim() + " " + zField.getText().trim();
	    try {
		application.runScriptLine(selectCommand);
		selectHistory.add(selectCommand);
		application.runScriptLine(command);
		rotateHistory.add(command);
	    }
	    catch (CommandException ce) {
		JOptionPane.showMessageDialog(frame, "Error executing command: " +
					      selectCommand + " or " + command + " : " + ce.getMessage());
	    }
	}
    }

    /**
     * Called when "Done" button is pressed.
     * Window disappears.
     *
     * @author Christine Viets
     */
    private class DoneListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    frame.setVisible(false);
	    frame = null;
	    finished = true;
	}
    }

    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    public boolean isFinished() {
	return finished;
    }

    /** Creates a window. */
    public void launchWizard(Component parentFrame) {
	rotateHistory = new ArrayList<String>();
	selectHistory = new ArrayList<String>();
	frame = new JFrame(FRAME_TITLE);
	addComponents(frame);
	frame.pack();
	frame.setVisible(true);
    }

    /** Adds the components to the window. */
    public void addComponents(JFrame _frame) {
	Container f = _frame.getContentPane();
	f.setLayout(new BorderLayout());
	JPanel top = new JPanel();
	top.setLayout(new FlowLayout());
	selectionField = new JTextField(getSelectionString(), COL_SIZE_SELECTION);
	top.add(new JLabel("Rotate:"));
	top.add(selectionField);
	JPanel rotatePanel = new JPanel();
	rotatePanel.setLayout(new FlowLayout());
	angleField = new JTextField("0.0", COL_SIZE_DOUBLE);
	xField = new JTextField("0.0", COL_SIZE_DOUBLE);
	yField = new JTextField("0.0", COL_SIZE_DOUBLE);
	zField = new JTextField("0.0", COL_SIZE_DOUBLE);
	rotatePanel.add(new JLabel("Rotate:  angle:"));
	rotatePanel.add(angleField);
	rotatePanel.add(new JLabel(" x:"));
	rotatePanel.add(xField);
	rotatePanel.add(new JLabel(" y:"));
	rotatePanel.add(yField);
	rotatePanel.add(new JLabel(" z:"));
	rotatePanel.add(zField);
	JPanel bottomPanel = new JPanel();
	JButton button = new JButton("Rotate");
	button.addActionListener(new RotateButtonListener());
	bottomPanel.add(button);
	button = new JButton("Close");
	button.addActionListener(new DoneListener());
	bottomPanel.add(button);
	f.add(top, BorderLayout.NORTH);
	f.add(rotatePanel, BorderLayout.CENTER);
	f.add(bottomPanel, BorderLayout.SOUTH);
    }

    /**
     * Returns String representation of what will be shifted by default:
     * the current selection's full name or
     * "all" if nothing is selected.
     */
    private String getSelectionString() {
	Object3DGraphController graphController = ((AbstractDesigner)application).getGraphController();
	Object3DController gControl = graphController.getGraph();
	Object3D selection = gControl.getSelectionRoot();
	if (selection == null) {
	    return "all";
	}
	String selectionName = selection.getName();
	Object3D curr = selection;
	while (curr.getParent() != null) {
	    curr = curr.getParent();
	    selectionName = curr.getName() + "." + selectionName;
	}
	return selectionName;
    }

}
