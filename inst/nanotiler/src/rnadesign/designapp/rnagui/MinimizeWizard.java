package rnadesign.designapp.rnagui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import rnadesign.rnacontrol.InvalidParametersException;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerConstants;

/** lets user choose to partner object, inserts correspnding link into controller */
public class MinimizeWizard implements GeneralWizard {

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    private boolean finished = false;
    private JFrame frame = null;
    private JTextField stepField;
    private java.util.List<ActionListener> actionListeners = new ArrayList<ActionListener>();
    private int numSteps = 100;
    private double scale = 1.0;
    private static String[] algorithmNames = {"Monte Carlo",
					      "Elastic Network Extrapolation"};
    private int algorithmId = 0;
    private JComboBox algorithmChoice;
    private Object3DGraphController graphController;
    public static final String FRAME_TITLE = "Minimization setup Wizard";
    
    private class DoneListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    readOutValues();
	    if (isValid()) {
		try {
		    int algorithmCode = 0;
		    switch (algorithmId) {
			case 0:
			    algorithmCode = Object3DGraphControllerConstants.MONTE_CARLO;
			    break;
			case 1:
			    algorithmCode = Object3DGraphControllerConstants.ELASTIC_NETWORK_EXTRAPOLATION;
			    break;
		    }
		    graphController.minimize(numSteps, scale, algorithmCode);
		    cleanUp();
		}
		catch (InvalidParametersException exception) {
		    JOptionPane.showMessageDialog(frame, "The current parameters were not ok!");
		}
	    }
	    else {
		JOptionPane.showMessageDialog(frame, "The current parameters are not ok!");
	    }
	}
    }

    private class CancelListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    cleanUp();
	}
    }

    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    public void cleanUp() {
	if (frame != null) {
	    frame.setVisible(false);
	    frame = null;
	}
	finished = true;
    }

    public boolean isFinished() {
	return finished;
    }

    /** generates and inserts new Object3D according to user dialog */
    public void launchWizard(Object3DGraphController graphController,
			     Component parentFrame) {
	if (graphController == null) {
	    log.info("GraphController received by launchWizard is null!");
	}
	this.graphController = graphController;
	// obtains current set of sequences:
	boolean result = checkValues();
	if (result) {
	    frame = new JFrame(FRAME_TITLE);
	    addComponents(frame);
	    frame.pack();
	    frame.setVisible(true);
	}
	else {
	    JOptionPane.showMessageDialog(parentFrame, "No objects defined so far!");
	}
    }

    public void addComponents(JFrame _frame) {
	Container f = _frame.getContentPane();
	f.setLayout(new BorderLayout());
	JPanel center = new JPanel();
	center.setLayout(new FlowLayout());
	center.setPreferredSize(new Dimension(400, 400));
	if (graphController == null) {
	    log.info("GraphController is null before generating left and right tree panels!");
	}
	// f.add(center, BorderLayout.CENTER);
	algorithmChoice = new JComboBox(algorithmNames);
	JPanel middlePanel = new JPanel();
	stepField = new JTextField("" + numSteps,6);
	middlePanel.setPreferredSize(new Dimension(300, 200));
	middlePanel.setLayout(new FlowLayout());
	middlePanel.add(algorithmChoice);
	middlePanel.add(new JLabel("Number of steps: "));
	middlePanel.add(stepField);
	JPanel bottomPanel = new JPanel();
	bottomPanel.setPreferredSize(new Dimension(300, 200));
	JButton button = new JButton("Cancel");
	button.addActionListener(new CancelListener());
	bottomPanel.add(button);
  	button = new JButton("Done");
	button.addActionListener(new DoneListener());
	bottomPanel.add(button);
	JPanel southPanel = new JPanel();
	southPanel.add(middlePanel);
	southPanel.add(bottomPanel);
	f.add(southPanel, BorderLayout.SOUTH);
    }

    /** check if values obtained from controller are ok */
    private boolean checkValues() {
	return (graphController.getGraph().getObjectCount() > 0);
    }

    /** reads values from mask */
    private void readOutValues() {
	algorithmId = algorithmChoice.getSelectedIndex();
	numSteps = Integer.parseInt(stepField.getText().trim());
    }

    /** returns true if currently set values could correspond to a valid stem */
    private boolean isValid() {
	readOutValues();
	return ((numSteps >= 0) && (scale > 0));
    }

}
