/*
 * NanoTiler: started from BorderLayoutDemo.java 
 */

package rnadesign.designapp.rnagui;

import viewer.deprecated.Cylinder;
import viewer.display.*;
import viewer.graphics.*;
import viewer.rnadesign.*;
import javax.swing.*;
import sequence.UnknownSequenceException;
import rnasecondary.RnaInteractionType;
import tools3d.objects3d.Object3DTools;
import rnadesign.rnamodel.Residue3D;
import rnasecondary.InteractionSet;
import sequence.UnknownSymbolException;
import sequence.DnaTools;
import sequence.UnevenAlignment;
import sequence.UnknownSequenceException;
import sequence.DuplicateNameException;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.XMLEncoder;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.ResourceBundle;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JSeparator;
import javax.swing.Scrollable;
import javax.swing.SwingConstants;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

import commandtools.*;
import controltools.ModelChangeEvent;
import controltools.ModelChangeListener;
import generaltools.StringTools;
import launchtools.QueueManager;
import launchtools.SimpleQueueManager;
import rnadesign.designapp.NanoTilerInterpreter;
import rnadesign.designapp.NanoTilerScripter;
import rnadesign.rnacontrol.BindingSiteController;
import rnadesign.rnacontrol.CameraController;
import rnadesign.rnacontrol.JunctionController;
import rnadesign.rnacontrol.LinkController;
import rnadesign.rnacontrol.Object3DController;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerConstants;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import rnadesign.rnacontrol.PdbJunctionController;
import rnadesign.rnacontrol.SimpleCameraController;
import rnadesign.rnacontrol.SequenceController;
//import rnadesign.rnamodel.DnaStrand;
import rnadesign.rnamodel.GeneralPdbWriter;
import rnadesign.rnamodel.JunctionCollector;
//import rnadesign.rnamodel.ProteinStrand;
import rnadesign.rnamodel.RnaStrand;
import rnasecondary.SecondaryStructureScriptFormatWriter;
import rnadesign.rnamodel.SequenceBindingSite;
import rnasecondary.SecondaryStructure;
import sequence.DuplicateNameException;
import sequence.Sequence;
import sequence.SequenceSubset;
import rnasecondary.MutableSecondaryStructure;
import tools3d.SimpleCamera;
import tools3d.Vector3D;
import tools3d.Vector4D;
import tools3d.objects3d.Link;
import tools3d.objects3d.LinkSet;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DDefaultWriter;
import tools3d.objects3d.Object3DIOException;
import tools3d.objects3d.Object3DLinkSetBundle;
import tools3d.objects3d.Object3DWriter;
import tools3d.objects3d.SimpleLinkSet;
import tools3d.objects3d.SimpleObject3DLinkSetBundle;
import viewer.graphics.MeshRenderer;
import viewer.graphics.Sphere;
import viewer.graph.rna.RnaRendererGraphController;

import static rnadesign.designapp.rnagui.PackageConstants.*;

public class NanoTiler extends NanoTilerScripter implements ModelChangeListener {

	private static ResourceBundle rb = ResourceBundle.getBundle("NanoTiler");

	public static final String WIKI_PAGE = rb.getString("wikihome"); // "http://knot.ncifcrf.gov:8000/NanoTilerHome";

	//     public static int numberOfJunctions; //this is not actually the number of junctions, but the number of PDB files. FIX IT!
	//     public static int numberOfKissingLoops = 0;
	//     public static String[] pdbFileNames;
	//     public static String junctionString; //Strings used by PdbJunctionController to store junction and kissing loop DB info
	//     public static String kissingLoopString; 

	//    public static String[] varArray;
    public static boolean developmentVersion = false;
	public static final String PDB_EXTENSION = ".pdb";
	public static final String NAMES_EXTENSION = ".names";
	public static final String POINTS_EXTENSION = ".points";
	public static String nanotilerHome = System.getenv("NANOTILER_HOME"); // System.getProperty("NANOTILER_HOME"); // read environment variable
        public static final String TUTORIAL_HOME = "file://"+nanotilerHome+"/doc/nanotiler_tutorial.html";
     	private static final String LOD_COARSE = "Coarse";
	private static final String LOD_MEDIUM = "Medium";
	private static final String LOD_FINE = "Fine";

	private JunctionController junctionController = new PdbJunctionController();//CV

	public static String tilingStatisticsString = new String("");

	// controller parameters:
	private RnaControllerParameters cparams = new RnaControllerParameters();
	// model parameters:
	private RnaModelParameters mparams = new RnaModelParameters();
	// gui parameters:
	private RnaGuiParameters params = new RnaGuiParameters();

	private GraphControllerDisplayManager graphControllerDisplayManager;

	private JFrame rootFrame;

	private List<Component> frames = new ArrayList<Component>();

	/** keeps track o		log.fine("Clone wizard activated");
f all active GraphControlPanels */
	private List<GraphControlPanel> graphControllPanels = new ArrayList<GraphControlPanel>();

	private static Logger log;

	public static boolean RIGHT_TO_LEFT = false;

	NanoTiler nanotilerApp; // workaround because referece is needed at time when "this" has different value

	/** this factory object creates shape painters typical for this application */
	public GeometryPainterFactory geometryPainterFactory = new NanoTilerGeometryPainterFactory();

        /** determines whether opengl graphics or java graphics will be used */
        public boolean openGLGraphics = false;

	/* LISTENERS */

	/** removes all data from project. Not correctly working yet */
	private final class ClearActionListener implements ActionListener {	
		public void actionPerformed(ActionEvent e) {
			log.fine("Clear button pressed!");
			String msg = "Are you sure you want to discard all data?";
			String title = "Clear Button Dialog";
			int choice = JOptionPane.showConfirmDialog(null,
					msg, title, JOptionPane.OK_CANCEL_OPTION);
			if (choice == 0) {
				log.fine("clear button!");
				graphController.clear();
				repaintApp();
			}
		}
	} // ClearActionListener

	private final class ConnectActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			GeneralWizard linkWizard = new InsertLinkWizard();
			if (graphController == null) {
				log.info("Graphcontroller is null before starting linkWizard!");
			}
			else {
				log.fine("Graphcontroller is not null before starting linkWizard!");
			}
			linkWizard.launchWizard(graphController, rootFrame);
		}
	}

	/** removes currently selected objects */
	private final class RemoveActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			// GeneralWizard wizard = new RemoveWizard();

			if (graphController == null) {
				log.info("Graphcontroller is null before remove command!");
			}
			else {
				graphController.getGraph().remove(); // remove currently selected objects
			}
			// 		JOptionPane.showConfirmDialog(null,
			// 					      msg, "Bad rotation parameter!", JOptionPane.ERROR_MESSAGE);
			// wizard.launchWizard(graphController, rootFrame);
		}
	}

	private final static class ExitActionListener implements ActionListener {	
		public void actionPerformed(ActionEvent e) {
			log.fine("Quit button pressed!");
			log.fine("quit button!");
			String msg = "Are you sure you want to quit the program?";
			String title = "Exit dialog";
			int choice = JOptionPane.showConfirmDialog(null,
					msg, title, JOptionPane.OK_CANCEL_OPTION);
			if (choice == 0) {
				System.exit(0);
			}
		}
	} // ExitActionListener

	private final class HierarchyUpListener implements ActionListener {	
		public void actionPerformed(ActionEvent e) {
			log.fine("HierarchyUp button pressed!");
			log.fine("HierarchyUp button: " + graphController.getGraph().getDepthMax());
			if (graphController.getGraph().getDepthMax() > 0) {
				graphController.getGraph().setDepthMax(graphController.getGraph().getDepthMax()-1);
			}
			log.fine("After: HierarchyUp button: " + graphController.getGraph().getDepthMax());
			repaintApp();
		}
	} 

	private final class HierarchyDownListener implements ActionListener {	
		public void actionPerformed(ActionEvent e) {
			log.fine("HierarchyDown button pressed!");
			log.fine("HierarchyDown button: " + graphController.getGraph().getDepthMax());
			graphController.getGraph().setDepthMax(graphController.getGraph().getDepthMax()+1);
			log.fine("After: HierarchyDown button: " + graphController.getGraph().getDepthMax());
			repaintApp();
		}
	} 


	private final class LaunchMcSymListener implements ActionListener {	

		public void actionPerformed(ActionEvent e) {

			log.fine("LaunchMcSymListener activated!");
			GeneralWizard wizard = new LaunchMcSymWizard();
			wizard.launchWizard(graphController, rootFrame);
		}

	}    

	private final class LaunchRnaInverseListener implements ActionListener {	

		private CommandApplication app;

		LaunchRnaInverseListener(CommandApplication app) {
			this.app = app;
		}

		public void actionPerformed(ActionEvent e) {
			log.fine("LaunchRnaInverseListener activated!");
			Wizard wizard = new LaunchRnaInverseWizard(this.app);
			wizard.launchWizard(rootFrame);
		}

	}    

	private final class LoadActionListener implements ActionListener {	
		public void actionPerformed(ActionEvent e) {
			log.fine("Load button pressed!");
			// creade window for reading file name:
			JFileChooser chooser = new JFileChooser();
			// Note: source for ExampleFileFilter can be found in FileChooserDemo,
			// under the demo/jfc directory in the JDK.
			//    	    ExampleFileFilter filter = new ExampleFileFilter();
			//    	    filter.addExtension("jpg");
			//    	    filter.addExtension("gif");
			//    	    filter.setDescription("JPG & GIF Images");
			//    	    chooser.setFileFilter(filter);
			// returns complete path names
			// chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			// TODO: not clean, change to variable:
			String cwdName = params.workDirectory;
			File cwd = new File(cwdName);
			chooser.setCurrentDirectory(cwd);
			int returnVal = chooser.showOpenDialog(rootFrame.getContentPane()); // parent);
			if(returnVal == JFileChooser.APPROVE_OPTION) {
				String fileName = chooser.getCurrentDirectory().toString() + java.io.File.separatorChar + chooser.getSelectedFile().getName().toString(); // won't run on Windows! fix!  -- CV fixed, should run on windows now -- TEST
				log.info("You chose this file: " + fileName);
				// open file
				try {
					FileInputStream fis = new FileInputStream(fileName);
					graphController.read(fis);
				}
				catch (Object3DIOException exc) {
					// TODO replace with better error window
					log.severe("Could not reading graph file: " + exc.getMessage());
				}
				catch (IOException exc) {
					// TODO replace with better error window
					log.severe("Could not open file: " + exc.getMessage());
				}
			}

			// read file and create graph
			repaintApp(); // update graphics window   
		}
	} // LoadActionListener

	/** whenevener the "model" has changed, the controller fires a ModelChangeEvent to all its registered listeners */
    // private final class LocalModelChangeListener implements ModelChangeListener {
    public void modelChanged(ModelChangeEvent e) {
	repaintApp();
    }
    // }

	/** generates single 3D object using input mask and adds it to hierarchy */
	private final class InputObject3DActionListener implements ActionListener { 
		public void actionPerformed(ActionEvent e) {
			Object3DInputMask inputMask = new Object3DInputMask(rootFrame, 
					graphController.getGraph().getGraph(), params);
			inputMask.pack();
		}
	}

	private final class AddObject3DActionListener implements ActionListener {	
		public void actionPerformed(ActionEvent e) {
			log.fine("Load button pressed!");
			// creade window for reading file name:
			JFileChooser chooser = new JFileChooser();
			String cwdName = params.workDirectory;
			File cwd = new File(cwdName);
			chooser.setCurrentDirectory(cwd);
			int returnVal = chooser.showOpenDialog(rootFrame.getContentPane()); // parent);
			if(returnVal == JFileChooser.APPROVE_OPTION) {
				String fileName = chooser.getCurrentDirectory() 
				+ SLASH + chooser.getSelectedFile().getName();
				log.info("You chose this file: " + fileName);
				// open file
				try {
					FileInputStream fis = new FileInputStream(fileName);
					graphController.readAndAdd(fis);
				}
				catch (Object3DIOException exc) {
					// TODO replace with better error window
					log.severe("Could not reading graph file: " + exc.getMessage());
				}
				catch (IOException exc) {
					// TODO replace with better error window
					log.severe("Could not open file: " + exc.getMessage());
				}
			}

			// read file and create graph
			// repaintApp(); // update graphics window   
		}
	} // AddObject3DActionListener

	/*
      private final class ImportKnotPlotActionListener implements ActionListener {	
      public void actionPerformed(ActionEvent e) {
      log.fine("ImportKnotPlotActionListener button pressed!");
      GeneralWizard wizard = new KnotPlotImportWizard();
      wizard.launchWizard(graphController, rootFrame);
      }
      } // KnotPlotImportListener
	 */
	/*
    private final class ImportPointSetActionListener implements ActionListener {	
    	public void actionPerformed(ActionEvent e) {
    	    log.fine("ImportPointSetActionListener button pressed!");
	    GeneralWizard wizard = new PointSetImportWizard();
	    wizard.launchWizard(graphController, rootFrame);
    	}
    } // KnotPlotImportListene
	 */
//	private final class ImportPointSet2ActionListener implements ActionListener {
//	public void actionPerformed(ActionEvent e) {
//	log.fine("ImportPointSetActionListener button pressed!");
//	GeneralWizard wizard = new PointSet2ImportWizard();
//	wizard.launchWizard(graphController, rootFrame);
//	}
//	}

	private final class ImportActionListener implements ActionListener {
	    private NanoTiler app;

	    ImportActionListener(NanoTiler app) {
		this.app = app;
	    }
	    
	    public void actionPerformed(ActionEvent e) {
		log.fine("ImportActionListener button pressed");
		GeneralWizard wizard = new ImportWizard(app);
		wizard.launchWizard(graphController, rootFrame);
	    }
	}

	private final class RunScriptActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			log.fine("Run script button pressed");
			GeneralWizard wizard = new RunScriptWizard(nanotilerApp);
			wizard.launchWizard(graphController, rootFrame);
		}


	}
	/*
    private final class ImportPdbActionListener implements ActionListener {	
    	public void actionPerformed(ActionEvent e) {
    	    log.fine("ImportPdbActionListener button pressed!");
	    GeneralWizard wizard = new PdbImportWizard();
	    wizard.launchWizard(graphController, rootFrame);
    	}
    } // ImportPdbActionListener
	 */
	// private final class ImportPdbProteinActionListener implements ActionListener {	
	// public void actionPerformed(ActionEvent e) {
	//log.fine("ImportPdbActionListener button pressed!");
	// GeneralWizard wizard = new PdbProteinImportWizard();
	// wizard.launchWizard(graphController, rootFrame);
	// }
	// } // ImportPdbProteinActionListener

	private final class ExportBplActionListener implements ActionListener {	
		public void actionPerformed(ActionEvent e) {
			// find filename:
			log.fine("Export-BPL item activated!");
			if (graphController.getGraph().getObjectCount() <= 0) {
				return; // no data defined
			}
			JFileChooser chooser = new JFileChooser();
			chooser.setApproveButtonText("Save");
			chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			String cwdName = params.workDirectory;
			File cwd = new File(cwdName);
			chooser.setCurrentDirectory(cwd);
			int returnVal = chooser.showSaveDialog(rootFrame.getContentPane()); // parent);
			if(returnVal == JFileChooser.APPROVE_OPTION) {
				String fileName = chooser.getCurrentDirectory() 
				+ SLASH + chooser.getSelectedFile().getName();
				log.info("You chose this file: " + fileName);
				// open output file
				FileOutputStream fis = null;
				try {
					fis = new FileOutputStream(fileName);
					Object3DWriter writer = null; // new Object3DBplWriter();
					// graphController.write(fis, writer);
					fis.close();
				}
				catch (IOException exc) {
					// TODO replace with better error window
					log.severe("Could not open file: " + exc.getMessage());
				}
			}
			log.fine("Finished writing RNA2D3D bpl button!");
		}
	} // BPLActionListener

	private final class ExportMcsymActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			log.fine("Export-MCSYM item activated!");
			JOptionPane.showMessageDialog(null,"Sorry, the program's not done yet.", "Error", JOptionPane.PLAIN_MESSAGE);
		}
	} //MCSYMActionListener

	class PdbFileFilter extends javax.swing.filechooser.FileFilter {
		public boolean accept(File f) {
			return f.isDirectory() || f.getName().toLowerCase().endsWith(PDB_EXTENSION);
		}

		public String getDescription() {
			return ".pdb files";
		}
	}

    /** Export scene graph as PDB file */
	private final class ExportPdbActionListener implements ActionListener {	
		public void actionPerformed(ActionEvent e) {
			// find filename:
			log.fine("Export-PDB item activated!");
			if (graphController.getGraph().getObjectCount() <= 0) {
				return; // no data defined
			}
			JFileChooser chooser = new JFileChooser();
			PdbFileFilter filter = new PdbFileFilter();
			//	    filter.addExtension("pdb");
			chooser.setApproveButtonText("Save");
			chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			chooser.setFileFilter(filter);
			String cwdName = params.workDirectory;
			File cwd = new File(cwdName);
			chooser.setCurrentDirectory(cwd);
			int returnVal = chooser.showSaveDialog(rootFrame.getContentPane()); // parent);    	    
			if(returnVal == JFileChooser.APPROVE_OPTION) {
				String fileName = chooser.getCurrentDirectory() 
				+ SLASH + chooser.getSelectedFile().getName();
				if ( !fileName.endsWith(PDB_EXTENSION) ) {
					fileName += PDB_EXTENSION;
				}
				log.info("You chose this file: " + fileName);
				// open output file
				FileOutputStream fis = null;
				try {
				    runScriptLine("exportpdb " + fileName );
				    // TODO: add option renumber=count|pdb|name junction=true|false
				}
				catch (CommandException exc) {
					// TODO replace with better error window
					log.severe("Could not open file: " + exc.getMessage());
				}
			}
			log.info("Finished writing PDB!");
		}
	} // ExportPdbActionListener

	private final class ExportSecondaryStructureListener implements ActionListener {	
		public void actionPerformed(ActionEvent e) {
			// find filename:
			log.fine("Export-Secondary structure item activated!");
			if (graphController.getGraph().getObjectCount() <= 0) {
				return; // no data defined
			}
			JFileChooser chooser = new JFileChooser();
			chooser.setApproveButtonText("Save");
			chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			String cwdName = params.workDirectory;
			File cwd = new File(cwdName);
			chooser.setCurrentDirectory(cwd);
			int returnVal = chooser.showSaveDialog(rootFrame.getContentPane()); // parent);    	    
			if(returnVal == JFileChooser.APPROVE_OPTION) {
				String fileName = chooser.getCurrentDirectory() 
				+ SLASH + chooser.getSelectedFile().getName();
				log.info("You chose this file: " + fileName);
				// open output file
				FileOutputStream fis = null;
				try {
					fis = new FileOutputStream(fileName);
					graphController.writeSecondaryStructure(fis, Object3DGraphControllerConstants.SECONDARY_FORMAT);
					fis.close();
				}
				catch (IOException exc) {
					// replace with better error window
					JOptionPane.showMessageDialog(null, "Could not open file: " + exc.getMessage());
				}
				catch (Object3DGraphControllerException ce) {
					JOptionPane.showMessageDialog(null, "Controller exception: " + ce.getMessage());
				}
			}
			log.fine("Finished writing Secondary structure button!");
		}
	} // ExportSecondaryStructureListener

	private final class ExportJunctionsPdbActionListener implements ActionListener {	
		public void actionPerformed(ActionEvent e) {
			// find filename:
			log.fine("Export-PDB item activated!");
			if (graphController.getGraph().getObjectCount() <= 0) {
				return; // no data defined
			}
			JFileChooser chooser = new JFileChooser();
			chooser.setApproveButtonText("Save");
			String cwdName = params.workDirectory;
			File cwd = new File(cwdName);
			chooser.setCurrentDirectory(cwd);
			int returnVal = chooser.showSaveDialog(rootFrame.getContentPane()); // parent);    	    
			if(returnVal == JFileChooser.APPROVE_OPTION) {
				String fileName = chooser.getCurrentDirectory().toString() + SLASH + chooser.getSelectedFile().getName().toString(); // won't run on Windows! fix!  -- CV fixed, should run on windows now -- TEST
				log.info("You chose this file: " + fileName);
				boolean originalMode = true; // TODO : set original mode with GUI
				// open output file
				graphController.writeJunctionsToPdb(fileName, originalMode);
			}
			log.fine("Finished writing Junctions PDB button!");
		}
	} // ExportPdbActionListener

        private final class RunDemoListener implements ActionListener{
	    String scriptFile;
	    public RunDemoListener(String scriptFile){
		this.scriptFile = scriptFile;
	    }
	    public void actionPerformed(ActionEvent e){
		String file = nanotilerHome + "/demo/scripts/" + scriptFile;
		String command = "source " + file;
		try{
		    nanotilerApp.runScriptLine(command);
		}
		catch(CommandException ce){
		    JOptionPane.showMessageDialog(rootFrame, ce.getMessage());
                }
	    }
	}

	private final class MinimizeListener implements ActionListener {	
		public void actionPerformed(ActionEvent e) {
			log.fine("MinimizeListener activated!");
			GeneralWizard wizard = new MinimizeWizard();
			wizard.launchWizard(graphController, rootFrame);
		}	
	}    

	private final class ElasticNetworkListener implements ActionListener {	
		public void actionPerformed(ActionEvent e) {
			log.fine("ElasticNetworkListener activated!");
			GeneralWizard wizard = new ElasticNetworkWizard();
			wizard.launchWizard(graphController, rootFrame);
		}	
	}

	/*
      private final class SelectActionListener implements ActionListener {
      public void actionPerformed(ActionEvent e) {
      log.fine("Select button pressed!");
      Wizard wizard = new SelectWizard();
      wizard.launchWizard(rootFrame);
      }
      }
	 */

	private final class QuaderListener implements ActionListener {
		private CommandApplication app;
		QuaderListener(CommandApplication app) {
			this.app = app;
		}
		public void actionPerformed(ActionEvent e) {
			log.fine("QuaderListener activated!");
			Wizard wizard = new QuaderWizard(this.app);
			wizard.launchWizard(rootFrame);
		}
	}
        private final class ManualRotateListener implements ActionListener{
	    private CommandApplication app;
	    ManualRotateListener(CommandApplication app){
		this.app = app;
	    }
	    public void actionPerformed(ActionEvent e){
		log.fine("ManualRotateListener activated");
		Wizard wizard = new ManualRotateWizard(this.app);
		wizard.launchWizard(rootFrame);
	    }
	}

	private final class RenameListener implements ActionListener {
		private CommandApplication app;
		RenameListener(CommandApplication app) {
			this.app = app;
		}
		public void actionPerformed(ActionEvent e) {
			log.fine("RenameListener activated!");
			Wizard wizard = new RenameWizard(this.app);
			wizard.launchWizard(rootFrame);
		}
	}

        private final class RemoveLinkListener implements ActionListener{
	    public void actionPerformed(ActionEvent e){
		log.fine("Remove link wizard activated");
		GeneralWizard wizard = new RemoveLinksWizard(nanotilerApp);
		wizard.launchWizard(graphController,rootFrame);
	    }
	}

	private final class ShiftListener implements ActionListener {
		private CommandApplication app;
		ShiftListener(CommandApplication app) {
			this.app = app;
		}
		public void actionPerformed(ActionEvent e) {
			log.fine("ShiftListener activated!");
			Wizard wizard = new ShiftWizard(this.app);
			wizard.launchWizard(rootFrame);
		}
	}

	private final class ChosenListener implements ActionListener {
		private CommandApplication app;
		ChosenListener(CommandApplication app) {
			this.app = app;
		}
		public void actionPerformed(ActionEvent e) {
			log.fine("ChosenListener activated!");
			Wizard wizard = new ChosenWizard(this.app);
			wizard.launchWizard(rootFrame);
		}
	}


	private final class SelectListener implements ActionListener {	
		private CommandApplication app;
		SelectListener(CommandApplication app) {
			this.app = app;
		}
		public void actionPerformed(ActionEvent e) {
			log.fine("SelectListener activated!");
			Wizard wizard = new SelectWizard(this.app);
			wizard.launchWizard(rootFrame);
		}
	}    

	private final class SecondaryStructureEditorListener implements ActionListener {	
		public void actionPerformed(ActionEvent e) {
			log.fine("New stem button pressed!");

			final SecondaryStructureEditorWizardNoThread wizard = new SecondaryStructureEditorWizardNoThread(nanotilerApp);
			wizard.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					MutableSecondaryStructure structure = wizard.getSecondaryStructure();
					SecondaryStructure struct;
					try {
						struct = graphController.generateSecondaryStructure();
					} catch(DuplicateNameException ex) {
						System.out.println("Cannot update secondary structure");
						return;
					}
					UnevenAlignment updatedSequences = structure.getSequences();
					UnevenAlignment originalSequences = struct.getSequences();
					System.out.println("Number of sequences from sse -- " + updatedSequences.getSequenceCount());
					for(int i = 0; i < updatedSequences.getSequenceCount(); ++i) {
						System.out.println(updatedSequences.getSequence(i));

						int index = originalSequences.getIndex(updatedSequences.getSequence(i).getName());
						if(index == -1) {
							try {
								Sequence s = updatedSequences.getSequence(i);
								graphController.addStrand(s.getName().trim(),
										s.sequenceString(),
										new Vector3D(0.0, 0.0, 0.0),
										new Vector3D(0.0, 0.0, 0.0),
										s.getAlphabet() == DnaTools.RNA_ALPHABET ||
										s.getAlphabet() == DnaTools.AMBIGUOUS_RNA_ALPHABET ?
												Object3DGraphControllerConstants.RNA :
													Object3DGraphControllerConstants.DNA);
							} catch(UnknownSymbolException ex) {

							}
						}

					}

					InteractionSet oldInteractions = struct.getInteractions();
					InteractionSet newInteractions = structure.getInteractions();

					// remove interactions in old one but not new one
					for(int i = 0; i < oldInteractions.size(); ++i) {
						System.out.println("Interaction: " + oldInteractions.get(i).getResidue1().getPos() + " - " + oldInteractions.get(i).getResidue2().getPos() + "\t" + oldInteractions.get(i).getInteractionType());
						boolean foundInteraction = false;
						for(int j = 0; j < newInteractions.size(); ++j) {

							if(newInteractions.get(j).compareTo(oldInteractions.get(i)) == 0) {
								foundInteraction = true;
								break;
							}
						}

						if(!foundInteraction) {
							Link l = graphController.getLinks().find((Residue3D) oldInteractions.get(i).getResidue1(), (Residue3D) oldInteractions.get(i).getResidue2());
							if(l != null)
								graphController.getLinks().remove(l);
							else {
								System.out.println("Could not remove link -- " + oldInteractions.get(i).getResidue1().getPos() + "  " + oldInteractions.get(i).getResidue2().getPos());
							}
						} else {
							System.out.println("Interaction present in new structure");

						}

					}

					try {
						originalSequences = graphController.generateSecondaryStructure().getSequences();
					} catch(DuplicateNameException ex) {
						return;
					}

					// add all interactions in new one but not old one
					for(int i = 0; i < newInteractions.size(); ++i) {
						boolean foundInteraction = false;
						for(int j = 0; j < oldInteractions.size(); ++j) {
							if(newInteractions.get(i).compareTo(oldInteractions.get(j)) == 0) {
								foundInteraction = true;
								break;
							}
						}

						if(!foundInteraction) {
							try {
								String seq1Name = newInteractions.get(i).getSequence1().getName();
								String seq2Name = newInteractions.get(i).getSequence2().getName();
								int res1Pos = newInteractions.get(i).getResidue1().getPos();
								int res2Pos = newInteractions.get(i).getResidue2().getPos();
								Sequence sequence1 = originalSequences.getSequence(seq1Name);
								Sequence sequence2 = originalSequences.getSequence(seq2Name);
								if (sequence1 != null && sequence2 != null) {
								    graphController.addBasePair(Object3DTools.getFullName((Residue3D)sequence1.getResidue(res1Pos)), Object3DTools.getFullName((Residue3D)sequence2.getResidue(res2Pos)), RnaInteractionType.WATSON_CRICK, 0);
								} else {
								    System.out.println("Could not find sequences with names " + seq1Name + " " + seq2Name);
								}
							} catch (Object3DGraphControllerException ex) {

 							} // catch (UnknownSequenceException uex) { // not thrown anymore

// 							}
						}
					}








				}
			});
			wizard.launchWizard(graphController, rootFrame);
		}
	}

	private final class NewStrandActionListener implements ActionListener {	
		public void actionPerformed(ActionEvent e) {
			log.fine("New Strand button pressed!");
			GeneralWizard wizard = new RnaStrandGuiWizard();
			// generate and insert new RNA strand according to user dialog:
			// Object3DHandle handle = wizard.wizardObject3D(graphController, rootFrame);
			wizard.launchWizard(graphController, rootFrame);
			repaintApp(); // update graphics window   
		}
	} // NewStrandActionListener

	private final class NewGridActionListener implements ActionListener {	
		public void actionPerformed(ActionEvent e) {
			log.fine("New Grid button pressed!");
			GeneralWizard wizard = new CubicGridWizard();
			wizard.launchWizard(graphController, rootFrame);
			repaintApp(); // update graphics window   
		}
	} // NewGridActionListener

//	private final class HideGraphicsActionListener implements ActionListener {	
//	public void actionPerformed(ActionEvent e) {
//	GeneralWizard wizard = new HideGraphicsWizard();
//	wizard.launchWizard(graphController, rootFrame);
//	repaintApp(); // update graphics window   
//	}
//	} // HideGraphicsActionListener

	private final class GraphitGridActionListener implements ActionListener {	
		public void actionPerformed(ActionEvent e) {
			log.fine("New Grid button pressed!");
			GeneralWizard wizard = new GraphitGridWizard();
			wizard.launchWizard(graphController, rootFrame);
			repaintApp(); // update graphics window   
		}
	} // NewGridActionListener

	/** tiles selected object using FragmentTiler */
	private final class StemTilerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			log.fine("Starting StemTilerListener.actionPerformed!");
			Object3D obj = graphController.getGraph().getSelectionRoot();
			LinkSet links = graphController.getLinks();
			Object3DLinkSetBundle bundle = new SimpleObject3DLinkSetBundle(obj, links);
			try {
				Object3DLinkSetBundle result = graphController.generateCoveringStems( bundle, "tiling", false, 'N', Object3DGraphControllerConstants.TILER_FRAGMENT);
				graphController.getGraph().addGraph(result.getObject3D());
				graphController.getLinks().addLinks(result.getLinks());
			}
			catch (Object3DGraphControllerException exc) {
				log.severe("StemTilerListener failed: "
						+ exc.getMessage());

				// do nothing so far... ?
			}
		}
	}

	private final class AddHelixListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {

			log.fine("add helix wizard started");
			GeneralWizard wizard = new AddHelixWizard(nanotilerApp);
			wizard.launchWizard(graphController, rootFrame);

		}


	}
        private final class AddGenerateLinkListener implements ActionListener{
        	public void actionPerformed(ActionEvent e){
	             log.fine("generate link wizard started");
	             GeneralWizard wizard = new GenLinkWizard(nanotilerApp);
	             wizard.launchWizard(graphController, rootFrame);
        	}
        }

        private final class AddSynthListener implements ActionListener{
	    public void actionPerformed(ActionEvent e){
		log.fine("synth wizard started");
		Wizard wizard = new SynthWizard(nanotilerApp);
		wizard.launchWizard(rootFrame);
	    }
	}


	/** adds a geometry like a tetrahedron */
	private final class PlatonicSolidListener implements ActionListener {	
		public void actionPerformed(ActionEvent e) {
			log.fine("New Platonic solid button pressed!");
			GeneralWizard wizard = new PlatonicSolidWizard();
			wizard.launchWizard(graphController, rootFrame);
			repaintApp(); // update graphics window   
		}
	}

	/** adds a geometry like a tetrahedron */
	private final class PlanarListener implements ActionListener {	
		public void actionPerformed(ActionEvent e) {
			log.fine("New planar shape button pressed!");
			GeneralWizard wizard = new PlanarWizard();
			wizard.launchWizard(graphController, rootFrame);
			repaintApp(); // update graphics window   
		}
	}
        private final class OptimizationWizardListener implements ActionListener{
	    public void actionPerformed(ActionEvent e){
		GeneralWizard wizard = new OptimizationWizard(nanotilerApp);
		wizard.launchWizard(graphController, rootFrame);
		repaintApp();
	    }
	}
        private final class BasepairOptimizationWizardListener implements ActionListener{
	    public void actionPerformed(ActionEvent e){
		GeneralWizard wizard = new OptbasepairsWizard(nanotilerApp);
		wizard.launchWizard(graphController, rootFrame);
		repaintApp();
	    }
	}

	/** adds a geometry like a tetrahedron */
	private final class PrismListener implements ActionListener {	
		public void actionPerformed(ActionEvent e) {
			log.fine("New prism shape button pressed!");
			//	    GeneralWizard wizard = new PrismWizard(); //CV - this results in a compile error
			//	    wizard.launchWizard(graphController, rootFrame);
			repaintApp(); // update graphics window   
		}
	}

	private final class SaveActionListener implements ActionListener {	
		public void actionPerformed(ActionEvent e) {
			log.fine("Save button pressed!");
			if (graphController.getGraph().getObjectCount() <= 0) {
				return; // no data defined
			}
			// find filename:	
			JFileChooser chooser = new JFileChooser();
			chooser.setApproveButtonText("Save");
			chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			String cwdName = params.workDirectory;
			File cwd = new File(cwdName);
			chooser.setCurrentDirectory(cwd);
			int returnVal = chooser.showSaveDialog(rootFrame.getContentPane()); // parent);
			if(returnVal == JFileChooser.APPROVE_OPTION) {
				String fileName = chooser.getCurrentDirectory() 
				// won't run on Windows! fix! TODO
				+ SLASH + chooser.getSelectedFile().getName();
				log.info("You chose this file: " + fileName);
				// open output file
				try {
					FileOutputStream fis = new FileOutputStream(fileName);
					Object3DWriter writer = new Object3DDefaultWriter();
					graphController.write(fis, writer);
				}
				catch (IOException exc) {
					// TODO replace with better error window
					log.severe("Could not open file: " + exc.getMessage());
				}
			}
			log.fine("Finished writing object3 native format!");
			log.fine("finished save button!");
		}
	} // SaveActionListener

	private final class SaveGuiPropertiesActionListener implements ActionListener {	
		public void actionPerformed(ActionEvent e) {
			log.fine("Save Gui Parameters button pressed!");
			if (graphController.getGraph().getObjectCount() <= 0) {
				return; // no data defined
			}
			// find filename:	
			JFileChooser chooser = new JFileChooser();
			chooser.setApproveButtonText("Save");
			String cwdName = params.workDirectory;
			File cwd = new File(cwdName);
			chooser.setCurrentDirectory(cwd);
			int returnVal = chooser.showOpenDialog(rootFrame.getContentPane()); // parent);
			if(returnVal == JFileChooser.APPROVE_OPTION) {
				String fileName = chooser.getCurrentDirectory() 
				// won't run on Windows! fix!
				+ SLASH + chooser.getSelectedFile().getName();
				log.info("You chose this file: " + fileName);
				// open output file
				try {
//					FileOutputStream fis = new FileOutputStream(fileName);
//					Object3DWriter writer = new Object3DDefaultWriter();
//					graphController.write(fis, writer);
					// save as XML file!
					XMLEncoder enc = new XMLEncoder(
							new BufferedOutputStream(
									new FileOutputStream(fileName)));
					enc.writeObject(params);
				}
				catch (IOException exc) {
					// TODO replace with better error window
					log.severe("Could not open file: " + exc.getMessage());
				}
			}
			log.fine("Finished writing object3 native format!");
			log.fine("finished save button!");
		}
	} // SaveGuiPropertiesActionListener
	
	private final class DisplayOptionsListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JDialog dialog = graphControllerDisplayManager.new GraphConfiguration("Display Options");
			dialog.setVisible(true);
		}
	}

	/** starts new window with text of selected sub-tree */
	private final class SelectedPanelListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			SelectedPanel selectedPanel = new SelectedPanel(new SelectedPanel.SelectedPanelParameters(), graphController);
			JFrame selectedFrame = new JFrame("Selected Objects:");
			selectedFrame.add(selectedPanel);
			selectedFrame.pack();
			selectedFrame.setVisible(true);
			registerComponent(selectedFrame);
		}
	}

	private final class ViewTreeListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			log.fine("View Tree Wizard launched");
			GeneralWizard wizard = new ViewTreeWizard(nanotilerApp);
			wizard.launchWizard(graphController, rootFrame);

		}

	}

	private final class ViewLinksListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			log.fine("View links wizard launched");
			GeneralWizard wizard = new ViewLinksWizard(nanotilerApp);
			wizard.launchWizard(graphController, rootFrame);

		}

	}

	/** choose background color */
	private final class ChooseBackgroundColorListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Color newCol = JColorChooser.showDialog(rootFrame, "Choose Background Color", params.colorBackground);
			for (int i = 0; i < graphControllPanels.size(); ++i) {
				GraphControlPanel gcp = ((GraphControlPanel)(graphControllPanels.get(i)));
				gcp.graphPanel.setBackground(newCol);
			}
			repaintApp(); // update graphics window
		}
	}

	private final class Atom3DActionListener implements ActionListener {
		public void actionPerformed (ActionEvent e) {



		}
	}

        private final class GenerateSignatureListener implements ActionListener{
	    public void actionPerformed(ActionEvent e){
	             log.fine("generate signature wizard started");
		     GeneralWizard wizard = new SignatureWizard(nanotilerApp);
		     wizard.launchWizard(graphController, rootFrame);
	    }
	}
        private final class DistanceListener implements ActionListener{
	    public void actionPerformed(ActionEvent e){
	             log.fine("generate distance wizard started");
		     GeneralWizard wizard = new DistanceWizard(nanotilerApp);
		     wizard.launchWizard(graphController, rootFrame);
	    }
	}	

	private final class MoveRightListener implements ActionListener {	
		public void actionPerformed(ActionEvent e) {
			graphController.getGraph().translateSelected(new Vector3D(params.moveStep, 0.0, 0.0));
			repaintApp();
		}
	}

	private final class MoveLeftListener implements ActionListener {	
		public void actionPerformed(ActionEvent e) {
			graphController.getGraph().translateSelected(new Vector3D(-params.moveStep, 0.0, 0.0));
			repaintApp();
		}
	}

	private final class MoveUpListener implements ActionListener {	
		public void actionPerformed(ActionEvent e) {
			graphController.getGraph().translateSelected(new Vector3D(0.0, 0.0, params.moveStep));
			repaintApp();
		}
	}

	private final class MoveDownListener implements ActionListener {	
		public void actionPerformed(ActionEvent e) {
			graphController.getGraph().translateSelected(new Vector3D(0.0, 0.0, -params.moveStep));
			repaintApp();
		}
	}

	private final class MoveNorthListener implements ActionListener {	
		public void actionPerformed(ActionEvent e) {
			graphController.getGraph().translateSelected(new Vector3D(0.0, params.moveStep, 0.0));
			repaintApp();
		}
	}

	private final class MoveSouthListener implements ActionListener {	
		public void actionPerformed(ActionEvent e) {
			graphController.getGraph().translateSelected(new Vector3D(0.0, -params.moveStep, 0.0));
			repaintApp();
		}
	}

	private final class RotateListener implements ActionListener {	
		public void actionPerformed(ActionEvent e) {
			// graphController.getGraph().translateSelected(new Vector3D(0.0, 0.0, -params.moveStep));
			// GeneralWizard wizard = new XMLRotationWizard();
		    //	GeneralWizard wizard = new RotateWizard();
		    //	wizard.launchWizard(graphController, rootFrame);
		    //	repaintApp();
		}
	}
        private final class AddFuseStrandsListener implements ActionListener{
	    public void actionPerformed(ActionEvent e){
		GeneralWizard wizard = new FuseStrandsWizard(nanotilerApp);
		wizard.launchWizard(graphController, rootFrame);
	    }
	}

	/** redraw all windows if a global tree selection was changed */
	private final class GlobalTreeSelectionListener 
	implements TreeSelectionListener {	
		public void valueChanged(TreeSelectionEvent e) {
			repaintApp();
		}
	}

	private final class StandardViewListener implements ActionListener {	
		public void actionPerformed(ActionEvent e) {
			// find GraphPanel components:
			for (int i = 0; i < frames.size(); ++i) {
				if (frames.get(i) instanceof GraphPanel) {
					((GraphPanel)(frames.get(i))).setPainter(new SimplePainter(params));
				}
			}
			repaintApp();
		}
	}

	private final class PointsViewListener implements ActionListener {	
		public void actionPerformed(ActionEvent e) {
			// find GraphPanel components:
			for (int i = 0; i < frames.size(); ++i) {
				if (frames.get(i) instanceof GraphPanel) {
					((GraphPanel)(frames.get(i))).setPainter(new PointPainter(params));
				}
			}
			repaintApp();
		}
	}

	private final class ViewRunningListener implements ActionListener {	

		public void actionPerformed(ActionEvent e) {
			log.fine("ViewRunningListener activated!");
			QueueManager queueManager = SimpleQueueManager.getInstance();
			JobViewer jobViewer = new JobViewer(queueManager);
			jobViewer.pack();
			jobViewer.setVisible(true);
		}

	}
        private final class ViewVariablesListener implements ActionListener{
	    public void actionPerformed(ActionEvent e){
		log.fine("Env wizard launched");
		GeneralWizard wizard = new EnvWizard(nanotilerApp);
		wizard.launchWizard(graphController,rootFrame);
	    }
	}
        private final class GenerateAtomLinksListener implements ActionListener{
	    public void actionPerformed(ActionEvent e){
		graphController.generateAtomLinks(graphController.getGraph().getGraph(),graphController.getAtomBondCutoff());
	    }
	}

	private final class PlaceJunctionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			log.fine("place junction wizard launched");
			GeneralWizard wizard = new PlaceJunctionWizard(nanotilerApp);
			wizard.launchWizard(graphController, rootFrame);

		}
	}
        private final class GraphicsHelpListener implements ActionListener{
	    public void actionPerformed(ActionEvent e){
		log.fine("Graphics manual wizard launched");
		GeneralWizard wizard = new GraphicsManualWizard(nanotilerApp);
		wizard.launchWizard(graphController,rootFrame);
	    }
	}

	private final class GrowListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			log.fine("grow wizard launched");
			GeneralWizard wizard = new GrowWizard(nanotilerApp);
			wizard.launchWizard(graphController, rootFrame);

		}

	}

        private final class GrowGraphListener implements ActionListener {
	    public void actionPerformed(ActionEvent e){
		log.fine("grow graph wizard launched");
		GeneralWizard wizard = new GrowGraphWizard(nanotilerApp);
		wizard.launchWizard(graphController, rootFrame);
	    }
	}

	private final class HelixConstraintListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			log.fine("helix constraint wizard launched");
			GeneralWizard wizard = new HelixConstraintWizard(nanotilerApp);
			wizard.launchWizard(graphController, rootFrame);

		}

	}

	private final class OptimizeHelicesListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			log.fine("optimizing helices");

			final JDialog dialog = new JDialog(rootFrame, "Optimize Helices", true);
			final JRadioButton hTrue = new JRadioButton("True");
			final JRadioButton hFalse = new JRadioButton("False");
			final ButtonGroup group = new ButtonGroup();
			group.add(hTrue);
			group.add(hFalse);
			hTrue.setSelected(true);

			JPanel panel = new JPanel();
			panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

			JPanel temp = new JPanel();
			temp.add(new JLabel("Helices"));
			temp.add(hTrue);
			temp.add(hFalse);

			panel.add(temp);

			temp = new JPanel();
			final JTextField steps = new JTextField(10);
			steps.setText("1");
			temp.add(new JLabel("Steps"));
			temp.add(steps);

			panel.add(temp);

			panel.add(Box.createVerticalStrut(10));

			temp = new JPanel();
			final JButton close = new JButton("Close");
			final JButton optimize = new JButton("Optimize");
			close.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					dialog.setVisible(false);
					dialog.dispose();

				}
			});

			optimize.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						Integer sts = Integer.parseInt(steps.getText());
						if(sts < 0) {
							return;
						}



						nanotilerApp.runScriptLine("opthelices steps=" + sts + " helices=" + (hTrue.isSelected() ? "true" : false));
					} catch(NumberFormatException en) {
						JOptionPane.showMessageDialog(rootFrame, en.getMessage());
						return;
					} catch(CommandException ex) {
						JOptionPane.showMessageDialog(rootFrame, ex.getMessage());
						return;
					}

					dialog.setVisible(false);
					dialog.dispose();

				}

			});


			temp.add(close);
			temp.add(optimize);

			panel.add(temp);

			dialog.add(panel);

			dialog.pack();
			dialog.setVisible(true);



		}


	}

	private final class LoadJunctionListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			log.fine("loadjunctionlistener activated!");
			GeneralWizard wizard = new JunctionParameterWizard();
			wizard.launchWizard(graphController, rootFrame);
		}

	}    
	/** Displays a window with the junction database info. Deprecated */
	private final class JunctionInfoListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			log.fine("junctioninfolistener activated!");
			String junctionInfo = new String();
			junctionInfo = junctionController.infoString();
			JFrame.setDefaultLookAndFeelDecorated(true);
			JFrame frame = new JFrame("Junction Database Info");
			frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			JTextArea textArea = new JTextArea(junctionInfo);
			JScrollPane scrollPane = new JScrollPane(textArea);
			frame.getContentPane().setLayout(new BorderLayout());
			frame.getContentPane().add(scrollPane, BorderLayout.CENTER);
			frame.pack();
			frame.setVisible(true); //if the textarea overflows the height or width of window, a scroll bar will not be created unless the window is maximized.
		}
	}

	private final class TilingStatisticsListener implements ActionListener { 
		public void actionPerformed(ActionEvent e) {
			log.fine("TilingStatisticsListener activated!");
			JFrame.setDefaultLookAndFeelDecorated(true);
			JFrame frame = new JFrame("Tiling Statistics");
			frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			JTextArea textArea = new JTextArea(tilingStatisticsString);
			textArea.setEditable(false);
			textArea.setText(graphController.getJunctionController().infoString());
			JScrollPane scrollPane = new JScrollPane(textArea);
			frame.getContentPane().setLayout(new BorderLayout());
			frame.getContentPane().add(scrollPane, BorderLayout.CENTER);
			frame.pack();
			frame.setVisible(true);
		}
	}

	private final class NanoTilerWikiListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			log.fine("Wiki Help activated!");
			try	   
			{new Browser(WIKI_PAGE);}
			catch(Exception ex) { log.severe("Error displaying " + WIKI_PAGE);}
			log.fine("Wiki Help finished!");
		}
	}

        private final class NanoTilerTutorialListener implements ActionListener{
	    public void actionPerformed(ActionEvent e){
		log.fine("NanoTiler tutorial launched");
		try{
		    new Browser(TUTORIAL_HOME);
		}
		catch(Exception ex){
		    log.severe("Error displaying "+ TUTORIAL_HOME);
		}
		log.fine("NanoTiler tutroial finished!");
	    }
	}

        private final class CommandHelpListener implements ActionListener{
	    public void actionPerformed(ActionEvent e){
		log.fine("Command Help activated");
		GeneralWizard wizard = new HelpWizard(nanotilerApp);
		wizard.launchWizard(graphController,rootFrame);
	    }
	}

        private final class CloneListener implements ActionListener{
	    public void actionPerformed(ActionEvent e){
		log.fine("Clone wizard activated");
		Wizard wizard = new CloneWizard(nanotilerApp);
		wizard.launchWizard(rootFrame);
	    }
	}
        private final class MoveListener implements ActionListener{
	    public void actionPerformed(ActionEvent e){
		log.fine("Move Wizard Started");
		Wizard wizard = new MoveWizard(nanotilerApp);
		wizard.launchWizard(rootFrame);
	    }
	}
        private final class AddBasepairsListener implements ActionListener{
	    public void actionPerformed(ActionEvent e){
		log.fine("AddBasepair wizard started");
		Wizard wizard = new AddBasepairsWizard(nanotilerApp);
		wizard.launchWizard(rootFrame);
	    }
	}
        private final class ElasticWizardListener implements ActionListener{
	    public void actionPerformed(ActionEvent e){
		log.fine("Elastic wizard started");
		GeneralWizard wizard = new ElasticWizard(nanotilerApp);
		wizard.launchWizard(graphController,rootFrame);
	    }
	}
        private final class ApplyModeWizardListener implements ActionListener{
	    public void actionPerformed(ActionEvent e){
		log.fine("Apply Mode wizard started");
		GeneralWizard wizard = new ApplyModeWizard(nanotilerApp);
		wizard.launchWizard(graphController,rootFrame);
	    }
	}
	private final class WireViewListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			log.fine("starting actionPerformed on WireViewListener");
			// find GraphPanel components:
			GeometryPainter geometryPainter = 
				geometryPainterFactory.createPainter(NanoTilerGeometryPainterFactory.WIRE_PAINTER);
			log.fine("mark 2 actionPerformed on WireViewListener");
			for (int i = 0; i < frames.size(); ++i) {
				log.fine("mark 3 actionPerformed on WireViewListener");
				if (frames.get(i) instanceof GraphPanel) {
					log.fine("mark 3b actionPerformed on WireViewListener");
					((GraphPanel)(frames.get(i))).setPainter(
							new GeneralGeometryPainter(params, geometryPainter, graphController));
				}
			}
			repaintApp();
			log.fine("ending actionPerformed on WireViewListener");
		}
	}

	JMenuItem menuItem;
	private class NoLinkGeometryListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			log.fine("Switching to NoLinkGeometryPainter");
			GeometryPainter geometryPainter;
			geometryPainter = geometryPainterFactory.createPainter(NanoTilerGeometryPainterFactory.SOLID_PAINTER, false);
//			}
			for (int i = 0; i < frames.size(); ++i) {
				if (frames.get(i) instanceof GraphPanel) {
					((GraphPanel)(frames.get(i))).setPainter(
							new GeneralGeometryPainter(params, geometryPainter, graphController));
				}
			}
			repaintApp();
			log.fine("Ending NoLinkGeometryPainter");
		}
	}

	/** if checkBox is selected than hide characters */
	JCheckBox checkBox;
	private class CharacterDisplayListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			log.fine("Switching to WireGeometryCharacterPainter!");
			GeometryPainter geometryPainter;
			if (checkBox.isSelected()) {
				geometryPainter = geometryPainterFactory.createPainter(NanoTilerGeometryPainterFactory.WIRE_PAINTER);
			}
			else {

				geometryPainter = geometryPainterFactory.createPainter(NanoTilerGeometryPainterFactory.WIRE_CHARACTER_PAINTER);
			}

			for (int i = 0; i < frames.size(); ++i) {
				if (frames.get(i) instanceof GraphPanel) {
					((GraphPanel)(frames.get(i))).setPainter(
							new GeneralGeometryPainter(params, geometryPainter, graphController));
				}
			}
			repaintApp();
			log.fine("End Switching to WireGeometryCharactersPainter");
		}
	}

	/** paints only edges; no faces, no points */
	private final class WireFrameViewListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			log.fine("starting actionPerformed on WireFrameViewListener");
			// find GraphPanel components:
			GeometryPainter geometryPainter = 
				geometryPainterFactory.createPainter(NanoTilerGeometryPainterFactory.WIREFRAME_PAINTER);
			for (int i = 0; i < frames.size(); ++i) {
				if (frames.get(i) instanceof GraphPanel) {
					((GraphPanel)(frames.get(i))).setPainter(
							new GeneralGeometryPainter(params, geometryPainter, graphController));
				}
			}
			repaintApp();
			log.fine("ending actionPerformed on WireFrameViewListener");
		}
	}

        private final class WelcomeWindowListener implements ActionListener{
	    public void actionPerformed(ActionEvent e){
		log.fine("Welcome window wizard started");
		GeneralWizard wizard = new WelcomeWindowWizard(nanotilerApp);
		wizard.launchWizard(graphController,rootFrame);
	    }
	}


//	private final class RainbowColorModelViewListener implements ActionListener {
//	public void actionPerformed(ActionEvent e) {
//	GeometryPainter geometryPainter = 
//	geometryPainterFactory.createPainter(NanoTilerGeometryPainterFactory.RAINBOW_PAINTER);
//	for (int i = 0; i < frames.size(); ++i) {
//	if (frames.get(i) instanceof GraphPanel) {
//	((GraphPanel)(frames.get(i))).setPainter(
//	new GeneralGeometryPainter(params, geometryPainter));
//	}
//	}
//	repaintApp();
//	}
//	}


//	private final class RnaStrandColorModelViewListener implements ActionListener {
//	public void actionPerformed(ActionEvent e) {
//	GeometryPainter geometryPainter = 
//	geometryPainterFactory.createPainter(NanoTilerGeometryPainterFactory.RNASTRAND_PAINTER);
//	for (int i = 0; i < frames.size(); ++i) {
//	if (frames.get(i) instanceof GraphPanel) {
//	((GraphPanel)(frames.get(i))).setPainter(
//	new GeneralGeometryPainter(params, geometryPainter));
//	}
//	}
//	repaintApp();
//	}
//	}



	/* PULL-DOWN MENU */

	private class RnaMenuBar extends JMenuBar {

		/**
		 * 
		 */
		private static final long serialVersionUID = -8919620750494034628L;
	    private NanoTiler app; // workaround for ImportWizard

		RnaMenuBar(NanoTiler app) {
		    this.app = app;
			JMenu menu = new JMenu("File");
 			// JMenuItem item = new JMenuItem("Open");
// 			item.addActionListener(new LoadActionListener());
// 			item.setToolTipText("Open File");
// 			menu.add(item);
// 			item = new JMenuItem("Add");
// 			item.addActionListener(new AddObject3DActionListener());
// 			item.setToolTipText("Add file");
// 			menu.add(item);
// 			item = new JMenuItem("Save");
// 			item.addActionListener(new SaveActionListener());
// 			item.setToolTipText("Save current structure");
// 			menu.add(item);
// 			item = new JMenuItem("Clear");
// 			item.addActionListener(new ClearActionListener());
// 			item.setToolTipText("Clear current structure");
// 			menu.add(item);
			// JMenu importMenu = new JMenu("Import...");
			// importMenu.setToolTipText("Import File");
			// item = new JMenuItem("PDB: RNA or DNA");
			// item.addActionListener(new ImportPdbActionListener());
			// importMenu.add(item);
			// item = new JMenuItem("PDB: Protein");
			// item.addActionListener(new ImportPdbProteinActionListener());
			// importMenu.add(item);
			// item = new JMenuItem("KnotPlot");
			// item.addActionListener(new ImportKnotPlotActionListener());
			// importMenu.add(item);
			// item = new JMenuItem("PointSet");
			// item.addActionListener(new ImportPointSetActionListener());
			// importMenu.add(item);
			JMenuItem item = new JMenuItem("Run Script");
			item.setToolTipText("Run a script");
			item.addActionListener(new RunScriptActionListener());
			menu.add(item);

			item = new JMenuItem("Import");
			item.setToolTipText("Import PDB file or PointSet");
			item.addActionListener(new ImportActionListener(app));
			menu.add(item);
			// importMenu.add(item);
			// menu.add(importMenu);
			item = new JMenuItem("Export");
			JMenu subMenu = new JMenu("Export...");
			subMenu.setToolTipText("Export file");
			item = new JMenuItem("PDB (all)");
			item.addActionListener(new ExportPdbActionListener());
			subMenu.add(item);
			item = new JMenuItem("PDB (Junctions only)");
			item.addActionListener(new ExportJunctionsPdbActionListener());
			subMenu.add(item);
// 			item = new JMenuItem("RNA2D3D BPL");
// 			item.addActionListener(new ExportBplActionListener());
// 			subMenu.add(item);
			/*
			  item = new JMenuItem("MCSYM");
			  item.addActionListener(new ExportMcsymActionListener());
			  subMenu.add(item);
			*/
			item = new JMenuItem("Secondary Structure");
			item.addActionListener(new ExportSecondaryStructureListener());
			subMenu.add(item);
// 			item = new JMenuItem("XML");
// 			subMenu.add(item);
			item = new JMenuItem("PDB");
			menu.add(subMenu);

			subMenu = new JMenu("Demo...");
			item = new JMenuItem("Grow");
			item.addActionListener(new RunDemoListener("grow.script"));
			subMenu.add(item);
			item = new JMenuItem("Grow graph");
			item.addActionListener(new RunDemoListener("growgraph.script"));
			subMenu.add(item);
			item = new JMenuItem("Mutate");
			item.addActionListener(new RunDemoListener("mutate.script"));
			subMenu.add(item);
			item = new JMenuItem("Optimize base pairs");
			item.addActionListener(new RunDemoListener("optbasepairs.script"));
			subMenu.add(item);
			item = new JMenuItem("Ring fuse");
			item.addActionListener(new RunDemoListener("ringfuse.script"));
			subMenu.add(item);
			item = new JMenuItem("Place Junction");
			item.addActionListener(new RunDemoListener("place.script"));
			subMenu.add(item);
			item = new JMenuItem("Run all demos...");
			item.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
				    try {
					runScriptLine("demo all");
				    }
				    catch (CommandException ce) {
					JOptionPane.showMessageDialog(null, ce.getMessage(), "Error", JOptionPane.PLAIN_MESSAGE);
				    }
				}
			    } );
			subMenu.add(item);
			menu.add(subMenu);

			menu.add(new JSeparator());
			item = new JMenuItem("Quit");
			item.addActionListener(new ExitActionListener());
			item.setToolTipText("Quit program");
			menu.add(item);
			add(menu);

			menu = new JMenu("Edit");
			item = new JMenuItem("Clear all");
			item.addActionListener(new ClearActionListener());
			item.setToolTipText("Clear all structures in window");
			menu.add(item);
			item = new JMenuItem("Clone");
			item.setToolTipText("Clone an object to a new name and location");
			item.addActionListener(new CloneListener());
			menu.add(item);

//			item = new JMenuItem("Clear all");
//			item.addActionListener(new ClearActionListener());
//			menu.add(item);
			//item = new JMenuItem("connect");
			//item.addActionListener(new ConnectActionListener());
			//menu.add(item);
			//item = new JMenuItem("disconnect");
			// TODO item.addActionListener(new DisconnectActionListener());
			//menu.add(item);

			//item = new JMenuItem("Remove...");
			//item.addActionListener(new RemoveActionListener());
			//item.setToolTipText("Remove strand");
			//menu.add(item);
			//item = new JMenuItem("Remove all atoms");
			//item.setToolTipText("Remove all atoms");
			//menu.add(item);
			//item = new JMenuItem("Scale");
			//menu.add(item);


			item = new JMenuItem("Fuse Strands");
			item.setToolTipText("Fuse two strands together");
			item.addActionListener(new AddFuseStrandsListener());
			menu.add(item);

			item = new JMenuItem("Move Object");
			item.setToolTipText("Move an object to a new location withing the tree");
			item.addActionListener(new MoveListener());
			menu.add(item);

			item = new JMenuItem("Select");
			item.addActionListener(new SelectListener(nanotilerApp));
			item.setToolTipText("Select an object");
			menu.add(item);
			item = new JMenuItem("Shift");
			item.addActionListener(new ShiftListener(nanotilerApp));
			item.setToolTipText("Shift an object");
			menu.add(item);
			item = new JMenuItem("Remove Link");
			item.setToolTipText("Remove an existing link between objects");
			item.addActionListener(new RemoveLinkListener());
			menu.add(item);

			item = new JMenuItem("Rename");
			item.addActionListener(new RenameListener(nanotilerApp));
			item.setToolTipText("Rename an object");
			menu.add(item);
			//button = new JButton("Quader");
			//button.addActionListener(new QuaderListener(nanotilerApp));
			//button.setToolTipText("Use quader command");
			//add(button);
			item = new JMenuItem("Rotate");
			item.addActionListener(new ManualRotateListener(nanotilerApp));
			menu.add(item);
			add(menu);



			menu = new JMenu("View");
// 			item = new JMenuItem("Chosen");
// 			item.addActionListener(new ChosenListener(nanotilerApp));
// 			item.setToolTipText("Display current selection");
//       			menu.add(item);
			//menuItem = new JMenuItem("Display Options");
			//menuItem.addActionListener(new DisplayOptionsListener());
			//menu.add(menuItem);
			menuItem = new JMenuItem("Selected Object");
			menuItem.addActionListener(new ChosenListener(nanotilerApp));
			menu.add(menuItem);
// 			menuItem = new JMenuItem("Selection Text");
// 			menuItem.addActionListener(new SelectedPanelListener());
// 			menuItem.setToolTipText("Selected Objects");
// 			menu.add(menuItem);
			//menuItem = new JMenuItem("Choose Background Color");
			//menuItem.addActionListener(new ChooseBackgroundColorListener());
			//menuItem.setToolTipText("Choose background color");
			//menu.add(menuItem);
			menuItem = new JMenuItem("Tree");
			menuItem.addActionListener(new ViewTreeListener());
			menuItem.setToolTipText("View the tree");
			menu.add(menuItem);
			menuItem = new JMenuItem("Links");
			menuItem.addActionListener(new ViewLinksListener());
			menuItem.setToolTipText("View all links");
			menu.add(menuItem);
			//menuItem = new JMenuItem("Links");
			//menuItem.addActionListener(new NoLinkGeometryListener());
			//menuItem.setToolTipText("Turn off / on links");
			//menu.add(menuItem);
			// more menuItems 
			menuItem = new JMenuItem("Atoms");
			menuItem.addActionListener(new Atom3DActionListener());
			menuItem.setToolTipText("Switch off atoms");
			menuItem = new JMenuItem("Generate Signature");
			menuItem.setToolTipText("Generate the signature of a given object");
			menuItem.addActionListener(new GenerateSignatureListener());
			menu.add(menuItem);
			menuItem = new JMenuItem("Get Distance");
			menuItem.setToolTipText("Get the distance between two given atoms");
			menuItem.addActionListener(new DistanceListener());
			menu.add(menuItem);
			menuItem = new JMenuItem("View Variables");
			menuItem.setToolTipText("View the existing set variables");
			menuItem.addActionListener(new ViewVariablesListener());
			menu.add(menuItem);
			add(menu);

			menu = new JMenu("Actions");
// 			item = new JMenuItem("Elastic network interpolation");
// 			item.addActionListener(new ElasticNetworkListener());
// 			menu.add(item);
// 			item = new JMenuItem("Minimize");
// 			item.addActionListener(new MinimizeListener());
// 			menu.add(item);
			item = new JMenuItem("Select Object");
			item.addActionListener(new SelectListener(nanotilerApp));
			item.setToolTipText("Select an object");
			menu.add(item);
			item = new JMenuItem("Shift Object");
			item.addActionListener(new ShiftListener(nanotilerApp));
			item.setToolTipText("Shift an Object");
			menu.add(item);
			item = new JMenuItem("Rename Object");
			item.addActionListener(new RenameListener(nanotilerApp));
			item.setToolTipText("Rename an Object");
			menu.add(item);
// 			item = new JMenuItem("Quader");
// 			item.addActionListener(new QuaderListener(nanotilerApp));
// 			item.setToolTipText("Run quader command");
// 			menu.add(item);
			item = new JMenuItem("Rotate");
			item.addActionListener(new ManualRotateListener(nanotilerApp));
			item.setToolTipText("Use Rotate command");
			menu.add(item);

			// add(menu);

			menu = new JMenu("Generate");

// 			item = new JMenuItem("Strand");
// 			item.addActionListener(new NewStrandActionListener());
// 			item.setToolTipText("Create Strand");
// 			menu.add(item);

			// item = new JMenuItem("Stem");
			// item.addActionListener(new NewStemActionListener());
			// item.setToolTipText("Add stem to structure");
			// menu.add(item);

			item = new JMenuItem("Cubic grid");
			item.addActionListener(new NewGridActionListener());
			item.setToolTipText("Generate cube");
			menu.add(item);
			item = new JMenuItem("Platonic solid");
			item.addActionListener(new PlatonicSolidListener());
			item.setToolTipText("Generate Tetrahedron, Cube, Octahedron, Dodecahedron, or Icosahedron");
			menu.add(item);
			item = new JMenuItem("Planar shape");
			item.addActionListener(new PlanarListener());
			item.setToolTipText("Generate triangle, square, pentagon, hexagon etc.");
			menu.add(item);
			//item = new JMenuItem("Prism shape");
			//item.addActionListener(new PrismListener());
			//item.setToolTipText("Generate prism: double triangle, square, pentagon, hexagon etc.");
			//menu.add(item);
// 			item = new JMenuItem("Stem Tiling");
// 			item.addActionListener(new StemTilerListener());
// 			item.setToolTipText("Trace currently selected object with a set of stems.");
// 			menu.add(item);
// 			item = new JMenuItem("Graphit grid");
// 			item.addActionListener(new GraphitGridActionListener());
// 			item.setToolTipText("Generate graphit grid");
// 			menu.add(item);
			
			item = new JMenuItem("Helix");
			item.addActionListener(new AddHelixListener());
			item.setToolTipText("Insert Helix between two branch descriptors");
			menu.add(item);

			item = new JMenuItem("Link");
			item.addActionListener(new AddGenerateLinkListener());
			item.setToolTipText("Generate a link between two specified objects");
			menu.add(item);

			item = new JMenuItem("Covalent bonds");
			item.addActionListener(new GenerateAtomLinksListener());
			item.setToolTipText("Generate a link between any two atoms which are within a given distance of one another");
			//menu.add(item);

			item = new JMenuItem("Synthesize Object");
			item.setToolTipText("Synthesize an object of specified type and location");
			item.addActionListener(new AddSynthListener());
			menu.add(item);
			item = new JMenuItem("Basepair Constraint");
			item.addActionListener(new AddBasepairsListener());
			menu.add(item);
			item = new JMenuItem("Helix Constraint");
			item.addActionListener(new HelixConstraintListener());
			menu.add(item);

			add(menu);

			menu = new JMenu("Optimize");
			item = new JMenuItem("Base Pairs");
			item.setToolTipText("Launch base pair optimization wizard");
			item.addActionListener(new BasepairOptimizationWizardListener());
			menu.add(item);
			item = new JMenuItem("Helices");
			item.addActionListener(new OptimizeHelicesListener());
			menu.add(item);
			add(menu);

			menu = new JMenu("Building blocks");
			item = new JMenuItem("Load Database");
			item.addActionListener(new LoadJunctionListener());
			menu.add(item);
// 			item = new JMenuItem("Junction Database Info");
// 			item.addActionListener(new JunctionInfoListener());
// 			menu.add(item);
			item = new JMenuItem("Database Info");
			item.addActionListener(new TilingStatisticsListener());
			menu.add(item);
			item = new JMenuItem("Place");
			item.addActionListener(new PlaceJunctionListener());
			menu.add(item);
			item = new JMenuItem("Grow");
			item.addActionListener(new GrowListener());
			menu.add(item);
			item = new JMenuItem("Grow Graph");
			item.addActionListener(new GrowGraphListener());
			menu.add(item);

// 			item = new JMenuItem("Secondary Structure Editor");
// 			item.addActionListener(new SecondaryStructureEditorListener());
// 			menu.add(item);
			add(menu);

// 			menu = new JMenu("Lattice");
// 			menu.setToolTipText("Define lattice (cell and space group) parameters");
// 			item = new JMenuItem("Cell");
// 			// item.addActionListener(new CellListener());
// 			item.setToolTipText("Define lattice cell parameters");
// 			menu.add(item);
// 			item = new JMenuItem("Space Group");
// 			// item.addActionListener(new SpaceGroupListener());
// 			item.setToolTipText("Define space group");
// 			menu.add(item);
// 			add(menu);

			menu = new JMenu("Help");
			//item = new JMenuItem("NanoTiler Wiki");
			//item.addActionListener(new NanoTilerWikiListener());
			//menu.add(item);
			item = new JMenuItem("NanoTiler Tutorial");
			item.setToolTipText("Open a webpage containing a NanoTiler tutorial");
			item.addActionListener(new NanoTilerTutorialListener());
			menu.add(item);
	                item = new JMenuItem("Command Help");
			item.setToolTipText("A list of all commands and help for each");
			item.addActionListener(new CommandHelpListener());
			menu.add(item);
			item = new JMenuItem("Graphics Help");
			item.setToolTipText("Provides all necessary information to effectively use the graphics display");
			item.addActionListener(new GraphicsHelpListener());
			menu.add(item);
			item = new JMenuItem("Welcome Window");
			item.setToolTipText("Launch the Welcome Window which is displayed when NanoTiler is initially opened.");
			item.addActionListener(new WelcomeWindowListener());
			menu.add(item);
			add(menu);
		}
	}

	/* SUB-PANELS */

	private class MaxDepthPanel extends CharPanelBase {

		MaxDepthPanel() {
			setPreferredSize(new Dimension(50, 50));
		}

		public void paint(Graphics g) {
			String s = "" + graphController.getGraph().getDepthMax();
			writeString(g, s, 1); 
		}

	}

	private class TopPanel extends JPanel {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1394592164988290863L;

		TopPanel() { 
			setBackground(params.colorBackground);
			setBorder(BorderFactory.createLineBorder(Color.black));
			setLayout(new FlowLayout());
// 			JButton button = new JButton("Quit");
// 			button.addActionListener(new ExitActionListener()); // performs exit action
// 			button.setToolTipText("Quit Program");
// 			add(button);
// 			button = new JButton("Load");
// 			button.addActionListener(new LoadActionListener()); // performs exit action
// 			button.setToolTipText("Load File");
// 			add(button);
// 			button = new JButton("Add");
// 			button.addActionListener(new AddObject3DActionListener()); // performs exit action
// 			button.setToolTipText("Adds File");
// 			add(button);
// 			button = new JButton("Save");
// 			button.addActionListener(new SaveActionListener()); // performs exit action
// 			button.setToolTipText("Save structure");
// 			add(button);
// 			button = new JButton("Clear");
// 			button.addActionListener(new ClearActionListener()); // performs exit action
// 			button.setToolTipText("Clear current structure");
// 			add(button);
                        
// 			add(new JSeparator());

		


//			button = new JButton("Strand");
//			button.addActionListener(new NewStrandActionListener()); // performs exit action
//			button.setToolTipText("Create Strand");
//			add(button);


// 			button = new JButton("Secondary Structure Editor");
// 			button.addActionListener(new SecondaryStructureEditorListener());
// 			button.setToolTipText("Edit the secondary structure");
// 			add(button);
//			JButton button = new JButton("Select");
//			button.addActionListener(new SelectListener(nanotilerApp));
//			button.setToolTipText("Select an object");
//			add(button);
// 			button = new JButton("Shift");
// 			button.addActionListener(new ShiftListener(nanotilerApp));
// 			button.setToolTipText("Shift an object");
// 			add(button);
// 			button = new JButton("Rename");
// 			button.addActionListener(new RenameListener(nanotilerApp));
// 			button.setToolTipText("Rename an object");
// 			add(button);
// 			button = new JButton("Chosen");
// 			button.addActionListener(new ChosenListener(nanotilerApp));
// 			button.setToolTipText("Display current selection");
//       			add(button);
			//button = new JButton("Quader");
			//button.addActionListener(new QuaderListener(nanotilerApp));
			//button.setToolTipText("Use quader command");
			//add(button);
// 			button = new JButton("Rotate");
// 			button.addActionListener(new ManualRotateListener(nanotilerApp));
// 			add(button);

//			button = new JButton("Grid");
//			button.addActionListener(new NewGridActionListener()); // performs exit action
//			button.setToolTipText("Insert Cubic Grid Wizard");
//			add(button);

//			button = new JButton("Hide");
//			button.addActionListener(new HideGraphicsActionListener());
//			button.setToolTipText("Hide graphics");
//			add(button);

		}
	} // TopPanel 



	private class RightPanel extends JPanel {

		/**
		 * 
		 */
		private static final long serialVersionUID = -5292034988486352842L;

		Object3DTreePanel selectionTree;

		private class MovePanel extends JPanel {
			/**
			 * 
			 */
			private static final long serialVersionUID = 3781759864250812197L;

			MovePanel() {
				setBackground(params.colorBackground);	
				setBorder(BorderFactory.createLineBorder(Color.black));
				setPreferredSize(new Dimension(params.movePanelWidth, 
						params.movePanelHeight));
				// setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
				setLayout(new GridLayout(6, 2));
				// JLabel label = new JLabel("Move object:");
				// 			label.setPreferredSize(new Dimension(params.movePanelButtonWidth, params.movePanelButtonHeight));
				// 			add(label);
				JButton button = new JButton("x+");
				button.setPreferredSize(new Dimension(params.movePanelButtonWidth, params.movePanelButtonHeight));
				button.setToolTipText("translate x+ direction");
				button.addActionListener(new MoveNorthListener());
				add(button);
				button = new JButton("x-");
				button.setPreferredSize(new Dimension(params.movePanelButtonWidth, params.movePanelButtonHeight));
				button.setToolTipText("translate x- direction");
				button.addActionListener(new MoveSouthListener());
				add(button);
				button = new JButton("y+");
				button.setPreferredSize(new Dimension(params.movePanelButtonWidth, params.movePanelButtonHeight));
				button.setToolTipText("translate y+ direction");
				button.addActionListener(new MoveRightListener());
				add(button);
				button = new JButton("y-");
				button.setPreferredSize(new Dimension(params.movePanelButtonWidth, params.movePanelButtonHeight));
				button.setToolTipText("translate y- direction");
				button.addActionListener(new MoveLeftListener());
				add(button);
				button = new JButton("z+");
				button.setPreferredSize(new Dimension(params.movePanelButtonWidth, params.movePanelButtonHeight));
				button.setToolTipText("translate z+ direction");
				button.addActionListener(new MoveUpListener());
				add(button);
				button = new JButton("z-");
				button.setPreferredSize(new Dimension(params.movePanelButtonWidth, params.movePanelButtonHeight));
				button.setToolTipText("translate z- direction");
				button.addActionListener(new MoveDownListener());
				add(button);

				button = new JButton("Rot");
				button.setPreferredSize(new Dimension(params.movePanelButtonWidth, params.movePanelButtonHeight));
				button.setToolTipText("Rotate");
				button.addActionListener(new RotateListener());
				add(button);
				button = new JButton(" "); // empty button
				button.setPreferredSize(new Dimension(params.movePanelButtonWidth, params.movePanelButtonHeight));
				add(button);
//				label = new JLabel("Hierarchy:");
//				label.setPreferredSize(new Dimension(params.movePanelButtonWidth, params.movePanelButtonHeight));
//				add(label);
//				add(new JSeparator());
				button = new JButton("HU");
				button.setPreferredSize(new Dimension(params.movePanelButtonWidth, params.movePanelButtonHeight));
				button.setToolTipText("move Hierarchy Up");
				button.addActionListener(new HierarchyUpListener());
				add(button);
				button = new JButton("HD");
				button.addActionListener(new HierarchyDownListener());
				button.setToolTipText("move Hierarchy Down");
				button.setPreferredSize(new Dimension(params.selectPanelButtonWidth, params.movePanelButtonHeight));
				add(button);
			}
		}


		RightPanel() {
			setBackground(params.colorBackground);
			setPreferredSize(new Dimension(params.rightPanelWidth, params.rightPanelHeight));
			setBorder(BorderFactory.createLineBorder(Color.black));
			setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
			JPanel rightHalf = new JPanel();
			rightHalf.setLayout(new BoxLayout(rightHalf, BoxLayout.Y_AXIS));
			rightHalf.setBorder(BorderFactory.createLineBorder(Color.black));
			JPanel leftHalf = new JPanel();
			JLabel treeLabel = new JLabel("Tree");
			treeLabel.setSize(new Dimension(50,25));

			GridBagLayout gridbag = new GridBagLayout();
			GridBagConstraints gridbagc = new GridBagConstraints();
			int[] rowH = new int[2];
			rowH[0] = 18;
			rowH[1] = params.rightPanelHeight - 18 ;
			gridbag.rowHeights = rowH;
			int[] columnW = new int[1];
			columnW[0] = params.rightPanelWidth/2 ;
			gridbag.columnWidths = columnW;
			gridbagc.gridx = 0;
			gridbagc.gridy = 0;
			gridbagc.fill = GridBagConstraints.BOTH;
			gridbag.setConstraints(treeLabel, gridbagc);

			leftHalf.setLayout(gridbag); 
			leftHalf.setBorder(BorderFactory.createLineBorder(Color.black));

			JLabel label = new JLabel("Move:   ");
			rightHalf.add(label);
			rightHalf.add(new MovePanel());
			rightHalf.add(new JSeparator());
			rightHalf.add(new JLabel("Select:  "));
			rightHalf.add(new SelectPanel(params.selectPanelParameters, graphController.getGraph()));
			Object3DController object3DController = graphController.getGraph();

			// selectionTree = new Object3DTreePanel(object3DController, 250, 300, false); // true stands for multi-selection
			// object3DController.addModelChangeListener(selectionTree); // alredy done in Object3DPanel constructor
			// selectionTree.addTreeSelectionListener(new GlobalTreeSelectionListener());
			gridbagc.gridx = 0;
			gridbagc.gridy = 1;
			gridbagc.weighty = 1.0;
			gridbagc.weightx = 1.0;
			// gridbag.setConstraints(selectionTree, gridbagc);

			leftHalf.add(treeLabel);
			// selectionTree.setSize(new Dimension(50, 200));
			// leftHalf.add(selectionTree);
//			leftHalf.add(new JLabel("Window"));
			// add(leftHalf);
			add(rightHalf);

		}
	} // RightPanel 

	private class LeftPanel extends JPanel {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		private class BlockPanel extends JPanel {
			/**
			 * 
			 */
			private static final long serialVersionUID = 8221200532104481130L;

			BlockPanel() {
				setBackground(params.colorBackground);
				setPreferredSize(new Dimension(params.blockPanelWidth, params.blockPanelHeight));
				setBorder(BorderFactory.createLineBorder(Color.black));

				// JButton button = new JButton("  Stem    ");
				// button.setPreferredSize(new Dimension(params.blockPanelButtonWidth, params.blockPanelButtonHeight));
				// button.addActionListener(new NewStemActionListener());
				// button.setToolTipText("Add stem to structure");
				// add(button);

				JButton button = new JButton("  Strand  ");
				button.setPreferredSize(new Dimension(params.blockPanelButtonWidth, params.blockPanelButtonHeight));
				button.addActionListener(new NewStrandActionListener());
				button.setToolTipText("Create strand with n atoms");
				add(button);

				button = new JButton("  Grid    ");
				button.setPreferredSize(new Dimension(params.blockPanelButtonWidth, params.blockPanelButtonHeight));
				button.addActionListener(new NewGridActionListener());
				button.setToolTipText("Insert Cubic Grid Wizard");
				add(button);

			}
		}

		private class EditPanel extends JPanel {
			/**
			 * 
			 */
			private static final long serialVersionUID = -2933863053809027476L;

			EditPanel() {
				setBackground(params.colorBackground);
				setPreferredSize(new Dimension(params.editPanelWidth, params.editPanelHeight));
				setBorder(BorderFactory.createLineBorder(Color.black));
				// setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

//				JButton button = new JButton(" connect  ");
//				button.addActionListener(new ConnectActionListener());
//				button.setPreferredSize(new Dimension(params.blockPanelButtonWidth, params.editPanelButtonHeight));
//				button.setToolTipText("Connect two atoms");
//				add(button);

//				button = new JButton("disconnect");
//				button.setPreferredSize(new Dimension(params.blockPanelButtonWidth, params.editPanelButtonHeight));
//				button.setToolTipText("Disconnect two currently connected atoms");
//				add(button);

//				button = new JButton("  remove  ");
//				button.addActionListener(new RemoveActionListener());
//				button.setPreferredSize(new Dimension(params.blockPanelButtonWidth, params.editPanelButtonHeight));
//				button.setToolTipText("Remove Link");
//				add(button);

			}
		}

		LeftPanel() {
			setBackground(params.colorBackground);
			setPreferredSize(new Dimension(params.leftPanelWidth, params.leftPanelHeight));
			setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
			// add(new BlockPanel());
			// add(new EditPanel());
		}
	} // LeftPanel 

	private class BottomPanel extends JPanel {

		// JEditorPane editor;

		/**
		 * 
		 */
		private static final long serialVersionUID = -233359782949400973L;

		BottomPanel(CommandApplication commandApplication) { 
			setBackground(params.colorBackground);
			setBorder(BorderFactory.createLineBorder(Color.black));
			setLayout(new BorderLayout());
			setPreferredSize(new Dimension(params.bottomPanelWidth, params.bottomPanelHeight));

			JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
			JScrollPane scrollPane = new JScrollPane(new BottomPanelCenter());

			// tabbedPane.addTab("Interactions", scrollPane);

			tabbedPane.addTab("Script commands", null, new ScriptCommandDisplay(commandApplication), "Script commands");
			tabbedPane.addTab("Secondary Structure", null, new RnaSecondaryStructureDisplay(), "Display Secondary Structure");
			// tabbedPane.addTab("Junctions", null, new JunctionDisplay(), "Display Junctions");


			add(tabbedPane);
			//add(new BottomPanelCenter(), BorderLayout.CENTER);
			setVisible(true);

			// editor = new JEditorPane();
			// editor.setEditable(false);
			// editor.setPreferredSize(new Dimension(params.bottomPanelWidth-10, 
			//			      params.bottomPanelHeight-10));
			// editor.setBorder(BorderFactory.createLineBorder(Color.black));
			// add(editor);
		}

		private class JunctionDisplay extends JPanel {
			private JTextArea textPane;

			public JunctionDisplay() {
				setPreferredSize(new Dimension(params.bottomPanelWidth, params.bottomPanelHeight));
				setLayout(new BorderLayout());
				textPane = new JTextArea();
				textPane.setEditable(false);
				textPane.setText("No Information Provided");

				graphController.addModelChangeListener(new ModelChangeListener() {
					public void modelChanged(ModelChangeEvent e) {
						JunctionCollector collector = graphController.generateJunctions();
						String text = "";
						if (collector != null) {
							for(int i = 0; i < collector.getJunctionCount(); ++i) {
								text += collector.getJunction(i).toString() + NEWLINE;
								text += "Branches:" + NEWLINE;
								for(int j = 0; j < collector.getJunction(i).getBranchCount(); ++j) {
									text += "\t" + collector.getJunction(i).getBranch(j).infoString();

								}
								text += NEWLINE + "Strands:" + NEWLINE;
								for(int j = 0; j < collector.getJunction(i).getStrandCount(); ++j) {
									text +=  "\t" + collector.getJunction(i).getStrand(j).sequenceString();
								}

								text += NEWLINE + NEWLINE + NEWLINE;

							}
						}
						textPane.setText(text);
					}
				});

				add(new JScrollPane(textPane));
			}


		}

		/**
		 * Displays an RNA secondary structure
		 * @author Calvin Grunewald
		 */
		private class RnaSecondaryStructureDisplay extends JPanel {
			private JTextArea textPane;



			public RnaSecondaryStructureDisplay() {

				setPreferredSize(new Dimension(params.bottomPanelWidth, params.bottomPanelHeight));
				setLayout(new BorderLayout());
				textPane = new JTextArea();

				textPane.setEditable(false);
				textPane.setText("No Information Provided");
				textPane.setFont(new Font("Monospaced", Font.PLAIN, 14));

				graphController.addModelChangeListener(new ModelChangeListener() {
					public void modelChanged(ModelChangeEvent e) {
						SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();

						String secText = "undefined";
						SecondaryStructure sec = null;
						try {
							sec = graphController.generateSecondaryStructure();
							if (sec != null) {
							    secText = writer.writeString(sec);
							}
						}
						catch (DuplicateNameException dne) {
							log.warning("Duplicate sequence names detected! Cannot generate secondary structure.");
						}
						textPane.setText(secText);
						repaint();
					}
				});

				add(new JScrollPane(textPane));


			}
		}




		private class BottomPanelCenter extends JPanel implements Scrollable {

			/**
			 * 
			 */
			private static final long serialVersionUID = -8705671670248050084L;

			private static final int COL_WIDTH = 10;
			private static final int ROW_HEIGHT = 14;

			BottomPanelCenter() {
				setBackground(params.colorBackground);
				setBorder(BorderFactory.createLineBorder(Color.black));
				// setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

				// TODO make the preferred size dynamic based upon the
				//    information to be displayed
				setPreferredSize(new Dimension(3 * params.bottomPanelCenterWidth, 
						3 * params.bottomPanelCenterHeight));	
			}
			/** returns x position for character in window.
			 * TODO : use parameter object instead of hard-coded constant
			 * @param row : column of character
			 * @return  : x position of character to be drawn
			 */
			private int getCharPosX(int col) {
				return 10 + col * COL_WIDTH;
			}

			/** returns y position for character in window.
			 * TODO : use parameter object instead of hard-coded constant
			 * @param row : row of character
			 * @return  : y position of character to be drawn
			 */
			private int getCharPosY(int row) {
				return 20 + row * ROW_HEIGHT; 
			}

			/** writes single character from character array for certain position */
			private void writeChar(Graphics g, char[] chars, int row, int col) {
				int xpos = getCharPosX(col);
				int ypos = getCharPosY(row);
				// char[] chars = new char[1];
				// chars[0] = c;
				g.drawChars(chars, col, 1, xpos, ypos);
			}

			/** writes sequence in n'th row of window */
			private void writeString(Graphics g, String s, int n) {
				char[] charArray = s.toCharArray();
				for (int i = 0; i < s.length(); ++i) {
					writeChar(g, charArray, n + 1, i);
				}
			}

			/** draw's i'th SequenceBindingSite defined by GraphController
			 * TODO : not clean yet, accessing model class SequenceBinding site directly 
			 */
			private void drawBindingSite(Graphics g, int n) {
				BindingSiteController bindingSites = graphController.getBindingSites();
				SequenceBindingSite site = bindingSites.getBindingSite(n);
				int seqId = 0; // graphController.getBindingSiteSequenceId(n);
				// start char :
				int len = site.getBindingSequence().size();
				int startId = site.getBindingSequence().getIndex(0);
				int stopId = site.getBindingSequence().getIndex(len-1);
				int startX = getCharPosX(startId);
				int stopX = getCharPosX(stopId);
				int posY = getCharPosY(seqId+1) + 3;
				g.drawLine(startX, posY, stopX, posY);
			}

			/** draws n'th stem defined by GraphController
			 * TODO : needs complete rewrite!
			 */
			private void drawStem(Graphics g, int n) {
				LinkController links = graphController.getLinks();
				Link link = links.get(n);
				if ((link.getObj1() instanceof SequenceBindingSite)
						&& (link.getObj2() instanceof SequenceBindingSite)) {
					SequenceBindingSite site1 = (SequenceBindingSite)link.getObj1();
					SequenceBindingSite site2 = (SequenceBindingSite)link.getObj2();
					SequenceController sequences = graphController.getSequences();
					int seqId1 = sequences.getSequenceId(site1.getBindingSequence().getSequence());
					int seqId2 = sequences.getSequenceId(site2.getBindingSequence().getSequence());
					log.info("Connectected sequences: " + seqId1 + " " + seqId2);
					// start char :
					if ((seqId1 >= 0) && (seqId2 >= 0)) {
						SequenceSubset subset1 = site1.getBindingSequence();
						SequenceSubset subset2 = site2.getBindingSequence();
						int startId1 = subset1.getIndex(0);
						int stopId1 =  subset1.getIndex(subset1.size()-1);
						int startId2 = subset2.getIndex(0);
						int stopId2 =  subset2.getIndex(subset2.size()-1);
						int startX = getCharPosX((startId1 + stopId1)/2);
						int stopX = getCharPosX((startId2 + stopId2)/2);
						int startY = getCharPosY(seqId1) + 14 + 3;
						int stopY = getCharPosY(seqId2) + 14 + 3;
						g.drawLine(startX, startY, stopX, stopY);
					}
					else {
						log.info("Could not find sequence object in controller!");
					}
				}
				else {
				}
			}

			/** returns info string describing sequence */
			String generateSequenceInfoString(Sequence seq) {
				String result = "";
				if (seq.getParentObject() != null) {
					// writing full id name:
					// result = result + Object3DTools.getFullName((Object3D)(seq.getParent())) + " : ";
					Object3D parent = (Object3D)(seq.getParentObject());
					if (parent instanceof RnaStrand) {
						result = "RNA; "; 
					}
//					else if (parent instanceof DnaStrand) {
//					result = "DNA; "; 
//					}
//					else if (parent instanceof ProteinStrand) {
//					result = "Protein; "; 
//					}
					result = StringTools.stringWithLength(result + seq.size() + " " + parent.getName(), 20, " ") + " : ";
				}
				result = result + seq.sequenceString();
				return result;
			}

			/** writes out all defined sequences */
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				int numObjects = graphController.getGraph().getObjectCount();
				SequenceController sequences = graphController.getSequences();
				int numSeq = sequences.getSequenceCount();
				BindingSiteController bindingSites = graphController.getBindingSites();
				int numSites = bindingSites.getBindingSiteCount();
				LinkController links = graphController.getLinks();
				int numLinks = links.size();
				String headerTxt = NEWLINE + "Objects: " + numObjects + " Sequences: " + numSeq 
				+ " Links: " + numLinks + NEWLINE;
				writeString(g, headerTxt, -1);

				for (int i = 0; i < numSeq; ++i) {
					String seqInfoString = generateSequenceInfoString( 
							sequences.getSequence(i));

					writeString(g, seqInfoString, i);
				} 
				for (int i = 0; i < numSites; ++i) {
					drawBindingSite(g, i);
				}
				for (int i = 0; i < numLinks; ++i) {
					drawStem(g, i);
				}

				// writeString(g, graphController.getGraph().getGraphText(),8);
			}

			public Dimension getPreferredScrollableViewportSize() {
				return getPreferredSize();


			}


			public int getScrollableBlockIncrement(Rectangle visibleRect, int orientation,
					int direction) {

				if(orientation == SwingConstants.HORIZONTAL) {
					return COL_WIDTH;
				}
				else
					return ROW_HEIGHT;
			}


			public boolean getScrollableTracksViewportHeight() {
				return false;


			}



			public boolean getScrollableTracksViewportWidth() {
				return false;



			}



			public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, 
					int direction) {

				return getScrollableBlockIncrement(visibleRect, orientation, direction);


			}
		} // class BottomPanelCenter

//		private class BottomPanelLeft extends JPanel {
//		/**
//		* 
//		*/
//		private static final long serialVersionUID = -4652128408294470618L;

//		BottomPanelLeft() {
//		setBackground(params.colorBackground);
//		setBorder(BorderFactory.createLineBorder(Color.black));
//		// setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
//		setPreferredSize(new Dimension(params.bottomPanelLeftWidth, params.bottomPanelLeftHeight));	
//		JButton button = new JButton("Add 3D object");
//		button.setPreferredSize(new Dimension(params.bottomPanelLeftButtonWidth, params.buttonHeight));	
//		button.addActionListener(new InputObject3DActionListener());
//		add(button);
//		button = new JButton("Add stem");	
//		button.setPreferredSize(new Dimension(params.bottomPanelLeftButtonWidth, params.buttonHeight));	
//		button.addActionListener(new NewStemActionListener());
//		add(button);
//		}
//		}
	} // BottomPanel

	NanoTiler(Object3DGraphController controller) {
		super(controller); // calling contructor of NanoTilerScripter
		if (log == null) {
			log = Logger.getAnonymousLogger();
			// log.addHandler(new ConsoleHandler());
		}
		if ((nanotilerHome == null) || (nanotilerHome.length() == 0)) {
			log.severe("NANOTILER_HOME variable is undefined!");
			System.exit(1);
		}
		this.nanotilerApp = this;
		// graphController.addModelChangeListener(new LocalModelChangeListener());
		graphController.addModelChangeListener(this);
		try {
			String nucdbName = rb.getString("nucleotides");
			if ((nucdbName == null) || (nucdbName.length() == 0)) {
				log.severe("Warning: undefined nucleotide database name! Use property nucleotides in property file");
			}
			else {
				char c = nucdbName.charAt(0);
				if (c != SLASH.charAt(0)) {
					nucdbName = nanotilerHome + SLASH + nucdbName;
				}
				graphController.readNucleotideDB(nucdbName);
			}
			//TODO: no hardcoding
		}
		catch (Object3DGraphControllerException e) {
			log.severe("Warning: could not read nucleotide database: " + e.getMessage());
		}
	} // NanoTiler constructor

	public void addComponentsToFrame(JFrame frame) {
		frame.setBackground(params.colorBackground);
		Container pane = frame.getContentPane();
		frame.setJMenuBar(new RnaMenuBar(this));
		if (!(pane.getLayout() instanceof BorderLayout)) {
			pane.add(new JLabel("Container doesn't use BorderLayout!"));
			return;
		}
		if (RIGHT_TO_LEFT) {
			pane.setComponentOrientation(
					java.awt.ComponentOrientation.RIGHT_TO_LEFT);
		}
		// center consists of two windows, one for x-y projection, one for x-z projection 
		JPanel centerPanel = new JPanel();
		//	GridBagLayout gbl = new GridBagLayout();
		if(openGLGraphics){
		    centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.PAGE_AXIS));//	centerPanel.setLayout(gbl);
		    
		    centerPanel.setBorder(BorderFactory.createLineBorder(Color.black));
		    //GraphControlPanel graphPanel = new GraphControlPanel(params, graphController);
		    CameraController xyProjector = new SimpleCameraController(new SimpleCamera());
		    xyProjector.setOrigin2D((int)(params.xScreenMax / 2), (int)(params.yScreenMax / 2));
		    //		xyProjector.setXYDimensions(params.xSpaceMin, params.xSpaceMax,
		    //		params.zSpaceMin, params.zSpaceMax,
		    //		params.xScreenMin, params.xScreenMax,
		    //		params.yScreenMin, params.yScreenMax);
		    //graphPanel.setCameraController(xyProjector);
		    //GeometryPainter geometryPainter = 
		    //    geometryPainterFactory.createPainter(NanoTilerGeometryPainterFactory.SOLID_PAINTER);
		    //GeneralGeometryPainter newPainter = new GeneralGeometryPainter(params, geometryPainter, graphController);
		    //newPainter.setCameraController(xyProjector); // keep same camera settings
		    //graphPanel.setPainter(newPainter);
		    //centerPanel.add(graphPanel);
		    
		    //		gbc.gridx = 2;
		    //		gbc.gridy = 1;
		    //		gbl.setConstraints(YSliderPanel,gbc);
		    //		centerPanel.add(YSliderPanel);
		    
		    graphControllerDisplayManager = new GraphControllerDisplayManager(graphController, nanotilerApp);

		    //graphControllPanels.add(graphPanel);
		    // centerPanel.add(new JSeparator());
		    //		graphPanel = new GraphControlPanel(params, graphController);
		    //		// set x-z view:
		    //		CameraController xzProjector = new SimpleCameraController(new SimpleCamera());
		    //		//         xzProjector.setXZDimensions(params.xSpaceMin, params.xSpaceMax,
		    //		//         		                    params.zSpaceMin, params.zSpaceMax,
		    //		//         		                    params.xScreenMin, params.xScreenMax,
		    //		//         		                    params.yScreenMin, params.yScreenMax);
		    //		graphPanel.setCameraController(xzProjector);
		    //		centerPanel.add(graphPanel);
		

		    centerPanel.add(graphControllerDisplayManager);
		}
		else{
		    RnaGuiParameters params = new RnaGuiParameters();
		    GraphControlPanel graphPanel = new GraphControlPanel(params, graphController);
		    CameraController xyProjector = new SimpleCameraController(new SimpleCamera());
		    xyProjector.setOrigin2D((int)(params.xScreenMax / 2), (int)(params.yScreenMax / 2));
		    graphPanel.setCameraController(xyProjector);
		    GeometryPainterFactory geometryPainterFactory = new NanoTilerGeometryPainterFactory();
		    GeometryPainter geometryPainter = 
			geometryPainterFactory.createPainter(NanoTilerGeometryPainterFactory.SOLID_PAINTER);
		    GeneralGeometryPainter newPainter = new GeneralGeometryPainter(params, geometryPainter, graphController);
		    newPainter.setCameraController(xyProjector); // keep same camera settings
		    graphPanel.setPainter(newPainter);
		    graphController.addModelChangeListener(graphPanel.graphPanel);
		    centerPanel.add(graphPanel);
		}
		
		pane.add(centerPanel, BorderLayout.CENTER);
		// top:
		pane.add(new TopPanel(), BorderLayout.NORTH);      
		// left:
		// pane.add(new LeftPanel(), BorderLayout.WEST);
		// right:
		// pane.add(new RightPanel(), BorderLayout.EAST);
		// bottom:
		BottomPanel bottom = new BottomPanel(this);
		pane.add(bottom, BorderLayout.SOUTH);    

	}
        /** launch the welcome window for nanotiler */
        private void launchWelcomeWindow(){
	    log.fine("Welcome window wizard started");
	    GeneralWizard wizard = new WelcomeWindowWizard(nanotilerApp);
	    wizard.launchWizard(graphController,rootFrame);
	}
        /**  */
        public void setGLGraphics(boolean choice){
	    openGLGraphics = choice;
	}
        /** window launched to choose whether opengl graphics or java graphics will be used */
        private void  chooseGLGraphics(){
	    // boolean choice = true;
	    String msg = "Use Java based graphics? Ok confirms, cancel starts OpenGL graphics.";
	    String title = "Choose Graphics Style";
	    JFrame choiceFrame = new JFrame("Use Java graphics? Ok confirms, cancel starts OpenGL graphics.");
	    // JPanel mainPanel = new JPanel();
	    int choice = JOptionPane.showConfirmDialog(null,
		       msg, title, JOptionPane.OK_CANCEL_OPTION);
	    if (choice == 0) {
		log.info("Java Foundation class graphics chosen!");
		openGLGraphics = false;
// 		graphController.clear();
// 		repaintApp();
	    }
	    else {
		log.info("OpenGL graphics chosen!");
		openGLGraphics = true;
	    }

// 	    mainPanel.setLayout(new BorderLayout());
// 	    final String[] graphicsChoices = {"Java Graphics","OpenGL Graphics"};
// 	    final JComboBox choiceBox = new JComboBox(graphicsChoices);
// 	    JButton doneButton = new JButton("Done");
// 	    doneButton.addActionListener(new ActionListener(){
// 		    public void actionPerformed(ActionEvent e){
// 			String graphicsChoice = (String)choiceBox.getSelectedItem();
// 			if(graphicsChoice.equals(graphicsChoices[0])){
// 			    openGLGraphics = false;
// 			}
// 			else{
// 			    openGLGraphics = true;
// 			}

// 		    }

// 		});
// 	    mainPanel.add(choiceBox, BorderLayout.NORTH);
// 	    mainPanel.add(doneButton, BorderLayout.SOUTH);

// 	    choiceFrame.add(mainPanel);
// 	    choiceFrame.pack();
// 	    choiceFrame.setVisible(true);
        }

        /** components added to own registry and update system */
	private void registerComponent(Component c) {
		frames.add(c);
	}

	/** sets log handler of application */
	public void setLogger(Logger logger) {
		log = logger;
	}

	/** defines resource bundle */
//	private void setResourceBundle(String fileName) {
//	rb = ResourceBundle.getBundle(fileName);
//	}


	private void repaintApp( ) {
		rootFrame.repaint();
		for (int i = 0; i < frames.size(); ++i) {
			if (frames.get(i) instanceof Component) {
				Component c = (Component)(frames.get(i));
				c.repaint();
			}
		}
		log.fine("called repaintApp!"); //  Object tree:");
		// graphController.getGraph().printTree(System.out);
	}

	private void setRootFrame(JFrame frame) {
		rootFrame = frame;
	}

	/**
	 * Create the GUI and show it.  For thread safety,
	 * this method should be invoked from the
	 * event-dispatching thread.
	 */
	private static void createAndShowGUI() {
		//Make sure we have nice window decorations.
		JFrame.setDefaultLookAndFeelDecorated(true);
		// Dimension preferred = new Dimension(1100, 742); // default: 879, 742

		//Create and set up the window.
		JFrame frame = new JFrame("NanoTiler");
		// frame.setPreferredSize(preferred);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Object3DGraphController controller = new Object3DGraphController();
		NanoTiler app = new NanoTiler(controller);
		Logger debugLog = Logger.getLogger("NanoTiler_debug");

		String debugFileName = "NanoTiler_debug.log"; // TODO : move to resource bundle file
		try {
			Handler debugLogFileHandler = new FileHandler(debugFileName);
			debugLogFileHandler.setLevel(Level.FINEST); // for debugging: report all messages
			debugLogFileHandler.setFormatter(new SimpleFormatter()); // not XML but raw text output
			debugLog.addHandler(debugLogFileHandler);
			debugLog.info("Writing debug info to file : " + debugFileName);
		}
		catch (IOException e) {
			debugLog.severe("Could not open debug output file name: " + debugFileName);
		}	
		debugLog.setLevel(Level.FINE);
		app.setLogger(debugLog);

		app.chooseGLGraphics();
		//Set up the content pane.
		app.addComponentsToFrame(frame);

		// app.setResourceBundle("NanoTiler.properties");
		//Use the content pane's default BorderLayout. No need for
		//setLayout(new BorderLayout());
		app.setRootFrame(frame);
		//Display the window.
		frame.pack();
		frame.setSize(1100,700);
		frame.setVisible(true);

		
		try {
		    app.runDefaultScript();
		}
		catch (CommandException ce) {
		    System.out.println("Could not execute default script: " + ce.getMessage());
		    System.exit(1);
		}
		catch (IOException ioe) {
		    System.out.println("Could not execute default script: " + ioe.getMessage());
		    System.exit(1);
		}
		app.launchWelcomeWindow();
		// frame.repaint();
		
	}

	public static void main(String[] args) {
		/**TODO: no hard-coding. Add environment variable*/
		//Schedule a job for the event-dispatching thread:
		//creating and showing this application's GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	
}

/*
      private final class NewStemActionListener implements ActionListener {	
      public void actionPerformed(ActionEvent e) {
      log.fine("New stem button pressed!");

      GeneralWizard wizard = new StemWizard();
      wizard.launchWizard(graphController, rootFrame);
      // 	    RnaBlock stem = new RnaPhysicalBlock();
      // 	    BeanEditor beanEditor = new XMLBeanEditor();
      // 	    // stem = (RnaBlock)beanEditor.launchEdit(stem, null, rootFrame);
      // 	    beanEditor.launchEdit(stem, rootFrame);
      //     	    if(stem != null) {
      // 		// write Xml for debugging purposes

      // 		// append to graph:
      // 		// graphController.addGraph(stem);

      //     	    }
      // 	    else {
      // 	    }

      //     	    // read file and create graph
      //     	    repaintApp(); // update graphics window   
      }
      } // LoadActionListener
 */

