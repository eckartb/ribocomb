/** Application for NanoTiler Application
 * 
 */
package rnadesign.designapp.rnagui;

/**
 * @author Eckart Bindewald
 *
 */
public class NanoTilerParameters {
	
    int depthMax = 4;
    
    int moveStep = 2;
    
    double circleRad = 10.0;
    
    double rectWidth = 10.0;
    
    double rectHeight = 10.0;
    
    double xScreenMax = 600;
    
    double xScreenMin = 0;
    
    double yScreenMax = 300;
    
    double yScreenMin = 0;
    
    double xSpaceMax = 100;
    
    double xSpaceMin = -100;
    
    double ySpaceMax = -100;
    
    double ySpaceMin = +100;
    
    double zSpaceMax = -200;
    
    double zSpaceMin = 200;
    
    int xMaxBottomPanel = 600;
	
    int yMaxBottomPanel = 200;
	
    String workDirectory = "."; // /home/bas/bindewae/project/rnano/examples";
	
}
