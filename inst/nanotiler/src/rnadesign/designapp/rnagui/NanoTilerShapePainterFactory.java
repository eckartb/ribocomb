package rnadesign.designapp.rnagui;

/** creates all the shape painters of the NanoTiler factory */
public class NanoTilerShapePainterFactory implements Shape3DPainterFactory {

    public static final int STANDARD_PAINTER = 2;

    public static final int SOLID_PAINTER = 2;

    public static final int WIRE_PAINTER = 3;

    /** @TODO NOT YET IMPLEMENTED! */
    public Shape3DPainter createWirePainter() {
	return null;
    }

    /** @TODO NOT YET IMPLEMENTED! */
    public Shape3DPainter createSolidPainter() {
	return null;
    }

    public Shape3DPainter createPainter() {
	return createPainter(STANDARD_PAINTER);
    }
    public Shape3DPainter createPainter(int styleId) {
	Shape3DPainter shapePainter = null;
	switch (styleId) {
	case SOLID_PAINTER:
	    shapePainter = createSolidPainter();
	    break;
	case WIRE_PAINTER:
	    shapePainter = createWirePainter();
	    break;
        default:
	    // TODO error!
	    // shapePainter = createPainter();
	}
	return shapePainter;
    }
}
