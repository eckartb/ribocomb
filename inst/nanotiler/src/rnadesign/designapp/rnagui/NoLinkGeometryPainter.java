package rnadesign.designapp.rnagui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.util.Properties;

import rnadesign.rnacontrol.CameraController;
import tools3d.Character3D;
import tools3d.Drawable;
import tools3d.Edge3D;
import tools3d.Face3D;
import tools3d.GeometryColorModel;
import tools3d.Point3D;
import tools3d.Vector3D;

/** paints only points; no edges */
public class NoLinkGeometryPainter extends AbstractGeometryPainter {

    public static final int FACE_POINT_MAX = 3;

    private double pointRadius = 2.0;

    final static BasicStroke stroke = new BasicStroke(2.0f);

    private int[] xTmp = new int[FACE_POINT_MAX];
    private int[] yTmp = new int[FACE_POINT_MAX];

    /** paints character for each point */
    public void paintCharacter(Graphics g, Character3D point) {
	if (!paintCharacterMode) {
	    return; // do not draw characters if not set
	}
	Graphics2D g2 = (Graphics2D)g;
	Color saveColor = g2.getColor();
	char[] characters = new char[1];
	characters[0] = point.getCharacter();
	if (point.getAppearance() != null) {
	    g2.setColor(point.getAppearance().getColor());
	}
	else {
	    g2.setColor(getDefaultPaintColor(point));
	}
	Vector3D pos3d = point.getPosition();
	CameraController camera = getCameraController();
	Point2D point2d = camera.project(point.getPosition());
	g.drawChars(characters, 0, 1, 
		     (int)point2d.getX(), (int)point2d.getY());
	g.setColor(saveColor);
    }

    /** paints single point */
    public void paintPoint(Graphics g, Point3D point) {
	Character3D char3D;
	Graphics2D g2 = (Graphics2D)g;

	CameraController camera = getCameraController();

	Vector3D pos3d = point.getPosition();
	Point2D point2d = camera.project(point.getPosition());
	if ((point2d.getX() < 0) || (point2d.getY() < 0)) {
	    return;
	}

	Color saveColor = g2.getColor();
	Color color;
	if (point.getAppearance() != null) {
	    color = point.getAppearance().getColor();
	}
	else {
	    color = getDefaultPaintColor(point);
	}
	g2.setColor(color);
	double radius = point.getRadius();
	if (radius < this.pointRadius) {
	    radius = this.pointRadius;
	}
	radius *= camera.getZoom();
	if (radius > 3.0) {
	    g2.fill(new Ellipse2D.Double(point2d.getX(), point2d.getY(),
					 radius, radius));
	}
	else { // shortcut for really small entities:
	    g2.drawRect((int)(point2d.getX()),(int)(point2d.getY()),(int)radius,(int)radius);
	    g.setColor(saveColor);
	    return;
	}
	
	double tmpRadius = radius;
	int numCircles = 3;
	int offSetX = (int)(0.5*radius/(double)numCircles);
	int offSetY = (int)(0.5*radius/(double)numCircles);
 	Color currentPaint = color;
	for(int i = 1; i < numCircles; ++i) {
	    g2.setPaint(currentPaint);
	    g2.setStroke(stroke);
	    tmpRadius = radius - offSetX*3*i;
	    if (tmpRadius < 3.0) {
		break;
	    }
	    g2.fill(new Ellipse2D.Double(point2d.getX() + offSetX*i, point2d.getY() + offSetY*i, tmpRadius, tmpRadius));
	    currentPaint = currentPaint.brighter();
	}
	
	g.setColor(saveColor);
    }

    /** paints single edge */
    public void paintEdge(Graphics g, Edge3D edge) {
	Graphics2D g2 = (Graphics2D)g;
	Color saveColor = g2.getColor();
	Properties properties = edge.getProperties();
	String physicalKey = "physical";
	CameraController camera = getCameraController();

 	if (edge.getAppearance() != null) {
 	    g2.setColor(edge.getAppearance().getColor());
 	}
 	else if ((properties != null) 
 		 && (physicalKey.equals(properties.getProperty("interaction_type")))) {
   	    g2.setColor(geometryColorModel.computeColor(null,
							null,
							edge.getProperties(),
							null,
							camera.getCamera()));
 	}
  	else {
	    g2.setColor(getDefaultPaintColor(edge));
  	}

	Vector3D first3d = edge.getFirst();
	Vector3D second3d = edge.getSecond();
	Point2D first2d = camera.project(first3d);
	Point2D second2d = camera.project(second3d);
//  	g.drawLine((int)first2d.getX(), (int)first2d.getY(),
//   		   (int)second2d.getX(), (int)second2d.getY());
// 	g2.fill(new Ellipse2D.Double(point2d.getX(), point2d.getY(),
// 				     pointRadius, pointRadius));
	g2.setColor(saveColor);
    }

    /** paints single face */
    public void paintFace(Graphics g, Face3D face) {
	int nn = face.size();
	if (nn > FACE_POINT_MAX) {
	    nn = FACE_POINT_MAX;
	}
	if (nn < 3) {
	    return;
	}

	CameraController camera = getCameraController();
	GeometryColorModel colorModel = getGeometryColorModel();
	for (int i = 0; i < nn; ++i) {
	    Vector3D v = face.getPoint(i);
	    Point2D p2d = camera.project(v);
	    xTmp[i] = (int)(p2d.getX());
	    yTmp[i] = (int)(p2d.getY());
	}

	Polygon polygon = new Polygon(xTmp, yTmp, nn);
	Graphics2D g2 = (Graphics2D)g;
	Color saveColor = g2.getColor();
	Vector3D normal = face.getNormal();
	Color color = colorModel.computeColor(getAmbiente(),
					      face.getAppearance(),
					      face.getProperties(),
					      normal,
					      camera.getCamera());
	g2.setColor(color);
	g2.fill(polygon);
	g2.setColor(saveColor);
    }

    /** Returns true if object will be painted. */
    public boolean isPaintable(Drawable p) {
	return true;
    }
    
}
