package rnadesign.designapp.rnagui;

import java.awt.Color;
import java.util.Properties;

import tools3d.Ambiente;
import tools3d.Appearance;
import tools3d.Camera;
import tools3d.GeometryColorModel;
import tools3d.Vector3D;
import java.util.TreeMap;

public class NucleotideColorModel implements GeometryColorModel {

  
    

    public static final Color DEFAULT_COLOR = Color.green;

    public static final Color A_COLOR = Color.blue;
    public static final Color T_COLOR = Color.gray;
    public static final Color U_COLOR = Color.gray;
    public static final Color C_COLOR = Color.orange;
    public static final Color G_COLOR = Color.red;
    public static final Color X_COLOR = Color.green.darker();


    public Color computeColor(Ambiente ambiente,
			      Appearance appearance,
			      Properties properties,
			      Vector3D faceNormal,
			      Camera camera) {
	if (properties == null)
	    return DEFAULT_COLOR;



	String className = properties.getProperty("class_name");
	String parentClassName = properties.getProperty("parent_class_name");
	String parentId = properties.getProperty("parent_id");
	
	if(className == null || parentId == null || parentClassName == null)
	    return DEFAULT_COLOR;

	if(className.equals("Nucleotide3D") || parentClassName.equals("Nucleotide3D")) {
	    Integer index = Integer.parseInt(parentId);
	    String base = properties.getProperty("base");

	    if(base == null)
		return DEFAULT_COLOR;
	    
	    Color ret = null;
	    if(base.equals("A"))
		ret = A_COLOR;
	    else if(base.equals("T"))
		ret = T_COLOR;
	    else if(base.equals("U"))
		ret = U_COLOR;
	    else if(base.equals("C"))
		ret = C_COLOR;
	    else if(base.equals("G"))
		ret = G_COLOR;
	    else
		ret = X_COLOR;

	   

	    return ret;
	}

	return DEFAULT_COLOR;
    }
    
    
}
