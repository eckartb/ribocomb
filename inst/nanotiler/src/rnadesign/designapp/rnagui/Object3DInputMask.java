/**
 * 
 */
package rnadesign.designapp.rnagui;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import tools3d.objects3d.Object3D;

/**
 * @author bindewae
 *
 */
public class Object3DInputMask extends JDialog implements ActionListener,
			PropertyChangeListener  {

    public static final String NEWLINE = System.getProperty("line.separator");

	Object3D obj;
	RnaGuiParameters params;
	private JTextField textField;
    private String typedText = null;
    private JOptionPane optionPane;
    private String btnString1 = "Enter";
    private String btnString2 = "Cancel";
	private String magicWord = "Geisel";

    public static Logger log = Logger.getLogger("NanoTiler_debug");
	
	Object3DInputMask(JFrame root, Object3D o, RnaGuiParameters prm) {
		 super(root, true); // 
		   // new BorderLayout());
		 log.fine("Starting constructor of Object3DInputMask!");
		obj = o;
		params = prm;
		setBackground(params.colorBackground);
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		textField = new JTextField(40);
		add(textField);
		
		JButton button;
		JPanel bottomRow = new JPanel();
		bottomRow.setLayout(new FlowLayout());
		button = new JButton("Cancel");
		bottomRow.add(button);
		button = new JButton("Close");
		bottomRow.add(button);
		add(bottomRow);
		 //Create an array of the text and components to be displayed.
        String msgString1 = "What was Dr. SEUSS's real last name?";
        String msgString2 = "(The answer is \"" + magicWord
                              + "\".)";
        Object[] array = {msgString1, msgString2, textField};
        Object[] options = {btnString1, btnString2};
		  //Create the JOptionPane.
        optionPane = new JOptionPane(array,
                                    JOptionPane.QUESTION_MESSAGE,
                                    JOptionPane.YES_NO_OPTION,
                                    null,
                                    options,
                                    options[0]);
		
//		Handle window closing correctly.
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent we) {
                /*
                 * Instead of directly closing the window,
                 * we're going to change the JOptionPane's
                 * value property.
                 */
                    optionPane.setValue(new Integer(
                                        JOptionPane.CLOSED_OPTION));
            }
        });
        log.fine("Ending constructor of Object3DInputMask!");
	}
	
	 /** This method clears the dialog and hides it. */
    public void clearAndHide() {
        textField.setText(null);
        setVisible(false);
    }

	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		optionPane.setValue(btnString1);
		
	}

	public void propertyChange(PropertyChangeEvent e) {
		// TODO Auto-generated method stub
	     String prop = e.getPropertyName();

	        if (isVisible()
	         && (e.getSource() == optionPane)
	         && (JOptionPane.VALUE_PROPERTY.equals(prop) ||
	             JOptionPane.INPUT_VALUE_PROPERTY.equals(prop))) {
	            Object value = optionPane.getValue();

	            if (value == JOptionPane.UNINITIALIZED_VALUE) {
	                //ignore reset
	                return;
	            }

	            //Reset the JOptionPane's value.
	            //If you don't do this, then if the user
	            //presses the same button next time, no
	            //property change event will be fired.
	            optionPane.setValue(
	                    JOptionPane.UNINITIALIZED_VALUE);

	            if (btnString1.equals(value)) {
	                    typedText = textField.getText();
	                String ucText = typedText.toUpperCase();
	                if (magicWord.equals(ucText)) {
	                    //we're done; clear and dismiss the dialog
	                    clearAndHide();
	                } else {
	                    //text was invalid
	                    textField.selectAll();
	                    JOptionPane.showMessageDialog(
	                                    Object3DInputMask.this,
	                                    "Sorry, \"" + typedText + "\" "
	                                    + "isn't a valid response." + NEWLINE
	                                    + "Please enter "
	                                    + magicWord + ".",
	                                    "Try again",
	                                    JOptionPane.ERROR_MESSAGE);
	                    typedText = null;
	                    textField.requestFocusInWindow();
	                }
	            } else { //user closed dialog or clicked cancel
//	                dd.setLabel("It's OK.  "
//	                         + "We won't force you to type "
//	                         + magicWord + ".");
	                typedText = null;
	                clearAndHide();
	            }
	        }
	}
	
}
