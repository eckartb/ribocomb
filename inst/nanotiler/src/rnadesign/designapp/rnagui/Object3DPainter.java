package rnadesign.designapp.rnagui;

import java.awt.Graphics;

import rnadesign.rnacontrol.CameraController;
import rnadesign.rnacontrol.Object3DGraphController;

public interface Object3DPainter {

    /** adds a forbidden class name */
    public void addForbidden(String className);

    /** removes all forbidden class names */
    public void clearForbidden();
    
    public void copy(Object3DPainter other);

    public void paint(Graphics g, Object3DGraphController control);

    public CameraController getCameraController();

    /** removes a forbidden class name */
    public void removeForbidden(String className);

    public void setCameraController(CameraController camera);

}
