package rnadesign.designapp.rnagui;

import java.awt.Graphics;

import rnadesign.rnacontrol.CameraController;
import tools3d.objects3d.Object3D;

/** paints a single object3d (withouth child nodes) */
public interface Object3DSinglePainter {
    
    public void paint(Graphics g, Object3D obj);

    public CameraController getCameraController();

    public void setCameraController(CameraController camera);

}
