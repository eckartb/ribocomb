package rnadesign.designapp.rnagui;

import tools3d.objects3d.Object3D;

/** decides on a painter for a given Object3D */
public interface Object3DSinglePainterChooser {
    
    public Object3DSinglePainter choosePainter(Object3D obj);

}
