package rnadesign.designapp.rnagui;

import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import tools3d.objects3d.Object3D;

public class Object3DTree extends JTree {

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    Object3DTreeModel model; //model is not initialized!
    public Object3DTree(Object3D graphNode) {
        super(new Object3DTreeModel(graphNode));
	
	//get a tree that allows one selection at a time
        getSelectionModel().setSelectionMode(
			     TreeSelectionModel.SINGLE_TREE_SELECTION);

	// Listen for when the selection changes
// 	model.addTreeSelectionListener(new TreeSelectionListener x);
	
        DefaultTreeCellRenderer renderer = new DefaultTreeCellRenderer();
        Icon personIcon = null;
        renderer.setLeafIcon(personIcon);
        renderer.setClosedIcon(personIcon);
        renderer.setOpenIcon(personIcon);
        setCellRenderer(renderer);
    }

    /**
     * Get the selected item in the tree, and call showAncestor with this
     * item on the model.
     */
    public void showAncestor(boolean b) {
        Object newRoot = null;
        TreePath path = getSelectionModel().getSelectionPath();
        if (path != null) {
            newRoot = path.getLastPathComponent();
        }
        ((Object3DTreeModel)getModel()).showAncestor(b, newRoot);
    }

    /** returns tree model */
    Object3DTreeModel getTreeModel() { 
	log.fine("model is " + model);
	return model; 
	//CV - Model is null. Where was model supposed to be initialized?
}

}
