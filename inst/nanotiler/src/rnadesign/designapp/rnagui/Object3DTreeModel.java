package rnadesign.designapp.rnagui;

import java.util.Vector;
import java.util.logging.Logger;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import tools3d.objects3d.Object3D;

public class Object3DTreeModel implements TreeModel {

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    private boolean showAncestors;
    private Vector<TreeModelListener> treeModelListeners = new Vector<TreeModelListener>();
    private Object3D rootObject;

    public Object3DTreeModel(Object3D root) {
        showAncestors = false;
        rootObject = root;
    }

    /**
     * Used to toggle between show ancestors/show descendant and
     * to change the root of the tree.
     */
    public void showAncestor(boolean b, Object newRoot) {
        showAncestors = b;
        Object3D oldRoot = rootObject;
        if (newRoot != null) {
	    rootObject = (Object3D)newRoot;
        }
        fireTreeStructureChanged(oldRoot);
    }


    //////////////// Fire events //////////////////////////////////////////////

    /**
     * The only event raised by this model is TreeStructureChanged with the
     * root as path, i.e. the whole tree has changed.
     */
    protected void fireTreeStructureChanged(Object3D oldRoot) {
	log.fine("Calling fireTreeStructureChanged");
        int len = treeModelListeners.size();
        TreeModelEvent e = new TreeModelEvent(this, 
                                              new Object[] {oldRoot});
        for (int i = 0; i < len; i++) {
            ((TreeModelListener)treeModelListeners.elementAt(i)).
		treeStructureChanged(e);
        }
    }


    //////////////// TreeModel interface implementation ///////////////////////

    /**
     * Adds a listener for the TreeModelEvent posted after the tree changes.
     */
    public void addTreeModelListener(TreeModelListener l) {
        treeModelListeners.addElement(l);
    }

    /**
     * Returns the child of parent at index index in the parent's child array.
     */
    public Object getChild(Object parent, int index) {
        Object3D p = (Object3D)parent;
	//         if (showAncestors) {
	//             if ((index > 0) && (p.get() != null)) {
	//                 return p.getMother();
	//             }
	//             return p.getFather();
	//         }
        return p.getChild(index);
    }

    /**
     * Returns the number of children of parent.
     */
    public int getChildCount(Object parent) {
        Object3D p = (Object3D)parent;
//         if (showAncestors) {
//             int count = 0;
//             if (p.getFather() != null) { 
//                 count++;
//             }
//             if (p.getMother() != null) { 
//                 count++;
//             }
//             return count;
//         }
        return p.size();
    }

    /**
     * Returns the index of child in parent.
     */
    public int getIndexOfChild(Object parent, Object child) {
        Object3D p = (Object3D)parent;
//         if (showAncestors) {
//             int count = 0;
//             Person father = p.getFather();
//             if (father != null) {
//                 count++;
//                 if (father == child) {
//                     return 0;
//                 }
//             }
//             if (p.getMother() != child) {
//                 return count;
//             }
//             return -1;
//         }
        return p.getIndexOfChild((Object3D)child);
    }

    /**
     * Returns the root of the tree.
     */
    public Object getRoot() {
        return rootObject;
    }

    /**
     * Returns true if node is a leaf.
     */
    public boolean isLeaf(Object node) {
        Object3D p = (Object3D)node;
//         if (showAncestors) {
//             return ((p.getFather() == null)
// 		    && (p.getMother() == null));
//         }
        return p.size() == 0;
    }

    /**
     * Removes a listener previously added with addTreeModelListener().
     */
    public void removeTreeModelListener(TreeModelListener l) {
        treeModelListeners.removeElement(l);
    }

    /**
     * Messaged when the user has altered the value for the item
     * identified by path to newValue.  Not used by this model.
     */
    public void valueForPathChanged(TreePath path, Object newValue) {
        log.info("*** valueForPathChanged : "
                           + path + " --> " + newValue);
    }
}
