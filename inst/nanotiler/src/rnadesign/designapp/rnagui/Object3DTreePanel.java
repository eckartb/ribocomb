package rnadesign.designapp.rnagui;

/*
 * class provides tree view of object3d tree
 */
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreeSelectionModel;

import controltools.ModelChangeEvent;
import controltools.ModelChangeListener;
import rnadesign.rnacontrol.Object3DController;
import tools3d.objects3d.Object3D;

public class Object3DTreePanel extends JPanel implements ModelChangeListener {

    public static final String NEWLINE = System.getProperty("line.separator");

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    List<TreeSelectionListener> selectionListeners = new ArrayList<TreeSelectionListener>();
    Object3DController graphController;
    Object3DTree tree;
    JScrollPane scrollPane;
    int selectionMode = TreeSelectionModel.SINGLE_TREE_SELECTION;
//     private Appearance selectionRootPoint = new Appearance(Color.CYAN);
    
    public Object3DTreePanel(Object3DController graphController,
			     int dimX, int dimY,
			     boolean multiSelectionMode) {
        super(new BorderLayout());
        this.graphController = graphController;
	if (multiSelectionMode) {
	    this.selectionMode = TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION;
	}
	else {
	    this.selectionMode = TreeSelectionModel.SINGLE_TREE_SELECTION;
	}
	log.fine("Constructing new Object3DTreePanel!");
	Runtime rt = Runtime.getRuntime();
        long free = rt.freeMemory(), total = rt.totalMemory(), used =  total - free;
        long max = rt.maxMemory();
	log.info("Free memory: " + free + NEWLINE + "Total Memory: " + total + NEWLINE + "Used: " + used + NEWLINE + "Max: " + max);

        //Construct the panel with the toggle buttons.
        JPanel buttonPanel = new JPanel();

        //Construct the tree.
        tree = constructNewTree(this.graphController.getGraph());

        scrollPane = new JScrollPane(tree);
        scrollPane.setPreferredSize(new Dimension(dimX, dimY));

        //Add everything to this panel.
        add(buttonPanel, BorderLayout.NORTH);
        add(scrollPane, BorderLayout.CENTER);
	// get notified when object3d tree changed:
	log.fine("Adding Object3DTreePanel to modelChangeListener of GraphController!");
	// this.graphController.addModelChangeListener(new LocalModelChangeListener());
	this.graphController.addModelChangeListener(this);
    }

    // change object3d tree to reflect current selection
    private void synchronizeSelection() {
	Object3D node = (Object3D)tree.getLastSelectedPathComponent();	
	if (node == null) {
	    return;
	}
	graphController.deselectAll();
	graphController.setSelectionCursor(node);
	graphController.selectCurrent();
    }

    public Object3DTreePanel(Object3DController graphController, int dimX, int dimY) {
	this(graphController, dimX, dimY, false);
    }

    public Object3DTreePanel(Object3DController graphController) {
	this(graphController, 200, 200);
    }

//     private class LocalModelChangeListener implements ModelChangeListener {
// 	public void modelChanged(ModelChangeEvent event) {
// 	    repaint();
// 	}
//     }

    private class Object3DTreeSelectionListener implements TreeSelectionListener {
	public void valueChanged(TreeSelectionEvent e) {
	    Object3D node = (Object3D)tree.getLastSelectedPathComponent();
	    if (node == null) {
		return;
	    }
	    else {
		log.fine("node is not null!");
		graphController.setSelectionCursor(node);
		graphController.selectCurrent();
// 		node = node.getAppearance(selectionRootPoint);
		// graphController.setSelectionCursor(node);
		fireTreeSelectionChanged(e); // notify all registered listeners
	    }
	}
    }

    public void addTreeSelectionListener(TreeSelectionListener listener) {
	this.selectionListeners.add(listener);
    }

    public void fireTreeSelectionChanged(TreeSelectionEvent event) {
	for (int i  = 0; i < selectionListeners.size(); ++i) {
	    TreeSelectionListener listener = (TreeSelectionListener)(selectionListeners.get(i));
	    listener.valueChanged(event);
	}
    }

    public Object3D getLastSelected() {
	return (Object3D)tree.getLastSelectedPathComponent();
    }

    public void modelChanged(ModelChangeEvent e) {
	log.fine("Called Object3DTreePanel.modelChanged!");
        Object3D newRoot = graphController.getGraph();
	Object3D oldRoot = null;
	if (tree.getTreeModel() != null) {
	    log.fine("getTreeModel is not null");
	    oldRoot = (Object3D)(tree.getTreeModel().getRoot());
	    // construct new tree object if root changed:
	    if (newRoot != oldRoot) {
		//Constructing the tree after any change:
		log.fine("Called Object3DTreePanel.modelChanged: root has changed!?");
		tree = constructNewTree(graphController.getGraph());
		remove(scrollPane);
		scrollPane = new JScrollPane(tree);
		add(scrollPane, BorderLayout.CENTER);
		repaint();
	    }
	}
	else {
	    log.fine("Called Object3DTreePanel.modelChanged: root has not changed");
	    tree = constructNewTree(graphController.getGraph());
	    remove(scrollPane);
	    scrollPane = new JScrollPane(tree);
	    add(scrollPane,BorderLayout.CENTER);
	    repaint();
	}
	//CV - if you get some kind of error it could be because I moved the repaint statement into the if statement. I think this is right, but if something goes wrong, change it back. :)
    }

    private Object3DTree constructNewTree(Object3D object3D) {
	Object3DTree newTree = new Object3DTree(object3D);
	newTree.getSelectionModel().setSelectionMode(selectionMode);
	// Listen for when the selection changes.
	newTree.addTreeSelectionListener(new Object3DTreeSelectionListener());
	return newTree;
    }

}
