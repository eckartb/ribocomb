package rnadesign.designapp.rnagui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

import commandtools.CommandApplication;
import commandtools.CommandException;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import rnadesign.designapp.AbstractDesigner;
import rnadesign.rnacontrol.Object3DController;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnamodel.RnaTools;
import tools3d.objects3d.Object3D;
import java.util.logging.Logger;

/**
 * GUI Implementation of optbasepairs command
 *
 * @author Brett Boyle
 */
public class OptbasepairsWizard implements GeneralWizard {

    private static final int COL_SIZE_NAME = 30; //TODO: size?
    private static final String FRAME_TITLE = "Optimize base pairs Wizard";

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    private boolean finished = false;
    private JFrame frame = null;
    private List<ActionListener> actionListeners = new ArrayList<ActionListener>();
    private CommandApplication application;
    private Object3DGraphController graphController;
    private JTextArea blocksField;
    private JTextField errorField;
    private JTextField stepsField;
    private Vector<String> tree;
    private String[] allowedNames = RnaTools.getRnaClassNames();
    private String[] forbiddenNames = new String[]{"Atom3D","Nucleotide3D"};
    private JPanel treePanel;
    private JButton treeButton;
    private JList treeList;
    private JLabel label;
    private JScrollPane treeScroll;

    /**
     * Constructor.
     *
     * @param application NanoTiler
     */ 
    public OptbasepairsWizard(CommandApplication application) {
	assert application != null;
	this.application = application;
    }

    /**
     * Called when "Cancel" button is pressed.
     * Window disappears.
     *
     */
    private class CancelListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    frame.setVisible(false);
	    frame = null;
	    finished = true;
	}
    }

    private class OptbasepairsListener implements ActionListener{
	public void actionPerformed(ActionEvent e){
	    String blocks = blocksField.getText();
	    if(blocks.length()!=0 && blocks.charAt(blocks.length()-1)==';'){
		blocks = blocks.substring(0,blocks.length()-1);
	    }
	    String command = "optbasepairs blocks="+blocks+" error="+errorField.getText().trim()+" steps="+stepsField.getText().trim();
	    System.out.println("Command: "+command);
	    try{
		application.runScriptLine(command);
	    }
	    catch(CommandException cex){
		JOptionPane.showMessageDialog(frame, cex + ": " + cex.getMessage());
	    }
	}
    }

    /**
     * Adds an action listener.
     *
     * @param listener The ActionListener to add.
     */
    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    /** Adds the components to the window. */
    public void addComponents() {
	Container f = frame.getContentPane();
	f.setLayout(new BorderLayout());

	JPanel instructionsPanel = new JPanel();
	instructionsPanel.setLayout(new BoxLayout(instructionsPanel,BoxLayout.Y_AXIS));
	label = new JLabel("  Select the blocks that you would like to include in the optimization");
	instructionsPanel.add(label);

	JPanel labelPanel = new JPanel();
	label = new JLabel("Blocks: ");
        labelPanel.add(label);

	JPanel fieldPanel = new JPanel();
	fieldPanel.setLayout(new BoxLayout(fieldPanel, BoxLayout.Y_AXIS));
	blocksField =  new JTextArea(20,20);
	blocksField.setLineWrap(true);
	JScrollPane resultsScroller = new JScrollPane(blocksField);
	resultsScroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
	fieldPanel.add(resultsScroller);

	JPanel treePanel = new TreePanel(blocksField);

	JPanel buttonPanel = new JPanel(new FlowLayout());
	label = new JLabel("Error:");
	buttonPanel.add(label);
	errorField = new JTextField(4);
	errorField.setText("0");
	buttonPanel.add(errorField);
	label = new JLabel("Steps:");
	buttonPanel.add(label);
	stepsField = new JTextField(6);
	stepsField.setText("100");
	buttonPanel.add(stepsField);
	JButton button = new JButton("Close");
	button.addActionListener(new CancelListener());
	buttonPanel.add(button);
	button = new JButton("Optimize Base Pairs");
	button.addActionListener(new OptbasepairsListener());
	buttonPanel.add(button);

	f.add(instructionsPanel, BorderLayout.NORTH);
	f.add(labelPanel, BorderLayout.WEST);
	f.add(fieldPanel, BorderLayout.CENTER);
	f.add(treePanel, BorderLayout.EAST);
	f.add(buttonPanel, BorderLayout.SOUTH);

    }

    /** Returns a String representation of the current selection. */
    private String getSelectionText() {
	Object3DGraphController graphController = ((AbstractDesigner)application).getGraphController();
	Object3DController gControl = graphController.getGraph();
	Object3D selection = gControl.getSelectionRoot();
	if (selection == null) {
	    return "";
	}
	String selectionName = selection.getName();
	Object3D curr = selection;
	while (curr.getParent() != null) {
	    curr = curr.getParent();
	    selectionName = curr.getName() + "." + selectionName;
	}
	return selectionName;
    }

    public boolean isFinished() {
	return finished;
    }

    /** Creates a window. */
    public void launchWizard(Object3DGraphController controller,Component parentFrame) {
	frame = new JFrame(FRAME_TITLE);
	if(controller==null){
	    log.info("GraphController received by lauchWizard is null!");
	}
	this.graphController = controller;
	addComponents();
	frame.pack();
	frame.setResizable(false);
	frame.setVisible(true);
    }
    /**
     * Called when a new value is selected in the list.
     * Allows user to pick an object from a list that
     * he/she wants selected.
     */
    public class SelectionListener implements ListSelectionListener {
	JTextArea field;
	public SelectionListener(JTextArea field){
	    this.field = field;
	}

	public void valueChanged(ListSelectionEvent e) {
	    if (e.getValueIsAdjusting() == true) {
		String text = tree.get(e.getFirstIndex());
		text = text.substring(0, text.indexOf(" "));
		if (text.equals(field.getText())) {
		    text = tree.get(e.getLastIndex());
		    text = text.substring(0, text.indexOf(" "));
		}
		field.append(text+";");
	    }
	}
    }
    /*
     * generates a panel containing the current tree in
     * list format.
     */
    private class TreePanel extends JPanel{
	public TreePanel(JTextArea field){
		Object3DGraphController controller = ((AbstractDesigner)application).getGraphController();
		tree = controller.getGraph().getTree(allowedNames, forbiddenNames);
		this.setLayout(new BorderLayout());
		JPanel labelPanel = new JPanel();
		labelPanel.setLayout(new BoxLayout(labelPanel, BoxLayout.Y_AXIS));
		labelPanel.add(new JLabel("Tree:"));
		this.add(labelPanel);
	        
		treePanel = new JPanel();
		treeList = new JList(tree);
		treeList.addListSelectionListener(new SelectionListener(field));
		treeScroll = new JScrollPane(treeList);
		treeScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		treeScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		treeScroll.setPreferredSize(new Dimension(300,300));
		treePanel.add(treeScroll);
		this.add(treePanel, BorderLayout.SOUTH);
	}
    }

}
