package rnadesign.designapp.rnagui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.BorderFactory;


import java.util.StringTokenizer;
import java.util.Vector;

import commandtools.CommandApplication;
import commandtools.Command;
import commandtools.AbstractCommand;
import commandtools.UnknownCommandException;
import commandtools.BadSyntaxException;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerConstants;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import rnadesign.rnacontrol.SequenceController;
import tools3d.Vector3D;
import rnadesign.designapp.NanoTilerInterpreter;
import rnadesign.rnamodel.RnaStrand;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DTools;
import rnadesign.rnamodel.Residue3D;
import rnasecondary.SecondaryStructure;
import rnasecondary.InteractionSet;
import rnasecondary.Interaction;
import sequence.Sequence;
import sequence.DuplicateNameException;
import commandtools.CommandException;
import rnasecondary.SecondaryStructureScriptFormatWriter;

import java.io.PrintStream;
import java.io.OutputStream;

import static rnadesign.designapp.PackageConstants.NEWLINE;

public class OptimizationWizard implements GeneralWizard {
    private static final int COL_SIZE = 15; 
    private boolean finished = false;

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    private JFrame frame = null;
    private JFrame keyFrame = null;

    private CommandApplication application;
    private NanoTilerInterpreter interpreter;
    private List<ActionListener> actionListeners = new ArrayList<ActionListener>();
    private Object3DGraphController graphController;
    private String[] commandNames;

    public static final String FRAME_TITLE = "Optimization Wizard";

    public OptimizationWizard(CommandApplication application){
	assert application != null;
	this.application = application;
	this.interpreter = (NanoTilerInterpreter)application.getInterpreter();
    }

    private class DoneListener implements ActionListener {

	public void actionPerformed(ActionEvent e) {
		frame.setVisible(false);
		frame = null;
		finished = true;
	}
    }

    private class LaunchKeyListener implements ActionListener {
	public void actionPerformed(ActionEvent e){
	    keyFrame = new JFrame("Sequence Optimization Interface Key");
	    keyFrame.setLayout(new BorderLayout());
	    JTextArea textArea = new JTextArea(10,10);
	    textArea.append("Structure: . . . ( . . ( . ) . . ) . . .\n  ( ) = Paired Bases\n . . = Unpaired Bases\n\n Sequence:  GCGGGAUAC \n");
	    textArea.setEditable(false);
	    keyFrame.add(textArea, BorderLayout.NORTH);
	    JButton closeButton = new JButton("Close");
	    closeButton.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			keyFrame.setVisible(false);
			keyFrame = null;
		    }
	    });
	    keyFrame.add(closeButton, BorderLayout.SOUTH);
	    keyFrame.pack();
	    keyFrame.setVisible(true);
	}
    }

    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    public boolean isFinished() {
	return finished;
    }

    /** generates and inserts new Object3D according to user dialog */
    public void launchWizard(Object3DGraphController graphController,
			     Component parentFrame) {
	assert graphController != null;
	if (graphController == null) {
	    log.info("GraphController received by launchWizard is null!");
	}
	this.graphController = graphController;
	frame = new JFrame(FRAME_TITLE);
	addComponents(frame);
	frame.pack();
	frame.setVisible(true);
    }

    public void addComponents(JFrame _frame) {
	Container f = _frame.getContentPane();
	f.setLayout(new BoxLayout(f,BoxLayout.Y_AXIS));


	//Setup the strand information here
	String[] allowedNames = new String[]{"RnaStrand"};
	String[] forbiddenNames = new String[]{"StrandJunction3D"};
	Vector<String> strandTree = graphController.getGraph().getTree(allowedNames,forbiddenNames);
	int strandCount = strandTree.size();
	Object3D root = graphController.getGraph().getGraph();

	SecondaryStructure secStruc = null;
	SecondaryStructureScriptFormatWriter secStrucWriter = new SecondaryStructureScriptFormatWriter();

	try {
	    secStruc = graphController.generateSecondaryStructure();
	}
	catch (DuplicateNameException dne) {
	    log.warning("Duplicate sequence names detected! Cannot generate secondary structure.");
	}

	List<String[]> secondaryStructureLists = secStrucWriter.writeSecondaryStructure(secStruc);
	String[] strandNames = new String[strandCount];
	ArrayList<RnaStrand> strandList = new ArrayList<RnaStrand>();
	ArrayList<Residue3D[]> residueLists = new ArrayList<Residue3D[]>();
	SequenceController seqController = graphController.getSequences();
	ArrayList<Sequence> sequenceList = new ArrayList<Sequence>();
	ArrayList<String[]> sequenceResidueLists = new ArrayList<String[]>();

	for(int i=0;i<strandCount;i++){
	    StringTokenizer strandTokenizer = new StringTokenizer(strandTree.get(i));
	    RnaStrand tempStrand = (RnaStrand)Object3DTools.findByFullName(root,strandTokenizer.nextToken());
	    strandList.add(tempStrand);
	    strandNames[i] = tempStrand.getName();
	    sequenceList.add(seqController.getSequence(strandNames[i]));
	    String currSequence = sequenceList.get(i).sequenceString();
	    String[] currSequenceElements = new String[currSequence.length()];
	    Residue3D[] currStrandResidues = new Residue3D[currSequence.length()];
	    for(int j=0;j<currSequence.length();j++){
		 currSequenceElements[j] = currSequence.substring(j,j+1);
		 currStrandResidues[j] = tempStrand.getResidue3D(j);
	    }
	    sequenceResidueLists.add(currSequenceElements);
	    residueLists.add(currStrandResidues);
	}


	//setup graphics(strand panels) layout

	ArrayList<JPanel> strandPanels = new ArrayList<JPanel>();
	ArrayList<JCheckBox[]> sequenceBoxes = new ArrayList<JCheckBox[]>();

	for(int j=0;j<strandCount;j++){
	    JPanel tempPanel = new JPanel();
	    tempPanel.setLayout(new BorderLayout());
	    tempPanel.setBorder(BorderFactory.createTitledBorder(strandNames[j]));

	    Residue3D[] tempResidueList = residueLists.get(j);
	    String[] tempSequence = sequenceResidueLists.get(j);
	    String[] tempStructure = secondaryStructureLists.get(j);
	    JCheckBox[] boxes = new JCheckBox[tempSequence.length];

	    JPanel innerTempPanel = new JPanel();
	    innerTempPanel.setLayout(new GridLayout(4,tempSequence.length,2,0));
	    innerTempPanel.setMinimumSize(new Dimension(50,50));

	    for(int s=0;s<boxes.length;s++){
		JLabel label = new JLabel(" "+tempStructure[s]);
		innerTempPanel.add(label);
	    }

	    for(int g=0;g<boxes.length;g++){
		JLabel label = new JLabel(" "+tempSequence[g]);
		innerTempPanel.add(label);
	    }

	    for(int l=0;l<boxes.length;l++){
		JCheckBox tempBox = new JCheckBox();
		tempBox.setPreferredSize(new Dimension(10,10));

		Residue3D tempResidue = tempResidueList[l];
		String residueStatus = tempResidue.getProperty("sequence_status");
		if(residueStatus!=null){
		    if(residueStatus.equals("adhoc") || residueStatus.equals("optimized")){
			tempBox.setSelected(true);
		    }
		}

		boxes[l] = tempBox;
		innerTempPanel.add(tempBox);
	    }
	    sequenceBoxes.add(boxes);
	    for(int u=0;u<boxes.length;u++){
		JLabel label = new JLabel(Integer.toString(u+1));
		innerTempPanel.add(label);
	    }

	    JPanel tempButtonPanel = new JPanel();
	    JButton selectAll = new JButton("Select all");
	    final int panelNumber = j;
	    final JCheckBox[] currBoxArray = sequenceBoxes.get(panelNumber);
	    selectAll.addActionListener(new ActionListener(){
		    JCheckBox[] boxArray = currBoxArray;

		    public void actionPerformed(ActionEvent e){
			for(int f=0;f<currBoxArray.length;f++){
			    currBoxArray[f].setSelected(true);
			}
		    }
	    });
	    tempButtonPanel.add(selectAll);

	    JButton deselectAll = new JButton("Deselect all");
            deselectAll.addActionListener(new ActionListener(){
		    JCheckBox[] boxArray = currBoxArray;

		    public void actionPerformed(ActionEvent e){
			for(int f=0;f<currBoxArray.length;f++){
			    currBoxArray[f].setSelected(false);
			}
		    }
	    });
	    tempButtonPanel.add(deselectAll);

	    final JTextField startParamField = new JTextField(3);
	    startParamField.setText("0");
	    final JTextField endParamField = new JTextField(3);
	    endParamField.setText("0");
	    JButton selectButton = new JButton("Select");
            selectButton.addActionListener(new ActionListener(){
		    JCheckBox[] boxArray = currBoxArray;

		    public void actionPerformed(ActionEvent e){
			if(!startParamField.getText().equals("") && !endParamField.getText().equals("")){
			    for(int f=Integer.parseInt(startParamField.getText());f<=Integer.parseInt(endParamField.getText())&& f<=currBoxArray.length;f++){
				
				if(f>0){
				    currBoxArray[f-1].setSelected(true);
				}
			    }
			}
		    }
	    });
	    JButton deselectButton = new JButton("Deselect");
            deselectButton.addActionListener(new ActionListener(){
		    JCheckBox[] boxArray = currBoxArray;

		    public void actionPerformed(ActionEvent e){
			if(!startParamField.getText().equals("") && !endParamField.getText().equals("")){
			    for(int f=Integer.parseInt(startParamField.getText());f<=Integer.parseInt(endParamField.getText()) && f<=currBoxArray.length;f++){
				if(f>0){
				    currBoxArray[f-1].setSelected(false);
				}
			    }
			}
		    }
	    });

	    tempButtonPanel.add(selectButton);
	    tempButtonPanel.add(startParamField);
	    tempButtonPanel.add(new JLabel("-"));
	    tempButtonPanel.add(endParamField);
	    tempButtonPanel.add(deselectButton);

	    tempPanel.add(innerTempPanel,BorderLayout.CENTER);
	    tempPanel.add(tempButtonPanel,BorderLayout.SOUTH);

	    strandPanels.add(tempPanel);

	}
	//add components to the frame/container
	JPanel strandPaneContainer = new JPanel();
	strandPaneContainer.setLayout(new BoxLayout(strandPaneContainer,BoxLayout.Y_AXIS));
	for(int u=0;u<strandPanels.size();u++){
	    JScrollPane tempStrandPane = new JScrollPane(strandPanels.get(u),JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	    tempStrandPane.setPreferredSize(new Dimension(400,140));

	    strandPaneContainer.add(tempStrandPane);
	    //f.add(tempStrandPane);
	}
	JScrollPane strandPaneContainerScroll = new JScrollPane(strandPaneContainer,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

	strandPaneContainerScroll.setPreferredSize(new Dimension(400,140));
	f.add(strandPaneContainerScroll);

	JPanel commandOptions = new JPanel();
	JLabel errorLabel = new JLabel("Error:");
	commandOptions.add(errorLabel);
	JTextField errorField = new JTextField(5);
	errorField.setText("0");
	commandOptions.add(errorField);
	JLabel iter1 = new JLabel("iter1:");
	commandOptions.add(iter1);
	JTextField iter1Field = new JTextField(7);
	iter1Field.setText("200");
	commandOptions.add(iter1Field);
	JLabel iter2 = new JLabel("iter2:");
	commandOptions.add(iter2);
	JTextField iter2Field = new JTextField(7);
	iter2Field.setText("200");
	commandOptions.add(iter2Field);

	//setup optimization button
	JButton runOptButton = new JButton("Run Optimization");
	if(strandCount==0){
	    runOptButton.setEnabled(false);
	}

	final ArrayList<JCheckBox[]> sequenceBoxes2 = sequenceBoxes;
	final ArrayList<String[]> sequenceResidueLists2 = sequenceResidueLists;
	final int strandCount2 = strandCount;
	final JFrame frame2 = _frame;
	final Container f2 = f;
	final String[] strandNames2 = strandNames;
	final JTextField iter1Field2 = iter1Field;
	final JTextField iter2Field2 = iter2Field;
	final JTextField errorField2 = errorField;

	final JButton runOptButton2 = runOptButton;
	if(strandCount2>0){
	    runOptButton.addActionListener(new ActionListener(){
		    String resCommand = "resproperty ";

		    public void actionPerformed(ActionEvent e){
			for(int i=0;i<strandCount2;i++){
			    System.out.println("Opt loop Started");
			    JCheckBox[] strandBoxes = sequenceBoxes2.get(i);
			    
			    int k=0;
			    boolean hasCheckedBox=false;
			    while(hasCheckedBox!=true && k<strandBoxes.length){
				if(strandBoxes[k].isSelected()){
				    hasCheckedBox = true;
				    resCommand += strandNames2[i]+":";
				}
				else{
				    k++;
				}
			    }
			    System.out.println(resCommand);

			    String[] residues = sequenceResidueLists2.get(i);
			    if(hasCheckedBox){
				for(int f=0;f<strandBoxes.length;f++){
				    if(strandBoxes[f].isSelected()){
					resCommand += Integer.toString(f+1)+",";
				    }
				}
				System.out.println(resCommand);
				resCommand = resCommand.substring(0,resCommand.length()-1);
				resCommand += ";";
				System.out.println(resCommand);
			    }
			}
			System.out.println(resCommand);
			resCommand = resCommand.substring(0,resCommand.length()-1);
			resCommand += " adhoc";
			System.out.println(resCommand);	
			if(!resCommand.equals("resproperty adhoc")){
			    try{
				application.runScriptLine("resproperty all fragment");
			    
				try{
				    application.runScriptLine(resCommand);
				    try{
					application.runScriptLine("optsequences error="+errorField2.getText().trim()+" iter="+iter1Field2.getText().trim()+" iter2="+iter2Field2.getText().trim());
				    }
				    catch(CommandException ce){
					JOptionPane.showMessageDialog(f2,ce+": "+ce.getMessage());
				    }
				}
				catch(CommandException cex){
				    JOptionPane.showMessageDialog(f2, cex+": "+cex.getMessage());
				}
			    }
			    catch(CommandException cex){
				JOptionPane.showMessageDialog(f2, cex+": "+cex.getMessage());
			    }
			}
			resCommand = "resproperty ";
			
			//frame.setVisible(false);
			//frame = null;
		    }

	    });
	    runOptButton.addActionListener(new DoneListener());
	}



	commandOptions.add(runOptButton);

	JButton closeButton = new JButton("Close");
	closeButton.addActionListener(new DoneListener());
	commandOptions.add(closeButton);
	JButton interfaceKeyButton = new JButton("Interface Key");
	interfaceKeyButton.addActionListener(new LaunchKeyListener());
	commandOptions.add(interfaceKeyButton);
	commandOptions.setMaximumSize(new Dimension(2000,50));

	f.add(commandOptions);

	
      
    }

}
