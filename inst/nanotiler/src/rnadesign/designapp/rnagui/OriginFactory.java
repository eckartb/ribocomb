package rnadesign.designapp.rnagui;

import tools3d.objects3d.Primitive3D;

public interface OriginFactory {

    public Primitive3D[] createOrigin();

}
