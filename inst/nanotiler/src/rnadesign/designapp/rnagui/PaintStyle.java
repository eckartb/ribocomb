package rnadesign.designapp.rnagui;

import java.awt.Color;

public class PaintStyle {

    Color color = Color.black;

    boolean solid = false;

    boolean wire = true;

    public Color getColor() { return color; }

    public boolean isWire() { return wire; }

    public boolean isSolid() { return solid; }

    

    public void setSolid(boolean b) { this.solid = b; }

    public void setWire(boolean b) { this.wire = b; }

}
