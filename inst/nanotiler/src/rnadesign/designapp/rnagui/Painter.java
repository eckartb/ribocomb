package rnadesign.designapp.rnagui;

import java.awt.Color;

import rnadesign.rnacontrol.CameraController;
import tools3d.Ambiente;

/** paints a single Shape3D 
 * this can be a base class to paint cubes, spheres, cylinders etc
 * @see Strategy pattern in Gamma et al: Design patterns
 */
public interface Painter {    

    public CameraController getCameraController();

    public Ambiente getAmbiente();

    public Color getDefaultPaintColor();

    public PaintStyle getPaintStyle();

    public void setAmbiente(Ambiente ambiente);

    public void setCameraController(CameraController camera);

    public void setDefaultPaintColor(Color color);

    /** info about color, solid and wire frame mode */
    public void setPaintStyle(PaintStyle style);

}
