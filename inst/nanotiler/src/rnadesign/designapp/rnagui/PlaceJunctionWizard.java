package rnadesign.designapp.rnagui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.event.*;
import javax.swing.*;
import javax.swing.text.*;
import java.text.ParseException;
import java.io.*;
import java.util.*;
import commandtools.*;
import controltools.*;
import rnadesign.rnamodel.*;

import tools3d.objects3d.*;

import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerConstants;

/** lets user choose to partner object, inserts correspnding link into controller */
public class PlaceJunctionWizard implements GeneralWizard {

    public static final String FRAME_TITLE = "Place Junction";

    private CommandApplication application;
    private static int version = 1;

    private final static Dimension PANEL_DIM = new Dimension(200, 200);

    private JList parentList;
    private JList junctionList;
    private JButton placeButton;
    private JButton closeButton;
    private JRadioButton kButton;
    private JRadioButton jButton;
    private JFormattedTextField xLocation, yLocation, zLocation;
    private JTextField nameField;
    private ButtonGroup group;

    private ArrayList<Object3D> objects;
    private ArrayList<JunctionInfo> junctions;

    private class JunctionInfo {
	StrandJunction3D junction;
	int order;
	int index;
    }


    private ArrayList<ActionListener> actionListeners = new ArrayList<ActionListener>();
    private boolean finished = false;
    private Object3DGraphController graphController;
    private JDialog frame;

    public PlaceJunctionWizard(CommandApplication application) {
	this.application = application;
    }

    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    public boolean isFinished() {
	return finished;
    }

    /** generates and inserts new Object3D according to user dialog */
    public void launchWizard(Object3DGraphController graphController,
			     Component parentFrame) {

	this.graphController = graphController;

	
	frame = new JDialog(parentFrame instanceof Frame ? (Frame) parentFrame : null, FRAME_TITLE, true);
	frame.addWindowListener(new WindowAdapter() {
		
		public void windowClosed(WindowEvent e) {
		    finished = true;
	       
		}

	    });

	JPanel panel = new JPanel();
	panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));

	JPanel typePanel = new JPanel();
	typePanel.setLayout(new BoxLayout(typePanel, BoxLayout.Y_AXIS));
	JLabel label = new JLabel("Type:");
	label.setAlignmentX(JComponent.CENTER_ALIGNMENT);
	typePanel.add(label);
	typePanel.add(Box.createVerticalStrut(10));
	typePanel.setPreferredSize(PANEL_DIM);

	jButton = new JRadioButton("Junction");
	kButton = new JRadioButton("Kissing Loop");
	group = new ButtonGroup();
	group.add(jButton);
	group.add(kButton);
	jButton.setAlignmentX(JComponent.CENTER_ALIGNMENT);
	kButton.setAlignmentX(JComponent.CENTER_ALIGNMENT);

	ActionListener listener = new PlaceTypeListener();
	jButton.addActionListener(listener);
	kButton.addActionListener(listener);

	typePanel.add(jButton);
	typePanel.add(kButton);

	panel.add(typePanel);
	panel.add(new JSeparator(SwingConstants.VERTICAL));

	JPanel parentListPanel = new JPanel();
	parentListPanel.setLayout(new BoxLayout(parentListPanel, BoxLayout.Y_AXIS));
	label = new JLabel("Parent:");
	label.setAlignmentX(JComponent.CENTER_ALIGNMENT);
	parentListPanel.add(label);
	parentListPanel.setPreferredSize(PANEL_DIM);

	parentListPanel.add(Box.createVerticalStrut(10));

	parentList = new JList();
	parentList.setAlignmentX(JComponent.CENTER_ALIGNMENT);
	parentList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	
	parentList.addListSelectionListener(new ParentSelectionListener());

	// query list of objects
	objects = new ArrayList<Object3D>();
	Object3DActionVisitor visitor = new Object3DActionVisitor(graphController.getGraph().getGraph(), new Object3DAction() {
		public void act(Object3D o) {
		    objects.add(o);
		}
	    });
	visitor.nextToEnd();

	parentList.setModel(new AbstractListModel() {

		public int getSize() {
		    return objects.size();
		}

		public Object getElementAt(int index) {
		    return Object3DTools.getFullName(objects.get(index));
		}

	    });

	parentListPanel.add(new JScrollPane(parentList));
	
	parentList.setEnabled(false);

	
	panel.add(parentListPanel);
	panel.add(new JSeparator(SwingConstants.VERTICAL));

	JPanel junctionListPanel = new JPanel();
	junctionListPanel.setLayout(new BoxLayout(junctionListPanel, BoxLayout.Y_AXIS));
	label = new JLabel("Junctions / Kissing Loops:");
	label.setAlignmentX(JComponent.CENTER_ALIGNMENT);
	junctionListPanel.add(label);
	junctionListPanel.setPreferredSize(PANEL_DIM);
	
	junctionListPanel.add(Box.createVerticalStrut(10));


	junctionList = new JList();
	junctionList.setAlignmentX(JComponent.CENTER_ALIGNMENT);
	junctionList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	junctionList.addListSelectionListener(new JunctionSelectionListener());

	junctionList.setEnabled(false);

	junctionListPanel.add(new JScrollPane(junctionList));

	panel.add(junctionListPanel);

	panel.add(new JSeparator(SwingConstants.VERTICAL));

	JPanel locationPanel = new JPanel();
	locationPanel.setLayout(new BoxLayout(locationPanel, BoxLayout.Y_AXIS));

	label = new JLabel("Location:");
	label.setAlignmentX(JComponent.CENTER_ALIGNMENT);
	locationPanel.add(label);
	locationPanel.setPreferredSize(PANEL_DIM);

	locationPanel.add(Box.createVerticalStrut(10));
	
	

	xLocation = new JFormattedTextField(new LocationFormatter());
	xLocation.setColumns(10);
	yLocation = new JFormattedTextField(new LocationFormatter());
	yLocation.setColumns(10);
	zLocation = new JFormattedTextField(new LocationFormatter());
	zLocation.setColumns(10);


	xLocation.setValue(new Double(0.0));
	yLocation.setValue(new Double(0.0));
	zLocation.setValue(new Double(0.0));

	xLocation.setEnabled(false);
	yLocation.setEnabled(false);
	zLocation.setEnabled(false);

	JPanel temp = new JPanel();
	//temp.setLayout(new BoxLayout(temp, BoxLayout.X_AXIS));
	temp.add(new JLabel("X:"));
	temp.add(xLocation);

	locationPanel.add(temp);

	temp = new JPanel();
	//temp.setLayout(new BoxLayout(temp, BoxLayout.X_AXIS));
	temp.add(new JLabel("Y:"));
	temp.add(yLocation);
	
	locationPanel.add(temp);
	
	temp = new JPanel();
	//temp.setLayout(new BoxLayout(temp, BoxLayout.X_AXIS));
	temp.add(new JLabel("Z:"));
	temp.add(zLocation);

	locationPanel.add(temp);


	temp = new JPanel();
	//temp.setLayout(new BoxLayout(temp, BoxLayout.X_AXIS));
	temp.add(new JLabel("Name:"));
	nameField = new JTextField(15);
	nameField.setEnabled(false);
	temp.add(nameField);
	

	locationPanel.add(temp);

	panel.add(locationPanel);

	closeButton = new JButton("Close");
	placeButton = new JButton("Place");

	closeButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    frame.setVisible(false);
		    frame.dispose();

		}

	    });

	JPanel buttonTemp = new JPanel();
	buttonTemp.add(closeButton);
	buttonTemp.add(placeButton);
	
	placeButton.setEnabled(false);
	placeButton.addActionListener(new PlaceButtonListener());

	
	frame.add(panel, BorderLayout.CENTER);
	frame.add(buttonTemp, BorderLayout.SOUTH);

	frame.pack();
	frame.setVisible(true);
	
    }

    private class LocationFormatter extends JFormattedTextField.AbstractFormatter {
	public Object stringToValue(String text) throws ParseException {
	    Double d = null;
	    try {
		d = Double.parseDouble(text);

	    } catch(NumberFormatException e) {
		throw new ParseException(e.getMessage(), 0);
	    }
	    
	    return d;

	}

	public String valueToString(Object value) throws ParseException {
	    if(!(value instanceof Number))
		throw new ParseException("Value not a number", 0);

	    return value.toString();

	}

	protected DocumentFilter getDocumentFilter() {

	    return new DocumentFilter() {
		    public void insertString(DocumentFilter.FilterBypass fb,
					     int offset,
					     String string,
					     AttributeSet attr) throws BadLocationException {


			try {

			    Double d = Double.parseDouble(string);
			    super.insertString(fb, offset, string, attr);
			} catch(NumberFormatException e) {

			}
			
		    }

		};

	}


    }

    private class PlaceTypeListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {

	    parentList.setEnabled(true);
	    parentList.clearSelection();
	    junctionList.setEnabled(false);
	    junctionList.clearSelection();
	    xLocation.setEnabled(false);
	    yLocation.setEnabled(false);
	    zLocation.setEnabled(false);
	    placeButton.setEnabled(false);
	    nameField.setEnabled(false);

	    StrandJunctionDB db = null;
	    if(e.getSource() == kButton)
		db = graphController.getJunctionController().getKissingLoopDB();
	    else if(e.getSource() == jButton)
		db = graphController.getJunctionController().getJunctionDB();

	    int numberJunctions = db.getJunctionCount();

	    if(numberJunctions == 0) {
		JOptionPane.showMessageDialog(frame, "No " + (kButton.isSelected() ? "Kissing Loops " : "Junctions ") + "available");
		parentList.setEnabled(false);
		return;

	    }

	    int junctionCount = 0;
	    int order = 1;

	    junctions = new ArrayList<JunctionInfo>();


	    while(junctionCount != numberJunctions) {
		StrandJunction3D[] j = db.getJunctions(order);
		junctionCount += j.length;
		for(int i = 0; i < j.length; ++i) {
		    JunctionInfo info = new JunctionInfo();
		    info.junction = j[i];
		    info.order = order;
		    info.index = i + 1;
		    junctions.add(info);
		}
		order++;
	    }

	    junctionList.setModel(new AbstractListModel() {
		    public int getSize() {
			return junctions.size();
		    }

		    public Object getElementAt(int index) {
			return Object3DTools.getFullName(junctions.get(index).junction);

		    }

		});
	}

    }

    private class ParentSelectionListener implements ListSelectionListener {
	public void valueChanged(ListSelectionEvent e) {
	    if(e.getValueIsAdjusting())
		return;

	    if(!junctionList.isEnabled())
		junctionList.setEnabled(true);
	}
    }

    private class JunctionSelectionListener implements ListSelectionListener {
	public void valueChanged(ListSelectionEvent e) {
	    xLocation.setEnabled(true);
	    yLocation.setEnabled(true);
	    zLocation.setEnabled(true);
	    placeButton.setEnabled(true);
	    nameField.setEnabled(true);

	}

    }

    private class PlaceButtonListener implements ActionListener {

	public void actionPerformed(ActionEvent e) {
	    if(nameField.getText().equals("")) {
	       JOptionPane.showMessageDialog(frame, "Must enter name");
	       return;
	    }
	       
	    String type = kButton.isSelected() ? "k" : "j";
	    int order = junctions.get(junctionList.getSelectedIndex()).order;
	    int index = junctions.get(junctionList.getSelectedIndex()).index;
	    try {
		String command = "place " + type + " " + parentList.getSelectedValue() + 
		    " " + nameField.getText() + " " + order + " " + index +
		    " " + xLocation.getValue() + " " + yLocation.getValue() +
		    " " + zLocation.getValue() + " v_" + version++;
		
		System.out.println(command);
		application.runScriptLine(command);
	    } catch(CommandException ex) {
		JOptionPane.showMessageDialog(frame, "Error. Could not place kissing loop/junction.\n" +
					      ex.getMessage());

	    }
		
	    frame.setVisible(false);
	    frame.dispose();

	}
    }

}
