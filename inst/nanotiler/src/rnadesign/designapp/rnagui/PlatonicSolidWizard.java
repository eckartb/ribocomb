package rnadesign.designapp.rnagui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerConstants;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import tools3d.Vector3D;

/** lets user choose to partner object, inserts correspnding link into controller */
public class PlatonicSolidWizard implements GeneralWizard {
    private static final int COL_SIZE_DOUBLE = 8; 
    private static final int COL_SIZE_INT = 5; 
    private static final int COL_SIZE_NAME = 15; 
    private boolean finished = false;

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    private JFrame frame = null;
    private JTextField seqCharField;
    private JTextField lengthField;

    private JTextField nameField;

    // start position:
    private JTextField sxField;
    private JTextField syField;
    private JTextField szField;

    private JCheckBox addStemsBox;
    private JCheckBox onlyFirstPathBox;
    private JComboBox geometryChoice;
    private JComboBox tilerChoice;
    private List<ActionListener> actionListeners = new ArrayList<ActionListener>();
    private Object3DGraphController graphController;
    private boolean addStemsFlag = false;
    private boolean onlyFirstPathMode = false;
    private double length = 100.0;
    private double sx = 0.0;
    private double sy = 0.0;
    private double sz = 0.0;
    private int geometryId = 0; // indicates what geometry was chosen
    private int tilerAlgorithm = Object3DGraphControllerConstants.TILER_RANDOM;
    private int nx = 3;
    private int ny = 3;
    private int nz = 3;
    private String name = "polyhedron";
    private String charString = "N"; // sequence character
    private static String[] geometryNames = {"Tetrahedron", "Cube", "Octahedron", "Dodecahedron", "Icosahedron"};
    private static String[] tilerNames = {"random", "complete", "simple", "fragment"};

    public static final String FRAME_TITLE = "Insert Platonic shape Wizard";

    private class DoneListener implements ActionListener {

	public void actionPerformed(ActionEvent e) {
		readOutValues();
		char sequenceChar = 'N';
		if (charString.length() > 0) {
		    sequenceChar = charString.charAt(0);
		}
		String childNameBase = "p";
		// generatePosition(), // ???
		int geometryCode = translateGeometryId(geometryId);
		try {
		    graphController.addGeometry(new Vector3D(sx,sy,sz),
						length, 
						name, childNameBase, addStemsFlag, 
						sequenceChar, onlyFirstPathMode,
						geometryCode, tilerAlgorithm);
		}
		catch (Object3DGraphControllerException exc) {
		    JOptionPane.showMessageDialog(frame, "An error occured during the tiling algorithm: " + exc.getMessage());
		}
		frame.setVisible(false);
		frame = null;
		finished = true;
	}
    }

    private class CancelListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    frame.setVisible(false);
	    frame = null;
	    finished = true;
	}
    }

    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    public boolean isFinished() {
	return finished;
    }

    /** generates and inserts new Object3D according to user dialog */
    public void launchWizard(Object3DGraphController graphController,
			     Component parentFrame) {
	if (graphController == null) {
	    log.info("GraphController received by launchWizard is null!");
	}
	this.graphController = graphController;
	frame = new JFrame(FRAME_TITLE);
	addComponents(frame);
	frame.pack();
	frame.setVisible(true);
    }

    public void addComponents(JFrame _frame) {
	Container f = _frame.getContentPane();
	f.setLayout(new BorderLayout());
	JPanel center = new JPanel();
	center.setLayout(new BoxLayout(center, BoxLayout.Y_AXIS));

	JPanel topPanel = new JPanel();
	JPanel xPanel = new JPanel();
	JPanel sPanel = new JPanel();
	JPanel checkPanel = new JPanel();
	
	addStemsBox = new JCheckBox("Add stems:",addStemsFlag);
	onlyFirstPathBox = new JCheckBox("Use first path:", onlyFirstPathMode);
	seqCharField = new JTextField(charString, 1);
	lengthField = new JTextField(""+length, COL_SIZE_DOUBLE);
	sxField = new JTextField(""+sx, COL_SIZE_DOUBLE);
	syField = new JTextField(""+sy, COL_SIZE_DOUBLE);
	szField = new JTextField(""+sz, COL_SIZE_DOUBLE);
	nameField = new JTextField(name, COL_SIZE_NAME);
	
	topPanel.add(new JLabel("Name:"));
	topPanel.add(nameField);
	// topPanel.add(new JLabel("Sequence character:"));
	// topPanel.add(seqCharField);
	geometryChoice = new JComboBox(geometryNames);
	xPanel.add(geometryChoice);
	tilerChoice = new JComboBox(tilerNames);
	// xPanel.add(tilerChoice);
	xPanel.add(new JLabel("Length:"));
	xPanel.add(lengthField);

	sPanel.add(new JLabel("Position: x:"));
	sPanel.add(sxField);
	sPanel.add(new JLabel("y:"));
	sPanel.add(syField);
	sPanel.add(new JLabel("z:"));
	sPanel.add(szField);

	// checkPanel.add(addStemsBox);
	// checkPanel.add(onlyFirstPathBox);

	center.add(xPanel);
	center.add(sPanel);
	center.add(checkPanel);

	JPanel bottomPanel = new JPanel();
	JButton button = new JButton("Cancel");
	button.addActionListener(new CancelListener());
	bottomPanel.add(button);
  	button = new JButton("Done");
	button.addActionListener(new DoneListener());
	bottomPanel.add(button);
	f.add(topPanel, BorderLayout.NORTH);
	f.add(bottomPanel, BorderLayout.SOUTH);
	f.add(center, BorderLayout.CENTER);
    }

    Vector3D generatePosition() {
	log.info("Line 174: PlatonicSolidWizard!");
	Vector3D result = null;
	double sx = Double.parseDouble(sxField.getText().trim());
	double sy = Double.parseDouble(syField.getText().trim());
	double sz = Double.parseDouble(szField.getText().trim());
	result = new Vector3D(sx,sy,sz);
	return result;
    }

    private void readOutValues() {
	addStemsFlag = addStemsBox.isSelected();
	onlyFirstPathMode = onlyFirstPathBox.isSelected();
	charString = seqCharField.getText().trim();
	length = Double.parseDouble(lengthField.getText().trim());
	name = nameField.getText().trim();
	geometryId = geometryChoice.getSelectedIndex();
	tilerAlgorithm = translateTilerId(tilerChoice.getSelectedIndex());
    }

    private int translateGeometryId(int id) {
	switch (id) {
	case 0: return Object3DGraphControllerConstants.TETRAHEDRON;
	case 1: return Object3DGraphControllerConstants.CUBE;
	case 2: return Object3DGraphControllerConstants.OCTAHEDRON;
	case 3: return Object3DGraphControllerConstants.DODECAHEDRON;
	case 4: return Object3DGraphControllerConstants.ICOSAHEDRON;
	}
	log.severe("internal error in PlatonicSolidWizard!");
	return  Object3DGraphControllerConstants.CUBE; // should never reach this point
    }

    private int translateTilerId(int id) {
	switch (id) {
	case 0: return Object3DGraphControllerConstants.TILER_RANDOM;
	case 1: return Object3DGraphControllerConstants.TILER_COMPLETE;
	case 2: return Object3DGraphControllerConstants.TILER_SIMPLE;
	case 3: return Object3DGraphControllerConstants.TILER_FRAGMENT;
	}
	log.severe("Internal error in PlatonicSolidWizard!");
	return  Object3DGraphControllerConstants.TILER_RANDOM; // should never reach this point
    }

}
