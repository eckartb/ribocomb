package rnadesign.designapp.rnagui;

import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import commandtools.CommandApplication;
import commandtools.CommandException;

/**
 * GUI Implementation of quader command.
 * Quader command translates objects in NanoTiler
 * in a grid format.
 *
 * @author Christine Viets
 */
public class QuaderWizard implements Wizard {

    private static final int COL_SIZE_INT = 3;
    private static final String FRAME_TITLE = "Quader Wizard";

    private boolean finished = false;
    private JFrame frame = null;
    private List<ActionListener> actionListeners = new ArrayList<ActionListener>();
    private CommandApplication application;

    private JTextField xMinField;
    private JTextField yMinField;
    private JTextField zMinField;
    private JTextField xMaxField;
    private JTextField yMaxField;
    private JTextField zMaxField;

    /**
     * Constructor.
     *
     * @param application NanoTiler
     */
    public QuaderWizard(CommandApplication application) {
	assert application != null;
	this.application = application;
    }

    /**
     * Called when "Cancel" or "Done" button is pressed.
     * Window disappears.
     *
     * @author Christine Viets
     */
    private class CancelListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    frame.setVisible(false);
	    frame = null;
	    finished = true;
	}
    }

    /**
     * Called when "Done" button is pressed.
     * Calls quader command.
     *
     * @author Christine Viets
     */
    private class DoneListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    int xMin = Integer.parseInt(xMinField.getText().trim());
	    int yMin = Integer.parseInt(yMinField.getText().trim());
	    int zMin = Integer.parseInt(zMinField.getText().trim());
	    int xMax = Integer.parseInt(xMaxField.getText().trim());
	    int yMax = Integer.parseInt(yMaxField.getText().trim());
	    int zMax = Integer.parseInt(zMaxField.getText().trim());
	    String command = "quader " + xMin + " " + yMin + " " +
		zMin + " " + xMax + " " + yMax + " " + zMax;
	    try {
		application.runScriptLine(command);
	    }
	    catch (CommandException ce) {
		JOptionPane.showMessageDialog(frame, ce + ": " + ce.getMessage());
	    }
	}
    }

    /**
     * Adds an action listener.
     *
     * @param listener The ActionListener to add.
     */
    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    /** Adds the components to the window. */
     public void addComponents() {
	Container f = frame.getContentPane();
	f.setLayout(new BoxLayout(f, BoxLayout.Y_AXIS));
	JPanel minMaxPanel = new JPanel(new GridLayout(4, 3));
	xMinField = new JTextField("0", COL_SIZE_INT);
	yMinField = new JTextField("0", COL_SIZE_INT);
	zMinField = new JTextField("0", COL_SIZE_INT);
	xMaxField = new JTextField("1", COL_SIZE_INT);
	yMaxField = new JTextField("1", COL_SIZE_INT);
	zMaxField = new JTextField("1", COL_SIZE_INT);
	minMaxPanel.add(new JLabel());
	minMaxPanel.add(new JLabel("min"));
	minMaxPanel.add(new JLabel("max"));
	minMaxPanel.add(new JLabel("x"));
	minMaxPanel.add(xMinField);
	minMaxPanel.add(xMaxField);
	minMaxPanel.add(new JLabel("y"));
	minMaxPanel.add(yMinField);
	minMaxPanel.add(yMaxField);
	minMaxPanel.add(new JLabel("z"));
	minMaxPanel.add(zMinField);
	minMaxPanel.add(zMaxField);
	JPanel buttonPanel = new JPanel(new FlowLayout());
	JButton button = new JButton("Cancel");
	button.addActionListener(new CancelListener());
	buttonPanel.add(button);
	button = new JButton("Done");
	button.addActionListener(new DoneListener());
	button.addActionListener(new CancelListener());
	buttonPanel.add(button);
	f.add(minMaxPanel);
	f.add(buttonPanel);
    }
	
    /** Returns a String representation of the current selection. */
    /*
    private String getSelectionText() {
	Object3DGraphController graphController = ((AbstractDesigner)application).getGraphController();
	Object3DController gControl = graphController.getGraph();
	Object3D selection = gControl.getSelectionRoot();
	if (selection == null) {
	    return "";
	}
	String selectionName = selection.getName();
	Object3D curr = selection;
	while (curr.getParent() != null) {
	    curr = curr.getParent();
	    selectionName = curr.getName() + "." + selectionName;
	}
	return selectionName;
    }
    */

    public boolean isFinished() {
	return finished;
    }

    /** Creates a window. */
    public void launchWizard(Component parentFrame) {
	frame = new JFrame(FRAME_TITLE);
	addComponents();
	frame.pack();
	frame.setVisible(true);
    }

}
