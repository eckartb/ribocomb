package rnadesign.designapp.rnagui;

import java.awt.Color;
import java.util.Properties;
import java.util.TreeMap;

import tools3d.Ambiente;
import tools3d.Appearance;
import tools3d.Camera;
import tools3d.GeometryColorModel;
import tools3d.Vector3D;

public class RainbowColorModel implements GeometryColorModel {


    private Integer redFactor = -5;
    private Integer blueFactor = 10;
    private Integer greenFactor = 2;

    private TreeMap<Integer, Color> colors = new TreeMap<Integer, Color>();


    public static final Color COLOR = Color.red;
    public static final Color DEFAULT_COLOR = Color.gray;


    public Color computeColor(Ambiente ambiente,
			      Appearance appearance,
			      Properties properties,
			      Vector3D faceNormal,
			      Camera camera) {


	if(properties == null)
	    return DEFAULT_COLOR;

	String className = properties.getProperty("class_name");
	String siblingId = properties.getProperty("sibling_id");
	String parentId = properties.getProperty("parent_id");
	String parentClassName = properties.getProperty("parent_class_name");

	if(className == null || siblingId == null || siblingId == null || parentClassName == null)
	    return DEFAULT_COLOR;

	if(className.equals("Nucleotide3D")) {
	    Integer index = Integer.parseInt(siblingId);
	    Color color = getColor(index, COLOR);
	    colors.put(index, color);
	    return color;
	}

	else if(parentClassName.equals("Nucleotide3D")) {
	    Integer index = Integer.parseInt(parentId);
	    Color color = colors.get(index);
	    if(color == null) {
		color = getColor(index, COLOR);
		colors.put(index, color);
	    }
	    return color;
	}

	return DEFAULT_COLOR;



    }

    /** Compute a color from the integer and color offset */
    public Color getColor(int index, Color offset) {
	int red = offset.getRed();
	int blue = offset.getBlue();
	int green = offset.getGreen();
	for(int i = 0; i < index; ++i) {
	    red = addFactorToColor(red, redFactor);
	    blue = addFactorToColor(blue, blueFactor);
	    green = addFactorToColor(green, greenFactor);
	}

	return new Color(red, blue, green);

    }

    private int addFactorToColor(int colorValue, Integer factor) {
	int result = colorValue + factor;
	if(result < 0) {
	    result *= -1;
	    if(factor == redFactor)
		redFactor *= -1;
	    else if(factor == blueFactor)
		blueFactor *= -1;
	    else if(factor == greenFactor)
		greenFactor *= -1;

	}

	if(result > 255) {
	    result = 255;
	    if(factor == redFactor)
		redFactor *= -1;
	    else if(factor == blueFactor)
		blueFactor *= -1;
	    else if(factor == greenFactor)
		greenFactor *= -1;
	    
	}

	return result;

    }
    
    
}
