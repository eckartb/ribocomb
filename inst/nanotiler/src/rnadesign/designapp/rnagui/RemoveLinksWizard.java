package rnadesign.designapp.rnagui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;
import java.util.*;
import commandtools.*;
import controltools.*;
import rnadesign.rnacontrol.*;

import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerConstants;

/** lets user choose to partner object, inserts correspnding link into controller */
public class RemoveLinksWizard implements GeneralWizard, ModelChangeListener {

    public static final String FRAME_TITLE = "Remove Links";

    private CommandApplication application;

    private JList list = new JList();

    private static int junction_suffix = 1;

    private int selectedLink;

    private ArrayList<ActionListener> actionListeners = new ArrayList<ActionListener>();
    private boolean finished = false;
    private Object3DGraphController graphController;
    private JFrame frame;
    private LinkController linkController;

    public RemoveLinksWizard(CommandApplication application) {
	this.application = application;
    }

    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    public boolean isFinished() {
	return finished;
    }

    /** generates and inserts new Object3D according to user dialog */
    public void launchWizard(Object3DGraphController graphController,
			     Component parentFrame) {

	this.graphController = graphController;
	linkController = graphController.getLinks();

	frame = new JFrame(FRAME_TITLE);
	frame.addWindowListener(new WindowAdapter() {
		
		public void windowClosed(WindowEvent e) {
		    finished = true;
	       
		    //ViewLinksWizard.this.graphController.getLinks().removeModelChangeListener(ViewLinksWizard.this);
		}

	    });

	this.graphController.getLinks().addModelChangeListener(this);
	
	JPanel panel = new JPanel();

	list.setAlignmentX(JComponent.CENTER_ALIGNMENT);
	list.setListData(getLinks());

	panel.setLayout(new BorderLayout());
	panel.setPreferredSize(new Dimension(300,300));
	panel.add(new JScrollPane(list),BorderLayout.CENTER);


	JPanel buttonPanel = new JPanel();
	JButton close = new JButton("Close");
	close.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    frame.setVisible(false);
		    frame.dispose();
		}
	    });
	close.setAlignmentX(JComponent.CENTER_ALIGNMENT);
	buttonPanel.add(close);
	JButton remove = new JButton("Remove Link");
	remove.addActionListener(new RemoveListener());
	buttonPanel.add(remove);
	panel.add(buttonPanel,BorderLayout.SOUTH);
	frame.add(panel);

	frame.pack();
	frame.setVisible(true);
	

    }
    public String[] getLinks(){
	String s = graphController.getLinks().toPrettyString();
	StringTokenizer tokenizer = new StringTokenizer(s, "\n");
	final String[] array = new String[tokenizer.countTokens()];
	int i=0;
	
	if(tokenizer.countTokens()<=1){
	    array[0]="";
	    return array;
	}

	while(tokenizer.hasMoreTokens()){
	    if(i==0){
		tokenizer.nextToken();
		array[i] = tokenizer.nextToken();
	    }
	    else{
		array[i] = tokenizer.nextToken();
	    }
	    i++;
	}
	return array;
    }
    private final class ListListener implements ActionListener{
	public void actionPerformed(ActionEvent e){
	    selectedLink = ((JList)e.getSource()).getSelectedIndex();
	}
    }
    private final class RemoveListener implements ActionListener{
	public void actionPerformed(ActionEvent e){
	    try{
		if ((selectedLink < 0) || (selectedLink >= linkController.size())) {
	            throw new CommandExecutionException("Undefined link number: " + (selectedLink+1));
	        }
	        try {
		    linkController.remove(selectedLink);
		    list.setListData(getLinks());
	        }
	        catch (Object3DGraphControllerException gce) {
		    throw new CommandExecutionException(gce.getMessage());
	        }
	    }
	    catch(CommandExecutionException cee){
		
	    }
	}
    }


    public void modelChanged(ModelChangeEvent e) {
	String s = graphController.getLinks().toPrettyString();
	StringTokenizer tokenizer = new StringTokenizer(s, "\n");
	final String[] array = new String[tokenizer.countTokens()];
	for(int i = 0; i < array.length; ++i)
	    array[i] = tokenizer.nextToken();
	
	list.setModel(new AbstractListModel() {
		public int getSize() { return array.length; }
		public Object getElementAt(int index) { return array[index]; }

	    });

	list.revalidate();
	frame.repaint();

    }

}
