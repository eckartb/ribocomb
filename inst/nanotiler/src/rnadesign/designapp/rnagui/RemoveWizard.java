package rnadesign.designapp.rnagui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import rnadesign.rnacontrol.Object3DController;
import rnadesign.rnacontrol.Object3DGraphController;

/** lets user choose to partner object, inserts correspnding link into controller */
public class RemoveWizard implements GeneralWizard {

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    private boolean finished = false;
    private JFrame frame = null;
    private Object3DTreePanel leftTreePanel;
    private List<ActionListener> actionListeners = new ArrayList<ActionListener>();
    private Object3DController graphController;
    public static final String FRAME_TITLE = "Insert Link Wizard";

    private class DoneListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    // Object3D o1 = leftTreePanel.getLastSelected();
	    // if (o1 != null) {
	    String msg = "";
	    graphController.remove();
	    frame.setVisible(false);
	    frame = null;
	    finished = true;
// 	    }
// 	    else {
// 	    }
	}
    }

    private class CancelListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    frame.setVisible(false);
	    frame = null;
	    finished = true;
	}
    }

    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    public boolean isFinished() {
	return finished;
    }

    /** generates and inserts new Object3D according to user dialog */
    public void launchWizard(Object3DGraphController graphController,
			     Component parentFrame) {
	if (graphController == null) {
	    log.info("GraphController received by launchWizard is null!");
	}
	this.graphController = graphController.getGraph();
	frame = new JFrame(FRAME_TITLE);
	addComponents(frame);
	frame.pack();
	frame.setVisible(true);
    }

    public void addComponents(JFrame _frame) {
	Container f = _frame.getContentPane();
	f.setLayout(new BorderLayout());
	JPanel center = new JPanel();
	center.setLayout(new FlowLayout());
	if (graphController == null) {
	    log.info("GraphController is null before generating left and right tree panels!");
	}
	// leftTreePanel = new Object3DTreePanel(graphController);
	// center.add(leftTreePanel);
	f.add(center, BorderLayout.CENTER);
	JPanel bottomPanel = new JPanel();
	JButton button = new JButton("Cancel");
	button.addActionListener(new CancelListener());
	bottomPanel.add(button);
  	button = new JButton("Done");
	button.addActionListener(new DoneListener());
	bottomPanel.add(button);
	f.add(bottomPanel, BorderLayout.SOUTH);
    }

}
