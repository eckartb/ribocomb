package rnadesign.designapp.rnagui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import commandtools.CommandApplication;
import commandtools.CommandException;
import rnadesign.designapp.AbstractDesigner;
import rnadesign.rnacontrol.Object3DController;
import rnadesign.rnacontrol.Object3DGraphController;
import tools3d.objects3d.Object3D;

/**
 * GUI Implementation of "rename" command
 *
 * @author Brett Boyle
 */
public class RenameWizard implements Wizard {

    // public static Logger log = Logger.getLogger("NanoTiler_debug");
    public static final int COL_SIZE_SELECTION = 20; //TODO: size?
    public static final int COL_SIZE_COORDINATE = 6;

    private boolean finished = false;
    private JFrame frame = null;
    private Container container = null;
    private List<ActionListener> actionListeners = new ArrayList<ActionListener>();
    public static final String FRAME_TITLE = "Rename Wizard";
    private JTextField selectionField;
    private JTextField nameField;
    private CommandApplication application;
    private Vector<String> tree;
    private String[] allowedNames;
    private String[] forbiddenNames;
    private JPanel treePanel;
    private JButton treeButton;
    private JList treeList;
    private JScrollPane treeScroll;
    private JLabel label;

    /**
     * Constructor.
     *
     * @param application NanoTiler
     */
    public RenameWizard(CommandApplication application) {
	assert application != null;
	this.application = application;
    }

    /**
     * Called when "Done" button is pressed.
     * Makes window disappear.
     *
     */
    private class CancelListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    frame.setVisible(false);
	    frame = null;
	    finished = true;
	}
    }

    /**
     * Called when "Synth Object" button is pressed.
     * User-entered data is put into the program.
     */
    private class DoneListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    String command = "rename " + selectionField.getText().trim() + " " + nameField.getText().trim();
	    try {
		application.runScriptLine(command);
	    }
	    catch (CommandException ce) {
		JOptionPane.showMessageDialog(frame, "Error executing command: " +
					      command + " : " + ce.getMessage());
	    }
	}
    }

    /**
     * Called when a new value is selected in the list.
     * Allows user to pick an object from a list that
     * he/she wants selected.
     *
     */
    public class SelectionListener implements ListSelectionListener {
	public void valueChanged(ListSelectionEvent e) {
	    if (e.getValueIsAdjusting() == true) {
		String text = tree.get(e.getFirstIndex());
		text = text.substring(0, text.indexOf(" "));
		if (text.equals(selectionField.getText())) {
		    text = tree.get(e.getLastIndex());
		    text = text.substring(0, text.indexOf(" "));
		}
		selectionField.setText(text);
	    }
	}
    }

    /**
     * Called when "View tree"/"Hide tree" button is pressed.
     * Displays or hides the list of objects in NanoTiler.
     *
     */
    private class TreeListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    if (treeButton.getText().equals("View tree")) {
		treeButton.setText("Hide tree");
		Object3DGraphController controller = ((AbstractDesigner)application).getGraphController();
		tree = controller.getGraph().getTree(allowedNames, forbiddenNames);
		Container j = new Container();
		j.setLayout(new BorderLayout());
		treePanel = new JPanel();
		treeList = new JList(tree);
		treeList.addListSelectionListener(new SelectionListener());
		treeScroll = new JScrollPane(treeList);
		treePanel.add(treeScroll);
		j.add(treePanel, BorderLayout.SOUTH);
		frame.getContentPane().add(j);
		frame.pack();
	    }
	    else {
		frame.getContentPane().remove(1);
		treeButton.setText("View tree");
		frame.pack();
	    }
	}
    }

    /**
     * Adds an action listener.
     *
     * @param listener The ActionListener to be added.
     */
    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }


    /** Adds the components to the window. */
    public void addComponents() {
	Container f = new Container();
	f.setLayout(new BorderLayout());

	JPanel top = new JPanel();
	treeButton = new JButton("View tree");
	treeButton.addActionListener(new TreeListener());
	top.add(treeButton);

	JPanel left = new JPanel();
	left.setLayout(new BoxLayout(left,BoxLayout.Y_AXIS));
	label = new JLabel("Original Object: ");
	left.add(label);
	label = new JLabel("New Name: ");
	left.add(label);

	JPanel center = new JPanel();
	center.setLayout(new BoxLayout(center,BoxLayout.Y_AXIS));
	selectionField = new JTextField(getSelectionText(),COL_SIZE_SELECTION);
	center.add(selectionField, BorderLayout.CENTER);
	nameField = new JTextField(COL_SIZE_SELECTION);
	center.add(nameField);

	JPanel bottomPanel = new JPanel();
	bottomPanel.setLayout(new BorderLayout());
	 JPanel southBottomPanel = new JPanel();
	 JButton button = new JButton("Close");
	 button.addActionListener(new CancelListener());
	 southBottomPanel.add(button);
	 button = new JButton("Rename Object");
	 button.addActionListener(new DoneListener());
	 southBottomPanel.add(button);
	bottomPanel.add(southBottomPanel,BorderLayout.SOUTH);


	f.add(left,BorderLayout.WEST);
	f.add(top, BorderLayout.NORTH);
	f.add(center, BorderLayout.CENTER);
	f.add(bottomPanel, BorderLayout.SOUTH);
	container.add(f);
    }
    /** get the selection text from the tree list */
    private String getSelectionText() {
	Object3DGraphController graphController = ((AbstractDesigner)application).getGraphController();
	Object3DController gControl = graphController.getGraph();
	Object3D selection = gControl.getSelectionRoot();
	if (selection == null) {
	    return "";
	}
	String selectionName = selection.getName();
	Object3D curr = selection;
	while (curr.getParent() != null) {
	    curr = curr.getParent();
	    selectionName = curr.getName() + "." + selectionName;
	}
	return selectionName;
    }

    public boolean isFinished() {
	return finished;
    }

    /** Creates a window. */
    public void launchWizard(Component parentFrame) {
	frame = new JFrame(FRAME_TITLE);
	container = frame.getContentPane();
	container.setLayout(new FlowLayout());
	addComponents();
	frame.pack();
	frame.setVisible(true);
    }
}
