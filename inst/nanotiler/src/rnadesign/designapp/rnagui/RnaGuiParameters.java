/** Application for NanoTiler Application
 * 
 */
package rnadesign.designapp.rnagui;

import java.awt.Color;
import java.io.Serializable;

/**
 * @author Eckart Bindewald
 *
 */
class RnaGuiParameters implements Serializable {
	
    static Color colorBackground = Color.WHITE;
	
    int moveStep = 2;
    
    double circleRad = 10.0;
    
    double rectWidth = 10.0;
    
    double rectHeight = 10.0;
    
    double xScreenMax = 600;
    
    double xScreenMin = 0;
    
    double yScreenMax = 300;
    
    double yScreenMin = 0;
    
    double xSpaceMax = 200;
    
    double xSpaceMin = -200;
    
    double ySpaceMax = -200;
    
    double ySpaceMin = +200;
    
    double zSpaceMax = -200;
    
    double zSpaceMin = 200;

    double xMin = -200.0;

    double xMax = 200.0;

    double xInit = 0.0;

    double yMin = -200.0;

    double yMax = 200.0;

    double yInit = 0.0;

    double thetaInit = 0.0;
    
    double thetaMin = 0.0; //Christine - changed thetaMin from 0 to 1 because for some reason the view completely changed from when it is displayed at 0 or 1... Just kidding. When thetaMin is 1 the program crashes. 
    
    double thetaMax = 360.0; // user scale

    double phiInit = 0.0;

    double phiMin = 0.0;

    double phiMax = 360;

    double zoomInit = 0.0;

    double zoomMin = -4.0;

    double zoomMax = 4.0;
	
    int bottomPanelWidth = 600;
    
    int bottomPanelHeight = 150;
    
    int bottomPanelCenterWidth = 1000;
    
    int bottomPanelCenterHeight = 130; //150
    
    int bottomPanelLeftWidth = 120;

    int bottomPanelLeftButtonWidth = 100;
       
    int bottomPanelLeftHeight = 150;
    
    int buttonHeight = 30;

    int cameraBottomPanelWidth = 400;
    int cameraBottomPanelHeight = 140;

    int cameraDisplayRows = 5;
    int cameraDisplayCols = 40;

    int cameraDisplayWidth = 200;
    int cameraDisplayHeight = 80;

    int xSliderPanelWidth = 300;
    int xSliderPanelHeight = 18;

    int ySliderPanelWidth = 18;
    int ySliderPanelHeight = 300;

    int cameraSliderPanelWidth = 200;
    int cameraSliderPanelHeight = 120;

    // camera panel parameters:
    int cameraLeftPanelWidth = 80;
    
    int cameraLeftPanelButtonWidth = 70;
    int cameraLeftPanelHeight = 300;
    
    int cameraRightPanelWidth = 160;
    int cameraRightPanelHeight = 300;
    
    int cameraRightPanelButtonWidth = 140;
    
    int editPanelButtonHeight = 40;
    int editPanelButtonWidth = 100;
    
    int editPanelHeight = 100;
    int editPanelWidth = 120;
    
    int blockPanelHeight = 100;
    int blockPanelWidth = 150;
	
    int blockPanelButtonHeight = 40;
    int blockPanelButtonWidth = 100;

    int leftPanelHeight = 300;
    int leftPanelWidth = 120;
    
    int movePanelButtonHeight = 50;
    int movePanelButtonWidth = 60;
    
    int movePanelHeight = 150;
    int movePanelWidth = 120;
    
    int selectPanelHeight = 40;
    int selectPanelWidth = 120;
    
    int selectPanelButtonWidth = 60;
	
    SelectPanel.SelectPanelParameters selectPanelParameters 
	= new SelectPanel.SelectPanelParameters();

    int rightPanelHeight = 500;
    
    int rightPanelWidth = 200;
	
    String workDirectory = ".";

    /** deep copy 
     * @TODO NOT YET IMPLEMENTED! 
     */
    public void copy(RnaGuiParameters other) {
	// 
    }

	/**
	 * @return Returns the blockPanelButtonHeight.
	 */
	public int getBlockPanelButtonHeight() {
		return blockPanelButtonHeight;
	}

	/**
	 * @param blockPanelButtonHeight The blockPanelButtonHeight to set.
	 */
	public void setBlockPanelButtonHeight(int blockPanelButtonHeight) {
		this.blockPanelButtonHeight = blockPanelButtonHeight;
	}

	/**
	 * @return Returns the blockPanelButtonWidth.
	 */
	public int getBlockPanelButtonWidth() {
		return blockPanelButtonWidth;
	}

	/**
	 * @param blockPanelButtonWidth The blockPanelButtonWidth to set.
	 */
	public void setBlockPanelButtonWidth(int blockPanelButtonWidth) {
		this.blockPanelButtonWidth = blockPanelButtonWidth;
	}

	/**
	 * @return Returns the blockPanelHeight.
	 */
	public int getBlockPanelHeight() {
		return blockPanelHeight;
	}

	/**
	 * @param blockPanelHeight The blockPanelHeight to set.
	 */
	public void setBlockPanelHeight(int blockPanelHeight) {
		this.blockPanelHeight = blockPanelHeight;
	}

	/**
	 * @return Returns the blockPanelWidth.
	 */
	public int getBlockPanelWidth() {
		return blockPanelWidth;
	}

	/**
	 * @param blockPanelWidth The blockPanelWidth to set.
	 */
	public void setBlockPanelWidth(int blockPanelWidth) {
		this.blockPanelWidth = blockPanelWidth;
	}

	/**
	 * @return Returns the bottomPanelCenterHeight.
	 */
	public int getBottomPanelCenterHeight() {
		return bottomPanelCenterHeight;
	}

	/**
	 * @param bottomPanelCenterHeight The bottomPanelCenterHeight to set.
	 */
	public void setBottomPanelCenterHeight(int bottomPanelCenterHeight) {
		this.bottomPanelCenterHeight = bottomPanelCenterHeight;
	}

	/**
	 * @return Returns the bottomPanelCenterWidth.
	 */
	public int getBottomPanelCenterWidth() {
		return bottomPanelCenterWidth;
	}

	/**
	 * @param bottomPanelCenterWidth The bottomPanelCenterWidth to set.
	 */
	public void setBottomPanelCenterWidth(int bottomPanelCenterWidth) {
		this.bottomPanelCenterWidth = bottomPanelCenterWidth;
	}

	/**
	 * @return Returns the bottomPanelHeight.
	 */
	public int getBottomPanelHeight() {
		return bottomPanelHeight;
	}

	/**
	 * @param bottomPanelHeight The bottomPanelHeight to set.
	 */
	public void setBottomPanelHeight(int bottomPanelHeight) {
		this.bottomPanelHeight = bottomPanelHeight;
	}

	/**
	 * @return Returns the bottomPanelLeftHeight.
	 */
	public int getBottomPanelLeftHeight() {
		return bottomPanelLeftHeight;
	}

	/**
	 * @param bottomPanelLeftHeight The bottomPanelLeftHeight to set.
	 */
	public void setBottomPanelLeftHeight(int bottomPanelLeftHeight) {
		this.bottomPanelLeftHeight = bottomPanelLeftHeight;
	}

	/**
	 * @return Returns the bottomPanelLeftWidth.
	 */
	public int getBottomPanelLeftWidth() {
		return bottomPanelLeftWidth;
	}

	/**
	 * @param bottomPanelLeftWidth The bottomPanelLeftWidth to set.
	 */
	public void setBottomPanelLeftWidth(int bottomPanelLeftWidth) {
		this.bottomPanelLeftWidth = bottomPanelLeftWidth;
	}

	/**
	 * @return Returns the bottomPanelWidth.
	 */
	public int getBottomPanelWidth() {
		return bottomPanelWidth;
	}

	/**
	 * @param bottomPanelWidth The bottomPanelWidth to set.
	 */
	public void setBottomPanelWidth(int bottomPanelWidth) {
		this.bottomPanelWidth = bottomPanelWidth;
	}

	/**
	 * @return Returns the buttonHeight.
	 */
	public int getButtonHeight() {
		return buttonHeight;
	}

	/**
	 * @param buttonHeight The buttonHeight to set.
	 */
	public void setButtonHeight(int buttonHeight) {
		this.buttonHeight = buttonHeight;
	}

	/**
	 * @return Returns the circleRad.
	 */
	public double getCircleRad() {
		return circleRad;
	}

	/**
	 * @param circleRad The circleRad to set.
	 */
	public void setCircleRad(double circleRad) {
		this.circleRad = circleRad;
	}

	/**
	 * @return Returns the colorBackground.
	 */
	public Color getColorBackground() {
		return colorBackground;
	}

	/**
	 * @param colorBackground The colorBackground to set.
	 */
	public void setColorBackground(Color colorBackground) {
		this.colorBackground = colorBackground;
	}

	/**
	 * @return Returns the editPanelButtonHeight.
	 */
	public int getEditPanelButtonHeight() {
		return editPanelButtonHeight;
	}

	/**
	 * @param editPanelButtonHeight The editPanelButtonHeight to set.
	 */
	public void setEditPanelButtonHeight(int editPanelButtonHeight) {
		this.editPanelButtonHeight = editPanelButtonHeight;
	}

	/**
	 * @return Returns the editPanelButtonWidth.
	 */
	public int getEditPanelButtonWidth() {
		return editPanelButtonWidth;
	}

	/**
	 * @param editPanelButtonWidth The editPanelButtonWidth to set.
	 */
	public void setEditPanelButtonWidth(int editPanelButtonWidth) {
		this.editPanelButtonWidth = editPanelButtonWidth;
	}

	/**
	 * @return Returns the editPanelHeight.
	 */
	public int getEditPanelHeight() {
		return editPanelHeight;
	}

	/**
	 * @param editPanelHeight The editPanelHeight to set.
	 */
	public void setEditPanelHeight(int editPanelHeight) {
		this.editPanelHeight = editPanelHeight;
	}

	/**
	 * @return Returns the editPanelWidth.
	 */
	public int getEditPanelWidth() {
		return editPanelWidth;
	}

	/**
	 * @param editPanelWidth The editPanelWidth to set.
	 */
	public void setEditPanelWidth(int editPanelWidth) {
		this.editPanelWidth = editPanelWidth;
	}

	/**
	 * @return Returns the leftPanelHeight.
	 */
	public int getLeftPanelHeight() {
		return leftPanelHeight;
	}

	/**
	 * @param leftPanelHeight The leftPanelHeight to set.
	 */
	public void setLeftPanelHeight(int leftPanelHeight) {
		this.leftPanelHeight = leftPanelHeight;
	}

	/**
	 * @return Returns the leftPanelWidth.
	 */
	public int getLeftPanelWidth() {
		return leftPanelWidth;
	}

	/**
	 * @param leftPanelWidth The leftPanelWidth to set.
	 */
	public void setLeftPanelWidth(int leftPanelWidth) {
		this.leftPanelWidth = leftPanelWidth;
	}

	/**
	 * @return Returns the movePanelButtonHeight.
	 */
	public int getMovePanelButtonHeight() {
		return movePanelButtonHeight;
	}

	/**
	 * @param movePanelButtonHeight The movePanelButtonHeight to set.
	 */
	public void setMovePanelButtonHeight(int movePanelButtonHeight) {
		this.movePanelButtonHeight = movePanelButtonHeight;
	}

	/**
	 * @return Returns the movePanelButtonWidth.
	 */
	public int getMovePanelButtonWidth() {
		return movePanelButtonWidth;
	}

	/**
	 * @param movePanelButtonWidth The movePanelButtonWidth to set.
	 */
	public void setMovePanelButtonWidth(int movePanelButtonWidth) {
		this.movePanelButtonWidth = movePanelButtonWidth;
	}

	/**
	 * @return Returns the movePanelHeight.
	 */
	public int getMovePanelHeight() {
		return movePanelHeight;
	}

	/**
	 * @param movePanelHeight The movePanelHeight to set.
	 */
	public void setMovePanelHeight(int movePanelHeight) {
		this.movePanelHeight = movePanelHeight;
	}

	/**
	 * @return Returns the movePanelWidth.
	 */
	public int getMovePanelWidth() {
		return movePanelWidth;
	}

	/**
	 * @param movePanelWidth The movePanelWidth to set.
	 */
	public void setMovePanelWidth(int movePanelWidth) {
		this.movePanelWidth = movePanelWidth;
	}

	/**
	 * @return Returns the moveStep.
	 */
	public int getMoveStep() {
		return moveStep;
	}

	/**
	 * @param moveStep The moveStep to set.
	 */
	public void setMoveStep(int moveStep) {
		this.moveStep = moveStep;
	}

	/**
	 * @return Returns the rectHeight.
	 */
	public double getRectHeight() {
		return rectHeight;
	}

	/**
	 * @param rectHeight The rectHeight to set.
	 */
	public void setRectHeight(double rectHeight) {
		this.rectHeight = rectHeight;
	}

	/**
	 * @return Returns the rectWidth.
	 */
	public double getRectWidth() {
		return rectWidth;
	}

	/**
	 * @param rectWidth The rectWidth to set.
	 */
	public void setRectWidth(double rectWidth) {
		this.rectWidth = rectWidth;
	}

	/**
	 * @return Returns the rightPanelHeight.
	 */
	public int getRightPanelHeight() {
		return rightPanelHeight;
	}

	/**
	 * @param rightPanelHeight The rightPanelHeight to set.
	 */
	public void setRightPanelHeight(int rightPanelHeight) {
		this.rightPanelHeight = rightPanelHeight;
	}

	/**
	 * @return Returns the rightPanelWidth.
	 */
	public int getRightPanelWidth() {
		return rightPanelWidth;
	}

	/**
	 * @param rightPanelWidth The rightPanelWidth to set.
	 */
	public void setRightPanelWidth(int rightPanelWidth) {
		this.rightPanelWidth = rightPanelWidth;
	}

	/**
	 * @return Returns the selectPanelHeight.
	 */
	public int getSelectPanelHeight() {
		return selectPanelHeight;
	}

	/**
	 * @param selectPanelHeight The selectPanelHeight to set.
	 */
	public void setSelectPanelHeight(int selectPanelHeight) {
		this.selectPanelHeight = selectPanelHeight;
	}

	/**
	 * @return Returns the selectPanelWidth.
	 */
	public int getSelectPanelWidth() {
		return selectPanelWidth;
	}

	/**
	 * @param selectPanelWidth The selectPanelWidth to set.
	 */
	public void setSelectPanelWidth(int selectPanelWidth) {
		this.selectPanelWidth = selectPanelWidth;
	}

	/**
	 * @return Returns the workDirectory.
	 */
	public String getWorkDirectory() {
		return workDirectory;
	}

	/**
	 * @param workDirectory The workDirectory to set.
	 */
	public void setWorkDirectory(String workDirectory) {
		this.workDirectory = workDirectory;
	}

	/**
	 * @return Returns the xScreenMax.
	 */
	public double getXScreenMax() {
		return xScreenMax;
	}

	/**
	 * @param screenMax The xScreenMax to set.
	 */
	public void setXScreenMax(double screenMax) {
		xScreenMax = screenMax;
	}

	/**
	 * @return Returns the xScreenMin.
	 */
	public double getXScreenMin() {
		return xScreenMin;
	}

	/**
	 * @param screenMin The xScreenMin to set.
	 */
	public void setXScreenMin(double screenMin) {
		xScreenMin = screenMin;
	}

	/**
	 * @return Returns the xSpaceMax.
	 */
	public double getXSpaceMax() {
		return xSpaceMax;
	}

	/**
	 * @param spaceMax The xSpaceMax to set.
	 */
	public void setXSpaceMax(double spaceMax) {
		xSpaceMax = spaceMax;
	}

	/**
	 * @return Returns the xSpaceMin.
	 */
	public double getXSpaceMin() {
		return xSpaceMin;
	}

	/**
	 * @param spaceMin The xSpaceMin to set.
	 */
	public void setXSpaceMin(double spaceMin) {
		xSpaceMin = spaceMin;
	}

	/**
	 * @return Returns the yScreenMax.
	 */
	public double getYScreenMax() {
		return yScreenMax;
	}

	/**
	 * @param screenMax The yScreenMax to set.
	 */
	public void setYScreenMax(double screenMax) {
		yScreenMax = screenMax;
	}

	/**
	 * @return Returns the yScreenMin.
	 */
	public double getYScreenMin() {
		return yScreenMin;
	}

	/**
	 * @param screenMin The yScreenMin to set.
	 */
	public void setYScreenMin(double screenMin) {
		yScreenMin = screenMin;
	}

	/**
	 * @return Returns the ySpaceMax.
	 */
	public double getYSpaceMax() {
		return ySpaceMax;
	}

	/**
	 * @param spaceMax The ySpaceMax to set.
	 */
	public void setYSpaceMax(double spaceMax) {
		ySpaceMax = spaceMax;
	}

	/**
	 * @return Returns the ySpaceMin.
	 */
	public double getYSpaceMin() {
		return ySpaceMin;
	}

	/**
	 * @param spaceMin The ySpaceMin to set.
	 */
	public void setYSpaceMin(double spaceMin) {
		ySpaceMin = spaceMin;
	}

	/**
	 * @return Returns the zSpaceMax.
	 */
	public double getZSpaceMax() {
		return zSpaceMax;
	}

	/**
	 * @param spaceMax The zSpaceMax to set.
	 */
	public void setZSpaceMax(double spaceMax) {
		zSpaceMax = spaceMax;
	}

	/**
	 * @return Returns the zSpaceMin.
	 */
	public double getZSpaceMin() {
		return zSpaceMin;
	}

	/**
	 * @param spaceMin The zSpaceMin to set.
	 */
	public void setZSpaceMin(double spaceMin) {
		zSpaceMin = spaceMin;
	}

}
