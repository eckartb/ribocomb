package rnadesign.designapp.rnagui;

import java.awt.Color;
import java.util.Properties;

import tools3d.Ambiente;
import tools3d.Appearance;
import tools3d.Camera;
import tools3d.GeometryColorModel;
import tools3d.Vector3D;

public class RnaStrandColorModel implements GeometryColorModel {
    
    public static final int COLOR_FULL = 1;
    public static final int COLOR_PASTELL = 2;

    private Color defaultColor = Color.GRAY;
    private Color junctionColor = Color.MAGENTA;
    private Color[] specialColors = new Color[4];
    private Color[] colors; // = { Color.green, Color.yellow,
			    //   Color.black, Color.red, Color.blue };
    private Color[] colorsBright;
    private Color[] colorsDark;
    private Color physicalColor = Color.RED; // used for chemical bonds, Watson-Crick interacions ets
    private Color chemicalColor = Color.CYAN;

    /** defines default colors.
     * According to Visbone Color wheel: http://www.visibone.com/colorlab/
     * lf blue : "light faded blue; mw blue: medium weak blue
     */
    public RnaStrandColorModel(int colorMode) {
	specialColors[0] = new Color(162, 224, 72); // spring green
	specialColors[1] = new Color(213, 123, 224); // magenta
	// specialColors[2] = new Color(224, 215, 184); // sand
	specialColors[2] = new Color(255, 153, 0);
	specialColors[3] = new Color(161, 28, 233); // purple
	switch (colorMode) {
	case COLOR_FULL:
	    colors = new Color[12];
	    colors[0] = new Color(0, 0, 255); // lf blue
	    colors[1] = new Color(255, 102, 102); // lf red
	    colors[2] = new Color(102, 255, 102); // lf green
	    colors[3] = new Color(255, 255, 102); // lf yellow
	    colors[4] = new Color(102, 255, 255); // lf cyan
	    colors[5] = new Color(255, 102, 255); // lf magenta
	    colors[6] = new Color(102, 102, 153); // mw blue
	    colors[7] = new Color(153, 102, 102); // mw red
	    colors[8] = new Color(102, 153, 102); // mw green
	    colors[9] = new Color(153, 153, 102); // mw yellow
	    colors[10] = new Color(102, 153, 153); // mw cyan
	    colors[11] = new Color(153, 102, 153); // mw magenta
	    colorsBright = new Color[colors.length];
	    colorsDark = new Color[colors.length];
	    for (int i = 0; i < colors.length; ++i) {
		colorsBright[i] = colors[i];
		colorsDark[i] = colors[i];
	    }
	    break;
	case COLOR_PASTELL:
	    colors = new Color[12];
	    
	    colors[0] = new Color(102, 102, 255); // lf blue
	    colors[1] = new Color(255, 102, 102); // lf red
	    colors[2] = new Color(102, 255, 102); // lf green
	    colors[3] = new Color(255, 255, 102); // lf yellow
	    colors[4] = new Color(102, 255, 255); // lf cyan
	    colors[5] = new Color(255, 102, 255); // lf magenta
	    
	    colors[6] = new Color(102, 102, 153); // mw blue
	    colors[7] = new Color(153, 102, 102); // mw red
	    colors[8] = new Color(102, 153, 102); // mw green
	    colors[9] = new Color(153, 153, 102); // mw yellow
	    colors[10] = new Color(102, 153, 153); // mw cyan
	    colors[11] = new Color(153, 102, 153); // mw magenta
	    colorsBright = new Color[colors.length];
	    colorsDark = new Color[colors.length];
	    for (int i = 0; i < colors.length; ++i) {
		colorsBright[i] = colors[i].brighter().brighter();
		colorsDark[i] = colors[i].darker().darker();
	    }
	    break;

	}

    }

    /** defines default colors.
     * According to Visbone Color wheel: http://www.visibone.com/colorlab/
     * lf blue : "light faded blue; mw blue: medium weak blue
     */
    public RnaStrandColorModel() {
	this(COLOR_FULL);
    }

    /*    public Color computeColor(Ambiente ambiente,
			      Appearance appearance,
			      Vector3D faceNormal,
			      Camera camera) {
	
	if (appearance != null) {
 	    return appearance.getColor();
	}
	
 	return defaultColor;
    }
    */
    
    /** returns number between 0 and 255 */
    private int colorCap(int n) {
	if (n < 0) {
	    return 0;
	}
	if (n > 255) {
	    return 255;
	}
	return n;
    }

    /** returns val1 for frac==0 and val2 for frac==1 */
    private double interpolateCol(double frac, int val1, int val2) {
	return (1.0-frac)* val1 + (frac * val2);
    }

    /** returns col1 for frac==0 and col2 for frac==1 */
    private Color interpolateCol(double frac, Color col1, Color col2) {
	return new Color((int)(interpolateCol(frac, col1.getRed(), col2.getRed())),
			 (int)(interpolateCol(frac, col1.getGreen(), col2.getGreen())),
			 (int)(interpolateCol(frac, col1.getBlue(), col2.getBlue())));
    }

    public Color computeColor(Ambiente ambiente,
			      Appearance appearance,
			      Properties properties,
			      Vector3D faceNormal,
			      Camera camera) {

 	if (properties != null) {
 	    /** read out property "strand_id"  and "interaction_type" */
	    String strandIdString = properties.getProperty("strand_id");
	    String physicalString = properties.getProperty("interaction_type");
	    String className = properties.getProperty("class_name");
	    // object is a physical link
	    if ((physicalString != null) && (physicalString.equals("watson_crick"))) {
		return physicalColor;
	    }
	    // object is a chemical bond link
 	    if ((physicalString != null) && (physicalString.equals("backbone"))) {
		return chemicalColor;
 	    }
 	    else if (strandIdString != null) {
 		// convert to integer:
 		int strand = Integer.parseInt(strandIdString);
		strand = strand % colors.length;
 		if ((strand >= 0) && (strand < colors.length)) {
 		    Color col = colors[strand];
		    String siblingIdString = properties.getProperty("sibling_id");
		    String siblingMaxString = properties.getProperty("sibling_max");
		    if ((siblingIdString != null) && (siblingMaxString != null)) {
			int siblingId = Integer.parseInt(siblingIdString);
			int siblingMax = Integer.parseInt(siblingMaxString);
			double frac = 0.5;
			if (siblingMax > 0) {
			    frac = (double)siblingId/(double)siblingMax;
			    // frac = frac + 0.2; // do not go to totally black
			}
// 			int red = colorCap((int)(col.getRed() * frac)); 
// 			int green = colorCap((int)(col.getGreen() * frac)); 
// 			int blue = colorCap((int)(col.getBlue() * frac)); 
			// return new Color(red, green, blue);
			return interpolateCol(frac, colorsBright[strand], colorsDark[strand]);
		    }
		    return col;		    
 		}
 	    }
	    else if (className != null) {
		return specialColors[Math.abs((int)(className.hashCode())) % specialColors.length];
	    }
	}
	else {
	}
	if (appearance != null) {
	    return appearance.getColor();
	}
	return defaultColor;
    }
    
    public void setDefaultColor(Color c) {
	defaultColor = c;
    }

}
