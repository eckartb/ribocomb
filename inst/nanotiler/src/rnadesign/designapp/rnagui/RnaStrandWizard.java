package rnadesign.designapp.rnagui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

import guitools.BeanEditor;
import guitools.XMLBeanEditor;
import rnadesign.rnacontrol.Object3DController;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DHandle;
import rnadesign.rnacontrol.SequenceController;
import rnadesign.rnamodel.Nucleotide3D;
import rnadesign.rnamodel.RnaStrand;
import rnadesign.rnamodel.SimpleRnaStrand;
import sequence.DnaTools;
import sequence.Sequence;
import sequence.SimpleSequence;
import sequence.UnknownSymbolException;
import tools3d.Vector3D;
import tools3d.objects3d.Object3D;

/** generate and insert new RNA strand according to user dialog */
public class RnaStrandWizard implements GeneralWizard {

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    Object3DController controller;
    SequenceController sequences;

    BeanEditor beanEditor = new XMLBeanEditor();
    // Set actionListeners = new HashSet();
    Component frame;

    /** if "Done" was pressed, insert Object3D into graph! */
    private class DoneActionListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    log.fine("Calling DoneActionListener in RnaStrandWizard!");
	    if (isFinished()) {
		log.fine("Calling DoneActionListener in RnaStrandWizard: text editor was finished!");
 		Object3DHandle object3DHandle = getObject3DHandle();
		if ((object3DHandle != null) && object3DHandle.isValid()) {
		    controller.addGraph(object3DHandle);
		    log.fine("Here was adding of graph!");
		}
		else {
		    log.fine("Object3DDebugHandle was null!");
		}
		log.fine("Inserting new object into main object tree!");
		// 	    Iterator iterator = actionListeners.iterator();
		// 	    while (iterator.hasNext()) {
		// 		ActionListener listener = (ActionListener)(iterator.next());
		// 		listener.actionPerformed(e);
		// 	    }
	    }
	    else {
		log.fine("Calling DoneActionListener in RnaStrandWizard: text editor was NOT finished!");
	    }
	    if (frame != null) {
		frame.repaint();
	    }
	    log.fine("RnaStrandWizard finished!");
	}
    }

    public void addActionListener(ActionListener listener) {
	beanEditor.addActionListener(listener);
    }

    /** generates and inserts new Object3D according to user dialog */
    public void launchWizard(Object3DGraphController controller,
			     Component frame) {
	this.controller = controller.getGraph();
	this.frame = frame;
	RnaStrand strand = new SimpleRnaStrand();
	initStrand(strand);
	// generate output stream that ends in string:
	// 	beanEditor = new XMLBeanEditor();
	// strand = (RnaStrand)beanEditor.launchEdit(strand, null, frame);
	beanEditor.addActionListener(new DoneActionListener());
	beanEditor.launchEdit(strand, frame);
    }

    public Object3DHandle getObject3DHandle() {
	return new Object3DHandle((Object3D)(beanEditor.getObject())); 
    }

    private Sequence generateInitSequence() {
	String s = "NNNNNNNNNNNNNNNNNNNN";
	// letters A,B,C,...

	char c = (char)((int)('A') + sequences.getSequenceCount());
	char[] chars = new char[1];
	chars[0] = c;
	String name = new String(chars);
	Sequence result = null;
	try {
	    result = new SimpleSequence(s, name, 
					DnaTools.AMBIGUOUS_RNA_ALPHABET);
	}
	catch (UnknownSymbolException e) {
	    // nothing
	}
	return result;
    }

    /** initialize RNA strand: better move this code to model package or at least controller */
    private void initStrand(RnaStrand strand) {
	RnaStrand helpStrand = new SimpleRnaStrand();
	Vector3D pos = new Vector3D(10.0, 20.0, 10.0);
	helpStrand.setRelativePosition(pos);
	strand.setRelativePosition(pos);
	Sequence sequence = generateInitSequence();
	// strand.setSequence(sequence);
	log.warning("Sorry, RNA Strand wizard is currently not functional");
	assert false;
	strand.setRelativePosition(pos);
	for (int i = 0; i < sequence.size(); ++i) {
	    // Vector3D nPos = helpStrand.getResiduePosition(i);
	    // TODO : fix belox code!
	    // strand.insertChild(new Nucleotide3D(sequence.getResidue(i).getSymbol(), i));
	}
	// strand.setPosition(new Vector3D(Math.random(), Math.random(), Math.random()));
    }

    public boolean isFinished() { return beanEditor.isFinished(); }

}
