package rnadesign.designapp.rnagui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import rnadesign.rnacontrol.Object3DController;
import rnadesign.rnacontrol.Object3DGraphController;

/** lets user choose to partner object, inserts correspnding link into controller */
public class RotateWizard implements GeneralWizard {

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    private boolean finished = false;
    private JFrame frame = null;
    private JTextField axField;
    private JTextField ayField;
    private JTextField azField;
    private JTextField angleField;
    private Object3DTreePanel leftTreePanel;
    private List<ActionListener> actionListeners = new ArrayList<ActionListener>();
    private Object3DController graphController;
    private double ax = 1.0; // define rotation axis
    private double ay = 0.0;
    private double az = 0.0;
    private double angle = 0.0;

    public static final String FRAME_TITLE = "Insert Link Wizard";

    private class DoneListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
// 	    Object3D o1 = leftTreePanel.getLastSelected();
// 	    if (o1 != null) {
	    readOutValues();
	    graphController.rotateSelected(ax, ay, az, angle);
	    frame.setVisible(false);
	    frame = null;
	    finished = true;
// 	    }
// 	    else {
// 		String msg = "No objects selected for rotation!";
// 		JOptionPane.showConfirmDialog(null,
// 					      msg, "Bad rotation parameter!", JOptionPane.ERROR_MESSAGE);
// 	    }
	}
    }

    private class CancelListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    frame.setVisible(false);
	    frame = null;
	    finished = true;
	}
    }

    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    public boolean isFinished() {
	return finished;
    }

    /** generates and inserts new Object3D according to user dialog */
    public void launchWizard(Object3DGraphController graphController,
			     Component parentFrame) {
	if (graphController == null) {
	    log.info("GraphController received by launchWizard is null!");
	}
	this.graphController = graphController.getGraph();
	frame = new JFrame(FRAME_TITLE);
	addComponents(frame);
	frame.pack();
	frame.setVisible(true);
    }

    public void addComponents(JFrame _frame) {
	Container f = _frame.getContentPane();
	f.setLayout(new BorderLayout());
	JPanel center = new JPanel();
	center.setLayout(new FlowLayout());
	if (graphController == null) {
	    log.info("GraphController is null before generating left and right tree panels!");
	}
	// leftTreePanel = new Object3DTreePanel(graphController);
	// 	center.add(leftTreePanel);
	f.add(center, BorderLayout.CENTER);
	JPanel topPanel = new JPanel();

	axField = new JTextField("0.0");
	ayField = new JTextField("0.0");
	azField = new JTextField("1.0");
	angleField = new JTextField("0.0");
	topPanel.add(new JLabel("Axis x:"));
	topPanel.add(axField);
	topPanel.add(new JLabel("y:"));
	topPanel.add(ayField);
	topPanel.add(new JLabel("z:"));
	topPanel.add(azField);
	topPanel.add(new JLabel("angle:"));
	topPanel.add(angleField);
	JPanel bottomPanel = new JPanel();
	JButton button = new JButton("Cancel");
	button.addActionListener(new CancelListener());
	bottomPanel.add(button);
  	button = new JButton("Done");
	button.addActionListener(new DoneListener());
	bottomPanel.add(button);
	f.add(topPanel, BorderLayout.NORTH);
	f.add(bottomPanel, BorderLayout.SOUTH);
    }

    private void readOutValues() {
	ax = Double.parseDouble(axField.getText().trim());
	ay = Double.parseDouble(ayField.getText().trim());
	az = Double.parseDouble(azField.getText().trim());
	angle = Double.parseDouble(angleField.getText().trim());
	angle *= Math.PI/180.0; // convert to radians
    }

}
