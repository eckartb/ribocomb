package rnadesign.designapp.rnagui;

import tools3d.RotationDescriptor;

/** class that handles rotating part of the object tree */
public interface RotationWizard extends GeneralWizard {

    RotationDescriptor getRotator();

}
