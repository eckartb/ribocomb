package rnadesign.designapp.rnagui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;
import java.util.*;
import commandtools.*;

import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerConstants;

/** lets user choose to partner object, inserts correspnding link into controller */
public class RunScriptWizard implements GeneralWizard {

    public static final String FRAME_TITLE = "Run Script";

    private CommandApplication application;

    private ArrayList<ActionListener> actionListeners = new ArrayList<ActionListener>();
    private boolean finished = false;
    private Object3DGraphController graphController;
    private JFrame frame;



    public RunScriptWizard(CommandApplication application) {
	this.application = application;
    }

    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    public boolean isFinished() {
	return finished;
    }

    /** generates and inserts new Object3D according to user dialog */
    public void launchWizard(Object3DGraphController graphController,
			     Component parentFrame) {

	this.graphController = graphController;

	frame = new JFrame(FRAME_TITLE);
	frame.addWindowListener(new WindowAdapter() {
		
		public void windowClosed(WindowEvent e) {
		    finished = true;
		}

	    });

	JPanel panel = new JPanel();

	final JTextField scriptLocation = new JTextField(40);
	panel.add(scriptLocation);
	
	final JButton browse = new JButton("Browse");
	browse.setToolTipText("Browse to a script file");
	browse.addActionListener(new ActionListener() {

		public void actionPerformed(ActionEvent e) {
		    JFileChooser fileChooser = new JFileChooser();
		    fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		    String cwdName = RunScriptWizard.this.graphController.getLastReadDirectory();
		    File cwd = new File(cwdName);
		    fileChooser.setCurrentDirectory(cwd);
		    int result = fileChooser.showOpenDialog(frame);
		    if(result == JFileChooser.APPROVE_OPTION) {
			File file = fileChooser.getSelectedFile();
			scriptLocation.setText(file.getAbsolutePath());

		    }

		}
	    });
	
	panel.add(browse);

	final JButton run = new JButton("Run");
	run.setToolTipText("Run the script");
	run.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    if(scriptLocation.getText().equals(""))
			JOptionPane.showMessageDialog(frame, "Must specify the path to a script file");

		    try {
			application.runScriptLine("source " + scriptLocation.getText());
			scriptLocation.setText("");
		    } catch(CommandException ex) {
			JOptionPane.showMessageDialog(frame, ex.getMessage());
		    }


		}

	    });

	panel.add(run);

	final JButton close = new JButton("Close");
	close.setToolTipText("Close the wizard");
	close.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    frame.setVisible(false);
		    frame.dispose();
		    
		}
	    });

	panel.add(close);

	frame.add(panel);

	frame.pack();
	frame.setVisible(true);
    }

}
