package rnadesign.designapp.rnagui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
// import java.awt.datatransfer.DataFlavor;
// import java.awt.datatransfer.StringSelection;
// import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
// import java.util.Date;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
// import javax.swing.event.ListSelectionEvent;
// import javax.swing.event.ListSelectionListener;

import commandtools.BadSyntaxException;
import commandtools.CommandApplication;
import commandtools.CommandException;
import commandtools.UnknownCommandException;
import commandtools.CommandExecutionException;
import static rnadesign.designapp.rnagui.PackageConstants.*;

public class ScriptCommandDisplay extends JPanel {

    private CommandApplication app;

    private CommandHistoryDisplay historyDisplay;
    private JTextField commandInputField; //PasteField
    private JFrame frame;

    private Logger log = Logger.getLogger("NanoTiler_debug");

    public ScriptCommandDisplay(CommandApplication application) { 
	this.app = application;
	assert this.app.getInterpreter() != null;
	addComponents();
    }

    private class SaveListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    // log.severe("Save History not yet Implemented");
	    // find filename:
		log.fine("Export-BPL item activated!");
			if (app.getCommandHistory()==null) {
				return; // no data defined
			}
			JFileChooser chooser = new JFileChooser();
			chooser.setApproveButtonText("Save");
			chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			File cwd = new File(".");
			chooser.setCurrentDirectory(cwd);
		       	int returnVal = chooser.showSaveDialog(commandInputField); // parent);
		       	     if(returnVal == JFileChooser.APPROVE_OPTION) {
				String fileName = chooser.getCurrentDirectory() 
				+ SLASH + chooser.getSelectedFile().getName();
				log.info("You chose this file: " + fileName);
				// open output file
				FileOutputStream fos = null;
				try {
					fos = new FileOutputStream(fileName);
					PrintStream ps = new PrintStream(fos);
					ps.println(app.getCommandHistory().toString());
					fos.close();
				}
				catch (IOException exc) {
				    log.severe("Error Writing to File: " + fileName + " " + exc.getMessage());
				}
				log.fine("Finished writing history!");
			     }
	}
    }

    private class RunCommandListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    String commandText = commandInputField.getText();

	    log.info("Running command: " + commandText);
	    try {
	    	app.runScriptLine(commandText);
	    }
	    catch (Exception ex) {
	    	log.warning(ex.getMessage());
	    }

	    commandInputField.setText("");
	    commandInputField.requestFocusInWindow();
	}
    }

    private class HelpCommandListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    String commandText = "help";
	    log.fine("Running command: " + commandText);
	    try {
		app.runScriptLine(commandText);
	    }
	    catch (UnknownCommandException uce) {
		log.warning(uce.getMessage());
	    }
	    catch (BadSyntaxException bse) {
		log.warning(bse.getMessage());
	    }
	    catch (CommandException ce) {
		log.warning(ce.getMessage());
	    }
	}
    }

    private class ClearCommandListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    commandInputField.setText("");
	    log.finest("Clear text input!");
	}
    }

    /* TODO: CEV
      private final class PasteActionListener implements MouseListener {
      public void mouseExited(MouseEvent e) { }
      public void mouseEntered(MouseEvent e) { }
      public void mouseReleased(MouseEvent e) { }
      public void mousePressed(MouseEvent e) { }
      public void mouseClicked(MouseEvent e) {
      System.out.println("Modifiers: " + e.getModifiers());
      //System.out.println("Source: " + e.getSource() + "\nid: " + e.getID() + "\nwhen: " + e.getWhen() + "\nx: " + e.getX() + "\ny: " + e.getY() + "\nclickcount: " + e.getClickCount() + "\npopuptrigger: " + e.isPopupTrigger());
      String oldText = commandInputField.getText();
      if (e.getButton() == MouseEvent.BUTTON2) {
      if (historyDisplay.getClipboard() != null) {
      System.out.println("Clipboard was not null");
      StringSelection stringSelection = (StringSelection)historyDisplay.getClipboard().getContents(this);
      System.out.println("String selection: " + stringSelection);
      String oldCommand = "";
      try {
      oldCommand = (String)stringSelection.getTransferData(DataFlavor.stringFlavor);
      System.out.println("oldCommand: " + oldCommand);
      }
      catch (UnsupportedFlavorException ufe) {
      System.out.println("ufe: " + ufe + ": " + ufe.getMessage());
      }
      catch (IOException ioe) {
      System.out.println("ioe: " + ioe + ": " + ioe.getMessage());
      }
      commandInputField.setText(oldCommand);
      // if (newCommand.equals(oldCommand)) {
      // System.out.println(newCommand + " equaled " + oldCommand + "!");
      // }
      // else {
      // System.out.println(newCommand + " does not equal " + oldCommand);
      // }
      }
      else {
      // commandInputField.setText(oldText);
      System.out.println("Clipboard is null!");
      // historyDisplay.setClipboard(newCommand);
      }
      }
      // System.out.println("mouse clicked: " + e);
      }
      }
    */

    /* TODO: CEV
      private final class HistoryDisplayListener implements ActionListener {
      public void actionPerformed(ActionEvent e) {
      System.out.println("HistoryDisplay: " + e);
      Object source = e.getSource();
      if (source instanceof ListSelectionEvent) {
      System.out.println("instance of listselectionevent");
      ListSelectionEvent selection = (ListSelectionEvent) source;
      Object selectionSource = selection.getSource();
      if (selectionSource instanceof JList) {
      System.out.println("instance of JList");
      JList history = (JList)selectionSource;
      if (selection.getValueIsAdjusting() == true) {
      System.out.println("Selection's value is adjusting!");
      String newCommand = (String)history.getSelectedValue(); //selection.getFirstIndex());
      System.out.println("Selected value: " + newCommand);
      if (historyDisplay.getClipboard() != null) {
      System.out.println("Clipboard was not null");
      StringSelection stringSelection = (StringSelection)historyDisplay.getClipboard().getContents(this);
      System.out.println("String selection: " + stringSelection);
      String oldCommand = "";
      try {
      oldCommand = (String)stringSelection.getTransferData(DataFlavor.stringFlavor);
      System.out.println("oldCommand: " + oldCommand);
      }
      catch (UnsupportedFlavorException ufe) {
      System.out.println("ufe: " + ufe + ": " + ufe.getMessage());
      }
      catch (IOException ioe) {
      System.out.println("ioe: " + ioe + ": " + ioe.getMessage());
      }
      if (newCommand.equals(oldCommand)) {
      System.out.println(newCommand + " equaled " + oldCommand + "!");
      }
      else {
      System.out.println(newCommand + " does not equal " + oldCommand);
      }
      }
      else {
      System.out.println("Clipboard is null!");
      historyDisplay.setClipboard(newCommand);
      }
      // text = text.substring(0, text.indexOf(" "));
      // if (text.equals(selectionField.getText())) {
      // text = tree.get(selection.getLastIndex());
      // text = text.substring(0, text.indexOf(" "));
      // }
      // selectionField.setText(text);
      }
      else {
      System.out.println("Selection's value is not adjusting!");
      }
      }
      }
      }
      }
    */

    private void addComponents() {
	setLayout(new FlowLayout());
	setBorder(BorderFactory.createLineBorder(Color.black));
	JPanel leftPanel = new JPanel();
	leftPanel.setLayout(new BorderLayout());
	JPanel rightPanel = new JPanel();
	rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.Y_AXIS));
	JPanel leftUpperPanel = new JPanel();
	JPanel leftLowerPanel = new JPanel();
	leftLowerPanel.setLayout(new FlowLayout(FlowLayout.TRAILING));
	leftUpperPanel.add(new JLabel("Command:"));
	commandInputField = new JTextField("",60);

	commandInputField.addKeyListener(new KeyListener() {
		public void keyPressed(KeyEvent e) { 
		    int id = e.getKeyCode(); 
		    if (id == KeyEvent.VK_ENTER) { 
			String commandText = commandInputField.getText();
			log.info("Running command: " + commandText);
			try {
			    app.runScriptLine(commandText);
			}
			catch (Exception ex) {
			    log.warning(ex.getMessage());
			}
			commandInputField.setText("");
		    }
		    else if (id == KeyEvent.VK_RIGHT) {
			String text = commandInputField.getText();
			String completed = app.getInterpreter().autoComplete(text);
			commandInputField.setText(completed);
		    } 
		    commandInputField.requestFocusInWindow();
		}
		public void keyReleased(KeyEvent e) { }
		public void keyTyped(KeyEvent e) { }
	    });

	// System.out.println("Commandinputfield's action: " + commandInputField.getAction());
	// PasteActionListener pasteListener = new PasteActionListener();
	// commandInputField.addMouseListener(pasteListener);
	// KeyStroke paste = KeyStroke.getKeyStroke(KeyEvent.VK_V, ActionEvent.CTRL_MASK, false);
	// MouseEvent pasteEvent = new MouseEvent(commandInputField, MouseEvent.MOUSE_CLICKED, ((new Date()).getTime()), 8, 138, 12, 1, false, MouseEvent.BUTTON2);
	// commandInputField.getInputMap().put(paste, "COPY"); //pasteListener.mouseClicked(pasteEvent));
	// System.out.println("Commandinputfield's action 2: " + commandInputField.getAction());
	leftUpperPanel.add(commandInputField);
	JButton runButton = new JButton("Run");
	runButton.addActionListener(new RunCommandListener());
	JButton clearButton = new JButton("Clear");
	clearButton.addActionListener(new ClearCommandListener());
	JButton helpButton = new JButton("Help");
	helpButton.addActionListener(new HelpCommandListener());
	leftLowerPanel.add(runButton);
	leftLowerPanel.add(clearButton);
	leftLowerPanel.add(helpButton);
	historyDisplay = new CommandHistoryDisplay(app);
	// historyDisplay.addActionListener(new HistoryDisplayListener());
	rightPanel.add(historyDisplay);
	JButton saveButton = new JButton("Save history");
	saveButton.addActionListener(new SaveListener());
	// saveButton.disable();
	rightPanel.add(saveButton);
	leftPanel.add(leftUpperPanel, BorderLayout.NORTH);
	leftPanel.add(leftLowerPanel, BorderLayout.SOUTH);
	add(leftPanel);
	add(rightPanel);
	
    }

}
