package rnadesign.designapp.rnagui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import rnadesign.rnacontrol.Object3DGraphController;
import rnasecondary.MutableSecondaryStructure;
import rnasecondary.SimpleMutableSecondaryStructure;
import sequence.DuplicateNameException;
import SecondaryStructureDesign.SecondaryStructureEditorPanel;

/** lets user choose to partner object, inserts corresponding link into controller */
public class SecondaryStructureEditorWizard implements GeneralWizard {
    
    private Object3DGraphController graphController;
    
    public static final String FRAME_TITLE = "Secondary Structure Editor";

    private ArrayList<ActionListener> actionListeners =
	new ArrayList<ActionListener>();

    boolean finished = false;
    
    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    public void cleanUp() {
	
    }

    public boolean isFinished() {
	return finished;
    }

    /** generates and inserts new Object3D according to user dialog.
     * blocks thread until dialog is finished */
    public void launchWizard(Object3DGraphController graphController,
			     Component parentFrame) {


	this.graphController = graphController;

	MutableSecondaryStructure structure = null;
	try {
	    structure =  graphController.generateSecondaryStructure();
	}
	catch (DuplicateNameException dne) {
	    structure = new SimpleMutableSecondaryStructure();
	}
	assert structure != null;
	final MutableSecondaryStructure ss = structure;
	final Component c = parentFrame;
	SwingUtilities.invokeLater(new Runnable() {
		public void run() {
		    
		    JFrame frame = null;
		    JDialog dialog = new JDialog(frame, FRAME_TITLE, true);
		    dialog.setLayout(new BorderLayout());
		    dialog.add(new SecondaryStructureEditorPanel(ss, dialog));
		    dialog.addWindowListener(new WindowAdapter() {
			    public void windowClosing(WindowEvent e) {
				finished = true;

			    }

			});

		    dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);


		    dialog.pack();
		    dialog.setLocationRelativeTo(c);
		    dialog.setVisible(true);
		    
		}
	    });
	
	
    
	
	try {
	    while(!finished)
		Thread.sleep(1000);
	    
	} catch (InterruptedException e) {
	    e.printStackTrace();
	    
	}
    }

    


}
