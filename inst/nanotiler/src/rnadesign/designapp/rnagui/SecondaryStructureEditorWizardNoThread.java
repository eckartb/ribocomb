package rnadesign.designapp.rnagui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import sequence.DuplicateNameException;
import rnadesign.rnacontrol.Object3DGraphController;
import rnasecondary.MutableSecondaryStructure;
import rnasecondary.SimpleMutableSecondaryStructure;
import commandtools.*;

import SecondaryStructureDesign.SecondaryStructureEditorPanel;

/** lets user choose to partner object, inserts corresponding link into controller */
public class SecondaryStructureEditorWizardNoThread implements GeneralWizard {
    
    private Object3DGraphController graphController;
    
    public static final String FRAME_TITLE = "Secondary Structure Editor";

    private MutableSecondaryStructure structure;

    private ArrayList<ActionListener> actionListeners =
	new ArrayList<ActionListener>();

    boolean finished = false;

    private CommandApplication app;

    public SecondaryStructureEditorWizardNoThread(CommandApplication app) {
	this.app = app;
    }
    
    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    public void cleanUp() {
	
    }

    public boolean isFinished() {
	return finished;
    }

    public void fireActionPerformed(ActionEvent e) {
	for(ActionListener l : actionListeners)
	    l.actionPerformed(e);
    }

    public MutableSecondaryStructure getSecondaryStructure() {

	return structure;
    }

    /** generates and inserts new Object3D according to user dialog.
     * blocks thread until dialog is finished */
    public void launchWizard(Object3DGraphController graphController,
			     Component parentFrame) {


	this.graphController = graphController;

	MutableSecondaryStructure structure = null;
	try {
	    structure =  graphController.generateSecondaryStructure();
	    System.out.println("Generating secondary structure");
	}
	catch (DuplicateNameException dne) {
	    structure = new SimpleMutableSecondaryStructure();
	}
	final MutableSecondaryStructure ss = structure;
	final Component c = parentFrame;
	JFrame frame = null;
	JDialog dialog = new JDialog(frame, FRAME_TITLE, true);
	dialog.setLayout(new BorderLayout());
	final SecondaryStructureEditorPanel ssep = new SecondaryStructureEditorPanel(ss, dialog, app);
	dialog.add(ssep);
	dialog.addWindowListener(new WindowAdapter() {
		public void windowClosed(WindowEvent e) {
		    if(finished)
			return;

		    SecondaryStructureEditorWizardNoThread.this.structure = ssep.getSecondaryStructure();

		    finished = true;
		    fireActionPerformed(new ActionEvent(SecondaryStructureEditorWizardNoThread.this, 0, "update nanotiler with secondary structure information"));
		}
		
	    });
	
	dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	
	
	dialog.pack();
	dialog.setLocationRelativeTo(c);
	System.out.println("Showing window");
	dialog.setVisible(true);
		    
    }

}
