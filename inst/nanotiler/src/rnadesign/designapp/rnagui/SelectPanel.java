package rnadesign.designapp.rnagui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;

import rnadesign.rnacontrol.Object3DController;
import tools3d.objects3d.Object3D;

public class SelectPanel extends JPanel {

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    private Object3DController graphController;

	/**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    public static class SelectPanelParameters {
	int height = 100;
	int width = 120;
	int buttonWidth = 60;
	int buttonHeight = 40;	
    }

    private void debugOutput() {
	Object3D selectObject = graphController.getSelectionCursor();
	if (selectObject != null) {
	    log.fine("Selection cursor: " + selectObject.isSelected() + " " + selectObject.infoString());
	}
	else {
	    log.fine("Selection Cursor is null!");
	}
	selectObject = graphController.getSelectionRoot();
	if (selectObject != null) {
	    log.fine("Selection root: " + selectObject.isSelected() + " " + selectObject.infoString());
	}
	else {
	    log.fine("Selection root is null!");
	}

    }


    private class DecSelectionListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    graphController.decSelectionCursor();
	    debugOutput();
	}
    }

    private class DeselectAllListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    graphController.deselectAll();
	    debugOutput();
	}
    }

    private class DeselectCurrentListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    graphController.deselectCurrent();
	    debugOutput();
	}
    }

    private class IncSelectionListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    graphController.incSelectionCursor();
	    debugOutput();
	}
    }

    private class MoveUpSelectionListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    graphController.moveUpSelectionCursor();
	    debugOutput();
	}
    }

    private class MoveDownSelectionListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    graphController.moveDownSelectionCursor();
	    debugOutput();
	}
    }

    private class ResetSelectionListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    graphController.resetSelectionCursor();
	    debugOutput();
	}
    }

    private class SelectCurrentListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    graphController.selectCurrent();
	    debugOutput();
	}
    }

    private class SelectAllListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    graphController.selectAll();
	    debugOutput();
	}
    }

    public SelectPanel(SelectPanelParameters params, Object3DController graphController) {
	this.graphController = graphController;
	setBackground(RnaGuiParameters.colorBackground);
	setPreferredSize(new Dimension(params.width, params.height));
	setBorder(BorderFactory.createLineBorder(Color.black));
	// setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
	setLayout(new GridLayout(3, 2));
	// JLabel label = new JLabel("Depth:");
	JButton button = new JButton("Up");
	button.setPreferredSize(new Dimension(params.buttonWidth, params.buttonHeight));
	button.addActionListener(new MoveUpSelectionListener());
	add(button);
	button = new JButton("Down");
	// button.addActionListener(new HierarchyDownListener());
	button.addActionListener(new MoveDownSelectionListener());
	button.setPreferredSize(new Dimension(params.buttonWidth, params.buttonHeight));
	add(button);
	button = new JButton("Next");
	button.setPreferredSize(new Dimension(params.buttonWidth, params.buttonHeight));
	button.addActionListener(new IncSelectionListener());
	// button.addActionListener(new HierarchyUpListener());
	add(button);
	button = new JButton("Prev");
	// button.addActionListener(new HierarchyDownListener());
	button.addActionListener(new DecSelectionListener());
	button.setPreferredSize(new Dimension(params.buttonWidth, params.buttonHeight));
	add(button);

	button = new JButton("Sel");
	button.setPreferredSize(new Dimension(params.buttonWidth, params.buttonHeight));
	button.addActionListener(new SelectCurrentListener());
	// button.addActionListener(new HierarchyUpListener());
	add(button);
	button = new JButton("Des");
	// button.addActionListener(new HierarchyDownListener());
	button.addActionListener(new DeselectCurrentListener());
	button.setPreferredSize(new Dimension(params.buttonWidth, params.buttonHeight));
	add(button);

    }
}
