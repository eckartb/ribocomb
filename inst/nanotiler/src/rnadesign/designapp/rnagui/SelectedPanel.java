package rnadesign.designapp.rnagui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import controltools.ModelChangeEvent;
import controltools.ModelChangeListener;
import rnadesign.rnacontrol.Object3DGraphController;

public class SelectedPanel extends JPanel implements ModelChangeListener {

    Font font = new Font("roman", Font.PLAIN, 6);
    SelectedPanelParameters params;
    JTextArea textArea;
    Object3DGraphController graphController;


    public static class SelectedPanelParameters {

	int height = 120;
	
	int width = 420;
	
	int nRows = 15;

	int nCols = 20;

	String title = "Selected Object Tree:";
    }

    public SelectedPanel(SelectedPanelParameters params, Object3DGraphController graphController) {
	super();
	setFont(font);
	setPreferredSize(new Dimension(params.width, params.height));
	setLayout(new BorderLayout());
	textArea = new JTextArea(params.nRows, params.nCols);
	textArea.setEditable(false);
	textArea.setLineWrap(true);
	add(textArea, BorderLayout.CENTER);
	this.graphController = graphController;
	this.graphController.addModelChangeListener(this);
    }

    public SelectedPanel(Object3DGraphController graphController) {
	this(new SelectedPanelParameters(), graphController);
    }

    // private class LocalModelChangeListener implements ModelChangeListener {
    public void modelChanged(ModelChangeEvent e) {
	repaint();
    }
    // }
    
    public void paint(Graphics g) {
	String text = graphController.getGraph().getSelectedGraphText();
	textArea.setText(text);
    }

}
