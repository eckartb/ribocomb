package rnadesign.designapp.rnagui;

import java.awt.Graphics;

import tools3d.Shape3D;

/** paints a single Shape3D 
 * this can be a base class to paint cubes, spheres, cylinders etc
 * @see Strategy pattern in Gamma et al: Design patterns
 */
public interface Shape3DPainter extends Painter {    

    /** returns names of Shape3D types that can be handled (like "sphere", "cone", empty string means all shapes can be handled */
    public String[] getHandledShapeNames();
    
    public void paint(Graphics g, Shape3D obj);

}
