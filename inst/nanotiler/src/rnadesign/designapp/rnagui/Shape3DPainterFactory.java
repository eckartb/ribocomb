package rnadesign.designapp.rnagui;

public interface Shape3DPainterFactory {
    
    Shape3DPainter createPainter(int styleId);

}
