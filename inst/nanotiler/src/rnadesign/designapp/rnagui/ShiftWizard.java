package rnadesign.designapp.rnagui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import commandtools.CommandApplication;
import commandtools.CommandException;
import rnadesign.designapp.AbstractDesigner;
import rnadesign.rnacontrol.Object3DController;
import rnadesign.rnacontrol.Object3DGraphController;
import tools3d.objects3d.Object3D;

/**
 * GUI Implementation of shift command.
 * Shifts selection, all, or new object x y z.
 *
 * @author Christine Viets
 */
public class ShiftWizard implements Wizard {

    public static Logger log = Logger.getLogger("NanoTiler_debug");
    private static final int COL_SIZE_SELECTION = 20; //TODO: size?
    private static final int COL_SIZE_DOUBLE = 8; //TODO: make public? One constants class?

    private boolean finished = false;
    private JFrame frame = null;
    private List<ActionListener> actionListeners = new ArrayList<ActionListener>();
    public static final String FRAME_TITLE = "Shift Wizard";
    private JTextField selectionField;
    private JTextField xField;
    private JTextField yField;
    private JTextField zField;
    private CommandApplication application;
    private List<String> shiftHistory; //commandHistory
    private List<String> selectHistory;

    /**
     * Constructor.
     *
     * @param application NanoTiler
     */
    public ShiftWizard(CommandApplication application) {
	assert application != null;
	this.application = application;
    }

    private class ShiftButtonListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    String selectCommand = "select " + selectionField.getText().trim();
	    String command = "shift " + xField.getText().trim() + " " + yField.getText().trim() + " " + zField.getText().trim();
	    try {
		application.runScriptLine(selectCommand);
		selectHistory.add(selectCommand);
		application.runScriptLine(command);
		shiftHistory.add(command);
	    }
	    catch (CommandException ce) {
		JOptionPane.showMessageDialog(frame, "Error executing command: " +
					      selectCommand + " or " + command + " : " + ce.getMessage());
	    }
	}
    }

    /**
     * Called when "Done" button is pressed.
     * Window disappears.
     *
     * @author Christine Viets
     */
    private class DoneListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    frame.setVisible(false);
	    frame = null;
	    finished = true;
	}
    }

    /**
     * Called when "Redo" button is pressed.
     * Redoes undo command.
     *
     * @author Christine Viets
     */
    private class RedoListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    if (shiftHistory.size() <= 0) {
		JOptionPane.showMessageDialog(frame, "Cannot redo. No action performed.");
		return;
	    }
	    String redoCommand = shiftHistory.get(shiftHistory.size() - 1);
	    if (redoCommand.startsWith("unshift")) {
		redoCommand = "shift " + redoCommand.substring(("unshift ").length());
		String selectCommand = selectHistory.get(selectHistory.size() - 2);
		try {
		    application.runScriptLine(redoCommand);
		    shiftHistory.add(redoCommand);
		    application.runScriptLine(selectCommand);
		    selectHistory.add(selectCommand);
		}
		catch(CommandException ce) {
		    JOptionPane.showMessageDialog(frame, "Error executing undo command: " + redoCommand + " or " + selectCommand + " : " + ce.getMessage());
		}
	    }
	    else {
		JOptionPane.showMessageDialog(frame, "Cannot redo shift. Try Undo.");
		return;
	    }
	}
    }
    
    /**
     * Called when "Undo" button is pressed.
     * Undoes last command.
     *
     * @author Christine Viets
     */
    private class UndoListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    assert shiftHistory.size() == selectHistory.size();
	    int commandIndex = (shiftHistory.size() - 1);
	    if (commandIndex < 0) {
		JOptionPane.showMessageDialog(frame, "There are no commands to undo!");
		return;
	    }
	    String commandToUndo = shiftHistory.get(commandIndex);
	    try {
		while (!commandToUndo.startsWith("shift")) {
		    commandIndex--;
		    commandToUndo = shiftHistory.get(commandIndex);
		}
	    }
	    catch(IndexOutOfBoundsException ie) {
		log.severe("Command history did not include shift! " + ie.getMessage());
	    }
	    if (commandIndex != (shiftHistory.size() - 1)) {
		commandIndex = (commandIndex - ((shiftHistory.size() - 1) - commandIndex));
		if (commandIndex < 0) {
		    JOptionPane.showMessageDialog(frame, "No more commands.");
		    return;
		}
		commandToUndo = shiftHistory.get(commandIndex);
		if (!commandToUndo.startsWith("shift")) {
		    JOptionPane.showMessageDialog(frame, "Unshift cannot be undone. Try Redo.");
		}
	    }
	    String shiftCommand = "unshift " + commandToUndo.substring(("shift ").length());
	    String selectCommand = selectHistory.get(commandIndex);
	    String previousSelectCommand = selectCommand;
	    if (!((commandIndex - 1) < 0)) { 
		previousSelectCommand = selectHistory.get(commandIndex - 1);
	    }
	    assert selectCommand.startsWith("select ");
	    assert previousSelectCommand.startsWith("select ");
	    if (!selectCommand.equals(previousSelectCommand)) {
		selectCommand = previousSelectCommand;
		selectionField.setText(selectCommand.substring(("select ").length()));
	    }
	    try {
		application.runScriptLine(shiftCommand);
		shiftHistory.add(shiftCommand);
		application.runScriptLine(selectCommand);
		selectHistory.add(selectCommand);
	    }
	    catch(CommandException ce) {
		JOptionPane.showMessageDialog(frame, "Error executing undo command: " + shiftCommand + " or " + selectCommand + " : " + ce.getMessage());
	    }
	}
    }

    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    public boolean isFinished() {
	return finished;
    }

    /** Creates a window. */
    public void launchWizard(Component parentFrame) {
	shiftHistory = new ArrayList<String>();
	selectHistory = new ArrayList<String>();
	frame = new JFrame(FRAME_TITLE);
	addComponents(frame);
	frame.pack();
	frame.setVisible(true);
    }

    /** Adds the components to the window. */
    public void addComponents(JFrame _frame) {
	Container f = _frame.getContentPane();
	f.setLayout(new BorderLayout());
	JPanel top = new JPanel();
	top.setLayout(new FlowLayout());
	selectionField = new JTextField(getSelectionString(), COL_SIZE_SELECTION);
	top.add(new JLabel("Shift:"));
	top.add(selectionField);
	JPanel shiftPanel = new JPanel();
	shiftPanel.setLayout(new FlowLayout());
	xField = new JTextField("0.0", COL_SIZE_DOUBLE);
	yField = new JTextField("0.0", COL_SIZE_DOUBLE);
	zField = new JTextField("0.0", COL_SIZE_DOUBLE);
	shiftPanel.add(new JLabel("Shift: x:"));
	shiftPanel.add(xField);
	shiftPanel.add(new JLabel(" y:"));
	shiftPanel.add(yField);
	shiftPanel.add(new JLabel(" z:"));
	shiftPanel.add(zField);
	JPanel bottomPanel = new JPanel();
	JButton button = new JButton("Shift");
	button.addActionListener(new ShiftButtonListener());
	bottomPanel.add(button);
	button = new JButton("Undo");
	button.addActionListener(new UndoListener());
	bottomPanel.add(button);
	button = new JButton("Redo");
	button.addActionListener(new RedoListener());
	bottomPanel.add(button);
	button = new JButton("Close");
	button.addActionListener(new DoneListener());
	bottomPanel.add(button);
	f.add(top, BorderLayout.NORTH);
	f.add(shiftPanel, BorderLayout.CENTER);
	f.add(bottomPanel, BorderLayout.SOUTH);
    }

    /**
     * Returns String representation of what will be shifted by default:
     * the current selection's full name or
     * "all" if nothing is selected.
     */
    private String getSelectionString() {
	Object3DGraphController graphController = ((AbstractDesigner)application).getGraphController();
	Object3DController gControl = graphController.getGraph();
	Object3D selection = gControl.getSelectionRoot();
	if (selection == null) {
	    return "all";
	}
	String selectionName = selection.getName();
	Object3D curr = selection;
	while (curr.getParent() != null) {
	    curr = curr.getParent();
	    selectionName = curr.getName() + "." + selectionName;
	}
	return selectionName;
    }

}
