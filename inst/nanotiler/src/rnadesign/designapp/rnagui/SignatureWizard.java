package rnadesign.designapp.rnagui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

import commandtools.CommandApplication;
import commandtools.CommandException;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import rnadesign.designapp.AbstractDesigner;
import rnadesign.rnacontrol.Object3DController;
import rnadesign.rnacontrol.Object3DGraphController;
import tools3d.objects3d.Object3D;
import java.util.logging.Logger;

/**
 * GUI Implementation of signature command.
 * Displays the signature of a certain root object.
 *
 * @author Brett Boyle
 */
public class SignatureWizard implements GeneralWizard {

    private static final int COL_SIZE_NAME = 30; //TODO: size?
    private static final String FRAME_TITLE = "Signature Wizard";

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    private boolean finished = false;
    private JFrame frame = null;
    private List<ActionListener> actionListeners = new ArrayList<ActionListener>();
    private CommandApplication application;
    private Object3DGraphController graphController;
    private JTextField objectField;
    private JTextArea resultsField;
    private Vector<String> tree;
    private String[] allowedNames = new String[]{"Object3D","RnaStrand","Atom3D","Nucleotide3D"};
    private String[] forbiddenNames = null;
    private JPanel treePanel;
    private JButton treeButton;
    private JList treeList;
    private JLabel label;
    private JScrollPane treeScroll;

    /**
     * Constructor.
     *
     * @param application NanoTiler
     */ 
    public SignatureWizard(CommandApplication application) {
	assert application != null;
	this.application = application;
    }

    /**
     * Called when "Cancel" button is pressed.
     * Window disappears.
     *
     */
    private class CancelListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    frame.setVisible(false);
	    frame = null;
	    finished = true;
	}
    }

    /**
     * Called when "Get Signature" button is pressed.
     * Generates the signature for the selected root.
     *
     */
    private class SignatureListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    //String command = "signature " + object1Field.getText().trim();
	    try {
	        String signatureResult = graphController.generateSignature(objectField.getText());
		resultsField.setText(signatureResult);
	    }
	    catch(Object3DGraphControllerException ce) {
		JOptionPane.showMessageDialog(frame, ce + ": " + ce.getMessage());
	    }
	}
    }

    /**
     * Adds an action listener.
     *
     * @param listener The ActionListener to add.
     */
    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    /** Adds the components to the window. */
    public void addComponents() {
	Container f = frame.getContentPane();
	f.setLayout(new BorderLayout());

	JPanel instructionsPanel = new JPanel();
	instructionsPanel.setLayout(new BoxLayout(instructionsPanel,BoxLayout.Y_AXIS));
	label = new JLabel("             Specify the parent object in the field below.");
	instructionsPanel.add(label);

	JPanel labelPanel = new JPanel();
	labelPanel.setLayout(new BoxLayout(labelPanel, BoxLayout.Y_AXIS));
	label = new JLabel("Object: ");
	labelPanel.add(label);
	label = new JLabel("Results: ");
        labelPanel.add(label);

	JPanel fieldPanel = new JPanel();
	fieldPanel.setLayout(new BoxLayout(fieldPanel, BoxLayout.Y_AXIS));
	objectField = new JTextField(getSelectionText(), COL_SIZE_NAME);
	fieldPanel.add(objectField);
	resultsField =  new JTextArea(20,20);
	resultsField.setLineWrap(true);
	JScrollPane resultsScroller = new JScrollPane(resultsField);
	resultsScroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
	fieldPanel.add(resultsScroller);

	JPanel treePanel = new TreePanel(objectField);

	JPanel buttonPanel = new JPanel(new FlowLayout());
	JButton button = new JButton("Close");
	button.addActionListener(new CancelListener());
	buttonPanel.add(button);
	button = new JButton("Generate Signature");
	button.addActionListener(new SignatureListener());
	buttonPanel.add(button);

	f.add(instructionsPanel, BorderLayout.NORTH);
	f.add(labelPanel, BorderLayout.WEST);
	f.add(fieldPanel, BorderLayout.CENTER);
	f.add(treePanel, BorderLayout.EAST);
	f.add(buttonPanel, BorderLayout.SOUTH);

    }

    /** Returns a String representation of the current selection. */
    private String getSelectionText() {
	Object3DGraphController graphController = ((AbstractDesigner)application).getGraphController();
	Object3DController gControl = graphController.getGraph();
	Object3D selection = gControl.getSelectionRoot();
	if (selection == null) {
	    return "";
	}
	String selectionName = selection.getName();
	Object3D curr = selection;
	while (curr.getParent() != null) {
	    curr = curr.getParent();
	    selectionName = curr.getName() + "." + selectionName;
	}
	return selectionName;
    }

    public boolean isFinished() {
	return finished;
    }

    /** Creates a window. */
    public void launchWizard(Object3DGraphController controller,Component parentFrame) {
	frame = new JFrame(FRAME_TITLE);
	if(controller==null){
	    log.info("GraphController received by lauchWizard is null!");
	}
	this.graphController = controller;
	addComponents();
	frame.pack();
	frame.setResizable(false);
	frame.setVisible(true);
    }
    /**
     * Called when a new value is selected in the list.
     * Allows user to pick an object from a list that
     * he/she wants selected.
     */
    public class SelectionListener implements ListSelectionListener {
	JTextField field;
	public SelectionListener(JTextField field){
	    this.field = field;
	}

	public void valueChanged(ListSelectionEvent e) {
	    if (e.getValueIsAdjusting() == true) {
		String text = tree.get(e.getFirstIndex());
		text = text.substring(0, text.indexOf(" "));
		if (text.equals(field.getText())) {
		    text = tree.get(e.getLastIndex());
		    text = text.substring(0, text.indexOf(" "));
		}
		field.setText(text);
	    }
	}
    }
    /*
     * generates a panel containing the current tree in
     * list format.
     */
    private class TreePanel extends JPanel{
	public TreePanel(JTextField field){
		Object3DGraphController controller = ((AbstractDesigner)application).getGraphController();
		tree = controller.getGraph().getTree(allowedNames, forbiddenNames);
		this.setLayout(new BorderLayout());
		JPanel labelPanel = new JPanel();
		labelPanel.setLayout(new BoxLayout(labelPanel, BoxLayout.Y_AXIS));
		labelPanel.add(new JLabel("Tree:"));
		this.add(labelPanel);
	        
		treePanel = new JPanel();
		treeList = new JList(tree);
		treeList.addListSelectionListener(new SelectionListener(field));
		treeScroll = new JScrollPane(treeList);
		treeScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		treeScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		treeScroll.setPreferredSize(new Dimension(300,300));
		treePanel.add(treeScroll);
		this.add(treePanel, BorderLayout.SOUTH);
	}
    }

}
