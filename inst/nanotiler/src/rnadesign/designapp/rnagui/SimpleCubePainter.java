package rnadesign.designapp.rnagui;

import java.awt.Graphics;

import tools3d.Shape3D;

/** paints a single Shape3D (in this case a cube)
 * @see Strategy pattern in Gamma et al: Design patterns
 */
public abstract class SimpleCubePainter extends AbstractShape3DPainter {

    public static final String[] handledShapeNames = {"cube"};
    
    /** @TODO implement painting of cube! */
    public void paint(Graphics g, Shape3D obj) {
    }

    public String[] getHandledShapeNames() { return handledShapeNames; }

}
