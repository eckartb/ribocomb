package rnadesign.designapp.rnagui;

import java.awt.Graphics;

import tools3d.Shape3D;

/** paints a single Shape3D (in this case a cylinder)
 * @see Strategy pattern in Gamma et al: Design patterns
 */
public abstract class SimpleCylinderPainter extends AbstractShape3DPainter {

    public static final String[] handledShapeNames = {"cylinder"};
    
    /** @TODO implement painting of cylinder! */
    public void paint(Graphics g, Shape3D obj) {

    }

    public String[] getHandledShapeNames() { return handledShapeNames; }

}
