package rnadesign.designapp.rnagui;

import java.util.ArrayList;
import java.util.List;

/** This painter works as a container of specialized painters who can handle certain types of shapes 
 *  @TODO : not yet implemented
 * @see Composite pattern in Gamma et al: Design patterns
 */
public abstract class SimpleFlexibleShape3DPainter implements FlexibleShape3DPainter {    

    List<Shape3DPainter> painters = new ArrayList<Shape3DPainter>();

    /** adds a painter that can handle certain types of shapes */
    public void addPainter(Shape3DPainter painter) {
	painters.add(painter);
    }

}
