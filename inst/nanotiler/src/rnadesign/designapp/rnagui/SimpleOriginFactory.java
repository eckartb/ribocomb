package rnadesign.designapp.rnagui;

import java.awt.Color;

import tools3d.Vector3D;
import tools3d.objects3d.Primitive3D;
import tools3d.objects3d.SimplePrimitive;

public class SimpleOriginFactory implements OriginFactory {

    public Primitive3D[] createOrigin() {
	Primitive3D[] result = new Primitive3D[3];
	SimplePrimitive p = new SimplePrimitive();
	p.add(new Vector3D(0.0, 0.0, 0.0));
	p.add(new Vector3D(10.0, 0.0, 0.0));
	p.setColor(Color.RED);
	result[0] = p;
	p = new SimplePrimitive();
	p.add(new Vector3D(0.0, 0.0, 0.0));
	p.add(new Vector3D(0.0, 10.0, 0.0));
	p.setColor(Color.RED);
	result[1] = p;
	p = new SimplePrimitive();
	p.add(new Vector3D(0.0, 0.0, 0.0));
	p.add(new Vector3D(0.0, 0.0, 10.0));
	p.setColor(Color.RED);
	result[2] = p;
	return result;
    }

}
