package rnadesign.designapp.rnagui;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.logging.Logger;

import rnadesign.rnacontrol.CameraController;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.SimpleCameraController;
import tools3d.Positionable3D;
import tools3d.ZBuffer;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Primitive3D;

public class SimplePainter implements Object3DPainter {

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    int[] xTmpArray;
    int[] yTmpArray;

    boolean originMode = true;

    Primitive3D[] originPrimitives;

    CameraController cameraController = new SimpleCameraController();

    private RnaGuiParameters params;
    private ZBuffer zBuf = new ZBuffer();

    SimplePainter(RnaGuiParameters p) {
	params = p;
	OriginFactory originFactory = new SimpleOriginFactory();
	originPrimitives = originFactory.createOrigin();
    }

    public void copy(Object3DPainter other) {
	if (other instanceof SimplePainter) {
	    SimplePainter c = (SimplePainter)other;
	    cameraController.copy(c.cameraController);
	    params.copy(c.params);
	}
    }

    public void drawObject3D(Graphics2D g2, Object3D obj3d) {
	// log.fine("Called drawObject3D");
	if (obj3d == null) {
	    log.warning("Called drawObject3D with null object!");
	    return;
	}
	Point2D point = cameraController.project(obj3d.getPosition());
	double posX = point.getX();
	double posY = point.getY(); // use transformation here instead!
	g2.fill(new Rectangle2D.Double(posX, posY,
				       params.rectWidth, params.rectHeight));
    }
    
    public void drawPrimitive(Graphics2D g2, Primitive3D p3d) {
	if (p3d == null) {
	    log.warning("Called drawPrimitive with null object!");
	    return;
	}
	if (p3d.size() <= 0) {
	    // do nothing
	}
	g2.setXORMode(p3d.getColor());
	if (p3d.size() == 1) { // draw point
	    Point2D point = cameraController.project(p3d.getPoint(0));
	    double posX = point.getX();
	    double posY = point.getY(); // use transformation here instead!
	    g2.fill(new Ellipse2D.Double(posX, posY,
					 params.circleRad, params.circleRad));	
	}
	else if (p3d.size() == 2) { // draw line
	    Point2D pointA = cameraController.project(p3d.getPoint(0));
	    Point2D pointB = cameraController.project(p3d.getPoint(1));
	    g2.draw(new Line2D.Double(pointA, pointB));	
	}
	else {
	    if ((xTmpArray == null) || (xTmpArray.length != p3d.size())) {
		xTmpArray = new int[p3d.size()];
		yTmpArray = new int[p3d.size()];
	    }
	    for (int i = 0; i < p3d.size(); ++i) {
		Point2D point = cameraController.project(p3d.getPoint(i));
		xTmpArray[i] = (int)point.getX();
		xTmpArray[i] = (int)point.getY();
	    }
	    g2.fill(new Polygon(xTmpArray, yTmpArray, xTmpArray.length));
	}
    }
    
    /** returns objecto that projects 3d data to 2d screen */
    public CameraController getCameraController() { return cameraController; }
    
    /** central paint method (not automatically used) */
    public void paint(Graphics g, Object3DGraphController graphController) {
	if (!graphController.isValid()) {
	    return; // do nothing, no objects defined
	}
    	
	Graphics2D g2 = (Graphics2D)g;
	graphController.getGraph().resetIterator();
	int counter = 0;
	zBuf.reset(); // empty z-Buffer
	zBuf.setDirection(cameraController.getViewDirection()); // sets direction for z-buffer
// 	while (graphController.hasMorePrimitives()) {
// 	    Primitive3D obj3d = graphController.getNextPrimitive();
// 	    if (obj3d == null) {
// 		break;
// 	    }	
// 	    if (counter > 10000) {
// 		System.exit(1);
// 	    }
// 	    // zBuf.add((Primitive3D)(obj3d.clone()));
// 	    zBuf.add(obj3d);
// 	    // drawPrimitive(g2, obj3d);
// 	}
	if (originMode) {
	    // add primitives that correspond to a coordinate system at the origin
	    for (int i = 0; i < originPrimitives.length; ++i) {
		zBuf.add(originPrimitives[i]);
	    }
	}
	zBuf.sort(); // highest z-values last : we need reverse!
	// log.fine("Zbuffer has now: " + zBuf.size() + " elements.");
	for (int i = zBuf.size()-1; i >= 0; --i) {
	    drawPositionable(g2, zBuf.get(i));
	}
	//			while (zBuf.hasNext()) {
	//		    	drawPrimitive(g2, zBuf.next());
	//    		}
    }

    public void drawPositionable(Graphics2D g2, Positionable3D p3d) {
	if (p3d instanceof Primitive3D) {
	    drawPrimitive(g2, (Primitive3D)p3d);
	}
	else if (p3d instanceof Object3D) {
	    drawObject3D(g2, (Object3D)p3d);
	}
    }

    public void setCameraController(CameraController p) {
	cameraController = p;
    }


    public void removeForbidden(String s) {
	assert false;
    }

    public void addForbidden(String s) {
	assert false;
    }

    public void clearForbidden() {
	assert false;
    }

}
