package rnadesign.designapp.rnagui;

import tools3d.objects3d.Object3D;

/** decides on a painter for a given Object3D */
public class SimpleSinglePainterChooser implements Object3DSinglePainterChooser {
    Object3DSinglePainter defaultPainter = new DefaultSinglePainter();

    public Object3DSinglePainter choosePainter(Object3D obj) {
	return defaultPainter;
    }

}
