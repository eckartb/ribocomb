package rnadesign.designapp.rnagui;

import java.awt.Graphics;

import tools3d.Shape3D;

/** paints a single Shape3D (in this case a Sphere)
 * @see Strategy pattern in Gamma et al: Design patterns
 */
public class SimpleSpherePainter extends AbstractShape3DPainter {
    
    public static final String[] handledShapeNames = {"sphere"};

    /** @TODO implement painting of sphere! */
    public void paint(Graphics g, Shape3D obj) {

    }

    public String[] getHandledShapeNames() { return handledShapeNames; }

}
