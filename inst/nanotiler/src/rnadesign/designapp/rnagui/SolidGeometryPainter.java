package rnadesign.designapp.rnagui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.util.Properties;

import rnadesign.rnacontrol.CameraController;
import tools3d.Character3D;
import tools3d.Drawable;
import tools3d.Edge3D;
import tools3d.Face3D;
import tools3d.GeometryColorModel;
import tools3d.Point3D;
import tools3d.Vector3D;
import tools3d.Matrix3DTools;

/** paints solid 3D atoms */
public class SolidGeometryPainter extends AbstractGeometryPainter {

    public static final int FACE_POINT_MAX = 3;
	
    private double pointRadius = 2.0;


    private static final double MAX_RADIUS = 30;

    final static BasicStroke stroke = new BasicStroke(2.0f);

    private final static Vector3D zeroPos = new Vector3D(0.0, 0.0, 0.0);

    private int[] xTmp = new int[FACE_POINT_MAX];
    private int[] yTmp = new int[FACE_POINT_MAX];

    
//     public SolidGeometryPainter() {
// 	addForbidden("Atom3D"); // just for debugging!
// 	// addForbidden("Nucleotide3D");
//     }
    
    /** paints character for a single point */
    public void paintCharacter(Graphics g, Character3D point) {
	if (!paintCharacterMode) {
	    return; // do not draw characters if not set
	}
	Graphics2D g2 = (Graphics2D)g;
	Color saveColor = g2.getColor();
	char[] characters = new char[1];
	characters[0] = point.getCharacter();
	if (point.getAppearance() != null) {
	    g2.setColor(point.getAppearance().getColor());
	}
	else {
	    g2.setColor(getDefaultPaintColor(point));
	}
	Vector3D pos3d = point.getPosition();
	CameraController camera = getCameraController();
	Point2D point2d = camera.project(point.getPosition());
	g.drawChars(characters, 0, 1, 
		     (int)point2d.getX(), (int)point2d.getY());
	g.setColor(saveColor);
    }

    /** paints single point */
    public void paintPoint(Graphics g, Point3D point) {
	Character3D char3D;
	Graphics2D g2 = (Graphics2D)g;

	CameraController camera = getCameraController();
	

	Vector3D pos3d = point.getPosition();


	Point2D point2d = camera.project(point.getPosition());
	if ((point2d.getX() < 0) || (point2d.getY() < 0)) {
	    return;
	}

	Color saveColor = g2.getColor();
	Color color;
	if (point.getAppearance() != null) {
	    color = point.getAppearance().getColor();
	}
	else {
	    color = getDefaultPaintColor(point);
	}
	g2.setColor(color);
	g2.setStroke(stroke);
	double radius = point.getRadius();
	
	
	if (radius < this.pointRadius) {
	    radius = this.pointRadius;
	}

	

	radius *= camera.getZoom();
     
	double tmpRadius = radius;
	double halfRadius = 0.5*tmpRadius;
	if (radius > 3.0) {
	    g2.fill(new Ellipse2D.Double(point2d.getX() - halfRadius, point2d.getY() - halfRadius,
					 radius, radius));
	}
	else { // shortcut for really small entities:
	    g2.drawRect((int)(point2d.getX()-halfRadius),(int)(point2d.getY()-halfRadius),(int)(2*radius),(int)(2*radius));
	    g2.setColor(saveColor);
	    return;
	}
	
	int numCircles = 5;
 	Color currentPaint = color.brighter();
	double inset = radius / numCircles;
	

	for(int i = 1; i < numCircles; ++i) {

	    tmpRadius = radius - (i * inset);
	    if (tmpRadius < 3.0) {
		break;
	    }

	    int redInc = (255 - currentPaint.getRed()) / numCircles;
	    int greenInc = (255 - currentPaint.getGreen()) / numCircles;
	    int blueInc = (255 - currentPaint.getBlue()) / numCircles;
	    
	    halfRadius = tmpRadius / 2;
	    currentPaint = new Color(currentPaint.getRed() + redInc, currentPaint.getGreen() + greenInc, currentPaint.getBlue() + blueInc);
	    g2.setColor(currentPaint);

	    /** makes ellipse solid color and paints smaller circles on it to create 3D circle. */
	    g2.fill(new Ellipse2D.Double(point2d.getX() - halfRadius, point2d.getY() - halfRadius, tmpRadius, tmpRadius));
	    
	}
	
	g2.setColor(saveColor);
    }

    /** paints single edge */
    public void paintEdge(Graphics g, Edge3D edge) {

	if(!getPaintLinks())
	    return; 

	Graphics2D g2 = (Graphics2D)g;
	Color saveColor = g2.getColor();
	Properties properties = edge.getProperties();
	String physicalKey = "watson_crick";
	String chemicalKey = "backbone";
	CameraController camera = getCameraController();
	if (edge.getAppearance() != null) {
	    g2.setColor(edge.getAppearance().getColor());
	}
	/** if watson crick or backbone, then paint edge. */
	else if ((properties != null) 
		 && (physicalKey.equals(properties.getProperty("interaction_type")))
		 || (chemicalKey.equals(properties.getProperty("interaction_type")))) {
	    g2.setColor(geometryColorModel.computeColor(getAmbiente(),
							edge.getAppearance(),
							edge.getProperties(),
							null,
							camera.getCamera()));
	    
	}
 	else {
 	    g2.setColor(getDefaultPaintColor(edge));
 	}
	
	Vector3D first3d = edge.getFirst();
	Vector3D second3d = edge.getSecond();
	Point2D first2d = camera.project(first3d);
	Point2D second2d = camera.project(second3d);
	g2.drawLine((int)first2d.getX(), (int)first2d.getY(), 
		   (int)second2d.getX(), (int)second2d.getY());
	g2.setColor(saveColor);
    }

    /** paints single face */
    public void paintFace(Graphics g, Face3D face) {
	int nn = face.size();
	if (nn > FACE_POINT_MAX) {
	    nn = FACE_POINT_MAX;
	}
	if (nn < 3) {
	    return;
	}

	CameraController camera = getCameraController();
	GeometryColorModel colorModel = getGeometryColorModel();
	for (int i = 0; i < nn; ++i) {
	    Vector3D v = face.getPoint(i);
	    Point2D p2d = camera.project(v);
	    xTmp[i] = (int)(p2d.getX());
	    yTmp[i] = (int)(p2d.getY());
	}

	Polygon polygon = new Polygon(xTmp, yTmp, nn);
	Graphics2D g2 = (Graphics2D)g;
	Color saveColor = g2.getColor();
	Vector3D normal = face.getNormal();
	Color color = colorModel.computeColor(getAmbiente(),
					      face.getAppearance(),
					      face.getProperties(),
					      normal,
					      camera.getCamera());
	g2.setColor(color);
	g2.fill(polygon);
	g2.setColor(saveColor);
    }


    /** Returns true if object will be painted.
     * TODO can be optimized here, not all objects have to be painted
     */
    public boolean isPaintable(Drawable p) {
	return super.isPaintable(p);
    }
    
}
