package rnadesign.designapp.rnagui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.SequenceController;
import sequence.Sequence;

/** lets user choose to partner object, inserts corresponding link into controller */
public class StemWizard implements GeneralWizard {

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    public static final int FIELD_INT_LENGTH = 5;
    public static final int FIELD_STRING_LENGTH = 15;
    private boolean finished = false;
    private JFrame frame = null;
    private JList leftList;
    private JList rightList;
    private JTextField startPosField;
    private JTextField stopPosField;
    private JTextField lengthField;
    private JTextField stemNameField;
    private JCheckBox interpolateCheckBox;
    private JCheckBox setEqualCheckBox;
    private java.util.List<ActionListener> actionListeners = new ArrayList<ActionListener>();
    private String[] sequenceNames;
    private Sequence[] sequences;
    private int sequenceSelection1 = 0; // by default first sequence is chosen
    private int sequenceSelection2 = 0;
    private int start = 0;
    private int stop = 0;
    private int length = 0;
    private boolean interpolateMode = true;
    private boolean setEqualMode = true;
    private String stemName;
    private Object3DGraphController graphController;
    public static final String FRAME_TITLE = "Generate Stem Wizard";
    
    private class DoneListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    readOutValues();
	    if (isValid()) {
		// graphController.addStem(start, stop, length, sequenceSelection1, sequenceSelection2);
		// graphController.addStem3D(start, stop, length, sequenceSelection1, sequenceSelection2, setEqualMode, interpolateMode, stemName); //method not implemented
		cleanUp();
	    }
	    else {
		JOptionPane.showMessageDialog(frame, "The current stem parameters are not ok!");
	    }
	}
    }

    private class CancelListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    cleanUp();
	}
    }

    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    public void cleanUp() {
	if (frame != null) {
	    frame.setVisible(false);
	    frame = null;
	}
	finished = true;
    }

    public boolean isFinished() {
	return finished;
    }

    /** generates and inserts new Object3D according to user dialog */
    public void launchWizard(Object3DGraphController graphController,
			     Component parentFrame) {
	if (graphController == null) {
	    log.info("GraphController received by launchWizard is null!");
	}
	this.graphController = graphController;
	// obtains current set of sequences:
	boolean result = checkValues();
	if (result) {
	    frame = new JFrame(FRAME_TITLE);
	    addComponents(frame);
	    frame.pack();
	    frame.setVisible(true);
	}
	else {
	    JOptionPane.showMessageDialog(parentFrame, "No sequences defined so far!");
	}
    }

    public void addComponents(JFrame _frame) {
	Container f = _frame.getContentPane();
	f.setLayout(new BorderLayout());
	JPanel center = new JPanel();
	center.setLayout(new FlowLayout());
	center.setPreferredSize(new Dimension(300, 200));
	if (graphController == null) {
	    log.info("GraphController is null before generating left and right tree panels!");
	}
	SequenceController sequences = graphController.getSequences();
	String[] names = new String[sequences.getSequenceCount()];
	for (int i = 0; i < names.length; ++i) {
	    names[i] = sequences.getSequenceName(i);
	}
	leftList = new JList(names); // Object3DTreePanel(graphController);
	rightList = new JList(names); // Object3DTreePanel(graphController);
	JLabel leftLabel = new JLabel("Match: ");
	JLabel rightLabel = new JLabel("With: ");
	center.add(leftLabel);
	center.add(leftList);
	center.add(rightLabel);
	center.add(rightList);
	f.add(center, BorderLayout.CENTER);
	JPanel middlePanel = new JPanel();
	JPanel lowerMiddlePanel = new JPanel();
	startPosField = new JTextField("" + (start+1),FIELD_INT_LENGTH);
	stopPosField = new JTextField("" + (stop+1),FIELD_INT_LENGTH);
	lengthField = new JTextField("" + length,FIELD_INT_LENGTH);
	stemNameField = new JTextField("", FIELD_STRING_LENGTH);
	setEqualCheckBox = new JCheckBox("Set residue positions:", true);
	interpolateCheckBox = new JCheckBox("Interpolate residue positions:", true);
	middlePanel.setPreferredSize(new Dimension(300, 200));
	middlePanel.setLayout(new FlowLayout());
	middlePanel.add(new JLabel("Start: "));
	middlePanel.add(startPosField);
	middlePanel.add(new JLabel("Stop: "));
	middlePanel.add(stopPosField);
	middlePanel.add(new JLabel("Length: "));
	middlePanel.add(lengthField);
	lowerMiddlePanel.add(setEqualCheckBox);
	lowerMiddlePanel.add(interpolateCheckBox);
	JPanel bottomPanel = new JPanel();
	bottomPanel.setPreferredSize(new Dimension(300, 200));
	JButton button = new JButton("Cancel");
	button.addActionListener(new CancelListener());
	bottomPanel.add(button);
  	button = new JButton("Done");
	button.addActionListener(new DoneListener());
	bottomPanel.add(button);
	JPanel southPanel = new JPanel();
	southPanel.setLayout(new BoxLayout(southPanel, BoxLayout.Y_AXIS));
	southPanel.add(middlePanel);
	southPanel.add(lowerMiddlePanel);
	southPanel.add(new JLabel("Name:"));
	southPanel.add(stemNameField);
	southPanel.add(bottomPanel);
	
	f.add(southPanel, BorderLayout.SOUTH);
    }

    /** check if values obtained from controller are ok */
    private boolean checkValues() {
	int numSeq = graphController.getSequences().getSequenceCount();
	if (numSeq == 0) { //CV - should you be able to do it if there is only one sequence? If not add || numSeq==1
	    return false;
	}
	return true;
    }

    /** reads values from mask */
    private void readOutValues() {
	sequenceSelection1 = leftList.getSelectedIndex();
	sequenceSelection2 = rightList.getSelectedIndex();
	start = Integer.parseInt(startPosField.getText().trim())-1;
	stop = Integer.parseInt(stopPosField.getText().trim())-1;
	length = Integer.parseInt(lengthField.getText().trim());
	interpolateMode = interpolateCheckBox.isSelected();
	setEqualMode = setEqualCheckBox.isSelected();
	stemName = stemNameField.getText().trim();
    }

    /** returns true if currently set values could correspond to a valid stem */
    private boolean isValid() {
	readOutValues();
	return ((start >= 0) && (stop >= 0) && (length > 0) 
		&& (stemName != null) && (stemName.length() > 0) && (leftList.getSelectedIndex() != rightList.getSelectedIndex()) );
    }

}
