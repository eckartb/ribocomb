package rnadesign.designapp.rnagui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import commandtools.CommandApplication;
import commandtools.CommandException;
import rnadesign.designapp.AbstractDesigner;
import rnadesign.rnacontrol.Object3DController;
import rnadesign.rnacontrol.Object3DGraphController;
import tools3d.objects3d.Object3D;

/**
 * GUI Implementation of synth bject command.
 * Opens window with text boxes to enter the
 * name of the object to be selected and its.
 * location.
 *
 * @author Brett Boyle
 */
public class SynthWizard implements Wizard {

    // public static Logger log = Logger.getLogger("NanoTiler_debug");
    public static final int COL_SIZE_SELECTION = 20; //TODO: size?
    public static final int COL_SIZE_COORDINATE = 6;

    private boolean finished = false;
    private JFrame frame = null;
    private Container container = null;
    private List<ActionListener> actionListeners = new ArrayList<ActionListener>();
    public static final String FRAME_TITLE = "Synth Wizard";
    private JTextField selectionField;
    private JTextField nameField;
    private JTextField xField,yField,zField;
    private JRadioButton simpleButton;
    private CommandApplication application;
    private Vector<String> tree;
    private String[] allowedNames;
    private String[] forbiddenNames;
    private JPanel treePanel;
    private JButton treeButton;
    private JList treeList;
    private JScrollPane treeScroll;
    private JLabel label;

    /**
     * Constructor.
     *
     * @param application NanoTiler
     */
    public SynthWizard(CommandApplication application) {
	assert application != null;
	this.application = application;
    }

    /**
     * Called when "Done" button is pressed.
     * Makes window disappear.
     *
     */
    private class CancelListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    frame.setVisible(false);
	    frame = null;
	    finished = true;
	}
    }

    /**
     * Called when a checkbox is checked or unchecked.
     * Removes or adds the checked object from the list.
     */
    public class CheckListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    JCheckBox checkBox = (JCheckBox)e.getSource();
	    String checkedType = checkBox.getText();
	    if (!checkBox.isSelected()) {
		removeAllowedName(checkedType);
		addForbiddenName(checkedType);
		setAllowedNames();
	    }
	    else {
		addAllowedName(checkedType);
		removeForbiddenName(checkedType);
		setAllowedNames(checkedType);
	    }
	    Object3DGraphController controller = ((AbstractDesigner)application).getGraphController();
	    tree = controller.getGraph().getTree(allowedNames, forbiddenNames);
	    treeList = new JList(tree);
	    treeList.addListSelectionListener(new SelectionListener());
	    treePanel.remove(treeScroll);
	    treeScroll = new JScrollPane(treeList);
	    treePanel.add(treeScroll);
	    frame.pack();
	}
    }

    /**
     * Called when "Synth Object" button is pressed.
     * User-entered data is put into the program.
     */
    private class DoneListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    String command = "synth simple  " + nameField.getText().trim() + " " + selectionField.getText().trim() + " " + xField.getText().trim() + " " + yField.getText().trim() + " " + zField.getText().trim();
	    try {
		application.runScriptLine(command);
	    }
	    catch (CommandException ce) {
		JOptionPane.showMessageDialog(frame, "Error executing command: " +
					      command + " : " + ce.getMessage());
	    }
	}
    }

    /**
     * Called when a new value is selected in the list.
     * Allows user to pick an object from a list that
     * he/she wants selected.
     *
     */
    public class SelectionListener implements ListSelectionListener {
	public void valueChanged(ListSelectionEvent e) {
	    if (e.getValueIsAdjusting() == true) {
		String text = tree.get(e.getFirstIndex());
		text = text.substring(0, text.indexOf(" "));
		if (text.equals(selectionField.getText())) {
		    text = tree.get(e.getLastIndex());
		    text = text.substring(0, text.indexOf(" "));
		}
		selectionField.setText(text);
	    }
	}
    }

    /**
     * Called when "View tree"/"Hide tree" button is pressed.
     * Displays or hides the list of objects in NanoTiler.
     *
     */
    private class TreeListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    if (treeButton.getText().equals("View tree")) {
		treeButton.setText("Hide tree");
		Object3DGraphController controller = ((AbstractDesigner)application).getGraphController();
		tree = controller.getGraph().getTree(allowedNames, forbiddenNames);
		setAllowedNames();
		Container j = new Container();
		j.setLayout(new BorderLayout());
		JPanel allowedPanel = new JPanel();
		allowedPanel.setLayout(new BoxLayout(allowedPanel, BoxLayout.Y_AXIS));
		allowedPanel.add(new JLabel("Allowed types:"));
		if (allowedNames != null) {
		    for (int i = 0; i < allowedNames.length; i++) {
			JCheckBox checkBox = new JCheckBox(allowedNames[i], true);
			checkBox.addActionListener(new CheckListener());
			allowedPanel.add(checkBox);
		    }
		    j.add(allowedPanel, BorderLayout.NORTH);
		}
		treePanel = new JPanel();
		treeList = new JList(tree);
		treeList.addListSelectionListener(new SelectionListener());
		treeScroll = new JScrollPane(treeList);
		treePanel.add(treeScroll);
		j.add(treePanel, BorderLayout.SOUTH);
		frame.getContentPane().add(j);
		frame.pack();
	    }
	    else {
		frame.getContentPane().remove(1);
		treeButton.setText("View tree");
		frame.pack();
	    }
	}
    }

    /**
     * Adds an action listener.
     *
     * @param listener The ActionListener to be added.
     */
    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    /**
     * Adds a type of object to the allowed names array.
     *
     * @param type The type of object to be added to the allowed names array.
     */
    private void addAllowedName(String type) {
	if (allowedNames == null) {
	    allowedNames = new String[1];
	    allowedNames[0] = type;
	}
	else {
	    boolean contains = false;
	    for (int i = 0; i < allowedNames.length; i++) {
		if (allowedNames[i] != null) {
		    if (allowedNames[i].equals(type)) {
			contains = true;
			i = allowedNames.length; //break?
		    }
		}
	    }
	    if (!contains) {
		String[] tmp = allowedNames;
		allowedNames = new String[allowedNames.length + 1];
		for (int i = 0; i < tmp.length; i++) {
		    allowedNames[i] = tmp[i];
		}
		allowedNames[allowedNames.length - 1] = type;
	    }
	}
    }

    /** Adds the components to the window. */
    public void addComponents() {
	Container f = new Container();
	f.setLayout(new BorderLayout());

	JPanel top = new JPanel();
	treeButton = new JButton("View tree");
	treeButton.addActionListener(new TreeListener());
	top.add(treeButton);

	JPanel left = new JPanel();
	left.setLayout(new BoxLayout(left,BoxLayout.Y_AXIS));
	label = new JLabel("Parent Object: ");
	left.add(label);
	label = new JLabel("Object Type: ");
	left.add(label);
	label = new JLabel(" ");
	left.add(label);
	label = new JLabel("Object Name: ");
	left.add(label);

	JPanel center = new JPanel();
	center.setLayout(new BoxLayout(center,BoxLayout.Y_AXIS));
	selectionField = new JTextField(getSelectionText(),COL_SIZE_SELECTION);
	center.add(selectionField, BorderLayout.CENTER);
	simpleButton = new JRadioButton("Simple",true);
	center.add(simpleButton);
	nameField = new JTextField(COL_SIZE_SELECTION);
	center.add(nameField);

	JPanel bottomPanel = new JPanel();
	bottomPanel.setLayout(new BorderLayout());
	 JPanel southBottomPanel = new JPanel();
	 JButton button = new JButton("Close");
	 button.addActionListener(new CancelListener());
	 southBottomPanel.add(button);
	 button = new JButton("Synth Object");
	 button.addActionListener(new DoneListener());
	 southBottomPanel.add(button);
	bottomPanel.add(southBottomPanel,BorderLayout.SOUTH);
	 JPanel northBottomPanel = new JPanel();
	 label = new JLabel("x: ");
	 northBottomPanel.add(label);
	 xField = new JTextField(COL_SIZE_COORDINATE);
	 xField.setText("0.0");
	 northBottomPanel.add(xField);
	 label = new JLabel("y: ");
	 northBottomPanel.add(label);
	 yField = new JTextField(COL_SIZE_COORDINATE);
	 yField.setText("0.0");
	 northBottomPanel.add(yField);
	 label = new JLabel("z: ");
	 northBottomPanel.add(label);
	 zField = new JTextField(COL_SIZE_COORDINATE);
	 zField.setText("0.0");
	 northBottomPanel.add(zField);
	bottomPanel.add(northBottomPanel,BorderLayout.NORTH);

	f.add(left,BorderLayout.WEST);
	f.add(top, BorderLayout.NORTH);
	f.add(center, BorderLayout.CENTER);
	f.add(bottomPanel, BorderLayout.SOUTH);
	container.add(f);
    }
    /** get the selection text from the tree list */
    private String getSelectionText() {
	Object3DGraphController graphController = ((AbstractDesigner)application).getGraphController();
	Object3DController gControl = graphController.getGraph();
	Object3D selection = gControl.getSelectionRoot();
	if (selection == null) {
	    return "";
	}
	String selectionName = selection.getName();
	Object3D curr = selection;
	while (curr.getParent() != null) {
	    curr = curr.getParent();
	    selectionName = curr.getName() + "." + selectionName;
	}
	return selectionName;
    }

    /**
     * Adds a type of object to the forbidden names array.
     *
     * @param type The type of object to be added to the forbidden names array.
     */
    private void addForbiddenName(String type) {
	if (forbiddenNames == null) {
	    forbiddenNames = new String[1];
	    forbiddenNames[0] = type;
	}
	else {
	    boolean contains = false;
	    for (int i = 0; i < forbiddenNames.length; i++) {
		if (forbiddenNames[i] != null) {
		    if (forbiddenNames[i].equals(type)) {
			contains = true;
			i = forbiddenNames.length; //break?
		    }
		}
	    }
	    if (!contains) {
		String[] tmp = forbiddenNames;
		forbiddenNames = new String[forbiddenNames.length + 1];
		for (int i = 0; i < tmp.length; i++) {
		    forbiddenNames[i] = tmp[i];
		}
		forbiddenNames[forbiddenNames.length - 1] = type;
	    }
	}
    }

    public boolean isFinished() {
	return finished;
    }

    /** Creates a window. */
    public void launchWizard(Component parentFrame) {
	frame = new JFrame(FRAME_TITLE);
	container = frame.getContentPane();
	container.setLayout(new FlowLayout());
	addComponents();
	frame.pack();
	frame.setVisible(true);
    }

    /**
     * Removes a type of object from the allowed names array.
     *
     * @param type The type of object to be removed from the allowed names array.
     */
    private void removeAllowedName(String type) {
	if (allowedNames == null) {
	    allowedNames = new String[1];
	    allowedNames[0] = type;
	}
	else {
	    boolean contains = false;
	    for (int i = 0; i < allowedNames.length; i++) {
		if (allowedNames[i] != null) {
		    if (allowedNames[i].equals(type)) {
			contains = true;
			i = allowedNames.length; //break?
		    }
		}
	    }
	    if (contains) {
		String[] tmp = allowedNames;
		allowedNames = new String[allowedNames.length - 1];
		for (int i = 0; i < allowedNames.length; i++) {
		    if (tmp[i] != null) {
			if (!tmp[i].equals(type)) {
			    allowedNames[i] = tmp[i];
			}
		    }
		}
	    }
	}
    }

    /**
     * Removes a type of object from the forbidden names array.
     *
     * @param type The type of object to be removed from the forbidden names array.
     */
    private void removeForbiddenName(String type) {
	if (forbiddenNames == null) {
	    forbiddenNames = new String[1];
	    forbiddenNames[0] = type;
	}
	else {
	    boolean contains = false;
	    for (int i = 0; i < forbiddenNames.length; i++) {
		if (forbiddenNames[i] != null) {
		    if (forbiddenNames[i].equals(type)) {
			contains = true;
			i = forbiddenNames.length; //break?
		    }
		}
	    }
	    if (contains) {
		String[] tmp = forbiddenNames;
		forbiddenNames = new String[forbiddenNames.length - 1];
		for (int i = 0; i < forbiddenNames.length; i++) {
		    if (tmp[i] != null) {
			if (!tmp[i].equals(type)) {
			    forbiddenNames[i] = tmp[i];
			}
		    }
		}
	    }
	}
    }

    /**
     * Sets allowed names based on the types of object that
     * already exist in the tree.
     */
    private void setAllowedNames() {
	String type = tree.get(0);
	int spaceIndex = (type.indexOf(" ") + 1);
	spaceIndex = (type.indexOf(" ", spaceIndex) + 1);
	spaceIndex = (type.indexOf(" ", spaceIndex) + 1);
	type = type.substring(spaceIndex, type.indexOf(" ", spaceIndex));
	setAllowedNames(type);
    }
	
    /**
     * Allows user to set the allowed names when there are
     * no tree items that are of that type yet.
     *
     * @param allowedType A type of object that is allowed.
     */
    private void setAllowedNames(String allowedType) {
	String[] oldAllowedNames = allowedNames;
	Vector<String> allowed = new Vector<String>();
	allowed.add(allowedType);
	for (int i = 0; i < tree.size(); i++) {
	    String line = tree.get(i);
	    int spaceIndex = (line.indexOf(" ") + 1);
	    spaceIndex = (line.indexOf(" ", spaceIndex) + 1);
	    spaceIndex = (line.indexOf(" ", spaceIndex) + 1);
	    line = line.substring(spaceIndex, line.indexOf(" ", spaceIndex));
	    if (!allowed.contains(line) &&
		line != null &&
		line != "null") {
		allowed.add(line);
	    }
	}
	allowedNames = new String[allowed.size()];
	for (int i = 0; i < allowed.size(); i++) {
	    allowedNames[i] = allowed.get(i);
	}
	if (allowedNames != null &&
	    oldAllowedNames != null) {
	    if (allowedNames.length != oldAllowedNames.length) {
		for (int i = 0; i < oldAllowedNames.length; i++) {
		    if (oldAllowedNames[i] != null) {
			if (!allowed.contains(oldAllowedNames[i]) &&
			    oldAllowedNames[i] != allowedType &&
			    oldAllowedNames[i] != null &&
			    oldAllowedNames[i] != "null") {
			    addForbiddenName(oldAllowedNames[i]);
			}
		    }
		}
	    }
	}
    }
}
