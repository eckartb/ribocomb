package rnadesign.designapp.rnagui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;
import java.util.*;
import commandtools.*;
import controltools.*;

import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerConstants;

/** lets user choose to partner object, inserts correspnding link into controller */
public class ViewLinksWizard implements GeneralWizard, ModelChangeListener {

    public static final String FRAME_TITLE = "Links";

    private CommandApplication application;

    private JList list = new JList();

    private static int junction_suffix = 1;


    private ArrayList<ActionListener> actionListeners = new ArrayList<ActionListener>();
    private boolean finished = false;
    private Object3DGraphController graphController;
    private JFrame frame;

    public ViewLinksWizard(CommandApplication application) {
	this.application = application;
    }

    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    public boolean isFinished() {
	return finished;
    }

    /** generates and inserts new Object3D according to user dialog */
    public void launchWizard(Object3DGraphController graphController,
			     Component parentFrame) {

	this.graphController = graphController;

	frame = new JFrame(FRAME_TITLE);
	frame.addWindowListener(new WindowAdapter() {
		
		public void windowClosed(WindowEvent e) {
		    finished = true;
	       
		    //ViewLinksWizard.this.graphController.getLinks().removeModelChangeListener(ViewLinksWizard.this);
		}

	    });

	this.graphController.getLinks().addModelChangeListener(this);
	
	JPanel panel = new JPanel();

	list.setListData(getLinks());
	list.setAlignmentX(JComponent.CENTER_ALIGNMENT);
	panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
	panel.setPreferredSize(new Dimension(300,300));

	panel.add(new JScrollPane(list));
	panel.add(Box.createVerticalStrut(10));
	JButton close = new JButton("Close");
	close.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    frame.setVisible(false);
		    frame.dispose();
		}
	    });
	close.setAlignmentX(JComponent.CENTER_ALIGNMENT);
	panel.add(close);
	frame.add(panel);

	frame.pack();
	frame.setVisible(true);
	

    }
    public String[] getLinks(){
	String s = graphController.getLinks().toPrettyString();
	StringTokenizer tokenizer = new StringTokenizer(s, "\n");
	final String[] array = new String[tokenizer.countTokens()];
	for(int i = 0; i < array.length; ++i){
	     array[i] = tokenizer.nextToken();
	}
	return array;
    }


    public void modelChanged(ModelChangeEvent e) {
	String s = graphController.getLinks().toPrettyString();
	StringTokenizer tokenizer = new StringTokenizer(s, "\n");
	final String[] array = new String[tokenizer.countTokens()];
	for(int i = 0; i < array.length; ++i)
	    array[i] = tokenizer.nextToken();
	
	list.setModel(new AbstractListModel() {
		public int getSize() { return array.length; }
		public Object getElementAt(int index) { return array[index]; }

	    });

	list.revalidate();
	frame.repaint();

    }

}
