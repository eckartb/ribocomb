package rnadesign.designapp.rnagui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;
import java.util.*;
import commandtools.*;

import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerConstants;

/** lets user choose to partner object, inserts correspnding link into controller */
public class ViewTreeWizard implements GeneralWizard {

    public static final String FRAME_TITLE = "Tree";

    private CommandApplication application;
    private int defaultWidth = 400;
    private int defaultHeight = 300;

    private JList list = new JList();
    private JPanel queryPanel;

    private ArrayList<ActionListener> actionListeners = new ArrayList<ActionListener>();
    private boolean finished = false;
    private Object3DGraphController graphController;
    private JFrame frame;

    private JPanel simpleQueryPanel;
    private JPanel advancedQueryPanel;

    private ListWriter writer = new ListWriter();

    public ViewTreeWizard(CommandApplication application) {
	this.application = application;
    }

    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    public boolean isFinished() {
	return finished;
    }

    /** generates and inserts new Object3D according to user dialog */
    public void launchWizard(Object3DGraphController graphController,
			     Component parentFrame) {

	this.graphController = graphController;

	frame = new JFrame(FRAME_TITLE);
	frame.addWindowListener(new WindowAdapter() {
		
		public void windowClosed(WindowEvent e) {
		    finished = true;
		}

	    });

	setupQueryPanels();


        queryPanel = new JPanel();
	queryPanel.setLayout(new BoxLayout(queryPanel, BoxLayout.Y_AXIS));

	JPanel modePanel = new JPanel();
	modePanel.add(new JLabel("Mode:"));

	ButtonGroup modeButtons = new ButtonGroup();
	final JRadioButton simpleButton = new JRadioButton("Simple");
	simpleButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    installSimpleQueryPanel();
		}

	    });
	final JRadioButton advancedButton = new JRadioButton("Advanced");
	advancedButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    installAdvancedQueryPanel();

		}

	    });
	modeButtons.add(simpleButton);
	modeButtons.add(advancedButton);

	modeButtons.setSelected(simpleButton.getModel(), true);

	modePanel.add(simpleButton);
	modePanel.add(advancedButton);
	
	queryPanel.add(modePanel, 0);
	queryPanel.add(new JSeparator(SwingConstants.HORIZONTAL), 1);
	queryPanel.add(simpleQueryPanel, 2);
	queryPanel.add(new JSeparator(SwingConstants.HORIZONTAL), 3);
	JButton close = new JButton("Close");
	close.addActionListener(new ActionListener() {

		public void actionPerformed(ActionEvent e) {
		    frame.setVisible(false);
		    frame.dispose();
		}
	    });
	close.setAlignmentX(JComponent.CENTER_ALIGNMENT);
	queryPanel.add(Box.createVerticalStrut(5), 4);

	queryPanel.add(close, 5);

	JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, queryPanel, new JScrollPane(list));
	
	frame.add(splitPane);

	frame.pack();
	frame.setVisible(true);
    }

    private void installSimpleQueryPanel() {
	queryPanel.remove(2);
	queryPanel.add(simpleQueryPanel, 2);
	queryPanel.revalidate();
	frame.pack();
	frame.repaint();

    }


    private void installAdvancedQueryPanel() {
	queryPanel.remove(2);
	queryPanel.add(advancedQueryPanel, 2);
	queryPanel.revalidate();
	frame.pack();
	frame.repaint();


    }

    private void setupQueryPanels() {
	simpleQueryPanel = new JPanel();
	simpleQueryPanel.setPreferredSize(new Dimension(defaultWidth, defaultHeight));
	simpleQueryPanel.setLayout(new BoxLayout(simpleQueryPanel, BoxLayout.Y_AXIS));

	ButtonGroup group = new ButtonGroup();
	
	final JRadioButton all = new JRadioButton("All");
	final JRadioButton strands = new JRadioButton("Strands");
	final JRadioButton atoms = new JRadioButton("Atoms");
	final JRadioButton helixends = new JRadioButton("Helix Ends");
	final JRadioButton residues = new JRadioButton("Residues");
	final JRadioButton junctions = new JRadioButton("Junctions");
	final JRadioButton kissingLoops = new JRadioButton("Kissing Loops");

	ActionListener listener = new ActionListener() {

		public void actionPerformed(ActionEvent e) {
		    Set<String> allowedNames = new HashSet<String>();
		    if (e.getSource() == atoms) {
			allowedNames.add("Atom3D");
		    }
		    else if (e.getSource() == strands) {
			allowedNames.add("RnaStrand");
		    }
		    else if (e.getSource() == helixends) {
			allowedNames.add("BranchDescriptor3D");
		    }
		    else if (e.getSource() == residues) {
			allowedNames.add("Nucleotide3D");
		    }
		    else if (e.getSource() == junctions) {
			allowedNames.add("StrandJunction3D");
		    }
		    else if (e.getSource() == kissingLoops) {
			allowedNames.add("KissingLoop3D");
		    }

		    writer.clear();
		    PrintStream ps = new PrintStream(writer);
		    graphController.getGraph().printTree(ps, allowedNames, null);

		    list.setModel(new AbstractListModel() {
			    private String[] objects = writer.getLines();

			    public Object getElementAt(int index) {
				return objects[index];
			    }

			    public int getSize() {
				return objects.length;
			    }

			});

		    list.revalidate();

		}

	    };

	all.addActionListener(listener);
	strands.addActionListener(listener);
	atoms.addActionListener(listener);
	helixends.addActionListener(listener);
	residues.addActionListener(listener);
	junctions.addActionListener(listener);
	kissingLoops.addActionListener(listener);

	all.setAlignmentX(JComponent.LEFT_ALIGNMENT);
	strands.setAlignmentX(JComponent.LEFT_ALIGNMENT);
	atoms.setAlignmentX(JComponent.LEFT_ALIGNMENT);
	helixends.setAlignmentX(JComponent.LEFT_ALIGNMENT);
	residues.setAlignmentX(JComponent.LEFT_ALIGNMENT);
	junctions.setAlignmentX(JComponent.LEFT_ALIGNMENT);
	kissingLoops.setAlignmentX(JComponent.LEFT_ALIGNMENT);

	group.add(all);
	group.add(strands);
	group.add(atoms);
	group.add(helixends);
	group.add(residues);
	group.add(junctions);
	group.add(kissingLoops);
	
	simpleQueryPanel.add(all);
	simpleQueryPanel.add(strands);
	simpleQueryPanel.add(atoms);
	simpleQueryPanel.add(helixends);
	simpleQueryPanel.add(residues);
	simpleQueryPanel.add(junctions);
	simpleQueryPanel.add(kissingLoops);

	advancedQueryPanel = new JPanel();
	advancedQueryPanel.setLayout(new BoxLayout(advancedQueryPanel, BoxLayout.Y_AXIS));

	advancedQueryPanel.add(new JLabel("Allowed:"));
	final JTextField allowedField = new JTextField(20);
	JPanel panel = new JPanel();
	panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
	panel.add(allowedField);
	advancedQueryPanel.add(panel);

	advancedQueryPanel.add(Box.createVerticalStrut(20));
	
	advancedQueryPanel.add(new JLabel("Forbidden:"));
	final JTextField forbiddenField = new JTextField(20);
	panel = new JPanel();
	panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
	panel.add(forbiddenField);
	advancedQueryPanel.add(panel);

	advancedQueryPanel.add(Box.createVerticalStrut(20));

	advancedQueryPanel.add(new JLabel("Name:"));
	final JTextField nameField = new JTextField(20);
	panel = new JPanel();
	panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
	panel.add(nameField);
	advancedQueryPanel.add(panel);

	JButton advancedSubmit = new JButton("Submit");

	listener = new ActionListener() {

		public void actionPerformed(ActionEvent e) {
		    String name = null;
		    Set<String> allowedArray = new HashSet<String>();
		    Set<String> forbiddenArray = new HashSet<String>();
		    if (!allowedField.getText().equals("")) {
			String[] allowedArrayRaw = allowedField.getText().trim().split(",");
			for(int i = 0; i < allowedArrayRaw.length; ++i) {
			    allowedArray.add(allowedArrayRaw[i]);
			}
			allowedField.setText("");
		    }
		    else if (!forbiddenField.getText().equals("")) {
			String[] forbiddenArrayRaw = forbiddenField.getText().trim().split(",");
			for(int i = 0; i < forbiddenArrayRaw.length; ++i) {
			    forbiddenArray.add(forbiddenArrayRaw[i]);
			}
			forbiddenField.setText("");
		    }
		    else if (!nameField.getText().equals("")) {
			name = nameField.getText();
			nameField.setText("");
		    }


		    writer.clear();
		    PrintStream ps = new PrintStream(writer);
		    if(name == null)
			graphController.getGraph().printTree(ps, allowedArray, forbiddenArray);
		    else
			graphController.getGraph().printTree(ps, name, allowedArray, forbiddenArray);

		    list.setModel(new AbstractListModel() {
			    private String[] objects = writer.getLines();

			    public Object getElementAt(int index) {
				return objects[index];
			    }

			    public int getSize() {
				return objects.length;
			    }

			});

		    list.revalidate();

		}

	    };

	advancedSubmit.addActionListener(listener);
	advancedSubmit.setAlignmentX(JComponent.LEFT_ALIGNMENT);
	
	advancedQueryPanel.add(Box.createVerticalStrut(20));
	advancedQueryPanel.add(advancedSubmit);
	advancedQueryPanel.add(Box.createVerticalStrut(5));

	
    }


    private class ListWriter extends OutputStream {
	private StringBuffer buffer = new StringBuffer();

	public void write(int b) {
	    buffer.append((char) b);
	}

	public void clear() {
	    buffer  = new StringBuffer();

	}

	public String getString() {
	    return buffer.toString();
	}

	public String[] getLines() {
	    StringTokenizer tokenizer = new StringTokenizer(getString(), "\n");
	    ArrayList<String> strings = new ArrayList<String>();
	    while(tokenizer.hasMoreTokens()) {
		strings.add(tokenizer.nextToken());
	    }

	    String[] ret = new String[strings.size()];
	    strings.toArray(ret);
	    return ret;

	}

    }

}
