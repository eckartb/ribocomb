package rnadesign.designapp.rnagui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BoxLayout;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextArea;

import commandtools.CommandApplication;
import commandtools.CommandException;
import rnadesign.designapp.AbstractDesigner;
import rnadesign.rnacontrol.Object3DController;
import rnadesign.rnacontrol.Object3DGraphController;
import tools3d.objects3d.Object3D;

/**
 * 
 */
public class WelcomeWindowWizard implements GeneralWizard {

    private static final int COL_SIZE_NAME = 20; //TODO: size?
    private static final String FRAME_TITLE = "NanoTiler Welcome and Basic Navigation Screen";

    private boolean finished = false;
    private JFrame frame = null;
    private List<ActionListener> actionListeners = new ArrayList<ActionListener>();
    private CommandApplication application;
    private Object3DGraphController controller;
    private String nanotilerHome = System.getenv("NANOTILER_HOME");
    public  final String TUTORIAL_HOME = "file://"+nanotilerHome+"/doc/NanoTilerUserManual.html";
    private final String NEWLINE = "\n";

    //demoScripts[] represents the scripts file names which are being added to the window
    private String[] demoScripts = new String[]{"grow.script", "optbasepairs.script", "mutate.script", "all"};
    /*the demoButtonNames[] parallel the demoScripts[]. When adding to 
     *demoScripts[] a name also needs to be added to demoButtonNames[]
     */
    private String[] demoButtonNames = new String[]{"Grow", "Optimize Basepair Constraints", "Mutate", "Run all demos" };
    private final Dimension buttonSize = new Dimension(250,25);

    private final int ROWS = 13;
    private final int COLS = 40;
    private String descriptionText = "Demo Scripts:"+NEWLINE+"  Each script demonstrates a functionality in NanoTiler. More demos are available using File->Demos and the script command \"demo\"."+NEWLINE+NEWLINE+"User Manual:"+NEWLINE+"  The user manual provides users with information about how to install, run and use the NanoTiler program." + NEWLINE;
   /**
     * Constructor.
     *
     * @param application NanoTiler
     */
    public WelcomeWindowWizard(CommandApplication application) {
	assert application != null;
	this.application = application;
    }

    /**
     * Called when "Cancel" button is pressed.
     * Window disappears.
     *
     */
    private class CancelListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    if (frame != null) {
		frame.setVisible(false);
		frame = null;
	    }
	    finished = true;
	}
    }
    /** 
     * called when a demo script button is pressed.
     *
     * script is launched 
     */
    private class ScriptListener implements ActionListener{
	String fileName;
	public ScriptListener(String fileName){
	    this.fileName = fileName;
	}
	public void actionPerformed(ActionEvent e){
	        frame.setVisible(false);
		boolean ok = true;
		if (!controller.getGraph().isEmpty()) {
		    String msg = "Are you sure you want to start a demo and discard all current data?";
		    String title = "Clear Button Dialog";
		    int choice = JOptionPane.showConfirmDialog(null,
							       msg, title, JOptionPane.OK_CANCEL_OPTION);
		    if (choice == 0) {
			ok = true;
		    }
		    else {
			ok = false;
		    }
		}
		if (ok) {
		    try{
			application.runScriptLine("clear all");
			String command = "";
			if ("all".equals(fileName)) {
			    command = "demo all";
			}
			else {
			    String path = nanotilerHome + "/demo/scripts/" + fileName;
			    command = "source " + path;
			}
			application.runScriptLine(command);
		    }
		    catch(CommandException ce){
			JOptionPane.showMessageDialog(frame, ce.getMessage());
		    }
		}
		frame.setVisible(true);
	}
    }

    private class TutorialListener implements ActionListener{
	public void actionPerformed(ActionEvent e){
		try{
		    new Browser(TUTORIAL_HOME);
		}
		catch(Exception ex){
		    JOptionPane.showMessageDialog(frame, ex.getMessage());
		}
	}
    }

    private final class CommandHelpListener implements ActionListener{
	public void actionPerformed(ActionEvent e){
	    GeneralWizard wizard = new HelpWizard(application);
	    wizard.launchWizard(controller,frame);
	}
    }

    private final class GraphicsHelpListener implements ActionListener{
	public void actionPerformed(ActionEvent e){
	    GeneralWizard wizard = new GraphicsManualWizard(application);
	    wizard.launchWizard(controller,frame);
	}
    }

    private final class LaunchListener implements ActionListener{
	public void actionPerformed(ActionEvent e){
	    GeneralWizard wizard = new WelcomeWindowWizard(application);
	    wizard.launchWizard(controller,frame);
	}
    }

    /**
     * Adds an action listener.
     *
     * @param listener The ActionListener to add.
     */
    public void addActionListener(ActionListener listener) {
	actionListeners.add(listener);
    }

    /** Adds the components to the window. */
    public void addComponents() {
	Container f = frame.getContentPane();
	f.setLayout(new BorderLayout());

	JPanel demoPanel = new JPanel();
	demoPanel.setLayout(new BoxLayout(demoPanel,BoxLayout.Y_AXIS));
	JLabel label = new JLabel("Demos:");
	demoPanel.add(label);
	for(int i=0;i<demoScripts.length;i++){
	    JButton demoTempButton = new JButton(demoButtonNames[i]);
	    demoTempButton.addActionListener(new ScriptListener(demoScripts[i]));
	    demoTempButton.setPreferredSize(buttonSize);
	    demoTempButton.setMinimumSize(buttonSize);
	    demoTempButton.setMaximumSize(buttonSize);
	    demoPanel.add(demoTempButton);
	}

	JPanel helpPanel = new JPanel();
	helpPanel.setLayout(new BoxLayout(helpPanel,BoxLayout.Y_AXIS));
	label = new JLabel("Help:");
	helpPanel.add(label);
	JButton tutorialButton = new JButton("User Manual");
	tutorialButton.addActionListener(new TutorialListener());
	tutorialButton.setPreferredSize(buttonSize);
	tutorialButton.setMinimumSize(buttonSize);
	tutorialButton.setMaximumSize(buttonSize);
	helpPanel.add(tutorialButton);
	JButton commandHelpButton = new JButton("Command Help");
	commandHelpButton.addActionListener(new CommandHelpListener());
	commandHelpButton.setPreferredSize(buttonSize);
	commandHelpButton.setMinimumSize(buttonSize);
	commandHelpButton.setMaximumSize(buttonSize);
	helpPanel.add(commandHelpButton);
	JButton graphicsHelpButton = new JButton("Graphics Help (OpenGL mode)");
	graphicsHelpButton.addActionListener(new GraphicsHelpListener());
	graphicsHelpButton.setPreferredSize(buttonSize);
	graphicsHelpButton.setMinimumSize(buttonSize);
	graphicsHelpButton.setMaximumSize(buttonSize);
	helpPanel.add(graphicsHelpButton);

	JPanel bottomPanel = new JPanel();
	JButton nanotilerButton = new JButton("Close");
	nanotilerButton.addActionListener(new CancelListener());
	bottomPanel.add(Box.createHorizontalGlue());
	bottomPanel.add(nanotilerButton);

	JPanel descriptionPanel = new JPanel();
	JTextArea descriptionArea = new JTextArea(descriptionText,ROWS,COLS);
	descriptionArea.setLineWrap(true);
	descriptionArea.setEditable(false);
	descriptionPanel.add(descriptionArea);

	f.add(helpPanel,BorderLayout.WEST);
	f.add(demoPanel,BorderLayout.CENTER);
	f.add(descriptionPanel,BorderLayout.EAST);
	f.add(bottomPanel,BorderLayout.SOUTH);
    }


    public boolean isFinished() {
	return finished;
    }

    /** Creates a window. */
    public void launchWizard(Object3DGraphController controller,Component parentFrame) {
	frame = new JFrame(FRAME_TITLE);
	this.controller = controller;
	addComponents();
	frame.pack();
	frame.setVisible(true);
    }

}
