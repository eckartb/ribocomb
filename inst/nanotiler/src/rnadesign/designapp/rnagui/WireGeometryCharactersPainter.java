package rnadesign.designapp.rnagui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;

import rnadesign.rnacontrol.CameraController;
import tools3d.Character3D;
import tools3d.Drawable;
import tools3d.Edge3D;
import tools3d.Face3D;
import tools3d.Point3D;
import tools3d.Vector3D;

/** exactly same as WireGeometryPainter except no characters are painted */
public class WireGeometryCharactersPainter extends AbstractGeometryPainter {

    private double pointRadius = 2;

    /** paints single point */
    public void paintPoint(Graphics g, Point3D point) {
	Graphics2D g2 = (Graphics2D)g;
	Color saveColor = g2.getColor();
	if (point.getAppearance() != null) {
	    g2.setColor(point.getAppearance().getColor());
	}
	else {
	    g.setColor(getDefaultPaintColor(point));
	}

	Vector3D pos3d = point.getPosition();
	CameraController camera = getCameraController();
	Point2D point2d = camera.project(pos3d);
	double radius = point.getRadius();
	if (radius < this.pointRadius) {
	    radius = this.pointRadius;
	}
	radius *= camera.getZoom();
	/** draws ellipse without filling in. */
 	g2.draw(new Ellipse2D.Double(point2d.getX(), point2d.getY(),
 				     radius, radius));	
	g.setColor(saveColor);
    }
    
    /** does not paint characters */
    public void paintCharacter(Graphics g, Character3D point) {
 	Graphics2D g2 = (Graphics2D)g;
 	Color saveColor = g2.getColor();
 	char[] characters = new char[1];
 	characters[0] = point.getCharacter();
 	if (point.getAppearance() != null) {
 	    g2.setColor(point.getAppearance().getColor());
 	}
 	else {
 	    g.setColor(getDefaultPaintColor(point));
 	}
	
 	Vector3D pos3d = point.getPosition();
 	CameraController camera = getCameraController();
 	Point2D point2d = camera.project(point.getPosition());
//  	g.drawChars(characters, 0, 1, 
// 		    (int)point2d.getX(), (int)point2d.getY());
 	g.setColor(saveColor);
    }

    /** paints single edge */
    public void paintEdge(Graphics g, Edge3D edge) {
	Graphics2D g2 = (Graphics2D)g;
	Color saveColor = g2.getColor();
	if (edge.getAppearance() != null) {
	    g2.setColor(edge.getAppearance().getColor());
	}
	else {
	    g.setColor(getDefaultPaintColor(edge));
	}
	Vector3D first3d = edge.getFirst();
	Vector3D second3d = edge.getSecond();
	CameraController camera = getCameraController();
	Point2D first2d = camera.project(first3d);
	Point2D second2d = camera.project(second3d);
   	g.drawLine((int)first2d.getX(), (int)first2d.getY(), 
   		   (int)second2d.getX(), (int)second2d.getY());
	g.setColor(saveColor);
    }

    /** paints single face */
    public void paintFace(Graphics g, Face3D face) {
	// TODO
    }
    
    /** Returns true if object will be painted. */
    public boolean isPaintable(Drawable p) {
	return true;
    }
}
