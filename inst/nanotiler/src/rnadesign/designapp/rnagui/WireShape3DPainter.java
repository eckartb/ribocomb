package rnadesign.designapp.rnagui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;

import tools3d.Appearance;
import tools3d.Cube;
import tools3d.Cylinder;
import tools3d.Shape3D;
import tools3d.Sphere;
import tools3d.Vector3D;

public class WireShape3DPainter extends AbstractShape3DPainter {

    String[] handledShapeNames = {"Sphere", "Cube", "Cylinder"};
    Vector3D radVec = new Vector3D(0.0, 0.0, 0.0);

    /** returns names of Shape3D types that can be handled (like "sphere", "cone", empty string means all shapes can be handled */
    public String[] getHandledShapeNames() {
	return handledShapeNames;
    }

    private Color getPaintColor(Shape3D shape) {
	Appearance appearance = shape.getAppearance();
	Color color = null;
	Color paintColor = null;
	if (appearance != null) {
	    color = appearance.getColor();
	}
	if (color != null) {
	    paintColor = color;
	}
	else {
	    paintColor = getDefaultPaintColor();
	}
	return paintColor;
    }

    private void paintSphere(Graphics g, Sphere obj) {
	Graphics2D g2 = (Graphics2D)g;
	double radius3D = obj.getBoundingRadius();
	Vector3D pos = obj.getPosition();
        double radius2D = radius3D * cameraController.getZoom();
	Point2D point2D = cameraController.project(pos);
	Color paintColor = getPaintColor(obj);
	g2.setXORMode(paintColor);
	g2.fill(new Ellipse2D.Double(point2D.getX(), point2D.getY(),
				     radius2D, radius2D));	
    }

    private void paintCylinder(Graphics g, Cylinder obj) {
	Graphics2D g2 = (Graphics2D)g;
	double radius3D = obj.getBoundingRadius();
	Vector3D pos = obj.getPosition();
	double radius2D = radius3D * cameraController.getZoom();
	Point2D point2D = cameraController.project(pos);
	Color paintColor = getPaintColor(obj);
	g2.setXORMode(paintColor);
	g2.fill(new Ellipse2D.Double(point2D.getX(), point2D.getY(),
				     radius2D, radius2D));	
    }

    private void paintCube(Graphics g, Cube obj) {
	Graphics2D g2 = (Graphics2D)g;
	double radius3D = obj.getBoundingRadius();
	Vector3D pos = obj.getPosition();
	double radius2D = radius3D * cameraController.getZoom();
	Point2D point2D = cameraController.project(pos);
	Color paintColor = getPaintColor(obj);
	g2.setXORMode(paintColor);
	g2.fill(new Ellipse2D.Double(point2D.getX(), point2D.getY(),
				     radius2D, radius2D));	
    }

    public void paint(Graphics g, Shape3D obj) {	
	if (obj instanceof Sphere) {
	    paintSphere(g, (Sphere)obj);
	}
	else if (obj instanceof Cylinder) {
	    paintCylinder(g, (Cylinder)obj);
	}
	else if (obj instanceof Cube) {
	    paintCube(g, (Cube)obj);
	}	
    }

}
