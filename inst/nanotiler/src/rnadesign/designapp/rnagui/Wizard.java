package rnadesign.designapp.rnagui;

import java.awt.Component;
import java.awt.event.ActionListener;
import javax.swing.JFrame;

import rnadesign.rnacontrol.Object3DGraphController;

public interface Wizard {

    public void addActionListener(ActionListener listener);

    public boolean isFinished();

    /** generates and inserts new Object3D according to user dialog */
    public void launchWizard(Component parentFrame);

}
