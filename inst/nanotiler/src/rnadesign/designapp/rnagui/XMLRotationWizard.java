package rnadesign.designapp.rnagui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

import guitools.BeanEditor;
import guitools.XMLBeanEditor;
import rnadesign.rnacontrol.Object3DGraphController;
import tools3d.RotationDescriptor;
import tools3d.SimpleRotationDescriptor;
import tools3d.Vector3D;

/** generate and insert new RNA strand according to user dialog */
public class XMLRotationWizard implements RotationWizard {

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    Object3DGraphController controller;
    BeanEditor beanEditor = new XMLBeanEditor();
    // Set actionListeners = new HashSet();
    Component frame;

    /** if "Done" was pressed, insert Object3D into graph! */
    private class DoneActionListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    log.fine("Calling DoneActionListener in RnaStrandWizard!");
	    if (isFinished()) {
		log.fine("Calling DoneActionListener in RnaStrandWizard: text editor was finished!");
		RotationDescriptor rotator = getRotator();
		if ((rotator != null) && rotator.isValid()) {
		    controller.getGraph().rotateSelected(rotator);
		    log.fine("Here was adding of graph!");
		}
		else {
		    log.info("Rotator was null or invalid!");
		}
		log.fine("Rotating object tree!");
	    }
	    else {
		log.fine("Calling DoneActionListener in XMLRotationWizard: text editor was NOT finished!");
	    }
	    if (frame != null) {
		frame.repaint();
	    }
	    log.fine("RnaStrandWizard finished!");
	}
    }

    public void addActionListener(ActionListener listener) {
	beanEditor.addActionListener(listener);
    }

    /** generates and inserts new Object3D according to user dialog */
    public void launchWizard(Object3DGraphController controller,
			     Component frame) {
	if (controller.getGraph().getSelectionRoot() == null) {
	    return; // nothing was selected!
	}
	this.controller = controller;
	this.frame = frame;
	RotationDescriptor rotator = new SimpleRotationDescriptor();
	init(rotator);
	beanEditor.addActionListener(new DoneActionListener());
	beanEditor.launchEdit(rotator, frame);
    }

    public RotationDescriptor getRotator() {
	RotationDescriptor rotator = (SimpleRotationDescriptor)(beanEditor.getObject()); 
	if (rotator != null) {
	    rotator.setAngle(rotator.getAngle() * RnaGuiConstants.DEG2RAD);
	}
	return rotator;
    }

    /** initialize RNA strand: better move this code to model package or at least controller */
    private void init(RotationDescriptor rotator) {
	rotator.setCenter(new Vector3D(0.0001, 0.0001, 0.0001));
	rotator.setAxis(new Vector3D(1.0, 0.0, 0.0));
	rotator.setAngle(90.0); // for user use degree!
    }

    public boolean isFinished() { return beanEditor.isFinished(); }

}
