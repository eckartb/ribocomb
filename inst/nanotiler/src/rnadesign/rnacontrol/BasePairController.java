package rnadesign.rnacontrol;

import java.io.InputStream;
import java.io.IOException;

public interface BasePairController extends LinkController {
    
    void read(InputStream is) throws IOException;

    String toInfoString();

}
