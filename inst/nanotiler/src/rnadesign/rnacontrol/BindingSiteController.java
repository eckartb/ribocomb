package rnadesign.rnacontrol;

import controltools.ModelChangeListener;
import rnadesign.rnamodel.SequenceBindingSite;

public interface BindingSiteController extends ModelChangeListener {

    public void clear();

    public int getBindingSiteCount();

    public void addBindingSite(SequenceBindingSite site);

    public SequenceBindingSite getBindingSite(int n);

    /** returns index of sequence of n'th binding site, -1 if not found */
    public int getBindingId(SequenceBindingSite site);

}
