package rnadesign.rnacontrol;

import java.io.*;
import java.util.*;
import java.util.logging.*;
import generaltools.StringTools;
import rnadesign.rnamodel.*;
import tools3d.objects3d.*;

import static rnadesign.rnacontrol.PackageConstants.*;

/** reads database of structures used for findig single and double stranded bridges */
public class BridgeItController {

    public static final int SIMPLE_HELIX = 1; // using HelixBridgeFinder
    public static final int DOUBLE_HELIX = 2; // using DoubleHelixBridgeFinder
    public static final double DEFAULT_ANGLE_WEIGHT = 10.0;

    private static Logger log = Logger.getLogger(LOGFILE_DEFAULT);
    private List<Object3DLinkSetBundle> bundleList;
    private Object3D nucleotideDB;
    private double angleWeight = 10.0;
    private double rms = 2.0;
    private int lenMin = 1;
    private int lenMax = 3;
    private int solutionMax = 10;
    private PdbJunctionController junctionController;
    private int helixAlgorithm = SIMPLE_HELIX;

    /** Default constructor */
    public BridgeItController() {
	this.bundleList = new ArrayList<Object3DLinkSetBundle>();
    }

    /** Reads for file names */
    public BridgeItController(List<String> fileNames, PdbJunctionController junctionController, Object3D nucleotideDB) {
	this();
	this.junctionController = junctionController;
	this.nucleotideDB = nucleotideDB;
	for (String s : fileNames) {
	    try {
		readAndAdd(s);
	    }
	    catch(IOException ioe) {
		System.out.println("Could not read from file: " + s + " : ignoring: " + ioe.getMessage());
	    }
	}
    }

    /** reads from file with list of filenames */
    public BridgeItController(String fileNameFile, PdbJunctionController junctionController) {
	this.junctionController = junctionController;
	try {
	    FileInputStream fis = new FileInputStream(fileNameFile);
	    String[] lines = StringTools.readAllLines(fis);
	    for (String s : lines) {
		s = s.trim();
		readAndAdd(s);
	    }
	}
	catch (IOException ioe) {
	    System.out.println(ioe.getMessage());
	    assert false;
	}
    }

    /** Central method: given two objects, find list of possible bridges in database */
    public List<Object3DLinkSetBundle> findBridges(JunctionMultiConstraintLink jLink) throws RnaModelException {
	int[] bestPerm = jLink.findBestPermutation();
	List<Object3DLinkSetBundle> result = null;
	for (int i = 0; i < jLink.size(); ++i) {
	    int id1 = bestPerm[bestPerm.length-1];
	    if (i > 0) {
		id1 = bestPerm[i-1];
	    }
	    int id2 = bestPerm[i];
	    List<Object3DLinkSetBundle> newBridgeBundle = findBridge(jLink.getThreePrimeObject(id1),
								     jLink.getFivePrimeObject(id2));
	    if ((newBridgeBundle != null) && (newBridgeBundle.size() > 0)) {
		result.add(newBridgeBundle.get(0)); // add highest scoring bridge
	    } else {
		throw new RnaModelException("Could not find all bridges for junction link " + jLink.getName());
	    }
	}
	return result;
    }

    /** Central method: given two objects, find list of possible bridges in database */
    public List<Object3DLinkSetBundle> findBridge(Object3D obj1, Object3D obj2) throws RnaModelException {
	assert (obj1 != null) ||  (obj2 != null);
	// List<Object3DLinkSetBundle> result = new ArrayList<Object3DLinkSetBundle>();
	if ((obj1 instanceof Nucleotide3D) && (obj2 instanceof Nucleotide3D)) {
	    return findSingleStrandBridge((Nucleotide3D)obj1, (Nucleotide3D)obj2);
	}
	else if ((obj1 != null) && (obj1 instanceof Nucleotide3D) && (obj2 == null)) {
	    return findSingleStrandBridge((Nucleotide3D)obj1, null); // corresponds to extension at strand end
	}
	else if ((obj2 != null) && (obj2 instanceof Nucleotide3D) && (obj1 == null)) {
	    return findSingleStrandBridge(null, (Nucleotide3D)obj2); // corresponds to extension at strand start
	}
	else if ((obj1 instanceof BranchDescriptor3D) && (obj2 instanceof BranchDescriptor3D)) {
	    List<StrandJunction3D> allowedJunctions = junctionController.retrieveAllJunctions();
	    return findHelixBridge((BranchDescriptor3D)obj1, (BranchDescriptor3D)obj2, allowedJunctions);
	}
	else if (new Random().nextDouble() >= 0) { // should always be true
	    throw new RnaModelException("Bridge between classes " + obj1.getClassName() + " and " + obj2.getClassName()
					+ " is not supported.");
	}
	return null;
    }

    private List<StrandJunction3D> retrieveAllowedJunctions(List<DBElementDescriptor> dbElements) {
	List<StrandJunction3D> result = new ArrayList<StrandJunction3D>();
	for (DBElementDescriptor dbElement : dbElements) {
	    result.addAll(junctionController.retrieveJunctions(dbElement));
	}
	return result;
    }

    /** Central method: given two objects, find list of possible bridges in database */
    public List<Object3DLinkSetBundle> findBridge(Object3D obj1, Object3D obj2,
						  List<DBElementDescriptor> dbElements) throws RnaModelException {
	// List<Object3DLinkSetBundle> result = new ArrayList<Object3DLinkSetBundle>();
	if ((obj1 instanceof Nucleotide3D) && (obj2 instanceof Nucleotide3D)) {
	    return findSingleStrandBridge((Nucleotide3D)obj1, (Nucleotide3D)obj2);
	}
	else if ((obj1 instanceof BranchDescriptor3D) && (obj2 instanceof BranchDescriptor3D)) {
	    List<StrandJunction3D> allowedJunctions = retrieveAllowedJunctions(dbElements);
	    return findHelixBridge((BranchDescriptor3D)obj1, (BranchDescriptor3D)obj2, allowedJunctions);
	}
	else if (new Random().nextDouble() >= 0) { // should always be true
	    throw new RnaModelException("Bridge between classes " + obj1.getClassName() + " and " + obj2.getClassName()
					+ " is not supported.");
	}
	return null;
    }

    /** Central method: given two objects, find list of possible bridges in database such that they are connected
     * by single strands which themselves are connected with a new helix
     */
    public List<Object3DLinkSetBundle> findBridgeWithHelix(Object3D obj1, Object3D obj2,
							   int numBP) throws RnaModelException {
	// List<Object3DLinkSetBundle> result = new ArrayList<Object3DLinkSetBundle>();
	if ((obj1 instanceof Nucleotide3D) && (obj2 instanceof Nucleotide3D)) {
	    return findSingleStrandBridgeWithHelix((Nucleotide3D)obj1, (Nucleotide3D)obj2, numBP);
	}
	else if (new Random().nextDouble() >= 0) { // should always be true
	    throw new RnaModelException("Bridge between classes " + obj1.getClassName() + " and " + obj2.getClassName()
					+ " is not supported.");
	}
	return null;
    }

    /** Central method: given two objects, find list of possible bridges in database such that they are connected
     * by single strands which themselves are connected with a new helix
     */
    public List<Object3DLinkSetBundle> findSingleStrandBridgeWithHelix(Nucleotide3D obj1, Nucleotide3D obj2,
								       int numBP) throws RnaModelException {
	assert(false); // FIXIT not yet implemented
	// first generate helix of appropriate size:
	// ...
	// optimize helix position:
	// ...
	// add two single strand bridges
	return null;
    }

    /** Central method: given two objects, find list of possible bridges in database */
    private List<Object3DLinkSetBundle> findSingleStrandBridge(Nucleotide3D obj1, Nucleotide3D obj2) {
	assert (obj1 != null) || (obj2 != null);
	List<Object3DLinkSetBundle> result = new ArrayList<Object3DLinkSetBundle>();
	BridgeFinder finder = new SingleStrandBridgeFinder(bundleList);
	finder.setRms(this.rms);
	finder.setLenMin(this.lenMin);
	finder.setLenMax(this.lenMax);
	finder.setSolutionMax(this.solutionMax);
	finder.setAngleWeight(this.angleWeight);
	return finder.findBridge(obj1, obj2);
    }

    SingleStrandBridgeFinder generateSingleStrandBridgeFinder() {
	return new SingleStrandBridgeFinder(bundleList);
    }

    SingleStrandsBridgeFinder generateSingleStrandBridgesFinder() {
	return new SingleStrandsBridgeFinder(bundleList);
    }

    /** Returns objects used for mining bridge fragments */
    public List<Object3DLinkSetBundle> getBundleList() { return bundleList; }

    /** Central method: given two objects, find list of possible bridges in database */
    public List<Object3DLinkSetBundle> findHelixBridge(BranchDescriptor3D obj1, BranchDescriptor3D obj2,
						       List<StrandJunction3D> junctions) {
	List<Object3DLinkSetBundle> result = new ArrayList<Object3DLinkSetBundle>();
	BridgeFinder finder = null;
	switch (helixAlgorithm) {
	case SIMPLE_HELIX:
	    finder = new HelixBridgeFinder(junctions, nucleotideDB); // bundleList);
	    break;
	case DOUBLE_HELIX:
	    finder = new DoubleHelixBridgeFinder(junctions, nucleotideDB); // bundleList);
	    break;
	default:
	    System.out.println("Unknown helix algorithm: "+ helixAlgorithm);
	    assert false;
	}
	finder.setAngleWeight(angleWeight);
	finder.setLenMax(lenMin);
	finder.setLenMax(lenMax);
	finder.setRms(this.rms);
	finder.setSolutionMax(solutionMax);
	return finder.findBridge(obj1, obj2);
    }

    public void readAndAdd(String fileName) throws IOException {
	log.info("Starting BridgeItController.readAndAdd " + fileName);
	assert bundleList!=null;
	RnaPdbRnaviewReader reader = new RnaPdbRnaviewReader();
	FileInputStream fis = new FileInputStream(fileName);
	log.info("Starting to read bundle...");
	Object3DLinkSetBundle bundle = reader.readBundle(fis);
	if (bundle == null) {
	    throw new IOException("Error reading from file: " + fileName);
	}
	log.info("Adding bundle");
	bundleList.add(bundle);
	log.info("Finished BridgeItController.readAndAdd " + fileName);
    }

    public void setAngleWeight(double weight) { this.angleWeight = weight; }

    public void setHelixAlgorithm(int helixAlgorithm) { this.helixAlgorithm = helixAlgorithm; }

    public void setLenMin(int n) { this.lenMin = n; }

    public void setLenMax(int n) { this.lenMax = n; }

    public void setRms(double rms) { this.rms = rms; }

    public void setSolutionMax(int n) { this.solutionMax = n; }

    public static boolean validateHelixAlgorithm(int n) {
	switch(n) {
	case SIMPLE_HELIX:
	    return true;
	case DOUBLE_HELIX:
	    return true;
	}
	return false;
    }

}
