package rnadesign.rnacontrol;

import java.awt.geom.Point2D;

import tools3d.Camera;
import tools3d.Vector3D;

public interface CameraController {

    public void copy(CameraController other);

    public Camera getCamera();

    /** returns text string with camera data */
    /*
    public String[] getCameraText(int cols, int rows);
    */

    /** returns viewing direction of camera */
    public Vector3D getViewDirection();

    public double getZoom();

    /** moves camera in 2d coordinates */
    public void moveCamera(double up, double right);

    /** central projection method */
    public Point2D project(Vector3D point);

    public void rotateCameraNorth(double angle);

    public void rotateCameraWest(double angle);

    public void setCamera(Camera camera);

    /** sets pixel coordinates corresponding to positions projected to (0,0) */
    public void setOrigin2D(int x, int y);

    /** tilt of camera counter-clockwise with respect to current projection screen */
    public void tiltCamera(double angle);

    /** update camera position from phi, psi angles 
     * @TODO NOT YET IMPLEMENTED!
     */
    public void updateCameraFromAngles(double theta, double phi, double psi, double dist);

    public void setZoom(double zoom);

}
