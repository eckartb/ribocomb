package rnadesign.rnacontrol;

import tools3d.*;
import symmetry.*;

public class CellKaleidoscope implements Kaleidoscope {

    Cell cell;
    LatticeSection section;

    public CellKaleidoscope(Cell cell, LatticeSection section) {
	this.cell = cell;
	this.section = section;
    }

    /** Add mirror objects (only with respect to cell repetitions) to geometry objects stored in ZBuffer. */
    public void apply(ZBuffer zBuffer) {
	// System.out.println("applying CellKaleidoscope with section: " + section);
	assert section.getVolume() > 1;
	int origSize = zBuffer.size();
	for (int i = section.getXMin(); i < section.getXMax(); ++i) {
	    Vector3D shiftX = (cell.getX().mul((double)i));
	    for (int j = section.getYMin(); j < section.getYMax(); ++j) {
		Vector3D shiftY = (cell.getY().mul((double)j));
		for (int k = section.getZMin(); k < section.getZMax(); ++k) {
		    if ((i == 0) && (j == 0) && (k == 0)) {
			continue;
		    }
		    Vector3D shiftZ = (cell.getZ().mul((double)k));
		    Vector3D shift = shiftX.plus(shiftY).plus(shiftZ);
		    // System.out.println("Applying shift: " + shift);
		    for (int ii = 0; ii < origSize; ++ii) {
			Positionable3D newPos = (Positionable3D)(zBuffer.get(ii).cloneDeep());
			newPos.translate(shift);
			zBuffer.add(newPos);
		    }
		}
	    }
	} 
    }

    /** how many symmetry mirror images are generated. 1: only original is there, nothing new is generated */
    public int getSymmetryCount() {
	return section.getVolume();
    }

}
