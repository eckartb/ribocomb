package rnadesign.rnacontrol;

import tools3d.ZBuffer;

public class DummyKaleidoscope implements Kaleidoscope {

    /** This version changes nothing. Other classes potentially add mirror objects to geometry objects stored in ZBuffer. */
    public void apply(ZBuffer zBuffer) { }

        /** how many symmetry mirror images are generated. 1: only original is there, nothing new is generated */
    public int getSymmetryCount() { return 1; } 

}
