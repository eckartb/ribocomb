package rnadesign.rnacontrol;

import java.util.logging.*;
import symmetry.*;
import tools3d.*;
import static rnadesign.rnacontrol.PackageConstants.*;

public class GridShapeTools {

    private static Logger log = Logger.getLogger(LOGFILE_DEFAULT);

    private static void addLine(Shape3DSet shapeSet, LineShape line) {
	shapeSet.add(line);
    }

    /** Creates set of line shapes corresponding to 12 edges of unit cell 
     * from http://web.mit.edu/hg_v1.2.1/distrib/mercury_1_2_1/docs/mercury/PortableHTML/mercurydocn143.html 
     * the angles between the vectors (alpha, the angle between b and c; beta, the angle between a and c; gamma, the angle between a and b).
     */
    public static Shape3DSet createUnitCellShape(Cell cell) {

	Shape3DSet shapeSet = new SimpleShape3DSet();

	Vector3D zero = new Vector3D(0, 0, 0);
	Vector3D x = cell.getX(); // new Vector3D(cell.getA(), 0, 0);
	Vector3D y = cell.getY(); // new Vector3D(cell.getB() * Math.cos(cell.getGamma()), cell.getB() * Math.sin(cell.getGamma()), 0.0);
// 	double zxh = Math.cos(cell.getBeta());
// 	double zyh = Math.cos(cell.getAlpha()) * Math.sin(cell.getGamma());
// 	double zzh = Math.sqrt(1.0 - (zxh * zxh) - (zyh*zyh));
	Vector3D z = cell.getZ(); // new Vector3D(zxh, zyh, zzh);
	// z.scale(cell.getC());

	Vector3D xy = x.plus(y);
	Vector3D xz = x.plus(z);
	Vector3D xyz = xy.plus(z);
	Vector3D yz = y.plus(z);

	LineShape lineShape = new LineShape(new Vector3D(zero), new Vector3D(x));
	addLine(shapeSet, lineShape);
	lineShape = new LineShape(new Vector3D(zero), new Vector3D(y));
	addLine(shapeSet, lineShape);
	lineShape = new LineShape(new Vector3D(x), new Vector3D(xy));
	addLine(shapeSet, lineShape);
	lineShape = new LineShape(new Vector3D(y), new Vector3D(xy));
	addLine(shapeSet, lineShape);
	lineShape = new LineShape(new Vector3D(z), new Vector3D(xz));
	addLine(shapeSet, lineShape);
	lineShape = new LineShape(new Vector3D(z), new Vector3D(yz));
	addLine(shapeSet, lineShape);
	lineShape = new LineShape(new Vector3D(xz), new Vector3D(xyz));
	addLine(shapeSet, lineShape);
	lineShape = new LineShape(new Vector3D(yz), new Vector3D(xyz));
	addLine(shapeSet, lineShape);
	lineShape = new LineShape(new Vector3D(zero), new Vector3D(z));
	addLine(shapeSet, lineShape);
	lineShape = new LineShape(new Vector3D(x), new Vector3D(xz));
	addLine(shapeSet, lineShape);
	lineShape = new LineShape(new Vector3D(xy), new Vector3D(xyz));
	addLine(shapeSet, lineShape);
	lineShape = new LineShape(new Vector3D(y), new Vector3D(yz));
	addLine(shapeSet, lineShape);

	log.finest("GridShapeTools.createUnitCellShape: created shape with " + shapeSet.size() + " lines!");
// 	for (int i = 0; i < shapeSet.size(); ++i) {
// 	    log.info("" + shapeSet.get(i).toString());
// 	}
	return shapeSet;
    }
    
    

}
