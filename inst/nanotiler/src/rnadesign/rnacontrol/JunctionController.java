package rnadesign.rnacontrol;

import java.io.PrintStream;
import java.util.Properties;
import java.util.List;
import rnadesign.rnamodel.BranchDescriptor3D;
import rnadesign.rnamodel.StrandJunctionDB;

/** This controller class provides and easy to use API for managing a database of RNA junctions (parsed from PDB files). This database is NOT part of the scene graph but handled independently. */
public interface JunctionController {

    public boolean isValid();

    public StrandJunctionDB getJunctionDB(); 

    public StrandJunctionDB getKissingLoopDB();

    public StrandJunctionDB getMotifDB();

    /** returns information about junction database (number of junctions and kissing loops etc) */
    public String infoString();

    public void printSuperposableJunctions(double rmsLimit, PrintStream ps);

    public void printSuperposableKissingLoops(double rmsLimit, PrintStream ps);

    /** ranks 2D junctions for interpolating loops */
    List<Properties> rankJunctionFits(BranchDescriptor3D b1, BranchDescriptor3D b2,
				      int n1Min, int n1Max, int n2Min, int n2Max);
    
    /** ranks 2D kissing loops for interpolating loops */
    List<Properties> rankKissingLoopFits(BranchDescriptor3D b1, BranchDescriptor3D b2,
					 int n1Min, int n1Max, int n2Min, int n2Max);

    /** Returns total number of building blocks. */
    int size(); 

}
 
