package rnadesign.rnacontrol;

import tools3d.ZBuffer;

public interface Kaleidoscope {

    /** potentially adds mirror objects to geometry objects stored in ZBuffer */
    void apply(ZBuffer zBuffer);

    /** how many symmetry mirror images are generated. 1: only original is there, nothing new is generated */
    int getSymmetryCount();
}
