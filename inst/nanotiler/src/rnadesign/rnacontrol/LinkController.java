package rnadesign.rnacontrol;

import java.util.*;
import controltools.ModelChanger;
import generaltools.ConstraintDouble;
import tools3d.objects3d.Link;
import tools3d.objects3d.ConstraintLink;
import tools3d.objects3d.TorsionLink;
import tools3d.objects3d.LinkSet;
import tools3d.objects3d.Object3D;
import rnasecondary.InteractionSet;
import rnadesign.rnamodel.BranchDescriptor3D;
import rnadesign.rnamodel.HelixConstraintLink;
import rnadesign.rnamodel.JunctionDBConstraintLink;
import rnadesign.rnamodel.Nucleotide3D;
import rnadesign.rnamodel.RnaModelException;
import rnadesign.rnamodel.StrandJunctionDB;
import rnadesign.rnamodel.MotifConstraintLink;
import rnadesign.rnamodel.SuperimposeConstraintLink;
import rnadesign.rnamodel.BlockCollisionConstraintLink;
import tools3d.objects3d.MultiConstraintLink;
import tools3d.objects3d.SimpleConstraintLink;

public interface LinkController extends LinkSet, ModelChanger {

    /** adds link between two objects */
    void addLink(Object3D o1, Object3D o2);

    /** adds a special link that indicates a helical constraint between two branch descriptors */
    public HelixConstraintLink addHelixConstraintLink(Object3D obj1, Object3D obj2, int basePairMin, int basePairMax,
						      double rms, int symId1, int symId2, String name) 
	throws Object3DGraphControllerException;

    /** adds a special link that encodes a junctions by list of corresponding helix descriptors */
    public JunctionDBConstraintLink addJunctionDBConstraintLink(String name, 
								List<BranchDescriptor3D> branches,
								StrandJunctionDB junctionDB) throws RnaModelException;

    /** adds a special link that indicates a helical constraint between two branch descriptors */
    public ConstraintLink addDistanceConstraintLink(Object3D obj1, Object3D obj2, double min, double max, int symId1, int symId2);

    /** adds a special link that indicates a helical constraint between two branch descriptors */
    public TorsionLink addTorsionConstraintLink(Object3D obj1, Object3D obj2, Object3D obj3, Object3D obj4,
						   double min, double max);
               
    /** adds a special link that indicates a helical constraint between two branch descriptors */
    public MotifConstraintLink addMotifConstraintLink(Object3D obj1, Object3D obj2, int length, List<String> dbFileNames);
    
    public SuperimposeConstraintLink addSuperimposeConstraintLink(Object3D obj1, Object3D obj2);
    
    public BlockCollisionConstraintLink addBlockCollisionConstraintLink(Object3D obj1, Object3D obj2);


    /** adds a special link that encodes a junctions by list of corresponding 3' and 5' residues */
    public MultiConstraintLink addJunctionMultiConstraintLink(String name,
							      List<Nucleotide3D> fivePrimeResidues, List<Nucleotide3D> threePrimeResidues,
							      ConstraintDouble constraint,
							      ConstraintDouble bridgableConstraint,
							      List<Integer> symIds) throws RnaModelException;

    /** adds a link set */
    void addLinks(LinkSet links);

    /** adds link */
    void add(Link link);

    /** returns set of new links that were linking objects in origTree */
    LinkSet cloneLinks(Object3D origTree, Object3D clonedTree) throws Object3DGraphControllerException ;

    LinkSet findBluntBasePairs();

    /** returns links with specified name */
    LinkSet findLinks(String name);

    InteractionSet getResidueInteractions();

    InteractionSet getHydrogenBondInteractions();

    /** removes link given a specified link number. */
    public void remove(int linkId) throws Object3DGraphControllerException;

    /** removes link */
    public void remove(Link link);

    /** removes link by name.
     * @returns number of links that have been removed. */
    public int remove(String linkName);

    /** removes link */
    public void remove(LinkSet links);

    String toPrettyString();

    String toPrettyString(String typeName);

}
