package rnadesign.rnacontrol;

import java.io.*;
import java.util.*;
import java.util.logging.*;
import generaltools.StringTools;
import rnadesign.designapp.NanoTilerScripter;
import rnadesign.rnamodel.predict3d.SubstructureDatabase;
import rnasecondary.SecondaryStructure;
import rnasecondary.SecondaryStructureParser;
import rnasecondary.ImprovedSecondaryStructureParser;
import rnasecondary.substructures.*;
import tools3d.objects3d.*;

import static rnadesign.rnacontrol.PackageConstants.*;


/** reads database of structures used for findig single and double stranded bridges */
public class MotifBlockController {
  
  //fastmode excludes the reading of bulge motifs
  private static boolean fastMode = false;
  
  Map<Class,List<Substructure>> data = new HashMap<Class,List<Substructure>>();
  Map<Substructure,String> names = new HashMap<Substructure,String>();

  private static Logger log = Logger.getLogger(LOGFILE_DEFAULT);

  // private List<SecondaryStructure> structures = new ArrayList<SecondaryStructure>();
  // private List<String> filenames =new ArrayList<String>();;
  //   /** Default constructor */
  //   public MotifBlockController() {
	// this.bundleList = new ArrayList<Object3DLinkSetBundle>();
  //   }
  
public MotifBlockController (String folderName, boolean mode) throws FileNotFoundException{
    this.fastMode = mode;
    init(folderName);
  }
  
public MotifBlockController(String folderName) throws FileNotFoundException{
  init(folderName);
}

    // /** Reads for file names */
    // public MotifBlockController(List<String> fileNames) {
  	//    readFiles(fileNames);
    // }
    
    private void init (String folderName) throws FileNotFoundException {
      log.info("Reading motif database from "+folderName);
      File folder = new File(folderName);
      if(!folder.isDirectory()){
        throw new FileNotFoundException(folderName);
      }
      if (!fastMode) {
        readFiles(FindBulge.class, folderName+SLASH+"bulges");
      }
      readFiles(FindHairpin.class, folderName+SLASH+"hairpins");
      readFiles(FindInternalLoop.class, folderName+SLASH+"internalloops");
      readFiles(FindJunction.class, folderName+SLASH+"junctions");
    }

    public void readFiles(Class type, String subDir) {
      File folder = new File(subDir+SLASH+"2D");
      File[] listOfFiles = folder.listFiles();
      if(listOfFiles==null){
        System.out.println("Error reading: "+folder);
      }
      // System.out.println(listOfFiles);
      List<String> fileNames = new ArrayList<String>();
      for (File file : listOfFiles) {
        if (true) { //TODO add validation for file
            fileNames.add((String)file.getName());
        }
      }
      List<Substructure> structures = new ArrayList<Substructure>();

      // Object3DGraphController graph = new Object3DGraphController();
      // NanoTilerScripter scripter = new NanoTilerScripter(graph);
	for (String name : fileNames) {
    String s = folder+SLASH+name;
    //System.out.println("Reading: "+s); //this line prints too much output into .o files on biowulf & moab
	    try {
      //   graph.readAndAdd(fis, importName, new Vector3D(scaleX, scaleY, scaleZ),
      //       new Vector3D(sx, sy, sz),
      //       format,
      //       addStemsFlag, sequenceChar, onlyFirstPathMode,
      //       findStemsFlag, addJunctionsFlag);
      //   // false, false);
      // }

      SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
	    SecondaryStructure structure = parser.parse(s);

        // Object3DGraphController graph = new Object3DGraphController(); //code used for import sec from pdb, now sec directly used
        // NanoTilerScripter scripter = new NanoTilerScripter(graph);
        // scripter.runScriptLine("import " + s);
        // SecondaryStructure structure = graph.generateSecondaryStructure(); //TODO store this info rather than parse every time
        Substructure sub = new Substructure(type,structure,null,null,null);
        structures.add(sub);

        //find corresponding 3d file name
        int l = name.length ();
        if(! name.substring((l-4),l) .equals (".sec") ){
          throw new Exception("File: "+s+" not a .sec file");
        }
        String name3d = name.substring(0,(l-4))+".pdb";
        String path3d = subDir+SLASH+"3D"+SLASH+name3d;
        File file3d = new File(path3d);
        if(! file3d . exists() ){
          throw new Exception("File: "+path3d+" does not exist");
        }
        this.names.put(sub,path3d);
        // scripter.runScriptLine("clear all");
      }
	    catch(Exception ioe) {
		System.out.println("Could not read from file: " + s + " : ignoring: " + ioe.getMessage());
	    }
      data.put(type,structures);
	}
    }

    // public String getFilename(int i){
    //   return this.filenames.get(i);
    // }
    // public SecondaryStructure getStructure(int i){
    //   return this.structures.get(i);
    // }
    //
    // public List<String> getFilenames(){
    //   return this.filenames;
    // }
    // public List<SecondaryStructure> getStructures(){
    //   return this.structures;
    // }
    public SubstructureDatabase getDatabase(){
      log.info("MotifDatabase initialized.");
      System.out.println("Total entries: "+names.keySet().size());
      for(Class c : data.keySet()){
        System.out.println("Type: "+c + "\nEntries: "+data.get(c).size());
      }
      return new SubstructureDatabase(data,names);
    }

}
