package rnadesign.rnacontrol;

import sequence.LetterSymbol;
import sequence.SimpleLetterSymbol;
import sequence.DnaTools;
import sequence.UnknownSymbolException;

public class Mutation {
    
    String sequenceName;

    int pos;

    LetterSymbol symbol;

    public Mutation(String sequenceName, int pos, char character) throws UnknownSymbolException {
	this.sequenceName = sequenceName;
	this.pos = pos;
	this.symbol = new SimpleLetterSymbol(character, DnaTools.AMBIGUOUS_RNA_ALPHABET);
    }

    public char getCharacter() { return symbol.getCharacter(); }

    public int getPos() { return this.pos; }

    public String getSequenceName() { return sequenceName; }

    public String toString() { return "" + sequenceName + " " + pos + " " + symbol.getCharacter(); }
    
}
