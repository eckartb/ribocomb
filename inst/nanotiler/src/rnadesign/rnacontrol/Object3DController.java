package rnadesign.rnacontrol;

import generaltools.*;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Vector;
import java.util.Properties;
import java.util.logging.*;

import rnadesign.rnamodel.AtomTools;
import rnadesign.rnamodel.Atom3D;
import rnadesign.rnamodel.BioPolymer;
import rnadesign.rnamodel.Molecule3D;
import rnadesign.rnamodel.MoleculeTools;
import rnadesign.rnamodel.NucleotideStrand;
import rnadesign.rnamodel.Nucleotide3D;
import rnadesign.rnamodel.NucleotideTools;
import rnadesign.rnamodel.NucleotideDBTools;
import rnadesign.rnamodel.RnaStrand;
import rnadesign.rnamodel.RnaModelException;
import rnadesign.rnamodel.RnaFolder;
import rnadesign.rnamodel.RnaUnfolder;
import rnadesign.rnamodel.Residue3D;
import rnadesign.rnamodel.SequenceCollector;
import rnadesign.rnamodel.StrandSimilarity;
import rnadesign.rnamodel.StrandJunction3D;
import sequence.Sequence;
import controltools.ModelChangeEvent;
import controltools.ModelChangeListener;
import controltools.ModelChanger;
import tools3d.Appearance;
import tools3d.CoordinateSystem;
import tools3d.RotationDescriptor;
import tools3d.SimpleRotationDescriptor;
import tools3d.Vector3D;
import tools3d.objects3d.CoordinateSystemObject3D;
import tools3d.objects3d.CoordinateSystem3D;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DAction;
import tools3d.objects3d.Object3DActionVisitor;
import tools3d.objects3d.Object3DDepthIterator;
import tools3d.objects3d.Object3DFirstSelectedWrapperAction;
import tools3d.objects3d.Object3DIterator;
import tools3d.objects3d.Object3DSet;
import tools3d.objects3d.Object3DSetSelected;
import tools3d.objects3d.Object3DSetTools;
import tools3d.objects3d.Object3DSimilarity;
import tools3d.objects3d.Object3DTools;
import tools3d.objects3d.Object3DTranslator;
import tools3d.objects3d.Object3DWriter;
import tools3d.objects3d.SimpleObject3D;
import tools3d.objects3d.SimpleObject3DSet;

import static rnadesign.rnacontrol.Object3DGraphControllerConstants.ATOM_COLLISION_DISTANCE;
import static rnadesign.rnacontrol.Object3DGraphControllerConstants.STRAND_FUSION_DIST_MAX;
import static rnadesign.rnacontrol.PackageConstants.*;

public class Object3DController implements ModelChanger {

    public static final int NO_SEQUENCE_RENAMING = 0;
    public static final int SEQUENCE_RENAMING_ABC = 1;
    public static final int SEQUENCE_RENAMING_COUNTER = 2;

    private static final Object3DGraphControllerEventConstants eventConst = new Object3DGraphControllerEventConstants();
    
    private int depthMax = 10;
    private List<ModelChangeListener> modelChangeListeners = new ArrayList<ModelChangeListener>();
    private Object3D graph; // central attribute: stores all 3D object as scene graph!
    private Object3D selectionCursor;
    private Object3D selectionRoot;
    private Object3DIterator iterator;
    private Vector3D[] points; // temporay array
    private String rootName = "root"; // name of generated root
    private Appearance selectionRootPoint = new Appearance(); // Color.CYAN);
    private int renameSequenceMode = SEQUENCE_RENAMING_COUNTER;
    private static Logger log = Logger.getLogger("NanoTiler_debug"); /** The generated debug log. */
    private HashMap<String, Object3D> aliasMap = new HashMap<String, Object3D>();

    /** default constructor
     * @TODO : initialize shape set and shapeSetFactory!
     */
    public Object3DController() {
	// sequences = new ArrayList();
	// bindingSites = new ArrayList();
	// links = new SimpleLinkSet();
	if (graph == null) {
	    setGraph(createRootNode()); // create dummy root node
	}
    }

    /* transform molecular positions to new coordinates
     * @param rootName Name of subtree or null for whole tree
     * @param className Name of 3D class that should be used. Typical: "Atom3D"
     * @returns Transforming coordinate system or null if undefined (for less than 3 positions)
     */
     public CoordinateSystem3D applyNormalizedOrientation(String rootName, String className) {
	 Object3D tree = getGraph();
	 if (rootName != null) {
	     tree = findByFullName(rootName);
	 }
	 if (tree == null) {
	     return null;
	 }
	 return Object3DTools.applyNormalizedOrientation(tree, className);
     }

    /** generates a sequence name that is globally unique */
    String generateNewSequenceName(String origName, Set<String> sofarNames) {
	assert origName != null;
	assert sofarNames != null;
	String result = origName;
	if (!sofarNames.contains(origName)) {
	    return origName; // no name collision
	}
	int counter = 1;
	switch (renameSequenceMode) {
	case NO_SEQUENCE_RENAMING: 
	    return origName;
	case SEQUENCE_RENAMING_ABC:
	    assert false; // not yet implemented
	    break;
	case SEQUENCE_RENAMING_COUNTER:
	    while (sofarNames.contains(result)) {
		result = origName + "_" + counter;
		++counter;
	    }
	    break;
	}
	return result;
    }

    /** Attempts to add a missing atom to a nucleotide. Implemented is "P" and "O3*" */
    void addAtom(Nucleotide3D obj, String atomName) throws Object3DGraphControllerException {
	assert(obj != null);
	assert(atomName != null);
	try {
	    if ("P".equals(atomName)) {
		// add phosphate
		NucleotideDBTools.addMissingPhosphor((Nucleotide3D)obj);
	    } else if ("OP".equals(atomName) || "O1P".equals(atomName) || "O2P".equals(atomName)) {
		// add phosphate
		NucleotideTools.addMissingOP((Nucleotide3D)obj);
	    } else if ("O3*".equals(atomName)) {
		// add O3* to nucleotide
		NucleotideTools.addMissingO3Prime((Nucleotide3D)obj);
	    }
	} catch (RnaModelException rme) {
	    throw new Object3DGraphControllerException(rme.getMessage());
	}
    }


    void addAtom(String nucleotideName, String atomName) throws Object3DGraphControllerException {
	assert(nucleotideName != null);
	assert(atomName != null);
	Object3D obj = Object3DTools.findByFullName(graph, nucleotideName);
	boolean result = false;
	if (obj == null) {
	    throw new Object3DGraphControllerException("Could not find object: " + nucleotideName);
	}
	if (obj instanceof Nucleotide3D) {
	    addAtom((Nucleotide3D)obj, atomName);
	} else if (obj instanceof RnaStrand) {
	    RnaStrand strand = (RnaStrand)obj;
	    for (int i = 0; i < strand.getResidueCount(); ++i) {
		addAtom((Nucleotide3D)(strand.getResidue3D(i)), atomName);
	    }
	} else {
	    throw new Object3DGraphControllerException("Object has to be nucleotide or strand");
	}

    }


    /** adds new ending to object names */
    public void addEnding(String objectFullName,
			  String ending, 
			  Set<String> allowedNames,
			  Set<String> forbiddenNames) throws Object3DGraphControllerException {
	assert graph != null;
	Object3D obj = Object3DTools.findByFullName(graph, objectFullName);
	if (obj == null) {
	    throw new Object3DGraphControllerException("Could not find object: " + objectFullName);
	}
	Object3DTools.addEnding(obj, ending, allowedNames, forbiddenNames);
    }

    /** returns number of children objects of specified 3D object */
    public int getChildrenCount(String fullName) throws Object3DGraphControllerException {
	log.finest("Starting Object3DController.getChildrenCount!");
	if (graph == null) {
	    throw new Object3DGraphControllerException("No object graph defined!");
	}
	Object3D obj = null;
	if ((fullName != null) && (fullName.length() > 0)) {
	    obj = Object3DTools.findByFullName(graph, fullName);
	}
	else {
	    obj = graph;
	}
	if (obj == null) {
	    throw new Object3DGraphControllerException("Could not find object with name: "
						       + fullName);
	}
	return obj.size();
    }
	
    /** adds sub tree. Adds root node if current tree is null */
    public boolean addGraph(Object3D g) {
	if (g == null) {
	    return false;
	}
	if (graph == null) {
	    setGraph(createRootNode());
	}
// 	if (graph.getChild(g.getName()) != null) {
// 	    log.warning("Could not add graph: name already exists in: " + graph.getFullName() + " : " + g.getName());
// 	    return false;
// 	}
	if (renameSequenceMode != NO_SEQUENCE_RENAMING) {
	    /** assures that all sequence names are globally unique */
	    SequenceCollector currentSeqCollector = Object3DGraphController.generateCollectedSequences(graph);
	    Set<String> currentSequenceNames = new HashSet<String>();
	    for (int i = 0; i < currentSeqCollector.getSequenceCount(); ++i) {
		currentSequenceNames.add(currentSeqCollector.getSequence(i).getName());
	    }
	    SequenceCollector newSeqCollector = Object3DGraphController.generateCollectedSequences(g);
	    for (int i = 0; i < newSeqCollector.size(); ++i) {
		Sequence seq = newSeqCollector.getSequence(i);
		seq.setName(generateNewSequenceName(seq.getName(), currentSequenceNames));
		currentSequenceNames.add(seq.getName());
	    }
	}
	String origName = g.getName();
	String newName = graph.insertChildSafe(g);
	if (!newName.equals(origName)) {
	    log.warning("Renaming object " + origName + " to " + newName + " upon insertion into " + rootName);
	}
	refresh(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED));
	return true; // successfully added
    }

    /** adds sub tree to node with name rootName */
    public void addGraph(Object3D g, String rootName) throws Object3DGraphControllerException {
	assert g != null;
	assert rootName != null;
	Object3D rootObj = Object3DTools.findByFullName(getGraph(), rootName);
	if (rootObj == null) {
	    throw new Object3DGraphControllerException("Could not find object with name: " + rootName);
	}
	if (renameSequenceMode != NO_SEQUENCE_RENAMING) {
	    /** assures that all sequence names are globally unique */
	    SequenceCollector currentSeqCollector = Object3DGraphController.generateCollectedSequences(graph);
	    Set<String> currentSequenceNames = new HashSet<String>();
	    for (int i = 0; i < currentSeqCollector.getSequenceCount(); ++i) {
		currentSequenceNames.add(currentSeqCollector.getSequence(i).getName());
	    }
	    SequenceCollector newSeqCollector = Object3DGraphController.generateCollectedSequences(g);
	    for (int i = 0; i < newSeqCollector.size(); ++i) {
		Sequence seq = newSeqCollector.getSequence(i);
		seq.setName(generateNewSequenceName(seq.getName(), currentSequenceNames));
		currentSequenceNames.add(seq.getName());
	    }
	}
// 	if (rootObj.getChild(g.getName()) != null) {
// 	    throw new Object3DGraphControllerException("Object with name " + g.getName() + " already exists in " + rootName);
// 	}
	String origName = g.getName();
	String newName = rootObj.insertChildSafe(g);
	if (!newName.equals(origName)) {
	    log.warning("Renaming object " + origName + " to " + newName + " upon insertion into " + rootName);
	}
	refresh(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED));
    }
        
    public void addGraph(Object3DHandle g) {
	addGraph(g.getObject3D());
	refresh(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED));
    }

//     public void addGraph(PointSet2 g) {
// 	if (g == null) { return; }
// 	if (graph == null) { setGraph(createRootNode()); }
// 	graph.insertChild(g);
// 	refresh(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED));
//     }
    
    public void addModelChangeListener(ModelChangeListener listener) {
	modelChangeListeners.add(listener);
    }

    public void clear() {
	graph = null;
	iterator = null;
	setGraph(createRootNode());
	// refresh(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED)); // already called by setGraph
    }

    /** creates root node */
    private Object3D createRootNode() {
	Object3D root = new SimpleObject3D();
	root.setName(rootName);
	return root;
    }

    /** Clones 3D object (subtree with name origFullName), inserts cloned subtree under object with name newParentFullName
     * and gives it the name newIndividualName.
     * Links are not yet cloned!!! */
    void cloneObject(String origFullName, String newParentFullName, String newIndividualName, int symId) 
     throws Object3DGraphControllerException {
	log.fine("Starting Object3DController.cloneObject!");
	if (graph == null) {
	    throw new Object3DGraphControllerException("No object graph defined!");
	}
	Object3D obj = Object3DTools.findByFullName(graph, origFullName);
	if (obj == null) {
	    throw new Object3DGraphControllerException("Object3DController: Could not find original object with name: "
						       + origFullName);
	}
	Object3D parent = Object3DTools.findByFullName(graph, newParentFullName);
	if (parent == null) {
	    throw new Object3DGraphControllerException("Object3DController: Could not find new parent object with name: "
						       + newParentFullName);
	}
	Object3D newObj = (Object3D)(obj.cloneDeep());
	newObj.setName(newIndividualName);
	int origSize = parent.size();
	parent.insertChild(newObj);
	assert parent.size() == (origSize + 1);
	log.fine("Finished Object3DController.cloneObject!");
    }

    /** Compute distance betwen two 3D objects specified by full name */
    public double computeDistance(String origFullName, String newParentFullName) 
     throws Object3DGraphControllerException {
	if (graph == null) {
	    throw new Object3DGraphControllerException("No object graph defined!");
	}
	Object3D obj = Object3DTools.findByFullName(graph, origFullName);
	if (obj == null) {
	    throw new Object3DGraphControllerException("Could not find object with name: "
						       + origFullName);
	}
	Object3D parent = Object3DTools.findByFullName(graph, newParentFullName);
	if (parent == null) {
	    throw new Object3DGraphControllerException("Could not find object with name: "
						       + newParentFullName);
	}
	return obj.distance(parent);
    }

    /** fuses strands such that strand 1 becomes longer, strand 2 will be deleted. Use Object3DGraphController to call method */
    String fuseStrands(String strandFullName1, String strandFullName2) throws Object3DGraphControllerException {
	String appendedStrandName = strandFullName1;
	// find out which strand to fuse with which:
	Object3D obj1 = findByFullName(strandFullName1);
	if (obj1 instanceof Nucleotide3D) {
		obj1=(Object3D)((Residue3D)obj1).getParentObject();
	}
	if ((obj1 == null) || (! (obj1 instanceof RnaStrand))) {
	    throw new Object3DGraphControllerException("Could not find strand with name " + strandFullName1);
	}
	Object3D obj2 = findByFullName(strandFullName2);
	if (obj2 instanceof Nucleotide3D) {
		obj2=(Object3D)((Residue3D)obj2).getParentObject();
	}
	if ((obj2 == null) || (! (obj2 instanceof RnaStrand))) {
	    throw new Object3DGraphControllerException("Could not find strand with name " + strandFullName2);
	}
	NucleotideStrand strand1 = (NucleotideStrand)obj1;
	NucleotideStrand strand2 = (NucleotideStrand)obj2;
	if (strand1.getResidueCount() == 0) {
	    throw new Object3DGraphControllerException("Strand must have at leat one residue: " + strandFullName1);
	}
	if (strand2.getResidueCount() == 0) {
	    throw new Object3DGraphControllerException("Strand must have at leat one residue: " + strandFullName2);
	}
	double d1 = strand1.getResidue3D(strand1.getResidueCount()-1).distance(strand2.getResidue3D(0));
	double d2 = strand2.getResidue3D(strand2.getResidueCount()-1).distance(strand1.getResidue3D(0));
	boolean atomMode = false; // if true, use atomic distance between O3* and P
	if ((strand1.getResidue3D(strand1.getResidueCount()-1).getIndexOfChild("O3*") >= 0)
	    && (strand2.getResidue3D(strand2.getResidueCount()-1).getIndexOfChild("O3*") >= 0)
	    && (strand2.getResidue3D(0).getIndexOfChild("P") >= 0)
	    && (strand1.getResidue3D(0).getIndexOfChild("P") >= 0)) {
	    atomMode = true;
	    d1 = strand1.getResidue3D(strand1.getResidueCount()-1).getChild("O3*").distance(strand2.getResidue3D(0).getChild("P"));
	    d2 = strand2.getResidue3D(strand2.getResidueCount()-1).getChild("O3*").distance(strand1.getResidue3D(0).getChild("P"));
	}
	if (d1 < STRAND_FUSION_DIST_MAX) {
	    // strand1.append((NucleotideStrand)(strand2.cloneDeep())); // TODO : dangerous: cloning messes up junctions. 
	    strand1.append(strand2); // TODO : dangerous: cloning messes up junctions. 
	    remove(strand2);
	    log.info("Appending " + strandFullName2 + " to " + strandFullName1);
	}
	else {
	    if (d2 < STRAND_FUSION_DIST_MAX) {
		strand2.append(strand1); // TODO : dangerous: cloning messes up junctions. 
		remove(strand1);
		log.info("Appending " + strandFullName1 + " to " + strandFullName2);
		appendedStrandName = strandFullName2;
	    }
	    else {
		throw new Object3DGraphControllerException("Strands are too far to be fused: " + d2);
	    }
	}
	return appendedStrandName;
    }

    private boolean hasClosebyAtom(Atom3D atom, RnaStrand strandOrig, Object3DSet strands, double distance) {
	double resCutoff = 20.0;
	// loop over strands
	for (int i = 0; i < strands.size(); ++i) {
	    RnaStrand strand = (RnaStrand)(strands.get(i));
	    if (strand == strandOrig) {
		continue; // ignore
	    }
	    // loop over residues
	    for (int j = 0; j < strand.getResidueCount(); ++j) {
		Residue3D residue = strand.getResidue3D(j);
		// loop over atom of residue
		for (int k = 0; k < residue.getAtomCount(); ++k) {
		    double d = atom.distance(residue.getAtom(k));
		    if (d > resCutoff) {
			break; // too far, skip to next residue
		    }
		    else if (d < distance) {
			return true;
		    }		    
		}
	    }
	}
	return false;
    }

    /** Returns true if for every atom of this strand we can find another atom in another strand within less distance */
    private boolean isDuplicateStrand(RnaStrand strand, Object3DSet strands, double distance) {
	// loop over residues
	for (int i = 0; i < strand.getResidueCount(); ++i) {
	    Residue3D residue = strand.getResidue3D(i);
	    // loop over atoms
	    for (int j = 0; j < residue.getAtomCount(); ++j) {
		if (!hasClosebyAtom(residue.getAtom(j), strand, strands, distance)) {
		    return false;
		}
		if (j > 0) {
		    break;
		}
	    }
	}
	return true;
    }

    public void reportMissingAtoms(Nucleotide3D nuc, PrintStream ps) {
	List<String> result = NucleotideTools.findMissingAtoms(nuc);
	if ((result != null) && (result.size() > 0)) {
	    ps.print("Nucleotide " + nuc.getFullName() + " has missing atoms:");
	    for (String name : result) {
		ps.print(" " + name);
	    }
	    ps.println();
	}
    }

    
    public void checkStructure(RnaStrand strand, PrintStream ps) {
	for (int i = 0; i < strand.getResidueCount(); ++i) {
	    reportMissingAtoms((Nucleotide3D)(strand.getResidue3D(i)), ps);
	}
    }

    /** Performs report on structural problems. Currently implemented: missing backbone atoms */
    public void checkStructure(PrintStream ps) {
	Object3DSet strands = Object3DTools.collectByClassName(getGraph(), "RnaStrand");
	for (int i = 0; i < strands.size(); ++i) {
	    checkStructure((RnaStrand)(strands.get(i)), ps);
	}
    }

    /** removes all sequence in which every atom has another atom in less than 0.1 Angstrom. Not valid for sequences part of a junction */
    public void removeDuplicateSequences() {
	Object3DSet strands = Object3DTools.collectByClassName(getGraph(), "RnaStrand");
	boolean isOk = true;
	double distanceCutoff = 1.5;
	do {
	    for (int i = strands.size()-1; i >= 0; --i) {
		assert strands.get(i) instanceof RnaStrand;
		RnaStrand strand = (RnaStrand)(strands.get(i));
		if (strand.getParent() instanceof StrandJunction3D) {
		    continue; // ignore strands that are part of a junction
		}
		if (isDuplicateStrand(strand, strands, distanceCutoff)) {
		    log.info("Removing strand: " + strand.getFullName());
		    remove(strand);
		    strands.remove(strand);
		    isOk = false;
		}
		else {
		    log.info("Keeping strand " + strand.getFullName());
		}
	    }
	}
	while (!isOk);
	refresh(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED));
    }

    /** splits strand such first sequence has residue 0..position-1, new strand has positions position..length-1 */
    public String splitStrand(String strandFullName, int position) throws Object3DGraphControllerException {
	String appendedStrandName = strandFullName;
	// find out which strand to fuse with which:
	Object3D obj1 = findByFullName(strandFullName);
	if ((obj1 == null) || (! (obj1 instanceof NucleotideStrand) ) || (obj1.getParent() == null)) {
	    throw new Object3DGraphControllerException("Could not find strand or parent object of object with name " + strandFullName);
	}
	NucleotideStrand strand1 = (NucleotideStrand)obj1;
	if (strand1.getResidueCount() < position) {
	    throw new Object3DGraphControllerException("Strand must have at leat one residue: " + strandFullName);
	}
	String newName = strand1.getName() + "_split";
	NucleotideStrand newStrand = (NucleotideStrand)(strand1.split(position, newName));
	if (strand1.getParent().getChild(newName) != null) { // check if name already exists:
	    throw new Object3DGraphControllerException("Object with name already exists: " + newStrand.getName());
	}
	strand1.getParent().insertChild(newStrand);
	refresh(new ModelChangeEvent(this, eventConst.MODEL_SELECTION_CHANGED));
	return strand1.getFullName();
    }

    private Object3D synthesizeObject(String className, Vector3D position, String name) throws Object3DGraphControllerException {
	assert className != null;
	assert name != null;
	assert position != null;
	Object3D obj = null;
	if (className.equals(SimpleObject3D.CLASS_NAME) || (className.equals("simple"))) {
	    obj = new SimpleObject3D(position);
	}
	else if (className.equals(CoordinateSystem3D.CLASS_NAME) || className.equals("cs") ) {
	    obj = new CoordinateSystem3D(position);
	} else {
	    throw new Object3DGraphControllerException("Generating object " + className + " is not available.");
	}
	obj.setName(name);
	return obj;
    }
    
    public void makeAlias(String name, String alias){
    	Object3D obj = findByFullName(name);
    	assert alias!="";
    	if ((obj == null) || (! (obj instanceof Nucleotide3D))) {
	    //throw new Object3DGraphControllerException("Could not find residue with name " + name);
		}
    	aliasMap.put(alias, obj);
    }
    
    public Object3D findByAlias(String prename) {
    
    	StringBuilder sb = new StringBuilder(prename);
    	sb.deleteCharAt(0); //deletes '#'
    	String name = sb.toString();
    	
    	Object3D obj = aliasMap.get(name);
    	
    	return obj;
    	
    }
    
    public void deleteAllAlias(){
    	aliasMap.clear();
    }
    
    /** returns pointer to found object with this full name */
    public Object3D findByFullName(String name) {
	if ((name ==  null) || (name.length() == 0)) {
	    return null;
	}
	
		if (name.charAt(0) == '#') {
	    return findByAlias(name); //alias
		}
	
        Object3D result = Object3DTools.findByFullName(graph, name);
// 	if ((result != null) && (!result.getFullName().equals(name))) {
// 	    log.info("Searched " + name + " found: " + result.getFullName());
// 	}
	return result;
    }

    /** returns set of objects matching pattern or precise name.
     * Example: root.import.* returns a set with all child nodes of root.import */
    public Object3DSet findAllByFullName(String name) throws Object3DGraphControllerException {
	Object3DSet result = new SimpleObject3DSet();
	if ((name ==  null) || (name.length() == 0)) {
	    throw new Object3DGraphControllerException("No viable name defined for findAllByFullName.");
	}
	if (name.charAt(name.length()-1) != '*') {
	    Object3D obj = Object3DTools.findByFullName(graph, name);
	    if (obj != null) {
		result.add(obj);
	    }
	} else {
	    if ((name.length() < 3) || (name.charAt(name.length()-2) != '.'))  {
		throw new Object3DGraphControllerException("Bad placeholder syntax in findAllByFullName: " + name);
	    }
	    String pName = name.substring(0, name.length()-2); // without trailing ".*"
	    Object3D pObj = findByFullName(pName);
	    if (pObj == null) {
		throw new Object3DGraphControllerException("Could not find parent object in findAllByFullName: " + pName);
	    }
	    for (int i = 0; i < pObj.size(); ++i) {
		result.add(pObj.getChild(i));
	    }
	}
	return result;
    }

    public void generateSignature(String signature)
    {
        System.out.println("Not yet implemented");
    }
    
    public Properties superposeStrands(String fullName1, String fullName2) throws Object3DGraphControllerException {
	Object3D obj1 = findByFullName(fullName1);
	Object3D obj2 = findByFullName(fullName2);
	if (obj1 == null) {
	    throw new Object3DGraphControllerException("Could not find object with name: " + fullName1);
	}
	if (obj2 == null) {
	    throw new Object3DGraphControllerException("Could not find object with name: " + fullName2);
	}
	Object3DSimilarity similarityGenerator = new StrandSimilarity();
	Properties properties = similarityGenerator.similarity(obj1, obj2);
	properties.setProperty("name1", fullName1);
	properties.setProperty("name2", fullName2);
	return properties;
    }

    public void synthesizeObject(String className, Vector3D position, String name,
				      String newParentFullName) throws Object3DGraphControllerException {
	assert className != null;
	assert name != null;
	assert position != null;
	Object3D obj = synthesizeObject(className, position, name);
	assert(obj.getName().equals(name));
	Object3D newParent = Object3DTools.findByFullName(graph, newParentFullName);
	if (newParent == null) {
	    throw new Object3DGraphControllerException("Could not find parent object with name: "
						       + newParentFullName);
	}
	if (newParent.getChild(obj.getName()) != null ) {
	    throw new Object3DGraphControllerException("Object " + newParentFullName + " already has child node with name " + name);
	}
	newParent.insertChild(obj);
    }

    /** move object to new location in hierarchy, it becomes child of newParentName */
    public void moveObject(Object3D obj, Object3D newParent) 
     throws Object3DGraphControllerException {
	log.fine("Starting Object3DController.moveObject!");
	Object3D oldParent = obj.getParent();
	if (oldParent == null) {
	    throw new Object3DGraphControllerException("Cannot move root object: " + obj.getFullName());
	}
	oldParent.removeChild(obj);
	newParent.insertChild(obj);
	log.fine("Finished Object3DController.moveObject!");
    }

    /** move object to new location in hierarchy, it becomes child of newParentName */
    public void moveObject(String origFullName, String newParentFullName) 
     throws Object3DGraphControllerException {
	log.fine("Starting Object3DController.moveObject!");
	if (graph == null) {
	    throw new Object3DGraphControllerException("No object graph defined!");
	}
	Object3D obj = Object3DTools.findByFullName(graph, origFullName);
	if (obj == null) {
	    throw new Object3DGraphControllerException("Could not find object with name: "
						       + origFullName);
	}
	Object3D newParent = Object3DTools.findByFullName(graph, newParentFullName);
	if (newParent == null) {
	    throw new Object3DGraphControllerException("Could not find object with name: "
						       + newParentFullName);
	}
	Object3D oldParent = obj.getParent();
	if (oldParent == null) {
	    throw new Object3DGraphControllerException("Cannot move root object: " + origFullName);
	}
	oldParent.removeChild(obj);
	newParent.insertChild(obj);
	log.fine("Finished Object3DController.moveObject!");
    }

    /** move object to new location in hierarchy, it becomes child of newParentName */
    public void moveToClosestObject(String origFullName, String parentFullName,
				    String distClassName, String assignClassName,
				    double distCutoff) throws Object3DGraphControllerException {
	log.fine("Starting Object3DController.moveObject!");
	if (graph == null) {
	    throw new Object3DGraphControllerException("No object graph defined!");
	}
	Object3D obj = Object3DTools.findByFullName(graph, origFullName);
	if (obj == null) {
	    throw new Object3DGraphControllerException("Could not find object with name: "
						       + origFullName);
	}
	String name = obj.getName();
	Object3D newParent = Object3DTools.findByFullName(graph, parentFullName);
	if (newParent == null) {
	    throw new Object3DGraphControllerException("Could not find object with name: "
						       + parentFullName);
	}
	Vector3D pos = obj.getPosition();
	Object3DSet assignObjectSet = Object3DTools.collectByClassName(newParent, assignClassName);
	double dMax = 1e30;
	Object3D bestParent = null;
	Object3D bestPartner = null;
	for (int i = 0; i < assignObjectSet.size(); ++i) {
	    Object3D other = Object3DTools.findClosestByClassName( assignObjectSet.get(i) , pos, distClassName);
	    if (other == null) {
		continue;
	    }
	    double d = other.getPosition().distance(pos);
	    if (d < dMax) {
		dMax = d;
		bestParent = assignObjectSet.get(i);
		bestPartner = other;
	    }
	}
	if ((bestParent != null) && (dMax <= distCutoff) ) {
	    log.info("Inserting object " + obj.getFullName() + " as child node of " + bestParent.getFullName()
		     + " because of close distance to " + bestPartner.getFullName() + " : " + dMax);
	    moveObject(obj, bestParent);
	}

	log.fine("Finished Object3DController.moveObject!");
    }

    /** actives all registered model change listeners */
    public void fireModelChanged(ModelChangeEvent changeEvent) {
	//log.info("calling Object3DController.fireModelChanged!");
	log.fine("calling Object3DController.fireModelChanged!");
  for (int i = 0; i < modelChangeListeners.size(); ++i) {
	    ModelChangeListener listener = (ModelChangeListener)(modelChangeListeners.get(i));
	    //log.info("Calling modelChanged for listener " + listener);
	    listener.modelChanged(changeEvent);
	}
    }

    public int getDepthMax() {
	return depthMax;
    }
    
    public Object3D getGraph() {
	return graph;
    }

    public Object3D getGraph(String fullName) {
	return Object3DTools.findByFullName(graph, fullName);
    }

    /** returns current selection cursor. Is only null if graph is null */
    public Object3D getSelectionCursor() {
 	return selectionCursor;
    }

    /** returns top node of selected subtree. Might be null if no objects are selected! */
    public Object3D getSelectionRoot() { 
	return selectionRoot;
    }

    public Vector<String> getTree(Set<String> allowedNames,
				  Set<String> forbiddenNames) {
	if (graph != null) {
	    return Object3DTools.getFullNameTree(graph,
						 allowedNames,
						 forbiddenNames);
	}
	else {
	    Vector<String> result = new Vector<String>();
	    result.add("No object tree defined!");
	    return result;
	}
    }

    public Vector<String> getTree(String[] allowedNamesRaw,
				  String[] forbiddenNamesRaw) {
	Set<String> allowedNames = new HashSet<String>();
	Set<String> forbiddenNames = new HashSet<String>();
	if(allowedNamesRaw != null){
	    for (String s : allowedNamesRaw) {
		allowedNames.add(s);
	    }
	}
	if(forbiddenNamesRaw != null){
	    for (String s : forbiddenNamesRaw) {
		forbiddenNames.add(s);
	    }
	}
	return getTree(allowedNames, forbiddenNames);
    }

    /** human readable output of object tree */
    public void printTree(PrintStream ps, Set<String> allowedNames, Set<String> forbiddenNames) {
	log.fine("Starting printTree[1]");
	if (graph != null) {
// 	    log.info("Allowed class names:");
// 	    if (allowedNames != null) {
// 		System.out.println(" " + allowedNames.size() + " ");
// 		for (String s : allowedNames) {
// 		    System.out.print(s + " ");
// 		}
// 		System.out.println();
// 	    }
// 	    log.info("Forbidden class names:");
// 	    if (forbiddenNames != null) {
// 		System.out.println(" " + forbiddenNames.size() + " ");
// 		for (String s : forbiddenNames) {
// 		    System.out.print(s + " ");
// 		}
// 		System.out.println();
// 	    }
	    Object3DTools.printFullNameTree(ps, graph, allowedNames, forbiddenNames);
	}
	else {
	    ps.println("No object tree defined!");
	}
    }

    /** human readable output of object tree */
    public void printTree(PrintStream ps, String name,
			  Set<String> allowedNames, Set<String> forbiddenNames) {
	log.fine("Starting printTree[2] for name: " + name);
	if (graph != null) {
	    Object3D obj = findByFullName(name);
	    if (obj == null) {
		ps.println("Could not find object with name: " + name);
	    }
	    else {
		log.fine("Calling printFullNameTree(ps, obj) for object " + obj.getFullName());
		Object3DTools.printFullNameTree(ps, obj, allowedNames, forbiddenNames);
	    }
	}
	else {
	    ps.println("No object tree defined!");
	}
    }

    /** removes all objects with this class name */
    public void removeByClassName(String className) {
	Object3D root = getSelectionCursor();
	if (root != null) {
	    Object3DSet set = Object3DTools.collectByClassName(root, className);
	    for (int i = 0; i < set.size(); ++i) {
		remove(set.get(i));
	    }
	}	
    }

    /** Returns center of mass of objects with certain class */
    public Vector3D computeCenterOfMass(Object3D treeRoot, String className) {
	if (treeRoot == null) {
	    treeRoot = getGraph(); // get default root
	}
	return Object3DSetTools.centerOfMass(Object3DTools.collectByClassName(treeRoot, className));
    }

    /** returns set with all atoms */
    public Object3DSet collectAtoms() {
	return Object3DTools.collectByClassName(getGraph(), "Atom3D");
    }

    /** returns set with all residues */
    public Object3DSet collectResidues() {
	return Object3DTools.collectByClassName(getGraph(), "Nucleotide3D");
    }

    /** returns set with all RNA strands (TODO: would not work with non-RNA sequences) */
    public Object3DSet collectStrands() {
	return Object3DTools.collectByClassName(getGraph(), "RnaStrand");
    }

    /** returns set with all junctions */
    public Object3DSet collectJunctions() {
	return Object3DTools.collectByClassName(getGraph(), "StrandJunction3D");
    }

    /** returns set with all kissing loops */
    public Object3DSet collectKissingLoops() {
	return Object3DTools.collectByClassName(getGraph(), "KissingLoop3D");
    }

    /** counts number of colliding atoms */
    public int countAtomCollisions(double distance, Level verboseLevel) {
	return AtomTools.countAtomCollisions(collectAtoms(), distance, verboseLevel);
    }

    /** removes all objects with this class name */
    public void rename(String newName) throws Object3DGraphControllerException {
	Object3D root = getSelectionRoot();
	if (root != null) {
	    if (root.getParent() == null) {
		throw new Object3DGraphControllerException("Cannot rename root object!");
	    }
	    root.setName(newName);
	}
	else {
	    throw new Object3DGraphControllerException("No object selected!");
	}
    }

    public void selectAll() {
	log.fine("calling Object3DController.selectAll()");
	selectionCursor = graph;
	selectionRoot = graph;
	Object3DAction selectSetter = new Object3DSetSelected(
				      Object3DSetSelected.SET_SELECT);
	Object3DActionVisitor selectVisitor = 
	    new Object3DActionVisitor(selectionRoot, selectSetter);
	selectVisitor.nextToEnd(); // visit root and all sub-nodes!
	refresh(new ModelChangeEvent(this, eventConst.MODEL_SELECTION_CHANGED));
    }

    public void select(Object3D obj) {
	log.fine("calling Object3DController.select()");
	assert Object3DTools.isAncestor(graph, obj) || (obj == graph);
	selectionCursor = obj;
	selectionRoot = obj;
	Object3DAction selectSetter = new Object3DSetSelected(
				      Object3DSetSelected.SET_SELECT);
	Object3DActionVisitor selectVisitor = 
	    new Object3DActionVisitor(selectionRoot, selectSetter);
	selectVisitor.nextToEnd(); // visit root and all sub-nodes!
	refresh(new ModelChangeEvent(this, eventConst.MODEL_SELECTION_CHANGED));
    }

    /** selects object by name */
    public void selectByFullName(String name) throws Object3DGraphControllerException {
	if (graph == null) {
	    throw new Object3DGraphControllerException("No object graph defined!");
	}
	Object3D obj = Object3DTools.findByFullName(graph, name);
	if (obj == null) {
	    throw new Object3DGraphControllerException("Could not find object with name: " + name);
	}
	assert Object3DTools.isAncestor(graph, obj) || (graph == obj);
	select(obj);
    }

    public void selectCurrent() {
	Object3DAction selectSetter = new Object3DSetSelected(
					         Object3DSetSelected.SET_SELECT);
	Object3DActionVisitor selectVisitor = 
	    new Object3DActionVisitor(selectionCursor, selectSetter);
	selectionRoot = selectionCursor;
	
	selectVisitor.nextToEnd(); // visit root and all sub-nodes!
	refresh(new ModelChangeEvent(this, eventConst.MODEL_SELECTION_CHANGED));
    }

    public void deselectAll() {
	selectionCursor = graph;
	selectionRoot = null;
	Object3DAction selectSetter = new Object3DSetSelected(
				      Object3DSetSelected.SET_SELECT);
	Object3DActionVisitor selectVisitor = 
	    new Object3DActionVisitor(selectionCursor, selectSetter);
	selectVisitor.nextToEnd(); // visit root and all sub-nodes!
	refresh(new ModelChangeEvent(this, eventConst.MODEL_SELECTION_CHANGED));
    }


    public void deselectCurrent() {
	Object3DAction deselectSetter = new Object3DSetSelected(
			   Object3DSetSelected.SET_DESELECT);
	Object3DActionVisitor deselectVisitor = 
	    new Object3DActionVisitor(selectionCursor, deselectSetter);
	deselectVisitor.nextToEnd(); // visit root and all sub-nodes!
	if (selectionCursor.equals(selectionRoot)) {
	    selectionRoot = null; // everything was deselected. Otherwise: keep selection root, only subtree was deselected
	}
	refresh(new ModelChangeEvent(this, eventConst.MODEL_SELECTION_CHANGED));
    }

    public boolean isEmpty() {
	return graph == null || graph.size() == 0;
    }

    /** removes subtree from graph. Not public anymore - call Object3DGraphController instead */
    void remove(Object3D subTree) {
	log.fine("Started method remove on Object3DController!");
	Object3DTools.remove(subTree);
	if (selectionRoot == null) selectionCursor = graph; // reset the cursor
	// refresh(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED));
    }

    /** Randomizes orientation of subtree */
    public void randomize(Object3D subTree) {
	log.fine("Started method randomize on Object3DController!");
	Object3DTools.randomizeOrientation(subTree);
	refresh(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED));
    }

    /** Randomizes orientation of subtree */
    public void randomizeTranslation(Object3D subTree, double scale) {
	log.fine("Started method randomize on Object3DController!");
	Object3DTools.randomizeTranslation(subTree, scale);
	refresh(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED));
    }

    /** Randomizes orientation of subtree */
    public void randomize(String subTreeName) throws Object3DGraphControllerException {
	Object3D obj = Object3DTools.findByFullName(graph, subTreeName);
	if (obj== null) {
	    throw new Object3DGraphControllerException("Could not find object:"+ subTreeName);
	}
	randomize(obj);
    }

    /** Randomizes orientation or translation of subtree */
    public void randomize(String subTreeName, String mode, double scale) throws Object3DGraphControllerException {
	Object3D obj = Object3DTools.findByFullName(graph, subTreeName);
	if (obj== null) {
	    throw new Object3DGraphControllerException("Could not find object:"+ subTreeName);
	}
	if ((mode == null) || (mode.equals("rotate"))) {
	    randomize(obj);
	} else if (mode.equals("translate")) {
	    randomizeTranslation(obj, scale);
	} else {
	    throw new Object3DGraphControllerException("Unknown randomization mode: " + mode);
	}
    }

    /** removes currently selected subtree from graph */
    public void remove() {
	log.fine("Started method remove on Object3DController!");
	Object3D selRoot = getSelectionCursor();
	if (selRoot != null) {
	    Object3DTools.remove(selRoot);
	    refresh(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED));
	}
    }

    public void remove(String fullName) throws Object3DGraphControllerException {
	Object3D obj = findByFullName(fullName);
	if (obj == null) throw new Object3DGraphControllerException("Could not find object with name: " + fullName);
	remove(obj);
    }

    public void resetSelectionCursor() { 
	selectionCursor = graph;
	refresh(new ModelChangeEvent(this, eventConst.MODEL_SELECTION_CHANGED));
    }

    public void moveUpSelectionCursor() {
	if ((selectionCursor != null) && (selectionCursor.getParent() != null)) {
	    selectionCursor = selectionCursor.getParent();
	}
	refresh(new ModelChangeEvent(this, eventConst.MODEL_SELECTION_CHANGED));
    }

    public void moveDownSelectionCursor() {
	if ((selectionCursor != null) && (selectionCursor.size() > 0)) {
	    selectionCursor = selectionCursor.getChild(0);
	}
	refresh(new ModelChangeEvent(this, eventConst.MODEL_TRANSLATED));
    }

    /** select instead next sibling */
    public void incSelectionCursor() {
	if ((selectionCursor != null) && (selectionCursor.getSiblingCount() > 0)
	    && ((selectionCursor.getSiblingId()+1) < selectionCursor.getSiblingCount()) ) {
	    selectionCursor = selectionCursor.getSibling(selectionCursor.getSiblingId() + 1);
	}
	refresh(new ModelChangeEvent(this, eventConst.MODEL_SELECTION_CHANGED));
    }

    
    public void decSelectionCursor() {
	if ((selectionCursor != null) && (selectionCursor.getSiblingCount() > 0)
	    && ((selectionCursor.getSiblingId() > 0) ) ) {
	    selectionCursor = selectionCursor.getSibling(selectionCursor.getSiblingId() - 1);
	}
	refresh(new ModelChangeEvent(this, eventConst.MODEL_SELECTION_CHANGED));
    }

    public Object3D getNextObject() {
	if (iterator == null) {
	    return null;
	}
	return iterator.getNextObject();
    }
    
    public int getObjectCount() {
	if (graph == null) {
	    return 0;
	}
	return graph.getTotalNumberOfObjects();
    }
    
    /** returns text representation of Object3DGraph */
    public String getGraphText() {
	if (graph != null) {
	    return graph.toString();
	}
	return "";
    }

    /** returns text representation of selected subset of Object3DGraph */
    public String getSelectedGraphText() {
	Object3D cursor = getSelectionCursor();
	String cursorText;
	if (cursor != null) {
	    cursorText = cursor.infoString();
	}
	else {
	    cursorText = "undefined";
	}
	Object3D selectionRoot = getSelectionRoot();
	String selectionRootText;
	if (selectionRoot != null) {
	    selectionRootText = Object3DTools.generateFullTreeLine(selectionRoot);
	}
	else {
	    selectionRootText = "undefined";
	}

	String result =//  "Cursor: " + cursorText + NEWLINE
	    "Selection-root: " + selectionRootText;

	return result;
    }

    /** returns short info text representation of selected subset of Object3DGraph */
    public String getSelectedGraphInfoText() {
	Object3D selectionRoot = getSelectionRoot();
	String selectionRootText;
	if (selectionRoot != null) {
	    selectionRootText = Object3DTools.getFullName(selectionRoot);
	}
	else {
	    selectionRootText = "undefined";
	}
	return selectionRootText;
    }

    public boolean hasMoreObjects() {
	if (iterator != null) {
	    return iterator.hasMoreObjects();
	}
	return false;
    }
    
    /* TODO */

    /** brings internal data up to date in case the Object3D tree has changed
     * TODO: depending on event type, parts of refresh calls are not necessary
     */
    private void refresh(ModelChangeEvent event) {
	iterator = new Object3DDepthIterator(graph);
	iterator.setDepthMax(depthMax);
	refreshSelection();
	fireModelChanged(event);
    }
    
    private void refreshSelection() {
	if (graph == null) {
	    return;
	}
	// deselect all objects:
	Object3DTools.setSelectAll(graph, false);
	// select objects of selection root:
	if (selectionRoot != null) {
	    Object3DTools.setSelectAll(selectionRoot, true);
	}
    }
	
    public void resetIterator() {
	if (iterator != null) {
	    iterator.reset();
	}
    }
    
    /** rotates selected objects */

    public void rotate(Object3D obj,
		       double ax, double ay, double az, double angle) 
    {
	log.fine("Rotating object: " + ax + " " + ay + " " + az
			   + " " + angle);
	Vector3D axis = new Vector3D(ax, ay, az);
	obj.rotate(axis, angle);
	refresh(new ModelChangeEvent(this, eventConst.MODEL_ROTATED));
    }

    /** rotates part of molecule around bond defined through atom (full) names one and two */
    public void rotateAroudBond(String name1, String name2, double angle) throws Object3DGraphControllerException {
	Object3D obj1 = Object3DTools.findByFullName(graph, name1);
	if (obj1 == null) {
	    throw new Object3DGraphControllerException("Could not find object with name: " + name1);
	}
	Object3D obj2 = Object3DTools.findByFullName(graph, name2);
	if (obj2 == null) {
	    throw new Object3DGraphControllerException("Could not find object with name: " + name2);
	}
	if (obj1 instanceof Atom3D) {
	    throw new Object3DGraphControllerException("Object 1 has to be of type atom: " + name1);
	}
	if (obj2 instanceof Atom3D) {
	    throw new Object3DGraphControllerException("Object 2 has to be of type atom: " + name2);
	}
	Object3D parent1 = Object3DTools.findAncestorByClassName(obj1, "RnaStrand");
	Object3D parent2 = Object3DTools.findAncestorByClassName(obj2, "RnaStrand");
	if ((parent1 == parent2) && (parent1 instanceof RnaStrand)) {
	    try {
		MoleculeTools.rotateAroundBond((Molecule3D)parent1, (Atom3D)obj1, (Atom3D)obj2, angle);
	    }
	    catch (RnaModelException rne) {
		throw new Object3DGraphControllerException(rne.getMessage());
	    }
	    finally {
		refresh(new ModelChangeEvent(this, eventConst.MODEL_ROTATED));
	    }
	}
	else {
	    throw new Object3DGraphControllerException("Atoms definining rotation must currently be of same RNA strand: " + name1 + " " 
						       + name2);
	}
    }
    
    /** rotate select subtree such than objects 1 and 2 point in direction */
    public void alignObject(String name1, String name2, Vector3D direction) throws Object3DGraphControllerException {
	if (direction.length() == 0.0) {
	    throw new Object3DGraphControllerException("Direction vector must be non-zero!");
	}
	Object3D obj1 = Object3DTools.findByFullName(graph, name1);
	if (obj1 == null) {
	    throw new Object3DGraphControllerException("Could not find object with name: " + name1);
	}
	Object3D obj2 = Object3DTools.findByFullName(graph, name2);
	if (obj2 == null) {
	    throw new Object3DGraphControllerException("Could not find object with name: " + name2);
	}
	Vector3D center = obj1.getPosition();
	Vector3D diff = obj2.getPosition().minus(obj1.getPosition());
	double angle = direction.angle(diff);
	Vector3D axis = diff.cross(direction); // direction.cross(diff); // TODO : should it be the other way around?
	RotationDescriptor rotation = new SimpleRotationDescriptor(axis, center, angle);
	rotateSelected(rotation);
	refresh(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED));
    }

    /** alignes tree such that orientation of orientable object is normalized */
    public void alignOrientation(String name) throws Object3DGraphControllerException {
	Object3D obj1 = Object3DTools.findByFullName(graph, name);
	if (obj1 == null) {
	    throw new Object3DGraphControllerException("Could not find object with name: " + name);
	}
	if (! (obj1 instanceof CoordinateSystemObject3D) ) {
	    throw new Object3DGraphControllerException("Object carries no direction information: " + name);
	}
	Object3D obj = getSelectionRoot();
	if (obj == null) {
	    obj = getGraph(); // use graph root if noting is selected
	}
	obj.passiveTransform3(((CoordinateSystemObject3D)obj1).getCoordinateSystem());
	refresh(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED));
    }

    /** rotates selected object around axis and angle */
    public void rotateSelected(RotationDescriptor rotator) {
	Object3D obj = getSelectionRoot();
	if ((obj != null) && (obj.isSelected())) {
	    obj.rotate(rotator.getCenter(),
		       rotator.getAxis(),
		       rotator.getAngle());
	}
	refresh(new ModelChangeEvent(this, eventConst.MODEL_ROTATED));
    }

    /** rotates selected object around axis and angle */
    public void rotateSelected(double ax, double ay, double az, double angle) {
	Object3D obj = getSelectionRoot();
	if ((obj != null) && (obj.isSelected())) {
	    RotationDescriptor rotator = new SimpleRotationDescriptor(new Vector3D(ax, ay, az), obj.getPosition(), angle);
	    rotateSelected(rotator);
	} else {
	    log.warning("Internal error: could not rotate object - no object chosen.");
	}
    }

    /** rotates selected object around axis and angle */
    public void rotateSelected(Vector3D axis, Vector3D center, double angle) {
	RotationDescriptor rotator = new SimpleRotationDescriptor(axis, center, angle);
	rotateSelected(rotator);
    }

    /** rotates selected objects around axis and angle and center defined by object name */
    public void rotateSelected(Vector3D axis, String centerObjectName, double angle) throws Object3DGraphControllerException {
	Object3D obj = Object3DTools.findByFullName(graph, centerObjectName);
	if (obj == null) {
	    throw new Object3DGraphControllerException("Could not find object: " + centerObjectName);
	}
	rotateSelected(axis, obj.getPosition(), angle);
    }

    /** rotates selected object around axis and angle */
    public void twistSelected(RotationDescriptor rotator) {
	Object3D obj = getSelectionRoot();
	if ((obj != null) && (obj.isSelected())) {
	    obj.twist(rotator.getCenter(),
		      rotator.getAxis(),
		      rotator.getAngle());
	}
	refresh(new ModelChangeEvent(this, eventConst.MODEL_ROTATED));
    }

    /** rotates selected object around axis and angle */
    public void twistSelected(Vector3D axis, Vector3D center, double angle) {
	RotationDescriptor rotator = new SimpleRotationDescriptor(axis, center, angle);
	twistSelected(rotator);
    }

    /** rotates selected object around axis and angle */
    public void twistSelected(double ax, double ay, double az, double angle) {
	Object3D obj = getSelectionRoot();
	RotationDescriptor rotator = new SimpleRotationDescriptor(new Vector3D(ax, ay, az), obj.getPosition(), angle);
	twistSelected(rotator);
    }

    /** rotates selected objects around axis and angle and center defined by object name */
    public void twistSelected(Vector3D axis, String centerObjectName, double angle) throws Object3DGraphControllerException {
	Object3D obj = Object3DTools.findByFullName(graph, centerObjectName);
	if (obj == null) {
	    throw new Object3DGraphControllerException("Could not find object: " + centerObjectName);
	}
	twistSelected(axis, obj.getPosition(), angle);
    }

    public void setDepthMax(int n) {
	depthMax = n;
	if (iterator != null) {
	    iterator.setDepthMax(n);
	}
	// TODO: event description not totally correct...
	refresh(new ModelChangeEvent(this, eventConst.MODEL_SELECTION_CHANGED));
    }
	
    public void setGraph(Object3D g) {
	graph = g;
	selectionCursor = graph;
	selectionRoot = null; // nothing selected so far
	refresh(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED));
    }
	
    public void setSelectionCursor(Object3D cursor) {
// 	Object3DTreeSelectionListener tsl = new Object3DTreeSelectionListener();
// 	if (tsl == cursor) {
// 	    Object3DTreePanel.addTreeSelectionListener(listener);
// 	    SolidGeometryPainter painter = new SolidGeometryPainter();
// 	    painter.paintPoint(g, point).setAppearance(Color.CYAN);
// 	}
	this.selectionCursor = cursor;
    }

    /** translates the current graph */
    public void translate(Vector3D v) {
	if (graph != null) {
	    graph.translate(v);
	}
       	refresh(new ModelChangeEvent(this, eventConst.MODEL_TRANSLATED));
    }

    /** applies the n'th symmetry operation to object and subtree. Fixit: no way to revert to initial position */
    public void applySymmetry(String rootName, int symId) throws Object3DGraphControllerException {
	Object3D tree = findByFullName(rootName);
	if (tree == null) {
	    throw new Object3DGraphControllerException("Could not find object: " + rootName);
	} else {
	    Object3DTools.applySymmetry(tree, symId);
	}
       	refresh(new ModelChangeEvent(this, eventConst.MODEL_TRANSLATED));
    }

    /** Returns mode used for sequence naming */
    public int getRenameSequenceMode() {
	return renameSequenceMode;
    }

    /** Returns sequence renaming mode */
    public void setRenameSequenceMode(int mode) {
	this.renameSequenceMode = mode; 
    }

    /** translates the current graph */
    public void translateSelected(Vector3D v) {
	if (graph != null) {
	    Object3DTranslator translatorAction = new Object3DTranslator(v);
	    // only apply action to roots of selected objects:
	    Object3DAction selectedAction = new Object3DFirstSelectedWrapperAction(translatorAction);
	    Object3DActionVisitor actionVisitor = new Object3DActionVisitor(graph, selectedAction);
	    actionVisitor.nextToEnd(); // apply to all nodes (improvement: only apply to roots )
	}
       	refresh(new ModelChangeEvent(this, eventConst.MODEL_TRANSLATED));
    }

    /** unfolds all strands using very rudimentary collision check */
    public Properties unfoldStrands() {
	RnaFolder unfolder = new RnaUnfolder();
	Properties result = unfolder.fold(getGraph());
	refresh(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED));
	return result;
    }
    
    /** write tree in format given by writer */
    public void write(OutputStream os, Object3DWriter writer) {
	writer.write(os, graph);
    }

    /** write selected tree in format given by writer */
    public void writeSelected(OutputStream os, Object3DWriter writer) {
	if (selectionRoot != null) {
	    writer.write(os, selectionRoot);
	}
    }
}
