package rnadesign.rnacontrol;

import Jama.*;
import java.awt.Color; // TODO : not clean, should not depend on awt package!
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.text.ParseException;
import java.util.*;
import java.util.logging.Logger;

import controltools.ModelChangeEvent;
import controltools.ModelChangeListener;
import controltools.ModelChanger;
import generaltools.AlgorithmFailureException;
import generaltools.ConstraintDouble;
import generaltools.SimpleConstraintDouble;
import generaltools.DebugTools;
import generaltools.Optimizer;
import generaltools.ParsingException;
import generaltools.PropertyTools;

import generaltools.*;
import launchtools.Job;
import launchtools.QueueManager;
import launchtools.RunCommand;
import launchtools.SimpleQueueManager;
import launchtools.SimpleRunCommand;
import numerictools.MonteCarloOptimizer;
import numerictools.OptimizationNDResult;
import rnadesign.rnamodel.*;
import rnasecondary.*;
import sequence.DnaTools;
import sequence.DuplicateNameException;
import sequence.Sequence;
import sequence.Residue;
import sequence.SimpleUnevenAlignment;
import sequence.UnevenAlignment;
import sequence.UnknownSymbolException;
import sequence.SimpleSequence;
import sequence.LetterSymbol;
import sequence.SimpleLetterSymbol;
import symmetry.*;
import graphtools.*;
import tools3d.*;
import tools3d.geometry.*;
import tools3d.objects3d.*;
import tools3d.objects3d.modeling.*;

// import SecondaryStructureDesign.*;
import secondarystructuredesign.*;

import java.util.Set;
import java.util.Iterator;

import rnadesign.designapp.NanoTilerScripter;

import static rnadesign.rnacontrol.PackageConstants.*;
import static rnadesign.rnacontrol.Object3DGraphControllerConstants.*;

public class Object3DGraphController implements ModelChanger,
						ModelChangeListener {

    public static String nanotilerHome = System.getenv(NANOTILER_HOME_VAR);
    private static Logger log = Logger.getLogger(LOGFILE_DEFAULT);
    private static ResourceBundle rb = ResourceBundle.getBundle("Controller");
    // private boolean forbidRinglessCollisionMode = false; // if true, abort growth process once a collision without ring formation is detected
    /** name of RNAinverse wrapper script */
    public static final String rnaInverseScriptName = rb.getString("rnaInverseScript"); // "risotto.pl"; // "RNAinverseSequenceOptimizer.pl";
    public static final String ccp4PdbsetBinary = rb.getString("ccp4PdbsetBinary"); // usally: pdbset, should be in PATH 
    private static final Object3DGraphControllerEventConstants eventConst = new Object3DGraphControllerEventConstants();
    private BridgeItController bridgeController;
    private SymmetryController symmetryController = new SymmetryController();
    private Color defaultColor = Color.BLUE;
    private Color[] depthColors = {Color.RED, Color.YELLOW, Color.GREEN, Color.BLUE};
    private Object3DController graph = new Object3DController();
    private int gridShapeMode = GRID_SHAPE_UNIT;
    private int symmetryMode = SYMMETRY_DEFAULT;
    private Appearance selectionCursorAppearance = new Appearance(Color.WHITE);
    private CorridorDescriptor corridorDescriptor = new CorridorDescriptor(CORRIDOR_DEFAULT_RADIUS, CORRIDOR_DEFAULT_START);
    private double stemFitRmsTolerance = 5.0; //2.5;
    private double selectionCursorRadius = 3.0;
    private Appearance selectionRootAppearance = new Appearance(Color.CYAN);
    private double selectionRootRadius = 2.0;
    private Vector3D[] points; // temporay array
    private List<ModelChangeListener> modelChangeListeners = new ArrayList<ModelChangeListener>();
    private FragmentGridTiler fgTiler = new FragmentGridTiler(); 
    private ForceFieldFactory forceFieldFactory = new RnaForceFieldFactory();
    private int importStemLengthMin= PDB_IMPORT_STEM_LENGTH_MIN;
    private SequenceController sequences = new SimpleSequenceController();
    private BindingSiteController bindingSites = new SimpleBindingSiteController();
    private LinkController links = new SimpleLinkController();
    private BasePairController basePairDB = new SimpleBasePairController(); // database of all defined base pair 3D structures
    private Shape3DSet shapeSet = null; // no implementation exists yet!
    private Shape3DSetFactory shapeSetFactory = new SimpleShapeSetFactory();
    private SpaceGroup spaceGroup = new DefaultSpaceGroup();
    private int spaceGroupSymmetryCount = -1;
    private final SpaceGroupFactory spaceGroupFactory = new Ccp4SpaceGroupFactory();
    private Cell cell = new DefaultCell();
    private LatticeSection latticeSection = new LatticeSection(0, 0, 0, 1,1,1);
    private Kaleidoscope kaleidoscope = new CellKaleidoscope(cell, latticeSection);
    private Object3D nucleotideDB = null;
    private String lastReadDirectory = ".";
    private String lastWriteDirectory = ".";
    // private StrandJunctionDB strandJunctionDB = new SimpleStrandJunctionDB(); // not enough, if used "read" method has be be called
    // private String strandJunctionDBName = "strandjunctiondb.dat";
    private PdbJunctionController junctionController = new PdbJunctionController();
    private double atomBondCutoff = 1.5;//used for generating covalent bonds between atoms within the cutoff range
    private List<Object3D> trash = new ArrayList<Object3D>(); // for deletion
    
    // private Logger dbLog = DebugTools.getLogger(); // obtain debug logger

    /**
     * Default constructor
     * TODO : initialize shape set and shapeSetFactory!
     */
    public Object3DGraphController() {
	// sequences = new ArrayList();
	// bindingSites = new ArrayList();
	// links = new SimpleLinkSet();
	graph.addModelChangeListener(this);
	links.addModelChangeListener(this);
	graph.addModelChangeListener(sequences);
	links.addModelChangeListener(sequences);
    }

    /** Attempts to add a missing atom to a nucleotide. Implemented is "P" and "O3*". Currently not implemented is adding a link. */    
    public void addAtom(String nucleotideName, String atomName) throws Object3DGraphControllerException {
	getGraph().addAtom(nucleotideName, atomName);
	// FIXIT: add link to atom
	refresh(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED));
    }

    /** adds objects and links */
    public void addBundle(Object3DLinkSetBundle bundle) {
	graph.addGraph(bundle.getObject3D());
	links.addLinks(bundle.getLinks());
    }
    
    public void emptyTrash() {
	for (Object3D obj: trash) {
	    remove(obj);
	}
	trash.clear();
    }

    /** generates a new link between two objects. Allowed values for linkTypeName: null or empty, "simple" */
   public void generateLink(String name1, String name2, String linkTypeName) throws
      Object3DGraphControllerException {
	Object3D obj1 = graph.findByFullName(name1);
	Object3D obj2 = graph.findByFullName(name2);
	if (obj1 == null) {
	    throw new Object3DGraphControllerException("Could not find object with name: " + name1);
	}
	if (obj2 == null) {
	    throw new Object3DGraphControllerException("Could not find object with name: " + name2);
	}
	if (obj1 == obj2) {
	    throw new Object3DGraphControllerException("Currently all links have to connect two different objects.");
	}
	links.add(new SimpleLink(obj1, obj2));
    }

    /** adds a base pair to a secondary structure. Does not move coordinates. */
     public void addBasePair(String objectName1, String objectName2,
			     int interactionTypeId, int offset, int symId1, int symId2) throws Object3DGraphControllerException {
 	addBasePair(objectName1, objectName2, interactionTypeId, offset);
     }

    /** adds a base pair to a secondary structure. Does not move coordinates. Uses potential offset of base index */
    public void addBasePair(String objectName1, String objectName2,
			    int interactionTypeId, int offset) throws Object3DGraphControllerException {
	Object3D obj1 = Object3DTools.findByFullName(graph.getGraph(), objectName1);
	Object3D obj2 = Object3DTools.findByFullName(graph.getGraph(), objectName2);
	if (obj1 == null) {
	    throw new Object3DGraphControllerException("Cannot find object: " + objectName1);
	}
	if (obj2 == null) {
	    throw new Object3DGraphControllerException("Cannot find object: " + objectName2);
	}
	if (obj1.getParent() == null) {
	    throw new Object3DGraphControllerException("Cannot find object parent: " + objectName1);
	}
	if (obj2.getParent() == null) {
	    throw new Object3DGraphControllerException("Cannot find object parent: " + objectName2);
	}
	if (offset != 0) {
	    if ((obj1.getSiblingId() + offset) >= obj1.getParent().size()) {
		throw new Object3DGraphControllerException("Illegal object index: " + obj2.getFullName());
	    }
	    if ((obj2.getSiblingId() - offset) < 0) {
		throw new Object3DGraphControllerException("Illegal object index: " + obj2.getFullName());
	    }
	    obj1 = obj1.getParent().getChild(obj1.getSiblingId() + offset);
	    obj2 = obj2.getParent().getChild(obj2.getSiblingId() - offset);
	}
	if (! ( obj1 instanceof Residue3D)) {
	    throw new Object3DGraphControllerException("Specified object is not a residue: " + objectName1);
	}
	if (! ( obj2 instanceof Residue3D)) {
	    throw new Object3DGraphControllerException("Specified object is not a residue: " + objectName2);
	}
	Residue3D res1 = (Residue3D)obj1;
	Residue3D res2 = (Residue3D)obj2;
	RnaInteractionType interactionType = new RnaInteractionType(interactionTypeId);
	Interaction interaction = new SimpleInteraction(res1, res2, interactionType);
	InteractionLink interactionLink = new InteractionLinkImp(res1, res2, interaction);
	int numBPOrig = links.getHydrogenBondInteractions().size();
	links.add(interactionLink);
	assert links.getHydrogenBondInteractions().size() == numBPOrig + 1;
	refresh(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED));
    }

    /** helical constraint between two branch descriptors */
    public void addHelixConstraint(String objectName1, String objectName2,
				   int basePairMin, int basePairMax, double rms,
				   int symId1, int symId2, String name) throws Object3DGraphControllerException {
	Object3D obj1 = Object3DTools.findByFullName(graph.getGraph(), objectName1);
	Object3D obj2 = Object3DTools.findByFullName(graph.getGraph(), objectName2);
	//List<Object3D> branches;
	
	if (obj1 == null) {
	    obj1 = findBranchDescriptor(objectName1);
	}
	if (obj1 == null) {
	    throw new Object3DGraphControllerException("Cannot find helix descriptor with name " + objectName1);
	}
	if (obj2 == null) {
	    obj2 = findBranchDescriptor(objectName2);
	}
	if (obj2 == null) {
	    throw new Object3DGraphControllerException("Cannot find helix descriptor with name " + objectName2);
	}
	if (obj1 == obj2) {
	    throw new Object3DGraphControllerException("Cannot place helix constraint between idential objects with names " + objectName1 + " " + objectName2);
	}
	links.addHelixConstraintLink(obj1, obj2, basePairMin, basePairMax, rms, symId1, symId2, name);
	//System.out.println(links.toPrettyString());
	graph.fireModelChanged(new ModelChangeEvent(this));
    }

    /** Returns branch descriptor based on a simpler name specifying the residue and id */
    private Object3D findBranchDescriptor(String objectName1) throws Object3DGraphControllerException{
		List<Object3D> branches;
		Object3D obj1 = null;
		try {
	    	branches = Object3DTools.collectByClassName(graph.getGraph(),"BranchDescriptor3D").getAsList();
	    	List<Residue3D> residues =  sequences.collectResidues(objectName1);
	    assert residues.size()==1;
	    assert residues.get(0).getClassName().equals("Nucleotide3D");
	    Residue3D firstRes = residues.get(0);
	    Vector3D firstPos = firstRes.getPosition();
	    //System.out.println("base position: "+ firstRes);
	    //log.info("calling "+ objectName1 + "\nResidue: "+ firstRes); 
	    //List<Object3D> branches = Object3DTools.collectByClassName(graph.getGraph(),"BranchDescriptor3D").getAsList();
	    //Collections.reverse(branches);
	    int index = 0;
	    BranchDescriptor3D bd = null;
	    boolean found = false;
	    String[] bps = objectName1.split(":");
	    
	    System.out.println("Base: "+bps[0]);
	    System.out.println("num: " +bps[1]);
	    System.out.println(bps[0]+"_"+bps[1]);
	      
	    System.out.println("Branches Found: ");
	    for(int i=0; i < branches.size(); i++){
		Object3D obj = branches.get(i);
		assert obj.getClassName().equals("BranchDescriptor3D");
		System.out.println(obj.getFullName()+"\ttype: " + branches.get(i).getClassName());
		// System.out.println(branches.get(i).infoString()+"\ttype: " + branches.get(i).getClassName());
	    }	
	    System.out.println("DONE");
	    
	    while(index < branches.size() && obj1 == null){
		Object3D oj = branches.get(index);
		System.out.println(oj.getFullName());
		//System.out.println(oj.infoString().contains(bps[0]+"_"+bps[1]));
		
		//WARNING: _cov_jnc hard coded
		if((oj instanceof BranchDescriptor3D) && oj.getFullName().contains("_cov_jnc")&& oj.getFullName().contains(bps[0])){
		    bd = (BranchDescriptor3D) oj;
		    System.out.println(bd.getFullName());
		    
		    //log.info("Checking helix descriptor: "+bd.fullString());
		    Nucleotide3D nucIn = (Nucleotide3D)(bd.getIncomingStrand().getResidue(bd.getIncomingIndex()));
		    Nucleotide3D nucOut =(Nucleotide3D)(bd.getOutgoingStrand().getResidue(bd.getOutgoingIndex()));
		    //log.info("Comparing to residue: " + nucIn.infoString());
		    //log.info("Specified residue position: " + firstPos);
		    //System.out.println("Distance Incoming Residue: " + firstRes.distance(nucIn)+" positions: "+ firstRes.getPosition() + " nucIn:" + nucIn.getPosition());
		    if(nucIn.getPosition().similar(firstPos)){
			//System.out.println("Found similarity with incoming nucleotide!");
			obj1 = bd;
			found = true;
		    } else {
			//System.out.println("Distance Outgoing residue: " + firstRes.distance(nucIn) + " positions: "+ firstRes.getPosition() + " " + nucOut.getPosition());
			//System.out.println("Comparing to residue: " + nucOut.infoString());
			if(nucOut.getPosition().similar(firstPos)){
			    //System.out.println("Found similarity with outgoing nucleotide!");
			    obj1 = bd;
			    found = true;
			}
		    }
		}
		index++;
	    }
	    if(obj1==null){
		System.out.println("looking again");
		index = 0;
		while(index < branches.size() && obj1 == null){
		    Object3D oj = branches.get(index);
		    System.out.println(oj.getFullName());
		    //System.out.println(oj.infoString().contains(bps[0]+"_"+bps[1]));
		    
		    //WARNING: _cov_jnc hard coded
		    if((oj instanceof BranchDescriptor3D) && oj.getFullName().contains(bps[0])){
			bd = (BranchDescriptor3D) oj;
			System.out.println(bd.getFullName());

			//log.info("Checking helix descriptor: "+bd.infoString());
			Nucleotide3D nucIn = (Nucleotide3D)(bd.getIncomingStrand().getResidue(bd.getIncomingIndex()));
			Nucleotide3D nucOut =(Nucleotide3D)(bd.getOutgoingStrand().getResidue(bd.getOutgoingIndex()));
			//log.info("Comparing to residue: " + nucIn.infoString());
			//log.info("Specified residue position: " + firstPos);
			//System.out.println("Distance Incoming Residue: " + firstRes.distance(nucIn)+" positions: "+ firstRes.getPosition() + " nucIn:" + nucIn.getPosition());
			if(nucIn.getPosition().similar(firstPos)){
			    System.out.println("Found similarity with incoming nucleotide!");
			    obj1 = bd;
			    found = true;
			} else {
			    //System.out.println("Distance Outgoing residue: " + firstRes.distance(nucIn) + " positions: "+ firstRes.getPosition() + " " + nucOut.getPosition());
			    //System.out.println("Comparing to residue: " + nucOut.infoString());
			    if(nucOut.getPosition().similar(firstPos)){
				System.out.println("Found similarity with outgoing nucleotide!");
				obj1 = bd;
				found = true;
			    }
			}
		    }
		    index++;
		}
	    }
	    if(obj1==null){
		//assert (!found);
		//throw new Object3DGraphControllerException("Cannot find first object: " + objectName1);

	    } else {
		assert found;
	    }
	} catch (ParsingException e) {
	    throw new Object3DGraphControllerException("Cannot parse first object: " + objectName1);
	}
	return obj1;
    }

    
    /** helical constraint between two branch descriptors */
    public ConstraintLink addDistanceConstraint(String objectName1, String objectName2,
						double min, double max, int symId1, int symId2) throws Object3DGraphControllerException {
	Object3D obj1 = Object3DTools.findByFullName(graph.getGraph(), objectName1);
	Object3D obj2 = Object3DTools.findByFullName(graph.getGraph(), objectName2);
	List<Object3D> branches;
	if(obj1 == null){
		obj1 = findBranchDescriptor(objectName1);
	}
	if(obj2 == null){
		obj2 = findBranchDescriptor(objectName2);
	}

	ConstraintLink newLink = links.addDistanceConstraintLink(obj1, obj2, min, max, symId1, symId2);
	graph.fireModelChanged(new ModelChangeEvent(this));
	return newLink;
    }

    /** helical constraint between two branch descriptors */
    public TorsionLink addTorsionConstraint(String objectName1, String objectName2,
					    String objectName3, String objectName4,
					    double min, double max) throws Object3DGraphControllerException {
	Object3D obj1 = Object3DTools.findByFullName(graph.getGraph(), objectName1);
	Object3D obj2 = Object3DTools.findByFullName(graph.getGraph(), objectName2);
	Object3D obj3 = Object3DTools.findByFullName(graph.getGraph(), objectName3);
	Object3D obj4 = Object3DTools.findByFullName(graph.getGraph(), objectName4);
	if (obj1 == null) {
	    throw new Object3DGraphControllerException("Cannot find object: " + objectName1);
	}
	if (obj2 == null) {
	    throw new Object3DGraphControllerException("Cannot find object: " + objectName2);
	}
	if (obj3 == null) {
	    throw new Object3DGraphControllerException("Cannot find object: " + objectName3);
	}
	if (obj4 == null) {
	    throw new Object3DGraphControllerException("Cannot find object: " + objectName4);
	}
	TorsionLink newLink = links.addTorsionConstraintLink(obj1, obj2, obj3, obj4, min, max);
	graph.fireModelChanged(new ModelChangeEvent(this));
	return newLink;
    }

    /** adds a special link that encodes a junctions by list of corresponding 3' and 5' residues */
    public MultiConstraintLink addJunctionMultiConstraintLink(String name, 
							      List<Nucleotide3D> fivePrimeResidues, List<Nucleotide3D> threePrimeResidues,
							      ConstraintDouble constraint,
							      ConstraintDouble bridgableConstraint,
							      List<Integer> symIds) throws Object3DGraphControllerException {
	MultiConstraintLink newLink = null;
	try {
	    newLink = getLinks().addJunctionMultiConstraintLink(name, fivePrimeResidues, threePrimeResidues, constraint,
								bridgableConstraint, symIds);
	    log.info("Adding junction link: " + newLink.toString());
	} catch (RnaModelException rme) {
	    throw new Object3DGraphControllerException(rme.getMessage());
	}
	graph.fireModelChanged(new ModelChangeEvent(this));
	return newLink;
    }

    /** adds a special link that encodes a junctions by list of corresponding 3' and 5' residues */
    public JunctionDBConstraintLink addJunctionDBConstraintLink(String name, 
								List<String> branchNames,
								boolean kissingLoopMode) throws Object3DGraphControllerException {
	JunctionDBConstraintLink newLink = null;
	List<BranchDescriptor3D> branches = new ArrayList<BranchDescriptor3D>();
	log.info("Added junctionDBConstraintLink of size " + branchNames.size());
	try {
	    for (int i = 0; i < branchNames.size(); ++i) {
		Object3D obj = getGraph().findByFullName(branchNames.get(i));		
		if ((obj != null) && (obj instanceof BranchDescriptor3D)) {
		    branches.add((BranchDescriptor3D)(obj));
		    log.info("Added branch " + branchNames.get(i));
		} else {
		    throw new Object3DGraphControllerException("The name " + branchNames.get(i) 
							       + " does not point to a helix descriptor (BranchDescriptor3D class)");
		}
	    }
	    assert(junctionController != null);
	    if (kissingLoopMode) {
		links.addJunctionDBConstraintLink(name, branches, junctionController.getKissingLoopDB());
	    } else {
		links.addJunctionDBConstraintLink(name, branches, junctionController.getJunctionDB());
	    }
	} catch (RnaModelException rme) {
	    throw new Object3DGraphControllerException(rme.getMessage());
	}
	graph.fireModelChanged(new ModelChangeEvent(this));
	return newLink;
    }

    /** adds a special link that encodes a junctions by list of corresponding 3' and 5' residues */
    public MultiConstraintLink addJunctionMultiConstraintLink(String name,
							      List<String> fivePrimeResidueNames, List<String> threePrimeResidueNames,
							      double min, double max, double minBridge, double maxBridge,
							      List<Integer> symIds) throws Object3DGraphControllerException {
	assert (fivePrimeResidueNames.size() == threePrimeResidueNames.size());
	List<Nucleotide3D> fivePrimeResidues  = new ArrayList<Nucleotide3D>();
	List<Nucleotide3D> threePrimeResidues  = new ArrayList<Nucleotide3D>();
	for (int i = 0; i < fivePrimeResidueNames.size(); ++i) {
	    Object3D obj5 = getGraph().findByFullName(fivePrimeResidueNames.get(i));
	    if (obj5 == null) {
		throw new Object3DGraphControllerException("Could not find object: " + fivePrimeResidueNames.get(i));
	    }
	    if (! (obj5 instanceof Nucleotide3D) ) {
		throw new Object3DGraphControllerException("5' residue does not have type Nucleotide3D: " + fivePrimeResidueNames.get(i) + " " + obj5.toString());
	    }
	    Object3D obj3 = getGraph().findByFullName(threePrimeResidueNames.get(i));
	    if (obj3 == null) {
		throw new Object3DGraphControllerException("Could not find object: " + threePrimeResidueNames.get(i));
	    }
	    if (! ( obj3 instanceof Nucleotide3D ) ) {
		throw new Object3DGraphControllerException("3' residue does not have type Nucleotide3D: " + threePrimeResidueNames.get(i) + " : " + obj3.toString());
	    }
	    fivePrimeResidues.add((Nucleotide3D)obj5);
	    threePrimeResidues.add((Nucleotide3D)obj3);
	}
	ConstraintDouble bridgeConstraint = null;
	if (maxBridge > minBridge) {
	    bridgeConstraint = new SimpleConstraintDouble(minBridge, maxBridge);
	}
	return addJunctionMultiConstraintLink(name, fivePrimeResidues, threePrimeResidues, new SimpleConstraintDouble(min, max),
					      bridgeConstraint, symIds);
    }
		
		public MotifConstraintLink addMotifConstraint(String objectName1, String objectName2, int length) throws Object3DGraphControllerException {
	Object3D obj1 = graph.findByFullName(objectName1);
	Object3D obj2 = graph.findByFullName(objectName2);
	if(obj1 == null){
		obj1 = findBranchDescriptor(objectName1);
	}
	if(obj2 == null){
		obj2 = findBranchDescriptor(objectName2);
	}

	List<String> dbFileNames = new ArrayList<String>();
	dbFileNames.add( nanotilerHome + SLASH + rb.getString("bridgedb") );

	MotifConstraintLink newLink = links.addMotifConstraintLink(obj1, obj2, length, dbFileNames);
	graph.fireModelChanged(new ModelChangeEvent(this));
	return newLink;
		}
		
		public SuperimposeConstraintLink addSuperimposeConstraint(String objectName1, String objectName2) throws Object3DGraphControllerException {
	Object3D obj1 = graph.findByFullName(objectName1);
	Object3D obj2 = graph.findByFullName(objectName2);
	if(obj1 == null){
		obj1 = findBranchDescriptor(objectName1);
	}
	if(obj2 == null){
		obj2 = findBranchDescriptor(objectName2);
	}

	SuperimposeConstraintLink newLink = links.addSuperimposeConstraintLink(obj1, obj2);
	graph.fireModelChanged(new ModelChangeEvent(this));
	return newLink;
		}
		
		public BlockCollisionConstraintLink addBlockCollisionConstraint(String objectName1, String objectName2) throws Object3DGraphControllerException {
	Object3D obj1 = graph.findByFullName(objectName1);
	Object3D obj2 = graph.findByFullName(objectName2);
	if(obj1 == null){
		obj1 = findBranchDescriptor(objectName1);
	}
	if(obj2 == null){
		obj2 = findBranchDescriptor(objectName2);
	}

	BlockCollisionConstraintLink newLink = links.addBlockCollisionConstraintLink(obj1, obj2);
	graph.fireModelChanged(new ModelChangeEvent(this));
	return newLink;
		}

    /** Clones 3D object (subtree with name origFullName), inserts cloned subtree under object with name newParentFullName
     * and gives it the name newIndividualName.
     */
    public void cloneObject(String origFullName, String newParentFullName, String newIndividualName, int symId) throws Object3DGraphControllerException {
	graph.cloneObject(origFullName, newParentFullName, newIndividualName, symId);
	int numInteractions = links.size();
	String newFullName = newParentFullName + "." + newIndividualName;
	Object3D origTree = graph.getGraph(origFullName);
	Object3D clonedTree = graph.getGraph(newFullName);
	if (origTree == null) {
	    throw new Object3DGraphControllerException("Object3DGraphController: Could not find original object with name: " 
						       + newParentFullName);
	}
	if (clonedTree == null) {
	    throw new Object3DGraphControllerException("Object3DGraphController: Could not find cloned object with name: " 
						       + newFullName);
	}
	LinkSet clonedLinks = links.cloneLinks(origTree, clonedTree);
	links.merge(clonedLinks);
	// log.fine("Number of original, cloned and final links: " + numInteractions + " , " + clonedLinks.size() 
	// + " " + links.size());
	// int numInteractions2 = generateSecondaryStructure().getInteractions().size();
	// assert numInteractions2 >= numInteractions; // must be more or equal size of base pairs
    }

    /** multiplies object and links according to grid points */
    public void addCloneGrid(Object3D toBeCloned,
			     Object3D gridRoot) {
	for (int i = 0; i < gridRoot.size(); ++i) {
	    Object3DLinkSetBundle cloneBundle = Object3DLinkSetBundleTools.generateClone(toBeCloned, links);
	    Object3D obj = cloneBundle.getObject3D();
	    obj.setPosition(gridRoot.getChild(i).getPosition());
	    addBundle(cloneBundle);
	}
    }
    
    /** adds a grid to the objects, optionally adds covering stems */
    public void addCubicGrid(Vector3D startPos,
			     Vector3D v1, Vector3D v2, Vector3D v3, 
			     int n1, int n2, int n3,
			     String name,
			     Object3D toBeClonedObject,
			     boolean addGridFlag,
			     boolean addStemsFlag,
			     char sequenceChar,
			     boolean onlyFirstPathMode,
			     int tilerAlgorithm)
	throws Object3DGraphControllerException {
	Object3DLinkSetBundle gridBundle = Grid3DTools.generateCubicGrid(startPos, v1, v2, v3, n1, n2, n3, name);
	if (addGridFlag) {
	    addBundle(gridBundle);
	}
	if (toBeClonedObject != null) {
	    addCloneGrid(toBeClonedObject, gridBundle.getObject3D());
	}
	if (addStemsFlag) {
	    String nameBase = name + "_cov";
	    Object3DLinkSetBundle coveringStems = generateCoveringStems(gridBundle, nameBase, onlyFirstPathMode, sequenceChar, tilerAlgorithm);
	    addBundle(coveringStems);
	}
    }
    
    /** adds a geometry (like a tetrahedron) to the objects */
    public void addGeometry(Vector3D startPos,
			    double length,
			    String name,
			    String childNameBase,
			    boolean addStemsFlag,
			    char sequenceChar,
			    boolean onlyFirstPathMode,
			    int geometryCode,
			    int tilerAlgorithm)
	throws Object3DGraphControllerException {
	assert childNameBase != null;
	Geometry geometry = null;
	if ((geometryCode > 100) && (geometryCode < 200)) {
	    // code 103 stands for triangle and so forth
	    geometry = PlanarTools.generateNtagon(geometryCode-100, length);
	}
	else if ((geometryCode > 200) && (geometryCode < 300)) {
	    // code 203 stands for triangular prism and so forth
	    geometry = PlanarTools.generatePrism(geometryCode-200, length,
						 length);
	    // TODO : height cannot be specified right now
	}
	else {
	    switch (geometryCode) {
	    case TETRAHEDRON:
		geometry = PlatonicTools.generateTetrahedron(length);
		break;
	    case CUBE:
		geometry = PlatonicTools.generateCube(length);
		break;
	    case OCTAHEDRON:
		geometry = PlatonicTools.generateOctahedron(length);
		break;
	    case DODECAHEDRON:
		geometry = PlatonicTools.generateDodecahedron(length);
		break;
	    case ICOSAHEDRON:
		geometry = PlatonicTools.generateIcosahedron(length);
		break;
	    }
	}
	assert geometry != null;
	Object3DLinkSetBundle bundle = Object3DLinkSetBundleTools.generateBundleFromGeometry(geometry, name, childNameBase, new SimpleObject3D());
	Object3D obj = bundle.getObject3D();
	obj.setPosition(startPos);
 	addBundle(bundle);
	if (addStemsFlag) {
	    String nameBase = name + "_cov";
	    Object3DLinkSetBundle coveringStems = generateCoveringStems(bundle, nameBase, onlyFirstPathMode, sequenceChar, tilerAlgorithm);
 	    addBundle(coveringStems);
	}
    }
    
    /** adds a grid to the objects, optionally adds covering stems */
    public void addGraphitGrid(Vector3D startPos,
			       double sideLength,
			       double height,
			       int n1, int n2,
			       String name, 
			       Object3D toBeClonedObject,
			       boolean addGridFlag, boolean addStemsFlag,
			       char sequenceChar,
			       boolean onlyFirstPathMode,
			       int tilerAlgorithm)
	throws Object3DGraphControllerException {
	Object3DLinkSetBundle gridBundle = Grid3DTools.generateGraphitGrid(startPos, sideLength, height, n1, n2, name);
	if (addGridFlag) {
	    addBundle(gridBundle);
	}
	if (toBeClonedObject != null) {
	    addCloneGrid(toBeClonedObject, gridBundle.getObject3D());
	}
	if (addStemsFlag) {
	    String nameBase = name + "_cov";
	    Object3DLinkSetBundle coveringStems = generateCoveringStems(gridBundle, nameBase, onlyFirstPathMode, sequenceChar, tilerAlgorithm);
	    addBundle(coveringStems);
	}
    }

    public void addModelChangeListener(ModelChangeListener listener) {
	modelChangeListeners.add(listener);
    }

    /**
     * Adds strand with name, sequence at position
     * with certain direction vector.
     */
    public void addStrand(String name,
			  String seq,
			  Vector3D pos,
			  Vector3D dir,
			  int typeId) throws UnknownSymbolException {
	Object3DLinkSetBundle bundle;
	switch (typeId) {
	case RNA: bundle = Rna3DTools.generateSimpleRnaStrand(name, seq,
							      pos, dir);
	    log.info("Generating RNA strand!");
	    break;
	case DNA: bundle = Dna3DTools.generateSimpleDnaStrand(name, seq,
							      pos, dir);
	    log.info("Generating DNA strand!");
	    break;
	case PROTEIN:
	    log.warning("Sorry, cannot generated Protein strand yet!");
	    return; // TODO
	default:
	    return; // do nothing in case of Protein
	}
	graph.addGraph(bundle.getObject3D());
	links.addLinks(bundle.getLinks());
	log.info("the number of links after adding the strand is: " + links);
    }
    
    /** Aligns second structure to first structure */
    private Properties alignStructures(Object3DSet objectBlocks1, Object3DSet objectBlocks2,
				       List<Atom3D> atoms1, List<Atom3D> atoms2, int numberSteps) throws Object3DGraphControllerException {
	assert (atoms1.size() == atoms2.size());
	Vector3D v1 = AtomTools.centerOfMass(atoms1);
	Vector3D v2 = AtomTools.centerOfMass(atoms2);
	Vector3D deltaV = v1.minus(v2);
	log.info("Shifting object blocks 2 by vector: " + deltaV);
	for (int i = 0; i < objectBlocks2.size(); ++i) {
	    objectBlocks2.get(i).translate(deltaV);
	}
	Random rnd = Randomizer.getInstance();
	String nameBase = "tmp." + rnd.nextDouble();
	ConstraintDouble zeroConstraint = new SimpleConstraintDouble(0.0, 0.0); // want zero distance
	LinkSet addedLinks = new SimpleLinkSet();
	for (int i = 0; i < atoms1.size(); ++i) {
	    Link link = new SimpleConstraintLink(atoms1.get(i), atoms2.get(i), zeroConstraint);
	    addedLinks.add(link);
	}
	// optimize constraints;
	List<Object3DSet> objectBlocks = new ArrayList<Object3DSet>();
	objectBlocks.add(objectBlocks1);
	objectBlocks.add(objectBlocks2);
	double errorScoreLimit = 0.01;
	double kt = 0.1;
	BasepairOptimizer optimizer = new BasepairOptimizer(addedLinks, objectBlocks, numberSteps, errorScoreLimit, basePairDB, null,
							    symmetryController.getSymCopies());
	optimizer.setOutputInterval(100000);
	optimizer.setKeepFirstFixedMode(true);
	optimizer.setKT(kt);
	optimizer.setVerboseLevel(0);
	Properties result = optimizer.optimize();
	refresh(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED));
	return result;
    }

    /** Aligns second structure to first structure based on fast coordinate vector alignment */
    private Properties alignStructuresFast(List<Atom3D> atoms1, List<Atom3D> atoms2, 
					   Matrix3D rot, Vector3D mc1, Vector3D mc2 ) {
	assert (atoms1.size() == atoms2.size());
	Vector3D[] v1 = new Vector3D[atoms1.size()];
	Vector3D[] v2 = new Vector3D[atoms2.size()];
	for (int i = 0; i < v1.length; ++i) {
	    v1[i] = atoms1.get(i).getPosition();
	    v2[i] = atoms2.get(i).getPosition();
	}
	Properties resultProp = new Properties();
	double result = Superimpose.superimpose(v1, v2, rot, mc1, mc2);
	resultProp.setProperty("score", "" + result);
	return resultProp;
    }

    public static List<Atom3D> generateAtomList(List<Residue3D> residues, String[] atomNames) throws Object3DGraphControllerException  {
	List<Atom3D> atoms = new ArrayList<Atom3D>();
	for (int i = 0; i < residues.size(); ++i) {
	    for (String name : atomNames) {
		Object3D child = residues.get(i).getChild(name);
		if ((child == null) || (!(child instanceof Atom3D))) {
		    throw new Object3DGraphControllerException("Could not find atom " + name 
							       + " in nucleotide " + residues.get(i).getFullName());
		}
		atoms.add((Atom3D)child);
	    }
	}
	return atoms;
    }
		
		public static List<Residue3D> generateResidueList(Object3D obj){ //dfs for residues
			List<Residue3D> result = new ArrayList<Residue3D>();
			if (obj instanceof Residue3D ){
					result.add((Residue3D)obj);
					assert(result!=null);
					return result;
				}
			for (int i = 0; i < obj.size(); ++i) {
				Object3D c = obj.getChild(i);
					result.addAll( generateResidueList(c) ); //go deeper in object tree
			}
			assert(result != null);
			return result;
		}

    /** Aligns second structure to first structure */
    public Properties alignStructures(Object3DSet objectBlocks1, Object3DSet objectBlocks2,
				      List<Residue3D> residues1, List<Residue3D> residues2, String[] atomNames, int numberSteps) 
	                                                        throws Object3DGraphControllerException {
	if (residues1.size() != residues2.size()) {
	    throw new Object3DGraphControllerException("Number of specified residues must be equal for aligning!");
	}
	List<Atom3D> atoms1 = generateAtomList(residues1, atomNames);
	List<Atom3D> atoms2 = generateAtomList(residues2, atomNames);
	// Properties result = alignStructures(objectBlocks1, objectBlocks2, atoms1, atoms2, numberSteps);
	Matrix3D rot = new Matrix3D();
	Vector3D mc1 = new Vector3D();
	Vector3D mc2 = new Vector3D();
	Properties result = alignStructuresFast(atoms1, atoms2, rot, mc1, mc2);
	for (int i = 0; i < objectBlocks2.size(); ++i) {
	    Object3DTools.applySuperposition(objectBlocks2.get(i), rot, mc1, mc2);
	}
	return result;
    }

    /** Aligns second structure to first structure */
    public Properties alignStructures(Object3DSet objectBlocks1, Object3DSet objectBlocks2,
				      String residueDescriptor1, String residueDescriptor2, String[] atomNames,
				      int numberSteps) 
	throws Object3DGraphControllerException {
	Properties result = null;
	try {
	    List<Residue3D> residues1 = sequences.collectResidues(residueDescriptor1);
	    List<Residue3D> residues2 = sequences.collectResidues(residueDescriptor2);
	    assert residues1 != null;
	    assert residues2 != null;
	    if ((residues1.size() == 0) || (residues1.size() != residues2.size())) {
		throw new Object3DGraphControllerException("Error in residue specification while aligning structures!");
	    }
	    System.out.println("Aligning structures by superposing the following residues:");
	    for (int i = 0; i < residues1.size(); ++i) {
		System.out.println(residues1.get(i).getFullName() + " : " + residues2.get(i).getFullName() + NEWLINE);
	    }
	    result = alignStructures(objectBlocks1, objectBlocks2, residues1, residues2, atomNames, numberSteps);
	} catch (ParsingException pe) {
	    throw new Object3DGraphControllerException(pe.getMessage());
	}
	return result;
    }
		
		/** Aligns second structure to first structure */
		public Properties alignSingleResidueStructures(Object3DSet objectBlocks1, Object3DSet objectBlocks2,
							String residueName1, String residueName2, String[] atomNames,
							int numberSteps) 
	throws Object3DGraphControllerException {
	Properties result = null;
	try {
			Residue3D res1 = (Residue3D)graph.findByFullName(residueName1);
			Residue3D res2 = (Residue3D)graph.findByFullName(residueName2);
			List<Residue3D> residues1 = new ArrayList<Residue3D>();
			List<Residue3D> residues2 = new ArrayList<Residue3D>();
			residues1.add(res1);
			residues2.add(res2);
			assert residues1 != null;
			assert residues2 != null;
			if ((residues1.size() == 0) || (residues1.size() != residues2.size())) {
		throw new Object3DGraphControllerException("Error in residue specification while aligning structures!");
			}
			System.out.println("Aligning structures by superposing the following residues:");
			for (int i = 0; i < residues1.size(); ++i) {
		System.out.println(residues1.get(i).getFullName() + " : " + residues2.get(i).getFullName() + NEWLINE);
			}
			result = alignStructures(objectBlocks1, objectBlocks2, residues1, residues2, atomNames, numberSteps);
	} catch (Exception pe) {
			throw new Object3DGraphControllerException(pe.getMessage());
	}
	return result;
		}

    /** Geneates bridges for all strands
     * @returns number of placed bridges */
    public int bridgeAllStrands(String nameBase, String rootName) throws Object3DGraphControllerException {
	assert graph != null;
	Properties properties = new Properties();
	if (bridgeController == null) {
	    initBridgeController();
	}
	assert bridgeController != null;
	SingleStrandsBridgeFinder bridgesFinder = bridgeController.generateSingleStrandBridgesFinder();
	Object3D root = graph.getGraph();
	if (nameBase != null) {
	    root = graph.findByFullName(nameBase);
	}
	if (root == null) {
	    throw new Object3DGraphControllerException("Could not find object with name " + nameBase);
	}
	Object3DSet strands = Object3DTools.collectByClassName(root, "RnaStrand");
	if (strands.size() == 0) {
	    throw new Object3DGraphControllerException("Could not find any RNA strands in subtree " + nameBase);
	}
	List<Object3DLinkSetBundle> result = bridgesFinder.findBridges(strands);
	log.info("Found " + result.size() + " bridges!");
	if (rootName == null) {
	    rootName = graph.getGraph().getName(); // should be "root" by default
	}
	for (int i = 0; i < result.size(); ++i) {
	    graph.addGraph(result.get(i).getObject3D(), rootName);
	    links.addLinks(result.get(i).getLinks());
	}
	return result.size();
    }

    /** Attempts to find bridging element (single stranded our double stranded) in database */
    public Properties bridgeIt(String name1, String name2, String rootName, String filePrefix, int n, double rms,
			       double angleWeight, int helixAlgorithm, int lenMin, int lenMax) throws Object3DGraphControllerException {
	Object3D obj1 = graph.findByFullName(name1);
	Object3D obj2 = graph.findByFullName(name2);
	Object3D root = graph.findByFullName(rootName);
	if ((obj1 == null) || (obj2 == null)) {
	    throw new Object3DGraphControllerException("Could not find objects: " + name1 + " " + name2);
	}
	return bridgeIt(obj1, obj2, root, filePrefix, n, rms, angleWeight, helixAlgorithm, lenMin, lenMax);
    }

    /** Attempts to find briging elements (single stranded our double stranded) in database for 
     * junction link with certain name */
    public Properties jBridgeIt(String jName, String rootName, String filePrefix, int n, double rms,
			       double angleWeight, int helixAlgorithm, int lenMin, int lenMax) throws Object3DGraphControllerException {
	LinkSet junctionList = links.findLinks(jName);
	Properties properties = new Properties();
	Object3D root = graph.findByFullName(rootName);
	if (root == null) {
	    throw new Object3DGraphControllerException("Could not find node with name " + rootName);
	}
	if ((junctionList == null) || (junctionList.size() == 0)) {
	    throw new Object3DGraphControllerException("Could not junction links with name: " + jName);
	}
	for (int i = 0; i < junctionList.size(); ++i) {
	    jBridgeIt((JunctionMultiConstraintLink)(junctionList.get(i)), root, filePrefix, n, rms, angleWeight, helixAlgorithm, lenMin, lenMax);
	}
	return properties;
    }


    /** Extend single stranded region by lenMax nucleotides */
    public Object3D extendIt(String name1, String rootName, String filePrefix, int n, double rms,
			       double angleWeight, int helixAlgorithm, int lenMax, String givenName) throws Object3DGraphControllerException {
	Object3D obj1 = graph.findByFullName(name1);
	assert obj1.getParent() != null;
	Object3D root = graph.findByFullName(rootName);
	if (obj1 == null) {
	    throw new Object3DGraphControllerException("Could not find object: " + name1);
	}
	if (!( obj1 instanceof Nucleotide3D)) {
	    throw new Object3DGraphControllerException("Currently strands can only be extended by specifying first or last nucleotide: " + name1);
	}
	if (obj1.getSiblingId() == 0) {
	    // first residue:
	    return bridgeItObject(null, obj1, root, filePrefix, n, rms, angleWeight, helixAlgorithm, lenMax, lenMax, givenName);
	}
	else if (obj1.getSiblingId() == obj1.getParent().size()-1) {
	    return bridgeItObject(obj1, null, root, filePrefix, n, rms, angleWeight, helixAlgorithm, lenMax, lenMax, givenName);
	}
	else if (Math.sin(0) < 10.0) { // workaround for true
	    throw new Object3DGraphControllerException("Wrong residue index. Currently strands can only be extended by specifying first or last nucleotide: " + name1);
	}
	return null;
    }

    /** Attempts to find briging element (single stranded our double stranded) in database */
    public Properties bridgeItCombined(String name1, String name2, String name3, String name4, String rootName, String filePrefix, 
				       int n, double rms, double angleWeight,
				       int helixAlgorithm, 
				       int lenMax, double collisionDistance) throws Object3DGraphControllerException {
	Object3D obj1 = graph.findByFullName(name1);
	Object3D obj2 = graph.findByFullName(name2);
	Object3D obj3 = graph.findByFullName(name3);
	Object3D obj4 = graph.findByFullName(name4);
	Object3D root = graph.findByFullName(rootName);
	if ((obj1 == null) || (obj2 == null) || (obj3 == null) || (obj4 == null) || (root == null)) {
	    throw new Object3DGraphControllerException("Could not find objects: " + name1 + " " + name2 + " " + name3 + " " + name4 + " " + root);
	}
	return bridgeItCombined(obj1, obj2, obj3, obj4, root, filePrefix, n, rms, angleWeight, helixAlgorithm, lenMax, collisionDistance);
    }

    /** Attempts to find briging element (single stranded our double stranded) in database */
    public Properties bridgeItCombined(Object3D obj1, Object3D obj2,
				       Object3D obj3, Object3D obj4, 
				       Object3D root, String filePrefix, int n, double rms, double angleWeight,
				       int helixAlgorithm, int lenMax, double collisionDistance) throws Object3DGraphControllerException {
	if (bridgeController == null) {
	    initBridgeController();
	}
	Properties properties = new Properties();
	// todo
	log.info("Started bridgeIt " + obj1.getFullName() + " " + obj2.getFullName());
	properties.setProperty("name1", obj1.getFullName());
	properties.setProperty("name2", obj2.getFullName());
	properties.setProperty("name3", obj3.getFullName());
	properties.setProperty("name4", obj4.getFullName());
	try {
	    bridgeController.setAngleWeight(angleWeight);
	    bridgeController.setLenMax(lenMax);
	    bridgeController.setRms(rms);
	    bridgeController.setSolutionMax(n);
	    bridgeController.setHelixAlgorithm(helixAlgorithm); // not great, side-effect
	    List<Object3DLinkSetBundle> bridges1 = bridgeController.findBridge(obj1, obj2);
	    List<Object3DLinkSetBundle> bridges2 = bridgeController.findBridge(obj3, obj4);
	    if ((bridges1 != null) && (bridges2 != null)) {
		log.info("Total number of found bridging fragments: " + bridges1.size() + " " + bridges2.size());
		Collections.sort(bridges1); // sort by score
		Collections.sort(bridges2); // sort by score
		for (int i = n-1; i >= 0; --i) {
		    if (i >= bridges1.size()) {
			continue;
		    }
		    Object3D bridge1 = bridges1.get(i).getObject3D();
		    for (int j = n - 1; j >= 0; --j) {
			if (j >= bridges2.size()) {
			    continue;
			}
			Object3D bridge2 = bridges2.get(j).getObject3D();
			int colls = AtomTools.countExternalCollisions(bridge1, bridge2, collisionDistance);
			if (colls > 0) {
			    log.info("Ignoring bridge combination " + (i+1) + " " + (j+1) + " because of collisions!");
			    continue;
			}
			String name1 = root.insertChildSafe(bridge1);
			String name2 = root.insertChildSafe(bridge2);
			// writing object to file system:
			GeneralPdbWriter writer = new GeneralPdbWriter();
			if ((filePrefix != null) && (filePrefix.length() > 0)) {
			    String bridgeFileName = filePrefix + (i+1) + "_" + (j+1) + ".pdb";
			    try {
				FileOutputStream fos = new FileOutputStream(bridgeFileName);
				writer.write(fos, root);
			    }
			    catch(IOException ioe) {
				log.warning("Could not write file with bridge: " + bridgeFileName);
			    }
			}
			if ((i == 0) && (j== 0)) {
			    PropertyTools.mergeProperties(properties, bridge1.getProperties(), name1);
			    links.merge(bridges1.get(i).getLinks());
			    links.merge(bridges2.get(j).getLinks());
			    log.info("Added top ranking bridge: " + bridge1.getFullName() + " with name " + name1 + " " + name2);
			}
			else {
			    root.removeChild(bridge1); // remove child node unless it is the top ranking one
			    root.removeChild(bridge2); // remove child node unless it is the top ranking one
			}
		    }
		}
	    }
	    else {
		log.warning("No bridge could be found between " + obj1.getFullName() + " and " + obj2.getFullName());
	    }
	}
	catch (RnaModelException rne) {
	    throw new Object3DGraphControllerException("RnaModelException in brigdeController: " + rne.getMessage());
	}
	return properties;
    }

    /** Attempts to find briging element (single stranded our double stranded) in database */
    public Properties bridgeIt(Object3D obj1, Object3D obj2, Object3D root, String filePrefix, int n, double rms, double angleWeight,
			       int helixAlgorithm, int lenMin, int lenMax) throws Object3DGraphControllerException {
	assert (obj1 != null) || (obj2 != null);
	if (bridgeController == null) {
	    initBridgeController();
	}

	Properties properties = new Properties();
	// todo
	// log.info("Started bridgeIt " + obj1.getFullName() + " " + obj2.getFullName());
	if (obj1 != null) {
	    properties.setProperty("name1", obj1.getFullName());
	}
	if (obj2 != null) {
	    properties.setProperty("name2", obj2.getFullName());
	}
	try {
	    bridgeController.setAngleWeight(angleWeight);
	    bridgeController.setLenMin(lenMin);
	    bridgeController.setLenMax(lenMax);
	    bridgeController.setSolutionMax(n);
	    bridgeController.setRms(rms);
	    List<Object3DLinkSetBundle> bridges = bridgeController.findBridge(obj1, obj2);
	    if (bridges != null) {
		log.info("Total number of found bridging fragments: " + bridges.size());
		if (bridges.size() > 0) {
		    Object3D bridge = bridges.get(0).getObject3D();
		    PropertyTools.merge(properties, bridge.getProperties());
		    log.info("Adding top ranking bridge: " + bridge.getFullName());
		    graph.addGraph(bridge);
		    // writing object to file system:
		    GeneralPdbWriter writer = new GeneralPdbWriter();
		    String bridgeFileName = "bridge_tmp.pdb";
		    try {
			FileOutputStream fos = new FileOutputStream(bridgeFileName);
			writer.write(fos, bridge);
		    }
		    catch(IOException ioe) {
			log.warning("Could not write file with bridge: " + bridgeFileName);
		    }
		    links.merge(bridges.get(0).getLinks());
		}
		else {
		    log.info("Could not find bridge solution " + (n+1) + " for specified geometry.");
		}
	    }
	    else {
		assert false;
	    }
	}
	catch (RnaModelException rne) {
	    throw new Object3DGraphControllerException("RnaModelException in brigdeController: " + rne.getMessage());
	}
	return properties;
    }
    
        /** Attempts to find briging element (single stranded our double stranded) in database */
    public Object3D bridgeItObject(String name1, String name2, String rootName, String filePrefix, int n, double rms, double angleWeight,
                               int helixAlgorithm, int lenMin, int lenMax, String givenName) throws Object3DGraphControllerException {
    
    Object3D obj1 = ( (name1 != null) ? graph.findByFullName(name1) : null);
	Object3D obj2 = ( (name2 != null) ? graph.findByFullName(name2) : null);
	Object3D root = graph.findByFullName(rootName);
	if ((obj1 == null) || (obj2 == null)) {
	    throw new Object3DGraphControllerException("Could not find objects: " + name1 + " " + name2);
	}
    return bridgeItObject(obj1, obj2, root, filePrefix, n, rms, angleWeight, helixAlgorithm, lenMin, lenMax, givenName);
    }
    
    public Object3D bridgeItObject(Object3D obj1, Object3D obj2, Object3D root, String filePrefix, int n, double rms, double angleWeight,
                               int helixAlgorithm, int lenMin, int lenMax, String givenName) throws Object3DGraphControllerException {
    
        if (bridgeController == null) {
            initBridgeController();
        }

        Properties properties = new Properties();
        // todo                                                                                                                                                                                                                 
        // log.info("Started bridgeIt " + obj1.getFullName() + " " + obj2.getFullName());                                                                                                                                       
        if (obj1 != null) {
            properties.setProperty("name1", obj1.getFullName());
        }
        if (obj2 != null) {
            properties.setProperty("name2", obj2.getFullName());
        }
        try {
            bridgeController.setAngleWeight(angleWeight);
            bridgeController.setLenMin(lenMin);
            bridgeController.setLenMax(lenMax);
            bridgeController.setSolutionMax(n);
            bridgeController.setRms(rms);
            List<Object3DLinkSetBundle> bridges = bridgeController.findBridge(obj1, obj2);
            if (bridges != null) {
                log.info("Total number of found bridging fragments: " + bridges.size());
                if (bridges.size() > 0) {
                    Object3D bridge = bridges.get(0).getObject3D();
                    PropertyTools.merge(properties, bridge.getProperties());
                    log.info("Adding top ranking bridge: " + bridge.getFullName());
                    // graph.addGraph(bridge);                                                                                                                                                                                  
                    // writing object to file system:                                                                                                                                                                           
//                  GeneralPdbWriter writer = new GeneralPdbWriter();                                                                                                                                                           
//                  String bridgeFileName = "bridge_tmp.pdb";                                                                                                                                                                   
//                  try {                                                                                                                                                                                                       
//                      FileOutputStream fos = new FileOutputStream(bridgeFileName);                                                                                                                                            
//                      writer.write(fos, bridge);                                                                                                                                                                              
//                  }                                                                                                                                                                                                           
//                  catch(IOException ioe) {                                                                                                                                                                                    
//                      log.warning("Could not write file with bridge: " + bridgeFileName);                                                                                                                                     
//                  }                                                                                                                                                                                                           
                    links.merge(bridges.get(0).getLinks());
                    
                    Object3DSet strands = Object3DTools.collectByClassName(bridge, "RnaStrand");
    				assert strands.size() == 1;
    				Object3D strand    = strands.get(0);
    				if(givenName != null && givenName != ""){
   						strand.setName(givenName);
   					}
			
       				return bridge; // properties;
                }
                else {
                    log.info("Could not find bridge solution " + (n+1) + " for specified geometry.");
                }
            }
            else {
                assert false;
            }
        }
        catch (RnaModelException rne) {
            throw new Object3DGraphControllerException("RnaModelException in brigdeController: " + rne.getMessage());
        }                                                                                                                                                                                           
    	return null;
    }

    /** Attempts to find briging element (single stranded our double stranded) in database */
    public Properties jBridgeIt(JunctionMultiConstraintLink jLink, Object3D root, String filePrefix, int n, double rms, double angleWeight,
			       int helixAlgorithm, int lenMin, int lenMax) throws Object3DGraphControllerException {
	assert (jLink != null);
	if (bridgeController == null) {
	    initBridgeController();
	}

	Properties properties = new Properties();
	// todo
	// log.info("Started bridgeIt " + obj1.getFullName() + " " + obj2.getFullName());
	properties.setProperty("name", jLink.getName());
	try {
	    bridgeController.setAngleWeight(angleWeight);
	    bridgeController.setLenMin(lenMin);
	    bridgeController.setLenMax(lenMax);
	    bridgeController.setSolutionMax(n);
	    bridgeController.setRms(rms);
	    List<Object3DLinkSetBundle> bridges = bridgeController.findBridges(jLink);
	    if (bridges != null) {
		log.info("Total number of found bridges: " + bridges.size());
		if (bridges.size() > 0) {
		    for (int i = 0; i < bridges.size(); ++i) {
			Object3D bridge = bridges.get(i).getObject3D();
			PropertyTools.merge(properties, bridge.getProperties());
			log.info("Adding top ranking bridge: " + bridge.getFullName());
			graph.addGraph(bridge);
			// writing object to file system:
			GeneralPdbWriter writer = new GeneralPdbWriter();
			String bridgeFileName = "bridge_tmp.pdb";
			try {
			    FileOutputStream fos = new FileOutputStream(bridgeFileName);
			    writer.write(fos, bridge);
			}
			catch(IOException ioe) {
			    log.warning("Could not write file with bridge: " + bridgeFileName);
			}
			links.merge(bridges.get(i).getLinks());
		    }
		}
		else {
		    log.info("Could not find bridge solution " + (n+1) + " for specified geometry.");
		}
	    }
	    else {
		assert false;
	    }
	}
	catch (RnaModelException rne) {
	    throw new Object3DGraphControllerException("RnaModelException in brigdeController: " + rne.getMessage());
	}
	return properties;
    }

    /** Central method: given two objects, find list of possible bridges in database such that they are connected
     * by single strands which themselves are connected with a new helix
     */
    /*
    public List<Object3DLinkSetBundle> findBridgeWithHelix(Object3D obj1, Object3D obj2,
							   int numBP,
							   Object3D parent, String name) throws RnaModelException {
	if (bridgeController == null) {
	    initBridgeController();
	}
	if ((obj1 instanceof Nucleotide3D) && (obj2 instanceof Nucleotide3D)) {
	    return findSingleStrandBridgeWithHelix((Nucleotide3D)obj1, (Nucleotide3D)obj2, numBP,
						   parent, name);
	}
	else if (new Random().nextDouble() >= 0) { // should always be true
	    throw new RnaModelException("Bridge between classes " + obj1.getClassName() + " and " + obj2.getClassName()
					+ " is not supported.");
	}
	return null;
    }
    */

    /** Central method: given two objects, find list of possible bridges in database such that they are connected
     * by single strands which themselves are connected with a new helix
     */
    /* 
    public Object3DLinkSetBundle findSingleStrandBridgeWithHelix(Nucleotide3D obj1, Nucleotide3D obj2,
								 int numBP, Object3D parent, name,
								 bool extendAtEnd) throws RnaModelException,
											FittingException, Object3DGraphControllerException {
	assert(extendAtEnd) ;// other way not yet implemented FIXIT
	double distMin = 3.0;
	double distMax = 3.0;
	// first generate helix of appropriate size:
	String helixName = name + "_helix";
	generateIsolatedHelix(new CoordinateSystem3D(obj1.getPosition()), 'C','G', parent.getAbsoluteName(), helixName, numBP);
	// optimize helix position:
	Object3D helixObj = getGraph().getGraph(parent.getAbsoluteName() + "." + helixName);
	assert (helixObj == 0);
	Nucleotide3D helixNuc1 = null;
	Nucleotide3D helixNuc2 = null;
	int helixStrandId1 = helixObj.getIndexOfChild(0, "RnaStrand");
	int helixStrandId2 = helixObj.getIndexOfChild(1, "RnaStrand");
	assert(helixStrandId1 < helixObj.size());
	assert(helixStrandId2 < helixObj.size());
	RnaStrand helixStrand1 = (RnaStrand)helixObj.getChild(id1);
	RnaStrand helixStrand2 = (RnaStrand)helixObj.getChild(id2);
	if (extendAtEnd) {
	    helixNuc1 = (Nucleotide3D)(helixStrand1.getChild(0));
	    helixNuc2 = (Nucleotide3D)(helixStrand2.getChild(0));
	} else {
	    helixNuc1 = (Nucleotide3D)(helixStrand1.getChild(helixStrand1.size()-1));
	    helixNuc2 = (Nucleotide3D)(helixStrand2.getChild(helixStrand2.size()-1));
	}
	SimpleLinkSet newLinks = new SimpleLinkSet();
	newLinks.add(new SimpleConstraintLink(obj1, helixNuc1, min, max));// addDistanceConstraintLink(obj1, obj2, min, max);
	newLinks.add(new SimpleConstraintLink(obj2, helixNuc2, min, max));// addDistanceConstraintLink(obj1, obj2, min, max);
	// add two single strand bridges
	List<Object3DSet> objectBlocks = 
	Optimizer optimizer = new BasepairOptimizer(graph.getGraph(), links, objectBlocks, numSteps, errorScoreLimit, basePairDB);
	Properties properties = basepairOptimizer.optimize();
	assert properties != null;
	refresh(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED));
	fireModelChangedOnGraphAndLinkController(new ModelChangeEvent(this, Object3DGraphControllerEventConstants.MODEL_MODIFIED));
	return properties;
    }
    */

    /** Clears all content (links and objects) FIXIT: other controllers might still contain data */
    public void clear() {
	links.clear();
	graph.clear();
	symmetryController.clear();
	// sequences.clear();
	// bindingSites.clear();
	refresh(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED));
    }
    
    /** actives all registered model change listeners */
    public void fireModelChanged(ModelChangeEvent changeEvent) {
	//log.info("Starting Object3DGraphController.fireModelChanged");
	for (int i = 0; i < modelChangeListeners.size(); ++i) {
	    ModelChangeListener listener = (ModelChangeListener)(modelChangeListeners.get(i));
	    //log.info("Firing model changed " + changeEvent + " for listener: " + listener);
	    listener.modelChanged(changeEvent); //CV
	}
// 	graph.fireModelChanged(changeEvent); // would lead to infinite loop in combination of listener registration in constructor
// 	links.fireModelChanged(changeEvent);
    }

    /** fire model change event on graph controller and link controller */
    private void fireModelChangedOnGraphAndLinkController(ModelChangeEvent event) {
	graph.fireModelChanged(event);
	links.fireModelChanged(event);
    }

    /** returns "collector" container containing all sequences in graph */
    static SequenceCollector generateCollectedSequences(Object3D root) {
	SequenceCollector collector = new SequenceCollector();
	Object3DActionVisitor visitor = new Object3DActionVisitor(root, collector); 
	visitor.nextToEnd();
	return collector;
    }
    
    /** generate 2 strands with stem for each link connecting objects */
    public Object3DLinkSetBundle generateCoveringStems(Object3DLinkSetBundle bundle, 
						       String nameBase,
						       boolean onlyFirstPathMode, 
						       char sequenceChar,
						       int tilerAlgorithm) throws Object3DGraphControllerException {
	log.info( "starting generateCoveringStems.");
	GridTiler tiler = null;
	switch (tilerAlgorithm) {
	case TILER_SIMPLE:
	    tiler = new SimpleGridTiler(nucleotideDB);
	    break;
	case TILER_COMPLETE:
	    tiler = new CompleteGridTiler(nucleotideDB);
	    break;
	case TILER_RANDOM:
	    tiler = new RandomGridTiler(nucleotideDB);
	    break;
	case TILER_FRAGMENT:
	    if (!junctionController.isValid()) {
		throw new Object3DGraphControllerException("Junction Database is not defined yet! Use Options menu.");
	    }
	    updateFragmentGridTiler(); // get reference to junction database
	    tiler = fgTiler;
	    break;
	case TILER_DEBUG:
	    if (!junctionController.isValid()) {
		throw new Object3DGraphControllerException("Junction Database is not defined yet! Use Options menu.");
	    }
	    tiler = new DebugGridTiler(junctionController.getJunctionDB());
	    break;
	case TILER_KISSINGLOOP_DEBUG:
	    if (!junctionController.isValid()) {
		throw new Object3DGraphControllerException("Junction/Kissing loop Database is not defined yet! Use Options menu.");
	    }
	    tiler = new DebugGridTiler(junctionController.getKissingLoopDB());
	    break;
	default:
	    throw new Object3DGraphControllerException("Unknow tiling algorithm id!");
	}
	// GridTiler tiler = new CompleteGridTiler();
	// assert tiler.getStrandJunctionDB() != null;
	Object3DLinkSetBundle stemBundle = tiler.generateTiling(bundle.getObject3D(), bundle.getLinks(), nameBase, sequenceChar, onlyFirstPathMode);
	log.fine("Generated stems: " + stemBundle.getObject3D());
	// 	graph.addGraph(stemBundle.getObject3D());
	// 	links.addLinks(stemBundle.getLinks());
	return stemBundle;
    }
    
    /**
     * Generate junctions based upon the graph
     */
    public JunctionCollector generateJunctions() {
	log.fine("generating junctions");
	JunctionCollector collector = new JunctionCollector();
	if ((graph != null) && (graph.getGraph() != null)) {
	    Object3DActionVisitor visitor = new Object3DActionVisitor(graph.getGraph(), collector);
	    visitor.nextToEnd(); // traverse scene graph
	}
	return collector;
    }
    
    /** From set of root names obtain set of junctions. From set of junctions generate set of helix constraints, add to set of links */
    public void generateRingFixConstraints(String[] names) {
	Object3DSet junctions = Object3DTools.collectByClassName(getGraph().getGraph(), names, SimpleStrandJunction3D.CLASS_NAME);
	// merge kissing loops
	junctions.merge(Object3DTools.collectByClassName(getGraph().getGraph(), names, KissingLoop3D.CLASS_NAME));
	log.info("Found " + junctions + " junctions or kissing loops.");
	RingFixLinkGenerator fixer = new RingFixLinkGenerator(junctions);
	fixer.run();
	LinkSet ringConstraints = fixer.getLinks();
	assert ringConstraints != null;
	if (ringConstraints.size() > 0) {
	    log.info("Adding " + ringConstraints.size() + " ring helix constraints.");
	}
	else {
	    log.warning("No ring helix constraints could be found!");
	}
	links.merge(ringConstraints);
    }

    /** Returns symmetry controller */
    public SymmetryController getSymmetryController() { return symmetryController; }

    /** From root structure, attempt to identify junctions, then try to find long strands connecting those junctions */
    public void ringFuseStrands(String[] names, String resultRoot, String resultName) throws Object3DGraphControllerException {
	if ((names == null) || (names.length == 0)) {
	    names = new String[1];
	    names[0] = getGraph().getGraph().getFullName(); // typically "root"
	}
	Object3DSet junctions = Object3DTools.collectByClassName(getGraph().getGraph(), names, SimpleStrandJunction3D.CLASS_NAME);
	assert junctions != null;
	Object3DSet strands = Object3DTools.collectByClassName(getGraph().getGraph(), SimpleRnaStrand.CLASS_NAME);
	assert strands != null;
	if ((junctions.size() == 0) || (strands.size() == 0)) {
	    throw new Object3DGraphControllerException("No strands or junctions available for fusing!");
	}
	for (int i = 0; i < strands.size(); ++i) {
	    assert strands.get(i).getClassName().equals("RnaStrand");
	}
	// merge kissing loops
	junctions.merge(Object3DTools.collectByClassName(getGraph().getGraph(), names, KissingLoop3D.CLASS_NAME));
	RingFuser fuser = new RingFuser(junctions, strands);
	fuser.run();
	List<RnaStrand> fusedStrands = fuser.getFusedStrands();
	if (fusedStrands.size() == 0) {
	    throw new Object3DGraphControllerException("No strands were fused!");
	}
	Vector3D position = Object3DSetTools.centerOfMass(new SimpleObject3DSet(fusedStrands));
	assert position.isValid();
	SimpleObject3D helpRoot = new SimpleObject3D(position);
	helpRoot.setName(resultName); // construct helper root node of result subtree
	for (RnaStrand strand: fusedStrands) {
	    helpRoot.insertChild(strand);
	}
	getGraph().addGraph(helpRoot, resultRoot);
	refresh(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED)); // update sequence controllers and other event listeners subscribing
    }

    /** From set of root names obtain set of junctions. From set of junctions generate set of helix constraints, add to set of links */
    public int createToeholds(String strandRoot,  int length, String fivePrimeAtom, String threePrimeAtom, double cutoff) throws Object3DGraphControllerException {
	// 	if ((names == null) || (names.length == 0)) {
	// 	    names = new String[1];
	// 	    names[0] = getGraph().getGraph().getFullName(); // typically "root"
	// 	}
	LinkSet bluntBps = links.findBluntBasePairs();
	for (int i = 0; i < bluntBps.size(); ++i) {
	    Link link1 = bluntBps.get(i);
	    assert link1.getObj1() instanceof Nucleotide3D;
	    assert link1.getObj2() instanceof Nucleotide3D;
	    Nucleotide3D bp1_1 = (Nucleotide3D)link1.getObj1();
	    Nucleotide3D bp1_2 = (Nucleotide3D)link1.getObj2();
	    if (!bp1_1.isTerminal() || !bp1_2.isTerminal()) {
		continue; // can happen if already visited
	    }
	    assert bp1_1.isTerminal();
	    assert bp1_2.isTerminal();
	    if (bp1_1.isLast() && bp1_2.isFirst()) {
		Nucleotide3D b = bp1_1;
		bp1_1 = bp1_2;
		bp1_2 = b;
	    }
	    for (int j = i+1; j < bluntBps.size(); ++j) {
		Link link2 = bluntBps.get(j);
		assert link2.getObj1() instanceof Nucleotide3D;
		assert link2.getObj2() instanceof Nucleotide3D;
		Nucleotide3D bp2_1 = (Nucleotide3D)link2.getObj1();
		Nucleotide3D bp2_2 = (Nucleotide3D)link2.getObj2();
		assert bp2_1.isTerminal();
		assert bp2_2.isTerminal();
		if (!bp2_1.isTerminal() || !bp2_2.isTerminal()) {
		    continue; // can happen if already visited
		}
		if (bp2_1.isLast() && bp2_2.isFirst()) {
		    Nucleotide3D b = bp2_1;
		    bp2_1 = bp2_2;
		    bp2_2 = b;
		}
		assert bp1_2.isLast();
		assert bp2_1.isFirst();
		boolean isAdjacent = NucleotideTools.isAdjacentBluntBasePairPair(bp1_1, bp1_2, bp2_1, bp2_2,
										 fivePrimeAtom, threePrimeAtom, cutoff);
		if (isAdjacent) {
		    System.out.println("Found adjacent pair!");
		    BioPolymer pol2 = (BioPolymer)(bp1_2.getParent());
		    int pol2Len = pol2.getResidueCount();
		    assert(pol2Len - 1 - length >= 0);
		    splitStrand(pol2.getFullName(), pol2Len-1-length);
		    fuseStrands(bp1_2.getParent().getFullName(), bp2_1.getParent().getFullName());
		    BioPolymer pol4 = (BioPolymer)(bp2_2.getParent());
		    int pol4Len = pol4.getResidueCount();
		    assert(pol4Len - 1 - length >= 0);
		    splitStrand(pol4.getFullName(), pol4Len-1-length);
		    fuseStrands(bp1_1.getParent().getFullName(), bp2_2.getParent().getFullName());
		    
		} else {
		    System.out.println("Pair is not adjacent!");
		}
	    }
	}
	System.out.println("Found " + bluntBps.size() + " base pairs with blunt ends!");
	refresh(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED)); // update sequence controllers and other event listeners subscribing
	return 0;
    }

    /** Fuse all strands where 5' and 3' ends are in vicinity */
    public void fuseAllStrands(String strandRoot, String resultName, String resultRoot, double scoreCutoff, 
			       double bridgeRms, int lengthMin, int lengthMax) throws Object3DGraphControllerException {
	// 	if ((names == null) || (names.length == 0)) {
	// 	    names = new String[1];
	// 	    names[0] = getGraph().getGraph().getFullName(); // typically "root"
	// 	}
	Object3DSet strands = Object3DTools.collectByClassName(getGraph().getGraph(strandRoot), SimpleRnaStrand.CLASS_NAME);
	assert strands != null;
	if (strands.size() == 0) {
	    throw new Object3DGraphControllerException("No strands or junctions available for fusing!");
	}
	for (int i = 0; i < strands.size(); ++i) {
	    assert strands.get(i).getClassName().equals("RnaStrand");
	}
	if (bridgeController == null) {
	    initBridgeController();
	}
	SingleStrandBridgeFinder ssBridgeFinder = bridgeController.generateSingleStrandBridgeFinder();
	ssBridgeFinder.setRms(bridgeRms);
	ssBridgeFinder.setLenMin(lengthMin);
	ssBridgeFinder.setLenMax(lengthMax);
	StrandFuser fuser = new StrandFuser(strands, ssBridgeFinder);
	fuser.setScoreCutoff(scoreCutoff);
	fuser.run();
	List<RnaStrand> fusedStrands = fuser.getFusedStrands();
	if (fusedStrands.size() == 0) {
	    throw new Object3DGraphControllerException("No strands were fused!");
	}
// 	Vector3D position = Object3DSetTools.centerOfMass(new SimpleObject3DSet(fusedStrands));
// 	assert position.isValid();
// 	SimpleObject3D helpRoot = new SimpleObject3D(position);
// 	helpRoot.setName(resultName); // construct helper root node of result subtree
// 	for (RnaStrand strand: fusedStrands) {
// 	    helpRoot.insertChildSafe(strand);
// 	}
// 	getGraph().addGraph(helpRoot, resultRoot);
	refresh(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED)); // update sequence controllers and other event listeners subscribing
    }

    /** From set of root names obtain set of strands. Apply fusing of closest strands once. */
    public Properties fuseClosestStrands(String strandRoot, String resultName, String resultRoot,
				   double scoreCutoff, 
				   double bridgeRms, int lengthMin, int lengthMax) throws Object3DGraphControllerException {
	// 	if ((names == null) || (names.length == 0)) {
	// 	    names = new String[1];
	// 	    names[0] = getGraph().getGraph().getFullName(); // typically "root"
	// 	}
	Object3DSet strands = Object3DTools.collectByClassName(getGraph().getGraph(strandRoot), SimpleRnaStrand.CLASS_NAME);
	assert strands != null;
	if (strands.size() == 0) {
	    throw new Object3DGraphControllerException("No strands or junctions available for fusing!");
	}
	for (int i = 0; i < strands.size(); ++i) {
	    assert strands.get(i).getClassName().equals("RnaStrand");
	}
	SimpleStrandFuser fuser = new SimpleStrandFuser(strands);
	fuser.setScoreCutoff(scoreCutoff);
	fuser.run();
	refresh(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED)); // update sequence controllers and other event listeners subscribing
	return (Properties)(fuser.getResult());
    }

    /** From set of root names obtain set of strands. Apply fusing of closest strands until nothing can be fused anymore. */
    public int fuseAllClosestStrands(String strandRoot, String resultName, String resultRoot,
					    double scoreCutoff) throws Object3DGraphControllerException {
	SimpleStrandFuser fuser = null;
	int count = 0;
	do {
	    Object3DSet strands = Object3DTools.collectByClassName(getGraph().getGraph(strandRoot), SimpleRnaStrand.CLASS_NAME);
	    log.info("Found " + strands.size() + " as candidate for fusing...");
	    assert strands != null;
	    if (strands.size() == 0) {
		throw new Object3DGraphControllerException("No strands or junctions available for fusing!");
	    }
	    fuser = new SimpleStrandFuser(strands);
	    fuser.setScoreCutoff(scoreCutoff);
	    fuser.run();
	    ++count;
	    // } while ( ((Properties)(fuser.getResult())).getProperty("score") != null);
	} while ( ((Properties)(fuser.getResult())).getProperty("fused_strands") != null);
	--count;
	refresh(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED)); // update sequence controllers and other event listeners subscribing
	return count;
    }

    /** Specify Strand via junction and branch id. Fuse strand path according to adjacent 5' and 3' ends starting from specified strand. */
    public void fuseJStrandPath(String jname, int bid, int xlen, String strandRoot, String resultName, String resultRoot,
					    double scoreCutoff) throws Object3DGraphControllerException {
	PathStrandFuser fuser = null;
	StrandJunction3D jnc = null;
	Object3D obj = graph.findByFullName(jname);
	System.out.println("# started Object3DGraphController.fuseJStrandPath with " + jname + " " + bid
			   + " " + xlen + " " + strandRoot + " currenlty defined sequences: " 
	   + sequences.getSequenceStrings().length);
 	if (obj != null && obj instanceof StrandJunction3D) {
	    jnc = (StrandJunction3D)obj;
	} else {
	    throw new Object3DGraphControllerException("Invalid junction name");
	}
	Object3DSet strands = Object3DTools.collectByClassName(getGraph().getGraph(strandRoot), SimpleRnaStrand.CLASS_NAME);
	log.info("Found " + strands.size() + " as candidate for fusing...");
	assert strands != null;
	if (strands.size() == 0) {
	    throw new Object3DGraphControllerException("No strands or junctions available for fusing!");
	}
	// now find strand that matches specifed branch descriptor:
	int sid = -1;
	for (int i = 0; i < strands.size(); ++i) {
	    RnaStrand strand = (RnaStrand)(strands.get(i));
	    int id = jnc.findStrandIndex(strand);
	    if (id >= 0) { // junction has this strand!!
		int bout = jnc.getOutgoingBranchId(id);
		if (bout == bid) { // branch descriptor matches!!!
		    sid = i; // found correct strand to start with
		    break;
		}
	    }
	}
	if (sid < 0) { // no matching sequence found
	    throw new Object3DGraphControllerException("No matching strand found for fusing!");
	}
	System.out.println("# found matching junction strand: " 
			   + strands.get(sid).getFullName());
	List<RnaStrand> strands2 = new ArrayList<RnaStrand>();
	strands2.add((RnaStrand)(strands.get(sid))); // make sure that found strand is first
	for (int i = 0; i < strands.size(); ++i) {
	    if (i != sid) {
		strands2.add((RnaStrand)(strands.get(i)));
	    }
	}
	assert strands.size() == strands2.size();
	System.out.println("# Creating PathStrandFuser object!");
	fuser = new PathStrandFuser(strands2);
	fuser.xlen = xlen; // additional shift if circular
	fuser.setScoreCutoff(scoreCutoff);
	System.out.println("# Launching run method on PathStrandFuser object!");
	fuser.run();
	System.out.println("# Returned from run method on PathStrandFuser object!");
	// } while ( ((Properties)(fuser.getResult())).getProperty("score") != null);
	RnaStrand resultStrand = fuser.getResultStrand();
	// System.out.println("# adding fused strand: " + resultStrand.getFullName() + " with " + resultStrand.size() + " residues whith current sequences " 
	// + sequences.getSequenceStrings().length);
	if (resultName != null) {
	    resultStrand.setName(resultName);
	}
	if (resultRoot == null) {
	    graph.addGraph(resultStrand);	
	} else {
	    graph.addGraph(resultStrand, resultRoot);	
	}
	// System.out.println("# number of sequences after adding fused strand: "
	// + sequences.getSequenceStrings().length);
	List<RnaStrand> deletables = fuser.getDeletableStrands();
	for (RnaStrand strand : deletables) {
	    // System.out.println("# Adding to trash " + strand.getFullName());
	    trash.add(strand);
	}
	// System.out.println("# number of sequences after adding to trash: "
	// + sequences.getSequenceStrings().length 
	// + " ; items in trash: " + trash.size());
	refresh(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED)); // update sequence controllers and other event listeners subscribing
    }

    /** Generates secondary structure dynamically using all information */
    public MutableSecondaryStructure generateSecondaryStructure() throws DuplicateNameException {
	if(graph.getGraph() == null) {
	    return null;
	}
	//log.info("Generating secondary structure");
	// collect the sequences that stem from this passed in node
	// scan sub stree
	// visitor.nextToEnd();
	// StemCollector stemCollector = new StemCollector();
	// stemVisitor.nextToEnd();
	InteractionSet hbondInteractions = links.getHydrogenBondInteractions();
	// new SimpleInteractionSet();
	// 	for(int i = 0; i < stemCollector.getStemCount(); ++i) {
	// 	    Stem s = stemCollector.getStem(i);
	// 	    for(int j = 0; j < s.size(); ++j) {
	// 		stemInteractions.add(s.get(j));
	// 	    }
	// 	}
	UnevenAlignment unevenAlignment = null;
	unevenAlignment = sequences.getUnevenAlignment();
	// log.info("Number found sequences: " +
	// 	 unevenAlignment.getSequenceCount() + 
	// 	 " number of hydrogen bond interactions: " + 
	// 	 hbondInteractions);
	// for (int i = 0; i < collector.getSequenceCount(); ++i) {
	// try {
	// sequences.addSequence(collector.getSequence(i));
	// log.finest("Found sequence: " + collector.getSequence(i));
	// if ((collector.getSequence(i).getParent() != null)
	// && (collector.getSequence(i).getParent() instanceof Object3D)) {
	// Object3D obj = (Object3D)(collector.getSequence(i).getParent());
	// log.finest("Parent name: " + obj.getName());
	// }
	// }
	// catch (DuplicateNameException e) {
	// log.severe("internal error: duplicate sequence names found!");
	// }
	// }
	MutableSecondaryStructure structure = new SimpleMutableSecondaryStructure(unevenAlignment, hbondInteractions);	
	structure.purgeOverlappingInteractions(SecondaryStructure.PURGE_ALL_OVERLAPPING);
	return structure;
    }    

    /** generates a signature of a graph structure */
    public String generateSignature(String fullName) throws Object3DGraphControllerException {
	Object3D obj = graph.findByFullName(fullName);
	if (obj == null) {
	    throw new Object3DGraphControllerException("Could not find object: " + fullName);
	}
	SignatureTranslatorCanonizer canonizer = new SignatureTranslatorCanonizer();
	String result = "";
	try {
	    result = canonizer.generateCanonizedRepresentation(new SimpleObject3DLinkSetBundle(obj, links));
	}
	catch (AlgorithmFailureException afe) {
	    throw new Object3DGraphControllerException(afe.getMessage());
	}
	return result;
    }
 
    /** call CCP4 program pdbset/gensym */
    private String generateSymgenCommands(int numSymmetries) {
	StringBuffer buf = new StringBuffer();
	buf.append(SymmetryTools.generateSymgenCommandsSimple(spaceGroup, cell));
	if (numSymmetries > 0) {
	    // get total number of exisiting symm
	    char currentChar = 'A';
	    for (int i = 0; i < numSymmetries; ++i) {
		for (int j = 0; j < sequences.getSequenceCount(); ++j) {
		    BioPolymer strand = (BioPolymer)(sequences.getSequence(j));
		    char origChar = GeneralPdbWriter.getOriginalStrandChar(strand);
		    buf.append("CHAIN SYMMETRY " + (i+1) + " " + origChar + " " + currentChar + NEWLINE);
		    // increase char
		    currentChar = GeneralPdbWriter.nextStrandChar(currentChar);
		}
	    }
	}
	return buf.toString();
    }

    /** returns base pair database */
    public BasePairController getBasePairDB() { return this.basePairDB; }
    
    public BindingSiteController getBindingSites() {
	return bindingSites;
    }
    
    /** returns crystallographic cell */
    public Cell getCell() { return cell; }

    /** returns reference to currently used fragment grid tiler */
    public FragmentGridTiler getFragmentGridTiler() { return this.fgTiler; }
    
    public Object3DController getGraph() {
	return graph;
    }
    
    public JunctionController getJunctionController() {
	return junctionController;
    }

    public Kaleidoscope getKaleidoscope() {
	return this.kaleidoscope;
    }
    
    /** returns last read directory */
    public String getLastReadDirectory() { return lastReadDirectory; }
    
    /**
     * returns n'th defined stem. A stem is a link between two objects, 
     * both of which are of the type SequenceBindingSite
     */
    public LinkController getLinks() {
	return links;
    }
    
    private Shape3D getSelectionCursorShape() {
	Object3D selectionCursor = graph.getSelectionCursor();
	if (selectionCursor == null) {
	    return null;
	}
	Shape3D shape = new Cube(selectionCursor.getPosition(), 
				 selectionCursorRadius );
	shape.setAppearance(selectionCursorAppearance);
	return shape;
    }
    
    private Shape3D getSelectionRootShape() {
	Object3D selectionRoot = graph.getSelectionRoot();
	if (selectionRoot == null) {
	    return null;
	}
	Shape3D shape = new Cube(selectionRoot.getPosition(), selectionRootRadius);
	log.fine("calling Object3DGraphController.getSelectionRootShape");
	shape.setAppearance(selectionRootAppearance);
	return shape;
    }
    
    /** returns sequence controller */
    public SequenceController getSequences() {
	return sequences;
    }
    
    /** returns current shape set */
    public Shape3DSet getShapeSet() {
	if (shapeSet == null) {
	    refreshShapeSet();
	}
	assert shapeSet != null;
	return shapeSet;
    }
    
    public SpaceGroup getSpaceGroup() { return this.spaceGroup; }
    
    /** returns number of space group symmetries */
    private int getSpaceGroupSymmetryCount() throws Object3DGraphControllerException {
	int result = spaceGroupSymmetryCount;
	if (result <= 0) {
	    result = SymmetryTools.computeSpaceGroupSymmetryCount(spaceGroup);
	}
	return result;
    }
    
    private void initSpaceGroupFactory() throws Object3DGraphControllerException {
	try {
	    String fileName = rb.getString("spacegroups.prmfile");
	    FileInputStream fis = new FileInputStream(fileName);
	    spaceGroupFactory.init(fis);
	    assert spaceGroupFactory.isValid();
	}
	catch (IOException ioe) {
	    throw new Object3DGraphControllerException(ioe.getMessage());
	}
	catch (ParsingException pe) {
	    throw new Object3DGraphControllerException(pe.getMessage());
	}
	assert spaceGroupFactory.isValid();
    }
    
    /** returns true iff at least one 3d object is defined */
    public boolean isValid() { return (getGraph().getObjectCount() > 0); }
    
    public void minimize(int steps, double scale, int algorithmCode)
	throws InvalidParametersException {
	MinimizationParameters parameters = new SimpleMinimizationParameters();
	parameters.setTimeStepMax(steps);
	parameters.setForceField(forceFieldFactory.generateForceField());
	parameters.setPrototype(new Nucleotide3D());
	Object3DSet objectSet = new SimpleObject3DSet(graph.getGraph());
	Minimizer minimizer = null;
	double energyCutoff = 10.0; // TODO : make variable
	switch (algorithmCode) {
	case MONTE_CARLO: 
	    minimizer = new MonteCarloMinimizer(objectSet, links, parameters);
	    break;
	default: log.warning("could not interpret algorithm id!"); 
	}
	minimizer.addModelChangeListener(this); // keep controller notified for changes
	log.fine("Starting minimization!");
	new Thread(minimizer).start();
	// minimizer.run();
	log.fine("Started minimization thread!");
    }
    
    /** TODO : handle change of underlying date model */
    public void modelChanged(ModelChangeEvent event) {
	log.fine("Called modelChanged: " + event.getEventId());
	// pass on event cascade:
	refresh(event);
	// fireModelChanged(event);
    }

    /** for a given base pair link, returns RnaInteractionType object. Return null
     * if the links is not an RNA interaction link */
    private RnaInteractionType extractRnaInteractionType(Link link) {
	if (! (link instanceof InteractionLink)) {
	    return null;
	}
	Interaction interaction = ((InteractionLink)(link)).getInteraction();
	InteractionType interactionType = interaction.getInteractionType();
	if (! (interactionType instanceof RnaInteractionType)) {
	    return null;
	}
	return (RnaInteractionType)interactionType;	
    }

    /** fuses strands such that strand 1 becomes longer, strand 2 will be deleted */
    public String fuseStrands(String strandFullName1, String strandFullName2) throws Object3DGraphControllerException {
	String appendedStrandName = graph.fuseStrands(strandFullName1, strandFullName2);
	refresh(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED));
	return appendedStrandName;
    }

    /** possible mutates base pair */
    private void overwriteBasePairSequences(String[] sequenceStrings,
					    InteractionLink basePair,
					    Set<Mutation> mutations,
					    double rmsLimit, boolean forceMode) throws FittingException, RnaModelException {
	// find sequence id of base pair:
	Object3D objOld1 = basePair.getObj1();
	Object3D objOld2 = basePair.getObj2();
	if (! (objOld1 instanceof Nucleotide3D) ) {
	    throw new RnaModelException("Object " + objOld1.getName() + " is not a nucleotide object!");
	}
	if (! ( objOld2 instanceof Nucleotide3D) ) {
	    throw new RnaModelException("Object " + objOld2.getName() + " is not a nucleotide object!");
	}
	Nucleotide3D oldNuc1 = (Nucleotide3D)objOld1;
	Nucleotide3D oldNuc2 = (Nucleotide3D)objOld2;
	RnaStrand strand1 = null;
	RnaStrand strand2 = null;
	if (! (oldNuc1.getParent() instanceof RnaStrand) ) {
	    throw new RnaModelException("Nucleotide does not have RNA strand as parent object: " + oldNuc1.getName());
	}
	if (! (oldNuc2.getParent() instanceof RnaStrand) ) {
	    throw new RnaModelException("Nucleotide does not have RNA strand as parent object: " + oldNuc2.getName());
	}
	strand1 = (RnaStrand)(oldNuc1.getParent());
	strand2 = (RnaStrand)(oldNuc2.getParent());
	String strand1Name = strand1.getName();
	String strand2Name = strand2.getName();
	int n1 = oldNuc1.getPos();
	int n2 = oldNuc2.getPos();
	if (mutations != null) {
	    boolean found1 = false;
	    boolean found2 = false;
	    Iterator<Mutation> it = mutations.iterator();
	    while (it.hasNext()) {
		Mutation mut = it.next();		
		if ((mut.getPos() == n1) && (mut.getSequenceName().equals(strand1Name))) {
		    found1 = true;
		}
		if ((mut.getPos() == n2) && (mut.getSequenceName().equals(strand2Name))) {
		    found2 = true;
		}
		if (found1 && found2) {
		    break;
		}
	    }
	    if (! (found1 && found2)) {
		// mutation not found in specified list
		return;
	    }
	}
	if (NucleotideTools.isAtStrandEnd(oldNuc1) || NucleotideTools.isAtStrandEnd(oldNuc2)) {
	    rmsLimit += RMS_STRAND_END_OFFSET; // allow for more lee-way at strand ends
	    log.info("Detected mutation at strand end. Allowing for less strict rms error: " + rmsLimit);
	}
	int strandId1 = sequences.getSequenceId(strand1);
	int strandId2 = sequences.getSequenceId(strand2);
	assert strandId1 >= 0;
	assert strandId2 >= 0;
	char c1New = sequenceStrings[strandId1].charAt(n1); // new residues
	char c2New = sequenceStrings[strandId2].charAt(n2); 
	char c1Old = oldNuc1.getSymbol().getCharacter();
	char c2Old = oldNuc2.getSymbol().getCharacter();
	if ((c1Old == c1New) && (c2Old == c2New) && (!forceMode)) {
	    return; // nothing to mutate!
	}
	InteractionType interactionType = basePair.getInteraction().getInteractionType();
	try {
	    LetterSymbol newSymbol1 = new SimpleLetterSymbol(c1New, DnaTools.AMBIGUOUS_RNA_ALPHABET);
	    LetterSymbol newSymbol2 = new SimpleLetterSymbol(c2New, DnaTools.AMBIGUOUS_RNA_ALPHABET);
	    if (interactionType instanceof RnaInteractionType) {
		RnaStrandTools.mutateBasePair(basePair, newSymbol1, newSymbol2, (RnaInteractionType)interactionType, basePairDB, rmsLimit, links);
	    }
	    else {
		throw new RnaModelException("Link does not discribe and RNA base pair: " + basePair);
	    }
	}
	catch (UnknownSymbolException use) {
	    throw new RnaModelException("Unknown sequence symbol found: " + use.getMessage());
	}
    }

    /** mutates sequences. New strategy: go through list of base pairs and mutate those as a pair! */
    private void overwriteSequences(String[] sequenceStrings, Set<Mutation> mutations, double rmsLimit, boolean forceMode) throws Object3DGraphControllerException {
	if (nucleotideDB == null) {
	    throw new Object3DGraphControllerException("No nucleotide database defined!");
	}
	if (basePairDB == null) {
	    throw new Object3DGraphControllerException("No base pair database defined!");
	}
	// go through list of base pair links:
	String[] origSequenceStrings = sequences.getSequenceStrings();
	for (int i = 0; i < links.size(); ++i) {
	    Link link = links.get(i);
	    RnaInteractionType rnaInteractionType = extractRnaInteractionType(link);
	    if (rnaInteractionType == null) {
		continue;
	    }
	    switch (rnaInteractionType.getSubTypeId()) {
	    case RnaInteractionType.NO_INTERACTION:
		break;
	    case RnaInteractionType.BACKBONE:
		break;
	    case RnaInteractionType.UNKNOWN_SUBTYPE:
		break;
	    case RnaInteractionType.WATSON_CRICK: 
		try {
		    overwriteBasePairSequences(sequenceStrings, (InteractionLink)link, mutations, rmsLimit, forceMode);
		}
		catch (RnaModelException rme) {
		    log.info("Could not mutate base pair: " + rme.getMessage());
		}
		catch (FittingException fe) {
		    log.info("Could fit mutated base pair: " + fe.getMessage());
		}
		break;
	    default:
		log.warning("Sorry, mutating residue pair " + rnaInteractionType + " is not yet implemented!");
	    }
	}
	// mutate all single-strand residues:  FIXIT: no forceMode yet
	for (int i = 0; i < sequenceStrings.length; ++i) {
	    sequences.overwriteSequence(sequenceStrings[i], i, nucleotideDB, rmsLimit, links);
	}
    }

    public void mutateSequences(Set<Mutation> mutations, double rmsLimit, boolean forceMode) throws Object3DGraphControllerException {
	Iterator<Mutation> iterator = mutations.iterator();
	String[] sequenceStrings = sequences.getSequenceStrings();
	log.info("Starting mutateSequences. The " + sequences.size() + " defined sequence names are:");
	for (int i = 0; i < sequences.size(); ++i) {
		System.out.println(sequences.get(i).getName()); // + " " + (Object3D)(sequences.get(i)).getFullName()  );
	}
	Set<String> allowed = new HashSet<String>();
	allowed.add("RnaStrand");
	System.out.println("Defined strands:");
	getGraph().printTree(System.out, allowed, new HashSet<String>());
	while (iterator.hasNext()) {
	    Mutation mut = iterator.next();
	    System.out.println("Mutation: " + mut);
	    
	    int seqId = sequences.getSequenceId(mut.getSequenceName());
	    System.out.println(mut.getSequenceName());
	    System.out.println(seqId);
	    if (seqId < 0) {
		    //Object3DTools.findByFullName(graph.getGraph(), bd1Name);
				Object3D obj = graph.findByFullName(mut.getSequenceName()); 
				if(obj == null){
					throw new Object3DGraphControllerException("Unknown sequence name: " + mut.getSequenceName());
				}
				if( obj instanceof Sequence ){
					
					seqId = sequences.getSequenceId(obj.getName());
			    System.out.println(mut.getSequenceName());
			    System.out.println(seqId);
					if(seqId<0){
						throw new Object3DGraphControllerException("Unknown sequence name: " + mut.getSequenceName());
					}
				} else{
						throw new Object3DGraphControllerException("Unknown sequence name: " + mut.getSequenceName());
					}
	    }
	    int pos = mut.getPos();
	    char newChar = mut.getCharacter();
	    String s = sequenceStrings[seqId];
	    StringBuffer buf = new StringBuffer(s);
	    log.info("Mutating sequence character: " + (pos+1) + " "
		     + buf.charAt(pos) + " to " + newChar);
	    buf.setCharAt(pos, newChar);
	    sequenceStrings[seqId] = buf.toString(); // write back mutated string
	}
	overwriteSequences(sequenceStrings, mutations, rmsLimit, forceMode);
    }
    
    
    public String mutatePermutatedSequences(SecondaryStructure targetStructure) throws Object3DGraphControllerException, DuplicateNameException {
	
	SecondaryStructure currentStructure = generateSecondaryStructure();
	System.out.println( "CURRENT SEQUENCE COUNT *** = " + currentStructure.getSequenceCount() );
	String[] sequenceNames = new String[ currentStructure.getSequenceCount() ];
	for(int i=0; i<currentStructure.getSequenceCount(); i++ ){
		sequenceNames[i] = "root.stem_"+i+"_root."+sequences.getSequenceName(i); // current model
		System.out.println(sequenceNames[i]);
	}
	
	String name = "mutateScriptGen";
	Properties params = new Properties();
	
	String script;
	script = ScriptController.generateMutateScript(currentStructure, targetStructure, sequenceNames, name, params, "helix");
	System.out.println(script);

	return script;
    }
    /** adds a helix for two given branch descriptors */
    public void generateHelix(BranchDescriptor3D bd1, BranchDescriptor3D bd2, char c1, char c2,
			      FitParameters stemFitParameters,
			      String stemRootName, int numBasePairs,
			      String helixName) throws FittingException, Object3DGraphControllerException {
	assert stemFitParameters != null;
	if (helixName == null) {
	    helixName = "h_" + bd1.getName() + "_" + bd2.getName();
	}
	int connectionAlgorithm = BranchDescriptorOptimizerFactory.MORPH_OPTIMIZER; /** Algorithm used for stem interpolation. */
	if (nucleotideDB == null) {
	    throw new Object3DGraphControllerException("No reference nucleotides defined. Consider command loadnucleotides.");
	}
	Object3DLinkSetBundle stemBundle = ConnectJunctionTools.generateConnectingStem(bd1, bd2, c1, c2,
										       helixName, stemFitParameters, nucleotideDB, connectionAlgorithm, numBasePairs);
	if (stemBundle != null) {
	    if (stemBundle.getObject3D() != null) {
		graph.addGraph(stemBundle.getObject3D(), stemRootName);
		links.merge(stemBundle.getLinks());
	    }
	    else {
		log.info("No stems were generated!");
		throw new FittingException("No helix could be generated for " + bd1.getName() + " and "
					   + bd2.getName());
	    }
	}
	else {
	    log.fine("No stems and links were generated!");
	    throw new FittingException("No helix could be generated for " + bd1.getName() + " and "
				       + bd2.getName());
	}
	refresh(new ModelChangeEvent(this, Object3DGraphControllerEventConstants.MODEL_MODIFIED));
    }

    /** Adds a helix for one given branch descriptors */
    public void generateHelix(BranchDescriptor3D bd1, char c1, char c2,
			      String stemRootName, int numBasePairs, String helixName, boolean propagate) throws FittingException, Object3DGraphControllerException {
	log.info("BranchDescriptor found");
	assert bd1 != null;
	if (helixName == null) {
	    helixName = "h_" + bd1.getName();
	}
	if (nucleotideDB == null) {
	    throw new Object3DGraphControllerException("No reference nucleotides defined. Consider command loadnucleotides.");
	}
        if (propagate) {
	    bd1.propagate(1);
	}
	Object3DLinkSetBundle stemBundle = null;
	stemBundle = ConnectJunctionTools.generateIdealStem(bd1, c1, c2, helixName, nucleotideDB, numBasePairs);
	if ((stemBundle != null) && (stemBundle.getObject3D() != null)) {
	    graph.addGraph(stemBundle.getObject3D(), stemRootName);
	    links.merge(stemBundle.getLinks());
	}
	else {
	    log.info("No stems were generated!");
	    if (propagate) {
		bd1.propagate(-1);
	    }
	    throw new FittingException("No helix could be generated for " + bd1.getName());
	}
	if (propagate) {
	    bd1.propagate(-1);
	}
	refresh(new ModelChangeEvent(this, Object3DGraphControllerEventConstants.MODEL_MODIFIED));
    }

    /** adds a helix without branch descriptors
     * @param c1 Character of first nucleotide (A|C|G|U)
     * @param c2 Character of nucleotides of second strand (A|C|G|U) */
    public void generateIsolatedHelix(CoordinateSystem3D cs, HelixParameters helixParameters, char c1, char c2,
				      String stemRootName, String helixName, int numBasePairs) throws FittingException, Object3DGraphControllerException {
	if (nucleotideDB == null) {
	    throw new Object3DGraphControllerException("No reference nucleotides defined. Consider command loadnucleotides.");
	}
	Object3DLinkSetBundle stemBundle = ConnectJunctionTools.generateIdealStem(cs,
						      helixParameters, c1, c2, helixName, nucleotideDB, numBasePairs);
	if ((stemBundle != null) && (stemBundle.getObject3D() != null)) {
	    graph.addGraph(stemBundle.getObject3D(), stemRootName);
	    links.merge(stemBundle.getLinks());
	}
	else {
	    log.info("No stems were generated!");
	    throw new FittingException("No helix could be generated for " + cs + " and nucleotide characters " + c1 + " " + c2);
	}
	refresh(new ModelChangeEvent(this, Object3DGraphControllerEventConstants.MODEL_MODIFIED));
    }

    public void generateHelix(String bd1Name, String bd2Name,
			      char c1, char c2,
			      FitParameters stemFitParameters,
			      String stemRootName,
			      int numBasePairs,
			      String helixName) 
	throws FittingException, Object3DGraphControllerException {
	assert stemFitParameters != null;
	Object3D obj1 = Object3DTools.findByFullName(graph.getGraph(), bd1Name);
	Object3D obj2 = Object3DTools.findByFullName(graph.getGraph(), bd2Name);
	if (obj1 == null) {
	    obj1 = findBranchDescriptor(bd1Name);
	}
	if (obj2 == null) {
	    obj2 = findBranchDescriptor(bd2Name);
	}
	if (obj1 == null) {
	    throw new Object3DGraphControllerException("Cannot find object: " + obj1);
	}
	if (obj2 == null) {
	    throw new Object3DGraphControllerException("Cannot find object: " + obj2);
	}
	if (! ( obj1 instanceof BranchDescriptor3D)) {
	    throw new Object3DGraphControllerException("Specified object is not a helix descriptor: " + bd1Name);
	}
	if (! ( obj2 instanceof BranchDescriptor3D)) {
	    throw new Object3DGraphControllerException("Specified object is not a helix descriptor: " + bd2Name);
	}
	generateHelix((BranchDescriptor3D)obj1, (BranchDescriptor3D)obj2, c1, c2, stemFitParameters, stemRootName,
		      numBasePairs, helixName);
    }
	
    public void generateHelix(String bd1Name,
			      char c1, char c2,
			      String stemRootName,
			      int numBasePairs,
			      String helixName,
			      boolean propagate) 
	throws FittingException, Object3DGraphControllerException {
	Object3D obj1 = Object3DTools.findByFullName(graph.getGraph(), bd1Name);

	if (obj1 == null) {
	    obj1 = findBranchDescriptor(bd1Name);
	}
	if (obj1 == null) {
	    throw new Object3DGraphControllerException("Cannot find object: " + obj1);
	}
	if (! ( obj1 instanceof BranchDescriptor3D)) {
	    throw new Object3DGraphControllerException("Specified object is not a helix descriptor: " + bd1Name);
	}
	log.info("Starting generateHelix with one BranchDescriptor found: " + obj1.getFullName());	
	generateHelix((BranchDescriptor3D)obj1, c1, c2, stemRootName, numBasePairs, helixName, propagate);
	
    }

    /** Tries to generate a new 3D junction from a given graph vertex name and insert it under root name */
    public Properties generateJunction(String cornerName, String rootName, double offset,
	     double distMin, double distMax, int angleDiv) throws Object3DGraphControllerException {
	log.info("Starting generateJunction vertex: " + cornerName + " root: " + rootName + " offset: " + offset + " distmin/max:"
		 + distMin + " " + distMax);
	Object3D corner = graph.findByFullName(cornerName);
	if (corner == null) {
	    throw new Object3DGraphControllerException("Could not find graph corner with name " + cornerName);
	}
	Vector3D cornerPosition = corner.getPosition();
	LinkSet neighborLinks = links.findLinks(corner);
	if ((neighborLinks == null) || (neighborLinks.size() == 0)) {
	    throw new Object3DGraphControllerException("Could not find linked neighbors for graph corner with name " + cornerName);
	}
	Vector3D[] neighborPositions = new Vector3D[neighborLinks.size()];
	for (int i = 0; i < neighborLinks.size(); ++i) {
	    neighborPositions[i] = neighborLinks.get(i).getPartner(corner).getPosition();
	    assert neighborPositions[i] != null;
	    assert neighborPositions[i].distance(cornerPosition) > 0;
	}
	if (nucleotideDB == null) {
	    throw new Object3DGraphControllerException("No reference nucleotides defined. Use command loadnucleotides.");
	}
	GraphJunctionFactory junctionFactory = new GraphJunctionFactory(corner.getPosition(), neighborPositions, nucleotideDB);
	junctionFactory.setOffset(offset);
	junctionFactory.setDistMin(distMin);
	junctionFactory.setDistMax(distMax);
	junctionFactory.setAngleDiv(angleDiv);
	Properties properties = new Properties(); // TODO: add some meaningful info
	try {
	    Object3DLinkSetBundle result = junctionFactory.generate();
	    assert result != null && result.getObject3D().size() > 0; //  && result.getObject3D() instanceof StrandJunction3D;
	    properties = result.getObject3D().getProperties();
	    if (rootName == null) {
		graph.addGraph(result.getObject3D());
	    }
	    else {
		graph.addGraph(result.getObject3D(), rootName);
	    }
	    links.merge(result.getLinks());
	}
	catch (Object3DException oe) {
	    throw new Object3DGraphControllerException("Graph Junction Factory could not generate Junction from graph: " + oe.getMessage());
	}
	return properties;
    }

    public Properties optimizeRnaIntegerGraph(String rootName, int numIter, double threshold, double offset, int verboseLevel) throws Object3DGraphControllerException {
	log.info("Called optimizeRnaIntegerGraph with root: " + rootName + " iter: " + numIter + " thresh: " + threshold
		 + " offset: " + offset);
	Object3D obj = graph.findByFullName(rootName);
	if (obj == null) {
	    throw new Object3DGraphControllerException("Could not find object with name " + rootName);
	}
	Object3DSet graphSet = Object3DLinkSetBundleTools.generateGraphSet(obj, getLinks());
	LinkSet graphLinks = Object3DLinkSetBundleTools.generateGraphLinks(obj, getLinks());
	// obtain potential:
	RnaIntegerPotential potential = new RnaIntegerPotential(graphSet, graphLinks, RnaConstants.TURN_HEIGHT, offset);
	MonteCarloOptimizer optimizer = new MonteCarloOptimizer();
	optimizer.setKt(0.2); // small temperature
	optimizer.setIterMax(numIter);
	optimizer.setVerboseLevel(verboseLevel);
	OptimizationNDResult optResult = optimizer.optimize(potential, potential.generateLowPosition());
	Properties result = new Properties();
	result.setProperty("opt_value", "" + optResult.getBestValue());
	if (optResult.getBestValue() < threshold) {
	    log.info("Setting optimized graph positions!");
	    double[] positions = optResult.getBestPosition();
	    for (int i = 0; i < graphSet.size(); ++i) {
		Vector3D v = new Vector3D(positions[3*i], positions[3*i+1], positions[3*i+2]);
		graphSet.get(i).setPosition(v);
	    }
	}
	else {
	    log.info("Not copying coordinates because threshold " + threshold
		     + " not reached in optimization: " + optResult.getBestValue());
	}
	return result;
    }

    /** Traces graph with RNA double helices.
     * @param offset Length of ends not covered by helices
     */
    public Properties traceRnaGraph(String rootName, int numIter, double kt, 
				    double threshold, double offset, double distMax, double distMin,
				    String baseName, int verboseLevel, boolean generateBridges,
				    int helixLenMax) throws Object3DGraphControllerException {
	log.info("Called traceRnaGraph with root: " + rootName + " iter: " + numIter + " thresh: " + threshold + " distmax/min:" + distMax + " " + distMin
		  + " offset: " + offset + " max helix length: " + helixLenMax);
	if (nucleotideDB == null) {
	    throw new Object3DGraphControllerException("Cannot start traceRnaGraph: No nucleotide database defined.");
	}
	if (!Object3DTools.validateName(baseName)) {
	    throw new Object3DGraphControllerException("Cannot start traceRnaGraph: Invalid base name: " + baseName);
	}
	Object3D obj = graph.findByFullName(rootName);
	if (obj == null) {
	    throw new Object3DGraphControllerException("Could not find object with name " + rootName);
	}
	Object3DSet graphSet = Object3DLinkSetBundleTools.generateGraphSet(obj, getLinks());
	LinkSet graphLinks = Object3DLinkSetBundleTools.generateGraphLinks(obj, getLinks());
	log.info("Found " + graphLinks.size() + " graph links to be traced by RNA.");
	// validate graph:
	if (!TraceRnaPotential.validate(graphSet, graphLinks, RnaConstants.TURN_HEIGHT,
					offset)) {
	    throw new Object3DGraphControllerException("The graph to be traced by RNA did not validate with the provided helix parameters. It probably contains too short edges.");
	}
	// obtain potential:
	TraceRnaPotential potential = new TraceRnaPotential(graphSet, graphLinks, RnaConstants.TURN_HEIGHT, offset,
							    distMax, distMin, helixLenMax);
	potential.setGenerateBridgesMode(generateBridges);
	potential.setVerboseLevel(verboseLevel);
	MonteCarloOptimizer optimizer = new MonteCarloOptimizer();
	optimizer.setIterMax(numIter);
	optimizer.setKt(kt);
	optimizer.setVerboseLevel(verboseLevel);
	log.info("Starting Monte Carlop optimization with " + numIter + " steps and temperature " + kt);
	OptimizationNDResult optResult = optimizer.optimize(potential, potential.generateLowPosition());
	log.info("Trace helix optimization finished with properties: " + optResult);
	Properties result = new Properties();
	result.setProperty("opt_value", "" + optResult.getBestValue());
	assert nucleotideDB != null;
	if (optResult.getBestValue() < threshold) {
	    log.info("Adding traced RNA graph!");
	    assert (optResult.getBestPosition() != null);
	    List<Object3DLinkSetBundle> bridgeBundle = null; // list of all possible bridge structures
            if (generateBridges) {
		if (bridgeController == null) {
		    initBridgeController();
		}
		bridgeBundle = bridgeController.getBundleList();
	    }
	    Object3DLinkSetBundle bundle = potential.generateRna(optResult.getBestPosition(), baseName, nucleotideDB, bridgeBundle);
	    graph.addGraph(bundle.getObject3D());
	    links.merge(bundle.getLinks());
	    log.info("Finished Adding traced RNA graph!");
	}
	fireModelChangedOnGraphAndLinkController(new ModelChangeEvent(this, Object3DGraphControllerEventConstants.MODEL_MODIFIED));
	return result;
    }

    /** Traces graph with RNA double helices.
     * @param offset Length of ends not covered by helices
     */
    public Properties traceAndModifyRnaGraph(String rootName, int numIter, double kt, 
					     double threshold, double offset, double distMax, double distMin,
					     String baseName, int verboseLevel) throws Object3DGraphControllerException {
	log.info("Called traceRnaGraph with root: " + rootName + " iter: " + numIter + " thresh: " + threshold + " distmax/min:" + distMax + " " + distMin
		  + " offset: " + offset);
	if (nucleotideDB == null) {
	    throw new Object3DGraphControllerException("Cannot start traceRnaGraph: No nucleotide database defined.");
	}
	if (!Object3DTools.validateName(baseName)) {
	    throw new Object3DGraphControllerException("Cannot start traceRnaGraph: Invalid base name: " + baseName);
	}
	Object3D obj = graph.findByFullName(rootName);
	if (obj == null) {
	    throw new Object3DGraphControllerException("Could not find object with name " + rootName);
	}
	Object3DSet graphSet = Object3DLinkSetBundleTools.generateGraphSet(obj, getLinks());
	LinkSet graphLinks = Object3DLinkSetBundleTools.generateGraphLinks(obj, getLinks());
	// validate graph:
	if (!TraceRnaGraphPotential.validate(graphSet, graphLinks, RnaConstants.TURN_HEIGHT,
					offset)) {
	    throw new Object3DGraphControllerException("The graph to be traced by RNA did not validate with the provided helix parameters. It probably contains too short edges.");
	}
	// obtain potential:
	TraceRnaGraphPotential potential = new TraceRnaGraphPotential(graphSet, graphLinks, RnaConstants.TURN_HEIGHT, offset,
							    distMax, distMin);
	potential.setVerboseLevel(verboseLevel);
	MonteCarloOptimizer optimizer = new MonteCarloOptimizer();
	optimizer.setIterMax(numIter);
	optimizer.setKt(kt);
	optimizer.setVerboseLevel(verboseLevel);
	log.info("Starting Monte Carlop optimization with " + numIter + " steps and temperature " + kt);
	OptimizationNDResult optResult = optimizer.optimize(potential, potential.generateLowPosition());
	log.info("Trace helix optimization finished with properties: " + optResult);
	Properties result = new Properties();
	result.setProperty("opt_value", "" + optResult.getBestValue());
	assert nucleotideDB != null;
	if (optResult.getBestValue() < threshold) {
	    log.info("Adding traced RNA graph!");
	    assert (optResult.getBestPosition() != null);
	    Object3DLinkSetBundle bundle = potential.generateRna(optResult.getBestPosition(), baseName, nucleotideDB);
	    graph.addGraph(bundle.getObject3D());
	    links.merge(bundle.getLinks());
	}
	fireModelChangedOnGraphAndLinkController(new ModelChangeEvent(this, Object3DGraphControllerEventConstants.MODEL_MODIFIED));
	return result;
    }

    /** Optimizes helices specified by helix constraints */
    public Properties optimizeHelices(int numSteps, double errorScoreLimit, double errorScoreInitialLimit, double kt, FitParameters stemFitParameters,
		      String stemRootName, Object3DSet movableObjects, Object3DSet parentObjects, int connectionAlgorithm,
				      int fuseStrandsMode, boolean firstFixedMode, int helixInterval) throws Object3DGraphControllerException {
	log.info("Starting optimizeHelices...");
	Properties properties = null;
	Object3D stemRoot = graph.findByFullName(stemRootName);
	if (stemRoot == null) {
	    throw new Object3DGraphControllerException("Could not find object " + stemRootName);
	}
	try {
	    properties = HelixOptimizerTools.optimizeHelices(numSteps, errorScoreLimit, errorScoreInitialLimit, kt, stemFitParameters,
							     movableObjects, parentObjects,
							     stemRoot, getLinks(), nucleotideDB, connectionAlgorithm, fuseStrandsMode,
							     firstFixedMode, helixInterval);
	}
	catch (FittingException fe) {
	    throw new Object3DGraphControllerException(fe.getMessage());
	}
	assert properties != null;
// 	HelixOptimizer optimizer = new HelixOptimizer(getGraph().getGraph(), getLinks(), numSteps, errorScoreLimit);
// 	if ((movableObjects != null) && (movableObjects.size() > 0)) {
// 	    log.fine("Setting movable objects: " + movableObjects.size());
// 	    optimizer.setMovableObjects(movableObjects); // keep everything else fixed in this mode
// 	}
// 	else {
// 	    log.fine("All objects are considered movable in optimization!");
// 	}
// 	Properties properties = optimizer.optimize();
// 	log.fine("Optimization of helices finished: " + properties);
// 	if (stemFitParameters != null) {
// 	    int connectionAlgorithm = BranchDescriptorOptimizerFactory.MONTE_CARLO_OPTIMIZER; /** Algorithm used for stem interpolation. */
// 	    for (int i = 0; i < links.size(); ++i) {
// 		if (links.get(i) instanceof HelixConstraintLink) {
// 		    HelixConstraintLink hcl = (HelixConstraintLink)(links.get(i));
// 		    BranchDescriptor3D bd1 = (BranchDescriptor3D)(hcl.getObj1());
// 		    BranchDescriptor3D bd2 = (BranchDescriptor3D)(hcl.getObj2());
// 		    // TODO : name generation looks like workaround, clean up
// 		    String helixName = "h_" + Object3DTools.getFullName(bd1).replace('.','_') 
// 			+ "_" + Object3DTools.getFullName(bd2).replace('.','_'); // replace dot with underscore for save name generateion
// 		    log.fine("Starting to generate interpolating helix with length " + hcl.getBasePairMin());
// 		    Object3DLinkSetBundle stemBundle = ConnectJunctionTools.generateConnectingStem(bd1, bd2,
// 				   helixName, stemFitParameters, nucleotideDB, connectionAlgorithm, hcl.getBasePairMin());
// 		    if (stemBundle != null) {
// 			log.info("Stem bundle generated with ConnectJunctionTools!");
// 			if (stemBundle.getObject3D() != null) {
// 			    log.fine("Adding helix object " + stemBundle.getObject3D().getName()
// 				     + " to tree node: " + stemRootName);
// 			    graph.addGraph(stemBundle.getObject3D(), stemRootName);
// 			    links.merge(stemBundle.getLinks());
// 			    PropertyTools.addProperty(properties, "generated_helices", stemBundle.getObject3D().getName());
// 			}
// 			else {
// 			    log.fine("No stems were generated!");
// 			}
// 		    }
// 		    else {
// 			log.fine("No stems and links were generated!");
// 		    }
// 		}
// 	    }
// 	}
	// refresh(new ModelChangeEvent(this, Object3DGraphControllerEventConstants.MODEL_MODIFIED));
	fireModelChangedOnGraphAndLinkController(new ModelChangeEvent(this, Object3DGraphControllerEventConstants.MODEL_MODIFIED));
	log.info("Finished optimizeHelices...");
	return properties;
    }

    /** Optimizes blocks according to base pair constraints */
    public Properties optimizeBasepairs(int numSteps, double errorScoreLimit, 
					double electrostaticWeight,
					double vdwWeight,
					double kt,
					List<Object3DSet> objectBlocks,
					List<Vector3D> fixedRotationAxisList,
					int[] symVdwActive,
					boolean keepFirstFixed,
					boolean placeBaseJunction,
					String rootName) throws Object3DGraphControllerException {
	assert objectBlocks != null;
	assert objectBlocks.size() > 0;
	assert rootName != null;
	Properties properties = BasepairOptimizerTools.optimizeBasepairs(links, basePairDB, numSteps, errorScoreLimit, electrostaticWeight,
									 vdwWeight, kt, objectBlocks, fixedRotationAxisList,
									 symVdwActive, keepFirstFixed, symmetryController.getSymCopies());
	assert properties != null;
	if (placeBaseJunction) {
	    log.info("Placing junctions corresponding to junction data base constraints...");
	    Set<Object> propKeys = properties.keySet(); // stringPropertyNames();
	    Iterator<Object> it = propKeys.iterator();
	    while (it.hasNext()) {
		String key = (String)(it.next());
		String valueString = properties.getProperty(key);
		assert (valueString != null);
		log.info("Checking key-value: " + key + " : " + valueString);
		String[] words = key.split("\\.");
		if ((words.length >= 2) 
		    && (words[words.length-1].equals(PdbJunctionController.JUNCTION_DB_NAME)
			|| words[words.length-1].equals(PdbJunctionController.KISSING_LOOP_DB_NAME))) {
		    boolean kissingLoopMode = words[words.length-1].equals(PdbJunctionController.KISSING_LOOP_DB_NAME);
		    log.info("Found junction key-value: " + key + " : " + valueString);
		    String[] orderId = valueString.split(" ");
		    assert orderId.length > 2;
		    try {
			int junctionOrder = Integer.parseInt(orderId[0]);
			int junctionId = Integer.parseInt(orderId[1]);
			String linkName = words[0];
			String strandEnding = "_" + linkName;
			String junctionName = linkName + "_" + junctionId;
			Vector3D position = new Vector3D(0,0,0);
			log.info("placing " + junctionOrder + " " + junctionId + " " + junctionName);
			StrandJunction3D junction = null;
			if (!kissingLoopMode) {
			    junction = junctionController.getJunctionDB().getJunction(junctionOrder, junctionId);
			    if (junction == null) {
				throw new Object3DGraphControllerException("Could not find appropriate junction: "
									   + junctionOrder + " " + junctionId); 
			    }
			} else {
			    junction = junctionController.getKissingLoopDB().getJunction(junctionOrder, junctionId);
			    if (junction == null) {
				throw new Object3DGraphControllerException("Could not find appropriate kissing loop: "
									   + junctionOrder + " " + junctionId); 
			    }
			}
			assert junction != null;
			List<StrandJunction3D> tmpJunctionList = new ArrayList<StrandJunction3D>();
			tmpJunctionList.add(junction);
			List<BranchDescriptor3D> branches = new ArrayList<BranchDescriptor3D>();
			for (int i = 2; i < orderId.length; ++i) {
			    String branchName = orderId[i];
			    Object3D obj = getGraph().findByFullName(branchName);
			    assert (obj != null) && (obj instanceof BranchDescriptor3D);
			    branches.add((BranchDescriptor3D)obj);
			}
			assert (branches.size() == junctionOrder);
			if (junctionOrder == 2) {
			    HelixBridgeFinder helixBridgeFinder = new HelixBridgeFinder(tmpJunctionList, nucleotideDB);
			    helixBridgeFinder.setInversionMode(false); // branch descriptors point outwards
			    helixBridgeFinder.setRms(16.0); // FIXIT just for debugging
			    List<Object3DLinkSetBundle> placedJunctions = helixBridgeFinder.findHelixBridge(branches.get(0), branches.get(1));
			    List<Object3DLinkSetBundle> placedJunctions2 = helixBridgeFinder.findHelixBridge(branches.get(1), branches.get(0));
			    double scoreLim = 1e10;
			    double score1 = scoreLim;
			    double score2 = scoreLim;
			    if (placedJunctions.size() > 0) {
				score1 = Double.parseDouble(placedJunctions.get(0).getObject3D().getProperty("score"));
			    } else {
				log.info("No fitting junction found.");
			    }
			    if (placedJunctions2.size() > 0) {
				score2 = Double.parseDouble(placedJunctions2.get(0).getObject3D().getProperty("score"));
			    } else {
				log.info("No fitting junction found (2).");
			    }
			    if (score1 <= score2) {
				if (score1 < scoreLim) {
				    log.info("Placing best fitting junction!");
				    addBundle(placedJunctions.get(0));
				}
			    } else if (score2 < scoreLim) {
				    log.info("Placing best fitting junction (2)!");
				    addBundle(placedJunctions2.get(0));				
			    }
			} else {
			    log.warning("Placing of junctions with an order greater 2 is currently not implemented!");
			}
			// placeJunction(junctionOrder, junctionId, rootName, junctionName, position, strandEnding);
		    } catch (NumberFormatException nfe) {
			log.severe("Internal error: there was now parsable junction id: " + nfe.getMessage());
			assert false;
		    }
		} else {
		    if (words.length >= 2) {
			log.info("not junctionId descriptor: " + words.length + " " + words[words.length-1]);
		    } else {
			log.info("too short junctionId descriptor: " + words.length + " " + key);
		    }
		}
	    }
	}
	refresh(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED));
	fireModelChangedOnGraphAndLinkController(new ModelChangeEvent(this, Object3DGraphControllerEventConstants.MODEL_MODIFIED));
	log.info("Result of optimizeBasePairs:");
	PropertyTools.printProperties(System.out, properties);
	return properties;
    }

    /** Optimizes blocks according to base pair constraints */
    public Properties optimizeSystematic(double electrostaticWeight,
					 double vdwWeight,
					 List<Object3DSet> objectBlocks,
					 IntegerPermutatorList permutators,
					 int outputIntervall) throws Object3DGraphControllerException {
	assert objectBlocks != null;
	assert objectBlocks.size() > 0;
	SystematicBasepairOptimizer basepairOptimizer = new SystematicBasepairOptimizer(links, objectBlocks, basePairDB, permutators);
	basepairOptimizer.setElectrostaticWeight(electrostaticWeight);
	basepairOptimizer.setOutputInterval(outputIntervall);
	basepairOptimizer.setVdwWeight(vdwWeight);
	Properties properties = basepairOptimizer.optimize();
	assert properties != null;
	refresh(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED));
	fireModelChangedOnGraphAndLinkController(new ModelChangeEvent(this, Object3DGraphControllerEventConstants.MODEL_MODIFIED));
	return properties;
    }
		
		public void randomizeBlocks(double radius, List<Object3DSet> blocks, boolean orient){
			for(Object3DSet block : blocks){
				Vector3D rand = Vector3DTools.randomPointInSphere(radius);
				for(Object3D obj : block.getAsList()){
					obj.setRelativePosition(rand);
					if(orient){
						Object3DTools.randomizeOrientation(rand, obj);
					}
				}
			}
		}

    /** Optimizes blocks according to distance constraints
     * very similar to optimizeBaseapair, unify! Done! */
//     public Properties optimizeDistances(int numSteps, double errorScoreLimit, 
// 					List<Object3DSet> objectBlocks) throws Object3DGraphControllerException {
// 	assert objectBlocks != null;
// 	Optimizer optimizer = new DistanceOptimizer(graph.getGraph(), links, objectBlocks, numSteps, errorScoreLimit, basePairDB);
// 	Properties properties = optimizer.optimize();
// 	assert properties != null;
// 	refresh(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED));
// 	fireModelChangedOnGraphAndLinkController(new ModelChangeEvent(this, Object3DGraphControllerEventConstants.MODEL_MODIFIED));
// 	return properties;
//     }


	/**Removes singlestrand regions from molecule and splits strands where removal occurs*/
	public void prepareScoreConstraints( SecondaryStructure firststructure ) throws DuplicateNameException,Object3DGraphControllerException {
		Properties properties = null;
		
		//SecondaryStructure firststructure = generateSecondaryStructure();
		List<Interaction> firststrandBreaks = SecondaryStructureTools.findSingleStrands(firststructure);
		int size = firststrandBreaks.size();
		for(int i=0; i<size; i++){
			SecondaryStructure structure = generateSecondaryStructure(); //must redo every time because splitting strand changes results
			List<Interaction> strandBreaks = SecondaryStructureTools.findSingleStrands(structure);
			Interaction inter = strandBreaks.get(0);
			Residue res1 = inter.getResidue1();
			Residue res2 = inter.getResidue2();
			String res1Name = ("root.import."+((Sequence)res1.getParentObject()).getName()+"."+(res1.getPos()+1));
			String res2Name = ("root.import."+((Sequence)res2.getParentObject()).getName()+"."+(res2.getPos()+1));
			double dist = (res2.getPos()-res1.getPos());
			ConstraintLink newLink = addDistanceConstraint( res1Name, res2Name, 5.0, dist , 0, 0);
			String strandName = ( "root.import."+ ((Sequence)res2.getParentObject()).getName() );
			graph.splitStrand( strandName, res2.getPos() );
		}
		/*InteractionSet interactions = structure.getInteractions();
		HashSet<Residue> paired = new HashSet<Residue>(); //holds all residues that are paired
		for (int i=0; i<interactions.size(); i++) {
			Interaction inter = interactions.get(i);
			paired.add( inter.getResidue1() );
			paired.add( inter.getResidue2() );
		}
		ArrayList<String> removeNames = new ArrayList<String>();
		for(int i=structure.getSequenceCount()-1; i>-1; i--){
			Sequence seq = structure.getSequence(i);
			for(int j=seq.size()-1; j>-1; j--){
				Residue res = seq.getResidue(j);
				if(!paired.contains(res)){
					String strandName = ("root.import."+ seq.getName());
					graph.splitStrand( strandName, j);
				}
			}
		}
		*/
		removeAllUnpaired();
	}

    public void splitStrand(String strandName, int position) throws Object3DGraphControllerException {
	graph.splitStrand( strandName, position );
	ModelChangeEvent event = new ModelChangeEvent(this, eventConst.MODEL_MODIFIED);
	graph.fireModelChanged(event);
	links.fireModelChanged(event);
    }

    private SecondaryStructureScorer findScorer(int scoringFunction) {
	switch (scoringFunction) {
        case DUMMY_SCORER:
            {
                log.info("Setting scorer to dummy mode!");
                DummyEnergySecondaryStructureScorer scorer = new DummyEnergySecondaryStructureScorer();
                scorer.setZeroMode(true); // always return zero!
                return scorer;
            }
	case DEFAULT_SCORER: 
	    return null;
        case RNACOFOLD_SCORER:
            log.info("Setting scorer to RNAcofold mode!");
            return new RnacofoldSecondaryStructureScorer();
        case PKNOTS_SCORER:
            log.info("Setting scorer to pknotsRG mode!");
            RnacofoldSecondaryStructureScorer scorer = new RnacofoldSecondaryStructureScorer();
            scorer.setPkMode(true);
            return scorer;
        case NUPACK_SCORER:
            log.info("Setting scorer to NUPACK mode!");
            return new NupackStructureScorer();
        case CRITON_SCORER:
            log.info("Setting scorer to criton-scorer!");
            return new CritonScorer();
	case PATHWAY_SCORER:
	    log.info("Setting scorer to pathway-scorer not supported anymore!");
	    assert false;
            return null; // new PathwaySecondaryStructureScorer();
        default:
            log.severe("Unknown secondary structure scorer id!");
            assert false;
	};
	return null;
    }

    /**
     * Optimizes sequences
     * TODO: import of optimized sequences.
     */
    public void optimizeSequences(boolean importSequenceFlag,
				  int optimizerAlgorithm,
				  int scoringFunction,
				  int scoringFunction2,
				  double rmsLimit,
				  double errorLimit,
				  int iterMax,
				  int iter2Max,
				  int rerun,
				  Properties params)
	throws IOException, DuplicateNameException, Object3DGraphControllerException, UnknownSymbolException {
	
	// Debug
	System.out.println("");
	System.out.println(">>>>> Running 2-step optimization algorithm <<<<<");
	System.out.println("");

	SecondaryStructure secStruct = generateSecondaryStructure();
	MonteCarloSequenceOptimizer sequenceOptimizer = new MonteCarloSequenceOptimizer();
	System.out.println("MonteCarloSequenceOptimizer initialized");
	sequenceOptimizer.setIterMax(iterMax);
	sequenceOptimizer.setIter2Max(iter2Max);
	sequenceOptimizer.setRerun(rerun);
	sequenceOptimizer.setErrorScoreLimit(errorLimit);
	SecondaryStructureScorer scorer1 = findScorer(scoringFunction);
	if (scorer1 != null) {
	    sequenceOptimizer.setDefaultScorer(scorer1);
	}
	assert sequenceOptimizer.getDefaultScorer() != null;
	SecondaryStructureScorer scorer2 = findScorer(scoringFunction2);
	if (scorer2 != null) {
	    sequenceOptimizer.setFinalScorer(scorer2);
	}
	assert sequenceOptimizer.getFinalScorer() != null;

	if ((secStruct == null) || (secStruct.getSequenceCount() == 0)) {
	    log.warning("No sequences defined that can be optimized!");
	    return;
	}
	if (optimizerAlgorithm == NO_SEQUENCE_OPTIMIZATION) {
	    log.info("No sequence optimization performed!");
	    return;
	}
	log.info("Starting optimizeSequences");
	String[] sequenceStrings;
	if (optimizerAlgorithm != SEQUENCE_OPTIMIZER_SCRIPT) {
	    sequenceStrings = sequenceOptimizer.optimize(secStruct);
	}
	else {
	    // write secondary structure to temporary file
	    File tmpInputFile = File.createTempFile("nanotiler",".sec");
	    String inputFileName = tmpInputFile.getAbsolutePath();
	    FileOutputStream fos = new FileOutputStream(tmpInputFile);
	    PrintStream ps = new PrintStream(fos);
	    // write secondary structure, but only write sec structure,
	    // not sequence:
	    SecondaryStructureWriter secWriter = new SecondaryStructureScriptFormatWriter();
	    log.info("Writing to file: " + inputFileName);
	    log.info("Writing content: " + secWriter.writeString(secStruct));
	    ps.println(secWriter.writeString(secStruct));
	    fos.close();
	    File tmpOutputFile = File.createTempFile("nanotiler", ".seq");
	    final String outputFileName = tmpOutputFile.getAbsolutePath();
	    if (tmpOutputFile.exists()) {
		tmpOutputFile.delete();
	    }
	    // generate command string
	    File tempFile = new File(rnaInverseScriptName);
	    String[] commandWords = {rnaInverseScriptName, inputFileName,
				     outputFileName}; 
	    // generate command
	    RunCommand command = new SimpleRunCommand(commandWords);
	    // create queue manager (singleton pattern)
	    QueueManager queueManager = SimpleQueueManager.getInstance();
	    // generate job
	    Job job = queueManager.createJob(command);
	    // add listener to job
	    // launch command
	    queueManager.submit(job);	    
	    log.info("queue manager finished job!");
	    // open output file:
	    log.info("Importing optimized sequences from " + outputFileName);
	    try {
		FileInputStream resultFile = new FileInputStream(outputFileName);
		String[] resultLines = StringTools.readAllLines(resultFile);
		sequenceStrings = RnaInverseTools.getSequenceStrings(resultLines);
	    }
	    catch (IOException ioe) {
		log.warning("Error when scraping result file from: " 
			    + outputFileName);
		throw ioe;
	    }
	}
	if (sequenceStrings == null) {
	    log.info("Could not find sufficiently optimized sequences!");
	    return;
	}
	if (importSequenceFlag) {
	    log.info("The new sequences are: ");	
	    overwriteSequences(sequenceStrings, null, rmsLimit, false); // FIXIT forceMode
	    modelChanged(new ModelChangeEvent(this,
					      eventConst.MODEL_MODIFIED));
	    log.info("Finished importing sequences!");
	}
	else { // do not import sequences
	    log.info("Optimized sequences were by request not imported!");
	}
	log.info("Finished optimize sequences!");
    }
    
    /**
     * Optimizes sequences
     * TODO: import of optimized sequences.
     */
    public void optimizeSequences(boolean importSequenceFlag,
                                  int optimizerAlgorithm,
                                  int scoringFunction,
                                  int scoringFunction2,
				  int scoringFunction3,
                                  double rmsLimit,
                                  double errorLimit,
                                  int iterMax,
                                  int iter2Max,
				  int iter3Max,
                                  int rerun)
        throws IOException, DuplicateNameException, Object3DGraphControllerException, UnknownSymbolException {
                                                                                                                                             
        SecondaryStructure secStruct = generateSecondaryStructure();
        MonteCarloSequenceOptimizer3Step sequenceOptimizer = new MonteCarloSequenceOptimizer3Step();
        System.out.println("MonteCarloSequenceOptimizer initialized");
        sequenceOptimizer.setIterMax(iterMax);
        sequenceOptimizer.setIter2Max(iter2Max);
	sequenceOptimizer.setIter3Max(iter3Max);
        sequenceOptimizer.setRerun(rerun);
        sequenceOptimizer.setErrorScoreLimit(errorLimit);
        sequenceOptimizer.setDefaultScorer(findScorer(scoringFunction));
	sequenceOptimizer.setMidScorer(findScorer(scoringFunction2));
        sequenceOptimizer.setFinalScorer(findScorer(scoringFunction3));

        if ((secStruct == null) || (secStruct.getSequenceCount() == 0)) {
            log.warning("No sequences defined that can be optimized!");
            return;
        }
        if (optimizerAlgorithm == NO_SEQUENCE_OPTIMIZATION) {
            log.info("No sequence optimization performed!");
            return;
        }
        log.info("Starting optimizeSequences");
        String[] sequenceStrings;
        if (optimizerAlgorithm != SEQUENCE_OPTIMIZER_SCRIPT) {
	    sequenceStrings = sequenceOptimizer.optimize(secStruct);
        }
        else {
            // write secondary structure to temporary file
            File tmpInputFile = File.createTempFile("nanotiler",".sec");
            String inputFileName = tmpInputFile.getAbsolutePath();
            FileOutputStream fos = new FileOutputStream(tmpInputFile);
            PrintStream ps = new PrintStream(fos);
            // write secondary structure, but only write sec structure,
            // not sequence:
            SecondaryStructureWriter secWriter = new SecondaryStructureScriptFormatWriter();
            log.info("Writing to file: " + inputFileName);
            log.info("Writing content: " + secWriter.writeString(secStruct));
            ps.println(secWriter.writeString(secStruct));
            fos.close();
            File tmpOutputFile = File.createTempFile("nanotiler", ".seq");
            final String outputFileName = tmpOutputFile.getAbsolutePath();
            if (tmpOutputFile.exists()) {
                tmpOutputFile.delete();
            }
            // generate command string
            File tempFile = new File(rnaInverseScriptName);
            String[] commandWords = {rnaInverseScriptName, inputFileName,
                                     outputFileName};
            // generate command
            RunCommand command = new SimpleRunCommand(commandWords);
            // create queue manager (singleton pattern)
            QueueManager queueManager = SimpleQueueManager.getInstance();
            // generate job
            Job job = queueManager.createJob(command);
            // add listener to job
	    // launch command
            queueManager.submit(job);
            log.info("queue manager finished job!");
            // open output file:
            log.info("Importing optimized sequences from " + outputFileName);
            try {
                FileInputStream resultFile = new FileInputStream(outputFileName);
                String[] resultLines = StringTools.readAllLines(resultFile);
                sequenceStrings = RnaInverseTools.getSequenceStrings(resultLines);
            }
            catch (IOException ioe) {
                log.warning("Error when scraping result file from: "
                            + outputFileName);
                throw ioe;
            }
        }
        if (sequenceStrings == null) {
            log.info("Could not find sufficiently optimized sequences!");
            return;
        }
        if (importSequenceFlag) {
            log.info("The new sequences are: ");
            overwriteSequences(sequenceStrings, null, rmsLimit, false); // FIXIT forceMode
            modelChanged(new ModelChangeEvent(this,
                                              eventConst.MODEL_MODIFIED));
            log.info("Finished importing sequences!");
        }
        else { // do not import sequences
            log.info("Optimized sequences were by request not imported!");
        }
        log.info("Finished 3-step sequence optimization!");
    }

    
    /** set all residues of all but first sequence to seqstatus = "ignore", unless this property is already defined. */
    private void initRiboswitchIgnoreStatus(SecondaryStructure secStruct) {
	for (int i = 1; i < secStruct.getSequenceCount(); ++i) {
	    Sequence seq = secStruct.getSequence(i);
	    for (int j = 0; j < seq.size(); ++j) {
		Residue res = seq.getResidue(j);
		String status = res.getProperty("seqstatus");
		if ((status == null) || (status.length() == 0)) {
		    res.setProperty("seqstatus", "ignore");
		}
	    }
	}
    }

    /**
     * Optimizes sequences to be bistable : depending on sequence weights (== concentrations), either main or alternative structure is predicted 
     */
    public void optimizeBistableSequences(boolean importSequenceFlag,
					  int optimizerAlgorithm,
					  double rmsLimit,
					  double errorLimit,
					  int iterMax,
					  int iter2Max,
					  int rerun)
	throws IOException, DuplicateNameException, Object3DGraphControllerException, UnknownSymbolException {
	SecondaryStructure secStruct = generateSecondaryStructure();
	initRiboswitchIgnoreStatus(secStruct); // set all residues of all but first sequence to seqstatus = "ignore"
	MonteCarloSequenceOptimizer sequenceOptimizer = new MonteCarloSequenceOptimizer();
	sequenceOptimizer.setFinalScorer(new RiboswitchSecondaryStructureScorer());
	// sequenceOptimizer.setIgnoreNotConstantMode(true); // only care about switching of constant part
	sequenceOptimizer.setIterMax(iterMax);
	sequenceOptimizer.setIter2Max(iter2Max);
	sequenceOptimizer.setRerun(rerun);
	sequenceOptimizer.setErrorScoreLimit(errorLimit);
	if ((secStruct == null) || (secStruct.getSequenceCount() == 0)) {
	    log.warning("No sequences defined that can be optimized!");
	    return;
	}
	if (optimizerAlgorithm == NO_SEQUENCE_OPTIMIZATION) {
	    log.info("No sequence optimization performed!");
	    return;
	}
	log.info("Starting optimizeSequences");
	String[] sequenceStrings;
	sequenceStrings = sequenceOptimizer.optimize(secStruct);
	if (sequenceStrings == null) {
	    log.info("Could not find succifiently optimized sequences!");
	    return;
	}
	if (importSequenceFlag) {
	    log.info("The new sequences are: ");	
	    overwriteSequences(sequenceStrings, null, rmsLimit, false); // FIXIT forceMode
	    modelChanged(new ModelChangeEvent(this,
					      eventConst.MODEL_MODIFIED));
	    log.info("Finished importing sequences!");
	}
	else { // do not import sequences
	    log.info("Optimized sequences were by request not imported!");
	}
	log.info("Finished optimize sequences!");
    }

    /**
     * Optimizes sequences to be bistable : depending on sequence weights (== concentrations), either main or alternative structure is predicted 
     */
    /*
    public void optimizeBistableSequences(SecondaryStructure secStruct2,
					  boolean importSequenceFlag,
					  int optimizerAlgorithm,
					  double rmsLimit,
					  double errorLimit,
					  int iterMax,
					  int iter2Max,
					  int rerun)
	throws IOException, DuplicateNameException, Object3DGraphControllerException, UnknownSymbolException {
	SecondaryStructure secStruct = generateSecondaryStructure();
	MonteCarloBistableSequenceOptimizer sequenceOptimizer = new MonteCarloBistableSequenceOptimizer();
	sequenceOptimizer.setIterMax(iterMax);
	sequenceOptimizer.setIter2Max(iter2Max);
	sequenceOptimizer.setRerun(rerun);
	sequenceOptimizer.setErrorScoreLimit(errorLimit);
	if ((secStruct == null) || (secStruct.getSequenceCount() == 0)) {
	    log.warning("No sequences defined that can be optimized!");
	    return;
	}
	if (optimizerAlgorithm == NO_SEQUENCE_OPTIMIZATION) {
	    log.info("No sequence optimization performed!");
	    return;
	}
	log.info("Starting optimizeSequences");
	String[] sequenceStrings;
	sequenceStrings = sequenceOptimizer.optimize(secStruct, secStruct2);
	if (sequenceStrings == null) {
	    log.info("Could not find succifiently optimized sequences!");
	    return;
	}
	if (importSequenceFlag) {
	    log.info("The new sequences are: ");	
	    overwriteSequences(sequenceStrings, rmsLimit);
	    modelChanged(new ModelChangeEvent(this,
					      eventConst.MODEL_MODIFIED));
	    log.info("Finished importing sequences!");
	}
	else { // do not import sequences
	    log.info("Optimized sequences were by request not imported!");
	}
	log.info("Finished optimize sequences!");
    }
    */

    /**
     * Optimizes sequences to be bistable : depending on sequence weights (== concentrations), either main or alternative structure is predicted 
     * @param altFileName file name of alternative secondary structure.
     */
    /*
    public void optimizeBistableSequences(String altFileName,
					  boolean importSequenceFlag,
					  int optimizerAlgorithm,
					  double rmsLimit,
					  double errorLimit,
					  int iterMax,
					  int iter2Max,
					  int rerun)
	throws ParseException, IOException, DuplicateNameException, Object3DGraphControllerException, UnknownSymbolException {
	ImprovedSecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
	optimizeBistableSequences(parser.parse(altFileName),
				  importSequenceFlag,
				  optimizerAlgorithm,
				  rmsLimit,
				  errorLimit,
				  iterMax,
				  iter2Max,
				  rerun);
    }
    */

    /** places cloned kissing loop of order order with index n to current graph, sets it at a certain position */
    public StrandJunction3D placeKissingLoop(int order, int n, String rootName, String newName, Vector3D position,
				 String strandEnding) throws Object3DGraphControllerException {
	StrandJunction3D junction = (StrandJunction3D)(junctionController.getKissingLoopDB().getJunction(order, n));
	if (junction == null) {
	    throw new Object3DGraphControllerException("No junction of order " + (order) + " and id " 
						       + (n+1) + " is currently loaded.");
	}
	junction = (StrandJunction3D)(junction.cloneDeep());
	junction.setPosition(position);
	junction.setName(newName);
	if ((strandEnding != null) && (strandEnding.length() > 0)) {
	    // modify names of rna strands to avoid duplicate strand names:
	    Set<String> allowedNames = new HashSet<String>();
	    allowedNames.add("RnaStrand");
	    Set<String> forbiddenNames = new HashSet<String>();
	    Object3DTools.addEnding(junction, strandEnding, allowedNames, forbiddenNames);
	}
	// workspace.insertChild(junction);
	graph.addGraph(junction, rootName);
	// modelChanged(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED));
	ModelChangeEvent event = new ModelChangeEvent(this, eventConst.MODEL_MODIFIED);
	graph.fireModelChanged(event);
	links.fireModelChanged(event);
	return junction;
    }

    /** places cloned general motif of order order with index n to current graph, sets it at a certain position */
    public StrandJunction3D placeMotif(int order, int n, String rootName, String newName, Vector3D position,
				       String strandEnding) throws Object3DGraphControllerException {
	StrandJunction3D junction = (StrandJunction3D)(junctionController.getMotifDB().getJunction(order, n));
	if (junction == null) {
	    throw new Object3DGraphControllerException("No junction of order " + (order) + " and id " 
						       + (n+1) + " is currently loaded.");
	}
	junction = (StrandJunction3D)(junction.cloneDeep());
	junction.setPosition(position);
	junction.setName(newName);
	if ((strandEnding != null) && (strandEnding.length() > 0)) {
	    // modify names of rna strands to avoid duplicate strand names:
	    Set<String> allowedNames = new HashSet<String>();
	    allowedNames.add("RnaStrand");
	    Set<String> forbiddenNames = new HashSet<String>();
	    Object3DTools.addEnding(junction, strandEnding, allowedNames, forbiddenNames);
	}
	// workspace.insertChild(junction);
	graph.addGraph(junction, rootName);
	// modelChanged(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED));
	ModelChangeEvent event = new ModelChangeEvent(this, eventConst.MODEL_MODIFIED);
	graph.fireModelChanged(event);
	links.fireModelChanged(event);
	return junction;
    }


    /** places cloned junction of order order with index n to current graph, sets it at a certain position */
    public StrandJunction3D placeJunction(int order, int n, String rootName, String newName, Vector3D position,
			      String strandEnding) throws Object3DGraphControllerException {
	StrandJunction3D junction = (StrandJunction3D)(junctionController.getJunctionDB().getJunction(order, n));
	if (junction == null) {
	    throw new Object3DGraphControllerException("No junction of order " + (order) + " and id " 
						       + (n+1) + " is currently loaded.");
	}
	junction = (StrandJunction3D)(junction.cloneDeep());
	junction.setPosition(position);
	junction.setName(newName);
	if ((strandEnding != null) && (strandEnding.length() > 0)) {
	    // modify names of rna strands to avoid duplicate strand names:
	    Set<String> allowedNames = new HashSet<String>();
	    allowedNames.add("RnaStrand");
	    Set<String> forbiddenNames = new HashSet<String>();
	    Object3DTools.addEnding(junction, strandEnding, allowedNames, forbiddenNames);
	}
	// workspace.insertChild(junction);
	graph.addGraph(junction, rootName);
	// modelChanged(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED));
	ModelChangeEvent event = new ModelChangeEvent(this, eventConst.MODEL_MODIFIED);
	graph.fireModelChanged(event);
	links.fireModelChanged(event);
	return junction;
    }

    public Object3D placeBuildingBlock(DBElementDescriptor element, String rootName, String newName,
				       Vector3D position, String strandEnding) throws Object3DGraphControllerException {
	Object3D result;
	switch (element.getType()) {
	case DBElementDescriptor.JUNCTION_TYPE:
	    result = placeJunction(element.getOrder(), element.getId(), rootName, newName, position, strandEnding);
	    break;
	case DBElementDescriptor.KISSING_LOOP_TYPE:
	    result = placeKissingLoop(element.getOrder(), element.getId(), rootName, newName, position, strandEnding);
	    break;
	default:
	    throw new Object3DGraphControllerException("placeBuildingBlock: Database type not implemented: " + element);
	}
	result.setProperty("db_element", element.toString()); // store which database entry it came from
	return result;
    }

    /** return junction as described in properties */
    private StrandJunction3D extractAndPlaceJunction(Properties properties, String rootName, String newName, Vector3D position) 
      throws Object3DGraphControllerException {
	String junctionType = properties.getProperty("junction_type");
	assert junctionType != null;
	StrandJunction3D junction = null;
	int junctionId = Integer.parseInt(properties.getProperty("junction_id"));
	int junctionOrder = Integer.parseInt(properties.getProperty("junction_order"));
	String ending = "_fit";
	if (junctionType.equals("StrandJunction3D")) {
	    junction = placeJunction(junctionOrder, junctionId, rootName, newName, position, ending);
	}
	else if (junctionType.equals("KissingLoop3D")) {
	    junction = placeKissingLoop(junctionOrder, junctionId, rootName, newName, position, ending);
	    // junctionController.getKissingLoopDB().getJunction(junctionOrder, junctionId);
	}
	else {
	    assert false; // unknown name
	}
	return junction;
    }

    /** return length of first connecting helix. Minus one: converts from helix propagation matrix count
     * to number of interpolating base pairs
     */
    private int extractHelix1(Properties properties) {
	return Integer.parseInt(properties.getProperty("num_alt1")) - 1; // set in Matrix4DTools
    }

    /** return length of second connecting helix. Minus one: converts from helix propagation matrix count
     * to number of interpolating base pairs
     */
    private int extractHelix2(Properties properties) {
	return Integer.parseInt(properties.getProperty("num_alt2")) - 1; // set in Matrix4DTools
    }

    /** Initializes bridge controller with file name file */
    public void initBridgeController(String fileNameFile) {
	if ((fileNameFile != null) && (fileNameFile.length() > 0) && (new File(fileNameFile)).exists()) {
	    log.info("Initializing bridge controller with filename file: " + fileNameFile);
	    this.bridgeController = new BridgeItController(fileNameFile, junctionController);
	}
	else {
	    log.info("Could not initializize bridge controller with file name: " + fileNameFile);
	}
    }

    /** Initializes bridge controller with file name file */
    public void initBridgeController(List<String> fileNames) {
	this.bridgeController = new BridgeItController(fileNames, junctionController, nucleotideDB);
    }


    /** Initializes bridge controller without file name file */
    public void initBridgeController() throws Object3DGraphControllerException {
	List<String> fileNames = new ArrayList<String>();
	fileNames.add(nanotilerHome + SLASH + rb.getString("bridgedb"));
	initBridgeController(fileNames);
	if (bridgeController == null) {
	    throw new Object3DGraphControllerException("Bridge database not initialized. Call bridgeinit");
	}
	else {
	    log.info("Bridge controller succesfully initialized.");
	}
    }
		
		public MotifBlockController initMotifBlockController() throws IOException{//default
			return initMotifBlockController( nanotilerHome + SLASH + "resources" + SLASH + "predict3dmotifs"); //TODO put in resource bundle
		}
		public MotifBlockController initMotifBlockController(String datahome) throws IOException{
			return new MotifBlockController( datahome); //TODO put in resource bundle
		}
		
		//mode for fastmode, true is faster, excludes bulge motifs
		public MotifBlockController initMotifBlockController(String datahome, boolean mode) throws IOException{
			return new MotifBlockController(datahome, mode); //TODO put in resource bundle
		}
		
		public TertiaryMotifBlockController initTertiaryMotifBlockController() throws IOException{//default
			return initTertiaryMotifBlockController( nanotilerHome + SLASH + "resources" + SLASH + "predict3dmotifs"); //TODO put in resource bundle
		}
		public TertiaryMotifBlockController initTertiaryMotifBlockController(String datahome) throws IOException{
			return new TertiaryMotifBlockController(datahome); //TODO put in resource bundle
		}

    /** places junction described in properties object into scaffold defined by b1 and b2 */
    private void placeFittedJunction(BranchDescriptor3D b1, BranchDescriptor3D b2,
				     Properties properties, double rms, String rootName) throws FittingException,
												Object3DGraphControllerException {
	int fuseStrands = HelixOptimizerTools.NO_FUSE;
	boolean firstFixedMode = true;
	Vector3D position = Vector3D.average(b1.getPosition(), b2.getPosition());
	String newName = b1.getName() + "_" + b2.getName();
	StrandJunction3D junction = extractAndPlaceJunction(properties, rootName, newName, position);
	assert junction != null;
	int n1 = extractHelix1(properties);
	int n2 = extractHelix2(properties);	
	boolean forwardMode = properties.getProperty("junction_direction").equals("forward");
	String name2 = junction.getBranch(0).getFullName();
	String name3 = junction.getBranch(1).getFullName();
	if (!forwardMode) {
	    name2 = junction.getBranch(1).getFullName();
	    name3 = junction.getBranch(0).getFullName();
	}
	log.info("starting addHelixConstraint (1)" + b1.getFullName() + " "+ name2 + " " + n1 + " " + rms);
	assert getGraph().findByFullName(b1.getFullName()) != null;
	assert getGraph().findByFullName(b2.getFullName()) != null;
	assert getGraph().findByFullName(name2) != null;
	assert getGraph().findByFullName(name3) != null;
	addHelixConstraint(b1.getFullName(), name2, n1, n1, rms, 0, 0, null); // null name
	log.info("starting addHelixConstraint (2)" + b2.getFullName() + " "+ name3 + " " + n2 + " " + rms);
	addHelixConstraint(b2.getFullName(), name3, n2, n2, rms, 0, 0, null);
	FitParameters stemFitParameters = new FitParameters(rms, Math.toRadians(30));
	Object3DSet movable = new SimpleObject3DSet();
	movable.add(junction.getBranch(0));
	movable.add(junction.getBranch(1));
	String helixRootName = "root";
	log.info("Starting to optimize helices!");
	Object3DSet parentObjects = null; // use defaults
	Properties prop = optimizeHelices(50000, 10.0, 1e10, 20.0, stemFitParameters, helixRootName, movable, parentObjects, BranchDescriptorOptimizerFactory.MORPH_OPTIMIZER, fuseStrands, firstFixedMode, 0); // premade optimization
	Vector3D testPosition = Vector3D.average(b1.getPosition(), b2.getPosition());
	assert position.distance(testPosition) < 0.1; // no change of scaffold
	double score = Double.parseDouble(prop.getProperty(Optimizer.FINAL_SCORE));
	if (score > rms) {
	    log.warning("Too bad junction fit ( " + score + " instad of limit " + rms + " ), removing junction again:");
	    getGraph().remove(junction);
	}
	log.info("Finished placeFittedJunction: " + prop);
    }

    /** ranks 2D junctions for interpolating loops
     * @param placeRms : rms limit for possible placement, smaller zero if no junction should be placed */
    public List<Properties> rankJunctionFits(BranchDescriptor3D b1, BranchDescriptor3D b2,
					     int n1Min, int n1Max, int n2Min, int n2Max, double placeRms,
					     String rootName) 
	throws FittingException, Object3DGraphControllerException {
	List<Properties> result = junctionController.rankJunctionFits(b1, b2, n1Min, n1Max,
								      n2Min, n2Max);
	if (placeRms >= 0) {
	    Vector3D testPos1 = b1.getPosition();
	    Vector3D testPos2 = b2.getPosition();
	    placeFittedJunction(b1, b2, result.get(0), placeRms, rootName);
	    assert (testPos1.distance(b1.getPosition()) < 0.1); // no moving of scaffold
	    assert (testPos2.distance(b2.getPosition()) < 0.1); // no moving of scaffold
	    refresh(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED));
	}
	return result;
    }

    /** ranks 2D junctions for interpolating loops
     * @param placeRms : rms limit for possible placement, smaller zero if no junction should be placed */
    public List<Properties> rankKissingLoopFits(BranchDescriptor3D b1, BranchDescriptor3D b2,
						int n1Min, int n1Max, int n2Min, int n2Max, double placeRms,
						String rootName) 
	throws FittingException, Object3DGraphControllerException {
	List<Properties> result = junctionController.rankKissingLoopFits(b1, b2, n1Min, n1Max,
									 n2Min, n2Max);
	if (placeRms >= 0) {
	    Vector3D testPos1 = b1.getPosition();
	    Vector3D testPos2 = b2.getPosition();
	    placeFittedJunction(b1, b2, result.get(0), placeRms, rootName);
	    assert (testPos1.distance(b1.getPosition()) < 0.1); // no moving of scaffold
	    assert (testPos2.distance(b2.getPosition()) < 0.1); // no moving of scaffold
	    refresh(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED));
	}
	return result;
    }

    /** generate links between atoms withing 1.5 angstroms of each other */
     public void generateAtomLinks(Object3D root, double cutoff) {
 	Object3DSet atoms = Object3DTools.collectByClassName(root, "Atom3D");
 	for (int i = 0; i < atoms.size(); ++i) {
 	    for (int j = i+1; j < atoms.size(); ++j) {	
 		double dist = atoms.get(i).distance(atoms.get(j));
 		if (dist < cutoff) {
 		    try{
 		         generateLink(atoms.get(i).toString(),atoms.get(j).toString(),"null");
 		    } catch(Object3DGraphControllerException e){System.out.println("Command error");}

 		}
 	    }
 	}
     }
     public double getAtomBondCutoff(){
 	return atomBondCutoff;
     }
    
    /** Returns minimum length of stems to be recognized when importing file */
    public int getImportStemLengthMin() {
	return this.importStemLengthMin;
    }

    /** Prints an info string for a junction */
    private void printDBElementInfo(PrintStream ps, DBElementDescriptor dbe) {
	assert dbe != null && ps != null;
	StrandJunction3D junction = null;
	if (dbe.getType() == DBElementDescriptor.JUNCTION_TYPE) {
	    junction = junctionController.getJunctionDB().getJunction(dbe.getOrder(), dbe.getId());
	}
	else if (dbe.getType() == DBElementDescriptor.KISSING_LOOP_TYPE) {
	    junction = junctionController.getKissingLoopDB().getJunction(dbe.getOrder(), dbe.getId());
	}
	else if (dbe.getType() == DBElementDescriptor.MOTIF_TYPE) {
	    junction = junctionController.getMotifDB().getJunction(dbe.getOrder(), dbe.getId());
	}
	else {
	    assert false; // unsupported junction type
	}
	if (junction != null) {
	    String propString = "";
	    if (junction.getProperties() != null) {
		propString += junction.getProperties();
	    }
	    else {
		propString = "undefined";
	    }
	    ps.print("" + dbe + ";properties:" + propString);
	}
	else {
	    ps.print(" junction_not_found_error");
	}
    }

    /** Prints a connectivity info string, showing the properties of each building block */
    private void printConnectivityInfo(PrintStream ps, GrowConnectivity connectivity) {
	List<DBElementDescriptor> buildingBlocks = connectivity.getBuildingBlocks();
	ps.print("" + buildingBlocks.size() + ";");
	for (int i = 0; i < buildingBlocks.size(); ++i) {
	    DBElementDescriptor dbe = buildingBlocks.get(i);
	    ps.print("bb:" + (i+1) + ":");
	    printDBElementInfo(ps, dbe);
	    ps.print(";");
	}
    }

    /** given a list of building blocks and a list of connections, grow until collisions are unavoidable */
    public Properties growBuildingBlocks(GrowConnectivity connectivity,
					 String rootName,
					 String nameBase,
					 Vector3D startPosition,
					 boolean addStemsFlag,
					 double stemRmsLimit,
					 boolean avoidCollisionsMode,
					 double ringClosureExportLimit, 
					 String ringClosureExportFileNameBase,
					 boolean randomMode) throws Object3DGraphControllerException {
	System.out.print("used_blocks_info:");
	printConnectivityInfo(System.out, connectivity);
	System.out.println();
	BuildingBlockGrower grower = new BuildingBlockGrower(junctionController.getJunctionDB(),
							     junctionController.getKissingLoopDB(),
							     junctionController.getMotifDB(),
							     nucleotideDB);
	grower.addStemsFlag = addStemsFlag;
	grower.avoidCollisionsMode = avoidCollisionsMode;
	grower.setRingClosureExportLimit(ringClosureExportLimit);
	grower.setStemRmsLimit(stemRmsLimit);
	grower.setRandomMode(randomMode);
	Properties properties = null;
	try {
	    properties = grower.growBuildingBlocks(connectivity, // blockList, connections,
						   rootName, nameBase,
						   startPosition,
						   ringClosureExportFileNameBase);
	}
	catch (FittingException fe) {
	    throw new Object3DGraphControllerException(fe.getMessage());
	}
	catch (IOException ioe) {
	    throw new Object3DGraphControllerException(ioe.getMessage());
	}
	addBundle(grower.getBundle()); // hopefully triggers refresh / listener mechanisms
	return properties;
    }	

    /** given a list of building blocks and a list of connections, grow until collisions are unavoidable */
    public Properties growBuildingBlocksCombinations(GrowConnectivity connectivityTemplate,
						     String rootName,
						     String nameBase,
						     Vector3D startPosition,
						     boolean addStemsFlag,
						     double stemRmsLimit,
						     boolean avoidCollisionsMode,
						     double ringClosureExportLimit, 
						     String ringClosureExportFileNameBase,
						     int helixLengthMax) throws Object3DGraphControllerException {
	if (connectivityTemplate.getBuildingBlocks() == null || connectivityTemplate.getBuildingBlocks().size() == 0) {
	    List<DBElementDescriptor> allDescriptors = junctionController.retrieveAllDescriptors();
	    log.info("Using all " + allDescriptors.size() + " loaded junctions and motifs as building blocks.");
	    connectivityTemplate.setBuildingBlocks(allDescriptors);
	}
	assert(connectivityTemplate.getBuildingBlocks() != null);
	assert(connectivityTemplate.getBuildingBlocks().size() > 0);
	GrowConnectivityCombinatorialFactory connFactory = new GrowConnectivityCombinatorialFactory(connectivityTemplate, helixLengthMax);
	// List<GrowConnectivity> connectivities = connFactory.generate();
	Properties finalResult = new Properties();
	boolean randomMode = false;
	while (connFactory.hasNext()) {
	    try {
		GrowConnectivity connectivity = connFactory.next();
		String prefix = connectivity.toStringConnections();
		if (prefix.startsWith("connect=")) {
		    prefix = prefix.substring(8);
		}
		prefix = "%" + prefix;
		prefix = prefix.replaceAll("\\(hxend\\)", "");
		prefix = prefix.replaceAll("\\;", ".");
		String ringExportName = ringClosureExportFileNameBase + prefix;
		log.info("Starting grow building blocks for connectivity " + connectivity.toString());
		log.info("Filename base for potential ring structures: " + ringExportName);
		log.info("Clearing workspace");
		clear();
		Properties result = growBuildingBlocks(connectivity, rootName, nameBase, startPosition, addStemsFlag,
						       stemRmsLimit, avoidCollisionsMode, ringClosureExportLimit, ringExportName, randomMode);
		PropertyTools.mergeProperties(finalResult, result, prefix); // add to final result
	    } catch (FittingException fe) {
		log.info("# Ignoring connectivity: " + fe.getMessage());
	    }
	}
	return finalResult;
    }

    /** given a list of building blocks and a list of connections, grow until collisions are unavoidable */
    public Properties growRandomBuildingBlocks(GrowConnectivity connectivity, // not needed
					       String rootName,
					       String nameBase,
					       Vector3D startPosition,
					       boolean addStemsFlag,
					       double stemRmsLimit,
					       boolean avoidCollisionsMode,
					       double ringClosureExportLimit, 
					       String ringClosureExportFileNameBase,
					       boolean randomMode) throws Object3DGraphControllerException {
	System.out.print("used_blocks_info:");
	printConnectivityInfo(System.out, connectivity);
	System.out.println();
	BuildingBlockRandomGrower grower = new BuildingBlockRandomGrower(junctionController.getJunctionDB(),
  							                 junctionController.getKissingLoopDB(),
							                 junctionController.getMotifDB(),
							                 nucleotideDB);
	grower.addStemsFlag = addStemsFlag;
	grower.avoidCollisionsMode = avoidCollisionsMode;
	grower.setRingClosureExportLimit(ringClosureExportLimit);
	grower.setStemRmsLimit(stemRmsLimit);
	grower.setRandomMode(randomMode);
	Properties properties = null;
	try {
	    properties = grower.growBuildingBlocks(connectivity, // blockList, connections,
	     					   rootName, nameBase,
	     					   startPosition,
	     					   ringClosureExportFileNameBase);
	}
	catch (FittingException fe) {
	    throw new Object3DGraphControllerException(fe.getMessage());
	}
	catch (IOException ioe) {
	    throw new Object3DGraphControllerException(ioe.getMessage());
	}
	addBundle(grower.getBundle()); // hopefully triggers refresh / listener mechanisms
	return properties;
    }	

    /** Central method for launching building block grow framework. Give a set of building blocks, connections,
     * and a graph structure, it loops through all compatible possibilities.*/
    public Properties growBuildingBlocksFramework(Object3DLinkSetBundle bundle, int generationCount, List<DBElementDescriptor> buildingBlockDescriptors,
						  List<List<Integer> > buildingBlockIndices,
						  int maxBlocks,
						  int maxConnections,
						  int helixVariation,
						  String nameBase,
						  boolean buildMode,
						  double ringClosureExportLimit) throws Object3DGraphControllerException {
	assert bundle != null && buildingBlockDescriptors != null && buildingBlockIndices != null && buildingBlockDescriptors.size() > 0;
	ConnectivityGenerator genConn = new SimpleConnectivityGenerator(bundle.getObject3D(), bundle.getLinks(),  buildingBlockDescriptors,
									buildingBlockIndices, generationCount);
	genConn.setBuildingBlockCountMax(maxBlocks);
	genConn.setConnectionCountMax(maxConnections);
	genConn.setHelixLengthVariation(helixVariation);
	GrowFramework growFramework = new GrowFramework(genConn, junctionController.getJunctionDB(), junctionController.getKissingLoopDB(), junctionController.getMotifDB(),
							nucleotideDB, nameBase);
	growFramework.setBuildMode(buildMode); // if false, only loop through iterations, do not build structures
	growFramework.setRingClosureExportLimit(ringClosureExportLimit);
	assert genConn.getConnectionCountMax() == maxConnections;
	if (nameBase != null) {
	    growFramework.setNameBase(nameBase);
	}
	if ((bundle != null) && (bundle.getObject3D() != null)
	    && (bundle.getObject3D().size() > 0)) {
	    // set target topology:
	    try {
		SignatureTranslatorCanonizer canonizer = new SignatureTranslatorCanonizer();
		String topology = canonizer.generateCanonizedRepresentation(bundle);
		log.info("Setting target topology to: " + topology);
		genConn.setTopology(topology);
	    }
	    catch (AlgorithmFailureException afe) {
		log.warning("Exception whlie generating signature of graph: "
			    + afe.getMessage());
	    }
	}
	log.info("Starting grow framework ...");
	growFramework.run();
	log.info("Finished grow framework.");
	if (growFramework.getException() != null) {
	    throw new Object3DGraphControllerException("Exception in growBuildingBlocksFramework: " + growFramework.getException().getMessage());
	}
	BuildingBlockGrower bestGrower = growFramework.getBestGrower();
	if (bestGrower != null) { // actually adds generated structure
	    Object3D genStruct = bestGrower.getRoot(); // one of the several generated structures
	    assert genStruct != null;
// 	    if (nameBase != null) {
// 		genStruct.setName(nameBase);
// 	    }
	    getGraph().addGraph(genStruct, getGraph().getGraph().getFullName()); // put one below root node
	    refresh(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED));
	}
	return growFramework.getProperties(); // return results;
    }

    /** Generates for each vertex a list of indices (with respect to buildingBlockDescriptors) that is compatible with that vertex */
    private List<Integer> generateBuildingBlockIndices(Object3D obj, LinkSet links, 
						       List<DBElementDescriptor> buildingBlockDescriptors) {
	List<Integer> result =new ArrayList<Integer>();
	for (int i = 0; i < buildingBlockDescriptors.size(); ++i) {
	    result.add(new Integer(i)); // TODO : simple implementation: all building blocks are allowed
	}
	return result;
    }

    /** Generates for each vertex a list of indices (with respect to buildingBlockDescriptors) that is compatible with that vertex */
    private List<List<Integer> > generateAllBuildingBlockIndices(Object3D root, LinkSet links,
								 List<DBElementDescriptor> buildingBlockDescriptors) {
	List<List<Integer> > result = new ArrayList<List<Integer> >();
	for (int i = 0; i < root.size(); ++i) {
	    result.add(generateBuildingBlockIndices(root.getChild(i), links, buildingBlockDescriptors));
	}
	log.warning("Using simplified implementation of generateBuildingBlockIndices");
	return result;
    }

    /** Returns object tree containing residues used for single-base mutations */
    public Object3D getNucleotideDB() { return this.nucleotideDB; }

    /** Generates all possible building block descriptors for a specific building block data base */
    private void addAllBuildingBlockDescriptors(List<DBElementDescriptor> elements,
						StrandJunctionDB junctionDB,
						int junctionType) {
	assert elements != null;
	int count = 0;
	for (int i = 0; i < junctionDB.size(); ++i) {
	    for (int j = 0; j < junctionDB.size(i); ++j) {
		DBElementDescriptor dbe = new DBElementDescriptor(i, j, junctionType, elements.size());
		elements.add(dbe);
		++count;
	    }
	}
	assert count == junctionDB.getJunctionCount();
    }

    /** Removes bases that are at the tails of the strand and that are not involved in base pairing */
    public void trimUnpaired(String rnastrandFullName) throws Object3DGraphControllerException {
	log.info("Called trim command for name: " + rnastrandFullName);
	Object3D obj = graph.findByFullName(rnastrandFullName);
	if ((obj == null) || (!(obj instanceof RnaStrand))) {
	    throw new Object3DGraphControllerException("Not a valid RNA strand: " + obj.getFullName());
	}
	RnaStrandTools.trimUnpaired((RnaStrand)obj, links);
	refresh(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED));
    }
    
    public void removeAllUnpaired() throws DuplicateNameException,Object3DGraphControllerException {
	SecondaryStructure structure = generateSecondaryStructure();
	InteractionSet interactions = structure.getInteractions();
	HashSet<Residue> paired = new HashSet<Residue>(); //holds all residues that are paired
	for (int i=0; i<interactions.size(); i++) {
		Interaction inter = interactions.get(i);
		paired.add( inter.getResidue1() );
		paired.add( inter.getResidue2() );
	}
	ArrayList<String> removeNames = new ArrayList<String>();
	for(int i=0; i<structure.getSequenceCount(); i++){
		Sequence seq = structure.getSequence(i);
		for(int j=0; j<seq.size(); j++){
			Residue res = seq.getResidue(j);
			if(!paired.contains(res)){
				String treeName = ("1.1." + (i+1) + "." + (j+1));
				String aliasName = (i + "_" + j);
				makeAlias( treeName, aliasName );
				removeNames.add(aliasName);
			}
		}
	}
	for(String i: removeNames){
		graph.remove("#" + i);
	}
	log.info("Removed unpaired residues");
	}

    public void remove(String treeName) throws Object3DGraphControllerException {
	Object3D obj = getGraph().findByFullName(treeName);
	remove(obj);
    }

    public void remove(Object3D tree) {
	log.info("Starting remove");
	HashSet<Object3D> removable = Object3DTools.convertTreeToHash(tree);
	graph.remove(tree);
	refreshLinks();
	refresh(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED));
	int n = links.size();
	for (int i = 0; i < n; ++i) {
	    Link link = links.get(i);
	    assert(!removable.contains(link.getObj1())); // check if links do not contain stale references
	    assert(!removable.contains(link.getObj2()));
	}
	
	assert(false);
	log.info("finished remove");
    }

	
	/**If thisSet is true, deletes all residues from list. If thisSet is false, deletes all residues except those on the list. If split, splits strand when it deletes */
	public void removeResidues(List<Residue> del, boolean thisSet, boolean split) throws DuplicateNameException,Object3DGraphControllerException {
SecondaryStructure structure = generateSecondaryStructure();
ArrayList<String> removeNames = new ArrayList<String>();
for(int i=0; i<structure.getSequenceCount(); i++){
	Sequence seq = structure.getSequence(i);
	for(int j=0; j<seq.size(); j++){
		Residue res = seq.getResidue(j);
		if(thisSet){
			if(del.contains(res)){
				String treeName = ("1.1." + (i+1) + "." + (j+1));
				String aliasName = (i + "_" + j);
				makeAlias( treeName, aliasName );
				removeNames.add(aliasName);
			}
		} else{
			if(!del.contains(res)){
				String treeName = ("1.1." + (i+1) + "." + (j+1));
				String aliasName = (i + "_" + j);
				makeAlias( treeName, aliasName );
				removeNames.add(aliasName);
			}
		}
		
	}
}
for(String i: removeNames){
	String name = ("#" + i);
	if(split){
		Nucleotide3D obj = (Nucleotide3D)graph.findByFullName(name);
		if(obj.getPos() != 0 && obj.getPos() != ((NucleotideStrand)obj.getParentObject()).size()-1 ){
			graph.splitStrand( ((NucleotideStrand)obj.getParentObject()).getFullName(), obj.getPos() );
		}
	}
	graph.remove(name);
}
}

    /** Finds RNA strands, that are purelely involved in a double-helix with another strand */
    public Object3DSet findPureHelicalStrands() {
	return RnaStrandTools.findPureHelicalStrands(getGraph().getGraph(), getLinks());
    }

    /** Generates all possible building block descriptors */
    private List<DBElementDescriptor> generateAllBuildingBlockDescriptors() {
	List<DBElementDescriptor> elements = new ArrayList<DBElementDescriptor>();
	StrandJunctionDB junctionDB = junctionController.getJunctionDB();
	StrandJunctionDB kissingLoopDB = junctionController.getKissingLoopDB();
	addAllBuildingBlockDescriptors(elements, junctionDB, DBElementDescriptor.JUNCTION_TYPE);
	addAllBuildingBlockDescriptors(elements, kissingLoopDB, DBElementDescriptor.KISSING_LOOP_TYPE);
	assert elements.size() == junctionDB.getJunctionCount() + kissingLoopDB.getJunctionCount();
	return elements;
    }

    public Properties growBuildingBlocksFramework(String rootName, String nameBase, int generationCount,
						  List<DBElementDescriptor> buildingBlockDescriptors,
						  int maxBlocks,
						  int maxConnections,
						  int helixVariation,
						  boolean buildMode,
						  double ringClosureLimit) throws Object3DGraphControllerException {
	Object3D obj = getGraph().findByFullName(rootName);
	if (obj == null) {
	    throw new Object3DGraphControllerException("Could not find object with name: " + rootName);
	}
	LinkSet newLinks = new SimpleLinkSet();
	newLinks.merge(getLinks());
	newLinks.removeBadLinks(obj);
	Object3DLinkSetBundle bundle = new SimpleObject3DLinkSetBundle(obj, newLinks);
	if ((buildingBlockDescriptors == null) || (buildingBlockDescriptors.size() == 0)) {
	    log.info("No specific building blocks defined. Using all loaded building blocks for graph growing.");
	    buildingBlockDescriptors = generateAllBuildingBlockDescriptors();
	}
	assert buildingBlockDescriptors != null;
	if (buildingBlockDescriptors.size() == 0) {
	    throw new Object3DGraphControllerException("No building blocks defined for self-assembly simulation! Consider commands loadjunctions and option blocks in command growgraph");
	}
	List<List<Integer> > indices = generateAllBuildingBlockIndices(obj, newLinks, buildingBlockDescriptors);
	return growBuildingBlocksFramework(bundle, generationCount, buildingBlockDescriptors, indices, maxBlocks, maxConnections, helixVariation, nameBase, buildMode,
					   ringClosureLimit);
    }

    /* TODO */
    public void read(InputStream is) throws Object3DIOException {
	DataInputStream dis = new DataInputStream(is);
	Object3DFactory reader = new GeneralRnaReader();
	links.clear();
	Object3D tmpGraph = reader.readAnyObject3D(dis);
	log.finest("read tree data:" + NEWLINE + tmpGraph.toString() +
		   NEWLINE);
	links.read(dis, tmpGraph); // read linkset also
	log.finest("read LinkSet:" + NEWLINE + links.toString() + NEWLINE);
	// TODO Object3DLinkNameFixer.fixAll(tmpGraph);
	graph.setGraph(tmpGraph);
    }
    
    /**
     * reads data from input stream, adds to current graph
     * TODO read links
     */
    public void readAndAdd(InputStream is) throws Object3DIOException {
	log.info("starting Object3DGraphController.readAndAdd(InputStream is)!");
	Object3DFactory reader = new GeneralRnaReader();
	Object3D tmpGraph = reader.readAnyObject3D(is);
	// Object3DLinkNameFixer.fixAll(tmpGraph);
	log.finest("read data:" + NEWLINE + tmpGraph.toString() + NEWLINE);
	graph.addGraph(tmpGraph);
	this.generateAtomLinks(graph.getGraph(tmpGraph.toString()),atomBondCutoff);
	log.info("Finished Object3DGraphController.readAndAdd(InputStream is)!");
    }

    /**
     * reads vector data from input stream (various formats possible),
     * adds to current graph, 
     * adds stems
     */
    public void readAndAdd(InputStream is,
			   String nameOrig,
			   Vector3D scaleVector,
			   Vector3D offset,
			   int formatId,
			   boolean addStemsFlag,
			   char sequenceChar,
			   boolean onlyFirstPathMode,
			   boolean findStemsFlag,
			   boolean addJunctionsFlag)
	throws Object3DIOException {
	log.info("starting Object3DGraphController.readAndAdd!");
	Object3DFactory reader;
	log.info("starting switch command with formatId: " + formatId);
	switch (formatId) {
	case KNOTPLOT_FORMAT:
	    reader = new KnotPlotReader();
	    log.fine("reading KnotPlot format!");
	    break;
	case PDB_FORMAT:
	    // reader = new RnaPdbReader();
	    reader = new RnaPdbRnaviewReader();
	    ((RnaPdbRnaviewReader)reader).setAddRnaviewMode(false);
	    log.fine("reading RNA in PDB format without RNAview information!");
	    break;
	    // case PDB_DNA_FORMAT:
	    // // reader = new PdbDnaReader();
	    // log.severe("reading DNA PDB format not supported!");
	    // throw new Object3DIOException("Unknown input format id!");
	    // case PDB_PROTEIN_FORMAT:
	    // reader = new ProteinPdbReader();
	    // log.info("reading protein PDB format!");
	    // break;
	case POINTSET_FORMAT:
	    reader = new PointSetReader();
	    log.fine("reading point set format!");
	    break;
 	case POINTSET2_FORMAT:
	    reader = new PointSetReader3();
 	    log.fine("reading point set 2 format!");
 	    break;
	case PDB_RNAVIEW_FORMAT: 
	    reader = new RnaPdbRnaviewReader();
	    ((RnaPdbRnaviewReader)reader).setResidueRenumberMode(false);
	    log.fine("reading RNA PDB / RNAVIEW format!");
	    break;
	case PDB_RNAVIEW_RENUMBER_FORMAT: reader = new RnaPdbRnaviewReader();
	    log.info("reading RNA PDB format with added RNAView information; Also renumbering residues!");
	    ((RnaPdbRnaviewReader)reader).setResidueRenumberMode(true);
	    assert false;
	    break;
	default: 
	    assert false; 
	    throw new Object3DIOException("Unknown input format id! DNA PDB and Protein PDB not supported.");
	}
	log.fine("successfully chosen reader!");
	String name = nameOrig + COVERING_ENDING;
	if (formatId != POINTSET2_FORMAT) {
	    Object3DLinkSetBundle bundle = reader.readBundle(is); //remove
	    //	Object3D tmpGraph = reader.readAnyObject3D(is);
	    Object3D tmpGraph = bundle.getObject3D(); //remove
	    log.fine("Object3DGraphController.readAndAdd: 3D object with size " + tmpGraph.size() + " read! " + tmpGraph.getPosition());
	    tmpGraph.setName(nameOrig);
	    // Vector3D scaleVector = new Vector3D(scale, scale, scale);
	    Vector3D one = new Vector3D(1.0, 1.0, 1.0);
	    if (! one.equals(scaleVector) ) {
		log.info("Scaling objects with factors: " + scaleVector.toString());
		tmpGraph.scale(scaleVector);
		// assert PointSetReader.testDebugPoint(); //Eckart
	    }
// 	    log.fine("First position of read graph: " +
// 		       Object3DTools.findFirstLeaf(tmpGraph));
	    log.fine("adding graph to graph controller...");
	    // assert PointSetReader.testDebugPoint(); //Eckart
	    if (!graph.addGraph(tmpGraph)) {
		throw new Object3DIOException("Object with name " + tmpGraph.getName() + " already exists!");
	    }
	    if (offset.length() > 0.0) {
		tmpGraph.translate(offset);
	    }
	    links.addLinks(bundle.getLinks());
	    if (findStemsFlag) {
		log.fine("findStemsFlag");
		Object3DLinkSetBundle stemBundle = null;
		if (formatId != PDB_RNAVIEW_FORMAT) {
		    stemBundle = StemTools.generateStems(tmpGraph);
		}
		else { // special case if PDB file is augmented with RNAVIEW info
		    stemBundle = StemTools.generateStemsFromLinks(tmpGraph, bundle.getLinks(), importStemLengthMin);
		}
		Object3D stemRoot = stemBundle.getObject3D();
		if (stemRoot.size() > 0) {
		    Object3DSet stemBranchDescriptors = BranchDescriptorTools.generateInvertedBranchDescriptors(new SimpleObject3DSet(stemBundle.getObject3D()), 0, stemFitRmsTolerance, 0);
		    log.info("Generated " + stemBranchDescriptors.size() + " branch descriptors for stems.");
		    // assert graph.findByFullName(tmpGraph.getFullName()) != null;
		    try {
			graph.addGraph(stemRoot, tmpGraph.getFullName());
		    }
		    catch (Object3DGraphControllerException e) {
			log.severe(e.getMessage()); // should never happen
			assert false;
		    }
		    links.addLinks(stemBundle.getLinks());
		    if (addJunctionsFlag) {
			log.info("Adding junctions using " + stemRoot.size() + " stems");
			String junctionName = name + JUNCTION_ENDING;
			String kissingLoopName = name + KISSING_LOOP_ENDING;
			// log.fine("First position of read graph before starting generateJunctions: " + Object3DTools.findFirstLeaf(tmpGraph));
			log.fine("Using generated stems for junction generation: ");
			for (int i = 0; i < stemRoot.size(); ++i) {
			    log.fine("" + ((RnaStem3D)(stemRoot.getChild(i))).getStemInfo());
			}
			// Vector3D testPos = Object3DTools.findFirstLeaf(tmpGraph).getPosition();
			Object3D junctions = BranchDescriptorTools.generateJunctions(stemRoot, junctionName,
							     junctionController.getParameters().branchDescriptorOffset,
							     corridorDescriptor, stemFitRmsTolerance,
							     junctionController.getParameters().loopLengthSumMax); 
			Object3D kissingLoops = BranchDescriptorTools.generateKissingLoops(stemRoot, kissingLoopName, 
							     junctionController.getParameters().branchDescriptorOffset,
											   stemFitRmsTolerance); 
			// Object3D junctions = BranchDescriptorTools.generateJunctions(stemRoot, junctionName, tmpGraph, 
			//                              branchDescriptorOffset);
			assert junctions != null;
			assert kissingLoops != null;
			log.info("Number of found junctions in imported object: " + junctions.size());
			log.info("Number of found kissing loops in imported object: " + kissingLoops.size());
			try {
			    if (junctions.size() > 0) { // only add if junctions were found
				graph.addGraph(junctions, tmpGraph.getFullName());
			    }
			    if (kissingLoops.size() > 0) { // only add if kissing loops were found
				graph.addGraph(kissingLoops, tmpGraph.getFullName());
			    }
			}
			catch (Object3DGraphControllerException e) {
			    log.severe(e.getMessage()); // should never happen
			    assert false;
			}
			// log.finest("First position of read graph after running generateJunctions: " + Object3DTools.findFirstLeaf(tmpGraph));
			// assert testPos.distance(Object3DTools.findFirstLeaf(tmpGraph).getPosition()) < 0.001; // test for side effects
			// 		    Object3DTools.printTree(System.out, tmpGraph);
		    }
		}
	    }
	    // 	Object3DTools.printTree(System.out, tmpGraph);
	    // log.fine("First position of read graph before starting FragmentGridTiler: " + Object3DTools.findFirstLeaf(tmpGraph));
	    if (addStemsFlag) {
		log.fine("addStemsFlag");
		// GridTiler tiler = new SimpleGridTiler();
		// TODO allow different tilers!
		updateFragmentGridTiler();
		assert fgTiler.getStrandJunctionDB() != null;
		// GridTiler tiler = fgTiler; // new FragmentGridTiler(junctionController.getJunctionDB(),
		// fgTiler.setStrandJunctionDB(junctionController.getJunctionDB());
		// fgTiler.setKissingLoopDB(junctionController.getKissingLoopDB());
		// junctionController.getKissingLoopDB());
		// ((FragmentGridTiler)(tiler)).setNucleotideDB(nucleotideDB); // sets nucleotide database (may be null)
		Object3DLinkSetBundle stemBundle = fgTiler.generateTiling(tmpGraph, bundle.getLinks(), 
									  name, sequenceChar, onlyFirstPathMode );
		if( stemBundle != null ) {
		    try {
			graph.addGraph(stemBundle.getObject3D(), tmpGraph.getFullName()); //TODO: add links while adding object3D
		    }
		    catch (Object3DGraphControllerException e) {
			log.severe(e.getMessage()); // should never happen
			assert false;
		    }

		    links.addLinks(stemBundle.getLinks());
		    // check if links were generated properly
		    if (stemBundle.getObject3D().size() > 0) {
			assert stemBundle.getLinks().size() > 0;
		    }
		}
	    }
	    else {
		log.fine("No placement of graph-based junctions or stems because addStemFlag was set to false!");
	    }
	    // 	Object3DTools.printTree(System.out, tmpGraph);
	    // log.fine("First position of read graph before finished readAndAdd: " + Object3DTools.findFirstLeaf(tmpGraph));
	    log.fine("readAndAdd finished!");
	}
	else if (reader instanceof PointSetReader3) {
	    log.info("Reader point set file with PointSetReader3!");
	    RotationInfo rotation = ((PointSetReader3)reader).readPoints2(is);
	    Object3D root = rotation.getRoot();
	    LinkSet linkSet = rotation.getLinkSet();
	    // int[] placeArray = rotation.getPlaceArray();
	    // ArrayList rotateList = rotation.getRotateList();
	    log.info("3D object with size " + root.size() + " read! " + root.getPosition());
	    root.setName(name);
	    Vector3D one = new Vector3D(1.0, 1.0, 1.0);
	    if (!one.equals(scaleVector)) {
		log.info("Scaling objects with factors: " + scaleVector);
		root.scale(scaleVector);
	    }
	    // log.fine("First position of read graph: " + Object3DTools.findFirstLeaf(root));
	    assert offset.length() == 0.0; // cannot handle offset currently. TODO
	    graph.addGraph(root); //TODO: add links while adding object3d
	    links.addLinks(linkSet);
	    if (findStemsFlag) {
		Object3DLinkSetBundle stemBundle = null;
		if (formatId != PDB_RNAVIEW_FORMAT) {
		    log.severe("PDB file should be augmented with RNAVIEW info!"); //TODO: FAQ
		    stemBundle = StemTools.generateStems(root);
		}
		else { // special case if PDB file is augmented with RNAVIEW info
		    stemBundle = StemTools.generateStemsFromLinks(root, linkSet, importStemLengthMin); // PDB_IMPORT_STEM_LENGTH_MIN);
		}
		Object3D stemRoot = stemBundle.getObject3D();
		if (stemRoot.size() > 0) {
		    graph.addGraph(stemRoot);
		    links.addLinks(stemBundle.getLinks());
		    if (addJunctionsFlag) {
			log.fine("Adding junctions!");
// <<<<<<< Object3DGraphController.java
// 			String junctionName = name + JUNCTION_ENDING;
// 			// 		    Object3DTools.printTree(System.out, tmpGraph);
// 			log.info("First position of read graph before starting generateJunctions: " + Object3DTools.findFirstLeaf(tmpGraph));
// 			Vector3D testPos = Object3DTools.findFirstLeaf(tmpGraph).getPosition();
// 			int[] junctionsToPlace = (int[])points2List.get(INT_ARRAY_INDEX);
// 			List rotateList = (ArrayList)points2List.get(ARRAYLIST_INDEX);
// 			Object3D junctions = BranchDescriptorTools.generateJunctions(stemRoot, junctionName, tmpGraph, 0,
// 					       corridorDescriptor, stemFitRmsTolerance, junctionController.getParameters().loopLengthSumMax);
// =======
			String junctionName = name + JUNCTION_ENDING;
			// log.fine("First position of read graph before starting generateJunctions: " + Object3DTools.findFirstLeaf(root));
			// Vector3D testPos = Object3DTools.findFirstLeaf(root).getPosition();
			Object3D junctions = BranchDescriptorTools.generateJunctions(stemRoot, junctionName, 0,
						    corridorDescriptor, stemFitRmsTolerance, junctionController.getParameters().loopLengthSumMax);

			assert junctions != null;
			if (junctions.size() > 0) { // only add if junctions were found
			    graph.addGraph(junctions);
			}
		    }
		}
	    }
	    if (addStemsFlag) {
		updateFragmentGridTiler();
		assert fgTiler.getStrandJunctionDB() != null;
		Object3DLinkSetBundle stemBundle = fgTiler.generateTiling(root, linkSet, 
									  name, sequenceChar, onlyFirstPathMode);
		if( stemBundle != null ) {
		    graph.addGraph(stemBundle.getObject3D()); //TODO: add links while adding object3D
		    links.addLinks(stemBundle.getLinks());
		}
	    }
	    log.fine("readAndAdd finished!");
	}
	else { //TODO: CEV
	    assert false;
	}
	/*
	  else { //if PointSetReader2
	  //	    PointSet2 tmpGraph = (PointSet2)reader.readAnyObject3D(is);
	  Object3D tmpGraph = reader.readAnyObject3D(is);
	  assert tmpGraph.size() > 0;
	  assert(tmpGraph.hasSymmetries());
	  log.info("Object3DGraphController.readAndAdd: 3D object with size " + tmpGraph.size() + " read! " + tmpGraph.getPosition());
	  tmpGraph.setName(nameOrig);
	  if (scale != 1.0) {
	  if (scale != 1.0) {
	  Vector3D scaleVector = new Vector3D(scale, scale, scale);
	  log.info("Scaling objects with factors: " + scaleVector.toString());
	  tmpGraph.scale(scaleVector);
	  }
	  }
	  log.info("First position of read graph: " + Object3DTools.findFirstLeaf(tmpGraph));
	  log.info("adding graph to graph controller...");
	  assert offset.length() == 0.0; // cannot handle offset currently
	  graph.addGraph(tmpGraph); //TODO: add links while adding object3d
	  links.addLinks(reader.getLinkSet());
	  //	    links.addLinks(bundle.getLinks());
	  if (findStemsFlag) {
	  log.info("findStemsFlag");
	  Object3DLinkSetBundle stemBundle = StemTools.generateStems(tmpGraph);
	  // 		Object3D stemRoot = stemBundle.getObject3D();
	  // 		if (stemRoot.size() > 0) {
	  // 		    graph.addGraph(stemRoot);
	  // 		    links.addLinks(stemBundle.getLinks());
	  // 		    if (addJunctionsFlag) {
	  // 			String junctionName = name + JUNCTION_ENDING;
	  // 			// 		    Object3DTools.printTree(System.out, tmpGraph);
	  // 			Vector3D testPos = Object3DTools.findFirstLeaf(tmpGraph).getPosition();
	  // 			Object3D junctions = BranchDescriptorTools.generateJunctions(stemRoot, junctionName, tmpGraph); // Object3D junctions = BranchDescriptorTools.generateJunctions(stemRoot, junctionName, tmpGraph, branchDescriptorOffset);
	  // 			assert junctions != null;
	  // 			if (junctions.size() > 0) { // only add if junctions were found
	  // 			    graph.addGraph(junctions);
	  // 			}
	  // 			assert testPos.distance(Object3DTools.findFirstLeaf(tmpGraph).getPosition()) < 0.001; // test for side effects
	  // 			// 		    Object3DTools.printTree(System.out, tmpGraph);
	  // 		    }
	  // 		}
	  }
	  log.info("First position of read graph before starting CompleGridTiler: " + Object3DTools.findFirstLeaf(tmpGraph));
	  assert tmpGraph.size() > 0;
	  if (addStemsFlag) {
	  log.info("addStemsFlag");
	  assert junctionController.getJunctionDB() != null;
	  updateFragmentGridTiler();
	  assert fgTiler.getStrandJunctionDB() != null;
	  // GridTiler tiler = fgTiler; // new FragmentGridTiler(junctionController.getJunctionDB(),
	  // junctionController.getKissingLoopDB());
	  // ((FragmentGridTiler)(tiler)).setNucleotideDB(nucleotideDB); // sets nucleotide database (may be null)
	  Object3DLinkSetBundle stemBundle = fgTiler.generateTiling(tmpGraph, links, 
	  name, sequenceChar, onlyFirstPathMode);
	  if( stemBundle != null ) {
	  graph.addGraph(stemBundle.getObject3D()); //TODO: add links while adding object3D
	  links.addLinks(stemBundle.getLinks());
	  }
	  }
	  
	  log.info("First position of read graph before finished readAndAdd: " + Object3DTools.findFirstLeaf(tmpGraph));
	  log.info("readAndAdd finished!");
	  }
	*/
	log.info("readAndAdd finished!");
    }
    
    /** reads nucleotide database from file name */
    public void readNucleotideDB(String fileName) throws Object3DGraphControllerException {
	Object3DFactory reader = new RnaPdbReader();
	try {
	    FileInputStream fis = new FileInputStream(fileName);
// 	    Object3DLinkSetBundle bundle = reader.readBundle(fis);
// 	    Object3D nucleotideDBTmp = bundle.getObject3D();
	    nucleotideDB = NucleotideDBTools.readNucleotideDB(fis); // prepareDB(nucleotideDBTmp);
	}
	catch (IOException exc) {
	    throw new Object3DGraphControllerException("Error reading nucleotide database. " + exc.getMessage());
	}
    }
    
    /**
     * brings internal data up to date in case the Object3D tree has changed
     * TODO: depending on event type, parts of refresh calls are not necessary
     */
    public void refresh(ModelChangeEvent event) {
	// 	iterator = new Object3DDepthIterator(graph);
	// 	iterator.setDepthMax(depthMax);
	//log.info("calling refresh");
	if (!(event.getSource() instanceof LinkController)) {
	    refreshLinks();
	}
	refreshBindingSites();
	refreshSequences();
	// refreshSelection(); // TODO check if code ok
	refreshShapeSet();
	fireModelChanged(event); //this is where repaint is being called...(trying to stop tree from collapsing
	//log.info("Done calling refresh");
    }
    
    private void refreshBindingSites() {
	if (graph.getGraph() != null) {
	    SequenceBindingSiteCollector collector = new SequenceBindingSiteCollector();
	    Object3DActionVisitor visitor = new Object3DActionVisitor(graph.getGraph(), collector); 
	    // scan whole tree:
	    visitor.nextToEnd();
	    // transfer to sequences here:
	    bindingSites.clear();
	    for (int i = 0; i < collector.size(); ++i) {
		bindingSites.addBindingSite(collector.get(i));
	    }
	}
	else {
	    bindingSites.clear();
	}
    }
    
    /** removes bad links. TODO : Should be private method? */
    public void refreshLinks() {
	links.removeBadLinks(graph.getGraph());
    }
    
    private void refreshSequences() {
	log.fine("starting refreshSequences!");
	if (graph.getGraph() != null) {
	    SequenceCollector collector = generateCollectedSequences(graph.getGraph()); // new SequenceCollector();
	    // Object3DActionVisitor visitor = new Object3DActionVisitor(graph.getGraph(), collector); 
	    // scan whole tree:
	    // visitor.nextToEnd();
	    // transfer to sequences here:
	    sequences.clear();
	    for (int i = 0; i < collector.getSequenceCount(); ++i) {
		try {
		    sequences.addSequence(collector.getSequence(i));
		    log.finest("Found sequence: " + collector.getSequence(i).sequenceString());
		    // 		    if ((collector.getSequence(i).getParentObject() != null)
		    // 			&& (collector.getSequence(i).getParentObject() instanceof Object3D)) {
		    // 			Object3D obj = (Object3D)(collector.getSequence(i).getParentObject());
		    // 			log.finest("Parent name: " + obj.getName());
		    // 		    }
		}
		catch (DuplicateNameException e) {
		    log.severe("internal error: duplicate sequence names found!");
		}
	    }
	}
	else {
	    sequences.clear();
	}
	log.fine("finished refreshSequences!");
    }

    /** Removes phosphate groups from 5' ends of sequences */
    public int removeFivePrimePhosphates() {
	log.fine("starting removeFivePrimePhosphates!");
	int count= 0;
	if (graph.getGraph() != null) {
	    Object3DSet strands = graph.collectStrands();
	    for (int i = 0; i < strands.size(); ++i) {
		RnaStrand strand = (RnaStrand)(strands.get(i));
		assert strand.getResidueCount() > 0;
		if (NucleotideTools.removePhosphateGroup((Nucleotide3D)(strand.getResidue(0)))) {
		    ++count;
		}
	    }
	}

	refresh(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED));
	log.fine("finished refreshSequences!");
	return count; // returns number of removed groups
    }
    
    /**
     * brings shape set in sync with Object3D graph 
     */
    private void refreshShapeSet() {
	assert (shapeSetFactory != null);
	shapeSetFactory.setDepthMax(graph.getDepthMax());
	shapeSet = shapeSetFactory.createShapeSet(graph.getGraph());
	assert shapeSet != null;
	// add shapes corresponding to links
	for (int i = 0; i < links.size(); ++i) {
	    Shape3DSet sSet = shapeSetFactory.createShapeSet(links.get(i));
	    if (sSet != null) {
		if (shapeSet != null) {
		    shapeSet.merge(sSet);
		}
		else {
		    shapeSet = sSet;
		}
	    }
	}
	// add shape for selection cursor:
	assert (shapeSet != null);
	shapeSet.add(getSelectionCursorShape());
	// add shape for selection root:
	shapeSet.add(getSelectionRootShape());
	// add shapes corresponding to unit cell:
	switch (gridShapeMode) {
	case GRID_SHAPE_NONE:
	    break; // add nothing
	case GRID_SHAPE_UNIT:
	    log.fine("Adding grid shape!");
	    shapeSet.merge(GridShapeTools.createUnitCellShape(getCell()));
	    break;
	case GRID_SHAPE_SECTION:
	    log.info("GRID_SHAPE_SECTION not yet implemented!");
	    break; // not yet implemented
	}
	assert shapeSet != null;
    }

    /** removes all sequence in which every atom has another atom in less than 0.1 Angstrom. Not valid for sequences part of a junction */
    public void removeDuplicateSequences() {
	getGraph().removeDuplicateSequences();
	refresh(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED));
    }
    
    /** resets junction controller with new parameters */
    public void resetJunctionController(PdbJunctionControllerParameters parameters) {
	// junctionController = new PdbJunctionController(parameters);
	junctionController.reset(parameters);
    }
    
    public double findRms ( String name1, String name2) throws Object3DGraphControllerException{
    	Object3D obj1 = graph.findByFullName(name1);
    	Object3D obj2 = graph.findByFullName(name2);
    	return findRms (obj1,obj2);
    }
    
    public double findRms ( Object3D obj1, Object3D obj2) throws Object3DGraphControllerException {
			//  	Object3DSet atms1 = AtomTools.findAtoms(obj1);
			//  	Object3DSet atms2 = AtomTools.findAtoms(obj2);
			String [] atmNames = {"C4*"};
			List<Residue3D> res1 = generateResidueList(obj1);
			List<Residue3D> res2 = generateResidueList(obj2);
			log.info("Matching "+res1.size()+" to "+res2.size()+" residues");
			List<Atom3D> atms1 = generateAtomList(res1, atmNames);
			List<Atom3D> atms2 = generateAtomList(res2, atmNames);
			log.info("Matching "+atms1.size()+" to "+atms2.size()+" atoms");
			if (atms1.size()!=atms2.size()) {
				throw new Object3DGraphControllerException("Atom number mismatch");
			}
			//  	Vector3D[] vec1 = Object3DSetTools.getCoordinates(atms1);
			//  	Vector3D[] vec2 = Object3DSetTools.getCoordinates(atms2);
			Vector3D[] vec1 = new Vector3D[atms1.size()];
			Vector3D[] vec2 = new Vector3D[atms2.size()];
			for(int i=0; i<atms1.size(); i++){
				vec1[i] = atms1.get(i).getPosition(0);
			}
			for(int i=0; i<atms2.size(); i++){
				vec2[i] = atms2.get(i).getPosition(0);
			}
			Superpose superposer = new KabschSuperpose();
			SuperpositionResult supResult = superposer.superpose( vec1, vec2);
			double rms = supResult.getRms();
			return rms;
    }
    
    /** returns crystallographic cell */
    public void setCell(double a, double b, double c, 
			double alpha, double beta, double gamma) { 
	cell.setA(a); cell.setB(b); cell.setC(c);
	cell.setAlpha(alpha); cell.setBeta(beta); cell.setGamma(gamma);
	modelChanged(new ModelChangeEvent(eventConst.LATTICE_MODIFIED));
    }
    
    /** Set grid shape mode */
    public void setGridShapeMode(int mode) {
	this.gridShapeMode = mode;
	refresh(new ModelChangeEvent(this, eventConst.VIEW_MODIFIED));
    }

    /** Returns minimum length of stems to be recognized when importing file */
    public void setImportStemLengthMin(int length) {
	this.importStemLengthMin = length;
    }
    
    /** sets last read directory */
    public void setLastReadDirectory(String dir) { this.lastReadDirectory = dir; }
    
    public void setLatticeSection(int xMin, int yMin, int zMin, int xMax, int yMax, int zMax) {
	this.latticeSection.setXMin(xMin);
	this.latticeSection.setYMin(yMin);
	this.latticeSection.setZMin(zMin);
	this.latticeSection.setXMax(xMax);
	this.latticeSection.setYMax(yMax);
	this.latticeSection.setZMax(zMax);
	refresh(new ModelChangeEvent(this, eventConst.VIEW_MODIFIED));
	// setSymmetryMode(symmetryMode);
    }
    
    public void setSpaceGroup(int number) throws Object3DGraphControllerException {
	log.info("called setSpaceGroup with " + number);
	if (! spaceGroupFactory.isValid()) {
	    initSpaceGroupFactory();
	}
	try {
	    log.info("Generating space group from factory class...");
	    SpaceGroup g = spaceGroupFactory.generate(number);
	    log.info("The generated space group is: " + g.toString());
	    setSpaceGroup(g);
	}
	catch (SymmetryException se) {
	    throw new Object3DGraphControllerException(se.getMessage());
	}
    }
    
    public void setSpaceGroup(String name) throws Object3DGraphControllerException {
	if (! spaceGroupFactory.isValid()) {
	    initSpaceGroupFactory();
	}
	try {
	    SpaceGroup g = spaceGroupFactory.generate(name);
	    setSpaceGroup(g);
	}
	catch (SymmetryException se) {
	    throw new Object3DGraphControllerException(se.getMessage());
	}
	// get total number of exisiting symmetries
	this.spaceGroupSymmetryCount = SymmetryTools.computeSpaceGroupSymmetryCount(spaceGroup);
    }
    
    public void setSpaceGroup(String name, String conventionName) throws Object3DGraphControllerException {
	if (! spaceGroupFactory.isValid()) {
	    initSpaceGroupFactory();
	}
	try {
	    setSpaceGroup(spaceGroupFactory.generate(name, conventionName));
	}
	catch (SymmetryException se) {
	    throw new Object3DGraphControllerException(se.getMessage());
	}
    }
    
    private void setSpaceGroup(SpaceGroup spaceGroup) throws Object3DGraphControllerException {
	this.spaceGroup = spaceGroup;
	this.spaceGroupSymmetryCount = SymmetryTools.computeSpaceGroupSymmetryCount(spaceGroup);
	log.info("Space group symmetry count was set to: " + this.spaceGroupSymmetryCount);
	setSymmetryMode(symmetryMode);
    }
    
    /** sets symmetry mode : results in different views */
    public void setSymmetryMode(int mode) throws Object3DGraphControllerException {
	this.symmetryMode = mode;
	switch (mode) {
	case SYMMETRY_NONE: kaleidoscope = new DummyKaleidoscope();
	    break;
	case SYMMETRY_CELL: kaleidoscope = new CellKaleidoscope(cell, latticeSection);
	    break;
	case SYMMETRY_ALL: kaleidoscope = new SymmetryKaleidoscope(spaceGroup, cell, latticeSection);
	    break;
	default:
	    assert false;
	    log.severe("Unknown symmetry view mode!");
	}
	assert kaleidoscope != null;
	refresh(new ModelChangeEvent(this, eventConst.VIEW_MODIFIED));
    }
    
    /**
     * uses space group and selected object to tile space.
     * Indices measure unit cell positions in grid space; max indices are inclusive.
     */
    public void tileSpace(int xmin, int ymin, int zmin,
			  int xmax, int ymax, int zmax,
			  String name) {
	log.severe("Sorry, tileSpace not yet implemented!");
	LatticeTiler tiler = new DefaultLatticeTiler();
	Object3DLinkSetBundle result = tiler.generateTiling(new SimpleObject3DLinkSetBundle(graph.getSelectionRoot(), links),
							    new DefaultLattice(cell, spaceGroup), latticeSection,
							    name);
	graph.addGraph(result.getObject3D());
	log.warning("Sorry, no cloning of links implemented yet!");
    }
    
    private void updateFragmentGridTiler() {
	fgTiler.setStrandJunctionDB(junctionController.getJunctionDB());
	fgTiler.setKissingLoopDB(junctionController.getKissingLoopDB());
	fgTiler.setNucleotideDB(this.nucleotideDB);
    }
    
    /** write tree in format given by writer */
    public void write(OutputStream os, Object3DWriter writer) {
	graph.write(os, writer);
	writer.write(os, links);
    }

    /** write sub-trees in format given by writer */
    public void write(OutputStream os, Object3DWriter writer, String[] subtreeNames) throws Object3DGraphControllerException {
	if ((subtreeNames == null) || (subtreeNames.length == 0)) {
	    write(os, writer);
	}
	else {
	    for (String subtree : subtreeNames) {
		Object3D tree = graph.findByFullName(subtree);
		if (tree != null) {
		    writer.write(os, tree);
		}
		else {
		    throw new Object3DGraphControllerException("Error in write method: Could not find object with name " + subtree);
		}
	    }
	    writer.write(os, links); // ok, this is not subtree specific... :-( TODO
	}
    }
    
    /**
     * write tree in format given by writer
     * originalMode : use original strand names from imported pdb file 
     */
    public void writeJunctionsToPdb(String fileNameBase, boolean originalMode) {
	Object3DSet junctions = Object3DTools.collectByClassName( graph.getGraph() , "StrandJunction3D");
	String currFileName = "";
	for (int i = 0; i < junctions.size(); ++i) {
	    StrandJunction3D junction = (StrandJunction3D)(junctions.get(i));
	    String pdbOutput = StrandJunctionTools.toPdb(junction, originalMode);
	    try {
		currFileName = fileNameBase + "." + (i+1);
		FileOutputStream fos = new FileOutputStream(currFileName);
		PrintStream ps = new PrintStream(fos);
		ps.println(pdbOutput);
		fos.close();
	    }
	    catch (IOException iox) {
		log.warning("could not write to " + currFileName);
	    }
	}
    }
    
    /** write default tree in PDB format and call CCP4 program pdbset/gensym */
    public void writePdbSymmetric(String fileName) throws Object3DGraphControllerException {
	SymmetryTools.writePdbSymmetric(fileName, graph.getGraph(), generateSymgenCommands(getSpaceGroupSymmetryCount()));
    }

    /** Writes "points" (xyz coordinates of objects) to a file for a given tree root node and all members with a specified className */
    public void writePoints(OutputStream os, String rootName, String className, boolean removeRoot) throws IOException, Object3DGraphControllerException {
	Object3D root = graph.findByFullName(rootName);
	if (root == null) {
	    throw new Object3DGraphControllerException("Could not find object with name: " + rootName);
	}
	Object3DSet objects = Object3DTools.collectByClassName(root, className);
	if (removeRoot) {
	    objects.remove(root); 
	}
	PointSetWriter2 writer = new PointSetWriter2();
	writer.write(os, objects, links);
    }


		public void writeSecondaryStructure(OutputStream os, int format) throws Object3DGraphControllerException { 
			SecondaryStructure secStruct = null;
			try {
			    secStruct = generateSecondaryStructure();
			}
			catch(DuplicateNameException e) {
			    throw new Object3DGraphControllerException(e.getMessage());
			}
				writeSecondaryStructure(os,format,secStruct);
    }
		
    /** writes secondary structure to file. Format is compatible with RnaInverse script. */
  public void writeSecondaryStructure(OutputStream os, int format, SecondaryStructure secStruct) throws Object3DGraphControllerException {
	assert secStruct != null;
	log.fine("Writing secondary structure with: " + secStruct.getSequenceCount() 
		 + " sequences and " + secStruct.getInteractionCount()
		 + " base pair interactions." + secStruct.toString());
	SecondaryStructureWriter secWriter = null;
	switch (format) {
	case Object3DGraphControllerConstants.SECONDARY_FORMAT:
	    secWriter = new SecondaryStructureScriptFormatWriter();
	    break;
	case Object3DGraphControllerConstants.CT_FORMAT:
	    secWriter = new SecondaryStructureCTFormatWriter();
	    break;
        case Object3DGraphControllerConstants.FASTA_FORMAT:
            secWriter = new SecondaryStructureFastaFormatWriter();
	    break;
	case Object3DGraphControllerConstants.SERVER_FORMAT:
	    secWriter = new SecondaryStructureServerFormatWriter();
	    break;
	default:
	    throw new Object3DGraphControllerException("Unknown output format id!");
	}
	assert (secWriter != null);
	PrintStream ps = new PrintStream(os);
	ps.println(secWriter.writeString(secStruct));
    }

    /** loop through the entire structure and add any missing covalent link */
    public void addMissingLinks(){
	Object3D root = getGraph().getGraph();

	addMissingLinks(root, links);
    }
    public void addMissingLinks(Object3D obj, LinkController linkSet){
	LinkController tempSet = new SimpleLinkController();

	if(obj instanceof NucleotideStrand){
	    NucleotideStrand nucStrand = (NucleotideStrand)obj;
	    for(int k = 0; k < nucStrand.getResidueCount(); k++){
		Nucleotide3D nuc1 = (Nucleotide3D)nucStrand.getResidue3D(k);
		tempSet.addLinks(NucleotideTools.getCovalentAtomLinks(nuc1));		
	    }
	    for(int f = 0; f < tempSet.size(); f++){
		if(linkSet.contains(tempSet.get(f))){
		    tempSet.remove(tempSet.get(f));
		    f--;
		}
	    }
	    linkSet.addLinks(tempSet);
	}
	if ((obj instanceof Residue3D) || (obj instanceof Atom3D)) {
	    return; // terminate search
	}
	// recursive call
	for (int i = 0; i < obj.size(); ++i) {
	    addMissingLinks(obj.getChild(i), links);
	}
    }
    
    public void makeAlias(String name, String alias){
    	graph.makeAlias(name,alias);
    }
    
}

