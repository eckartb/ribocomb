package rnadesign.rnacontrol;

import rnadesign.rnamodel.RnaConstants;

public class Object3DGraphControllerConstants {

    public static final int KNOTPLOT_FORMAT = 2;
    public static final int PDB_FORMAT = 3; //TODO: remove? Only RNAVIEW used now
    public static final int PDB_DNA_FORMAT = 4;
    // public static final int PDB_PROTEIN_FORMAT = 5;
    public static final int PDB_RNAVIEW_FORMAT = 5; // TODO: verify! changed from 8!? EB 7/2007
    public static final int POINTSET_FORMAT = 6;
    public static final int POINTSET2_FORMAT = 7;
    public static final int NTL_FORMAT = 8;
    public static final int PDB_RNAVIEW_RENUMBER_FORMAT = 9;
    public static final int SECONDARY_FORMAT = 13;
    public static final int CT_FORMAT = 14;
    public static final int FASTA_FORMAT = 15;
    public static final int SERVER_FORMAT = 16;

    // Platonic solids:
    public static final int TETRAHEDRON = 10;
    public static final int CUBE = 11;
    public static final int OCTAHEDRON = 12;
    public static final int DODECAHEDRON = 13;
    public static final int ICOSAHEDRON = 14;

    // Optimization algorithm:
    public static final int MONTE_CARLO = 1;
    public static final int ELASTIC_NETWORK_EXTRAPOLATION = 2;

    // Tiling algorithms:
    public static final int TILER_SIMPLE = 1;
    public static final int TILER_COMPLETE = 2;
    public static final int TILER_RANDOM = 3;
    public static final int TILER_FRAGMENT = 4;
    public static final int TILER_DEBUG = 5;
    public static final int TILER_KISSINGLOOP_DEBUG = 6;

    /** defines different types of biomolecules */
    public static final int RNA = 0;
    public static final int DNA = 1;
    public static final int PROTEIN = 2;

    /** sequence optimizer algorithm */
    public static final int NO_SEQUENCE_OPTIMIZATION = 0;
    public static final int SEQUENCE_OPTIMIZER_SCRIPT = 1;
    public static final int MONTE_CARLO_SEQUENCE_OPTIMIZER = 2;

    public static final int DEFAULT_SCORER = 0; // default secondary structure scorer
    public static final int RNACOFOLD_SCORER = 1;
    public static final int PKNOTS_SCORER = 2;
    public static final int NUPACK_SCORER = 3;
    public static final int CRITON_SCORER = 4;
    public static final int DUMMY_SCORER = 5; // all secondary structures are scored as "zero"
    public static final int SIMPLE_SCORER = 6; // uses SimpleSeconaryStructurePredictorScorer
    public static final int PATHWAY_SCORER = 7;

    /** Misc constants */
    public static final int PDB_IMPORT_STEM_LENGTH_MIN = 3; // minimum stem length for PDB import
    public static final double CORRIDOR_DEFAULT_RADIUS = 0.0; // 5.0
    public static final double CORRIDOR_DEFAULT_START = 2.0; // 5.0;

    public static final int GRID_SHAPE_NONE = 0;
    public static final int GRID_SHAPE_UNIT = 1;
    public static final int GRID_SHAPE_SECTION = 2;
    public static final int GRID_SHAPE_DEFAULT = GRID_SHAPE_NONE;

    public static final int SYMMETRY_NONE = 0; // no symmetry "mirror images"
    public static final int SYMMETRY_CELL = 1; // one copy per lattice cell
    public static final int SYMMETRY_ALL  = 2; // symmetry copies according to space group and lattice cells
    public static final int SYMMETRY_DEFAULT  = SYMMETRY_CELL; // default symmetry mode

    public static final String COVERING_ENDING = "_cov";
    public static final String JUNCTION_ENDING = "_jnc";
    public static final String KISSING_LOOP_ENDING = "_kl";

    public static final double ATOM_COLLISION_DISTANCE = RnaConstants.ATOM_COLLISION_DISTANCE; // collision distance for non-hydrogen atoms

    public static final double RMS_STRAND_END_OFFSET = 0.5; // allow this much more lee-way for mutating bases at strand ends

    public static final double STRAND_FUSION_DIST_MAX = 12.0; // maxium distance between two connecting residues of strands to be fused

}
