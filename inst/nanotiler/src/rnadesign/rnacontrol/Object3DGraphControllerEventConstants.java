package rnadesign.rnacontrol;

public class Object3DGraphControllerEventConstants {

    public static final int MODEL_TRANSLATED = 1;

    public static final int MODEL_ROTATED = 2;

    public static final int MODEL_SELECTION_CHANGED = 3;

    public static final int MODEL_MODIFIED = 4;

    public static final int VIEW_MODIFIED = 5;

    public static final int LATTICE_MODIFIED = 6;

    private String[] eventDescriptions = { "UNDEFINED_CONTROL_EVENT", 
					   "MODEL_TRANSLATED", "MODEL_ROTATED", 
					   "MODEL_SELECTION_CHANGED", "MODEL_MODIFIED", "VIEW_MODIFIED",
					   "SPACE_GROUP_MODIFIED"
    };

    /** returns description of event */
    public String getEventDescription(int id) throws IndexOutOfBoundsException{
	return eventDescriptions[id];
    }

}
