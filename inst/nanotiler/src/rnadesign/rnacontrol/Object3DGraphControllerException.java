package rnadesign.rnacontrol;

import generaltools.ApplicationException;

public class Object3DGraphControllerException extends ApplicationException {

    public Object3DGraphControllerException() {
	super("Unspecified controller exception.");
    }

    public Object3DGraphControllerException(String msg) {
	super(msg);
    }

}
