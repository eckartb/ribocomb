package rnadesign.rnacontrol;

import tools3d.objects3d.Object3D;

/** this class is merely a wrapper if the gui has to handle a "pointer" to a subtree without violating the
 * model-view-controller design pattern 
 */
public class Object3DHandle {

    Object3D object3D;

    public Object3DHandle(Object3D obj) {
	this.object3D = obj;
    }

    /** returns true if object defined */
    public boolean isValid() { return (object3D != null); }

    public Object3D getObject3D() { return object3D ; }

}
