package rnadesign.rnacontrol;

import java.util.Iterator;

public interface Object3DSelection {

    /** adds object */
    public void add(Object3DHandle obj);

    public void clear();

    /** returns iterator to object3d handles */
    public Iterator iterator();

    /** removes object */
    public void remove(Object3DHandle obj);

    public int size();

}

