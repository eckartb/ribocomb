package rnadesign.rnacontrol;

import java.io.File;

public class PackageConstants {

    public static final double RAD2DEG = 180.0 / Math.PI;

    public static final double DEG2RAD = Math.PI / 180.0;

    public static final String PDB_CHAIN_CHAR = "pdb_chain_char";

    public static final String LOGFILE_DEFAULT = "NanoTiler_debug";
    
    public static final String NEWLINE = System.getProperty("line.separator");

    public static final String ENDL = NEWLINE;

    public static final String NANOTILER_HOME_VAR = "NANOTILER_HOME";

    public static String SLASH = File.separator; // either "/" or "\"  depending on Unix of Windows

}
