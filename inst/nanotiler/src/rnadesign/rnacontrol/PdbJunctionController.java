package rnadesign.rnacontrol;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.Character;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import rnadesign.designapp.rnagui.NanoTiler;
import rnadesign.rnamodel.*;

/**
 * This controller class provides an easy to use
 * API for managing a database of RNA junctions
 * (parsed from PDB files) 
 */
public class PdbJunctionController implements JunctionController {

    public static final String NEWLINE = System.getProperty("line.separator");
    public static final String JUNCTION_DB_NAME = "junctionDB";
    public static final String KISSING_LOOP_DB_NAME = "kissingLoopDB";
    public static final String MOTIF_DB_NAME = "motifDB";
    public static final int JUNCTIONS_LENGTH = 11;
    public static final int JUNCTION_ORDER_LENGTH = 22;
    public static final int ORIGINAL_JUNCTIONSTRING_LENGTH = 155;
    public static final int ORIGINAL_J_LENGTH = 144;
    public static final int KISSINGLOOPS_LENGTH = 15;
    public static final int ORIGINAL_KISSINGLOOPSTRING_LENGTH = 159;
    public static final int ORIGINAL_K_LENGTH = 144;
    public static final int LOOP_LENGTH_SUM_MAX = 50;

    private int branchDescriptorOffset = 2; // changed Nov 2007, EB
    private CorridorDescriptor corridorDescriptor = new CorridorDescriptor(Object3DGraphControllerConstants.CORRIDOR_DEFAULT_RADIUS,
									   Object3DGraphControllerConstants.CORRIDOR_DEFAULT_START);
    private StrandJunctionDB junctionDB; // database of junctions
    private StrandJunctionDB kissingLoopDB; // database of junctions
    private StrandJunctionDB motifDB; // database of junctions
    private String nameFileName; // name of file with PDB file names
    private String readDir; // files names are in this directory if relative file names
    private String writeDir; // The directory to write the .ntl files to.
    private boolean noWriteFlag = false; // False = write .ntl files.
    private int maxOrders = 4;
    private int loopLengthSumMax = LOOP_LENGTH_SUM_MAX;
    private int stemLengthMin = 3;
    private boolean rnaMode = true;
    private boolean randomMode = true;
    private boolean intHelixCheck= true;
    private boolean motifMode = false; // if true, all 3D objects are viewed as general motifs instead of junctions and kissing loops 

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    public PdbJunctionController() { }

    public PdbJunctionController(int maxOrders,
				 String nameFileName,
				 String readDir,
				 String writeDir,
				 boolean noWriteFlag,
				 boolean rnaMode,
				 int branchDescriptorOffset,
				 int stemLengthMin,
				 CorridorDescriptor corridorDescriptor,
				 int loopLengthSumMax, boolean intHelixChecker, boolean motifMode) {
 	this.maxOrders = maxOrders;
 	this.nameFileName = nameFileName;
 	this.readDir = readDir;
 	this.writeDir = writeDir;
 	this.noWriteFlag = noWriteFlag;
 	this.rnaMode = rnaMode;
 	this.branchDescriptorOffset = branchDescriptorOffset;
 	this.stemLengthMin = stemLengthMin;
 	this.corridorDescriptor = corridorDescriptor;
 	this.loopLengthSumMax = loopLengthSumMax;
	this.intHelixCheck = intHelixChecker;
	this.motifMode = motifMode;
	init();
	log.info("Generated PdbJunction controller with minimum stem length " + stemLengthMin);
    }

    /* public PdbJunctionController(int maxOrders,
				 String nameFileName,
				 String readDir,
				 String writeDir,
				 boolean noWriteFlag,
				 boolean rnaMode,
				 int branchDescriptorOffset,
				 int stemLengthMin,
				 CorridorDescriptor corridorDescriptor,
				 int loopLengthSumMax) {
	this(maxOrders, nameFileName, readDir, writeDir, noWriteFlag, rnaMode, branchDescriptorOffset, stemLengthMin, corridorDescriptor, loopLengthSumMax, true, false);
	}
    */

    public PdbJunctionController(PdbJunctionControllerParameters prm) {
	this(prm.orderMax, prm.nameFileName,
	     prm.readDir,
	     prm.writeDir,
	     prm.noWriteFlag, 
	     prm.rnaMode,
	     prm.branchDescriptorOffset,
	     prm.stemLengthMin,
	     prm.corridorDescriptor,
	     prm.loopLengthSumMax, prm.intHelixCheck, prm.motifMode);
	log.info("Generated PdbJunction controller using parameter object with minimum stem length " + stemLengthMin);
    }

    /** returns newly generated parameter class */
    public PdbJunctionControllerParameters getParameters() { 
	return new PdbJunctionControllerParameters(maxOrders,
						   nameFileName,
						   readDir,
						   writeDir,
						   noWriteFlag,
						   rnaMode,
						   branchDescriptorOffset,
						   stemLengthMin,
						   corridorDescriptor,
						   loopLengthSumMax, intHelixCheck, motifMode);
    }

    public int getJunctionCount() {
	return junctionDB.retrieveAllJunctions().size();
    }

    public int getKissingLoopCount() {
	return kissingLoopDB.retrieveAllJunctions().size();
    }

    public int getMotifCount() {
	return motifDB.retrieveAllJunctions().size();
    }

    public int size() { return getMotifCount() + getKissingLoopCount() + getJunctionCount(); }

    private void init() {
	log.fine("Calling init!!");
	try {
	    // try {
		String[] fileNames = readNames(nameFileName);
		int[] cloneDownSizes = readCloneDownSizes(nameFileName);
		junctionDB = new SimpleStrandJunctionDB(maxOrders);
		kissingLoopDB = new SimpleStrandJunctionDB(maxOrders);
		motifDB = new SimpleStrandJunctionDB(maxOrders);
		kissingLoopDB.setName(KISSING_LOOP_DB_NAME);
		motifDB.setName(MOTIF_DB_NAME);
		junctionDB.setJunctionType(DBElementDescriptor.JUNCTION_TYPE);
		kissingLoopDB.setJunctionType(DBElementDescriptor.KISSING_LOOP_TYPE);
		motifDB.setJunctionType(DBElementDescriptor.MOTIF_TYPE);

		log.fine("Starting to parse PDB files.");
		StrandJunctionDBTools.parsePdbFilesToDB(junctionDB,
							kissingLoopDB,
							motifDB,
							fileNames,
							cloneDownSizes,
							readDir,
							writeDir,
							noWriteFlag,
							rnaMode,
							randomMode,
							branchDescriptorOffset,
							stemLengthMin,
							corridorDescriptor,
							loopLengthSumMax, intHelixCheck, motifMode);
		log.fine("PdbJunctionController initialized!");
		log.fine("Junctions:" + NEWLINE + junctionDB.infoString());
		// setJunctionString(junctionDB.infoString());
		log.fine("Kissing Loops:" + NEWLINE + kissingLoopDB.infoString());
		// setKissingLoopString(kissingLoopDB.infoString());
		// taken out: controller code should know nothing about gui!
		// JOptionPane.showMessageDialog(null,"Done loading database.","Dialog Box", JOptionPane.PLAIN_MESSAGE);
		assert isValid();
// 	    }
// 	    catch(NullPointerException e) {
// 		log.severe("PdbJunctionController: Null pointer exception");
// 	    }
	}
	catch (IOException ioe) {
	    log.severe("Error reading file names: " + ioe.getMessage());
	}
    }

    /** Stores junction information */
    //TODO : remove junctions of order 0
    //uncomment if we're able to load a junction database and be able to access it after loading another.
//     public void setJunctionString(String j) { //setJunctionString
//  	String temp = ("Junctions:" + NEWLINE);
// 	int m = 0;
// 	int n = 0;
// 	char c;
//  	this.junctionString = junctionString = ("Junctions:" + NEWLINE + j);
// 	// Code removed: Removed1
//     }

    /** returns junction information*/
    public String getJunctionString() {
	if (junctionDB == null) {
	    return "No junction data defined.";
	}
	StringBuffer buf = new StringBuffer();
	for (int i = 1; i <= maxOrders; ++i) {
	    if (junctionDB.size(i) > 0) {
		buf.append("Number of loaded junctions of order " + i + " : " + junctionDB.size(i) + "\n");
	    }
	}
	return buf.toString();
    }

    /** returns kissing loop information */
    public String getKissingLoopString() {
	if (kissingLoopDB == null) {
	    return "No kissing loop data defined.";
	}
	return "Number of loaded kissing loops: " + kissingLoopDB.size(2);
    }

    /** returns information regarding loaded kissing loops */
    public String getMotifString() {
	if (motifDB == null) {
	    return "No motif data defined.";
	}
	StringBuffer buf = new StringBuffer();
	for (int i = 1; i <= maxOrders; ++i) {
	    if (motifDB.size(i) >= 0) {
		buf.append("Number of loaded general motifs of order " + i + " : " + motifDB.size(i) + "\n");
	    }
	}
	return buf.toString();
    }
    
    public boolean isValid() {
	return (junctionDB != null) && (kissingLoopDB != null);
    }

    public StrandJunctionDB getJunctionDB() {
	log.fine("called getJunctionDB");
	if (!isValid()) {
	    log.finest("starting init");
	    init();
	}
	log.finest("junctionDB: " + junctionDB);
	return junctionDB;
    }

    public StrandJunctionDB getKissingLoopDB() {
	log.fine("called getKissingLoopDB");
	if (!isValid()) {
	    log.finest("starting init.");
	    init();
	}
	log.finest("kissingLoopDB: " + kissingLoopDB);
	return kissingLoopDB;
    }

    public StrandJunctionDB getMotifDB() {
	log.fine("called getMotifDB");
	if (!isValid()) {
	    log.finest("starting init");
	    init();
	}
	return motifDB;
    }

    /** returns information about junction database
     * (number of junctions and kissing loops etc)
     * TODO implement method
     */
    public String infoString() {
	log.finest("called infoString.");
	return getJunctionString() + NEWLINE
	    + getKissingLoopString() + NEWLINE //  + getListOfPdbs();
	    + getMotifString(); //  + getListOfPdbs();
    }

    public static String generateSuperposableJunctionString(int order, int n, double rmsLimit, StrandJunctionDB db) {
	int[] junctions = db.getSuperposableJunctions(order, n, rmsLimit);
	log.fine("Found " + junctions.length + " superposable objects for junction " + order + " " + (n + 1));
	StringBuffer buf = new StringBuffer();
	buf.append("" + order + " " + n);
	for (int i = 0; i < junctions.length; ++i) {
	    buf.append(" " + (junctions[i] + 1 ));
	}
	return buf.toString();
    }

    public static void printSuperposableJunctions(double rmsLimit, StrandJunctionDB db, PrintStream ps) {
	if (db == null) {
	    log.warning("No junction database defined.");
	    return;
	}
	for (int order = 1; order < db.size(); ++order) {
	    for (int n = 0; n < db.size(order); ++n) {
		String s = generateSuperposableJunctionString(order, n, rmsLimit, db);
		ps.println(s);
	    }
	}
    }

    public void printSuperposableJunctions(double rmsLimit, PrintStream ps) {
	printSuperposableJunctions(rmsLimit, this.junctionDB, ps);
    }

    public void printSuperposableKissingLoops(double rmsLimit, PrintStream ps) {
	printSuperposableJunctions(rmsLimit, this.kissingLoopDB, ps);
    }

    /** ranks 2D junctions for interpolating loops */
    public List<Properties> rankJunctionFits(BranchDescriptor3D b1, BranchDescriptor3D b2,
					     int n1Min, int n1Max, int n2Min, int n2Max) {
	return junctionDB.rankLoopFits(b1, b2, n1Min, n1Max, n2Min, n2Max);
    }

    /** ranks 2D kissing loops for interpolating loops */
    public List<Properties> rankKissingLoopFits(BranchDescriptor3D b1, BranchDescriptor3D b2,
						int n1Min, int n1Max, int n2Min, int n2Max) {
	List<Properties> result = kissingLoopDB.rankLoopFits(b1, b2, n1Min, n1Max, n2Min, n2Max);
	return result;
    }

    /** sets new parameters for database */
    public void reset(PdbJunctionControllerParameters prm) {
	log.fine("Called reset: " + prm);
	branchDescriptorOffset = prm.branchDescriptorOffset;
	loopLengthSumMax = prm.loopLengthSumMax;
	corridorDescriptor = prm.corridorDescriptor;
	maxOrders = prm.orderMax;
	nameFileName = prm.nameFileName;
	readDir = prm.readDir;
	writeDir = prm.writeDir;
	noWriteFlag = prm.noWriteFlag;
	rnaMode = prm.rnaMode;
	stemLengthMin = prm.stemLengthMin;
	randomMode = prm.randomMode;
	intHelixCheck = prm.intHelixCheck;
	motifMode = prm.motifMode;
	junctionDB = null;
	kissingLoopDB = null;
	init();
    }

    /** read names from file */
    private String[] readNames(String fileName) throws IOException {
	log.fine("called readNames");
	try {
	    log.fine("fileName: " + fileName);
	    FileInputStream fis = new FileInputStream(fileName);
	    DataInputStream dis = new DataInputStream(fis);
	    String line;
	    List<String> list = new ArrayList<String>();
	    do {
		line = dis.readLine();
		if (line == null) {
		    break;
		}
		String s = line.trim();
		String[] words = s.split(" ");
		if ((words.length > 0) && (words[0].length() > 0)) {
		    list.add(words[0]);
		} else if (s.length() > 0) {
		    list.add(s);
		}
	    }
	    while (line != null);
	    String[] result = new String[list.size()];
	    fis.close();


 	    for (int i = 0; i < result.length; ++i) {
 		result[i] = (String)(list.get(i));
	    }

	    // TODO taken out all code concerning pdb files. Should better be part of junctionDB, kissingLoopDB.

// 		try {
// 		    String[] tempArray = new String[this.pdbFileNames.length];
// 		    tempArray = this.pdbFileNames;
// 		    this.pdbFileNames = new String[this.pdbFileNames.length + list.size()];
// 		    for (int x = 0; x < tempArray.length; x++) {
// 			this.pdbFileNames[x] = tempArray[x];
// 		    }
// 		    // setListOfPdbs(result[i],i);
// 		}
// 		catch (NullPointerException npe) {
// 		    // NanoTiler.pdbFileNames = new String[list.size()];
// 		    // setListOfPdbs(result[i],i);
// 		}
// 	    }
	    // setJunctionNumber(result.length);
	    return result;
	}
	catch (NullPointerException e) {
	    log.warning(e + ":  " + e.getMessage()); 
	    return null;
	}
    }

    /** read names from file */
    private int[] readCloneDownSizes(String fileName) throws IOException {
	log.fine("called readNames");
	int[] result = null;
	try {
	    log.fine("fileName: " + fileName);
	    FileInputStream fis = new FileInputStream(fileName);
	    DataInputStream dis = new DataInputStream(fis);
	    String line;
	    List<Integer> list = new ArrayList<Integer>();
	    do {
		line = dis.readLine();
		if ((line == null) || (line.length() == 0)) {
		    break;
		}
		String s = line.trim();
		String[] words = s.split(" ");
		if ((words.length > 1) && (words[1].length() > 0)) {
		    list.add(Integer.parseInt(words[1]));
		} else {
		    list.add(0); // dummy value
		}
	    }
	    while (line != null);
	    result = new int[list.size()];
	    fis.close();
	    
 	    for (int i = 0; i < result.length; ++i) {
 		result[i] = list.get(i).intValue();
	    }

	    // TODO taken out all code concerning pdb files. Should better be part of junctionDB, kissingLoopDB.

// 		try {
// 		    String[] tempArray = new String[this.pdbFileNames.length];
// 		    tempArray = this.pdbFileNames;
// 		    this.pdbFileNames = new String[this.pdbFileNames.length + list.size()];
// 		    for (int x = 0; x < tempArray.length; x++) {
// 			this.pdbFileNames[x] = tempArray[x];
// 		    }
// 		    // setListOfPdbs(result[i],i);
// 		}
// 		catch (NullPointerException npe) {
// 		    // NanoTiler.pdbFileNames = new String[list.size()];
// 		    // setListOfPdbs(result[i],i);
// 		}
// 	    }
	    // setJunctionNumber(result.length);

	}
	catch (NullPointerException e) {
	    log.warning(e + ":  " + e.getMessage()); 
	    return null;
	}
	catch (NumberFormatException nfe) {
	    throw new IOException("Error parsing integer for clone down lengths: " + nfe.getMessage());
	}
	return result;
    }

    /** Returns all junctions that are compatible with the database element descriptor */
    public List<StrandJunction3D> retrieveJunctions(DBElementDescriptor dbElement) {
	if (dbElement.getType() == DBElementDescriptor.KISSING_LOOP_TYPE) {
	    return kissingLoopDB.retrieveJunctions(dbElement);
	}
	return junctionDB.retrieveJunctions(dbElement);
    }

    /** Returns all descriptors junctions and kissing loops as one list */
    public List<DBElementDescriptor> retrieveAllDescriptors() {
        List<DBElementDescriptor> result = new ArrayList<DBElementDescriptor>();
	if (junctionDB != null) {
	    result.addAll(junctionDB.retrieveAllDescriptors());
	}
	if (kissingLoopDB != null) {
	    result.addAll(kissingLoopDB.retrieveAllDescriptors());
	}
	if (motifDB != null) {
	    result.addAll(motifDB.retrieveAllDescriptors());
	}
	return result;
    }

    /** Returns all junctions and kissing loops as one list */
    public List<StrandJunction3D> retrieveAllJunctions() {
        List<StrandJunction3D> result = new ArrayList<StrandJunction3D>();
	if (junctionDB != null) {
	    result.addAll(junctionDB.retrieveAllJunctions());
	}
	if (kissingLoopDB != null) {
	    result.addAll(kissingLoopDB.retrieveAllJunctions());
	}
	return result;
    }

   
}

/*

Removed1

// 	else {
//  	    for(int i = 0; i <= 5; i++) { //int i = 1
//  		if( j.length() == ORIGINAL_J_LENGTH )
// 		    m = (int)j.charAt( JUNCTION_ORDER_LENGTH*(i+1) + (2*i) ) - (int)'0';
// 		else { 
// 		    int x = 0;
// 		    m = 0;
// 		    boolean foundAll = false;
// 		    while( foundAll == false ) {
// 			int m2 = (int)j.charAt( j.indexOf("Junctions of order " + i + ": ") + JUNCTION_ORDER_LENGTH  + x ) - (int)'0';
// 			m = (m*10)+m2;
// 			c = j.charAt(j.indexOf("Junctions of order " + i + ": ") + JUNCTION_ORDER_LENGTH  + (x+1));
// 			if( (int)c == (int)NEWLINE )
// 			    foundAll = true;
// 			x++;
// 		    }
// 		}
// 		if(junctionString.length() == ORIGINAL_JUNCTIONSTRING_LENGTH) //change value of original length
// 		    n = (int)junctionString.charAt( JUNCTIONS_LENGTH + JUNCTION_ORDER_LENGTH*(i+1) + (2*i) ) - (int)'0';
// 		else {
// 		    int x = 0;
// 		    n = 0;
// 		    boolean foundAll = false;
// 		    try {
// 			while( foundAll == false ) {
// 			    int n2 = (int)junctionString.charAt(junctionString.indexOf("Junctions of order " + i + ": ") + JUNCTION_ORDER_LENGTH  + x ) - (int)'0';
// 			    n = (n*10)+n2;
// 			    c = junctionString.charAt(junctionString.indexOf("Junctions of order " + i + ": ") + JUNCTION_ORDER_LENGTH  + (x+1) );
// 			    if ( (int)c == (int)NEWLINE ) 
// 				foundAll = true;
// 			    x++;
// 			}
// 		    }
// 		    catch (StringIndexOutOfBoundsException e) {}
// 		}
// 	    temp += ("Junctions of order " + i + ": " + (n+m) + NEWLINE);
// 	    }
// 	    this.junctionString = temp;
// 	    NanoTiler.junctionString = this.junctionString;
// 	}

Removed2

// 	else {
//  	    for(int i = 0; i <= 5; i++) {
//  		if( k.length() == ORIGINAL_K_LENGTH )
// 		    m = (int)k.charAt( JUNCTION_ORDER_LENGTH*(i+1) + (2*i) ) - (int)'0';
// 		else { 
// 		    int x = 0;
// 		    m = 0;
// 		    boolean foundAll = false;
// 		    while( foundAll == false ) {
// 			int mx = (int)k.charAt( k.indexOf("Junctions of order " + i + ": ") + JUNCTION_ORDER_LENGTH  + x ) - (int)'0';
// 			m = (m*10)+mx;
// 			c = k.charAt(k.indexOf("Junctions of order " + i + ": ") + JUNCTION_ORDER_LENGTH  + (x+1)); 
// 			if( (int)c == (int)NEWLINE ) 
// 			    foundAll = true;
// 			x++;
// 		    }
// 		}
// 		if(kissingLoopString.length() == ORIGINAL_KISSINGLOOPSTRING_LENGTH)
// 		    n = (int)kissingLoopString.charAt( KISSINGLOOPS_LENGTH + JUNCTION_ORDER_LENGTH*(i+1) + (2*i) ) - (int)'0';
// 		else {
// 		    int x = 0;
// 		    n = 0;
// 		    boolean foundAll = false;
// 		    while( foundAll == false ) {
// 			int nx = (int)kissingLoopString.charAt( kissingLoopString.indexOf("Junctions of order " + i + ": ") + JUNCTION_ORDER_LENGTH + x ) - (int)'0';
// 			n = (n*10)+nx;
// 			c = kissingLoopString.charAt(kissingLoopString.indexOf("Junctions of order " + i + ": ") + JUNCTION_ORDER_LENGTH + (x+1) );
// 			if( (int)c == (int)NEWLINE )
// 			    foundAll = true;
// 			x++;
// 		    }
// 		}
// 		temp += ("Junctions of order " + i + ": " + (n+m) + NEWLINE);
// 	    }
// 	    this.kissingLoopString = temp;
// 	    NanoTiler.kissingLoopString = this.kissingLoopString;
// 	}


*/
