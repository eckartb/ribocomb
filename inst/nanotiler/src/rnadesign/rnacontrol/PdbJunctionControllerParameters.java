package rnadesign.rnacontrol;

import rnadesign.rnamodel.CorridorDescriptor;

public class PdbJunctionControllerParameters {

    // TODO : public variables should be avoided!

    public int branchDescriptorOffset = 2; // 0 changed by EB on Aug 29, 2007, back on Sep 6, 2007
    public int loopLengthSumMax = PdbJunctionController.LOOP_LENGTH_SUM_MAX;
    public CorridorDescriptor corridorDescriptor = new CorridorDescriptor(Object3DGraphControllerConstants.CORRIDOR_DEFAULT_RADIUS,
									  Object3DGraphControllerConstants.CORRIDOR_DEFAULT_START);
    
    public int orderMax = 5;

    public String nameFileName = "junctions.names";
    public String readDir = ".";

    //    public String writeName = "junction.ntl";
    public String writeDir = "."; //Windows compatible?
    public boolean noWriteFlag = false;
    public boolean randomMode = false;
    public boolean rnaMode = true;
    public int stemLengthMin;
    public boolean intHelixCheck=true;
    public boolean motifMode=false;

    /*    public PdbJunctionControllerParameters(int orderMax,
					   String nameFileName,
					   String readDir,
					   //String writeName,
					   String writeDir,
					   boolean noWriteFlag,
					   boolean rnaMode,
					   int branchDescriptorOffset,
					   int stemLengthMin,
					   CorridorDescriptor corridorDescriptor,
					   int loopLengthSumMax, boolean motifMode) {
	this.orderMax = orderMax;
	this.nameFileName = nameFileName;
	this.readDir = readDir;
	this.writeDir = writeDir;
	this.noWriteFlag = noWriteFlag;
	this.rnaMode = rnaMode;
	this.branchDescriptorOffset = branchDescriptorOffset;
	this.stemLengthMin = stemLengthMin;
	this.corridorDescriptor = corridorDescriptor;
	this.loopLengthSumMax = loopLengthSumMax;
	this.motifMode = motifMode;
    }
    */

    public PdbJunctionControllerParameters(int orderMax,
					   String nameFileName,
					   String readDir,
					   //String writeName,
					   String writeDir,
					   boolean noWriteFlag,
					   boolean rnaMode,
					   int branchDescriptorOffset,
					   int stemLengthMin,
					   CorridorDescriptor corridorDescriptor,
					   int loopLengthSumMax, boolean inthelixC, boolean motifMode) {
	this.orderMax = orderMax;
	this.nameFileName = nameFileName;
	this.readDir = readDir;
	this.writeDir = writeDir;
	this.noWriteFlag = noWriteFlag;
	this.rnaMode = rnaMode;
	this.branchDescriptorOffset = branchDescriptorOffset;
	this.stemLengthMin = stemLengthMin;
	this.corridorDescriptor = corridorDescriptor;
	this.loopLengthSumMax = loopLengthSumMax;
	this.intHelixCheck= inthelixC;
	this.motifMode = motifMode;
    }

}
