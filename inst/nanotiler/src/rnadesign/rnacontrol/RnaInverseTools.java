package rnadesign.rnacontrol;

public class RnaInverseTools {

    /** returns sequence strings scraped from RNAinverseGroup output */
    public static String[] getSequenceStrings(String[] inputLines) {
	int numEntries = inputLines.length / 6;
	String[] result = new String[numEntries];
	for (int i = 0; i < numEntries; i++) {
	    result[i] = inputLines[i*6];
	}
	return result;
    }

}
