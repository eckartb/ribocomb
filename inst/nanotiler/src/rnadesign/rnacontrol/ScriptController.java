package rnadesign.rnacontrol;

import java.util.*;
import tools3d.objects3d.*;
import tools3d.symmetry2.*;
import rnasecondary.*;
import rnadesign.rnamodel.predict3d.*;
import rnadesign.rnamodel.MutateScriptGenerator;
import java.util.logging.Logger;

import static rnadesign.rnacontrol.PackageConstants.*;

/** "Controller" class for generating scripts the execute a complex series of commands */
public class ScriptController {

    private static Logger log = Logger.getLogger(LOGFILE_DEFAULT);
    
    public static String generateGraphTraceScript(Object3D graphRoot, LinkSet links, String name,
						  List<SymEdgeConstraint> symConstraints, Properties params,
						  String algorithm, boolean kissingLoopMode) throws Object3DGraphControllerException {
	// Object3DSet vertices, 
	// ScriptGenerator generator = new TraceGraphScriptGenerator(vertices, links, name, symConstraints, params);
	ScriptGenerator generator = null;
	if ("helix".equals(algorithm)) {
	    generator = new TraceGraphScriptGenerator2(graphRoot, links, name, symConstraints, params);
	} else if ("db".equals(algorithm)) {
	    generator = new TraceGraphDBScriptGenerator(graphRoot, links, name, symConstraints, params, kissingLoopMode);
	} else {
	    throw new Object3DGraphControllerException("Unknown trance script generator algorithm: " + algorithm + " . Defined are: helix|db");
	}
	assert generator != null;
	return generator.generate();
    }

    public static String generateSecondaryTraceScript(SecondaryStructure structure, String fileName, String name,
						      Properties params,
						      String algorithm) throws Object3DGraphControllerException { 
	
	ScriptGenerator generator = new TraceSecondaryScriptGenerator(structure, fileName, name, params);
	String result = "echo INTERNAL ERROR";
	if ("helix".equals(algorithm)) {
	    result = generator.generate(); // "echo This is a mock-up script correspondint to reading a secondary structure and creating a script for generating a 3D structure.";
	} else {
	    throw new Object3DGraphControllerException("Unknown trace secondary structure script generator algorithm: " + algorithm + " . Defined are: helix");
	}
	return result;
    }
    
    public static String generateSecondaryInternalLoopTraceScript(SecondaryStructure structure, String fileName, String name, SubstructureDatabase data,
                  Properties params,
                  String algorithm) throws Object3DGraphControllerException { 
  
  ScriptGenerator generator = new TraceSecondaryMotifScriptGenerator(structure, fileName, name, data, params);
  String result = "echo INTERNAL ERROR";
  if ("helix".equals(algorithm)) {
      result = generator.generate(); // "echo This is a mock-up script correspondint to reading a secondary structure and creating a script for generating a 3D structure.";
  } else {
      throw new Object3DGraphControllerException("Unknown trace secondary structure script generator algorithm: " + algorithm + " . Defined are: helix");
  }
  return result;
    }
    
    public static String generatePRSwSecScript(SecondaryStructure structure, String fileName, String prsFileName, String name, TertiarySubstructureDatabase data,
                  Properties params,
                  String algorithm) throws Object3DGraphControllerException { 
  
                  ScriptGenerator generator = new PRSwSecScriptGenerator(structure, fileName, prsFileName, name, data, params);
                  String result = "echo INTERNAL ERROR";
                  if ("helix".equals(algorithm)) {
                      result = generator.generate(); 
                  } else {
                      throw new Object3DGraphControllerException("Unknown trace secondary structure script generator algorithm: " + algorithm + " . Defined are: helix");
                  }
                  return result;
    }

    public static String generateMutateScript(SecondaryStructure currentStructure, SecondaryStructure targetStructure, String[] sequenceNames, String name,
						      Properties params,
						      String algorithm) throws Object3DGraphControllerException { 
	
	ScriptGenerator generator = new MutateScriptGenerator(currentStructure, targetStructure, sequenceNames, name, params);
	String result = "echo INTERNAL ERROR";
	if ("helix".equals(algorithm)) {
	    result = generator.generate(); // "echo This is a mock-up script correspondint to reading a secondary structure and creating a script for generating a 3D structure.";
	} else {
	    throw new Object3DGraphControllerException("Unknown mutate script generator algorithm: " + algorithm + " . Defined are: helix|db");
	}
	return result;
    }
    
}
