package rnadesign.rnacontrol;

public interface ScriptGenerator {

    String generate();

}