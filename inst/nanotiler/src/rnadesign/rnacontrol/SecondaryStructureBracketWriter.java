package rnadesign.rnacontrol;

import java.util.logging.Logger;

import rnasecondary.Interaction;
import rnasecondary.RnaInteractionType;
import rnasecondary.SecondaryStructure;
import rnasecondary.SecondaryStructureWriter;
import sequence.Residue;
import sequence.Sequence;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DTools;

public class SecondaryStructureBracketWriter implements SecondaryStructureWriter {

    public static final String NEWLINE = System.getProperty("line.separator");
    public static final char NO_BASE_PAIR_CHAR = '.';

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    private boolean writeSequenceMode = true;

    public SecondaryStructureBracketWriter() { }

    public SecondaryStructureBracketWriter(boolean writeSequenceMode) {
	this.writeSequenceMode = writeSequenceMode;
    }

    private String writeSequence(SecondaryStructure structure, int seqId) {
	Sequence sequence = structure.getSequence(seqId);
	String result = sequence.sequenceString();
	// get secondary structure representation
	return result;
    }

    /** returns full sequence name, checks if part of Object3D hierarchy */
    public static String sequenceFullName(Sequence seq) {
	String result = seq.getName();
	if (seq.getParentObject() instanceof Object3D) {
	    Object3D parent = (Object3D)(seq.getParentObject());
	    result = Object3DTools.getFullName(parent) + "." + result;
	}
	return result;
    }

    /** returns full sequence name, checks if part of Object3D hierarchy */
    public static String sequenceParentName(Sequence seq) {
	String result = seq.getName();
	if (seq.getParentObject() instanceof Object3D) {
	    Object3D parent = (Object3D)(seq.getParentObject());
	    result = parent.getName() + "." + result;
	}
	return result;
    }

    /** returns index of sequence to which residue belongs
     * TODO : very slow implementation ! 
     * TODO : contains BUG !!! */
    private int findSequence(Residue res, SecondaryStructure structure) {
	assert false; // TODO: fix below code!!!
	return -1;
	
// 	assert res != null;
// 	assert structure != null;
// 	// Sequence seq = res.getSequence();
// 	assert seq != null;
// 	String seqName = sequenceParentName(seq);
// 	assert seqName != null;
// 	for (int i = 0; i < structure.getSequenceCount(); ++i) {
// 	    String seqNameOther = sequenceParentName(structure.getSequence(i));
// 	    // if complete name is identical:
// 	    log.fine("Comparing: " + seqName + " " 
// 			       + seqNameOther);
// 	    if (seqName.equals(seqNameOther)) {
// 		log.info("Identical names found! " + i);
// 		return i;
// 	    }
// 	}
// 	return -1;
    }

    private void addInteraction(Interaction interaction, 
				Interaction[][] interactionArray,
				SecondaryStructure structure) {
	Residue res1 = interaction.getResidue1();
	Residue res2 = interaction.getResidue2();
	assert res1 != null;
 	assert res2 != null;
	int seqId1 = findSequence(res1, structure);
	int seqId2 = findSequence(res2, structure);
	if ((seqId1 >= 0) && (seqId2 >= 0)) {
	    log.fine("Adding interaction(1): " + seqId1 + " " 
			       + res1.getPos() + " " + interaction);
	    log.fine("Adding interaction(2): " + seqId2 + " " 
			       + res2.getPos() + " " + interaction);
	    interactionArray[seqId1][res1.getPos()] = interaction;
	    interactionArray[seqId2][res2.getPos()] = interaction;
	}
	else {
	    log.warning("could not find: " 
			       + res1 + " " + res2);
// 	    log.fine("Respective sequences:");
// 	    log.fine("" + res1.getSequence());
// 	    log.fine(sequenceParentName(res1.getSequence()));
// 	    log.fine("" + res2.getSequence());
// 	    log.fine(sequenceParentName(res2.getSequence()));
	    for (int i = 0; i < structure.getSequenceCount(); ++i) {
		log.fine("" + (i+1) + " : " + structure.getSequence(i));
		log.fine(sequenceParentName(structure.getSequence(i)));
	    }
	}
    }

    /** returns 'B' when given 'A' etc */
    private char incChar(char c) {
	int n = (int)c;
	++n;
	return (char)n;
    }

    /** returns 'A' when given 'B' etc */
    private char decChar(char c) {
	int n = (int)c;
	--n;
	return (char)n;
    }

    private void generateCharArray(char[][] charArray, 
				   Interaction[][] interactionArray,
				   SecondaryStructure structure) {
	char interactionChar = 'A';
	interactionChar = decChar(interactionChar);
	for (int i = 0; i < interactionArray.length; ++i) {
	    for (int j = 0; j < interactionArray[i].length; ++j) {
		Interaction interaction = interactionArray[i][j];
		if (interaction == null) {
		    charArray[i][j] = NO_BASE_PAIR_CHAR;
		    continue;
		}
		Residue res1 = interaction.getResidue1();
		Residue res2 = interaction.getResidue2();
		int pos1 = res1.getPos();
		int pos2 = res2.getPos();
		if (interaction.isIntraSequence()) {
		    if (pos1 < pos2) {
			charArray[i][pos1] = '(';
			charArray[i][pos2] = ')';
		    }
		    else if (pos1 > pos2) {
			charArray[i][pos1] = ')';
			charArray[i][pos2] = '(';
		    }
		}
		else { // interaction between two different sequences
		    int seqId1 = findSequence(res1, structure);
		    int seqId2 = findSequence(res2, structure);
		    // check if new stem:
		    boolean newStemMode = true;
		    if ((pos1 > 0) && ((pos2+1) < charArray[seqId2].length)) {
			if (charArray[seqId1][pos1-1] == charArray[seqId2][pos2+1]) {
			    newStemMode= false;
			}
		    }
		    if (newStemMode) {
			interactionChar = incChar(interactionChar);
		    }
		    charArray[seqId1][pos1] = interactionChar;
		    charArray[seqId2][pos2] = interactionChar;
		}
	    }
	}
    }

    /** generates string from secondary structure */
    public String writeString(SecondaryStructure structure) {
	Interaction[][] interactionArray = new Interaction[structure.getSequenceCount()][0];
	char[][] charArray = new char[structure.getSequenceCount()][0];
	for (int i = 0; i < structure.getSequenceCount(); ++i) {
	    interactionArray[i] = new Interaction[structure.getSequence(i).size()];
	    charArray[i] = new char[structure.getSequence(i).size()];
	}
	for (int i = 0; i < structure.getInteractionCount(); ++i) {
	    Interaction interaction = structure.getInteraction(i);
	    if (interaction.getInteractionType().getSubTypeId() != RnaInteractionType.BACKBONE) {
		addInteraction(interaction, interactionArray, structure);
	    }
	}
	generateCharArray(charArray, interactionArray, structure);
	String result = "";
	for (int i = 0; i < structure.getSequenceCount(); ++i) {
	    if (writeSequenceMode) {
		result = result + writeSequence(structure, i) + NEWLINE; // TODO bad style
	    }
	    result = result + new String(charArray[i]) + NEWLINE;
	}
	return result;
    }

}
