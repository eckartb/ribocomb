package rnadesign.rnacontrol;

import java.util.*;
import controltools.ModelChangeListener;
import generaltools.ParsingException;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.LinkSet;
import sequence.*;
import sequence.DuplicateNameException;
import sequence.Sequence;
import sequence.SequenceContainer;
import sequence.UnevenAlignment;
import rnadesign.rnamodel.Residue3D;

public interface SequenceController extends SequenceContainer, ModelChangeListener {

    /** returns index of sequence , -1 if not found */
    public int getSequenceId(Sequence s);

    /** returns index of sequence (specified by name) , -1 if not found */
    public int getSequenceId(String name);

    /** returns sequence with given name, null if not found */
    public Sequence getSequence(String name);

    /** returns index of sequence , -1 if not found */
    // public int getSequenceIdFast(Sequence s);

    public String getSequenceName(int n);

    /** returns all sequences in form of "UnevenAlignment" class */
    public UnevenAlignment getUnevenAlignment() throws DuplicateNameException;

    public String[] getSequenceStrings();

    /** overwrites sequences of n'th strand. Careful: length has to match, sequence order has to be correct. */
    public void overwriteSequence(Sequence s, int n, Object3D nucleotideDB, double rmsLimit, LinkSet links);

    /** overwrites sequences of n'th strand. Careful: length has to match, sequence order has to be correct. */
    public void overwriteSequence(String s, int n, Object3D nucleotideDB, double rmsLimit, LinkSet links);

    /** renumbers all residues to count from 1 to n */
    public void renumberAllResidues();

    /** sets a certain property of all specified residues */
    public void setResiduesProperty(String residueNames, String key, String value) throws ParsingException;

    /** sets a status of all specified residues */
    public void setResiduesStatus(String residueNames, String status) throws ParsingException;

    /** returns list of residues for list given in the form: A:3-5;B:6,13-14 or special keyword "all" */
    public List<Residue3D> collectResidues(String residueNames) throws ParsingException;

}
