package rnadesign.rnacontrol;

import java.util.logging.Logger;
import java.util.ArrayList;
import controltools.*;
import java.io.*;
import tools3d.objects3d.*;
import rnadesign.rnamodel.*;
import rnasecondary.*;

import static rnadesign.rnacontrol.PackageConstants.*;

public class SimpleBasePairController extends SimpleLinkController implements BasePairController {

    private static Logger log = Logger.getLogger(LOGFILE_DEFAULT);
    private int minStemLength = 3;

    /** adds link */
    private void addNoChange(Link link) { 
	// 	Object3D o1 = link.getObj1();
	// 	Object3D o2 = link.getObj2();
	// 	if ((o1 instanceof Residue) && (o2 instanceof Residue)) {
	// 	    // TODO not clean, assumes basically Watson-Crick interaction
	// 	    residueInteractions.add(new SimpleInteraction((Residue)o1, 
	// 							  (Residue)o2, 
	// 							  defaultInteraction));
	// 	}
	if (link instanceof InteractionLink) {
	    Object3D obj1 = link.getObj1();
	    Object3D obj2 = link.getObj2();
	    if (! (obj1 instanceof Nucleotide3D)) {
		log.warning("Link has to connect nucleotides: " + obj1.getName());
		return;
	    }
	    if (! (obj2 instanceof Nucleotide3D)) {
		log.warning("Link has to connect nucleotides: " + obj2.getName());
		return;
	    }
	    residueInteractions.add(((InteractionLink)(link)).getInteraction());
	    log.finest("Added link is interaction link: " 
		     + link.getObj1().getName() + " " + link.getObj2().getName()); 
	    links.add(link); 	
	}
	else {
	    log.warning("Ignoring link that is not interaction link: " 
			  + link.getObj1().getName() + " " + link.getObj2().getName()); 
	    return;
	}

    }

    /** adds link */
    public void add(Link link) {
	addNoChange(link);
	fireModelChanged(new ModelChangeEvent(this));
    }
    
    /** reads PDB file, scans stems */
    public void read(InputStream is) throws IOException {
	Object3DFactory reader = new RnaPdbRnaviewReader();
	Object3DLinkSetBundle bundle = reader.readBundle(is);
	Object3D root = bundle.getObject3D();
	LinkSet tmpLinks = bundle.getLinks();
	Object3DLinkSetBundle stemBundle = StemTools.generateStemsFromLinks(root, tmpLinks, minStemLength);
	Object3D stemRoot = stemBundle.getObject3D();
	for (int i = 0; i < stemRoot.size(); ++i) {
	    RnaStem3D stem = (RnaStem3D)(stemRoot.getChild(i));
	    Stem stemInfo = stem.getStemInfo();
	    for (int j = 0; j < stem.getLength(); ++j) {
		Interaction interaction = stemInfo.get(j); // gets n'th interaction
		Object3D res1 = (Object3D)(interaction.getResidue1());
		Object3D res2 = (Object3D)(interaction.getResidue2());
		// only add, if all required atoms exist:
		if (NucleotideDBTools.hasTripodAtoms(res1) && NucleotideDBTools.hasTripodAtoms(res2)) {
		    InteractionLink interactionLink = new InteractionLinkImp(res1,
									     res2, interaction);
		    log.fine("Added base pair to SimpleBasePairController " + interactionLink);
		    add(interactionLink);
		}
		else {
		    log.info("Required atoms missing for residues: " 
			     + Object3DTools.getFullName(res1) + " " 
			     + Object3DTools.getFullName(res2));
		}
	    }
	}
    }

    public String toInfoString(int n) {
	InteractionLink link = (InteractionLink)(get(n));
	Interaction interaction = link.getInteraction();
	InteractionType interactionType = interaction.getInteractionType();
	Nucleotide3D nuc1 = (Nucleotide3D)(link.getObj1());
	Nucleotide3D nuc2 = (Nucleotide3D)(link.getObj2());
	String result = interactionType.toString() + " " + nuc1.getName() + " " + nuc1.getSymbol().getCharacter() + " "
	    + nuc2.getName() + " " + nuc2.getSymbol().getCharacter();
	return result;
    }

    public String toInfoString() {
	StringBuffer buf = new StringBuffer();
	buf.append("Number of defined base pairs: " + size() + NEWLINE);
	for (int i = 0; i < size(); ++i) {
	    buf.append("" + (i+1) + " " + toInfoString(i) + NEWLINE);
	}
	return buf.toString();
    }


}
