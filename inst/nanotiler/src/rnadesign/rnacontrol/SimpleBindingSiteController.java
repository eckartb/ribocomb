package rnadesign.rnacontrol;

import java.util.ArrayList;
import java.util.List;

import controltools.ModelChangeEvent;
import rnadesign.rnamodel.SequenceBindingSite;

public class SimpleBindingSiteController implements BindingSiteController {

    private List<SequenceBindingSite> bindingSites = new ArrayList<SequenceBindingSite>();

    public void addBindingSite(SequenceBindingSite site) {
	bindingSites.add(site);
    }

    public void clear() {
	bindingSites.clear();
    }

    public SequenceBindingSite getBindingSite(int n) {
	return (SequenceBindingSite)(bindingSites.get(n));
    }

    public int getBindingSiteCount() {
	return bindingSites.size();
    }


    /** returns index of sequence of n'th binding site, -1 if not found */
    public int getBindingId(SequenceBindingSite site) {
	for (int i = 0; i < bindingSites.size(); ++i) {
	    if (site.equals(getBindingSite(i))) {
		return i;
	    }
	}
	return -1;
    }

    /** TODO : update data when underlying objects have changed */
    public void modelChanged(ModelChangeEvent event) {

    }

}
