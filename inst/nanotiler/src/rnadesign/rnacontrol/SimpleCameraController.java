package rnadesign.rnacontrol;

import java.awt.geom.Point2D;
import java.text.NumberFormat;

import tools3d.Camera;
import tools3d.CameraTools;
import tools3d.Matrix3D;
import tools3d.Matrix3DTools;
import tools3d.SimpleCamera;
import tools3d.Vector3D;

public class SimpleCameraController implements CameraController {

    public static final String NEWLINE = System.getProperty("line.separator");

    /** camera position, orientation and zoom */
    private Camera camera;

    /** point of interest */
    private Vector3D interest;

    private static final Vector3D zFixed = new Vector3D(0.0, 0.0, 1.0);

    private NumberFormat nf = NumberFormat.getInstance();

    public SimpleCameraController() {
	camera = new SimpleCamera();
	interest = new Vector3D(0.0, 0.0, 0.0);
	nf.setMaximumFractionDigits(1);
	nf.setMinimumFractionDigits(1);
    }

    public SimpleCameraController(Camera camera) {
	this.camera = camera;
	interest = new Vector3D(0.0, 0.0, 0.0);
	nf.setMaximumFractionDigits(1);
	nf.setMinimumFractionDigits(1);
    }

    public void copy(CameraController other) {
	if (other instanceof SimpleCameraController) {
	    camera.copy(((SimpleCameraController)other).camera);
	    interest.copy(((SimpleCameraController)other).interest);
	    nf = ((SimpleCameraController)other).nf;
	}
    }

    public Camera getCamera() { return camera; }

    /** returns viewing direction of camera */
    public Vector3D getViewDirection() { return camera.getViewDirection(); }

    public Vector3D getInterest() {
	return interest;
    }

    public double getZoom() {
	return camera.getZoom();
    }

    /** moves camera in 2d coordinates */
    public void moveCamera(double x, double y) {
	camera.translate(x,y);
    }

    /** central projection method */
    public Point2D project(Vector3D point) { return camera.project(point); }

    public void rotateCamera(Vector3D rotVec, double angle) {
	Matrix3D orient = camera.getOrientation();
	// Vector3D rotVec = orient.getX();
	Matrix3D rot = Matrix3DTools.rotationMatrix(rotVec, angle);
	Vector3D pos = camera.getPosition();
	pos.copy(rot.multiply(pos)); // rotate position
	Vector3D x = orient.getXRow();
	Vector3D y = orient.getYRow();
	Vector3D z = orient.getZRow();
	x.copy(rot.multiply(x));
	y.copy(rot.multiply(y));
	z.copy(rot.multiply(z));
	// 	rot.setX(x);
	// 	rot.setY(x);
	// 	rot.setZ(x);
	// camera.setPosition(pos);
	camera.setOrientation(orient);
	// System.out.println("New camera orientation:");
	// System.out.println(orient.toString());
    }

    public void rotateCameraNorth(double angle) {
	Vector3D rotVec = camera.getOrientation().getXRow();
	rotateCamera(rotVec, angle);
    }

    public void rotateCameraWest(double angle) {
	Vector3D rotVec = camera.getOrientation().getYRow();
	rotateCamera(rotVec, angle);
    }

    public void setCamera(Camera camera) { this.camera = camera; }

    public void setInterest(Vector3D interest) {
	this.interest = interest;
    }

    /** sets pixel coordinates corresponding to positions projected to (0,0) */
    public void setOrigin2D(int x, int y) {
	camera.setOrigin2D(x, y);
    }

    /** tilt of camera counter-clockwise with respect to current projection screen 
     * @TODO NOT YET IMPLEMENTED 
     */
    public void tiltCamera(double angle) { }

    /** update camera position from phi, psi angles (in radians)
     * @TODO NOT YET IMPLEMENTED!
     */
    public void updateCameraFromAngles(double theta, double phi, double psi, double dist) {
	CameraTools.updateCameraFromAngles(camera, theta, phi, psi, dist, interest);
    }

    public void setZoom(double zoom) {
	camera.setZoom(zoom);
    }

}

/** returns text string with camera data */
/*
  public String[] getCameraText(int cols, int rows) {
  String[] result = new String[4];
  Vector3D pos = camera.getPosition();
  Matrix3D m = camera.getOrientation();
  result[0] = "p:  " + nf.format(pos.getX()) + ";" + nf.format(pos.getY()) + ";" + nf.format(pos.getZ()) + NEWLINE;
  result[1] = "dx: " + nf.format(m.getXX()) + ";" + nf.format(m.getXY()) + ";" + nf.format(m.getXZ());
  result[2] = "dy: " + nf.format(m.getYX()) + ";" + nf.format(m.getYY()) + ";" + nf.format(m.getYZ());
  result[3] = "dz: " + nf.format(m.getZX()) + ";" + nf.format(m.getZY()) + ";" + nf.format(m.getZZ());
  return result;
  }
*/

