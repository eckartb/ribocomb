package rnadesign.rnacontrol;

import java.io.DataInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import controltools.ModelChangeEvent;
import controltools.ModelChangeListener;
import generaltools.ConstraintDouble;
import generaltools.SimpleConstraintDouble;
import generaltools.StringTools;
import rnadesign.rnamodel.*;
import rnasecondary.*;
import tools3d.objects3d.*;

import static rnadesign.rnacontrol.PackageConstants.*;

public class SimpleLinkController implements LinkController {

    protected static Logger log = Logger.getLogger("NanoTiler_debug");

    protected List<Link> links = new ArrayList<Link>();

    private List<ModelChangeListener> listeners = new ArrayList<ModelChangeListener>();

    protected InteractionSet residueInteractions = new SimpleInteractionSet();

    public InteractionType defaultInteraction = new RnaInteractionType();

    /** default controller */
    public SimpleLinkController() { }

    public SimpleLinkController(LinkSet linkSet) {
	this();
	merge(linkSet); // add all links this this container
    }

    /** Method not used in this class. */
    public LinkSet cloneDeep() {
	assert false;
	return new SimpleLinkSet();
    }
    public LinkSet cloneDeep(Object3D root) {
	assert false;
	return new SimpleLinkSet();
    }

    /** adds a special link that indicates a helical constraint between two branch descriptors */
    public HelixConstraintLink addHelixConstraintLink(Object3D obj1, Object3D obj2, int basePairMin, int basePairMax,
						      double rms, int symId1, int symId2, String name) 
      throws Object3DGraphControllerException {
	if (! (obj1 instanceof BranchDescriptor3D)) {
	    throw new Object3DGraphControllerException("Object " + obj1.getName() + " has to be a BranchDescriptor3D !");
	}
	if (! (obj2 instanceof BranchDescriptor3D)) {
	    throw new Object3DGraphControllerException("Object " + obj2.getName() + " has to be a BranchDescriptor3D !");
	}
	HelixConstraintLink link = new SimpleHelixConstraintLink((BranchDescriptor3D)obj1, (BranchDescriptor3D)obj2, basePairMin,
								 basePairMax, rms);
	link.setName(name);
	link.setSymId1(symId1);
	link.setSymId2(symId2);
	add(link);
	return link;
    }

    /** adds a special link that indicates a helical constraint between two branch descriptors */
    public ConstraintLink addDistanceConstraintLink(Object3D obj1, Object3D obj2, double min, double max, int symId1, int symId2) {
	SimpleConstraintLink link = new SimpleConstraintLink(obj1, obj2, min, max, symId1, symId2);
	add(link);
	return link;
    }

    /** adds a special link that encodes angle between two vectors (obj1-obj2) and (obj4 - obj3) */
    public TorsionLink addTorsionConstraintLink(Object3D obj1, Object3D obj2, Object3D obj3, Object3D obj4,
						double min, double max) {
	SimpleTorsionLink link = new SimpleTorsionLink(obj1, obj2, obj3, obj4, min, max);
	add(link);
	return link;
    }

    /** adds a special link that encodes a junctions by list of corresponding 3' and 5' residues */
    public MultiConstraintLink addJunctionMultiConstraintLink(String name, 
							      List<Nucleotide3D> fivePrimeResidues, List<Nucleotide3D> threePrimeResidues,
							      ConstraintDouble constraint,
							      ConstraintDouble bridgableConstraint,
							      List<Integer> symIds) throws RnaModelException {
	if ((symIds != null) && (! ((symIds.size() == 0) || (symIds.size() == fivePrimeResidues.size())))) {
	    log.info("Number of symmetry operatotions and 5' residues for this junction: " + symIds.size() + " " + fivePrimeResidues.size());
	    throw new RnaModelException("Number of symmetry ids does not match number of branches of junction!");
	}
	MultiConstraintLink link = new JunctionMultiConstraintLink(fivePrimeResidues, threePrimeResidues, constraint,
								   bridgableConstraint);
	link.setName(name);
	if (symIds != null) {
	    for (int i = 0; i < symIds.size(); ++i) {
		link.setSymId(i, symIds.get(i));
	    }
	}
	add(link);
	return link;
    }

    /** adds a special link that encodes a junctions by list of corresponding helix descriptors */
    public JunctionDBConstraintLink addJunctionDBConstraintLink(String name, 
								List<BranchDescriptor3D> branches,
								StrandJunctionDB junctionDB) throws RnaModelException {
	log.info("Added junctionDBConstraint link of size " + branches.size());
	JunctionDBConstraintLink link = new JunctionDBConstraintLink(branches, junctionDB);
	link.setName(name);
	add(link);
	return link;
    }
    
    public MotifConstraintLink addMotifConstraintLink(Object3D obj1, Object3D obj2, int length, List<String> dbFileNames) {
	MotifConstraintLink link = new MotifConstraintLink(obj1, obj2, length, null, dbFileNames); //null is placeholder for sequence
	add(link);
	return link;
    }

    public SuperimposeConstraintLink addSuperimposeConstraintLink(Object3D obj1, Object3D obj2) {
	SuperimposeConstraintLink link = new SuperimposeConstraintLink(obj1, obj2); //null is placeholder for sequence
	add(link);
	return link;
    }
    
    public BlockCollisionConstraintLink addBlockCollisionConstraintLink(Object3D obj1, Object3D obj2) {
	BlockCollisionConstraintLink link = new BlockCollisionConstraintLink(obj1, obj2); //null is placeholder for sequence
	add(link);
	return link;
    }


    /** adds link */
    private void addNoChange(Link link) { 
	// 	Object3D o1 = link.getObj1();
	// 	Object3D o2 = link.getObj2();
	// 	if ((o1 instanceof Residue) && (o2 instanceof Residue)) {
	// 	    // TODO not clean, assumes basically Watson-Crick interaction
	// 	    residueInteractions.add(new SimpleInteraction((Residue)o1, 
	// 							  (Residue)o2, 
	// 							  defaultInteraction));
	// 	}
	if (link instanceof InteractionLink) {
	    residueInteractions.add(((InteractionLink)(link)).getInteraction());
	    log.finest("Added link is interaction link: " 
		     + link.getObj1().getName() + " " + link.getObj2().getName()); 
	}
	else {
	    log.finest("Added link is not interaction link: " 
		     + link.getObj1().getName() + " " + link.getObj2().getName()); 
	}
	links.add(link); 	
    }

    /** adds link */
    public void add(Link link) {
	addNoChange(link);
	fireModelChanged(new ModelChangeEvent(this));
    }

    /** adds links from link set, fires ChangeEvent only once */
    public void addLinks(LinkSet newLinks) { 
	if (newLinks == null) {
	    log.warning("LinkSet was null.");
	    return;
	}
	for (int i = 0; i < newLinks.size(); ++i) {
	    addNoChange(newLinks.get(i));
	}
	fireModelChanged(new ModelChangeEvent(this));
    }

    /** returns set of new links that were linking objects in origTree */
    public LinkSet cloneLinks(Object3D origTree, Object3D clonedTree) throws Object3DGraphControllerException {
	if (origTree.size() != clonedTree.size()) {
	    throw new Object3DGraphControllerException("Links between trees cannot be cloned, because they have a different number of links.");
	}
	LinkSet result = new SimpleLinkSet();
	Object3D root = Object3DTools.findRoot(clonedTree);
	String indexNameOrigRoot = Object3DTools.getFullIndexName(origTree);
	String indexNameClonedRoot = Object3DTools.getFullIndexName(clonedTree);
	// search for link that contains objects
	for (int i = 0; i < size(); ++i) {
	    Link link = get(i);
	    if (link instanceof MultiLink) {
		log.info("Warning: not cloning Multi-Link: " + link.toString());
		continue;
	    }
	    Object3D obj1 = link.getObj1();
	    Object3D obj2 = link.getObj2();
	    if (Object3DTools.isAncestor(origTree, obj1)
		&& Object3DTools.isAncestor(origTree, obj2)) {
		String indexName1 = Object3DTools.getFullIndexName(obj1);
		String indexName2 = Object3DTools.getFullIndexName(obj2);
		// remove leader sequence of path:
		String indexName1b = indexName1.replaceFirst(indexNameOrigRoot, indexNameClonedRoot);
		String indexName2b = indexName2.replaceFirst(indexNameOrigRoot, indexNameClonedRoot);
		// log.info("Replaced " + indexName1 + " to " + indexName1b + " and " 
		// + indexName2 + " to " + indexName2b);
		indexName1 = indexName1b;
		indexName2 = indexName2b;
		// use index name (like 3.6.7). Assumes that both cloned and original tree have same structure */
		Object3D obj1Clone = Object3DTools.findByFullName(root, indexName1);
		Object3D obj2Clone = Object3DTools.findByFullName(root, indexName2);
		if (obj1Clone == null) {
		    throw new Object3DGraphControllerException("SimpleLinkController: Could not find object 1 with name " + indexName1);
		}
		if (obj2Clone == null) {
		    throw new Object3DGraphControllerException("SimpleLinkController: Could not find object 2 with name " + indexName2);
		}
		// log.info("Original object names: " 
		// + Object3DTools.getFullName(obj1) + " now " + Object3DTools.getFullName(obj1Clone) + " and "
		//  + Object3DTools.getFullName(obj2) + " now " + Object3DTools.getFullName(obj2Clone) );

		Link clonedLink = (Link)(link.clone(obj1Clone, obj2Clone));
		result.add(clonedLink);
	    }
	}
	return result;
    }

    /** return minimum number of links connecting object
     *  zero indicates there is no connection 
     */
    public int getLinkOrder(Object3D obj) {
	int result = 0;
	for (int i = 0; i < size(); ++i) {
	    result += get(i).linkOrder(obj);
	}
	return result;
    }

    /** returns links connecting to object 1 */
    public LinkSet findLinks(Object3D obj) {
	LinkSet result = new SimpleLinkSet();
	for (int i = 0; i < size(); ++i) {
	    if (get(i).linkOrder(obj) > 0) {
		result.add(get(i));
	    }
	}
	return result;
    }

    /** returns links with specified name */
    public LinkSet findLinks(String name) {
	LinkSet result = new SimpleLinkSet();
	if (name == null) {
	    return result;
	}
	for (int i = 0; i < size(); ++i) {
	    if (name.equals(get(i).getName())) {
		result.add(get(i));
	    }
	}
	return result;
    }

    /** returns rank of link that connects to a certain object (zero if first link connecting to that object, one if second and so forth) */
    public int getLinkRank(Object3D obj, Link link) {
	LinkSet links = findLinks(obj);
	for (int i = 0; i < links.size(); ++i) {
	    if (links.get(i) == link) {
		return i;
	    }
	}
	return -1; // not found
    }

    /** adds links from link set, fires ChangeEvent only once.
     * Duplicate name with addLinks, decide on one. */
    public void merge(LinkSet newLinks) {
	addLinks(newLinks);
    }

    public void addLink(Object3D o1, Object3D o2) {
	add(new SimpleLink(o1, o2));
    }

    public void addModelChangeListener(ModelChangeListener listener) {
	listeners.add((ModelChangeListener)listener);
    }

    public InteractionSet getResidueInteractions() {
	return residueInteractions;
    }

    /** Returns all hydrogen bond interactions */
    public InteractionSet getHydrogenBondInteractions() {
	log.fine("Warning: calling slow method: collecting hydrogen bond interactions, starting with " 
		 + residueInteractions.size() + " interactions.");
	InteractionSet result = new SimpleInteractionSet();
	for (int i = 0; i < residueInteractions.size(); ++i) {
	    Interaction interaction = residueInteractions.get(i);
	    InteractionType interactionType = interaction.getInteractionType();
	    if (interactionType instanceof RnaInteractionType) {
		if (interactionType.getSubTypeId() != RnaInteractionType.BACKBONE) {
		    result.add(interaction);
		}
	    } else if (interactionType instanceof BasePairInteractionType) {
		result.add(interaction);
	    }
	}
	return result;
    }

    public LinkSet findBluntBasePairs() {
	InteractionSet interactions = getHydrogenBondInteractions();
	LinkSet result = new SimpleLinkSet();
	log.info("Starting findBluntBasePairs with " + size() + " base pairs.");
	for (int i = 0; i < interactions.size(); ++i) {
	    Interaction bp = interactions.get(i);
	    sequence.Residue r1 = bp.getResidue1();
	    sequence.Residue r2 = bp.getResidue2();
	    assert r1 != r2;
	    if ((r1 instanceof Residue3D) && (r2 instanceof Residue3D)) {
		Residue3D res1 = (Residue3D)(r1);
		Residue3D res2 = (Residue3D)(r2);
		assert res1 != res2;
		if ((res1.getParent() instanceof BioPolymer) && (res2.getParent() instanceof BioPolymer)) {
		    if (res1.isTerminal() && res2.isTerminal()) {
			BioPolymer poly1 = (BioPolymer)res1.getParent();
			BioPolymer poly2 = (BioPolymer)res2.getParent();
			int id1 = res1.getPos();
			int id2 = res2.getPos();
			int len1 = poly1.getResidueCount();
			int len2 = poly2.getResidueCount();
			System.out.println("Base pair " + id1 + " " + id2 + " " + len1 + " " + len2);
			if ((res1.isFirst() && res2.isLast()) || (res2.isFirst() && res1.isLast())) {
			    System.out.println("Is blunt end!");
			    assert res1.isTerminal();
			    assert res2.isTerminal();
			    result.add(new InteractionLinkImp(res1, res2, bp));
			}
		    }
		}
	    }
	}
	// check post-conditions:
	for (int i = 0; i < result.size(); ++i) {
	    Link link1 = result.get(i);
	    assert link1.getObj1() instanceof Nucleotide3D;
	    assert link1.getObj2() instanceof Nucleotide3D;
	    Nucleotide3D bp1_1 = (Nucleotide3D)link1.getObj1();
	    Nucleotide3D bp1_2 = (Nucleotide3D)link1.getObj2();
	    assert bp1_1.isTerminal();
	    assert bp1_2.isTerminal();
	}
	return result;
    }

    /** returns link number between objects */
    public int getLinkNumber(Object3D obj1, Object3D obj2) {
	int result = 0;
	for (int i = 0; i < size(); ++i) {
	    if (get(i).isLinked(obj1, obj2)) {
		++result;
	    }
	}
	return result;
    }

    public void removeAndAdd(Object3D objectTree, Object3DSet vertexSet) {
	assert (false);
    }

    /*
    public void removeModelChangeListener(ModelChangeListener listener) {
	listeners.remove(listener);
    }
    */

    /** replaces oldobject reference with reference to new object */
    public void replaceObjectInLinks(Object3D oldObject, Object3D newObject) {
	for (int i = 0; i < this.size(); ++i) {
	    get(i).replaceObjectInLink(oldObject, newObject);
	}
    }

    public void clear() { 
	links.clear(); 
	residueInteractions.clear(); // clear all interactions 
	fireModelChanged(new ModelChangeEvent(this));
    }

    /** returns n'th link */
    public Link get(int n) throws IndexOutOfBoundsException { return (Link)(links.get(n)); }

    /** returns number of links */
    public int size() { return links.size(); }

    /** returns true if link found in set (independent of object order */
    public boolean contains(Link link) {
	for (int i = 0; i < links.size(); ++i) {
	    if (link.equals(get(i))) {
		return true;
	    }
	}
	return false;
    }

    /** returns link corresponding objects 1 and 2, null if those objects are not linked */
    public Link find(Object3D obj1, Object3D obj2) {
	Link testLink = new SimpleLink(obj1, obj2);
	for (int i = 0; i < links.size(); ++i) {
	    if (testLink.equals(get(i))) {
		return get(i);
	    }
	}
	return null;
    }

    /** returns link corresponding objects 1 and 2, null if those objects are not linked */
    public LinkSet findLinks(Object3D obj1, Object3D obj2) {
	LinkSet result = new SimpleLinkSet();
	for (int i = 0; i < size(); ++i) {
	    if (get(i).isLinked(obj1, obj2)) {
		result.add(get(i));
	    }
	}

	return result;
    }


    /** removes link */
    public void remove(Link link) { 
	if (link instanceof InteractionLink) {
	    residueInteractions.remove(((InteractionLink)(link)).getInteraction()); 
	}
	links.remove(link); 
	fireModelChanged(new ModelChangeEvent(this)); 
    }

    /** removes set of link */
    public void remove(LinkSet otherLinks) { 
	for (int i = 0; i < otherLinks.size(); ++i) {
	    Link link = otherLinks.get(i);
	    if (link instanceof InteractionLink) {
		residueInteractions.remove(((InteractionLink)(link)).getInteraction()); 
	    }
	    links.remove(link); 
	}
	fireModelChanged(new ModelChangeEvent(this)); 
    }

    /** removes link given a specified link number. */
    public void remove(int linkId) throws Object3DGraphControllerException { 
	if ((linkId < 0) || (linkId >= size())) {
	    throw new Object3DGraphControllerException("Undefined link id: " + linkId);
	}
	Link link = get(linkId);
	remove(link);
    }

    /** removes link given a specified link name.
     * @param String linkName name of link to be removed
     * @returns number of removed links.
    */
    public int remove(String linkName) { 
        assert (linkName != null) && (linkName.length() > 0);
	LinkSet rmLinks = findLinks(linkName);
        assert rmLinks != null;
	for (int i = 0; i < rmLinks.size(); ++i) {
	    remove(rmLinks.get(i));
	}
	return rmLinks.size();
    }

    /** remove links that cannot be found in object tree. TODO : slow method! */
    public void removeBadLinks(Object3D tree) {
	if (tree == null) {
	    links = new ArrayList<Link>(); // remove all links
	    return;
	}
	Object3DCollector collector = Object3DCollector.collectAll(tree); // transform tree to set
	for (int i = size()-1; i >= 0; --i) {
	    Link link = get(i);
	    if (! ( (collector.contains(link.getObj1())) && (collector.contains(link.getObj2())) ) ) {
		links.remove(link);
	    }
	}
	// TODO : Interactions are currently not removed from InteractionSet
	fireModelChanged(new ModelChangeEvent(this));
    }

    /** returns output string */
    public String toString() {
	String result = "(LinkSet " + size() + " ";
	for (int i = 0; i < size(); ++i) {
	    result = result + get(i).toString() + " ";
	}
	result = result + ")";
	return result;
    }

    private String generatePrettyStringLine(int n) {
	String result = "Link " + Object3DTools.getFullName(get(n).getObj1()) + " " + Object3DTools.getFullName(get(n).getObj2());
	if (get(n) instanceof InteractionLink) {
	    InteractionLink link = (InteractionLink)get(n);
	    result = result + " " + link.getInteraction().getInteractionType().toString();
	}
	else if (get(n) instanceof HelixConstraintLink) {
	    HelixConstraintLink hcl = (HelixConstraintLink)(get(n));
	    result = result + " helix-constraint bpmin=" + hcl.getBasePairMin() + " bpmax=" 
		+ hcl.getBasePairMax() + " rms=" + hcl.getRms() + " name="+ hcl.getName();
	    
	}
	else if (get(n) instanceof AngleLink) {
	    result = result + " " + get(n).toString();
	}
	else if (get(n) instanceof TorsionLink) {
	    result = result + " " + get(n).toString();
	}
	else if (get(n) instanceof JunctionMultiConstraintLink) {
	    result = get(n).toString();
	} else {
	    result = result + " weird: " + get(n).toString();
	}
	return result;
    }

    public String toPrettyString() {
	StringBuffer buf = new StringBuffer();
	buf.append("LinkCount " + size() + NEWLINE);
	for (int i = 0; i < size(); ++i) {
	    buf.append("" + (i+1) + " " + generatePrettyStringLine(i) + NEWLINE);
	}
	return buf.toString();
    }
    
    
    public String toPrettyString(String linkType) {
	log.info("Starting toPrettyString(" + linkType + ")");
	if ((linkType == null) || (linkType.length() == 0)) {
	    return toPrettyString();
	}
	RnaInteractionType interactionType = null;
	if (linkType.equals("bp")) {
	    interactionType = new RnaInteractionType(RnaInteractionType.WATSON_CRICK);
	}
	else if (linkType.equals("backbone")) {
	    interactionType = new RnaInteractionType(RnaInteractionType.BACKBONE);
	}
	else {
	    return toPrettyString();
	}
	StringBuffer buf = new StringBuffer();
	buf.append("LinkCount " + size() + NEWLINE);
	for (int i = 0; i < size(); ++i) {
	    Link link = get(i);
	    if (link instanceof InteractionLink) {
		InteractionLink intLink = (InteractionLink)link;
		InteractionType otherType = intLink.getInteraction().getInteractionType();
		if (interactionType.equals(otherType)) {
		    buf.append("" + (i+1) + " " + generatePrettyStringLine(i) + NEWLINE);
		}
	    }
	}
	log.info("Finished toPrettyString(" + linkType + ")");
	return buf.toString();
    }

    /** reads links, given information about exisiting trees 
     * (used for converting object names into object references)
     */
	public void read(InputStream is, Object3D tree) throws Object3DIOException {
	log.fine("Starting SimpleLinkController: read");
	StringTools st = new StringTools();
 	String expected = "(LinkSet";
	DataInputStream dis = new DataInputStream(is);
 	String word = st.readWord(dis);
 	if (!word.equals(expected)) {
 	    throw new Object3DIOException("LinkSet.read: " + expected + " excepted instead of " + word);
 	}
 	word = st.readWord(dis);
 	int numLinks = 0;
 	try {
 	    numLinks = Integer.parseInt(word);
 	}
 	catch (NumberFormatException e) {
 	    throw new Object3DIOException("createObject3DGraph: Could not parse number of children: " + word);	
 	}
 	// read individual links
 	for (int i = 0; i < numLinks; ++i) {
	    log.fine("Current size: " + size()
			   + " reading: " + i);
 	    Link link = new SimpleLink();
	    link.read(dis, tree);
	    add(link);
 	}
 	expected = ")"; // end of reading links
 	word = st.readWord(dis);
 	if (!word.equals(expected)) {
 	    throw new Object3DIOException("createObject3D: " + expected 
 					  + " excepted instead of " + word);
 	}
	fireModelChanged(new ModelChangeEvent(this));
    }

    public void fireModelChanged(ModelChangeEvent event) {
	for (int i = 0; i < listeners.size(); ++i) {
	    ModelChangeListener listener = (ModelChangeListener)(listeners.get(i));
	    listener.modelChanged(event);
	}
    }

}
