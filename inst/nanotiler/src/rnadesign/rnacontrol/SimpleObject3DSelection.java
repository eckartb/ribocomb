package rnadesign.rnacontrol;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/** this class represents a selected subset used by GUI. It uses Object3DHandle
 * so that the GUI only interacts with classes from the controller 
 * and not from the underlying data model.
 */
public class SimpleObject3DSelection implements Object3DSelection {

    private Set<Object3DHandle> set = new HashSet<Object3DHandle>();
    
    // TODO: check for consistency
    public void add(Object3DHandle obj) {
	set.add(obj);
    }
    
    /** clears container */
    public void clear() {
	set.clear();
    }

    /** returns iterator to object3d handles */
    public Iterator iterator() {
	return set.iterator();
    }

    // TODO: check for consistency
    public void remove(Object3DHandle obj) {
	set.remove(obj);
    }
    
    /** returns size of container */
    public int size() {
	return set.size();
    }

}

