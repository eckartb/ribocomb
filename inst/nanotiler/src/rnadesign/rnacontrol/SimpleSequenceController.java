package rnadesign.rnacontrol;

import java.util.*;
import java.util.logging.*;
import controltools.*;
import sequence.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import controltools.ModelChangeEvent;
import controltools.ModelChangeListener;
import generaltools.ParsingException;
import generaltools.StringTools;
import rnadesign.rnamodel.FittingException;
import rnadesign.rnamodel.InteractionLink;
import rnadesign.rnamodel.Nucleotide3D;
import rnadesign.rnamodel.Residue3D;
import rnadesign.rnamodel.RnaStrand;
import rnadesign.rnamodel.RnaStrandTools;
import rnadesign.rnamodel.RnaModelException;
import tools3d.objects3d.*;
import sequence.DuplicateNameException;
import sequence.Sequence;
import sequence.SimpleUnevenAlignment;
import sequence.UnevenAlignment;

public class SimpleSequenceController implements SequenceController, ModelChangeListener {

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    private List<Sequence> sequences = new ArrayList<Sequence>();

    public void addSequence(Sequence sequence) {
	sequences.add(sequence);
    }

    public void clear() {
	sequences.clear();
    }

//     /** returns index of sequence , -1 if not found */
//     public int getSequenceId(Sequence s) {
// 	return getSequenceIdFast(s);
// 	if (s == null) {
// 	    return -1;
// 	}
// 	for (int i = 0; i < sequences.size(); ++i) {
// 	    if (s.equals(getSequence(i))) {
// 		return i;
// 	    }
// 	}
// 	return -1;
// }

    /** returns index of sequence , -1 if not found */
    public int getSequenceId(Sequence s) {
	if (s == null) {
	    return -1;
	}
	for (int i = 0; i < sequences.size(); ++i) {
	    if (s.getName().equals(getSequence(i).getName())) {
		return i;
	    }
	}
	return -1;
    }

    /** returns last token. TODO: StringTokenizer is deprecated, but use because I could not figure out how to use split using the "." character as delimiter . */
    public String getLastToken(StringTokenizer tokens) {
	String lastToken = null;
	while (tokens.hasMoreTokens()) {
	    lastToken = tokens.nextToken();
	}
	return lastToken;
    }

    /** returns index of sequence , -1 if not found */
    public int getSequenceId(String name) {
	if (name == null) {
	    return -1;
	}
	StringTokenizer tokens = new StringTokenizer(name, ".");
	if (tokens.countTokens() > 1) {
	    name = getLastToken(tokens);
	}
// 	String[] words = name.split("\Q.\E");
// 	if (words.length > 1) {
// 	    name = words[words.length-1]; // user gave complete name instead of just strand name
// 	    log.info("Using last component of complete path name of sequence: " + name);
// 	}
// 	else {
	log.fine("Using effective Sequence name: " + name);
	// 	}
	for (int i = 0; i < sequences.size(); ++i) {
	    if (name.equals(getSequence(i).getName())) {
		return i;
	    }
	}
	return -1;
    }


    public Sequence getSequence(int n) {
	return (Sequence)(sequences.get(n));
    }

    public Sequence get(int n) {
	assert(n < size());
	return getSequence(n);
    }

    /** returns sequence with given name, null if not found */
    public Sequence getSequence(String name) {
	if (name == null) {
	    return null;
	}
	for (int i = 0; i < getSequenceCount(); ++i) {
	    if (getSequence(i).getName().equals(name)) {
		return getSequence(i);
	    }
	}
	return null;
    }
    
    public String getSequenceName(int n) {
	return getSequence(n).getName();
    }

    public int getSequenceCount() {
	return sequences.size();
    }
    
    public int size() { 
	return getSequenceCount();
    }

    /** returns all sequences as strings */
    public String[] getSequenceStrings() {
	String[] result = new String[getSequenceCount()];
	for (int i = 0; i < getSequenceCount(); ++i) {
	    result[i] = getSequence(i).sequenceString();
	}
	return result;
    }

    /** update data depending on changed model */
    public void modelChanged(ModelChangeEvent e) {
	// should there be code???
    }

    /** returns all sequences in form of "UnevenAlignment" class */
    public UnevenAlignment getUnevenAlignment() throws DuplicateNameException {
	UnevenAlignment result = new SimpleUnevenAlignment();
	for (int i = 0; i < getSequenceCount(); ++i) {
	    result.addSequence(getSequence(i));
	}
	return result;
    }

    private boolean linkConsistencyCheck(InteractionLink link) {
	return link.isValid();

    }

    private boolean linkConsistencyCheck(LinkSet links) {
	for (int i = 0; i < links.size(); ++i) {
	    Link link = links.get(i);
	    if (link instanceof InteractionLink) {
		if (! linkConsistencyCheck((InteractionLink)link)) {
		    return false;
		}
	    }
	}
	return true;
    }

    /** overwrites sequences of n'th strand. Careful: length has to match, sequence order has to be correct. */
    public void overwriteSequence(Sequence s, int n, Object3D nucleotideDB, double rmsLimit,
				  LinkSet links) {	
	log.fine("Starting SimpleSequenceController.overwriteSequence " + n);
	Sequence seq = getSequence(n);
	if (seq instanceof RnaStrand) {
	    RnaStrand strand = (RnaStrand)seq;
	    assert strand.getResidueCount() == s.size(); // TODO: better: use getResidueCount
	    for (int i = 0; i < strand.size(); ++i) {
		if (!strand.getResidue(i).getSymbol().equals(s.getResidue(i).getSymbol())) {
		    log.fine("Mutating residue " + (i+1) + " of strand " + strand.getName());
		    try {
			RnaStrandTools.mutateResidue(strand, s.getResidue(i).getSymbol(), i, nucleotideDB,
						     rmsLimit, links);
			// set status to "optimized", probably was status "ADHOC"
			strand.getResidue(i).setProperty(SequenceStatus.name, SequenceStatus.optimized);
			assert strand.getResidue(i).getSymbol().equals(s.getResidue(i).getSymbol());
		    }
		    catch (RnaModelException rme) {
			log.warning("Cannot mutate residue " + (i+1) + " " + rme.getMessage());
		    }
		    catch (FittingException rme) {
			log.warning("Fitting exception while mutating residue " + (i+1) + " : " + rme.getMessage());
		    }
		}
	    }
	    assert linkConsistencyCheck(links);
	}
	else {
	    log.warning("Internal error: sequence n is not of type RnaStrand!");
	}
	
    }

    /** overwrites sequences of n'th strand. Careful: length has to match, sequence order has to be correct. */
    public void overwriteSequence(String s, int n, Object3D nucleotideDB, double rmsLimit,
				  LinkSet links) {	
	log.fine("Starting SimpleSequenceController.overwriteSequence " + (n+1));
	Sequence seq = getSequence(n);
	if (seq instanceof RnaStrand) {
	    RnaStrand strand = (RnaStrand)seq;
	    assert strand.getResidueCount() == s.length(); // TODO: better: use getResidueCount
	    for (int i = 0; i < strand.size(); ++i) {
		if (strand.getResidue(i).getSymbol().getCharacter() != s.charAt(i)) {
		    log.fine("Mutating residue " + (i+1) + " of strand " + strand.getName());
		    try {
			LetterSymbol newSymbol = new SimpleLetterSymbol(s.charAt(i), DnaTools.AMBIGUOUS_RNA_ALPHABET);
			RnaStrandTools.mutateResidue(strand, newSymbol, i, nucleotideDB,
						     rmsLimit, links);
			// set status to "optimized", probably was status "ADHOC"
			strand.getResidue(i).setProperty(SequenceStatus.name, SequenceStatus.optimized);
			assert strand.getResidue(i).getSymbol().equals(newSymbol);
		    }
		    catch (RnaModelException rme) {
			log.warning("Cannot mutate residue " + (i+1) + " " + rme.getMessage());
		    }
		    catch (FittingException rme) {
			log.warning("Fitting exception while mutating residue " + (i+1) + " : " + rme.getMessage());
		    }
		    catch (UnknownSymbolException use) {
			log.warning("Unknown sequence symbol detected at position " + (i+1)
				    + " : " + s.charAt(i) + " " + s);
		    }
		}
	    }
	    assert linkConsistencyCheck(links);
	}
	else {
	    log.warning("Internal error: sequence n is not of type RnaStrand!");
	}
	
    }

    /** returns list of residues for list given in the form: A:3-5 */
    private List<Residue3D> collectChainResidues(String residueNames) throws ParsingException {
	String[] words = residueNames.split(":");
	if (words.length < 1) {
	    throw new ParsingException("Expected residue specifier in the format chain:numbers . Example: B:3-5,8,19 or C:");
	}
	List<Residue3D> residues = new ArrayList<Residue3D>();
	String chainName = words[0];
	Sequence sequence = getSequence(chainName);
	if (sequence == null) {
	    throw new ParsingException("Could not find sequence with name: " + chainName);
	}
	if (words.length == 1) { // no residues specified, only chain name:  add all residues of seqence
	    for (int i = 0; i < sequence.size(); ++i) {
		assert (sequence.getResidue(i) instanceof Residue3D);
		residues.add((Residue3D)(sequence.getResidue(i)));
	    }
	}
	else if (words.length == 2) {
	    List<Integer> ids = StringTools.parseNumbers(words[1]);
	    for (Integer idObj : ids) {
		int id = idObj.intValue()-1;
		if ((id < 0) || (id >= sequence.size())) {
		    throw new ParsingException("Could not find residue with id " +  idObj + " in sequence " + sequence.getName());
		}
		assert (sequence.getResidue(id) instanceof Residue3D);
		residues.add((Residue3D)(sequence.getResidue(id))); // convert to internal counting
	    }
	}
	else {
	    throw new ParsingException("Internal error! Expected residue specifier in the format chain:numbers . Example: B:3-5,8,19 or C:");
	}
	return residues;
    }


    /** returns list of residues for list given in the form: A:3-5;B:6,13-14 */
    private List<Residue3D> collectAllResidues() {
	List<Residue3D> residues = new ArrayList<Residue3D>();
	for (int i = 0; i < getSequenceCount(); ++i) {
	    Sequence sequence = getSequence(i);
	    for (int j = 0; j < sequence.size(); ++j) {
		assert(sequence.getResidue(j) instanceof Residue3D);
		residues.add((Residue3D)(sequence.getResidue(j)));
	    }
	}
	return residues;
    }

    /** renumbers all residues to count from 1 to n */
    private void renumberResidues(Sequence sequence) {
	for (int i = 0; i < sequence.size(); ++i) {
	    Nucleotide3D nuc = (Nucleotide3D)(sequence.getResidue(i));
	    nuc.setResidueNumberInName(i+1); // set to this name
	}
    }

    /** renumbers all residues to count from 1 to n */
    public void renumberAllResidues() {
	for (int i = 0; i < getSequenceCount(); ++i) {
	    Sequence sequence = getSequence(i);
	    renumberResidues(sequence);
	}
    }

    /** returns list of residues for list given in the form: A:3-5;B:6,13-14 or special keyword "all" */
    public List<Residue3D> collectResidues(String residueNames) throws ParsingException {
	List<Residue3D> residues = new ArrayList<Residue3D>();
	if ((residueNames == null) || (residueNames.equals(""))) {
	    return residues;
	}
	if (residueNames.equals("all")) { // special case
	    return collectAllResidues();
	}
	String[] words = residueNames.split(";");
	for (int i = 0; i < words.length; ++i) {
	    residues.addAll(collectChainResidues(words[i]));
	}
	return residues;
    }

    /** sets residue property for list given in the form: A:3-5;B:6,13-14 or special keyword "all" */
    public void setResiduesProperty(String residueNames, String key, String value) throws ParsingException {
	List<Residue3D> residues = collectResidues(residueNames);
	for (Residue3D residue : residues) {
	    log.info("Setting property " + key + " of " + residue.infoString() 
		     + " to: " + value);
	    residue.setProperty(key, value);
	}
    }

    /** sets residue status for list given in the form: A:3-5;B:6,13-14 or special keyword "all" */
    public void setResiduesStatus(String residueNames, String status) throws ParsingException {
	setResiduesProperty(residueNames, SequenceStatus.name, status);
    }

}
