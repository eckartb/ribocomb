package rnadesign.rnacontrol;

import java.util.Properties;
import java.util.logging.Logger;

import chemistrytools.ChemicalElement;
import rnadesign.rnamodel.AminoAcid3D;
import rnadesign.rnamodel.BranchDescriptor3D;
import rnadesign.rnamodel.InteractionLink;
import rnadesign.rnamodel.Nucleotide3D;
import rnadesign.rnamodel.Protein3DTools;
import rnadesign.rnamodel.ProteinStrand;
import rnadesign.rnamodel.Rna3DTools;
import rnadesign.rnamodel.RnaPhysicalProperties;
import rnadesign.rnamodel.RnaStrand;
import rnadesign.rnamodel.StrandJunction3D;
import rnadesign.rnamodel.Atom3D;
import rnasecondary.Interaction;
import rnasecondary.InteractionType;
import rnasecondary.RnaInteractionType;
import tools3d.CharacterShape;
import tools3d.Cube;
import tools3d.Cylinder;
import tools3d.LineShape;
import tools3d.PointShape;
import tools3d.Shape3D;
import tools3d.Shape3DSet;
import tools3d.SimpleShape3DSet;
import tools3d.Vector3D;
import tools3d.objects3d.FurthestFinder;
import tools3d.objects3d.Link;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DSet;
import tools3d.objects3d.Shape3DSetFactory;

/** @TODO implement factory object!!!
 */
public class SimpleShapeSetFactory implements Shape3DSetFactory {

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    private boolean addDebugInfoMode = false;

    private int depthMax = -1;

    public static final double SPHERE_RADIUS_MIN = 1.0;

    private void adjustShapeProperties(Shape3D shape, Object3D object, 
				       Properties properties) {
	shape.setSelected(object.isSelected());
	shape.setProperties(properties);
    }

    /** returns id of strand. TODO : not yet implemented! */
    private int getStrandId(RnaStrand strand) {
	Object3D root = strand.getParent();
	if (root == null) {
	    return 0;
	}
	String name = strand.getName();
	Object3DSet strands = Rna3DTools.collectRnaStrand(root);
	for (int i = 0; i < strands.size(); ++i) {
	    RnaStrand otherStrand = (RnaStrand)(strands.get(i));
	    String otherName = otherStrand.getName();
	    if (otherName.equals(name)) {
		return i;
	    }
	}
	return strands.size();
    }

    /** returns id of strand. */
    private int getStrandId(ProteinStrand strand) {
	Object3D root = strand.getParent();
	if (root == null) {
	    return 0;
	}
	String name = strand.getName();
	Object3DSet strands = Protein3DTools.collectProteinStrands(root);
	for (int i = 0; i < strands.size(); ++i) {
	    ProteinStrand otherStrand = (ProteinStrand)(strands.get(i));
	    String otherName = otherStrand.getName();
	    if (otherName.equals(name)) {
		return i;
	    }
	}
	return strands.size();
    }

    /** returns shape set for single object, ignoring child nodes */
    public Shape3DSet createSingleObjectShapeSet(Object3D obj) {
	Shape3DSet shapeSet = new SimpleShape3DSet();
	if (obj == null) {
	    log.warning("called with null object!");
	    return shapeSet;
	}
	// determine radius to furthest object:
	Object3D furthestObject = FurthestFinder.find(obj);
	if (furthestObject == null) {
	    log.warning("no furthest object found!");
	    return shapeSet;
	}
	double radius = obj.distance(furthestObject);
	if (radius < SPHERE_RADIUS_MIN) {
	    radius = SPHERE_RADIUS_MIN;
	}

	Shape3D shape = null;
	Properties objectProperties = new Properties();
	// stores depth of current object:
	objectProperties.setProperty("depth", ""+obj.getDepth());
	objectProperties.setProperty("class_name", "" + obj.getClassName());
	int siblingId = 1;
	if (obj.getParent() != null) {
	    objectProperties.setProperty("sibling_id", ""+obj.getSiblingId());
	    objectProperties.setProperty("sibling_max", ""
					 +obj.getParent().size());
	    objectProperties.setProperty("parent_id", ""+obj.getParent().getSiblingId());
	    objectProperties.setProperty("parent_class_name", ""+obj.getParent().getClassName());
	}

	if(obj instanceof Atom3D) {
	    Atom3D atom = (Atom3D) obj;
	    ChemicalElement chemicalElement = atom.getElement();
	    if(chemicalElement != null) {
		String name = chemicalElement.getShortName();
		objectProperties.setProperty("element", name);
	    }

	    if(atom.getParent().getClassName().equals("Nucleotide3D")) {
		String base = "" + ((Nucleotide3D)atom.getParent()).getSymbol().getCharacter();
		objectProperties.setProperty("base", base);

	    }
	}

	RnaPhysicalProperties rna = new RnaPhysicalProperties();
// 	if (obj instanceof RnaBlock) {
// 	    RnaBlock strand = (RnaBlock)(obj);
// 	    Vector3D pos = strand.getPosition();
// 	    shape = new Cylinder(obj.getPosition(), 2*radius, radius);
// 	    adjustShapeProperties(shape, obj, objectProperties);
// 	    shapeSet.add(shape);
	    
// 	    // add 4 strand ends:

// 	    Vector3D tmpPos = new Vector3D();
// 	    tmpPos.copy(obj.getPosition());
// 	    tmpPos.setX(pos.getX() - 0.5*radius);
// 	    tmpPos.setZ(pos.getZ() + 1.5*radius);
// 	    shape = new Cylinder(tmpPos, radius, 0.25*radius);
// 	    adjustShapeProperties(shape, obj, objectProperties);
// 	    shapeSet.add(shape);
	    
// 	    tmpPos = new Vector3D();
// 	    tmpPos.copy(pos);
// 	    tmpPos.setX(pos.getX() + 0.5*radius);
// 	    tmpPos.setZ(pos.getZ() + 1.5*radius);
// 	    shape = new Cylinder(tmpPos, radius, 0.25*radius);
// 	    adjustShapeProperties(shape, obj, objectProperties);
// 	    shapeSet.add(shape);
	    
// 	    tmpPos = new Vector3D();
// 	    tmpPos.copy(pos);
// 	    tmpPos.setX(pos.getX() - 0.5*radius);
// 	    tmpPos.setZ(pos.getZ() - 1.5*radius);
// 	    shape = new Cylinder(tmpPos, radius, 0.25*radius);
// 	    adjustShapeProperties(shape, obj, objectProperties);
// 	    shapeSet.add(shape);
	    
// 	    tmpPos = new Vector3D();
// 	    tmpPos.copy(pos);
// 	    tmpPos.setX(pos.getX() + 0.5*radius);
// 	    tmpPos.setZ(pos.getZ() - 1.5*radius);
// 	    shape = new Cylinder(tmpPos, radius, 0.25*radius);
// 	    adjustShapeProperties(shape, obj, objectProperties);
// 	    shapeSet.add(shape);
// 	}
//	else
	if (obj instanceof RnaStrand) {
	    RnaStrand strand = (RnaStrand)(obj);
	    radius = rna.getSingleStrandWidth();
	    double height = rna.computeSingleStrandLength(strand.getResidueCount());
	    // TODO your programming ! Get instead of a constant a different strand id for different strands
	    int strandId = getStrandId(strand);
	    objectProperties.setProperty("strand_id", "" + strandId);
	    
	    if (strand.size() == 0) { // no explicit nucleotides defined
		shape = new Cylinder(obj.getPosition(), height, radius);
		adjustShapeProperties(shape, obj, objectProperties);
		shapeSet.add(shape);
		// add single letters:
		int nRes = strand.getResidueCount();
		for (int i = 0; i < nRes; ++i) {
		    Vector3D pos = strand.getResiduePosition(i);
		    char c = strand.getResidue(i).getSymbol().getCharacter(); // sequence character like "A" or "C"
		    shape = new CharacterShape(c);
		    shape.setPosition(pos);
		    adjustShapeProperties(shape, obj, objectProperties);
		    shapeSet.add(shape);
		}
	    }
	    else {
		double lRadius = 1.0;
		shape = new PointShape(obj.getPosition(), lRadius);
		adjustShapeProperties(shape, obj, objectProperties);
		shapeSet.add(shape);
	    }
	}
	else if (obj instanceof ProteinStrand) {
	    ProteinStrand strand = (ProteinStrand)(obj);
	    radius = rna.getSingleStrandWidth();
	    // TODO : wrong here, use instead protein constants
	    double height = rna.computeSingleStrandLength(strand.getResidueCount());
	    int strandId = getStrandId(strand);
	    objectProperties.setProperty("strand_id", "" + strandId);
	    
	    if (strand.size() == 0) { // no explicit nucleotides defined
		shape = new Cylinder(obj.getPosition(), height, radius);
		adjustShapeProperties(shape, obj, objectProperties);
		shapeSet.add(shape);
		// add single letters:
		int nRes = strand.getResidueCount();
		for (int i = 0; i < nRes; ++i) {
		    Vector3D pos = strand.getResiduePosition(i);
		    char c = strand.getResidue(i).getSymbol().getCharacter(); // sequence character like "A" or "C"
		    shape = new CharacterShape(c);
		    shape.setPosition(pos);
		    adjustShapeProperties(shape, obj, objectProperties);
		    shapeSet.add(shape);
		}
	    }
	    else {
		if (addDebugInfoMode) {
		    shape = new Cube(obj.getPosition(), new Vector3D(radius, radius , radius));
		    adjustShapeProperties(shape, obj, objectProperties);
		    shapeSet.add(shape);
		}
	    }
	}
	else if (obj instanceof Nucleotide3D) {
	    Nucleotide3D residue = (Nucleotide3D)(obj);
	    Object3D parent = residue.getParent();
	    int strandId = 1;
	    if ((parent != null) && (parent instanceof RnaStrand)) {
		RnaStrand parentStrand = (RnaStrand)parent;
		strandId = getStrandId(parentStrand);
	    }
	    objectProperties.setProperty("strand_id", "" + strandId);

	    radius = rna.getAverageBaseRadius();
	    shape = new PointShape(obj.getPosition(), radius);

	    adjustShapeProperties(shape, obj, objectProperties);
	    shapeSet.add(shape);	    
	    // add single letter:
	    if (residue.getSymbol() == null) {
		log.severe("Nucleotide3D without symbol!");
		System.exit(1);
	    }
	    char c = residue.getSymbol().getCharacter(); 
	    // sequence character like "A" or "C"
	    objectProperties.setProperty("base", "" + c);
	    shape = new CharacterShape(c);
	    shape.setPosition(obj.getPosition());
	    adjustShapeProperties(shape, obj, objectProperties);
	    shapeSet.add(shape);
	}
	else if (obj instanceof AminoAcid3D) {
	    AminoAcid3D residue = (AminoAcid3D)(obj);
	    Object3D parent = residue.getParent();
	    int strandId = 1;
	    if ((parent != null) && (parent instanceof ProteinStrand)) {
		ProteinStrand parentStrand = (ProteinStrand)parent;
		strandId = getStrandId(parentStrand);
	    }
	    // TODO : use protein parameters here
	    objectProperties.setProperty("strand_id", "" + strandId);
	    radius = rna.getAverageBaseRadius();
	    shape = new PointShape(obj.getPosition(), radius);
	    adjustShapeProperties(shape, obj, objectProperties);
	    shapeSet.add(shape);	    
	    // add single letter:
	    if (residue.getSymbol() == null) {
		log.severe("Amino acid without symbol!");
		System.exit(1);
	    }
	    char c = residue.getSymbol().getCharacter(); 
	    // sequence character like "A" or "C"
	    shape = new CharacterShape(c);
	    shape.setPosition(obj.getPosition());
	    adjustShapeProperties(shape, obj, objectProperties);
	    shapeSet.add(shape);
	}
	else if (obj instanceof StrandJunction3D) {
	    radius = 3.0; // 0.5 * Object3DTools.generateBoundingRadius(obj);
	    shape = new PointShape(obj.getPosition(), radius);
	    adjustShapeProperties(shape, obj, objectProperties);
	    shapeSet.add(shape);	    
	}
	else if (obj instanceof BranchDescriptor3D) {
	    radius = 2.0; // 0.5 * Object3DTools.generateBoundingRadius(obj);
	    Vector3D pos = obj.getPosition();
	    Vector3D pos2 = pos.plus(((BranchDescriptor3D)obj).getDirection());
	    shape = new PointShape(pos, radius);
	    adjustShapeProperties(shape, obj, objectProperties);
	    shapeSet.add(shape);	    
	    Shape3D shape2 = new LineShape(pos, pos2);
	    adjustShapeProperties(shape2, obj, objectProperties);
	    shapeSet.add(shape2);	    
	}
	else {
	    double lRadius = 1.0;
	    shape = new PointShape(obj.getPosition(), lRadius);
	    adjustShapeProperties(shape, obj, objectProperties);
	    shapeSet.add(shape);
	}

	return shapeSet;
    }

    /** creates set of shapes for object tree */
    public Shape3DSet createShapeSet(Object3D obj) {
	if ((obj == null)
	    || ((depthMax >= 0) && (obj.getDepth() > depthMax)) ) {
	    return new SimpleShape3DSet(); // return empty set
	}
	Shape3DSet result = createSingleObjectShapeSet(obj);
	for (int i = 0; i < obj.size(); ++i) {
	    result.merge(createShapeSet(obj.getChild(i)));
	}
	return result;
    }

    /** creates set of shapes for object tree */
    public Shape3DSet createShapeSet(Link link) {
	Shape3DSet result = new SimpleShape3DSet();
	Shape3D newShape = new LineShape(link.getObj1().getPosition(),
					 link.getObj2().getPosition());
	Properties linkProperties = new Properties();
	if (link instanceof InteractionLink) {
	    InteractionLink intLink = (InteractionLink)link;
	    Interaction interaction = intLink.getInteraction();
	    // obtain InteractionType from interaction	    
	    InteractionType intType = interaction.getInteractionType();
	    // find out if interaction type is BACKBONE or WATSON_CRICK
	    if (intType.getSubTypeId() == RnaInteractionType.WATSON_CRICK) {
		linkProperties.setProperty("interaction_type", "watson_crick");
	    }
	    if (intType.getSubTypeId() == RnaInteractionType.BACKBONE) {
		linkProperties.setProperty("interaction_type", "backbone");
	    }
	}
	
	newShape.setProperties(linkProperties);
	result.add(newShape);

	// TODO: vector differences	
	if( link.getTypeName().equals("Link") ) {
	}
	else if( link.getTypeName().equals("Vector") ) {
	}

	return result;
    }
    
    public int getDepthMax() { return this.depthMax; }
    
    public void setDepthMax(int n) { this.depthMax = n; }

}
