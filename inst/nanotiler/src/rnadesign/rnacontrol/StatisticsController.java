package rnadesign.rnacontrol;

import rnadesign.rnamodel.TilingStatistics;

public class StatisticsController implements TilingStatisticsController {
    
    TilingStatistics tilingStatistics = new TilingStatistics();
    
    public void setTilingStatistics(TilingStatistics statistics) {
	this.tilingStatistics = statistics;
    }
    
    /**returns a string of the final angle and distance stored
     *TODO: use this class and make sure the variables are correct
     */
    public TilingStatistics getTilingStatistics() { return tilingStatistics; }
    
}
