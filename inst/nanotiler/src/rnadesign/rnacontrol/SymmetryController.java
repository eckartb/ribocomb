package rnadesign.rnacontrol;

import tools3d.*;
import tools3d.symmetry2.*;
import tools3d.objects3d.CoordinateSystem3D;
import static rnadesign.rnacontrol.PackageConstants.*;

public class SymmetryController {

    SymCopies symCopies;

    public SymmetryController() {
	symCopies = SymCopySingleton.getInstance();
	init();
    }

    private void init() {
	symCopies.add(new CoordinateSystem3D(new Vector3D(0.0, 0.0, 0.0)));
    }
    
    public int size() { return symCopies.size(); }

    public void add(CoordinateSystem cs) { symCopies.add(new CoordinateSystem3D(cs)); }

    public void add(Vector3D base, Vector3D x, Vector3D y) { symCopies.add(new CoordinateSystem3D(base, x, y)); }

    public void add(Matrix4D matrix) { symCopies.add(new CoordinateSystem3D(matrix)); }

    public void clear() { 
	symCopies.clear(); 
	init();
    }

    public SymCopies getSymCopies() { return symCopies; }
    
    public String infoString() {
	StringBuffer buf = new StringBuffer();
	buf.append("Number coordinate systems: " + size() + NEWLINE);
	for (int i = 0; i < size(); ++i) {
	    buf.append("Coordinate system " + (i+1) + NEWLINE);
	    buf.append(symCopies.get(i).toString() + NEWLINE);
	}
	return buf.toString();
    }

    

}
