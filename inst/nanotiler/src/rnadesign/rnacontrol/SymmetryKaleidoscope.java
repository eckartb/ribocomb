package rnadesign.rnacontrol;

import tools3d.*;
import symmetry.*;
import java.util.logging.*;

import static rnadesign.rnacontrol.PackageConstants.*;

public class SymmetryKaleidoscope implements Kaleidoscope {

    Logger log = Logger.getLogger(LOGFILE_DEFAULT);

    Cell cell;
    LatticeSection section;
    CoordinateSystem[] coordinateSystems;

    public SymmetryKaleidoscope(SpaceGroup spaceGroup, Cell cell, LatticeSection section)
        throws Object3DGraphControllerException {
	this.cell = cell;
	this.section = section;
	this.coordinateSystems = SymmetryTools.generateCoordinateSystems(spaceGroup, cell);
    }

    void transform(Positionable3D obj, CoordinateSystem cs, Vector3D shift) {
	obj.activeTransform(cs);
	obj.translate(shift);
    }

    /** Add mirror objects (only with respect to cell repetitions) to geometry objects stored in ZBuffer. */
    public void apply(ZBuffer zBuffer) {
	log.fine("applying SymmetryKaleidoscope with section: " + section);
	assert section.getVolume() > 0;
	int origSize = zBuffer.size();
	for (int i = section.getXMin(); i < section.getXMax(); ++i) {
	    Vector3D shiftX = (cell.getX().mul((double)i));
	    for (int j = section.getYMin(); j < section.getYMax(); ++j) {
		Vector3D shiftY = (cell.getY().mul((double)j));
		for (int k = section.getZMin(); k < section.getZMax(); ++k) {
		    Vector3D shiftZ = (cell.getZ().mul((double)k));
		    Vector3D shift = shiftX.plus(shiftY).plus(shiftZ);
		    log.info("Applying shift: " + shift);
		    for (int jj = 0; jj < coordinateSystems.length; ++jj) {
			if ((i == 0) && (j == 0) && (k == 0) && (jj == 0)) {
			    continue; // do not copy current object
			}
			log.info("Applying coordinate system: " + coordinateSystems[jj]);
			CoordinateSystem cs = coordinateSystems[jj];
			for (int ii = 0; ii < origSize; ++ii) {
			    Positionable3D newPos = (Positionable3D)(zBuffer.get(ii).cloneDeep());
			    transform(newPos, cs, shift);
			    zBuffer.add(newPos);
			}
		    }
		}
	    }
	} 
    }

    /** how many symmetry mirror images are generated. 1: only original is there, nothing new is generated */
    public int getSymmetryCount() {
	return section.getVolume();
    }

}
