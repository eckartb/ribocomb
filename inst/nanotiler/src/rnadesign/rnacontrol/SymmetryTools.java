package rnadesign.rnacontrol;

import generaltools.StringTools;
import java.io.*;
import java.util.logging.*;
import java.util.ResourceBundle;
import launchtools.*;
import rnadesign.rnamodel.*;
import symmetry.*;
import tools3d.*;
import tools3d.objects3d.*;

import static rnadesign.rnacontrol.PackageConstants.*;

public class SymmetryTools {

    private static Logger log = Logger.getLogger(LOGFILE_DEFAULT);
    private static ResourceBundle rb = ResourceBundle.getBundle("Controller");
    public static final String ccp4PdbsetBinary = rb.getString("ccp4PdbsetBinary"); // usally: pdbset, should be in PATH 

    /** call CCP4 program pdbset/gensym */
    public static int computeSpaceGroupSymmetryCount(SpaceGroup spaceGroup) throws Object3DGraphControllerException {
	log.info("Starting computerNumberSpaceGroupSymmetries!");
	Object3D dummyAtom = new Atom3D();
	int result = -1;
	Cell defaultCell = new DefaultCell();
	try {
	    File tmpFile = File.createTempFile("pdbset_count", ".pdb");
	    String tmpFileName = tmpFile.getAbsolutePath();
	    writePdbSymmetric(tmpFileName, dummyAtom, generateSymgenCommandsSimple(spaceGroup, defaultCell)); // number of symmetries not known
	    result = AbstractPdbReader.countPdbAtoms(tmpFileName);
	}
	catch(IOException ioe) {
	    throw new Object3DGraphControllerException("Internal error in computeSpaceGroupSymmetryCount : " 
						       + ioe.getMessage());
	}
	// count how many atoms were generates
	assert result > 0;
	log.info("Finished computerNumberSpaceGroupSymmetries!");
	return result; 
    }

    public static CoordinateSystem[] generateCoordinateSystems(SpaceGroup spaceGroup, Cell cell) 
	throws Object3DGraphControllerException {
	Vector3D[] initialVectors = new Vector3D[4];
	initialVectors[0] = new Vector3D(0,0,0);
	initialVectors[1] = new Vector3D(1,0,0);
	initialVectors[2] = new Vector3D(0,1,0);
	initialVectors[3] = new Vector3D(0,0,1);
	Vector3D[] newVectors = generateSymmetricVectors(initialVectors, spaceGroup, cell);
	assert (newVectors.length % initialVectors.length) == 0;
	assert newVectors.length > 0;
	int numSym = newVectors.length / initialVectors.length;
	CoordinateSystem[] result = new CoordinateSystem[numSym];
	for (int i = 0; i < numSym; ++i) {
	    int pc = 4 * i;
	    Vector3D basePos = newVectors[pc]; 
	    Vector3D x = newVectors[pc+1].minus(basePos); 
	    Vector3D y = newVectors[pc+2].minus(basePos); 
	    Vector3D z = newVectors[pc+3].minus(basePos);
	    result[i] = new CoordinateSystem3D(basePos, x, y);
	    if (!result[i].isCartesian()) {
		throw new Object3DGraphControllerException("Non-cartesian coordinate system detected!");
	    }
	}
	log.info("Generated " + result.length + " coordinate systems from space group " + spaceGroup);
	return result;
    }

    /** call CCP4 program pdbset/gensym */
    static String generateSymgenCommandsSimple(SpaceGroup spaceGroup, Cell cell) {
	StringBuffer buf = new StringBuffer();
	buf.append(cell.toString() + NEWLINE);
	buf.append("SPACEGROUP " + spaceGroup.getNumber() + NEWLINE);
	buf.append("SYMGEN " + spaceGroup.getNumber() + NEWLINE);
	return buf.toString();
    }

    /** generates a set of symmetry copies of coordinates representing asymetric unit.
     */
    public static Vector3D[] generateSymmetricVectors(Vector3D[] coords,
						      SpaceGroup spaceGroup,
						      Cell cell) throws Object3DGraphControllerException {
	assert coords != null;
	if (coords.length == 0) {
	    return new Vector3D[0];
	}
	log.info("Starting computerNumberSpaceGroupSymmetries!");
	Object3D tree = new SimpleObject3D();
	for (int i = 0; i < coords.length; ++i) {
	    Atom3D atom = new Atom3D(coords[i]);
	    atom.setName("C");
	    tree.insertChild(new Atom3D(coords[i]));
	}
	Vector3D[] result = null;
	Cell defaultCell = new DefaultCell();
	try {
	    File tmpFile = File.createTempFile("pdbset_all", ".pdb");
	    String tmpFileName = tmpFile.getAbsolutePath();
	    writePdbSymmetric(tmpFileName, tree, generateSymgenCommandsSimple(spaceGroup, defaultCell)); // number of symmetries not known
	    FileInputStream fis = new FileInputStream(tmpFileName);
	    String[] pdbLines = StringTools.readAllLines(fis);
	    result = AbstractPdbReader.readPdbVectors(pdbLines);
	}
	catch(IOException ioe) {
	    throw new Object3DGraphControllerException("Internal error in computeSpaceGroupSymmetryCount : " 
						       + ioe.getMessage());
	}
	// count how many atoms were generates
	assert result != null;
	log.info("Finished generateSymmetricVectors!");
	assert result.length >= coords.length;
	assert (result.length % coords.length) == 0; // must be integer multiple!
	return result; 
    }

    /** call CCP4 program pdbset/gensym with given set of commands */
    public static void launchSymgen(String inFileName, String outFileName, String symmetryCommands) throws Object3DGraphControllerException {
	log.info("Starting launchGensym!");
	// generate command file for pdbset command:
	String commandFileName = "";
	try {
	    File commandFile = File.createTempFile("pdbset", ".prm");
	    commandFileName = commandFile.getAbsolutePath();
	    FileOutputStream fos = new FileOutputStream(commandFile);
	    PrintStream ps = new PrintStream(fos);
	    // ps.println(generateSymgenCommands(numSymmetries));
	    ps.println(symmetryCommands);
	    ps.close();
	}
	catch (IOException ioe) {
	    throw new Object3DGraphControllerException("IO Error while creating temp file for pdbset/symgen: "
						       + ioe.getMessage());
	}
	if (commandFileName.length() == 0) {
	    throw new Object3DGraphControllerException("IO Error while creating temp file for pdbset/symgen!");
	}
	String[] commandWords = { ccp4PdbsetBinary, inFileName, outFileName, commandFileName };
	RunCommand command = new SimpleRunCommand(commandWords);
	log.info("Created command for launching: " + command.toString());
	// create queue manager (singleton pattern)
	QueueManager queueManager = SimpleQueueManager.getInstance();
	// generate job
	Job job = queueManager.createJob(command);
	// add listener to job
	
	// launch command
	queueManager.submit(job);	    
	log.info("queue manager finished job!");
	log.info("Finished launchGensym!");
    }

    static void writePdbSymmetric(String fileName, Object3D tree, String symmetryCommands) throws Object3DGraphControllerException {
	log.info("Starting writePdbSymmetric!");
	try {
	    String tmpName = fileName + "_asym.pdb";
	    FileOutputStream fos = new FileOutputStream(tmpName);    
	    GeneralPdbWriter writer = new GeneralPdbWriter();
	    writer.write(fos, tree);
	    fos.close();
	    SymmetryTools.launchSymgen(tmpName, fileName, symmetryCommands);
	}
	catch (IOException exc) {
	    // replace with better error window
	    throw new Object3DGraphControllerException("Error opening/writing to file " + fileName);
	}
	log.info("Finished writePdbSymmetric!");
    }

}
