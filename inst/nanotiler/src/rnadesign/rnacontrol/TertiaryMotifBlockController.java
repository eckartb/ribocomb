package rnadesign.rnacontrol;

import java.io.*;
import java.util.*;
import java.util.logging.*;
import generaltools.StringTools;
import rnadesign.designapp.NanoTilerScripter;
import rnadesign.rnamodel.*;
import rnadesign.rnamodel.TertiarySubstructure;
import rnadesign.rnamodel.predict3d.TertiarySubstructureDatabase;
import rnasecondary.substructures.*;
import tools3d.objects3d.*;

import static rnadesign.rnacontrol.PackageConstants.*;

/** reads database of structures used for findig single and double stranded bridges */
public class TertiaryMotifBlockController {

  Map<Class,List<TertiarySubstructure>> data = new HashMap<Class,List<TertiarySubstructure>>();
  Map<TertiarySubstructure,String> names = new HashMap<TertiarySubstructure,String>();

  private static Logger log = Logger.getLogger(LOGFILE_DEFAULT);


public TertiaryMotifBlockController(String folderName) throws FileNotFoundException {
  log.info("Reading motif database from "+folderName);
  File folder = new File(folderName);
  if(!folder.isDirectory()){
    throw new FileNotFoundException(folderName);
  }
  readFiles(FindBulge.class, folderName+SLASH+"bulges");
  readFiles(FindHairpin.class, folderName+SLASH+"hairpins");
  readFiles(FindInternalLoop.class, folderName+SLASH+"internalloops");
}

    public void readFiles(Class type, String subDir) {
      File folder = new File(subDir+SLASH+"3D"); //3D based predictions
      File[] listOfFiles = folder.listFiles();
      if(listOfFiles==null){
        System.out.println("Error reading: "+folder);
      }
      
      List<String> fileNames = new ArrayList<String>();
      for (File file : listOfFiles) {
        if (true) { //TODO add validation for file
            fileNames.add((String)file.getName());
        }
      }
      List<TertiarySubstructure> structures = new ArrayList<TertiarySubstructure>();

      int i =0;
    	for (String name : fileNames) {
          // //TODO remove
          // i++;
          // if (i%16 != 0) {continue;}
          //^ used to limit database to certain size in order to decrease prediction time
          
          String s = folder+SLASH+name;
    	    try {

              InputStream stream = new FileInputStream(s);
              Object3DFactory reader = new RnaPdbRnaviewReader ();
              Object3D databaseStructure = reader.readAnyObject3D(stream);
              
              if (databaseStructure == null) {
                System.out.println ("Failed to create Object3D of " + s);
              }

              TertiarySubstructure sub = new TertiarySubstructure(type, databaseStructure); //passed type of motif, Object3D structure
              structures.add(sub);

              //find corresponding 3d file name
              int l = name.length ();
              if(!name.substring((l-4),l).equals (".pdb") ){
                throw new Exception("File: "+s+" not a .pdb file");
              }
              String name3d = name.substring(0,(l-4))+".pdb";
              String path3d = subDir+SLASH+"3D"+SLASH+name3d;
              File file3d = new File(path3d);
              if(!file3d.exists() ){
                throw new Exception("File: "+path3d+" does not exist");
              }
              this.names.put(sub,path3d);
          }
    	    catch(Exception ioe) {
    		      System.out.println("Could not read from file: " + s + " : ignoring: " + ioe.getMessage());
    	    }
          data.put(type,structures);
    	 }
    }

    public TertiarySubstructureDatabase getDatabase(){
      log.info("MotifDatabase initialized.");
      System.out.println("Total entries: "+names.keySet().size());
      for(Class c : data.keySet()){
        System.out.println("Type: "+c + "\nEntries: "+data.get(c).size());
      }
      return new TertiarySubstructureDatabase(data,names);
    }

}
