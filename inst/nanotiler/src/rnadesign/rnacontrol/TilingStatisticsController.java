package rnadesign.rnacontrol;

import rnadesign.rnamodel.TilingStatistics;

public interface TilingStatisticsController {

    //    public void setTilingStatistics(double angle, double dist);

    //public String getTilingStatistics();

    public void setTilingStatistics(TilingStatistics statistics);

    public TilingStatistics getTilingStatistics();

}

 
