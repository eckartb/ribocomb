package rnadesign.rnacontrol;

import java.util.*;
import java.util.logging.*;
import tools3d.Vector3D;
import tools3d.objects3d.*;
import tools3d.symmetry2.*;
import rnadesign.rnamodel.*;
import rnadesign.rnacontrol.*;

import static rnadesign.rnacontrol.PackageConstants.*;

/** This class generates a script that traces a graph using the JunctionDBContrataints 
 * (constraints that specify helix ends without needing helix lengths).
 */
public class TraceGraphDBScriptGenerator implements ScriptGenerator {

    private Object3D graphRoot;
    private Object3DSet vertices;
    private LinkSet links;
    private double distMin = 6.0;
    private double distMax = 12.0;
    private int[] helixLengths;
    private int[] origHelixIndices; // for specifying which helices are the original of the symmetry copies
    private boolean kissingLoopMode = false;
    private double kt = 10.0;
    private String name;
    private int numSteps = 100000;
    private int numStepsHelix = 500000;
    private boolean randomizeMode = false;
    private String jName = "jdb"; // prefix of junction link names
    private double vdwWeight = 1.0;
    private String hName = "traced_"; // "helix"; // root.traced.traced_1_root.traced_1_forw
    private int checkLen = 0;
    private double scale = 1.4;
    private List<SymEdgeConstraint> symConstraints;
    private static Logger log = Logger.getLogger(LOGFILE_DEFAULT);

    public TraceGraphDBScriptGenerator(Object3D graphRoot, LinkSet links, String name, List<SymEdgeConstraint> symConstraints,
				     Properties params, boolean kissingLoopMode) {
	// Object3DSet vertices
	this.graphRoot=graphRoot;
	this.kissingLoopMode = kissingLoopMode;
	this.vertices = new SimpleObject3DSet();
	for (int i = 0; i < graphRoot.size(); ++i) {
	    vertices.add(graphRoot.getChild(i));
	}
	this.links = new SimpleLinkSet();
	this.name = name;
	System.out.println("Starting TraceGraphDBScriptGenerator with vertices:");
	for (int i = 0; i < vertices.size(); ++i) {
	    System.out.println("" + (i+1) + " : " + vertices.get(i).getFullName());
	}
	for (int i = 0; i < links.size(); ++i) {
	    if (links.get(i) instanceof MultiLink) {
		continue; // only regular links
	    }
	    if (vertices.contains(links.get(i).getObj1()) && vertices.contains(links.get(i).getObj2())) {
		this.links.add(links.get(i));
	    } else {
		System.out.println("Could not add link: " + links.get(i));
	    }
	}
	this.symConstraints = symConstraints;
	parseParams(params);
	System.out.println("Starting TraceGraphDBScriptGenerator with " + vertices.size() + " vertices and " + this.links.size() + " links.");
    }

    private void parseParams(Properties params) {
	if (params.getProperty("min") != null) {
	    this.distMin = Double.parseDouble(params.getProperty("min"));
	}
	if (params.getProperty("max") != null) {
	    this.distMax = Double.parseDouble(params.getProperty("max"));
	}
    }

    public String generate() {
	StringBuffer buf = new StringBuffer();
	buf.append("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb" + NEWLINE);
	buf.append("set distMin " + distMin + NEWLINE);
	buf.append("set distMax " + distMax + NEWLINE);
	// buf.append("set distMin2 10" + NEWLINE);
	// buf.append("set distMax2 40" + NEWLINE);
        buf.append("tracernagraph root=" + graphRoot.getFullName() + NEWLINE);
//	buf.append("synth simple " + name + " root 0 0 0" + NEWLINE);
	buf.append("tree strand" + NEWLINE);
	generateHelices();
	buf.append("exportpdb " + name + "_initial.pdb" + NEWLINE);
	buf.append("tree strand" + NEWLINE);
	buf.append(generateVertexConstraints() + NEWLINE);
	buf.append(generateOptConstraints() + NEWLINE);
	//	buf.append(generateOptConstraints() + " vdw=1" + NEWLINE);
	String outputName = name + ".pdb";
	buf.append("exportpdb " + outputName + NEWLINE);
	return buf.toString();
    }
    
    /** Returns true, if n'th link is a symmetry copy of a different link described by symmetry constraint symId */
    private boolean isLinkDescribedBySymmetryConstraint(Link link, SymEdgeConstraint symConstraint) {
	Object3D obj1 = link.getObj1();
	Object3D obj2 = link.getObj2();
	int id1 = vertices.indexOf(obj1);
	int id2 = vertices.indexOf(obj2);
	return (((id1 == symConstraint.currId1) && (id2 == symConstraint.currId2))
		|| ((id2 == symConstraint.currId1) && (id1 == symConstraint.currId2)));
    }

    /** Returns index of symmetry descriptor which describes link/helix n */
    private int findLinkSymmetryId(int n) {
	Link link = links.get(n);
	Object3D obj1 = link.getObj1();
	Object3D obj2 = link.getObj2();
	int id1 = vertices.indexOf(obj1);
	int id2 = vertices.indexOf(obj2);
	log.info("Trying to find symmetry operation that involves link " + n + " " + obj1.getFullName() + " " 
		 + obj2.getFullName() + " " + id1 + " " + id2);
	if (symConstraints != null) {
	    for (int i = 0; i < symConstraints.size(); ++i) {
		if (isLinkDescribedBySymmetryConstraint(links.get(n), symConstraints.get(i))) {
		    return i; // link is a symmetry copy
		} else {
		    log.info("negative result for " + symConstraints.get(i));
		}
	    }
	}
	return -1;
    }

    /** Returns index of helix that serves as symmetry original copy of helix n */
    private boolean isLinkSymmetryCopyHelix(SymEdgeConstraint symConstraint, Link link) {
	int id1 = vertices.indexOf(link.getObj1());
	int id2 = vertices.indexOf(link.getObj2());
	return ((symConstraint.origId1 == id1) && (symConstraint.origId2 == id2))
	    || ((symConstraint.origId1 == id2) && (symConstraint.origId2 == id1));
    }

    /** Returns index of helix that serves as symmetry original copy of helix n */
    private int findLinkSymmetryCopyHelix(SymEdgeConstraint symConstraint) {
	for (int i = 0; i < links.size(); ++i) {
	    if (isLinkSymmetryCopyHelix(symConstraint, links.get(i))) {
		return i;
	    }
	}
	return -1;
    }

    /** Returns index of helix that serves as symmetry original copy of helix n */
    private int findLinkSymmetryCopyHelix(int n) {
	if (symConstraints != null) {
	    for (int i = 0; i < symConstraints.size(); ++i) {
		if (isLinkDescribedBySymmetryConstraint(links.get(n), symConstraints.get(i))) {
		    return findLinkSymmetryCopyHelix(symConstraints.get(i));
		}
	    }
	}
	return -1;
    }

    private String generateHelices() {
	this.helixLengths = new int[links.size()];
	this.origHelixIndices = new int[links.size()];
	StringBuffer buf = new StringBuffer();
	for (int i = 0; i < links.size(); ++i) {
	    // check if helix "only" a mirror copy:
	    if (findLinkSymmetryCopyHelix(i) >= 0) {
		log.info("Skipping helix generation for link " + (i+1) + " because it is a symmetry copy.");
		origHelixIndices[i] = findLinkSymmetryCopyHelix(i);
		helixLengths[i] = -1; // indicate symmetry copy
		continue;
	    } else {
		origHelixIndices[i] = 0;
	    }
	    double dist = links.get(i).getObj1().distance(links.get(i).getObj2());
	    int numBp = ((int)(dist / RnaConstants.HELIX_RISE)) + 1;
	    String helixName = hName + (i+1);
	    buf.append("genhelix bp=" + numBp + " root=root." + name + " name=" + helixName +  NEWLINE);
	    Vector3D pos1 = links.get(i).getObj1().getPosition();
	    Vector3D pos2 = links.get(i).getObj2().getPosition();
	    Vector3D pos = Vector3D.average(pos1, pos2);
	    pos1.scale(scale);
	    pos2.scale(scale);
	    buf.append("synth simple clonedpoints root 0 0 0" + NEWLINE);
	    buf.append("synth simple p1 root.clonedpoints " + pos1.getX() + " " + pos1.getY() + " " + pos1.getZ() + NEWLINE);
	    buf.append("synth simple p2 root.clonedpoints " + pos2.getX() + " " + pos2.getY() + " " + pos2.getZ() + NEWLINE);

	    // buf.append("select root." + name + "." + hName + i + "_root" + NEWLINE);
	    // 	    buf.append("randomize root." + name + "." + hName + i + "_root" + NEWLINE);
	    // buf.append("shift " + pos.getX() + " " + pos.getY() + " " + pos.getZ() + NEWLINE);
	    String helixRoot = "root." + name + "." + helixName + "_root";
	    buf.append("gendistconstraint " + helixRoot + "." + helixName + "_forw.1.P root.clonedpoints.p1 0 0" + NEWLINE);
	    buf.append("gendistconstraint " + helixRoot + "." + helixName + "_back.1.P root.clonedpoints.p2 0 0" + NEWLINE);
	    
	    buf.append("optconstraints kt=100 steps="+ numStepsHelix + " firstfixed=true blocks=root.clonedpoints;" + helixRoot + NEWLINE);
	    buf.append("remove root.clonedpoints" + NEWLINE);
	    if (randomizeMode) {
		buf.append("randomize root." + name + "." + helixName + "_root" + NEWLINE);
	    }
	    helixLengths[i] = numBp;
	} 
	return buf.toString();
    }

    private boolean isSymmetryMode() {  return (symConstraints != null) && (symConstraints.size() > 0); }

    private String generateConstraints() {
	StringBuffer buf = new StringBuffer();

	for (int i = 0; i < helixLengths.length; ++i) {
	    assert(helixLengths[i] != 0);
	    if (helixLengths[i] > 0) {
		int id1 = i;
		int id2 = i + 1;
		if (id2 == helixLengths.length) {
		    id2 = 0;
		}
		String helixName1 = hName + id1;
		String helixName2 = hName + id2;
		String obj1Name = "root." + name + "." + helixName1 + "_root." + helixName1 + "_forw." + helixLengths[id1] + ".O3*";
		String obj2Name = "root." + name + "." + helixName2 + "_root." + helixName2 + "_forw.1.P";
		String obj1bName = "root." + name + "." + helixName1 + "_root." + helixName1 + "_back.1.P";
		String obj2bName = "root." + name + "." + helixName2 + "_root." + helixName2 + "_back." + helixLengths[id2] + ".O3*";
		buf.append("gendistconstraint " + obj1Name + " " + obj2Name + " ${distMin} ${distMax}" + NEWLINE);
		buf.append("gendistconstraint " + obj1bName + " " + obj2bName + " ${distMin} ${distMax}" + NEWLINE);
		// FIXIT: check if length greater checkLen
		// FIXIT: check if length greater checkLen
// 	    for (int len1 = 2; len1 < checkLen; ++len1) {
// 		for (int len2 = 2; len2 <= checkLen; ++len2) {
// 		    String obj3Name = "root." + name + "." + helixName1 + "_root." + helixName1 + "_forw." + (helixLengths[id1] - len1) + ".P";
// 		    String obj4Name = "root." + name + "." + helixName2 + "_root." + helixName2 + "_forw." + len2 + ".P";
// 		    buf.append("gendistconstraint " + obj3Name + " " + obj4Name + " ${distMin2} ${distMax2}" + NEWLINE);
// 		    String obj5Name = "root." + name + "." + helixName1 + "_root." + helixName1 + "_back." + len1 + ".P";
// 		    String obj6Name = "root." + name + "." + helixName2 + "_root." + helixName2 + "_back." + (helixLengths[id2] - len2) + ".P";
// 		    buf.append("gendistconstraint " + obj5Name + " " + obj6Name + " ${distMin2} ${distMax2}" + NEWLINE);
// 		}
// 	    }
	    } 
	}
	return buf.toString();
    }

    /** Finds link ids of edges that contain vertex with id vertexId */
    private List<Integer> findVertexLinkIds(int vertexId) {
	Object3D obj = vertices.get(vertexId);
	List<Integer> result = new ArrayList<Integer>();
	for (int i = 0; i < links.size(); ++i) {
	    assert( ! (links.get(i) instanceof MultiLink));
	    if (links.get(i).linkOrder(obj) > 0) {
		result.add(i); // auto-boxing
	    }
	}
	return result;
    }

    private String generateVertexConstraints() {
	StringBuffer buf = new StringBuffer();
	for (int i = 0; i < vertices.size(); ++i) {
	    buf.append(generateVertexConstraint(i) + NEWLINE);
	}
	return buf.toString();
    }

    private String toInfoString() {
	StringBuffer result = new StringBuffer();
	result.append(" vertices: ");
	for (int i = 0; i < vertices.size(); ++i) {
	    result.append(vertices.get(i).getFullName());
	    result.append(" ");
	}
	result.append(" edges: ");
	for (int i = 0; i < links.size(); ++i) {
	    result.append(links.get(i).toString() + " " );
	}
	result.append(" helix lengths: ");
	if (helixLengths == null) {
	    result.append("undefined. ");
	} else {
	    for (int i : helixLengths) {
		result.append(" " + i);
	    }
	}
	result.append(" orig helix indices: ");
	if (origHelixIndices == null) {
	    result.append("undefined. ");
	} else {
	    for (int i : origHelixIndices) {
		result.append(" " + i);
	    }
	}
	if (symConstraints != null) {
	    result.append(" Symmetry constraints: ");
	    for (SymEdgeConstraint constraint : symConstraints) {
		result.append(" " + constraint.toString());
	    }
	} else {
	    result.append("No symmetry constraints defined.");
	}
	return result.toString();
    }

    private String generateVertexConstraint(int n) {
	log.info("Generating junction constraint for vertex " + n + " : " + vertices.get(n));
	StringBuffer buf = new StringBuffer();
	List<Integer> linkIds = findVertexLinkIds(n);
	System.out.println("Link ids for this vertex: ");
	for (Integer nt : linkIds) {
	    System.out.print(" " + nt);
	}
	List<Integer> symIds = new ArrayList<Integer>();
	String endsText = "";
	int objCount = 0;
	System.out.println("Starting generateVertexConstraint ( " + n + " status:");
	System.out.println(toInfoString());
	// loop over links that involve this vertex n:
	for (int j = 0; j < linkIds.size(); ++j) {	    
	    int id = linkIds.get(j); // helix with index id is current
	    int origId = id;
	    log.info("Working on link " + id + " : " + links.get(id).getObj1().getFullName() + " , " + links.get(id).getObj2().getFullName());
	    // check if link is a symmetry copy
	    if (isSymmetryMode()) {
		int symId = findLinkSymmetryId(id);
		if (symId >= 0) {
		    symIds.add(symConstraints.get(symId).symId); // add global id of symmetry operation
		    log.info("Changing helix id from " + id + " to " + origHelixIndices[id] + " using symmetry descriptor " + symId + " " + symConstraints.get(symId).toString());
		    origId = id;
		    id = origHelixIndices[id];
		} else {
		    log.info("This helix " + id + " is not a symmetry copy!");
		    symIds.add(0);
		}
	    } else {
		symIds.add(0);
	    }
	    Link link = links.get(origId);
	    boolean proper = false; // if obj2 of link is current vertex, the direction is "proper"
	    if (link.getObj1() != vertices.get(n)) {
		assert(link.getObj2() == vertices.get(n));
		proper = true;
	    } else {
		assert(link.getObj2() != vertices.get(n));
	    }
	    String helixName1 = hName + (id+1);
	    String obj1Name = "";
	    String obj2Name = "";
	    // assert(helixLengths[id] > 0); // no symmetry copy
	    String name2 = "traced";
	    if (proper) {
		// example name: root.traced.traced_1_root.traced_1_forw_1_traced_1_back_29
		// root.traced.traced_1_root.traced_1_back_1_traced_1_forw_29
		// obj1Name = "root." + name2 + "." + helixName1 + "_root." + helixName1 + "_forw_1_" + helixName1 + "_back_" + helixLengths[id]; // + helixLengths[id];
		obj1Name = "root." + name2 + "." + helixName1 + "_root.(hxend)1"; // + helixLengths[id];
		// obj2Name = "root." + name2 + "." + helixName1 + "_root." + helixName1 + "_back.1";
	    } else {
		obj1Name = "root." + name2 + "." + helixName1 + "_root.(hxend)2";
		// obj1Name = "root." + name2 + "." + helixName1 + "_root." + helixName1 + "_back.LAST"; // + helixLengths[id];
		// obj2Name = "root." + name2 + "." + helixName1 + "_root." + helixName1 + "_forw.1";
	    }
	    if (endsText.length() == 0) {
		endsText = obj1Name;
	    } else {
		endsText = endsText + "," + obj1Name;
	    }
	    objCount += 2;
	}
	StringBuffer symText = new StringBuffer();
	if (isSymmetryMode()) {
	    symText.append("sym=");
	    for (int i = 0; i < symIds.size(); ++i) {
		if (i > 0) {
		    symText.append(",");
		}
		symText.append("" + symIds.get(i));
	    }
	}
	if ((endsText.length() > 0) && (objCount > 2)) {
	    buf.append("genjunctiondbconstraint " + jName + (n+1) + " hd=" + endsText + " kl=" + Boolean.toString(kissingLoopMode) + NEWLINE); // + " min=${distMin} max=${distMax} " + symText.toString() + NEWLINE);
	}
	    // FIXIT: check if length greater checkLen
	    // FIXIT: check if length greater checkLen
	    // 		for (int len1 = 2; len1 < checkLen; ++len1) {
	    // 		    for (int len2 = 2; len2 <= checkLen; ++len2) {
	    // 			String obj3Name = "root." + name + "." + helixName1 + "_root." + helixName1 + "_forw." + (helixLengths[id1] - len1) + ".P";
	    // 			String obj4Name = "root." + name + "." + helixName2 + "_root." + helixName2 + "_forw." + len2 + ".P";
	    // 			buf.append("gendistconstraint " + obj3Name + " " + obj4Name + " ${distMin2} ${distMax2}" + NEWLINE);
	    // 			String obj5Name = "root." + name + "." + helixName1 + "_root." + helixName1 + "_back." + len1 + ".P";
	    // 			String obj6Name = "root." + name + "." + helixName2 + "_root." + helixName2 + "_back." + (helixLengths[id2] - len2) + ".P";
	    // 			buf.append("gendistconstraint " + obj5Name + " " + obj6Name + " ${distMin2} ${distMax2}" + NEWLINE);
	    // 		    }
	    // 		}
	return buf.toString();
    }

    // blocks=root.traced.helix0_root;
    private String generateOptConstraints() {
	StringBuffer buf = new StringBuffer();
	buf.append("optconstraints steps=" + numSteps + " kt=" + kt + " vdw=" + vdwWeight + " blocks=");
	int appCount = 0;
	String name2="traced"; // FIXIT: avoid hard-coding
	for (int i = 0; i < helixLengths.length; ++i) {
	    if (helixLengths[i] > 0) { // do not actively move symmetry copies indicated by negative values
		String helixName = "root." + name2 + "." + hName + (i+1) + "_root";
		if (appCount > 0) {
		    buf.append(";");
		}
		buf.append(helixName);
		appCount++;
	    } 
	}
	return buf.toString();
    }

}
