package rnadesign.rnalaunch;

import java.io.InputStream;

/** interface that sould be used by all classes and interfaces
 * defining input and output operations in rnalaunch.
 */
public interface InterfaceElement {
    
    /** reads object from stream */
    public void read(InputStream is);

    /** writes string representation of object */
    String toString();

}
