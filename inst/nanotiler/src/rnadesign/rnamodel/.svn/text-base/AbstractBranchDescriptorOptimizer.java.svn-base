package rnadesign.rnamodel;

import java.util.logging.Logger;

import numerictools.DoubleArrayTools;
import tools3d.CoordinateSystem;
import tools3d.Vector3D;

import static rnadesign.rnamodel.RnaConstants.HELIX_RISE;

public abstract class AbstractBranchDescriptorOptimizer implements BranchDescriptorOptimizer {

    protected static Logger log = Logger.getLogger("NanoTiler_debug");

    /** given two branch descriptors, computes a new branch descriptor corresponding to interpolating helix; phase is provided by caller.
     */
    protected static BranchDescriptor3D interpolateBranchDescriptor(RnaStem3D stem,
								  BranchDescriptor3D branch1,
								  BranchDescriptor3D branch2,
								  double phase) throws FittingException {
	Vector3D branch1NewBase = branch1.getBasePosition().plus(branch1.getDirection().mul(HELIX_RISE));
	Vector3D branch2NewBase = branch2.getBasePosition().plus(branch2.getDirection().mul(HELIX_RISE));
	Vector3D newZ = branch2NewBase.minus(branch1NewBase);
	assert(newZ.lengthSquare() > 0.0);
	newZ.normalize();
	Vector3D newPos = branch1NewBase; // branch1.getBasePosition().plus(newZ.mul(HELIX_RISE)); // shifted by one base pair
	CoordinateSystem cs = BranchDescriptorTools.generatePropagatedBranchDescriptorCoordinateSystem(branch1, 1);
	// newZ corresponds to z vectoer, find rotated x vector corresponding:
	double ang = newZ.angle(branch1.getDirection());
	if ((ang > 0.0) && (stem.getLength() >= 3)) {
	    if (ang == Math.PI) {
		throw new FittingException("Could not generate interpolated branch descriptor");
	    }
	    // Vector3D rotVec = newZ.cross(Vector3D.EZ);
	    Vector3D rotVec = cs.getZ().cross(newZ);
	    assert(rotVec.length() > 0.0);
	    rotVec.normalize();
	    cs.rotate(rotVec, ang);
// 	    newX = Matrix3DTools.rotate(newX, rotVec, ang);
// 	    assert(Math.abs(newX.angle(newZ) - (0.5 * Math.PI)) < 0.05); // check if 90 degree between x and z vector
	}
	if (phase != 0.0) {
	    cs.rotate(cs.getZ(), phase);
	}
// 	newX = Matrix3DTools.rotate(newX, newZ, phase);	
// 	Vector3D newY = newZ.cross(newX);
// 	assert(Math.abs(newY.length() - 1.0) < 0.01); // approximal unit vector
// 	CoordinateSystem cs = new CoordinateSystem3D(newPos, newX, newY);
	NucleotideStrand outgoingStrand = stem.getStrand1(); // corresponds to OUTGOING strand wrt to BranchDescriptor! Careful! TODO: Verify
	NucleotideStrand incomingStrand = stem.getStrand2(); // corresponds to INCOMING strand wrt to BranchDescriptor! Careful! TODO: Verify
	assert(outgoingStrand.getResidueCount() == stem.getLength());
	assert(incomingStrand.getResidueCount() == stem.getLength()); // only can handle properly sized strands
	int incomingIndex = stem.getLength() - 1;
	int outgoingIndex = 0;
	return new SimpleBranchDescriptor3D(incomingStrand, outgoingStrand, incomingIndex, outgoingIndex, cs);
    }

    /** given a branch descriptors, computes a new branch descriptor corresponding to propagated position (pos of next base pair)
     */
    protected static BranchDescriptor3D propagateBranchDescriptor(RnaStem3D stem,
								  BranchDescriptor3D branch1) {
	Vector3D branch1NewBase = branch1.getBasePosition().plus(branch1.getDirection().mul(HELIX_RISE));
	Vector3D newZ = branch1.getDirection(); // no change in direction yet
	assert(newZ.lengthSquare() > 0.0);
	newZ.normalize();
	Vector3D newPos = branch1NewBase; // branch1.getBasePosition().plus(newZ.mul(HELIX_RISE)); // shifted by one base pair
	CoordinateSystem cs = BranchDescriptorTools.generatePropagatedBranchDescriptorCoordinateSystem(branch1, 1);
	NucleotideStrand outgoingStrand = stem.getStrand1(); // corresponds to OUTGOING strand wrt to BranchDescriptor! Careful! TODO: Verify
	NucleotideStrand incomingStrand = stem.getStrand2(); // corresponds to INCOMING strand wrt to BranchDescriptor! Careful! TODO: Verify
	assert(outgoingStrand.getResidueCount() == stem.getLength());
	assert(incomingStrand.getResidueCount() == stem.getLength()); // only can handle properly sized strands
	int incomingIndex = stem.getLength() - 1;
	int outgoingIndex = 0;
	return new SimpleBranchDescriptor3D(incomingStrand, outgoingStrand, incomingIndex, outgoingIndex, cs);
    }

    /** Computes distance error using 4 reference points (reference positions of first and last base pairs
     * @param stemLength : describing length of stem to be interpolated
     * @param branch1 : first branchdescriptor of outside part
     * @param branch2 : first branchdescriptor of outside part
     * @param branch3 : branch descriptor of helix to be interpolated
     */
    protected static double computeBranchDescriptorError(int stemLength,
						       BranchDescriptor3D branch1,
						       BranchDescriptor3D branch2,
						       BranchDescriptor3D b) {
	int lastBp = stemLength - 1;
	double[] t = new double[4];
	t[0] = branch1.computeHelixPosition(1, BranchDescriptor3D.OUTGOING_STRAND).distanceSquare(
			       b.computeHelixPosition(0, BranchDescriptor3D.OUTGOING_STRAND));
	t[1] = branch1.computeHelixPosition(1, BranchDescriptor3D.INCOMING_STRAND).distanceSquare(
			       b.computeHelixPosition(0, BranchDescriptor3D.INCOMING_STRAND));
	t[2] = branch2.computeHelixPosition(1, BranchDescriptor3D.INCOMING_STRAND).distanceSquare(
			       b.computeHelixPosition(lastBp, BranchDescriptor3D.OUTGOING_STRAND));
	t[3] = branch2.computeHelixPosition(1, BranchDescriptor3D.OUTGOING_STRAND).distanceSquare(
			       b.computeHelixPosition(lastBp, BranchDescriptor3D.INCOMING_STRAND));
	return Math.sqrt(t[DoubleArrayTools.findHighestElement(t)]); // corresponds to maximum norm
    }

    /** Computes distance error using 4 reference points (reference positions of first and last base pairs
     * Debug version, ignoring terms t3 and t4!
     * @param stem : describing stem to be interpolated (includes strands, start and stop info)
     * @param branch1 : first branchdescriptor of outside part
     * @param branch2 : first branchdescriptor of outside part
     * @param branch3 : branch descriptor of helix to be interpolated
     */
    protected static double computeBranchDescriptorErrorDebug(int stemLength,
							    BranchDescriptor3D branch1,
							    BranchDescriptor3D branch2,
							    BranchDescriptor3D b) {
	int lastBp = stemLength - 1;
	double t1 = branch1.computeHelixPosition(1, BranchDescriptor3D.OUTGOING_STRAND).distanceSquare(
			       b.computeHelixPosition(0, BranchDescriptor3D.OUTGOING_STRAND));
	double t2 = branch1.computeHelixPosition(1, BranchDescriptor3D.INCOMING_STRAND).distanceSquare(
			       b.computeHelixPosition(0, BranchDescriptor3D.INCOMING_STRAND));
// 	double t3 = branch2.computeHelixPosition(1, BranchDescriptor3D.INCOMING_STRAND).distanceSquare(
// 			       b.computeHelixPosition(lastBp, BranchDescriptor3D.OUTGOING_STRAND));
// 	double t4 = branch2.computeHelixPosition(1, BranchDescriptor3D.OUTGOING_STRAND).distanceSquare(
// 			       b.computeHelixPosition(lastBp, BranchDescriptor3D.INCOMING_STRAND));
	return Math.sqrt(0.5 * (t1 + t2));
    }


}
