package rnadesign.rnamodel;

import java.util.*;
import tools3d.objects3d.*;
import tools3d.Vector3D;
import chemistrytools.ChemicalElement;
import chemistrytools.DefaultChemicalElement;
import chemistrytools.PeriodicTableImp;
import java.util.logging.*;

import static rnadesign.rnamodel.PackageConstants.*;

public class AtomTools {

    private static Logger log = Logger.getLogger(LOGFILE_DEFAULT);
    
    public static double defaultCollisionDistance = 2.0; // 5;

    /** Counts atom collisions. TODO: take individual atom radii into account */
    public static int countAtomCollisions(Object3DSet atomSet, double collisionDistance,
					  Level verboseLevel) {
	int counter = 0;
	boolean reportFlag = true;
// 	if (verboseLevel != null) {
// 	    reportFlag = (verboseLevel.intValue() <= log.getLevel().intValue());
// 	}
	for (int i = 0; i < atomSet.size(); ++i) {
	    Atom3D atom1 = (Atom3D)(atomSet.get(i));
	    if ((atom1.getElement()!=null) && "H".equals(atom1.getElement().getShortName())) {
		continue; // do not count hydrogen atoms
	    }
	    for (int j = i+1; j < atomSet.size(); ++j) {
		if (atom1.distance(atomSet.get(j)) < collisionDistance) {
		    Atom3D atom2 = (Atom3D)(atomSet.get(j));
		    if (atom1.getParent() == atom2.getParent()) {
			continue; // same residue, ignore!
		    }
		    if ((atom2.getElement()!=null) && "H".equals(atom2.getElement().getShortName())) {
			// do not count hydrogen atoms
		    }
		    else {
			++counter; 
			if (reportFlag){
			    log.info("Collision: " + atom1.getFullName() + " " + atom2.getFullName());
			}
		    }
		}
	    }
	}
	return counter;
    }
    
    public static boolean isCovalentlyBonded(Atom3D a1, Atom3D a2) {
    	if(a1.getLinks().find(a1, a2) != null)
    		return true;
    	
    	return a1.getPosition().distance(a2.getPosition()) < 1.5;
    }

    /** returns true if atom is hydrogen atom */
    public static boolean isHydrogen(Atom3D atom) {
	if (atom.getElement()!=null) {
	    return "H".equals(atom.getElement().getShortName());
	}
	return atom.getName().charAt(0) == 'H';
    }

    /** returns H for hydrogen atom, C for carbon. Careful: also H for Helium! */
    public static char getElementChar(Atom3D atom) {
	if (atom.getElement()!=null) {
	    return atom.getElement().getShortName().charAt(0);
	}
	return atom.getName().charAt(0);
    }
					 
    /** Counts atom collisions. TODO: take individual atom radii into account */
    public static int countExternalCollisions(Object3DSet atomSet1, Object3DSet atomSet2, double collisionDistance) {
	int counter = 0;
	for (int i = 0; i < atomSet1.size(); ++i) {
	    Atom3D atom1 = (Atom3D)(atomSet1.get(i));
	    if ((atom1.getElement()!=null) && "H".equals(atom1.getElement().getShortName())) {
		continue;
	    }
	    for (int j = 0; j < atomSet2.size(); ++j) {
		if (atom1.distance(atomSet2.get(j)) < collisionDistance) {
		    Atom3D atom2 = (Atom3D)(atomSet2.get(j));
		    if ((atom2.getElement()!=null) && "H".equals(atom2.getElement().getShortName())) {
			// do not count hydrogen atoms
		    }
		    else {
			++counter; 
		    }
		}
	    }
	}
	return counter;
    }

    /** Counts atom collisions. TODO: take individual atom radii into account */
    private static int countExternalCollisions(Vector3D[] positions, Object3D subTree, Object3D largeTree, double collisionDistance) {
	if (subTree == largeTree) {
	    return 0;
	}
	int counter = 0;
	if (largeTree instanceof Atom3D) {
	    Vector3D pos = largeTree.getPosition();
	    for (int i = 0; i < positions.length; ++i) {
		if (positions[i].distance(pos) < collisionDistance) {
		    ++counter;
		}
	    }
	}
	for (int i = 0; i < largeTree.size(); ++i) {
	    counter += countExternalCollisions(positions, subTree, largeTree.getChild(i), collisionDistance);
	}
	return counter;
    }

    /** Counts atom collisions; recursively call itself until "largeTree" is of class Atom3D. TODO: take individual atom radii into account */
    public static int countExternalCollisions(Object3DSet atomSet, 
					      Object3D subTree,
					      Object3D largeTree,
					      Object3DSet ignoreTrees,
					      double collisionDistance) {
	int compCount = 0; // counts number of comparisons
	// assert positions.size() == atomSet.size();
	if (subTree == largeTree) { // do not count collisions with itself
	    return 0;
	}
	for (int i = 0; i < ignoreTrees.size(); ++i) {
	    if (ignoreTrees.get(i) == largeTree) {
		return 0;
	    }
	}
	int counter = 0;
	if (largeTree instanceof Atom3D) {
	    Vector3D pos = largeTree.getPosition();
	    for (int i = 0; i < atomSet.size(); ++i) {
		if (atomSet.get(i) == largeTree) {
		    continue; // ignore self
		}
		if (atomSet.get(i).getPosition().distance(pos) < collisionDistance) {
		    ++counter;
		}
		++compCount;
	    }
	}
	for (int i = 0; i < largeTree.size(); ++i) {
	    counter += countExternalCollisions(atomSet, subTree, largeTree.getChild(i), ignoreTrees, collisionDistance);
	}
// 	log.info("Number of collions and comparisons: " + subTree.getFullName() + " " + largeTree.getFullName() + " : " 
// 		 + counter + " " + compCount);
	return counter;
    }

    /** Counts atom collisions; recursively call itself until "largeTree" is of class Atom3D. TODO: take individual atom radii into account */
    public static int countExternalCollisions(Vector3D[] positions,
					       Object3DSet atomSet, 
					       Object3D subTree,
					       Object3D largeTree,
					       Object3DSet ignoreTrees,
					       double collisionDistance) {
	assert positions.length == atomSet.size();
	if (subTree == largeTree) {
	    return 0;
	}
	for (int i = 0; i < ignoreTrees.size(); ++i) {
	    if (ignoreTrees.get(i) == largeTree) {
		return 0;
	    }
	}
	int counter = 0;
	if (largeTree instanceof Atom3D) {
	    Vector3D pos = largeTree.getPosition();
	    for (int i = 0; i < positions.length; ++i) {
		if (atomSet.get(i) == largeTree) {
		    continue; // ignore self
		}
		if (positions[i].distance(pos) < collisionDistance) {
		    ++counter;
		}
	    }
	}
	for (int i = 0; i < largeTree.size(); ++i) {
	    counter += countExternalCollisions(positions, atomSet, subTree, largeTree.getChild(i), ignoreTrees, collisionDistance);
	}
	return counter;
    }

    /** Counts atom collisions. TODO: take individual atom radii into account */
    public static int countExternalCollisions(Object3D subTree, Object3D largeTree, double collisionDistance) {
	Object3DSet atomSet1 = Object3DTools.collectByClassName(subTree, "Atom3D");
	Vector3D[] positions = new Vector3D[atomSet1.size()];
	for (int i = 0; i < atomSet1.size(); ++i) {
	    positions[i] = atomSet1.get(i).getPosition();
	}
	return countExternalCollisions(positions, subTree, largeTree, collisionDistance);

    }

    /** Counts atom collisions. Between subTree and largeTree not counting ignoreTrees */
    public static int countExternalCollisions(Object3D subTree, Object3D largeTree, Object3DSet ignoreTrees, double collisionDistance) {
	Object3DSet atomSet1 = Object3DTools.collectByClassName(subTree, "Atom3D");
	Vector3D[] positions = new Vector3D[atomSet1.size()];
	for (int i = 0; i < atomSet1.size(); ++i) {
	    positions[i] = atomSet1.get(i).getPosition();
	}
	return countExternalCollisions(positions, atomSet1, subTree, largeTree, ignoreTrees, collisionDistance);
    }

    /** returns center of mass */
    public static Vector3D centerOfMass(List<Atom3D> objSet) {
	Vector3D sum = new Vector3D(0,0,0);
	for (int i = 0; i < objSet.size(); ++i) {
	    sum.add(objSet.get(i).getPosition());
	}
	sum.scale(1.0 / objSet.size());
	return sum;
    }

    /** Extracts graph from object3d structure : each node corresponds to one junction */
    public static Object3DLinkSetBundle convertGraphToMolecule(Object3D root, LinkSet links, String nameBase) {
	Object3D resultObj = new SimpleObject3D();
	resultObj.setName(nameBase);
	resultObj.setPosition(root.getPosition());
	LinkSet resultLinks = new SimpleLinkSet();
	for (int i = 0; i < root.size(); ++i) {
	    Atom3D atom = new Atom3D(root.getChild(i).getPosition());
	    String suggested = root.getChild(i).getProperty("atom_element");
	    ChemicalElement element = null;
	    if ((suggested != null) && (suggested.length()>0)) {
		element = new DefaultChemicalElement(suggested);
	    }
	    else {
		int linkOrder = links.getLinkOrder(root.getChild(i));
		if (linkOrder > 0) {
		    if (linkOrder < 5) {
			element = PeriodicTableImp.generateDefaultChemicalElementByValency(linkOrder);
		    }
		    else {
			log.warning("Object has link order greater four, cannot be mapped to chemical element, ignoring: " + root.getChild(i).getFullName());
			continue;
		    }
		}
		else {
		    log.warning("Object with link order zero detected, ignoring: " + root.getChild(i).getFullName());
		    continue;
		}
	    }
	    if (element != null) {
		atom.setElement(element);
		atom.setName(element.getShortName()); // TODO : name duplication!
	    }
	    else {
		log.warning("Cannot generate chemical element representing " + Object3DTools.getFullName(root.getChild(i)));
	    }
	    resultObj.insertChildSave(atom);
	}
	// generate corresponding links:
	for (int i = 0; i < root.size(); ++i) {
	    for (int j = i+1; j < root.size(); ++j) {
		int linkNumber = links.getLinkNumber(root.getChild(i), root.getChild(j));
		if (linkNumber > 0) {
		    resultLinks.add(new SimpleLink(resultObj.getChild(i), resultObj.getChild(j)));
		}
	    }
	}
	return new SimpleObject3DLinkSetBundle(resultObj, resultLinks);
    }

}
