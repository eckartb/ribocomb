
/** Factory class is responsible for reading/parsing a data file and creating
 *  a corresponding Object3DGraph
 */
package rnadesign.rnamodel;

import java.awt.Color;
import java.io.*;

import sequence.*;
import tools3d.Vector3D;
import tools3d.objects3d.*;
import java.util.logging.*;

/**
 * @author Eckart Bindewald
 *
 */
public class ProteinPdbReader implements Object3DFactory {

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    public static final int PRECISION = 3;

    public static final int NO_TRANSLATION = 0;

    public static final int AMBER_TO_STANDARD = 1;

    private int translationMode = NO_TRANSLATION;
	
    char badChar = 'X';

    /** Constructor needs a buffered reader (provides methods for linewise reading) */
    public ProteinPdbReader() {
    }

    /** reads object and links. */
    public Object3DLinkSetBundle readBundle(InputStream is) throws Object3DIOException {
	Object3D obj = readAnyObject3D(is);
	// add links:
	LinkSet links = new SimpleLinkSet();
	for (int i = 1; i < obj.size(); ++i) {
	    links.add(new SimpleLink(obj.getChild(i-1), obj.getChild(i)));
	}
	return new SimpleObject3DLinkSetBundle(obj, links);
    }

    /** returns keyword of PDB line like HETATM, ATOM or REMARK */
    private String getKeyword(String line) {
	if ((line != null) && (line.length() > 7)) {
	    return line.substring(0,7).trim();
	}
	return null;
    }

    /** returns true if current line starts with keyword "ATOM" */
    private boolean isAtom(String line) {
	if (line.length() < 50) {
	    return false;
	}
	String keyword = getKeyword(line);
	return (keyword.equals("ATOM"));
    }

    private char getStrandName(String line) {
	return line.charAt(21);
    }

    private String getResidueName(String line) {
	String result = line.substring(17, 17+3).trim();
	switch (translationMode) {
	case NO_TRANSLATION:
	    break;
	case AMBER_TO_STANDARD:
	    result = translateAmberToStandardResidueName(result);
	    break;
	}
	return result;
    }

    /** translates an atom name from Amber nomenclature to standard
     * nomenclature.
     * replaces ' character to * character.
     */
    private String translateAmberToStandardAtomName(String s) {
	return s.replace('\'', '*');
    }

    /** translates an atom name from Amber nomenclature to standard
     * nomenclature.
     * changes "RC" to "C" or "RC5" to "C"
     */
    private String translateAmberToStandardResidueName(String s) {
	if ((s.length() > 1) && (s.charAt(0) == 'R')) {
	    return s.substring(1,2);
	}
	return s;
    }

    private String getAtomName(String line) {
	String result = line.substring(12, 12+4).trim();
	switch (translationMode) {
	case NO_TRANSLATION:
	    break;
	case AMBER_TO_STANDARD:
	    result = translateAmberToStandardAtomName(result);
	    break;
	}
	return result;
    }

    private int getResidueId(String line) {
	String word = line.substring(22, 22+4).trim();
	return Integer.parseInt(word);
    }

    private int getAtomCounter(String line) {
	return Integer.parseInt(line.substring(7, 7+4).trim());
    }

    private double getX(String line)
    {
	return Double.parseDouble(line.substring(30, 30 + 5+PRECISION).trim());
    }

    private double getY(String line)
    {
	// old start value: 38
	return Double.parseDouble(line.substring(35+PRECISION, 
						 35 + PRECISION + 5+PRECISION).trim());
    }

    private double getZ(String line)
    {
	// old start value 46
	int startPos = 40+(2*PRECISION);
	return Double.parseDouble(line.substring(startPos, startPos + 5+PRECISION).trim());
    }

    /** generates new Atom3D object from PDB line */
    private Atom3D generateAtomFromPdb(String line) {
	Vector3D pos = new Vector3D(getX(line), getY(line), getZ(line));
	String name = getAtomName(line);
	Atom3D atom = new Atom3D();
	atom.setPosition(pos);
	atom.setName(name);
	return atom;
    }

    /** returns one-letter character (ACGUT) from possible three letter code */
    private char getResidueCharacter(String residueName) {
	if (residueName.equals("ALA")) { return 'A'; }
	else if (residueName.equals("CYS")) { return 'C'; }
	else if (residueName.equals("ASP")) { return 'D'; }
	else if (residueName.equals("GLU")) { return 'E'; }
	else if (residueName.equals("CYS")) { return 'F'; }
	else if (residueName.equals("GLY")) { return 'G'; }
	else if (residueName.equals("HIS")) { return 'H'; }
	else if (residueName.equals("ILE")) { return 'I'; }
	else if (residueName.equals("LYS")) { return 'K'; }
	else if (residueName.equals("LEU")) { return 'L'; }
	else if (residueName.equals("MET")) { return 'M'; }
	else if (residueName.equals("ASN")) { return 'N'; }
	else if (residueName.equals("PRO")) { return 'P'; }
	else if (residueName.equals("GLN")) { return 'Q'; }
	else if (residueName.equals("ARG")) { return 'R'; }
	else if (residueName.equals("SER")) { return 'S'; }
	else if (residueName.equals("THR")) { return 'T'; }
	else if (residueName.equals("VAL")) { return 'V'; }
	else if (residueName.equals("TRP")) { return 'W'; }
	else if (residueName.equals("TYR")) { return 'Y'; }
	return badChar;
    }

    /** returns translation mode. */
    public int getTranslationMode() { return translationMode; }
    
    /* (non-Javadoc)
     * @see rnamodel.Object3DGraphFactory#createObject3DGraph()
     */
    public Object3D readAnyObject3D(InputStream is) throws Object3DIOException {
	DataInputStream dis = new DataInputStream(is);
	Object3D root = new SimpleObject3D();
	root.setName("PdbImport");
	char currentStrandName = '_';
	int currentResidueId = -9999;
	int currentAtomId = -9999;
	ProteinStrand currentStrand = null;
	AminoAcid3D currentResidue = null;
	String line;
	try {
	    do {
		line = dis.readLine(); // read new atom
		if (line == null) {
		    break;
		}
		if (isAtom(line)) {
		    int residueId = getResidueId(line);
		    String residueName = getResidueName(line);
		    String atomName = getAtomName(line);
		    char strandName = getStrandName(line);
		    Atom3D atom = generateAtomFromPdb(line);
		    if ((currentStrand == null) || (strandName != currentStrandName)) {
			// generate new strand:
			currentStrand = new SimpleProteinStrand(ProteinTools.PROTEIN_ALPHABET);
			currentStrand.setName(""+strandName);
			currentStrandName = strandName;
			currentStrand.setPosition(atom.getPosition());
			root.insertChild(currentStrand);
		    }
		    if ((currentResidue == null) || (currentResidueId != residueId)) {
			// generate new amino acid: position is equal to position of first atom
			try {
			    char c = getResidueCharacter(residueName);
			    LetterSymbol symbol = new SimpleLetterSymbol(c, ProteinTools.PROTEIN_ALPHABET);
			    currentResidue = new AminoAcid3D(symbol, 
							     currentStrand.getResidueCount(), 
							     atom.getPosition());
			    String resName = residueName + residueId;
			    currentResidue.setName(resName);
			    currentStrand.insertChild(currentResidue);
			    currentResidueId = residueId;
			}
			catch (UnknownSymbolException e) {
			    log.warning("Warning: could not interpret amino acid symbol from line: " + line);
			    // do nothing otherwise
			}
		    }
		    currentResidue.insertChild(atom);
		    // TODO : cannot handle 
		}
	    }
	    while (line != null);
	}
	catch (IOException exp) {
	    // do nothing
	}
	return root;
    }
    

   /** reads and creates Vector3D in format (Vector3D x y z ) */
    public static Vector3D readVector3D(DataInputStream dis) throws Object3DIOException {
	String word = readWord(dis);
	double x = 0;
	double y = 0;
	double z = 0;
	try {
	    x = Double.parseDouble(word);
	}
	catch (NumberFormatException e) {
	    throw new Object3DIOException("Could not parse x coordinate for Vector3D " + word);
	}
	word = readWord(dis);
	try {
	    y = Double.parseDouble(word);
	}
	catch (NumberFormatException e) {
	    throw new Object3DIOException("Could not parse y coordinate for Vector3D " + word);
	}	
	word = readWord(dis);
	try {
	    z = Double.parseDouble(word);
	}
	catch (NumberFormatException e) {
	    throw new Object3DIOException("Could not parse z coordinate for Vector3D " + word);
	}
	return new Vector3D(x, y, z);	
    }

    /** reads a single word from data stream */
    public static String readWord(DataInputStream dis) {
	String s = new String("");
	char c = ' ';
	
	// first skip white space
	do {
			try {
			   c = (char)dis.readByte();
			}
			catch (IOException e) {
				break; // end of file reached
			}
		}
		while (Character.isWhitespace(c));
		
		if (!Character.isWhitespace(c)) {
			s = s + c;
		}
		else {
			return s;
		}
		
		while (true) {
			try {
			   c = (char)dis.readByte();
			 if (!Character.isWhitespace(c)) {
				 s = s + c;
			 }
			 else {
				 break;
			 }
			}
			catch (IOException e) {
				break; // end of file reached
			}
		}
		return s;
	}

    public void setTranslationMode(int mode) { translationMode = mode; }

}
