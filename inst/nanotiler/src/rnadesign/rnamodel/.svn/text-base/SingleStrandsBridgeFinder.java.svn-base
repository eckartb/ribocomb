package rnadesign.rnamodel;

import java.util.*;
import java.util.logging.*;
import tools3d.objects3d.*;
import numerictools.*;

import static rnadesign.rnamodel.RnaConstants.*;

/** Interface for classes that find 3D bridges between two objects */
public class SingleStrandsBridgeFinder implements StrandBridgeFinder {

    private Logger log = Logger.getLogger(PackageConstants.LOGGER_NAME);

    private double distMax = 30.0;

    private int lenMax = 3;

    private int lenMin = 1;

    private static double STRAND_BP_PENALTY = 20.0;

    private SingleStrandBridgeFinder bridgeFinder;

    public void setDistMax(double x) { this.distMax = x; }

    public void setRms(double rms) { bridgeFinder.setRms(rms); }

    public void setAngleWeight(double weight) { bridgeFinder.setAngleWeight(weight); }

    public void setLenMax(int n) { bridgeFinder.setLenMax(n); }

    public SingleStrandsBridgeFinder(List<Object3DLinkSetBundle> bundleList) {
	this.bridgeFinder = new SingleStrandBridgeFinder(bundleList);
	this.bridgeFinder.setLenMax(lenMax);
	this.bridgeFinder.setLenMin(lenMin);
    }

    /** Generates bridge such between 3' end of strand 1 and 5' end of strand 2*/
    private Object3DLinkSetBundle generateBridge(RnaStrand strand1, RnaStrand strand2) {
	List<Object3DLinkSetBundle> result = bridgeFinder.findBridge(strand1.getResidue3D(strand1.getResidueCount()-1),
								     strand2.getResidue3D(0));
	if ((result == null) || (result.size() == 0)) {
	    return null;
	}
	return result.get(0); // returns first bridge solution
    }

    /** Computes distance between last O3* atom of strand1 and first P atom of strand2 */
    private double threePrimeFivePrimeDistance(RnaStrand strand1, RnaStrand strand2) {
	Object3D atom1 = strand1.getResidue3D(strand1.getResidueCount()-1).getChild("O3*");
	Object3D atom2 = strand2.getResidue3D(0).getChild("P");
	double dist1 = atom1.distance(atom2);
	
	if ((strand1.size() > 2) && (strand2.size() > 2) && (dist1 < HELIX_BP_P_O3_DIST)) {
	    Object3D atom1b = strand1.getResidue3D(strand1.getResidueCount()-3).getChild("O3*");
	    Object3D atom2b = strand2.getResidue3D(2).getChild("P");
	    double dist2 = atom1b.distance(atom2b);
	    if (dist2 < HELIX_BP_P_O3_DIST) {
		dist1 += STRAND_BP_PENALTY; // strands are base paired! Do not bridge them
	    }
	}
	return dist1; 
    }

    public List<Object3DLinkSetBundle> findBridges(Object3DSet strands) {
	log.info("Starting findBridges");
	// Object3D bridges = new SimpleObject3D("bridges");
	// LinkSet links = new SimpleLinkSet();
	List<Object3DLinkSetBundle> bridges = new ArrayList<Object3DLinkSetBundle>();
	// Object3DSet strands = Object3DTools.collectByClassName(placedHelices, "RnaStrand");
	double[][] distMatrix = new double[strands.size()][strands.size()];
	List<ObjectPairScore> indexList = new ArrayList<ObjectPairScore>();
	for (int i = 0; i < strands.size(); ++i) {
	    RnaStrand strand1 = (RnaStrand)(strands.get(i));
	    distMatrix[i][i] = 0.0;
	    for (int j = i+1; j < strands.size(); ++j) {
		RnaStrand strand2 = (RnaStrand)(strands.get(j));
		double score = threePrimeFivePrimeDistance(strand1, strand2);
		indexList.add(new ObjectPairScore(score, new Integer(i), new Integer(j)));
		score = threePrimeFivePrimeDistance(strand2, strand1);
		indexList.add(new ObjectPairScore(score, new Integer(j), new Integer(i)));
	    }
	}
	Collections.sort(indexList);
	System.out.println("Bridge sores:");
	for (int i = 0; i < 20 ;++i) {
	    System.out.println("" + (i+1) + " : " + indexList.get(i));
	}
	boolean[] fivePrimeBridged = new boolean[strands.size()];
	boolean[] threePrimeBridged = new boolean[strands.size()];
	for (int i = 0; i < strands.size(); ++i) {
	    fivePrimeBridged[i] = false;
	    threePrimeBridged[i] = false;
	}
	for (int i = 0; i < indexList.size(); ++i) {
	    int id1 = ((Integer)(indexList.get(i).getObject1())).intValue();
	    int id2 = ((Integer)(indexList.get(i).getObject2())).intValue();
	    double score = indexList.get(i).getScore();
	    if (score > distMax) {
		break;
	    }
	    if ((!fivePrimeBridged[id2]) && (!threePrimeBridged[id1])) {
		log.info("Generating bridge between strands " + strands.get(id1).getName()
			 + " and " + strands.get(id2).getName());
		Object3DLinkSetBundle bridge = generateBridge((RnaStrand)(strands.get(id1)), (RnaStrand)(strands.get(id2)));
		if ((bridge != null) && (bridge.getObject3D().size() > 0)) {
		    bridges.add(bridge);
		}
		else {
		    log.info("Could not generating bridge!");
		}
		fivePrimeBridged[id2] = true;
		threePrimeBridged[id1] = true;
	    }
	}
	log.info("Finished findBridges");
	return bridges;
    }


}
