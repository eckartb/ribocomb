package rnadesign.rnamodel;

import java.util.*;
import tools3d.*;
import tools3d.objects3d.*;

public class AI3DTools {

    public static final String TMP_TOKEN = "__RINGFIX_HELIXSTATUS";
    public static final String TMP_TOKEN_PLACED = "occupied";
    public static final String TMP_TOKEN_JUNCTIONCLASS = "__JUNCTION_CLASS";

    public static boolean strandEquivalent(NucleotideStrand strand1, NucleotideStrand strand2) {
	assert strand1 != null;
	assert strand2 != null;
	return (strand1.sequenceString().equals(strand2.sequenceString()));
    }

    /** returns true if two strands occupied same space */
    public static Vector3D[] strandResiduePositions(BioPolymer strand, String atomName) {
	Vector3D[] result = new Vector3D[strand.getResidueCount()];
	for (int i = 0; i < strand.getResidueCount(); ++i) {
	    Object3D atom = strand.getResidue3D(i).getChild(atomName);
	    if (atom != null) {
		result[i] = atom.getPosition();
	    }
	    else {
		result[i] = strand.getResidue3D(i).getPosition(); // emergency solution in case no atom of this kind is available
	    }
	}
	return result;
    }

    /** returns true if two strands occupied same space, using RMS cutoff and testing only atoms with specified name */
    public static boolean checkStrandDuplicate(BioPolymer strand1, BioPolymer strand2, double rmsCutoff, String atomName) {
	boolean firstTest = (strand1.sequenceString().equals(strand2.sequenceString()));
	if (!firstTest) {
	    return false;
	}
	// check RMS:
	double rms = Vector3DTools.computeRms(strandResiduePositions(strand1, atomName), strandResiduePositions(strand2, atomName));
	return (rms < rmsCutoff);
    }

    public static boolean junctionEquivalent(StrandJunction3D junction1, StrandJunction3D junction2) {
	if (junction1 instanceof KissingLoop3D) {
	    if (! (junction2 instanceof KissingLoop3D)) {
		return false;
	    }
	}
	else {
	    if (junction2 instanceof KissingLoop3D) {
		return false;
	    }	    
	}
	if (junction1.getStrandCount() != junction2.getStrandCount()) {
	    return false;
	}
	for (int i = 0; i < junction1.getStrandCount(); ++i) {
	    if (!strandEquivalent(junction1.getStrand(i), junction2.getStrand(i))) {
		return false;
	    }
	}
	return true;
    }

    public static List<List<StrandJunction3D> > classifyJunctions(List<StrandJunction3D> junctions) {
	assert junctions != null;
	List<List<StrandJunction3D> > junctionClasses = new ArrayList<List<StrandJunction3D> >();
	int count = 0;
	for (StrandJunction3D junction: junctions) {
	    boolean found = false;
	    for (int i = 0; i < junctionClasses.size(); ++i) {
		assert junctionClasses.get(i).size() > 0;
		if (junctionEquivalent(junctionClasses.get(i).get(0), junction)) {
		    junctionClasses.get(i).add(junction);
		    junction.setProperty(TMP_TOKEN_JUNCTIONCLASS, "" +i);
		    ++count;
		    found = true;
		    break;
		}
	    }
	    if (!found) {
		List<StrandJunction3D> newClass = new ArrayList<StrandJunction3D>();
		newClass.add(junction);
		junction.setProperty(TMP_TOKEN_JUNCTIONCLASS, "" + junctionClasses.size());
		junctionClasses.add(newClass);
		++count;
	    }
	}
	assert junctionClasses != null && count == junctions.size();
	return junctionClasses;
    }

}
