package rnadesign.rnamodel;

import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DTools;
import tools3d.objects3d.SimpleObject3D;
import sequence.*;

public abstract class AbstractBiopolymer extends SimpleObject3D implements BioPolymer {

    protected static Alphabet alphabet = DnaTools.AMBIGUOUS_RNA_ALPHABET;	

    protected String getSequenceName() {
	return getName() + ".s";
    }

//     public Sequence getSequence() { 
// 	return this; // new version: BioPolymer IS sequence!
	// return sequence;
// 	Sequence sequence = new SimpleSequence(getSequenceName(), alphabet);
// 	for (int i = 0; i < getResidueCount(); ++i) {
// 	    assert(getResidue(i).getPos() == sequence.size());
// 	    sequence.addResidue(getResidue(i));
// 	}
// 	sequence.setParent(this);
// 	return sequence;
//     }

    /** orders sequences by name */
//     public int compareTo(Sequence other) {
// 	if (other instanceof Object3D) {
// 	    return Object3DTools.getFullName(this).compareTo(Object3DTools.getFullName((Object3D)other));
// 	}
// 	return getName().compareTo(other.getName());
//     }

    public String sequenceString() { 
	// log.warning("Slow method: SimpleRnaStrand.getSequence()!");
	StringBuffer result = new StringBuffer(); //  seqName = getSequenceName();
	// Sequence sequence = new SimpleSequence("", seqName, alphabet);
	for (int i = 0; i < size(); ++i) {
	    result.append("" + getResidue(i).getSymbol().getCharacter());
	}
	return result.toString(); 
    }

    /** Shortens current sequence to have length position (residues 0..position-1), and returns new sequence with positions position...length-1.
     * No cloning of residues is being done, just reorganizing. */
    protected BioPolymer split(int position, BioPolymer newStrand) {
	assert newStrand.size() == 0;
	assert position < this.getResidueCount();
	int oldLength = getResidueCount();
	String oldSequence = sequenceString();
	for (int i = position; i < getResidueCount(); ++i) {
	    newStrand.addResidue(getResidue(i));
	}
	for (int i = getResidueCount()-1; i >= position; --i) {
	    assert getChild(i) instanceof Residue3D; // user must have added another object
	    removeChild(i);
	}
	System.out.println("Original length: "+ oldLength
			   + " number of residues after split: " + getResidueCount() + " " + newStrand.getResidueCount());
	assert (getResidueCount() + newStrand.getResidueCount()) == oldLength;
	assert oldSequence.equals(sequenceString() + newStrand.sequenceString());
	return newStrand;
    }


}
