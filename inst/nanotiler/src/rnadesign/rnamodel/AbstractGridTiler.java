package rnadesign.rnamodel;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import generaltools.ApplicationBugException;
import graphtools.IntegerList;
import graphtools.IntegerListList;
import graphtools.IntPair;
import graphtools.PathTools;
import rnasecondary.SimpleStem;
import rnasecondary.Stem;
import sequence.UnknownSymbolException;
import tools3d.CoordinateSystem;
import tools3d.objects3d.CoordinateSystem3D;
import tools3d.Vector3D;
import tools3d.Vector3DTools;
import tools3d.objects3d.Link;
import tools3d.objects3d.LinkSet;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DLinkSetBundle;
import tools3d.objects3d.Object3DSet;

import static rnadesign.rnamodel.RnaConstants.HELIX_RISE;

/** generate tiling of RnaStrand objects and RnaStem3D objects 
 * tracing given set of objects 
 */
public abstract class AbstractGridTiler implements GridTiler {

    protected static RnaPhysicalProperties rpp = new RnaPhysicalProperties(); // mainly used as shortcut
    /** number of residues per loop. Important variable */
    // protected static int residuePerLoopCount = 0; // 3;

    protected int verboseLevel = 0;

    private Logger log = Logger.getLogger("NanoTiler_debug");

    /** estimates the number of residues needed for a segment of a certain length */
    protected int estimateSegmentResidueCount(double pathLength, int residuePerLoopCount) {
	double curveLength = residuePerLoopCount * rpp.RNA_SINGLESTRAND_LENGTH_PER_RESIDUE;
	// int result = (int)(((pathLength-curveLength) / rpp.RNA_DOUBLESTRAND_LENGTH_PER_RESIDUE));
	int result = (int)(((pathLength-curveLength) / HELIX_RISE));
	if (result < 0) {
	    result = 0;
	}
	result += residuePerLoopCount; // actually 2 * (0.5 * residuePerLoopCount)
	return result;
    }

    /** estimates the number of residues needed for a certain path */
    int estimateResidueCount(IntegerList path, Object3DSet objectSet, int residuePerLoopCount) {
	int result = 0;
	for (int i = 1;  i < path.size(); ++i) {
	    Vector3D pos1 = objectSet.get(path.get(i-1)).getPosition();
	    Vector3D pos2 = objectSet.get(path.get(i)).getPosition();
	    double length = pos1.distance(pos2);
	    result += estimateSegmentResidueCount(length, residuePerLoopCount);
	}
	return result;
    }

    /** generates string with residueCount characters 'N' */
    protected String generateSequenceString(int residueCount, char defaultChar) {
	char[] seqChars = new char[residueCount];
	for (int i = 0; i < residueCount; ++i) {
	    seqChars[i] = defaultChar;
	}
	return new String(seqChars);
    }

    /** generates strand with name "name" that can trace a path defined by "path" and objectSet */
    protected Object3DLinkSetBundle generateStrand(IntegerList path, Object3DSet objectSet, String name, char defaultSequenceChar,
						   int residuePerLoopCount) {
	int someExtra = 0; // TODO : not clean, should be zero
	int residueCount = estimateResidueCount(path, objectSet, residuePerLoopCount) + someExtra;
	log.fine("Called estimate residue count: " + objectSet.size() + " path: " + path + " result: " + residueCount);
	String sequenceString = generateSequenceString(residueCount, defaultSequenceChar);
	String sequenceName = name + ".s";
	RnaStrand strand;
	Vector3D startPos = objectSet.get(path.get(0)).getPosition();
	Vector3D direction = objectSet.get(path.get(1)).getPosition().minus(startPos);
	Object3DLinkSetBundle bundle;
	try {
	    bundle = Rna3DTools.generateSimpleRnaStrand(name, sequenceString, startPos, direction);
	    strand =(RnaStrand)(bundle.getObject3D());
	}
	catch (UnknownSymbolException e) {
	    throw new ApplicationBugException("Internal error when trying to generate " + sequenceName + " sequence: " 
					      + sequenceString);
	}
	strand.setPosition(objectSet.get(path.get(0)).getPosition());
	log.fine("Finished AbstractGridTiler.generateStrand: " + bundle.getObject3D().size() 
			   + " " + bundle.getLinks().size());
	return bundle;
    }

    private boolean isCircularPath(IntegerList path) {
	return ((path.size() > 2)
		&& (path.get(0) == path.get(path.size()-1)));
    }
    
    /** returns start and stop pos of part of strand belonging to path between objectId1 and objectId2 */
    protected IntPair getResidueRange(IntegerList pathOrig, Object3DSet objectSet,
			    int objectId1, int objectId2, int residuePerLoopCount) {
	log.fine("Started getResidueRange with path: " + pathOrig 
			   + " ids: " + objectId1 + " " + objectId2);
	// estimate residue needed for initial path;
	boolean circularMode = false;
	IntegerList path = pathOrig;
	if (isCircularPath(pathOrig)) {
	    path = pathOrig.subset(0, pathOrig.size()-1);
	    circularMode = true;
	    log.fine("Circular mode detected! Using: " + path);
	}
	IntegerList initialPath = new IntegerList();
	IntegerList mainPath = new IntegerList();
	boolean watsonStrand = false;
	// special case for stem between first and last data point:
	if ((path.get(0) == objectId1) && (path.get(path.size()-1) == objectId2) && (path.size() > 2)) {
	    log.fine("Special case 1: segement between start and end point of path!");
	    mainPath.add(path.get(0));
	    mainPath.add(path.get(path.size()-1));
	    for (int i = 0; i < path.size(); ++i) {
		initialPath.add(path.get(i));
	    }
	}
	else if ((path.get(0) == objectId2) && (path.get(path.size()-1) == objectId1) && (path.size() > 2)) {
	    log.fine("Special case 2: segement between start and end point of path!");
	    watsonStrand = true;
	    mainPath.add(path.get(path.size()-1));
	    mainPath.add(path.get(0));
	    for (int i = 0; i < path.size(); ++i) {
		initialPath.add(path.get(i));
	    }
	}
	else {
	    log.fine("General case: segement not touching start and end points of path!");
	    initialPath.add(path.get(0));
	    for (int i = 1; i < path.size(); ++i) {
		boolean found = false;
		if ((path.get(i-1) == objectId1) && (path.get(i) == objectId2)) {
		    watsonStrand = true;
		    found = true;
		}
		else if ((path.get(i-1) == objectId2) && (path.get(i) == objectId1)) {
		    watsonStrand = false; // reverse direction
		    found = true;
		}
		if (found) {
		    mainPath.add(path.get(i-1));
		    mainPath.add(path.get(i));
		    break;
		}
		else {
		    initialPath.add(path.get(i));
		}
	    }
	}
	if (watsonStrand) {
	    log.fine("Watson-strand mode! initial and main paths: "
			       + initialPath + " , " + mainPath);
	}
	int initialResidues = estimateResidueCount(initialPath, objectSet, residuePerLoopCount);
	int mainResidues = estimateResidueCount(mainPath, objectSet, residuePerLoopCount);
//  	if (!watsonStrand) { // go other way round:
//  	    initialPath.clear();
//  	    for (int i = path.size()-1; i >= 1; --i) {
//  		initialPath.add(path.get(i));
//  		if ((path.get(i-1) == objectId2) && (path.get(i) == objectId1)) {
//  		    break;
//  		}
//  	    }
//  	    log.fine("Crick-strand mode! initial and main paths: "
//  				   + initialPath + " , " + mainPath);
//  	    initialResidues = estimateResidueCount(initialPath, objectSet);
//  	    mainResidues = estimateResidueCount(mainPath, objectSet);
//  	}
	if (mainResidues <= 0) {
	    throw new ApplicationBugException("Internal error in getResidueRange: "
					      + initialPath + " , " + mainPath + " , " + initialResidues 
					      + " " + mainResidues);
	}
	IntPair result;
	if (watsonStrand) {
	    result = new IntPair(initialResidues, initialResidues + mainResidues -1); // start and stop pos in Watson strand
	}
	else {
	    result = new IntPair(initialResidues + mainResidues -1, initialResidues); // start and stop pos in Crick strand 
	}
	log.fine("Finished getResidueRange with result: " + result);
	return result;
    }


    private boolean isWatsonStrand(IntPair pair) {
	return pair.getFirst() <= pair.getSecond();
    }

    /** Generates stem from paths, strands and objectset and object ids.
     * PRECONDITION: path1 and strand1 must correspond to the "Watson-strand" of the stem: the sequence that is counting forward
     * with respect to the counting of the stem position 
     */
    protected Stem generateStem(IntegerList path1, IntegerList path2, RnaStrand strand1, RnaStrand strand2,
		      Object3DSet objectSet, int objectId1, int objectId2, int residuePerLoopCount) {
	log.fine("Starting generateStem with paths: " + path1 + " , " + path2 + " object ids: " 
			   + objectId1 + " " + objectId2);
	IntPair residueRange1 = getResidueRange(path1, objectSet, objectId1, objectId2, residuePerLoopCount);
	IntPair residueRange2 = getResidueRange(path2, objectSet, objectId1, objectId2, residuePerLoopCount);
	if (isWatsonStrand(residueRange2)) {
	    throw new ApplicationBugException("Internal error (152) in SimpleGridTiler.generateStem ! ");
	}
	// TODO verify!!! I am sure the next lines are still not totally correct, especially wrt directionality ...
	log.fine("Residue range of path " + path1.toString() + " ids: " + objectId1 + " " + objectId2 + " : " + residueRange1);
	log.fine("Residue range of path " + path2.toString() + " ids: " + objectId1 + " " + objectId2 + " : " + residueRange2);
	    
	int startPos = residueRange1.getFirst();
	int stopPos = residueRange2.getFirst(); // was getSecond(), but the order of the Crick strand is reversed!
	int length = Math.abs(residueRange1.getSecond()-residueRange1.getFirst()) + 1;
	log.fine("Calling new SimpleStem with " + startPos + " " + stopPos + " "
		 + length + " " + strand1.sequenceString() + " " + strand2.sequenceString());
	if ((startPos < 0) || (stopPos < 0) ||(length < 0)) {
	    throw new ApplicationBugException("Internal error (1) in SimpleGridTiler.generateStem ! "
					      + startPos + " " + stopPos + " " + length + " " + strand1.getResidueCount()
					      + " " + strand2.getResidueCount());
	}	    
	if (!SimpleStem.isValid(startPos, stopPos, length, strand1.getResidueCount(), strand2.getResidueCount())) {
	    log.fine("oops, stem parameters are not valid: " + startPos + " "
		     + stopPos + " " + length + " " + strand1.getResidueCount()
		     + strand2.getResidueCount());
	    return null;
	} 
	Stem stem = new SimpleStem(startPos, stopPos, length, strand1, strand2);
	if (!stem.isValid()) {
// 	    throw new ApplicationBugException("Internal error in SimpleGridTiler.generateStem ! "
// 					      + startPos + " " + stopPos + " " + length + " " + strand1.getResidueCount()
// 					      + " " + strand2.getResidueCount());
	    log.fine("oh uh... : " + stem.getStartPos() + " " + stem.getStopPos() + " "
		     + stem.size() );
	    return null;
	}
	return stem;
    }

    /** generates RnaStem3D. Paths must match the strands in strandSet */
    protected Object3DLinkSetBundle generateStem3D(IntegerListList paths, Object3DSet objectSet, Object3DSet strands,
						   Link link, String stemName, Object3D nucleotideDB,
						   int offset) {
	log.fine("Starting generateStem3D!");
	// find the two strands that have "link" in common
	// equivalent to finding two paths that have the two object ids corresponding to "link" in common
	int id1 = objectSet.indexOf(link.getObj1());
	int id2 = objectSet.indexOf(link.getObj2());
	if ((id1 < 0) || (id2 < 0)) {
	    throw new ApplicationBugException("Malformed Object3D Link found in generateStem!");
	}
	// founds the two paths that contain both object ids. There should be only two paths having a direct connection
	List<Integer> foundPaths = new ArrayList<Integer>();
	for (int i = 0; i < paths.size(); ++i) {
	    // do NOT delete last element temporarily
	    IntegerList path = paths.get(i); // .subset(0, paths.get(i).size()-1);
	    if (PathTools.containsPair(path, id1, id2)) {
		foundPaths.add(new Integer(i));
	    }
	    if (PathTools.containsPair(path, id2, id1)) {
		foundPaths.add(new Integer(i));
	    }
// 	    if (path.contains(id1) && path.contains(id2)) {
// 		log.fine("Path " + i + " ( " + path.toString() + " ) "
// 				   + id1 + " " + id2);
// 		if ((Math.abs(path.indexOf(id1)-path.indexOf(id2))  == 1) 
// 		    || (Math.abs(path.indexOf(id1)-path.indexOf(id2))  == (path.size()-1))) {
// 		    foundPaths.add(new Integer(i));
// 		}
// 	    }
	}
	if (foundPaths.size() < 2) {
	    throw new ApplicationBugException("no two paths found in generateStem!");
	}
	if (foundPaths.size() > 2) {
	    throw new ApplicationBugException("more than two paths found in generateStem!");
	}
	int pathId1 = ((Integer)(foundPaths.get(0))).intValue();
	int pathId2 = ((Integer)(foundPaths.get(1))).intValue();
	// int offset = 0; // when to start helix. Might be interesting to pass as parameter TODO: test, verify, implement
	log.fine("Using path (=strand) ids: " + pathId1 + " " 
			   + pathId2);
	RnaStrand strand1 = (RnaStrand)(strands.get(pathId1));
	RnaStrand strand2 = (RnaStrand)(strands.get(pathId2));
        Object3DLinkSetBundle stem3d; //  = new SimpleRnaStem3D(stem, objectSet.get(id1).getPosition(), objectSet.get(id2).getPosition(),
	//               objectSet.get(id2).getPosition().plus(offset1), objectSet.get(id1).getPosition().plus(offset2));
	// stem3d.setName(stemName);
	IntPair residueRange1 = getResidueRange(paths.get(pathId1), objectSet, 
						id1, id2, 2*offset);
	log.fine("Residue range of first path: " + residueRange1);
	if (isWatsonStrand(residueRange1)) {
	    log.fine("First path is Watson strand!");
	    Stem stem = generateStem(paths.get(pathId1), paths.get(pathId2), 
				     strand1, strand2, objectSet, id1, id2, 2*offset);
	    if (stem == null) { 
		log.fine("Warning: generated stem was null!");
		return null;
	    }
	    else if (!stem.isValid()) {
		log.fine("Warning: generated stem is not valid!");
		return null;
	    }
// 	    Vector3D offset1 = new Vector3D(1,1,1);
// 	    Vector3D offset2 = new Vector3D(-1,-1,-1); // TODO : these are really dumm (and wrong) offset vectors
	    Vector3D startPos3d = objectSet.get(id1).getPosition();
	    Vector3D direction = objectSet.get(id2).getPosition().minus(objectSet.get(id1).getPosition());
	    assert direction.lengthSquare() > 0.0;
	    direction.normalize();
	    Vector3D xDir = Vector3DTools.generateRandomOrthogonalDirection(direction);
	    xDir.normalize();
	    Vector3D yDir = direction.cross(xDir);
	    CoordinateSystem cs = new CoordinateSystem3D(startPos3d, xDir, yDir);
	    BranchDescriptor3D branchDescriptor = new SimpleBranchDescriptor3D(strand2, strand1, stem.getStopPos(), stem.getStartPos(), cs);
	    log.severe("calling outdated method Rna3DTools.generateRnaStem3D!");
	    //	    stem3d = Rna3DTools.generateRnaStem3D(strand1, strand2, stem.getStartPos(), stem.getStopPos(), stem.size(), true, stemName,
	    //						  startPos3d, direction); //outdated
	    stem3d = Rna3DTools.generateRnaStem3D( strand1, strand2, stem.getStartPos(), stem.getStopPos(), stem.size(), offset, true, stemName,
						   branchDescriptor, nucleotideDB);
	}
	else {
	    log.fine("First path is Crick strand!");
	    Stem stem = generateStem(paths.get(pathId2), paths.get(pathId1), 
				     strand2, strand1, objectSet, id1, id2, 2*offset);
	    if (stem == null) {
		log.fine("Warning: stem is null!");
		return null;
	    }
	    else if (!stem.isValid()) {
		log.fine("Warning: generated stem is not valid!");
		return null;
	    }

	    Vector3D startPos3d = objectSet.get(id1).getPosition();
	    Vector3D direction = objectSet.get(id2).getPosition().minus(objectSet.get(id1).getPosition());
	    assert direction.lengthSquare() > 0.0;
	    direction.normalize();
	    Vector3D xDir = Vector3DTools.generateRandomOrthogonalDirection(direction);
	    xDir.normalize();
	    Vector3D yDir = direction.cross(xDir);
	    CoordinateSystem cs = new CoordinateSystem3D(startPos3d, xDir, yDir);
	    BranchDescriptor3D branchDescriptor = new SimpleBranchDescriptor3D(strand1, strand2, stem.getStartPos(), stem.getStopPos(), cs);
	    log.severe("calling outdated method Rna3DTools.generateRnaStem3D!");
	    //	    stem3d = Rna3DTools.generateRnaStem3D(strand2, strand1, stem.getStartPos(), stem.getStopPos(), stem.size(), true, stemName,
	    //						  startPos3d, direction); //TODO: outdated
	    stem3d = Rna3DTools.generateRnaStem3D( strand2, strand1, stem.getStartPos(), stem.getStopPos(), stem.size(), offset, true, stemName,
						   branchDescriptor, nucleotideDB );
	}
	log.fine("Finished generateStem3D!");
	return stem3d;
    }


    /** sets strand coordinates according to stems and links and paths. The n'th stem corresponds to the n'th link */
    private void setStrandCoordinates(Object3DSet strandSet, 
				      Object3DSet stemSet, 
				      Object3D resultObjects, 
				      LinkSet stemLinks) {
	// setStemCoordinates(strandSet, stemSet, resultObjects, stemLinks); // not necessary
	// interpolate strands for non-stem regions:
	for (int i = 0; i < strandSet.size(); ++i) {
	    Rna3DTools.interpolateNonStems((RnaStrand)(strandSet.get(i)), resultObjects);
	}
    }

    /** returns set of strand that trace the given set of objects */
    /*
    public Object3DLinkSetBundle generateTiling(Object3D objects, LinkSet links, String baseName, char defaultSequenceChar) {
	log.fine("Starting generateTiling!");
	Object3DSet objectSet = new SimpleObject3DSet(objects);
	// first generate graph
	GraphBase graph = GridPathTools.generateGraph(objectSet, links);
	// obtain circular paths:
	GraphBaseOrderSet graphOrderSet = new SimpleGraphBaseOrderSet(graph, 4);
	IntegerListList circularPaths = GraphBaseOrderSetTools.getUniqueCyclicalPaths(graphOrderSet);
	Object3D resultObjects = new SimpleObject3D();
	Object3DSet strandSet = new SimpleObject3DSet();
	Object3DSet stemSet = new SimpleObject3DSet();
	resultObjects.setName("tilingroot");
	// generate a strand for each circular path
	log.fine("generating strand for each circular path out of " + circularPaths.size());
	for (int i = 0; i < circularPaths.size(); ++i) {
	    String strandName = baseName + ".strand" + (i+1); // like grid.1, grid.2 etc : different name for each strand
	    RnaStrand strand = generateStrand(circularPaths.get(i), objectSet, strandName, defaultSequenceChar);
	    log.fine("Strand " + strand.getName() + " with size " + strand.getResidueCount() + " " + strand.size()
			       + " generated.");
	    resultObjects.insertChild(strand);
	    strandSet.add(strand);
	}
	// generate a Stem3D for each link
	log.fine("generating Stem3D for each link");
	for (int i = 0; i < links.size(); ++i) {
	    String stemName = baseName + ".stem" + (i+1);
	    RnaStem3D stem = generateStem3D(circularPaths, objectSet, strandSet, links.get(i), stemName);
	    if (stem != null) {
		resultObjects.insertChild(stem);
		stemSet.add(stem);
	    }
	}
	// set strand coordinates (only loop regions necessary):
	setStrandCoordinates(strandSet, stemSet, resultObjects, links);

	LinkSet newLinks = new SimpleLinkSet();
	// so far no new links except the new stems
	Object3DLinkSetBundle resultBundle = new SimpleObject3DLinkSetBundle(resultObjects, newLinks);
	log.fine("Finished generateTiling!");
	return resultBundle;
    }
    */

    public int getVerboseLevel() { return this.verboseLevel; }

    public void setVerboseLevel(int n) { this.verboseLevel = n; }
}
