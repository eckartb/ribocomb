/** Factory class is responsible for reading/parsing a PDB file and creating
 *  a corresponding Object3DGraph
 */
package rnadesign.rnamodel;

import generaltools.StringTools;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.Logger;

import chemistrytools.DefaultChemicalElement;

import rnasecondary.Interaction;
import rnasecondary.InteractionType;
import rnasecondary.RnaInteractionType;
import rnasecondary.SimpleInteraction;
import tools3d.Vector3D;
import tools3d.objects3d.SimpleLink;
import tools3d.objects3d.Link;
import tools3d.objects3d.LinkSet;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DFactory;
import tools3d.objects3d.Object3DIOException;
import tools3d.objects3d.Object3DSet;
import tools3d.objects3d.SimpleLinkSet;
import rnadesign.rnamodel.NucleotideTools;

import static rnadesign.rnamodel.PackageConstants.*;

/**
 * @author Eckart Bindewald
 *
 */
public abstract class AbstractPdbReader implements Object3DFactory {

    protected static Logger log = Logger.getLogger("NanoTiler_debug");

    public static final char BAD_RESIDUE_CHAR = 'X';

    public static final double COLLISION_DISTANCE = 0.7;

    public static final int EXPECT_NONSTANDARD_NUCLEOTIDES = 1;

    public static final int EXPECT_STANDARD_NUCLEOTIDES = 2;

    public static final int PRECISION = 3;

    public static final int NO_TRANSLATION = 0;

    public static final int AMBER_TO_STANDARD = 1;

    public static char defaultStrandName = 'A'; // name of strand instead of "space" character
    public static char defaultAltStrandName = 'a'; // if more than 26 chains, use small letters
    public static char defaultAlt2StrandName = '!'; // if more than 26 chains, use small letters
    public static char defaultAlt3StrandName = '\200'; // worst case; octal 200 is decimal 128

    protected int translationMode = AMBER_TO_STANDARD; // NO_TRANSLATION;

    protected int readMode = EXPECT_STANDARD_NUCLEOTIDES;

    protected void addStrandBackboneLinks(NucleotideStrand obj, LinkSet links) {
	for (int i = 1; i < obj.size(); ++i) {
	    if ((obj.getChild(i-1) instanceof Nucleotide3D) && (obj.getChild(i) instanceof Nucleotide3D)) {
		InteractionType interactionType = new RnaInteractionType(RnaInteractionType.BACKBONE);
		Interaction interaction = new SimpleInteraction((Nucleotide3D)(obj.getChild(i-1)), (Nucleotide3D)(obj.getChild(i)), interactionType);
		InteractionLink link = new InteractionLinkImp(obj.getChild(i-1), obj.getChild(i), interaction);
		links.add(link);
		// log.fine("Adding link between residues " + i + " and " + (i+1));
	    }
	    else {
		log.finest("child nodes " + i + " and " + (i+1) + " are not of type Nucleotide3D!");
	    }
	}
    }

    protected void addBackboneLinks(Object3D obj, LinkSet links) {
	if (obj instanceof NucleotideStrand) {
	    addStrandBackboneLinks((NucleotideStrand)obj, links);
	}
	if ((obj instanceof Residue3D) || (obj instanceof Atom3D)) {
	    return; // terminate search
	}
	// recursive call
	for (int i = 0; i < obj.size(); ++i) {
	    addBackboneLinks(obj.getChild(i), links);
	}
    }

    protected void addCovalentAtomLinks(NucleotideStrand obj, LinkSet links){
	// System.out.println("Starting addCovalentAtomLinks() in reader class");
	for(int k = 0; k < obj.getResidueCount(); k++){
	    Nucleotide3D nuc1 = (Nucleotide3D)obj.getResidue3D(k);
	    if(k >0){
		Nucleotide3D nuc2 = (Nucleotide3D)obj.getResidue3D(k-1);
		NucleotideTools.addCovalentAtomLinksFast(nuc1, nuc2, links);
	    }
	    else{
		NucleotideTools.addCovalentAtomLinksFast(nuc1,links);
	    }
	    /*	    for(int i = 0; i < nuc1.getAtomCount();i++){
		Atom3D atom1 = nuc1.getAtom(i);
		for(int n=i+1; n < nuc1.getAtomCount(); n++){
		    Atom3D atom2 = nuc1.getAtom(n);
		    if(NucleotideTools.isCovalentlyBonded(nuc1,atom1,atom2)){
			Link link = new SimpleLink(atom1, atom2);
			links.add(link);
		    }
		}
	    }
	    if(k > 0){
		Nucleotide3D nuc2 = (Nucleotide3D)obj.getResidue3D(k-1);
		Object3D obj1 = nuc1.getChild("P");
		Object3D obj2 = nuc2.getChild("O3*");
		if(obj1 != null  && obj2 != null){
		    Link specialLink = new SimpleLink(obj1,obj2);
		    links.add(specialLink);
		}
		}*/ 
	}
	// System.out.println("Finished addCovalentAtomLinks() in reader class");
    }

    protected void addCovalentLinks(Object3D obj, LinkSet links){
	if(obj instanceof NucleotideStrand){
	    addCovalentAtomLinks((NucleotideStrand)obj, links);
	}
	if ((obj instanceof Residue3D) || (obj instanceof Atom3D)) {
	    return; // terminate search
	}
	// recursive call
	for (int i = 0; i < obj.size(); ++i) {
	    addCovalentLinks(obj.getChild(i), links);
	}

    }


    /** convinience method that counts number of atom records in PDB file */
    public static int countPdbAtoms(String[] lines) {
	int count = 0;
	for (int i = 0; i < lines.length; ++i) {
	    if (lines[i].indexOf("ATOM") == 0) {
		++count;
	    }
	}
	return count;
    }

    /** convinience method that counts number of atom records in PDB file */
    public static int countPdbAtoms(String fileName) throws IOException {
	FileInputStream fis = new FileInputStream(fileName);
	String[] allLines = StringTools.readAllLines(fis);
	return countPdbAtoms(allLines);
    }

    /** returns keyword of PDB line like HETATM, ATOM or REMARK */
    protected static String getKeyword(String line) {
	if (line != null) {
	    if (line.length() > 6) {
		return line.substring(0,6).trim();
	    }
	    else {
		return line.trim(); // TODO not entirely clean
	    }
	}
	return null;
    }

    public int getReadMode() { return readMode; }

    /** returns true if TER keyword found */
    protected boolean isTer(String line) {
	String line2 = line.trim();
	return line2.equals("TER");
    }

    /** returns true if ENDML keyword found */
    protected boolean isEndMdl(String line) {
	String line2 = line.trim();
	return line2.equals("ENDMDL");
    }

    /** returns true if REMARK keyword found */
    protected boolean isRemark(String line) {
	return "REMARK".equals(getKeyword(line));
    }

    /** returns true if current line starts with keyword "ATOM" */
    protected static boolean isAtom(String line) {
	if (line.length() < 50) {
	    return false;
	}
	String keyword = getKeyword(line);
	return (keyword.equals("ATOM"));
    }

    protected char getStrandName(String line) {
	return line.charAt(21);
    }

    protected String getResidueName(String line) {
	String result = line.substring(17, 17+3).trim();
	switch (translationMode) {
	case NO_TRANSLATION:
	    break;
	case AMBER_TO_STANDARD:
	    result = translateAmberToStandardResidueName(result);
	    break;
	}
	return result;
    }

    /** translates an atom name from Amber nomenclature to standard
     * nomenclature.
     * replaces ' character to * character.
     */
    protected String translateAmberToStandardAtomName(String s) {
	return s.replace('\'', '*');
    }

    /** translates an atom name from Amber nomenclature to standard
     * nomenclature.
     * changes "RC" to "C" or "RC5" to "C"
     */
    protected String translateAmberToStandardResidueName(String s) {
	char lastChar = s.charAt(s.length()-1);
	if ((s.length() > 1) && ((lastChar == '3') || (lastChar == '5'))) { // remove trailing 3 or 5 
	    s = s.substring(0, s.length()-1);
	}
	if ((s.length() > 1) && (s.charAt(0) == 'R')) { // remove leading "R"
	    return s.substring(1,2);
	}
	return s;
    }

    /** parses out chemical element. TODO : use optional identifier at end of line if available */
    protected String getAtomElementName(String line) {
	String result = line.substring(13, 14).trim();
	return result;
    }

    protected String getAtomName(String line) {
	String result = line.substring(12, 12+4).trim();
	switch (translationMode) {
	case NO_TRANSLATION:
	    break;
	case AMBER_TO_STANDARD:
	    result = translateAmberToStandardAtomName(result);
	    break;
	}
	return result;
    }

    /** returns typically space or 'A', or 'B' */
    protected char getVersionChar(String line) {
	return line.charAt(16);
    }

    protected int getResidueId(String line) {
	// log.finest("Calling getResidueId: " + line);
	String word = line.substring(22, 22+4).trim();
	return Integer.parseInt(word);
    }

    /** returns "A" for residue with id 190A */
    protected char getResidueVersionChar(String line) {
	// log.finest("Calling getResidueId: " + line);
	if (line.length() <= 26) {
	    return ' ';
	}
	return line.charAt(26);
    }

    protected int getAtomCounter(String line) {
	return Integer.parseInt(line.substring(7, 7+4).trim());
    }

    protected static double getX(String line)
    {
	return Double.parseDouble(line.substring(30, 30 + 5+PRECISION).trim());
    }

    protected static double getY(String line)
    {
	// old start value: 38
	return Double.parseDouble(line.substring(35+PRECISION, 
						 35 + PRECISION + 5+PRECISION).trim());
    }

    protected static double getZ(String line)
    {
	// old start value 46
	int startPos = 40+(2*PRECISION);
	int endPos = startPos + 5+PRECISION;
	if (endPos > line.length()) {
	    log.severe("Bad ATOM record encountered: " + NEWLINE + line);
	    assert false;
	}
	return Double.parseDouble(line.substring(startPos, endPos).trim());
    }

    /** generates new Atom3D object from PDB line */
    protected Atom3D generateAtomFromPdb(String line) {
	Vector3D pos = new Vector3D(getX(line), getY(line), getZ(line));
	String name = getAtomName(line);
	//	int residueId = getResidueId(line);
	//	String residueName = getResidueName(line);
	//	char strandName = getStrandName(line);
	Atom3D atom = new Atom3D();
	atom.setPosition(pos);
	atom.setName(name);
	
	DefaultChemicalElement element = new DefaultChemicalElement(getAtomElementName(line));
	atom.setElement(element);
	//	atom.setResidueId(residueId);
	//	atom.setResidueName(residueName);
	//	atom.setStrandName(strandName);
	return atom;
    }

    /** returns positions of ATOMS. Ignores HETATM records. */
    public static Vector3D[] readPdbVectors(String[] lines) {
	int numAtoms = countPdbAtoms(lines);
	Vector3D[] result = new Vector3D[numAtoms];
	int pc = 0;
	for (int i = 0; i < lines.length; ++i) {
	    if (isAtom(lines[i])) {
		result[pc++] = new Vector3D(getX(lines[i]), getY(lines[i]), getZ(lines[i]));
	    }
	}
	return result;
    }

    /** returns one-letter character (ACGUT) from possible three letter code */
    protected char getStandardResidueCharacter(String residueName) {
	assert residueName.length() > 0;
	char badChar = BAD_RESIDUE_CHAR;
	int length = residueName.length();
	char result = badChar;
	if (Character.isDigit(residueName.charAt(residueName.length()-1))) {
	    return residueName.charAt(0); // convert for example U9 into U
	}
	switch (length) {
	case 1: result = residueName.charAt(0);
	    break;
	case 2: // take care of weird notation like "+G" for G
	    if ((residueName.charAt(0) == '+') || (residueName.charAt(0) == 'R')) {
		result = residueName.charAt(1);
	    }
	    break;
	case 3: 
	    if (residueName.substring(1,3).equals("3\'")) {
		result = residueName.charAt(0);
	    }
	    if (residueName.substring(1,3).equals("5\'")) {
		result = residueName.charAt(0); // used for output of DSViewer PRO
	    }
	    if (residueName.equals("ADE")) {
		result = 'A';
	    }
	    else if (residueName.equals("CYT")) {
		result = 'C';
	    }
	    else if (residueName.equals("GUA")) {
		result = 'G';
	    }
	    else if (residueName.equals("THY")) {
		result = 'T';
	    }
	    else if (residueName.equals("URA")) {
		result = 'U';
	    }
// 	    else if (residueName.equals("PSU")) {
// 		result = 'U';
// 	    }
// 	    else if (residueName.equals("2MG")) {
// 		result = 'G';
// 	    }
	    break;
	default:
	    return residueName.charAt(0);
	}
	return result;
    }

    /** returns one-letter character (ACGUT) from possible three letter code */
    protected char getResidueCharacter(String residueName) {
	char badChar = BAD_RESIDUE_CHAR;
	int length = residueName.length();
	char result = getStandardResidueCharacter(residueName);
	if ((result == BAD_RESIDUE_CHAR) && (readMode == EXPECT_NONSTANDARD_NUCLEOTIDES)) {
	    char c = residueName.charAt(residueName.length()-1);
	    if ((c == 'A') || (c == 'C') || (c == 'G') || (c == 'U') || (c == 'T')) {
		result = c; // example: YG
	    }
	}
	if ((result == 'B')) {
	    log.severe("Bad residue character B generated from " + residueName);
	}
	return result;
    }

    /** Returns true if residue name is RNA or DNA */
    protected boolean isLegalResidueName(String residueName) {
	return (getResidueCharacter(residueName) != BAD_RESIDUE_CHAR);
    }

    public int getTranslationMode() { return translationMode; }

    protected static double findMinimumDistance(Object3D obj, Object3DSet objSet) {
	assert (objSet.size() > 0);
	Vector3D pos = obj.getPosition();
	double dBest = pos.distance(objSet.get(0).getPosition());
	for (int i = 1; i < objSet.size(); ++i) {
	    double d = pos.distance(objSet.get(i).getPosition());
	    if (d < dBest) {
		dBest = d;
	    }
	}
	return dBest;
    }

    /** returns default strand name for n'th strand (used when pdb was has no strand name) */
    protected char getDefaultStrandName(int strandCounter) {
	// log.info("getDefaultStrandName input counter: " + strandCounter);
	char strandName = defaultStrandName; // use upper letter characters
	if (strandCounter >= 82 ) { // worst case
	    strandName = defaultAlt3StrandName;
	    strandCounter -= 82;
	    // log.info("case 1");
	}
	else if (strandCounter >= 52) { // use characters like !,(,; ...
	    strandName = defaultAlt2StrandName;
	    strandCounter -= 52;
	    // log.info("case 2");
	} 
	else if (strandCounter >= 26) { // use lower letter characters
	    strandName = defaultAltStrandName;
	    strandCounter -= 26;
	    // log.info("case 3");
	}
	// log.info("Using strandname and counter: " + strandName + " " + strandCounter);
	return (char)(((int)strandName) + strandCounter);
    }

    /** Returns next character in unicode list */
    char incChar(char c) {
	return (char)(((int)c) + 1); // convert to integer, increase, convert back to character
    }

    /** Tries to squeeze as much as possible out of PDB chain numbering.
     * converts: " " -> A, A->B ... Z->a, a->b, z->0, 0->1, 9->A
     */
    char incStrandChar(char c) {
	if (c == ' ') {
	    return 'A';
	}
	if ((c >= 'A') && (c < 'Z')) {
	    return incChar(c);
	}
	else if (c == 'Z') {
	    return '0';
	}
	else if ((c >= '0') && (c < '9')) {
	    return incChar(c);
	}
	log.warning("Too many strands, starting over at strand name A");
	return 'A'; // start over
    }

    /** Tries to squeeze as much as possible out of PDB chain numbering.
     * converts: " " -> A, A->B ... Z->A
     */
    char incStrandCharAToZ(char c) {
	if ((c >= 'A') && (c < 'Z')) {
	    return incChar(c);
	}
	return 'A'; // start over
    }

    /** returns new strand name when a strand with this name alread exists. */
    protected String getHigherOrderStrandName(String strandName) {
	assert strandName != null && strandName.length() > 0;
	if ((strandName.length() == 1) && (!strandName.equals("9"))) {
	    return "" + incStrandChar(strandName.charAt(0));
	}
	if (strandName.equals("9")) {
	    return "A2"; // special case
	}
	// start adding number if not yet existent:
	if (Character.isLetter(strandName.charAt(strandName.length()-1))) {
	    return strandName + "2";
	    // 	    // first try to convert to lower case:
	    // 	    String lower = strandName.toLowerCase();
	    // 	    if (lower.equals(strandName)) { // was already lower case
	    // 		return strandName.toUpperCase() + "2";
	    // 	    }
	    // 	    return lower;
	}
	int numberIndex = StringTools.indexOfDigit(strandName);
	// assert ((numberIndex > 0) && (numberIndex < strandName.length()));
	assert (numberIndex < strandName.length());
	String base = strandName.substring(0, numberIndex);
	String rest = strandName.substring(numberIndex, strandName.length());
	if ((base.length() == 1)) {
	    char c = base.charAt(0);
	    if ((c >= 'A') && (c <= 'Z')) {
		if (c < 'Z') {
		    c = incStrandCharAToZ(c);
		    return "" + c + rest;
		}
		else {
		    base = "A";
		}
	    }
	    else {
		log.severe("Unexpected strand character: " + c);
		assert false; // should never happen
	    }
	}
	int num = 0;
	try {
	    num = Integer.parseInt(rest);
	}
	catch (NumberFormatException nfe) {
	    log.warning("Could not parse strandname: " + strandName);
	    assert false;
	}
	if ((base.length() == 0) && (num == 9)) { // name is only number, finally use 2 digits
	    return "A2";
	}
	return base + (num+1);
    }
 
   /** reads and creates Vector3D in format (Vector3D x y z ) */
    public static Vector3D readVector3D(DataInputStream dis) throws Object3DIOException {
	// log.fine("starting readVector!");	
	String word = readWord(dis);
	double x = 0;
	double y = 0;
	double z = 0;
	try {
	    x = Double.parseDouble(word);
	}
	catch (NumberFormatException e) {
	    throw new Object3DIOException("Could not parse x coordinate for Vector3D " + word);
	}
	word = readWord(dis);
	try {
	    y = Double.parseDouble(word);
	}
	catch (NumberFormatException e) {
	    throw new Object3DIOException("Could not parse y coordinate for Vector3D " + word);
	}	
	word = readWord(dis);
	try {
	    z = Double.parseDouble(word);
	}
	catch (NumberFormatException e) {
	    throw new Object3DIOException("Could not parse z coordinate for Vector3D " + word);
	}
	// log.finest("ending readVector!");
	return new Vector3D(x, y, z);	
    }

    /** reads a single word from data stream */
    public static String readWord(DataInputStream dis) {
	String s = new String("");
	char c = ' ';
	
	// first skip white space
	do {
			try {
			   c = (char)dis.readByte();
			}
			catch (IOException e) {
				break; // end of file reached
			}
		}
		while (Character.isWhitespace(c));
		
		if (!Character.isWhitespace(c)) {
			s = s + c;
		}
		else {
		    //log.finest("found word: " + s);
			return s;
		}
		
		while (true) {
			try {
			   c = (char)dis.readByte();
			 if (!Character.isWhitespace(c)) {
				 s = s + c;
			 }
			 else {
				 break;
			 }
			}
			catch (IOException e) {
				break; // end of file reached
			}
		}
		//log.finest("found word: " + s);
		return s;
	}

    public void setReadMode(int mode) { this.readMode = mode; }

    public void setTranslationMode(int mode) { translationMode = mode; }

    public LinkSet getLinkSet() { assert false; return new SimpleLinkSet(); }



}
