package rnadesign.rnamodel;

import chemistrytools.ChemicalElement;
import chemistrytools.PeriodicTableImp;
import java.io.*;
import tools3d.Vector3D;
import tools3d.objects3d.*;
import generaltools.StringTools;
import freeware.PrintfFormat; // used in CDK package 
import java.util.logging.*;
import java.util.*;

import static rnadesign.rnamodel.PackageConstants.*;

public abstract class AbstractPdbWriter implements Object3DWriter {

    public static final int RESIDUE_PDB_NUMBER = 1;
    public static final int RESIDUE_RECOUNTED_NUMBER = 2;
    public static final int RESIDUE_NAMED_NUMBER = 3;
    public static final int RESIDUE_TOTAL_COUNT = 4;

    public static final int DIALECT_DEFAULT = 1;
    public static final int DIALECT_SIGNATURE = 2;
    public static final String REMARK_HEADER = "REMARK";    
    protected int dialect = DIALECT_DEFAULT;
    protected Object3DTester objectTester = null; // if not null, pass "check" test for all objects to be written
    protected int lineCounter = 1;
    protected boolean junctionMode = true; // false; // output of Junctions and BranchDescriptors?
    protected boolean remarkMode = false; // if true, write properties as REMARK entries
    public static boolean debugMode = false;
    protected int originalMode = RESIDUE_RECOUNTED_NUMBER; // RESIDUE_PDB_NUMBER; // if true, use original number and chain naming of PDB file
    /** converts atom name to PDB 4 letter name.
     * TODO : not totally correct for special cases, for
     * example CL (chlorine) has format CL__ instead of _CL_ */
    private static String atomNameToPdb(String name) {
	String result = name;
	if (name.length() > 4) {
	    result = name.substring(0, 4);
	}
	else if (name.length() < 4) {
	    result = " " + name;
	    // fill with spaces from right until length 4 is reached:
	    result = StringTools.fillRight(result, 4, " "); 
	}
	return result;
    }

    public Object3DTester getObjectTester() { return objectTester; }

    public void setObjectTester(Object3DTester tester) { this.objectTester = tester; }

    public void setRemarkMode(boolean b) { this.remarkMode = b; }

    /** sets mode of oringinal PDB numbering on or off */
    public void setOriginalMode(int mode) {
	this.originalMode = mode;
    }

    /** iff true, writes out junctions */
    public void setJunctionMode(boolean b) { this.junctionMode = b; }

    /** write record to PDB file.
     * some lines of code taken from CDK package.
     * recordName = "ATOM  " or "HETATM" */
    private String writeAtom(String atomName, 
			     double x, double y, double z, String residueName,
			     char strandChar, int residueCount, String recordName,
			     ChemicalElement element) {
	//	String result = "ATOM "  + lineCounter + " " + atomName;
// 	if (isdigit(aName[0]) && (aName.size() < 4)) {
// 	    printChar(os,' ',5-aName.size());
// 	}
// 	else {
// 	    printChar(os,' ',4-aName.size());
// 	}
	// result = result + " " + residueName + " " + strandChar + " " + residueCount + " "
	// + x + " " + y + " " + z;
	String result;
	// String recordName = "ATOM  ";
	PrintfFormat serialFormat = new PrintfFormat("%5d");
	PrintfFormat residueCountFormat = new PrintfFormat("%4d");
	// PrintfFormat atomNameFormat = new PrintfFormat("%-4s");
	// PrintfFormat residueNameFormat = new PrintfFormat("%-3s");
	PrintfFormat positionFormat = new PrintfFormat("%8.3f");
	if (dialect == DIALECT_SIGNATURE) {
	    positionFormat = new PrintfFormat("%10.5f");
	}
	StringBuffer buffer = new StringBuffer();
	buffer.setLength(0);
	buffer.append(recordName);
	buffer.append(serialFormat.sprintf(lineCounter++));
	buffer.append(' ');
	if (dialect == DIALECT_SIGNATURE) {
	    buffer.append(" " + atomName + "    " );
	}
	else {
	    buffer.append(atomNameToPdb(atomName));
	}
	buffer.append(' ');
	buffer.append(StringTools.fillLeft(residueName, 3, " "));
	buffer.append(' ');
	buffer.append(strandChar);
	if (dialect == DIALECT_SIGNATURE) {
	    buffer.append(' ');
	}
	buffer.append(residueCountFormat.sprintf(residueCount));
	if (dialect == DIALECT_SIGNATURE) {
	    buffer.append(" ");
	}
	else {
	    buffer.append("    ");
	}
	// buffer.append('          ');
	buffer.append(positionFormat.sprintf(x));
	buffer.append(positionFormat.sprintf(y));
	buffer.append(positionFormat.sprintf(z));
	if (dialect == DIALECT_DEFAULT) {
	    buffer.append("  1.00  0.00  ");
	}
	else if (dialect == DIALECT_SIGNATURE) {
	    buffer.append(" " + element.getShortName() + "_     "
			  + PeriodicTableImp.findDefaultValency(element) 
			  + " 0  0.00000");
	}
	result = buffer.toString();
	// os << molName << " " << chain << setw(4) << molNum
	// 	<< setw(9+precision) << setprecision(precision) << v.x()
	// 	<< setw(5+precision) << setprecision(precision) << v.y()
	// 	<< setw(5+precision) << setprecision(precision) << v.z() 
	// 	   << setw(4+2) << setprecision(2) << val1
	// 	   << setw(4+2) << setprecision(2) << val2
	// 	   << "           " << a.getChemicalElementChar();
	return result;
    }

    protected List<String> extractRemarks(Object3D node) {
	Properties properties = node.getProperties();
	List<String> result =  new ArrayList<String>();
	if (properties != null) {
	    Enumeration emu = properties.propertyNames();
	    while (emu.hasMoreElements()) {
		 String s = (String)(emu.nextElement());
		 if (s.startsWith(REMARK_HEADER)) {
		     result.add(REMARK_HEADER + "    " + properties.getProperty(s) + SLASH);
		 }
	    }
	}
	return result;
    }

    protected List<String> extractPropertiesAsRemarks(Object3D node) {
	Properties properties = node.getProperties();
	List<String> result =  new ArrayList<String>();
	if (properties != null) {
	    Enumeration emu = properties.propertyNames();
	    while (emu.hasMoreElements()) {
		 String s = (String)(emu.nextElement());
		 result.add(REMARK_HEADER + "    " + s + "=" + properties.getProperty(s) + SLASH);
	    }
	}
	return result;
    }
    
    /** write ATOM record to PDB file.
     * some lines of code taken from CDK package */
    private String writeAtom(String atomName, 
			     double x, double y, double z, String residueName,
			     char strandChar, int residueCount,
			     ChemicalElement element) {
	return writeAtom(atomName, x, y, z, residueName, strandChar, residueCount, "ATOM  ",
			 element);
    }

    /** write HETATM record to PDB file. */
    private String writeHeteroAtom(String atomName, 
				   double x, double y, double z, String residueName,
				   char strandChar, int residueCount,
				   ChemicalElement element) {
	return writeAtom(atomName, x, y, z, residueName, strandChar, residueCount, "HETATM",
			 element);
    }

    public String writeAtom(Atom3D atom, 
			     String residueName, char strandChar, 
			     int residueCount) {
	Vector3D pos = atom.getPosition();
	double x = pos.getX();
	double y = pos.getY();
	double z = pos.getZ();
	atom.setProperty("pdb_atom_id", ""+lineCounter); // workaround for storing the id of the atom in the pdb file
	return writeAtom(atom.getName(), x, y, z, residueName, strandChar, residueCount,
			 atom.getElement());
    }

    public String writeHeteroAtom(Atom3D atom, 
			   String residueName, char strandChar, 
			   int residueCount) {
	Vector3D pos = atom.getPosition();
	double x = pos.getX();
	double y = pos.getY();
	double z = pos.getZ();
	atom.setProperty("pdb_atom_id", ""+lineCounter);
	return writeHeteroAtom(atom.getName(), x, y, z, residueName, strandChar, residueCount,
			       atom.getElement());
    }


}
