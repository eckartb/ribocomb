package rnadesign.rnamodel;

import java.io.*;
import java.util.*;
import java.util.logging.*;
import tools3d.Vector3D;
import tools3d.objects3d.*;
import generaltools.StringTools;
import freeware.PrintfFormat; // used in CDK package 

import static rnadesign.rnamodel.PackageConstants.*;

/** writes PDB file (a set of RNA or protein strands)
 */
public class AmberPdbWriter extends AbstractPdbWriter {

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    public static final String PDB_CHAR_PROPERTY = "pdb_chain_char";

    public static final char DEFAULT_STRAND_CHAR = ' ';
    private char strandChar = DEFAULT_STRAND_CHAR;
    private int currentResidueOutputId = 1;
    private int residueCount = 1;
    private int formatId = Object3DFormatter.PDB_FORMAT;

    public AmberPdbWriter() { reset(); }

    public int getFormatId() { return formatId; }


 
    public void reset() { 
	lineCounter = 1;
	resetStrandChar();
	residueCount = 1;
	currentResidueOutputId = 1;
    }
   
    public void resetStrandChar() {
	strandChar = ' ';
    }

    public void write(OutputStream os, Object3D node) {
	PrintStream pw = new PrintStream(os);
	// pw.println("Starting AmberPdbWriter.write");
	pw.println(writeString(node));
	// pw.println("Finished AmberPdbWriter.write");
    }

    public char getStrandChar() { return strandChar; }

    public static char getOriginalStrandChar(BioPolymer strand) {
	Properties properties = strand.getProperties();
	if (properties != null) {
	    String chainString = properties.getProperty(PDB_CHAR_PROPERTY);
	    if ((chainString != null) && (chainString.length() ==1)) {
		return chainString.charAt(0);
	    }
	}
	return DEFAULT_STRAND_CHAR;
    }

    public char getStrandChar(NucleotideStrand strand) {
	if ((originalMode == RESIDUE_PDB_NUMBER) && (strand != null)) {
	    return getOriginalStrandChar(strand);
	}
	// otherwise use overwritten strand character
	return strandChar;
    }

    public static char nextStrandChar(char c) {
	return ' ';
    }

    /** helper method that returns increasedstrand character. */
     public void incStrandChar() {
	 assert false;
     }

    /** allows for different output renumbering modes */
    private int getResidueOutputNumber(Nucleotide3D nuc, int currentCount) {
	int result = 0;
	switch (originalMode) {
	case RESIDUE_PDB_NUMBER:
	    result = nuc.getResidueNumberInPdb(); // _CHARgetAssignedNumber(); // number from pdb file
	    break;
	case RESIDUE_RECOUNTED_NUMBER:
	    result = nuc.getPos() + 1;
	    break;
	case RESIDUE_NAMED_NUMBER:
	    result = nuc.getResidueNumberInName();
	    break;
	case RESIDUE_TOTAL_COUNT:
	    result = currentResidueOutputId;
	    break;
	default:
	    assert false; // unknown mode
	}
	return result;
    }

    private String writeNucleotide(Nucleotide3D nuc, char strandChar, String lastChar) {
	String result = "";
	// String residueName = getThreeLetterString(nuc.getName()); // TODO : better solution!
	// TODO : residuename might be G12 for G nucleotide 12 ... 
	char nucChar = nuc.getSymbol().getCharacter();
// 	StringBuffer buffer = new StringBuffer();
// 	buffer.setLength(0);
// 	buffer.append(nucChar);
	String residueName = "R" + nucChar + lastChar; 

// 	new String(buffer);
// 	if (residueName.length() > 1) {
// 	    residueName = residueName.substring(0, 1);
// 	}
	
	if (nuc.getAtomCount() > 0) {
	    for (int i = 0; i < nuc.getAtomCount(); ++i) {
		result = result + writeAtom(nuc.getAtom(i), 
				   residueName, strandChar, residueCount) 
		    + ENDL;
	    }

	}
	else { 
	    // write one "C4'" atom for each nucleotide if no atoms defined
	    // using C4' because currenlty no other atoms are defined
	    Atom3D atom = new Atom3D(nuc.getPosition());
	    atom.setName("C4*");
	    result = writeAtom(atom, residueName, strandChar, residueCount)
		+ ENDL;
	}
	return result;
    }

    private String writeAminoAcid(AminoAcid3D aa, char strandChar) {
	String result = "";
	// String residueName = getThreeLetterString(aa.getName()); // TODO : better solution!
	// TODO : residuename might be G12 for G aaleotide 12 ... 
	char aaChar = aa.getSymbol().getCharacter();
	String residueName = Protein3DTools.oneLetterToThreeLetter(aaChar);
	if (residueName.length() > 1) {
	    residueName = residueName.substring(0, 1);
	}
	if (aa.getAtomCount() > 0) {
	    for (int i = 0; i < aa.getAtomCount(); ++i) {
		result = result + writeAtom(aa.getAtom(i), 
					    residueName, strandChar, residueCount) 
		    + ENDL;
	    }

	}
	else { // write one "C" atom for each residue if no atoms defined
	    Atom3D atom = new Atom3D(aa.getPosition());
	    atom.setName("C");
	    result = writeAtom(atom, residueName, strandChar, residueCount)
		+ ENDL;
	}
	return result;
    }

    /** TODO : problem with incStrandChar */
    public String writeStrand(NucleotideStrand strand, char strandChar, int startPos, int endPos) {
	log.fine("Writing nucleotide strand with character: " + strandChar);
	String result = ""; // "REMARK new strand " + ENDL;
	for (int i = startPos; i < endPos; ++i) {
	    String lastChar = "";
	    if (i == startPos) {
		lastChar = "5";
	    } else if (i == (endPos - 1)) {
		lastChar = "3";
	    }
	    result = result + writeNucleotide((Nucleotide3D)(strand.getResidue(i)), strandChar, lastChar);
	    residueCount++;
	}
	result = result + "TER                                          " + ENDL;
	log.fine("Finished writing nucleotide strand with character: " + strandChar);
	incStrandChar();
	return result;
    }

    public String writeStrand(NucleotideStrand strand, char strandChar) {
	return writeStrand(strand, strandChar, 0, strand.getResidueCount());
    }

    private String writeProteinStrand(ProteinStrand strand, char strandChar) {
	String result = "";
	for (int i = 0; i < strand.getResidueCount(); ++i) {
	    result = result + writeAminoAcid((AminoAcid3D)(strand.getResidue(i)), strandChar);
	    residueCount++;
	}
	result = result + "TER                                          " + ENDL;
	incStrandChar();
	return result;
    }

    /** writes PDB file. TODO : write protein, write REMARKS */
    private String writeStringInternal(Object3D node) {
	StringBuffer result = new StringBuffer();
	if (remarkMode) {
	    // result.append("remarks: ");
	    List<String> remarks = extractPropertiesAsRemarks(node); 
	    for (String s : remarks) {
		result.append(s + ENDL);
	    }
	    // result.append(" endremarks.");
	    // return node.toString();
	}
	if ((objectTester != null) && (!objectTester.check(node))) {
	    return ""; // custom test failed!
	}
	if (node instanceof NucleotideStrand) {
	    NucleotideStrand strand = (NucleotideStrand)node;
	    return writeStrand(strand, getStrandChar(strand));
	}
	else if (node instanceof ProteinStrand) {
	    return writeProteinStrand((ProteinStrand)node, strandChar); // TODO : orig strand character mode not working yet
	}
	else if (node instanceof Nucleotide3D) {
	    Nucleotide3D nucleotide = (Nucleotide3D)node;
	    Object3D parent = nucleotide.getParent();
	    NucleotideStrand strand = null;
	    if (parent instanceof NucleotideStrand) {
		strand = (NucleotideStrand)parent;
	    }
	    return writeNucleotide(nucleotide, getStrandChar(strand), ""); // FIXIT residueCount?
	    
	}
	else if (node instanceof AminoAcid3D) {
	    return writeAminoAcid((AminoAcid3D)node, strandChar);
	}
	else if (node instanceof Atom3D) {
	    NucleotideStrand strand = null;
	    Object3D parent = node.getParent();
	    if (parent != null) {
		Object parent2 = parent.getParent();
		if (parent2 instanceof NucleotideStrand) {
		    strand = (NucleotideStrand)parent2;
		}
	    }
	    return writeAtom((Atom3D)node, "UNK", getStrandChar(strand), 1) + ENDL;
	}

	if ((!junctionMode) && ((node instanceof StrandJunction3D) || (node instanceof BranchDescriptor3D))) {
	    return result.toString(); // do not write strand junctions!
	}
	for (int i = 0; i < node.size(); ++i) {
	    result = result.append(writeStringInternal(node.getChild(i)));
	} 
	return result.toString();
    }

    /** writes PDB file. TODO : write protein, write REMARKS */
    public String writeString(Object3D node) {
	reset();
	if (debugMode) {
	    Object3DSet strandSet = Object3DTools.collectByClassName(node, "RnaStrand");
	    System.out.println("Starting AmberPdbWriter for RNA strands:");
	    for (int i = 0; i < strandSet.size(); ++i) {
		System.out.println(strandSet.get(i).getFullName());
	    }
	}
	return writeStringInternal(node);
    }
    
    /** No writing of bonds or links for current version of PDB writer */
    public void write(OutputStream os, LinkSet links) {
	PrintStream pw = new PrintStream(os);
	// pw.println(links.toString());
	// 	pw.println("Starting AmberPdbWriter.write(os, links)");
	pw.println("");
    }

}
