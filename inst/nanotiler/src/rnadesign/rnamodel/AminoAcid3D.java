package rnadesign.rnamodel;

import java.io.InputStream;

import sequence.LetterSymbol;
import sequence.Sequence;
import sequence.Residue;
import tools3d.Vector3D;
import tools3d.objects3d.SimpleObject3D;
import java.util.List;
import java.util.ArrayList;

import static rnadesign.rnamodel.PackageConstants.*;

/** represents RNA or DNA base */
public class AminoAcid3D extends SimpleObject3D implements Residue3D {

    private String assignedName = "";

    private int assignedNumber;

    // private Sequence sequence;

    private int pos; // position in sequence (n'th nucleotide)

    private final String CLASS_NAME = "AminoAcid3D";

    private LetterSymbol symbol;

    /** please use with care, still needs to be tested. object is not valid, but still useful for nucleotide */
    public AminoAcid3D() {
	assignedNumber = 0;
	pos = 0;
    }

    public AminoAcid3D(LetterSymbol symbol, int pos, Vector3D position) {
	super(position);
	this.symbol = symbol;
	// this.sequence = sequence;
	this.pos = pos;
	assignedNumber = 0;
    }

    public Object clone() {
	assert false;
	return new AminoAcid3D(symbol, pos, getRelativePosition());
    }

    /** returns assigned number (like residue number occuring in PDB file */
    public int getAssignedNumber() { return assignedNumber; }

    public String getAssignedName() { return assignedName; }

    /** returns number of child nodes that are atoms */
    public int getAtomCount() {
	return size(); // TODO !!!
    }

    /** returns n'th atom child */
    public Atom3D getAtom(int n) {
	return (Atom3D)(getChild(n)); // TODO !!!
    }

    public String getClassName() { return CLASS_NAME; }

    /** returns order of covalent bond or zero if atoms not bonded. TODO: fully implement */
    public int getCovalentBondOrder(Atom3D atom1, Atom3D atom2) {
	assert false;
	return -1;
    }

    /** returns list of atoms that are covalently bonded with this atom */
    public List<Atom3D> getCovalentlyBondedAtoms(Atom3D atom) {
	List<Atom3D> resultAtoms = new ArrayList<Atom3D>();
	AminoAcid3D res = (AminoAcid3D)(atom.getParent());
	for (int j = 0; j < res.size(); ++j) {
	    Atom3D atomOther = (Atom3D)(getChild(j));
	    if ((atom != atomOther) && (getCovalentBondOrder(atom, atomOther) > 0)) {
		resultAtoms.add(atomOther);
	    }
	}
	return resultAtoms;
    }

    public Object getParentObject() { return getParent(); }

    public LetterSymbol getSymbol() {
	return symbol;
    }


    /** Returns residue number as given in PDB file, or default if not defined. */
    public int getResidueNumberInPdb() {
	String prop = getProperty(PDB_RESIDUE_ID);
	if (prop == null) {
	    return getPos()+1;
	}
	return Integer.parseInt(prop);
    }

    /** returns true if residue not equal "X" */
    public boolean isKnownResidue() {
	char c = getName().charAt(0);
	return (c != 'X');
    }

    public boolean isFirst() {
	if ((getParent() == null) || (!(getParent() instanceof BioPolymer))) {
	    return false;
	}
	return getPos() == 0;
    }

    public boolean isLast() {
	if ((getParent() == null) || (!(getParent() instanceof BioPolymer))) {
	    return false;
	}
	return getPos() + 1 == ((BioPolymer)(getParent())).getResidueCount();
    }

    public boolean isTerminal() { return isFirst() || isLast(); }

    /** returns true if part of same sequence */
    public boolean isSameSequence(Residue other) {
	if (other instanceof AminoAcid3D) {
	    AminoAcid3D otherNuc = (AminoAcid3D)other;
	    return (getParent() == otherNuc.getParent());
	}
	return false;
    }

    public void setSymbol(LetterSymbol symbol) {
	this.symbol = symbol;
    }

    // public Sequence getSequence() { return sequence; }

    public int getPos() { return pos; }

    public void read(InputStream is) {
	// TODO
    }

    public void setAssignedName(String name) { this.assignedName = name; }

    /** sets assigned number (like residue number occuring in PDB file */
    public void setAssignedNumber(int number) { this.assignedNumber = number; }

    public void setParentObject(Object obj) {
	if (obj instanceof tools3d.objects3d.Object3D) {
	    super.setParent((tools3d.objects3d.Object3D)obj);
	}
	throw new RuntimeException("Parent of Nucleotide3D class must be an Object3D");
    }

    public void setPos(int pos) { this.pos = pos; }

    // public void setSequence(Sequence s) { this.sequence = sequence; }

}
