package rnadesign.rnamodel;

import tools3d.Vector3D;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.SimpleObject3D;

import chemistrytools.ChemicalElement;

/** represents an Atom in 3D space */
public class Atom3D extends SimpleObject3D implements Object3D {

    public static final String CLASS_NAME = "Atom3D";

    private ChemicalElement element;

    private double occupancy = 1.0;

    private double bFactor = 0.0;
    
    /* ATOM WRITING INFO  christine*/
    //    private int residueId;
    //    private String residueName;
    ////    private String atomName;
    //    private char strandName;

    public Atom3D(Vector3D pos) {
	super(pos);
    }

    public Atom3D() {
	super();
    }

    public String toStringLisp() {
	String lispString = "(" + getClassName() + " " + toStringBody() + " )";
	//element
	//occupancy
	//bFactor
	return lispString;
    }

    /** "deep" clone not including parent and children: every object is newly generated!
     */
    public Object cloneDeepThis() {
	Atom3D obj = new Atom3D();
	obj.copyDeepThisCore(this);
	if (element != null) {
	    obj.element = (ChemicalElement)(this.element.clone());
	}
	obj.occupancy = this.occupancy;
	obj.bFactor = this.bFactor;
	return obj;
    }

    /** returns chemical element */
    public ChemicalElement getElement() { return this.element; }

    public String getClassName() { return CLASS_NAME; }

    /** sets chemical element */
    public void setElement(ChemicalElement element) { this.element = element; }

    public double getBFactor() { return this.bFactor; }

    public double getOccupancy() { return this.occupancy; }

    public void setBFactor(double d) { this.bFactor = d; }

    public void setOccupancy(double d) { this.occupancy = d; }

    /* ATOM WRITING INFO */

    // use getName() instead: super
    //    public String getAtomName() {
    //	return atomName;
    //    }

    //    public int getResidueId() {
    //	return residueId;
    //    }

    //    public String getResidueName() {
    //	return residueName;
    //    }

    //    public char getStrandName() {
    //	return strandName;
    //    }

    // use setName(String name) instead: super
    //    public void setAtomName(String name) {
    //	this.atomName = name;
    //    }

    //    public void setResidueId(int id) {
    //	this.residueId = id;
    //    }

    //    public void setResidueName(String name) {
    //	this.residueName = name;
    //    }

    //    public void setStrandName(char c) {
    //	this.strandName = c;
    //    }

}
