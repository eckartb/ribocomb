package rnadesign.rnamodel;

import rnasecondary.*;
import tools3d.objects3d.Object3DSet;
import tools3d.objects3d.LinkSet;

public interface BasePairClassifier {

    BasePairInteractionType classifyBasePair(Nucleotide3D n1, Nucleotide3D n2) throws RnaModelException;

    LinkSet classifyBasePairs(Object3DSet bases) throws RnaModelException;

}