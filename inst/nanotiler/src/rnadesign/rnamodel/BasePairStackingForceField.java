package rnadesign.rnamodel;

import generaltools.ConstrainableDouble;
import generaltools.ConstraintDouble;
import generaltools.DoubleFunctor;
import tools3d.Vector3D;
import tools3d.objects3d.ConstraintLink;
import tools3d.objects3d.Link;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.modeling.ForceFieldLinkPairElement;

public class BasePairStackingForceField implements ForceFieldLinkPairElement {

    ConstraintDouble centerConstraint;
    ConstraintDouble angleConstraint;
    DoubleFunctor centerFunctor;
    DoubleFunctor angleFunctor;

    public BasePairStackingForceField(ConstraintDouble centerConstraint,
				      DoubleFunctor centerFunctor,
				      ConstraintDouble angleConstraint,
				      DoubleFunctor angleFunctor) {
	this.centerConstraint = centerConstraint;
	this.centerFunctor = centerFunctor;
	this.angleConstraint = angleConstraint;
	this.angleFunctor = angleFunctor;
    }

    public static Vector3D getLinkPosition(Link link) {
	Vector3D dv = link.getObj2().getPosition().plus(link.getObj1().getPosition());
	return dv.mul(0.5);
    }

    /** return energy of pair of links */
    public double pairEnergy(Link link1, Link link2) {
	if (!handles(link1, link2)) {
	    return 0.0;
	}
	ConstraintLink cl1 = (ConstraintLink)link1;
	ConstraintLink cl2 = (ConstraintLink)link2;
	Vector3D p1 = getLinkPosition(cl1);
	Vector3D p2 = getLinkPosition(cl2);
	double centerDist = p1.distance(p2);
	double result = centerFunctor.doubleFunc(centerConstraint.getDiff(centerDist));
	return result;
    }

    /** returns if both objects are siblings with adjacent sibling id */
    public static boolean isSibling(Object3D o1, Object3D o2) {
	Object3D p1 = o1.getParent();
	Object3D p2 = o2.getParent();
	if ((p1 != null) && (p2 != null)) {
	    return p1.equals(p2);
	}
	return false;
    }

    /** returns if both objects are siblings with adjacent sibling id */
    public static boolean isAdjacent(Object3D o1, Object3D o2) {
	if (isSibling(o1, o2)) {
	    int sid1 = o1.getSiblingId();
	    int sid2 = o2.getSiblingId();
	    int d = sid1 - sid2;
	    if (d < 0) {
		d *= -1;
	    }
	    return (d == 1);
	}
	return false;
    }

    public static boolean isAdjacent(Link link1, Link link2) {
	return ( ((isAdjacent(link1.getObj1(), link2.getObj1())) 
		  && (isAdjacent(link1.getObj2(), link2.getObj2())) ) 

		 ||  ((isAdjacent(link1.getObj1(), link2.getObj2())) 
		  && (isAdjacent(link1.getObj2(), link2.getObj1())) ) );
    }

    /** returns true if it can compute pairEnergy of link pair */
    public boolean handles(Link link1, Link link2) {
	if ((link1 instanceof ConstrainableDouble) && (link2 instanceof ConstrainableDouble)) {
	    // check if adjacent:
	    if (isAdjacent(link1, link2) ) {
		return true;
	    }
	    else {
		return false;
	    }
	}
	return false;
    }

}
