package rnadesign.rnamodel;

import generaltools.ConstrainableDouble;
import generaltools.ConstraintDouble;
import generaltools.DoubleFunctor;
import tools3d.Vector3D;
import tools3d.objects3d.ConstraintLink;
import tools3d.objects3d.Link;
import tools3d.objects3d.LinkSet;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DSet;
import tools3d.objects3d.modeling.ForceField;

public class BasePairStackingForceField2 implements ForceField {

    ConstraintDouble centerConstraint;
    ConstraintDouble angleConstraint;
    ConstraintDouble bendConstraint;
    DoubleFunctor centerFunctor;
    DoubleFunctor angleFunctor;
    DoubleFunctor bendFunctor;

    public BasePairStackingForceField2(ConstraintDouble centerConstraint,
					      DoubleFunctor centerFunctor,
					      ConstraintDouble angleConstraint,
					      DoubleFunctor angleFunctor,
					      ConstraintDouble bendConstraint,
					      DoubleFunctor bendFunctor) {
	this.centerConstraint = centerConstraint;
	this.centerFunctor = centerFunctor;
	this.angleConstraint = angleConstraint;
	this.angleFunctor = angleFunctor;
	this.bendConstraint = bendConstraint;
	this.bendFunctor = bendFunctor;
    }

    public static Vector3D getLinkPosition(Link link) {
	Vector3D dv = link.getObj2().getPosition().plus(link.getObj1().getPosition());
	return dv.mul(0.5);
    }

    /** return energy of pair of links. TODO: more efficient implementation
     * TODO : corrent implementation of tripletEnergy that works even if
     * links are not organized as stems. */
    public double energy(Object3DSet objects, LinkSet links) {
	double result = 0.0;
	for (int i = 1; i < links.size(); ++i) {
	    for (int j = 0; j < i; ++j) {
		Link link1 = links.get(i);
		Link link2 = links.get(j);
		result += pairEnergy(link1, link2);
	    }
	}
	for (int i = 2; i < links.size(); ++i) { 
	    result += tripletEnergy(links.get(i-2), links.get(i-1), links.get(i));
	}
	return result;
    }

    /** return energy of pair of links */
    public double pairEnergy(Link link1, Link link2) {
	if (!handles(link1, link2)) {
	    return 0.0;
	}
	ConstraintLink cl1 = (ConstraintLink)link1;
	ConstraintLink cl2 = (ConstraintLink)link2;
	Vector3D p1 = getLinkPosition(cl1);
	Vector3D p2 = getLinkPosition(cl2);
	double centerDist = p1.distance(p2);
	double result = centerFunctor.doubleFunc(centerConstraint.getDiff(centerDist));
	return result;
    }

    /** return energy of pair of links. Assumes link2 to be in middle between link1 and link2 */
    public double tripletEnergy(Link link1, Link link2, Link link3) {
	if ((!handles(link1, link2)) || (!handles(link2, link3))) {
	    return 0.0;
	}
	ConstraintLink cl1 = (ConstraintLink)link1;
	ConstraintLink cl2 = (ConstraintLink)link2;
	ConstraintLink cl3 = (ConstraintLink)link3;
	Vector3D p1 = getLinkPosition(cl1);
	Vector3D p2 = getLinkPosition(cl2);
	Vector3D p3 = getLinkPosition(cl3);
	double bend = p2.angle(p1, p3);
	double result = bendFunctor.doubleFunc(bendConstraint.getDiff(bend));
	return result;
    }

    /** returns if both objects are siblings with adjacent sibling id */
    public static boolean isSibling(Object3D o1, Object3D o2) {
	Object3D p1 = o1.getParent();
	Object3D p2 = o2.getParent();
	if ((p1 != null) && (p2 != null)) {
	    return p1.equals(p2);
	}
	return false;
    }

    /** returns if both objects are siblings with adjacent sibling id */
    public static boolean isAdjacent(Object3D o1, Object3D o2) {
	if (isSibling(o1, o2)) {
	    int sid1 = o1.getSiblingId();
	    int sid2 = o2.getSiblingId();
	    int d = sid1 - sid2;
	    if (d < 0) {
		d *= -1;
	    }
	    return (d == 1);
	}
	return false;
    }

    public static boolean isAdjacent(Link link1, Link link2) {
	return ( ((isAdjacent(link1.getObj1(), link2.getObj1())) 
		  && (isAdjacent(link1.getObj2(), link2.getObj2())) ) 

		 ||  ((isAdjacent(link1.getObj1(), link2.getObj2())) 
		  && (isAdjacent(link1.getObj2(), link2.getObj1())) ) );
    }

    /** returns true if it can compute pairEnergy of link pair */
    public boolean handles(Link link1, Link link2) {
	if ((link1 instanceof ConstrainableDouble) && (link2 instanceof ConstrainableDouble)) {
	    // check if adjacent:
	    if (isAdjacent(link1, link2) ) {
		return true;
	    }
	    else {
		return false;
	    }
	}
	return false;
    }

}
