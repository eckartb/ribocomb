package rnadesign.rnamodel;

import tools3d.objects3d.*;
import rnasecondary.*;

public class BasePairTools {
    
    public static String toString(InteractionLink basePair) {
	Object3D obj1 = basePair.getObj1();
	Object3D obj2 = basePair.getObj2();
	InteractionType type = basePair.getInteraction().getInteractionType();
	StringBuffer buf = new StringBuffer();
	if (obj1 instanceof Nucleotide3D && obj2 instanceof Nucleotide3D && type instanceof BasePairInteractionType) {
	    Nucleotide3D n1 = (Nucleotide3D)obj1;
	    Nucleotide3D n2 = (Nucleotide3D)obj2;
	    String s1 = n1.getParent().getName();
	    String s2 = n2.getParent().getName();
	    BasePairInteractionType bpt = (BasePairInteractionType)type;
	    buf.append(s1 + " " + n1.getName() + " " + (n1.getPos()+1) + " " + s2 + " " + n2.getName() + " " + (n2.getPos()+1) + " " + bpt.getSubTypeName() );
	}
	return buf.toString();
    }

    public static String toString(LinkSet links) {
	StringBuffer result = new StringBuffer();
	for (int i = 0; i < links.size(); ++i) {
	    if (links.get(i) instanceof InteractionLink) {
		InteractionLink basePair = (InteractionLink)(links.get(i));
		Object3D obj1 = basePair.getObj1();
		Object3D obj2 = basePair.getObj2();
		InteractionType type = basePair.getInteraction().getInteractionType();
		StringBuffer buf = new StringBuffer();
		int pc = 0;
		if (obj1 instanceof Nucleotide3D && obj2 instanceof Nucleotide3D && type instanceof BasePairInteractionType) {
		    Nucleotide3D n1 = (Nucleotide3D)obj1;
		    Nucleotide3D n2 = (Nucleotide3D)obj2;
		    String s1 = n1.getParent().getName();
		    String s2 = n2.getParent().getName();
		    BasePairInteractionType bpt = (BasePairInteractionType)type;
		    buf.append("" + (++pc) + " " + (i+1) + " " + s1 + " " + n1.getName() + " " + (n1.getPos()+1) + " " + n1.getAssignedNumber() + " " + s2 + " " + n2.getName() + " " + (n2.getPos()+1) + " " + n2.getAssignedNumber() + " " + bpt.getSubTypeName() );
		}
		result.append(buf.toString() + "\n"); // PackageConstants.SLASH);
	    }
	}
	return result.toString();
    }
}