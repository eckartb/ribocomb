package rnadesign.rnamodel;

import java.io.PrintStream;
import java.util.logging.*;
import java.util.*;
import generaltools.Optimizer;
import generaltools.Randomizer;
import generaltools.PropertyTools;
import tools3d.objects3d.*;
import tools3d.*;
import tools3d.symmetry2.*;
import graphtools.AdjacencyTools;
import numerictools.DoubleArrayTools;
import numerictools.DoubleTools;
import rnasecondary.*;
import sequence.LetterSymbol;

import static rnadesign.rnamodel.PackageConstants.*;

public class BasepairOptimizer implements Optimizer {

    public static final double INITIAL_ERROR = 1e30;
    public static final double TRANSLATION_STEP = 4.0;
    public static final double ANGLE_STEP = 15.0 * DEG2RAD; // 90 * DEG2RAD

    private HelixParameters defaultHelixParameters = new HelixParameters();
    private boolean addHelicesMode = false;
    private double annealingFactor = 0.95; //  0.985;
    private int annealingInterval = 10000; // scale step width and kt every so often
    private int chosen = 0;
    private boolean keepFirstFixedMode = false; // true; // if true, do not mutate first coordinate system
    protected int outputInterval = 10000; // scale step width and kt every so often
    private Object3D root;
    private boolean firstPass = true; // only true during first evalutation of each optimization run
    private double scaleLimit = 10.0;
    private double scaleMult = 0.02;
    private Map<Object3D, Integer> objectGroupIdMap = new HashMap<Object3D, Integer>();
    private List<InteractionLink> basepairConstraints;
    private List<ConstraintLink> distanceConstraints;
    private List<AngleLink> angleConstraints;
    private List<TorsionLink> torsionConstraints;
    private List<JunctionMultiConstraintLink> junctionConstraints;
    private List<JunctionDBConstraintLink> junctionDBConstraints;
    private List<MotifConstraintLink> motifConstraints;
    private List<SuperimposeConstraintLink> superimposeConstraints;
    private List<BlockCollisionConstraintLink> blockCollisionConstraints;
    private LinkSet allLinks;
    private LinkSet basePairDB;
    private double scoreMinimumLimit = 0.5; // if below this score, stop optimizing
    private List<Object3DSet> objectBlocks;
    private Vector3D[][] objectResidueBlocksOrig; // stores for object block a list of residue positions (each residue on sample atom like C4*)
    private Vector3D[][] objectResidueBlocksCurr; // stores for object block a list of residue positions (each residue on sample atom like C4*)
    private String refAtomName = "C4*"; // "P"; // "C4*";
    private double[] lastScores; // score assigned to each block in last round
    private double[] currScores; // score assigned to each block in current round
    // private int[][] branchDescriptorGroups;
    // private int[] branchDescriptorGroupIds;
    private int[] groupConstraintCounts;
    // private List<BranchDescriptor3D> branchDescriptors;
    protected List<CoordinateSystem> origCoordinateSystems;
    protected List<CoordinateSystem> optCoordinateSystems;
    private double[] coordScoreAverages; // average constraint violation for each coordinate system
    private double[] coordScoreAveragesTmp; // average constraint violation for each coordinate system
    private double[] coordScoreAveragesSaved; // average constraint violation for each coordinate system
    private boolean[] moveFlags;
    private int numberSteps;
    private double ktOrig = 2.0;
    protected static Logger log = Logger.getLogger(LOGGER_NAME);
    private double electrostaticLength = 5.0;
    private double vdwLength = 4.5;
    private double vdwLength2 = 4.5;
    private int electroStride = 1; // for skipping base pairs in slow electrostatic term
    private double electrostaticWeight = 0.0; // 0.1;
    private double vdwWeight = 0.0; // 10.0; // 0.1;
    private double vdwWeight2 = 0.0; // 10.0; // 0.1;
    private double errorScoreLimit;
    private GaussianOrientableMutator mutator = new GaussianOrientableMutator(TRANSLATION_STEP, ANGLE_STEP);
    private GaussianOrientableMutator mutatorOrig = new GaussianOrientableMutator(TRANSLATION_STEP, ANGLE_STEP);
    private List<Vector3D> axisList;
    private static SymCopies symCopies; // keeps all stored symmetry operations
    private int[] symVdwActive; // stores symmetry copies used for Van de Waals computation
    private int verboseLevel = 1;

    public BasepairOptimizer(LinkSet links, List<Object3DSet> objectBlocks, int numberSteps, double errorScoreLimit, LinkSet basePairDB, int[] _symVdwActive, SymCopies _symCopies) {
	init(links, objectBlocks, numberSteps, errorScoreLimit, basePairDB, _symVdwActive, _symCopies);
	log.info("Successfully created constraint optimizer object.");
    }

    protected void init(LinkSet links, List<Object3DSet> objectBlocks, int numberSteps, double errorScoreLimit, LinkSet basePairDB,
			int[] _symVdwActive, SymCopies _symCopies) {
	String objBlockString = "";
	for (int i = 0; i < objectBlocks.size(); ++i) {
	    for (int j = 0; j < objectBlocks.get(i).size(); ++j) {
		objBlockString += objectBlocks.get(i).get(j).getFullName();
		if ((j+1) < objectBlocks.get(i).size()) {
		    objBlockString = objBlockString + ",";
		}
	    }
	    objBlockString += ";";
	}
	log.info("Starting to initialize constraint optimizer object: " + objBlockString);
	this.symCopies = _symCopies; // SymCopySingleton.getInstance(); // current list of symmetry operations
	this.numberSteps = numberSteps;
	this.errorScoreLimit = errorScoreLimit;
	this.basepairConstraints =  new ArrayList<InteractionLink>();
	this.distanceConstraints = new ArrayList<ConstraintLink>();
	this.angleConstraints = new ArrayList<AngleLink>();
	this.torsionConstraints = new ArrayList<TorsionLink>();
	this.junctionConstraints = new ArrayList<JunctionMultiConstraintLink>();
	this.junctionDBConstraints = new ArrayList<JunctionDBConstraintLink>();
  this.motifConstraints = new ArrayList<MotifConstraintLink>();
  this.superimposeConstraints = new ArrayList<SuperimposeConstraintLink>();
  this.blockCollisionConstraints = new ArrayList<BlockCollisionConstraintLink>();
	this.allLinks = links; // might be needed for checking hydrogen bonding
	this.objectBlocks = objectBlocks;
	this.basePairDB = basePairDB;
	this.symVdwActive = _symVdwActive;
	if (_symVdwActive == null) {
	    this.symVdwActive = new int[symCopies.size()];
	    for (int i = 0; i < this.symVdwActive.length; ++i) {
		this.symVdwActive[i] = i; // all symmetry copies are active
	    }
	}
	assert basePairDB != null;
	assert objectBlocks != null;
	assert objectBlocks.size() > 0;
	for (int i = 0; i < links.size(); ++i) {
	    Link link = links.get(i);
	    if (RnaLinkTools.isBasePairLink(link) && checkLinkRelevant(link)) {
		basepairConstraints.add((InteractionLink)link);
	    } else if (link instanceof JunctionMultiConstraintLink) {
		System.out.println("Hey " + link);
		if (checkJunctionLinkRelevant((JunctionMultiConstraintLink)link)) {
		    junctionConstraints.add((JunctionMultiConstraintLink)link);
		    log.info("Junction constraint added to BasepairOptimizer: " + link.toString());
		} else {
		    log.info("Junction constraint not relevant: " + link.toString());
		}
	    } else if (link instanceof JunctionDBConstraintLink) {
		junctionDBConstraints.add((JunctionDBConstraintLink)link);
	    }
	    else if (link instanceof ConstraintLink) {
		if (checkLinkRelevant(link)) {
		    log.info("Adding distance constraint: " + link.toString());
		    distanceConstraints.add((ConstraintLink)link);
		} else {
		    log.info("Link is not relevant: " + link);
		}
	    }
      else if (link instanceof MotifConstraintLink){
        if(checkLinkRelevant(link)){
          log.info("Adding motif constraint: " + link.toString());
          motifConstraints.add((MotifConstraintLink)link);
        } else{
                log.info("Link is not relevant: " + link);
            }
      }
    else if ( link instanceof SuperimposeConstraintLink){
      if(checkLinkRelevant(link)){
        log.info("Adding superimpose constraint: " + link.toString());
        superimposeConstraints.add((SuperimposeConstraintLink)link);
      } else{
        log.info("Superimpose constraint not relevant: " + link.toString());
      }
    }
    else if ( link instanceof BlockCollisionConstraintLink){
      if(checkBlockCollisionRelevant((BlockCollisionConstraintLink)link)){
          log.info("Adding block collision constraint: " + link.toString());
          blockCollisionConstraints.add((BlockCollisionConstraintLink)link);
      } else{
        log.info("Block collision constraint not relevant: " + link.toString());
      }
    }
	    else if ((link instanceof AngleLink) && (checkLinkRelevant(link))) {
		angleConstraints.add((AngleLink)link);
	    }
	    else if (link instanceof TorsionLink) {
		if (checkTorsionRelevant((TorsionLink)link)) {
		    torsionConstraints.add((TorsionLink)link);
		} else {
		    log.info("Torsion constraint not relevant: " + link.toString());
		}
	    } else {
		log.finest("Did not recognize type of link: " + link.toString());
	    }
	}
	if ((basepairConstraints.size() == 0) && (distanceConstraints.size() == 0) && (angleConstraints.size() == 0)
	    && (torsionConstraints.size() == 0) && (junctionConstraints.size() == 0) && (junctionDBConstraints.size() == 0)
      && (motifConstraints.size() == 0) && (superimposeConstraints.size() == 0) && (blockCollisionConstraints.size() == 0)) {
	    log.warning("No constraints defined! Use command genbasepair or gendistconstraint or gentorsionconstraintbefore start optimize_basepairs");
	}
//	else { //now runs without any constraints defined

// 	    this.branchDescriptors = findBranchDescriptors(helixConstraints);
// 	    this.branchDescriptorGroups = findBranchDescriptorGroups(branchDescriptors, allLinks);
// 	    this.branchDescriptorGroupIds = findGroupIds(branchDescriptorGroups, this.branchDescriptors.size());
	    this.origCoordinateSystems = generateCoordinateSystems(objectBlocks.size());
	    this.optCoordinateSystems = generateCoordinateSystems(objectBlocks.size());
	    coordScoreAverages = new double[objectBlocks.size()];
	    coordScoreAveragesTmp = new double[objectBlocks.size()];
	    coordScoreAveragesSaved = new double[objectBlocks.size()];
	    lastScores = new double[objectBlocks.size()];
	    currScores = new double[objectBlocks.size()];
	    resetCoordScoreAverages();
	    moveFlags = new boolean[coordScoreAverages.length];
	    for (int i = 0; i < moveFlags.length; ++i) {
		moveFlags[i] = true;
	    }
	    groupConstraintCounts = new int[origCoordinateSystems.size()]; // how many contstrants affecting each group
	    String outCount = "";
	    int counter = 0;
	    for (int i = 0; i < groupConstraintCounts.length; ++i) {
 		groupConstraintCounts[i] = countGroupConstraints(i);
 		outCount = outCount + groupConstraintCounts[i] + " ";
		counter += groupConstraintCounts[i];
 	    }
	    initObjectResidueBlocks();
	    assert counter > 0; //make sure there is at least one constraint in effect
	    if (optCoordinateSystems.size() < 2) {
		keepFirstFixedMode = false; // switch off mode useful for avoiding "drifting"
	    }
	    log.info("Group constraint counts: " + outCount);
	//}
	log.info("Successfully initialized constraint optimizer object.");
    }

    /** overwrite last scores for each block with current scores. Set current scores to zero. */
    private void storeScores() {
	for (int i = 0; i < lastScores.length; ++i) {
	    lastScores[i] = currScores[i];
	    currScores[i] = 0.0;
	}
    }

    /** returns true, if link points towards two objects that belong to different groups */
    private boolean checkLinkRelevant(Link link) {
	Object3D obj1 = link.getObj1();
	Object3D obj2 = link.getObj2();
	int g1 = findObjectGroupId(obj1);
	int g2 = findObjectGroupId(obj2);
	if ((g1 < 0) || (g2 < 0)) {
	    return false;
	}
	if (g1 != g2) {
	    return true;
	}
	if (link instanceof ConstraintLink) {
	    ConstraintLink cLink = (ConstraintLink)link;
	    if (cLink.getSymId1() != cLink.getSymId2()) { // same block but different symmetry copy
		return true;
	    }
	}
	return false;
    }

    /** returns true, if constraint link points towards four objects that belong to different groups
     * FIXIT: handling of symmetry copies not yet implemented.
     */
    private boolean checkTorsionRelevant(TorsionLink link) {
	Object3D obj1 = link.getObj1();
	Object3D obj2 = link.getObj2();
	Object3D obj3 = link.getObj3();
	Object3D obj4 = link.getObj4();
	int g1 = findObjectGroupId(obj1);
	int g2 = findObjectGroupId(obj2);
	int g3 = findObjectGroupId(obj3);
	int g4 = findObjectGroupId(obj4);
	if ((g1 < 0)  || (g2 < 0) || (g3 < 0) || (g4 < 0)) {
	    return false;
	}
	if ((g1 == g2) && (g1 == g2) && (g1 == g3) && (g1 == g4)) {
	    return false;
	}
	return true; // make sure they point to different groups
    }

    /** returns true, if constraint link points towards four objects that belong to different groups
     * FIXIT: handling of symmetry copies not yet implemented.
     */
    private boolean checkJunctionLinkRelevant(JunctionMultiConstraintLink link) {
	log.info("Starting checkJunctionLinkRelevant: " + link.getBranchCount() + " branches.");
	assert(link.size() > 0);
	int g1 = findObjectGroupId(link.getObj(0));
	System.out.println("g1: " + g1);
	for (int i = 1; i < link.size(); ++i) {
	    int g2 = findObjectGroupId(link.getObj(i));
	    System.out.println("g2: " + link.getObj(i).getFullName());
	    if ((g1 != g2) || (link.getSymId(0) != link.getSymId(i))) {
		return true;
	    }
	}
	return false; // make sure they point to different groups
    }
    
    private boolean checkBlockCollisionRelevant(BlockCollisionConstraintLink link) {
  int g1 = findObjectGroupId( link.getObj1().getChild(0) );
  int g2 = findObjectGroupId( link.getObj2().getChild(0) ); //need child to find parent
  if ((g1 < 0) || (g2 < 0)) {
      log.info("block not found: "+link);
      return false;
  }
  if (g1 != g2) {
      return true;
  }
  return false;
    }

    private List<CoordinateSystem> generateCoordinateSystems(int n) {
	List<CoordinateSystem> result = new ArrayList<CoordinateSystem>();
	for (int i = 0; i < n; ++i) {
	    result.add(new CoordinateSystem3D(new Vector3D(0.0, 0.0, 0.0)));
	}
	return result;
    }

    /** Find out, to which coordinate system trafo the object belongs FIXIT: speedup */
    private int findObjectGroupId(Object3D object) {
	assert objectGroupIdMap != null;
	Integer nObj = objectGroupIdMap.get(object);
	if (nObj == null) {
	    nObj = new Integer(_findObjectGroupId(object));
	    objectGroupIdMap.put(object, nObj);
	}
	return nObj.intValue();
    }

    /** Find out, to which coordinate system trafo the object belongs FIXIT: speedup */
    private int _findObjectGroupId(Object3D object) {
	for (int i = 0; i < objectBlocks.size(); ++i) {
	    Object3DSet objSet = objectBlocks.get(i);
	    for (int j = 0; j < objSet.size(); ++j) {
		if (Object3DTools.isAncestor(objSet.get(j), object)) {
		    return i; // return group
		}
	    }
	}
	return -1; // not found
    }

    private void setMovableObject(Object3D object) {
	int id = findObjectGroupId(object);
	if (id >= 0) {
	    moveFlags[id] = true;
	    log.fine("Setting object " + object.getName() + " to movable group: "
		     + (id+1));
	}
	else {
	    log.warning("Could not find suitable parent object for " + object.getName());
	    assert false;
	}
    }

    /** sets flags about which objects are movable */
    public void setMovableObjects(Object3DSet objects) {
	assert objects != null;
	assert objects.size() > 0;
	for (int i = 0; i < moveFlags.length; ++i) {
	    moveFlags[i] = false; // nothing can be moved except the specified objects
	}
	for (int i = 0; i < objects.size(); ++i) {
	    setMovableObject(objects.get(i));
	}
    }

    /** reset each element to 1.0. Will be used to compute *geometric* mean of constraint violations for each coordinate system */
    private void resetCoordScoreAverages() {
	for (int i = 0; i < coordScoreAverages.length; ++i) {
	    coordScoreAverages[i] = 1.0;
	    coordScoreAveragesTmp[i] = 1.0;
	    // coordScoreAveragesSaved[i] = 1.0;
	}
    }

    /** reset each element to 1.0. Will be used to compute *geometric* mean of constraint violations for each coordinate system */
    private void saveCoordScoreAverages() {
	for (int i = 0; i < coordScoreAverages.length; ++i) {
	    coordScoreAveragesSaved[i] = coordScoreAverages[i];
	}
    }

    /** reset each element to 1.0. Will be used to compute *geometric* mean of constraint violations for each coordinate system */
    private void restoreCoordScoreAverages() {
	for (int i = 0; i < coordScoreAverages.length; ++i) {
	    coordScoreAverages[i] = coordScoreAveragesSaved[i];
	}
    }

    /** returns strand junction parent of itself */
    /*
    private static Object3D findSuitableParentObject(Object3D b) {
	if (b instanceof StrandJunction3D) {
	    return b;
	}
	Object3D result = Object3DTools.findAncestorByClassName(b, "StrandJunction3D");
	if (result != null) {
	    return result;
	}
	result = Object3DTools.findAncestorByClassName(b, "KissingLoop3D");
	if (result != null) {
	    return result;
	}
	return null;
    }
    */

    /** apply tranformations to 3D objects. TODO: implementation not yet correct! */
    protected void applyTransformations(List<CoordinateSystem> coords) {
	List<Object3D> appliedObjects = new ArrayList<Object3D>(); // keep track which objects have been transformed
	assert coords.size() == objectBlocks.size();
	assert moveFlags.length == objectBlocks.size();
	assert moveFlags.length == coords.size();
	// log.info("Number of transformations to be applied: " + objectBlocks.size());
	for (int i = 0; i < objectBlocks.size(); ++i) {
	    if (!moveFlags[i]) {
		continue;
	    }
	    for (int j = 0; j < objectBlocks.get(i).size(); ++j) {
		// find ancestor object:
		Object3D newRoot = objectBlocks.get(i).get(j);
		if (newRoot != null) {
		    // check if one of previously rotated objects is parent
		    if (Object3DTools.findAncestorOrEqual(newRoot, appliedObjects) == null) {
			newRoot.activeTransform(coords.get(i));
			// log.fine("Applying transformation " + (i+1) + " " + coords.get(i) + " to " + newRoot.getName());
			appliedObjects.add(newRoot);
		    }
		}
	    }
	}
    }

    /** finds index of group to which "n" belongs, return -1 if not found */
    /* private int findGroupId(int[][] groups, int id) {
	for (int i = 0; i < groups.length; ++i) {
	    for (int j = 0; j < groups[i].length; ++j) {
		if (groups[i][j] == id) {
		    return i;
		}
	    }
	}
	return -1;
    }
    */

    /** finds index of group to which all entries belong, return -1 if not found */
    /* private int[] findGroupIds(int[][] groups, int size) {
	int[] result = new int[size];
	for (int i = 0; i < size; ++i) {
	    result[i] = findGroupId(groups, i);
	}
	return result;
    }
    */

    /** Computes distance error for base pair (DRMS of test pair versus reference pair)
     * @param n1 : coordinates of nucleotide 1
     * @param n2 : coordinates of nucleotide 2
     * @param refNt1 : coordinates of reference nucleotide 1
     * @param refNt2 : coordinates of reference nucleotide 2
     */
    protected static double computeBasePairError(Vector3D[] n1,
						 Vector3D[] n2,
						 Vector3D[] refNt1,
						 Vector3D[] refNt2) {
	assert n1.length == refNt1.length;
	assert n2.length == refNt2.length;
	assert n1.length > 0;
	assert n2.length > 0;
	double sum = 0.0;
	double d1,d2,dd;
	for (int i = 0; i < n1.length; ++i) {
	    for (int j = 0; j < n2.length; ++j) {
		dd = n1[i].distance(n2[j]) - refNt1[i].distance(refNt2[j]);
		sum += dd * dd;
	    }
	}
	assert sum > 0.0;
	return Math.sqrt(sum / (n1.length * n2.length));
    }

    /** applies active coordinate system to vector */
    private static void applyCoordinateTransformation(Vector3D[] v,
					       CoordinateSystem cs) {
	for (int i = 0; i < v.length; ++i) {
	    v[i] = cs.activeTransform(v[i]);
	}
    }

    /** Computes distance error for base pair
     * @param cs1 : coordinate transformation applied to block 1
     * @param cs2 : coordinate transformation applied to block 2
     * @param constraint : helix constraint describing minimum and maximum stem length
     */
    protected static double computeBasePairError(CoordinateSystem cs1,
						 CoordinateSystem cs2,
						 Nucleotide3D n1,
						 Nucleotide3D n2,
						 Nucleotide3D refNt1,
						 Nucleotide3D refNt2) throws RnaModelException {
	Object3DSet n1Set = NucleotideDBTools.extractAtomMultipod(n1);
	Object3DSet n2Set = NucleotideDBTools.extractAtomMultipod(n2);
	Object3DSet refN1Set = NucleotideDBTools.extractAtomMultipod(refNt1);
	Object3DSet refN2Set = NucleotideDBTools.extractAtomMultipod(refNt2);
	Vector3D[] n1v = Object3DSetTools.getCoordinates(n1Set);
	Vector3D[] n2v = Object3DSetTools.getCoordinates(n2Set);
	Vector3D[] refN1v = Object3DSetTools.getCoordinates(refN1Set);
	Vector3D[] refN2v = Object3DSetTools.getCoordinates(refN2Set);
	// convert to coordinate systems cs:
	applyCoordinateTransformation(n1v, cs1);
	applyCoordinateTransformation(n2v, cs2);
	double result = computeBasePairError(n1v, n2v, refN1v, refN2v);
	assert result > 0.0;
	return result;
    }

    /** Computes distance error for base pair
     * @param cs1 : coordinate transformation applied to block 1
     * @param cs2 : coordinate transformation applied to block 2
     * @param cLink : distance constraint describing minimum and maximum distance
     */
    protected static double computeDistanceConstraintError(CoordinateSystem cs1,
							   CoordinateSystem cs2,
							   ConstraintLink cLink) {
	Object3D obj1 = cLink.getObj1();
	Object3D obj2 = cLink.getObj2();
	Vector3D pos1 = obj1.getPosition();
	Vector3D pos2 = obj2.getPosition();
	// convert to coordinate systems cs:
	Vector3D pos1b = cs1.activeTransform(pos1);
	if (cLink.getSymId1() > 0) { // check if symmetry operations needed first:
	    // System.out.println("Applying symmetry operation " + cLink.getSymId1() + " to " + pos1b + " : " + symCopies.get(cLink.getSymId1()));
	    pos1b = symCopies.get(cLink.getSymId1()).activeTransform(pos1b);
	    // System.out.println("Applied symmetry operation " + cLink.getSymId1() + " to " + pos1b);
	}
	Vector3D pos2b = cs2.activeTransform(pos2);
	if (cLink.getSymId2() > 0) {
	    // System.out.println("Applying symmetry operation " + cLink.getSymId2() + " to " + pos2b + " : " + symCopies.get(cLink.getSymId2()));
	    pos2b = symCopies.get(cLink.getSymId2()).activeTransform(pos2b);
	    // System.out.println("Applied symmetry operation " + cLink.getSymId2() + " to " + pos2b);
	}
	return cLink.getConstraint().getDiff(pos1b.distance(pos2b));
    }

    /** Computes out of range error of angle between 3 objects (third object is "center" of angle)
     * @param cs1 : coordinate transformation applied to block 1
     * @param cs2 : coordinate transformation applied to block 2
     * @param cLink : distance constraint describing minimum and maximum distance
     */
    protected static double computeAngleConstraintError(CoordinateSystem cs1,
							CoordinateSystem cs2,
							CoordinateSystem cs3,
							AngleLink cLink) {
	Object3D obj1 = cLink.getObj1();
	Object3D obj2 = cLink.getObj2();
	Object3D obj3 = cLink.getObj3();
	Vector3D pos1 = obj1.getPosition();
	Vector3D pos2 = obj2.getPosition();
	Vector3D pos3 = obj3.getPosition();
	// convert to coordinate systems cs:
	Vector3D pos1b = cs1.activeTransform(pos1);
	Vector3D pos2b = cs2.activeTransform(pos2);
	Vector3D pos3b = cs3.activeTransform(pos3);
	return cLink.computeError(pos1b, pos2b, pos3b);
    }

    /** Computes error due to out-of-range torsion angle between 4 objects
     * @param cs1 : coordinate transformation applied to block 1
     * @param cs2 : coordinate transformation applied to block 2
     * @param cs3 : coordinate transformation applied to block 3
     * @param cs4 : coordinate transformation applied to block 4
     * @param cLink : torsion angle constraint describing minimum and maximum distance
     */
    protected static double computeTorsionConstraintError(CoordinateSystem cs1,
							  CoordinateSystem cs2,
							  CoordinateSystem cs3,
							  CoordinateSystem cs4,
							  TorsionLink cLink) {
	Object3D obj1 = cLink.getObj1();
	Object3D obj2 = cLink.getObj2();
	Object3D obj3 = cLink.getObj3();
	Object3D obj4 = cLink.getObj4();
	Vector3D pos1 = obj1.getPosition();
	Vector3D pos2 = obj2.getPosition();
	Vector3D pos3 = obj3.getPosition();
	Vector3D pos4 = obj4.getPosition();
	// convert to coordinate systems cs:
	Vector3D pos1b = cs1.activeTransform(pos1);
	Vector3D pos2b = cs2.activeTransform(pos2);
	Vector3D pos3b = cs3.activeTransform(pos3);
	Vector3D pos4b = cs4.activeTransform(pos4);
	return Math.toDegrees(cLink.computeError(pos1b, pos2b,pos3b,pos4b)); // multiply
    }

    /** Computes distance error for base pair. The method searches a database of base pairs
     * and returns the RMS with respect to the smallest distance RMS (DRMS).
     * In principle this could facilitate non-standard base pairings.
     * @param cs1 : coordinate transformation applied to block 1
     * @param cs2 : coordinate transformation applied to block 2
     * @param constraint : helix constraint describing minimum and maximum stem length
     */
    protected double computeBasePairError(CoordinateSystem cs1,
					  CoordinateSystem cs2,
					  InteractionLink basepairConstraint) throws RnaModelException {
	Nucleotide3D n1 = (Nucleotide3D)basepairConstraint.getObj1();
	Nucleotide3D n2 = (Nucleotide3D)basepairConstraint.getObj2();
	log.fine("Starting computeBasePairError for " + n1.getFullName() + " " + n2.getFullName());
	LetterSymbol symbol1 = n1.getSymbol();
	LetterSymbol symbol2 = n2.getSymbol();
	RnaInteractionType interactionType = (RnaInteractionType)(basepairConstraint.getInteraction().getInteractionType());
	boolean interactionFound = false;
	double bestError = INITIAL_ERROR;
	for (int i = 0; i < basePairDB.size(); ++i) {
	    InteractionLink link = (InteractionLink)(basePairDB.get(i));
	    Nucleotide3D refN1 = (Nucleotide3D)(link.getObj1());
	    Nucleotide3D refN2 = (Nucleotide3D)(link.getObj2());
	    LetterSymbol s1 = (refN1).getSymbol();
	    LetterSymbol s2 = (refN2).getSymbol();
	    RnaInteractionType dbInteraction = (RnaInteractionType)(link.getInteraction().getInteractionType());
	    if (dbInteraction.getSubTypeId() != interactionType.getSubTypeId()) {
		continue; // wrong subtype
	    }
	    double error = INITIAL_ERROR;
	    if ((s1.getCharacter() == symbol1.getCharacter())
		&& (s2.getCharacter() == symbol2.getCharacter())) {
		interactionFound = true;
		error = computeBasePairError(cs1, cs2, n1, n2, refN1, refN2);
		assert error > 0.0;
	    }
	    else if ((s1.getCharacter() == symbol2.getCharacter())
		     && (s2.getCharacter() == symbol1.getCharacter())) {
		interactionFound = true;
		error = computeBasePairError(cs1, cs2, n1, n2, refN2, refN1);
		assert error > 0.0;
	    }
	    if (error < bestError) {
		bestError = error;
		break; // stop with first found solution
	    }
	}
	if (!interactionFound) {
	    throw new RnaModelException("Could not find " + interactionType + " " + symbol1 + " " + symbol2
					+ " in base pair database!");
	}
	log.fine("Finished computeBasePairError for " + n1.getFullName() + " " + n2.getFullName() + " with score: " + bestError);
	assert bestError > 0.0;
	return bestError;
    }

    /** central method for scoring modified coordinate systems */
    private double scoreBasepairConstraint(InteractionLink basepairConstraint, List<CoordinateSystem> coordinateSystems) throws RnaModelException {
	Nucleotide3D n1 = (Nucleotide3D)basepairConstraint.getObj1();
	Nucleotide3D n2 = (Nucleotide3D)basepairConstraint.getObj2();
	// how do I find the group to which these nucleotides are connected?
	int g1 = findObjectGroupId(n1);
	int g2 = findObjectGroupId(n2);
	if (g1 < 0) {
	    throw new RnaModelException("Nucleotide " + n1.getFullName() + " is not part of movable block! Use option blocks=subtree1;subtree2...");
	}
	if (g2 < 0) {
	    throw new RnaModelException("Nucleotide " + n2.getFullName() + " is not part of movable block! Use option blocks=subtree1;subtree2...");
	}
	assert g1 >= 0;
	assert g2 >= 0;
	assert n1 != n2;
	assert coordinateSystems.get(g1).isValid();
	assert coordinateSystems.get(g2).isValid();
	double result = computeBasePairError(coordinateSystems.get(g1), coordinateSystems.get(g2), basepairConstraint);
	assert ! Double.isNaN(result); // make sure it is not Not a Number
	assert ! Double.isInfinite(result);
	if (result > 0.0) {
	    coordScoreAveragesTmp[g1] *= result;
	    coordScoreAveragesTmp[g2] *= result;
	    assert DoubleTools.isValidAndPositive(coordScoreAveragesTmp[g1]);
	    assert DoubleTools.isValidAndPositive(coordScoreAveragesTmp[g2]);
	}
	else {
	    log.finest("Zero constraint score between " + result + " " +  n1.getFullName()
		       + " and " + n2.getFullName());
	}
	return result;
    }

    /** central method for scoring modified coordinate systems */
    private double scoreDistanceConstraint(ConstraintLink cLink, List<CoordinateSystem> coordinateSystems) throws RnaModelException {
	Object3D n1 = cLink.getObj1();
	Object3D n2 = cLink.getObj2();
	// how do I find the group to which these nucleotides are connected?
	int g1 = findObjectGroupId(n1);
	int g2 = findObjectGroupId(n2);
	if (g1 < 0) {
	    throw new RnaModelException("Nucleotide " + n1.getFullName() + " is not part of movable block! Use option blocks=subtree1;subtree2...");
	}
	if (g2 < 0) {
	    throw new RnaModelException("Nucleotide " + n2.getFullName() + " is not part of movable block! Use option blocks=subtree1;subtree2...");
	}
	assert g1 >= 0;
	assert g2 >= 0;
	assert n1 != n2;
	assert coordinateSystems.get(g1).isValid();
	assert coordinateSystems.get(g2).isValid();
	double result = computeDistanceConstraintError(coordinateSystems.get(g1), coordinateSystems.get(g2), cLink);
	assert ! Double.isNaN(result); // make sure it is not Not a Number
	assert ! Double.isInfinite(result);
	if (result > 0.0) {
	    coordScoreAveragesTmp[g1] *= result;
	    coordScoreAveragesTmp[g2] *= result;
	    assert DoubleTools.isValidAndPositive(coordScoreAveragesTmp[g1]);
	    assert DoubleTools.isValidAndPositive(coordScoreAveragesTmp[g2]);
	}
	else {
	    log.fine("Zero constraint score between " + result + " " +  n1.getFullName()
		     + " and " + n2.getFullName() + " with distance " + n1.distance(n2));
	}
	return result;
    }

    /** central method for scoring modified coordinate systems */
    private double scoreAngleConstraint(AngleLink cLink, List<CoordinateSystem> coordinateSystems) throws RnaModelException {
	Object3D n1 = cLink.getObj1();
	Object3D n2 = cLink.getObj2();
	Object3D n3 = cLink.getObj3();
	// how do I find the group to which these nucleotides are connected?
	int g1 = findObjectGroupId(n1);
	int g2 = findObjectGroupId(n2);
	int g3 = findObjectGroupId(n3);
	if (g1 < 0) {
	    throw new RnaModelException("Nucleotide " + n1.getFullName() + " is not part of movable block! Use option blocks=subtree1;subtree2...");
	}
	if (g2 < 0) {
	    throw new RnaModelException("Nucleotide " + n2.getFullName() + " is not part of movable block! Use option blocks=subtree1;subtree2...");
	}
	if (g3 < 0) {
	    throw new RnaModelException("Nucleotide " + n3.getFullName() + " is not part of movable block! Use option blocks=subtree1;subtree2...");
	}
	assert g1 >= 0;
	assert g2 >= 0;
	assert g3 >= 0;
	assert n1 != n2;
	assert n1 != n3;
	assert coordinateSystems.get(g1).isValid();
	assert coordinateSystems.get(g2).isValid();
	assert coordinateSystems.get(g3).isValid();
	double result = computeAngleConstraintError(coordinateSystems.get(g1), coordinateSystems.get(g2), coordinateSystems.get(g3),
						    cLink);
	assert ! Double.isNaN(result); // make sure it is not Not a Number
	assert ! Double.isInfinite(result);
	return result;
    }

    /** central method for scoring modified coordinate systems */
    private double scoreTorsionConstraint(TorsionLink cLink, List<CoordinateSystem> coordinateSystems) throws RnaModelException {
	Object3D n1 = cLink.getObj1();
	Object3D n2 = cLink.getObj2();
	Object3D n3 = cLink.getObj3();
	Object3D n4 = cLink.getObj4();
	// how do I find the group to which these nucleotides are connected?
	int g1 = findObjectGroupId(n1);
	int g2 = findObjectGroupId(n2);
	int g3 = findObjectGroupId(n3);
	int g4 = findObjectGroupId(n4);
	if (g1 < 0) {
	    throw new RnaModelException("Nucleotide " + n1.getFullName() + " is not part of movable block! Use option blocks=subtree1;subtree2...");
	}
	if (g2 < 0) {
	    throw new RnaModelException("Nucleotide " + n2.getFullName() + " is not part of movable block! Use option blocks=subtree1;subtree2...");
	}
	if (g3 < 0) {
	    throw new RnaModelException("Nucleotide " + n3.getFullName() + " is not part of movable block! Use option blocks=subtree1;subtree2...");
	}
	if (g4 < 0) {
	    throw new RnaModelException("Nucleotide " + n4.getFullName() + " is not part of movable block! Use option blocks=subtree1;subtree2...");
	}
	assert g1 >= 0;
	assert g2 >= 0;
	assert g3 >= 0;
	assert g4 >= 0;
	assert n1 != n2;
	assert n1 != n3;
	assert n1 != n4;
	assert coordinateSystems.get(g1).isValid();
	assert coordinateSystems.get(g2).isValid();
	assert coordinateSystems.get(g3).isValid();
	assert coordinateSystems.get(g4).isValid();
	double result = computeTorsionConstraintError(coordinateSystems.get(g1), coordinateSystems.get(g2), coordinateSystems.get(g3),
						      coordinateSystems.get(g4), cLink);
	assert ! Double.isNaN(result); // make sure it is not Not a Number
	assert ! Double.isInfinite(result);
	return result;
    }

    /** central method for scoring modified coordinate systems */
    private double scoreJunctionConstraint(JunctionMultiConstraintLink cLink, List<CoordinateSystem> coordinateSystems) throws RnaModelException {
	List<CoordinateSystem> csUsed = new ArrayList<CoordinateSystem>();
	for (int i = 0; i < cLink.getBranchCount(); ++i) {
	    Object3D obj = cLink.getThreePrimeObject(i);
	    int g = findObjectGroupId(obj);
	    if (g < 0) {
		throw new RnaModelException("Nucleotide " + obj.getFullName() + " is not part of movable block! Use option blocks=subtree1;subtree2...");
	    }
	    CoordinateSystem3D cs = (CoordinateSystem3D)(coordinateSystems.get(g));
	    CoordinateSystem3D csClone = cs;
	    if (cLink.getSymId(i) > 0) { // symmetry copy is being used!!
		csClone = (CoordinateSystem3D)(cs.cloneDeep());
		assert(cLink.getSymId(i) < symCopies.size());
		csClone.activeTransform(symCopies.get(cLink.getSymId(i)));
	    }
	    csUsed.add(csClone);
	}
	assert (csUsed.size() == cLink.getBranchCount());
	double result = cLink.getDiff(csUsed);
	assert ! Double.isNaN(result); // make sure it is not Not a Number
	assert ! Double.isInfinite(result);
	// log.info("Score of junction constraint: " + result + " : " + cLink.toString());
	return result;
    }

    /** central method for scoring modified coordinate systems */
    private Properties scoreJunctionDBConstraint(JunctionDBConstraintLink cLink, List<CoordinateSystem> coordinateSystems) throws RnaModelException {
	List<CoordinateSystem> csUsed = new ArrayList<CoordinateSystem>();
	for (int i = 0; i < cLink.getBranchCount(); ++i) {
	    Object3D obj = cLink.getBranch(i);
	    int g = findObjectGroupId(obj);
	    if (g < 0) {
		throw new RnaModelException("Nucleotide " + obj.getFullName() + " is not part of movable block! Use option blocks=subtree1;subtree2...");
	    }
	    CoordinateSystem3D cs = (CoordinateSystem3D)(coordinateSystems.get(g));
	    CoordinateSystem3D csClone = cs;
	    if (cLink.getSymId(i) > 0) { // symmetry copy is being used!!
		csClone = (CoordinateSystem3D)(cs.cloneDeep());
		csClone.activeTransform(symCopies.get(cLink.getSymId(i)));
	    }
	    csUsed.add(csClone);
	}
	assert (csUsed.size() == cLink.getBranchCount());
	Properties properties = new Properties();
	properties = cLink.computeScore(csUsed);
	assert properties != null;
	    // log.info("Score of junction constraint: " + result + " : " + cLink.toString());
	return properties;
    }

    private double scoreMotifConstraint(MotifConstraintLink mLink, List<CoordinateSystem> coordinateSystems) throws RnaModelException {


      int g1 = findObjectGroupId( mLink.getObj1() );
    	int g2 = findObjectGroupId( mLink.getObj2() );

    	assert g1 >= 0;
    	assert g2 >= 0;
    	assert coordinateSystems.get(g1).isValid();
    	assert coordinateSystems.get(g2).isValid();
    	CoordinateSystem cs1 = coordinateSystems.get(g1);
      CoordinateSystem cs2 = coordinateSystems.get(g2);

    	double result = mLink.computeScore(cs1, cs2);
    	return result;
    }

    private double scoreSuperimposeConstraint(SuperimposeConstraintLink link, List<CoordinateSystem> coordinateSystems) throws RnaModelException {

      int g1 = findObjectGroupId( link.getObj1() );
    	int g2 = findObjectGroupId( link.getObj2() );

    	assert g1 >= 0;
    	assert g2 >= 0;
    	assert coordinateSystems.get(g1).isValid();
    	assert coordinateSystems.get(g2).isValid();
    	CoordinateSystem cs1 = coordinateSystems.get(g1);
      CoordinateSystem cs2 = coordinateSystems.get(g2);

    	double result = link.computeScore(cs1, cs2);
    	return result;
    }

    private double scoreBlockCollisionConstraint(BlockCollisionConstraintLink link, List<CoordinateSystem> coordinateSystems) throws RnaModelException { //TODO something wrong?

      int g1 = findObjectGroupId( link.getObj1().getChild(0) );
      int g2 = findObjectGroupId( link.getObj2().getChild(0) );

      assert g1 >= 0;
      assert g2 >= 0;
      assert coordinateSystems.get(g1).isValid();
      assert coordinateSystems.get(g2).isValid();
      CoordinateSystem cs1 = coordinateSystems.get(g1);
      CoordinateSystem cs2 = coordinateSystems.get(g2);

      double result = link.computeScore(cs1, cs2);
      // System.out.println("Block collision score: "+result);
      return result;
    }

    private double computeElectrostaticTerm(double residueDistance) {
	if (electrostaticWeight == 0.0) {
	    return 0.0;
	}
	if (residueDistance < electrostaticLength) {
	    return electrostaticWeight * Math.exp(- (residueDistance*residueDistance)/(electrostaticLength*electrostaticLength));
	}
	return electrostaticWeight * Math.exp(-1) * electrostaticLength/residueDistance;
    }

    private double computeVdwTerm(double residueDistance) {
	if ((vdwWeight == 0.0) || (residueDistance > 5.0 * vdwLength)) {
	    return 0.0;
	}
	return vdwWeight * Math.exp(-(residueDistance*residueDistance)/(vdwLength*vdwLength));
    }

    /** Used for 2-bead model. Interaction is similar to Gaussian normal  */
    private double computeVdwTerm2(double residueDistance) {
	if ((vdwWeight2 == 0.0) || (residueDistance > 5.0 * vdwLength2)) {
	    return 0.0;
	}
	return vdwWeight2 * Math.exp(-(residueDistance*residueDistance)/(vdwLength2*vdwLength2));
    }

    public void setFixedRotationAxis(List<Vector3D> _axisList) {
	axisList = _axisList;
    }

    private double scoreRepulsion(List<CoordinateSystem> coordinateSystems) {
	updateObjectResidueBlocks(coordinateSystems); // apply coordinate systems
	// System.out.println("Current position of first object: " + objectResidueBlocksCurr[0][0]);
	double result = 0.0;
	int scn = symVdwActive.length; // number of symmetry copies used for repulsion term. "1" for no symmetries
	int blockOff = 1; // normal case: no computation within same block
	double dist = 0.0;
	if (scn > 1) {
	    blockOff = 0; // must compute interaction within same block because of symmetry copies
	}
	for (int block = 0; block < objectResidueBlocksCurr.length; ++block) {
	    for (int block2 = block+blockOff; block2 < objectResidueBlocksCurr.length; ++block2) {
		for (int res1 = 0; res1 < objectResidueBlocksCurr[block].length; res1+=electroStride) {
		    for (int res2 = 0; res2 < objectResidueBlocksCurr[block2].length; res2+=electroStride) {
			if (symCopies.size() < 2) {
			    dist = objectResidueBlocksCurr[block][res1].distance(objectResidueBlocksCurr[block2][res2]);
			    result += computeElectrostaticTerm(dist);
			    result += computeVdwTerm(dist);
			} else {
			    for (int sc1 = 0; sc1 < scn; sc1++) {
				Vector3D v1 = objectResidueBlocksCurr[block][res1];
				if (sc1 > 0) {
				    assert(symVdwActive[sc1] < symCopies.size());
				    v1 = symCopies.get(symVdwActive[sc1]).activeTransform(v1);
				} // otherwise: symCopies.get(0) corresponds to original orientation of cartesion coordiante system, neutral transform
				for (int sc2 = 0; sc2 < scn; sc2++) {
				    if ((block == block2) && (sc1 == sc2)) {
					continue;
				    }
				    Vector3D v2 = objectResidueBlocksCurr[block2][res2];
				    if (sc2 > 0) {
					v2 = symCopies.get(symVdwActive[sc2]).activeTransform(v2);
				    } // otherwise: symCopies.get(0) corresponds to original orientation of cartesion coordiante system, neutral transform
				    dist = v1.distance(v2);
				    result += computeElectrostaticTerm(dist);
				    result += computeVdwTerm(dist);
				}
			    }
			}
		    }
		}
	    }
	}
	return result;
    }

    private void initObjectResidueBlocks() {
	assert objectBlocks.size() > 0;
	objectResidueBlocksOrig = new Vector3D[objectBlocks.size()][];
	objectResidueBlocksCurr = new Vector3D[objectBlocks.size()][];
	// count number of nucleotides for each block:
	for (int block = 0; block < objectBlocks.size(); ++block) {
	    Object3DSet blockResidues = new SimpleObject3DSet();
	    for (int blockItem = 0; blockItem < objectBlocks.get(block).size(); ++blockItem) {
		blockResidues.merge(Object3DTools.collectByClassName(objectBlocks.get(block).get(blockItem), "Nucleotide3D"));
	    }
	    assert (objectResidueBlocksOrig != null);
	    assert (block < objectResidueBlocksOrig.length);
	    assert (blockResidues != null);
	    assert (blockResidues.size() > 0);
	    objectResidueBlocksOrig[block] = new Vector3D[blockResidues.size()];
	    objectResidueBlocksCurr[block] = new Vector3D[blockResidues.size()];
	    for (int res = 0; res < blockResidues.size(); ++res) {
		assert (objectResidueBlocksOrig != null);
		assert (block < objectResidueBlocksOrig.length);
		assert (res < objectResidueBlocksOrig[block].length);
		assert (res < blockResidues.size());
		assert (blockResidues.get(res) != null);
		if (blockResidues.get(res).getChild(refAtomName) == null) {
		    System.out.println("Error while initializing base pair constraint optimization: a residue did not have an atom of type "
				       + refAtomName);
		    System.exit(1);
		}
		objectResidueBlocksOrig[block][res] = blockResidues.get(res).getChild(refAtomName).getPosition();
		objectResidueBlocksCurr[block][res] = new Vector3D(objectResidueBlocksOrig[block][res]);
	    }
	}
	assert(objectResidueBlocksOrig.length == objectBlocks.size());
    }

    private void updateObjectResidueBlocks(List<CoordinateSystem> coordinateSystems) {
	assert (objectResidueBlocksOrig.length == coordinateSystems.size());
	for (int block = 0; block < objectResidueBlocksOrig.length; ++block) {
	    if ((chosen >= 0) && (block != chosen)) {
		continue;
	    }
	    for (int res = 0; res < objectResidueBlocksOrig[block].length; ++res) {
		objectResidueBlocksCurr[block][res] = coordinateSystems.get(block).activeTransform(objectResidueBlocksOrig[block][res]);
	    }
	}
    }

    /** central method for scoring modified coordinate systems */
    protected double scoreCoordinateSystems(List<CoordinateSystem> coordinateSystems, Properties properties) throws RnaModelException {
	double score = 0.0;
	resetCoordScoreAverages();
	for (InteractionLink link : basepairConstraints) {
	    double term = scoreBasepairConstraint(link, coordinateSystems);
	    // if (term > 0.0) {
	    // 	log.info("Added interactionLink score term " + term + " " + (term * term));
	    // }
	    score += term * term; // weight worst constraints higher!
	}
	for (ConstraintLink link : distanceConstraints) {
	    double term = scoreDistanceConstraint(link, coordinateSystems);
	    if ((term > 0.0) && (verboseLevel > 2)) {
		log.info("Added distanceConstraintLink score term " + term + " " + (term * term));
	    }
	    score += term * term;
	}
	for (AngleLink link : angleConstraints) {
	    double term = scoreAngleConstraint(link, coordinateSystems);
	    if ((term > 0.0) && (verboseLevel > 2)) {
		log.info("Added angleLink score term " + term + " " + (term * term));
	    }
	    score += term * term;
	}
	for (TorsionLink link : torsionConstraints) {
	    double term = scoreTorsionConstraint(link, coordinateSystems);
	    if ((term > 0.0) && (verboseLevel > 2)) {
		log.info("Added torsionLink score term " + term + " " + (term * term));
	    }
	    score += term * term;
	}
	for (JunctionMultiConstraintLink link : junctionConstraints) {
	    double term = scoreJunctionConstraint(link, coordinateSystems);
	    if ((term > 0.0) && (verboseLevel > 2)) {
		log.info("Added junctionMultiLink score term " + term + " " + (term * term));
	    }
	    score += term * term;
	}
	for (JunctionDBConstraintLink link : junctionDBConstraints) {
	    Properties props = scoreJunctionDBConstraint(link, coordinateSystems);
	    double term = Double.parseDouble(props.getProperty("score"));
	    if (properties != null) {
		PropertyTools.mergeProperties(properties, props, link.getName() + ".");
	    }
	    if ((term > 0.0) && (verboseLevel > 2)) {
		log.info("Added junctionDBConstraintLink score term " + term + " " + (term * term));
	    }
	    score += term * term;
	}
  for (MotifConstraintLink link : motifConstraints) {
	    double term = scoreMotifConstraint(link, coordinateSystems);
	    if ((term > 0.0) && (verboseLevel > 2)) {
		log.info("Added motifConstraintLink score term " + term );
	    }
	    score += term;
	}
  for (SuperimposeConstraintLink link : superimposeConstraints) {
      double term = scoreSuperimposeConstraint(link,coordinateSystems);
      if ((term > 0.0) && (verboseLevel > 2)) {
    log.info("Added superimposeConstraintLink score term " + term );
      }
      score += term;
  }
  for (BlockCollisionConstraintLink link : blockCollisionConstraints) {
      double term = scoreBlockCollisionConstraint(link, coordinateSystems); //TODO something wrong?
      if ((term > 0.0) && (verboseLevel > 2)) {
    log.info("Added block collision score term " + term );
      }
      score += term;
  }
	if ((electrostaticWeight != 0.0) || (vdwWeight != 0.0)) {
	    double repTerm = scoreRepulsion(coordinateSystems);
	    if ((repTerm > 0.0) && (verboseLevel > 2)) {
		log.info("Added electrostatic repulsion score term " + repTerm);
	    }
	    score += repTerm;
	}
	// compute geometric mean:
	for (int i = 0; i < coordScoreAverages.length; ++i) {
	    if (groupConstraintCounts[i] <= 0) {
		coordScoreAverages[i] = 1.0;
	    }
	    else {
		assert groupConstraintCounts[i] > 0;
		coordScoreAveragesTmp[i] = Math.pow(coordScoreAveragesTmp[i], 1.0/(double)(groupConstraintCounts[i]));
		assert DoubleTools.isValidAndPositive(coordScoreAveragesTmp[i]);
		// if (coordScoreAveragesTmp[i] < coordScoreAverages[i]) {
		coordScoreAverages[i] = coordScoreAveragesTmp[i];
		// }
	    }
	}
	firstPass = false;
	if (verboseLevel > 1) {
	    log.info("Current score of coordinate systems: " + score);
	}
	return score;
    }

    private Object3DSet convertToObject3DSet(List<Object3D> objects) {
	Object3DSet result = new SimpleObject3DSet();
	for (int i = 0; i < objects.size(); ++i) {
	    result.add(objects.get(i));
	}
	return result;
    }

    protected List<CoordinateSystem> cloneCoordinateSystems(List<CoordinateSystem> coords) {
	List<CoordinateSystem> result = new ArrayList<CoordinateSystem>();
	for (int i = 0; i < coords.size(); ++i) {
	    CoordinateSystem3D csClone = (CoordinateSystem3D)(coords.get(i).cloneDeep());
	    assert csClone.size() == ((CoordinateSystem3D)(coords.get(i))).size();
	    assert csClone.size() == 3;
	    result.add(csClone);
	}
	return result;
    }

    /** Copies coordinate systems. Notice that the order is opposite of the norm: copyCoordinateSystems(destination, source)
     */
    protected void copyCoordinateSystems(List<CoordinateSystem> optCoords,
				       List<CoordinateSystem> origCoords) {
	assert optCoords != null;
	assert origCoords != null;
	assert optCoords.size() == origCoords.size();
	for (int i = 0; i < origCoords.size(); ++i) {
	    CoordinateSystem3D cs = (CoordinateSystem3D)(optCoords.get(i));
// 	    log.info("Coordinate system " + (i+1) + " " + cs.toString());
// 	    assert ((CoordinateSystem3D)(optCoords.get(i))).size() == 3;
// 	    assert ((CoordinateSystem3D)(origCoords.get(i))).size() == 3;
	    optCoords.get(i).copy(origCoords.get(i));
	}
    }

    /** how many constraints are invold in group n. TODO: should be much faster */
    private int countGroupConstraints(int n) {
	int count = 0;
	for (int i = 0; i < basepairConstraints.size(); ++i) {
	    Object3D obj1 = basepairConstraints.get(i).getObj1();
	    Object3D obj2 = basepairConstraints.get(i).getObj2();
	    // how do I find the index of this branch descriptor?
	    if ((findObjectGroupId(obj1) == n) || (findObjectGroupId(obj2) == n)) {
		++count;
	    }
	}
	for (int i = 0; i < distanceConstraints.size(); ++i) {
	    Object3D obj1 = distanceConstraints.get(i).getObj1();
	    Object3D obj2 = distanceConstraints.get(i).getObj2();
	    // how do I find the index of this branch descriptor?
	    if ((findObjectGroupId(obj1) == n) || (findObjectGroupId(obj2) == n)) {
		++count;
	    }
	}
	for (int i = 0; i < distanceConstraints.size(); ++i) {
	    Object3D obj1 = distanceConstraints.get(i).getObj1();
	    Object3D obj2 = distanceConstraints.get(i).getObj2();
	    // how do I find the index of this branch descriptor?
	    if ((findObjectGroupId(obj1) == n) || (findObjectGroupId(obj2) == n)) {
		++count;
	    }
	}
	// Fixit: check angle constraints, torsion constraints
	for (int i = 0; i < junctionConstraints.size(); ++i) {
	    for (int j = 0; j < junctionConstraints.get(i).size(); ++j) {
		Object3D obj = junctionConstraints.get(i).getObj(j);
		if (findObjectGroupId(obj) == n) {
		    ++count;
		    break;
		}
	    }
	    // Object3D obj2 = distanceConstraints.get(i).getObj2();
	    // how do I find the index of this branch descriptor?
	}
	for (int i = 0; i < junctionDBConstraints.size(); ++i) {
	    for (int j = 0; j < junctionDBConstraints.get(i).size(); ++j) {
		Object3D obj = junctionDBConstraints.get(i).getObj(j);
		if (findObjectGroupId(obj) == n) {
		    ++count;
		    break;
		}
	    }
	    // Object3D obj2 = distanceConstraints.get(i).getObj2();
	    // how do I find the index of this branch descriptor?
	}
	return count;
    }

    private void mutateCoordinateSystems(List<CoordinateSystem> coords, double annealingMult) {
	assert coords != null;
	assert !Double.isInfinite(annealingMult);
	assert !Double.isNaN(annealingMult);
	int startId = 0;
	if (keepFirstFixedMode) {
	    startId = 1;
	}
	// first move coordinate systems that are not end points:
	assertMovables();

	mutator.copy(mutatorOrig);
	// assert coordScoreAverages[i] >= 0.0;
	// assert DoubleTools.isValidAndPositive(coordScoreAverages[i]);
	// use geometric mean of scores:
	double totalScale = annealingMult; // * scaleMult * coordScoreAverages[i];
	assert DoubleTools.isReasonable(totalScale);
	log.fine("Initial scale for mutator: " + totalScale + " " + annealingMult + " "
		 + scaleMult); //  + " " + coordScoreAverages[i]);
	if (totalScale > scaleLimit) {
	    totalScale = scaleLimit;
	}
	assert !Double.isInfinite(totalScale);
	assert !Double.isNaN(totalScale);
	mutator.scaleAngleStep(totalScale);
	mutator.scaleTranslationStep(totalScale);
	Random rand = Randomizer.getInstance(); // singleton pattern
	do {
	    chosen = rand.nextInt(coords.size()); // choose only one coordinate system
	}
	while (!moveFlags[chosen]);
	if (axisList != null) {
	    mutator.setFixedAxis(axisList.get(chosen)); // check
	}
	mutator.mutate(coords.get(chosen));

	//	for (int i = startId; i < coords.size(); ++i) {
	    // log.fine("Trying to mutate coordinate system " + i);
	// 	    if (!moveFlags[i]) {
		// log.fine("Not mutating coordinate system " + i + " because it is not allowed to move");
	//		continue; // do not mutate this coordinate system
	// }
	    /*
	    if (groupConstraintCounts[i] < 2) {
		log.fine("Not mutating coordinate system " + i + " because it has only one constraint: " + groupConstraintCounts[i]);
		continue;// only move if more than one constraint at this coordinate system
	    }
	    */
//  	    log.fine("Mutator used for coordinate system: " + (i+1) + " " + coords.get(i)
//  		     + " : " + totalScale + " mut: " + mutator);
	    // mutator.rotate(coords.get(i));
	    // mutator.translate(coords.get(i));
	// 	    if (axisList != null) {
	// mutator.setFixedAxis(axisList.get(i)); // check
	// }
	// mutator.mutate(coords.get(i));
	    // log.fine("result of mutation: " + coords.get(i));
	// }
	// assertMovables();
	// now move coordinate systems that are end points (having only one constraint)
	/*
	for (int i = 0; i < basepairConstraints.size(); ++i) {
	    try {
		applyEndPointHelixConstraint(basepairConstraints.get(i), coords);
	    }
	    catch (FittingException fe) {
		log.fine("Not allowed to move coordinate system for end point fitting: " + fe.getMessage());
	    }
	}
	*/

	assertMovables();
    }

    private void printCoordinateSystems(PrintStream ps, List<CoordinateSystem> coords) {
	for (int i = 0; i < coords.size(); ++i) {
	    ps.println("" + (i+1) + " " + coords.get(i).toString());
	}
    }

    /** returns true, if at least one coordinate system to be optimized has more than one constraints attached to it */
    private boolean hasComplexGroups() {
// 	for (int i = 0; i < groupConstraintCounts.length; ++i) {
// 	    if (moveFlags[i] && (groupConstraintCounts[i] > 1)) {
// 		return true;
// 	    }
// 	}
// 	return false;
	return true; // TODO : better implementation
    }

    private String boolFlagString(boolean[] flags) {
	String s = "";
	for (int i = 0; i < flags.length; ++i) {
	    s = s + flags[i] + " ";
	}
	return s;
    }

    public double getScoreMinimumLimit() { return this.scoreMinimumLimit; }

    public void setKeepFirstFixedMode(boolean flag) { this.keepFirstFixedMode = flag; }

    public void setScoreMinimumLimit(double x) { this.scoreMinimumLimit = x; }

    public Properties optimize() {
	assert basepairConstraints != null;
	assert torsionConstraints != null;
	assert distanceConstraints != null;
	assert optCoordinateSystems != null;
  System.out.println(optCoordinateSystems);
	log.info("Starting optimization! Base pair constraints: " + basepairConstraints.size()
		 + " distance constraints: " + distanceConstraints.size()
		 + " angle constraints: " + angleConstraints.size()
		 + " torsion constraints: " + torsionConstraints.size()
		 + " junction constraints: " + junctionConstraints.size()
		 + " junction DB constraints: " + junctionDBConstraints.size()
		 + " number of blocks: " + optCoordinateSystems.size()
		 + " number of defined symmetry operations: " + symCopies.size());
	log.info("Weight of repulsion term: " + electrostaticWeight + " kT: " + ktOrig);
	log.info("Weight of Vdw term: " + vdwWeight + " kT: " + ktOrig);
	Properties properties = new Properties();
	chosen = -1;
	try {
	Random rand = Randomizer.getInstance(); // singleton pattern
	if ((optCoordinateSystems == null) || (optCoordinateSystems.size() == 0)) {
	    String errMsg = "No coordinate systems defined for optimization. Maybe add helix constraints?";
	    log.info(errMsg);
	    properties.setProperty(MESSAGE, errMsg);
	    return properties;
	}
	log.info("Number of relevant coordinate systems: " + origCoordinateSystems.size());
	log.info("Movable coordinate systems: " + boolFlagString(moveFlags));
	String s2 = "";
	firstPass = true;
	// mutator.copy(mutatorOrig); // reset mutator in case several optimizations are run
// 	printCoordinateSystems(origCoordinateSystems);
// 	printCoordinateSystems(optCoordinateSystems);
	copyCoordinateSystems(optCoordinateSystems, origCoordinateSystems); // copies original to opt coordinate systems
	List<CoordinateSystem> saveCoords = cloneCoordinateSystems(optCoordinateSystems);
	List<CoordinateSystem> bestCoords = cloneCoordinateSystems(optCoordinateSystems);
	double oldScore = scoreCoordinateSystems(optCoordinateSystems, null);
	saveCoordScoreAverages();
	double bestScore = oldScore;
	double kt = ktOrig;
	double oldScoreMul = 0.1; // emperical factor for initial kt
	if ((oldScoreMul*oldScore) > kt) {
	    kt = oldScoreMul * oldScore;
	}
	properties.setProperty(START_SCORE,""+ bestScore);
	int iter = 0;
	assertMovables();

	for (iter = 0; iter < numberSteps; ++iter) {
	    double annealingMult = Math.pow(annealingFactor, (int)(iter/annealingInterval));
	    assert !Double.isNaN(annealingMult);
	    kt = ktOrig * annealingMult;
	    if ((iter % outputInterval) == 0) {
		String scoreString = "";
		for (int i = 0; i < coordScoreAverages.length; ++i) {
		    scoreString = scoreString + Math.sqrt(coordScoreAverages[i]) + " ";
		}
		log.info("Iteration: " + iter + " Best score so far: " + bestScore
			 + " . Annealing parameters: " + annealingMult
			 + " , " + kt // + " coordinate system multipliers: " + scoreString
			 + " mutator: " + mutator.toString());
	    }
	    copyCoordinateSystems(saveCoords, optCoordinateSystems); // copies working copy to safety copy
	    mutateCoordinateSystems(optCoordinateSystems, annealingMult);
	    double newScore = scoreCoordinateSystems(optCoordinateSystems, null);
	    // System.out.println("New score: " + newScore);
	    if (Math.exp(-((newScore-oldScore)/kt)) > rand.nextDouble()) {
		oldScore = newScore;
		saveCoordScoreAverages();
		if (newScore < bestScore) {
		    bestScore = newScore;
		    copyCoordinateSystems(bestCoords, optCoordinateSystems); // stores best trafos
		    if (verboseLevel > 0) {
			log.info("New best score found: " + bestScore + " at iteration " + (iter + 1) + " annealing factor: " + annealingMult
				 + " kt: " + kt);
		    }
		    // outputCoordinateSystems(bestCoords);
		    if (newScore < scoreMinimumLimit) {
			log.info("Quitting optimization, because score is lower than minimum score cutoff: " + scoreMinimumLimit);
			break;
		    }
		}
	    }
	    else {
		log.fine("Rejecting step : " + newScore + " with so far best score: " + bestScore + " at iteration " + iter);
		copyCoordinateSystems(optCoordinateSystems, saveCoords); // restore previous coordinate systems
		restoreCoordScoreAverages();
	    }
	    if (newScore < errorScoreLimit) {
		log.info("Optimization goal achieved!");
		break;
	    }
	    if (!hasComplexGroups()) {
		log.info("Quitting optimization loop, because all coordinate systems could be placed analytically.");
		break;
	    }
	}
	double bestScore2 = scoreCoordinateSystems(bestCoords, properties);
	if (Math.abs(bestScore2 - bestScore) >= 0.01) {
	    log.warning("Constraint score not reproducible: " + bestScore2 + " " + bestScore);
	}
	// assert (Math.abs(bestScore2 - bestScore) < 0.01);
	// apply transformation to objects!
	applyTransformations(bestCoords);
	properties.setProperty(NUMBER_STEPS, "" + (iter+1));
	properties.setProperty(FINAL_SCORE, ""+ bestScore);
	}
	catch (RnaModelException ex) {
	    properties.setProperty(ERROR, ex.getMessage());
	}
	log.info("Optimization finished: " + properties);
	assertMovables();
	return properties;
    }

    void outputCoordinateSystems(PrintStream ps, List<CoordinateSystem> coords) {
	ps.println("Coordinate systems: ");
	for (int i = 0; i < coords.size(); ++i) {
	    ps.println("" + i + " " + coords.get(i));
	}
    }

    /** Weight of "electrostatic" term. The term represents a general repulsion between residues, not necessarilly electrostatics in its strict meaning. */
    public void setElectrostaticWeight(double value) { this.electrostaticWeight = value; }

    /** Sets value of kT */
    public void setKT(double value) { ktOrig = value; }

    public void setOutputInterval(int interval) { this.outputInterval = interval; }

    public void setVdwWeight(double value) { this.vdwWeight = value; }

    public void setVdwLength(double value) { this.vdwLength = value; }

    public void setVerboseLevel(int level) { this.verboseLevel = level; }

    protected void assertMovables() {
	for (int i = 0; i < moveFlags.length; ++i) {
	    if (origCoordinateSystems.get(i) == optCoordinateSystems.get(i)) {
		assert false;
	    }
	    if ((!moveFlags[i])  && (origCoordinateSystems.get(i).distance(optCoordinateSystems.get(i)) > 0.1)) {
		assert false; // coordinate system was moved!
	    }
	}
    }

}
