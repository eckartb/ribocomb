package rnadesign.rnamodel;

import java.util.*;
import tools3d.*;
import tools3d.objects3d.*;
import tools3d.symmetry2.*;

public class BasepairOptimizerTools {

        /** Optimizes blocks according to base pair constraints */
    public static Properties optimizeBasepairs(LinkSet links, LinkSet basePairDB, int numSteps, double errorScoreLimit, 
					       double electrostaticWeight,
					       double vdwWeight,
					       double kt,
					       List<Object3DSet> objectBlocks,
					       List<Vector3D> fixedRotationAxisList,
					       int[] symVdwActive,
					       boolean keepFirstFixed,
                                               SymCopies symCopies) {
	assert objectBlocks != null;
	assert objectBlocks.size() > 0;
	BasepairOptimizer basepairOptimizer = new BasepairOptimizer(links, objectBlocks, numSteps, errorScoreLimit, basePairDB,
								    symVdwActive, symCopies);
	basepairOptimizer.setElectrostaticWeight(electrostaticWeight);
	basepairOptimizer.setKeepFirstFixedMode(keepFirstFixed);
	basepairOptimizer.setVdwWeight(vdwWeight);
	if (fixedRotationAxisList != null) {
	    basepairOptimizer.setFixedRotationAxis(fixedRotationAxisList);
	}
	basepairOptimizer.setKT(kt);
	Properties properties = basepairOptimizer.optimize();
	assert properties != null;
	return properties;
    }


}
