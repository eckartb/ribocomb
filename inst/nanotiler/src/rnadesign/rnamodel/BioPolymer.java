/** 
 * This interface describes the concept of an RNA strand.
 * An RNA strand is a Object3D with one and only one
 * sequence defined.
 */
package rnadesign.rnamodel;

import sequence.*;
import tools3d.Vector3D;
import tools3d.objects3d.Object3D;

/**
 * @author Eckart Bindewald
 *
 */
public interface BioPolymer extends Molecule3D, Sequence {

    /** appends other polymer to end */
    public void append(BioPolymer other);

    /** appends other polymer to end */
    public void appendAndDelete(BioPolymer other);

    /** returns cloned subsequence startPos...-stopPos-1 */
    public Object cloneDeep(int startPos, int stopPos);

    /** adds single sequence. Does not throw DuplicateNameException because only one sequence is stored at a time. */
    // public void setSequence(Sequence s);

    /** returns number of children that are of type Nucleotide3D */
    public int getResidueCount();

    /** returns n'th nucleotide */
    public Residue3D getResidue3D(int n);

    /** returns residue whose assigned number is equal to n. */
    public Residue3D getResidueByAssignedNumber(int n);

    /** returns residue whose PDB id (stored in properties.PDB_RESIDUE_ID) number is equal to n. */
    public Residue3D getResidueByPdbId(int n);

    /** returns a position even if there is no child residue n defined */
    public Vector3D getResiduePosition(int n);

    /** appends other polymer at start */
    public void prepend(BioPolymer other);

    /** appends other polymer at start */
    public void prependAndDelete(BioPolymer other);

    /** rotates around angle no angleId of residue with id residueId (counting starts from zero) */
    public void rotateAroundBackboneAngle(int residueId, int angleId, double angle) throws RnaModelException;

    /** Shortens current sequence to have length position (residues 0..position-1), and returns new sequence with positions position...length-1.
     * No cloning of residues is being done, just reorganizing. */
    public BioPolymer split(int position, String newName);

}
