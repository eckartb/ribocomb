package rnadesign.rnamodel;

import tools3d.Vector3D;

/** helper methods for general biopolymers (super class of ProteinStrand and NucleotideStrand).
 * TODO : residue.getPosition() might give different answers (center of mass currently). Use better definition! 
 */
public class BioPolymerTools {

    /** computes direction vector in strand direction (N-Terminus to C-terminus for proteins, 5' to 3' for nucleotide strands */
    public static Vector3D computeDirection(BioPolymer strand, int residueIndex) {
	assert strand.size() > 1;
	Vector3D p0 = strand.getResidue3D(residueIndex).getPosition();
	if (residueIndex >= (strand.getResidueCount() - 1)) {
	    assert (residueIndex > 0);
	    Vector3D p1 = strand.getResidue3D(residueIndex-1).getPosition();
	    p0.sub(p1);
	    p0.normalize();
	    return p0;
	}
	else if (residueIndex == 0) {
	    assert ((residueIndex +1 ) < strand.getResidueCount());
	    Vector3D p1 = strand.getResidue3D(residueIndex+1).getPosition();
	    p1.sub(p0);
	    p1.normalize();
	    return p1;
	}
	Vector3D pForw = strand.getResidue3D(residueIndex+1).getPosition();
	Vector3D pBack = strand.getResidue3D(residueIndex-1).getPosition();
	Vector3D p1 = pForw.minus(p0);
	Vector3D p2 = p0.minus(pBack);
	p1.add(p2);
	p1.normalize();
	return p1;
    }

    /** generates cloned subsequence with residues start, ... start+length-1 */
    public static BioPolymer generateSubSequence(BioPolymer strand, int start, int length) {
	BioPolymer result = (BioPolymer)(strand.cloneDeep());
	result.clear();
	for (int i = 0; i < length; ++i) {
	    result.insertChild(strand.getResidue3D(i+start));
	}
	return result;
    }

}
