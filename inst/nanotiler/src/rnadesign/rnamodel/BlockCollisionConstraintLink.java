/** implements concept of two linked objects */
package rnadesign.rnamodel;

import java.util.List;
import java.util.ArrayList;

import tools3d.objects3d.SimpleLink;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DSet;
import tools3d.objects3d.Object3DSetTools;
import tools3d.Vector3D;
import tools3d.Vector3DTools;
import tools3d.CoordinateSystem;
import sequence.Residue;

public class BlockCollisionConstraintLink extends SimpleLink {

    // private Residue start1;
    // private Residue start2;
    // private Residue end1;
    // private Residue end2;
    // Vector3D[] h_1 = new Vector3D [2];
    // Vector3D[] h_2 = new Vector3D [2];
    
    double distance = 9.0;
    Vector3D[] coord1;
    Vector3D[] coord2;

    public BlockCollisionConstraintLink(Object3D o1, Object3D o2) {
	super(o1, o2);

    // Object3D s11 = o1.getChild(0);
    // Object3D s12 = o1.getChild(1);
    // Object3D s21 = o2.getChild(0);
    // Object3D s22 = o2.getChild(1);
    // System.out.println("Block collision between "+o1.getClass()+" and "+o2.getClass());
    // System.out.println("children: "+s11.getClass()+" "+s12.getClass()+" "+s21.getClass()+" "+s22.getClass());
    // 
    // h_1 = findHelixEndPoints((RnaStrand)s11,(RnaStrand)s12);
    // h_2 = findHelixEndPoints((RnaStrand)s21,(RnaStrand)s22);
    
    Object3DSet atms1 = AtomTools.findAtoms(o1);
    Object3DSet atms2 = AtomTools.findAtoms(o2);
    coord1 = Object3DSetTools.getCoordinates(atms1);
    coord2 = Object3DSetTools.getCoordinates(atms2);

    }

    public double computeScore( CoordinateSystem cs1, CoordinateSystem cs2){
      // Vector3D h1 [] = new Vector3D[2];
      // Vector3D h2 [] = new Vector3D[2];
      // 
      // h1 [0] = cs1.activeTransform(h_1[0]);
      // h1 [1] = cs1.activeTransform(h_1[1]);
      // h2 [0] = cs2.activeTransform(h_2[0]);
      // h2 [1] = cs2.activeTransform(h_2[1]);
      // System.out.println ("Internal Block Collision Score: " + score);
      // return score;
      
      Vector3D[] newCoord1 = new Vector3D[ coord1.length ];
      Vector3D[] newCoord2 = new Vector3D[ coord2.length ];
      
      for(int i=0; i<coord1.length; i++){
         newCoord1[i] = cs1.activeTransform( coord1[i] );
       }
      for(int i=0; i<coord2.length; i++){
         newCoord2[i] = cs2.activeTransform( coord2[i] );
      }

      double collcount = 0.0;
      for(int i=0; i<newCoord1.length; i+=3){
        for(int j=0; j<newCoord2.length; j+=3){
          if(newCoord1[i].distance(newCoord2[j]) < distance){
            collcount += 1;
          }
        }
      }

      // double sqrScore = Math.pow(score,2);
      return collcount;
      
      
      // for (int i = 0; i < h1.length; ++i) {
      // System.out.println("" + (i + 1) + " : " + h1[i]);
      // }
      // for (int i = 0; i < h2.length; ++i) {
      // System.out.println("" + (i + 1) + " : " + h2[i]);
      // }
      // 
      // for (int i = 0; i < h1_.length; ++i) {
      // System.out.println("" + (i + 1) + " : " + h1_[i]);
      // }
      // for (int i = 0; i < h2_.length; ++i) {
      // System.out.println("" + (i + 1) + " : " + h2_[i]);
      // }
      // Vector3D end11 = h1_[0];
      // Vector3D end12 = h1_[1];
      // Vector3D end21 = h2_[0];
      // Vector3D end22 = h2_[1];
      // System.out.println("Overlap: "+Vector3DTools.capsuleIntersectionVolume(h11, h12, 20.0, h21, h22, 20.0));
      //(Vector3D base1, Vector3D top1, double r1, Vector3D base2, Vector3D top2, double r2)
      //all points on helix, r1 radius of helix
      //double score = Vector3DTools.capsuleIntersectionVolume (h1 [0], h1 [1], 10.0, h2 [0], h2 [1], 10.0);
      // double score = Vector3DTools.computeSphereIntersectionVolume (h1 [0], h1 [1], 1000.0, h2 [0], h2 [1], 102.0);
      //double score = Vector3DTools.capsuleIntersectionVolume(h1[0],h1[1],10.0,h2[0],h2[1],10.0);
      // Vector3D h11 = new Vector3D(-689.1926607703246, 536.2127426023994, -301.5023236660967);
      // Vector3D h12 = new Vector3D(-668.0417199973901, 549.7859776545455, -297.7500441854528 );
      // Vector3D h21 = new Vector3D(267.668097401389, -227.11185010380373, 462.4082121684878 );
      // Vector3D h22 = new Vector3D(290.1654654721174, -215.30516032876926 ,462.7891493463009 );
      // Vector3D h11 = new Vector3D(0.0,0.0,0.0);
      // Vector3D h12 = new Vector3D(10.0,0.0,0.0);
      // Vector3D h21 = new Vector3D(0.0,1000.0,0.0);
      // Vector3D h22 = new Vector3D(10.0,1000.0,0.0);
      // 
      // System.out.println("Overlap: "+Vector3DTools.capsuleIntersectionVolume(h11, h12, 0.0, h21, h22, 5.0));
      // 
    // double score = Vector3DTools.capsuleIntersectionVolume(h11, h12, 20.0, h21, h22, 20.0);
    // double score = Vector3DTools.capsuleIntersectionVolume(end11, end12, 10.0, end21, end22, 10.0);
    // double score = 0.0;
    }

    public Vector3D[] findHelixEndPoints(RnaStrand s1, RnaStrand s2){
      
      Vector3D zero_vec = new Vector3D (0.0, 0.0, 0.0);
      
      int size = s1.size();
      List<Residue3D> residues = new ArrayList<Residue3D>();
      residues.add(s1.getResidue3D(0));
      residues.add(s2.getResidue3D(size-1));
      residues.add(s1.getResidue3D(size-1));
      residues.add(s2.getResidue3D(0));
      String[] atomNames = {"C4"};
      List<Atom3D> atoms = new ArrayList<Atom3D>();
    	for (int i = 0; i < residues.size(); ++i) {
    	    for (String name : atomNames) {
    		Object3D child = residues.get(i).getChild(name);
    		if ((child == null) || (!(child instanceof Atom3D))) {
    		    System.out.println("Could not find atom " + name
    							       + " in nucleotide " + residues.get(i).getFullName());
    		}
    		atoms.add((Atom3D)child);
    	    }
    	}

      assert atoms.size() == 4;
      Vector3D[] pos = new Vector3D[4];
      for(int i=0; i<pos.length; i++){
        pos[i]=atoms.get(i).getPosition(0);
      }
      Vector3D[] result = {Vector3D.average(pos[0],pos[1]),Vector3D.average(pos[2],pos[3]), Vector3D.average (zero_vec, zero_vec)};
      return result;
    }

}
