package rnadesign.rnamodel;

import tools3d.CoordinateSystem;
import tools3d.Vector3D;
import tools3d.objects3d.CoordinateSystemObject3D;

public interface BranchDescriptor3D extends CoordinateSystemObject3D {
    
    /** use for method computeHelixPosition */
    public static final int OUTGOING_STRAND = 1;
    public static final int INCOMING_STRAND = 2;
    public static final double DISTANCE_TOLERANCE = 0.0001;

    /** returns direction of branch descriptor */
    Vector3D getDirection(); 

    Vector3D getBasePosition();

    /** returns helix parameters */
    HelixParameters getHelixParameters(); 

    /** computes idealized position of reference atom of base of n'th basePair belonging to strand==OUTGOING_STRAND, or strand==INCOMING_STRAND.
     * Remember: outgoing strand leaves the junction, but points *towards* the stem unless otherwise noted. */
    Vector3D computeHelixPosition(int basePair, int strand);

    /** returns true if content is consistent and plausible */
    boolean isValid();

    void setBasePosition(Vector3D pos);

    /** returns true if incoming strand is equal to outgoing strand */
    boolean isSingleSequence();

    NucleotideStrand getIncomingStrand();

    NucleotideStrand getOutgoingStrand();

    /** returns residue id where incoming strand starts (end of helix - offset; internal counting) */
    int getIncomingIndex();

    /** returns residue id where strand strand ends (end of helix + offset; internal counting) */
    int getOutgoingIndex();

    /** how far "into the helix" are the strand reference residues? Normal: 2 */
    int getOffset();

    /** returns human readable information about branch descriptor */
    String infoString();

    /** propagate branch descriptor along ideal helix. The offset "off" describes the number of base pairs to move.
     * @return Returns clone propagated coordinate system. */
    CoordinateSystem generatePropagatedCoordinateSystem(int offset);

    /** propagate branch descriptor along ideal helix. The offset "off" describes the number of base pairs to move. */
    void propagate(int off);

    void setHelixParameters(HelixParameters prm);

    void setIncomingIndex(int n);

    /** how for "into the helix" are the strand reference residues? Normal: 2 */
    // void setOffset(int offset );    

    /** sets branch descriptor offset. How many bases is the descriptor within the stem?
     * 0 : branch descriptor is at border of stem */
    void setOffset(int n);

    void setOutgoingIndex(int n);

    void setIncomingStrand(NucleotideStrand strand);

    void setOutgoingStrand(NucleotideStrand strand);

}
