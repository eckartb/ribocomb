/** writes scence graph
 * 
 */
package rnadesign.rnamodel;

import java.io.OutputStream;

import tools3d.objects3d.LinkSet;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DAbstractXmlWriter;
import tools3d.objects3d.Object3DTools;
import tools3d.objects3d.SimpleObject3D;

/** writes BranchDescriptor3D in Lisp format.
 * @author Eckart Bindewald
 *
 */
public class BranchDescriptor3DXmlWriter extends Object3DAbstractXmlWriter {
    
    /** TODO not yet implemented */
    public void write(OutputStream os, Object3D tree) {
	assert false;
    }

    /** TODO not yet implemented */
    public void write(OutputStream os, LinkSet links) { assert false; }
    
    
    public String writeString(Object3D tree) {

	assert (tree instanceof BranchDescriptor3D);
	BranchDescriptor3D branch = (BranchDescriptor3D)tree;
	StringBuffer buf = new StringBuffer();
	buf.append(tools.getHeader(branch.getClassName()));
	
	buf.append(SimpleObject3D.toStringBody(branch)); // write basics of object TODO : not format independent!
	buf.append(" " + tools.writeBoolean("isSingle", branch.isSingleSequence()));
	String incomingStrandName = Object3DTools.getFullName(branch.getIncomingStrand());
	String outgoingStrandName = Object3DTools.getFullName(branch.getOutgoingStrand());
	buf.append(" " + incomingStrandName + " " + branch.getIncomingIndex() + " " 
		   + outgoingStrandName + " " 
		   + branch.getOutgoingIndex() );
	
	buf.append(tools.getFooter(branch.getClassName()));

	return buf.toString();
    }
	
}
