package rnadesign.rnamodel;

public interface BranchDescriptorOptimizer {

    public String getClassName();

    /** returns branch descriptor that is a best fit of a helix between two bordering helices. */
    public BranchDescriptor3D optimize(RnaStem3D stem,
				       BranchDescriptor3D branch1,
				       BranchDescriptor3D branch2) throws FittingException;
    
}
