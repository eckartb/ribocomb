package rnadesign.rnamodel;

public class BranchDescriptorOptimizerFactory {

    public static final int SIMPLE_OPTIMIZER = 1;
    public static final int STRETCH_OPTIMIZER = 2;
    public static final int MONTE_CARLO_OPTIMIZER = 3;
    public static final int MORPH_OPTIMIZER = 4;

    /** factory method that generates appropriate branch descriptor optimizer */
    public static BranchDescriptorOptimizer generate(int optimizerCode,
						     FitParameters prm) {
	switch (optimizerCode) {
	case SIMPLE_OPTIMIZER:
	    return new SimpleBranchDescriptorOptimizer(prm);
	case STRETCH_OPTIMIZER:
	    return new StretchedBranchDescriptorOptimizer(prm);
	case MONTE_CARLO_OPTIMIZER:
	    return new MonteCarloBranchDescriptorOptimizer(prm);
	case MORPH_OPTIMIZER:
	    return new MorphBranchDescriptorOptimizer(prm);
	default:
	    assert(false);
	}
	return null;
    }

}
