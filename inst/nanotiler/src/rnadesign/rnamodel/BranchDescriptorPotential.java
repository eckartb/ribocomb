package rnadesign.rnamodel;

import tools3d.Vector3D;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DPotential;

/** assigns score how well a second branch descriptor can be
 * connected to this branch descriptor given a helix */
public class BranchDescriptorPotential implements Object3DPotential {

    private BranchDescriptor3D branch;

    public static final double COLLISION_PENALTY = 10000.0;

    public BranchDescriptorPotential(BranchDescriptor3D branch) {
	this.branch = branch;
    }

    public double computeValue(Object3D obj) {
	assert obj instanceof BranchDescriptor3D;
	BranchDescriptor3D b = (BranchDescriptor3D)(obj);
	// compute angle: not necessary, already part of projection terms
	// double angle = Math.PI - b.getDirection().angle(branch); // the larger the angle the better
	// check if project positive:
	Vector3D dv = b.getPosition().minus(branch.getPosition());
	if (dv.length() > 0.0) {
	    dv.normalize();
	}
	else {
	    return COLLISION_PENALTY;
	}
	// maximize projections (collinearity) by giving negative projection as score
	double ang1 = dv.angle(branch.getDirection()) / Math.PI;
	double ang2 = (Math.PI - dv.angle(b.getDirection())) / Math.PI;
	// double proj = 2.0 - ( branch.getDirection().dot(dv) - b.getDirection().dot(dv) );
	// todo : check if 
	return 0.5 * (ang1 + ang2);
    }

    public BranchDescriptor3D getBranch() { return branch; }

}
