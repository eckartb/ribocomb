package rnadesign.rnamodel;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.IOException;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.logging.Logger;
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;

import org.testng.annotations.Test;

import generaltools.Randomizer;
import generaltools.TestTools;
import graphtools.IntegerList;
import graphtools.IntegerListList;
import numerictools.OptimizationNDResult;
import numerictools.PotentialND;
import numerictools.SimulatedAnnealingOptimizer;
import numerictools.MonteCarloOptimizer;
import numerictools.IntegerArrayTools;
import numerictools.DoubleArrayTools;
import rnasecondary.Stem;
import tools3d.CoordinateSystem;
import tools3d.LineShape;
import tools3d.Matrix3D;
import tools3d.Matrix4D;
import tools3d.Matrix3DTools;
import tools3d.Matrix4DTools;
import tools3d.SuperpositionTools;
import tools3d.SuperpositionResult;
import tools3d.KabschSuperpose;
import tools3d.Vector3D;
import tools3d.Vector3DTools;
import tools3d.objects3d.CoordinateSystem3D;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DFactory;
import tools3d.objects3d.Object3DLinkSetBundle;
import tools3d.objects3d.Object3DSet;
import tools3d.objects3d.Object3DSetTools;
import tools3d.objects3d.Object3DTools;
import tools3d.objects3d.SimpleObject3D;
import tools3d.objects3d.SimpleObject3DSet;

import static rnadesign.rnamodel.PackageConstants.*;

import static rnadesign.rnamodel.RnaConstants.*;

public class BranchDescriptorTools {

    // use simplified algorithm for finding directions of stems?
    public static double RAD2DEG = 180.0 / Math.PI;
    public static double DEG2RAD = Math.PI / 180.0;
    public static int junctionOrderMin = 2; // minimum allowed order of junctions // TODO : allow hairpins: set this variable to one
    public static boolean simpleStemDirectionMode = true; // TODO : currently the simplified version works better ... :-( 
    public static final double JUNCTION_PLAUSIBLE_DIST = 20.0;
    public static final double BRANCH_PLAUSIBLE_DIST = 50.0; // sorry, effectively switching off test... :-( TODO ! 15.0; // 8.0
    public static final double KISSING_LOOP_DIST_MAX = 50.0; // maximum allowed distance between kissing loop stems
    public static final int KISSING_LOOP_INTERNAL_HELIX_MAX = 1; // this many internal helices are allowed in kissing loop. 1 means: only the actual kissing loop interaction helix is allowed
    public static final double HELIX_CENTER_ASSERTION_DIST = 50.0; // 25.0;
    public static final int STEM_FIND_ITER_MAX = 20; // how many tries for finding a stem in 3d
    public static final String REF_ATOM_NAME = "C4*";
    public static final String REF_ATOM_NAME_ALT = "C4'";
    public static int residueRange = 30; // 45; // 80; // 15; // 21; // residues have to be fairly close together between incoming and outgoing strand
    public static final int STEM_ORIENTATION_ARRAY_LEN = CoordinateSystem3D.ARRAY_DIM;
    private static Logger log = Logger.getLogger("NanoTiler_debug");
    private static final int JUNCTION_PATH_ITER_MAX = 500; // 10000; // modify for changing clustering algorithm for generating junctions
    static int debugLevel = 2;

    /** computes "outgoing" direction vector of branch descriptor */
    public static Vector3D computeDirection(BranchDescriptor3D branch) {
	Vector3D p1 = BioPolymerTools.computeDirection(branch.getOutgoingStrand(), branch.getOutgoingIndex());
	Vector3D p2 = BioPolymerTools.computeDirection(branch.getIncomingStrand(), branch.getIncomingIndex()); // use negative of incoming direction
	p1.sub(p2);
	p1.normalize();
	return p1;
    }

    /** returns angle beween two branchDescriptors */
    public static double computeBranchDescriptorAngle(BranchDescriptor3D b1, BranchDescriptor3D b2) {
	return b1.getDirection().angle(b2.getDirection());
    }

    /** returns transformation needed to go from branch descriptor1 to 2. Both branch descriptors should point towards each other! */
    public static Matrix4D computeBranchDescriptorTransformation(BranchDescriptor3D b1, BranchDescriptor3D b2) {
	// b1*query=b2 -> query = b1-1 * b2
	Matrix4D m1 = b1.getCoordinateSystem().generateMatrix4D();
	Matrix4D m2 = b2.getCoordinateSystem().generateMatrix4D();
	return m1.inverse().multiply(m2);
    }

    /** returns transformation needed to go from switching from outgoing to incoming branch descriptor. Difficult!
     * Requirements: applied twice should give original matrix. 
     * Careful: result described new orientation, not transformation matrix. You can compute the transformation matrix 
     * to convert one branchdescriptor into its opposing version by applyging activeTransform(trnsf) with trnsf = newMatrix.multiply(oldMatrix.inverse())
     * see method invertBranchDescriptor(b,propagation) as an example for usage.
     * It is only rotation and shift, basically resulting in opposing z-axis with preserving handedness: m -> A*m ; A * A * m -> m
     * -> A A = 1;
     * @param propagation Most of time, propagation == 1 will be most useful: it is the one adjacent base pair position, that would fit perfectly
     * if coming from an opposing stem.
     * propagation == 0 : switching meaning of stem and reversing stem direction
     */
    public static Matrix4D computeBranchDescriptorInvertedTransformation(Matrix4D m, int propagation) {
	CoordinateSystem cs = new CoordinateSystem3D(m);
	assert Math.abs(cs.getX().length()-1.0) < 0.1;
	assert Math.abs(cs.getY().length()-1.0) < 0.1;
	assert Math.abs(cs.getZ().length()-1.0) < 0.1;
	// propagated new n'th base pair up or down stream:
	if (propagation != 0) {
	    cs.translate(cs.getZ().mul(propagation * HELIX_RISE));
	    double ang = propagation * ((2.0 * Math.PI) / HELIX_BASE_PAIRS_PER_TURN); // should that not be HELIX_PHASE_OFFSET?
	    cs.rotate(cs.getZ(), ang);
	}
	Vector3D newZ = cs.getZ().mul(-1.0); //point in opposite direction
	Vector3D newX = Matrix3DTools.rotate(cs.getX(), cs.getZ(), HELIX_PHASE_OFFSET); // , cs.getPosition());
	assert Math.abs(newX.length()-1.0) < 0.1;
	Vector3D newY = newZ.cross(newX);

	Vector3D newBase = cs.getPosition().plus(cs.getZ().mul(HELIX_OFFSET));
// 	cs.translate(cs.getZ().mul(HELIX_RISE * offs));
// 	// rotate coordinate system:
// 	double ang = ((2.0 * Math.PI) / HELIX_BASE_PAIRS_PER_TURN) * offs; // should that not be HELIX_PHASE_OFFSET?
// 	cs.rotate(cs2.getZ(), ang);	    
	return new CoordinateSystem3D(newBase, newX, newY).generateMatrix4D();
    }

    /** returns transformation needed to go in a circle from branch descriptor one to two. Difficult!
     * Flips second branch descriptor.
     * how to test: for two branch descriptors that are a perfect opposing match,
     * the result should be the unit matrix, because no transformation ("loop") is necessary
     * to go from one branch descriptor to the other.
     */
    public static Matrix4D computeBranchDescriptorFlippedTransformation(BranchDescriptor3D b1,
									BranchDescriptor3D b2) {
	Matrix4D m1 = b1.getCoordinateSystem().generateMatrix4D();
	Matrix4D m2 = computeBranchDescriptorInvertedTransformation(b2.getCoordinateSystem().generateMatrix4D(),1);
	return m1.inverse().multiply(m2);
    }

    /** Tests properties of branch descriptor matrix "inversion" */
    @Test(groups={"fast"})
    public void testComputeBranchDescriptorInvertedTransformation() {
	Matrix3D m3 = new Matrix3D(1.0);
	Vector3D v = new Vector3D(1,2,3);
	Matrix4D m4 = new Matrix4D(m3, v);
	assert m4.isHomogenous(); 
	Matrix4D m4b = computeBranchDescriptorInvertedTransformation(m4, 1);
	Matrix4D m4c = computeBranchDescriptorInvertedTransformation(m4b, 1);
	assert m4c.isHomogenous(); 
	assert (m4.minus(m4c)).norm() < 0.01; // must be close to original 
    }

    /** returns transformation needed to go from branch descriptor1 to 2. Both branch descriptors should point towards each other! */
    public static Matrix4D computeBranchDescriptorInvertedTransformation(BranchDescriptor3D b1, BranchDescriptor3D b2, int propagation) {
	// b1*query=b2 -> query = b1-1 * b2
	Matrix4D m1 = computeBranchDescriptorInvertedTransformation(b1.getCoordinateSystem().generateMatrix4D(), propagation);
	Matrix4D m2 = computeBranchDescriptorInvertedTransformation(b2.getCoordinateSystem().generateMatrix4D(), propagation);
	return m1.inverse().multiply(m2);
    }

    /** Invertes direction of branch descriptor.
     * @param b The branch descriptor (helix end) to modify
     * @param propagation Distance of result in base pairs. Zero: in place 1: one base pair neighboring position etc 
     */
    public static void invertBranchDescriptor(BranchDescriptor3D b, int propagation) {
	CoordinateSystem origCs = b.getCoordinateSystem();
	Matrix4D origMatrix = origCs.generateMatrix4D();
	Vector3D origPos = origCs.getPosition();
	Vector3D oldDirection = origCs.getZ();
	Matrix4D mNew = computeBranchDescriptorInvertedTransformation(origMatrix, propagation);
	// something * mOld   = mNew
	// something = mNew * mOld(-1)
	Matrix4D mTransf = mNew.multiply(origMatrix.inverse());
	b.activeTransform(new CoordinateSystem3D(mTransf));
	Vector3D newPos = b.getCoordinateSystem().getPosition();
	Vector3D newDirection = b.getCoordinateSystem().getZ();
	double dist = origPos.distance(newPos);
	double slop = 5.0;
	// should not move much more than number of base pairs plus helix offset (about 4.2A)
// 	log.info("Distance: " + dist 
// 		 + " Angle difference between inverted branch descriptors: " +Math.toDegrees(oldDirection.angle(newDirection)));
	assert dist < (slop + Math.abs(RnaConstants.HELIX_OFFSET) + propagation * RnaConstants.HELIX_RISE);
	assert oldDirection.angle(newDirection) > Math.toRadians(177.0); // must be opposing each other
    }

    /** uses simulated annealing optimization to identify best fit of stem base wrt to all stem coordinates */
    private static double[] findStemBaseAndDirectionOld(RnaStem3D stem, double rmsTolerance) throws FittingException {
	// log.fine("Starting findStemBaseAndDirection: " + stem.getName());
	// log.fine(StemTools.stemResidueLongInfo(stem));

	Stem stemInfo = stem.getStemInfo();
	int len = stemInfo.size();
	PotentialND stemBasePotential = new StemBasePotential(stem);
	SimulatedAnnealingOptimizer optimizer = new SimulatedAnnealingOptimizer();
	//MonteCarloOptimizer optimizer = new MonteCarloOptimizer();
	//other optimizer options, lacks a step width decay function
	optimizer.setIterMax(2000);
	optimizer.setStepWidth(2.0);
	//	optimizer.setKT(10.0); // compilation problem EB 2012
	// optimizer.setDecay(0.999);
	double[] scalings = new double[9];
	scalings[0] = 1.0;
	scalings[1] = 1.0;
	scalings[2] = 1.0;
	scalings[3] = 0.1; // angles are modified using smaller steps
	scalings[4] = 0.1;
	scalings[5] = 0.1;
	scalings[6] = 0.1;
	scalings[7] = 0.1;
	scalings[8] = 0.1;
	//optimizer.stepScalings = scalings;
	double[] bestResult = new double[CoordinateSystem3D.ARRAY_DIM];
	double bestValue = 999;
	for (int i = 0; i < STEM_FIND_ITER_MAX; ++i) {
	    String usedRefName1 = REF_ATOM_NAME;
	    String usedRefName2 = REF_ATOM_NAME;
	    if (stem.getStartResidue(0).getIndexOfChild(usedRefName1) < 0) {
		usedRefName1 = REF_ATOM_NAME_ALT; // AMBER alternative
	    }
	    if (stem.getStartResidue(len-1).getIndexOfChild(usedRefName2) < 0) {
		usedRefName2 = REF_ATOM_NAME_ALT; // AMBER alternative
	    }
	    if (stem.getStartResidue(0).getIndexOfChild(usedRefName1) < 0) {
		throw new FittingException("Could not find atom with name: " + REF_ATOM_NAME + " or "
					   + REF_ATOM_NAME_ALT + " in residue " + stem.getStartResidue(0).getName());
	    }
	    if (stem.getStartResidue(len-1).getIndexOfChild(usedRefName2) < 0) {
		throw new FittingException("Could not find atom with name: " + REF_ATOM_NAME + " or " + REF_ATOM_NAME_ALT 
					   + " in residue " + stem.getStartResidue(len-1).getName());
	    }
	    Vector3D fiveStartPosition = stem.getStartResidue(0).getChild(usedRefName1).getPosition().plus(SuperpositionTools.generateRandomVector(5.0));
	    Vector3D fiveEndPosition = stem.getStartResidue(len-1).getChild(usedRefName2).getPosition().plus(SuperpositionTools.generateRandomVector(5.0));
	    Vector3D z = fiveEndPosition.minus(fiveStartPosition);
	    assert (z.lengthSquare() > 0.0);
	    z.normalize();
	    Vector3D x = Vector3DTools.generateRandomOrthogonalDirection(z);
	    x.normalize();
	    Vector3D y = z.cross(x);
	    y.normalize();
	    double[] v = new double[CoordinateSystem3D.ARRAY_DIM]; // 9-dimensional start vector consisting of base and direction and co-direction
	    fiveStartPosition.setArray(v, 0);
	    x.setArray(v, 3);
	    y.setArray(v, 6);
	    OptimizationNDResult optimizationResult = optimizer.optimize(stemBasePotential, v);
	    double[] result = optimizationResult.getBestPosition();
	    if ((i == 0) || (optimizationResult.getBestValue() < bestValue)) {
		bestValue = optimizationResult.getBestValue();
		for (int j = 0; j < bestResult.length; ++j) {
		    bestResult[j] = result[j];
		}
		if (bestValue < rmsTolerance) {
		    break; // no need for further optimization
		}
	    }
	    if(i==STEM_FIND_ITER_MAX)
		log.info("Max Reached.");
	}
	assert bestResult.length == STEM_ORIENTATION_ARRAY_LEN;
	// log.fine("Best Stem fit RMS: " + bestValue);
	if (bestValue > rmsTolerance) {
	    log.fine("Bad stem fit encountered!");
	    throw new FittingException("Could not find stem position!");
	}
	return bestResult;
    }

    /** uses Kabsch algorithm to identify best fit of stem base w.r.t. to all stem coordinates */
    private static SuperpositionResult findStemBaseAndDirection(RnaStem3D stem, double rmsTolerance) throws FittingException {
	// log.fine("Starting findStemBaseAndDirection: " + stem.getName());
	// log.fine(StemTools.stemResidueLongInfo(stem));
	// helix length:
	log.fine("Starting findStemBaseAndDirection");
	HelixParameters helixParameters = new HelixParameters(); // default helix parameters cannot be changed
	int fitLen = stem.getLength();
	if (stem.getLength() < fitLen) {
	    throw new FittingException("Cannot fit helix parameters: helix too short");
	}
	// create coordinates of idealized helix:
	Vector3D[] idealCoordinates = new Vector3D[2*fitLen];
	CoordinateSystem3D cs0 = new CoordinateSystem3D(new Vector3D());
	log.fine("Unit coordinate system: " + cs0.toString());
	for (int i = 0; i < fitLen; ++i) {
	    LineShape line = Rna3DTools.computeHelix(i, cs0, helixParameters);
	    Vector3D p1 = line.getPosition1();
	    Vector3D p2 = line.getPosition2();
	    idealCoordinates[2*i] = p1;
	    idealCoordinates[2*i+1] = p2;
	}
	Vector3D[] realCoordinates = new Vector3D[2*fitLen];
	int step = 1; // (stemInfo.size() - 1)/2; // only check first, middle and last base pair!
	if (step < 1) {
	    step = 1;
	}
	for (int i = 0; i < stem.getLength(); i+= step) {
	    String usedRefName1 = REF_ATOM_NAME;
	    String usedRefName2 = REF_ATOM_NAME;
	    if (stem.getStartResidue(i).getIndexOfChild(usedRefName1) < 0) {
		usedRefName1 = REF_ATOM_NAME_ALT; // AMBER alternative
	    }
	    if (stem.getStopResidue(i).getIndexOfChild(usedRefName2) < 0) {
		usedRefName2 = REF_ATOM_NAME_ALT; // AMBER alternative
	    }
	    if (stem.getStartResidue(i).getIndexOfChild(usedRefName1) < 0) {
		throw new FittingException("Could not find atom with name: " + REF_ATOM_NAME + " or "
					   + REF_ATOM_NAME_ALT + " in residue " + stem.getStartResidue(i).getName());
	    }
	    if (stem.getStopResidue(i).getIndexOfChild(usedRefName2) < 0) {
		throw new FittingException("Could not find atom with name: " + REF_ATOM_NAME + " or "
					   + REF_ATOM_NAME_ALT + " in residue " + stem.getStopResidue(i).getName());
	    }
	    Vector3D p1Orig = stem.getStartResidue(i).getChild(usedRefName1).getPosition();
	    Vector3D p2Orig = stem.getStopResidue(i).getChild(usedRefName2).getPosition();
	    realCoordinates[2*i] = p1Orig;
	    realCoordinates[2*i+1] = p2Orig;
	}
	log.fine("Starting superimpose algorithm");
	KabschSuperpose superposer = new KabschSuperpose();
	SuperpositionResult result = superposer.superpose(realCoordinates, idealCoordinates);
	log.fine("Finished superimpose algorithm");
	if (result.getRms() > rmsTolerance) {
	    log.fine("Bad stem fit encountered!");
	    throw new FittingException("Could not find stem position!");
	}
	log.fine("Finished findStemBaseAndDirection");
	return result;
    }

    /** uses local optimization to identify best fit of stem base wrt to all stem coordinates */
    private static CoordinateSystem findStemCoordinateSystemOld(RnaStem3D stem, double rmsTolerance) throws FittingException {
	double[] csArray = findStemBaseAndDirectionOld(stem, rmsTolerance);
	assert (csArray.length == CoordinateSystem3D.ARRAY_DIM);
	return new CoordinateSystem3D(csArray);
    }

    /** New (Kabsch) algorithm for determining stem coordinate system based on base pair fitting */
    private static CoordinateSystem findStemCoordinateSystem(RnaStem3D stem, double rmsTolerance) throws FittingException {
	log.fine("Starting findStemCoordinateSystem");
	SuperpositionResult result = findStemBaseAndDirection(stem, rmsTolerance);
	CoordinateSystem3D cs = new CoordinateSystem3D(new Vector3D());
	log.fine("Created coordinate system: " + cs.toString());;
	log.fine("TEMP UNIT COORDINATE SYSTEM: " + cs.toString());
	result.applyTransformation(cs);
	log.fine("Finished findStemCoordinateSystem");
	return cs;
    }

    /** uses local optimization to identify best fit of stem base wrt to all stem coordinates */
    /* private static double[] findStemBaseAndDirectionSimple(RnaStem3D stem) {
	Stem stemInfo = stem.getStemInfo();
	int len = stemInfo.size();
	double[] v = new double[6];
	if ((stem.getStartResidue(0).getIndexOfChild(REF_ATOM_NAME) < 0)
	    || (stem.getStopResidue(0).getIndexOfChild(REF_ATOM_NAME) < 0)
	    || (stem.getStartResidue(len-1).getIndexOfChild(REF_ATOM_NAME) < 0)
	    || (stem.getStopResidue(len-1).getIndexOfChild(REF_ATOM_NAME) < 0)) {
	    log.severe("Warning: cannot compute findStemBaseAndDirection because of missing atom data: " + REF_ATOM_NAME);
	    return null; // not sufficiently defined!
	}
	Vector3D fiveStartPosition = (stem.getStartResidue(0).getChild(REF_ATOM_NAME).getPosition().plus(stem.getStopResidue(0).getChild(REF_ATOM_NAME).getPosition())).mul(0.5);	    
	Vector3D fiveEndPosition =   (stem.getStartResidue(len-1).getChild(REF_ATOM_NAME).getPosition().plus(stem.getStopResidue(len-1).getChild(REF_ATOM_NAME).getPosition())).mul(0.5);	    
	v[0] = fiveStartPosition.getX();
	v[1] = fiveStartPosition.getY();
	v[2] = fiveStartPosition.getZ();
	v[3] = fiveEndPosition.getX(); // startDirection.getX();
	v[4] = fiveEndPosition.getY(); // startDirection.getY();
	v[5] = fiveEndPosition.getZ(); // startDirection.getZ();
	return v;
    }
    */

    /** computes base of other end of stem given base of first end and direction vector */
    private static Vector3D findStemBaseBackwards(RnaStem3D stem, Vector3D base, Vector3D directionOrig, int offset) {
	assert (offset < stem.getLength());
	assert base != null;
	assert directionOrig != null;
	assert directionOrig.lengthSquare() > 0.0;
	Vector3D dir = new Vector3D(directionOrig);
	double newLength = HELIX_RISE * (stem.getLength()-offset);
	Vector3D newBase = base.plus(dir.mul(newLength));
	return newBase;
    }

    private static String generateBranchDescriptorResidueName(Residue3D residue) {
	char symbol = residue.getSymbol().getCharacter();
	return "" + symbol;
    }

    public static String generateBranchDescriptorStrandName(BranchDescriptor3D bd) {
	NucleotideStrand inStrand = bd.getIncomingStrand();
	NucleotideStrand outStrand = bd.getOutgoingStrand();
	String inStrandName = inStrand.getProperty("pdb_chain_char");
	if (inStrandName == null) {
	    inStrandName = inStrand.getName();
	}
	String outStrandName = outStrand.getProperty("pdb_chain_char");
	if (outStrandName == null) {
	    outStrandName = outStrand.getName();
	}
	Residue3D inResidue = inStrand.getResidue3D(bd.getIncomingIndex() + bd.getOffset());
	Residue3D outResidue = outStrand.getResidue3D(bd.getOutgoingIndex() - bd.getOffset());
	char inChar = inResidue.getSymbol().getCharacter();
	char outChar = outResidue.getSymbol().getCharacter();
	String result = inStrandName + "_" + inChar + inResidue.getAssignedNumber()
	    + "_"
	    + outStrandName + "_" + outChar + outResidue.getAssignedNumber();
	return result;
    }

    /** generates new branch descriptor coordinate system corresponding to next n'th base pair */
    public static CoordinateSystem generatePropagatedBranchDescriptorCoordinateSystem(BranchDescriptor3D branchOrig, int n) {
	BranchDescriptor3D branch = (BranchDescriptor3D)(branchOrig.cloneDeep());
	HelixParameters prm = branchOrig.getHelixParameters();
	double rise = prm.rise;
	branch.translate(branch.getDirection().mul(n * rise));// add rise around direction:
	double angTarget = (2.0 * Math.PI) / prm.basePairsPerTurn;// angle per base pair:
	// rotate x and y vectors around this angle:
	branch.rotate(branch.getDirection(), angTarget); // rotate around local z-axis
	return branch.getCoordinateSystem();
    }

    /** defines up to two branch descriptors (one for each end of the helix.
     * Careful: the generated branchDescriptos are added as 2 children to the stem!
     * TODO : if one fitting exception is thrown for one end of stems, no branch-descriptor is generated at all. */
    private static Object3DSet generateBranchDescriptors(RnaStem3D stem, 
							 int branchDescriptorOffset,
							 double rmsTolerance) throws FittingException {
	// log.fine("Starting generateBranchDescriptors for " + stem.toString());
	NucleotideStrand strand1 = stem.getStrand1();
	NucleotideStrand strand2 = stem.getStrand2();
	int offs = branchDescriptorOffset; // how many base pairs are taken from the stem?
	int length = stem.getLength();
	// TODO : verify ! Also: add more stem content!
	Object3DSet objectSet = new SimpleObject3DSet();
	if ((stem.getStemInfo().getStopPos() >= offs) 
	    && (stem.getStemInfo().getStartPos()+offs < strand1.size()) ){
	    CoordinateSystem cs = findStemCoordinateSystem(stem, rmsTolerance); // TODO : problem: how do we know which of the two coordinate systems was found?
	    // move cs according to offset:
	    cs.translate(cs.getZ().mul(HELIX_RISE * offs));
	    // rotate coordinate system:
	    double ang = ((2.0 * Math.PI) / HELIX_BASE_PAIRS_PER_TURN) * offs;
	    cs.rotate(cs.getZ(), ang);
	    BranchDescriptor3D branch1 = new SimpleBranchDescriptor3D(strand2, strand1, 
								      stem.getStemInfo().getStopPos()-offs, 
								      stem.getStemInfo().getStartPos()+offs,
								      cs);	    
	    branch1.setOffset(branchDescriptorOffset);
	    assert (branch1.isValid());
	    objectSet.add(branch1);
	    stem.insertChild(branch1);
	    assert Object3DSetTools.distanceMin(branch1, stem) < BRANCH_PLAUSIBLE_DIST;
	    // assert Object3DSetTools.distanceMin(branch1, atomRoot) < BRANCH_PLAUSIBLE_DIST;
	}
	int off2 = length-offs-1;
	if ((stem.getStemInfo().getStartPos()+off2 < strand1.size())
	    && (stem.getStemInfo().getStopPos()-off2 >= 0)
	    &&(stem.getStemInfo().getStartPos()+off2 >= 0)
	    &&(stem.getStemInfo().getStopPos()-off2 < strand2.size())) {//modified BM... stops error messages but doesn't improve recognition rate.
	    CoordinateSystem cs2 = findStemCoordinateSystem(stem.generateReverseStem(), rmsTolerance); // TODO : problem: who do we know which of the two coordinate systems was found?
	    cs2.translate(cs2.getZ().mul(HELIX_RISE * offs));
	    // rotate coordinate system:
	    double ang = ((2.0 * Math.PI) / HELIX_BASE_PAIRS_PER_TURN) * offs;
	    cs2.rotate(cs2.getZ(), ang);
	    BranchDescriptor3D branch2 = new SimpleBranchDescriptor3D(
						      strand1, strand2, 
						      stem.getStemInfo().getStartPos()+off2, 
						      stem.getStemInfo().getStopPos()-off2,
						      cs2); // changed: taken out offset offs (formerly off2)
	    branch2.setOffset(branchDescriptorOffset);
	    assert (branch2.isValid());
	    objectSet.add(branch2);
	    stem.insertChild(branch2);
	    assert Object3DSetTools.distanceMin(branch2, stem) < BRANCH_PLAUSIBLE_DIST;
	}
	assert(objectSet.size() <= 2);
	// log.info("Finished generateBranchDescriptors for " + stem.toString());
    	return objectSet;
    }

    @Test(groups={"fast"})
    public void testGenerateBranchDescriptors() {
	// TODO : more test code here
    }
    
    @Test(groups={"slow", "new"})
    public void testGenerateBranchDescriptorsSlow() {
	log.info(TestTools.generateMethodHeader("testGenerateBranchDescriptorsSlow"));
	int offset = 2;
	double rmsTolerance = 3.0;
	Object3DFactory reader = new RnaPdbReader();
	RnaStem3D stem = null;
	try {
	    ResourceBundle rb = ResourceBundle.getBundle("JunctionScanner");
	    assert(rb!= null);
	    String fileNames = rb.getString("testFileNames");
	    assert(fileNames != null);
	    StringTokenizer tokens = new StringTokenizer(fileNames, " ,");
	    while (tokens.hasMoreTokens()) {
		String fileName = tokens.nextToken();
		if (fileName.length() < 2) {
		    continue;
		}
		log.info("Reading file: " + fileName);
		InputStream is = new FileInputStream(fileName);
		assert(is!=null);
		Object3DLinkSetBundle bundle = reader.readBundle(is);
		Object3D root = bundle.getObject3D();
		Object3D stems = StemTools.generateStems(root).getObject3D();	
		log.info("Detected " + stems.size() + " double-helices.");
		assert(stems.size() > 0);
		stem = (RnaStem3D)stems.getChild(0); // get first stem
		assert(stem.getLength() >= 3); // ignore too short stems
		/** defines up to two branch descriptors (one for each end of the helix.
		 * Careful: the generated branchDescriptos are added as 2 children to the stem! */
		Object3DSet descriptorSet = generateBranchDescriptors(stem, offset, rmsTolerance);
		assert(descriptorSet.size() == 2);
		BranchDescriptor3D branch1 = (BranchDescriptor3D)(descriptorSet.get(0));
		BranchDescriptor3D branch2 = (BranchDescriptor3D)(descriptorSet.get(1));
 		log.info("Stem length: " + stem.getLength() + " stem: " + stem.getStemInfo());
 		log.info("Branch descriptor 1: " + branch1.getName() + " " + branch1.getCoordinateSystem());
 		log.info("Branch descriptor 2: " + branch2.getName() + " " + branch2.getCoordinateSystem());
		double angle = RAD2DEG * branch1.getDirection().angle(branch2.getDirection());
		// log.fine("Angle between branch descriptors: " + angle);
		assert(angle > 120.0);
	    }
	}
	catch (IOException ioe) {
	    log.severe(ioe.getMessage());
	    assert(false);
	}
	catch (FittingException fe) {
	    String tmp = "";
	    if (stem != null) {
		tmp = tmp + stem.toString();
	    }
	    log.warning("Could not find 3D fit for stem " + tmp);
	    assert false;
	}
	log.info(TestTools.generateMethodFooter("testGenerateBranchDescriptorsSlow"));
    }

    /** defines up to two branch descriptors (one for each end of the helix */
    public static Object3DSet generateBranchDescriptors(Object3DSet stems, 
							 int branchDescriptorOffset,
							 double rmsTolerance) {
	// log.info("Starting generateBranchDescriptors (1)");
	// Vector3D testPos = Object3DTools.findFirstLeaf(atomRoot).getPosition();
	Object3DSet result = new SimpleObject3DSet();
	for (int i = 0; i < stems.size(); ++i) {
	    Object3D obj = stems.get(i);
	    if (obj instanceof RnaStem3D) {
		RnaStem3D stem = (RnaStem3D)obj;
		try {
		    Object3DSet tmpResult = generateBranchDescriptors(stem, branchDescriptorOffset, rmsTolerance);
		    result.merge(tmpResult);
		}
		catch (FittingException fe) {
		    log.fine("Ignoring stem because not able to find good 3D fit : " + stem.getStemInfo().toString());
		}
	    }
	}
	// make sure all generated branch descriptors are close to atom:
// 	for (int i = 0; i < result.size(); ++i) {
// 	    assert Object3DSetTools.distanceMin(result.get(i), atomRoot) < BRANCH_PLAUSIBLE_DIST;
// 	}
//	assert testPos.distance(Object3DTools.findFirstLeaf(atomRoot).getPosition()) < 0.001; // test for side effects
	// log.info("Finished generateBranchDescriptors (1)");
	return result;
    }

    /** defines up to two branch descriptors (one for each end of the helix, in this version they are pointing away from the helix. */
    public static Object3DSet generateInvertedBranchDescriptors(Object3DSet stems, 
								int branchDescriptorOffset,
								double rmsTolerance,
								int propagation) {
	// log.info("Starting generateBranchDescriptors (1)");
	// Vector3D testPos = Object3DTools.findFirstLeaf(atomRoot).getPosition();
	Object3DSet result = new SimpleObject3DSet();
	for (int i = 0; i < stems.size(); ++i) {
	    Object3D obj = stems.get(i);
	    if (obj instanceof RnaStem3D) {
		RnaStem3D stem = (RnaStem3D)obj;
		try {
		    Object3DSet tmpResult = generateBranchDescriptors(stem, branchDescriptorOffset, rmsTolerance);
		    if (tmpResult != null) {
			for (int j = 0; j < tmpResult.size(); ++j) {
			    invertBranchDescriptor((BranchDescriptor3D)(tmpResult.get(j)), propagation);
			}
		    }
		    result.merge(tmpResult);
		}
		catch (FittingException fe) {
		    log.info("Ignoring stem because not able to find good 3D fit : " + stem.getStemInfo().toString());
		}
	    }
	}
	// make sure all generated branch descriptors are close to atom:
// 	for (int i = 0; i < result.size(); ++i) {
// 	    assert Object3DSetTools.distanceMin(result.get(i), atomRoot) < BRANCH_PLAUSIBLE_DIST;
// 	}
//	assert testPos.distance(Object3DTools.findFirstLeaf(atomRoot).getPosition()) < 0.001; // test for side effects
	// log.info("Finished generateBranchDescriptors (1)");
	return result;
    }

    /** generates clusters of linked entities given a connection matrix */
    private static int[] generateLinkedClusterVector(int[][] mtx) {
	int[] clusterIds = new int[mtx.length];
	for (int i = 0; i < clusterIds.length; ++i) {
	    clusterIds[i] = -1;
	}
	int clusterCount = 0;
	for (int i = 0; i < mtx.length; ++i) {
	    if (clusterIds[i] < 0) {
		// move all contacts to found cluster
		boolean found = false;
		for (int j = 0; j < mtx.length; ++j) {
		    if ((mtx[i][j] > 0) && (clusterIds[j] >= 0)) {
			found = true;
			clusterIds[i] = clusterIds[j];
			break;
		    }
		}
		if (!found) { // start new cluster
		    clusterIds[i] = clusterCount++;
		} 
	    }
	    // move all contacts to found cluster
	    for (int j = 0; j < mtx.length; ++j) {
		if ((i == j) || (clusterIds[j] >= 0)) {
		    continue; // already classified
		}
		if (mtx[i][j] > 0) {
		    clusterIds[j] = clusterIds[i]; // put to same cluster
		}
	    }
	}
	return clusterIds;
    }

    /** generates clusters of linked entities given a connection matrix 
     * assumes assymetric maxtrix[i][j] . 
     * If j>i and matrix[i][j] = 1: incoming strand of branchdescriptor i is
     * outgoing strand of branchdescriptor j
     * If j<i and matrix[i][j] = 1: outgoing strand of branchdescriptor i is
     * incoming strand of branchdescriptor j
     */
    private static int[] generateLinkedClusterVector2(int[][] mtx) {
	int[] clusterIds = new int[mtx.length];
	for (int i = 0; i < clusterIds.length; ++i) {
	    clusterIds[i] = -1;
	}
	int clusterCount = 0;
	for (int i = 0; i < mtx.length; ++i) {
	    if (clusterIds[i] < 0) {
		// move all contacts to found cluster
		boolean found = false;
		for (int j = 0; j < mtx.length; ++j) {
		    if ((mtx[i][j] > 0) && (clusterIds[j] >= 0)) {
			found = true;
			clusterIds[i] = clusterIds[j];
			break;
		    }
		}
		if (!found) { // start new cluster
		    clusterIds[i] = clusterCount++;
		} 
	    }
	    // move all contacts to found cluster
	    for (int j = 0; j < mtx.length; ++j) {
		if ((i == j) || (clusterIds[j] >= 0)) {
		    continue; // already classified
		}
		if (mtx[i][j] > 0) {
		    clusterIds[j] = clusterIds[i]; // put to same cluster
		}
	    }
	}
	return clusterIds;
    }

    private static int findHighestIndex(int[] v) {
	int bestId = 0;
	int bestVal = v[bestId];
	for (int i = 1; i < v.length; ++i) {
	    if (v[i] > bestVal) {
		bestVal = v[i];
		bestId = i;
	    }
	}
	return bestId;
    }

    /** generates clusters of linked entities given a 1d vector describing to which cluster each entity belongs */
    private static IntegerListList translateClusterVectorToLists(int[] clusterVector) {
	// find out number of different clusters: (assume continuous counting from 0 to (n-1) for n clusters:
	int numClusters = clusterVector[findHighestIndex(clusterVector)] + 1;
	IntegerListList clusters = new IntegerListList();
	for (int i = 0; i < numClusters; ++i) {
	    clusters.add(new IntegerList());
	}
	for (int i = 0; i < clusterVector.length; ++i) {
	    IntegerList list = clusters.get(clusterVector[i]);
	    list.add(i);
	}
	return clusters;
    }

    /** generates clusters of linked entities given a connection matrix */
    private static IntegerListList generateLinkedClusters(int[][] mtx) {
	int[] clusterVector = generateLinkedClusterVector(mtx);
	return translateClusterVectorToLists(clusterVector);
    }

    /** New version! working with asymetric matrix
     * TODO still not implemented
     * generates clusters of linked entities given a connection matrix */
    private static IntegerListList generateLinkedClusters2(int[][] mtx) {
	int[] clusterVector = generateLinkedClusterVector2(mtx);
	return translateClusterVectorToLists(clusterVector);
    }

    /** returns matrix describing which pairs of BranchDescriptors are compatible */
    private static int[][] generateCompatibilityMatrix(Object3DSet branchDescriptors) {
	assert (branchDescriptors != null);
	assert (branchDescriptors.size() > 0); 
	int nb = branchDescriptors.size();
	int[][] compMatrix = new int[nb][nb];
	for (int i = 0; i < nb; ++i) {
	    compMatrix[i][i] = 0;
	    // log.finest("Checking branch descriptor compatibility: "  + (i+1));
	    for (int j = i+1; j < nb; ++j) {
		if (isCompatible((BranchDescriptor3D)(branchDescriptors.get(i)),
				 (BranchDescriptor3D)(branchDescriptors.get(j)))) {
		    compMatrix[i][j] = 1;
		}
		else {
		    compMatrix[i][j] = 0;
		}
		compMatrix[j][i] = compMatrix[i][j];
	    }
	}
	assert compMatrix.length > 0;
	return compMatrix;
    }

    /** generates random path of branch descriptors. CompMatrix is a matrix telling which BranchDescriptors are compatible. */
    private static IntegerList generateRandomPath(int[][] mtx, int[][] compMatrix,
						  Object3DSet branchDescriptors,
						  Random rand)
    {
	assert mtx.length > 0;
	assert compMatrix.length > 0;
	assert branchDescriptors.size() > 0;
	int nn = compMatrix.length;
	IntegerList result = new IntegerList();
	IntegerListList linked = new IntegerListList();
	for (int i = 0; i < mtx.length; ++i) {
	    IntegerList lineList = new IntegerList();
	    for (int j = 0; j < mtx[i].length; ++j) {
		if (mtx[i][j] == 1) {
		    lineList.add(j);
		}
	    }
	    linked.add(lineList);
	}
	int curr = rand.nextInt(nn); // choose random starting point
	result.add(curr);
	do {
	    IntegerList lineList = linked.get(curr); // get all connections of current row
	    if (lineList.size() > 0) {
		int m = rand.nextInt(lineList.size()); // choose next successor
		int succ = lineList.get(m); // id of successor
		// check if successor is already in path and is compatible:
		for (int j = 0; j < result.size(); ++j) {
		    int otherId = result.get(j);
		    if (otherId == succ) {
			// circle detected. Return result
			return result;
		    }
		    if (compMatrix[otherId][succ] == 0) {
			return result; // branch descriptors not compatible. return.
		    }
		}
		result.add(succ);
		curr = succ;
	    }
	    else {
		// dead end. Return.
		return result;
	    }
	}
	while (result.size() < nn);
	return result;
    }

    /** find next branchdescriptor for which the incoming strand of the query branch descriptor is the outgoing branch descriptor */
    private static int findSubsequentBranchDescriptor(int id,  Object3DSet branchDescriptors, int[][] connMtx, int[][] compMatrix) {
	// log.info("Starting findSubsequenceBranchDescrtiptor " + id + " in list: " + (new IntegerList(connMtx[id])));
	int bestDiff = 9999;
	int bestId = -1;
	for (int j = 0; j < connMtx.length; ++j) {
	    if (j == id) {
		continue; // ignore hits with itself
	    }
	    if (isConnectedIncomingOutgoing(branchDescriptors, id, j) && (compMatrix[id][j] > 0)) {
		int diff = ((BranchDescriptor3D)(branchDescriptors.get(j))).getOutgoingIndex()
		    -  ((BranchDescriptor3D)(branchDescriptors.get(id))).getIncomingIndex();
		if ((diff > 0) && (diff < bestDiff)) {
		    bestDiff = diff;
		    bestId = j;
		}
	    }
	}
	// log.info("result of findSubsequenceBranchDescriptor: "+ bestId);
	return bestId;
    }

    /** generates circular path of branch descriptors. CompMatrix is a matrix telling which BranchDescriptors are compatible. */
    private static IntegerListList generateShortestPaths(int[][] mtx, 
							 Object3DSet branchDescriptors)
    {
	// log.info("Starting generateShortestPaths");
	assert mtx.length > 0;
	assert branchDescriptors.size() > 0;
	int nn = branchDescriptors.size();
	IntegerListList finalResult = new IntegerListList();
// 	IntegerListList linked = new IntegerListList();
// 	for (int i = 0; i < mtx.length; ++i) {
// 	    IntegerList lineList = new IntegerList();
// 	    for (int j = 0; j < mtx[i].length; ++j) {
// 		if (mtx[i][j] == 1) {
// 		    lineList.add(j);
// 		}
// 	    }
// 	    linked.add(lineList);
// 	}
	int[][] compMatrix = generateCompatibilityMatrix(branchDescriptors);

//    	log.info("Connectivity matrix:");
//    	IntegerArrayTools.writeMatrix(System.out, mtx);
//   	log.info("Generating compatibility matrix...");
//   	log.info("Finished generating compatibility matrix: " + compMatrix.length);
//   	IntegerArrayTools.writeMatrix(System.out, compMatrix);

	for (int curr = 0; curr < nn; ++curr) { // loop over all starting branch descriptors
	    IntegerList result = new IntegerList();
	    result.add(curr);
	    // IntegerList lineList = linked.get(curr); // get all connections of current row
	    int succ = curr;
	    // log.info("Working on finding path starting with branch descriptor " + curr);
	    do {
		int succ2 = findSubsequentBranchDescriptor(succ, branchDescriptors, mtx, compMatrix);
		// log.fine("Subsequent branch descriptor of id " + succ + " : " + succ2);
		if ((succ2 == succ) || (succ2 < 0) || (compMatrix[succ2][succ] == 0)) {
		    break; // could not find good subsequent branch descriptor. Quit loop
		}
		if (succ2 == curr) { // circle !?
		    // log.fine("Circle detected with " + (succ2) + " and " + result);
		    // check if path really new:
		    result = result.rotateToSmallest(); // rotate 
		    // check if path already known:
		    boolean found = false;
		    for (int j = 0; j < finalResult.size(); ++j) {
			IntegerList otherPath = finalResult.get(j);
			if (otherPath.equals(result)) {
			    found = true; // equivalent path found!
			    break;
			}
		    }
		    if (!found) {
			// log.fine("Legal Circular detected with " + result);
			finalResult.add(result); // circle detected
		    }
		    break;
		}
		// check if successor is already in path and is compatible:
		boolean dupFound = false;
		for (int j = 1; j < result.size(); ++j) {
		    int otherId = result.get(j);
		    if (otherId == succ2) {
			// inside circle detected! save result
			// finalResult.add(result);
			// log.info("Incomplete circular path detected! Ignoring path.");
			dupFound = true;
			break;
		    }
		}
		if (dupFound) {
		    break;  // bad path !
		}
		result.add(succ2);
		succ = succ2;
	    }
	    while (result.size() < nn);
	}
	// log.info("finished generateShortestPaths");
	return finalResult;
    }

    /** New version! working with asymetric matrix
     * generates clusters of linked entities given a connection matrix */
    private static IntegerListList generateLinkedClusters3(int[][] mtx, Object3DSet branchDescriptors,
							   int iterMax) {
	assert mtx.length > 0;
	assert branchDescriptors.size() > 0;
	Random rnd = Randomizer.getInstance();
	int nb = branchDescriptors.size();
	int[][] compMatrix = generateCompatibilityMatrix(branchDescriptors);
	IntegerListList paths = new IntegerListList();
	for (int i = 0; i < iterMax; ++i) {
	    IntegerList path = generateRandomPath(mtx, compMatrix, branchDescriptors, rnd);
	    // check if path ok:
	    boolean found = false;
	    for (int j = 0; j < paths.size(); ++j) {
		IntegerList otherPath = paths.get(j);
		if (otherPath.containsAll(path)) {
		    found = true;
		    break;
		}
	    }
	    if (!found) {
		paths.add(path); // add new path because no duplicate found
	    }
	}
	return paths;
    }

    /** New version! working with asymetric matrix
     * generates clusters of linked entities given a connection matrix */
    private static IntegerListList generateLinkedClusters4(int[][] mtx, Object3DSet branchDescriptors,
							   int iterMax) {
	assert mtx.length > 0;
	assert branchDescriptors.size() > 0;
	Random rnd = Randomizer.getInstance();
	int nb = branchDescriptors.size();
	// TODO : too much debug info. Take out!
//  	log.info("Connectivity matrix:");
//  	IntegerArrayTools.writeMatrix(System.out, mtx);
// 	log.info("Generating compatibility matrix...");
 	int[][] compMatrix = generateCompatibilityMatrix(branchDescriptors);
// 	log.info("Finished generating compatibility matrix: " + compMatrix.length);
// 	IntegerArrayTools.writeMatrix(System.out, compMatrix);
	IntegerListList paths = new IntegerListList();
	IntegerListList rotatedPaths = new IntegerListList();
	for (int i = 0; i < iterMax; ++i) {
	    // log.finest("Generating random path: " + i + "...");
	    IntegerList path = generateRandomPath(mtx, compMatrix, branchDescriptors, rnd);
	    // log.finest("Random path: " + i + " : " + path.toString());
	    assert path.isUnique();
	    if (path.size() > 1) {
		assert isConnectedIncomingOutgoing(branchDescriptors, path.get(0), path.get(1));
	    }
	    IntegerList rotPath = new IntegerList(path);
	    rotPath = rotPath.rotateToSmallest(); // rotate 
	    // check if path already known:
	    boolean found = false;
	    for (int j = 0; j < rotatedPaths.size(); ++j) {
		IntegerList otherPath = paths.get(j);
		if (otherPath.equals(rotPath)) {
		    found = true;
		    break;
		}
	    }
	    if (!found) {
		paths.add(path); // add new path because no duplicate found
		rotatedPaths.add(rotPath);
	    }
	}
	return paths;
    }

    /** returns true if in allowed interval. Only works if position n is on same strand! */
    private static boolean isAllowedPosition(BranchDescriptor3D d, int n) {
	if (!d.isSingleSequence()) {
	    return true; // TODO : not sure about this case !!! :-(
	}
	if (d.getOutgoingIndex() > d.getIncomingIndex()) {
	    // unusual case: do not allow positions in loop that defines "outside"
	    // example PDB:1C2X positions 32-50
	    if ((n < d.getOutgoingIndex()) && (n > d.getIncomingIndex())) {
		return true;
	    }
	    else {
		return false;
	    }
	}
	// regular case: do not allow positions in loop that defines "outside"
	// example 1C2X : positions 48-34
	if ((n < d.getOutgoingIndex()) && (n > d.getIncomingIndex())) {
	    return false;
	}
	return true;
    }

    /** if branchdescriptors are compatible. Unsymetric test, use only symmetrized version
    */
    private static boolean isCompatibleHalf(BranchDescriptor3D d1,
					    BranchDescriptor3D d2) 
    {
	if (d1.isSingleSequence()) {
	    // find forbidden region:
	    int n1 = d2.getIncomingIndex();
	    if (d1.getIncomingStrand() == d2.getIncomingStrand()) {
		if (!isAllowedPosition(d1, n1)) {
		    return false;
		}
	    }
	    int n2 = d2.getOutgoingIndex();
	    if (d1.getIncomingStrand() == d2.getOutgoingStrand()) {
		if (!isAllowedPosition(d1, n2)) {
		    return false;
		}
	    }
	}
	return true;
    }

    /** if branchdescriptors are compatible return true. Symmetrized version
     */
    private static boolean isCompatible(BranchDescriptor3D d1,
					BranchDescriptor3D d2) 
    {
	return ((d1.distance(d2) <= BRANCH_PLAUSIBLE_DIST) && isCompatibleHalf(d1, d2) && isCompatibleHalf(d2, d1));
    }

    /** returns true if branch descriptors are DIRECTLY connected.
     * Checks if connection is between incoming strand of d1 and outgoing strand of d2 
    */
    private static boolean isConnectedIncomingOutgoing(Object3DSet branchDescriptors,
						       int n1,
						       int n2) {
	assert n1 != n2;
	BranchDescriptor3D d1 = (BranchDescriptor3D)(branchDescriptors.get(n1));
	BranchDescriptor3D d2 = (BranchDescriptor3D)(branchDescriptors.get(n2));
// 	log.finest("Starting isConnectIncomingOutgoing: "
// 		 + n1 + " " + n2 + " " + branchDescriptors.size() + " "
// 		 + d2.getOutgoingStrand().getName() + " "
// 		 + d2.getOutgoingIndex() + " " 
// 		 + d1.getIncomingStrand().getName() + " "
// 		 + d1.getIncomingIndex() + " " 
// 		 + d1.infoString() + " " + d2.infoString());
	// check if branchDescriptors have some parent (expect: RnaStem3D)
	RnaStem3D parent1 = (RnaStem3D)(d1.getParent());
	Stem stemInfo1 = parent1.getStemInfo();
	String fullStem1Name = Object3DTools.getFullName(parent1);
	RnaStem3D parent2 = (RnaStem3D)(d2.getParent());
	String fullStem2Name = Object3DTools.getFullName(parent2);
	// check if parents are identical: (same name):
	if ((parent1 != null) && (parent1 == parent2)) { // fullStem1Name.equals(fullStem2Name)) {
	    log.fine("Branch Descriptors belong to same stem! Quitting!");
	    return false; // branch descriptors belong to same stem! They are in this case defined as not connected.
	}
	NucleotideStrand incoming = d1.getIncomingStrand();
	NucleotideStrand outgoing = d2.getOutgoingStrand();
	if (incoming != outgoing) {
	    log.fine("BranchDescriptors are not connected because their strands are different!");
	    return false;
	}
	int resDiff = d2.getOutgoingIndex() - d1.getIncomingIndex();
	if ((resDiff <= 0) || (resDiff > residueRange)) {
	    log.fine("Branch descriptors are not connected because of wrong residue difference: " + resDiff);
	    return false;
	}
  	if (stemInfo1.isSingleSequence() && (d1.getOutgoingIndex() < d2.getOutgoingIndex())
 	    && (d1.getOutgoingIndex() > d1.getIncomingIndex()) ) {
 	    log.fine("Branch Descriptors are connect because of special case!");
  	    return false; // special case: detected circular loop for stem1
  	}
	log.fine("BranchDescriptors are connected! " + resDiff);		
	return true;
    }

    /** clusters BranchDescriptors such that each BranchDescriptor cluster corresponds to one Junction. */
    private static IntegerListList clusterBranchDescriptors(Object3DSet branchDescriptors, int iterMax) {
	assert branchDescriptors.size() > 0;
	int n = branchDescriptors.size();
	int[][] mtx = new int[n][n];
	for (int i = 0; i < mtx.length; ++i) {
	    // log.finest("Checking branch descriptor: " + (i+1));
	    for (int j = i+1; j < mtx.length; ++j) {
		if (isConnectedIncomingOutgoing(branchDescriptors, i, j)) {
		    mtx[i][j] = 1;
		}
		else {
		    mtx[i][j] = 0;
		}
		if (isConnectedIncomingOutgoing(branchDescriptors, j, i)) {
		    mtx[j][i] = 1;
		}
		else {
		    mtx[j][i] = 0;
		}
	    }
	}
	// log.info("Generated branch link matrix...");
	//  IntegerListList result = generateLinkedClusters4(mtx, branchDescriptors, iterMax);
	IntegerListList result = generateShortestPaths(mtx, branchDescriptors);
	assert ((result.size() == 0) || (result.get(0).size() < 2) 
		|| isConnectedIncomingOutgoing(branchDescriptors, result.get(0).get(0), result.get(0).get(1)));
	// log.fine("Generated clusters: " + result);
	// check overall size:
// 	int sum = 0; 
// 	for (int i = 0; i < result.size(); ++i) {
// 	    sum += result.size();
// 	}
// 	assert sum == branchDescriptors.size();
	return result;
    }

    /** adjusts two branch descriptors that are connected by one strand (incoming first, outgoing second).
     * The methods generats a cloned strand that is chopped, also it adjust the indicies of the branch descriptors.
     * TODO : difficult method, check correctness
     */
    public static void adjustBranchDescriptorPair(BranchDescriptor3D bIn, BranchDescriptor3D bOut) {
	// log.finest("Starting adjustBranchDescriptorPair!");
	assert !(bIn.isProbablyEqual(bOut));
	assert bIn.getIncomingStrand().sequenceString().equals(bOut.getOutgoingStrand().sequenceString()); 
	assert bIn.isValid();
	assert bOut.isValid();
	NucleotideStrand strandClone = (NucleotideStrand)(bIn.getIncomingStrand().cloneDeep());
	bIn.setIncomingStrand(strandClone);
	bOut.setOutgoingStrand(strandClone);
	assert bIn.getIncomingStrand() != bIn.getOutgoingStrand();
	assert bOut.getIncomingStrand() != bOut.getOutgoingStrand();
	Residue3D incRes = bIn.getIncomingStrand().getResidue3D(bIn.getIncomingIndex());
	Residue3D outRes = bIn.getOutgoingStrand().getResidue3D(bIn.getOutgoingIndex());
	int startId = bIn.getIncomingIndex();
	int stopId = bOut.getOutgoingIndex();
	int desiredLen = stopId - startId + 1;
	int desiredRemoved = strandClone.size() - desiredLen;
	String newName = strandClone.getName() + "_" + (startId+1) + "-" + (stopId+1);
	strandClone.setName(newName);
	int removeCounterEnd = 0;
	int removeCounterStart = 0;
	int newStart = bIn.getIncomingIndex() - removeCounterStart;
	int newStop = bOut.getOutgoingIndex() - removeCounterStart;
	if (startId < stopId) {
	    for (int ii = strandClone.size()-1; ii >= (stopId + 1); --ii) {
		assert strandClone.size() > 0;
		assert (strandClone.size() - 1) > stopId;
 		strandClone.removeChild(strandClone.size()-1); // TODO : check if sequence gets adjusted properly!
 		++removeCounterEnd;
		bOut.setOutgoingStrand(strandClone);
	    } 
 	    for (int ii = startId-1; ii >= 0; --ii) {
 		assert ii < strandClone.size();
 		strandClone.removeChild(0);
		bIn.setIncomingStrand(strandClone);
		bIn.setIncomingIndex(bIn.getIncomingIndex() - 1);
		bOut.setOutgoingIndex(bOut.getOutgoingIndex() - 1);
		bOut.setOutgoingStrand(strandClone);
 		++removeCounterStart;
 	    }
	}
	else if (startId > stopId) {
	    // log.fine("Strange status of branch descriptors detected..." + bIn.getName() + " " + bOut.getName());
	    // DO NOTHING !
	    // TODO check !!!
	    return;
	}
	newStart = bIn.getIncomingIndex();
	newStop = bOut.getOutgoingIndex();
	bIn.setIncomingIndex(newStart);
	bOut.setOutgoingIndex(newStop);
	bIn.setIncomingStrand(strandClone);
	bOut.setOutgoingStrand(strandClone);
	Residue3D incRes2 = bIn.getIncomingStrand().getResidue3D(bIn.getIncomingIndex());
	assert incRes.isProbablyEqual(incRes2);
	int newLen = (newStop - newStart + 1);
	assert bIn.isValid();
	assert bOut.isValid();
	assert bIn.getIncomingIndex() == 0;
    }

    /** adjusts a single branch descriptor that contains one looping strand.
     * The methods generates a cloned strand that is chopped, also it adjust the indicies of the branch descriptor.
     */
    public static void adjustBranchDescriptorLoop(BranchDescriptor3D b) {
	assert b.isSingleSequence();
	NucleotideStrand strandClone = (NucleotideStrand)(b.getIncomingStrand().cloneDeep());
	int startId = b.getIncomingIndex();
	int stopId = b.getOutgoingIndex();
	String newName = strandClone.getName() + "_" + (startId+1) + "-" + (stopId+1);
	strandClone.setName(newName);
	// log.fine("shortening strand: " + strandClone.size() + " " + startId + " " + stopId);
	int removeCounter = 0;
	if (startId < stopId) {
	    for (int ii = strandClone.size()-1; ii >= stopId + 1; --ii) {
		strandClone.removeChild(ii); // TODO : check if sequence gets adjusted properly!
	    } 
	    for (int ii = startId-1; ii >= 0; --ii) {
		assert ii < strandClone.size();
		strandClone.removeChild(ii);
		++removeCounter;
	    }
	}
	else if (startId > stopId) {
	    // DO NOTHING !
	    // TODO check !!!
	}
	int newStart = b.getIncomingIndex() - removeCounter;
	assert(newStart >= 0);
	int newStop = b.getOutgoingIndex() - removeCounter;
	b.setIncomingIndex(newStart);
	b.setOutgoingIndex(newStop);
	b.setIncomingStrand(strandClone);
	b.setOutgoingStrand(strandClone);
    }

    /** finds branch descriptor such that branch descriptor n shares incoming strand with 
     * other branchdescriptor (outgoing strand)
     */
    private static int findConnectedBranchDescriptor(Object3DSet branchCluster, int n) {
	BranchDescriptor3D b = (BranchDescriptor3D)(branchCluster.get(n));
	for (int i = 0; i < branchCluster.size(); ++i) {
	    if (i == n) {
		continue;
	    }
	    if (isConnectedIncomingOutgoing(branchCluster, n, i)) {
		return i;
	    }
	}
	return branchCluster.size();
    }

    /** fast and approximate check if two residues are probably the same entity, even if they were cloned
     * and their respective sequences have been chopped. */
    private static boolean isProbablyEqual(Residue3D res1,
				     Residue3D res2) {
	if (res1.size() != res2.size()) {
	    return false;
	}
	if (!(res1.getName().equals(res2.getName()))) {
	    return false;
	}
	if (res1.distance(res2) > 0.001) {
	    return false;
	}
	return true;
    }

    /** returns true if junction has this internal branch descrtpor and strand */
    private static boolean isPartOfStrand(NucleotideStrand strand, 
				Residue3D residue,
				int startIndex, 
				int stopIndex) {
	int pos = residue.getPos();
	return residue.isSameSequence(strand.getResidue(0))
	    && ((pos >= startIndex) && (pos <= stopIndex));
    }

    /** returns true if junction has this internal helices */
    private static boolean isInternalHelix(NucleotideStrand strand,
				 BranchDescriptor3D branchDescriptor,
				 int startIndex, int stopIndex) {
	int offset = branchDescriptor.getOffset();
	Residue3D inResidue = branchDescriptor.getIncomingStrand().getResidue3D(branchDescriptor.getIncomingIndex() + offset + 1);
	Residue3D outResidue = branchDescriptor.getOutgoingStrand().getResidue3D(branchDescriptor.getOutgoingIndex() - offset - 1);
	return isPartOfStrand(strand, inResidue, startIndex, stopIndex)
	    || isPartOfStrand(strand, outResidue, startIndex, stopIndex);
    }

    /** returns true if junction has this internal helices */
    private static boolean isInternalHelix(StrandJunction3D junction, BranchDescriptor3D branchDescriptor) {
	int strandCount = junction.getStrandCount();
	int offset = branchDescriptor.getOffset();
	for (int i = 0; strandCount < junction.getStrandCount(); ++i) {
	    NucleotideStrand strand = junction.getStrand(i);
	    int startIndex = offset + 1;
	    int stopIndex = strand.getResidueCount() - offset - 2;
	    assert stopIndex >= 0;
	    if ((stopIndex >= startIndex) && (junction.getLoopLength(i) >= 3)) {
		if (isInternalHelix(strand, branchDescriptor, startIndex, stopIndex)) {
		    return true;
		}
	    }
	}
	return false;
    }

    /** returns true if junction has internal helices */
    private static boolean junctionHasInternalHelices(StrandJunction3D junction, Object3DSet branchDescriptors) {
	assert false; // version not working, because junction uses already cloned strands
	for (int i = 0; i < branchDescriptors.size(); ++i) {
	    if (isInternalHelix(junction, (BranchDescriptor3D)(branchDescriptors.get(i)))) {
		return true;
	    }
	}
	return false;
    }
    
    /** only works if strands are defined for junction and not beyond! */
    private static boolean isInternalStem(Stem stem, int offset) {
	if ((stem.getStartPos() >= offset +1) && (stem.getStopPos() <= stem.getSequence1().size() - offset -2)) {
	    return true;
	}
	if ((stem.getStopPos() >= offset +1) && (stem.getStopPos() <= stem.getSequence2().size() - offset -2)) {
	    return true;
	}
	return false;
    }

    /** returns true if junction has internal helices */
    private static boolean junctionHasInternalHelices(StrandJunction3D junction) {
	int offset = junction.getBranch(0).getOffset(); // TODO : current restriction: assume all BranchDescriptors have same offset
	Object3DSet allStems = new SimpleObject3DSet();
	for (int i = 0; i < junction.getStrandCount(); ++i) {
	    NucleotideStrand strand1 = junction.getStrand(i);
	    for (int j = i+1; j < junction.getStrandCount(); ++j) {
		NucleotideStrand strand2 = junction.getStrand(j);
		Object3DSet stems = StemTools.generateStems(strand1, strand2, "s_" + (i+1) + "_" + (j+1));
		allStems.merge(stems);
	    }
	}
	for (int i = 0; i < allStems.size(); ++i) {
	    RnaStem3D stem = (RnaStem3D)allStems.get(i);
	    if (isInternalStem(stem.getStemInfo(), offset)) {
		return true;
	    }
	}
	return false;
    }

    /** generates a set of StrandJunction objects given a set of branchdescriptors.
     * 	TODO : first and last branch descriptor are only partially adjusted
     */
    private static Object3DSet generateJunctions(Object3DSet branchDescriptors,
						CorridorDescriptor corridorDescriptor,
						int iterMax, int loopLengthSumMax) {
	log.fine("Starting generateJunctions (4) with corridorDescriptor " + corridorDescriptor);
	StrandJunctionNomenclature lilleyNomenclature = new LilleyNomenclature();
	StrandJunctionNomenclature incomingNomenclature = new IncomingNomenclature();
	if (branchDescriptors.size() == 0) {
	    log.fine("No branch descriptors defined, returning empty set of junctions!");
	    return new SimpleObject3DSet(); // return empty set!
	}
	// Vector3D testPos = Object3DTools.findFirstLeaf(atomRoot).getPosition();
	for (int i = 0; i < branchDescriptors.size(); ++i) {
	    BranchDescriptor3D bd = (BranchDescriptor3D)(branchDescriptors.get(i));
	    // 	    log.finest("Info about branch descriptor " + (i+1) + " " + bd.getName() + " "  
	    // + (bd.getIncomingIndex() + 1) + " " + (bd.getOutgoingIndex() + 1));
// 	    double dist = Object3DSetTools.distanceMin(branchDescriptors.get(i), atomRoot);
// 	    if ( dist >= BRANCH_PLAUSIBLE_DIST) {
// 		log.warning("" + dist + " : BranchDescriptor3D " + (i+1) + " is too far: " + branchDescriptors.get(i));
// 		assert false;
// 	    }
	}
	log.fine("Starting to cluster branch descriptors...");
	IntegerListList clusters = clusterBranchDescriptors(branchDescriptors, iterMax); // clusters can be overlapping
	log.fine("Finished clustering branch descriptors: " + clusters.size());
	// 	assert testPos.distance(Object3DTools.findFirstLeaf(atomRoot).getPosition()) < 0.001; // test for side effects
	// just for testing:
	for (int i = 0; i < branchDescriptors.size(); ++i) {
// 	    double dist = Object3DSetTools.distanceMin(branchDescriptors.get(i), atomRoot);
// 	    if ( dist >= BRANCH_PLAUSIBLE_DIST) {
// 		log.warning("" + dist + " : BranchDescriptor3D " + (i+1) + " is too far: " + branchDescriptors.get(i));
// 		assert false;
// 	    }
	}
	// log.info("Clustered branch descriptors: " + clusters);
	Object3DSet result = new SimpleObject3DSet();
	Set<String> resultJunctionNames = new HashSet<String>();
	for (int i = 0; i < clusters.size(); ++i) {
	    boolean skipJunctionFlag = false;
	    Object3DSet branchCluster = new SimpleObject3DSet();
	    Object3DSet branchClonedCluster = new SimpleObject3DSet();
	    IntegerList cluster = clusters.get(i);
	    // log.fine("generateJunctions: working on cluster " + (i+1) + " with size " + cluster.size());
	    assert cluster.isUnique();
	    // assert testPos.distance(Object3DTools.findFirstLeaf(atomRoot).getPosition()) < 0.001; // test for side effects
	    if (cluster.size() > 1) {
		assert isConnectedIncomingOutgoing(branchDescriptors, cluster.get(0), cluster.get(1));	    
	    }
	    // just checking if atoms are nearby 
	    if (debugLevel > 1) {
		for (int k = 0; k < cluster.size(); ++k) {
// 		    double dist = Object3DSetTools.distanceMin(branchDescriptors.get(cluster.get(k)), atomRoot);
// 		    if ( dist >= BRANCH_PLAUSIBLE_DIST) {
// 			// log.fine("" + dist + " : BranchDescriptor3D " + (i+1) + " is too far: " + branchDescriptors.get(i));
// 			assert false;
// 		    }
		}
	    }
// 	    for (int j = 0; j < cluster.size(); ++j) {
// 		log.fine("Cluster member: " + (i+1) + " " + cluster.get(j) + " " 
// 			   + branchDescriptors.get(cluster.get(j)).getName());
// 	    }
	    // generate a set of cloned branchdescriptors:
	    for (int j = 0; j < cluster.size(); ++j) {
		branchClonedCluster.add((Object3D)((branchDescriptors.get(cluster.get(j))).cloneDeep()));
		branchCluster.add(branchDescriptors.get(cluster.get(j)));
	    }
	    // log.fine("Found branch descriptor path: " + cluster.toString());
	    // TODO : first and last branch descriptor are only partially adjusted !!!??? :-(
	    if (branchClonedCluster.size() > 1) {
		for (int j = 0; j < branchClonedCluster.size(); ++j) {	    
		    int jj2 = j;
		    int jj1 = j-1;
		    if (j == 0) {
			jj1 = branchClonedCluster.size()-1;
		    }
		    else {
			assert isConnectedIncomingOutgoing(branchDescriptors, cluster.get(jj1), cluster.get(jj2));
			assert isConnectedIncomingOutgoing(branchCluster, jj1, jj2);
		    }
		    assert jj1 != jj2;
		    BranchDescriptor3D b1 = (BranchDescriptor3D)(branchClonedCluster.get(jj1));
		    BranchDescriptor3D b2 = (BranchDescriptor3D)(branchClonedCluster.get(jj2));
		    if (!b1.getIncomingStrand().sequenceString().equals(b2.getOutgoingStrand().sequenceString())) {
			log.fine("Different strands, even though they should be the same: " 
				 + b1.getIncomingStrand().sequenceString() + " " + b2.getOutgoingStrand().sequenceString() + " " + jj1 + " " + jj2);
			skipJunctionFlag = true;
			break; // skipping this junction!
		    }
		    assert b1.getIncomingStrand().sequenceString().equals(b2.getOutgoingStrand().sequenceString()); 
		    adjustBranchDescriptorPair(b1,b2); // critical: two cloned strands get consolidated to one strand 
		    // assert testPos.distance(Object3DTools.findFirstLeaf(atomRoot).getPosition()) < 0.001; // test for side effects
		}
		if (skipJunctionFlag) {
		    log.fine("Skipping ill defined junction!");
		    skipJunctionFlag = false;
		    continue;
		}
		// TODO : force cyclical path! Check if that is always ok!!! ????
		StrandJunction3D junction = new SimpleStrandJunction3D(branchClonedCluster);
		if (!junction.isValid()) {
		    log.fine("Junction was not valid, skipping");
		    continue; // skip this junction
		}
		String incomingName = incomingNomenclature.generateNomenclature(junction);
		if ((junction.getBranchCount() >= junctionOrderMin)
		    && junction.isValid() && (!resultJunctionNames.contains(incomingName))) {
		    log.fine("Important junction test: corridor: " + junction.corridorCheck(corridorDescriptor) 
			     + " " + corridorDescriptor + " looplength sum: "  + (LilleyNomenclature.computeLoopLengthSum(junction))
			     + " nomenclature: " + lilleyNomenclature.generateNomenclature(junction) );
		    if (junction.corridorCheck(corridorDescriptor) && (LilleyNomenclature.computeLoopLengthSum(junction) <= loopLengthSumMax)
			&& (LilleyNomenclature.computeLoopLengthSum(junction) >= 0) 
			&& (lilleyNomenclature.generateNomenclature(junction).length() > 0)) {

			if (junctionHasInternalHelices(junction)) {
			    junction.setInternalHelices(true);
			}
			else {
			    junction.setInternalHelices(false);
			}
			result.add(junction);
			resultJunctionNames.add(incomingName);
			if (debugLevel > 1) {
			    // assert testPos.distance(Object3DTools.findFirstLeaf(atomRoot).getPosition()) < 0.001; // test for side effects
// 			    for (int k = 0; k < branchDescriptors.size(); ++k) {
// 				double dist = Object3DSetTools.distanceMin(branchDescriptors.get(k), atomRoot);
// 				if ( dist >= BRANCH_PLAUSIBLE_DIST) {
// 				    // log.fine("" + dist + " : BranchDescriptor3D " + (k+1) + " is too far: " + branchDescriptors.get(k).getName());
// 				    assert false;
// 				}
// 			    }
			}
		    }
		    else {
			log.fine("Ignoring junction " + junction.getName() + " because corridor test failed." + corridorDescriptor);
		    }
		}
		else {
		    log.info("Warning: generated junction was not valid!");
		}
	    }
	    else {
		log.fine("BranchCloneCluster too small " + cluster.size());
	    }
// 	    if (debugLevel > 1) {
// 		assert testPos.distance(Object3DTools.findFirstLeaf(atomRoot).getPosition()) < 0.001; // test for side effects
// 	    }
	}
	// log.fine("Adding hairpins...");
	// add single branch fragments:
	int singleCount = 0;
	for (int i = 0; i < branchDescriptors.size(); ++i) {
	    BranchDescriptor3D branch = (BranchDescriptor3D)(branchDescriptors.get(i));
	    // if indices are the other way round it indicates that the non-stem part of the described regions is not a loop but disconnected 
	    if (branch.isSingleSequence() && (branch.getOutgoingIndex() > branch.getIncomingIndex())) {
		BranchDescriptor3D branchClone = (BranchDescriptor3D)(branchDescriptors.get(i).cloneDeep());
		Object3DSet branchSet = new SimpleObject3DSet();
		branchSet.add(branchClone); // consists of only one branch descriptor
		StrandJunction3D junction = new SimpleStrandJunction3D(branchSet);
		String incomingName = incomingNomenclature.generateNomenclature(junction);
		if ((junction.getBranchCount() >= junctionOrderMin)
		    && junction.corridorCheck(corridorDescriptor)
		    && (!resultJunctionNames.contains(incomingName))) {
		    if (junctionHasInternalHelices(junction, branchDescriptors)) {
			junction.setInternalHelices(true);
		    }
		    else {
			junction.setInternalHelices(false);
		    }
		    result.add(junction);
		    resultJunctionNames.add(incomingName);
		    ++singleCount;
		}
		else {
		    // log.fine("Warning: generated hairpin junction failed because of corridor test. " + corridorDescriptor);
		}
	    }
	}
	// log.fine("Number of single branches added: " + singleCount);
	// assert testPos.distance(Object3DTools.findFirstLeaf(atomRoot).getPosition()) < 0.001; // test for side effects
	//	log.info("Finished generateJunctions (4) with corridorDescriptor " + corridorDescriptor);
	return result;
    }

    /** returns indices of stems that are part of loop region */
    private static Set<Integer> generateLoopStemIndices(BranchDescriptor3D branch, Object3DSet stemSet) {
	Set<Integer> result = new HashSet<Integer>();
	for (int i = 0; i < stemSet.size(); ++i) {
	    RnaStem3D stem = (RnaStem3D)(stemSet.get(i));
	    if (KissingLoop3D.isLoopStem(branch, stem)) {
		result.add(new Integer(i));
	    }
	}
	return result;
    }

    /** returns true, if branch descriptor occupies a free "corridor" with a certain radius.                                                                                                                                     
     * If supplied radius is smaller or equal zero, true is returned. */
    public static boolean corridorCheck(BranchDescriptor3D branch, CorridorDescriptor corridorDescriptor, Object3DSet atomSet) {
        double corridorRadius = corridorDescriptor.radius;
        double corridorStart = corridorDescriptor.start;
        if (corridorRadius <= 0.0) {
            return true;
        }
        // Object3DSet atomSet = Object3DTools.collectByClassName(this, "Atom3D"); // obtain all atoms ... TODO: improve speed of slow method                                                                                    
	assert atomSet.size() > 0;
        if (! Object3DTools.corridorCheck(branch.getPosition(), branch.getDirection(), corridorRadius, corridorStart, atomSet)) {
            return false;
        }
        return true;
    }

    /** generates a set of StrandJunction objects given a set of branchdescriptors.
     * 	TODO : first and last branch descriptor are only partially adjusted !
     * Includes option for internal helices check.
     */
    private static Object3DSet generateJunctions(Object3DSet branchDescriptors,
						CorridorDescriptor corridorDescriptor,
						 int iterMax, int loopLengthSumMax, boolean IntHelicesCheck) {
	log.fine("Starting generateJunctions (4) with corridorDescriptor " + corridorDescriptor);
	StrandJunctionNomenclature lilleyNomenclature = new LilleyNomenclature();
	StrandJunctionNomenclature incomingNomenclature = new IncomingNomenclature();
	if (branchDescriptors.size() == 0) {
	    log.fine("No branch descriptors defined, returning empty set of junctions!");
	    return new SimpleObject3DSet(); // return empty set!
	}
	// Vector3D testPos = Object3DTools.findFirstLeaf(atomRoot).getPosition();
	for (int i = 0; i < branchDescriptors.size(); ++i) {
	    BranchDescriptor3D bd = (BranchDescriptor3D)(branchDescriptors.get(i));
	    // 	    log.finest("Info about branch descriptor " + (i+1) + " " + bd.getName() + " "  
	    // + (bd.getIncomingIndex() + 1) + " " + (bd.getOutgoingIndex() + 1));
// 	    double dist = Object3DSetTools.distanceMin(branchDescriptors.get(i), atomRoot);
// 	    if ( dist >= BRANCH_PLAUSIBLE_DIST) {
// 		log.warning("" + dist + " : BranchDescriptor3D " + (i+1) + " is too far: " + branchDescriptors.get(i));
// 		assert false;
// 	    }
	}
	log.fine("Starting to cluster branch descriptors...");
	IntegerListList clusters = clusterBranchDescriptors(branchDescriptors, iterMax); // clusters can be overlapping
	log.fine("Finished clustering branch descriptors: " + clusters.size());
	// 	assert testPos.distance(Object3DTools.findFirstLeaf(atomRoot).getPosition()) < 0.001; // test for side effects
	// just for testing:
	for (int i = 0; i < branchDescriptors.size(); ++i) {
// 	    double dist = Object3DSetTools.distanceMin(branchDescriptors.get(i), atomRoot);
// 	    if ( dist >= BRANCH_PLAUSIBLE_DIST) {
// 		log.warning("" + dist + " : BranchDescriptor3D " + (i+1) + " is too far: " + branchDescriptors.get(i));
// 		assert false;
// 	    }
	}
	// log.info("Clustered branch descriptors: " + clusters);
	Object3DSet result = new SimpleObject3DSet();
	Set<String> resultJunctionNames = new HashSet<String>();
	for (int i = 0; i < clusters.size(); ++i) {
	    boolean skipJunctionFlag = false;
	    Object3DSet branchCluster = new SimpleObject3DSet();
	    Object3DSet branchClonedCluster = new SimpleObject3DSet();
	    IntegerList cluster = clusters.get(i);
	    // log.fine("generateJunctions: working on cluster " + (i+1) + " with size " + cluster.size());
	    assert cluster.isUnique();
	    // assert testPos.distance(Object3DTools.findFirstLeaf(atomRoot).getPosition()) < 0.001; // test for side effects
	    if (cluster.size() > 1) {
		assert isConnectedIncomingOutgoing(branchDescriptors, cluster.get(0), cluster.get(1));	    
	    }
	    // just checking if atoms are nearby 
	    if (debugLevel > 1) {
		for (int k = 0; k < cluster.size(); ++k) {
// 		    double dist = Object3DSetTools.distanceMin(branchDescriptors.get(cluster.get(k)), atomRoot);
// 		    if ( dist >= BRANCH_PLAUSIBLE_DIST) {
// 			// log.fine("" + dist + " : BranchDescriptor3D " + (i+1) + " is too far: " + branchDescriptors.get(i));
// 			assert false;
// 		    }
		}
	    }
// 	    for (int j = 0; j < cluster.size(); ++j) {
// 		log.fine("Cluster member: " + (i+1) + " " + cluster.get(j) + " " 
// 			   + branchDescriptors.get(cluster.get(j)).getName());
// 	    }
	    // generate a set of cloned branchdescriptors:
	    for (int j = 0; j < cluster.size(); ++j) {
		branchClonedCluster.add((Object3D)((branchDescriptors.get(cluster.get(j))).cloneDeep()));
		branchCluster.add(branchDescriptors.get(cluster.get(j)));
	    }
	    // log.fine("Found branch descriptor path: " + cluster.toString());
	    // TODO : first and last branch descriptor are only partially adjusted !!!??? :-(
	    if (branchClonedCluster.size() > 1) {
		for (int j = 0; j < branchClonedCluster.size(); ++j) {	    
		    int jj2 = j;
		    int jj1 = j-1;
		    if (j == 0) {
			jj1 = branchClonedCluster.size()-1;
		    }
		    else {
			assert isConnectedIncomingOutgoing(branchDescriptors, cluster.get(jj1), cluster.get(jj2));
			assert isConnectedIncomingOutgoing(branchCluster, jj1, jj2);
		    }
		    assert jj1 != jj2;
		    BranchDescriptor3D b1 = (BranchDescriptor3D)(branchClonedCluster.get(jj1));
		    BranchDescriptor3D b2 = (BranchDescriptor3D)(branchClonedCluster.get(jj2));
		    if (!b1.getIncomingStrand().sequenceString().equals(b2.getOutgoingStrand().sequenceString())) {
			log.fine("Different strands, even though they should be the same: " 
				 + b1.getIncomingStrand().sequenceString() + " " + b2.getOutgoingStrand().sequenceString() + " " + jj1 + " " + jj2);
			skipJunctionFlag = true;
			break; // skipping this junction!
		    }
		    assert b1.getIncomingStrand().sequenceString().equals(b2.getOutgoingStrand().sequenceString()); 
		    adjustBranchDescriptorPair(b1,b2); // critical: two cloned strands get consolidated to one strand 
		    // assert testPos.distance(Object3DTools.findFirstLeaf(atomRoot).getPosition()) < 0.001; // test for side effects
		}
		if (skipJunctionFlag) {
		    log.fine("Skipping ill defined junction!");
		    skipJunctionFlag = false;
		    continue;
		}
		// TODO : force cyclical path! Check if that is always ok!!! ????
		StrandJunction3D junction = new SimpleStrandJunction3D(branchClonedCluster);
		if (!junction.isValid()) {
		    log.fine("Junction was not valid, skipping");
		    continue; // skip this junction
		}
		String incomingName = incomingNomenclature.generateNomenclature(junction);
		if ((junction.getBranchCount() >= junctionOrderMin)
		    && junction.isValid() && (!resultJunctionNames.contains(incomingName))) {
		    log.fine("Important junction test: corridor: " + junction.corridorCheck(corridorDescriptor) 
			     + " " + corridorDescriptor + " looplength sum: "  + (LilleyNomenclature.computeLoopLengthSum(junction))
			     + " nomenclature: " + lilleyNomenclature.generateNomenclature(junction) );
		    if (junction.corridorCheck(corridorDescriptor) && (LilleyNomenclature.computeLoopLengthSum(junction) <= loopLengthSumMax)
			&& (LilleyNomenclature.computeLoopLengthSum(junction) >= 0) 
			&& (lilleyNomenclature.generateNomenclature(junction).length() > 0)) {

			if ((junctionHasInternalHelices(junction)) && IntHelicesCheck) {
			    junction.setInternalHelices(true);
			}
			else {
			    junction.setInternalHelices(false);
			}
	
			result.add(junction);
			resultJunctionNames.add(incomingName);
			if (debugLevel > 1) {
			    // assert testPos.distance(Object3DTools.findFirstLeaf(atomRoot).getPosition()) < 0.001; // test for side effects
// 			    for (int k = 0; k < branchDescriptors.size(); ++k) {
// 				double dist = Object3DSetTools.distanceMin(branchDescriptors.get(k), atomRoot);
// 				if ( dist >= BRANCH_PLAUSIBLE_DIST) {
// 				    // log.fine("" + dist + " : BranchDescriptor3D " + (k+1) + " is too far: " + branchDescriptors.get(k).getName());
// 				    assert false;
// 				}
// 			    }
			}
		    }
		    else {
			log.fine("Ignoring junction " + junction.getName() + " because corridor test failed." + corridorDescriptor);
		    }
		}
		else {
		    log.info("Warning: generated junction was not valid!");
		}
	    }
	    else {
		log.fine("BranchCloneCluster too small " + cluster.size());
	    }
// 	    if (debugLevel > 1) {
// 		assert testPos.distance(Object3DTools.findFirstLeaf(atomRoot).getPosition()) < 0.001; // test for side effects
// 	    }
	}
	// log.fine("Adding hairpins...");
	// add single branch fragments:
	int singleCount = 0;
	for (int i = 0; i < branchDescriptors.size(); ++i) {
	    BranchDescriptor3D branch = (BranchDescriptor3D)(branchDescriptors.get(i));
	    // if indices are the other way round it indicates that the non-stem part of the described regions is not a loop but disconnected 
	    if (branch.isSingleSequence() && (branch.getOutgoingIndex() > branch.getIncomingIndex())) {
		BranchDescriptor3D branchClone = (BranchDescriptor3D)(branchDescriptors.get(i).cloneDeep());
		Object3DSet branchSet = new SimpleObject3DSet();
		branchSet.add(branchClone); // consists of only one branch descriptor
		StrandJunction3D junction = new SimpleStrandJunction3D(branchSet);
		String incomingName = incomingNomenclature.generateNomenclature(junction);
		if ((junction.getBranchCount() >= junctionOrderMin)
		    && junction.corridorCheck(corridorDescriptor)
		    && (!resultJunctionNames.contains(incomingName))) {
		    if (junctionHasInternalHelices(junction, branchDescriptors)) {
			junction.setInternalHelices(true);
		    }
		    else {
			junction.setInternalHelices(false);
		    }
		    result.add(junction);
		    resultJunctionNames.add(incomingName);
		    ++singleCount;
		}
		else {
		    // log.fine("Warning: generated hairpin junction failed because of corridor test. " + corridorDescriptor);
		}
	    }
	}
	// log.fine("Number of single branches added: " + singleCount);
	// assert testPos.distance(Object3DTools.findFirstLeaf(atomRoot).getPosition()) < 0.001; // test for side effects
	//	log.info("Finished generateJunctions (4) with corridorDescriptor " + corridorDescriptor);
	return result;
    }

    /** generates a set of StrandJunction objects given a set of branchdescriptors.
     * 	TODO : first and last branch descriptor are only partially adjusted !
     * Includes option for internal helices check.
     */
    protected static Object3DSet generateGeneralMotifs(Object3DSet branchDescriptorsOrig,
						       CorridorDescriptor corridorDescriptor,
						       Object3DSet additional) {
	log.info("Starting generateGeneralMotifs with corridorDescriptor " + corridorDescriptor);
	int iterMax = JUNCTION_PATH_ITER_MAX; // not needed for motif scan
	StrandJunctionNomenclature lilleyNomenclature = new LilleyNomenclature();
	StrandJunctionNomenclature incomingNomenclature = new IncomingNomenclature();
	if (branchDescriptorsOrig.size() == 0) {
	    log.info("No branch descriptors defined, returning empty set of junctions!");
	    return new SimpleObject3DSet(); // return empty set!
	}
        Object3DSet branchDescriptors = new SimpleObject3DSet();
	Object3DSet atoms = Object3DTools.collectByClassName(additional, "Atom3D"); // get all atoms
	log.info("Extraced " + atoms.size() + " atoms from structure. " + additional.toString());
	// Vector3D testPos = Object3DTools.findFirstLeaf(atomRoot).getPosition();
	for (int i = 0; i < branchDescriptorsOrig.size(); ++i) {
	    BranchDescriptor3D bd = (BranchDescriptor3D)(branchDescriptorsOrig.get(i));
	    log.info("Motif helix end descriptor" + (i+1) + " " + bd.getIncomingIndex() + " " 
		     + bd.getOutgoingIndex()
		     + " Inc strand size: " + bd.getIncomingStrand().size() 
		     + " Outg strand size: " + bd.getOutgoingStrand().size());
	    if ((bd.getIncomingIndex() <= 1) && ((bd.getOutgoingIndex() + 2) >= bd.getOutgoingStrand().size())) {
		log.info("Adding helix end descriptor " + bd.getName());
		if (corridorCheck(bd, corridorDescriptor, atoms)) {
		    branchDescriptors.add(bd);
		} else {
		    log.info("Corridor check failed for " + bd.getFullName());
		}
	    }
	}
	Object3DSet result = new SimpleObject3DSet();
	if (branchDescriptors.size() > 1) {
	    GeneralHelixMotif3D junction = new GeneralHelixMotif3D(branchDescriptors, additional);
	    result.add(junction);
	} else {
	    log.info("Insufficient number " + branchDescriptors.size() + " of usable helix-end descriptors defined: returning empty set of junctions!");
	}
	return result;
    }

    /** generates a set of KissingLoop3D objects given a set of branchdescriptors.
     */
    private static Object3DSet generateKissingLoops(Object3DSet branchDescriptors, Object3DSet stemSet) {
	// log.info("Starting generateKissingLoops(3) :" + branchDescriptors.size()
	// 	   + " " + stemSet.size() + " " + atomRoot.size());
	if (branchDescriptors.size() == 0) {
	    return new SimpleObject3DSet(); // return empty set!
	}
// 	for (int i = 0; i < branchDescriptors.size(); ++i) {
// 	    double dist = Object3DSetTools.distanceMin(branchDescriptors.get(i), atomRoot);
// 	    if ( dist >= BRANCH_PLAUSIBLE_DIST) {
// 		log.finest("" + dist + " : BranchDescriptor3D " + (i+1) + " is too far: " + branchDescriptors.get(i));
// 		assert false;
// 	    }
// 	}
	Object3DSet result = new SimpleObject3DSet();
	boolean[] singleFlags = new boolean[branchDescriptors.size()];
	List<Set<Integer> > loopStemIndices = new ArrayList< Set<Integer> >();
	// log.info("Starting to generate loop stem indices!");
	for (int i = 0; i < branchDescriptors.size(); ++i) {
	    singleFlags[i] = ((BranchDescriptor3D)branchDescriptors.get(i)).isSingleSequence();
	    loopStemIndices.add( generateLoopStemIndices((BranchDescriptor3D)branchDescriptors.get(i), stemSet));
	}
	// log.info("Finished generating loop stem indices!");
	for (int i = 0; i < branchDescriptors.size(); ++i) {
	    BranchDescriptor3D branch1 = (BranchDescriptor3D)(branchDescriptors.get(i));
	    // log.info("Checking for kissing loop partner: " + branch1.getName() + " with "
	    // + loopStemIndices.get(i).size() + " internal helices.");
	    if (!singleFlags[i]) {
		// log.info("branch descriptor does not correspond to single sequence!");
		// log.info(branch1.getOutgoingStrand().getName() + " " + branch1.getIncomingStrand().getName());
		continue;
	    }
	    int numInternalHelices = loopStemIndices.get(i).size();
	    if ((numInternalHelices == 0) || (numInternalHelices > KISSING_LOOP_INTERNAL_HELIX_MAX)) {
		log.fine("Ignoring branch descriptor because bad number of internal helices: " + branchDescriptors.get(i).getName());
		continue;
	    }

	    for (int j = i+1; j < branchDescriptors.size(); ++j) {
		BranchDescriptor3D branch2 = (BranchDescriptor3D)(branchDescriptors.get(j));
		if (!singleFlags[j]) {
		    log.fine("branch descriptor 2 does not correspond to single sequence: " + branch2.getName());
		    log.fine(branch2.getOutgoingStrand().getName() + " " + branch2.getIncomingStrand().getName());
		    continue;
		}
		int numInternalHelices2 = loopStemIndices.get(j).size();
		if ((numInternalHelices2 == 0) || (numInternalHelices2 > KISSING_LOOP_INTERNAL_HELIX_MAX)) {
		    log.fine("Ignoring second branch descriptor because of too many internal helices: " 
			     + branchDescriptors.get(j).getName()); 
		    continue;
		}
		else {
		    log.fine("comparing branch descriptor " + branchDescriptors.get(j).getName() + " with "
			     + loopStemIndices.get(j).size() + " internal helices.");
		}

		if (branch2.distance(branch1) > KISSING_LOOP_DIST_MAX) {
		    log.fine("The distance between the branch discriptors is too large: " + branch2.distance(branch1)); 
		    continue;
		}
		if (KissingLoop3D.isKissingLoop(branch1, branch2, stemSet, 
						loopStemIndices.get(i), loopStemIndices.get(j) ) ) {
			    // if indices are the other way round it indicates that the non-stem part of the described regions is not a loop but disconnected 
		    // log.fine("Kissing loop found: " + branch1.getName() + " " + branch2.getName());
		    Object3DSet branchSet = new SimpleObject3DSet();
		    BranchDescriptor3D b1 = (BranchDescriptor3D)(branch1.cloneDeep());
		    adjustBranchDescriptorLoop(b1);
		    branchSet.add(b1); // consists of only one branch descriptor
		    BranchDescriptor3D b2 = (BranchDescriptor3D)(branch2.cloneDeep());
		    adjustBranchDescriptorLoop(b2);
		    branchSet.add(b2); // consists of only one branch descriptor
		    KissingLoop3D kissingLoop = new KissingLoop3D(branchSet);
		    result.add(kissingLoop);
		}
		else {
		    log.fine("No kissing loop found: " + (i+1) + " " + (j+1) + branch1.getName() + " " + branch2.getName());
		}
	    }
	}
	log.fine("Finished generateKissingLoops(3)");
	return result;
    }


 /** generates a set of KissingLoop3D objects given a set of branchdescriptors, with the use of the internal helix checked as an option
     */
    private static Object3DSet generateKissingLoops(Object3DSet branchDescriptors, Object3DSet stemSet, boolean IntHelixCheck) {
	// log.info("Starting generateKissingLoops(3) :" + branchDescriptors.size()
	// 	   + " " + stemSet.size() + " " + atomRoot.size());
	if (branchDescriptors.size() == 0) {
	    return new SimpleObject3DSet(); // return empty set!
	}
// 	for (int i = 0; i < branchDescriptors.size(); ++i) {
// 	    double dist = Object3DSetTools.distanceMin(branchDescriptors.get(i), atomRoot);
// 	    if ( dist >= BRANCH_PLAUSIBLE_DIST) {
// 		log.finest("" + dist + " : BranchDescriptor3D " + (i+1) + " is too far: " + branchDescriptors.get(i));
// 		assert false;
// 	    }
// 	}
	Object3DSet result = new SimpleObject3DSet();
	boolean[] singleFlags = new boolean[branchDescriptors.size()];
	List<Set<Integer> > loopStemIndices = new ArrayList< Set<Integer> >();
	// log.info("Starting to generate loop stem indices!");
	for (int i = 0; i < branchDescriptors.size(); ++i) {
	    singleFlags[i] = ((BranchDescriptor3D)branchDescriptors.get(i)).isSingleSequence();
	    loopStemIndices.add( generateLoopStemIndices((BranchDescriptor3D)branchDescriptors.get(i), stemSet));
	}
	// log.info("Finished generating loop stem indices!");
	for (int i = 0; i < branchDescriptors.size(); ++i) {
	    BranchDescriptor3D branch1 = (BranchDescriptor3D)(branchDescriptors.get(i));
	    // log.info("Checking for kissing loop partner: " + branch1.getName() + " with "
	    // + loopStemIndices.get(i).size() + " internal helices.");
	    if (!singleFlags[i]) {
		// log.info("branch descriptor does not correspond to single sequence!");
		// log.info(branch1.getOutgoingStrand().getName() + " " + branch1.getIncomingStrand().getName());
		continue;
	    }
	    int numInternalHelices = loopStemIndices.get(i).size();
	    if (((numInternalHelices == 0) || (numInternalHelices > KISSING_LOOP_INTERNAL_HELIX_MAX)) && IntHelixCheck) {
		log.fine("Ignoring branch descriptor because bad number of internal helices: " + branchDescriptors.get(i).getName());
		continue;
	    }

	    for (int j = i+1; j < branchDescriptors.size(); ++j) {
		BranchDescriptor3D branch2 = (BranchDescriptor3D)(branchDescriptors.get(j));
		if (!singleFlags[j]) {
		    log.fine("branch descriptor 2 does not correspond to single sequence: " + branch2.getName());
		    log.fine(branch2.getOutgoingStrand().getName() + " " + branch2.getIncomingStrand().getName());
		    continue;
		}
		int numInternalHelices2 = loopStemIndices.get(j).size();
		if (((numInternalHelices2 == 0) || (numInternalHelices2 > KISSING_LOOP_INTERNAL_HELIX_MAX)) && IntHelixCheck) {
		    log.fine("Ignoring second branch descriptor because of too many internal helices: " 
			     + branchDescriptors.get(j).getName()); 
		    continue;
		}
		else {
		    log.fine("comparing branch descriptor " + branchDescriptors.get(j).getName() + " with "
			     + loopStemIndices.get(j).size() + " internal helices.");
		}

		if (branch2.distance(branch1) > KISSING_LOOP_DIST_MAX) {
		    log.fine("The distance between the branch discriptors is too large: " + branch2.distance(branch1)); 
		    continue;
		}
		if (KissingLoop3D.isKissingLoop(branch1, branch2, stemSet, 
						loopStemIndices.get(i), loopStemIndices.get(j) ) ) {
			    // if indices are the other way round it indicates that the non-stem part of the described regions is not a loop but disconnected 
		    // log.fine("Kissing loop found: " + branch1.getName() + " " + branch2.getName());
		    Object3DSet branchSet = new SimpleObject3DSet();
		    BranchDescriptor3D b1 = (BranchDescriptor3D)(branch1.cloneDeep());
		    adjustBranchDescriptorLoop(b1);
		    branchSet.add(b1); // consists of only one branch descriptor
		    BranchDescriptor3D b2 = (BranchDescriptor3D)(branch2.cloneDeep());
		    adjustBranchDescriptorLoop(b2);
		    branchSet.add(b2); // consists of only one branch descriptor
		    KissingLoop3D kissingLoop = new KissingLoop3D(branchSet);
		    result.add(kissingLoop);
		}
		else {
		    log.fine("No kissing loop found: " + (i+1) + " " + (j+1) + branch1.getName() + " " + branch2.getName());
		}
	    }
	}
	log.fine("Finished generateKissingLoops(3)");
	return result;
    }

    /** generates a set of StrandJunction objects given a root that contains stems */
    private static Object3DSet generateJunctions(Object3D stemRoot, int branchDescriptorOffset,
						CorridorDescriptor corridorDescriptor,
						double rmsTolerance, int loopLengthSumMax) {
	log.fine("Starting generateJunctions (3) using offset " + branchDescriptorOffset + " and rms: "
		 + rmsTolerance + " loop length sum max: " + loopLengthSumMax + " and corridor " + corridorDescriptor);
	int iterMax = JUNCTION_PATH_ITER_MAX; 
	Object3DSet stemSet = Object3DTools.collectByClassName(stemRoot, "RnaStem3D");
// 	log.fine("Found stems: ");
// 	for (int i = 0; i < stemSet.size(); ++i) {
// 	    log.info("Stem " + (i+1) + " : " + " toString: " + stemSet.get(i));
// 	}
	Object3DSet branchDescriptors = generateBranchDescriptors(stemSet, branchDescriptorOffset, rmsTolerance);
 	log.fine("Number of generated branch descriptors: " + branchDescriptors.size());
 	for (int i = 0; i < branchDescriptors.size(); ++i) {
 	    log.fine("Branch Descriptor " + (i+1) + " : "
		     + ((BranchDescriptor3D)branchDescriptors.get(i)).infoString());
 	}
	Object3DSet junctions = generateJunctions(branchDescriptors, corridorDescriptor, iterMax, loopLengthSumMax);
 	log.fine("Found branchDescriptors and Junctions: " 
 			   + branchDescriptors.size() + " " 
 			   + junctions.size());
	log.fine("Finished generateJunctions (3)");
	return junctions;
    }

    //with check intHelix option
    private static Object3DSet generateJunctions(Object3D stemRoot, int branchDescriptorOffset,
						CorridorDescriptor corridorDescriptor,
						 double rmsTolerance, int loopLengthSumMax, boolean checkIntHelices) {
	log.fine("Starting generateJunctions (3) using offset " + branchDescriptorOffset + " and rms: "
		 + rmsTolerance + " loop length sum max: " + loopLengthSumMax + " and corridor " + corridorDescriptor);
	int iterMax = JUNCTION_PATH_ITER_MAX; 
	Object3DSet stemSet = Object3DTools.collectByClassName(stemRoot, "RnaStem3D");
// 	log.fine("Found stems: ");
// 	for (int i = 0; i < stemSet.size(); ++i) {
// 	    log.info("Stem " + (i+1) + " : " + " toString: " + stemSet.get(i));
// 	}
	Object3DSet branchDescriptors = generateBranchDescriptors(stemSet, branchDescriptorOffset, rmsTolerance);
 	log.fine("Number of generated branch descriptors: " + branchDescriptors.size());
 	for (int i = 0; i < branchDescriptors.size(); ++i) {
 	    log.fine("Branch Descriptor " + (i+1) + " : "
		     + ((BranchDescriptor3D)branchDescriptors.get(i)).infoString());
 	}
	Object3DSet junctions = generateJunctions(branchDescriptors, corridorDescriptor, iterMax, loopLengthSumMax, checkIntHelices);
 	log.fine("Found branchDescriptors and Junctions: " 
 			   + branchDescriptors.size() + " " 
 			   + junctions.size());
	log.fine("Finished generateJunctions (3)");
	return junctions;
    }

    /** generates a set of StrandJunction objects given a root that contains stems */
    private static Object3DSet generateKissingLoops(Object3D stemRoot, int branchDescriptorOffset,
						   double rmsTolerance) {
	log.fine("Starting generateKissingLoops (2)!");
	Object3DSet stemSet = Object3DTools.collectByClassName(stemRoot, "RnaStem3D");
// 	log.fine("Found stems: ");
// 	for (int i = 0; i < stemSet.size(); ++i) {
// 	    log.fine("Stem " + (i+1) + " : " + stemSet.get(i));
// 	}
	Object3DSet branchDescriptors = generateBranchDescriptors(stemSet, branchDescriptorOffset, rmsTolerance);
	log.fine("Number of generated branch descriptors: " + branchDescriptors.size());
	Object3DSet kissingLoops = generateKissingLoops(branchDescriptors, stemSet);
// 	log.info("Found branchDescriptors and kissing loops: " 
// 			   + branchDescriptors.size() + " " 
// 			   + kissingLoops.size());
// 	log.info("Finished generateKissingLoops (2)!");
	return kissingLoops;
    }

    /** generates a set of StrandJunction objects given a root that contains stems, with intHelixCheck option */
    private static Object3DSet generateKissingLoops(Object3D stemRoot, int branchDescriptorOffset,
						    double rmsTolerance, boolean intHelixChecker) {
	log.fine("Starting generateKissingLoops (2)!");
	Object3DSet stemSet = Object3DTools.collectByClassName(stemRoot, "RnaStem3D");
// 	log.fine("Found stems: ");
// 	for (int i = 0; i < stemSet.size(); ++i) {
// 	    log.fine("Stem " + (i+1) + " : " + stemSet.get(i));
// 	}
	Object3DSet branchDescriptors = generateBranchDescriptors(stemSet, branchDescriptorOffset, rmsTolerance);
	log.fine("Number of generated branch descriptors: " + branchDescriptors.size());
	Object3DSet kissingLoops = generateKissingLoops(branchDescriptors, stemSet, intHelixChecker);
// 	log.info("Found branchDescriptors and kissing loops: " 
// 			   + branchDescriptors.size() + " " 
// 			   + kissingLoops.size());
// 	log.info("Finished generateKissingLoops (2)!");
	return kissingLoops;
    }

    /** generates a set of StrandJunction objects given a root that contains stems */
    public static Object3D generateJunctions(Object3D stemRootOrig,
					     String baseName,
					     int branchDescriptorOffset,
					     CorridorDescriptor corridorDescriptor,
					     double rmsTolerance,
					     int loopLengthSumMax) {
	assert stemRootOrig != null;
	assert baseName != null;
	log.fine("Starting BranchDescriptorTools.generateJunctions (2)!");
	Object3D root = new SimpleObject3D();
	root.setName(baseName);
	Object3DSet stemSet = Object3DTools.collectByClassName(stemRootOrig, "RnaStem3D");
	assert stemSet.size() > 0;
	if (stemSet.size() > 0) {
	    Vector3D origPos = stemSet.get(0).getPosition();
	    log.fine("First leaf vector: " + origPos);
	    Object3D stemRoot = (Object3D)(stemRootOrig.cloneDeep());
	    Object3DSet junctions = generateJunctions(stemRoot, branchDescriptorOffset, corridorDescriptor, rmsTolerance,
						      loopLengthSumMax);
	    if (junctions.size() == 0) {
		log.fine("no junctions were found in structure!");
	    }
	    for (int i = 0; i < junctions.size(); ++i) {
		Object3D junction = junctions.get(i);
		log.fine("Generated junction " + (i+1) + " with position: " + junction.getPosition());
// 		if (Object3DSetTools.distanceMin(junction, atomRoot) >= JUNCTION_PLAUSIBLE_DIST) {
// 		    log.fine("Warning: Object3DSetTools.distanceMin too far: "
// 				       + junction.getPosition() + " " + Object3DSetTools.distanceMin(junction, atomRoot));
// 		}
// 		assert (Object3DSetTools.distanceMin(junction, atomRoot) < JUNCTION_PLAUSIBLE_DIST); 
		String newName = "j" + (i+1);
		junction.setName(newName);
		root.insertChild(junction);
	    }
	    // check if atom positions were moved:
	    assert origPos.distance(Object3DTools.collectByClassName(stemRootOrig, "RnaStem3D").get(0).getPosition()) < 0.001;
	    log.fine("First leaf vector at end of method: " + (Object3DTools.findFirstLeaf(stemRootOrig)));
	}
	else {
	    log.fine("no atoms were found in structure!");
	}
	log.fine("Quitting BranchDescriptorTools.generateJunctions (2)!");
	return root;
    }


    /** generates a set of StrandJunction objects given a root that contains stem, with check int helices option */
    public static Object3D generateJunctions(Object3D stemRootOrig,
					     String baseName,
					     int branchDescriptorOffset,
					     CorridorDescriptor corridorDescriptor,
					     double rmsTolerance,
					     int loopLengthSumMax, boolean checkIntHelices) {
	assert stemRootOrig != null;
	assert baseName != null;
	log.fine("Starting BranchDescriptorTools.generateJunctions (2)!");
	Object3D root = new SimpleObject3D();
	root.setName(baseName);
	Object3DSet stemSet = Object3DTools.collectByClassName(stemRootOrig, "RnaStem3D");
	assert stemSet.size() > 0;
	if (stemSet.size() > 0) {
	    Vector3D origPos = stemSet.get(0).getPosition();
	    log.fine("First leaf vector: " + origPos);
	    Object3D stemRoot = (Object3D)(stemRootOrig.cloneDeep());
	    Object3DSet junctions = generateJunctions(stemRoot, branchDescriptorOffset, corridorDescriptor, rmsTolerance,
						      loopLengthSumMax, checkIntHelices);
	    if (junctions.size() == 0) {
		log.fine("no junctions were found in structure!");
	    }
	    for (int i = 0; i < junctions.size(); ++i) {
		Object3D junction = junctions.get(i);
		log.fine("Generated junction " + (i+1) + " with position: " + junction.getPosition());
// 		if (Object3DSetTools.distanceMin(junction, atomRoot) >= JUNCTION_PLAUSIBLE_DIST) {
// 		    log.fine("Warning: Object3DSetTools.distanceMin too far: "
// 				       + junction.getPosition() + " " + Object3DSetTools.distanceMin(junction, atomRoot));
// 		}
// 		assert (Object3DSetTools.distanceMin(junction, atomRoot) < JUNCTION_PLAUSIBLE_DIST); 
		String newName = "j" + (i+1);
		junction.setName(newName);
		root.insertChild(junction);
	    }
	    // check if atom positions were moved:
	    assert origPos.distance(Object3DTools.collectByClassName(stemRootOrig, "RnaStem3D").get(0).getPosition()) < 0.001;
	    log.fine("First leaf vector at end of method: " + (Object3DTools.findFirstLeaf(stemRootOrig)));
	}
	else {
	    log.fine("no atoms were found in structure!");
	}
	log.fine("Quitting BranchDescriptorTools.generateJunctions (2)!");
	return root;
    }

    /** generates a set of StrandJunction objects given a root that contains stems */
    public static Object3D generateKissingLoops(Object3D stemRootOrig,
						String baseName,
						int branchDescriptorOffset,
						double rmsTolerance) {
	assert stemRootOrig != null;
	assert baseName != null;
	log.fine("Starting BranchDescriptorTools.generateKissingLoops (1)!");
	// log.fine("First position of read graph before starting generateJunctions: " + Object3DTools.findFirstLeaf(atomRoot));
	Object3D root = new SimpleObject3D();
	root.setName(baseName);
	Object3DSet stemSet = Object3DTools.collectByClassName(stemRootOrig, "RnaStem3D");
	assert stemSet.size() > 0;
	if (stemSet.size() > 0) {
	    Object3D stemRoot = (Object3D)(stemRootOrig.cloneDeep());
	    Object3DSet kissingLoops = generateKissingLoops(stemRoot, branchDescriptorOffset, rmsTolerance);
	    if (kissingLoops.size() == 0) {
		log.fine("no kissing loops were found in structure!");
	    }
	    for (int i = 0; i < kissingLoops.size(); ++i) {
		Object3D kissingLoop = kissingLoops.get(i);
		log.fine("Generated kissingLoop " + (i+1) + " with position: " + kissingLoop.getPosition());
		// 		if (Object3DSetTools.distanceMin(kissingLoop, atomRoot) >= JUNCTION_PLAUSIBLE_DIST) {
		// 		    log.info("Warning: Object3DSetTools.distanceMin too far: "
		// 				       + kissingLoop.getPosition() + " " + Object3DSetTools.distanceMin(kissingLoop, atomRoot));
		// 		}
		// 		assert (Object3DSetTools.distanceMin(kissingLoop, atomRoot) < JUNCTION_PLAUSIBLE_DIST); 
		String newName = "k" + (i+1);
		kissingLoop.setName(newName);
		root.insertChild(kissingLoop);
	    }
	    // log.fine("First leaf vector at end of method: " + (Object3DTools.findFirstLeaf(stemRootOrig)));
	}
	else {
	    log.fine("no atoms were found in structure!");
	}
	log.fine("Quitting BranchDescriptorTools.generateKissingLoops (1)!");
	return root;
    }
    //with inthelixCheck option
   public static Object3D generateKissingLoops(Object3D stemRootOrig,
						String baseName,
						int branchDescriptorOffset,
					       double rmsTolerance, boolean intHelixCheck) {
	assert stemRootOrig != null;
	assert baseName != null;
	log.fine("Starting BranchDescriptorTools.generateKissingLoops (1)!");
	// log.fine("First position of read graph before starting generateJunctions: " + Object3DTools.findFirstLeaf(atomRoot));
	Object3D root = new SimpleObject3D();
	root.setName(baseName);
	Object3DSet stemSet = Object3DTools.collectByClassName(stemRootOrig, "RnaStem3D");
	assert stemSet.size() > 0;
	if (stemSet.size() > 0) {
	    Object3D stemRoot = (Object3D)(stemRootOrig.cloneDeep());
	    Object3DSet kissingLoops = generateKissingLoops(stemRoot, branchDescriptorOffset, rmsTolerance, intHelixCheck);
	    if (kissingLoops.size() == 0) {
		log.fine("no kissing loops were found in structure!");
	    }
	    for (int i = 0; i < kissingLoops.size(); ++i) {
		Object3D kissingLoop = kissingLoops.get(i);
		log.fine("Generated kissingLoop " + (i+1) + " with position: " + kissingLoop.getPosition());
		// 		if (Object3DSetTools.distanceMin(kissingLoop, atomRoot) >= JUNCTION_PLAUSIBLE_DIST) {
		// 		    log.info("Warning: Object3DSetTools.distanceMin too far: "
		// 				       + kissingLoop.getPosition() + " " + Object3DSetTools.distanceMin(kissingLoop, atomRoot));
		// 		}
		// 		assert (Object3DSetTools.distanceMin(kissingLoop, atomRoot) < JUNCTION_PLAUSIBLE_DIST); 
		String newName = "k" + (i+1);
		kissingLoop.setName(newName);
		root.insertChild(kissingLoop);
	    }
	    // log.fine("First leaf vector at end of method: " + (Object3DTools.findFirstLeaf(stemRootOrig)));
	}
	else {
	    log.fine("no atoms were found in structure!");
	}
	log.fine("Quitting BranchDescriptorTools.generateKissingLoops (1)!");
	return root;
    }

}

