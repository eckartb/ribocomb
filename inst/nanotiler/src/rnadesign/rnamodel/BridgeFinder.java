package rnadesign.rnamodel;

import java.util.List;
import tools3d.objects3d.*;

/** Interface for classes that find 3D bridges between two objects */
public interface BridgeFinder {

    List<Object3DLinkSetBundle> findBridge(Object3D obj1, Object3D obj2);

    int getLenMin();

    int getLenMax();

    double getRms();

    void setRms(double rms);

    void setAngleWeight(double weight);

    void setLenMin(int n);

    void setLenMax(int n);

    void setSolutionMax(int n);

}
