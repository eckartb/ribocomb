package rnadesign.rnamodel;

import java.io.*;
import java.util.*;
import java.util.logging.*;
import tools3d.*;
import tools3d.objects3d.*;
import generaltools.*;

import static rnadesign.rnamodel.BranchDescriptorOptimizerFactory.MONTE_CARLO_OPTIMIZER;
import static rnadesign.rnamodel.HelixOptimizerTools.GENERATED_HELICES;

public class BuildingBlockRandomGrower {

    public boolean forbidRinglessCollisionMode = false;

    public static final double HELIX_INITIAL_SCORE = 1e8; // maxium initial score for building block placement

    public static int HELIX_LENGTH_MAX = 10; // can be changed!

    public static final String ROOT_DEFAULT_NAME = "root";

    private static final String FOUND_AGAIN = "found_again"; // property name indicating if a building block was already placed

    private static final String DB_ELEMENT = "db_element";

    // private static final double ATOM_COLLISION_DISTANCE = RnaConstants.ATOM_COLLISION_DISTANCE;

    private double atomCollisionDistance = 3.5;

    private int junctionCoreOffset = 2; // collisions of two outermost base pairs are not counted.

    private final StrandJunctionDB junctionDB;

    private final StrandJunctionDB kissingLoopDB;

    private final StrandJunctionDB motifDB;

    private final Object3D nucleotideDB;

    public static final double RING_NOT_CLOSED_SCORE = 1000.0; // used to indicate failed ring closure

    public boolean addStemsFlag = true;

    public static boolean debugMode = false;

    public boolean avoidCollisionsMode = true;

    private boolean firstFixedMode = true; // keep first building block fixed in space

    private int fuseStrandsMode = HelixOptimizerTools.NO_FUSE; 

    private double ringClosureExportLimit = 10.0;

    private static Logger log = Logger.getLogger(PackageConstants.LOGFILE_DEFAULT);

    private boolean randomMode = false;

    private static Random rnd = new Random();

    private double stemRmsLimit = 5.0;

    private double stemAngleLimit = Math.PI;

    private JunctionGraph3D targetGraph; // target structure to be designed!

    private Object3D root;

    private LinkSet links;

    public BuildingBlockRandomGrower(StrandJunctionDB junctionDB, StrandJunctionDB kissingLoopDB, StrandJunctionDB motifDB, Object3D nucleotideDB) {
	this.junctionDB = junctionDB;
	this.kissingLoopDB = kissingLoopDB;
	this.motifDB = motifDB;
	this.nucleotideDB = nucleotideDB;
	init();
    }

    public void clear() {
	init();
    }
    
    /** Returns root node which might contain added structures */
    public Object3D getRoot() { return root; }

    public void init() {
	root = new SimpleObject3D();
	root.setName(ROOT_DEFAULT_NAME);
	links = new SimpleLinkSet();
    }

    /** extracts db element descriptor, describing from which database entry the object came from */
    private DBElementDescriptor extractDBElementDescriptor(Object3D obj) {
	String s = obj.getProperty("db_element");
	if (s == null) {
	    return null;
	}
	return DBElementDescriptor.parse(s);
    }

    public Object3DLinkSetBundle getBundle() { 
	return new SimpleObject3DLinkSetBundle(root, links);
    }

    public boolean isRandomMode() { return randomMode; }

    /** returns maximum limit for stem fitting (useful for "graph mode") */
    public double getStemRmsLimit() { return stemRmsLimit; }

    public JunctionGraph3D getTargetGraph() { return targetGraph; }

    /** Returns reference to an equivalent building block that was already placed.
     * TODO linear search is not efficient; however this should not be performance bottleneck. */
    private StrandJunction3D findPlacedJunction(List<List<Object3D> > placedBlocks,
						DBElementDescriptor dbe) {
	assert dbe != null;
	String dbes = dbe.toString();
	assert dbes != null && dbes.length() > 0;
	for (List<Object3D> row : placedBlocks) {
	    for (Object3D obj : row) {
		if (obj instanceof StrandJunction3D) {
		    if (dbes.equals(obj.getProperty(DB_ELEMENT))) {
			return (StrandJunction3D)obj;
		    }
		}
	    }
	}
	return null; // nothing found !
    }

    /** places cloned kissing loop of order order with index n to current graph, sets it at a certain position */
    public StrandJunction3D placeKissingLoop(DBElementDescriptor dbe,
					     String rootName, String newName, Vector3D position,
					     String strandEnding,
					     List<List<Object3D> > placedBlocks,
					     boolean graphFlag) throws FittingException {
	assert dbe != null;
	int n = dbe.getId();
	StrandJunction3D dbJunction = kissingLoopDB.getJunction(dbe.getOrder(), n);
	if (dbJunction == null) {
	    throw new FittingException("No junction of order " + dbe.getOrder() + " and id " 
				       + (n+1) + " is currently loaded.");
	}
	assert StrandJunctionTools.isHelixEndFree(dbJunction, "(hxend)1");
	StrandJunction3D junction = null;
	if (graphFlag) {
	    junction = findPlacedJunction(placedBlocks, dbe);
	}
	if (junction == null) {
	    junction = (StrandJunction3D)(dbJunction.cloneDeep());
	    junction.setPosition(position);
	    junction.setName(newName);
	    if ((strandEnding != null) && (strandEnding.length() > 0)) {
		// modify names of rna strands to avoid duplicate strand names:
		Set<String> allowedNames = new HashSet<String>();
		allowedNames.add("RnaStrand");
		Set<String> forbiddenNames = new HashSet<String>();
		Object3DTools.addEnding(junction, strandEnding, allowedNames, forbiddenNames);
	    }
	    root.insertChild(junction);
	}
	else {
	    junction.setProperty(FOUND_AGAIN, "true");
	}
	// graph.addGraph(junction, rootName);
	// modelChanged(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED));
	// ModelChangeEvent event = new ModelChangeEvent(this, eventConst.MODEL_MODIFIED);
	// 	graph.fireModelChanged(event);
	// links.fireModelChanged(event);
	return junction;
    }

    /** places cloned junction of order order with index n to current graph, sets it at a certain position */
    public StrandJunction3D placeJunction(DBElementDescriptor dbe,
					  String rootName, 
					  String newName, 
					  Vector3D position,
					  String strandEnding,
					  List<List<Object3D> > placedBlocks,
					  boolean graphFlag) throws FittingException {
	int order = dbe.getOrder();
	int n = dbe.getId();
	StrandJunction3D dbJunction = junctionDB.getJunction(dbe.getOrder(), dbe.getId());
	if (dbJunction == null) {
	    throw new FittingException("No junction of order " + (order) + " and id " 
				       + (n+1) + " is currently loaded.");
	}
	assert StrandJunctionTools.isHelixEndFree(dbJunction, "(hxend)1");
	StrandJunction3D junction = null;
	if (graphFlag) {
	    junction = findPlacedJunction(placedBlocks, dbe); // if already placed, return this block
	}
	if (junction == null) {
	    junction = (StrandJunction3D)(dbJunction.cloneDeep());
	    junction.setPosition(position);
	    junction.setName(newName);
	    if ((strandEnding != null) && (strandEnding.length() > 0)) {
		// modify names of rna strands to avoid duplicate strand names:
		Set<String> allowedNames = new HashSet<String>();
		allowedNames.add("RnaStrand");
		Set<String> forbiddenNames = new HashSet<String>();
		Object3DTools.addEnding(junction, strandEnding, allowedNames, forbiddenNames);
	    }
	    root.insertChild(junction);
	}
	else {
	    junction.setProperty(FOUND_AGAIN, "true");
	}
	// graph.addGraph(junction, rootName);
	// fire change events later such that events are pooled
	// modelChanged(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED));
	// 	ModelChangeEvent event = new ModelChangeEvent(this, eventConst.MODEL_MODIFIED);
	// 	graph.fireModelChanged(event);
	// 	links.fireModelChanged(event); 
	return junction;
    }

    /** places cloned junction of order order with index n to current graph, sets it at a certain position */
    public StrandJunction3D placeMotif(DBElementDescriptor dbe,
				       String rootName, 
				       String newName, 
				       Vector3D position,
				       String strandEnding,
				       List<List<Object3D> > placedBlocks,
				       boolean graphFlag) throws FittingException {
	int order = dbe.getOrder();
	int n = dbe.getId();
	StrandJunction3D dbJunction = motifDB.getJunction(dbe.getOrder(), dbe.getId());
	if (dbJunction == null) {
	    throw new FittingException("No motif of order " + (order) + " and id " 
				       + (n+1) + " is currently loaded.");
	}
	assert StrandJunctionTools.isHelixEndFree(dbJunction, "(hxend)1");
	StrandJunction3D junction = null;
	if (graphFlag) {
	    junction = findPlacedJunction(placedBlocks, dbe); // if already placed, return this block
	}
	if (junction == null) {
	    junction = (StrandJunction3D)(dbJunction.cloneDeep());
	    junction.setPosition(position);
	    junction.setName(newName);
	    if ((strandEnding != null) && (strandEnding.length() > 0)) {
		// modify names of rna strands to avoid duplicate strand names:
		Set<String> allowedNames = new HashSet<String>();
		allowedNames.add("RnaStrand");
		Set<String> forbiddenNames = new HashSet<String>();
		Object3DTools.addEnding(junction, strandEnding, allowedNames, forbiddenNames);
	    }
	    root.insertChild(junction);
	}
	else {
	    junction.setProperty(FOUND_AGAIN, "true");
	}
	// graph.addGraph(junction, rootName);
	// fire change events later such that events are pooled
	// modelChanged(new ModelChangeEvent(this, eventConst.MODEL_MODIFIED));
	// 	ModelChangeEvent event = new ModelChangeEvent(this, eventConst.MODEL_MODIFIED);
	// 	graph.fireModelChanged(event);
	// 	links.fireModelChanged(event); 
	return junction;
    }

    public StrandJunction3D placeBuildingBlock(DBElementDescriptor element, String rootName, String newName,
				       Vector3D position, String strandEnding,
				       List<List<Object3D> > placedBlocks,
				       boolean graphFlag) throws FittingException {
	StrandJunction3D result;
	switch (element.getType()) {
	case DBElementDescriptor.JUNCTION_TYPE:
	    result = placeJunction(element, rootName, newName, position, strandEnding,
				   placedBlocks, graphFlag);
	    break;
	case DBElementDescriptor.KISSING_LOOP_TYPE:
	    result = placeKissingLoop(element, rootName, newName, position, strandEnding,
				      placedBlocks, graphFlag);
	    break;
	case DBElementDescriptor.MOTIF_TYPE:
	    result = placeMotif(element, rootName, newName, position, strandEnding,
				placedBlocks, graphFlag);
	    break;
	default:
	    throw new FittingException("placeBuildingBlock: Database type not implemented: " + element);
	}
	assert result != null;
	result.setProperty("db_element", element.toString()); // store which database entry it came from
	return result;
    }

    /** returns true, if one of the junctions has at least one helix end that is not occupied yet */
    private boolean hasFreeHelixEnds(List<List<Object3D> > placedBlocks) {
	for (int i = 0; i < placedBlocks.size(); ++i) {
	    for (int j = 0; j < placedBlocks.get(i).size(); ++j) {
		if (StrandJunctionTools.hasFreeHelixEnds(placedBlocks.get(i).get(j))) {
		    return true;
		}
	    }
	}
	return false;
    }
    
    /** Returns coordinate system of first helix end descriptor. */
    private CoordinateSystem3D getBuildingBlockCoordinateSystem(Object3D block) {
	Object3D obj = block.getChild("(hxend)1"); // this notation used external counting: first child node of class branch descriptor is denoted as (BranchDescriptor3D)1 or (hxend)1
	if (obj == null) {
	    System.out.println("Problem: cannot find helix end descriptor in object " + block.getName());
	    for (int i = 0; i < block.size(); ++i) {
		System.out.println(block.getChild(i).getName() + " "
				   + block.getChild(i).getClassName());
	    }
	}
	assert obj != null;
	assert obj instanceof BranchDescriptor3D;
	BranchDescriptor3D b = (BranchDescriptor3D)obj;
	return (CoordinateSystem3D)(b.getCoordinateSystem());
    }

    /** stores representative orientation of building block */
    private void storeCoordinateSystem(Object3D block, DBElementDescriptor elementDescriptor,
				       List<CoordinateSystem3D> storedOrientations ) {
	CoordinateSystem3D cs = getBuildingBlockCoordinateSystem(block);
	cs.setProperty("db_element", elementDescriptor.toString());
	storedOrientations.add(cs);
	// log.fine("Storing coordinate system: " + cs + " to " + storedOrientations.size());
    }

    /** given two coordinate systems, how "costly" is it to superpose one onto the other? Answer here: 
     * linear combination between distance of two coordinate systems and the one rotation angle 
     * in an axis - angle representation 
     */
    private double computeRingClosureScore(CoordinateSystem3D cs,
					   DBElementDescriptor dbe,
					   CoordinateSystem3D storedOrientation,
					   double distWeight,
					   double angleWeight) {
	assert dbe != null;
	assert cs != null;
	assert storedOrientation != null;
	String prop = storedOrientation.getProperty("db_element");
	assert prop != null;
	if (!dbe.equals(DBElementDescriptor.parse(prop))) {
	    return RING_NOT_CLOSED_SCORE;
	}
	double dist = cs.distance(storedOrientation);
	CoordinateSystem3D storedClone = (CoordinateSystem3D)(storedOrientation.cloneDeep());
	storedClone.passiveTransform3(cs);
	double angle = AxisAngle.toAxisAngle(new Matrix3D(storedClone.generateMatrix4D())).getAngle();
	double score = (distWeight * dist) + (angleWeight * angle);
// 	log.info("Score of " + cs + " " + cs.getProperties() + " " + dbe + " compared to " 
// 		 + storedOrientation + " " + storedOrientation.getProperties() + " score: " + score);
	if (score > RING_NOT_CLOSED_SCORE) {
	    score = RING_NOT_CLOSED_SCORE;
	}
	return score;
    }

    /** for a given building block (and its local coordinate system), find another coordinate system that closes the ring.
     * Returns -1 if no ring closing element found. */
    private int findRingClosureIndex(Object3D block,
				     DBElementDescriptor dbe,
				     List<CoordinateSystem3D> storedOrientations,
				     double distWeight,
				     double angleWeight) {
	double bestScore = RING_NOT_CLOSED_SCORE;
	CoordinateSystem3D cs = getBuildingBlockCoordinateSystem(block);
	int bestId = -1;
	for (int i = 0; i < storedOrientations.size(); ++i) {
	    double score = computeRingClosureScore(cs, dbe, storedOrientations.get(i), distWeight, angleWeight);
	    if (score < bestScore) {
		bestScore = score;
		bestId = i;
	    }
	}
	return bestId;
    }

    /** export current scene graph to pdb */
    private void exportPdb(String fileName) throws IOException {
	if (!fileName.endsWith(".pdb")) {
	    fileName += ".pdb";
	}
	FileOutputStream fos = new FileOutputStream(fileName);    
	int format = 0;
	GeneralPdbWriter writer = new GeneralPdbWriter();
	writer.write(fos, root);
	// graph.write(os, writer);
	// writer.write(os, links);
	fos.close();
    }

    /** export current scene graph to pdb */
    private void exportPdbNoHelices(String fileName) throws IOException {
	if (!fileName.endsWith(".pdb")) {
	    fileName += ".pdb";
	}
	Object3DSet helixStrands = RnaStrandTools.findPureHelicalStrands(root, links);
	log.info("Writing PDB structure to file " + fileName + " excluding " + helixStrands.size() + " helical strands.");
	Object3DTester forbiddenStrandTester = new ForbiddenObject3DTester(helixStrands.getAsList());
	FileOutputStream fos = new FileOutputStream(fileName);    
	int format = 0;
	GeneralPdbWriter writer = new GeneralPdbWriter();
	writer.setObjectTester(forbiddenStrandTester);
	writer.write(fos, root);
	// graph.write(os, writer);
	// writer.write(os, links);
	fos.close();
    }

    /** Write out all properties objects of building block generations */
    void printBuildStatus(PrintStream ps, List<List<Object3D> > placedBlocks) {
	for (int i = 0; i < placedBlocks.size(); ++i) {
	    ps.print("Generation " + (i+1) + " ");
	    for (int j = 0; j < placedBlocks.get(i).size(); ++j) {
		ps.print(placedBlocks.get(i).get(j).getFullName() + " : " + placedBlocks.get(i).get(j).getProperties());
	    }
	    ps.println();
	}
    }

    /** Returns all atoms but not the outermost one add helix ends */
    private Object3DSet extractCoreAtoms(BioPolymer strand, int offset) {
	assert strand != null;
	assert offset >= 0;
	Object3DSet result = new SimpleObject3DSet();
	for (int i = offset; (i+offset) < strand.getResidueCount(); ++i) {
	    result.merge(Object3DTools.collectByClassName(strand.getResidue3D(i), "Atom3D")); // extract all atoms
	}
	return result;
    }

    /** Returns all atoms but not the outermost one add helix ends */
    private Object3DSet extractCoreAtoms(StrandJunction3D junction, int offset) {
	assert junction != null;
	assert offset >= 0;
	Object3DSet result = new SimpleObject3DSet();
	for (int i = 0; i < junction.getStrandCount(); ++i) {
	    result.merge(extractCoreAtoms(junction.getStrand(i), offset));
	}
	assert result != null;
	return result;
    }

    /** Returns all atoms but not the outermost one add helix ends */
    private Object3DSet extractStrandCoreAtoms(Object3D obj, int offset) {
	assert obj != null;
	assert offset >= 0;
	Object3DSet strands = Object3DTools.collectByClassName(obj, "RnaStrand");
	assert strands.size() > 0;
	Object3DSet result = new SimpleObject3DSet();
	for (int i = 0; i < strands.size(); ++i) {
	    result.merge(extractCoreAtoms((RnaStrand)(strands.get(i)), offset));
	}
	assert result != null;
	return result;
    }

    String[] findNewElements(String[] words1, String[] words2) {
	if ((words1 == null) || (words1.length == 0)) {
	    return words2;
	}
	int lenDiff = words2.length - words1.length;
	String[] result = new String[lenDiff];
	for (int i = words1.length; i < words2.length; ++i) {
	    result[i - words1.length] = words2[i];
	}
	return result;
    }

    /** counts atom clashes between newBlock and root ignoring subtrees of root specified in avoidSet */
    private int countExternalCollisions(Object3DSet newAtoms, Object3D newBlock, Object3D root, Object3DSet avoidSet, double atomCollisionDistance) {
	int result = AtomTools.countExternalCollisions(newAtoms, newBlock, root,
						 avoidSet, atomCollisionDistance);
	log.fine("Counting collisions between " + newAtoms.size() + " and " + Object3DTools.collectByClassName(root, "Atom3D").size() + " atoms: "
		 + result);
	return result;
    }

    /** given a list of building blocks and a list of connections, grow until collisions are unavoidable
     * @param placedBlock building block to grow from 
     * @param placedBlocks building blocks placed so far. Only needed for graph flag; in this mode each building block can only be placed once
     * @param blockList building blocks to choose from
     * @param connections building block connections
     * @param storedOrientations list of orientation of first branch descriptor of each placed building block
     * @param rootName name base for building blocks; not consistently used, currently only for helices 
     * @param blockCounter counts building blocks
     * @param exportPdbFileNameBase output PDB file name
     * @param tabooSet list of building blocks to be ignored in collision check
     * @param delayStage only grow building block connections whose delaystage is lower or equal to this number
     * @param graphFlag if true, each building block can only be placed once
     */
    private Object3DSet growBuildingBlock(Object3D placedBlock,
					  List<List<Object3D> > placedBlocks, // only needed for graph flag
					  List<DBElementDescriptor> blockList,
					  List<CoordinateSystem3D> storedOrientations, 
					  String rootName,
					  String nameBase,
					  String exportPdbFileNameBase,
					  Object3DSet tabooSet,
					  int delayStage,
					  boolean graphFlag) throws FittingException {
	assert placedBlock != null;
	assert placedBlock instanceof StrandJunction3D;
	log.fine("Starting growBuildingBlock core with "
		 + " rootname " + rootName + " name base " + nameBase + " graph=" + graphFlag 
		 + " ; so far placed generations: " + placedBlocks.size() + " using placed block: " + placedBlock.getFullName() 
		 + " delay=" + delayStage);
	// + " blockCounter=" + blockCounter 
	if (debugMode) {
	    printBuildStatus(System.out, placedBlocks);
	}
	if (blockList.size() == 0) {
	    throw new FittingException("No building blocks defined!");
	}
	int blockCounter = 0; // new blocks placed so far
	int placementAttempts = 0;
	int collisionCountTot = 0;
	double ringDistWeight = 1.0;
	double ringAngleWeight = 0.5 * Math.toDegrees(1.0); // a 10 degree orientation error is treated like a 5A distance TODO !?
	Object3DSet result = new SimpleObject3DSet();
	String helixPrefix = "h_";
	// place seed building block:
	// List<Object3D> newBlockList;
	DBElementDescriptor element = extractDBElementDescriptor(placedBlock);
	assert element != null;
	log.fine("Descriptor of placed block: " + element + " Properties: " + placedBlock.getProperties());
	int ringlessCollision = 0;
	FitParameters stemFitParameters = null;
	double bestRingClosureScore = RING_NOT_CLOSED_SCORE;
	int ringCounter = 0;
	if (addStemsFlag) {
	    stemFitParameters = new FitParameters(stemRmsLimit, stemAngleLimit); // default parameters are ok, because stems are positioned precisely
	}
	String connectionNames = "";
	assert element != null;
	int blockOrder = element.getOrder(); // number of helices from which something can be extended
	int PLACEMENT_ATTEMPTS_MAX = 50;
	StrandJunction3D[] j2v = junctionDB.getJunctions(2);    
	StrandJunction3D[] j3v = junctionDB.getJunctions(3);    
	// for (int i = 0; i < connections.size(); ++i) {
	List<Integer> freeHelices = new ArrayList<Integer>();
	for (int i = 0; i < blockOrder; ++i) {
	    if (StrandJunctionTools.isHelixEndFree((StrandJunction3D)placedBlock, i)) {
		freeHelices.add(i);
	    }
	}
	Collections.shuffle(freeHelices); 

	for (int hi = 0; hi < freeHelices.size(); ++hi) { // try to place new block at this helix
	    // while (fre && (placementAttempts++ < PLACEMENT_ATTEMPTS_MAX) ) {
	    int hid = freeHelices.get(hi);
	    List<DBElementConnectionDescriptor> connections = new ArrayList<DBElementConnectionDescriptor>();
	    // TODO : create one single randomized connection descriptor
	    int helixId1 = hid;
	    int basePairCount = rnd.nextInt(HELIX_LENGTH_MAX);
	    int descriptorId = rnd.nextInt(); // pick random numbe
	    DBElementDescriptor descriptor1 = element; // current block
	    int j2id = rnd.nextInt(j2v.length); 	    // now find random block from list of 2-way junctions
	    DBElementDescriptor descriptor2 = new DBElementDescriptor(2, j2id, DBElementDescriptor.JUNCTION_TYPE, descriptorId);
	    int helixId2 = rnd.nextInt(2); // pick random helix
	    DBElementConnectionDescriptor conn = new DBElementConnectionDescriptor(descriptor1, descriptor2, helixId1, helixId2, basePairCount);
	    connections.add(conn);
	    int i = 0;
	    if (connections.get(i).getDelayStage() > delayStage) {
		log.fine("Delaying connection " + (i+1) + " : " + connections.get(i));
		continue;
	    }
	    log.fine("Testing connection " + (i+1) + " : " + connections.get(i));
	    if (connections.get(i).getDescriptor1().equals(element)) {
		log.fine("" + (i+1) + " Found hit for first descriptor of connection: " + connections.get(i));
		String ending = "b" + (blockCounter+1);
		String name = nameBase + "_" + ending;
		String helixendNamePlaced = connections.get(i).getHelixendName1();
		if (StrandJunctionTools.isHelixEndFree(placedBlock, helixendNamePlaced)) { // check if helix end is alreay used
		    // new version of place building block is smart: if graph flag is true
		    // it might return a building block that was already placed before!
		    StrandJunction3D newBlock = placeBuildingBlock(connections.get(i).getDescriptor2(), rootName, name, Vector3D.ZVEC, "_" + ending, placedBlocks, graphFlag);
		    assert newBlock != null;
		    Object3D b1 = placedBlock.getChild(helixendNamePlaced);
		    if ((b1 == null) || (!(b1 instanceof BranchDescriptor3D))) {
			throw new FittingException("Illegal helix end descriptor name: " + connections.get(i).getHelixendName1());
		    }
		    BranchDescriptor3D bd1 = (BranchDescriptor3D)b1;
		    String helixendNameNew = connections.get(i).getHelixendName2();
		    if (!StrandJunctionTools.isHelixEndFree(newBlock, helixendNameNew)) {
			throw new FittingException("Strange error in building block grower: helix of building block was unexpectantly occupied! Check graph definition: " + newBlock.getFullName());
		    }
		    Object3D b2 = newBlock.getChild(helixendNameNew);
		    if ((b2 == null) || (!(b2 instanceof BranchDescriptor3D))) {
			throw new FittingException("Illegal helix end descriptor name: " + connections.get(i).getHelixendName2());
		    }
		    BranchDescriptor3D bd2 = (BranchDescriptor3D)b2;
		    double rms = 1.0; // dummy value
		    HelixConstraintLink constraintLink = new SimpleHelixConstraintLink(bd1, bd2, 
			       connections.get(i).getBasePairCount(), connections.get(i).getBasePairCount(), rms);
		    Properties optProperties = new Properties();
		    if (!"true".equals(newBlock.getProperty(FOUND_AGAIN))) {
			// HelixConstraintLink constraintLink = links.addHelixConstraintLink(bd1, bd2, 
			// connections.get(i).getBasePairCount(), connections.get(i).getBasePairCount(), rms);
			links.add(constraintLink);
			// should be easy fit, because new junction:
			// String[] genHelixNamesOrig = PropertyTools.getIndividualProperties(optProperties, GENERATED_HELICES);
			optProperties = HelixOptimizerTools.optimizeHelices(10, 1.0, HELIX_INITIAL_SCORE, 10.0, stemFitParameters, null, null,
									    root, links, nucleotideDB, MONTE_CARLO_OPTIMIZER, fuseStrandsMode, firstFixedMode,0); // last zero: not helix length change
			links.remove(constraintLink); // remove again
		    }
		    else if (graphFlag) { // building block was already placed! Do not move, just check if helix possible
			// check if helix connection could be formed properly:
			double error = HelixOptimizer.computeBranchDescriptorError(bd1, bd2, constraintLink);
			// TODO : set optProperties, generate helix
			if (error > (2.0 * stemFitParameters.getRmsLimit())) {
			    throw new FittingException("Could not place helix from " + b1.getFullName() 
						       +  " to already placed building block " + b2.getFullName());
			}
			else {
			    optProperties = HelixOptimizerTools.generateHelix(constraintLink, BranchDescriptorOptimizerFactory.MORPH_OPTIMIZER,
						      stemFitParameters, nucleotideDB, root, links, fuseStrandsMode);
			    log.fine("Ring closing in graph mode(1): " + constraintLink + " " + optProperties);
			    StrandJunctionTools.setHelixEndUsed(placedBlock, helixendNamePlaced);
			    StrandJunctionTools.setHelixEndUsed(newBlock, helixendNameNew);
			    links.remove(constraintLink);
			}
		    }
		    else {
			assert false; // should never be here
		    }
		    // counts collision
		    int collisionCount = 0;
		    if (avoidCollisionsMode) {
			Object3DSet avoidSet = new SimpleObject3DSet();
			// avoidSet.add(placedBlock); // do not count collisions with previous block
 			String[] addedHelixNames = PropertyTools.getIndividualProperties(optProperties, GENERATED_HELICES);
 			if (addedHelixNames !=  null) {
			    log.fine("Checking " + addedHelixNames.length + " new helices for steric clashes.");
 			    for (int j = 0; j < addedHelixNames.length; ++j) {
 				String helixFullName = rootName + "." + addedHelixNames[j];
 				Object3D addedHelix = Object3DTools.findByFullName(root, helixFullName);
 				if (addedHelix == null) {
 				    log.severe("Internal error: could not find helix " + helixFullName);
 				    Set<String> forbiddenNames = new HashSet<String>();
				    forbiddenNames.add("Atom3D");
 				    Object3DTools.printFullNameTree(System.out, root, null, forbiddenNames);
 				}
				else {
				    assert addedHelix != null;
				    // there should be 2 strands below helix object:
				    assert Object3DTools.collectByClassName(addedHelix, "RnaStrand").size() == 2;
				    Object3DSet helixAtomCore = extractStrandCoreAtoms(addedHelix, junctionCoreOffset);
				    int collisionCountTmp = countExternalCollisions(helixAtomCore, addedHelix, root,
										    avoidSet, atomCollisionDistance);
				    if (collisionCountTmp > 0) {
					log.fine("Helix " + addedHelix.getFullName() + " has " + collisionCountTmp
						 + " collisions!");
				    }
				    else {
					log.fine("Helix " + addedHelix.getFullName() + " has no collisions!");
				    }
				    collisionCountTot += collisionCountTmp; // directly increase total counter, no removing of helices at the moment
				}
 			    }
 			}
			Object3DSet junctionAtomCore = extractCoreAtoms(newBlock, junctionCoreOffset);
			// collisionCount = AtomTools.countExternalCollisions(newBlock, root, avoidSet, ATOM_COLLISION_DISTANCE);
			assert junctionAtomCore.size() > 0;
			collisionCount += countExternalCollisions(junctionAtomCore, newBlock, root,
								 avoidSet, atomCollisionDistance);
		    }
		    boolean ringClosedFlag = false;
		    // finds coordinate system that comes closest to ring:
		    int ringCsIndex = -1;
		    if (!graphFlag) {
			ringCsIndex = findRingClosureIndex(newBlock, connections.get(i).getDescriptor2(),
							   storedOrientations, ringDistWeight, ringAngleWeight);
		    }
		    // compute and store ring closure score independent of the question if there are collisions!
		    double ringClosureScore = RING_NOT_CLOSED_SCORE;
		    if (ringCsIndex >= 0) {
			ringClosureScore = computeRingClosureScore(getBuildingBlockCoordinateSystem(newBlock), 
									  connections.get(i).getDescriptor2(),
									  storedOrientations.get(ringCsIndex), ringDistWeight, ringAngleWeight);
			if (ringClosureScore < bestRingClosureScore) {
			    bestRingClosureScore = ringClosureScore;
			}
			if (ringClosureScore <= ringClosureExportLimit) {
// 			    String filename = exportPdbFileNameBase + "_1.pdb"; // "_" + newBlock.getName() + 
// 			    // encourages overwriting of partially assembled structures with same parameters!
// 			    exportPdb(filename);
			    // adds link from previously placed block to other placed block that forms ring:
			    CoordinateSystem3D csRing = storedOrientations.get(ringCsIndex);
			    if ((csRing.getParent() == null) || (csRing.getParent().getParent() == null) 
				|| (! (csRing.getParent().getParent() instanceof StrandJunction3D))) {
				log.warning("Internal error: expect a junction to be in found ring structure");
			    }
			    else {
				StrandJunction3D ringJunction = (StrandJunction3D)(csRing.getParent().getParent());
				if (StrandJunctionTools.isHelixEndFree(ringJunction, helixendNameNew)) { // make sure that helix end of found new junction is set to "used"				
				    links.add(new SimpleLink(placedBlock, ringJunction));
				    ringClosedFlag = true;
				    StrandJunctionTools.setHelixEndUsed(placedBlock, helixendNamePlaced);
				    log.fine("Adding ring-closing link from " + Object3DTools.getFullName(placedBlock) 
					     + " to " + Object3DTools.getFullName(ringJunction));
				    StrandJunctionTools.setHelixEndUsed(ringJunction, helixendNameNew); // make sure that helix end of found new junction is set to "used"
				    ++ringCounter;
				}
				else {
				    log.warning("Strange: ring has already been closed concerning placed junction " 
						+ Object3DTools.getFullName(placedBlock) 
						+ " and ring junction " 
						+ Object3DTools.getFullName(ringJunction));
				}
			    }
			    // log.info("Export ring structure " + ringCounter + " to file name: " + filename);
			    tabooSet.add(newBlock); // do count newly added block in graph, it already is placed in ring befire
			}
		    }
		    if ((collisionCount == 0) && (!ringClosedFlag) && checkCompatibleWithTargetGraph()
			&& StrandJunctionTools.isHelixEndFree(placedBlock, helixendNamePlaced)) { // new check
			newBlock.setProperty("ring_closure_score", "" + ringClosureScore); 
			StrandJunctionTools.setHelixEndUsed(placedBlock, helixendNamePlaced);
			result.add(newBlock);
			links.add(new SimpleLink(placedBlock, newBlock));
			StrandJunctionTools.setHelixEndUsed(newBlock, helixendNameNew);
			storeCoordinateSystem(newBlock, connections.get(i).getDescriptor2(), storedOrientations); // store representative orientation
			++blockCounter; // keep this building block
			if (connectionNames.length() == 0) {
			    connectionNames = connections.get(i).toShortString("_");
			} else {
			    connectionNames = connectionNames + "_" + connections.get(i).toShortString("_");
			}
			assert connectionNames != null;
			log.fine("Properties of placed and new block: " + placedBlock.getFullName() + " : " + placedBlock.getProperties() + " ; " 
				 + newBlock.getFullName() + " : " + newBlock.getProperties());
		    }
		    else {
			log.fine("Removing new block because of collision or ring closure: " + newBlock.getFullName());
			if (!"true".equals(newBlock.getProperty(FOUND_AGAIN))) {
			    Object3DTools.remove(newBlock); // remove because of collisions; do not remove if in graph mode and block was previously placed
			    if (graphFlag) {
				throw new FittingException("Could not place block " + newBlock.getFullName() + " because of collisions!");
			    }
			}
			if (ringClosureScore > ringClosureExportLimit) { // no ring closure
			    ++ringlessCollision;
			    collisionCountTot += collisionCount;
			}
		    }
		}
		else {
		    log.fine("Helixend is already used: " + Object3DTools.getFullName(placedBlock) + " " + helixendNamePlaced + " " + placedBlock.getProperty("used_helixends"));
		}
	    }
	    // TODO : remove code duplication
	    if (connections.get(i).getDescriptor2().equals(element)) {
		log.fine("" + (i+1) + " Found hit for second descriptor of connection(2): " + connections.get(i));
		String ending = "b" + (blockCounter+1);
		String name = nameBase + "_" + ending;
		String helixendNamePlaced = connections.get(i).getHelixendName2();
		if (StrandJunctionTools.isHelixEndFree(placedBlock, helixendNamePlaced)) { // check if helix end is alreay used
		    StrandJunction3D newBlock = placeBuildingBlock(connections.get(i).getDescriptor1(), rootName, name, Vector3D.ZVEC, "_" + ending, placedBlocks, graphFlag);
		    Object3D b1 = placedBlock.getChild(connections.get(i).getHelixendName2());
		    if ((b1 == null) || (!(b1 instanceof BranchDescriptor3D))) {
			throw new FittingException("Illegal helix end descriptor name: " + connections.get(i).getHelixendName2());
		    }
		    BranchDescriptor3D bd1 = (BranchDescriptor3D)b1;
		    String helixendNameNew = connections.get(i).getHelixendName1();
		    log.fine("new block properties(2): " + newBlock.getFullName() + " " +newBlock.getProperties());
		    if (!StrandJunctionTools.isHelixEndFree(newBlock, helixendNameNew)) {
			throw new FittingException("Strange error in building block grower: helix of building block was unexpectantly occupied! Check graph definition: " + newBlock.getFullName());
		    }
		    assert StrandJunctionTools.isHelixEndFree(newBlock, helixendNameNew);
		    Object3D b2 = newBlock.getChild(helixendNameNew);
		    if ((b2 == null) || (!(b2 instanceof BranchDescriptor3D))) {
			throw new FittingException("Illegal helix end descriptor name: " + connections.get(i).getHelixendName1());
		    }
		    BranchDescriptor3D bd2 = (BranchDescriptor3D)b2;
		    double rms = 1.0; // dummy value
		    HelixConstraintLink constraintLink = new SimpleHelixConstraintLink(bd1, bd2, 
			      connections.get(i).getBasePairCount(), connections.get(i).getBasePairCount(), rms);
		    Properties optProperties = new Properties();
		    if (!"true".equals(newBlock.getProperty(FOUND_AGAIN))) {
			links.add(constraintLink);
			// 		    HelixConstraintLink constraintLink = links.addHelixConstraintLink(bd1, bd2, connections.get(i).getBasePairCount(),
// 										      connections.get(i).getBasePairCount(), 1.0);
			optProperties = HelixOptimizerTools.optimizeHelices(10, 1.0, HELIX_INITIAL_SCORE, 10.0, stemFitParameters, null, null,root, links, nucleotideDB,
									    MONTE_CARLO_OPTIMIZER, fuseStrandsMode, firstFixedMode, 0);
			links.remove(constraintLink); // remove again
		    }
		    else if (graphFlag) {
			// check if helix connection could be formed properly:
			double error = HelixOptimizer.computeBranchDescriptorError(bd1, bd2, constraintLink);
			assert false;
			if (error > (2.0 * stemFitParameters.getRmsLimit())) {
			    throw new FittingException("Could not place helix from " + b1.getFullName() 
						       +  " to already placed building block " + b2.getFullName());
			}
			else {
			    // log.info("Ring closing in graph mode(2): " + constraintLink);
			    optProperties = HelixOptimizerTools.generateHelix(constraintLink, BranchDescriptorOptimizerFactory.MORPH_OPTIMIZER,
						      stemFitParameters, nucleotideDB, root, links, fuseStrandsMode);
			    log.fine("Ring closing in graph mode(2): " + constraintLink + " " + optProperties);
			    StrandJunctionTools.setHelixEndUsed(placedBlock, helixendNamePlaced);
			    StrandJunctionTools.setHelixEndUsed(newBlock, helixendNameNew);
			    links.remove(constraintLink);
			}
		    }
		    else {
			assert false; // should never be here
		    }
		    int collisionCount = 0;
		    if (avoidCollisionsMode) {
			Object3DSet avoidSet = new SimpleObject3DSet();
 			// avoidSet.add(placedBlock); // do not count collisions with previous block
 			String[] addedHelixNames = PropertyTools.getIndividualProperties(optProperties, GENERATED_HELICES);
 			if (addedHelixNames !=  null) {
			    // collision check for all new helices
 			    for (int j = 0; j < addedHelixNames.length; ++j) {
 				String helixFullName = rootName + "." + addedHelixNames[j];
 				Object3D addedHelix = Object3DTools.findByFullName(root, helixFullName);
 				log.fine("Added helix with name " + helixFullName + " avoid set.(2)");
 				assert addedHelix != null;
 				// avoidSet.add(addedHelix);
 				if (addedHelix == null) {
 				    log.severe("Internal error: could not find helix " + helixFullName);
 				    Set<String> forbiddenNames = new HashSet<String>();
				    forbiddenNames.add("Atom3D");
 				    Object3DTools.printFullNameTree(System.out, root, null, forbiddenNames);
 				}
				else {
				    assert addedHelix != null;
				    // there should be 2 strands below helix object:
				    assert Object3DTools.collectByClassName(addedHelix, "RnaStrand").size() == 2;
				    Object3DSet helixAtomCore = extractStrandCoreAtoms(addedHelix, junctionCoreOffset);
				    int collisionCountTmp = countExternalCollisions(helixAtomCore, addedHelix, root,
										    avoidSet, atomCollisionDistance);
				    if (collisionCountTmp > 0) {
					log.fine("Helix " + addedHelix.getFullName() + " has " + collisionCountTmp
						 + " collisions!");
				    }
				    else {
					log.fine("Helix " + addedHelix.getFullName() + " has no collisions!");
				    }
				    collisionCountTot += collisionCountTmp; // directly increase total counter, no removing so far
				}
 			    }
 			}
			Object3DSet junctionAtomCore = extractCoreAtoms(newBlock, junctionCoreOffset);
			assert junctionAtomCore.size() > 0;
			collisionCount += countExternalCollisions(junctionAtomCore, newBlock, root,
								 avoidSet, atomCollisionDistance);
			// collisionCount = AtomTools.countExternalCollisions(newBlock, root, avoidSet, ATOM_COLLISION_DISTANCE);
		    }
		    // index of so far best stored orientation:
		    boolean ringClosedFlag = false;
		    int ringCsIndex = -1;
		    if (!graphFlag) { // check for only if graph mode is not switched on. In graph mode, junctions never get generated twice
			ringCsIndex = findRingClosureIndex(newBlock, connections.get(i).getDescriptor1(),
							   storedOrientations, ringDistWeight, ringAngleWeight);
		    }
		    double ringClosureScore = RING_NOT_CLOSED_SCORE;
		    if (ringCsIndex >= 0) {
			ringClosureScore = computeRingClosureScore(getBuildingBlockCoordinateSystem(newBlock), connections.get(i).getDescriptor1(),
									  storedOrientations.get(ringCsIndex), ringDistWeight, ringAngleWeight);
			if (ringClosureScore < bestRingClosureScore) {
			    bestRingClosureScore = ringClosureScore;
			}
			if (ringClosureScore <= ringClosureExportLimit) {
			    // String filename = exportPdbFileNameBase + "_2.pdb"; // + "_" + newBlock.getName() 
			    // encounrages overwriting of partially assembled structures!
			    // exportPdb(filename);
			    // adds link from previously placed block to other placed block that forms ring:
			    CoordinateSystem3D csRing = storedOrientations.get(ringCsIndex);
			    if ((csRing.getParent() == null) || (csRing.getParent().getParent() == null) 
				|| (! (csRing.getParent().getParent() instanceof StrandJunction3D))) {
				log.warning("Internal error: expect a junction to be in found ring structure");
			    }
			    else {
				StrandJunction3D ringJunction = (StrandJunction3D)(csRing.getParent().getParent());
				if (StrandJunctionTools.isHelixEndFree(ringJunction, helixendNameNew)) { // make sure that helix end of found new junction is set to "used"
				    links.add(new SimpleLink(placedBlock, ringJunction));
				    ringClosedFlag = true;
				    StrandJunctionTools.setHelixEndUsed(placedBlock, helixendNamePlaced);
				    log.fine("Adding ring-closing link from " + Object3DTools.getFullName(placedBlock) 
					     + " to " + Object3DTools.getFullName(ringJunction) + " (2)");
				    StrandJunctionTools.setHelixEndUsed(ringJunction, helixendNameNew); // make sure that helix end of found new junction is set to "used"
				    ++ringCounter; // TODO: problem: this code should be independent from exportPdbFileName etc
				}
				else {
				    log.warning("Strange: ring has already been closed concerning placed junction " 
						+ Object3DTools.getFullName(placedBlock) 
						+ " and ring junction " 
						+ Object3DTools.getFullName(ringJunction));
				}
			    }
			    // log.info("Export ring structure " + ringCounter + " to file name: " + filename);
			    // Object3DSet tabooSet = new SimpleObject3DSet();
			    tabooSet.add(newBlock); // do count newly added block in graph, it already is placed in ring before
			    // Object3DLinkSetBundle extractedGraph = StrandJunctionTools.extractJunctionGraph(graph.getGraph(), links, "graph");
			    // GraphWriter graphWriter = new GraphWriter();
			    // log.info("Extracted graph: " + graphWriter.toString(extractedGraph));
			}
		    }
		    if ((collisionCount == 0) && (!ringClosedFlag) && checkCompatibleWithTargetGraph()
			&& StrandJunctionTools.isHelixEndFree(placedBlock, helixendNamePlaced)) { // new check{
			newBlock.setProperty("ring_closure_score", "" + ringClosureScore);
			result.add(newBlock);
			links.add(new SimpleLink(placedBlock, newBlock));
			StrandJunctionTools.setHelixEndUsed(placedBlock, helixendNamePlaced);
			StrandJunctionTools.setHelixEndUsed(newBlock, helixendNameNew);
			storeCoordinateSystem(newBlock, connections.get(i).getDescriptor1(), storedOrientations); // store representative orientation
			if (connectionNames.length() == 0) {
			    connectionNames = connections.get(i).toShortString("_");
			} else {
			    connectionNames = connectionNames + "_" + connections.get(i).toShortString("_");
			}
			assert connectionNames != null;
			++blockCounter;
			log.fine("Properties of placed and new block (2): " + placedBlock.getFullName() + " : " + placedBlock.getProperties() + " ; " 
				 + newBlock.getFullName() + " : " + newBlock.getProperties());
		    }
		    else {
			log.fine("Removing new block because of collision (2)! " + newBlock.getFullName());
			if (!"true".equals(newBlock.getProperty(FOUND_AGAIN))) {
			    Object3DTools.remove(newBlock); // remove block again!
			    if (graphFlag) { // no removed blocks allowd in graph mode:
				throw new FittingException("Could not place block " + newBlock.getFullName() + " because of collisions!");
			    }
			}
			if (ringClosureScore > ringClosureExportLimit) { // no ring closure
			    ++ringlessCollision;
			    collisionCountTot += collisionCount;
			}
		    }
		}
		else {
		    log.fine("Helixend is already used (2): " + Object3DTools.getFullName(placedBlock) + " " + helixendNamePlaced + " " + placedBlock.getProperty("used_helixends"));
		}
	    }
	}
	result.setProperty("ring_closure_score", "" + bestRingClosureScore);
	result.setProperty("ring_count", ""+ringCounter);
	result.setProperty("ringless_collisions", "" + ringlessCollision);
	result.setProperty("connection_names", connectionNames);
	PropertyTools.plusProperty(result.getProperties(), "collision_count", collisionCountTot);
	log.fine("Finished growBuildingBlock core with building blocks: " + result.size());
	return result;
    }

    /** given a list of building blocks and a list of connections, grow for one generation until collisions are unavoidable */
    private Properties growBuildingBlocks(List<List<Object3D> > placedBlocks,
					  List<DBElementDescriptor> blockList,
					  List<DBElementConnectionDescriptor> connections,
					  List<CoordinateSystem3D> storedOrientations,
					  String rootName,
					  String nameBase,
					  String ringClosureExportFileNameBase,
					  Object3DSet tabooSet,
					  int delayStage, 
					  int sizeMax,
					  boolean graphFlag) throws FittingException {
	log.fine("Starting growBuildingBlocks with " + blockList.size() + " blocks, " + connections.size() + 
		 " connections, rootname " + rootName + " name base " + nameBase);
	if (blockList.size() == 0) {
	    throw new FittingException("No building blocks defined!");
	}
	Properties resultProperties = new Properties();
	resultProperties.setProperty("ringless_collisions", "0");
	int blockCounter = 0;
	for (int i = 0; i < placedBlocks.size(); ++i) {
	    blockCounter += placedBlocks.get(i).size();
	}
	int genNumber = placedBlocks.size(); // how many generations have been placed so far
	List<Object3D> newBlockList = new ArrayList<Object3D>();
	List<Object3D> lastPlacedList = placedBlocks.get(placedBlocks.size()-1);
	String name = nameBase + "_g" + genNumber;
	double bestRingClosureScore = RING_NOT_CLOSED_SCORE;
	int ringCountTot = 0;
	int collisionCountTot = 0;
	String connectionNames = "";
	for (int i = 0; i < lastPlacedList.size(); ++i) {
	    Object3DSet newObjects = growBuildingBlock(lastPlacedList.get(i), placedBlocks, blockList, storedOrientations, 
						       rootName, name,
						       ringClosureExportFileNameBase, tabooSet, delayStage, graphFlag);
	    assert newObjects != null;
	    connectionNames = connectionNames + "h" + newObjects.getProperty("connection_names");
	    collisionCountTot += PropertyTools.parseInt(newObjects.getProperties(), "collision_count");
	    String ringlessCollisionString = newObjects.getProperty("ringless_collisions");
	    assert ringlessCollisionString != null;
	    if (forbidRinglessCollisionMode && (ringlessCollisionString != null) && (!ringlessCollisionString.equals("0"))) {
		System.out.println("Detecting ringless collision! Quitting loop!");
		resultProperties.setProperty("ringless_collisions", ringlessCollisionString);
		break;
	    }
	    String ringClosureScoreText = newObjects.getProperty("ring_closure_score");
	    assert ringClosureScoreText != null;
	    double ringClosureScore = Double.parseDouble(ringClosureScoreText);
	    ringCountTot += PropertyTools.parseInt(newObjects.getProperties(), "ring_count");
	    if (ringClosureScore < bestRingClosureScore) {
		bestRingClosureScore = ringClosureScore;
	    }

	    for (int j = 0; j < newObjects.size(); ++j) {
		newBlockList.add(newObjects.get(j));
	    }
	    blockCounter += newObjects.size();
	    if ((sizeMax > 0) && (blockCounter >= sizeMax)) {
		log.info("Quitting growth loop because maximum size exceeded!");
		break;
	    }
	}
	placedBlocks.add(newBlockList);
	resultProperties.setProperty("collision_count", "" + collisionCountTot);
	resultProperties.setProperty("placed_blocks", ""+blockCounter);
	resultProperties.setProperty("ring_count", "" + ringCountTot);
	resultProperties.setProperty("ring_closure_score", "" + bestRingClosureScore);
	log.fine("Finished growBuildingBlocks!");
	return resultProperties;
    }

    private int findDelayMaximum(List<DBElementConnectionDescriptor> connections) {
	int result = 0;
	for (int i = 0; i < connections.size(); ++i) {
	    if (connections.get(i).getDelayStage() > result) {
		result = connections.get(i).getDelayStage();
	    }
	}
	return result;
    }

    /** Returns true if topology is contained in allowed set */
    private boolean computeIsGraphAllowedTopology(Set<String> allowedTopologies, String topology) {
	if ((allowedTopologies == null) || (allowedTopologies.size() == 0) || allowedTopologies.contains(topology)) {
	    return true;
	}
	return false;
    }

    /** Returns number of building blocks */
    public static int extractTopologySize(String topology) {
	String[] words = topology.split(";");
	assert words.length > 0;
	String connections = words[words.length-1].replace(">", ""); // last with last word, remove ">" characters
	String[] pairs = connections.split("%");
	Set<Integer> blocks = new HashSet<Integer>();
	for (String s: pairs) {
	    String[] pair = s.split("-");
	    if (pair.length == 2) {
		try {
		    blocks.add(new Integer(Integer.parseInt(pair[0])));
		}
		catch (NumberFormatException nfe) {
		    log.severe("Internal parsing error at word " + s + " for topology " + topology);
		    assert false;
		}
	    }
	}
	return blocks.size();
    }

    /** Returns maximum size (number of building blocks) of topologies */
    public static int extractTopologyMaxSize(Set<String> allowedTopologies, int sizeMax) {
	if ((allowedTopologies == null) || (allowedTopologies.size() == 0)) {
	    return sizeMax;
	}
	int bestSize = 0;
	for (String s : allowedTopologies) {
	    int n = extractTopologySize(s);
	    if (n  > bestSize) {
		bestSize = n;
	    }
	}
	if ((sizeMax > 0) && (sizeMax < bestSize)) {
	    bestSize = sizeMax;
	}
	return bestSize;
    }

    /** given a list of building blocks and a list of connections, grow until collisions are unavoidable */
    public Properties growBuildingBlocks(List<DBElementDescriptor> blockList,
					 List<DBElementConnectionDescriptor> connections,
					 String rootName,
					 String nameBase,
					 Vector3D startPosition,
					 int numGenerations,
					 String ringClosureExportFileNameBase,
					 Set<String> allowedTopologies,
					 int sizeMax,
					 boolean graphFlag) throws FittingException, IOException {
	log.info("Starting growBuildingBlocks with " + blockList.size() + " blocks, " + connections.size() + 
		 " connections, rootname " + rootName + " name base " + nameBase + " and start position " + startPosition);
	if (blockList.size() == 0) {
	    throw new FittingException("No building blocks defined!");
	}
	Properties resultProperties = new Properties();
	resultProperties.setProperty("ringless_collisions", "0");
	// place seed building block:
	int blockCounter = 1;
	String newName = nameBase + "_seed";
	String fullName = rootName + "." + newName;
	List<List<Object3D> > placedBlocks = new ArrayList<List<Object3D> >();
	Object3D firstBlock = placeBuildingBlock(blockList.get(0), rootName, newName, startPosition, "_b0",
						 placedBlocks, graphFlag);
	log.fine("Properties of first placed block: " + firstBlock.getProperties());
	assert firstBlock.getProperty("used_helixends") == null; // should not be set, it comes from clean database entry
	Properties tmpProperties = new Properties();

	placedBlocks.add(new ArrayList<Object3D>());
	placedBlocks.get(0).add(firstBlock);
	List<CoordinateSystem3D> storedOrientations = new ArrayList<CoordinateSystem3D>();
	Object3DSet tabooSet = new SimpleObject3DSet(); // stores all overlapping junctions that for added for debugging purposes
	double bestRingClosureScore = RING_NOT_CLOSED_SCORE;
	int ringCountTot = 0;
	int delayMax = findDelayMaximum(connections); // find maximum of delay generations
	int collisionCountTot = 0;
	sizeMax = extractTopologyMaxSize(allowedTopologies, sizeMax); // maximum number of building blocks
	int delayStage = 0;
	int gen = 0; // variable that indicates how many generations were used in the growth process
	String connectionNames = "";
	for (delayStage = 0; delayStage <= delayMax; ++delayStage) {
	    for (gen = 1; gen <= numGenerations; ++gen) {
		log.fine("Building block grow generation: " + gen);
		if (randomMode) {
		    log.info("Shuffling connectivity order at grow-generation " + gen);
		    Collections.shuffle(connections);
		}
		tmpProperties = growBuildingBlocks(placedBlocks, blockList, connections, storedOrientations, 
						   rootName, nameBase, 
						   ringClosureExportFileNameBase, 
						   tabooSet, delayStage, sizeMax, graphFlag);
		connectionNames = connectionNames + "g" + tmpProperties.getProperty("connection_names");
		if (debugMode) {
		    log.info("Resulting properties after grow generation " + gen + " : " + tmpProperties);
		}
		String ringlessCollisionString = tmpProperties.getProperty("ringless_collisions");
		if (forbidRinglessCollisionMode &&(ringlessCollisionString!=null)&&(!ringlessCollisionString.equals("0"))) {
		    System.out.println("Detecting ringless collision! Quitting loop over generations!");
		    resultProperties.setProperty("ringless_collisions", ringlessCollisionString);
		    break;
		}
		collisionCountTot += PropertyTools.parseInt(tmpProperties, "collision_count");
		ringCountTot += PropertyTools.parseInt(tmpProperties, "ring_count");
		String ringClosureScoreText = tmpProperties.getProperty("ring_closure_score");
		assert ringClosureScoreText != null;
		double ringClosureScore = Double.parseDouble(ringClosureScoreText);
		if (ringClosureScore < bestRingClosureScore) {
		    bestRingClosureScore = ringClosureScore;
		}
		int numPlaced = placedBlocks.get(placedBlocks.size()-1).size();
		blockCounter += numPlaced;
		log.fine("Placed " + numPlaced + " building blocks in generation " + gen);
		if ( numPlaced == 0) {
		    log.fine("No further placements possible!");
		    break;
		}
	    }
	    if (ringCountTot == 0) {
		log.fine("Quitting delay loop, because no rings were found so far.");
		break;
	    }
	}
	// check if cage was generated:
	int cageCount = 0;
	if (!hasFreeHelixEnds(placedBlocks)) {
	    log.info("Finished structure (cage or ring) found!");
	    cageCount = 1;
	}
	String ringlessCollisionsString = resultProperties.getProperty("ringless_collisions");
	if (ringlessCollisionsString == null) {
	    ringlessCollisionsString = "0";
	}
	boolean ringlessCollisionFree = (ringlessCollisionsString==null)||(ringlessCollisionsString.equals("0"));
	Object3DLinkSetBundle extractedGraph = StrandJunctionTools.extractJunctionGraph(root, links, "graph", tabooSet);
	Object3DLinkSetBundleWriter graphWriter = new GraphWriter();
	log.info("Extracted graph: " + graphWriter.toString(extractedGraph));
	SignatureTranslatorCanonizer canonizer = new SignatureTranslatorCanonizer();
	String canonicString = "ntop"; // default name: no topology available
	try {
	    if (ringlessCollisionFree) {
		canonicString = canonizer.generateCanonizedRepresentation(extractedGraph);
	    }
	}
	catch (AlgorithmFailureException afe) {
	    log.warning("Cannot canonize graph: " + afe.getMessage());
	}
	// return true if no topologies specified or if current topology is found in allowedTopologies
	boolean graphCompatible = computeIsGraphAllowedTopology(allowedTopologies, canonicString); 
	// writes found ring-structures to file:

	if ((ringClosureExportLimit < 0.0) || ((ringCountTot > 0) && (ringClosureExportFileNameBase != null) && (!ringClosureExportFileNameBase.equals(""))
	    && (ringlessCollisionFree || (!forbidRinglessCollisionMode) ) ) ) {
	    if (graphCompatible) {
		if ((!avoidCollisionsMode) || (collisionCountTot == 0)) {
		    String filename = ringClosureExportFileNameBase ; // "_" + newBlock.getName() +
		    //		    int s = filename.lastIndexOf("/");
		    String sub1 = filename; // filename.substring(0,s+1);
		    //		    String sub2 = filename.substring(s+1);

// 		    for (int i = 0; i < placedBlocks.size(); i++) {
// 			List<Object3D> completed = placedBlocks.get(i);
// 			for (int j = 0; j < completed.size(); j++) {
// 			    Object3D currJunc = completed.get(j);
// 			    if ((i+j) > 0) sub1 = sub1 + "_";
// 			    sub1 = sub1  + currJunc.getFilename();
// 			}
// 		    }

		    for (int i = 0; i < blockList.size(); i++) {
			sub1 = sub1  + "b" + blockList.get(i).toStringCore("_") + "_";
		    }
		    if (randomMode) {
			assert connectionNames != null;
			sub1 = sub1 + connectionNames; // shows connection rules that were actually used
		    } else {
			for (int i = 0; i < connections.size(); i++) {
			    sub1 = sub1  + "c" + connections.get(i).toShortString("_") + "_";
			}
		    }
		    sub1 = sub1 + "d" + delayStage + "g" + gen; // save generation
		    filename = sub1; //  + ".pdb"; // sub2 "_" + newBlock.getName() +
		    filename = filename + canonicString + ".pdb";
		    // extract directory information
		    
                    // check if directory exists
 
                    // if directory does not exist, create it:

		    String filenameNoHelices = sub1 +  "_nohelices.pdb"; // sub2 + "_" + newBlock.getName() + 
		    log.info("Exporting ring structure to file names: " + filename + " " + filenameNoHelices);
		    // encourages overwriting of partially assembled structures with same parameters!
		    exportPdb(filename);
		    exportPdbNoHelices(filenameNoHelices);
		}
		else {
		    log.info("No exporting structure because of atomic clashes!");
		}
	    }
	    else {
		String inf = "Graph was not compatible with target topologies: " + canonicString + " : " + allowedTopologies;
		log.info(inf);
		throw new FittingException(inf);
	    }
	}
	resultProperties.setProperty("canonized", canonicString);
	resultProperties.setProperty("connection_names", connectionNames);
	resultProperties.setProperty("graph_compatible", "" + graphCompatible);
	resultProperties.setProperty("cage_count", "" + cageCount);
	resultProperties.setProperty("placed_blocks", "" + blockCounter);
	resultProperties.setProperty("ring_closure_score", "" + bestRingClosureScore);
	resultProperties.setProperty("ring_count", "" + ringCountTot);
	resultProperties.setProperty("collision_count", "" + collisionCountTot);
	log.info("Finished growBuildingBlocks! Total number of placed building blocks: " + blockCounter
		 + " Ring closure score: " + bestRingClosureScore + " ring_count: " + ringCountTot
		 + " cage_count: " + cageCount + " canonized_graph: " + canonicString
		 + " ringless_collisions: "+ ringlessCollisionsString + " collision_count: " + collisionCountTot);
	// fireModelChangedOnGraphAndLinkController(new ModelChangeEvent(eventConst.MODEL_MODIFIED));
	return resultProperties;
    }

    /** given a list of building blocks and a list of connections, grow until collisions are unavoidable */
    public Properties growBuildingBlocks(GrowConnectivity connectivity,
					 String rootName,
					 String nameBase,
					 Vector3D startPosition,
					 String ringClosureExportFileNameBase) throws FittingException, IOException {
	return growBuildingBlocks(connectivity.getBuildingBlocks(), connectivity.getConnections(),
       	  rootName, nameBase, startPosition, connectivity.getNumGenerations(), ringClosureExportFileNameBase, connectivity.getTopologies(),
				  connectivity.getSizeMax(), connectivity.isGraphFlag());
    }
    
    public void setFuseStrandsMode(int fuseMode) {
	this.fuseStrandsMode = fuseMode;
    }

    public void setRandomMode(boolean b) { this.randomMode = b; }

    public void setRingClosureExportLimit(double limit) { this.ringClosureExportLimit = limit; }

    /** sets maximum limit for stem fitting (useful for "graph mode") */
    public void setStemRmsLimit(double d) { this.stemRmsLimit = d; }

    /** sets the target graph to be designed */
    public void setTargetGraph(JunctionGraph3D graph) { this.targetGraph = graph; }

    /** Returns true if currently built structure is compatible with target structure */
    private boolean checkCompatibleWithTargetGraph() {
	return true; // TODO
    }

}
