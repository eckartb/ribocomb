package rnadesign.rnamodel;

import java.io.*;
import tools3d.*;
import tools3d.objects3d.*;
import generaltools.*;
import sequence.*;
import java.util.logging.*;
import java.util.Properties;
import static rnadesign.rnamodel.RnaConstants.*;
import rnasecondary.*;

import org.testng.annotations.*;

/** static methods for connecting two junctions with an interpolated stem */
public class ConnectJunctionTools {
    
    /* ID codes of different algorithms */
//     public static final int RIGID_BEST_FIT = 1;
//     public static final int RIGID_DEBUG = 2;
//     public static final int STRETCH = 3;

    private static int mcIterMax = 100;

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    /** returns "optimal" number of base pairs to bridge branch1 and branch2 */
    static int computeBestStemLength(BranchDescriptor3D branch1, 
				     BranchDescriptor3D branch2) {
	// position of next prospective base pair:
	HelixParameters prm1 = branch1.getHelixParameters();
	HelixParameters prm2 = branch2.getHelixParameters();
	Vector3D branch1NewBase = branch1.getBasePosition(); // .plus(branch1.getDirection().mul(prm1.rise));
	Vector3D branch2NewBase = branch2.getBasePosition(); // .plus(branch2.getDirection().mul(prm2.rise));
	double dist = branch1NewBase.distance(branch2NewBase);
	// first and last position are taken up by branch descriptor
	// int numPairs = (int)(((dist-HELIX_OFFSET)/HELIX_RISE + 0.5)) + 1; // added another base EB 6/2007
	int numPairs = (int)(((dist-HELIX_OFFSET)/HELIX_RISE + 0.5)) - 1; // added another base EB 6/2007
	assert numPairs >= 0;
	return numPairs;
    }

    /** central method for generating stem that connects two
     * BranchDescriptors.
     * TODO : overlap at first base pair possible. Fix!
     * @param angleLimit do not connect branch descriptor if deviation from ideal angle is larger than this angle. Perfect angle corresponds to zero.
     */
    public static Object3DLinkSetBundle generateConnectingStem(BranchDescriptor3D branch1, 
							       BranchDescriptor3D branch2,
							       char c1,
							       char c2,
							       String baseName,
							       FitParameters fitParameters,
							       Object3D nucleotideDB,
							       int algorithm,
							       int  numPairs) {
	assert branch1 != branch2;
	assert branch1.isValid();
	assert branch2.isValid();
	if (numPairs < 0) {
	    numPairs = computeBestStemLength(branch1, branch2);
	}
	log.info("Starting ConnectJunctionTools.generateConnectingStem: " 
		 + branch1.getPosition() + " ; " + branch2.getPosition() + " ; " + baseName
		 + " estimated helix length: " + numPairs + " base pairs");
	double angleError = Math.PI - branch1.getDirection().angle(branch2.getDirection());
	assert (angleError >= 0.0);
	if (angleError > fitParameters.getAngleLimit()) {
	    log.info("Aborting generateConnectingStem because of too large branch descriptor angle");
	    return null;
	}
	Vector3D avgPos = Vector3D.average(branch1.getPosition(), branch2.getPosition());
	assert branch1.isValid();
	assert branch2.isValid();
	Vector3D savedDirection1 = branch1.getDirection();
	Vector3D stemDir = branch2.getBasePosition().minus(branch1.getBasePosition());
	assert stemDir.lengthSquare() > 0.0;
	stemDir.normalize();
	// branch1.setDirection(stemDir);
	assert branch1.isValid();
	assert branch2.isValid();
	Vector3D basePos = branch1.getBasePosition();

	if (numPairs <= 0) {
	    log.info("Aborting generateConnectingStem because the stem length was estimated to be smaller than one.");
	    return null;
	}
	// generate 2 new strands:
	String s1 = "";
	String s2 = "";
	assert branch1.isValid();
	assert branch2.isValid();
	s1 = StringTools.stringFromChar(c1, numPairs); // TODO : should be unspecified ("N")
	s2 = StringTools.stringFromChar(c2, numPairs);
	String strand1Name = baseName + "_" + "forw";
	String strand2Name = baseName + "_" + "back";
	String stemName = baseName + "_stem";
	Object3D resultTree = new SimpleObject3D();
	String setName = baseName + "_root";
	resultTree.setName(setName);
	resultTree.setPosition(avgPos);
	LinkSet resultLinks = new SimpleLinkSet();
	Vector3D pos1 = new Vector3D(0.0, 0.0, 0.0);
	Vector3D dir1 = new Vector3D(0.0, 0.0, 1.0);
	Vector3D pos2 = new Vector3D(10.0, 0.0, 0.0);
	Vector3D dir2 = new Vector3D(1.0, 0.0, 0.0);
	Object3DLinkSetBundle strand1Bundle = null;
	Object3DLinkSetBundle strand2Bundle = null;
	assert branch1.isValid();
	assert branch2.isValid();
	try {
	    strand1Bundle = Rna3DTools.generateSimpleRnaStrand(strand1Name, s1,  pos1, dir1);
	    strand2Bundle = Rna3DTools.generateSimpleRnaStrand(strand2Name, s2,  pos2, dir2);
	}
	catch (UnknownSymbolException e) {
	    log.warning("Internal error in ConnectJunctionTools.generateConnectingStem");
	    System.exit(0);
	}
	assert strand1Bundle.getObject3D() instanceof RnaStrand;
	assert strand2Bundle.getObject3D() instanceof RnaStrand;
	RnaStrand strand1 = (RnaStrand)(strand1Bundle.getObject3D());
	RnaStrand strand2 = (RnaStrand)(strand2Bundle.getObject3D());
	resultLinks.merge(strand1Bundle.getLinks());
	resultLinks.merge(strand2Bundle.getLinks());
	assert branch1.isValid();
	assert branch2.isValid();
	Object3DLinkSetBundle stemBundle = null;
// 	FitParameters fitParameters = new FitParameters(rmsLimit, angleErrorLimit);
// 	fitParameters.setIterMax(iterMax);
	BranchDescriptorOptimizer branchDescriptorOptimizer = BranchDescriptorOptimizerFactory.generate(algorithm, fitParameters); 
	log.info("Using optimizer " + branchDescriptorOptimizer.getClassName() + " for interpolating stem!");
	assert(branchDescriptorOptimizer != null);
	stemBundle = generateInterpolatedRnaStem3D(strand1, strand2,
						   0, numPairs-1, numPairs, 1,
						   true, stemName,
						   branch1, branch2,
						   nucleotideDB,
						   branchDescriptorOptimizer);

// 	switch (algorithm) {
// 	case RIGID_BEST_FIT: 
// 	    stemBundle = generateInterpolatedRnaStem3D(strand1, strand2,
// 						       0, numPairs-1, numPairs, 1,
// 						       true, stemName,
// 						       branch1, branch2, rmsLimit,
// 						       nucleotideDB,
// 						       branchDescriptorOptimizer);
// 	    break;
// 	case STRETCH:
// 	    stemBundle = generateStretchedRnaStem3D(strand1, strand2,
// 						    0, numPairs-1, numPairs, 1,
// 						    true, stemName,
// 						    branch1, branch2, rmsLimit,
// 						    nucleotideDB,
// 						    branchDescriptorOptimizer);
// 	    break;
// 	default:
// 	    log.severe("Unknown algorithm id in ConnectJunctionTools.generateConnectingStem");
// 	    assert false;
// 	}

	if (stemBundle == null) {
	    log.info("Stem could not be generated!");
	    return null; // no suitable solution found!
	}

	assert((stemBundle.getObject3D().size() > 0) && (stemBundle.getLinks().size() > 0));
	resultLinks.merge(stemBundle.getLinks());
	resultTree.insertChild(strand1Bundle.getObject3D());
	resultTree.insertChild(strand2Bundle.getObject3D());
	resultTree.insertChild(stemBundle.getObject3D());
	// branch1.setDirection(savedDirection1);

	// TODO : activate these assertions
// 	assert(strand1.getResidue(0).getPosition().distance(branch1.computeHelixPosition(1, BranchDescriptor3D.OUTGOING_STRAND)) < (3*rmsLimit));
// 	assert(strand2.getResidue(numPairs-1).getPosition().distance(branch1.computeHelixPosition(1, BranchDescriptor3D.INCOMING_STRAND)) < (3*rmsLimit));
// 	assert(strand1.getResidue(numPairs-1).getPosition().distance(branch2.computeHelixPosition(1, BranchDescriptor3D.INCOMING_STRAND)) < (3*rmsLimit));
// 	assert(strand2.getResidue(0).getPosition().distance(branch2.computeHelixPosition(1, BranchDescriptor3D.OUTGOING_STRAND)) < (3*rmsLimit));
 
	return new SimpleObject3DLinkSetBundle(resultTree, resultLinks);
    }

    /** central method for generating stem that emanates from one branch descriptor
     */
    public static Object3DLinkSetBundle generateIdealStem(BranchDescriptor3D branch1, 
							  char c1, char c2,
							  String baseName,
							  Object3D nucleotideDB,
							  int  numPairs) {
	assert branch1.getCoordinateSystem().isValid();
//  	log.fine("Starting ConnectJunctionTools.generateIdealStem: " 
//  		 + branch1.getPosition() + " ; " + branch2.getPosition() + " ; " + baseName +
//  		 " estimated helix length: " + numPairs + " base pairs");
	Vector3D stemDir = branch1.getDirection();
	assert stemDir.lengthSquare() > 0.0;
	stemDir.normalize();
	Vector3D basePos = branch1.getBasePosition();
	if (numPairs <= 0) {
	    log.fine("Aborting generateIdealStem because the stem length was estimated to be smaller than one.");
	    return null;
	}
	// generate 2 new strands:
	String s1 = "";
	String s2 = "";
	s1 = StringTools.stringFromChar(c1, numPairs); // TODO : should be unspecified ("N")
	s2 = StringTools.stringFromChar(c2, numPairs);
	String strand1Name = baseName + "_" + "forw";
	String strand2Name = baseName + "_" + "back";
	String stemName = baseName + "_stem";
	Object3D resultTree = new SimpleObject3D();
	String setName = baseName + "_root";
	resultTree.setName(setName);
	resultTree.setPosition(branch1.getPosition());
	LinkSet resultLinks = new SimpleLinkSet();
	Vector3D pos1 = new Vector3D(0.0, 0.0, 0.0);
	Vector3D dir1 = new Vector3D(0.0, 0.0, 1.0);
	Vector3D pos2 = new Vector3D(10.0, 0.0, 0.0);
	Vector3D dir2 = new Vector3D(1.0, 0.0, 0.0);
	Object3DLinkSetBundle strand1Bundle = null;
	Object3DLinkSetBundle strand2Bundle = null;
	try {
	    strand1Bundle = Rna3DTools.generateSimpleRnaStrand(strand1Name, s1,  pos1, dir1);
	    strand2Bundle = Rna3DTools.generateSimpleRnaStrand(strand2Name, s2,  pos2, dir2);
	}
	catch (UnknownSymbolException e) {
	    log.warning("Internal error in ConnectJunctionTools.generateIdealStem");
	    System.exit(0);
	}
	assert strand1Bundle.getObject3D() instanceof RnaStrand;
	assert strand2Bundle.getObject3D() instanceof RnaStrand;
	RnaStrand strand1 = (RnaStrand)(strand1Bundle.getObject3D());
	RnaStrand strand2 = (RnaStrand)(strand2Bundle.getObject3D());
	resultLinks.merge(strand1Bundle.getLinks());
	resultLinks.merge(strand2Bundle.getLinks());
	Object3DLinkSetBundle stemBundle = null;
// 	FitParameters fitParameters = new FitParameters(rmsLimit, angleErrorLimit);
// 	fitParameters.setIterMax(iterMax);
// 	stemBundle = generateIdealRnaStem3D(strand1, strand2,
// 						   0, numPairs-1, numPairs, 1,
// 						   true, stemName,
// 						   branch1,
// 						   nucleotideDB);
	boolean setEqualMode = true;
	int startPos = 0;
	int stopPos = numPairs-1;
	int helixOffset = 0;
	stemBundle = Rna3DTools.generateRnaStem3D(strand1, strand2, startPos, stopPos, numPairs, helixOffset, 
						  setEqualMode, stemName, branch1, nucleotideDB);

	CoordinateSystem cs = branch1.getCoordinateSystem(); // branch1.generatePropagatedCoordinateSystem(1); // 
	BranchDescriptor3D br1 = new SimpleBranchDescriptor3D(strand1, strand2, startPos, stopPos, cs); // workaround, TODO: test
	Matrix4D m2 = BranchDescriptorTools.computeBranchDescriptorInvertedTransformation(cs.generateMatrix4D(), numPairs); // TODO : test
	CoordinateSystem3D cs2 = new CoordinateSystem3D(m2);
	BranchDescriptor3D br2 = new SimpleBranchDescriptor3D(strand2, strand1, startPos, stopPos, cs2); // workaround, TODO: test
// 	switch (algorithm) {
// 	case RIGID_BEST_FIT: 
// 	    stemBundle = generateInterpolatedRnaStem3D(strand1, strand2,
// 						       0, numPairs-1, numPairs, 1,
// 						       true, stemName,
// 						       branch1, branch2, rmsLimit,
// 						       nucleotideDB,
// 						       branchDescriptorOptimizer);
// 	    break;
// 	case STRETCH:
// 	    stemBundle = generateStretchedRnaStem3D(strand1, strand2,
// 						    0, numPairs-1, numPairs, 1,
// 						    true, stemName,
// 						    branch1, branch2, rmsLimit,
// 						    nucleotideDB,
// 						    branchDescriptorOptimizer);
// 	    break;
// 	default:
// 	    log.severe("Unknown algorithm id in ConnectJunctionTools.generateConnectingStem");
// 	    assert false;
// 	}

	if (stemBundle == null) {
	    log.fine("Stem could not be generated!");
	    return null; // no suitable solution found!
	}

	assert((stemBundle.getObject3D().size() > 0) && (stemBundle.getLinks().size() > 0));
	resultLinks.merge(stemBundle.getLinks());
	resultTree.insertChild(strand1Bundle.getObject3D());
	resultTree.insertChild(strand2Bundle.getObject3D());
	resultTree.insertChild(stemBundle.getObject3D());
	resultTree.insertChild(br1);
	resultTree.insertChild(br2);
	// branch1.setDirection(savedDirection1);

	// TODO : activate these assertions
// 	assert(strand1.getResidue(0).getPosition().distance(branch1.computeHelixPosition(1, BranchDescriptor3D.OUTGOING_STRAND)) < (3*rmsLimit));
// 	assert(strand2.getResidue(numPairs-1).getPosition().distance(branch1.computeHelixPosition(1, BranchDescriptor3D.INCOMING_STRAND)) < (3*rmsLimit));
// 	assert(strand1.getResidue(numPairs-1).getPosition().distance(branch2.computeHelixPosition(1, BranchDescriptor3D.INCOMING_STRAND)) < (3*rmsLimit));
// 	assert(strand2.getResidue(0).getPosition().distance(branch2.computeHelixPosition(1, BranchDescriptor3D.OUTGOING_STRAND)) < (3*rmsLimit));
 
	return new SimpleObject3DLinkSetBundle(resultTree, resultLinks);
    }

    /** central method for generating stem that emanates from one branch descriptor
     */
    public static Object3DLinkSetBundle generateIdealStem(CoordinateSystem3D cs,
							  HelixParameters prm,
							  char c1,
							  char c2,
							  String baseName,
							  Object3D nucleotideDB,
							  int  numPairs) {
//  	log.fine("Starting ConnectJunctionTools.generateIdealStem: " 
//  		 + branch1.getPosition() + " ; " + branch2.getPosition() + " ; " + baseName +
//  		 " estimated helix length: " + numPairs + " base pairs");
	if (prm == null) {
	    prm = new HelixParameters(); // obtain default parameters
	}
	Vector3D stemDir = cs.getZ();
	assert stemDir.lengthSquare() > 0.0;
	stemDir.normalize();
	Vector3D basePos = cs.getPosition();
	if (numPairs <= 0) {
	    log.fine("Aborting generateIdealStem because the stem length was estimated to be smaller than one.");
	    return null;
	}
	// generate 2 new strands:
	String s1 = "";
	String s2 = "";
	s1 = StringTools.stringFromChar(c1, numPairs); // TODO : should be unspecified ("N")
	s2 = StringTools.stringFromChar(c2, numPairs);
	String strand1Name = baseName + "_" + "forw";
	String strand2Name = baseName + "_" + "back";
	String stemName = baseName + "_stem";
	Object3D resultTree = new SimpleObject3D();
	String setName = baseName + "_root";
	resultTree.setName(setName);
	resultTree.setPosition(cs.getPosition());
	LinkSet resultLinks = new SimpleLinkSet();
	Vector3D pos1 = new Vector3D(0.0, 0.0, 0.0);
	Vector3D dir1 = new Vector3D(0.0, 0.0, 1.0);
	Vector3D pos2 = new Vector3D(10.0, 0.0, 0.0);
	Vector3D dir2 = new Vector3D(1.0, 0.0, 0.0);
	Object3DLinkSetBundle strand1Bundle = null;
	Object3DLinkSetBundle strand2Bundle = null;
	try {
	    strand1Bundle = Rna3DTools.generateSimpleRnaStrand(strand1Name, s1,  pos1, dir1);
	    strand2Bundle = Rna3DTools.generateSimpleRnaStrand(strand2Name, s2,  pos2, dir2);
	}
	catch (UnknownSymbolException e) {
	    log.warning("Internal error in ConnectJunctionTools.generateIdealStem");
	    System.exit(0);
	}
	assert strand1Bundle.getObject3D() instanceof RnaStrand;
	assert strand2Bundle.getObject3D() instanceof RnaStrand;
	RnaStrand strand1 = (RnaStrand)(strand1Bundle.getObject3D());
	RnaStrand strand2 = (RnaStrand)(strand2Bundle.getObject3D());
	resultLinks.merge(strand1Bundle.getLinks());
	resultLinks.merge(strand2Bundle.getLinks());
	Object3DLinkSetBundle stemBundle = null;
// 	FitParameters fitParameters = new FitParameters(rmsLimit, angleErrorLimit);
// 	fitParameters.setIterMax(iterMax);
// 	stemBundle = generateIdealRnaStem3D(strand1, strand2,
// 						   0, numPairs-1, numPairs, 1,
// 						   true, stemName,
// 						   branch1,
// 						   nucleotideDB);
	boolean setEqualMode = true;
	int startPos = 0;
	int stopPos = numPairs-1;
	int helixOffset = 0;
	// BranchDescriptor3D branch1 = new SimpleBranchDescriptor3D(cs); // workaround, TODO: test
	BranchDescriptor3D branch1 = new SimpleBranchDescriptor3D(strand1, strand2, startPos, stopPos, cs); // workaround, TODO: test
	Matrix4D m2 = BranchDescriptorTools.computeBranchDescriptorInvertedTransformation(cs.generateMatrix4D(), numPairs); // TODO : test
	CoordinateSystem3D cs2 = new CoordinateSystem3D(m2);
	BranchDescriptor3D branch2 = new SimpleBranchDescriptor3D(strand2, strand1, startPos, stopPos, cs2); // workaround, TODO: test
	branch1.setHelixParameters(prm);
	branch2.setHelixParameters(prm);
	stemBundle = Rna3DTools.generateRnaStem3D(strand1, strand2, startPos, stopPos, numPairs, helixOffset, 
						  setEqualMode, stemName, branch1, nucleotideDB);
// 	switch (algorithm) {
// 	case RIGID_BEST_FIT: 
// 	    stemBundle = generateInterpolatedRnaStem3D(strand1, strand2,
// 						       0, numPairs-1, numPairs, 1,
// 						       true, stemName,
// 						       branch1, branch2, rmsLimit,
// 						       nucleotideDB,
// 						       branchDescriptorOptimizer);
// 	    break;
// 	case STRETCH:
// 	    stemBundle = generateStretchedRnaStem3D(strand1, strand2,
// 						    0, numPairs-1, numPairs, 1,
// 						    true, stemName,
// 						    branch1, branch2, rmsLimit,
// 						    nucleotideDB,
// 						    branchDescriptorOptimizer);
// 	    break;
// 	default:
// 	    log.severe("Unknown algorithm id in ConnectJunctionTools.generateIdealStem");
// 	    assert false;
// 	}

	if (stemBundle == null) {
	    log.fine("Stem could not be generated!");
	    return null; // no suitable solution found!
	}

	assert((stemBundle.getObject3D().size() > 0) && (stemBundle.getLinks().size() > 0));
	resultLinks.merge(stemBundle.getLinks());
	resultTree.insertChild(strand1Bundle.getObject3D());
	resultTree.insertChild(strand2Bundle.getObject3D());
	resultTree.insertChild(stemBundle.getObject3D());
	resultTree.insertChild(branch1);
	resultTree.insertChild(branch2);
	// branch1.setDirection(savedDirection1);

	// TODO : activate these assertions
// 	assert(strand1.getResidue(0).getPosition().distance(branch1.computeHelixPosition(1, BranchDescriptor3D.OUTGOING_STRAND)) < (3*rmsLimit));
// 	assert(strand2.getResidue(numPairs-1).getPosition().distance(branch1.computeHelixPosition(1, BranchDescriptor3D.INCOMING_STRAND)) < (3*rmsLimit));
// 	assert(strand1.getResidue(numPairs-1).getPosition().distance(branch2.computeHelixPosition(1, BranchDescriptor3D.INCOMING_STRAND)) < (3*rmsLimit));
// 	assert(strand2.getResidue(0).getPosition().distance(branch2.computeHelixPosition(1, BranchDescriptor3D.OUTGOING_STRAND)) < (3*rmsLimit));
 
	return new SimpleObject3DLinkSetBundle(resultTree, resultLinks);
    }


    /** central method for generating stem that emanates from one branch descriptor
     */
    @Test(groups={"new"})
    public void testGenerateIdealStem() {
	String methodName = "testGenerateIdealStem";
	String outputPdbName = methodName + ".pdb";
	BranchDescriptor3D bd = new SimpleBranchDescriptor3D(new CoordinateSystem3D(Vector3D.ZVEC));
	assert bd.getCoordinateSystem().isValid();
	int numPairs = 10;
	String base = "bd";
	// read nucleotide database:
	String nucleotideDBFileName = "../resources/nucleotidesDB.pdb";
	try {
	    FileInputStream fis = new FileInputStream(nucleotideDBFileName);
	    Object3D nucleotideDB = NucleotideDBTools.readNucleotideDB(fis);
	    fis.close();
	    Object3DLinkSetBundle result = generateIdealStem(bd, 'C', 'G', base, nucleotideDB, numPairs);
	    System.out.println("Generated stem");
	    Object3DTools.printTree(System.out, result.getObject3D());
	    Object3D resultRoot = result.getObject3D();
	    assert resultRoot.getIndexOfChild(base + "_back") >= 0;
	    assert resultRoot.getIndexOfChild(base + "_forw") >= 0;
	    assert resultRoot.getChild(base + "_back").size() == numPairs;
	    assert resultRoot.getChild(base + "_forw").size() == numPairs;
	    FileOutputStream fos = new FileOutputStream(outputPdbName);
	    
	    GeneralPdbWriter writer = new GeneralPdbWriter();
	    writer.write(fos, result.getObject3D());
	    fos.close();
	}
	catch (java.io.IOException ioe) {
	    System.out.println("Error in file " + nucleotideDBFileName + " : " + ioe.getMessage());
	}
	
    }

    /** central method for generating stem that emanates without branch descriptor
     */
    @Test(groups={"new"})
    public void testGenerateIdealStem2() {
	String methodName = "testGenerateIdealStem2";
	String outputPdbName = methodName + ".pdb";
	CoordinateSystem3D cs = new CoordinateSystem3D(Vector3D.ZVEC);
	int numPairs = 10;
	String base = "bd";
	// read nucleotide database:
	String nucleotideDBFileName = "../resources/nucleotidesDB.pdb";
	try {
	    FileInputStream fis = new FileInputStream(nucleotideDBFileName);
	    Object3D nucleotideDB = NucleotideDBTools.readNucleotideDB(fis);
	    fis.close();
	    // null: use default helix parameters 
	    Object3DLinkSetBundle result = generateIdealStem(cs, null, 'A', 'U', base, nucleotideDB, numPairs);
	    System.out.println("Generated stem");
	    Object3DTools.printTree(System.out, result.getObject3D());
	    Object3D resultRoot = result.getObject3D();
	    assert resultRoot.getIndexOfChild(base + "_back") >= 0;
	    assert resultRoot.getIndexOfChild(base + "_forw") >= 0;
	    assert resultRoot.getChild(base + "_back").size() == numPairs;
	    assert resultRoot.getChild(base + "_forw").size() == numPairs;
	    FileOutputStream fos = new FileOutputStream(outputPdbName);
	    
	    GeneralPdbWriter writer = new GeneralPdbWriter();
	    writer.write(fos, result.getObject3D());
	    fos.close();
	}
	catch (java.io.IOException ioe) {
	    System.out.println("Error in file " + nucleotideDBFileName + " : " + ioe.getMessage());
	}
	
    }

    /** convinience method to generate "artificial" SimpleRnaStem3D object from ideal helix coodinates. 
     * Needs two branch descriptors. Starts optimization in order to find best fitting stem.
     * Branch descriptors describe OUTSIDE connecting stems: adding one base pair results in inside branch descriptor.
     */
    public static Object3DLinkSetBundle generateInterpolatedRnaStem3D(RnaStrand strand1, 
								      RnaStrand strand2, 
								      int startPos,
								      int stopPos, 
								      int length, // length of helix 
								      int helixOffset, // usually 0 (starting directly at branch positions or 1 (starting 1 base pair later) deprecated
								      boolean setEqualMode,
								      String stemName,
								      BranchDescriptor3D branch1,
								      BranchDescriptor3D branch2,
								      Object3D nucleotideDB,
								      BranchDescriptorOptimizer branchDescriptorOptimizer)
    {
	assert branch1.isValid();
	assert branch2.isValid();
	assert(branchDescriptorOptimizer != null);
	Stem stem = new SimpleStem(startPos, stopPos, length, strand1, strand2);
	if (!stem.isValid()) {
	    throw new ApplicationBugException("Invalid stem parameters in RNA3DTools.generateRnaStem3D!");
	}

	// 	Vector3D base = new Vector3D(branch.getPosition()); // TODO : branch descriptor does not contain proper helix center!
	// 	// log.fine("Getting residue endPos " + (startPos+length-1) + " of strand1 with size " + strand1.getResidueCount());
	// 	// Vector3D endPos = strand1.getResidue(startPos+ length -1).getPosition();
	// 	Vector3D direction = new Vector3D(branch.getDirection()); // endPos.minus(base);
	// 	direction.normalize();
	// 	// log.fine("Getting residue endPos " + (stopPos) + " of strand2 with size " + strand2.getResidueCount());
	// 	Vector3D base2 = strand2.getResidue(stopPos).getPosition();
	// 	Vector3D rpTmp = base2.minus(base);
	// 	Vector3D rp = direction.cross(rpTmp);
	// 	rp.normalize(); // relative vector pointing to first base pair from base (approximately). Also orth. to direction 
	
	RnaStem3D stem3D = new SimpleRnaStem3D(stem);
	// computes optimized branch descriptor
	log.info("Starting optimizing branch descriptors : branch1: " + branch1.getName() + " branch2: " + branch2.getName());
	assert branch1.isValid();
	assert branch2.isValid();
	BranchDescriptor3D branch = null;
	log.info("step 1.");
	try {
	    log.info("Calling branchDescriptorOptimizer.optimize ...");
	    branch = branchDescriptorOptimizer.optimize(stem3D, branch1, branch2);
	    if (branch == null) {
		log.info("Branch Descriptor optimizer " + branchDescriptorOptimizer.getClassName() + " could not find suitable solution.");
		return null;
	    }
	    log.info("Result of branchDescriptorOptimizer.optimize " + branch.toString());
	}
	catch (FittingException fe) {
	    log.info("Branch Descriptor optimizer " + branchDescriptorOptimizer.getClassName() + " could not find suitable solution!");
	    return null; // no suitable solution found!	    
	}
	log.info("step 2.");
	if (branch == null) {
	    log.info("Branch Descriptor optimizer " + branchDescriptorOptimizer.getClassName() + " could not find suitable solution.");
	    return null; // no suitable solution found!
	}
	Properties bProp = branch.getProperties();
	assert bProp != null;
	String fitProperty = bProp.getProperty("fit_score");
	assert((fitProperty != null) && (fitProperty.length() > 0.0));
	log.info("Resulting optimized branch descriptors : branch: " + branch.getName() + " fit_score: " + fitProperty);
	Object3DLinkSetBundle resultBundle = Rna3DTools.generateRnaStem3D(strand1, strand2, startPos, stopPos, length, 0, 
									  setEqualMode, stemName, branch, nucleotideDB);
	Object3D resultTree = resultBundle.getObject3D();
	Object3DSet resultStems = Object3DTools.collectByClassName(resultTree, "RnaStem3D");
	assert resultStems.size() == 1;
	Object3D resultStem = resultStems.get(0);
	Properties prop = resultStem.getProperties();
	if (prop == null) {
	    prop = new Properties();
	}
	prop.setProperty("fit_score", fitProperty); // used later for evaluating fit of axial kissing loop!
	resultStem.setProperties(prop);
// 	assert(strand1.getResidue(startPos).getPosition().distance(branch1.computeHelixPosition(1, BranchDescriptor3D.OUTGOING_STRAND)) < RMS_LARGE_LIMIT);
// 	assert(strand2.getResidue(stopPos).getPosition().distance(branch1.computeHelixPosition(1, BranchDescriptor3D.INCOMING_STRAND)) < RMS_LARGE_LIMIT);
// 	assert(strand1.getResidue(startPos + length - 1).getPosition().distance(branch2.computeHelixPosition(1, BranchDescriptor3D.INCOMING_STRAND)) < RMS_LARGE_LIMIT);
	// assert(strand2.getResidue(stopPos - (length - 1)).getPosition().distance(branch2.computeHelixPosition(1, BranchDescriptor3D.OUTGOING_STRAND)) < RMS_LARGE_LIMIT);
	return resultBundle;
    }


    /** convinience method to generate "artificial" SimpleRnaStem3D object from ideal helix coodinates. 
     * Needs two branch descriptors. Starts optimization in order to find best fitting stem.
     * Branch descriptors describe OUTSIDE connecting stems: adding one base pair results in inside branch descriptor.
     */
    public static Object3DLinkSetBundle generateIdealRnaStem3D(RnaStrand strand1, 
							       RnaStrand strand2, 
							       int startPos,
							       int stopPos, 
							       int length, // length of helix 
							       int helixOffset, // usually 0 (starting directly at branch positions or 1 (starting 1 base pair later) deprecated
							       boolean setEqualMode,
							       String stemName,
							       BranchDescriptor3D branch1,
							       Object3D nucleotideDB) 
    {
	assert branch1.getCoordinateSystem().isValid();
	Stem stem = new SimpleStem(startPos, stopPos, length, strand1, strand2);
	if (!stem.isValid()) {
	    throw new ApplicationBugException("Invalid stem parameters in RNA3DTools.generateRnaStem3D!");
	}

	// 	Vector3D base = new Vector3D(branch.getPosition()); // TODO : branch descriptor does not contain proper helix center!
	// 	// log.fine("Getting residue endPos " + (startPos+length-1) + " of strand1 with size " + strand1.getResidueCount());
	// 	// Vector3D endPos = strand1.getResidue(startPos+ length -1).getPosition();
	// 	Vector3D direction = new Vector3D(branch.getDirection()); // endPos.minus(base);
	// 	direction.normalize();
	// 	// log.fine("Getting residue endPos " + (stopPos) + " of strand2 with size " + strand2.getResidueCount());
	// 	Vector3D base2 = strand2.getResidue(stopPos).getPosition();
	// 	Vector3D rpTmp = base2.minus(base);
	// 	Vector3D rp = direction.cross(rpTmp);
	// 	rp.normalize(); // relative vector pointing to first base pair from base (approximately). Also orth. to direction 
	
	RnaStem3D stem3D = new SimpleRnaStem3D(stem);
	// computes optimized branch descriptor
	log.fine("Starting optimizing branch descriptors : branch1: " + branch1.getName());
	Object3DLinkSetBundle resultBundle = Rna3DTools.generateRnaStem3D(strand1, strand2, startPos, stopPos, length, 0, 
									  setEqualMode, stemName, branch1, nucleotideDB);
	Object3D resultTree = resultBundle.getObject3D();
	Object3DSet resultStems = Object3DTools.collectByClassName(resultTree, "RnaStem3D");
	assert resultStems.size() == 1;
	Object3D resultStem = resultStems.get(0);
	Properties prop = resultStem.getProperties();
	if (prop == null) {
	    prop = new Properties();
	}
	resultStem.setProperties(prop);
// 	assert(strand1.getResidue(startPos).getPosition().distance(branch1.computeHelixPosition(1, BranchDescriptor3D.OUTGOING_STRAND)) < RMS_LARGE_LIMIT);
// 	assert(strand2.getResidue(stopPos).getPosition().distance(branch1.computeHelixPosition(1, BranchDescriptor3D.INCOMING_STRAND)) < RMS_LARGE_LIMIT);
// 	assert(strand1.getResidue(startPos + length - 1).getPosition().distance(branch2.computeHelixPosition(1, BranchDescriptor3D.INCOMING_STRAND)) < RMS_LARGE_LIMIT);
	// assert(strand2.getResidue(stopPos - (length - 1)).getPosition().distance(branch2.computeHelixPosition(1, BranchDescriptor3D.OUTGOING_STRAND)) < RMS_LARGE_LIMIT);
	return resultBundle;
    }

    /** convinience method to generate "artificial" SimpleRnaStem3D object from ideal helix coodinates. 
     * Needs two branch descriptors. Starts optimization in order to find best fitting stem.
     * Branch descriptors describe OUTSIDE connecting stems: adding one base pair results in inside branch descriptor.
     */
    /*
    public static Object3DLinkSetBundle generateStretchedRnaStem3D(RnaStrand strand1, 
								   RnaStrand strand2, 
								   int startPos,
								   int stopPos, 
								   int length, // length of helix 
								   int helixOffset, // usually 0 (starting directly at branch positions or 1 (starting 1 base pair later) deprecated
								   boolean setEqualMode,
								   String stemName,
								   BranchDescriptor3D branch1,
								   BranchDescriptor3D branch2,
								   double rmsLimit,
								   Object3D nucleotideDB)
    {
	assert branch1.isValid();
	assert branch2.isValid();
	log.fine("Starting RNA3DTools.generateStretchedRnaStem3D");
	// log.fine("Computing stem3d: " + strand1BasePos + "  " + directionOrig);
	Stem stem = new SimpleStem(startPos, stopPos, length, strand1.getSequence(), strand2.getSequence());
	if (!stem.isValid()) {
	    throw new ApplicationBugException("Invalid stem parameters in Rna3DTools.generateRnaStem3D!");
	}
	RnaStem3D stem3D = new SimpleRnaStem3D(stem);
	// computes optimized branch descriptor
	// log.fine("Starting optimizing branch descriptors : branch1: " + branch1 + " branch2: " + branch2);
	assert branch1.isValid();
	assert branch2.isValid();
	BranchDescriptor3D branch = BranchDescriptorOptimizer.optimizeStretchedBranchDescriptor2(stem3D, branch1, branch2, rmsLimit,
												 mcIterMax);
	if (branch == null) {
	    return null; // no suitable solution found!
	}
	Properties bProp = branch.getProperties();
	assert bProp != null;
	String fitProperty = bProp.getProperty("fit_score");
	String stretchFactor = bProp.getProperty("stretch_factor");
	assert(stretchFactor != null);
	assert((fitProperty != null) && (fitProperty.length() > 0.0));
	// log.fine("Resulting optimized branch descriptors : branch: " + branch + " fit_score: " + fitProperty);
	Object3DLinkSetBundle resultBundle = Rna3DTools.generateRnaStem3D(strand1, strand2, startPos, stopPos, length, 0, 
									  setEqualMode, stemName, branch, nucleotideDB);
	Object3D resultTree = resultBundle.getObject3D();
	Object3DSet resultStems = Object3DTools.collectByClassName(resultTree, "RnaStem3D");
	assert resultStems.size() == 1;
	Object3D resultStem = resultStems.get(0);
	Properties prop = resultStem.getProperties();
	if (prop == null) {
	    prop = new Properties();
	}
	prop.setProperty("fit_score", fitProperty); // used later for evaluating fit of axial kissing loop!
	prop.setProperty("stretch_factor", stretchFactor); // used later for evaluating fit of axial kissing loop!
	resultStem.setProperties(prop);
// 	assert(strand1.getResidue(startPos).getPosition().distance(branch1.computeHelixPosition(1, BranchDescriptor3D.OUTGOING_STRAND)) < RMS_LARGE_LIMIT);
// 	assert(strand2.getResidue(stopPos).getPosition().distance(branch1.computeHelixPosition(1, BranchDescriptor3D.INCOMING_STRAND)) < RMS_LARGE_LIMIT);
// 	assert(strand1.getResidue(startPos + length - 1).getPosition().distance(branch2.computeHelixPosition(1, BranchDescriptor3D.INCOMING_STRAND)) < RMS_LARGE_LIMIT);
	// assert(strand2.getResidue(stopPos - (length - 1)).getPosition().distance(branch2.computeHelixPosition(1, BranchDescriptor3D.OUTGOING_STRAND)) < RMS_LARGE_LIMIT);
	return resultBundle;
    }
    */

}
