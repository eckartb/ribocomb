package rnadesign.rnamodel;

import java.util.logging.*;
import java.util.Properties;
import java.util.Set;
import java.util.Iterator;
import sequence.SequenceStatus;
import tools3d.*;
import tools3d.objects3d.*;
import rnasecondary.*;
import sequence.*;
import static rnadesign.rnamodel.PackageConstants.*;

/** using refactoring procedure "extract class" on FragmentGridTiler.generateConnectingStems */
public class ConnectingStemGenerator {

    public static final int APPEND_NONE = 0;
    public static final int APPEND_ALL = 1;
    public static final int APPEND_STICKY_ENDS = 2;

    public static final double STRAND_SYMMETRY_COLLISION_CUTOFF = 10.0; // minimum distance between symmetry generated strands

    private Logger log = Logger.getLogger(LOGFILE_DEFAULT);

    private final FragmentGridTiler tiler;

    private final Object3DSet junctionsOrig;
    private final String baseName;
    private final Object3DSet vertexSet;
    private final LinkSet links;
    private final int requiredJunctionIndex;
    private final int appendStrandsMode;
    private char c1 = 'G'; // characters used for stem generation. Make changeable!
    private char c2 = 'C';

    public static int stickyTailLength = 9; // TODO : avoid this static public variable

    public ConnectingStemGenerator(final FragmentGridTiler tiler,
				   final Object3DSet junctionsOrig, 
				   final String baseName, 
				   final Object3DSet vertexSet, 
				   final LinkSet links,
				   final int requiredJunctionIndex,
				   final int appendStrandsMode) {
	log.fine("Constructing ConnectingStemGenerator!");
	this.tiler = tiler;
	this.junctionsOrig = junctionsOrig;
	this.baseName = baseName;
	this.vertexSet = vertexSet;
	this.links = links;
	this.requiredJunctionIndex = requiredJunctionIndex;
	this.appendStrandsMode = appendStrandsMode;
	log.fine("Finished constructing ConnectingStemGenerator!");
    }

    public void setBasepairCharacters(char c1, char c2) {
	this.c1 = c1;
	this.c2 = c2;
    }

    /** returns set of strand that trace the given set of objects.
     * Method assumes that the set of junctions corresponds one-to-one with the set of vertices 
     * TODO : not clean programming. Depeonds on order of junctions to be the same as order of vertexSet
     * @param requiredJunctionIndex if greater or equal zero, require all generated stems to interact with the junction with this index
     */
    public Object3DLinkSetBundle generate() {
	log.info("Starting ConnectingStemGenerator.generate!");
	// updateModes(this.fgTiler);
	boolean debugFoundConnection = false;
	tiler.setStemStatistics(new TilingStatistics());
	Object3DSet junctions = new SimpleObject3DSet();
	for (int i = 0; i < junctionsOrig.size(); ++i) {
	    if ((junctionsOrig.get(i) == null) || (junctionsOrig.get(i) instanceof StrandJunction3D)) {
		junctions.add(junctionsOrig.get(i));
	    }
	}
	int[][] branchFlags = new int[junctions.size()][StrandJunction3D.BRANCH_MAX];
	assert junctions.size() == vertexSet.size();
	log.info("Starting ConnectingStemGenerator.generate() "
			   + junctionsOrig.size() + " " + junctions.size() + " " + links.size() + " " + baseName + " " + vertexSet.size());
	Object3D resultTree = new SimpleObject3D();
	resultTree.setName("stems");
	LinkSet resultLinks = new SimpleLinkSet();
	// loop over links
	int numFound = 0; // number of connected junctions
	int numTest = 0; // counts number of connected vertices:
	for (int i = 0; i < vertexSet.size(); ++i) {
	    for (int j = i+1; j < vertexSet.size(); ++j) {
		if (links.getLinkNumber(vertexSet.get(i), vertexSet.get(j)) > 0) {
		    ++numTest;
		}
	    }
	}
	log.info("Pairs of connected vertices: " + numTest);
	if (numTest == 0) {
	    log.warning("No vertex links found!");
	}

	if (junctions.size() == 0) {
	    log.warning("No junctions defined!");
	}
	for (int i = 0; i < junctions.size(); ++i) {
	    if ((junctions.get(i)==null) || (! (junctions.get(i) instanceof StrandJunction3D))) {
		continue;
	    }
	    StrandJunction3D junction1 = (StrandJunction3D)(junctions.get(i));
	    Object3D vertex1 = vertexSet.get(i);
	    Object3DSet neighbors1 = LinkTools.findNeighbors(vertex1, links);
	    int junction1BranchCount = junction1.getBranchCount();
	    for (int j = i+1; j < junctions.size(); ++j) {
		if ((junctions.get(j) == null) || (! (junctions.get(j) instanceof StrandJunction3D))) {
		    continue;
		}
		if ((requiredJunctionIndex >= 0) && (requiredJunctionIndex != i) && (requiredJunctionIndex != j)) {
		    continue; // in this mode, stems must interact with this junction
		}
		// 		log.fine("Trying to connect junction pair " + (i+1) + " " + junction1.getName() 
		// 				   + " " + (j+1) + " " + junctions.get(j).getName());
		// check if there is a link between the junctions at all (if their corresponding vertices are linked:
		if (links.getLinkNumber(vertexSet.get(i), vertexSet.get(j)) == 0) {
		    continue;
		}
		log.info("Found connected junctions: " + (i+1) + " " + (j+2));
		++numFound;
		debugFoundConnection = true;
		StrandJunction3D junction2 = (StrandJunction3D)(junctions.get(j));
		assert junction1 != junction2;
		if ( junction1.distance(junction2) == 0.0) {
		    log.warning("Strange, two junctions have distance zero: " + (i+1) + " " + (j+1));
		}
		Object3D vertex2 = vertexSet.get(j); // one-to-one relationship between vertices and junctions
		Object3DSet neighbors2 = LinkTools.findNeighbors(vertex2, links);
		int junction2BranchCount = junction2.getBranchCount();
		// find appropriate branch indices:
		// for (int ii = 0; ii < neighbors1.size(); ++ii) {
// 		double bestAngle = 0.0;
// 		double bestDist = 1e10;
		// look for maximum opposing direction
		log.info("Before for statement. junction1BranchCount=" + junction1BranchCount);
		int[] branchIndices = null;
		try {
		    branchIndices = findBestBranchDescriptorPair(junction1, junction2, i, j,  branchFlags, tiler.getStemFitParameters());
		}
		catch (FittingException fe) {
		    log.info("Skipping junction pair because no suitable branch descriptor pair could be found!");
		    continue;
		}
		assert (branchIndices != null) && (branchIndices.length == 2) && (branchIndices[0] >= 0) && (branchIndices[1] >= 0);
		int branch1Idx = branchIndices[0];
		int branch2Idx = branchIndices[1];
		branchFlags[i][branch1Idx] = 1; // set flag to indicate that junctions are used!
		branchFlags[j][branch2Idx] = 1;
		BranchDescriptor3D branch1 = junction1.getBranch(branch1Idx);
		BranchDescriptor3D branch2 = junction2.getBranch(branch2Idx);
		assert branch1 != branch2;
		if (branch1.distance(branch2) <= 0.0) { 
		    // avoid identical branch descriptors
		    log.info("Error detected: same branches " + branch1Idx + " " + branch2Idx + " int junctions: " 
			     + junction1 + " and " + junction2);
		    log.finest("" + branch1);
		    log.finest("" + branch2);
		    continue;
		}
		assert branch1.distance(branch2) > 0.0; // avoid identical branch descriptors
		//		assert branch1.getOutObject() != null;
		//		assert branch2.getOutObject() != null;
		assert branch1.isValid();
		assert branch2.isValid();
		// below angles and distances are not correct to introduce into statistics object, they are merely
		// reflecting the two chosen branch descriptors
		// stemStatistics.updateAverageAngle( bestAngle * (Math.PI/180) ); // initially, a LARGE angle is good (anti-parallel branch descriptors)
		//should add another angle: angle between junction and graph
		// stemStatistics.updateAverageDistance(bestDist); // distance is quite meaning less at the moment TODO : measure gap between stem and junction
		Object3DLinkSetBundle stemBundle;
		if (!tiler.getKissingLoopMode()) {
		    //distance between stem and junction
		    log.info("Kissing loop mode is off!");
		    // TODO : careful with angleLimit: should be twice as large as for junctions!
		    stemBundle = ConnectJunctionTools.generateConnectingStem(branch1, branch2, c1, c2, baseName, 
									     tiler.getStemFitParameters(), 
									     tiler.getNucleotideDB(), 
									     tiler.getConnectionAlgorithm(), -1);
		    if (stemBundle != null) {
			// new code: extend sequences of junctions to form "sticky ends":
			// obtain generated stem:
			tiler.getStemStatistics().updateCoverage(true);
			Object3D stemBundleObject = stemBundle.getObject3D();
			Object3DSet stemSet = Object3DTools.collectByClassName(stemBundleObject, SimpleRnaStem3D.CLASS_NAME);
			Object3DSet atomSet = Object3DTools.collectByClassName(stemBundleObject, "Atom3D");
			assert ((atomSet != null) && (atomSet.size() > 0)); // some atoms must be placed
			assert stemSet.size() == 1; // one and only one stem found
			RnaStem3D stem = (RnaStem3D)(stemSet.get(0));
			// all residue get "adhoc" property because sequence is not optimized yet 
			Object3DTools.setRecursiveProperty(stem.getStrand1(), SequenceStatus.name, SequenceStatus.adhoc, "Nucleotide3D");
			Object3DTools.setRecursiveProperty(stem.getStrand2(), SequenceStatus.name, SequenceStatus.adhoc, "Nucleotide3D");
			String fitScoreString = stem.getProperties().getProperty("fit_score");
			assert (fitScoreString != null);
			double fitScore = Double.parseDouble(fitScoreString);
			tiler.getStemStatistics().updateAverageAngle(fitScore);
			tiler.getStemStatistics().updateAverageDistance(fitScore);
			switch (appendStrandsMode) {
			case APPEND_NONE:
			    log.info("Using ConnectingStemGenerator.generate.APPEND_NONE:");
			    resultTree.insertChild(stemBundle.getObject3D());
			    break;
			case APPEND_ALL:
			    {
				log.info("Using ConnectingStemGenerator.generate.APPEND_ALL:");
				// obtain strands:
				NucleotideStrand strand1 = branch1.getOutgoingStrand();
				NucleotideStrand strand2 = branch2.getOutgoingStrand(); // TODO : wraon, should be prepend in one case! Fix!!!
				strand1.append(stem.getStrand1()); // extend strands. TODO: better control over "sticky ends"
				strand2.append(stem.getStrand2());
			    }
			    break;
			case APPEND_STICKY_ENDS:
			    log.info("Using ConnectingStemGenerator.generate.APPEND_STICKY_ENDS:");
			    if (stem.getLength() < (2 * stickyTailLength)) {
				log.fine("Stem too short for sticky end generation: " + stem.getLength() + " " + stickyTailLength);
				NucleotideStrand strand1 = branch1.getOutgoingStrand();
				NucleotideStrand strand2 = branch2.getOutgoingStrand(); // TODO : wrong, should be prepend in one case! Fix!!!
				strand1.append(stem.getStrand1());
				strand2.append(stem.getStrand2());
			    }
			    else {
				NucleotideStrand strand1 = branch1.getOutgoingStrand();
				NucleotideStrand strand1b = branch2.getIncomingStrand();
				NucleotideStrand strand2 = branch2.getOutgoingStrand(); // TODO : wrong, should be prepend in one case! Fix!!!
				NucleotideStrand strand2b = branch1.getIncomingStrand(); // TODO : wrong, should be prepend in one case! Fix!!!
				int cutPos = computeStickyEndCut(stem.getLength());
				strand1.append((NucleotideStrand)(stem.getStrand1().cloneDeep(0, cutPos-1)));
				// TODO : careful: prepend commands will make BranchDescriptor indices wrong!
				strand1b.prepend((NucleotideStrand)(stem.getStrand1().cloneDeep(cutPos, stem.getStrand1().size())));
				strand2.append((NucleotideStrand)(stem.getStrand2().cloneDeep(0, cutPos-1)));			       
				strand2b.prepend((NucleotideStrand)(stem.getStrand2().cloneDeep(cutPos, stem.getStrand2().size())));				
			    }
			    break;
			default:
			    assert false; // bad mode
			}
			resultLinks.merge(stemBundle.getLinks());				
		    }
		    else {
			log.info("Stem bundle received from ConnectJunctionTools was null!");
			tiler.getStemStatistics().updateCoverage(false);
		    }
		}
		else {
		    log.info("Generating interpolating stems in kissing loop mode started!");
		    stemBundle = KissingLoopTools.generateConnectingStem(branch1, branch2, c1, c2, baseName, tiler.getStemFitParameters(), 
									 tiler.getNucleotideDB(),
									 tiler.getKissingLoopDB(), tiler.getConnectionAlgorithm(),
									 tiler.getAxialSteps());

		    // TODO : kissingLoopStatistics

		    // 		    if (stemBundle != null) {
		    // 			// new code: extend sequences of junctions to form "sticky ends":
		    // 			// obtain generated stem:
		    // 			Object3D stemBundleObject = stemBundle.getObject3D();
		    // 			Object3DSet kissingLoops = Object3DTools.collectByClassName(stemBundleObject, "KissingLoop3D"); // SimpleRnaStem3D.CLASS_NAME);
		    // 			Object3DSet stems = Object3DTools.collectByClassName(stemBundleObject, "RnaStem3D"); // SimpleRnaStem3D.CLASS_NAME);
		    // 			for (int k = 0; k < kissingLoops.size(); ++k) {
		    // 			    KissingLoop3D kissingLoop = (KissingLoop3D)(kissingLoops.get(k));
		    // 			    for (int m = 0; m < kissingLoop.getStrandCount(); ++m) {
		    // 				// obtain strands:
		    // 				// NucleotideStrand strand1 = branch1.getOutgoingStrand();
		    // 				// 				strand1.append(stem.getStrand1()); // extend strands. TODO: better control over "sticky ends"
		    // 				// 				strand2.append(stem.getStrand2());
		    // 				resultTree.insertChild(kissingLoop.getStrand(m));
		    // 			    }
		    // 			}
		    // 			for (int k = 0; k < stems.size(); ++k) {
		    // 			    RnaStem3D stem = (RnaStem3D)(stems.get(k));
		    // 			    resultTree.insertChild(stm.getStrand1());
		    // 			    resultTree.insertChild(stem.getStrand2());
		    // 			}
		    // 			resultLinks.merge(stemBundle.getLinks()); 
		    // 		    }

		}
		
		// find branch descriptors belonging to that link:
		// Object3DSet branchSet;
		// 		int linkIdx = junction1.findBranchConnectionLink(junction2, links); // findBranchDescriptors(junctions, links);
		// 		if (linkIdx >= 0) {
		// 		    Link link = links.get(linkIdx);
		// 		    BranchDescriptor3D branch1 = (BranchDescriptor3D)(link.getObj1());
		// 		    BranchDescriptor3D branch2 = (BranchDescriptor3D)(link.getObj2());
		// 		    Object3DLinkSetBundle stemBundle = ConnectJunctionTools.generateConnectingStem(branch1, branch2, baseName);
		// 		    resultTree.insertChild(stemBundle.getObject3D());
		// 		    resultLinks.merge(stemBundle.getLinks());
		// 		}

		// 		if (branchSet.size() == 2) { // otherwise no connection was found
		// 		    BranchDescriptor3D branch1 = (BranchDescriptor3D)(branchSet.get(0));
		// 		    BranchDescriptor3D branch2 = (BranchDescriptor3D)(branchSet.get(1));
		// 		    Object3DLinkSetBundle stemBundle = ConnectJunctionTools.generateConnectingStem(branch1, branch2, baseName);
		// 		    resultTree.insertChild(stemBundle.getObject3D());
		// 		    resultLinks.merge(stemBundle.getLinks());
		// 		}
	    }
	}
	// please "dead end" stems:
	for (int i = 0; i < links.size(); ++i) {
	    Link link = links.get(i);
	    if (link.getProperty("silent") != null) {
		log.info("Silent link found, ignoring for placement!");
		continue; // do not place silent links
	    }
	    Object3D vertex1 = link.getObj1(); // one-to-one relationship between vertices and junctions
	    Object3D vertex2 = link.getObj2(); // one-to-one relationship between vertices and junctions
	    int index1 = vertexSet.indexOf(vertex1);
	    int index2 = vertexSet.indexOf(vertex2);	    
	    assert index1 >= 0;
	    assert index2 >= 0;
	    Object3DSet neighbors1 = LinkTools.findNeighbors(vertex1, links);
	    Object3DSet neighbors2 = LinkTools.findNeighbors(vertex2, links);
	    assert neighbors1.size() > 0;
	    assert neighbors2.size() > 0;
	    if ((neighbors1.size() == 1) && (neighbors2.size() == 0)) {
		log.severe("Sorry, helix placement of isolated line not yet implemented!");
	    }
	    StrandJunction3D junction = null;
	    Object3D vertex = null;
	    Object3D junctionVertex = null;
	    int junctionIndex = -1;
	    int otherIndex = -1;
	    if (neighbors2.size() == 1) {
		junctionIndex = index1;
		otherIndex = index2;
		vertex = vertex2;
	    }
	    else if (neighbors1.size() == 1) {
		junctionIndex = index2;
		otherIndex = index1;
		vertex = vertex1;
	    }
	    else {
		continue;
	    }
	    junction = (StrandJunction3D)(junctions.get(junctionIndex));
	    junctionVertex = vertexSet.get(junctionIndex); // vertex on which junction was placed
	    Object3DLinkSetBundle culDeSacStems = new SimpleObject3DLinkSetBundle();
	    if ((junction != null) && (vertex != null) && (junctionVertex != null) ) {
		// find out if symmetry involved:
		int symIndex = findMatchingSymmetry(link, junctionVertex);
		try {
		    if (symIndex < 0) {
			log.info("Trying to generate free stem for junction " + junction.getName());
			culDeSacStems = generateFreeStem(junctions, junctionIndex, vertex, junctionVertex, baseName, 
							 tiler.getStemFitParameters(), 
							 tiler.getNucleotideDB(), 
							 tiler.getConnectionAlgorithm(),
							 branchFlags);
			assert culDeSacStems != null;
		    }
		    else { // symmetry found
			log.info("Trying to generate symmetric stem for junction " + junction.getName());
			CoordinateSystem3D cs = (CoordinateSystem3D)(junctionVertex.getChild(symIndex));
			culDeSacStems = generateSymmetryStem(junctions, junctionIndex, cs, baseName, 
							     tiler.getStemFitParameters(), 
							     tiler.getNucleotideDB(), 
							     tiler.getConnectionAlgorithm(),
							     branchFlags);
		    }
		    assert culDeSacStems != null;
		    if (appendStrandsMode == APPEND_NONE) {
			if (culDeSacStems.getObject3D() != null) {
			    log.info("Generated free stem for junction " + junction.getName());
			    resultTree.insertChild(culDeSacStems.getObject3D());
			}
			else {
			    log.info("Could not generate cul de sac stem for junction " + junction.getName());
			}
		    }
		    resultLinks.merge(culDeSacStems.getLinks());
		}
		catch (FittingException fe) {
		    log.info("Could not place cul de sac helix for junction " + junction.getName());
		    tiler.getStemStatistics().updateCoverage(false);
		}
	    }
	}

	log.info("Finished ConnectingStemGenerator.generate: " + resultTree.size() + " " + resultLinks.size() + " "
		 + numFound);
	// assrt numFound > 0;
	if (!debugFoundConnection) {
	    log.warning("No stems found for junction index: " + requiredJunctionIndex);
	}
	return new SimpleObject3DLinkSetBundle(resultTree, resultLinks);
    }

    private int findMatchingSymmetry(Link link, Object3D obj) {
	int result = -1;
	Properties properties = link.getProperties();
	if (properties != null) {
	    Set keys = properties.keySet();
	    Iterator keyIterator = keys.iterator();
	    String symmetryName = "";
	    while (keyIterator.hasNext()) {
		String key = (String)(keyIterator.next());
		if (key.indexOf("symmetry:") == 0) {
		    log.finest("Parsing key: " + key);
		    String[] words = key.split(":");
		    log.finest("Parsed words: " + words.length + " " 
			     + words[0]);
		    assert words.length == 2;
		    log.finest("Found symmetry in link: " + words[1]);
		    symmetryName = words[1];
		    break;
		}
	    }
	    int symIndex = obj.getIndexOfChild(symmetryName);
	    if ((symIndex >= 0) && (symIndex < obj.size())) {
		log.finest("Symmetry also found in object: " + obj.getName() + " " 
			 + obj.getChild(symIndex).getName());
		result = symIndex;
	    }
	    else {
		log.finest("Symmetry could not be found in junction vertex!");
	    }
	}
	return result;
    }

    // problem: angle is random!
    BranchDescriptor3D generateDummyBranchDescriptor(Object3D vertex, Object3D junctionVertex) throws FittingException {
	Vector3D pos = vertex.getPosition();
	Vector3D direction = junctionVertex.getPosition().minus(vertex.getPosition());
	if (direction.length() == 0.0) {
	    throw new FittingException("Both fitting points are identical!");
	}
	direction.normalize();
	Vector3D x = Vector3DTools.generateRandomOrthogonalDirection(direction);
	x.normalize();
	Vector3D y = direction.cross(x);
	CoordinateSystem cs = new CoordinateSystem3D(vertex.getPosition(), x, y);
	String sequence = "A";
	String name = "culdesac";
	
	NucleotideStrand inStrand = null;
	NucleotideStrand outStrand = null;
	try {
	    inStrand = (NucleotideStrand)(Rna3DTools.generateSimpleRnaStrand(name, sequence, pos, direction).getObject3D());
	    outStrand = (NucleotideStrand)(Rna3DTools.generateSimpleRnaStrand(name, sequence, pos, direction.mul(-1)).getObject3D());
	}
	catch (UnknownSymbolException e) {
	    e.printStackTrace();
	    assert false;
	}
	int incomingIndex = 0;
	int outgoingIndex = 0;
	return new SimpleBranchDescriptor3D(inStrand, outStrand, incomingIndex, outgoingIndex, cs);
    }

    /** generates stem pointing from junction to position of cul de sac vertex ("vertex").
     * TODO: retry, or looking out for fit errors.
     */
    Object3DLinkSetBundle generateFreeStem(Object3DSet junctions,
					   int junctionId,
					   Object3D vertex,
					   Object3D junctionVertex,
					   String baseName, 
					   FitParameters stemFitParameters,
					   Object3D nucleotideDB,
					   int connectionAlgorithm,
					   int[][] branchFlags) throws FittingException {
	StrandJunction3D junction = (StrandJunction3D)(junctions.get(junctionId));
	BranchDescriptor3D bd = generateDummyBranchDescriptor(vertex, junctionVertex); // problem: angle is random!
	int bestIndex = 0;
	double bestAngle = 0;
	for (int i = 0; i < junction.getBranchCount(); ++i) {
	    double angle = bd.getDirection().angle(junction.getBranch(i).getDirection());
	    if (angle > bestAngle) {
		bestAngle = angle;
		bestIndex = i;
	    }
	}
	if (isBranchDescriptorUsed(junctionId, bestIndex, branchFlags)) {
	    throw new FittingException("Branch descriptor already used!");
	}
	BranchDescriptor3D junctionBranch = junction.getBranch(bestIndex);
	Object3DLinkSetBundle stemBundle = ConnectJunctionTools.generateConnectingStem(junction.getBranch(bestIndex), bd, c1, c2, baseName, 
										       stemFitParameters, 
										       nucleotideDB, 
										       connectionAlgorithm, -1);

	if (stemBundle == null) {
	    tiler.getStemStatistics().updateCoverage(false);
	    throw new FittingException("Could not generate free stem for junction " + junction.getName());
	}
	assert stemBundle != null;
	setBranchDescriptorUsed(junctionId, bestIndex, branchFlags);
	Object3DSet stems = Object3DTools.collectByClassName(stemBundle.getObject3D(), SimpleRnaStem3D.CLASS_NAME); // SimpleRnaStem3D.CLASS_NAME);
	assert stems.size() > 0;
	RnaStem3D stem = (RnaStem3D)(stems.get(0));
// 	    log.severe("bad class: " + stemBundle.getObject3D().getName() + NEWLINE);
// 	    Object3DTools.printFullNameTree(System.out, stemBundle.getObject3D());
	// TODO : avoid code duplication (see above) !!!
	Object3DTools.setRecursiveProperty(stem.getStrand1(), SequenceStatus.name, SequenceStatus.adhoc, "Nucleotide3D");
	Object3DTools.setRecursiveProperty(stem.getStrand2(), SequenceStatus.name, SequenceStatus.adhoc, "Nucleotide3D");
	String fitScoreString = stem.getProperties().getProperty("fit_score");
	assert (fitScoreString != null);
	double fitScore = Double.parseDouble(fitScoreString);
	tiler.getStemStatistics().updateCoverage(true);
	tiler.getStemStatistics().updateAverageAngle(fitScore);
	tiler.getStemStatistics().updateAverageDistance(fitScore);
	switch (appendStrandsMode) {
	case APPEND_NONE:
	    log.fine("Using ConnectingStemGenerator.generate.APPEND_NONE:");
	    break;
	case APPEND_ALL:
	    {
		log.fine("Using ConnectingStemGenerator.generate.APPEND_ALL:");
		// obtain strands:
		NucleotideStrand inStrand = junctionBranch.getIncomingStrand();
		NucleotideStrand outStrand = junctionBranch.getOutgoingStrand(); // TODO : wraon, should be prepend in one case! Fix!!!
		outStrand.append(stem.getStrand1());
		inStrand.prepend(stem.getStrand2()); // extend strands. TODO: better control over "sticky ends"
		stemBundle.setObject3D(null);
	    }
	    break;
	case APPEND_STICKY_ENDS:
	    {
		log.fine("Using ConnectingStemGenerator.generateFreeStem.APPEND_STICKY_ENDS:");
		log.fine("Cannot handle sticky tails for free helices yet: " + stem.getLength() + " " + stickyTailLength);
		NucleotideStrand inStrand = junctionBranch.getIncomingStrand();
		NucleotideStrand outStrand = junctionBranch.getOutgoingStrand(); // TODO : wraon, should be prepend in one case! Fix!!!
		outStrand.append(stem.getStrand1());
		inStrand.prepend(stem.getStrand2()); // extend strands. TODO: better control over "sticky ends"
		stemBundle.setObject3D(null);
	    }
	    break;
	default:
	    assert false; // bad mode
	}

	// TODO : retry!
	assert stemBundle != null;
	return stemBundle;
    }


    /** generates stem pointing from junction to position of symmetry-generated copy of junction
     * TODO: retry, or looking out for fit errors.
     */
    Object3DLinkSetBundle generateSymmetryStem(Object3DSet junctions,
					       int junctionId,
					       CoordinateSystem transformation,
					       String baseName, 
					       FitParameters stemFitParameters,
					       Object3D nucleotideDB,
					       int connectionAlgorithm,
					       int[][] branchFlags) throws FittingException {
	StrandJunction3D junction = (StrandJunction3D)(junctions.get(junctionId));
	StrandJunction3D symJunction = (StrandJunction3D)(junction.cloneDeep());
	// apply transformation to cloned junction:
	symJunction.activeTransform2(transformation);
	CoordinateSystem cs = transformation;
	log.fine("Trying to connect symmetric junctions: positions are: " + junction.getPosition() + " " + symJunction.getPosition() + " with cs "
		 + transformation + " distances: " + cs.distance(junction) + " " + cs.distance(symJunction)
		 + " angle: "
		 + RAD2DEG*(junction.getPosition().minus(cs.getPosition())).angle(symJunction.getPosition().minus(cs.getPosition())));
	Object3DLinkSetBundle stemBundle = generateConnectingStem(junction, symJunction, junctionId, -1, branchFlags, baseName, 
								  stemFitParameters, 
								  nucleotideDB, 
								  connectionAlgorithm,
								  transformation
								  );
	assert stemBundle != null;
	return stemBundle;
    }

    /** Computes best fitting branch descriptor pair between any two junctions.
     * TODO : currently, only the angle is considered. This should be much improved: collision check, distance check. */
    int[] findBestBranchDescriptorPair(StrandJunction3D junction1,
				       StrandJunction3D junction2,
				       int i,
				       int j,
				       int[][] branchFlags,
				       FitParameters fitParameters) throws FittingException {
	assert junction1 != null && junction2 != null;
	assert branchFlags != null;
	int junction1BranchCount = junction1.getBranchCount();
	int junction2BranchCount = junction2.getBranchCount();
	int branch1Idx = -1;
	int branch2Idx = -1;
	double bestAngle = 0.0;
	double bestDist = 0.0;
	for (int ii = 0; ii < junction1BranchCount; ++ii) {
	    // check if branch id has already been used:
	    if (isBranchDescriptorUsed(i, ii, branchFlags)) {
		// log.fine("Quitting loop (3)");
		continue;
	    }
	    BranchDescriptor3D branch1 = junction1.getBranch(ii);
	    // for (int jj = 0; jj < neighbors2.size(); ++jj) {
	    for (int jj = 0; jj < junction2BranchCount; ++jj) {
		if (isBranchDescriptorUsed(j, jj, branchFlags)) { // already in use!
		    // log.fine("Quitting loop (4)");
		    continue;
		}
		BranchDescriptor3D branch2 = junction2.getBranch(jj);
		double angle = branch1.getDirection().angle(branch2.getDirection()); //should add angle measurement between branch and graph
		double dist = branch1.distance(branch2); //should be distance between sticky ends that are supposed to be together
		log.fine("Chosen junction branches angle, dist: " + (RAD2DEG*angle) + ", " + dist);
		if ((angle > bestAngle) && (Math.abs(angle-Math.PI) <= fitParameters.getAngleLimit())) {
		    // if ((dist < bestDist) && (angle >= angleLimit)) {
		    bestAngle = angle;
		    bestDist = dist;
		    branch1Idx = ii;
		    branch2Idx = jj;
		}
		//if (graphAngle < bestGraphAngle) {
		//bestGraphAngle = graphAngle; Should there be other things in here?
		//} 
		// 			if (links.getLinkNumber(neighbors1.get(ii), neighbors2.get(jj)) > 0) {
		// 			    branch1Idx = ii;
		// 			    branch2Idx = jj;
		// 			    break;
		// 			}
	    }
	}
	if (branch1Idx < 0) {
	    throw new FittingException("No good branch descriptor pair could be found for junctions!");
	}
	int[] result = new int[2];
	result[0] = branch1Idx;
	result[1] = branch2Idx;
	assert (result != null) && (result.length == 2) && (result[0] >= 0) && (result[1]>= 0);
	return result;
    }
    
    boolean isBranchDescriptorUsed(int junctionId,
				   int branchId,
				   int[][] branchFlags) {
	if ((junctionId >= 0) && (junctionId < branchFlags.length)) {
	    if (branchFlags[junctionId][branchId] == 1) {
		return true;
	    }
	}
	return false;
    }

    void setBranchDescriptorUsed(int junctionId,
				 int branchId,
				 int[][] branchFlags) {
	if ((junctionId >= 0) && (junctionId < branchFlags.length)) {
	    branchFlags[junctionId][branchId] = 1;
	}
    }
    
    /** Computes best fitting stem between any two junctions */
    Object3DLinkSetBundle generateConnectingStem(StrandJunction3D junction1,
						 StrandJunction3D junction2,
						 int junction1Id,
						 int junction2Id,
						 int[][] connectionFlags,
						 String baseName, 
						 FitParameters stemFitParameters, 
						 Object3D nucleotideDB, 
						 int connectionAlgorithm,
						 CoordinateSystem transformation) throws FittingException {
	int[] bdIds = findBestBranchDescriptorPair(junction1, junction2, junction1Id, junction2Id, connectionFlags, stemFitParameters);
	int branch1Idx = bdIds[0];
	int branch2Idx = bdIds[1];
	InteractionType watsonCrickType = new RnaInteractionType(RnaInteractionType.WATSON_CRICK);
	if (isBranchDescriptorUsed(junction1Id, branch1Idx, connectionFlags)
	    || isBranchDescriptorUsed(junction2Id, branch2Idx, connectionFlags)) {
	    throw new FittingException("Stem cannot be placed because Branch descriptors are already in use!");
	}
	if (junction2Id < 0) {
	    BranchDescriptor3D b1 = junction1.getBranch(branch1Idx);
	    BranchDescriptor3D b2 = junction2.getBranch(branch2Idx);
	    log.fine("Trying to connect symmetric junction branch descriptors: " + b1.getPosition()
		     + " " + b2.getPosition() + " with distance " + b1.distance(b2) + " and angle " + RAD2DEG*b1.getDirection().angle(b2.getDirection()));
	}
	Object3DLinkSetBundle stemBundle = ConnectJunctionTools.generateConnectingStem(junction1.getBranch(branch1Idx), 
										       junction2.getBranch(branch2Idx),
										       c1,c2,
										       baseName, stemFitParameters, nucleotideDB, connectionAlgorithm, -1);
	if (stemBundle == null) {
	    throw new FittingException("Could not find suitable stem fit for symmetric branch descriptor pair!");
	}
	LinkSet stemBundleLinks = stemBundle.getLinks();
	setBranchDescriptorUsed(junction1Id, branch1Idx, connectionFlags); // set branch descriptor as used
	setBranchDescriptorUsed(junction2Id, branch2Idx, connectionFlags); // set branch descriptor as used

	Object3DSet stems = Object3DTools.collectByClassName(stemBundle.getObject3D(), SimpleRnaStem3D.CLASS_NAME); // SimpleRnaStem3D.CLASS_NAME);
	assert stems.size() > 0;
	RnaStem3D stem = (RnaStem3D)(stems.get(0));
// 	    log.severe("bad class: " + stemBundle.getObject3D().getName() + NEWLINE);
// 	    Object3DTools.printFullNameTree(System.out, stemBundle.getObject3D());
	// TODO : avoid code duplication (see above) !!!
	Object3DTools.setRecursiveProperty(stem.getStrand1(), SequenceStatus.name, SequenceStatus.adhoc, "Nucleotide3D");
	Object3DTools.setRecursiveProperty(stem.getStrand2(), SequenceStatus.name, SequenceStatus.adhoc, "Nucleotide3D");
	String fitScoreString = stem.getProperties().getProperty("fit_score");
	assert (fitScoreString != null);
	double fitScore = Double.parseDouble(fitScoreString);
	tiler.getStemStatistics().updateCoverage(true);
	tiler.getStemStatistics().updateAverageAngle(fitScore);
	tiler.getStemStatistics().updateAverageDistance(fitScore);
	BranchDescriptor3D branch1 = null;
	if (branch1Idx >= 0) {
	    branch1 = junction1.getBranch(branch1Idx);
	}
	BranchDescriptor3D branch2 = null;
	if (branch2Idx >= 0) {
	    branch2 = junction2.getBranch(branch2Idx);
	}
	switch (appendStrandsMode) {
	case APPEND_NONE:
	    log.fine("Using ConnectingStemGenerator.generate.APPEND_NONE:");
	    break;
	case APPEND_ALL:
	    {
		log.fine("Using ConnectingStemGenerator.generate.APPEND_ALL:");
		// obtain strands:

		NucleotideStrand strand1 = branch1.getOutgoingStrand(); // TODO : wraon, should be prepend in one case! Fix!!!
		NucleotideStrand strand2 = branch2.getOutgoingStrand(); // TODO : wraon, should be prepend in one case! Fix!!!
		if (junction1Id >= 0) {
		    strand1.append(stem.getStrand1());
		}
		else {
		    assert junction2Id >= 0;
		    branch2.getIncomingStrand().prepend(stem.getStrand1());
		}
		if (junction2Id >= 0) {
		    strand2.append(stem.getStrand2());
		}
		else {
		    assert junction1Id >= 0;
		    branch1.getIncomingStrand().prepend(stem.getStrand2());
		}
		stemBundle.setObject3D(null);
	    }
	    break;
	case APPEND_STICKY_ENDS:
// 	    {
// 		log.fine("Using ConnectingStemGenerator.generateFreeStem.APPEND_STICKY_ENDS:");
// 		log.fine("Cannot handle sticky tails for free helices yet: " + stem.getLength() + " " + stickyTailLength);
// 		NucleotideStrand inStrand = junctionBranch.getIncomingStrand();
// 		NucleotideStrand outStrand = junctionBranch.getOutgoingStrand(); // TODO : wraon, should be prepend in one case! Fix!!!
// 		outStrand.append(stem.getStrand1());
// 		inStrand.prepend(stem.getStrand2()); // extend strands. TODO: better control over "sticky ends"
// 		stemBundle.setObject3D(null);

	    log.fine("Using ConnectingStemGenerator.generate.APPEND_STICKY_ENDS:");
	    if (stem.getLength() < (2 * stickyTailLength)) {
		log.fine("Stem too short for sticky end generation: " + stem.getLength() + " " + stickyTailLength);
		NucleotideStrand strand1 = branch1.getOutgoingStrand();
		NucleotideStrand strand2 = branch2.getOutgoingStrand(); // TODO : wrong, should be prepend in one case! Fix!!!
		if (junction1Id >= 0) {
		    strand1.append(stem.getStrand1());
		}
		else {
		    assert junction2Id >= 0;
		    branch2.getIncomingStrand().prepend(stem.getStrand1());
		}
		if (junction2Id >= 0) {
		    strand2.append(stem.getStrand2());
		}
		else {
		    assert junction1Id >= 0;
		    branch1.getIncomingStrand().prepend(stem.getStrand2());
		}
	    }
	    else {
		NucleotideStrand strand1 = branch1.getOutgoingStrand();
		NucleotideStrand strand1b = branch2.getIncomingStrand();
		NucleotideStrand strand2 = branch2.getOutgoingStrand(); // TODO : wrong, should be prepend in one case! Fix!!!
		NucleotideStrand strand2b = branch1.getIncomingStrand(); // TODO : wrong, should be prepend in one case! Fix!!!
		int cutPos = computeStickyEndCut(stem.getLength());
		NucleotideStrand appendedHalf1 = (NucleotideStrand)(stem.getStrand1().cloneDeep(0, cutPos)); // added +1 EB
		strand1.append(appendedHalf1);
		NucleotideStrand prependedHalf1 = (NucleotideStrand)(stem.getStrand2().cloneDeep(cutPos, stem.getStrand2().size()));
		strand2b.prepend(prependedHalf1);				
		// TODO : careful: prepend commands will make BranchDescriptor indices wrong!
		// add links :
		for (int i = 0; i < appendedHalf1.getResidueCount(); ++i) {
		    Residue r1 = appendedHalf1.getResidue(i);
		    int i2 = prependedHalf1.getResidueCount()-1-i;
		    if (i2 < 0) {
			break;
		    }
		    Residue r2 = prependedHalf1.getResidue(i2);
		    stemBundleLinks.add(new InteractionLinkImp((Object3D)r1, (Object3D)r2, new SimpleInteraction(r1, r2, watsonCrickType)));
		    log.fine("added watson crick link(1)!");
		}
		if (junction2Id >= 0) {
		    // add other half:
		    NucleotideStrand prependedHalf2 = (NucleotideStrand)(stem.getStrand1().cloneDeep(cutPos, stem.getStrand1().size()));
		    NucleotideStrand appendedHalf2 = (NucleotideStrand)(stem.getStrand2().cloneDeep(0, cutPos)); // added +1 EB
		    strand1b.prepend(prependedHalf2);
		    strand2.append(appendedHalf2);			       
		    for (int i = 0; i < appendedHalf2.getResidueCount(); ++i) {
			Residue r1 = appendedHalf2.getResidue(i);
			int i2 = prependedHalf2.getResidueCount()-1-i;
			if (i2 < 0) {
			    break;
			}
			Residue r2 = prependedHalf2.getResidue(i2);
			stemBundleLinks.add(new InteractionLinkImp((Object3D)r1, (Object3D)r2, new SimpleInteraction(r1, r2, watsonCrickType)));
			log.fine("added watson crick link(2)!");
		    }
		}
		else {
		    log.fine("Computing new symmetry sticky tail helices!");
		    BranchDescriptor3D branch3 = junction1.getBranch(branch2Idx); // corresponding branch descriptor on main junction
		    strand1b = branch3.getIncomingStrand();
		    strand2 = branch3.getOutgoingStrand(); // TODO : wrong, should be prepend in one case! Fix!!!
		    NucleotideStrand newStrand1 = (NucleotideStrand)(stem.getStrand1().cloneDeep(cutPos, stem.getStrand1().size()));
		    NucleotideStrand newStrand2 = (NucleotideStrand)(stem.getStrand2().cloneDeep(0, cutPos )); // added + 1 EB
		    // assert (newStrand1.getResidueCount() + newStrand2.getResidueCount()) == 2 * stem.getLength();
		    if (transformation != null) {
			newStrand1.passiveTransform2(transformation); // transform BACK to original junction
			newStrand2.passiveTransform2(transformation);
		    }
		    // workaround: check if centers of mass sufficiently different, otherwise do not add:
		    Vector3D v11 = MoleculeTools.averageAtomPosition(appendedHalf1);
		    Vector3D v12 = MoleculeTools.averageAtomPosition(prependedHalf1);
		    Vector3D v1 = Vector3D.average(v11, v12);
		    Vector3D v21 = MoleculeTools.averageAtomPosition(newStrand1);
		    Vector3D v22 = MoleculeTools.averageAtomPosition(newStrand2);
		    Vector3D v2 = Vector3D.average(v21, v22);
		    double dist = v1.distance(v2);
		    if (dist >= STRAND_SYMMETRY_COLLISION_CUTOFF) {
			strand1b.prepend(newStrand1);
			strand2.append(newStrand2);
			// add links corresponding to symmetry image:
			for (int i = 0; i < newStrand2.getResidueCount(); ++i) {
			    Residue r1 = newStrand2.getResidue(i);
			    int i2 = newStrand1.getResidueCount()-1-i;
			    if (i2 < 0) {
				break;
			    }
			    Residue r2 = newStrand1.getResidue(i2);
			    stemBundleLinks.add(new InteractionLinkImp((Object3D)r1, (Object3D)r2, new SimpleInteraction(r1, r2, watsonCrickType)));
			    log.fine("added watson crick link(3)!");
			}
		    }
		    else {
			log.info("Not adding strand halfs because symmetry images would lead to collisions!");
		    }
		}

	    }
	    break;

	default:
	    assert false; // bad mode
	}
	log.fine("finished generateConnectingStem with " + stemBundle.getObject3D() + " objects and " + stemBundle.getLinks().size() + " links.");
	return stemBundle;
    }

    private int computeStickyEndCut(int stemLength) {
	int middle = stemLength/2;
	int d = stickyTailLength/2;
	int result = middle - d;
	return result;
    }
    
    //    public static int getStickyTailLength() { return stickyTailLength; }

    // public static void setStickyTailLength(int length) { this.stickyTailLength = length; }


}
