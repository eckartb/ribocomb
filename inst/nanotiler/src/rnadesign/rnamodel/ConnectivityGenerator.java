package rnadesign.rnamodel;

import tools3d.Vector3D;
import tools3d.objects3d.LinkSet;
import tools3d.objects3d.Object3D;
import java.util.*;

public interface ConnectivityGenerator {

    Iterator<GrowConnectivity> iterator();

    void setTopology(String s);

    /** Returns maximum number of allowed non-equivalent building blocks (number of different database entries) */
    int getBuildingBlockCountMax();

    /** Returns maximum number of allowed non-equivalent connections between non-equivalent building blocks */
    int getConnectionCountMax();

    int getNumGenerations();

    void setNumGenerations(int generations);

    /** Returns list of defined vertices */
    List<Object3D> getVertices();

    /** returns order (number of edges/links) of n'th vertex */
    int getVertexOrder(int n);

    /** Returns links corresponding to edges between vertices */
    LinkSet getLinks();

    /** Return set of building blocks */
    List<DBElementDescriptor> getBuildingBlocks();

    /** Returns list of list, each sublist i containing indicies of building blocks (relative to getBuildingBlocks list) that are allowd at vertex i 
     * (index i with respect to getVertices */
    List<List<Integer> > getBuildingBlockIndices();

    /** For each building block store directions of all helices */
    List<List<Vector3D> > getBuildingBlocksHelixDirections();

    /** Returns helix length variation */
    int getHelixLengthVariation();


    String getTopology();

    /** Sets maximum number of allowed non-equivalent building blocks (number of different database entries) */
    void setBuildingBlockCountMax(int n);

    /** Sets maximum number of allowed non-equivalent connections between non-equivalent building blocks */
    void setConnectionCountMax(int n);

    /** Set helix length variation */
    void setHelixLengthVariation(int n);

}
