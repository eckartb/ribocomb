package rnadesign.rnamodel;

import generaltools.ConstraintDouble;
import generaltools.ConstrainableDouble;
import tools3d.*;
import tools3d.objects3d.*;
import tools3d.objects3d.modeling.*;

/** returns zero energy if all constraints are fullfilled, higher zero otherwise */
public class ConstraintForceField implements ForceField {

    private double scale = 1.0; // scale of penalty term

    public ConstraintForceField(double scale) {
	this.scale = scale;
    }

    /** returns zero if value in constraint intervall, penalty difference otherwise */
    private double constraintDist(ConstraintDouble constraint, double x) {
	if (x < constraint.getMin()) {
	    return (constraint.getMin()-x);
	}
	else if (x > constraint.getMax()) {
	    return (x-constraint.getMax());
	}
	return 0.0;
    }

    /** returns energy: only a function of violated constraints */
    public double energy(Object3DSet objects, LinkSet links) {
	double result = 0.0;
	for (int i = 0; i < links.size(); ++i) {
	    Link link = links.get(i);
	    if (link instanceof ConstrainableDouble) {
		ConstraintDouble constraint = ((ConstrainableDouble)link).getConstraint();
		double dist = link.getObj1().distance(link.getObj2());
		double diff = constraintDist(constraint, dist);
		result += scale * constraint.getForceConstant() * diff * diff;
	    }
	}
	return result;
    }


}
