package rnadesign.rnamodel;

public class CorridorDescriptor {

    public double radius = 0.0;

    public double start = 0.0;

    public CorridorDescriptor(double _radius, double _start) {
	this.radius = _radius;
	this.start = _start;
    }

    public CorridorDescriptor(CorridorDescriptor other) {
	this.radius = other.radius;
	this.start = other.start;
    }

    public String toString() { return "(CorridorDescriptor " + radius + " " + start + " )"; }
    
}
    
