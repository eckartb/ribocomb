package rnadesign.rnamodel;

import generaltools.StringTools;

public class DBElementConnectionDescriptor {

    public static final String HELIX_PREFIX = "(hxend)";

    private DBElementDescriptor descriptor1;
    private DBElementDescriptor descriptor2;
    private String helixendName1;
    private String helixendName2;
    private int basePairCount;
    private int delayStage = 0; // only grow this connection if delayStage <= currentDelayStage of grow algorithm

    /** Constructor of connection descriptors. 
     * @param descriptor1 Describes starting building block
     * @param descriptor2 Describes end building block
     * @param helixendName1 Describes id of helix of starting building block (count starts at one). Example: (hxend)1 or (hxend)2
     * @param helixendName2 Describes id of helix of end building block (count starts at one). Example: (hxend)1 or (hxend)2
     * @param basePairCount number of base pairs of helix
     */
    public DBElementConnectionDescriptor(DBElementDescriptor descriptor1,
					 DBElementDescriptor descriptor2,
					 String helixendName1,
					 String helixendName2,
					 int basePairCount) {
	init(descriptor1, descriptor2, helixendName1, helixendName2, basePairCount);
	assert validate();
    }

    /** Constructor of connection descriptors. 
     * @param descriptor1 Describes starting building block
     * @param descriptor2 Describes end building block
     * @param helixId1 Describes id of helix of starting building block (count starts at zero)
     * @param helixId2 Describes id of helix of end building block (count starts at zero)
     * @param basePairCount number of base pairs of helix
     */
    public DBElementConnectionDescriptor(DBElementDescriptor descriptor1,
					 DBElementDescriptor descriptor2,
					 int helixId1, 
					 int helixId2,
					 int basePairCount) {

	this(descriptor1, descriptor2, "(hxend)" + (helixId1+1), "(hxend)" + (helixId2 + 1), basePairCount);
	assert validate();
    }

    /** Constructor of connection descriptors. 
     * @param descriptor1 Describes starting building block
     * @param descriptor2 Describes end building block
     * @param helixendName1 Describes id of helix of starting building block (count starts at one). Example: (hxend)1 or (hxend)2
     * @param helixendName2 Describes id of helix of end building block (count starts at one). Example: (hxend)1 or (hxend)2
     * @param basePairCount number of base pairs of helix
     */
    private void init(DBElementDescriptor descriptor1,
		 DBElementDescriptor descriptor2,
		 String helixendName1,
		 String helixendName2,
		 int basePairCount) {
	if (compareTo(descriptor1, helixendName1, descriptor2, helixendName2) > 0) {
	    init(descriptor2, descriptor1, helixendName2, helixendName1, basePairCount);
	}
	else {
	    this.descriptor1 = descriptor1;
	    this.descriptor2 = descriptor2;
	    this.helixendName1 = helixendName1;
	    this.helixendName2 = helixendName2;
	    this.basePairCount = basePairCount;
	}
	assert validate();	
    }

    private int compareTo(DBElementDescriptor descriptor1,
			  String helixendName1,
			  DBElementDescriptor descriptor2,
			  String helixendName2) {
	int comp1 = descriptor1.compareTo(descriptor2);
	if (comp1 != 0) {
	    return comp1;
	}
	int comp2 = helixendName1.compareTo(helixendName2); // careful: spelling important, (hxend)1 is NOT equivalent (BranchDescriptor3D)1 TODO
	return comp2;
	
    }

    /** Releay deep copy of object. */
    public Object clone() {
	DBElementConnectionDescriptor result = new DBElementConnectionDescriptor((DBElementDescriptor)(this.descriptor1.clone()),
										 (DBElementDescriptor)(this.descriptor2.clone()),
										 new String(this.helixendName1),
										 new String(this.helixendName2),
										 this.basePairCount);
	result.setDelayStage(this.getDelayStage());
	return result;
    }

    public int getBasePairCount() { return this.basePairCount; }
    
    public DBElementDescriptor getDescriptor1() { 
	return descriptor1; 
    }
    
    public DBElementDescriptor getDescriptor2() { 
	return descriptor2; 
    }

    public int getDelayStage() { return delayStage; }

    public String getHelixendName1() { return this.helixendName1; }

    /** Returns 0 instead of (hxend)1 */
    public static int getHelixendId(String name) { 
	int result = Integer.parseInt(StringTools.getRightDigits(name))-1;
	assert result >= 0;
	return result;
    }

    /** Returns 0 instead of (hxend)1 */
    public int getHelixendId1() { 
	return getHelixendId(this.helixendName1);
    }

    public String getHelixendName2() { return this.helixendName2; }

    /** Returns 0 instead of (hxend)1 */
    public int getHelixendId2() { 
	return getHelixendId(this.helixendName2);
    }

    public void setDelayStage(int delayStage) { this.delayStage = delayStage; }

    /** Returns true, if the described connection is similar, meaning the connected helices of the compared connection are equivalent. Ignores length. */
    public boolean isSimilar(DBElementConnectionDescriptor other) {
	boolean check1 = descriptor1.isSimilar(descriptor1) && descriptor2.isSimilar(other.descriptor2)
	    && helixendName1.equals(other.helixendName1) && helixendName2.equals(other.helixendName2);
	if (check1) {
	    return true;
	}
	// check if connection is stored the other way around:
	boolean check2 = descriptor1.isSimilar(descriptor2) && descriptor2.isSimilar(other.descriptor1)
	    && helixendName1.equals(other.helixendName2) && helixendName2.equals(other.helixendName1);
	if (check2) {
	    return true;
	}
	return false;
    }

    public boolean equals(Object other) {
	if (other instanceof DBElementConnectionDescriptor) {
	    return hashCode() == other.hashCode();
	}
	return false;
    }

    public int hashCode() { return toString().hashCode(); }

    public String toShortString(String sep) { return "" + (getDescriptor1().getDescriptorId()+1) + sep + (getDescriptor2().getDescriptorId()+1) + sep
	    + getHelixendName1().substring(7) + sep + getHelixendName2().substring(7) + sep + basePairCount; }

    public String toString() { return "(DBElementConnectionDescriptor " + getDescriptor1() + " " + getDescriptor2() + " " + getHelixendName1() + " " + getHelixendName2() + " " + basePairCount + " )"; }


    public boolean validate() {
	return descriptor1 != null && descriptor2 != null && helixendName1 != null && helixendName2 != null && basePairCount >= 0
	    && descriptor1.compareTo(descriptor2) <= 0;
    }
    
}
