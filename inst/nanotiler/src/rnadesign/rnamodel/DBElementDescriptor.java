package rnadesign.rnamodel;

import generaltools.TestTools;
import graphtools.IntegerList;
import org.testng.*;
import org.testng.annotations.*;

public class DBElementDescriptor implements Comparable<DBElementDescriptor> {

    public static final int UNKNOWN_TYPE = 0;
    public static final int JUNCTION_TYPE = 1;
    public static final int KISSING_LOOP_TYPE = 2;
    public static final int PROTEIN_TYPE = 3;
    public static final int RNA_HELIX_TYPE = 4;
    public static final int LIGAND_TYPE = 5;
    public static final int MOTIF_TYPE = 6;
    public static final int JUNCTION_TYPE_MAX = 7;

    private int order;
    
    private int id;

    private int type;

    private int hash;

    /** id of the descriptor (order in which user specified them, duplicate specifications of same database elements obtain different descriptorIds. */
    private int descriptorId = 0; 

    public DBElementDescriptor() {
	this.order = 2;
	this.id = 0;
	this.type = JUNCTION_TYPE;
	this.descriptorId = 0;
	// this.hash = toString().hashCode();
	rehash();
    }

    public DBElementDescriptor(int order, int id, int type, int descriptorId) {
	this.order = order;
	this.id = id;
	this.type = type;
	this.descriptorId = descriptorId;
	// this.hash = toString().hashCode();
	rehash();
    }

    public Object clone() {
	DBElementDescriptor result = new DBElementDescriptor(this.order, this.id, this.type, this.descriptorId);
	return result;
    }

    /** returns list of integers describing state of object. Used in comparable method */
    private IntegerList toIntegerList() {
	IntegerList list = new IntegerList();
	list.add(order);
	list.add(id);
	list.add(type);
	list.add(descriptorId);
	return list;
    }
    
    /** Returns zero iff this element is equal to other element */
    public int compareTo(DBElementDescriptor other) {
	IntegerList l1 = toIntegerList();
	IntegerList l2 = other.toIntegerList();
	int result = l1.compareTo(l2);
	// compare with equals method:
	assert ((result == 0) && this.equals(other)) || ((result != 0) && (!this.equals(other)));
	return result;
    }

    public int getDescriptorId() { return descriptorId; }

    public int getId() { return id; }

    public int getOrder() { return order; }

    public int getType() { return type; }

    public int hashCode() { return hash; }

    /** Returns true of both objects have the same content. Slow version of comparison. */
    public boolean equalsSlow(Object obj) {
	if (obj instanceof DBElementDescriptor) {
	    DBElementDescriptor other = (DBElementDescriptor)obj;
	    boolean result = (getId() == other.getId() && getOrder() == other.getOrder() && getType() == other.getType() 
			      && getDescriptorId() == other.getDescriptorId());
	    boolean resultCheck = (hashCode() == other.hashCode());
	    assert result == resultCheck;
	    return result;
	}
	return false;
    }

    /** Returns true of both objects have the same content ignoring the descriptor id. */
    public boolean equivalent(DBElementDescriptor other) {
	return (getId() == other.getId() && getOrder() == other.getOrder() && getType() == other.getType());
    }


    /** Equals method relies on proper computation of hashCode */
    public boolean equals(Object obj) {
	if (obj instanceof DBElementDescriptor) {
	    boolean result = (hash == obj.hashCode());
	    assert result == equalsSlow(obj);
	    return result;
	}
	return false;
    }

    /** Converts junction type id into string. Better: java enums! */
    public static String getTypeString(int junctionType) {
	switch (junctionType) {
	case JUNCTION_TYPE: return "j";
	case KISSING_LOOP_TYPE: return "k";
	case MOTIF_TYPE: return "m";
	default:
	    assert false; // not yet implemented
	}
	return null;
    }

    /** Converts junction type string into id. Better: java enums! */
    public static int parseTypeString(String typeString) {
	assert typeString != null;
	if (typeString.equals("j")) {
	    return JUNCTION_TYPE;
	}
	else if (typeString.equals("k")) {
	    return KISSING_LOOP_TYPE;
	}
	else if (typeString.equals("m")) {
	    return MOTIF_TYPE;
	}
	assert false; // could not interpret string
	return UNKNOWN_TYPE;
    }

    /** parses text given by toString method */
    public static DBElementDescriptor parse(String s) {
	if ((s == null) || (s.length() == 0)) {
	    return null;
	}
	s = s.trim();
	String[] words = s.split(" ");
	if (words.length != 6) {
	    return null;
	}
	return new DBElementDescriptor(Integer.parseInt(words[2]), Integer.parseInt(words[3])-1, parseTypeString(words[1]), Integer.parseInt(words[4]));
    }

    /** Setting of descriptor id. */
    public void setDescriptorId(int n) { 
	this.descriptorId = n; 
	rehash(); // added recomputing of hash code. Deep deep bug fix! 
    }

    /** Returns true, is content is "similar": all fields must be identical, however the descriptorId is ignored */
    public boolean isSimilar(DBElementDescriptor other) {
	return type == other.getType() && order == other.getOrder() && id == other.getId();
    }

    /** Recompute hash code. Call for all setter functions! */
    private void rehash() { this.hash = toStringCore(" ").hashCode(); }

    public String toStringCore(String sep) {
	return "" + getTypeString(type) + sep + order + sep + (id+1) + sep + descriptorId;
    }

    public String toString() {
	return "(DBElementDescriptor " + toStringCore(" ") + " )";
    }
    
    @Test (groups={"new"})
    void testParseDBElementDescriptor() {
	System.out.println(TestTools.generateMethodHeader("testParseDBElementDescriptor"));
	DBElementDescriptor dbe = new DBElementDescriptor(2,3,JUNCTION_TYPE,4);
	String sdbe = dbe.toString();
	System.out.println("String represenatino of DBElementDescriptor (2,3,1,4): " + sdbe);
	DBElementDescriptor dbe2 = DBElementDescriptor.parse(sdbe);
	assert dbe2.equals(dbe);
	System.out.println(TestTools.generateMethodFooter("testParseDBElementDescriptor"));
    }

    @Test (groups={"new"})
    void testClone() {
	System.out.println(TestTools.generateMethodHeader("DBElemetDescriptor.testClone"));
	DBElementDescriptor dbe = new DBElementDescriptor(2,3,JUNCTION_TYPE,4);
	DBElementDescriptor dbe2 = (DBElementDescriptor)(dbe.clone());
	assert dbe2.equals(dbe);
	System.out.println(TestTools.generateMethodFooter("DBElementDescriptor.testClone"));
    }

}
