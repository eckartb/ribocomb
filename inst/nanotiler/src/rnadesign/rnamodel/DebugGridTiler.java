package rnadesign.rnamodel;

import tools3d.objects3d.*;
import tools3d.*;
import tools3d.numerics3d.Geom3DTools;
import numerictools.BaseConversion;
import numerictools.DoubleArrayTools;
import graphtools.PermutationGenerator;
import java.util.logging.*;

/** Uses building block approach (for junctions) to trace a graph with RNA strands */
public class DebugGridTiler implements GridTiler {

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    private int verboseLevel = 0;

    // private boolean connectJunctionsMode = false; // true;

    private int junctionOrderMin = 1; // ignore junctions with less arms //used to be 3 but debug mode did not display all junctions.

    private double scale = 50.0;

    private double directionLength = 5.0;

    // first dimension: order of junction (how many connections). Second dimension: counter for database entries
    private StrandJunctionDB strandJunctionDB;

    /** constructor needs a defined database of junctions */
    public DebugGridTiler(StrandJunctionDB strandJunctionDB) {
	this.strandJunctionDB = strandJunctionDB;
    }

    // public boolean isConnectJunctionsMode() { return connectJunctionsMode; }


    /** uses stored set of known junctions to place them for each node */
    private StrandJunction3D placeJunction(int order, int id, String baseName) {
	assert baseName != null;
	assert ((strandJunctionDB != null) && (strandJunctionDB.isValid()));

	log.fine("started placeJunction!");

	// determine how many links point at vertex (order of vertex)
	StrandJunction3D[] strandJunctions = strandJunctionDB.getJunctions(order);
	if ((strandJunctions == null) || (strandJunctions.length < id)) {
	    return null;
	}
	assert (strandJunctions != null) && (strandJunctions.length > id);
	log.fine("number of database junctions with that order: "
			   + strandJunctions.length);
	StrandJunction3D resultJunction = (StrandJunction3D)(strandJunctions[id].cloneDeep());
	Vector3D newPos = new Vector3D((order-2) * scale, id * scale, 0);
	resultJunction.setPosition(newPos);
	return resultJunction;
    }

    /** uses stored set of known junctions to place them for each node */
    private Object3DSet placeJunctions(Object3DSet objectSet, 
				       LinkSet links, 
				       String baseName) {
	// PRECONDITIONS:
	assert objectSet != null;
	assert links != null;
	assert baseName != null;
	log.fine("Started placeJunctions!");
	Object3DSet result = new SimpleObject3DSet();
	Object3D newStrands = new SimpleObject3D();
	newStrands.setName(baseName + "_strands");
	for (int i = 0; i < strandJunctionDB.size(); ++i) {
	    for (int j = 0; j < strandJunctionDB.size(i); ++j) {
		String name = baseName + "_junc" + (i+1) + "_" + j;
		log.fine("Placing junction " + name);
		StrandJunction3D junction = placeJunction(i, j, baseName);
		if (junction.getBranchCount() < junctionOrderMin) {
		    junction = null;
		}
		if (junction != null) {
		    junction.setName(name);
		    StrandJunctionTools.placeStrands(junction, newStrands); // add strands 
		    result.add(junction);
		}
		else {
		    log.fine("Junction " + name + " could not be generated!");
		    result.add(junction); //  adds null to maintain one-to-one relationship
		}
	    }
	}
	result.add(newStrands);
	log.fine("ended placeJunctions!");
	return result;
    }

    /** find two branch descriptors that belong to link */
    Object3DSet findBranchDescriptors(Object3DSet junctions, LinkSet links) {
	Object3DSet resultSet = new SimpleObject3DSet();
	for (int i = 0; i < junctions.size(); ++i) {
	    StrandJunction3D junction1 = (StrandJunction3D)(junctions.get(i));
	    for (int j = i+1; j < junctions.size(); ++j) {
		StrandJunction3D junction2 = (StrandJunction3D)(junctions.get(j));
		int linkIdx = junction1.findBranchConnectionLink(junction2, links);
		// link found!
		if (linkIdx >= 0) {
		    Link link = links.get(linkIdx);
		    resultSet.add(link.getObj1());
		    resultSet.add(link.getObj2());
		    return resultSet;
		}
	    }
	}
	return resultSet; // not found!?
    }


    /** returns set of strand that trace the given set of objects */
    public Object3DLinkSetBundle generateTiling(Object3D objectTree, LinkSet links, 
						String baseName, char defaultSequenceChar, boolean onlyFirstPathMode) 
    {
	// PRECONDITIONS:
	assert objectTree != null;
	assert links != null;
	assert baseName != null;
	log.fine("started generateTiling!");	
	// add to final tree:
	Object3D resultTree = new SimpleObject3D();
	resultTree.setName(baseName);
	resultTree.setPosition(objectTree.getPosition());
	LinkSet newLinks = new SimpleLinkSet();
	// generate object set:
	Object3DSet vertexSet = new SimpleObject3DSet(objectTree);
	// remove objects that are not linked
	for (int i = vertexSet.size()-1; i >= 0; --i) {
	    if (LinkTools.countLinks(vertexSet.get(i), links) == 0) {
		vertexSet.remove(vertexSet.get(i));
	    }
	}
	// choose and place framents for all vertices ("junctions")
	Object3DSet junctions = placeJunctions(vertexSet, links, baseName);

// 	if (connectJunctionsMode) {
// 	    // interpolate strands between fragments
// 	    // Object3DLinkSetBundle stemBundle = generateConnectingStems(junctions, links, baseName, vertexSet);
// 	    Object3D stemRoot = stemBundle.getObject3D();
// 	    log.fine("Generated " + stemRoot.size() + " connecting stems.");
// 	    resultTree.insertChild(stemBundle.getObject3D());
// 	    newLinks.merge(stemBundle.getLinks());
// 	}

	assert junctions != null;

	// TODO : determine cutting points for strands
	for (int i = 0; i < junctions.size(); ++i) {
	    if (junctions.get(i) != null) {
		resultTree.insertChild(junctions.get(i));
	    }
	}
	
	Object3DLinkSetBundle result = new SimpleObject3DLinkSetBundle(resultTree, newLinks);

	log.fine("Finished generateTiling!");

	assert result != null;    
	return result;
    }

    /** returns verbose level. 0: silent, 1: average, 2: very verbose */
    public int getVerboseLevel() { return verboseLevel; }

    /** sets verbose level. 0: silent, 1: average, 2: very verbose */
    public void setVerboseLevel(int n) { this.verboseLevel = n; }

    /** TODO implement, return tilingStatistics */
    public TilingStatistics getTilingStatistics() { return null; }
}
