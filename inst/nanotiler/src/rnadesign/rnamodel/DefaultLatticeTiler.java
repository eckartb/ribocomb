package rnadesign.rnamodel;

import tools3d.*;
import tools3d.objects3d.*;
import symmetry.*;

/** generates simple copy-transform-paste tiling of unit-cell */
public class DefaultLatticeTiler implements LatticeTiler {

    private void moveUnitCell(Object3D unitCell,
			      Lattice lattice,
			      int i, int j, int k) {
	assert false;
// 	CoordinateSystem cs = spaceGroup.generateTransform(i, j, k);
// 	cs.applyActiveTransform(unitCell);
    }

    private String generateCellName(String base,
				    int i, 
				    int j, int k) {
	return base + "_" + i + "_" + j + "_" + j;
    }

    private void updateTiling(Object3DLinkSetBundle result,
			      Object3DLinkSetBundle unitCell,
			      Lattice lattice,
			      int i, int j, int k, 
			      String name) {
	assert false; // TODO : not yet implemented
// 	Object3DLinkSetBundle cellCopy = (Object3DLinkSetBundle)(unitCell.cloneDeep());
// 	Object3D root = cellCopy.getObject3D();
// 	root.setName(generateCellName(name, i, j, k));
// 	moveUnitCell(root, spaceGroup, i, j, k);
// 	result.getObject3D().insertChild(root);
// 	result.getLinks().merge(cellCopy.getLinks());
    }

    public Object3DLinkSetBundle generateTiling(Object3DLinkSetBundle unitCell,
						Lattice lattice,
						LatticeSection section,
						String name) {
	assert false; // TODO : not yet implemented
 	Object3D root = new SimpleObject3D();
 	root.setName(name);
 	// root.setPosition(spaceGroup.generateTransform(section.getXMin(),section.getYMin(),section.getZMin()).getPosition());
	LinkSet links = new SimpleLinkSet();
 	Object3DLinkSetBundle result = new SimpleObject3DLinkSetBundle(root, links);
// 	for (int i = section.getXMin(); i <= section.getXMax(); ++i) {
// 	    for (int j = section.getYMin(); j <= section.getYMax(); ++j) {
// 		for (int k = section.getZMin(); k <= section.getZMax(); ++k) {
// 		    updateTiling(result, unitCell, lattice, i, j, k, name);
// 		}
// 	    }
// 	}
 	return result;
    }

}
