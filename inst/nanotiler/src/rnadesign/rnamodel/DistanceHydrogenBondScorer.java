package rnadesign.rnamodel;

import tools3d.Vector3D;

class DistanceHydrogenBondScorer implements HydrogenBondScorer {

    double distMax = 3.0;

    public double quality(Atom3D hDonor, Atom3D hAcceptor, Vector3D hEstimate) {
	assert(hEstimate.distance(hDonor.getPosition()) < 2.0);
	// String nd = hDonor.getElement().getShortName();
	// String na = hAcceptor.getElement().getShortName();
	// if (nd.equals("O") && na.equals("N") && (hDonor.distance(hAcceptor) <= distMax)) {
	double d1 = hDonor.distance(hAcceptor); // donor - acceptor distance
	double d2 = hEstimate.distance(hAcceptor.getPosition()); // hydrogen atom to acceptor
	// if (d2 <= distMax && (d2 < d1)) {
	if  ((d2 <= distMax) && (d2 < d1)) {
	    return PERFECT_SCORE;
	}
	// System.out.println("The distance was too high for hydrogen bond: " + hDonor.distance(hAcceptor));
	return NO_HBOND_SCORE;
    }

}