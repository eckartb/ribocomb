package rnadesign.rnamodel;

import generaltools.*;
import tools3d.*;
import tools3d.objects3d.*;
import sequence.*;
import rnasecondary.*;
import java.util.*;
import java.util.logging.*;

/** defines helper functions and constants for DNA.
 * TODO : still uses RNA parameters.
 */
public class Dna3DTools {

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    public static final double RAD2DEG = 180.0 / Math.PI;

    public static final double DEG2RAD = Math.PI / 180.0;

    public static final ResourceBundle rb = ResourceBundle.getBundle("DnaConstants");

    // public static final double HELIX_DELTA_ANGLE = 36.0 * DEG2RAD; // rize in z-coordinate per base-pair in helix

    // public static final double HELIX_CENTER_DIST = 5.0; // distance of center of base pair from helix axis

    // public static final double HELIX_BASE_DIST = 5.0; // distance of centers of bases of base pair

    public static final double RESIDUE_DIST = Double.parseDouble(rb.getString("RESIDUE_DIST")); // 3.5;

    // public static final double WATSON_CRICK_DIST_MIN = 3.0;

    public static final double WATSON_CRICK_DIST = Double.parseDouble(rb.getString("WATSON_CRICK_DIST")); // 4.0;

    // public static final double WATSON_CRICK_DIST_MAX = 5.0;

    ////////// values taken from Aalberts 2005, Nucleic Acids Research
    // TODO move to RnaPhysicalParameters
    public static final double HELIX_BASE_PAIRS_PER_TURN = Double.parseDouble(rb.getString("HELIX_BASE_PAIRS_PER_TURN")); // 11.2;

    public static final double HELIX_RADIUS = Double.parseDouble(rb.getString("HELIX_RADIUS")); // 9.9; 

    /** rize in z-coordinate per base-pair in helix or height per stack */
    public static final double HELIX_RISE = Double.parseDouble("HELIX_RISE"); // 2.7; 

    /** vertical offset between complementary strands */
    public static final double HELIX_OFFSET = Double.parseDouble(rb.getString("HELIX_OFFSET")); // -4.2;

    /** vertical offset between complementary strands */
    public static final double HELIX_PHASE_OFFSET = Double.parseDouble(rb.getString("HELIX_PHASE_OFFSET")); // 93.0*DEG2RAD;

    /** minimum and maximum distances of bases in RNA chain TODO */
    // public static final ConstraintDouble DNA_CHAIN_CONSTRAINT = new SimpleConstraintDouble(WATSON_CRICK_DIST_MIN,
    // 									     WATSON_CRICK_DIST_MAX);


    /** generates DNA strand with one Nucleotide3D object per residue 
     * and no atoms.
     * TODO : still uses RNA parameters. 
     */
    public static Object3DLinkSetBundle generateSimpleDnaStrand(String name,
	    String seqString, Vector3D pos , Vector3D dir) throws UnknownSymbolException {
	log.severe("starting generateSimpleRnaStrand with " + name +
			   " " + seqString + " sorry, currently not supported!");
	return new SimpleObject3DLinkSetBundle();
	
// 	Alphabet alphabet = DnaTools.AMBIGUOUS_DNA_ALPHABET;
// 	DnaStrand strand = new SimpleDnaStrand(alphabet);
// 	strand.setName(name);
// 	strand.setPosition(pos);
// 	log.fine("generated initial strand " +strand.size() + " " + seqString);
// 	// RnaStrand helpStrand = new SimpleRnaStrand(seqString, name);
// 	String seqName = name + ".seq";
// 	Sequence seq = new SimpleSequence(seqString, seqName, alphabet); // strand.getSequence(0);
// 	log.fine("Using sequence: " + seq.size() + " " + seq);
// 	int residueCount = seqString.length();
// 	for (int i = 0; i < residueCount; ++i) {
// 	    LetterSymbol symbol = seq.getResidue(i).getSymbol();
// 	    Nucleotide3D residue = new Nucleotide3D(symbol, seq, i, pos.plus(dir.mul(RESIDUE_DIST*i)));
// 	    // log.fine("Generated nucleotide " + i + " : " + residue);
// 	    double r = RnaPhysicalProperties.RNA_AVERAGE_BASE_RADIUS; // TODO
// 	    residue.setDimensions(new Vector3D(r, r, r));
// 	    Character character = new Character(symbol.getCharacter());
// 	    String resName = "" + character.toString() + (i+1);
// 	    residue.setName(resName);
// 	    residue.setBoundingRadius(r);
// 	    strand.insertChild(residue);
// 	    // log.fine("added residue! " +strand.size());
// 	}
// 	LinkSet links = new SimpleLinkSet();
// 	for (int i = 1; i < strand.size(); ++i) {
// 	    links.add(new SimpleConstraintLink(strand.getChild(i-1),strand.getChild(i), DNA_CHAIN_CONSTRAINT)); 
// 	    // log.fine("links added! " + links.size());
// 	}
// 	log.fine("Finished generateSimpleRnaStrand!");
// 	return new SimpleObject3DLinkSetBundle(strand, links);
    }

}
