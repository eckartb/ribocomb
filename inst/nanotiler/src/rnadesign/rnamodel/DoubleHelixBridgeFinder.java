package rnadesign.rnamodel;

import java.util.*;
import tools3d.*;
import tools3d.objects3d.*;
import java.util.logging.*;

import static rnadesign.rnamodel.PackageConstants.*;

/** Interface for classes that find 3D bridges between two objects */
public class DoubleHelixBridgeFinder implements BridgeFinder {

    private List<StrandJunction3D> junctions;

    private Object3D nucleotideDB;

    private Logger log = Logger.getLogger(LOGGER_NAME);

    private double angleWeight = 10.0; // 50.0; // angle errors: 

    private double rms = 2.0;

    private int solutionMax = 10;

    private int lenMax = 5;

    private int lenMin = 1;

    private int len1Max = lenMax;

    private int len2Max = lenMax;

    private int len3Max = lenMax;

    private int verboseLevel = 2;

    private boolean avgMode = false;// TODO: true would be average between two transformations. Not working properly, because of incomplete quaternion average formalism (see AxisAngle, Matrix4DTools class and their references. All that is missing is properly interpolating between quaternions)
    

    private boolean inversionMode = true; // use calls with branch descriptors pointing inwards, internally transformed to pointing outwards

    private char c1 = 'G';
    private char c2 = 'C';

    private int prop = 1; // need for inversion of BranchDescriptor3D

    private class HelixJunctionSolution implements Comparable<HelixJunctionSolution> {

	double score;
	int junctionId1;
	int junctionId2;

	int helixId11; 
	int helixId12; // id of helix 1 of junction 2
	int helixId21;
	int helixId22;
	int helixLen1;
	int helixLen2;
	int helixLen3;
	
	public int compareTo(HelixJunctionSolution sol2) {
	    // public int compareTo(Object other) { // before Java 1.5
	    // assert other instanceof HelixJunctionSolution;
	    // HelixJunctionSolution sol2 = (HelixJunctionSolution)other;
	    if (score < sol2.score) {
		return -1;
	    }
	    else if (score > sol2.score) {
		return 1;
	    }
	    return 0; // equal
	}
	

	/** Converts to 3D model */
	public Object3DLinkSetBundle convertTo3D(Matrix4D orient1, Matrix4D orient2) {
	    // assert helixId1 != helixId2;
	    StrandJunction3D junction1 = (StrandJunction3D)(junctions.get(junctionId1).cloneDeep());
	    StrandJunction3D junction2 = (StrandJunction3D)(junctions.get(junctionId2).cloneDeep());
	    assert helixId11 >= 0 && helixId11 < junction1.getBranchCount();
	    assert helixId12 >= 0 && helixId12 < junction1.getBranchCount();
	    assert helixId21 >= 0 && helixId21 < junction2.getBranchCount();
	    assert helixId22 >= 0 && helixId22 < junction2.getBranchCount();
	    // superpose helix 1 and its extra base pairs.
	    Matrix4D m1 = junction1.getBranch(helixId11).generatePropagatedCoordinateSystem(helixLen1).generateMatrix4D();
	    Matrix4D conv1 = orient1.multiply(m1.inverse());
	    junction1.activeTransform(new CoordinateSystem3D(conv1));
	    BranchDescriptorTools.invertBranchDescriptor(junction2.getBranch(helixId21), prop);
	    Matrix4D m2 = junction2.getBranch(helixId21).getCoordinateSystem().generateMatrix4D();
	    Matrix4D orientH = junction1.getBranch(helixId12).generatePropagatedCoordinateSystem(helixLen2).generateMatrix4D();
	    BranchDescriptorTools.invertBranchDescriptor(junction2.getBranch(helixId21), prop);	    
	    Matrix4D conv2 = orientH.multiply(m2.inverse());
	    junction2.activeTransform(new CoordinateSystem3D(conv2));
	    Object3D root = new SimpleObject3D();
	    root.setPosition(Vector3D.average(junction1.getPosition(),junction2.getPosition()));
	    root.setName("bridge");
	    root.setProperty("score", "" + score);
	    root.setProperty("junction1_id", "" + (junctionId1+1) );
	    root.setProperty("junction2_id", "" + (junctionId2+1) );
	    root.setProperty("helix_id_1_1", "" + (helixId11 + 1));
	    root.setProperty("helix_id_1_2", "" + (helixId12 + 1));
	    root.setProperty("helix_id_2_1", "" + (helixId21 + 1));
	    root.setProperty("helix_id_2_2", "" + (helixId22 + 1));
	    root.setProperty("helix_len_1", "" + helixLen1);
	    root.setProperty("helix_len_2", "" + helixLen2);
	    root.setProperty("helix_len_3", "" + helixLen3);
	    root.insertChildSafe(junction1);
	    root.insertChildSafe(junction2);
	    if (nucleotideDB != null) {
		if (helixLen1 > 0) {
		    String stem1Name = "stem1";
		    junction1.getBranch(helixId11).propagate(1);
		    Object3DLinkSetBundle stem1Bundle = ConnectJunctionTools.generateIdealStem(junction1.getBranch(helixId11), c1, c2,
											       stem1Name, nucleotideDB, helixLen1);
		    junction1.getBranch(helixId11).propagate(-1);
		    if (stem1Bundle.getObject3D() != null) {
			log.info("Generated bridge stem 1: " + stem1Bundle.getObject3D().getFullName());
			junction1.insertChildSafe(stem1Bundle.getObject3D());
		    }
		    else {
			log.info("No helix (1) could be generated for " + junction1.getBranch(helixId11).getFullName() + " and length " + helixLen2);
		    }
		}
		if (helixLen2 > 0) {
		    String stem2Name = "stem2";
		    junction1.getBranch(helixId12).propagate(1);
		    Object3DLinkSetBundle stem2Bundle = ConnectJunctionTools.generateIdealStem(junction1.getBranch(helixId12), c1, c2,
											       stem2Name, nucleotideDB, helixLen2);
		    junction1.getBranch(helixId12).propagate(-1);
		    if (stem2Bundle.getObject3D() != null) {
			log.info("Generated bridge stem 2: " + stem2Bundle.getObject3D().getFullName());
			junction1.insertChildSafe(stem2Bundle.getObject3D());
		    }
		    else {
			log.info("No helix (2) could be generated for " + junction1.getBranch(helixId12).getFullName() + " and length " + helixLen2);
		    }
		}
		if (helixLen3 > 0) {
		    String stem3Name = "stem3";
		    BranchDescriptor3D bd = junction2.getBranch(helixId22);
		    assert bd != null;
		    bd.propagate(1);
		    Object3DLinkSetBundle stem3Bundle = ConnectJunctionTools.generateIdealStem(bd, c1, c2,
											       stem3Name, nucleotideDB, helixLen3);
		    bd.propagate(-1);
		    if (stem3Bundle.getObject3D() != null) {
			log.info("Generated bridge stem 3: " + stem3Bundle.getObject3D().getFullName());
			junction2.insertChildSafe(stem3Bundle.getObject3D());
		    }
		    else {
			log.info("No helix (3) could be generated for " + bd.getFullName() + " and length " + helixLen3);
		    }
		}
	    }
	    else {
		log.warning("Could not generate bridging helices because no reference nucleotides are defined!");
	    }
	    return new SimpleObject3DLinkSetBundle(root, new SimpleLinkSet());
	}
    }

    public DoubleHelixBridgeFinder() { }

    public DoubleHelixBridgeFinder(List<StrandJunction3D> junctions,
			     Object3D nucleotideDB) { this.junctions = junctions; this.nucleotideDB = nucleotideDB; }

    public List<Object3DLinkSetBundle> findBridge(Object3D obj1, Object3D obj2) {
	log.info("Called HelixBridgeFinder : " + obj1.getFullName() + " " + obj2.getFullName()
		 + " rms: " + rms + " verbose: " + verboseLevel); 
	if ((!(obj1 instanceof BranchDescriptor3D)) || (!(obj2 instanceof BranchDescriptor3D))) {
	    log.warning("Expected BranchDescriptor3D data type: " + obj1.getFullName() + " " + obj2.getFullName());
	    return null;
	}
	return findHelixBridge((BranchDescriptor3D)obj1, (BranchDescriptor3D)obj2);
    }
    
    /** Central command for finding branch descriptor pair in database of structural elements */
    public List<Object3DLinkSetBundle> findHelixBridge(BranchDescriptor3D bd1, BranchDescriptor3D bd2) {
	double distOrig = bd1.distance(bd2);
	log.info("Trying to bridge distance " + distOrig);
	if (inversionMode) { // use calls with branch descriptors pointing inwards, internally transformed to pointing outwards
	    double angle = bd1.getDirection().angle(bd2.getDirection());
	    BranchDescriptorTools.invertBranchDescriptor(bd1, prop);
	    BranchDescriptorTools.invertBranchDescriptor(bd2, prop);
	    log.info("Inverted searching for junction with connector helices and angle: " 
		     + bd1.getFullName() + " " + bd2.getFullName() + " Angle: " + Math.toDegrees(angle) + " distance: " + distOrig);
	    double distNew = bd1.distance(bd2);
	    assert distOrig != distNew;
	    assert Math.abs(distOrig - distNew) < 7.0;
	}
	double angle2 = bd1.getDirection().angle(bd2.getDirection());
	log.info("Searching for junction with helices and angle and distance:" 
		 + bd1.getFullName() + " " + bd2.getFullName() + " Angle: " + Math.toDegrees(angle2) + " dist: " + bd1.distance(bd2));
	List<Object3DLinkSetBundle> result = new ArrayList<Object3DLinkSetBundle>();
	if (!isValid()) {
	    log.warning("No junctions defined in helix bridge finder!");
	}
	else {
	    Matrix4D md = convertBranchDescriptorPairToMatrix(bd1, bd2);
	    Matrix4D m1 = bd1.getCoordinateSystem().generateMatrix4D();
	    Matrix4D m2 = bd2.getCoordinateSystem().generateMatrix4D();
	    List<HelixJunctionSolution> solutions = findHelixBridge(md); // findHelixBridge2(bd1, bd2);
	    if (!inversionMode) {
		BranchDescriptorTools.invertBranchDescriptor(bd1, prop);
		BranchDescriptorTools.invertBranchDescriptor(bd2, prop);
	    }	    
	    for (int i = 0; (i < solutions.size()) && (i < solutionMax); ++i) {
		result.add(solutions.get(i).convertTo3D(m1, m2));
	    }
	    if (!inversionMode) {
		BranchDescriptorTools.invertBranchDescriptor(bd1, prop);
		BranchDescriptorTools.invertBranchDescriptor(bd2, prop);
	    }
	}
	// revert inversion to original orientation!
	if (inversionMode) {
	    BranchDescriptorTools.invertBranchDescriptor(bd1, prop);
	    BranchDescriptorTools.invertBranchDescriptor(bd2, prop);
	}
	return result;
    }

    /** Returns relative 4D matrix corresponding to two branch descriptors (pointing outwards)
     */
    private Matrix4D convertBranchDescriptorPairToMatrix(BranchDescriptor3D bd1, BranchDescriptor3D bd2) {
	Matrix4D m1 = bd1.getCoordinateSystem().generateMatrix4D();
	Matrix4D m2 = bd2.getCoordinateSystem().generateMatrix4D();
	return convertBranchDescriptorPairToMatrix(m1, m2); // 
    }

    /** Returns relative 4D matrix corresponding to two branch descriptors (pointing outwards)
     */
    private Matrix4D convertBranchDescriptorPairToMatrix(Matrix4D m1, Matrix4D m2) {
	return m1.inverse().multiply(m2); // m2.multiply(m1.inverse());
    }

    /** Central command for finding branch descriptor pair in database of structural elements
     * @param matrix 4D matrix describing relative orientation of two branch descriptors: m2 * (m1**-1)*/
    private List<HelixJunctionSolution> findHelixBridge(Matrix4D matrix) {
	assert junctions != null;
	log.info("Called findHelixBridge: " + matrix);
	List<HelixJunctionSolution> result = new ArrayList<HelixJunctionSolution>();
	for (int i = 0; i < junctions.size(); ++i) {
	    for (int j = i; j < junctions.size(); ++j) {
		List<HelixJunctionSolution> junctionSol = findJunctionSolutions(matrix, i, j);
		for (HelixJunctionSolution sol : junctionSol) {
		    if (sol.score <= this.rms) {
			result.add(sol); // only store good solutions
			break; // only store one solution for each bridge combination
		    }
		}
	    }
	}
	Collections.sort(result); // solution with lowest error score is ranked first
	return result;
    }

    /** Finds all solutions for one junction id */
    private List<HelixJunctionSolution> findJunctionSolutions(Matrix4D matrix, int junctionId1, int junctionId2) {
	List<HelixJunctionSolution> solutions = new ArrayList<HelixJunctionSolution>();
	StrandJunction3D junction1 = junctions.get(junctionId1);
	StrandJunction3D junction2 = junctions.get(junctionId2);
	for (int hid11 = 0; hid11 < junction1.getBranchCount(); ++hid11) {
	    for (int hid12 = 0; hid12 < junction1.getBranchCount(); ++hid12) { // try other direction
		if (hid12 == hid11) {
		    continue;
		}
		for (int hid21 = 0; hid21 < junction2.getBranchCount(); ++hid21) {
		    for (int hid22 = 0; hid22 < junction2.getBranchCount(); ++hid22) { // try other direction
			if (hid21 != hid22) {
			    solutions.addAll(findJunctionSolution(matrix, junctionId1, junctionId2, hid11, hid12, hid21, hid22));
			}
		    }
		}
	    }
	}
	Collections.sort(solutions); // solution with lowest error score is ranked first
	return solutions;
    }

    private List<HelixJunctionSolution> findJunctionSolution(Matrix4D matrix, int junctionId1, int junctionId2,
							     int hid11, int hid12, int hid21, int hid22) {
	List<HelixJunctionSolution> solutions = new ArrayList<HelixJunctionSolution>();
	double bestScore = 1e30;
	for (int len1 = 0; len1 < len1Max; ++len1) {
	    for (int len2 = 0; len2 < len2Max; ++len2) {
		for (int len3 = 0; len3 < len3Max; ++len3) {
		    double score = scoreJunctionSolution(matrix, junctionId1, junctionId2, hid11, hid12, hid21, hid22, len1, len2, len3);
		    if (verboseLevel > 3) {
			log.info("Testing bridge: id; " + junctionId1 + " " + junctionId2 + " h " + hid11 + " " + hid12 + " " + hid21 + " " + hid22
				 + " l: " + len1 + " " + len2 + " " + len3 
				 + " score: " + score);
		    }
		    if (score < rms) {
			if ((verboseLevel > 2) || (score < bestScore)) {
			    log.info("Feasible bridge found: ids: " + junctionId1 + " " + junctionId2 + " h " + hid11 + " " + hid12 + " " 
				     + hid21 + " " + hid22
				     + " l: " + len1 + " " + len2 + " " + len3 + " score: " + score);
			}
			if (score < bestScore) {
			    bestScore = score;
			}
			HelixJunctionSolution sol = new HelixJunctionSolution();
			sol.score = score;
			sol.junctionId1 = junctionId1;
			sol.junctionId2 = junctionId2;
			sol.helixLen1 = len1;
			sol.helixLen2 = len2;
			sol.helixLen3 = len3;
			sol.helixId11 = hid11;
			sol.helixId12 = hid12;
			sol.helixId21 = hid21;
			sol.helixId22 = hid22;
			solutions.add(sol);
		    }
		}
	    }
	}
	return solutions;
    }

    public int getLenMin() { return 1; }
    
    public int getLenMax() { return 1000; }

    public double getRms() { return rms; }

    /** Returns true if helix finder object is operational */
    public boolean isValid() {
	return junctions != null && junctions.size() > 0;
    }

    /** Central method scores the quality of junction (junctionId) and helices hid1, hid2 with lengths len1, len2
     * to bridge gap described by matrix. 
     * @param matrix Relative orientation of two branch descriptors pointing outwards 
     */
    private double scoreJunctionSolution(Matrix4D matrix, int junctionId1, int junctionId2,
					 int hid11, int hid12, int hid21, int hid22, int len1, int len2, int len3) {
	assert hid11 != hid12;
	assert hid21 != hid22;

	BranchDescriptor3D bd11 = junctions.get(junctionId1).getBranch(hid11);
	BranchDescriptor3D bd12 = junctions.get(junctionId1).getBranch(hid12);
	BranchDescriptor3D bd21 = junctions.get(junctionId2).getBranch(hid21);
	BranchDescriptor3D bd22 = junctions.get(junctionId2).getBranch(hid22);
	Matrix4D jMatrix = convertBranchDescriptorPairToMatrix(bd11.generatePropagatedCoordinateSystem(len1).generateMatrix4D(),
							       bd12.generatePropagatedCoordinateSystem(len2).generateMatrix4D());
	// BranchDescriptorTools.invertBranchDescriptor(bd21, prop); // workaround: direction of bd21 should be same as bd12. Made to point inwards!
	Matrix4D m2 = BranchDescriptorTools.computeBranchDescriptorInvertedTransformation(bd21.getCoordinateSystem().generateMatrix4D(),prop);
	Matrix4D jMatrix2 = convertBranchDescriptorPairToMatrix(m2, // no extra helix needed
								bd22.generatePropagatedCoordinateSystem(len3).generateMatrix4D());
	// 	BranchDescriptorTools.invertBranchDescriptor(bd21, prop);
	Matrix4D finalMatrix = jMatrix.multiply(jMatrix2);
	double result = scoreMatrixDifference(matrix, finalMatrix);
	// double result2 = scoreMatrixDifference(jMatrix, matrix);
	// assert (Math.abs(result-result2) < 0.01);
	// System.out.println("Scoring bridge vector " + matrix.subvector() + " versus junction vector: " + jMatrix.subvector() + " for lengths "
	// + len1 + " " + len2 + " helix ids: " + hid1 + " " + hid2 + " result: " + result);
	return result;
    }

    /** Scores difference between two 4D matrices */
    private double scoreMatrixDifference(Matrix4D m1, Matrix4D m2) {
	// Matrix4D dm = convertBranchDescriptorPairToMatrix(m1, m2);
	// dm.subtract(Matrix4D.I4); // substract identity matrix: useful for euclidian matrix norm ??? QUESTIONABLE!!!
	return AxisAngle.distanceQuatNorm(m1, m2, angleWeight);// return matrix norm
	// return m1.subvector().distance(m2.subvector()); // dummy implementation
    }

    public void setBasepairCharacters(char c1, char c2) {
	this.c1 = c1;
	this.c2 = c2;
    }

    /** Sets weight of angle norm versus distance norm */
    public void setAngleWeight(double weight) { this.angleWeight = weight; }

    public void setInversionMode(boolean m) { this.inversionMode = m; }

    public void setLenMax(int n) {
	this.lenMax = n;
	this.len1Max = n;
	this.len2Max = n;
	this.len3Max = n;
    }

    public void setLenMin(int n) { this.lenMin = n; }

    public void setJunctions(List<StrandJunction3D> junctions) { this.junctions = junctions; }

    public void setRms(double rms) { this.rms = rms; }

    public void setSolutionMax(int n) { this.solutionMax = n; }

    public void setVerboseLevel(int level) { this.verboseLevel = level; }

}
