package rnadesign.rnamodel;

import static rnadesign.rnamodel.PackageConstants.*;

/** contains parameters describing quality of molecular fit */
public class FitParameters {

    private double angleLimit = Math.PI / 4;

    private double angleWeight = 1.0;

    private int iterMax = 1000;

    private double rmsLimit = 5.0; 

    public FitParameters(double rmsLimit, double angleLimit) {
	this.rmsLimit = rmsLimit;
	this.angleLimit = angleLimit;
    }

    public FitParameters(FitParameters other) {
	this.angleLimit = other.angleLimit;
	this.angleWeight = other.angleWeight;
	this.iterMax = other.iterMax;
	this.rmsLimit = other.rmsLimit;
    }
    
    public Object clone() {
	return new FitParameters(this);
    }

    public double getRmsLimit() { return rmsLimit; }

    public double getAngleLimit() { return angleLimit; }

    public double getAngleWeight() { return angleWeight; }

    public int getIterMax() { return iterMax; }

    public void setAngleLimit(double angleLimit) { 
	this.angleLimit = angleLimit; 
    }

    public void setAngleWeight(double angleWeight) { 
	this.angleWeight = angleWeight;
    }

    public void setIterMax(int iterMax) { this.iterMax = iterMax; }

    public void setRmsLimit(double rmsLimit) { this.rmsLimit = rmsLimit; }

    /** returns info string */
    public String toString() {
	return "rms: " + rmsLimit + " angle: " + (angleLimit * RAD2DEG) + " angleweight: " + angleWeight
	    + " iterations: " + iterMax;
    }

}
