package rnadesign.rnamodel;

public class FittingException extends Exception {

    public FittingException(String message) {
	super(message);
    }

    public FittingException(Exception e) {
	super(e);
    }

}
