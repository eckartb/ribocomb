package rnadesign.rnamodel;

import generaltools.DoubleFunctor;

/** defines class providing real valued function */
public class GaussianFunctor implements DoubleFunctor {

    private double scale;
    private double offs;
    private double sigma;
    private double sigmaSquare;

    public GaussianFunctor(double sigma, double offs, double scale) {
	this.sigma = sigma;
	this.sigmaSquare = sigma * sigma;
	this.offs = offs;
	this.scale = scale;
    }

    /** function that maps real value to another real value */
    public double doubleFunc(double value) {
	value = value - offs;
	return scale * Math.exp(-value*value / sigmaSquare);
    }

    public double getSigma() {
	return sigma;
    }

}
