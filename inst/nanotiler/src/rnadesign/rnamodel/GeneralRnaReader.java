
/** Factory class is responsible for reading/parsing a data file and creating
 *  a corresponding Object3DGraph
 */
package rnadesign.rnamodel;

import java.awt.Color;
import java.io.*;

import sequence.*;
import tools3d.Vector3D;
import tools3d.objects3d.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.*;

/**
 * @author Eckart Bindewald
 *
 */
public class GeneralRnaReader implements Object3DFactory, Object3DFormatter {

    private static Logger log = Logger.getLogger("NanoTiler_debug");
	
    private int formatId = Object3DFormatter.LISP_FORMAT;

    /** Constructor needs a buffered reader (provides methods for linewise reading) */
    public GeneralRnaReader() {
    }
    
    /** reads object and links. TODO : Not yet implemented! */
    public Object3DLinkSetBundle readBundle(InputStream is) throws Object3DIOException {
	return null;
    }

    /* (non-Javadoc)
     * @see rnamodel.Object3DGraphFactory#createObject3DGraph()
     */
    public Object3D readAnyObject3D(InputStream is) 
	throws Object3DIOException {
	log.fine("Starting readAnyObject3D!");
	DataInputStream dis = new DataInputStream(is);
	String objectName = readWord(dis);
	Object3D object3d = null; 
	if (objectName.equals("(Object3D")) {
	    object3d = new SimpleObject3D();
	    readObject3DBody(dis, object3d);
	}	
	// 	else if (objectName.equals("(Object3DGraph")) {
	// object3d =  readObject3DGraphBody(dis);	
	// }
// 	else if (objectName.equals("(RnaBlock")) {
// 		object3d = new RnaPhysicalBlock();
// 	    readRnaBlockBody(dis, (RnaPhysicalBlock)object3d);
// 	} 
	else if (objectName.equals("(RnaStrand")) {
	    object3d = new SimpleRnaStrand();
	    readRnaStrandBody(dis, (RnaStrand)object3d);
	}
	else if (objectName.equals("(Nucleotide3D")) {
	    object3d = new Nucleotide3D();
	    readObject3DBody(dis, object3d); // TODO : fix references in readRnaStrandBody
	    // expect name of form C24
	    char c = object3d.getName().charAt(0);
	    Nucleotide3D nuc = (Nucleotide3D)object3d;
	    // TODO : alphabet not clean here
	    try {
		nuc.setSymbol(new SimpleLetterSymbol(c, 
					     DnaTools.AMBIGUOUS_RNA_ALPHABET));
	    }
	    catch (UnknownSymbolException exc) {
		throw new Object3DIOException("Unknown RNA nucleotide symbol: "
					      + object3d.getName());
	    }
	}
	else if (objectName.equals("(Binding")) {
	    object3d = readBindingSiteBody(dis);
	}
	else {
	    throw new Object3DIOException("readAnyObject3D: " + objectName 
					  + " : unknown type name");
	}
	String expected = ")"; // end of reading object
	String word = readWord(dis);
	if (!word.equals(expected)) {
	    throw new Object3DIOException("readAnyObject3D: " + objectName + " : "
					  + expected	+ " excepted instead of " + word);
	}
	return object3d;
    }
    
    /* (non-Javadoc)
     * @see rnamodel.Object3DGraphFactory#createObject3DGraph()
     */
    private void readObject3DBody(DataInputStream dis, Object3D object3d) 
		throws Object3DIOException {
	log.fine("Starting readObject3D!");
	String word = null; // readWord(dis);
	//		String expected = "(Object3D";
	//		if (!word.equals(expected)) {
	//			throw new Object3DIOException(expected + " excepted instead of " + word);
	//		}
	//		
	object3d.clear();
	word = readWord(dis);
	// sets name:
	object3d.setName(word);
	Vector3D v = readVector3D(dis);
	object3d.setRelativePosition(v);
	
// 	word = readWord(dis);
// 	if (!word.equals(expected)) {
// 	    throw new Object3DIOException("createObject3D: " + expected + " excepted instead of " + word);
// 	}
	
	// read children objects
	
	String expected = "(children";
	word = readWord(dis);
	if (!word.equals(expected)) {
	    throw new Object3DIOException("createObject3D: " + expected + " excepted instead of " + word);
	}
	word = readWord(dis);
	int numChildren = 0;
	try {
	    numChildren = Integer.parseInt(word);
	}
	catch (NumberFormatException e) {
	    throw new Object3DIOException("createObject3D: Could not parse number of children: " + word);	
	}
	// read individual children: recursive call, the child node can itself be the root of a whole tree!
	for (int i = 0; i < numChildren; ++i) {
	    Object3D child = readAnyObject3D(dis);
	    object3d.insertChild(child);
	}
	expected = ")"; // end of reading children
	word = readWord(dis);
	if (!word.equals(expected)) {
	    throw new Object3DIOException("createObject3D: " + expected + " excepted instead of " + word);
	}

	log.fine("Ending readObject3DBody!");
    }
    

    /** resds body of SequenceBindingSite definition */
    private SequenceBindingSite readBindingSiteBody(DataInputStream dis)
	throws Object3DIOException {
	
	SequenceBindingSite object3D = new SimpleSequenceBindingSite();
	readObject3DBody(dis, object3D);

	// expect sequences:
	String expected = "(subset";
	String word = readWord(dis);
	if (!word.equals(expected)) {
	    throw new Object3DIOException("readBindingSiteBody: " + expected + " excepted instead of " + word);
	}
	
	SequenceSubset subset = new SimpleSequenceSubset();
	SequenceReader sequenceReader = new SequenceReader();
	try {
	    sequenceReader.readSequenceSubsetBody(dis, subset);
	}
	catch (SequenceIOException e) {
	    throw new Object3DIOException(e);
	}

	expected = ")"; // end of reading sequence subset
	word = readWord(dis);
	if (!word.equals(expected)) {
	    throw new Object3DIOException("readBindingSiteBody: " + expected 
					  + " excepted instead of " + word);
	}
	object3D.setBindingSequence(subset);
	return object3D;
    }
    

    /** reads body of SequenceBindingSite definition
     * TODO set objects instead of names!!!
 */
    private void readLinkBody(DataInputStream dis, Link link)
	throws Object3DIOException {
		link.clear();
		String word = readWord(dis);
		link.setName(word);
// 		link.setObj1(readWord(dis));
// 		link.setObj2(readWord(dis));
    }
	
    /* reads and generates RnaPhysicalBlock object 
     * @see rnamodel.Object3DGraphFactory#createObject3DGraph()
     */
    /*
    private void readRnaBlockBody(DataInputStream dis, RnaPhysicalBlock rna) 
	throws Object3DIOException {
	log.fine("Starting readRnaBlockBody!");
	String word = null; 
	
	// RnaPhysicalBlock rna = new RnaPhysicalBlock();

	readObject3DBody(dis, rna);

	// expect sequences:
	String expected = "(sequences";
	word = readWord(dis);
	if (!word.equals(expected)) {
	    throw new Object3DIOException("readRnaBlockBody1: " + expected + " excepted instead of " + word);
	}
	word = readWord(dis);
	int numSequences = 0;
	try {
	    numSequences = Integer.parseInt(word);
	}
	catch (NumberFormatException e) {
	    throw new Object3DIOException("readRnaBlockBody2: Could not parse number of sequences: " + word);	
	}
	// read individual sequences
	for (int i = 0; i < numSequences; ++i) {
	    // Object3D child = readAnyObject3D(dis);
	    word = readWord(dis);
	    rna.addSequence(word);
	}
	expected = ")"; // end of reading sequences
	word = readWord(dis);
	if (!word.equals(expected)) {
	    throw new Object3DIOException("readRnaBlockBody3: " + expected + " excepted instead of " + word);
	}
	
	// expect binding sites:
	/*
	expected = "(sites";
	word = readWord(dis);
	if (!word.equals(expected)) {
	    throw new Object3DIOException("readRnaBlockBody4: " + expected + " excepted instead of " + word);
	}
	word = readWord(dis);
	int numSites = 0;
	try {
	    numSites = Integer.parseInt(word);
	}
	catch (NumberFormatException e) {
	    throw new Object3DIOException("readRnaBlockBody5: Could not parse number of binding sites: " + word);	
	}
	// read individual binding sites
	for (int i = 0; i < numSites; ++i) {
	    SequenceBindingSite site = new SequenceBindingSite();
	    expected = "(site";
	    word = readWord(dis);
	    if (!word.equals(expected)) {
		    throw new Object3DIOException("readRnaBlockBody: " + expected + " excepted instead of " + word);
		}
	    readBindingSiteBody(dis, site);
	    rna.addBindingSite(site);
	    expected = ")"; // end of "(site "
	    word = readWord(dis);
	    if (!word.equals(expected)) {
		    throw new Object3DIOException("readRnaBlockBody: " + expected + " excepted instead of " + word);
		}
	}
	expected = ")"; // end of reading binding sites
	word = readWord(dis);
	if (!word.equals(expected)) {
	    throw new Object3DIOException("readRnaBlockBody6: " + expected + " excepted instead of " + word);
	}

	
	log.fine("Ending readRnaBlockBody!");

    }
*/

    /* (non-Javadoc)
     * @see rnamodel.Object3DGraphFactory#createObject3DGraph()
     */
    private void readRnaStrandBody(DataInputStream dis, RnaStrand object3d) 
		throws Object3DIOException {
	log.fine("Starting readRnaStrand!");
	assert false; //currently not supported
	String word = null; // readWord(dis);
	object3d.clear();

	word = readWord(dis);
	// sets RNA name:
	object3d.setName(word);

	Vector3D v = readVector3D(dis);
	object3d.setRelativePosition(v);

	readChildren(dis, object3d);

	// reads sequence:
	// object3d.setSequence(readSequence(dis));
	readSequence(dis); // discard
	// TODO : set reference of nucleotide children to sequence

	log.fine("Ending readRnaStrandBody!");
    }

    /** reads children objects into given object3d */
    private void readChildren(DataInputStream dis, Object3D object3d) throws Object3DIOException {
	String expected = "(children";
	String word = readWord(dis);
	if (!word.equals(expected)) {
	    throw new Object3DIOException("createObject3D: " + expected + " excepted instead of " + word);
	}
	word = readWord(dis);
	int numChildren = 0;
	try {
	    numChildren = Integer.parseInt(word);
	}
	catch (NumberFormatException e) {
	    throw new Object3DIOException("createObject3D: Could not parse number of children: " + word);	
	}

	// read individual children: recursive call, the child node can itself be the root of a whole tree!
	for (int i = 0; i < numChildren; ++i) {
	    Object3D child = readAnyObject3D(dis);
	    object3d.insertChild(child);
	}
	expected = ")"; // end of reading children
	word = readWord(dis);
	if (!word.equals(expected)) {
	    throw new Object3DIOException("createObject3D: " + expected + " excepted instead of " + word);
	}
    }

    /** returns header string. Central for switching formats! */
    public String getHeader(String className) {
	return Object3DFormats.getHeader(className, formatId);
    }

    /** returns header string of object of class "className". 
     * Central for switching formats! 
     */
    public String getFooter(String className) {
	return Object3DFormats.getFooter(className, formatId);
    }

    /** returns format mode according to Object3DFormats */
    public int getFormatId() { return formatId; }

    /** sets format mode according to Object3DFormats */
    public void setFormatId(int n) { this.formatId = n; }

    public void readHeader(DataInputStream dis, String className) throws Object3DIOException {
	String expected = getHeader(className);
	String word = readWord(dis);
	if (!word.equals(expected)) {
	    throw new Object3DIOException("Header " + expected + " excepted instead of " + word);
	}

    }

    public void readFooter(DataInputStream dis, String className) throws Object3DIOException {
	String expected = getFooter(className);
	String word = readWord(dis);
	if (!word.equals(expected)) {
	    throw new Object3DIOException("Header " + expected + " excepted instead of " + word);
	}
    }

    /** reads and creates Vector3D in format (Vector3D x y z ) */
    public Sequence readSequence(DataInputStream dis) throws Object3DIOException {
	log.fine("starting readSequence!");	
	readHeader(dis, "Sequence");
	String name = readWord(dis);
	String alphabetName = readWord(dis);
	Alphabet alphabet;
	if (alphabetName.equals("RNA")) {
	    alphabet = DnaTools.RNA_ALPHABET;
	}
	else if (alphabetName.equals("RNA_A")) {
	    alphabet = DnaTools.AMBIGUOUS_RNA_ALPHABET;
	}
	else if (alphabetName.equals("DNA")) {
	    alphabet = DnaTools.DNA_ALPHABET;
	}
	else if (alphabetName.equals("DNA_A")) {
	    alphabet = DnaTools.AMBIGUOUS_DNA_ALPHABET;
	}
	else {
	    throw new Object3DIOException("Unknown sequence type" 
					  + alphabetName);
	}
	String seq = readWord(dis); // actual sequence data
	Sequence sequence = null;
	try {
	    sequence = new SimpleSequence(seq, name, alphabet);
	}
	catch (UnknownSymbolException e) {
	    throw new Object3DIOException("Unknown sequence character encountered.");
	}
	readFooter(dis, "Sequence");
	return sequence;
    }

	/** reads and creates Vector3D in format (Vector3D x y z ) */
	public static Vector3D readVector3D(DataInputStream dis) throws Object3DIOException {
		log.fine("starting readVector!");	
		String word = readWord(dis);
		String expected = "(v3";
		if (!word.equals(expected)) {
			throw new Object3DIOException(expected + " excepted instead of " + word);
		}
		
		word = readWord(dis);
		if (word.length() == 0) {
			throw new Object3DIOException("Not x coordinate provided to Vector3D " + word);
		}
		double x = 0;
		double y = 0;
		double z = 0;
		try {
			x = Double.parseDouble(word);
		}
		catch (NumberFormatException e) {
			throw new Object3DIOException("Could not parse x coordinate for Vector3D " + word);
		}
		word = readWord(dis);
		try {
			y = Double.parseDouble(word);
		}
		catch (NumberFormatException e) {
			throw new Object3DIOException("Could not parse y coordinate for Vector3D " + word);
		}	
		word = readWord(dis);
		try {
			z = Double.parseDouble(word);
		}
		catch (NumberFormatException e) {
			throw new Object3DIOException("Could not parse z coordinate for Vector3D " + word);
		}
		word = readWord(dis);
		expected = ")";
		if (!word.equals(expected)) {
			throw new Object3DIOException("readVector3D: " + expected + " excepted instead of " + word);
		}
	    log.fine("ending readVector!");
		return new Vector3D(x, y, z);	
	}

	/** reads and creates Vector3D in format (Vector3D x y z ) */
	public static Primitive3D readPrimitive3D(DataInputStream dis) throws Object3DIOException {
		log.fine("starting readPrimitive3D!");	
		String word = readWord(dis);
		String expected = "(pg3";
		if (!word.equals(expected)) {
			throw new Object3DIOException(expected + " excepted instead of " + word);
		}

		Vector3D cv = readVector3D(dis); // color vector
		Color color = new Color((float)cv.getX(), (float)cv.getY(), (float)cv.getZ());
		word = readWord(dis);
		int numPoints = 0;
		try {
			numPoints = Integer.parseInt(word);
		}
		catch (NumberFormatException e) {
			throw new Object3DIOException("readPrimitive3D: Could not parse number of points: " + word);	
		}
		Vector3D[] points = new Vector3D[numPoints];
		for (int i = 0; i < numPoints; ++i) {
		    points[i] = readVector3D(dis);
		}
		log.fine("ending readVector!");
		return new Polygon3D(points, color);	
	}
	
	/** reads a single word from data stream */
	public static String readWord(DataInputStream dis) {
		String s = new String("");
		char c = ' ';
		
		// first skip white space
		do {
			try {
			   c = (char)dis.readByte();
			}
			catch (IOException e) {
				break; // end of file reached
			}
		}
		while (Character.isWhitespace(c));
		
		if (!Character.isWhitespace(c)) {
			s = s + c;
		}
		else {
			log.fine("found word: " + s);
			return s;
		}
		
		while (true) {
			try {
			   c = (char)dis.readByte();
			 if (!Character.isWhitespace(c)) {
				 s = s + c;
			 }
			 else {
				 break;
			 }
			}
			catch (IOException e) {
				break; // end of file reached
			}
		}
		log.fine("found word: " + s);
		return s;
	}

}
