package rnadesign.rnamodel;

import tools3d.objects3d.*;

public class Graph3D extends SimpleObject3D {

    public void insertChild(Object3D child) {
	assert child.size() == 0; // no composite objects
	assert child instanceof GraphVertexDescriptor3D;
	super.insertChild(child);
    }

    /** Make child children of node.
     * Relative position of child changes according to position of parent !*/
    public void insertChild(Object3D child, int position) {
	assert child.size() == 0; // no composite objects
	assert child instanceof GraphVertexDescriptor3D;
	super.insertChild(child, position);
    }

}
