package rnadesign.rnamodel;

import generaltools.*;
import tools3d.objects3d.*;
import tools3d.CoordinateSystem;
import tools3d.Vector3D;
import tools3d.Vector3DTools;
import graphtools.*;
import numerictools.*;
import java.util.*;

import java.util.logging.*;

/** For a given junction order and out-of-plane angle, generate a 3D Junction
 * @author Eckart Bindewald
 *
 */
public class GraphBestJunctionFactory implements Object3DFactory2 {

    private static Logger log = Logger.getLogger(PackageConstants.LOGGER_NAME);
    private static final String SCORE_NAME = "score";
    private Object3D nucleotideDB;
    private char c1 = 'G';
    private char c2 = 'C';
    private int numBasePairs = 10;
    private int offsetMax = 20;
    private double offsetStep = 1.0; // how to vary offset (1.0 means 1 Angstroem steps off distance of helix base from vertex 
    private double angleStep = 2.0*Math.PI / 120.0;

    private String rootName = "j";

    /** rotation angle between sides projected onto rotation plane, 120 degree for all symmetric 3 way junctions */
    private double rotAngle; 
    /** out of plane angle of sides and rotation plane. 45 degree for cube, 60 for tetrahedron */
    private double planeAngle;
    /** Order of junction. 3 for 3-way junction etc. */
    private int junctionOrder;
    private double collisionPenalty = 100.0;
    private double collisionDistance = 2.0;
    private boolean propagateMode = false; // true;

    public GraphBestJunctionFactory(int numSides, double planeAngle, Object3D nucleotideDB) {
	this.junctionOrder = numSides;
	this.planeAngle = planeAngle;
	this.nucleotideDB = nucleotideDB;
	this.rotAngle = 2.0 * Math.PI / numSides;
    }

    /** Generates n'th helix with certain offset from vertex angle rotation angle around its z-axis */
    private Object3DLinkSetBundle generateHelix(double offset, double angle, int n) {
	log.finest("Starting generateHelix " + offset + " " + angle + " " + n);
	CoordinateSystem3D cs = new CoordinateSystem3D(new Vector3D(0,0,0));
	cs.translate(Vector3D.EZ.mul(offset)); // translate in z-direction
	log.finest("CS after translation of " + offset + " : " + cs.toString());
	assert Math.abs(cs.getPosition().getZ() - offset) < 0.01;
	double angZ = angle;
	if (propagateMode) {
	    angZ = n * rotAngle; // other symmetry
	}
	if (angZ != 0.0) {
	    cs.rotate(Vector3D.EZ, angZ); // rotate around z-axis
	}
	log.finest("CS after first z-axis rotation of angle: " + angle + " : " + cs.toString());
	// out of plane rotation:
	double ang = 0.5 * Math.PI + planeAngle;
	Vector3D axis = new Vector3D(Vector3D.EY);
	assert axis.getX() == 0.0;
	assert axis.getY() == 1.0;
	assert axis.getZ() == 0.0;
	log.finest("Rotating around axis " + axis + " angle: " + Math.toDegrees(ang));
	cs.rotate(Vector3D.ZVEC, axis, ang);
	if (n != 0) {
	    log.finest("Rotating around z-axis " + axis + " angle: " + Math.toDegrees(n*rotAngle));
	    cs.rotate(Vector3D.ZVEC, Vector3D.EZ, n * rotAngle);
	}
	log.finest("CS after second rotation: " + cs.toString());
	BranchDescriptor3D bd = new SimpleBranchDescriptor3D(cs);
	assert bd.distance(cs) < 0.01;
	assert Math.abs(bd.getPosition().length() - offset) < 0.1;
	Object3DLinkSetBundle result = generateHelix(bd, "h" + (n+1));
	result.getObject3D().insertChild(bd);
	return result;
    }

    /** Score two helices by distance of O3* and P atoms plus penalty for atomic clashes */
    private double scoreTwoHelices(Object3D helix1, Object3D helix2) {
	String[] breakNames = {"Nucleotide3D"};
	Object3DSet strands1 = Object3DTools.collectByClassName(helix1, "RnaStrand", breakNames);
	Object3DSet strands2 = Object3DTools.collectByClassName(helix2, "RnaStrand", breakNames);
	assert strands1.size() == 2;
	assert strands2.size() == 2;
	RnaStrand strand1 = (RnaStrand)(strands1.get(0));
	RnaStrand strand2 = (RnaStrand)(strands1.get(1));
	double score = strand1.getResidue3D(0).getChild("O3*").distance(
				strand2.getResidue3D(strand2.getResidueCount()-1).getChild("P"));
	int numColl = AtomTools.countExternalCollisions(helix1, helix2, collisionDistance);
	score += numColl * collisionPenalty;
	return score;
    }

    /** Ensures that branch descriptors are not at same location */
    private boolean verifyBranchDescriptors(List<BranchDescriptor3D> bdList) {
	System.out.println("BranchDescriptors: ");
	for (int i = 0; i < bdList.size(); ++i) {
	    BranchDescriptor3D bd = bdList.get(i);
	    System.out.println(bd.getPosition() + " " + bd.getDirection());
	}
	for (int i = 0; i < bdList.size(); ++i) {
	    BranchDescriptor3D bd1 = bdList.get(i);
	    for (int j = i+1; j < bdList.size(); ++j) {
		BranchDescriptor3D bd2 = bdList.get(j);
		if (bd1.distance(bd2) < 0.1) {
		    return false;
		}
	    }
	}
	return true;
    }

    private Object3DLinkSetBundle generateHelices(double offset, double angle) {
	assert junctionOrder >= 2;
	Object3D root = new SimpleObject3D(rootName);
	LinkSet links = new SimpleLinkSet();
	List<BranchDescriptor3D> bdList = new ArrayList<BranchDescriptor3D>();
	for (int i = 0; i < junctionOrder; ++i) {
	    Object3DLinkSetBundle helixBundle = generateHelix(offset, angle, i);
	    Object3D obj = helixBundle.getObject3D();
	    root.insertChild(obj);
	    links.merge(helixBundle.getLinks());
	    bdList.add((BranchDescriptor3D)(obj.getChild(obj.size()-1)));
	}
	root.setProperty(SCORE_NAME, "" + scoreTwoHelices(root.getChild(0), root.getChild(1)));
	// assert (offset <= 0.0) || verifyBranchDescriptors(bdList);
	return new SimpleObject3DLinkSetBundle(root, links);
    }

    /** Finds best set of helices for given out-of-plane angle and junction order */
    private Object3DLinkSetBundle generateBestHelices() {
	log.info("Starting generateBestHelices: " + junctionOrder + " angle: " + Math.toDegrees(planeAngle));
	Object3DLinkSetBundle bestHelices = null;
	double bestScore = 1e10;
	for (int offsetCount = 0; offsetCount < offsetMax; ++offsetCount) {
	    double offset = offsetStep * offsetCount;
	    int angleCountMax = 1 + (int)(2.0 * Math.PI / angleStep);
	    for (int angleCount = 0; angleCount < angleCountMax; ++angleCount) {
		double angle = angleCount * angleStep;
		if (angle >= 2.0 * Math.PI) {
		    break;
		}
		log.fine("Working on offset, angle: " + offset + " " + Math.toDegrees(angle));
		// assert angle < 2.0 * Math.PI;
		Object3DLinkSetBundle bundle = generateHelices(offset, angle);
		double score = Double.parseDouble(bundle.getObject3D().getProperty(SCORE_NAME));
		if ((score + 0.001) < bestScore) {
		    bestScore = score;
		    bestHelices = bundle;
		    log.info("New best score found at offset , angle : " + offset + " " + Math.toDegrees(angle)
			     + " : " + score);
		}
	    }
	}
	log.info("Finished generateBestHelices with score " + bestScore);
	return bestHelices;
    }

    /** Central method for starting algorithm. TODO: add bridges. */
    public Object3DLinkSetBundle generate() {
	return generateBestHelices(); 
    }

	/*
    public Object3DLinkSetBundle generate() throws Object3DException {
	assert corner != null && neighbors != null;
	log.info("Starting to generate Junction from corner position " + corner);
	if (neighbors.length < 2) {
	    throw new Object3DException("At least two neighbor positions must be specified, found only: " + neighbors.length);
	}
	bds = new ArrayList<BranchDescriptor3D>();
	helices = new ArrayList<Object3DLinkSetBundle>();
	// place branch descriptors
	for (int i = 0; i < neighbors.length; ++i) {
	    BranchDescriptor3D bd = generateBranchDescriptor(corner, neighbors[i]);
	    bd.setName("bd" + (i+1));
	    bds.add(bd);
	    log.info("Placed branch descriptor " + (i+1) + " : " + bd.infoString());
	    assert Math.abs(bd.getPosition().distance(corner) - offset) < 0.1;
	}
	log.info("Placed " + bds.size() + " branch descriptors.");
	// generate helices corresponding to branch descriptors:
	for (int i = 0; i < neighbors.length; ++i) {
	    String helixName = "h" + (i+1);
	    helices.add(generateHelix(bds.get(i), helixName));
	}
	log.info("Generated " + helices.size() + " helices.");
	optimizeHelices();

	return generateFinalBundle();
    }
	*/

    private Object3DLinkSetBundle generateHelix(BranchDescriptor3D bd, String helixName) {
	return ConnectJunctionTools.generateIdealStem(bd, c1, c2, helixName, nucleotideDB, numBasePairs);
    }

    public double getCollisionDistance() { return this.collisionDistance; }

    public boolean getPropagateMode() { return this.propagateMode; }
    
    public void setCollisionDistance(double x) { this.collisionDistance = x; }


    public void setPropagateMode(boolean b) { this.propagateMode = b; }

}

