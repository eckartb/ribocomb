package rnadesign.rnamodel;

import tools3d.objects3d.*;

/** for a given graph, return grow commands that might generatate this graph */
public abstract class GraphGrowConnectivityFactory implements GrowConnectivityFactory {

    protected Object3D root;
    protected Object3DSet vertices;
    protected LinkSet links;
    protected StrandJunctionDB junctionDB;
    protected StrandJunctionDB kissingLoopDB;

    public GraphGrowConnectivityFactory(Object3DLinkSetBundle graph,
					StrandJunctionDB junctionDB,
					StrandJunctionDB kissingLoopDB) {
	assert graph != null;
	this.root = graph.getObject3D();
	this.links = graph.getLinks();
	this.junctionDB = junctionDB;
	this.kissingLoopDB = kissingLoopDB;
	// this.vertices = extractConnectedEdges(this.root, this.links);
    }

    /** extracts only those vertices that are connected by links */
    public static Object3DSet extractConnectedVertices(Object3D root,
						       LinkSet links) {
	Object3DSet result = new SimpleObject3DSet();
	Object3DSet set = new SimpleObject3DSet(root);
	for (int i = 0; i < set.size(); ++i) {
	    if (links.getLinkOrder(set.get(i)) > 0) {
		result.add(set.get(i));
	    }
	}
	return result;
    }

}
