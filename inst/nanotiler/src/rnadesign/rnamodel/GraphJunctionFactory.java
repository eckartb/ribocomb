package rnadesign.rnamodel;

import generaltools.*;
import tools3d.objects3d.*;
import tools3d.CoordinateSystem;
import tools3d.Vector3D;
import tools3d.Vector3DTools;
import graphtools.*;
import numerictools.*;
import java.util.*;

import java.util.logging.*;

/** For a given graph corner and neighbor directions, generate a 3D Junction
 * @author Eckart Bindewald
 *
 */
public class GraphJunctionFactory implements Object3DFactory2 {

    private static Logger log = Logger.getLogger(PackageConstants.LOGGER_NAME);
    private Object3D nucleotideDB;
    private List<Object3DLinkSetBundle> helices;
    private List<BranchDescriptor3D> bds;
    private char c1 = 'G';
    private char c2 = 'C';
    private int logInterval = 10000;
    private int numBasePairs = 10;
    private double offset = 20.0;
    private double distMin = 5.0; // 1.0;
    private double distMax = 8.0;
    private double limitScore = 0.0;
    private Vector3D corner;
    private Vector3D[] neighbors;
    private double[] angles;
    private int angleDiv = 120; // how many angles are tried
    private double angleStep = 2.0 * Math.PI / angleDiv;
    private String rootName = "j";
    private SimpleConstraintDouble constraint;

    public GraphJunctionFactory(Vector3D corner, Vector3D[] neighbors, Object3D nucleotideDB) {
	assert corner != null;
	assert neighbors != null;
	assert nucleotideDB != null;
	this.corner = corner;
	this.neighbors = neighbors;
	this.nucleotideDB = nucleotideDB;
	this.angles = new double[neighbors.length];
	for (int i = 0; i < angles.length; ++i) {
	    angles[i] = 0.0;
	}
	initConstraint();
    }

    private void initConstraint() {
	this.constraint = new SimpleConstraintDouble(distMin, distMax);
	constraint.setSquareMode(false);
    }

    public Object3DLinkSetBundle generate() throws Object3DException {
	assert corner != null && neighbors != null;
	log.info("Starting to generate Junction from corner position " + corner);
	if (neighbors.length < 2) {
	    throw new Object3DException("At least two neighbor positions must be specified, found only: " + neighbors.length);
	}
	bds = new ArrayList<BranchDescriptor3D>();
	helices = new ArrayList<Object3DLinkSetBundle>();
	// place branch descriptors
	for (int i = 0; i < neighbors.length; ++i) {
	    BranchDescriptor3D bd = generateBranchDescriptor(corner, neighbors[i]);
	    bd.setName("bd" + (i+1));
	    bds.add(bd);
	    log.info("Placed branch descriptor " + (i+1) + " : " + bd.infoString());
	    assert Math.abs(bd.getPosition().distance(corner) - offset) < 0.1;
	}
	log.info("Placed " + bds.size() + " branch descriptors.");
	// generate helices corresponding to branch descriptors:
	for (int i = 0; i < neighbors.length; ++i) {
	    String helixName = "h" + (i+1);
	    helices.add(generateHelix(bds.get(i), helixName));
	}
	log.info("Generated " + helices.size() + " helices.");
	Properties properties = optimizeHelices();

	return generateFinalBundle(properties);
    }

    private BranchDescriptor3D generateBranchDescriptor(Vector3D cornerPos, Vector3D neighborPos) throws Object3DException {
	Vector3D z = neighborPos.minus(cornerPos);
	if (z.lengthSquare() == 0.0) {
	    throw new Object3DException("Corner coincided with neighbor position!");
	}
	z.normalize();
	Vector3D base = cornerPos.plus(z.mul(offset)); // base position of new helix
	assert Math.abs(base.distance(cornerPos) - offset) < 0.1;
	Vector3D y = Vector3DTools.generateRandomOrthogonalDirection(z);
	Vector3D x = y.cross(z);
	CoordinateSystem3D cs = new CoordinateSystem3D(base, x, y, z);
	assert cs.getPosition().distance(base) < 0.01;
	log.info("Using coordinate system " + cs);
	BranchDescriptor3D bd = new SimpleBranchDescriptor3D(cs);
	double dist = bd.getPosition().distance(cornerPos);
	assert Math.abs(dist - offset) < 0.1;
	return bd;
    }

    private Object3DLinkSetBundle generateFinalBundle(Properties properties) {
	Object3D root = new SimpleObject3D();
	LinkSet links = new SimpleLinkSet();
	root.setName(rootName);
	for (int i = 0; i < helices.size(); ++i) {
	    Object3D helix = helices.get(i).getObject3D();
	    // rotate helix
	    BranchDescriptor3D bd = bds.get(i);
	    helix.rotate(bd.getBasePosition(), bd.getDirection(), angles[i]);
	    root.insertChild(helix);
	    links.merge(helices.get(i).getLinks());
	}
	for (int i = 0; i < bds.size(); ++i) {
	    BranchDescriptor3D bd = bds.get(i);
	    // rotate branch descriptor:
	    bd.rotate(bd.getBasePosition(), bd.getDirection(), angles[i]);
	    root.insertChild(bd);
	}
	root.setProperties(properties);
	return new SimpleObject3DLinkSetBundle(root, links);
    }

    private Object3DLinkSetBundle generateHelix(BranchDescriptor3D bd, String helixName) {
	return ConnectJunctionTools.generateIdealStem(bd, c1, c2, helixName, nucleotideDB, numBasePairs);
    }

    private Properties optimizeHelices() {
	log.info("Starting to optimize helices!");
	IntegerArrayGenerator intGen = new IntegerArrayGenerator(angles.length, angleDiv);
	int[] bestComb = intGen.get();
	double bestScore = computeHelixScore(bestComb, -1.0);
	int count = 0;
	while (intGen.hasNext()) {
	    ++count;
	    double score = computeHelixScore(intGen.next(), bestScore);
	    if (score < bestScore) {
		bestScore = score;
		bestComb = intGen.get();
		log.info("New best optimization score at iteration " + count + " : " + bestScore);
		if (score <= limitScore) {
		    log.info("Limit score reached, quitting loop!");
		    break; 
		}
	    }
	}
	
	for (int i = 0; i < bestComb.length; ++i) {
	    angles[i] = bestComb[i] * angleStep;
	}
	IntegerArrayTools.writeArray(System.out, bestComb);
	log.info("Finished to optimize helices!");
	Properties properties = new Properties();
	properties.setProperty("best_score", "" + bestScore);
	return properties;
    }
    
    /** For given set of angles, compute scoring function. Quit if cutoff > 0.0 and score greater than cutoff. */
    private double computeHelixScore(int[] angleCombination, double cutoff) {
	    assert angleCombination != null && angleCombination.length == angles.length;
	    double score = 0.0;
	    for (int i = 1; i < angles.length; ++i) {
		score += computeHelixScore(angleCombination, cutoff, i-1, i);
	    }
	    score += computeHelixScore(angleCombination, cutoff, angles.length - 1, 0);
	    return score;
    }

    /** For given set of angles, compute scoring function. Quit if cutoff > 0.0 and score greater than cutoff.
     * @param id1 id of helix to be connected with its 5' end of the first strand
     * @param id2 id of helix to be connected with its 3' end of the second strand
     */
    private double computeHelixScore(int[] angleCombination, double cutoff, int id1, int id2) {
	assert id1 != id2;
	double score = 0.0;
	Vector3D p1 = computeConnectingPosition1(angleCombination, id1);
	Vector3D p2 = computeConnectingPosition2(angleCombination, id2);
	double dist = p1.distance(p2);
	score = constraint.getDiff(dist);
	return score;
    }

    /** 
     * @param id id of helix to be connected with its 5' end of the first strand
     */
    private Vector3D computeConnectingRawPosition1(int id) {
	Object3D root = helices.get(id).getObject3D();
	int idx = root.getIndexOfChild(1, "RnaStrand");
	assert idx >= 0;
	RnaStrand strand = (RnaStrand)(root.getChild(idx));
	Residue3D residue = strand.getResidue3D(strand.getResidueCount()-1);
	Object3D atom = residue.getChild("O3*");
	double dist = atom.distance(bds.get(id));
	if (dist > 15.0) {
	    log.warning("Strange distance: " + bds.get(id).infoString() + " and " + atom);
	}
	assert dist < 15.0; // must be close to base of branch descriptor
	return atom.getPosition();
    }

    /** 
     * @param id id of helix to be connected with its 3' end of the first strand
     */
    private Vector3D computeConnectingRawPosition2(int id) {
	Object3D root = helices.get(id).getObject3D();
	int idx = root.getIndexOfChild(0, "RnaStrand");
	assert idx >= 0;
	RnaStrand strand = (RnaStrand)(root.getChild(idx));
	Residue3D residue = strand.getResidue3D(0);
	Object3D atom = residue.getChild("P");
	assert atom.distance(bds.get(id)) < 11.0; // not too far from branch descriptor center
	return atom.getPosition();
    }

    /** For given set of angles, compute scoring function. Quit if cutoff > 0.0 and score greater than cutoff.
     * @param id1 id of helix to be connected with its 5' end of the first strand
     */
    private Vector3D computeConnectingPosition1(int[] angleCombination, int id) {
	Vector3D p = computeConnectingRawPosition1(id);
	double angle = angleStep * angleCombination[id];
	BranchDescriptor3D bd = bds.get(id);
	double dOrig = bd.getBasePosition().distance(p);
	p.rotate(bd.getBasePosition(), bd.getDirection(), angle);
	double dNew = bd.getBasePosition().distance(p);
	assert (Math.abs(dOrig - dNew) < 0.01); // distance to rotation center not changed
	return p;
    }

    /** For given set of angles, compute scoring function. Quit if cutoff > 0.0 and score greater than cutoff.
     * @param id1 id of helix to be connected with its 5' end of the first strand
     */
    private Vector3D computeConnectingPosition2(int[] angleCombination, int id) {
	Vector3D p = computeConnectingRawPosition2(id);
	double angle = angleStep * angleCombination[id];
	p.rotate(bds.get(id).getBasePosition(), bds.get(id).getDirection(), angle);
	return p;
    }

    public double getOffset() { return offset; }

    /** Sets number of angle steps per helix */
    public void setAngleDiv(int n) { this.angleDiv = n; }

    public void setDistMax(double distMax) {
	this.distMax = distMax;
	initConstraint();
    }

    public void setDistMin(double distMin) { 
	this.distMin = distMin; 
	initConstraint();
    }

    public void setOffset(double offset) { this.offset = offset; }

}

