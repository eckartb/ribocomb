package rnadesign.rnamodel;

import tools3d.*;
import tools3d.objects3d.*;
import graphtools.*;
import java.util.logging.*;

public class Grid3DTools {

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    /** returns position of "lower left" corner of a unit cell with coordinates n1, n2, n3 */
    public static Vector3D unitCellPos(Vector3D startPos, Vector3D v1, Vector3D v2, Vector3D v3, 
			 int n1, int n2, int n3) {
	Vector3D result = new Vector3D(startPos);
	result.add(v1.mul((double)n1));
	result.add(v2.mul((double)n2));
	result.add(v3.mul((double)n3));
	return result;
    }

    /** Generates cubic grid with objects and links. StartPos is "lower left" corner of cell 0 0 0.
     * The unit cell vectors are v1, v2, v3. The number of cells are n1*n2*n3 
     */
    public static Object3DLinkSetBundle generateCubicGrid(Vector3D startPos, Vector3D v1, Vector3D v2, Vector3D v3, 
					    int n1, int n2, int n3, String name) {
	Object3D[][][] grid = new Object3D[n1][n2][n3];
	Object3D groupObj = new SimpleObject3D(startPos);
	groupObj.setName(name);
	for (int i = 0; i < n1; ++i) {
	    for (int j = 0; j < n2; ++j) {
		for (int k = 0; k < n3; ++k) {
		    Object3D obj = new SimpleObject3D(unitCellPos(startPos, v1, v2, v3, i,j,k));
		    String objName = name + "." + i + "." + j + "." + k;
		    obj.setName(objName);
		    grid[i][j][k] = obj;
		    groupObj.insertChild(obj);
		}
	    }
	}
	// add links:
	LinkSet links = new SimpleLinkSet();
	for (int i = 0; i < n1; ++i) {
	    for (int j = 0; j < n2; ++j) {
		for (int k = 0; k < n3; ++k) {
		    if (i > 0) {
			Link link1 = new SimpleLink(grid[i][j][k], grid[i-1][j][k]);
			links.add(link1);
		    }
		    if (j > 0) {
			Link link2 = new SimpleLink(grid[i][j][k], grid[i][j-1][k]);
			links.add(link2);
		    }
		    if (k > 0) {
			Link link3 = new SimpleLink(grid[i][j][k], grid[i][j][k-1]);
			links.add(link3);
		    }
		}
	    }
	}
	Object3DLinkSetBundle result = new SimpleObject3DLinkSetBundle(groupObj, links);

	// log.fine("Trying to find cyclical paths:");
	// findCyclicalPaths(groupObj, links, 4);
	// log.fine("Finished trying to find cyclical paths:");
	return result;
    }

    /** Generates cubic grid with objects and links. StartPos is "lower left" corner of cell 0 0 0.
     * The unit cell vectors are v1, v2, v3. The number of cells are n1*n2*n3 
     */
    public static Object3DLinkSetBundle generateGraphitGrid(Vector3D startPos, Vector3D v1, Vector3D v2, Vector3D v3, 
							    int n1, int n2, int n3, String name) {
	Object3D[][][] grid = new Object3D[n1][n2][n3];
	Object3D groupObj = new SimpleObject3D(startPos);
	groupObj.setName(name);
	for (int i = 0; i < n1; ++i) {
	    for (int j = 0; j < n2; ++j) {
		for (int k = 0; k < n3; ++k) {
		    Object3D obj = new SimpleObject3D(unitCellPos(startPos, v1, v2, v3, i,j,k));
		    String objName = name + "." + i + "." + j + "." + k;
		    obj.setName(objName);
		    grid[i][j][k] = obj;
		    groupObj.insertChild(obj);
		}
	    }
	}
	// add links:
	LinkSet links = new SimpleLinkSet();
	for (int i = 0; i < n1; ++i) {
	    for (int j = 0; j < n2; ++j) {
		for (int k = 0; k < n3; ++k) {
		    if (k == 0) {
			if (i > 0) {
			    Link link1 = new SimpleLink(grid[i][j][k], grid[i-1][j][k]);
			    links.add(link1);
			}
			if (j > 0) {
			    Link link2 = new SimpleLink(grid[i][j][k], grid[i][j-1][k]);
			    links.add(link2);
			}
			if (k > 0) {
			    Link link3 = new SimpleLink(grid[i][j][k], grid[i][j][k-1]);
			    links.add(link3);
			}
			if ((j > 0) && ((i+1)<grid.length)) {
			    Link link4 = new SimpleLink(grid[i][j][k], grid[i+1][j-1][k]);
			    links.add(link4);
			}
		    }
		    else {
			Link link = new SimpleLink(grid[i][j][k], grid[i][j][k-1]);
			links.add(link);
		    }
		}
	    }
	}
	
	// remove all objects that have 6 or more links
	for (int i = 0; i < n1; ++i) {
	    for (int j = 0; j < n2; ++j) {
		for (int k = 0; k < n3; ++k) {
		    Object3D obj = grid[i][j][k];
		    if (links.getLinkOrder(obj) >= 6) {
			grid[i][j][k] = null;
			groupObj.removeChild(obj);
			links.removeBadLinks(groupObj);
		    }
		}
	    }
	}
	// remove all objects that no links
	for (int i = 0; i < n1; ++i) {
	    for (int j = 0; j < n2; ++j) {
		for (int k = 0; k < n3; ++k) {
		    Object3D obj = grid[i][j][k];
		    if ((obj != null) && (links.getLinkOrder(obj) == 0)) {
			grid[i][j][k] = null;
			groupObj.removeChild(obj);
		    }
		}
	    }
	}

	Object3DLinkSetBundle result = new SimpleObject3DLinkSetBundle(groupObj, links);

	// log.fine("Trying to find cyclical paths:");
	// findCyclicalPaths(groupObj, links, 4);
	// log.fine("Finished trying to find cyclical paths:");
	return result;
    }

    /** Generates cubic grid with objects and links. StartPos is "lower left" corner of cell 0 0 0.
     * The unit cell vectors are v1, v2, v3. The number of cells are n1*n2*n3 
     */
    public static Object3DLinkSetBundle generateGraphitGrid(Vector3D startPos, double a, double c,
							    int n1, int n2, String name) {
	int n3 = 2;
	double ang = Math.PI/3.0; // corresponds to 60 degree
	Vector3D v1 = new Vector3D(1.0, 0.0, 0.0);
	Vector3D v2 = new Vector3D(Math.sin(ang), Math.cos(ang), 0);
	Vector3D v3 = new Vector3D(0, 0, 1);
	v1.scale(a);
	v2.scale(a);
	v3.scale(c);
	
	Object3DLinkSetBundle result = generateGraphitGrid(startPos, v1, v2, v3, n1, n2, n3, name);

	// log.fine("Trying to find cyclical paths:");
	// findCyclicalPaths(groupObj, links, 4);
	// log.fine("Finished trying to find cyclical paths:");
	return result;
    }

    /** generates graph (implemented as connection matrix) from set of objects and set of links */
    public static GraphBase generateGraph(Object3D rootNode, LinkSet links) {
	Object3DSet object3DSet = new SimpleObject3DSet(rootNode);
	int nn = object3DSet.size();
	GraphBase graph = new SimpleGraph(nn);
	for (int i = 0; i < nn; ++i) {
	    for (int j = i + 1; j < nn; ++j) {
		if (links.find(object3DSet.get(i), object3DSet.get(j)) != null) {
		    graph.setConnected(i, j, true);
		}
	    }
	}
	return graph;
    }

    public static void findCyclicalPaths(Object3D rootNode, LinkSet links, int pathOrder) {
	log.fine("Starting findCyclicalPaths!");
	GraphBase graph = generateGraph(rootNode, links);
// 	log.fine("Generated graph:");
// 	log.fine(graph.toString());
	GraphBaseOrderSet graphSet = new SimpleGraphBaseOrderSet(graph, pathOrder);
	log.fine("Generated path set:");
	log.fine(graphSet.toString());
	for (int i = 0; i < graphSet.size(); ++i) {
	    IntegerListList cyclicalPaths = graphSet.getCyclicalPaths(i);
	    log.fine("Checking object " + i + " : ");
	    if ((cyclicalPaths != null) && (cyclicalPaths.size() > 0)) {
		log.fine("Number of cyclical paths of node " + i);
		for (int j = 0; j < cyclicalPaths.size(); ++j) {
		    IntegerList path = cyclicalPaths.get(j);
		    log.fine("Path " + j + " : ");
		    for (int k = 0; k < path.size(); ++k) {
			log.fine(" " + path.get(k));
		    }
		    log.fine("");
		}
	    }
	    else {
		log.fine("No cyclical paths for node " + i);
	    }
	}
	log.fine("Finished findCyclicalPaths!");
    }
    
}
