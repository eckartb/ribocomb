package rnadesign.rnamodel;

import tools3d.objects3d.*;
import graphtools.*;

public class GridPathTools {

    /** generates connection graph from set of object3d and links */
    public static GraphBase generateGraph(Object3DSet objects, LinkSet links) {

	GraphBase graph = new SimpleGraph(objects.size());
	for (int i = 0; i < objects.size(); ++i) {
	    for (int j = i+1; j < objects.size(); ++j) {
		Link connection = links.find(objects.get(i), objects.get(j));
		if (connection != null) {
		    graph.setConnected(i,j,true);
		}
	    }
	}
	return graph;
    }

}
