package rnadesign.rnamodel;

import tools3d.objects3d.*;

public interface GridTiler {

    /** returns set of strand that trace the given set of objects */
    Object3DLinkSetBundle generateTiling(Object3D objectSet, LinkSet links, String baseName, char defaultSequenceChar, boolean onlyFirstPathMode);

}
