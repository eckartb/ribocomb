package rnadesign.rnamodel;

import java.util.*;
import java.util.logging.*;
import generaltools.CollectionTools;
import generaltools.TestTools;

import org.testng.*;
import org.testng.annotations.*;

/** class representing input of Grow command */
public class GrowConnectivity {
    
    public static final String HELIX_PREFIX = DBElementConnectionDescriptor.HELIX_PREFIX;

    private static Logger log = Logger.getLogger(PackageConstants.LOGFILE_DEFAULT);

    private List<DBElementDescriptor> buildingBlocks;
    private List<DBElementConnectionDescriptor> connections;
    private boolean graphFlag = false; // if true, each block occurs only one, describes one node in graph
    private int numGenerations = 4;
    private Set<String> topologies;
    private int sizeMax = -1; // if greater zero, specify max size
    private int nonEquivalentBuildingBlockCount = 0;
    private int nonEquivalentConnectionCount = 0;
    private boolean debugMode = false;

    public GrowConnectivity() { 
	this.buildingBlocks = new ArrayList<DBElementDescriptor>();
	this.connections = new ArrayList<DBElementConnectionDescriptor>();
	this.topologies = new HashSet<String>();
	// update();
	// assert validate(); does not validate, because no building blocks are defined
    }

    public GrowConnectivity(List<DBElementDescriptor> buildingBlocks, List<DBElementConnectionDescriptor> connections,
			    Set<String> topologies, int numGenerations) { 
	this();
	if (buildingBlocks != null) {
	    this.buildingBlocks = buildingBlocks;
	}
	if (connections != null) {
	    this.connections = connections;
	}
	if (topologies != null) {
	    this.topologies = topologies;
	}
	this.numGenerations = numGenerations;
	update();
	assert validate();
    }

    private List<DBElementConnectionDescriptor> generateAllConnections(DBElementDescriptor block1, DBElementDescriptor block2, 
								       int minLen, int maxLen) {
	List<DBElementConnectionDescriptor> result = new ArrayList<DBElementConnectionDescriptor>();
	log.fine("Starting to generate all connections for two blocks of orders " + block1.getOrder() + " and " + block2.getOrder() 
		 + " based on blocks " + block1 + " and " + block2);
	for (int hx1 = 0; hx1 < block1.getOrder(); ++hx1) {
	    for (int hx2 = 0; hx2 < block2.getOrder(); ++hx2) {
		for (int len = minLen; len <= maxLen; ++len) {
		    DBElementConnectionDescriptor conn = new DBElementConnectionDescriptor(block1, block2, hx1, hx2, len);
		    log.finest("Generated building block connectivity " + conn.toString());
		    result.add(conn);
		}
	    }
	}
	return result;
    }
    
    public void synthesizeAllConnections(int minLen, int maxLen) {
	connections = new ArrayList<DBElementConnectionDescriptor>();
	for (int i = 0; i < buildingBlocks.size(); ++i) {
	    for (int j = i;  j < buildingBlocks.size(); ++j) {
		connections.addAll(generateAllConnections(buildingBlocks.get(i), buildingBlocks.get(j), minLen, maxLen));
	    }
	}
    }

    public void randomizeConnectivityOrder() {
	Collections.shuffle(connections);
    }

    public void add(DBElementDescriptor buildingBlock) { buildingBlocks.add(buildingBlock); update(); }

    public void add(DBElementConnectionDescriptor connection) { connections.add(connection); update(); }

    public void addTopology(String topology) { this.topologies.add(topology); update(); }

    public List<DBElementDescriptor> getBuildingBlocks() { return buildingBlocks; }

    /** Returns set of representative building blocks; one type of representative for each different database entry */
    private List<DBElementDescriptor> computeBuildingBlockRepresentatives() { 
	List<DBElementDescriptor> result = new ArrayList<DBElementDescriptor>();
	Set<DBElementDescriptor> testSet = new HashSet<DBElementDescriptor>();
	for (DBElementDescriptor dbe : buildingBlocks) {
	    DBElementDescriptor dbe2 = (DBElementDescriptor)(dbe.clone());
	    dbe2.setDescriptorId(0); // always set same descriptor id: this reduces info to what database junction was used.
	    if (!testSet.contains(dbe2)) {
		testSet.add(dbe2);
		result.add(dbe2);
	    }
	    else {
		DBElementDescriptor rep = (DBElementDescriptor)(CollectionTools.contains(testSet, dbe2));
		result.add(rep);
	    }
	}
	assert result.size() == buildingBlocks.size();
	return result;
    }

    /** Returns number of building blocks derived from same database entry */
    private int computeNonEquivalentBuildingBlockCount() { 
	assert validate();
	Set<DBElementDescriptor> testSet = new HashSet<DBElementDescriptor>();
	for (DBElementDescriptor dbe : buildingBlocks) {
	    DBElementDescriptor dbe2 = (DBElementDescriptor)(dbe.clone());
	    dbe2.setDescriptorId(0); // always set same descriptor id: this reduces info to what database junction was used.
	    testSet.add(dbe2);
	}
	assert testSet.size() <= buildingBlocks.size();
	return testSet.size();
    }

    @Test (groups={"new"})
    public void testComputeNonEquivalentBuildingBlockCount() {
	String methodName = "testComputeNonEquivalentBuildingBlockCount";
	System.out.println(TestTools.generateMethodHeader(methodName));
	DBElementDescriptor db1 = new DBElementDescriptor(2, 1, DBElementDescriptor.JUNCTION_TYPE, 1);
	DBElementDescriptor db2 = new DBElementDescriptor(2, 1, DBElementDescriptor.JUNCTION_TYPE, 2);
	DBElementDescriptor db3 = new DBElementDescriptor(2, 1, DBElementDescriptor.JUNCTION_TYPE, 3);
	List<DBElementDescriptor> bbList = new ArrayList<DBElementDescriptor>();
	bbList.add(db1);
	bbList.add(db2);
	bbList.add(db3);
	GrowConnectivity connectivity = new GrowConnectivity(bbList, null, null,3);
	System.out.println("Defined grow connectivity: " + connectivity);
	assert connectivity.validate();
	assert connectivity.getBuildingBlocks().size() == 3;
	assert connectivity.getNonEquivalentBuildingBlockCount() == 1;
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test (groups={"new"})
    public void testComputeNonEquivalentConnectionCount() {
	String methodName = "testComputeNonEquivalentConnectionCount";
	System.out.println(TestTools.generateMethodHeader(methodName));
	DBElementDescriptor db1 = new DBElementDescriptor(2, 1, DBElementDescriptor.JUNCTION_TYPE, 0);
	DBElementDescriptor db2 = new DBElementDescriptor(2, 1, DBElementDescriptor.JUNCTION_TYPE, 1);
	DBElementDescriptor db3 = new DBElementDescriptor(2, 1, DBElementDescriptor.JUNCTION_TYPE, 2);
	int bp = 5;
	String hx1 = "(hxend)1";
	String hx2 = "(hxend)2";
	String hx3 = "(hxend)3";
	DBElementConnectionDescriptor c1 = new DBElementConnectionDescriptor(db1, db2, hx1, hx2, bp);
	DBElementConnectionDescriptor c2 = new DBElementConnectionDescriptor(db2, db3, hx1, hx2, bp);
	DBElementConnectionDescriptor c3 = new DBElementConnectionDescriptor(db3, db1, hx1, hx2, bp);
	List<DBElementDescriptor> bbList = new ArrayList<DBElementDescriptor>();
	List<DBElementConnectionDescriptor> connList = new ArrayList<DBElementConnectionDescriptor>();
	bbList.add(db1);
	bbList.add(db2);
	bbList.add(db3);
	connList.add(c1);
	connList.add(c2);
	connList.add(c3);
	System.out.println("Defined 3 connections: ");
	for (DBElementConnectionDescriptor c : connList) {
	    System.out.println(c);
	}
	GrowConnectivity connectivity = new GrowConnectivity(bbList, connList, null,3);
	System.out.println("Defined grow connectivity: " + connectivity);
	assert connectivity.validate();
	assert connectivity.getBuildingBlocks().size() == 3;
	assert connectivity.getNonEquivalentBuildingBlockCount() == 1;
	assert connectivity.getConnections().size() == 3;
	assert connectivity.getNonEquivalentConnectionCount() == 1;
	// difficult: add reverse connection:
	// this connection should be equivalent to c3!!!
	DBElementConnectionDescriptor c4 = new DBElementConnectionDescriptor(db1, db3, hx2, hx1, bp);
	System.out.println("Adding new connection: " + c4);
	System.out.println("Should be same as previous connection: " + c3);
	assert c4.equals(c3);
	connectivity.add(c4);
	System.out.println("Defined grow connectivity after adding " + c4 + " : " + connectivity);
	assert connectivity.getBuildingBlocks().size() == 3;
	assert connectivity.getNonEquivalentBuildingBlockCount() == 1;
	assert connectivity.getConnections().size() == 4;
	assert connectivity.getNonEquivalentConnectionCount() == 1;
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Returns number of different connections if building blocks are represented through the database entry they were derived from. */
    private int computeNonEquivalentConnectionCount() {
	assert validate();
	// compute new set of connections with respect to non-equivalent set of building blocks
	Set<DBElementConnectionDescriptor> testSet = new HashSet<DBElementConnectionDescriptor>();
	for (DBElementConnectionDescriptor dbc : connections) {
	    DBElementConnectionDescriptor dbc2 = (DBElementConnectionDescriptor)(dbc.clone());
	    DBElementDescriptor deb1 = dbc2.getDescriptor1();
	    DBElementDescriptor deb2 = dbc2.getDescriptor2();
	    assert deb1.compareTo(deb2) <= 0;
	    deb1.setDescriptorId(0);
	    deb2.setDescriptorId(0);
	    // this might lead to a reordering!
	    DBElementConnectionDescriptor dbc3 = new DBElementConnectionDescriptor(deb1, deb2, dbc2.getHelixendName1(),dbc2.getHelixendName2(),
										   dbc2.getBasePairCount());
	    testSet.add(dbc3);
	}
	if (debugMode) {
	    log.info("Set of non-equivalent connections: ");
	    for (DBElementConnectionDescriptor conn : testSet) {
		System.out.println("" + conn);
	    }
	}
	assert testSet.size() <= connections.size();
	return testSet.size();	
    }

    /** Returns number of building blocks derived from same database entry */
    public int getNonEquivalentBuildingBlockCount() { 
	assert nonEquivalentBuildingBlockCount == computeNonEquivalentBuildingBlockCount();
	return nonEquivalentBuildingBlockCount;
    }

    /** Returns number of different connections if building blocks are represented through the database entry they were derived from. */
    public int getNonEquivalentConnectionCount() {
	assert nonEquivalentConnectionCount == computeNonEquivalentConnectionCount();
	return nonEquivalentConnectionCount;
    }

    /** Returns defined connections. */
    public List<DBElementConnectionDescriptor> getConnections() { return connections; }

    /** returns number of generations */
    public int getNumGenerations() { return numGenerations; }

    /* Returns defined topology descriptors */
    public Set<String> getTopologies() { return topologies; }

    /** Maximum size of structure */
    public int getSizeMax() { return sizeMax; }

    /** Returns graph mode flag. */
    public boolean isGraphFlag() { return graphFlag; }

    public void setBuildingBlocks(List<DBElementDescriptor> _buildingBlocks) {
	this.buildingBlocks = _buildingBlocks;
    }

    /** Sets graph mode flag. */
    public void setGraphFlag(boolean b) { this.graphFlag = b; }

    /** Sets number of generations */
    public void setNumGenerations(int n) { this.numGenerations = n; }

    /** Sets maximum size of structure to be built. */
    public void setSizeMax(int size) { this.sizeMax = size; }

    /** Returns string representatino of building block descriptor. */
    private static String toStringBlock(DBElementDescriptor dbe) {
	String result = DBElementDescriptor.getTypeString(dbe.getType()) + "," + dbe.getOrder() + "," + (dbe.getId()+1) + "," + dbe.getDescriptorId();
	return result;
    }

    /** Returns string representatino of building block connection descriptor. */
    public static String toStringConnection(DBElementConnectionDescriptor dcd) {
	return "" + (dcd.getDescriptor1().getDescriptorId()+1) + "," + (dcd.getDescriptor2().getDescriptorId()+1) + ","
	    + dcd.getHelixendName1() + "," + dcd.getHelixendName2() + "," + dcd.getBasePairCount();
    }

    private String toStringBlocks() {
	StringBuffer result =new StringBuffer();
	result.append("blocks=");
	for (int i = 0; i < buildingBlocks.size(); ++i) {
	    result.append(toStringBlock(buildingBlocks.get(i)));
	    if ((i+1) < buildingBlocks.size()) {
		result.append(";");
	    }
	}
	return result.toString();
    }

    public String toStringConnections() {
	StringBuffer result =new StringBuffer();
	result.append("connect=");
	for (int i = 0; i < connections.size(); ++i) {
	    result.append(toStringConnection(connections.get(i)));
	    if ((i+1) < connections.size()) {
		result.append(";");
	    }
	}
	return result.toString();
    }

    /** Returns string representation. */
    public String toString() {
	String result = toStringBlocks() + " " + toStringConnections() + " gen=" + numGenerations + " graph=" + graphFlag;
	return result;
    }

    /** Updated cashed variables equivalentBuildingBlockCount and equivalentConnectionCount */
    private void update() {
	nonEquivalentBuildingBlockCount = computeNonEquivalentBuildingBlockCount();
	nonEquivalentConnectionCount = computeNonEquivalentConnectionCount();
    }

    public boolean validate() {
	boolean check1 = buildingBlocks != null && connections != null;
	if (!check1) {
	    return false;
	}
	for (DBElementDescriptor dbe : buildingBlocks) {
	    if (dbe == null) {
		return false;
	    }
	}
	for (DBElementConnectionDescriptor dbc : connections) {
	    if (dbc == null) {
		return false;
	    }
	}
	return true;
    }

}
