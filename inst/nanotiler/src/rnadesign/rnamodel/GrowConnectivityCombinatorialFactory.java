package rnadesign.rnamodel;

import static rnadesign.rnacontrol.PackageConstants.*;

import java.util.*;
import graphtools.*;
import java.util.logging.*;

/** for a given graph, return random grow commands that might generatate this graph */
public class GrowConnectivityCombinatorialFactory implements GrowConnectivityFactory  {

    public static final int ACTIVE_RULE_DEFAULT_LIMIT = 3;

    private static Logger log = Logger.getLogger(LOGFILE_DEFAULT);

    private class HelixDescriptor {
	int blockId;
	int helixId;
    }

    private class HelixPairDescriptor {
	int helixId1;
	int helixId2;
	HelixPairDescriptor(int id1, int id2) {
	    assert(id1 < id2);
	    helixId1 = id1;
	    helixId2 = id2;
	}
    }

    int activeRuleLimit = ACTIVE_RULE_DEFAULT_LIMIT;
    GrowConnectivity connectivityTemplate;
    List<GrowConnectivity> connectivities;
    Iterator<GrowConnectivity> connIter;
    List<HelixDescriptor> helixDescriptors;
    List<HelixPairDescriptor> ruleDescriptors;
    int helixLengthMax;
    boolean useAllBlocksMode = true;

    public GrowConnectivityCombinatorialFactory(GrowConnectivity _connectivityTemplate, int _helixLengthMax) {
	this.connectivityTemplate = _connectivityTemplate;
	this.helixLengthMax = _helixLengthMax;
	init();
    }

    /** UNFINISHED IMPLEMENTATION: Returns true if more grow connectivities can be generated */
    public boolean hasNext() {
	return connIter.hasNext();
    }

    private void init() {
	connectivities = new ArrayList<GrowConnectivity>();	
	helixDescriptors = new ArrayList<HelixDescriptor>();
	List<DBElementDescriptor> buildingBlocks = connectivityTemplate.getBuildingBlocks();

	int nBlock = buildingBlocks.size();
	for (int i = 0; i < nBlock; ++i) {
	    DBElementDescriptor buildingBlock = buildingBlocks.get(i);
	    int order = buildingBlock.getOrder();
	    for (int j = 0; j < order; ++j) {
		HelixDescriptor desc = new HelixDescriptor();
		desc.blockId = i;
		desc.helixId = j;
		helixDescriptors.add(desc);
	    }
	}
	int helixCount = helixDescriptors.size();
	ruleDescriptors = new ArrayList<HelixPairDescriptor>();
	int activeRuleMax = helixCount / 2; // max number of interaction rules (inclusive) that are simultaneously active
	if (activeRuleMax > activeRuleLimit) {
	    activeRuleMax = activeRuleLimit;
	}
	for (int i = 0; i < helixCount; ++i) {
	    for (int j = i+1; j < helixCount; ++j) {
		ruleDescriptors.add(new HelixPairDescriptor(i,j));
	    }
	}
	// loop over number of active helix connections
	for (int i = 1; i <= activeRuleMax; ++i) {
	    IntegerPermutator connArrayIter = new IntegerArrayIncreasingGenerator(i, ruleDescriptors.size());
	    // assert(connArrayIter.hasNext());
	    // loop over helix connectivities
	    do {
		int[] conn = connArrayIter.get();
		if (isValidConnectivity(conn)) {
		    IntegerPermutator connLengthIter = new IntegerArrayGenerator(i, helixLengthMax+1);
		    do {
			int[] connLengths = connLengthIter.get();
			assert(connLengths.length == conn.length);
			// now we have all involved helices and helix lengths; generate descriptor:
			List<DBElementConnectionDescriptor> newConnections = new ArrayList<DBElementConnectionDescriptor>();
			// generate all connection descriptors corresponding to iterators:
			HashSet<DBElementDescriptor> usedBlocks = new HashSet<DBElementDescriptor>();
			for (int j = 0; j < conn.length; ++j) {
			    assert(conn[j] < ruleDescriptors.size());
			    HelixPairDescriptor rule = ruleDescriptors.get(conn[j]);
			    int hid1 = rule.helixId1;
			    int hid2 = rule.helixId2;
			    assert(hid1 < helixDescriptors.size());
			    assert(hid2 < helixDescriptors.size());
			    HelixDescriptor hd1 = helixDescriptors.get(hid1);
			    HelixDescriptor hd2 = helixDescriptors.get(hid2);
			    int hd1b = hd1.blockId; // block id of specific helix
			    int hd2b = hd2.blockId;
			    int hd1h = hd1.helixId; // helix id of specific helix
			    int hd2h = hd2.helixId;
			    DBElementDescriptor block1 = buildingBlocks.get(hd1b);
			    DBElementDescriptor block2 = buildingBlocks.get(hd2b);
			    usedBlocks.add(block1);
			    usedBlocks.add(block2);
			    int bpCount = connLengths[j];
			    DBElementConnectionDescriptor newConn = new DBElementConnectionDescriptor(block1, block2, hd1h, hd2h, bpCount);
			    newConnections.add(newConn);
			}
			if ((!useAllBlocksMode) || (usedBlocks.size()==buildingBlocks.size())) {
			    GrowConnectivity newGConn = new GrowConnectivity(buildingBlocks, newConnections,
									     connectivityTemplate.getTopologies(), connectivityTemplate.getNumGenerations());
			    log.fine("Created connectivity " + newGConn.toString());
			    connectivities.add(newGConn);
			}
		    } while (connLengthIter.hasNext() && connLengthIter.inc());
		}
	    } while (connArrayIter.hasNext() && connArrayIter.inc());
	    // loop over helix lengths
	}

	connIter = connectivities.iterator();
    }

    /** Checks, whether each helix is used only once */
    private boolean isValidConnectivity(int[] conn) {
	// ensure that all numbers are great than its own index: (i.e. store 2->3 but not 3->2
	Set<Integer> helixIds = new HashSet<Integer>();
	for (int i = 0; i < conn.length; ++i) {
	    assert(conn[i] < ruleDescriptors.size());
	    HelixPairDescriptor desc = ruleDescriptors.get(conn[i]);
	    int id1 = desc.helixId1;
	    int id2 = desc.helixId2;
	    if (helixIds.contains(id1) || helixIds.contains(id2)) { // uses autoboxing for Integer <-> int conversion
		return false;
	    }
	    helixIds.add(id1);
	    helixIds.add(id2);
	}
	return true;
    }
    
    /** UNFINISHED IMPLEMENTATION: returns "next" growconnectivity that might be compatible with graph */
    public GrowConnectivity next() throws FittingException { 
	return connIter.next();
    }

}
