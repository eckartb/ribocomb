package rnadesign.rnamodel;

public interface GrowConnectivityFactory {

    /** returns true if more grow connectivities can be generated */
    boolean hasNext();

    /** returns "next" growconnectivity */
    GrowConnectivity next() throws FittingException;

}
