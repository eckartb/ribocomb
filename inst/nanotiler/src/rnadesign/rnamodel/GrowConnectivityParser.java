package rnadesign.rnamodel;

public class GrowConnectivityParser implements GrowConnectivityFactory {

    /** returns true if more grow connectivities can be generated */
    public boolean hasNext() { assert false; return false; }

    /** returns "next" growconnectivity */
    public GrowConnectivity next() { assert false; return null; }

}
