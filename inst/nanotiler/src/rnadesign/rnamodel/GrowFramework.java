package rnadesign.rnamodel;

import generaltools.PropertyTools;
import java.io.*;
import java.util.*;
import java.math.BigInteger;
import java.math.BigDecimal;
import java.util.logging.*;
import tools3d.Vector3D;
import tools3d.objects3d.*;
import java.util.Arrays;

public class GrowFramework implements Runnable {

    public static final String NAME_BASE_SUFFIX = "_fr";

    private static Logger log = Logger.getLogger(PackageConstants.LOGFILE_DEFAULT);

    private boolean buildMode = true;
    private boolean debugMode = true;
    private Properties properties = new Properties();
    private ConnectivityGenerator connGenerator;
    private Exception exception;
    private String rootName = "root"; // "grow";
    private String nameBase = "root"; // "grow";
    private Vector3D startPosition = new Vector3D(0,0,0);
    private StrandJunctionDB junctionDB;
    private StrandJunctionDB kissingLoopDB;
    private StrandJunctionDB motifDB;
    private Object3D nucleotideDB;
    private double ringClosureExportLimit = 15;
    private BuildingBlockGrower bestGrower; // contains best generated structure
    private int exportMax = 100;
    private double[][] vertexFingerprints;
    private double angleTolerance = Math.toRadians(45.0);

    public GrowFramework(ConnectivityGenerator connGenerator,
			 StrandJunctionDB junctionDB, StrandJunctionDB kissingLoopDB, StrandJunctionDB motifDB,
			 Object3D nucleotideDB, String nameBase) {
	this.connGenerator = connGenerator;
	this.junctionDB = junctionDB;
	this.kissingLoopDB = kissingLoopDB;
	this.motifDB = motifDB;
	this.nucleotideDB = nucleotideDB;
    }

    /** Returns building block grower that (if not null) contains a succesfully grown structure */
    public BuildingBlockGrower getBestGrower() { return this.bestGrower; }

    public Exception getException() { return this.exception; }

    /** Returns maximum number of structres that will be exported */
    public int getExportMax() { return exportMax; }

    public boolean isBuildMode() { return buildMode; }

    public Properties getProperties() { return properties; }

    public String getNameBase() {
	return nameBase;
    }

    /** Performs structure / connectivity search. */
    public void run() {
	log.info("Starting GrowFramework.run()...");
	boolean initial = true;
	this.properties = new Properties(); // start over 
	this.bestGrower = null;
	int writeCount = 0;
	int structureCount = 0;
	this.exception = null; // fresh start
	this.vertexFingerprints = generateAllVertexFingerprints();
	boolean orderCheck = verifyOrders();
	if (!orderCheck) {
	    this.exception = new RnaModelException("Not all needed building blocks are provided.");
	    return;
	}
	// SimpleConnectivityIterator iter = (SimpleConnectivityIterator)(connGenerator.iterator());
	SmallConnectivityIterator iter = (SmallConnectivityIterator)(connGenerator.iterator());
	log.info("Generated connectivity iterator!");
	if (debugMode) {
	    long count = 1;
	    do {
		System.out.println("Iterated test grow connectivity: " + (count++) + " : " + iter.next());
	    }
	    while (iter.hasNext());
	    System.out.println("Number of iterations in dry run: " + count);
	    // check if total number steps correct:
	    if (!(new BigInteger("" + count).equals(iter.getTotal()))) {
		System.out.println("Discrepancy in tried steps and expected steps: tried: " + count + " expected: " + iter.getTotal()
				   + " Don't worry, this might be due to additional checks in the connectivity iterator.");
	    }
	    iter.reset();
	}
	assert iter != null;
	// assert iter.validate();
	int exportCount = 0; // counts how many structures were exported
	// while (iter.hasNext()) {
	boolean skip = false; // keeps track if iterator was forced to skip items
	do {
	    log.info("Start of do loop!");
	    skip = false; // keeps track if iterator was forced to skip items
	    GrowConnectivity conn = iter.getGrowConnectivity();
	    log.info("Starting new iteration with connectivity: " + conn);
	    if (conn == null) { // can happen if some rules are not fullfilled
		if (initial) {
		    log.info("Could not generate initial connectivity!");
		} else {
		    log.info("Could not generate connectivity!");
		}
		continue; 
	    }
	    assert conn != null;
	    log.info("Trying connectivity: " + conn);
	    assert !conn.isGraphFlag(); // changed!
//  	    if (!validateBuildingBlockAngles(conn)) {
// 		try {
// 		    boolean skipResult = iter.skipToNextBuildingBlock();
// 		    log.info("Skipped building blocks to: " + iter.getGrowConnectivity() + " : " + skipResult);
// 		    assert skipResult; // if skipping not possible, an exception should be thrown
// 		}
// 		catch (IllegalAccessException iae) {
// 		    log.info("Quitting GrowFramework.run(), could not find any further compatible building block combinations.");
// 		    return;
// 		}
// 		skip = true;
//  		log.info("Skipping connectivity because building block angles where too different: " + conn.toString());
//  		continue;
//  	    }
	    if (buildMode) {
		BuildingBlockGrower grower = new BuildingBlockGrower(junctionDB, kissingLoopDB, motifDB, nucleotideDB);
		grower.setRingClosureExportLimit(ringClosureExportLimit);
		String ringClosureExportFileNameBase = nameBase + NAME_BASE_SUFFIX;
		try {
			Properties properties = grower.growBuildingBlocks(conn, rootName, nameBase, startPosition,
									  ringClosureExportFileNameBase);
			assert grower.getRoot() != null;
			log.info("Successfully generated structure from connectivity: " + conn + " : " + properties);
			++structureCount; // if no exception found, it means that the graph was successfully generated.
			this.bestGrower = grower;
			if ((ringClosureExportFileNameBase != null)
			    && (ringClosureExportFileNameBase.length() > 0)
			    && (!(properties.getProperty("ring_count").equals("0") ) ) ) {
			    String outputFileName = ringClosureExportFileNameBase + "_" + (++writeCount) + ".pdb";
			    if (exportCount < exportMax) {
				log.info("Writing connectivity " + conn + " to file: " + outputFileName);
				GeneralPdbWriter writer = new GeneralPdbWriter();
				FileOutputStream fos = new FileOutputStream(outputFileName);
				PropertyTools.writeToPdbRemarks(fos, properties);
				writer.write(fos, grower.getRoot());
				fos.close();
				exportCount++;
			    }
			    else {
				log.info("Not exporting structure to file " + outputFileName + " because maximum number of exported structures reached!");
			    }
			}
			else {
			    log.info("Not writing built structure to file: " + properties);
			}
			System.out.println("Info of root:");
			System.out.println(grower.getRoot().infoString());
// 			System.out.println("Removing " + grower.getRoot().getFullName());
// 			if (grower.getRoot().getParent
// 			grower.getRoot().getParent().removeChild(grower.getRoot());
			for (int c = grower.getRoot().size()-1; c >= 0; --c) {
			    if (grower.getRoot().getChild(c).getName().startsWith(nameBase + "_")) {
				System.out.println("Removing child node " + 
					   grower.getRoot().getChild(c).getFullName());
				grower.getRoot().removeChild(c);
			    }
			}
		}
		catch (FittingException fe) {
		    log.info("Fitting exception " + fe.getMessage() + " for grow connectivty: " + conn);
		    // keep going!
		}
		catch (IOException ioe) {
		    log.severe("IO exception in building block growing: " + ioe.getMessage());
		    this.exception = ioe;
		    break;
		}
	    } {
		log.info("Not building structure!");
	    }
	    log.info("End of do-loop iteration");
	}
	while (iter.hasNext() && (skip || (iter.next() != null))); // next iteration, if skip is true, no next statement is necessary
	this.properties.setProperty("write_count", "" + writeCount);
	this.properties.setProperty("structure_count", "" + structureCount);
	log.info("Finished GrowFramework.run()...");
    }

    public void setBuildMode(boolean b) { this.buildMode = b; }

    /** Returns maximum number of structres that will be exported */
    public void setExportMax(int n) { this.exportMax = n; }

    public void setRingClosureExportLimit(double limit) { this.ringClosureExportLimit = limit; }

    public void setNameBase(String s) {
	this.nameBase = s;
    }

    private List<Double> generateFingerprint(Object3D vertex, LinkSet links) {
	LinkSet connLinks = links.findLinks(vertex);
	List<Double> result = new ArrayList<Double>();
	for (int i = 0; i < connLinks.size(); ++i) {
	    Link link1 = connLinks.get(i);
	    for (int j = i+1; j < connLinks.size(); ++j) {
		Link link2 = connLinks.get(j);
		assert link1.findCommon(link2).size() == 1;
		double ang = link1.angle(link2);
		result.add(ang); // autoboxing
	    }
	}
	Collections.sort(result);
	return result;
    }

    /** Generates angle "fingerprint" of vertex: all link-link angles of all connected links, sorted */
    private double[] generateVertexFingerprint(Object3D vertex, LinkSet links) {
	LinkSet connLinks = links.findLinks(vertex);
	double[] result = new double[(connLinks.size()*(connLinks.size()-1))/2];
	int pc = 0;
	for (int i = 0; i < connLinks.size(); ++i) {
	    Link link1 = connLinks.get(i);
	    for (int j = i+1; j < connLinks.size(); ++j) {
		Link link2 = connLinks.get(j);
		assert link1.findCommon(link2).size() == 1;
		double ang = link1.angle(link2);
		assert pc < result.length;
		result[pc++] = ang; // autoboxing
	    }
	}
	assert pc == result.length;
	Arrays.sort(result);
	return result;
    }

    private double[][] generateAllVertexFingerprints() {
	List<Object3D> vertices = connGenerator.getVertices();
	LinkSet links = connGenerator.getLinks();
	int n = vertices.size();
	double[][] result = new double[n][0];
	for (int i = 0; i < n; ++i) {
	    result[i] = generateVertexFingerprint(vertices.get(i), links);
	}
	return result;
    }

    private boolean validateFingerprint(double[] vertexFingerprint, double[] junctionFingerprint, double angleTolerance) {
	assert vertexFingerprint != null && junctionFingerprint != null;
	if (vertexFingerprint.length != junctionFingerprint.length) {
	    return false;
	}
	if (angleTolerance < 0.0) {
	    return true;
	}
	for (int i = 0; i < vertexFingerprint.length; ++i) {
	    if (Math.abs(vertexFingerprint[i] - junctionFingerprint[i]) > angleTolerance) {
		return false;
	    }
	}
	return true;
    }

    /** returns true, if angles of n'th building block descriptor are compatible with n'th vertex */
    private boolean validateBuildingBlockAngles(GrowConnectivity conn, int n) {
	DBElementDescriptor dbe = conn.getBuildingBlocks().get(n); // n'th building block descriptor
	Object3D vertex = connGenerator.getVertices().get(n);
	LinkSet links = connGenerator.getLinks();
	double[] vertexFingerprint = vertexFingerprints[n];
	StrandJunction3D junction = null;
	if (dbe.getType() == DBElementDescriptor.JUNCTION_TYPE) {
	    junction = junctionDB.getJunction(dbe.getOrder(), dbe.getId());
	}
	else if (dbe.getType() == DBElementDescriptor.KISSING_LOOP_TYPE) {
	    junction = kissingLoopDB.getJunction(dbe.getOrder(), dbe.getId());
	}
	else {
	    log.severe("Unsupported junction type: " + dbe.getType());
	    assert false; // not allowed
	    System.exit(1);
	}
	double[] junctionFingerprint = StrandJunctionDBExportTools.generateJunctionFingerPrint(junction);
	return validateFingerprint(vertexFingerprint, junctionFingerprint, angleTolerance);
    }

    private boolean validateBuildingBlockAngles(GrowConnectivity conn) {
	int n = conn.getBuildingBlocks().size();
	for (int i = 0; i < n; ++i) {
	    if (!validateBuildingBlockAngles(conn, i)) {
		return false;
	    }
	}
	return true;
    }

//     private boolean validateConnectivity(GrowConnectivity conn) {
// 	return validateBuildingBlockAngles(conn);
//     }

    /** Return true iff for the given graph there is a junction/kissing loop of the correct order for evervy graph vertex */
    private boolean verifyOrders() {
	log.info("starting verifyOrders()...");
	int vertexCount = connGenerator.getVertices().size();
	for (int i = 0; i < vertexCount; ++i) {
	    int order = connGenerator.getVertexOrder(i);
	    // ignore order "1" (hairpins are not placed)
	    if ((order > 1) && (junctionDB.size(order) == 0) 
		&& (kissingLoopDB.size(order) == 0) ) {
		return false; // no proper junction of that order found
	    }
	}
	log.info("Finished verifyOrders()...");
	return true;
    }

}
