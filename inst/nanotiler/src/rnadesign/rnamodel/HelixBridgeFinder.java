package rnadesign.rnamodel;

import java.util.*;
import tools3d.*;
import tools3d.objects3d.*;
import java.util.logging.*;

import static rnadesign.rnamodel.PackageConstants.*;

/** Interface for classes that find 3D bridges between two objects */
public class HelixBridgeFinder implements BridgeFinder {

    private List<StrandJunction3D> junctions;

    private Object3D nucleotideDB;

    private Logger log = Logger.getLogger(LOGGER_NAME);

    private double angleWeight = 10.0; // 50.0; // angle errors: 

    private double rms = 2.0;

    private int solutionMax = 10;

    private int len1Max = 20;

    private int len2Max = 20;

    private int lenMin = 0;
    
    private int verboseLevel = 3;

    private boolean avgMode = false;// TODO: true would be average between two transformations. Not working properly, because of incomplete quaternion average formalism (see AxisAngle, Matrix4DTools class and their references. All that is missing is properly interpolating between quaternions)
    

    private boolean inversionMode = true; // use calls with branch descriptors pointing inwards, internally transformed to pointing outwards

    private char c1 = 'G';
    private char c2 = 'C';

    private class HelixJunctionSolution implements Comparable<HelixJunctionSolution> {

	double score;
	int junctionId;
	int helixId1;
	int helixId2;
	int helixLen1;
	int helixLen2;
	
	public int compareTo(HelixJunctionSolution sol2) {
	    // assert other instanceof HelixJunctionSolution;
	    // HelixJunctionSolution sol2 = (HelixJunctionSolution)other;
	    if (score < sol2.score) {
		return -1;
	    }
	    else if (score > sol2.score) {
		return 1;
	    }
	    return 0; // equal
	}
	
	/** Converts to 3D model */
	public Object3DLinkSetBundle convertTo3D(Matrix4D orient1, Matrix4D orient2) {
	    assert helixId1 != helixId2;
	    StrandJunction3D junction = (StrandJunction3D)(junctions.get(junctionId).cloneDeep());
	    assert helixId1 >= 0 && helixId1 < junction.getBranchCount();
	    assert helixId2 >= 0 && helixId2 < junction.getBranchCount();
	    // superpose helix 1 and its extra base pairs. TODO: understand why -1 works best...
	    Matrix4D m1 = junction.getBranch(helixId1).generatePropagatedCoordinateSystem(helixLen1).generateMatrix4D();
	    Matrix4D m2 = junction.getBranch(helixId2).generatePropagatedCoordinateSystem(helixLen2).generateMatrix4D();
	    Matrix4D conv1 = orient1.multiply(m1.inverse());
	    if (avgMode) { // compute average transformation between using either helix as reference orientation
		Matrix4D conv2 = orient2.multiply(m2.inverse());
		// junction.activeTransform(new CoordinateSystem3D(conv2));
		Matrix4D avgConv = Matrix4DTools.computeAverageTransformationQuat(conv1, conv2);
		junction.activeTransform(new CoordinateSystem3D(avgConv));
	    }
	    else {
		junction.activeTransform(new CoordinateSystem3D(conv1));
	    }
	    junction.setProperty("score", "" + score);
	    junction.setProperty("junction_id", "" + (junctionId+1) );
	    junction.setProperty("helix_id_1", "" + (helixId1 + 1));
	    junction.setProperty("helix_id_2", "" + (helixId2 + 1));
	    junction.setProperty("helix_len_1", "" + helixLen1);
	    junction.setProperty("helix_len_2", "" + helixLen2);
	    if (nucleotideDB != null) {
		if (helixLen1 > 0) {
		    String stem1Name = "stem1";
		    junction.getBranch(helixId1).propagate(1);
		    Object3DLinkSetBundle stem1Bundle = ConnectJunctionTools.generateIdealStem(junction.getBranch(helixId1), c1, c2,
											       stem1Name, nucleotideDB, helixLen1);
		    junction.getBranch(helixId1).propagate(-1);
		    log.info("Generated bridge stem 1: " + stem1Bundle.getObject3D().getFullName());
		    junction.insertChild(stem1Bundle.getObject3D());
		}
		if (helixLen2 > 0) {
		    String stem2Name = "stem2";
		    junction.getBranch(helixId2).propagate(1);
		    Object3DLinkSetBundle stem2Bundle = ConnectJunctionTools.generateIdealStem(junction.getBranch(helixId2), c1, c2,
											       stem2Name, nucleotideDB, helixLen2);
		    junction.getBranch(helixId2).propagate(-1);
		    log.info("Generated bridge stem 2: " + stem2Bundle.getObject3D().getFullName());
		    junction.insertChild(stem2Bundle.getObject3D());
		}
	    }
	    else {
		log.warning("Could not generate bridging helices because no reference nucleotides are defined!");
	    }
	    return new SimpleObject3DLinkSetBundle(junction, new SimpleLinkSet());
	}
    }

    public HelixBridgeFinder() { }

    public HelixBridgeFinder(List<StrandJunction3D> junctions,
			     Object3D nucleotideDB) { this.junctions = junctions; this.nucleotideDB = nucleotideDB; }

    public List<Object3DLinkSetBundle> findBridge(Object3D obj1, Object3D obj2) {
	log.info("Called HelixBridgeFinder : " + obj1.getFullName() + " " + obj2.getFullName()
		 + " rms: " + rms + " verbose: " + verboseLevel); 
	if ((!(obj1 instanceof BranchDescriptor3D)) || (!(obj2 instanceof BranchDescriptor3D))) {
	    log.warning("Expected BranchDescriptor3D data type: " + obj1.getFullName() + " " + obj2.getFullName());
	    return null;
	}
	return findHelixBridge((BranchDescriptor3D)obj1, (BranchDescriptor3D)obj2);
    }
    
    /** Central command for finding branch descriptor pair in database of structural elements */
    public List<Object3DLinkSetBundle> findHelixBridge(BranchDescriptor3D bd1, BranchDescriptor3D bd2) {
	int prop = 1; // should be one or zero?
	double distOrig = bd1.distance(bd2);
	log.info("Trying to bridge distance " + distOrig);
	if (inversionMode) { // use calls with branch descriptors pointing inwards, internally transformed to pointing outwards
	    double angle = bd1.getDirection().angle(bd2.getDirection());
	    BranchDescriptorTools.invertBranchDescriptor(bd1, prop);
	    BranchDescriptorTools.invertBranchDescriptor(bd2, prop);
	    log.info("Inverted searching for junction with connector helices and angle: " 
		     + bd1.getFullName() + " " + bd2.getFullName() + " Angle: " + Math.toDegrees(angle) + " distance: " + distOrig);
	    double distNew = bd1.distance(bd2);
	    assert distOrig != distNew;
	    assert Math.abs(distOrig - distNew) < 7.0;
	}
	double angle2 = bd1.getDirection().angle(bd2.getDirection());
	log.info("Searching for junction with helices and angle and distance:" 
		 + bd1.getFullName() + " " + bd2.getFullName() + " Angle: " + Math.toDegrees(angle2) + " dist: " + bd1.distance(bd2));
	List<Object3DLinkSetBundle> result = new ArrayList<Object3DLinkSetBundle>();
	if (!isValid()) {
	    log.warning("No junctions defined in helix bridge finder!");
	}
	else {
	    Matrix4D md = convertBranchDescriptorPairToMatrix(bd1, bd2);
	    Matrix4D m1 = bd1.getCoordinateSystem().generateMatrix4D();
	    Matrix4D m2 = bd2.getCoordinateSystem().generateMatrix4D();
	    List<HelixJunctionSolution> solutions = findHelixBridge(md); // findHelixBridge2(bd1, bd2);

	    if (!inversionMode) {
		BranchDescriptorTools.invertBranchDescriptor(bd1, prop);
		BranchDescriptorTools.invertBranchDescriptor(bd2, prop);
	    }	    
	    for (int i = 0; (i < solutions.size()) && (i < solutionMax); ++i) {
		result.add(solutions.get(i).convertTo3D(m1, m2));
	    }
	    if (!inversionMode) {
		BranchDescriptorTools.invertBranchDescriptor(bd1, prop);
		BranchDescriptorTools.invertBranchDescriptor(bd2, prop);
	    }
	}
	// revert inversion to original orientation!
	if (inversionMode) {
	    BranchDescriptorTools.invertBranchDescriptor(bd1, prop);
	    BranchDescriptorTools.invertBranchDescriptor(bd2, prop);
	}
	return result;
    }

    /** Returns relative 4D matrix corresponding to two branch descriptors (pointing outwards)
     */
    private Matrix4D convertBranchDescriptorPairToMatrix(BranchDescriptor3D bd1, BranchDescriptor3D bd2) {
	Matrix4D m1 = bd1.getCoordinateSystem().generateMatrix4D();
	Matrix4D m2 = bd2.getCoordinateSystem().generateMatrix4D();
	return convertBranchDescriptorPairToMatrix(m1, m2); // 
    }

    /** Returns relative 4D matrix corresponding to two branch descriptors (pointing outwards)
     */
    private Matrix4D convertBranchDescriptorPairToMatrix(Matrix4D m1, Matrix4D m2) {
	return m1.inverse().multiply(m2); // m2.multiply(m1.inverse());
    }

    /** Central command for finding branch descriptor pair in database of structural elements
     * @param matrix 4D matrix describing relative orientation of two branch descriptors: m2 * (m1**-1)*/
    private List<HelixJunctionSolution> findHelixBridge(Matrix4D matrix) {
	assert junctions != null;
	log.info("Called findHelixBridge: " + matrix);
	List<HelixJunctionSolution> result = new ArrayList<HelixJunctionSolution>();
	for (int i = 0; i < junctions.size(); ++i) {
	    List<HelixJunctionSolution> junctionSol = findJunctionSolutions(matrix, i);
	    for (HelixJunctionSolution sol : junctionSol) {
		if (sol.score <= this.rms) {
		    result.add(sol); // only store good solutions
		}
	    }
	}
	Collections.sort(result);
	return result;
    }

    /** Central command for finding branch descriptor pair in database of structural elements
     * @param matrix 4D matrix describing relative orientation of two branch descriptors: m2 * (m1**-1)*/
    private List<HelixJunctionSolution> findHelixBridge2(BranchDescriptor3D scaffBd1, BranchDescriptor3D scaffBd2) {
	assert junctions != null;
	// log.info("Called findHelixBridge: " + matrix);
	List<HelixJunctionSolution> result = new ArrayList<HelixJunctionSolution>();
	for (int i = 0; i < junctions.size(); ++i) {
	    List<HelixJunctionSolution> junctionSol = findJunctionSolutions2(scaffBd1, scaffBd2, i);
	    for (HelixJunctionSolution sol : junctionSol) {
		if (sol.score <= this.rms) {
		    result.add(sol); // only store good solutions
		}
	    }
	}
	Collections.sort(result);
	return result;
    }

    /** Finds all solutions for one junction id */
    private List<HelixJunctionSolution> findJunctionSolutions(Matrix4D matrix, int junctionId) {
	List<HelixJunctionSolution> solutions = new ArrayList<HelixJunctionSolution>();
	StrandJunction3D junction = junctions.get(junctionId);
	for (int hid1 = 0; hid1 < junction.getBranchCount(); ++hid1) {
	    for (int hid2 = 0; hid2 < junction.getBranchCount(); ++hid2) { // try other direction
		if (hid1 != hid2) {
		    solutions.addAll(findJunctionSolution(matrix, junctionId, hid1, hid2));
		}
	    }
	}
	return solutions;
    }

    /** Finds all solutions for one junction id */
    private List<HelixJunctionSolution> findJunctionSolutions2(BranchDescriptor3D scaffBd1, BranchDescriptor3D scaffBd2, int junctionId) {
	List<HelixJunctionSolution> solutions = new ArrayList<HelixJunctionSolution>();
	StrandJunction3D junction = junctions.get(junctionId);
	for (int hid1 = 0; hid1 < junction.getBranchCount(); ++hid1) {
	    for (int hid2 = 0; hid2 < junction.getBranchCount(); ++hid2) { // try other direction
		if (hid1 != hid2) {
		    solutions.addAll(findJunctionSolution2(scaffBd1, scaffBd2, junctionId, hid1, hid2));
		}
	    }
	}
	return solutions;
    }

    private List<HelixJunctionSolution> findJunctionSolution(Matrix4D matrix, int junctionId, int hid1, int hid2) {
	List<HelixJunctionSolution> solutions = new ArrayList<HelixJunctionSolution>();
	double bestScore = 1e30;
	for (int len1 = lenMin; len1 < len1Max; ++len1) {
	    for (int len2 = lenMin; len2 < len2Max; ++len2) {
		double score = scoreJunctionSolution(matrix, junctionId, hid1, hid2, len1, len2);
		if (verboseLevel > 3) {
		    log.info("Testing bridge: id; " + junctionId + " h " + hid1 + " " + hid2 + " l: " + len1 + " " + len2
			     + " score: " + score);
		}
		if (score < rms) {
		    if ((verboseLevel > 2) || (score < bestScore)) {
			log.info("Feasible bridge found: id; " + junctionId + " h " + hid1 + " " + hid2 + " l: " + len1 + " " + len2
				 + " score: " + score);
		    }
		    bestScore = score;
		    HelixJunctionSolution sol = new HelixJunctionSolution();
		    sol.score = score;
		    sol.junctionId = junctionId;
		    sol.helixLen1 = len1;
		    sol.helixLen2 = len2;
		    sol.helixId1 = hid1;
		    sol.helixId2 = hid2;
		    solutions.add(sol);
		}
	    }
	}
	return solutions;
    }

    private List<HelixJunctionSolution> findJunctionSolution2(BranchDescriptor3D scaffBd1, BranchDescriptor3D scaffBd2, int junctionId, int hid1, int hid2) {
	List<HelixJunctionSolution> solutions = new ArrayList<HelixJunctionSolution>();
	double bestScore = 1e30;
	StrandJunction3D junction = junctions.get(junctionId);
	BranchDescriptor3D bd1 = junction.getBranch(hid1);
	BranchDescriptor3D bd2 = junction.getBranch(hid2);
	for (int len1 = 0; len1 < len1Max; ++len1) {
	    for (int len2 = lenMin; len2 < len2Max; ++len2) {
		Properties fitResult = SimpleStrandJunctionDB.scoreJunctionLoopFit(scaffBd1, scaffBd2, bd1, bd2, len1, len2, angleWeight);
		assert fitResult.getProperty("score") != null;
		double score = Double.parseDouble(fitResult.getProperty("score"));
		if (verboseLevel > 3) {
		    log.info("Testing bridge: id; " + junctionId + " h " + hid1 + " " + hid2 + " l: " + len1 + " " + len2
			     + " score: " + score);
		}
		if (score < rms) {
		    if ((verboseLevel > 2) || (score < bestScore)) {
			log.info("Feasible bridge found: id; " + junctionId + " h " + hid1 + " " + hid2 + " l: " + len1 + " " + len2
				 + " score: " + score);
		    }
		    bestScore = score;
		    HelixJunctionSolution sol = new HelixJunctionSolution();
		    sol.score = score;
		    sol.junctionId = junctionId;
		    sol.helixLen1 = len1;
		    sol.helixLen2 = len2;
		    sol.helixId1 = hid1;
		    sol.helixId2 = hid2;
		    solutions.add(sol);
		}
	    }
	}
	return solutions;
    }

    public int getLenMin() { return 1; }
    
    public int getLenMax() { return 1000; }


    public double getRms() { return rms; }

    /** Returns true if helix finder object is operational */
    public boolean isValid() {
	return junctions != null && junctions.size() > 0;
    }

    /** Central method scores the quality of junction (junctionId) and helices hid1, hid2 with lengths len1, len2
     * to bridge gap described by matrix. 
     * @param matrix Relative orientation of two branch descriptors pointing outwards 
     */
    private double scoreJunctionSolution(Matrix4D matrix, int junctionId, int hid1, int hid2, int len1, int len2) {
	assert hid1 != hid2;
	BranchDescriptor3D bd1 = junctions.get(junctionId).getBranch(hid1);
	BranchDescriptor3D bd2 = junctions.get(junctionId).getBranch(hid2);
	Matrix4D jMatrix = convertBranchDescriptorPairToMatrix(bd1.generatePropagatedCoordinateSystem(len1).generateMatrix4D(),
							       bd2.generatePropagatedCoordinateSystem(len2).generateMatrix4D());
	double result = scoreMatrixDifference(matrix, jMatrix);
	double result2 = scoreMatrixDifference(jMatrix, matrix);
	assert (Math.abs(result-result2) < 0.01);
	// System.out.println("Scoring bridge vector " + matrix.subvector() + " versus junction vector: " + jMatrix.subvector() + " for lengths "
	// + len1 + " " + len2 + " helix ids: " + hid1 + " " + hid2 + " result: " + result);
	return result;
    }

    /** Scores difference between two 4D matrices */
    private double scoreMatrixDifference(Matrix4D m1, Matrix4D m2) {
	// Matrix4D dm = convertBranchDescriptorPairToMatrix(m1, m2);
	// dm.subtract(Matrix4D.I4); // substract identity matrix: useful for euclidian matrix norm ??? QUESTIONABLE!!!
	return AxisAngle.distanceQuatNorm(m1, m2, angleWeight);// return matrix norm
	// return m1.subvector().distance(m2.subvector()); // dummy implementation
    }

    public void setBasepairCharacters(char c1, char c2) {
	this.c1 = c1;
	this.c2 = c2;
    }

    /** Sets weight of angle norm versus distance norm */
    public void setAngleWeight(double weight) { this.angleWeight = weight; }

    public void setInversionMode(boolean m) { this.inversionMode = m; }

    public void setLenMax(int n) {
	this.len1Max = n;
	this.len2Max = n;
    }

    public void setLenMin(int n) { lenMin = n; }

    public void setJunctions(List<StrandJunction3D> junctions) { this.junctions = junctions; }

    public void setRms(double rms) { this.rms = rms; }

    public void setSolutionMax(int n) { this.solutionMax = n; }

    public void setVerboseLevel(int level) { this.verboseLevel = level; }

}
