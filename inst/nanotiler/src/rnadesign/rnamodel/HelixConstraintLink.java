package rnadesign.rnamodel;

import tools3d.objects3d.Link;
import tools3d.objects3d.SymmetryLinker;

public interface HelixConstraintLink extends Link, SymmetryLinker {

    public double getRms();

    public void setRms(double rms);

    public int getBasePairMin();

    public int getBasePairMax();

    public void setBasePairMin(int n);

    public void setBasePairMax(int n);

}
