package rnadesign.rnamodel;

import java.io.PrintStream;
import java.util.logging.*;
import java.util.Properties;
import java.util.List;
import java.util.ArrayList;
import java.util.Random;
import generaltools.Optimizer;
import generaltools.Randomizer;
import tools3d.objects3d.*;
import tools3d.symmetry2.*;
import tools3d.*;
import graphtools.AdjacencyTools;
import numerictools.DoubleArrayTools;
import numerictools.DoubleTools;

import static rnadesign.rnamodel.PackageConstants.*;

public class HelixOptimizer implements Optimizer {

    public static final double TRANSLATION_STEP = 4.0; // 10.0;
    public static final double ANGLE_STEP = Math.toRadians(15.0); // 90.0 * DEG2RAD;

    private HelixParameters defaultHelixParameters = new HelixParameters();
    private boolean addHelicesMode = false;
    private double annealingFactor = 0.985;
    private int annealingInterval = 10000; // scale step width and kt every so often
    private boolean keepFirstFixedMode = true; // if true, do not mutate first coordinate system
    private int outputInterval = 10000; // scale step width and kt every so often
    private Object3D root;
    private boolean firstPass = true; // only true during first evalutation of each optimization run
    private double scaleLimit = 10.0;
    private double scaleMult = 0.02;
    private List<HelixConstraintLink> helixConstraints;
    private List<ConstraintLink> distanceConstraints;
    private LinkSet allLinks;
    private int[][] branchDescriptorGroups;
    private int[] branchDescriptorGroupIds;
    private int[] groupConstraintCounts;
    private List<BranchDescriptor3D> branchDescriptors;
    private List<CoordinateSystem> origCoordinateSystems;
    private List<CoordinateSystem> optCoordinateSystems;
    private double[] coordScoreAverages; // average constraint violation for each coordinate system
    private double[] coordScoreAveragesTmp; // average constraint violation for each coordinate system
    private double[] coordScoreAveragesSaved; // average constraint violation for each coordinate system
    private int helixMutInterval = 0; // if greater zero: mutate helix lengths!
    private double helixConstraintMutationProb = 0.5; // probability with which a helix length is mutated
    private boolean[] moveFlags;
    private int numberSteps;
    private double ktOrig = 10.0;
    private static Logger log = Logger.getLogger(LOGGER_NAME);
    private double errorScoreLimit;
    private double errorScoreInitialLimit = 1e10;
    private GaussianOrientableMutator mutator = new GaussianOrientableMutator(TRANSLATION_STEP, ANGLE_STEP);
    private GaussianOrientableMutator mutatorOrig = new GaussianOrientableMutator(TRANSLATION_STEP, ANGLE_STEP);
    private Object3DSet parentObjects;
    private static SymCopies symCopies; // keeps all stored symmetry operations

    public HelixOptimizer(Object3D graph, LinkSet links, int numberSteps, double errorScoreLimit, Object3DSet parents) {
	this.root = graph;
	this.numberSteps = numberSteps;
	this.errorScoreLimit = errorScoreLimit;
	this.errorScoreInitialLimit = errorScoreInitialLimit;
	this.helixConstraints =  new ArrayList<HelixConstraintLink>();
	this.distanceConstraints = new ArrayList<ConstraintLink>();
	this.allLinks = links; // might be needed for checking hydrogen bonding
	setParentObjects(parents);
	for (int i = 0; i < links.size(); ++i) {
	    Link link = links.get(i);
	    if (link instanceof HelixConstraintLink) {
		helixConstraints.add((HelixConstraintLink)link);
	    }
	    else if (link instanceof ConstraintLink) {
		distanceConstraints.add((ConstraintLink)link);
	    }
	}
	if (helixConstraints.size() == 0) {
	    log.warning("No helix constraints defined! Use command helixconstraint before start opthelices");
	}
	else {
	    this.branchDescriptors = findBranchDescriptors(helixConstraints);
	    this.branchDescriptorGroups = findBranchDescriptorGroups(branchDescriptors, allLinks);
	    this.branchDescriptorGroupIds = findGroupIds(branchDescriptorGroups, this.branchDescriptors.size());
	    this.origCoordinateSystems = findCoordinateSystems(branchDescriptors, branchDescriptorGroups);
	    this.optCoordinateSystems = findCoordinateSystems(branchDescriptors, branchDescriptorGroups);
	    coordScoreAverages = new double[branchDescriptorGroups.length];
	    coordScoreAveragesTmp = new double[branchDescriptorGroups.length];
	    coordScoreAveragesSaved = new double[branchDescriptorGroups.length];
	    resetCoordScoreAverages();
	    moveFlags = new boolean[coordScoreAverages.length];
	    for (int i = 0; i < moveFlags.length; ++i) {
		moveFlags[i] = true;
	    }
	    groupConstraintCounts = new int[origCoordinateSystems.size()];
	    String outCount = "";
	    for (int i = 0; i  <groupConstraintCounts.length; ++i) {
		groupConstraintCounts[i] = countGroupConstraints(i);
		outCount = outCount + groupConstraintCounts[i] + " ";
	    }
	    // log.info("Group constraint counts: " + outCount);
	}
	this.symCopies = SymCopySingleton.getInstance(); // current list of symmetry operations
    }

    /** gets starting "temperature" for allowed score moves */
    public double getKtOrig() { return ktOrig; }

    /** Find out, to which coordinate system trafo the object belongs */
    private int findObjectGroupId(Object3D object) {
	Object3D parent = findSuitableParentObject(object);
	if (parent ==  null) {
	    return -1;
	}
	for (int i = 0; i < branchDescriptors.size(); ++i) {
	    if (findSuitableParentObject(branchDescriptors.get(i)) == parent) {
		// found!
		return branchDescriptorGroupIds[i];
	    }
	}
	return -1; // not found
    }

    /** Sets limit for maximum initial score allowed */
    public double getErrorScoreInitialLimit() {
	return this.errorScoreInitialLimit;
    }

    /** Sets limit for maximum initial score allowed */
    public void setErrorScoreInitialLimit(double limit) {
	this.errorScoreInitialLimit = limit;
    }
    
    public void setHelixMutInterval(int value) { helixMutInterval = value; }

    /** sets starting "temperature" for allowed score moves */
    public void setKtOrig(double kt) { this.ktOrig = kt; }

    private void setMovableObject(Object3D object) {
	int id = findObjectGroupId(object);
	if (id >= 0) {
	    moveFlags[id] = true;
	    log.fine("Setting object " + object.getName() + " to movable group: " 
		     + (id+1));
	}
	else {
	    log.warning("Could not find suitable parent object for " + object.getName());
	    assert false;
	}
    }

    public void setMovableObjects(Object3DSet objects) {
	assert objects != null;
	assert moveFlags != null;
	if (objects.size() == 0) {
	    return;
	}
	for (int i = 0; i < moveFlags.length; ++i) {
	    moveFlags[i] = false; // nothing can be moved except the specified objects
	}
	for (int i = 0; i < objects.size(); ++i) {
	    setMovableObject(objects.get(i));
	}
    }

    /** reset each element to 1.0. Will be used to compute *geometric* mean of constraint violations for each coordinate system */
    private void resetCoordScoreAverages() {
	for (int i = 0; i < coordScoreAverages.length; ++i) {
	    coordScoreAverages[i] = 1.0;
	    coordScoreAveragesTmp[i] = 1.0;
	}
    }

    /** reset each element to 1.0. Will be used to compute *geometric* mean of constraint violations for each coordinate system */
    private void saveCoordScoreAverages() {
	for (int i = 0; i < coordScoreAverages.length; ++i) {
	    coordScoreAveragesSaved[i] = coordScoreAverages[i];
	}
    }

    /** reset each element to 1.0. Will be used to compute *geometric* mean of constraint violations for each coordinate system */
    private void restoreCoordScoreAverages() {
	for (int i = 0; i < coordScoreAverages.length; ++i) {
	    coordScoreAverages[i] = coordScoreAveragesSaved[i];
	}
    }

    /** Sets parent objects: this defines which blocks are moved as a group */
    private void setParentObjects(Object3DSet parentObjects) {
	this.parentObjects = parentObjects;
    }

    /** returns strand junction parent of itself */
    private Object3D findSuitableParentObject(Object3D b) {
	assert (b != null);
	if ((parentObjects != null) && (parentObjects.size() > 0)) {
	    Object3D result = Object3DSetTools.findAncestor(parentObjects,b);
	    if (result == null) {
		System.out.println("Could not find parent object for " +
				   b.getFullName() + " in: ");
		for (int i = 0; i < parentObjects.size(); ++i) {
		    System.out.println(parentObjects.get(i).getFullName());
		}
		return null;
	    }
	    assert(result != null);
	    log.fine("Suitable parent object of " + b.getFullName() + " is: " + result.getFullName());
	    return result;
	}
	if (b instanceof StrandJunction3D) {
	    return b;
	}
	Object3D result = Object3DTools.findAncestorByClassName(b, "StrandJunction3D");
	if (result != null) {
	    return result;
	}
	result = Object3DTools.findAncestorByClassName(b, "KissingLoop3D");
	if (result != null) {
	    return result;
	}
	return null;
    }

    /** apply tranformations to 3D objects. TODO: implementation not yet correct! */
    private void applyTransformations(List<CoordinateSystem> coords) {
	List<Object3D> appliedObjects = new ArrayList<Object3D>(); // keep track which objects have been transformed
	assert coords.size() == branchDescriptorGroups.length;
	assert moveFlags.length == coords.size();
	log.fine("Number of transformations to be applied: " + branchDescriptorGroups.length); 
	for (int i = 0; i < branchDescriptorGroups.length; ++i) {
	    if (!moveFlags[i]) {
		continue;
	    }
	    for (int j = 0; j < branchDescriptorGroups[i].length; ++j) {
		// find ancestor object:
		Object3D newRoot = findSuitableParentObject(branchDescriptors.get(branchDescriptorGroups[i][j]));
		if (newRoot != null) {
		    // check if one of previously rotated objects is parent
		    if (Object3DTools.findAncestorOrEqual(newRoot, appliedObjects) == null) {
			newRoot.activeTransform(coords.get(i));
			// log.fine("Applying transformation " + (i+1) + " " + coords.get(i) + " to " + newRoot.getName());
			appliedObjects.add(newRoot);
		    }
		}
	    }
	}
    }

    /** finds index of group to which "n" belongs, return -1 if not found */
    private int findGroupId(int[][] groups, int id) {
	for (int i = 0; i < groups.length; ++i) {
	    for (int j = 0; j < groups[i].length; ++j) {
		if (groups[i][j] == id) {
		    return i;
		}
	    }
	}
	return -1;
    }

    /** finds index of group to which all entries belong, return -1 if not found */
    private int[] findGroupIds(int[][] groups, int size) {
	int[] result = new int[size];
	for (int i = 0; i < size; ++i) {
	    result[i] = findGroupId(groups, i);
	}
	return result;
    }

    /** Computes distance error using 4 reference points (reference positions of first and last base pairs
     * @param branch1 : first branchdescriptor of outside part
     * @param branch2 : second branchdescriptor of outside part
     * @param cs1 : coordinate transformation applied to branch1
     * @param cs2 : coordinate transformation applied to branch2
     * @param constraint : helix constraint describing minimum and maximum stem length
     */
    static double computeBranchDescriptorError(BranchDescriptor3D branch1,
					       BranchDescriptor3D branch2,
					       CoordinateSystem cs1,
					       CoordinateSystem cs2,
					       int stemLength) {
	assert cs1.isValid();
	assert cs2.isValid();
	int lastBp = stemLength - 1;
	double[] t = new double[4];
	Vector3D p1o = branch1.computeHelixPosition(stemLength+1, BranchDescriptor3D.OUTGOING_STRAND);
	Vector3D p1i = branch1.computeHelixPosition(stemLength+1, BranchDescriptor3D.INCOMING_STRAND);
	Vector3D p2o = branch2.computeHelixPosition(stemLength+1, BranchDescriptor3D.OUTGOING_STRAND);
	Vector3D p2i = branch2.computeHelixPosition(stemLength+1, BranchDescriptor3D.INCOMING_STRAND);

	Vector3D q2i = branch2.computeHelixPosition(0, BranchDescriptor3D.INCOMING_STRAND);
	Vector3D q2o = branch2.computeHelixPosition(0, BranchDescriptor3D.OUTGOING_STRAND);
	Vector3D q1i = branch1.computeHelixPosition(0, BranchDescriptor3D.INCOMING_STRAND);
	Vector3D q1o = branch1.computeHelixPosition(0, BranchDescriptor3D.OUTGOING_STRAND);

	assert p1o.isValid();
	assert p2o.isValid();
	assert p1i.isValid();
	assert p2i.isValid();
	assert q1o.isValid();
	assert q1i.isValid();
	assert q2o.isValid();
	assert q2i.isValid();

	assert p1o.isReasonable();
	assert p2o.isReasonable();
	assert p1i.isReasonable();
	assert p2i.isReasonable();
	assert q1o.isReasonable();
	assert q1i.isReasonable();
	assert q2o.isReasonable();
	assert q2i.isReasonable();

	// apply transformations:
	p1o = cs1.activeTransform(p1o); // TODO : is activeTransform2 better?
	p1i = cs1.activeTransform(p1i);
	p2o = cs2.activeTransform(p2o);
	p2i = cs2.activeTransform(p2i);
	q1o = cs1.activeTransform(q1o);
	q1i = cs1.activeTransform(q1i);
	q2o = cs2.activeTransform(q2o);
	q2i = cs2.activeTransform(q2i);

	assert p1o.isValid();
	assert p2o.isValid();
	assert p1i.isValid();
	assert p2i.isValid();
	assert q1o.isValid();
	assert q1i.isValid();
	assert q2o.isValid();
	assert q2i.isValid();	

	assert p1o.isReasonable();
	assert p2o.isReasonable();
	assert p1i.isReasonable();
	assert p2i.isReasonable();
	assert q1o.isReasonable();
	assert q1i.isReasonable();
	assert q2o.isReasonable();
	assert q2i.isReasonable();	

	Vector3D h = p1o.minus(q2i);
	assert h.isValid();
	double d2 = h.getX()*h.getX() + h.getY()*h.getY() + h.getZ()*h.getZ();
	if (!DoubleTools.isValidAndNotNegative(d2)) {
	    log.severe("Could not compute length square of vector: " + h + " : " + d2);
	    assert false;
	}
	double d = h.lengthSquare();
	t[0] = p1o.distanceSquare(q2i);
	assert Math.abs(t[0] - d) < 0.001;
	t[1] = p1i.distanceSquare(q2o);
	t[2] = p2o.distanceSquare(q1i);
	t[3] = p2i.distanceSquare(q1o);

	for (int i = 0; i < 4; ++i) {
	    if (!DoubleTools.isValid(t[i])) {
		log.info("Not a number in t " + i + " " + t[i] + " in computeBranchDescriptorError: " + branch1.getName() + " " + branch2.getName() 
			 + cs1 + " " + cs2 + " points: " + p1o + "," + q2i + " " + p1i + "," + q2o + " "
			 + p2o + "," + q1i + " " + p2i + "," + q1o);
	    }
	}
	double result = t[DoubleArrayTools.findHighestElement(t)]; // corresponds to maximum norm
	assert (DoubleTools.isValidAndNotNegative(result));
	assert result >= 0.0;
	if (result >= 0) {
	    result = Math.sqrt(result);
	}
	else {
	    log.severe("Could not generate square root of " + result);
	    assert false;
	}
	if (!DoubleTools.isValidAndNotNegative(result)) {
	    log.info("Weird score in computeBranchDescriptorError: " + result + " " + branch1.getName() + " " + branch2.getName() 
		     + cs1 + " " + cs2 + " points: " + p1o + "," + q2i + " " + p1i + "," + q2o + " "
		     + p2o + "," + q1i + " " + p2i + "," + q1o);
	}
	assert DoubleTools.isValidAndNotNegative(result); // make sure it is not Not a Number or Infinite
	// log.info("BranchDescriptorError: " + result + " cs: " + cs1 + " cs2: " + cs2 + " bd1: " + branch1.getCoordinateSystem() 
	// + " bd2: " + branch2.getCoordinateSystem());
	return result;
    }

    /** Computes distance error using 4 reference points (reference positions of first and last base pairs;
     * Convinience method without local coordinate systems.
     * @param branch1 : first branchdescriptor of outside part
     * @param branch2 : second branchdescriptor of outside part
     * @param constraint : helix constraint describing minimum and maximum stem length
     */
    public static double computeBranchDescriptorError(BranchDescriptor3D branch1,
						      BranchDescriptor3D branch2,
						      HelixConstraintLink constraint) {
	assert false;
	return computeBranchDescriptorError(branch1, branch2, 
					    CoordinateSystem3D.CARTESIAN,
					    CoordinateSystem3D.CARTESIAN, constraint.getBasePairMax());
    }

    /** central method for scoring modified coordinate systems */
    private double scoreHelixConstraint(HelixConstraintLink helixConstraint, List<CoordinateSystem> coordinateSystems) {
	BranchDescriptor3D b1 = (BranchDescriptor3D)helixConstraint.getObj1();
	BranchDescriptor3D b2 = (BranchDescriptor3D)helixConstraint.getObj2();
	// how do I find the index of this branch descriptor?
	int id1 = branchDescriptors.indexOf(b1); // TODO slow!!!
	int id2 = branchDescriptors.indexOf(b2); // TODO slow!!!
	// how do I find the group to which these branch descriptors are connected?
	int g1 = branchDescriptorGroupIds[id1];
	int g2 = branchDescriptorGroupIds[id2];
	assert g1 >= 0;
	assert g2 >= 0;
	assert b1 != b2;
	assert coordinateSystems.get(g1).isValid();
	assert coordinateSystems.get(g2).isValid();
	int stemLength = helixConstraint.getBasePairMax();
	CoordinateSystem3D cs1 = (CoordinateSystem3D)(coordinateSystems.get(g1));
	CoordinateSystem3D cs2 = (CoordinateSystem3D)(coordinateSystems.get(g2));
	if (helixConstraint.getSymId1() > 0) {
	    cs1 = (CoordinateSystem3D)(cs1.cloneDeep());
	    CoordinateSystem s1 = symCopies.get(helixConstraint.getSymId1());
	    cs1.passiveTransform(s1);
	}
	if (helixConstraint.getSymId2() > 0) {
	    cs2 = (CoordinateSystem3D)(cs2.cloneDeep());
	    CoordinateSystem s2 = symCopies.get(helixConstraint.getSymId2());
	    cs2.passiveTransform(s2);
	}
	double result = computeBranchDescriptorError(b1, b2, cs1, cs2, stemLength);
	assert ! Double.isNaN(result); // make sure it is not Not a Number
	assert ! Double.isInfinite(result);
	if (result > 0.0) {
	    // coordScoreAveragesTmp[g1] *= result; TODO : maby uncomment? Reactivate ?
	    // coordScoreAveragesTmp[g2] *= result;
	    assert DoubleTools.isValidAndPositive(coordScoreAveragesTmp[g1]);
	    assert DoubleTools.isValidAndPositive(coordScoreAveragesTmp[g2]);
	}
	else {
	    log.fine("Zero constraint score between " + result + " " +  b1.getName()
		     + " and " + b2.getName());
	}
	return result;
    }

    public void setKeepFirstFixedMode(boolean mode) { this.keepFirstFixedMode = mode; }

    /** move coordinate system that are end-points do achieve perfectly matched helix constraints */
    private double applyEndPointHelixConstraint(HelixConstraintLink helixConstraint, List<CoordinateSystem> coordinateSystems) throws FittingException {
	assert helixConstraint != null;
	BranchDescriptor3D b1 = (BranchDescriptor3D)helixConstraint.getObj1();
	BranchDescriptor3D b2 = (BranchDescriptor3D)helixConstraint.getObj2();
	// how do I find the index of this branch descriptor?
	int id1 = branchDescriptors.indexOf(b1); // TODO slow!!!
	int id2 = branchDescriptors.indexOf(b2); // TODO slow!!!
	// how do I find the group to which these branch descriptors are connected?
	int g1 = branchDescriptorGroupIds[id1];
	int g2 = branchDescriptorGroupIds[id2];
	assert g1 >= 0;
	assert g2 >= 0;
	assert b1 != b2;
	assert coordinateSystems.get(g1).isValid();
	assert coordinateSystems.get(g2).isValid();
	boolean valid = false;
	if (groupConstraintCounts[g2] == 1) { // adjust second coordinate systems
	    perfectlyMatchHelixConstraint(helixConstraint, coordinateSystems);
	    // log.info("Perfectly matched helix constraint: " + helixConstraint + "\n");
	    valid = true;
	}
	else if (groupConstraintCounts[g1] == 1) { // adjust first coordinate system:
	    HelixConstraintLink constraintLink = ((HelixConstraintLink)(helixConstraint.clone()));
	    constraintLink.swap();
	    perfectlyMatchHelixConstraint(constraintLink, coordinateSystems);
	    // log.info("Perfectly matched (swapped) helix constraint: " + constraintLink + "\n");
	    valid = true;
	}

	double result = computeBranchDescriptorError(b1, b2, coordinateSystems.get(g1), coordinateSystems.get(g2), helixConstraint.getBasePairMax());
	assert ! Double.isNaN(result); // make sure it is not Not a Number
	assert ! Double.isInfinite(result);
	if (valid) {
	    if (addHelicesMode) {
		// ? FIXIT
	    }
	    if (result >= 0.0) {
// 		coordScoreAveragesTmp[g1] *= result; // TODO : uncomment? reactivate?
// 		coordScoreAveragesTmp[g2] *= result;
		assert DoubleTools.isValidAndPositive(coordScoreAveragesTmp[g1]);
		assert DoubleTools.isValidAndPositive(coordScoreAveragesTmp[g2]);
		log.fine("The placement score is: " + result);
	    }
	    else {
		log.fine("Negative constraint score between " + result + " " +  b1.getName()
			 + " and " + b2.getName());
	    }
	}
	return result;
    }


    /** central method for scoring modified coordinate systems */
    private CoordinateSystem perfectlyMatchHelixConstraint(int n,
							   CoordinateSystem cs1, HelixParameters prm) {
	LineShape line = Rna3DTools.computeHelix(n+1, cs1, prm); // position of c4' atom of outgoing and incoming strand
	Vector3D zDir = cs1.getZ();
	zDir.normalize();
	Vector3D newBase = cs1.getPosition().plus((zDir.mul((n+1)*prm.rise)).plus(zDir.mul(prm.offset)));
	Vector3D newX = line.getPosition2().minus(newBase); // reverse meaning of incoming and outgoing strand
	assert newX.length() > 0.0;
	newX.normalize();
	Vector3D newZ = zDir.mul(-1.0); // reverse direction
	Vector3D newY = newZ.cross(newX);
	log.fine("Initializing coordinate system with " +
		 newBase + " " + newX + " " + newY + " " + (newX.angle(newY)*RAD2DEG) + " " + (newZ.angle(newX)*RAD2DEG));
	return new CoordinateSystem3D(newBase, newX, newY);
    }

    /** central method for scoring modified coordinate systems */
    private double perfectlyMatchHelixConstraint(HelixConstraintLink helixConstraint, List<CoordinateSystem> coordinateSystems) throws FittingException {
	BranchDescriptor3D b1 = (BranchDescriptor3D)helixConstraint.getObj1();
	BranchDescriptor3D b2 = (BranchDescriptor3D)helixConstraint.getObj2();
	// how do I find the index of this branch descriptor?
	int id1 = branchDescriptors.indexOf(b1); // TODO slow!!!
	int id2 = branchDescriptors.indexOf(b2); // TODO slow!!!
	// how do I find the group to which these branch descriptors are connected?
	int g1 = branchDescriptorGroupIds[id1];
	int g2 = branchDescriptorGroupIds[id2];
	if (!moveFlags[g2]) {
	    throw new FittingException("Not allowed to move coordinate system " + g2);
	}
	assert g1 >= 0;
	assert g2 >= 0;
	assert b1 != b2;
	CoordinateSystem cs1 = coordinateSystems.get(g1);
	CoordinateSystem cs2 = coordinateSystems.get(g2);
	assert coordinateSystems.get(g1).isValid();
	assert coordinateSystems.get(g2).isValid();
	CoordinateSystem cs = b1.getCoordinateSystem();
	CoordinateSystem newCs = perfectlyMatchHelixConstraint(helixConstraint.getBasePairMin(), cs, defaultHelixParameters);
	newCs.activeTransform(cs1); // move according to cs1
	// decompose newCs, such that b2.getCoordinateSystem().activeTransform(newCs2) = newCs
	// TODO b2.getCoordSystem.activeTransform(x) = newCs  : x * b2cs = newCs  -> x = newCs * b2ssT
	CoordinateSystem cTmp = b2.getCoordinateSystem().inverse(); // first move back to origin
	cTmp.activeTransform(newCs);
	cs2 = cTmp;
	coordinateSystems.set(g2, cs2);
	double result = computeBranchDescriptorError(b1, b2, cs1, cs2, helixConstraint.getBasePairMax());
	assert ! Double.isNaN(result); // make sure it is not Not a Number
	assert ! Double.isInfinite(result);
	if (result > 0.0) {
	    // coordScoreAveragesTmp[g1] *= result; TODO : uncomment? reactivate?
	    // coordScoreAveragesTmp[g2] *= result;
	    assert DoubleTools.isValidAndPositive(coordScoreAveragesTmp[g1]);
	    assert DoubleTools.isValidAndPositive(coordScoreAveragesTmp[g2]);
	}
	else {
	    log.fine("Zero constraint score between " + result + " " +  b1.getName()
		     + " and " + b2.getName());
	}
	return result;
    }

    /** central method for scoring modified coordinate systems */
    private double scoreCoordinateSystems(List<CoordinateSystem> coordinateSystems) {
// 	log.info("Scoring coordianate systems: " );
// 	for (int i = 0 ;i < coordinateSystems.size(); ++i) {
// 	    System.out.println("" + coordinateSystems.get(i));
// 	}
	double score = 0.0;
	resetCoordScoreAverages();
	for (int i = 0; i < helixConstraints.size(); ++i) {
	    double term = scoreHelixConstraint(helixConstraints.get(i), coordinateSystems);
	    score += term * term;
	}
	// compute geometric mean:
	for (int i = 0; i < coordScoreAverages.length; ++i) {
	    coordScoreAveragesTmp[i] = Math.pow(coordScoreAveragesTmp[i], 1.0/(double)(groupConstraintCounts[i]));
	    // if (coordScoreAveragesTmp[i] < coordScoreAverages[i]) {
	    coordScoreAverages[i] = coordScoreAveragesTmp[i];
	    // }
	}
	firstPass = false;
	return score;
    }

    private Object3DSet convertToObject3DSet(List<Object3D> objects) {
	Object3DSet result = new SimpleObject3DSet();
	for (int i = 0; i < objects.size(); ++i) {
	    result.add(objects.get(i));
	}
	return result;
    }

    public static CoordinateSystem findCoordinateSystem(List<BranchDescriptor3D> branchDescriptors,
							  int[] branchDescriptorGroup) {
	Object3DSet set = new SimpleObject3DSet();
	for (int i = 0; i < branchDescriptorGroup.length; ++i) {
	    set.add(branchDescriptors.get(branchDescriptorGroup[i]));
	}
	CoordinateSystem result = new CoordinateSystem3D(new Vector3D(0,0,0), // Object3DSetTools.centerOfMass(set),
							 Vector3D.EX, Vector3D.EY);
	return result;
    }

    
    private static List<CoordinateSystem> findCoordinateSystems(List<BranchDescriptor3D> branchDescriptors,
								  int[][] branchDescriptorGroups) {
	// assert branchDescriptorGroups.length == branchDescriptors.size();
	List<CoordinateSystem> result = new ArrayList<CoordinateSystem>();
	for (int i = 0; i < branchDescriptorGroups.length; ++i) {
	    result.add(findCoordinateSystem(branchDescriptors, branchDescriptorGroups[i]));
	}
	return result;
    }

    private List<BranchDescriptor3D> findBranchDescriptors(List<HelixConstraintLink> helixConstraints) {
	List<BranchDescriptor3D> result = new ArrayList<BranchDescriptor3D>();
	for (int i = 0; i < helixConstraints.size(); ++i) {
	    BranchDescriptor3D b1 = (BranchDescriptor3D)(helixConstraints.get(i).getObj1());
	    if (!result.contains(b1)) {
		result.add(b1);
	    }
	    BranchDescriptor3D b2 = (BranchDescriptor3D)(helixConstraints.get(i).getObj2());
	    if (!result.contains(b2)) {
		result.add(b2);
	    }
	}
	return result;
    }

    /** checks if from same junction or kissing loop */
    public boolean checkBranchDescriptorsConnected(BranchDescriptor3D b1, BranchDescriptor3D b2, LinkSet links) {
	return findSuitableParentObject(b1) == findSuitableParentObject(b2); 
    }

    /** returns groups of branch descriptors that are moved as one block. Typical example: all branchdescriptors of one junction 
     * are moved leaving their relative orientation constant. */
    private int[][] findBranchDescriptorGroups(List<BranchDescriptor3D> branchDescriptors,
					       LinkSet allLinks) {
	boolean[][] adjacencyMatrix = findBranchDescriptorGroupMatrix(branchDescriptors, allLinks);
	// AdjacencyTools.writeMatrix(System.out, adjacencyMatrix);
	List<List<Integer> > groups = AdjacencyTools.findConnectedSets(adjacencyMatrix); // find solution to clique problem!	
// 	System.out.println("Found groups: " );
// 	AdjacencyTools.writeGroups(System.out, groups);
	int[][] result = new int[groups.size()][];
	for (int i = 0; i < groups.size(); ++i) {
	    result[i] = new int[groups.get(i).size()];
	    for (int j = 0; j < groups.get(i).size(); ++j) {
		result[i][j] = groups.get(i).get(j).intValue();
	    }
	}
	return result;
    }

    private boolean[][] findBranchDescriptorGroupMatrix(List<BranchDescriptor3D> branchDescriptors,
							       LinkSet allLinks) {
	assert branchDescriptors.size() > 0;
	boolean[][] result = new boolean[branchDescriptors.size()][branchDescriptors.size()];
	for (int i = 0; i < result.length; ++i) {
	    result[i][i] = true; // each branch descriptor is in same group as itself
	    for (int j = i+1; j < result[i].length; ++j) {
		result[i][j] = checkBranchDescriptorsConnected(branchDescriptors.get(i), branchDescriptors.get(j), allLinks);
		result[j][i] = result[i][j];
	    }
	}
	return result;
    }

    private List<CoordinateSystem> cloneCoordinateSystems(List<CoordinateSystem> coords) {
	List<CoordinateSystem> result = new ArrayList<CoordinateSystem>();
	for (int i = 0; i < coords.size(); ++i) {
	    CoordinateSystem3D csClone = (CoordinateSystem3D)(coords.get(i).cloneDeep());
	    assert csClone.size() == ((CoordinateSystem3D)(coords.get(i))).size();
	    assert csClone.size() == 3;
	    result.add(csClone);
	}
	return result;
    }
    private void copyCoordinateSystems(List<CoordinateSystem> optCoords, 
				       List<CoordinateSystem> origCoords) {
	assert optCoords != null;
	assert origCoords != null;
	assert optCoords.size() == origCoords.size();
	for (int i = 0; i < origCoords.size(); ++i) {	    
	    CoordinateSystem3D cs = (CoordinateSystem3D)(optCoords.get(i));
// 	    log.info("Coordinate system " + (i+1) + " " + cs.toString());
// 	    assert ((CoordinateSystem3D)(optCoords.get(i))).size() == 3;
// 	    assert ((CoordinateSystem3D)(origCoords.get(i))).size() == 3;
	    optCoords.get(i).copy(origCoords.get(i));
	}
    }

    /** how many constraints are invold in group n. TODO: should be much faster */
    private int countGroupConstraints(int n) {
	int count = 0;
	for (int i = 0; i < helixConstraints.size(); ++i) {
	    BranchDescriptor3D b1 = (BranchDescriptor3D)helixConstraints.get(i).getObj1();
	    BranchDescriptor3D b2 = (BranchDescriptor3D)helixConstraints.get(i).getObj2();
	    // how do I find the index of this branch descriptor?
	    int id1 = branchDescriptors.indexOf(b1); // TODO slow!!!
	    int id2 = branchDescriptors.indexOf(b2); // TODO slow!!!
	    // how do I find the group to which these branch descriptors are connected?
	    int g1 = branchDescriptorGroupIds[id1];
	    int g2 = branchDescriptorGroupIds[id2];
	    // assert g1 != g2;
	    if ((g1 == g2) && (helixConstraints.get(i).getSymId1() == helixConstraints.get(i).getSymId2())) {
		log.warning("Helix constraint + " + (i+1) + " points twice to the same group: " + helixConstraints.get(i));
		assert helixConstraints.get(i) instanceof HelixConstraintLink;
	    }
	    if (g1 == n) {
		++count;
	    }
	    if (g2 == n) {
		++count;
	    }
	}
	return count;
    }

    private void mutateCoordinateSystems(List<CoordinateSystem> coords, double annealingMult) {
	assert coords != null;
	assert !Double.isInfinite(annealingMult);
	assert !Double.isNaN(annealingMult);
	int startId = 0;
	if (keepFirstFixedMode && (coords.size() > 1)) {
	    startId = 1;
	}
	// first move coordinate systems that are not end points:
	assertMovables();
	// log.info("Starting mutateCoordinateSystems with " + coords.size() + " coordinate systems and a start id of " + (startId + 1));
	for (int i = startId; i < coords.size(); ++i) {
	    // log.info("Trying to mutate coordinate system " + i);
	    if (!moveFlags[i]) {
		log.info("Not mutating coordinate system " + i + " because it is not allowed to move");
		continue; // do not mutate this coordinate system
	    }
 	    if (groupConstraintCounts[i] < 1) {
 		log.info("Not mutating coordinate system " + i + " because it has no constraint: " + groupConstraintCounts[i]);
 		continue;// only move if more than one constraint at this coordinate system
 	    }
	    mutator.copy(mutatorOrig);
	    assert coordScoreAverages[i] >= 0.0;
	    assert !Double.isInfinite(coordScoreAverages[i]);
	    assert !Double.isNaN(coordScoreAverages[i]);
	    // use geometric mean of scores:
	    double totalScale = annealingMult * scaleMult * coordScoreAverages[i];
	    assert DoubleTools.isReasonable(totalScale);
 	    // log.info("Initial scale for mutator: " + totalScale + " " + annealingMult + " " 
	    // + scaleMult + " " + coordScoreAverages[i]);
	    if (totalScale > scaleLimit) {
		totalScale = scaleLimit;
	    }
	    assert !Double.isInfinite(totalScale);
	    assert !Double.isNaN(totalScale);
	    mutator.scaleAngleStep(totalScale);
	    mutator.scaleTranslationStep(totalScale);
  	    log.fine("Mutator used for coordinate system: " + (i+1) + " " + coords.get(i)
  		     + " : " + totalScale + " mut: " + mutator);
	    mutator.rotate(coords.get(i));
	    mutator.translate(coords.get(i));
	    // log.info("result of mutation: " + coords.get(i));
	}
	assertMovables();
	// now move coordinate systems that are end points (having only one constraint)
	for (int i = 0; i < helixConstraints.size(); ++i) {
	    try {
		applyEndPointHelixConstraint(helixConstraints.get(i), coords);
	    }
	    catch (FittingException fe) {
		log.info("Not allowed to move coordinate system for end point fitting: " + fe.getMessage());
	    }
	}
	assertMovables();
    }
    
    private void printCoordinateSystems(List<CoordinateSystem> coords) {
	for (int i = 0; i < coords.size(); ++i) {
	    System.out.println("" + (i+1) + " " + coords.get(i).toString());
	}
    }

    /** returns true, if at least one coordinate system to be optimized has more than one constraints attached to it */
    private boolean hasComplexGroups() {
// 	for (int i = 0; i < groupConstraintCounts.length; ++i) {
// 	    if (moveFlags[i] && (groupConstraintCounts[i] > 1)) {
// 		return true;
// 	    }
// 	}
// 	return false;
	return true; // TODO : better implementation
    }

    private String boolFlagString(boolean[] flags) {
	String s = "";
	for (int i = 0; i < flags.length; ++i) {
	    s = s + flags[i] + " ";
	}
	return s;
    }

    // saves helix constraints lengths to integer array
    private void extractHelixConstraintLengths(List<HelixConstraintLink> helixConstraints, int[] helixLengths) {
	assert(helixLengths != null);
	assert(helixConstraints != null);
	assert(helixLengths.length== helixConstraints.size());
	for (int i = 0; i < helixLengths.length; ++i) {
	    assert(helixConstraints.get(i).getBasePairMin() == helixConstraints.get(i).getBasePairMax());
	    helixLengths[i] = helixConstraints.get(i).getBasePairMin();
	}
    }

    // sets helix constraints lengths from integer array
    private void setHelixConstraintLengths(int[] helixLengths, List<HelixConstraintLink> helixConstraints) {
	assert(helixLengths.length== helixConstraints.size());
	for (int i = 0; i < helixLengths.length; ++i) {
	    assert(helixConstraints.get(i).getBasePairMin() == helixConstraints.get(i).getBasePairMax());
	    helixConstraints.get(i).setBasePairMin(helixLengths[i]);
	    helixConstraints.get(i).setBasePairMax(helixLengths[i]);
	}
    }

    /** Mutates lengths of helix constraints with given probability */
    void mutateHelixLengths(List<HelixConstraintLink> helixConstraints, double helixConstraintMutationProb) {
	Random rand = Randomizer.getInstance(); // singleton pattern
	boolean modified = false;
        for (int i = 0; i < helixConstraints.size(); ++i) {
	    double x = rand.nextDouble();
	    if (x < helixConstraintMutationProb) { // mutate
		HelixConstraintLink hcLink = helixConstraints.get(i);
		assert (hcLink.getBasePairMin() == hcLink.getBasePairMax());
		int bp = hcLink.getBasePairMin();
		if (x < (0.5 * helixConstraintMutationProb) ) { // 50% chance of decrease
		    if (bp > 0) {
			hcLink.setBasePairMin(bp - 1);
			hcLink.setBasePairMax(bp - 1);
			assert(helixConstraints.get(i).getBasePairMin() == bp-1);
			assert(helixConstraints.get(i).getBasePairMax() == bp-1);
			modified = true;
		    }
		} else { // increase
		    hcLink.setBasePairMin(bp + 1);
		    hcLink.setBasePairMax(bp + 1);
		    modified = true;
		    assert(helixConstraints.get(i).getBasePairMin() == bp+1);
		    assert(helixConstraints.get(i).getBasePairMax() == bp+1);
		}
	    }
	}
	if (!modified) {
	    log.warning("No helix lengths were modified!");
	} else {
	    System.out.print("Modified helix constraint lengths: ");
	    for (int i = 0; i < helixConstraints.size(); ++i) {
		System.out.print("" + helixConstraints.get(i).getBasePairMin() + " ");
	    }
	    System.out.println();
	}
    }

    public Properties optimize() {
	log.info("Starting helix optimization! Number of found helix constraints: " + helixConstraints.size()
		 + " Number of found distance constraints: " + distanceConstraints.size()
		 + " error limit: " + errorScoreLimit + " initial error limit: " + errorScoreInitialLimit
		 + " keep-first-fixed: " + keepFirstFixedMode);
	Properties properties = new Properties();
	Random rand = Randomizer.getInstance(); // singleton pattern
	if ((optCoordinateSystems == null) || (optCoordinateSystems.size() == 0)) {
	    String errMsg = "No coordinate systems defined for optimization. Maybe add helix constraints?";
	    log.fine(errMsg);
	    properties.setProperty(MESSAGE, errMsg);
	    properties.setProperty(ERROR, errMsg);
	    return properties;
	}
	log.fine("Number of relevant coordinate systems: " + origCoordinateSystems.size());
	log.fine("Movable coordinate systems: " + boolFlagString(moveFlags));
	String s2 = "";
	assert(branchDescriptors != null);
	for (int i = 0; i < branchDescriptors.size(); ++i) {
	    assert(branchDescriptors.get(i) != null);
	    if (findSuitableParentObject(branchDescriptors.get(i)) == null) {
		properties.setProperty(ERROR, "No suitable parent found for " + branchDescriptors.get(i).getFullName());
		return properties;
	    }
	    assert(findSuitableParentObject(branchDescriptors.get(i)) != null);
	    assert(branchDescriptorGroupIds != null);
	    s2 = s2 + " " + i + " " + branchDescriptors.get(i).getFullName();
	    s2 = s2 + " " + branchDescriptorGroupIds[i];
	    s2 = s2 + " parent: " + findSuitableParentObject(branchDescriptors.get(i)).getFullName() + " " + NEWLINE;
	}
	log.fine("Branch descriptors in optimization: " + s2);
	log.fine("Number of relevant coordinate systems: " + origCoordinateSystems.size());
	firstPass = true;
	// mutator.copy(mutatorOrig); // reset mutator in case several optimizations are run
// 	printCoordinateSystems(origCoordinateSystems);
// 	printCoordinateSystems(optCoordinateSystems);
	copyCoordinateSystems(optCoordinateSystems, origCoordinateSystems); // copies original to opt coordinate systems
	List<CoordinateSystem> saveCoords = cloneCoordinateSystems(optCoordinateSystems);
	List<CoordinateSystem> bestCoords = cloneCoordinateSystems(optCoordinateSystems);
        int[] helixBestLengths = new int[helixConstraints.size()]; // not sure if this works if size zero
        int[] helixSavedLengths = new int[helixConstraints.size()]; // not sure if this works if size zero
	extractHelixConstraintLengths(helixConstraints,helixSavedLengths); // copies working copy to safety copy
	extractHelixConstraintLengths(helixConstraints,helixBestLengths); // copies working copy to safety copy
	double oldScore = scoreCoordinateSystems(optCoordinateSystems);
	// if the starting score is too bad, do not even start optimization:
	if (oldScore > errorScoreInitialLimit) {
	    String errMsg = "No helix optimization started because the initial score " + oldScore
			+ " is higher than the maximum initial error score limit: " + errorScoreInitialLimit;
	    log.warning(errMsg);
	    properties.setProperty(MESSAGE, errMsg);
	    properties.setProperty(ERROR, errMsg);
	    return properties;
	}
	saveCoordScoreAverages();
	double bestScore = oldScore;
	double kt = ktOrig;
	double oldScoreMul = 0.1; // emperical factor for initial kt
	if ((oldScoreMul*oldScore) > kt) {
	    kt = oldScoreMul * oldScore;
	}
	properties.setProperty(START_SCORE,""+ bestScore);
	int iter = 0;
	assertMovables();
        // central loop over optimization steps
	for (iter = 0; iter < numberSteps; ++iter) {
            boolean mutateConstraintsMode = false; // if true, do not mutate coordinate systems, but constraints
	    if ((helixMutInterval > 0) && (iter > 0) && ((iter % helixMutInterval) == 0)) {
 		mutateConstraintsMode = true;
 		log.info("Mutating helix constraints!");
 	    }
	    double annealingMult = Math.pow(annealingFactor, (int)(iter/annealingInterval));
	    kt = ktOrig * annealingMult;
	    if (((iter % outputInterval) == 1) || mutateConstraintsMode) {
		String scoreString = "";
		for (int i = 0; i < coordScoreAverages.length; ++i) {
		    scoreString = scoreString + Math.sqrt(coordScoreAverages[i]) + " ";
		}
                if (mutateConstraintsMode) {
		    log.info("Iteration: " + iter + " Best score so far: " + bestScore 
			     + " . Annealing paramenters: " + annealingMult
			     + ", " + kt // + " coordinate system multipliers: " + scoreString
			     + " Helix lengths: ");		    
		    for (int i = 0; i < helixSavedLengths.length; ++i) {
			System.out.print("" + helixSavedLengths[i] + " ( " + helixBestLengths[i] + " ) ");
		    }
		} else {
		    log.info("Iteration: " + iter + " Best score so far: " + bestScore 
			     + " . Annealing parameters: " + annealingMult
			     + " , " + kt); //  + " coordinate system multipliers: " + scoreString);
		}
	    }
	    copyCoordinateSystems(saveCoords, optCoordinateSystems); // copies working copy to safety copy
	    if (!mutateConstraintsMode) {
		// System.out.println("Mutating coordinate system...");
		mutateCoordinateSystems(optCoordinateSystems, annealingMult);
	    } else {
		assert(helixSavedLengths.length== helixConstraints.size());
		extractHelixConstraintLengths(helixConstraints,helixSavedLengths); // copies working copy to safety copy
		mutateHelixLengths(helixConstraints, helixConstraintMutationProb);
	    }
	    double newScore = scoreCoordinateSystems(optCoordinateSystems);
	    // System.out.println("New score: " + newScore);
	    if (Math.exp(-((newScore-oldScore)/kt)) > rand.nextDouble()) {
		oldScore = newScore;
		saveCoordScoreAverages();
		if (newScore < bestScore) {
		    bestScore = newScore;
		    copyCoordinateSystems(bestCoords, optCoordinateSystems); // stores best trafos
		    if (helixMutInterval > 0) {
			extractHelixConstraintLengths(helixConstraints, helixBestLengths); // stores helix lengths
		    }
		    log.fine("New best score found: " + bestScore + " at iteration " + (iter + 1) + " using kt " + kt);
		    if (helixMutInterval > 0) {
			System.out.println("Currently best helix constraint lengths: " );
			for (int i = 0; i < helixSavedLengths.length; ++i) {
			    System.out.print("" + helixBestLengths[i] + " ");
			}
		    }
		    // outputCoordinateSystems(System.out,bestCoords);
		}
	    }
	    else {
		log.fine("Rejecting step : " + newScore + " with so far best score: " + bestScore );
		copyCoordinateSystems(optCoordinateSystems, saveCoords); // restore previous coordinate systems
		if (mutateConstraintsMode) {
		    setHelixConstraintLengths(helixSavedLengths, helixConstraints); // stores helix lengths
		}
		restoreCoordScoreAverages();
	    }
	    if (newScore < errorScoreLimit) {
		log.fine("Optimization goal achieved!");
		break;
	    }
	    if (!hasComplexGroups()) {
		log.fine("Quitting optimization loop, because all coordinate systems could be placed analytically.");
		break;
	    }
	}
	// apply transformation to objects!
	applyTransformations(bestCoords);
	if (helixMutInterval > 0) {
	    setHelixConstraintLengths(helixBestLengths, helixConstraints);
	}
	properties.setProperty(NUMBER_STEPS, "" + (iter+1));
	properties.setProperty(FINAL_SCORE, ""+ bestScore);
	log.info("Optimization finished: " + properties);
	assertMovables();
	return properties;
    }

    void outputCoordinateSystems(PrintStream ps, List<CoordinateSystem> coords) {
	ps.println("Coordinate systems: ");
	for (int i = 0; i < coords.size(); ++i) {
	    ps.println("" + i + " " + coords.get(i));
	}
    }

    void assertMovables() {
	for (int i = 0; i < moveFlags.length; ++i) {
	    if (origCoordinateSystems.get(i) == optCoordinateSystems.get(i)) {
		assert false;
	    }
	    if ((!moveFlags[i])  && (origCoordinateSystems.get(i).distance(optCoordinateSystems.get(i)) > 0.1)) {
		assert false; // coordinate system was moved!
	    }
	}
    }

    public boolean validate() {
	return moveFlags != null && moveFlags.length > 0;
    }

}
