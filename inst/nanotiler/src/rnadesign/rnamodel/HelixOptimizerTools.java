package rnadesign.rnamodel;


import java.util.logging.*;
import java.util.Properties;
import java.util.List;
import java.util.ArrayList;
import java.util.Random;
import generaltools.Optimizer;
import generaltools.Randomizer;
import tools3d.objects3d.*;
import tools3d.symmetry2.*;
import tools3d.*;
import graphtools.AdjacencyTools;
import numerictools.DoubleArrayTools;
import numerictools.DoubleTools;

import rnadesign.rnamodel.PackageConstants.*;

import static rnadesign.rnamodel.PackageConstants.*;

import generaltools.*;

public class HelixOptimizerTools {

    private static Logger log = Logger.getLogger(LOGFILE_DEFAULT);

    public static final int NO_FUSE = 0;
    public static final int FUSE_APPEND = 1;
    public static final int FUSE_PREPEND = 2;

    public static final String GENERATED_HELICES = "generated_helices";

    /** Returns standard name of helix connecting two branch descriptors */
    public static String generateHelixName(BranchDescriptor3D bd1, BranchDescriptor3D bd2) {
	return "h_" + Object3DTools.getFullName(bd1).replace('.','_') 
	    + "_" + Object3DTools.getFullName(bd2).replace('.','_'); // replace dot with underscore for save name generateion
    }

    /** Generates helix between two branch descriptors using specified connection algorithm (see HelixOptimizerFactory)
     */
    public static Object3DLinkSetBundle generateHelix(HelixConstraintLink hcl, int connectionAlgorithm, FitParameters stemFitParameters,
						      Object3D nucleotideDB) throws FittingException {
	char c1 = 'G';
	char c2 = 'C'; // TODO : make changeable!
	BranchDescriptor3D bd1 = (BranchDescriptor3D)(hcl.getObj1());
	BranchDescriptor3D bd2 = (BranchDescriptor3D)(hcl.getObj2());
	String helixName = generateHelixName(bd1, bd2);
	if (hcl.getSymId1() > 0) {
	    bd1 = (BranchDescriptor3D)(bd1.cloneDeep());
	    SymCopies symCopies = SymCopySingleton.getInstance(); // current list of symmetry operations
	    CoordinateSystem cs = symCopies.get(hcl.getSymId1());
	    bd1.activeTransform(cs);
	}
	if (hcl.getSymId2() > 0) {
	    bd2 = (BranchDescriptor3D)(bd2.cloneDeep());
	    SymCopies symCopies = SymCopySingleton.getInstance(); // current list of symmetry operations
	    CoordinateSystem cs = symCopies.get(hcl.getSymId2());
	    bd2.activeTransform(cs);
	}
	Properties properties = new Properties();
	log.info("Starting to generate interpolating helix with length " + hcl.getBasePairMin());
	Object3DLinkSetBundle stemBundle = ConnectJunctionTools.generateConnectingStem(bd1, bd2,c1, c2,
				       helixName, stemFitParameters, nucleotideDB, connectionAlgorithm, hcl.getBasePairMin());
	if ((stemBundle == null) || (stemBundle.getObject3D() == null) || (stemBundle.getObject3D().size() < 1)) {
	    throw new FittingException("Could not generate helix for branch descriptors " + bd1.getFullName() + " " + bd2.getFullName());
	}
	return stemBundle;
    }

    /** Generates helix between two branch descriptors using specified connection algorithm (see HelixOptimizerFactory)
     */
    public static Properties generateHelix(HelixConstraintLink hcl, int connectionAlgorithm, FitParameters stemFitParameters,
					   Object3D nucleotideDB,
					   Object3D root, LinkSet links, int fuseStrandsMode) throws FittingException {
	Properties properties = new Properties();
	log.fine("Starting to generate interpolating helix with length " + hcl.getBasePairMin());
	Object3DLinkSetBundle stemBundle = generateHelix(hcl, connectionAlgorithm, stemFitParameters, nucleotideDB);
	//  ConnectJunctionTools.generateConnectingStem(bd1, bd2,
	// 	       helixName, stemFitParameters, nucleotideDB, connectionAlgorithm, hcl.getBasePairMin());
	if (stemBundle != null) {
	    log.fine("Stem bundle generated with ConnectJunctionTools!");
	    if (stemBundle.getObject3D() != null) {
		log.fine("Adding helix object " + stemBundle.getObject3D().getName()
			 + " to tree node: " + root.getFullName());
		switch (fuseStrandsMode) {
		case NO_FUSE:
		    assert(stemBundle.getObject3D() != null);
		    root.insertChildSafe(stemBundle.getObject3D()); // possibly renames helix if name already exists
		    PropertyTools.addProperty(properties, GENERATED_HELICES, stemBundle.getObject3D().getName());
		    break;
		case FUSE_APPEND: {
		    Object3DSet strands = Object3DTools.collectByClassName(stemBundle.getObject3D(), "RnaStrand");
		    assert strands.size() == 2;
		    RnaStrand strand1 = (RnaStrand)(((BranchDescriptor3D)(hcl.getObj1())).getOutgoingStrand());
		    RnaStrand strand2 = (RnaStrand)(((BranchDescriptor3D)(hcl.getObj2())).getOutgoingStrand());
		    strand1.append((RnaStrand)(strands.get(0)));
		    strand2.append((RnaStrand)(strands.get(1)));
		}
		    break;
		case FUSE_PREPEND: {
		    Object3DSet strands = Object3DTools.collectByClassName(stemBundle.getObject3D(), "RnaStrand");
		    assert strands.size() == 2;
		    RnaStrand strand1 = (RnaStrand)(((BranchDescriptor3D)(hcl.getObj2())).getIncomingStrand());
		    RnaStrand strand2 = (RnaStrand)(((BranchDescriptor3D)(hcl.getObj1())).getIncomingStrand());
		    strand1.prepend((RnaStrand)(strands.get(0)));
		    strand2.prepend((RnaStrand)(strands.get(1)));
		}
		    break;
		}
		links.merge(stemBundle.getLinks());
	    }
	    else {
		log.fine("No stems were generated!");
	    }
	}
	else {
	    log.fine("No stems and links were generated!");
	}
	return properties;
    }

    /** Optimizes helices specified by helix constraints by moving occording building blocks */
    public static Properties optimizeHelices(int numSteps, double errorScoreLimit, double errorScoreInitialLimit,
					     double kt, FitParameters stemFitParameters,
					     Object3DSet movableObjects,
					     Object3DSet parentObjects,
					     Object3D root, LinkSet links,
					     Object3D nucleotideDB,
					     int connectionAlgorithm,
					     int fuseStrands,
					     boolean firstFixedMode,
					     int helixMutInterval) throws FittingException {
	HelixOptimizer optimizer = new HelixOptimizer(root, links, numSteps, errorScoreLimit, parentObjects);
	if (!optimizer.validate()) {
	    throw new FittingException("Could not generate valid optimizer. Maybe no helix constraints are specified?");
	}
	optimizer.setKtOrig(kt);
	optimizer.setHelixMutInterval(helixMutInterval); // if greater zero: mutate helix lengths every this many steps
	optimizer.setKeepFirstFixedMode(firstFixedMode);
	if ((movableObjects != null) && (movableObjects.size() > 0)) {
	    log.fine("Setting movable objects: " + movableObjects.size());
	    optimizer.setMovableObjects(movableObjects); // keep everything else fixed in this mode
	}
	else {
	    log.fine("All objects are considered movable in optimization!");
	}
	int helixCount = 0;
	optimizer.setErrorScoreInitialLimit(errorScoreInitialLimit);
	Properties properties = optimizer.optimize(); // Actual optimization!!
	log.fine("Optimization of helices finished: " + properties);
	// optionally add linker helices
	if ((stemFitParameters != null) && (properties != null) && ((properties.getProperty(Optimizer.ERROR) == null)
	    || (properties.getProperty(Optimizer.ERROR).length() > 0))) {
	    // int connectionAlgorithm = BranchDescriptorOptimizerFactory.MONTE_CARLO_OPTIMIZER; /** Algorithm used for stem interpolation. */
	    for (int i = 0; i < links.size(); ++i) {
		if (links.get(i) instanceof HelixConstraintLink) {
		    ++helixCount;
		    HelixConstraintLink hcl = (HelixConstraintLink)(links.get(i));
		    BranchDescriptor3D bd1 = (BranchDescriptor3D)(hcl.getObj1());
		    BranchDescriptor3D bd2 = (BranchDescriptor3D)(hcl.getObj2());
		    try {
			Properties newProperties = generateHelix(hcl, connectionAlgorithm, stemFitParameters, nucleotideDB, root, links,
								 fuseStrands);
			PropertyTools.merge(properties, newProperties); // keeps track of "generated_helices" properties with all helix names

		    }
		    catch (FittingException fe) {
			log.warning("Could not place helix between branch descriptors " 
				    + bd1.getFullName() + " "
				    + bd2.getFullName());
		    }
		}
	    }
	    // assert helixCount == 0 || properties.getProperty(GENERATED_HELICES).split(PropertyTools.DELIMITER).length == helixCount; 
	}
	// refresh(new ModelChangeEvent(this, Object3DGraphControllerEventConstants.MODEL_MODIFIED));
	return properties;
    }
}
