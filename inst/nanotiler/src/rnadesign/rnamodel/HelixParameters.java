package rnadesign.rnamodel;

import static rnadesign.rnamodel.RnaConstants.*;
import tools3d.Vector3D;
import tools3d.CoordinateSystem;
import tools3d.objects3d.CoordinateSystem3D;

/** Parameters describing shape (but not orientation and position) of a RNA helix.
 * @see Aalberts 2005 as well as http://mathworld.wolfram.com/Helix.html */
public class HelixParameters {

    public double rise; //  = RNA3DTools._RISE;

    public double basePairsPerTurn; //  = RNA3DTools._BASE_PAIRS_PER_TURN;

    public double offset; //  = RNA3DTools._OFFSET;

    public double phaseOffset; // = RNA3DTools._PHASE_OFFSET;

    public double radius;

    /** rotation and translation of ideal geometry per base pair */
    private CoordinateSystem deformation;

    public HelixParameters() {
	rise = HELIX_RISE;	
	basePairsPerTurn = HELIX_BASE_PAIRS_PER_TURN;
	offset = HELIX_OFFSET;
	phaseOffset = HELIX_PHASE_OFFSET;
	radius = HELIX_RADIUS;
    }

    public HelixParameters(HelixParameters other) {
	rise = other.rise;
	basePairsPerTurn = other.basePairsPerTurn;
	offset = other.offset;
	phaseOffset = other.phaseOffset;
	radius = other.radius;
	if (other.deformation != null) {
	    deformation = (CoordinateSystem)(other.deformation.cloneDeep());
	}
	else {
	    deformation = null;
	}
    }

    /** computes curvature */
    public double computeCurvature() { return (radius / ((radius*radius) + (rise * rise))); }

    /** computes torsion */
    public double computeTorsion() { return (rise / ((radius*radius) + (rise * rise))); }

    public double getRise() { return rise; }

    public double getBasePairsPerTurn() { return basePairsPerTurn; }

    public CoordinateSystem getDeformation() { return deformation; }

    public double getOffset() { return offset; }

    public double getRadius() { return radius; }

    public void initDeformation() { this.deformation = new CoordinateSystem3D(Vector3D.ZVEC); }

    public void setRise(double rise) { this.rise = rise; }

    public void setBasePairsPerTurn(double x) { this.basePairsPerTurn = x; }

    public void setDeformation(CoordinateSystem deformation) { this.deformation = deformation; }

    public void setOffset(double offset) { this.offset = offset; }

    public void setPhaseOffset(double phaseOffset) { this.phaseOffset = phaseOffset; }

    public void setRadius(double radius) { this.radius = radius; }

    public String toString() {
	String result = "rise: " + rise + " basePairsPerTurn: " + basePairsPerTurn + " offset: " + offset
	    + " phaseOffset: " + phaseOffset + " radius: " + radius;
	if (deformation != null) {
	    result = result + " def-x: " + deformation.getX() + " def-y: " + deformation.getY() + " def-z: "
		+ deformation.getZ();
	}
	return result;
    }

}
