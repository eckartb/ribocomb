package rnadesign.rnamodel;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import tools3d.objects3d.*;

import static rnadesign.rnamodel.PackageConstants.NEWLINE;

public class HeteroAtomPdbWriter extends AbstractPdbWriter implements Object3DLinkSetBundleWriter {

    public int getFormatId() { return PDB_FORMAT; }

    private boolean polygrafMode = true; // if true, compatible with "tra" progam from signature package

    public HeteroAtomPdbWriter() { this.dialect = DIALECT_SIGNATURE; }
    
    /** generates PDB header */
    public String generateHeader(String description, String author) {
	StringBuffer buf = new StringBuffer();
	buf.append("BIOGRF  321" + NEWLINE);
	buf.append("DESCRP " + description + NEWLINE);
	buf.append("REMARK Created by " + author + " on " + (new Date()));
	buf.append("REMARK " + NEWLINE);
	if (polygrafMode) {
	    buf.append("FORCEFIELD DREIDING" + NEWLINE);
	    buf.append("FORMAT ATOM   (a6,1x,i5,1x,a5,1x,a3,1x,a1,1x,a5,3f10.5,1x,a5,i3,i2,1x,f8.5,f10.5)"
		       + NEWLINE);
	}
	return buf.toString();
    }
   
    private String generateFooter() {
	return "END" + NEWLINE;
    }

    private String generateBody(Object3DSet atoms, String residueName, char strandChar, int residueCount) {
	StringBuffer buf = new StringBuffer();
	for (int i = 0; i < atoms.size(); ++i) {
	    buf.append(writeHeteroAtom((Atom3D)atoms.get(i), residueName, strandChar, residueCount) 
		       + NEWLINE);
	}
	return buf.toString();
    }

    private int getPdbAtomNumber(Object3D atom, int defaultId) {
	String idString = atom.getProperty("pdb_atom_id");
	int result = defaultId;
	if (idString == null) {
	    return result;
	}
	try {
	    result = Integer.parseInt(idString);
	}
	catch (NumberFormatException nfe) {
	    return defaultId;
	}
	return result;
    }

    private String generateConnections(List<Integer> connections) {
	StringBuffer buf = new StringBuffer();
	buf.append("CONECT  ");
	for (int i = 0; i < connections.size(); ++i) {
	    buf.append("   " + connections.get(i));
	}
	buf.append(NEWLINE);
	return buf.toString();
    }

    private String generateConnection(int n, Object3DSet atoms, LinkSet links) {
	assert n >= 0;
	assert n < atoms.size();
	Atom3D atom = (Atom3D)(atoms.get(n));
	List<Integer> linkIds = new ArrayList<Integer>();
	int thisId = getPdbAtomNumber(atom, (n+1));
	linkIds.add(new Integer(thisId));
	for (int i = 0; i < atoms.size(); ++i) {
	    if (i == n) {
		continue; // do not check bonds with itself
	    }
	    int linkNumber = links.getLinkNumber(atom, atoms.get(i));
	    if (linkNumber > 0) {
		linkIds.add(new Integer(getPdbAtomNumber(atoms.get(i), (i+1))));
	    }
	}
	return generateConnections(linkIds);
    }

    private String generateConnections(Object3DSet atoms, LinkSet links) {
	StringBuffer buf = new StringBuffer();
	if (polygrafMode) {
	    buf.append("FORMAT CONECT (a6,12i6)" + NEWLINE);
	}
	for (int i = 0; i < atoms.size(); ++i) {
	    buf.append(generateConnection(i, atoms, links));
	}
	return buf.toString();
    }

    private String generateLinks(Object3DSet atoms, LinkSet links) {
	StringBuffer buf = new StringBuffer();

	return buf.toString();
    }

    public String toString(Object3DLinkSetBundle bundle) {
	StringBuffer buf = new StringBuffer();
	Object3D obj = bundle.getObject3D();
	Object3DSet atoms = Object3DTools.collectByClassName(obj, "Atom3D");
	String residueName = obj.getName();
	if (residueName.length() > 3) {
	    residueName = residueName.substring(0, 3);
	}
	residueName = residueName.toUpperCase();
	char strandChar = 'H'; // stands for hetero. TODO: make more flexible!
	int residueCount = 1; // TODO: make more flexible
	String description = obj.getProperty("description");
	if ((description == null) || (description.length() == 0)) {
	    description = "No description available.";
	}
	String author = obj.getProperty("author");
	if ((author == null) || (author.length() == 0)) {
	    author = "NanoTiler";
	}
	buf.append(generateHeader(description, author));
	buf.append(generateBody(atoms, residueName, strandChar, residueCount));
	buf.append(generateConnections(atoms, bundle.getLinks()));
	buf.append(generateFooter());
	return buf.toString();
    }

    public String writeString(Object3D node) {
	Object3DLinkSetBundle bundle = new SimpleObject3DLinkSetBundle(node, new SimpleLinkSet());
	return toString(bundle);
    }

    /** Writes link set : FIXIT: not yet implemented. */
    public void write(OutputStream os, LinkSet links) {
	assert false; // FIXIT
    }

    /** Writes subtree : FIXIT: not yet implemented. */
    public void write(OutputStream os, Object3D node) {
	assert false; // FIXIT
    }
}
