package rnadesign.rnamodel;

import rnasecondary.*;

import tools3d.objects3d.LinkSet;

public interface HydrogenBondFinder {

    LinkSet find(Nucleotide3D n1, Nucleotide3D n2) throws RnaModelException;

}