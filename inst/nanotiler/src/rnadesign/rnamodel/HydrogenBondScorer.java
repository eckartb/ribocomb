package rnadesign.rnamodel;

import tools3d.Vector3D;

interface HydrogenBondScorer {

    public static final double NO_HBOND_SCORE = 0.0;

    public static final double PERFECT_SCORE = 1.0;

    double quality(Atom3D hDonor, Atom3D hAcceptor, Vector3D hEstimate);

}