package rnadesign.rnamodel;

import tools3d.objects3d.*;

class HydrogenBondTools {

    /** For a set of donor and acceptors, find hydrogen bonds. Slow all-versus all search, avoid providing large sets of atoms. */
    public static LinkSet findHydrogenBonds(Object3DSet hDonors, Object3DSet hAcceptors, HydrogenBondScorer scorer, HydrogenPositionEstimator estimator, double scoreCutoff) throws RnaModelException {
	LinkSet links = new SimpleLinkSet();
	// System.out.println("Trying to find all hydrogen bonds between " + hDonors.size() + " donors and " + hAcceptors.size() + " acceptors.");
	for (int i  = 0; i < hDonors.size(); ++i) {
	    Object3D donor = hDonors.get(i);
	    int bestAcc = -1;
	    double bestScore = 0.0;
	    if (donor instanceof Atom3D) {
		for (int j  = 0; j < hAcceptors.size(); ++j) {
		    Object3D acceptor = hAcceptors.get(j);
		    if (acceptor instanceof Atom3D) {
			double score = scorer.quality((Atom3D)donor, (Atom3D)acceptor, estimator.estimate((Atom3D)donor)); // no estimated 3D point of hydrogen
			// System.out.println("The H-Bond score between " + donor.getFullName() + " and " + acceptor.getFullName() + " is : " + score);
			if (score >= scoreCutoff && score > bestScore) {
			    bestAcc = j;
			    bestScore = score;
			    // System.out.println("New best h-bond!");
			}
		    }
		}
		if (bestAcc >= 0) {
		    assert(bestAcc < hAcceptors.size());
		    // System.out.println("Adding h-bond:");
		    links.add(new SimpleLink(donor, hAcceptors.get(bestAcc))); // order is important by convention: donor first, acceptor second
		}
	    }
	}
	// System.out.println("Finished trying to find all hydrogen bonds between " + hDonors.size() + " donors and " + hAcceptors.size() + " acceptors. Found " + links.size() + " hydrogen bonds.");
	return links;
    }

}