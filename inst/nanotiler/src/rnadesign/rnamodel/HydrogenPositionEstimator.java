package rnadesign.rnamodel;

import tools3d.Vector3D;

public interface HydrogenPositionEstimator {

    /** For a given donor atom, estimates the position of a hydrogen */
    Vector3D estimate(Atom3D donor) throws RnaModelException;

}