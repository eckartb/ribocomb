package rnadesign.rnamodel;

import java.util.Arrays;
import java.util.Comparator;
import java.util.logging.*;
import static rnadesign.rnamodel.PackageConstants.*;

public class IncomingNomenclature implements StrandJunctionNomenclature {

    public static final char DEFAULT_CHAIN_CHAR = 'A';

    private String prefix = "j";
    private String prefix2 = "k";
    private Logger log = Logger.getLogger("NanoTiler_debug");
    private boolean loopMode = true;

    private char getChainCharacter(NucleotideStrand strand) {
	assert strand != null;
	String s1 = strand.getProperty(PDB_CHAIN_CHAR);
	if ((s1 != null) && (s1.length() > 0)) {
	    return s1.charAt(0);
	}
	assert ((strand.getName() != null) && (strand.getName().length() > 0));
	return strand.getName().charAt(0);
    }

    // private class: comparater used for sorting branch descriptors
    private class BranchDescriptorComparator implements Comparator<BranchDescriptor3D> {

	public int compare(BranchDescriptor3D b1, BranchDescriptor3D b2) {
// 	    String s1 = generateBranchNomenclature(b1);
// 	    String s2 = generateBranchNomenclature(b2);
// 	    return s1.compareTo(s2);
	    
 	    char c1 = getChainCharacter(b1.getIncomingStrand());
 	    char c2 = getChainCharacter(b2.getIncomingStrand());

 	    if (c1 < c2) {
 		return -1;
 	    }
 	    else if (c1 > c2) {
 		return +1;
 	    }
 	    else { // if same strand
 		Residue3D res1 = getBranchDescriptorResidue(b1);
 		Residue3D res2 = getBranchDescriptorResidue(b2);
 		int id1 = res1.getAssignedNumber();
 		int id2 = res2.getAssignedNumber();
 		if (id1 < id2) {
 		    return -1;
 		}
 		if (id1 > id2) {
 		    return +1;
 		}
 	    }
 	    return 0;
	}
    }

    public boolean isLoopMode() { return this.loopMode; }

    private String getResidueName(Residue3D residue) {
	int num = residue.getAssignedNumber();
	char c = residue.getSymbol().getCharacter();
	String result = "" + c;
	assert result.length() == 1;
	result = result + num;
	return result;
    }

    private Residue3D getBranchDescriptorResidue(BranchDescriptor3D b) {
	NucleotideStrand strand = b.getIncomingStrand();
	int index = b.getIncomingIndex()+b.getOffset();
	if (index >= strand.getResidueCount()) {
	    index = strand.getResidueCount() - 1;
	    log.fine("IncomingNomenClature: reduced index because end of strand reached: " 
			+ strand.getName() + " " + b.getIncomingIndex() + " " + b.getOffset());
	}
	assert index < strand.getResidueCount();
	return strand.getResidue3D(index);
    }

    private String generateBranchNomenclature(BranchDescriptor3D b) {
	NucleotideStrand strand = b.getIncomingStrand();
	char c = getChainCharacter(strand);
	if (Character.isWhitespace(c)) {
	    c = DEFAULT_CHAIN_CHAR;	    
	}
	String result = "" + c;
	assert result.length() == 1;
	result = result + "-" + getResidueName(getBranchDescriptorResidue(b));
	return result;
    }

    /** Generates a name, like for example "2HS4H" according to some nomenclature */
    public String generateNomenclature(StrandJunction3D junction) {
	assert junction.getBranchCount() == junction.getStrandCount();
	BranchDescriptor3D[] branches = new BranchDescriptor3D[junction.getBranchCount()];
	String[] branchNames = new String[junction.getBranchCount()];
	for (int i = 0; i < branches.length; ++i) {
	    branches[i] = junction.getBranch(i);
	}
	Arrays.sort(branches, new BranchDescriptorComparator()); // sort such that lowest residue index is first
	for (int i = 0; i < branches.length; ++i) {
	    branchNames[i] = generateBranchNomenclature(branches[i]);
	    // System.out.println("Name of branch " + (i+1) + " : " + branchNames[i]);
	}
	StringBuffer buf = new StringBuffer();
	String usedPrefix=prefix;
	if(junction.isKissingLoop()){
	    usedPrefix=prefix2;
	}
	buf.append(usedPrefix + junction.getBranchCount() + "_");
	buf.append(branchNames[0]);
	for (int i = 1; i < branchNames.length; ++i) {
	    buf.append("_" + branchNames[i]);
	}
	return buf.toString();
    }

    public void setLoopMode(boolean b) { this.loopMode = b; }

    public void setPrefix(String prefix) { this.prefix = prefix; }

}
