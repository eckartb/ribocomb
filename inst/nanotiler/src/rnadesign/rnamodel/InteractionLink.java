package rnadesign.rnamodel;

import tools3d.objects3d.Link;
import rnasecondary.Interaction;

/** This interface describes a link between two Object3D objects with a physical quality described by the Interaction class.
 * A typical example is a Watson-Crick base pair interaction between two Nucleotide3D objects.
 */
public interface InteractionLink extends Link {

    /** returns interaction */
    public Interaction getInteraction();

    public boolean isValid();

    /** sets interaction */
    public void setInteraction(Interaction interaction);

}
