package rnadesign.rnamodel;

import rnasecondary.*;
import sequence.Residue;
import tools3d.objects3d.Link;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.SimpleLink;

/** standard implementation of InteractionLink */
public class InteractionLinkImp extends SimpleLink implements InteractionLink {
    
    private Interaction interaction;

    /** full constructor */
    public InteractionLinkImp(Object3D o1, Object3D o2, Interaction interaction) {
	super(o1, o2);
	this.interaction = interaction;
	assert o1 == interaction.getResidue1();
	assert o2 == interaction.getResidue2();
    }

    /** full constructor */
    public InteractionLinkImp(Interaction interaction) {
	super((Object3D)interaction.getResidue1(), (Object3D)interaction.getResidue2());
	this.interaction = interaction;
    }
    
    public Object clone() {
	InteractionLinkImp l = new InteractionLinkImp(getObj1(), getObj2(), (Interaction)this.interaction.clone());
	l.setName(getName());
	l.setName(new String(getName()));
	l.setTypeName(new String(getTypeName()));
	// l.setLinkProperties((Properties)(getLinkProperties().clone()));
	return l;
    }

    public Object clone(Object3D newObj1, Object3D newObj2) {
	InteractionLinkImp l = (InteractionLinkImp)(clone());
	l.setObj1(newObj1);
	l.setObj2(newObj2);
// 	l.setObj1(getObj1()); // bug!
// 	l.setObj2(getObj2());
	return l;
    }

    /** returns interaction */
    public Interaction getInteraction() { return interaction; }


    public boolean isValid() {
	return (interaction.getResidue1() == getObj1()) && (interaction.getResidue2() == getObj2());
    }

    /** replaces oldobject reference with reference to new object */
    public void replaceObjectInLink(Object3D oldObject, Object3D newObject) {

	assert newObject instanceof Residue;
	if (interaction.getResidue1() == oldObject) {
	    interaction.setResidue1((Residue)newObject);
	    assert getObj1() == oldObject;
	}
	if (interaction.getResidue2() == oldObject) {
	    interaction.setResidue2((Residue)newObject);
	    assert getObj2() == oldObject;
	}
	super.replaceObjectInLink(oldObject, newObject);
	// assert false; // should never be here
    }
    
    /** sets interaction */
    public void setInteraction(Interaction interaction) { this.interaction = interaction; }
    
    public void setObj1(Object3D o) { 
	assert o instanceof Residue;
	super.setObj1(o);
	interaction.setResidue1((Residue)o);
    }

    public void setObj2(Object3D o) { 
	assert o instanceof Residue;
	super.setObj2(o);
	interaction.setResidue2((Residue)o);
    }


}
