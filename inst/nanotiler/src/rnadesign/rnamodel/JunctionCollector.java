/**
 * 
 */
package rnadesign.rnamodel;
import sequence.*;
import tools3d.objects3d.*;
import java.util.*;
import rnasecondary.Stem;

/** Collects all junctions in an Object3D tree
 * @author Calvin Grunewald
 *
 */
public class JunctionCollector implements Object3DAction {
	
    private List<StrandJunction3D> list; //list of collected sequences
	
    public JunctionCollector() {
	list = new ArrayList<StrandJunction3D>(); //list of collected sequences is cleared
    }
	
    public void act(Object3D obj) {
	if (obj instanceof StrandJunction3D) {
	    addJunction((StrandJunction3D) obj);
	}
    }

    /** add a stem to the collector */
    public void addJunction(StrandJunction3D j) {
	list.add(j);
    }
	
    public void clear() {
	list.clear();
    }
    
    /** gets n'th collected stem */
	public StrandJunction3D getJunction(int n) {
	    if (n < getJunctionCount()) {
		return (StrandJunction3D)(list.get(n));
		}
		return null;
	}
	
	/** returns number of collected stems so far */
	public int getJunctionCount() {
	    return list.size();
	}

}
