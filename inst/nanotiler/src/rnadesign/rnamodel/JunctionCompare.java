package rnadesign.rnamodel;

import java.io.*;
import java.util.zip.DataFormatException;
import java.util.*;
import generaltools.IOTools;
import generaltools.StringTools;
import tools3d.*;
import tools3d.objects3d.*;

/** Main method that is used to score similarity between junctions (scanner output) */
class JunctionCompare {

    public static final double NOMATCH_SCORE = 1000.0; // score corresponding to non-matching junctions
    public static final int TEST_INTERVAL = 1000000;
    public static final int JUNCTION_ORDER_MAX = 4;
    public static double translationRandStep = 10.0; // used for possibly randomizing the position of a helix position

    public static void main(String[] args) {
	if (args.length < 2) {
	    System.out.println("All-versus-all comparison of a set of junctions (via their JunctionScanner output). Usage: java rnadesign.rnamodel.JunctionCompare n|r filenamefile|[file1 file2 file3 ...] ");
	    System.exit(0);
	}
	int numFiles = 0;
        List<String> fileNameList = new ArrayList<String>();
        List<String> fileNameList2 = new ArrayList<String>();
	String mode = args[0];
	if (args.length == 2) {
            try {
		FileInputStream is = new FileInputStream(args[1]);
		String[] allLines = StringTools.readAllLines(is);
		for (int i = 0; i < allLines.length; ++i) {
		    String s = allLines[i].trim();
		    if (s.length() > 0) {
			fileNameList.add(s);
			fileNameList2.add(s);
		    }
		}
	    }
	    catch (IOException ioe) {
		System.err.println("Error reading file with filenames: " + ioe.getMessage());
		System.exit(1);
	    }
	} else  if (args.length == 3) {
            try {
		FileInputStream is = new FileInputStream(args[1]);
		String[] allLines = StringTools.readAllLines(is);
		for (int i = 0; i < allLines.length; ++i) {
		    String s = allLines[i].trim();
		    if (s.length() > 0) {
			fileNameList.add(s);
		    }
		}
		is.close();
		FileInputStream is2 = new FileInputStream(args[2]);
		allLines = StringTools.readAllLines(is2);
		for (int i = 0; i < allLines.length; ++i) {
		    String s = allLines[i].trim();
		    if (s.length() > 0) {
			fileNameList2.add(s);
		    }
		}
		is2.close();
	    }
	    catch (IOException ioe) {
		System.err.println("Error reading file with filenames: " + ioe.getMessage());
		System.exit(1);
	    }
	} else {
	    for (int i = 1; i < args.length; ++i) {
		fileNameList.add(args[i]);
	    }
	}
	double[][] scoreMatrix = new double[fileNameList.size()][fileNameList2.size()];
	for (int i = 0; i < scoreMatrix.length; ++i) {
	    for (int j = 0; j < scoreMatrix[i].length; ++j) {
		scoreMatrix[i][j] = NOMATCH_SCORE;
	    }
	}
	JunctionHashGenerator generator = new JunctionHashGenerator();
	HelixParameters helixParameters = new HelixParameters();
	CoordinateSystem3D randCoords = new CoordinateSystem3D(new Vector3D(0.0, 0.0, 0.0));
	if (mode.equals("r")) {
	    System.out.println("Randomizing helix positions (orientation and translation)...");
	    Object3DTools.randomizeOrientation(randCoords);
	    Object3DTools.randomizeTranslation(randCoords, translationRandStep);
	    System.out.println("" + randCoords);
	}
	for (int i = 0; i < fileNameList.size(); ++i) {
	    String filen1 = fileNameList.get(i);
	    System.out.println("# Working on file 1: " + filen1);
	    try {
		FileInputStream fis1 = new FileInputStream(filen1);
		JunctionScanOutputParser parser = new JunctionScanOutputParser(fis1);
		parser.parse();
                fis1.close();
		List<CoordinateSystem3D> orientations1Orig = parser.getOrientations();
		if (mode.equals("r")) {
		    for (int ii = 0; ii < orientations1Orig.size(); ++ii) {
			orientations1Orig.get(ii).activeTransform(randCoords);
		    }
		}
		List<CoordinateSystem3D> orientations1 = generator.generateNormalizedCoordinates(orientations1Orig, helixParameters);
		// a normalization step is missing here!
		List<Matrix4D> trafos1 = generator.generateNormalizedTransformations(orientations1);
		// run self-test:
		if ((i > 0) && ((i % TEST_INTERVAL) == 0)) {
		    // self-test: we are using the same junction two times; the second copy is rotated and translated
		    // by a randomly generated matrix
		    System.out.println("# Starting self-test using file " + filen1);
		    Matrix4D randMatrix = Matrix4DTools.generateRandomHomogeneousMatrix(1.0, 1.0);
		    CoordinateSystem3D csRand = new CoordinateSystem3D(randMatrix);
		    List<CoordinateSystem3D> orientations1OrigClone = new ArrayList<CoordinateSystem3D>();
		    for (int ii = 0; ii < orientations1Orig.size(); ++ii) {
			CoordinateSystem3D newCs = (CoordinateSystem3D)(orientations1Orig.get(ii).cloneDeep());
			newCs.activeTransform3(csRand);
			// assert(newCs.getPosition().distance(orientations1Orig.get(ii).getPosition()) < 0.001);
			// a second mean transformation: add a base pair! Only for first helix!
			CoordinateSystem3D newCs2 = SimpleBranchDescriptor3D.generatePropagatedCoordinateSystem(newCs, helixParameters, 1);
			if (ii == 0) {
			    orientations1OrigClone.add(newCs2); // alternative: add newCs
			} else {
			    orientations1OrigClone.add(newCs); // alternative: add newCs
			}
		    }
		    assert(orientations1OrigClone.size() == orientations1Orig.size());
		    for (int ii =0; ii < orientations1Orig.size(); ++ii) {
			System.out.println("# Originally parsed orientations (1): " + ii + "\n");
			System.out.println(orientations1Orig.get(ii) + "\n" + orientations1OrigClone.get(ii));
		    } 
		    Vector3D p1 = new Vector3D(orientations1OrigClone.get(0).getPosition());
		    List<CoordinateSystem3D> orientations1Clone = generator.generateNormalizedCoordinates(orientations1OrigClone, helixParameters);
		    Vector3D p2 = new Vector3D(orientations1OrigClone.get(0).getPosition());
		    assert (p1.distance(p2) < 0.001); // no movement
		    // a normalization step is missing here!
		    List<Matrix4D> trafos1Clone = generator.generateNormalizedTransformations(orientations1Clone);
		    assert (trafos1.size() == trafos1Clone.size());
		    double scoreSelf = generator.scoreNormalizedTransformations(trafos1, trafos1);
		    double score = generator.scoreNormalizedTransformations(trafos1, trafos1Clone);
		    System.out.println("# result of self test " + (i+1) + " " + IOTools.extractRelativeName(fileNameList.get(i)) + " : " + score + " " 
				       + scoreSelf + "\n");
		    if (Math.abs(score) >= 0.0001) {
			for (int ii =0; ii < trafos1.size(); ++ii) {
			    System.out.println("# Originally parsed orientations: " + ii + "\n");
			    System.out.println(orientations1Orig.get(ii) + "\n" + orientations1OrigClone.get(ii));
			    System.out.println("# Normalized orientations: " + ii + "\n");
			    System.out.println(orientations1.get(ii) + "\n" + orientations1Clone.get(ii));
			    System.out.println("# Transformation " + ii + "\n");
			    System.out.println(trafos1.get(ii) + "\n" + trafos1Clone.get(ii));
			} 
		    }
		    assert(Math.abs(score) < 0.0001);
		}
		for (int j = 0; j < fileNameList2.size(); ++j) {
		    String filen2 = fileNameList2.get(j);
		    // System.out.println("Working on file 2: " + filen2);
		    try {
			FileInputStream fis2 = new FileInputStream(filen2);
			JunctionScanOutputParser parser2 = new JunctionScanOutputParser(fis2);
			parser2.parse();
			fis2.close();
			List<CoordinateSystem3D> orientations2Orig = parser2.getOrientations();
			if (mode.equals("r")) {
			    for (int ii = 0; ii < orientations2Orig.size(); ++ii) {
				orientations2Orig.get(ii).activeTransform(randCoords);
			    }
			}
			List<CoordinateSystem3D> orientations2 = generator.generateNormalizedCoordinates(orientations2Orig, helixParameters);
			List<Matrix4D> trafos2 = generator.generateNormalizedTransformations(orientations2);
			if ((trafos1.size() == trafos2.size()) && (trafos1.size() <= JUNCTION_ORDER_MAX)) {
			    assert (trafos1.size() == trafos2.size());
			    double score = generator.scoreNormalizedTransformations(trafos1, trafos2);
			    System.out.println("# Comparing " + (i+1) + " " + IOTools.extractRelativeName(filen1) + " " 
					       + (j+1) + " " + IOTools.extractRelativeName(filen2) + " : " + score);
			    scoreMatrix[i][j] = score;
			} else {
			    scoreMatrix[i][j] = NOMATCH_SCORE;
			}
			// scoreMatrix[j][i] = scoreMatrix[i][j]; // not necessarily symmetric
		    } catch (IOException ioe) {
			System.out.println("# Warning: IO exception while processing file1: " 
					   + filen1 + " file2: " + filen2);
			System.err.println(ioe.getMessage());
		    } catch (DataFormatException dfe) {
			System.out.println("# Warning: data format exception while processing file1: " 
					   + filen1 + " file2: " + filen2);
			System.err.println(dfe.getMessage());
		    } catch (java.awt.geom.NoninvertibleTransformException nite) {
			System.out.println("# Warning: could not generate normalized coordinates. Current file1: " 
					   + filen1 + " file2: " + filen2);
		    }
		}
	    } catch (IOException ioe) {
		System.err.println(ioe.getMessage());
	    } catch (DataFormatException dfe) {
		System.err.println(dfe.getMessage());
	    } catch (java.awt.geom.NoninvertibleTransformException nite) {
		System.out.println("# Warning: could not generate normalized coordinates. Current file1: " + filen1);
	    }
	    
	}
	// output of matrix to standard output
	System.out.println("# Result matrix:");
	for (int i = 0; i < scoreMatrix.length; ++i) {
	    for (int j = 0; j < scoreMatrix[i].length; ++j) {
		System.out.print(scoreMatrix[i][j]);
		if ((j+1) < scoreMatrix[i].length) {
		    System.out.print(" ");
		}
	    }
	    System.out.println();
	}
    }

}
