/** implements concept of two linked objects */
package rnadesign.rnamodel;

import tools3d.objects3d.*;
import tools3d.*;
import java.util.*;
import java.util.logging.*;
import generaltools.ConstraintDouble;
import generaltools.SimpleConstraintDouble;
import graphtools.PermutationGenerator;

/** This class takes a set of helix descriptors or 5'ends and 3' ends as an input */
public class JunctionDBConstraintLink extends SimpleMultiLink { // implements MultiConstraintLink {

    private List<Integer> symIds = new ArrayList<Integer>();

    private static Logger log = Logger.getLogger(PackageConstants.LOGGER_NAME);

    List<BranchDescriptor3D> branches;
    
    StrandJunctionDB junctionDB;

    boolean scoreAllMode = true;

    public static final double NO_MATCH_SCORE = 100.0;

    public JunctionDBConstraintLink(List<BranchDescriptor3D> _branches, StrandJunctionDB _junctionDB)  {
	assert (_branches != null);
	this.junctionDB = _junctionDB;
	for (int i = 0; i < _branches.size(); ++i) {
	    addObj(_branches.get(i));
	    symIds.add(0); // each helix descriptor is original "non-mirror" version; otherwise use integer numbers greater zero.
	}
	log.info("Added junctionDBConstraint link of size " + size());
	assert (this.junctionDB != null);
    }

    /** Overwrites addObj method from SimpleMultiLink. Only difference: added assert */
    public void addObj(Object3D obj) {
	assert(obj instanceof BranchDescriptor3D);
	super.addObj(obj);
    }

    /** Returns zero, if database containts perfectly matching junction and score greater zero otherwise */
    public double computeScore() {
	List<BranchDescriptor3D> branches = getBranches();
	double bestScore = NO_MATCH_SCORE;
	if (scoreAllMode) {
	    List<Double> sims = junctionDB.scoreAllJunctions(branches);
	    Iterator<Double> it = sims.iterator();
	    //	    if (!it.hasNext()) {
	    // log.info("No junctions to score found!");
	    // }
	    while (it.hasNext()) {
		double score = it.next();
		// log.info("Found junction db score: " + score);
		if (score < bestScore) {
		    bestScore = score;
		}
	    }
	} else { // use hash 
	    assert false;
	    Map<Integer, Double> sims = junctionDB.findAndScoreSimilarJunctions(branches);
	    // double[] scores = junctionDB.scoreSimilarJunctions(branches, sims);
	    bestScore = NO_MATCH_SCORE;
	    Set<Integer> keys = sims.keySet();
	    Iterator<Integer> it = keys.iterator();
	    while (it.hasNext()) {
		double score = sims.get(it.next());
		if (score < bestScore) {
		    bestScore = score;
		}
	    }
	}
	return bestScore;
    }

    /** Returns zero, if database containts perfectly matching junction and score greater zero otherwise */
    public Properties computeScore(List<CoordinateSystem> activeTransformations) {
	List<BranchDescriptor3D> branches = getBranches();
	double bestScore = NO_MATCH_SCORE;
	int bestId = 0;
	if (scoreAllMode) {
	    List<Double> sims = junctionDB.scoreAllJunctions(branches, activeTransformations);
	    Iterator<Double> it = sims.iterator();
	    if (!it.hasNext()) {
		log.info("No junctions to score found!");
	    }
	    int count = 0;
	    while (it.hasNext()) {
		double score = it.next();
		// log.info("Found junction db score: " + score);
		if (score < bestScore) {
		    bestScore = score;
		    bestId = count;
		}
		++count;
	    }
	} else { // juse hash 
	    assert false;
	    Map<Integer, Double> sims = junctionDB.findAndScoreSimilarJunctions(branches, activeTransformations);
	    // double[] scores = junctionDB.scoreSimilarJunctions(branches, sims);
	    bestScore = NO_MATCH_SCORE;
	    Set<Integer> keys = sims.keySet();
	    Iterator<Integer> it = keys.iterator();
	    while (it.hasNext()) {
		int id = it.next();
		double score = sims.get(id);
		if (score < bestScore) {
		    bestScore = score;
		    bestId = id;
		}
	    }
	}
	Properties properties = new Properties();
	properties.setProperty("score", "" + bestScore);
	String junctionIdString = "" + branches.size() + " " + bestId;
	for (int i = 0; i < branches.size(); ++i) {
	    junctionIdString = junctionIdString + " " + branches.get(i).getFullName();
	}
	properties.setProperty(junctionDB.getName(), junctionIdString); // store order and id of junction
	return properties;
    }

    /** Returns n'th branch descriptor */
    public BranchDescriptor3D getBranch(int n) {
	return (BranchDescriptor3D)(getObj(n));
    }

    public int getBranchCount() { return size(); }

    public List<BranchDescriptor3D> getBranches() {
	List<BranchDescriptor3D> branches = new ArrayList<BranchDescriptor3D>();
	for (int i = 0; i < size(); ++i) {
	    branches.add(getBranch(i));
	}
	return branches;
    }

    /** Scores structure of junction. The provided coordinate systems correspond to how each "branch" of a junction
     * has to be transformed. Careful with symmetry: this data structure only stores the symmetry copy ids of 
     * individual branches. It does not store the actual symmetry transformation. It is up to the calling class
     * to compute a set of coordinate systems that matches the provided symmetry constraints. */
    /* public double getDiff(List<CoordinateSystem> csList) {
	permGen.reset();
	double bestDiff = 1e30;
	double newDiff = 0;
	// System.out.println("Used coordinate systems: ");
	// for (CoordinateSystem cs : csList) {
	// System.out.print(cs + " ; " );
	// }
	double bridgeTerm = 0.0;
	if (bridgableConstraint != null) {
	    try {
		bridgeTerm = getBridgable3(csList);
	    } catch (RnaModelException rne) {
		throw new RuntimeException(rne.getMessage()); // should never happen, missing atoms
	    }
	}
	do {
	    newDiff = bridgeTerm + getDiff(permGen.get(), csList);
	    if (newDiff < bestDiff) {
		bestDiff = newDiff;
	    }
	} while (permGen.inc());
	return bestDiff;
    }
    */

    /*
    private double getDiff(int[] perm) {
	double result = 0.0;
	double dist;
	for (int i = 0; i < perm.length; ++i) {
	    int id1 = perm[perm.length-1];
	    if (i > 0) {
		id1 = perm[i-1];
	    }
	    int id2 = perm[i];
	    assert id1 != id2;
	    dist = fivePrimePositions[id1].distance(threePrimePositions[id2]);
	    result += constraint.getDiff(dist);
	}
	return result;
    }

    private double getDiff(int[] perm, List<CoordinateSystem> csList) {
	assert perm.length == csList.size();
	double result = 0.0;
	double dist;
	for (int i = 0; i < perm.length; ++i) {
	    int id1 = perm[perm.length-1];
	    if (i > 0) {
		id1 = perm[i-1];
	    }
	    int id2 = perm[i];
	    assert id1 != id2;
	    assert id1 < csList.size();
	    CoordinateSystem cs1 = csList.get(id1);
	    CoordinateSystem cs2 = csList.get(id2);
	    // dist = cs1.activeTransform(getThreePrimeObject(id1).getChild(ATOM_3_NAME).getPosition()).distance(cs2.activeTransform(getFivePrimeObject(perm[id2]).getChild(ATOM_5_NAME).getPosition()));
	    dist = cs1.activeTransform(fivePrimePositions[id1]).distance(cs2.activeTransform(threePrimePositions[id2]));
	    result += constraint.getDiff(dist);
	}
	return result;
    }
    */

    public int getSymId(int index) { return symIds.get(index); }

    public void setSymId(int index, int _symId1) { 
	while (symIds.size() <= index) {
	    symIds.add(-1);
	}
	symIds.set(index, _symId1);
    }

    public void setSymId1(int _symId1) { setSymId(0, _symId1); }

    public void setSymId2(int _symId2) { setSymId(1, _symId2); }

    public String toPrettyString() {
	return toString();
    }

    public String toString() {
	String result = "(JunctionDBConstraintLink " + getName();
	for (int i = 0; i < size(); ++i) {
	    result = result + " " + getObj(i).getFullName();
	}
	for (int i = 0; i < symIds.size(); ++i) {
	    result = result + " " + symIds.get(i);
	}
	result = result +  " )"; // " + getConstraint().toString() + " )";
	return result;
    }

    public boolean validate() {
	return (getBranchCount() > 0) && (symIds.size() == getBranchCount());
    }

}
	
