package rnadesign.rnamodel;

import tools3d.objects3d.*;

/** describes connectivity of junctions in 3D. Use addLinks method to add helix constrains ! */
public class JunctionGraph3D extends SimpleObject3D {

    private GrowConnectivity connectivity;

    public JunctionGraph3D() {

    }

    public JunctionGraph3D(GrowConnectivity connectivity) {
	this.connectivity = connectivity;
    }

    public void insertChild(Object3D child) {
	assert child.size() == 0; // no composite objects
	assert child instanceof GraphVertexDescriptor3D;
	super.insertChild(child);
    }

    /** Make child children of node.
     * Relative position of child changes according to position of parent !*/
    public void insertChild(Object3D child, int position) {
	assert child.size() == 0; // no composite objects
	assert child instanceof GraphVertexDescriptor3D;
	super.insertChild(child, position);
    }

}
