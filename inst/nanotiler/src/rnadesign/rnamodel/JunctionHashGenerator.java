package rnadesign.rnamodel;

import java.util.*;
import graphtools.PermutationGenerator;
import java.util.logging.Logger;

import graphtools.*;
import sequence.SequenceStatus;
import tools3d.*;
import tools3d.numerics3d.*;
import tools3d.objects3d.*;

public class JunctionHashGenerator {

    int offsetMax = 21;
    double epsilonRot = 0.5; // 0.2;
    double epsilonTrans = 2.0; // 0.2;

    public JunctionHashGenerator() {
	
    }

    /** Store coordinate systems that are as close as possible to virtual center point */
    public List<CoordinateSystem3D> generateNormalizedCoordinates(List<BranchDescriptor3D> branches) throws java.awt.geom.NoninvertibleTransformException {
	assert(branches.size() > 1);
	// first compute "center of gravity*; for some reason Object3DSetTools.centerOfMass did not work
	Vector3D center = StrandJunctionTools.computeJunctionCenter(branches);
	
	// for each helix, compute point that is on its direction line that is closest to center of mass:
	List<CoordinateSystem3D> csList = new ArrayList<CoordinateSystem3D>();
	for (int i = 0; i < branches.size(); ++i) {
	    double d = Geom3DTools.computeDistanceLinePoint(center, branches.get(i).getPosition(), branches.get(i).getCoordinateSystem().getZ());
	    int offsetBest2 = (int)(Math.round(d/branches.get(i).getHelixParameters().getRise())); 
// 	    double distBest = center.distance(branches.get(i).generatePropagatedCoordinateSystem(0).getPosition());
// 	    int offsetBest = 0;
// 	    for (int j = -offsetMax; j <= offsetMax; ++j) { // TODO: can be made more efficient than complete enumeration
// 		double dist = center.distance(branches.get(i).generatePropagatedCoordinateSystem(j).getPosition());
// 		if (dist < distBest) {
// 		    distBest = dist;
// 		    offsetBest = j;
// 		}
// 	    }
// 	    if (Math.abs(offsetBest) == offsetMax) {
// 		throw new java.awt.geom.NoninvertibleTransformException("Unable to normalize junction helix orientations for helix " + i);
// 	    }
	    csList.add((CoordinateSystem3D)(branches.get(i).generatePropagatedCoordinateSystem(offsetBest2))); // store best found coordinate system
	}
	// now compute projection of that point to each z-axis:
	return csList;
    }

    /** Store coordinate systems that are as close as possible to virtual center point */
    public List<CoordinateSystem3D> generateNormalizedCoordinates(List<CoordinateSystem3D> branches, HelixParameters helixParameters) throws java.awt.geom.NoninvertibleTransformException {
	assert(branches.size() > 1);
	Vector3D p1 = new Vector3D(branches.get(0).getPosition());
	// first compute "center of gravity*; for some reason Object3DSetTools.centerOfMass did not work
	Vector3D center = StrandJunctionTools.computeCS3DJunctionCenter(branches);
	Vector3D p2 = new Vector3D(branches.get(0).getPosition());
	assert (p1.distance(p2) < 0.001);
	// for each helix, compute point that is on its direction line that is closest to center of mass:
	List<CoordinateSystem3D> csList = new ArrayList<CoordinateSystem3D>();
	for (int i = 0; i < branches.size(); ++i) {
            double d = Geom3DTools.computeDistanceLinePoint(center, branches.get(i).getPosition(), branches.get(i).getZ());
	    int offsetBest2 = (int)(Math.round(d/helixParameters.getRise())); 
// 	    double distBest = center.distance(SimpleBranchDescriptor3D.generatePropagatedCoordinateSystem(branches.get(i), helixParameters, 0).getPosition());
// 	    int offsetBest = 0;
// 	    p2 = new Vector3D(branches.get(0).getPosition());
// 	    assert (p1.distance(p2) < 0.001);
// 	    for (int j = -offsetMax; j <= offsetMax; ++j) { // TODO: can be made more efficient than complete enumeration
// 		double dist = center.distance(SimpleBranchDescriptor3D.generatePropagatedCoordinateSystem(branches.get(i), helixParameters, j).getPosition());
// 		// center.distance(branches.get(i).generatePropagatedCoordinateSystem(j).getPosition());
// 		if (dist < distBest) {
// 		    distBest = dist;
// 		    offsetBest = j;
// 		}
// 		p2 = new Vector3D(branches.get(0).getPosition());
// 		assert (p1.distance(p2) < 0.001);
// 	    }
// 	    if (Math.abs(offsetBest) == offsetMax) {
// 		throw new java.awt.geom.NoninvertibleTransformException("Unable to normalize junction helix orientations for helix " + i);
// 	    }
// 	    System.out.println("Estimate best offset " + offsetBest + " new method: " + offsetBest2);
	    csList.add(SimpleBranchDescriptor3D.generatePropagatedCoordinateSystem(branches.get(i), helixParameters, offsetBest2)); // store best found coordinate system
	}
	p2 = new Vector3D(branches.get(0).getPosition());
	assert (p1.distance(p2) < 0.001);
	// now compute projection of that point to each z-axis:
	return csList;
    }

    /** Store coordinate systems that are as close as possible to virtual center point. Branch descriptors a being transformed by 
     * active coordinate transformations defined in activeTransformations. */
    public List<CoordinateSystem3D> generateNormalizedCoordinates(List<BranchDescriptor3D> branches, List<CoordinateSystem> activeTransformations) throws java.awt.geom.NoninvertibleTransformException {
	assert(branches.size() > 1);
	assert(branches.size() == activeTransformations.size());
	// first compute "center of gravity*; for some reason Object3DSetTools.centerOfMass did not work
	Vector3D center = StrandJunctionTools.computeJunctionCenter(branches, activeTransformations);
	// for each helix, compute point that is on its direction line that is closest to center of mass:
	List<CoordinateSystem3D> csList = new ArrayList<CoordinateSystem3D>();
	for (int i = 0; i < branches.size(); ++i) {
	    CoordinateSystem cs = activeTransformations.get(i);
	    double d = Geom3DTools.computeDistanceLinePoint(center, cs.activeTransform(branches.get(i).getPosition()),
							    cs.activeTransform(branches.get(i).getCoordinateSystem().getZ()));
	    int offsetBest2 = (int)(Math.round(d/branches.get(i).getHelixParameters().getRise())); 
	    // 	    double distBest = center.distance(branches.get(i).generatePropagatedCoordinateSystem(0).getPosition());
	    // 	    int offsetBest = 0;
	    
	    // 	    for (int j = -offsetMax; j <= offsetMax; ++j) { // TODO: can be made more efficient than complete enumeration
	    // 		double dist = center.distance(branches.get(i).generatePropagatedCoordinateSystem(j).getPosition());
	    // 		if (dist < distBest) {
	    // 		    distBest = dist;
	    // 		    offsetBest = j;
	    // 		}
	    // 	    }
	    // 	    if (Math.abs(offsetBest) == offsetMax) {
	    // 		throw new java.awt.geom.NoninvertibleTransformException("Unable to normalize junction helix orientations for helix " + i);
	    // 	    }
	    CoordinateSystem3D csNew = (CoordinateSystem3D)(branches.get(i).generatePropagatedCoordinateSystem(offsetBest2)); // store best found coordinate system
	    csNew.activeTransform(activeTransformations.get(i));
	    csList.add(csNew);
	}
	// now compute projection of that point to each z-axis:
	return csList;
    }

    /** returns transformation needed to go from branch descriptor1 to 2. Both branch descriptors should point towards each other! */
    /* public static Matrix4D computeBranchDescriptorTransformation(BranchDescriptor3D b1, BranchDescriptor3D b2) {
        // b1*query=b2 -> query = b1-1 * b2                                                                                                   
        Matrix4D m1 = b1.getCoordinateSystem().generateMatrix4D();
        Matrix4D m2 = b2.getCoordinateSystem().generateMatrix4D();
        return m1.inverse().multiply(m2);
    }
    */

    /** Sum of non-diagonal part of rotation matrix elements */
    private double getMatrix4DSignatureSum(Matrix4D m) {
	double sum = m.getXY() + m.getXZ() + m.getYZ();
	return sum;
    }

    /** Returns list of 4D transformation matrices that are required to convert between different supplied local coordinate systems */
    public List<Matrix4D> generateNormalizedTransformations(List<CoordinateSystem3D> csList) {
	List<Matrix4D> homogs = new ArrayList<Matrix4D>();
	for (int i = 0; i < csList.size(); ++i) {
	    homogs.add(csList.get(i).generateMatrix4D());
	}
        assert(homogs.size() == csList.size());
	List<Matrix4D> result = new ArrayList<Matrix4D>();
	for (int i = 0; i < csList.size(); ++i) {
	    for (int j = (i+1); j < csList.size(); ++j) {
		Matrix4D m1 = homogs.get(i).inverse().multiply(homogs.get(j));
		Matrix4D m2 = homogs.get(j).inverse().multiply(homogs.get(i));
		// which one to choose? // choose sum of rotation matrix elements as criterion
		double s1 = getMatrix4DSignatureSum(m1);
		double s2 = getMatrix4DSignatureSum(m2);
		if (s1 < s2) {
		    result.add(m1);
		} else {
		    result.add(m2);
		}
	    }
	}
	Collections.sort(result);
	return result;
    }

    /** Returns list of 4D transformation matrices that are required to convert between different supplied non-normalized helix descriptors. */
    public List<Matrix4D> generateCompleteNormalizedTransformations(StrandJunction3D junction) throws java.awt.geom.NoninvertibleTransformException {
	List<BranchDescriptor3D> branches = new ArrayList<BranchDescriptor3D>();
	for (int i = 0; i < junction.getBranchCount(); ++i) {
	    branches.add(junction.getBranch(i));
	}
	List<CoordinateSystem3D> csList = generateNormalizedCoordinates(branches);
	List<Matrix4D> result = generateNormalizedTransformations(csList);
	return result;
    }

    /** Returns list of 4D transformation matrices that are required to convert between different supplied non-normalized helix descriptors. */
    public List<Matrix4D> generateCompleteNormalizedTransformations(List<BranchDescriptor3D> branches) throws java.awt.geom.NoninvertibleTransformException {
	List<CoordinateSystem3D> csList = generateNormalizedCoordinates(branches);
	List<Matrix4D> result = generateNormalizedTransformations(csList);
	return result;
    }

    /** Returns list of 4D transformation matrices that are required to convert between different supplied non-normalized helix descriptors. */
    public List<Matrix4D> generateCompleteNormalizedTransformations(List<BranchDescriptor3D> branches, List<CoordinateSystem> activeTransforms) throws java.awt.geom.NoninvertibleTransformException {
	List<CoordinateSystem3D> csList = generateNormalizedCoordinates(branches, activeTransforms);
	List<Matrix4D> result = generateNormalizedTransformations(csList);
	return result;
    }

    /** Returns list of TWO hash codes for each transformation matrix (trafo matrix and its inverse) */
    public List<String> generateCompleteHashes(List<BranchDescriptor3D> branches) throws java.awt.geom.NoninvertibleTransformException {
        assert (branches.size() > 0);
	List<Matrix4D> mList = generateCompleteNormalizedTransformations(branches);
	List<String> result = new ArrayList<String>();
	for (int i = 0; i < mList.size(); ++i) {
	    result.add(Matrix4DTools.computeEpsilonHash(mList.get(i), epsilonRot, epsilonTrans));
	    result.add(Matrix4DTools.computeEpsilonHash(mList.get(i).inverse(), epsilonRot, epsilonTrans));
	}
	return result;
    }

    /** Returns list of TWO hash codes for each transformation matrix (trafo matrix and its inverse) */
    public List<String> generateCompleteHashes(List<BranchDescriptor3D> branches, List<CoordinateSystem> activeTransforms) throws java.awt.geom.NoninvertibleTransformException {
        assert (branches.size() > 0);
	List<Matrix4D> mList = generateCompleteNormalizedTransformations(branches, activeTransforms);
	List<String> result = new ArrayList<String>();
	for (int i = 0; i < mList.size(); ++i) {
	    result.add(Matrix4DTools.computeEpsilonHash(mList.get(i), epsilonRot, epsilonTrans));
	    result.add(Matrix4DTools.computeEpsilonHash(mList.get(i).inverse(), epsilonRot, epsilonTrans));
	}
	return result;
    }

    /** Returns similarity score of two transformation matrices. Also considers "backwards" transformation. */
    private double scoreNormalizedTransformations(Matrix4D trafo1, Matrix4D trafo2) {
	double norm1 = trafo2.minus(trafo1).norm();
	double norm2 = trafo2.minus(trafo1.inverse()).norm();
	return (norm1 < norm2) ? norm1 : norm2;
    }

    /** Scores two lists of 4D transformation matrices that are required to convert between different supplied local coordinate systems.
     *  A permutation is given that tell which transformation from the first list to compare with which transformation 
     *  from the second list. */
    private double scoreNormalizedTransformations(List<Matrix4D> trafos1, List<Matrix4D> trafos2, int[] perm) {
	assert(trafos1.size() == trafos2.size());
	assert(perm.length == trafos1.size());
	double score = 0.0;
	for (int i = 0; i < trafos1.size(); ++i) {
            assert(i < trafos1.size());
            assert(perm[i] < trafos1.size());
	    score += scoreNormalizedTransformations(trafos1.get(i), trafos2.get(perm[i]));
	}
	return score;
    }

    /** Returns score corresponding to lowest-scoring permutation between two lists of transformation matrices */
    public double scoreNormalizedTransformations(List<Matrix4D> trafos1, List<Matrix4D> trafos2) {
	assert(trafos1.size() == trafos2.size());
	PermutationGenerator permutator = new PermutationGenerator(trafos1.size());
	double bestScore = 1e30;
	do {
	    int[] perm = permutator.get();
	    double score = scoreNormalizedTransformations(trafos1, trafos2, perm);
	    if (score < bestScore) {
		bestScore = score;
	    }
	} while (permutator.inc());
	return bestScore;
    }

    /** Returns score corresponding to lowest-scoring permutation between two lists of transformation matrices */
    /* public double scoreNormalizedTransformations(List<CoordinateSystem3D> trafos1, List<CoordinateSystem3D> trafos2) {
	assert(trafos1.size() == trafos2.size());
	List<Matrix4D> matrices1 = new ArrayList<Matrix4D>();
	List<Matrix4D> matrices2 = new ArrayList<Matrix4D>();
	for (int i = 0; i < trafos1.size(); ++i) {
	    matrices1.add(trafos1.get(i).generateMatrix4D());
	    matrices2.add(trafos2.get(i).generateMatrix4D());
	}
	return scoreNormalizedTransformations(matrices1, matrices2);
    }    
    */

}
