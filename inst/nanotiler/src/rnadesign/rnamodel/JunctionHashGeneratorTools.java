package rnadesign.rnamodel;

import java.io.*;
import java.util.*;
import graphtools.PermutationGenerator;
import java.util.logging.Logger;
import java.util.zip.DataFormatException;
import graphtools.*;
import sequence.SequenceStatus;
import tools3d.*;
import tools3d.objects3d.*;

/** Static helper methods that are used to score similarity between junctions (scanner output) */
class JunctionHashGeneratorTools {

    public static double scoreJunctionSimilarity(List<CoordinateSystem3D> orientations1, 
					  List<CoordinateSystem3D> orientations2) throws IOException, DataFormatException {
	JunctionHashGenerator generator = new JunctionHashGenerator();
	List<Matrix4D> trafos1 = generator.generateNormalizedTransformations(orientations1);
	List<Matrix4D> trafos2 = generator.generateNormalizedTransformations(orientations2);
	double score = generator.scoreNormalizedTransformations(trafos1, trafos2);
	return score;
    }

    public static double scoreJunctionSimilarity(InputStream fis, InputStream fis2) throws IOException, DataFormatException {
	JunctionScanOutputParser parser = new JunctionScanOutputParser(fis);
	parser.parse();
	List<CoordinateSystem3D> orientations1 = parser.getOrientations();
	JunctionScanOutputParser parser2 = new JunctionScanOutputParser(fis2);
	parser2.parse();
	List<CoordinateSystem3D> orientations2 = parser2.getOrientations();
	return scoreJunctionSimilarity(orientations1, orientations2);
    }

}
