/** implements concept of two linked objects */
package rnadesign.rnamodel;

import tools3d.objects3d.*;
import tools3d.*;
import java.util.*;
import java.util.logging.*;
import generaltools.ConstraintDouble;
import generaltools.SimpleConstraintDouble;
import graphtools.PermutationGenerator;

/** This class takes a set of helix descriptors or 5'ends and 3' ends as an input */
public class JunctionMultiConstraintLink extends SimpleMultiLink implements MultiConstraintLink {

    private ConstraintDouble constraint;

    private ConstraintDouble bridgableConstraint;

    private List<Integer> symIds = new ArrayList<Integer>();

    private List<Integer> symIds2 = new ArrayList<Integer>(); // alternative implementation based on strand ends and not branches

    private PermutationGenerator permGen;

    private static Logger log = Logger.getLogger(PackageConstants.LOGGER_NAME);

    private static final String ATOM_3_NAME = "P";

    private static final String ATOM_3_NAME_PREC = "O5*"; // precursor

    private static final String ATOM_5_NAME = "O3*";

    private static final String ATOM_5_NAME_PREC = "C3*"; // precursor

    private Vector3D[] threePrimePositions; // FIXIT : dangerous: positions could be outdated; better: caching of positions at Object3D level
    private Vector3D[] fivePrimePositions;

    public JunctionMultiConstraintLink(List<Nucleotide3D> fivePrimeResidues, List<Nucleotide3D> threePrimeResidues,
				       ConstraintDouble constraint, ConstraintDouble _bridgableConstraint) throws RnaModelException {
	assert (fivePrimeResidues.size() == threePrimeResidues.size());
	assert (fivePrimeResidues.size() > 0);
	for (int i = 0; i < fivePrimeResidues.size(); ++i) {
	    if (!isProperNucleotide(fivePrimeResidues.get(i))) {
		Nucleotide3D nuc = fivePrimeResidues.get(i);
		System.out.println(ATOM_5_NAME + " : " + nuc.getIndexOfChild(ATOM_5_NAME));
		System.out.println(ATOM_3_NAME + " : " + nuc.getIndexOfChild(ATOM_3_NAME));
		throw new RnaModelException("5' nucleotide has missing atoms: " + fivePrimeResidues.get(i).toString());
	    }
	    if (!isProperNucleotide(threePrimeResidues.get(i))) {
		Nucleotide3D nuc = threePrimeResidues.get(i);
		System.out.println(ATOM_5_NAME + " : " + nuc.getIndexOfChild(ATOM_5_NAME) + " " 
				   + ATOM_3_NAME + " : " + nuc.getIndexOfChild(ATOM_3_NAME));
		throw new RnaModelException("3' nucleotide has missing atoms: " + threePrimeResidues.get(i).toString());
	    }
	    addObj(fivePrimeResidues.get(i));
	    addObj(threePrimeResidues.get(i));
	    symIds.add(0);
	}
	this.constraint = constraint;
	this.bridgableConstraint = _bridgableConstraint;
	this.permGen = new PermutationGenerator(fivePrimeResidues.size());
	initPositions(fivePrimeResidues, threePrimeResidues);
	assert(validate());
	log.info("Successfully created JunctionMultiConstraintLink: " + this.toString());
	log.info("Created junction multi constraint link with " + fivePrimeResidues.size() 
		 + " branches and constraints: " + constraint.getMin() + " " + constraint.getMax());
	if (bridgableConstraint != null) {
	    log.info("" + bridgableConstraint.getMin() + " " + bridgableConstraint.getMax());
	}
    }

    private void initPositions(List<Nucleotide3D> fivePrimeResidues, List<Nucleotide3D> threePrimeResidues) {
	assert(fivePrimeResidues.size() == threePrimeResidues.size());
	this.fivePrimePositions = new Vector3D[fivePrimeResidues.size()];
	this.threePrimePositions = new Vector3D[threePrimeResidues.size()];
	for (int i = 0; i < fivePrimeResidues.size(); ++i) {
	    fivePrimePositions[i] = fivePrimeResidues.get(i).getChild(ATOM_5_NAME).getPosition();
	    threePrimePositions[i] = threePrimeResidues.get(i).getChild(ATOM_3_NAME).getPosition();
	}
    }

    public int getBranchCount() { return size()/2; }

//     public double getDiff() {
// 	permGen.reset();
// 	double bestDiff = 1e30;
// 	double newDiff = 0;
// 	while (permGen.hasNext()) {
// 	    newDiff = getDiff(permGen.next());
// 	    if (newDiff < bestDiff) {
// 		bestDiff = newDiff;
// 	    }
// 	} 
// 	return bestDiff;
//     }

    public int[] findBestPermutation(List<CoordinateSystem> csList) {
	permGen.reset();
	double bestDiff = 1e30;
	double newDiff = 0;
	int[] bestPerm = new int[permGen.size()];
	double bridgeTerm = 0.0;
	// System.out.println("Used coordinate systems: ");
	// for (CoordinateSystem cs : csList) {
	// System.out.print(cs + " ; " );
	// }
	if (bridgableConstraint != null) {
	    try {
		bridgeTerm = getBridgable3(csList);
	    } catch (RnaModelException rne) {
		throw new RuntimeException(rne.getMessage()); // should never happen, missing atoms
	    }
	}
	do {
	    newDiff = bridgeTerm + getDiff(permGen.get(), csList);
	    if (newDiff < bestDiff) {
		bestDiff = newDiff;
		int[] tmpAry = permGen.get();
		for (int i = 0; i < tmpAry.length; ++i) {
		    bestPerm[i] = tmpAry[i];
		}
	    }
	} while (permGen.inc());
	return bestPerm;
    }

    public int[] findBestPermutation() {
	permGen.reset();
	double bestDiff = 1e30;
	double newDiff = 0;
	int[] bestPerm = new int[permGen.size()];
	// System.out.println("Used coordinate systems: ");
	// for (CoordinateSystem cs : csList) {
	// System.out.print(cs + " ; " );
	// }
	double bridgeTerm = 0.0;
	if (bridgableConstraint != null) {
	    try {
		bridgeTerm = getBridgable3();
	    } catch (RnaModelException rne) {
		throw new RuntimeException(rne.getMessage()); // should never happen, missing atoms
	    }
	}
	do {
	    newDiff = bridgeTerm + getDiff(permGen.get());
	    if (newDiff < bestDiff) {
		bestDiff = newDiff;
		int[] tmpAry = permGen.get();
		for (int i = 0; i < tmpAry.length; ++i) {
		    bestPerm[i] = tmpAry[i];
		}
	    }
	} while (permGen.inc());
	return bestPerm;
    }

    /** Scores structure of junction. The provided coordinate systems correspond to how each "branch" of a junction
     * has to be transformed. Careful with symmetry: this data structure only stores the symmetry copy ids of 
     * individual branches. It does not store the actual symmetry transformation. It is up to the calling class
     * to compute a set of coordinate systems that matches the provided symmetry constraints. */
    public double getDiff(List<CoordinateSystem> csList) {
	permGen.reset();
	double bestDiff = 1e30;
	double newDiff = 0;
	// System.out.println("Used coordinate systems: ");
	// for (CoordinateSystem cs : csList) {
	// System.out.print(cs + " ; " );
	// }
	double bridgeTerm = 0.0;
	if (bridgableConstraint != null) {
	    try {
		bridgeTerm = getBridgable3(csList);
	    } catch (RnaModelException rne) {
		throw new RuntimeException(rne.getMessage()); // should never happen, missing atoms
	    }
	}
	do {
	    newDiff = bridgeTerm + getDiff(permGen.get(), csList);
	    if (newDiff < bestDiff) {
		bestDiff = newDiff;
	    }
	} while (permGen.inc());
	return bestDiff;
    }

    private double getDiff(int[] perm) {
	double result = 0.0;
	double dist;
	for (int i = 0; i < perm.length; ++i) {
	    int id1 = perm[perm.length-1];
	    if (i > 0) {
		id1 = perm[i-1];
	    }
	    int id2 = perm[i];
	    assert id1 != id2;
	    dist = fivePrimePositions[id1].distance(threePrimePositions[id2]);
	    result += constraint.getDiff(dist);
	}
	return result;
    }

    private double getDiff(int[] perm, List<CoordinateSystem> csList) {
	assert perm.length == csList.size();
	double result = 0.0;
	double dist;
	for (int i = 0; i < perm.length; ++i) {
	    int id1 = perm[perm.length-1];
	    if (i > 0) {
		id1 = perm[i-1];
	    }
	    int id2 = perm[i];
	    assert id1 != id2;
	    assert id1 < csList.size();
	    CoordinateSystem cs1 = csList.get(id1);
	    CoordinateSystem cs2 = csList.get(id2);
	    // dist = cs1.activeTransform(getThreePrimeObject(id1).getChild(ATOM_3_NAME).getPosition()).distance(cs2.activeTransform(getFivePrimeObject(perm[id2]).getChild(ATOM_5_NAME).getPosition()));
	    dist = cs1.activeTransform(fivePrimePositions[id1]).distance(cs2.activeTransform(threePrimePositions[id2]));
	    result += constraint.getDiff(dist);
	}
	return result;
    }

    private double getBridgable3(List<CoordinateSystem> csList) throws RnaModelException {
	assert getBranchCount() == csList.size();
	double result = 0.0;
	double dist;
	for (int i = 0; i < getBranchCount(); ++i) {
	    CoordinateSystem cs1 = csList.get(i);
	    for (int j = i+1; j < getBranchCount(); ++j) {
		CoordinateSystem cs2 = csList.get(j);
		// dist = cs1.activeTransform(getThreePrimeObject(id1).getChild(ATOM_3_NAME).getPosition()).distance(cs2.activeTransform(getFivePrimeObject(perm[id2]).getChild(ATOM_5_NAME).getPosition()));
		result += isBridgable2((Nucleotide3D)(getFivePrimeObject(i)), 
				       (Nucleotide3D)(getThreePrimeObject(j)), cs1, cs2);
		result += isBridgable2((Nucleotide3D)(getFivePrimeObject(j)), 
				       (Nucleotide3D)(getThreePrimeObject(i)), cs2, cs1);
	    }
	}
	return result;
    }

    private double getBridgable3() throws RnaModelException {
	double result = 0.0;
	double dist;
	for (int i = 0; i < getBranchCount(); ++i) {
	    for (int j = i+1; j < getBranchCount(); ++j) {
		result += isBridgable2((Nucleotide3D)(getFivePrimeObject(i)), 
				       (Nucleotide3D)(getThreePrimeObject(j)));
		result += isBridgable2((Nucleotide3D)(getFivePrimeObject(j)), 
				       (Nucleotide3D)(getThreePrimeObject(i)));
	    }
	}
	return result;
    }

    private double getBridgable(int[] perm) throws RnaModelException {
	assert false; // deprecated
	double result = 0.0;
	double dist;
	for (int i = 0; i < perm.length; ++i) {
	    int id1 = perm[perm.length-1];
	    if (i > 0) {
		id1 = perm[i-1];
	    }
	    int id2 = perm[i];
	    assert id1 != id2;
	    // dist = cs1.activeTransform(getThreePrimeObject(id1).getChild(ATOM_3_NAME).getPosition()).distance(cs2.activeTransform(getFivePrimeObject(perm[id2]).getChild(ATOM_5_NAME).getPosition()));
	    result += isBridgable2((Nucleotide3D)(getFivePrimeObject(id1)), 
				   (Nucleotide3D)(getThreePrimeObject(id2)));
	}
	return result;
    }

    /** Given an atom position and a "precursor" position, compute p1 + length * (p1-p2)/|p1-p2| */
    Vector3D extrapolatePosition(Vector3D origPos, Vector3D precPos, double length) {
	if (origPos == precPos) {
	    return origPos;
	}
	Vector3D dv = origPos.minus(precPos);
	if (length > 0.0) {
	    dv.normalize();
	    dv.scale(length);
	}
	return origPos.plus(dv);
    }

    /** Return 0 for bridgable residue pair */
    private double isBridgable(Nucleotide3D fivePrimeRes, Nucleotide3D threePrimeRes, CoordinateSystem cs1, CoordinateSystem cs2) {
	assert false; // deprecated
	Vector3D lineStart = cs1.activeTransform(extrapolatePosition(fivePrimeRes.getChild(ATOM_5_NAME).getPosition(),
								     fivePrimeRes.getChild(ATOM_5_NAME_PREC).getPosition(), 2.0));
	Vector3D lineEnd = cs2.activeTransform(extrapolatePosition(threePrimeRes.getChild(ATOM_3_NAME).getPosition(),
								   threePrimeRes.getChild(ATOM_3_NAME_PREC).getPosition(), 2.0));
	Vector3D directionAndLength = lineEnd.minus(lineStart);
	double result = 0.0;
	for (int i = 0; i < fivePrimeRes.size(); ++i) {
	    if (fivePrimeRes.getChild(i).getName().equals(ATOM_5_NAME)) {
		continue;
	    }
	    Vector3D pos = cs1.activeTransform(fivePrimeRes.getChild(i).getPosition());
	    double d = GeometryTools.distanceToLineSegment(pos, lineStart, directionAndLength);
	    result += bridgableConstraint.getDiff(d);
	}
	for (int i = 0; i < threePrimeRes.size(); ++i) {
	    if (threePrimeRes.getChild(i).getName().equals(ATOM_3_NAME)) {
		continue;
	    }
	    Vector3D pos = cs2.activeTransform(threePrimeRes.getChild(i).getPosition());
	    double d = GeometryTools.distanceToLineSegment(pos, lineStart, directionAndLength);
	    result += bridgableConstraint.getDiff(d);
	}
	return result;
    }

    /** Return 0 for bridgable residue pair. First is the "fivePrimeResidue", the residue from which the loop starts,
     * second is the 3' residue, the residue to which the loop goes (5' to 3' direction) */
    private double isBridgable2(Nucleotide3D fivePrimeRes, Nucleotide3D threePrimeRes, CoordinateSystem cs1, CoordinateSystem cs2) throws RnaModelException {
	Vector3D planeNormal = NucleotideTools.computeBasePlaneNormal(fivePrimeRes);
	assert(planeNormal.length() > 0.99 && planeNormal.length() < 1.01);
	planeNormal = cs1.activeTransform(planeNormal).minus(cs1.getPosition());;
	assert(planeNormal.length() > 0.99 && planeNormal.length() < 1.01);
	Vector3D planeOffset = cs1.activeTransform(NucleotideTools.getGlycosilicBondAtom(fivePrimeRes).getPosition());
	double result = 0.0;
	for (int i = 0; i < threePrimeRes.size(); ++i) {
	    Vector3D pos = cs2.activeTransform(threePrimeRes.getChild(i).getPosition());
	    double d = GeometryTools.planeDistance(planeNormal, planeOffset, pos);
	    if (d < 0.0) {
		result -= d; // add penalty
	    }
	}
	// other way around
	Vector3D planeNormal2 = NucleotideTools.computeBasePlaneNormal(threePrimeRes);
	assert(planeNormal2.length() > 0.99 && planeNormal2.length() < 1.01);
	planeNormal2 = cs2.activeTransform(planeNormal2).minus(cs2.getPosition());
	assert(planeNormal2.length() > 0.99 && planeNormal2.length() < 1.01);
	Vector3D planeOffset2 = cs2.activeTransform(NucleotideTools.getGlycosilicBondAtom(threePrimeRes).getPosition());
	planeNormal2.scale(-1.0); // normal vector is in direction of backbone, however we want here direction away from helix
	assert(planeNormal2.length() < 1.01);
	assert(planeNormal2.length() > 0.99);
	for (int i = 0; i < fivePrimeRes.size(); ++i) {
	    Vector3D pos = cs1.activeTransform(fivePrimeRes.getChild(i).getPosition());
	    double d = GeometryTools.planeDistance(planeNormal2, planeOffset2, pos);
	    if (d < 0.0) {
		result -= d; // add penalty
	    }
	}

	return result;
    }

    /** Return 0 for bridgable residue pair */
    private double isBridgable2(Nucleotide3D fivePrimeRes, Nucleotide3D threePrimeRes) throws RnaModelException {
	Vector3D planeNormal = NucleotideTools.computeBasePlaneNormal(fivePrimeRes);
	Vector3D planeOffset = NucleotideTools.getGlycosilicBondAtom(fivePrimeRes).getPosition();
	double result = 0.0;
	for (int i = 0; i < threePrimeRes.size(); ++i) {
	    Vector3D pos = threePrimeRes.getChild(i).getPosition();
	    double d = GeometryTools.planeDistance(planeNormal, planeOffset, pos);
	    if (d < 0.0) {
		result -= d; // add penalty
	    }
	}
	// other way around
	Vector3D planeNormal2 = NucleotideTools.computeBasePlaneNormal(threePrimeRes);
	Vector3D planeOffset2 = NucleotideTools.getGlycosilicBondAtom(threePrimeRes).getPosition();
	planeNormal2.scale(-1.0); // normal vector is in direction of backbone, however we want here direction away from helix
	for (int i = 0; i < fivePrimeRes.size(); ++i) {
	    Vector3D pos = fivePrimeRes.getChild(i).getPosition();
	    double d = GeometryTools.planeDistance(planeNormal2, planeOffset2, pos);
	    if (d < 0.0) {
		result -= d; // add penalty
	    }
	}

	return result;
    }


    /** Return 0 for bridgable residue pair */
    private double isBridgable(Nucleotide3D fivePrimeRes, Nucleotide3D threePrimeRes) {
	assert false; // deprecated
	Vector3D lineStart = fivePrimeRes.getChild(ATOM_5_NAME).getPosition();
	Vector3D lineEnd = threePrimeRes.getChild(ATOM_3_NAME).getPosition();
	Vector3D directionAndLength = lineEnd.minus(lineStart);
	double result = 0.0;
	for (int i = 0; i < fivePrimeRes.size(); ++i) {
	    Vector3D pos = fivePrimeRes.getChild(i).getPosition();
	    double d = GeometryTools.distanceToLineSegment(pos, lineStart, directionAndLength);
	    result += bridgableConstraint.getDiff(d);
	}
	for (int i = 0; i < threePrimeRes.size(); ++i) {
	    Vector3D pos = threePrimeRes.getChild(i).getPosition();
	    double d = GeometryTools.distanceToLineSegment(pos, lineStart, directionAndLength);
	    result += bridgableConstraint.getDiff(d);
	}
	return result;
    }

    /** Returns index of 5' object of n'th branch */
    public int getFivePrimeIndex(int n) { return n * 2; }

    /** Returns index of 3' object of n'th branch */
    public int getThreePrimeIndex(int n) { return (n * 2) + 1; }

    public Object3D getFivePrimeObject(int n) { return getObj(getFivePrimeIndex(n)); }

    public Object3D getThreePrimeObject(int n) { return getObj(getThreePrimeIndex(n)); }

    /** returns constraint */
    public ConstraintDouble getConstraint() { return constraint; }

    public int getSymId(int index) { return symIds.get(index); }

    public int getSymId1() { return symIds.get(0); }

    public int getSymId2() { return symIds.get(1); }

    public boolean isProperNucleotide(Nucleotide3D nuc) {
	return (nuc.getIndexOfChild(ATOM_5_NAME) >= 0) && (nuc.getIndexOfChild(ATOM_3_NAME) >= 0);
    }

    public void setSymId(int index, int _symId1) { 
	while (symIds.size() <= index) {
	    symIds.add(-1);
	}
	symIds.set(index, _symId1);
    }

    public void setSymId1(int _symId1) { setSymId(0, _symId1); }

    public void setSymId2(int _symId2) { setSymId(1, _symId2); }

    public String toPrettyString() {
	return toString();
    }

    public String toString() {
	String result = "(JunctionMultiConstraintLink " + getName();
	for (int i = 0; i < size(); ++i) {
	    result = result + " " + getObj(i).getFullName();
	}
	for (int i = 0; i < symIds.size(); ++i) {
	    result = result + " " + symIds.get(i);
	}
	result = result +  " " + getConstraint().toString() + " )";
	return result;
    }

    public boolean validate() {
	return (getBranchCount() > 0) && (symIds.size() == getBranchCount()) && (getBranchCount() == threePrimePositions.length)
	    && (getBranchCount() == fivePrimePositions.length);
    }

}
	
