package rnadesign.rnamodel;

/** Checks if residues in junction are missing */
public class JunctionQualityChecker {

    public boolean isContinuous(StrandJunction3D junction) {

	for (int i = 0; i < junction.getStrandCount(); ++i) {
	    if (!isContinuousStrand(junction.getStrand(i))) {
		return false;
	    }
	}
	return true;
    }

    private boolean isContinuousStrand(NucleotideStrand strand) {
	int size = strand.getResidueCount();
	if (size < 2) {
	    return true;
	}
	Residue3D firstResidue = strand.getResidue3D(0);
	Residue3D lastResidue = strand.getResidue3D(size-1);
	assert firstResidue instanceof Nucleotide3D;
	assert lastResidue instanceof Nucleotide3D;
	// size according to assigned residue ids from pdb file:
	int size2 = ((Nucleotide3D)(lastResidue)).getAssignedNumber()
	    - ((Nucleotide3D)(firstResidue)).getAssignedNumber() + 1;
	return size2 == size;
    }

}
