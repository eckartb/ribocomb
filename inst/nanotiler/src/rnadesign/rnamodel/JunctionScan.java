package rnadesign.rnamodel;

import java.util.logging.*;

public class JunctionScan {

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    public static void main(String[] args) {
	log.setLevel(Level.FINEST);
	JunctionScanner scanner = new JunctionScanner(log);
	scanner.main(args);
    }

}
