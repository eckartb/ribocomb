package rnadesign.rnamodel;

import java.io.InputStream;
import java.io.IOException;
import java.util.*;
import java.util.zip.DataFormatException;
import tools3d.*;
import tools3d.objects3d.*;
import generaltools.PropertyTools;

public class JunctionScanOutputParser {
    
    int numStrands;
    List<CoordinateSystem3D> orientations;
    Properties properties;

    public JunctionScanOutputParser(InputStream is) throws IOException {
	properties = new Properties();
	properties.load(is);
	orientations = new ArrayList<CoordinateSystem3D>();
    }

    public List<CoordinateSystem3D> getOrientations() {
	return orientations;
    }
       

    private CoordinateSystem3D parseHelixOrientation(int externalId) throws DataFormatException {
	String lead = "H" + externalId + "_"; // like H1_ for helix 1
	double px = PropertyTools.parseDouble(properties, lead + "P_X");
	double py = PropertyTools.parseDouble(properties, lead + "P_Y");
	double pz = PropertyTools.parseDouble(properties, lead + "P_Z");

	double dxx = PropertyTools.parseDouble(properties, lead + "DX_X");
	double dxy = PropertyTools.parseDouble(properties, lead + "DX_Y");
	double dxz = PropertyTools.parseDouble(properties, lead + "DX_Z");

	double dyx = PropertyTools.parseDouble(properties, lead + "DY_X");
	double dyy = PropertyTools.parseDouble(properties, lead + "DY_Y");
	double dyz = PropertyTools.parseDouble(properties, lead + "DY_Z");

	Vector3D base = new Vector3D(px, py, pz);
	Vector3D ex = new Vector3D(dxx, dxy, dxz);
	Vector3D ey = new Vector3D(dyx, dyy, dyz);

	return new CoordinateSystem3D(base, ex, ey);

    }

    private void parseHelixOrientations(int numStrands) throws DataFormatException {
	orientations.clear();
	for (int i = 0; i < numStrands; ++i) {
	    int ii = i + 1; // external nomenclature
	    CoordinateSystem3D cs = parseHelixOrientation(ii);
	    if (cs != null) {
		orientations.add(cs);
	    } else {
		throw new DataFormatException("Error parsing helix " + ii);
	    }
	}
    }

    public void parse() throws DataFormatException {
	numStrands = Integer.parseInt(properties.getProperty("NUM_STRANDS"));
	parseHelixOrientations(numStrands);
    }

}
