package rnadesign.rnamodel;

import java.io.*;
import tools3d.objects3d.*;
import tools3d.objects3d.modeling.*;
import rnadesign.rnamodel.*;
import rnasecondary.*;
import java.util.logging.Logger;

import org.testng.annotations.*;
import java.util.ResourceBundle;

/** This class evaluates a PDB with the RnaSpot potential generated
 * with the program RnaSpotGen.
 */
public class JunctionScanner {

    private static final String DEFAULT_LOG_NAME = "junctionscan";

    private  String SLASH = PackageConstants.SLASH; // either "/" or "\"  depending on Unix of Windows

    public static final char JUNCTION_MODE_CHAR = 'j';

    public static final char KISSINGLOOP_MODE_CHAR = 'k';

    public static final int LOOP_LENGTH_SUM_MAX = 50;

    public static final int SEVERE_ERROR = 1;

    private static ResourceBundle rb = ResourceBundle.getBundle("JunctionScanner");

    private  int branchDescriptorOffset = Integer.parseInt(rb.getString("branchDescriptorOffset")); 
							   
    private boolean collisionCheckMode = false;
    
    private  double corridorRadius = Double.parseDouble(rb.getString("corridorRadius")); 

    private  double corridorStart = Double.parseDouble(rb.getString("corridorStart")); 

    private  int minStemLength = Integer.parseInt(rb.getString("minStemLength")); 

    private  boolean debugMode = false;

    private  String pdbEnding = ".pdb"; // ".ob3";
    
    private  String dbEnding = ".db.txt"; // ".ob3";
    
    /** Determines how accurate the local coordinate system has to fit atoms describing connector stem orientation. */
    private double stemFitRmsTolerance = Double.parseDouble(rb.getString("stemFitRmsTolerance")); 
    
    // private  Logger log = Logger.getLogger("global");
    // The below logger is initialized in NanoTiler.createAndShowGUI
    private Logger log;

    public JunctionScanner(Logger log) {
	this.log = log;
    }

    public JunctionScanner() {
	this(Logger.getLogger(DEFAULT_LOG_NAME));
    }

    /** writes pdb output */
     void writePdb(PrintStream ps, Object3D obj) {
	GeneralPdbWriter writer = new GeneralPdbWriter();
	writer.setOriginalMode(GeneralPdbWriter.RESIDUE_PDB_NUMBER); // use original PDB naming and numbering
	String pdbOutput = writer.writeString(obj);
	ps.println(pdbOutput);
    }

    /** writes a single junction */
    void writeJunctionPdb(PrintStream ps, StrandJunction3D junction, boolean originalMode) {
	 String pdbOutput = StrandJunctionTools.toPdb(junction, originalMode);
	ps.println(pdbOutput);
    }

    /** writes junction object in Lisp format. */
     void writeJunctionLisp(PrintStream ps, StrandJunction3D junction) {
	Object3DWriter writer = new StrandJunction3DLispWriter();
	String s = writer.writeString(junction);
	ps.println(s);
    }

    /** writes a single junction */
     void writeJunction(PrintStream ps, StrandJunction3D junction, int format, boolean originalMode) {
	switch (format) {
	case Object3DFormatter.LISP_FORMAT:
	    log.info("Writing junctions in Lisp format!");
	    writeJunctionLisp(ps, junction);
	    break;
	case Object3DFormatter.PDB_FORMAT:
	    log.info("Writing junctions in PDB format!");
	    writeJunctionPdb(ps, junction, originalMode);
	    break;
	case Object3DFormatter.XML_FORMAT:
	    assert false; // NOT YET IMPLEMENTED
	    break;
	default:
	    assert false;
	}
    }

    /** writes stem to print stream */
     void writeStem(PrintStream ps, RnaStem3D stem) {
	Stem stemInfo = stem.getStemInfo();
	ps.println("" + stemInfo);
    }

    /** writes all stems that are part of hierarchy to printstream */
     void writeStems(PrintStream ps, Object3D root) {
	if (root instanceof RnaStem3D) {
	    writeStem(ps, (RnaStem3D)root);
	    ps.println();
	}
	else {
	    for (int i = 0; i < root.size(); ++i) {
		writeStems(ps, root.getChild(i));
	    } 
	}
    }
    
    /** writes a set of junctions */
     void writeJunctions(String outFileBase, Object3D junctions,
			       int format, String ending, boolean originalMode) {
	int[] orderCounters = new int[10];
	JunctionQualityChecker qualityChecker = new JunctionQualityChecker();
	StrandJunctionNomenclature lilleyNomenClature = new LilleyNomenclature();
	StrandJunctionNomenclature incomingNomenclature = new IncomingNomenclature();
	for (int i = 0; i < junctions.size(); ++i) {
	    if (junctions.getChild(i) == null) {
		continue;
	    }
	    StrandJunction3D junction = (StrandJunction3D)(junctions.getChild(i));
	    int order = junction.getBranchCount();
	    if ((order < 1) || (order >= orderCounters.length)) {
		log.warning("Ignoring junction of order " + order);
		continue;
	    }
	    if (!qualityChecker.isContinuous(junction)) {
		log.warning("Junction is not everywhere continuous. Ignoring junction.");
		continue;		
	    }
// 	    if (order == 1) {
// 		// log.warning("sorry, stem-loops not implemented.");
// 		continue;
// 	    }
	    // print info to standard output:
	    orderCounters[order]++; // increase count of this order
// 	    String fileName = outFileBase + "." 
// 		+ order + "." + orderCounters[order] + pdbEnding;
	    String incomingName = incomingNomenclature.generateNomenclature(junction);
	    String incomingFileName = outFileBase + "_" + incomingName + pdbEnding;
	    String dbFileName = outFileBase + "_" + incomingName + dbEnding;
	    log.info("##################### Info about junction: " + incomingFileName + " : " 
		     + StrandJunctionTools.generateJunctionAngleInfo(junction) 
		     + " Nomenclature: " + incomingNomenclature.generateNomenclature(junction));
	    log.info("# Writing junction: " + incomingFileName);
// 	    try {
// 		FileOutputStream fos = new FileOutputStream(fileName);
// 		PrintStream ps = new PrintStream(fos);
// 		writeJunction(ps, junction , format);
// 		fos.close();
// 	    }
// 	    catch (IOException e) {
// 		log.severe("Error writing output file: " + fileName);
// 	    }
	    try {
		FileOutputStream fos = new FileOutputStream(incomingFileName);
		PrintStream ps = new PrintStream(fos);
		writeJunction(ps, junction , format, originalMode);
		fos.close();
	    }
	    catch (IOException e) {
		log.severe("Error writing output file: " + incomingFileName);
	    }
	    try {
		FileOutputStream fos2 = new FileOutputStream(dbFileName);
		PrintStream ps2 = new PrintStream(fos2);
		ps2.println(StrandJunctionDBExportTools.generateJunctionDBInfo(junction));
		fos2.close();
	    }
	    catch (IOException e) {
		log.severe("Error writing output file: " + dbFileName + " : " + e.getMessage());
	    }
	    log.info("##################### End of Info about junction: " + dbFileName); 
	}
    }

    private void exit(int errorCode) {
	System.exit(errorCode);
    }

    /** non-static main method */
    public  void main(String[] args) {
	boolean deleteAtomsMode = false; //  true;
	boolean originalMode = true;
	CorridorDescriptor corridorDescriptor = new CorridorDescriptor(corridorRadius, corridorStart);
	System.out.print("JunctionScan called with: ");
	for (int i = 0; i < args.length; ++i) {
	    System.out.print(args[i] + " ");
	}
	if (args.length < 3) {
	    log.severe("JunctionScan usage: JunctionScan readdir writedir pdbfilename [mode]");
	    System.exit(0);
	}
	System.out.println("");
	String readDir = args[0];
	String writeDir = args[1];
	// int format = Object3DFormatter.LISP_FORMAT;
	int format = Object3DFormatter.PDB_FORMAT;
	int loopLengthSumMax = LOOP_LENGTH_SUM_MAX;
	ForceField collisionDetector = new CollisionForceField();
	String fileName = args[2];
	String fileNameOrig = fileName;
	String[] words = fileNameOrig.split(SLASH);
	String fileNameOrigBase = words[words.length-1];
	String mode = "" + JUNCTION_MODE_CHAR + KISSINGLOOP_MODE_CHAR;
	if (args.length > 3) {
	    mode = args[3];
	}
	log.info("Using junction scanner mode: " + mode);
	String frontChar = fileName.substring(0,1);
	if ((!frontChar.equals(SLASH)) && (!frontChar.equals("."))) {
	    fileName = readDir + SLASH + fileNameOrigBase;
	}
	// renumber mode:
	boolean renumberMode = false;
	String renumberString = rb.getString("renumber");
	if ((renumberString != null) && renumberString.equals("true")) {
	    renumberMode = true;
	    System.out.println("Setting renumber mode to true!");
	}
	else {
	    System.out.println("Setting renumber mode to false (default)!");
	}
	// open file:
	try {
	    System.out.print("Processing file " + fileName);
	    InputStream is = new FileInputStream(fileName);
	    RnaPdbRnaviewReader reader = new RnaPdbRnaviewReader();
	    reader.setResidueRenumberMode(renumberMode);
	    Object3DLinkSetBundle bundle = reader.readBundle(is);
	    log.info("Finished reading coordinate file");
	    Object3D root = bundle.getObject3D();
	    LinkSet links = bundle.getLinks();
// 	    log.info("Number of read links: " + links.size());
// 	    for (int i = 0; i < links.size(); ++i) {
// 		System.out.println("" + links.get(i));
// 	    }
	    Object3DSet atomSet = Object3DTools.collectByClassName(root, "Atom3D"); // get all atoms
	    if (atomSet.size() == 0) {
		log.severe("No atoms read for file: " + fileName);
		exit(SEVERE_ERROR);
	    }
	    log.info("Number of read atoms: " + atomSet.size());
	    if (collisionCheckMode) {
		double collisionEnergy = collisionDetector.energy(atomSet, links); //CV - maybe make a checkbox so if you know there are no collisions you can turn this off - will not take as long for large files.
		if (collisionEnergy > 0.0) {
		    log.warning("Sorry, collisions detected in structure: " + fileName);
		    exit(SEVERE_ERROR);
		}
		else {
		    log.info("No collisions detected!");
		}
	    }
	    //}
	    if (deleteAtomsMode) {
		log.info("Removing all atoms!");
		Object3DTools.removeByClassName(root, "Atom3D");
	    }
	    is.close();
	    if (debugMode) {
		String debugFileName = "debug.pdb";
		try {
		    log.info("Writing debug pdb file: " + debugFileName);
		    FileOutputStream fos = new FileOutputStream(debugFileName);
		    PrintStream ps = new PrintStream(fos);
		    writePdb(ps, root);
		}
		catch (IOException e) {
		    log.severe("Could not open debug file: " + debugFileName);
		}
	    }
	    // old code: analyze coordinate data
	    // Object3DLinkSetBundle stemBundle = StemTools.generateStems(root, links);
	    // new code: use links which in turn were generated with the help 
	    // of RNAVIEW annotation in REMARK section of PDB file
	    log.info("Generating stems from " + links.size() + " links...");
	    Object3DLinkSetBundle stemBundle = StemTools.generateStemsFromLinks(root, links, minStemLength);
	    Object3D stemRoot = stemBundle.getObject3D();
	    System.out.println("Found stems: ");
	    writeStems(System.out, stemRoot);
	    String stemFileName = writeDir + SLASH + fileNameOrigBase + "_stems.out";
	    if (stemFileName.length() > 0) {
		Object3DSet branchDescriptors = BranchDescriptorTools.generateBranchDescriptors(new SimpleObject3DSet(stemBundle.getObject3D()), branchDescriptorOffset, stemFitRmsTolerance);
		FileOutputStream fos = null;
		try {
		    fos = new FileOutputStream(stemFileName);
		    PrintStream ps = new PrintStream(fos);
		    for (int i = 0; i < branchDescriptors.size(); i++) {
			String branchText = StrandJunctionDBExportTools.generateBranchDBInfo((BranchDescriptor3D)(branchDescriptors.get(i)), i);
			ps.println(branchText);
			// writeStems(ps, stemRoot);
		    }
		} catch (IOException ioe) {
		    System.out.println("Error writing stems to file: " + stemFileName + " : " + ioe.getMessage());
		} finally {
		    if (fos != null) {
			fos.close();
		    }
		}
	    }
	    if (stemRoot.size() > 0) {
		String outFileBase = writeDir + SLASH + fileNameOrigBase;
		if (mode.indexOf(JUNCTION_MODE_CHAR) >= 0) {
		    log.info("JunctionScan: Generating junctions!");		    
		    String junctionName = "junctions";
		    Object3D junctions = BranchDescriptorTools.generateJunctions(stemRoot, junctionName,  branchDescriptorOffset,
										 corridorDescriptor, stemFitRmsTolerance,
										 loopLengthSumMax);
		    log.info("Number of initial junctions: " + junctions.size());
		    writeJunctions(outFileBase, junctions, format, pdbEnding, originalMode);
		}
		if (mode.indexOf(KISSINGLOOP_MODE_CHAR) >= 0) {
		    String kissingLoopName = "kissingloops";
		    Object3D kissingLoops = BranchDescriptorTools.generateKissingLoops(stemRoot, kissingLoopName,  branchDescriptorOffset,
										       stemFitRmsTolerance);
		    // TODO : add corridor descriptor and naming convention
		    log.info("Number of initial kissing loops: " + kissingLoops.size());
		    writeJunctions(outFileBase, kissingLoops, format, pdbEnding, originalMode);
		}
	    }
	    else {
		log.warning("Could not find any stems!");
	    }				
	    
	    // keep only nucleotides or amino acids:
	    // 		Object3DSet objectSet = new SimpleObject3DSet();
	    // 		LinkSet links = new SimpleLinkSet();
	    // 		String[] objectNames = {"Nucleotide3D", "AminoAcid3D"};
	    // 		// generate object set such that it contains only amino acids ant nucleotides
	    // 		Object3DSetTools.addToSet(objectSet, root, objectNames);
	    
	    // 		// run elastic network algorithm:
	    // 		EigenvalueDecomposition decomp = eniAlgorithm.computeNormalModes(objectSet, links);
	    // 		// output:
	    // 		Syste m.out.println("Results:");
	    // 		writeDecomposition(System.out, decomp);
	}
	catch (IOException exp) {
	    log.severe("Error opening input file: " + fileName);
	}
	System.out.println("Good bye!");
    }

    @Test(groups={"slow", "new"})
    public void testJunctionScannerMain() {
	System.out.println("Starting JunctionScanner.testMain");
	ResourceBundle rb = ResourceBundle.getBundle("JunctionScanner");
	CorridorDescriptor corridorDescriptor = new CorridorDescriptor(corridorRadius, corridorStart);
	assert (rb != null);
	String fileNames = rb.getString("testFileNames");
	assert(fileNames != null);
	String[] tokens = fileNames.split(",");
	String[] numberJunctions = rb.getString("testJunctions").split(",");
	String[] lilleyNames = rb.getString("testJunctionsLilley").split(",");
	String[] incomingNames = rb.getString("testJunctionsIncoming").split(",");
	assert numberJunctions != null;
	assert numberJunctions.length == tokens.length;
	assert lilleyNames != null;
	String junctionName = "testJunc";
	int loopLengthSumMax = LOOP_LENGTH_SUM_MAX;
	Object3DFactory reader = new RnaPdbRnaviewReader();
	for (int i = 0; i < tokens.length; ++i) {
	    String fileName = tokens[i];
	    if (fileName.length() < 2) {
		continue;
	    }
	    else {
		System.out.println("Reading file: " + fileName);
	    }
	    try {
		InputStream is = new FileInputStream(fileName);
		assert(is!=null);
		Object3DLinkSetBundle bundle = reader.readBundle(is);
		is.close();		
		Object3D root = bundle.getObject3D();
		assert root.size() > 0;
		LinkSet links = bundle.getLinks();
		assert links.size() > 0;
		Object3DLinkSetBundle stemBundle = StemTools.generateStemsFromLinks(root, links, minStemLength);
		Object3D stemRoot = stemBundle.getObject3D();
		assert stemRoot.size() > 0;
		Object3D junctions = BranchDescriptorTools.generateJunctions(stemRoot, junctionName, branchDescriptorOffset,
									     corridorDescriptor, stemFitRmsTolerance,
									     loopLengthSumMax);
		Object3DSet junctionSet = Object3DTools.collectByClassName(
				   junctions, "StrandJunction3D");
		System.out.println("Number of found junctions: " + junctionSet.size());
		StrandJunctionNomenclature incomingNomenclature = new IncomingNomenclature();
		StrandJunctionNomenclature lilleyNomenclature = new LilleyNomenclature();
		for (int j = 0; j < junctionSet.size(); ++j) {
		    StrandJunction3D junction = (StrandJunction3D)(junctionSet.get(j));
		    String incomingName = incomingNomenclature.generateNomenclature(junction);
		    String lilleyName = lilleyNomenclature.generateNomenclature(junction);
		    System.out.println("Junction " + (j+1) + " : " + incomingName + " " + lilleyName);
		    if (lilleyNames.length > 0) {
			boolean found = false;
			for (int k = 0; k < lilleyNames.length; ++k) {
			    System.out.println("Comparing to Lilley name: " + lilleyNames[k]);
			    if (lilleyNames[k].equals(lilleyName)) {
				found = true;
				break;
			    }
			}
			if (! found) {
			    System.out.println("Junction Lilley nomenclature not found in filename: " 
					       + fileName + " : " + lilleyName);
			}
		    }
		    else {
			// assert false; // no junction should be found
			System.out.println("No junctions defined in test property file!");
		    }
		    if (incomingNames.length > 0) {
			boolean found = false;
			for (int k = 0; k < incomingNames.length; ++k) {
			    System.out.println("Comparing to Incoming name: " + incomingNames[k]);
			    if (incomingNames[k].equals(incomingName)) {
				found = true;
				break;
			    }
			}
			if (! found) {
			    System.out.println("Junction Incoming nomenclature not found: "
					       + fileName + " : " + incomingName);
			    // assert false; // name must be found!
			}
		    }
		    else {
			// assert false; // no junction should be found
		    }
		}
	        if (junctionSet.size() != Integer.parseInt(numberJunctions[i])) {
		    log.warning("Number of found junctions is not equal to the ones defined in JunctionScanner property file: "
				+ junctionSet.size() + " " + numberJunctions[i]);
		}
	    }
	    catch(Object3DIOException e1) {
		System.out.println("Object3DIOException: " + e1.getMessage());
		assert false;
	    }
	    catch(IOException ioe) {
		System.out.println("IO Error: " + ioe.getMessage());
		assert false;
	    }

	}
    }
   
    @Test(groups={"slow", "new"})
    public void testJunctionScannerKissingLoops() {
	ResourceBundle rb = ResourceBundle.getBundle("JunctionScanner");
	CorridorDescriptor corridorDescriptor = new CorridorDescriptor(corridorRadius, corridorStart);
	assert (rb != null);
	String fileNames = rb.getString("testKissingLoopNames");
	assert(fileNames != null);
	String[] tokens = fileNames.split(",");
	String[] numberJunctions = rb.getString("testKissingLoops").split(",");
	assert numberJunctions != null;
	String[] incomingNames = null;
	String junctionName = "testKissingLoop";
	Object3DFactory reader = new RnaPdbRnaviewReader();
	for (int i = 0; i < tokens.length; ++i) {
	    String fileName = tokens[i];
	    if (fileName.length() < 2) {
		continue;
	    }
	    else {
		System.out.println("Reading file: " + fileName);
	    }
	    try {
		InputStream is = new FileInputStream(fileName);
		assert(is!=null);
		Object3DLinkSetBundle bundle = reader.readBundle(is);
		is.close();		
		Object3D root = bundle.getObject3D();
		LinkSet links = bundle.getLinks();
		Object3DLinkSetBundle stemBundle = StemTools.generateStemsFromLinks(root, links, minStemLength);
		Object3D stemRoot = stemBundle.getObject3D();
		assert stemRoot.size() > 0;
		Object3D junctions = BranchDescriptorTools.generateKissingLoops(stemRoot, junctionName, branchDescriptorOffset,
										stemFitRmsTolerance);
		Object3DSet junctionSet = Object3DTools.collectByClassName(
						   junctions, "KissingLoop3D");
		System.out.println("Number of found kissing loops: " + junctionSet.size());
		StrandJunctionNomenclature incomingNomenclature = new IncomingNomenclature();
		StrandJunctionNomenclature lilleyNomenclature = new LilleyNomenclature();
		for (int j = 0; j < junctionSet.size(); ++j) {
		    StrandJunction3D junction = (StrandJunction3D)(junctionSet.get(j));
		    String incomingName = incomingNomenclature.generateNomenclature(junction);
		    System.out.println("Kissing loop " + (j+1) + " : " + incomingName);
		    if ((incomingNames != null) && (incomingNames.length > 0)) {
			boolean found = true; // test taken out
// 			for (int k = 0; k < incomingNames.length; ++k) {
// 			    System.out.println("Comparing to Incoming name: " + incomingNames[k]);
// 			    if (incomingNames[k].equals(incomingName)) {
// 				found = true;
// 				break;
// 			    }
// 			}
			if (! found) {
			    System.out.println("Kissing loop Incoming nomenclature not found!");
			    assert false; // name must be found!
			}
		    }
// 		    else {
// 			assert false; // no junction should be found
// 		    }
		}
		System.out.println("Number of recognized kissing loops: " + junctionSet.size());
		assert junctionSet.size() == Integer.parseInt(numberJunctions[i]);
	    }
	    catch(Object3DIOException e1) {
		System.out.println("Object3DIOException: " + e1.getMessage());
		assert false;
	    }
	    catch(IOException ioe) {
		System.out.println("IO Error: " + ioe.getMessage());
		assert false;
	    }
	    
	}
    }
    

}
    
