package rnadesign.rnamodel;

import tools3d.*;
import tools3d.objects3d.*;
import sequence.*;
import graphtools.PermutationGenerator;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.*;
import static rnadesign.rnamodel.PackageConstants.*;

public class JunctionSimilarity implements Object3DSimilarity {

    Logger log = Logger.getLogger(LOGFILE_DEFAULT);

    double sequenceIdentityLimitFraction = 1.0;

    private Vector3D[] generatePositions(StrandJunction3D jnc, int[] permutation) {
	List<Vector3D> result = new ArrayList<Vector3D>();
	for (int i = 0; i < jnc.getBranchCount(); ++i) {
	    NucleotideStrand strand = jnc.getStrand(permutation[i]);
	    for (int j = 0; j < strand.getResidueCount(); ++j) {
		result.add(strand.getResidue3D(j).getChild("C4*").getPosition());
	    }
	}
	Vector3D[] finalResult = new Vector3D[result.size()];
	for (int i = 0; i < result.size(); ++i) {
	    finalResult[i] = result.get(i);
	}
	return finalResult;	
    }

    private Vector3D[] generatePositions(StrandJunction3D j) {
	int[] perm = new int[j.getBranchCount()];
	for (int i = 0; i < perm.length; ++i) {
	    perm[i] = i;
	}
	return generatePositions(j, perm);
    }


    /*    private Properties similarity(StrandJunction3D j1, StrandJunction3D j2,
				  int[] permutation) {
	assert permutation != null;
	if ((j1.getBranchCount() != j2.getBranchCount())
	    || (permutation.length != j1.getBranchCount())) {
	    return null;
	}
	
    }
    */

    private double computeSequenceIdentifyFraction(StrandJunction3D jnc1,
						   StrandJunction3D jnc2,
						   int[] perm) {
	assert perm != null;
	int countId = 0;
	int countTot = 0;
	for (int i = 0; i < perm.length; ++i) {
	    NucleotideStrand strand1 = jnc1.getStrand(i);
	    NucleotideStrand strand2 = jnc2.getStrand(perm[i]);
	    if (strand1.size() != strand2.size()) { // sequence sizes do not match
		return -1.0;
	    }
	    int numIdent = SequenceTools.countIdentical(strand1, strand2);
	    countId += numIdent;
	    countTot += strand1.size();
	}
	return ((double)countId) / ((double)countTot);
    }

    public Properties similarity(Object3D obj1, Object3D obj2) {
	Properties properties = new Properties();
	properties.setProperty(Object3DSimilarity.SUPERPOSABLE, "false");
	if (! ((obj1 instanceof StrandJunction3D) && (obj2 instanceof StrandJunction3D))) {
	    return properties;
	}
	StrandJunction3D jnc1 = (StrandJunction3D)obj1;
	StrandJunction3D jnc2 = (StrandJunction3D)obj2;
	int numStrands = jnc1.getBranchCount();
	if (jnc2.getBranchCount() != numStrands) {
	    return properties;
	}
	PermutationGenerator permGen = new PermutationGenerator(numStrands);
	Vector3D[] vSet1 = generatePositions(jnc1);
	double bestDrms = 999999.9;
	int[] bestPerm = null;
	while (permGen.hasMore()) {
	    int perm[] = permGen.getNext();
	    double seqIdentityFraction = computeSequenceIdentifyFraction(jnc1, jnc2, perm);
	    if (seqIdentityFraction < sequenceIdentityLimitFraction) {
		continue; // includes case of negative fraction, indicating incompatible sequences
	    }
	    Vector3D[] vSet2 = generatePositions(jnc2, perm);
	    double drms = Vector3DTools.computeDrms(vSet1, vSet2);
	    if (drms < bestDrms) {
		bestDrms = drms;
		bestPerm = perm;
	    }
	}
	if (bestPerm != null) {
	    properties.setProperty(Object3DSimilarity.DRMS, "" + bestDrms);
	    properties.setProperty(Object3DSimilarity.SUPERPOSABLE, "true");
	}
	else {
	    log.info("Junctions seem to be not superposable!");
	}
	return properties;
    }

}
