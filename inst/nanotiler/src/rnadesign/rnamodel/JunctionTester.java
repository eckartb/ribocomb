package rnadesign.rnamodel;

import java.io.*;
import tools3d.objects3d.*;
import tools3d.objects3d.modeling.*;
import rnadesign.rnamodel.*;
import rnasecondary.*;
import java.util.logging.Logger;

/** This class evaluates a PDB with the RnaSpot potential generated
 * with the program RnaSpotGen.
 */
public class JunctionTester {

    private  String SLASH = File.separator; // either "/" or "\"  depending on Unix of Windows

    private  int branchDescriptorOffset = 2; // use 4 instead of 2: larger stems included!

    private double corridorRadius = -1.0;
    
    private double corridorStart = 10.0;

    private double stemFitRmsTolerance = 2.0;

    private boolean originalMode = true;

    private  int gap = 4;
    
    private  double slack = 15.0;

    private  boolean debugMode = false; // true;

    // private  Logger log = Logger.getLogger("global");
    // The below logger is initialized in NanoTiler.createAndShowGUI
    private  Logger log = Logger.getLogger("NanoTiler_debug");


    /** writes pdb output */
     void writePdb(PrintStream ps, Object3D obj) {
	GeneralPdbWriter writer = new GeneralPdbWriter();
	String pdbOutput = writer.writeString(obj);
	ps.println(pdbOutput);
    }

    /** writes a single junction */
     void writeJunctionPdb(PrintStream ps, StrandJunction3D junction) {
	String pdbOutput = StrandJunctionTools.toPdb(junction, originalMode);
	ps.println(pdbOutput);
    }

    /** writes junction object in Lisp format. */
     void writeJunctionLisp(PrintStream ps, StrandJunction3D junction) {
	Object3DWriter writer = new StrandJunction3DLispWriter();
	String s = writer.writeString(junction);
	ps.println(s);
    }
    
    /** writes a single junction */
     void writeJunction(PrintStream ps, StrandJunction3D junction, int format) {
	switch (format) {
	case Object3DFormatter.LISP_FORMAT:
	    log.info("Writing junctions in Lisp format!");
	    writeJunctionLisp(ps, junction);
	    break;
	case Object3DFormatter.PDB_FORMAT:
	    log.info("Writing junctions in PDB format!");
	    writeJunctionPdb(ps, junction);
	    break;
	case Object3DFormatter.XML_FORMAT:
	    assert false; // NOT YET IMPLEMENTED
	    break;
	default:
	    assert false;
	}
    }

    /** writes stem to print stream */
     void writeStem(PrintStream ps, RnaStem3D stem) {
	Stem stemInfo = stem.getStemInfo();
	ps.println("" + stemInfo);
    }

    /** writes all stems that are part of hierarchy to printstream */
     void writeStems(PrintStream ps, Object3D root) {
	if (root instanceof RnaStem3D) {
	    writeStem(ps, (RnaStem3D)root);
	    ps.println();
	}
	else {
	    for (int i = 0; i < root.size(); ++i) {
		writeStems(ps, root.getChild(i));
	    } 
	}
    }
    
    /** writes a set of junctions */
     void writeJunctions(String outFileBase, Object3D junctions,
			       int format, String ending) {
	int[] orderCounters = new int[10];
	for (int i = 0; i < junctions.size(); ++i) {
	    if (junctions.getChild(i) == null) {
		continue;
	    }
	    StrandJunction3D junction = (StrandJunction3D)(junctions.getChild(i));
	    int order = junction.getBranchCount();
	    if ((order < 1) || (order >= orderCounters.length)) {
		log.warning("weird junction encountered.");
		continue;
	    }
	    if (order == 1) {
		log.warning("sorry, stem-loops not implemented.");
		continue;
	    }
	    // print info to standard output:
	    orderCounters[order]++; // increase count of this order
	    String fileName = outFileBase + "." 
		+ order + "." + orderCounters[order] + ending;
	    log.info("##################### Info about junction: " + fileName + " : " 
		     + StrandJunctionTools.generateJunctionAngleInfo(junction));
	    log.info("Writing junction: " + fileName);
	    try {
		FileOutputStream fos = new FileOutputStream(fileName);
		PrintStream ps = new PrintStream(fos);
		writeJunction(ps, junction , format);
		fos.close();
	    }
	    catch (IOException e) {
		log.severe("Error writing output file: " + fileName);
	    }
	}
    }

    /** main method */
    public void main(String[] args) {
	boolean deleteAtomsMode = false; //  true;
	System.out.print("ElasticNetTest version 1.3 call with: ");
	for (int i = 0; i < args.length; ++i) {
	    System.out.print(args[i] + " ");
	}
	if (args.length < 3) {
	    log.severe("JunctionTest usage: JunctionTest readdir writedir pdbfilename");
	    System.exit(0);
	}
	System.out.println("");
	CorridorDescriptor corridorDescriptor = new CorridorDescriptor(corridorRadius, corridorStart);
	GeneralPdbWriter writer = new GeneralPdbWriter();
	String readDir = args[0];
	String writeDir = args[1];
	String ending = ".pdb"; // ".ob3";
	// int format = Object3DFormatter.LISP_FORMAT;
	int format = Object3DFormatter.PDB_FORMAT;
	ForceField collisionDetector = new CollisionForceField();
	for (int i = 2; i < args.length; ++i) {
	    String fileName = args[i];
	    String frontChar = fileName.substring(0,1);
	    if ((!frontChar.equals(SLASH)) && (!frontChar.equals("."))) {
		fileName = readDir + SLASH + args[i];
	    }
	    // open file:
	    try {
		System.out.print("Processing file " + (i+1) + " : " + fileName);
		InputStream is = new FileInputStream(fileName);
		Object3DFactory reader;
		reader = new RnaPdbRnaviewReader();
		Object3DLinkSetBundle bundle = reader.readBundle(is);
		Object3D root = bundle.getObject3D();
		LinkSet links = bundle.getLinks();
		Object3DSet atomSet = Object3DTools.collectByClassName(root, "Atom3D"); // get all atoms
		//if(know no collisions) {
		double collisionEnergy = collisionDetector.energy(atomSet, links); //CV - maybe make a checkbox so if you know there are no collisions you can turn this off - will not take as long for large files.
		if (collisionEnergy > 0.0) {
		    log.warning("Sorry, collisions detected in structure: " + fileName);
		    continue;
		}
		else {
		    log.fine("No collisions detected!");
		}
		//}
		if (deleteAtomsMode) {
		    log.info("Removing all atoms!");
		    Object3DTools.removeByClassName(root, "Atom3D");
		}
		is.close();

		if (debugMode) {
		    String debugFileName = "debug.pdb";
		    try {
			FileOutputStream fos = new FileOutputStream(debugFileName);
			PrintStream ps = new PrintStream(fos);
			writePdb(ps, root);
		    }
		    catch (IOException e) {
			log.severe("Could not open debug file: " + debugFileName);
		    }
		}
		Object3DLinkSetBundle stemBundle = StemTools.generateStems(root);
		Object3D stemRoot = stemBundle.getObject3D();
		writeStems(System.out, stemRoot);
		if (stemRoot.size() > 0) {
		    log.fine("JunctionTest: Generating junctions with corridor descriptor " + corridorDescriptor);
		    String junctionName = "junctions";
		    Object3D junctions = BranchDescriptorTools.generateJunctions(stemRoot, junctionName, branchDescriptorOffset,
										 corridorDescriptor, stemFitRmsTolerance,
										 JunctionScanner.LOOP_LENGTH_SUM_MAX);
		    String outFileBase = writeDir + SLASH + args[i] + "_junc";
		    writeJunctions(outFileBase, junctions, format, ending);
		}
		else {
		    log.warning("Could not find any stems!");
		}				

		// keep only nucleotides or amino acids:
// 		Object3DSet objectSet = new SimpleObject3DSet();
// 		LinkSet links = new SimpleLinkSet();
// 		String[] objectNames = {"Nucleotide3D", "AminoAcid3D"};
// 		// generate object set such that it contains only amino acids ant nucleotides
// 		Object3DSetTools.addToSet(objectSet, root, objectNames);

// 		// run elastic network algorithm:
// 		EigenvalueDecomposition decomp = eniAlgorithm.computeNormalModes(objectSet, links);
// 		// output:
// 		Syste m.out.println("Results:");
// 		writeDecomposition(System.out, decomp);
	    }
	    catch (IOException exp) {
		log.severe("Error opening input file: " + fileName);
	    }
	}

    }

}
