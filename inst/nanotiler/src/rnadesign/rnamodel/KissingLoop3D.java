package rnadesign.rnamodel;

import java.util.Iterator;
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
import tools3d.Vector3D;
import tools3d.objects3d.*;
import rnasecondary.Stem;
import java.util.logging.*;

/** Describes concept on n-way junction of strand.
 * Each arm of the junction corresponds to one or two sequences. 
 * There is no function "addBranch". Instead one defines
 * new branches by adding them with "insertChild"
 */
public class KissingLoop3D extends SimpleObject3D implements StrandJunction3D {

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    public static final double BRANCH_MEET_DIST = 30.0;

    // private List branches = new ArrayList();
    // private List strands = new ArrayList();

    private int branchCount = 0;

    private boolean internalHelices = false;

    public static final int KISSING_LOOP_SIZE_MAX = 50; // avoid extremely large loops

    /** returns for n'th sequence the branch id for which it is the incoming sequence. */
    private int[] incomingBranchIds = new int[BRANCH_MAX];    
    /** returns for n'th sequence the branch id for which it is the outgoing sequence. */
    private int[] outgoingBranchIds = new int[BRANCH_MAX];
    /** index of incoming sequence of n'th branch */
    private int[] incomingSeqIds = new int[BRANCH_MAX];
    /** index of outgoing sequence of n'th branch */
    private int[] outgoingSeqIds = new int[BRANCH_MAX];

    public static final String CLASS_NAME = "KissingLoop3D";

    /** default constructor is just used internally for methods like "cloneDeep" */
    public KissingLoop3D() { }

    public KissingLoop3D(Object3DSet branchesOrig) {
	assert (branchesOrig != null) && (branchesOrig.size() == 2);
	log.fine("Starting KissingLoop3D(Object3DSet): " + branchesOrig.size());
	Vector3D pos = new Vector3D(0, 0, 0);
	for (int i = 0; i < branchesOrig.size(); ++i) {
	    BranchDescriptor3D branch = (BranchDescriptor3D)(((BranchDescriptor3D)(branchesOrig.get(i))).cloneDeep());
	    // search if incoming or outgoing sequence of branch descriptor is alrady known.
	    // if yes, overwrite with already known sequence
	    // BranchDescriptor3D branch = (BranchDescriptor3D)(branchesOrig.get(i)); 
	    assert (branch.isSingleSequence());
	    // log.fine("branch descriptor position " + (i+1) + " : " + branch.getPosition());
	    pos.add(branch.getPosition());
	    // branches.add(branch);
	    insertChild(branch);
	    // addStrands(branch);
	    // 	    addIncomingStrand(branch.getIncomingStrand(), getBranchCount()-1);
	    // 	    addOutgoingStrand(branch.getOutgoingStrand(), getBranchCount()-1);
	}
	pos.scale(1.0 / (double)(branchesOrig.size()));
	// TODO : better: center of mass of all atoms in middle area
	// cloneStrands(); // use only clone copy of strands // not necessary because BranchDescriptors are already cloned
	// linePos not used anymore:
	// Vector3D linePos = StrandJunctionTools.computeJunctionCenter(this); // compute medium position where lines meet
	// 	if (linePos.distance(pos) < BRANCH_MEET_DIST) {
	// 	    log.fine("BranchDescriptor meet position was plausible: " + linePos 
	// 			       + " using nonetheless average BranchDescriptor position: " + pos);
	// 	    setIsolatedPosition(linePos);
	// 	    // Object3DTools.balanceTree(this); // set position to center of mass
	// 	}
	// 	else {
	// Object3DTools.balanceTree(this); // dangerous! Might mess up direction of branch descriptors
	setIsolatedPosition(pos); // set position to center of mass
	    // log.fine("Warning: BranchDescriptor meet position was not plausible: " + linePos 
	    // + " using instead average BranchDescriptor position: " + getPosition());
	// }
	// avoid setPosition method which in turn calls new version of translate
	// pos = pos.minus(getPosition());
	// setPosition(pos); // 
	// 	setRelativePosition(getRelativePosition().plus(pos));
	// 	Vector3D negShift = pos.mul(-1.0); // compensate shift for child nodes
	// 	for (int i = 0; i < size(); ++i) {
	// 	    Object3D child = getChild(i);
	// 	    child.translate(negShift); // translate in opposite direction. Relatively cheap method
	// 	}
	log.fine("Quitting KissingLoop3D(Object3DSet): " 
			   + getPosition() + " " + size() + " " + getBranchCount() + " " + getStrandCount());
	// assert isValid(); // checks if for each strand is the incoming strand of one branch and the outgoing strand of another branch
	assert checkChildrenUnique();
    }

    /** check if incoming and outgoing strands already exist. Add them as child nodes if they do not exist yet.
     * TODO : verify for complication.
     */
    private void addStrands(BranchDescriptor3D branch) {
	// find out index of branch:
	// int branchIndex = getIndexOfChild(branch);
	int branchIndex = getChildClassCounter(branch); // big change! Check if not bugs where introduced.
	assert branchIndex >= 0; // must be found
	NucleotideStrand incoming = branch.getIncomingStrand();
	NucleotideStrand outgoing = branch.getOutgoingStrand();
	int inId = findStrandIndex(incoming);
	if (inId < 0) { // strand not found
	    insertChild(incoming);
	    inId = size()-1;
	}
	else {
	    // adjust incoming strand of branch descriptor:
	    branch.setIncomingStrand(getStrand(inId));
	}
	incomingBranchIds[inId] = branchIndex; 
	incomingSeqIds[branchIndex] = inId;
	int outId = findStrandIndex(outgoing);
	if (outId < 0) { // strand not found
	    insertChild(outgoing);
	    outId = size()-1;
	}
	else {
	    // adjust incoming strand of branch descriptor:
	    branch.setOutgoingStrand(getStrand(outId));
	}
	outgoingBranchIds[outId] = branchIndex; 
    	outgoingSeqIds[branchIndex] = outId;
    }

//     private void addIncomingStrand(NucleotideStrand strand, int branchIndex) {
// 	int id = findStrandIndex(strand);
// 	// 	if (id < 0) {
// 	// 	    strands.add(strand);
// 	// 	    id = strands.size()-1;
// 	// 	}
// 	incomingBranchIds[id] = branchIndex; 
// 	incomingSeqIds[branchIndex] = id;
//     }

//     private void addOutgoingStrand(NucleotideStrand strand, int branchIndex) {
// 	int id = findStrandIndex(strand);
// 	if (id < 0) {
// 	    strands.add(strand);
// 	    id = strands.size()-1;
// 	}
// 	outgoingBranchIds[id] = branchIndex; 
//     	outgoingSeqIds[branchIndex] = id;
//     }

    /** performs deep copy of strands */
//     private void cloneStrands() {
// 	for (int i = 0; i < strands.size(); ++i) {
// 	    strands.set(i, getStrand(i).cloneDeep());
// 	}
//     }

    /** "deep" clone including children: every object is newly generated!
     * TODO
     */
    public Object cloneDeepThis() {
	KissingLoop3D obj = new KissingLoop3D();
	obj.copyDeepThisCore(this);
	obj.branchCount = branchCount;

	obj.incomingBranchIds = new int[BRANCH_MAX];
	obj.outgoingBranchIds = new int[BRANCH_MAX];
	obj.incomingSeqIds = new int[BRANCH_MAX];
	obj.outgoingSeqIds = new int[BRANCH_MAX];
	for (int i = 0; i < BRANCH_MAX; ++i) {
	    obj.incomingBranchIds[i] = incomingBranchIds[i];
	    obj.outgoingBranchIds[i] = outgoingBranchIds[i];
	    obj.incomingSeqIds[i] = incomingSeqIds[i];
	    obj.outgoingSeqIds[i] = outgoingSeqIds[i];
	}



	// 	obj.setName(getName());
	// 	obj.setParent(getParent());
	// 	obj.setPosition(getPosition());
	// 	for (int i = 0; i < getBranchCount(); ++i) {
	// 	    BranchDescriptor3D branch = (BranchDescriptor3D)(getBranch(i).cloneDeep()); // clone new branch and sequence!
	// 	    // obj.branches.add(branch);
	// 	    // obj.insertChild(branch);
	// 	    obj.addIncomingStrand(branch.getIncomingStrand(), getBranchCount()-1);
	// 	    obj.addOutgoingStrand(branch.getOutgoingStrand(), getBranchCount()-1);
	// 	}
	
	// do not copy arrays (incomingBranchIds etc). Rely on them being updated in the 
	// specialized version of insertChild
	
	return obj; // TODO : test if ok!
    }

    /** "deep" clone including children: every object is newly generated! Extra code because branch descriptors
     * must have correct references to strands which are their "siblings" in the hierarchy. */
    public Object cloneDeep() {
	KissingLoop3D newObj = (KissingLoop3D)(cloneDeepThis());

	newObj.setFilename(getFilename());

	for (int i = 0; i < size(); ++i) {
	    newObj.insertRawChild((Object3D)(getChild(i).cloneDeep()));
	}
	NucleotideStrand[] strands = getStrands();
	int branchCount = getBranchCount();
	// the following code makes sure that the new branch descriptors do not have their own copies of the strands
	// but use the strands defined in the new junction
	for (int i = 0; i < branchCount; ++i) {
	    // find which strand 
	    BranchDescriptor3D branch = getBranch(i);
	    BranchDescriptor3D newBranch = newObj.getBranch(i);
	    NucleotideStrand inStrand = branch.getIncomingStrand();
	    NucleotideStrand outStrand = branch.getOutgoingStrand();
	    for (int j = 0; j < strands.length; ++j) {
		if (inStrand == strands[j]) {
		    newBranch.setIncomingStrand(newObj.getStrand(j));
		}
		if (outStrand == strands[j]) {
		    newBranch.setOutgoingStrand(newObj.getStrand(j));
		}
	    }
	}
	return newObj;
    }

    /** Not yet implemented ! */
    public Object3DSet cloneDown(int junctionOrder) { assert false; return null; }


    /** returns true, if all branch descriptor occupy a free "corridor" with a certain radius.
     * If supplied radius is smaller or equal zero, true if returned. */
    public boolean corridorCheck(CorridorDescriptor corridorDescriptor ) {
	double corridorRadius = corridorDescriptor.radius;
	double corridorStart = corridorDescriptor.start;
	if (corridorRadius <= 0.0) {
	    return true;
	}
	Object3DSet atomSet = Object3DTools.collectByClassName(this, "Atom3D"); // obtain all atoms
	assert atomSet.size() > 0;
	for (int i = 0; i < getBranchCount(); ++i) {
	    BranchDescriptor3D branch = getBranch(i);
	    if (! Object3DTools.corridorCheck(branch.getPosition(), branch.getDirection(), corridorRadius, corridorStart, atomSet)) {
		return false;
	    }
	}
	return true; 
    }

    /** return index of strand if found, -1 otherwise */
    public int findStrandIndex(NucleotideStrand strand) {
	for (int i = 0; i < getStrandCount(); ++i) {
	    if (getStrand(i).isProbablyEqual(strand)) { // fast check
		return i;
	    }
	}
	return -1;
    }

    /** appends strand m to strand n, removes strand m.
     * Does not make sends for kissing loop, there
     * should be only one sequence. */
    public void fuseStrands(int n, int m) {
	assert false;
    }

    /** returns number of branches */
    public int getBranchCount() { 
	// log.fine("Calling slow version of getBranchCount");
	// return branchCount;
	return getChildCount("BranchDescriptor3D");
	// return branches.size(); 
    }

    public String getClassName() { return CLASS_NAME; }

    /** index of incoming sequence of n'th branch */
    public int[] getIncomingSeqIds() { return incomingSeqIds; }

    /** index of outgoing sequence of n'th branch */
    public int[] getOutgoingSeqIds() { return outgoingSeqIds; }

    /** returns number of defined strands */
    public int getStrandCount() { 
	// log.fine("Calling slow version of getStrandCount");
	int counter = 0;
	for (int i = 0; i < size(); ++i) {
	    if (getChild(i) instanceof NucleotideStrand) {
		++counter;
	    }
	}
	return counter;
	// return strands.size(); 
    }
    
    /** returns descriptor for n'th branch */
    public BranchDescriptor3D getBranch(int n) { 
	int idx = getIndexOfChild(n, "BranchDescriptor3D");
	assert (idx >= 0);
	return (BranchDescriptor3D)(getChild(idx));
	// return (BranchDescriptor3D)(branches.get(n)); 
    }

    /** Returns length of strand loops - NOT YET IMPLEMENTED */
    public int getLoopLength(int strandId) {
	int inBId = getIncomingBranchId(strandId); // return branch descriptor id of 
	int outBId = getOutgoingBranchId(strandId);
	BranchDescriptor3D inBranch = getBranch(inBId);
	BranchDescriptor3D outBranch = getBranch(outBId);
	int inId = inBranch.getIncomingIndex() + inBranch.getOffset() + 1; // points to first nucl. of loop
	int outId = outBranch.getOutgoingIndex() - outBranch.getOffset() - 1; // points to last nucleotid of loop
	int diff = outId - inId + 1;
	// diff -= getBranch(inBId).getOffset() - getBranch(outBId).getOffset(); // offset; // should be used, but not part of branch descriptor anymore
	// log.warning("Warning: no branch descriptor offset used for getLoopLength");
	if (diff < 0) {
	    log.severe("Warning: bad indices: " + inBId + " " + outBId + " strand: "
		       + strandId + " "
		       + getStrand(strandId));
	}
	assert diff >= 0;
	return diff;
    }
    
    /** returns n'th strand. */
    public NucleotideStrand getStrand(int n) { 
	// return (NucleotideStrand)(strands.get(n)); 
	int counter = -1;
	for (int i = 0; i < size(); ++i) {
	    if (getChild(i) instanceof NucleotideStrand) {
		++counter;
		if (counter == n) {
		    return (NucleotideStrand)(getChild(i));
		}
	    }
	}
	// int idx = getIndexOfChild(n, "NucleotideStrand3D"); // avoid, because if would not work with RnaStrand etc
	assert false; // no strand found!
	return null;
    }
    
    /** returns references to all strands. */
    public NucleotideStrand[] getStrands() { 
	int strandCount = getStrandCount();
	NucleotideStrand[] strands = new NucleotideStrand[strandCount];
	for (int i = 0; i < strandCount; ++i) {
	    strands[i] = getStrand(i);
	}
	return strands;
    }

    /** returns for n'th sequence the branch id for which it is the
     * incoming sequence.
     */
    public int getIncomingBranchId(int n) { return incomingBranchIds[n]; }

    /** returns for n'th sequence the branch id for which it is the
     * incoming sequence.
     */
    public int[] getIncomingBranchIds() { return incomingBranchIds; }

    /** returns for n'th sequence the branch id for which it is the
     * outgoing sequence.
     */
    public int getOutgoingBranchId(int n) { return outgoingBranchIds[n]; }

    /** returns for n'th sequence the branch id for which it is the
     * outgoing sequence.
     */
    public int[] getOutgoingBranchIds() { return outgoingBranchIds; }

    public boolean hasInternalHelices() { return internalHelices; }

    public void insertChild(Object3D obj) {
	super.insertChild(obj);
	if (obj instanceof BranchDescriptor3D) {
	    ++branchCount;
	    BranchDescriptor3D branch = (BranchDescriptor3D)obj;
	    addStrands(branch);
	}
    }

    /** use original method from super class */
    public void insertRawChild(Object3D obj) {
	super.insertChild(obj);
    }

    /** Returns true iff junction contains two strands as kissing loop */
    public boolean isKissingLoop() { return true; }

    public void removeChild(Object3D obj) {
	super.removeChild(obj);
	if (obj instanceof BranchDescriptor3D) {
	    --branchCount;
	    assert false; // implementation not complete yet
	}
    }

    /** returns true if a strand is going from 
     * incoming branchid to outgoing branchid.
     * True only if directionality correct.
     */
    public boolean isConnected(int incomingBranchId, int outgoingBranchId) { 
	return incomingSeqIds[incomingBranchId] == outgoingSeqIds[outgoingBranchId];
    }

    /** A kissing loop is regular if and only if getBranchCount() == getStrandCount() AND getBranchCount()==2 */
    public boolean isRegular() {
	return (getBranchCount() == getStrandCount()) && (getBranchCount() == 2);
    }

    /** checks if defined and consistent */
    public boolean isValid() {
	int branchCount = getBranchCount();
	int strandCount = getStrandCount();
	if ((branchCount != 2) || (strandCount != 2)) {
	    log.fine("Warning: BranchCount and StrandCount are not equal 2: " + getBranchCount() + " "
			       + getStrandCount());
	    return false;
	}
	for (int i = 0; i < branchCount; ++i) {
	    BranchDescriptor3D branch = getBranch(i);
	    if (!branch.isValid()) {
		return false;
	    }
	    if (!branch.isSingleSequence()) {
		return false;
	    }
	    // write branch descriptor
// 	    String branchPdb = toPdb(branch, i, writer);
// 	    result = result + branchPdb;
	    
	}
	return true;
    }

    /** rotates this object and all child nodes around center
     * rotation-axis and angle
     */
//     public void rotate(Vector3D center, 
// 		       Vector3D axis, double angle) {
// 	super.rotate(center, axis, angle);
// 	for (int i = 0; i < getStrandCount(); ++i) {
// 	    NucleotideStrand strand = getStrand(i);
// 	    strand.rotate(center, axis, angle);
// 	}	
// 	for (int i = 0; i < getBranchCount(); ++i) {
// 	    NucleotideStrand branch = getBranch(i);
// 	    branch.rotate(center, axis, angle);
// 	}	
//    }

    /** Translate object. */
//     public void translate(Vector3D vec) {
// 	super.translate(vec);
// 	for (int i = 0; i < getStrandCount(); ++i) {
// 	    NucleotideStrand strand = getStrand(i);
// 	    strand.translate(vec);
// 	}
// 	for (int i = 0; i < getBranchCount(); ++i) {
// 	    BranchDescriptor3D branch = getBranch(i);
// 	    branch.translate(vec);
// 	}
//     }

    /** returns index of link that links two branch descriptors between junctions. 
     * TODO : careful: assumes only one link leading from one junction to next!
     */
    public int findBranchConnectionLink(StrandJunction3D junction, LinkSet links) {
	int branchCount = getBranchCount();
	for (int i = 0; i < branchCount; ++i) {
	    BranchDescriptor3D branch1 = getBranch(i);
	    for (int j = 0; j < branchCount; ++j) {
		BranchDescriptor3D branch2 = junction.getBranch(j);
		for (int k = 0; k < links.size(); ++k) {
		    Link link = links.get(k);
		    if (link.isLinked(branch1, branch2)) {
			return k;
		    }
		}
	    }
	}
	return -1;
    }

    /** static method that returns true if and only if one strand of the stem belongs to loop of branch descriptor */
    public static boolean isLoopStem(BranchDescriptor3D branch1,
				     RnaStem3D stem) { 
	if (!branch1.isSingleSequence()) {
	    return false;
	}
	if ( branch1.getIncomingIndex() >= branch1.getOutgoingIndex()) {
	    return false; // no real loop
	}
	if ((branch1.getOutgoingIndex()-branch1.getIncomingIndex())  > KISSING_LOOP_SIZE_MAX) {
	    return false; // loop size too long!
	}
	Stem stemInfo = stem.getStemInfo();
	// check strand 1:
	NucleotideStrand strand1 = stem.getStrand1();
	NucleotideStrand strand11 = branch1.getIncomingStrand();
	// check both strands of given stem:
	if (strand11 == strand1) {
	    boolean found = false;
	    for (int i = 0; i < stemInfo.size(); ++i) {
		int startPos = stemInfo.getStartPos(i).getPos();
		if ((startPos > branch1.getIncomingIndex()) && (startPos < branch1.getOutgoingIndex())) {
		    return true;
		}
		else {
		    break; // do not keep checking
		}
	    }
	}
	strand1 = stem.getStrand2();
	if (strand11 != strand1) {
	    log.fine("No common strand found in isLoopStem!");
	    return false; // no common strand found
	}
	boolean found = false;
	for (int i = 0; i < stemInfo.size(); ++i) {
	    int stopPos = stemInfo.getStopPos(i).getPos();
	    if ((stopPos > branch1.getIncomingIndex()) && (stopPos < branch1.getOutgoingIndex())) {
		return true;
	    }
	    else {
		break; // do not keep checking
	    }
	}
	return false;
    }


    /** static method that returns true if and only if the 2 branch descriptors form a kissing loop */
    private static boolean isKissingLoopStem(BranchDescriptor3D branch1, BranchDescriptor3D branch2,
					    RnaStem3D stem) { 
	assert branch1.isSingleSequence() && branch2.isSingleSequence();
	// assert branch1.getIncomingStrand() != branch2.getIncomingStrand();
	assert branch1.getIncomingIndex() < branch1.getOutgoingIndex(); // real loop
	assert branch2.getIncomingIndex() < branch2.getOutgoingIndex(); // real loop
	return isLoopStem(branch1, stem) && isLoopStem(branch2, stem);
    }

    /** static method that returns true if and only if there are two residues of the two kissing loop strands 
     * that are extremely close in space. 
     */
    public static boolean hasCommonKissingLoopResidues(BranchDescriptor3D branch1, BranchDescriptor3D branch2) {
	assert branch1.isSingleSequence() && branch2.isSingleSequence();
	assert branch1.getIncomingIndex() < branch1.getOutgoingIndex();
	assert branch2.getIncomingIndex() < branch2.getOutgoingIndex();
	NucleotideStrand strand1 = branch1.getIncomingStrand(); // should be equal to outgoing strand
	NucleotideStrand strand2 = branch2.getIncomingStrand(); // should be equal to outgoing strand
	for (int pos1 = branch1.getIncomingIndex(); pos1 <= branch1.getOutgoingIndex(); ++pos1) {
	    Residue3D residue1 = strand1.getResidue3D(pos1);
	    for (int pos2 = branch2.getIncomingIndex(); pos2 <= branch2.getOutgoingIndex(); ++pos2) {
		Residue3D residue2 = strand2.getResidue3D(pos2);
		if (residue1.getPosition().distance(residue2.getPosition()) < DISTANCE_TOLERANCE) {
		    return true;
		}
	    }
	}
	return false;
    }

    /** static method that returns true if and only if the 2 branch descriptors form a kissing loop */
    public static boolean isKissingLoop(BranchDescriptor3D branch1, BranchDescriptor3D branch2,
					Object3DSet stemSet) { 
	boolean result = false;
	int len1 = branch1.getOutgoingIndex() - branch1.getIncomingIndex() + 1;
	int len2 = branch2.getOutgoingIndex() - branch2.getIncomingIndex() + 1;
	NucleotideStrand strand1 = branch1.getIncomingStrand();
	NucleotideStrand strand2 = branch2.getIncomingStrand();
	if ((branch1.isSingleSequence()) && (len1 > 1)
	    && (branch2.isSingleSequence()) && (len2 > 1)) {
	    if (hasCommonKissingLoopResidues(branch1, branch2)) {
		return false; // avoid overlapping residues
	    }
	     // (NucleotideStrand)(BioPolymerTools.generateSubSequence(branch1.getIncomingStrand(), branch1.getIncomingIndex(), 
	    // len1));
	    
	    // NucleotideStrand strand2 = branch2.getIncomingStrand(); // (NucleotideStrand)(BioPolymerTools.generateSubSequence(branch2.getIncomingStrand(), branch2.getIncomingIndex(), 
	    // len2));
// 	    Object3DSet stemSet = null;
// 	    if (strand1 == strand2) {
// 		stemSet = StemTools.generateStems(strand1, "kissingLoopStems");
// 	    }
// 	    else {
// 		stemSet = StemTools.generateStems(strand1, strand2, "kissingLoopStems");
// 	    }

 	    for (int i = 0; i < stemSet.size(); ++i) {
 		RnaStem3D stem = (RnaStem3D)(stemSet.get(i));
 		if (isKissingLoopStem(branch1, branch2, stem)) {
 		    // log.fine("Kissing loop stem found!!!!! " + stem.getStemInfo());
 		    result = true;
 		    break;
 		}
 	    }
	}
	return result;
    }

    private static Set<Integer> computeIntersection(Set<Integer> set1,
					      Set<Integer> set2) {
	Set<Integer> result = new HashSet<Integer>();
	Iterator<Integer> it = set1.iterator();
	while (it.hasNext()) {
	    Integer i = it.next();
	    if (set2.contains(i)) {
		result.add(i);
	    }
	    if ((result.size() == set1.size()) || (result.size() == set2.size())) {
		break; // can't be larger than any one set
	    }
	}
	return result;
    }

    /** static method that returns true if and only if the 2 branch descriptors form a kissing loop */
    public static boolean isKissingLoop(BranchDescriptor3D branch1, BranchDescriptor3D branch2,
					Object3DSet stemSet,
					Set<Integer> loopStemIndices1,
					Set<Integer> loopStemIndices2) { 
	int len1 = branch1.getOutgoingIndex() - branch1.getIncomingIndex() + 1;
	int len2 = branch2.getOutgoingIndex() - branch2.getIncomingIndex() + 1;
	NucleotideStrand strand1 = branch1.getIncomingStrand();
	NucleotideStrand strand2 = branch2.getIncomingStrand();
	if ((branch1.isSingleSequence()) && (len1 > 1)
	    && (branch2.isSingleSequence()) && (len2 > 1)) {
	    if (computeIntersection(loopStemIndices1, loopStemIndices2).size() == 0) {
		return false;
	    }
	    if (hasCommonKissingLoopResidues(branch1, branch2)) {
		return false; // avoid overlapping residues
	    }
	    // (NucleotideStrand)(BioPolymerTools.generateSubSequence(branch1.getIncomingStrand(), branch1.getIncomingIndex(), 
	    // len1));	    
	    // NucleotideStrand strand2 = branch2.getIncomingStrand(); // (NucleotideStrand)(BioPolymerTools.generateSubSequence(branch2.getIncomingStrand(), branch2.getIncomingIndex(), 
	    // len2));
// 	    Object3DSet stemSet = null;
// 	    if (strand1 == strand2) {
// 		stemSet = StemTools.generateStems(strand1, "kissingLoopStems");
// 	    }
// 	    else {
// 		stemSet = StemTools.generateStems(strand1, strand2, "kissingLoopStems");
// 	    }
// 	    log.fine("Checking individual stems for being in the kissing loop.");
// 	    for (int i = 0; i < stemSet.size(); ++i) {
// 		RnaStem3D stem = (RnaStem3D)(stemSet.get(i));
// 		if (isKissingLoopStem(branch1, branch2, stem)) {
// 		    log.fine("Kissing loop stem found!!!!! " + stem.getStemInfo());
// 		    result = true;
// 		    break;
// 		}
// 	    }
	}
	return true;
    }


    public void setInternalHelices(boolean flag) { this.internalHelices = flag; }
    
}
