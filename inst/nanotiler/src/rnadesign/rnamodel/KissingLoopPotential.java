package rnadesign.rnamodel;

import tools3d.objects3d.*;

import java.util.logging.Logger;

/** assigns score how well a kissing loop is located in between 
 * two stems */
public class KissingLoopPotential implements Object3DPotential {

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    private BranchDescriptorPotential branch1Potential;
    private BranchDescriptorPotential branch2Potential;

    public KissingLoopPotential(BranchDescriptor3D branch1,
				BranchDescriptor3D branch2) {
	this.branch1Potential = new BranchDescriptorPotential(branch1);
	this.branch2Potential = new BranchDescriptorPotential(branch2);
    }

    public double computeValue(Object3D obj) {
	assert obj instanceof KissingLoop3D;
	KissingLoop3D kissingLoop = (KissingLoop3D)(obj);
	assert kissingLoop.getBranchCount() == 2;
	BranchDescriptor3D b1 = kissingLoop.getBranch(0);
	BranchDescriptor3D b2 = kissingLoop.getBranch(1);
	double d= branch1Potential.getBranch().distance(branch2Potential.getBranch());
	double dobj = b1.distance(b2);
	double distdist = 0.5 * Math.abs(d - dobj); // average minimum possible distance
	if (distdist == 0.0) {
	    log.warning("kissing loop too large!");
	    distdist = 0.001; 
	}
	double d11 = branch1Potential.getBranch().distance(b1);
	double d21 = branch2Potential.getBranch().distance(b2);

	// try other way around:
	double d12 = branch1Potential.getBranch().distance(b1);
	double d22 = branch2Potential.getBranch().distance(b2);

	// double dd = d1 - d2 / (0.5* (d1 + d2)); // difference between branch descriptor distances
	// dd = dd * dd; // try to keep distances even
 	double term1a=  0.5 * (branch1Potential.computeValue(kissingLoop.getBranch(0))
 	    + branch2Potential.computeValue(kissingLoop.getBranch(1))); //  + dd + (d1*d1) + (d2*d2);
 	double term2a=  0.5 * (branch1Potential.computeValue(kissingLoop.getBranch(1))
 	    + branch2Potential.computeValue(kissingLoop.getBranch(0))); //  + dd + (d1*d1) + (d2*d2);
	double term1 = Math.abs(Math.sqrt(0.5 * ((d11 * d11) + (d21 * d21)))-distdist) / distdist; // fraction
	double term2 = Math.abs(Math.sqrt(0.5 * ((d12 * d12) + (d22 * d22)))-distdist) / distdist;
	double result = term1 + term1a;
	if ((term2 + term2a) < result) {
	    result = term2 + term2a; // choose best assignment
	}
	return result;
    }

}
