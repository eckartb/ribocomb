package rnadesign.rnamodel;

import tools3d.*;
import tools3d.objects3d.*;
import generaltools.StringTools;
import generaltools.Randomizer;
import sequence.*;
import java.util.Properties;
import java.util.Random;
import java.util.logging.*;
import static rnadesign.rnamodel.RnaConstants.*;

/** static methods for connecting two junctions */
public class KissingLoopTools {

    public static final String NEWLINE = System.getProperty("line.separator");
    
    private static Logger log = Logger.getLogger("NanoTiler_debug");

    private static Random rnd = Randomizer.getInstance();

    /** returns "optimal" number of base pairs to bridge branch1 and branch2 */
    private static int computeBestStemLength(BranchDescriptor3D branch1, 
					     BranchDescriptor3D branch2) {
// 	double dist = branch1.getPosition().distance(branch2.getPosition());
// 	// first and last position are taken up by branch descriptor
// 	int numPairs = (int)((dist/HELIX_RISE + 0.5)) -1;
// 	return numPairs;
	return ConnectJunctionTools.computeBestStemLength(branch1, branch2);
    }

    /** optimizes position of kissing loop so that it is inbetween branch1 and branch2 */
    private static double optimizeKissingLoopPosition(BranchDescriptor3D branch1,
						      BranchDescriptor3D branch2,
						      KissingLoop3D kissingLoop) {
	// log.fine("Starting KissingLoopTools.optimizeKissingLoopPosition !");
	kissingLoop.setPosition(Vector3D.average(branch1.getPosition(), branch2.getPosition()));
	BrownianMotionOptimizer optimizer = new BrownianMotionOptimizer();
	optimizer.setIterMax(2000);
	optimizer.setTransStep(0.0); // no translation anymore
	KissingLoopPotential potential = new KissingLoopPotential(branch1, branch2);
	double result = optimizer.optimize(kissingLoop, potential);
	// log.fine("Finished KissingLoopTools.optimizeKissingLoopPosition: " + result);
	return result; // return optimization score
    }

    /** returns v1, v2, average and orthogonal vector */
    private static Vector3D[] generate4DVectorsFromPair(Vector3D v1, Vector3D v2) {
	Vector3D dv = v2.minus(v1);
	assert(dv.length() > 0);
	Vector3D[] result = new Vector3D[4];


	result[2] = Vector3D.average(v1, v2);
	result[0] = v1.minus(result[2]);
	result[0].normalize();
	result[0].add(result[2]);

	result[1] = v2.minus(result[2]);
	result[1].normalize();
	result[1].add(result[2]);
	while (result[3] == null) {
	    Vector3D rv = new Vector3D(rnd.nextDouble(), rnd.nextDouble(), rnd.nextDouble());
	    result[3] = rv.cross(dv);
	    if (result[3].lengthSquare() == 0.0) {
		result[3] = null;
	    }
	    else {
		result[3].normalize();
		result[3].add(result[2]); // orthogonal vector has length 1
	    }
	}
	return result;
    }

    /** returns v1, v2, average and orthogonal vector */
    private static Vector3D[] generate3DVectorsFromPair(Vector3D v1, Vector3D v2) {
	Vector3D dv = v2.minus(v1);
	assert(dv.length() > 0);
	Vector3D[] result = new Vector3D[3];

	result[2] = Vector3D.average(v1, v2);
	result[0] = v1.minus(result[2]);
	result[0].normalize();
	result[0].add(result[2]);

	while (result[1] == null) {
	    Vector3D rv = new Vector3D(rnd.nextDouble(), rnd.nextDouble(), rnd.nextDouble());
	    result[1] = rv.cross(dv);
	    if (result[1].lengthSquare() == 0.0) {
		result[1] = null;
	    }
	    else {
		result[1].normalize();
		result[1].add(result[2]); // orthogonal vector has length 1
	    }
	}
	return result;
    }

    /** optimizes position of kissing loop so that it is inbetween branch1 and branch2 */
    private static double optimizeKissingLoopPosition2(BranchDescriptor3D branch1,
						       BranchDescriptor3D branch2,
						       KissingLoop3D kissingLoop) {
	log.finest("Starting KissingLoopTools.optimizeKissingLoopPosition ! " 
			   + branch1.getPosition() + " " + branch2.getPosition() + " "
			   + kissingLoop.getBranch(0).getPosition() + " " + kissingLoop.getBranch(1).getPosition());;
	Vector3D[] given = generate3DVectorsFromPair(branch1.getPosition(), branch2.getPosition());
	Vector3D[] fitting1 = generate3DVectorsFromPair(kissingLoop.getBranch(0).getPosition(),
						       kissingLoop.getBranch(1).getPosition());
	Vector3D[] fitting2 = generate3DVectorsFromPair(kissingLoop.getBranch(1).getPosition(),
							kissingLoop.getBranch(0).getPosition());
	Superpose superposer = new MinSuperpose();
	SuperpositionResult result1 = superposer.superpose(given, fitting1);
	SuperpositionResult result2 = superposer.superpose(given, fitting2);
	SuperpositionResult result = result1;
	if (result1.getRms() > result2.getRms()) {
	    result = result2; // chose other connection
	}
	result.applyTransformation(kissingLoop);
	log.finest("Ending KissingLoopTools.optimizeKissingLoopPosition ! " 
			   + branch1.getPosition() + " " + branch2.getPosition() + " "
			   + kissingLoop.getBranch(0).getPosition() + " " + kissingLoop.getBranch(1).getPosition());;
	return result.getRms(); // return optimization score
    }

    /** computes position of kissing loop so that it is inbetween branch1 and branch2 */
    private static double optimizeKissingLoopPosition3(BranchDescriptor3D branch1,
						       BranchDescriptor3D branch2,
						       KissingLoop3D kissingLoop,
						       double axialAngle) {
	Vector3D klCenter = Vector3D.average(kissingLoop.getBranch(0).getPosition(), kissingLoop.getBranch(1).getPosition());
	kissingLoop.setIsolatedPosition(klCenter);
	kissingLoop.setPosition(Vector3D.average(branch1.getPosition(), branch2.getPosition()));
	Vector3D dv = branch2.getPosition().minus(branch1.getPosition());
	Vector3D dvk = kissingLoop.getBranch(1).getPosition().minus(kissingLoop.getBranch(0).getPosition());
	double angle = dv.angle(dvk);
	if (angle == 0.0) {
	    return 0.0; // nothing left to do!
	}
	Vector3D cv = dv.cross(dvk);
	if (cv.lengthSquare() == 0.0) {
	    return 0.0;
	}
	cv.normalize();
	Matrix3D rotationMatrix = Matrix3D.rotationMatrix(cv, -angle);
	Matrix3D rotationMatrix2 = Matrix3D.rotationMatrix(dv, axialAngle);
	kissingLoop.rotate(cv, -angle); // klCenter, rotationMatrix); // cv, -angle); // rotate back to angle zero
	kissingLoop.rotate(dv, axialAngle); // klCenter, rotationMatrix2); // dv, axialAngle);
	return 0;
    }

    /** central method for generating stem that connects two
     * BranchDescriptors.
     * TODO : overlap at first base pair possible. Fix!
     * @param connectionAlgorithm : algorithm used by ConnectJunctionTools for interpolating stems
     */
    public static Object3DLinkSetBundle generateConnectingStem(BranchDescriptor3D branch1, 
							       BranchDescriptor3D branch2,
							       char c1,
							       char c2,
							       String baseName,
							       FitParameters fitParameters,
							       Object3D nucleotideDB,
							       StrandJunctionDB kissingLoopDB,
							       int connectionAlgorithm,
							       int axialSteps
							       ) {
	assert branch1 != branch2;
	assert branch1.isValid();
	assert branch2.isValid();
	assert axialSteps > 0;
	log.fine("Starting KissingLoopTools.generateConnectingStem: " 
		 + branch1.getName() + " ; " 
		 + branch2.getName() + " ; " + baseName);
	if ((kissingLoopDB == null) || (kissingLoopDB.size() == 0)) {
	    return null;
	}
	double angleError = Math.PI - branch1.getDirection().angle(branch2.getDirection());
	assert (angleError >= 0.0);
	if (angleError > fitParameters.getAngleLimit()) {
	    return null;
	}
	Vector3D avgPos = Vector3D.average(branch1.getPosition(), branch2.getPosition());
	assert branch1.isValid();
	assert branch2.isValid();
	Vector3D savedDirection1 = branch1.getDirection();
	Vector3D stemDir = branch2.getBasePosition().minus(branch1.getBasePosition());
	assert stemDir.lengthSquare() > 0.0;
	stemDir.normalize();
	//commented out by CV - still used by FragmentGridTiler: generateConnectingStems//	assert false; // TODO : not valid code anymode; find replacement for setDirection if necessary
	// branch1.setDirection(stemDir);
	assert branch1.isValid();
	assert branch2.isValid();
	Vector3D basePos = branch1.getBasePosition();
	int numPairs = computeBestStemLength(branch1, branch2);
	// generate 2 new strands:
	String s1 = "";
	String s2 = "";
	assert branch1.isValid();
	assert branch2.isValid();
	s1 = StringTools.stringFromChar('G', numPairs); // TODO : should be unspecified ("N")
	s2 = StringTools.stringFromChar('C', numPairs);
	String strand1Name = baseName + "_" + "forw";
	String strand2Name = baseName + "_" + "back";
	String stemName = baseName + "_stem";
	Object3D resultTree = new SimpleObject3D();
	String setName = baseName + "_root";
	resultTree.setName(setName);
	resultTree.setPosition(avgPos);
	LinkSet resultLinks = new SimpleLinkSet();
	StrandJunction3D[] kissingLoops = kissingLoopDB.getJunctions(2);
	if ((kissingLoops == null) || (kissingLoops.length == 0)) {
	    return null;
	}

	// Object3DTools.balanceTree(kissingLoop);
	// kissingLoop.setPosition(avgPos);

	double bestFitScore = 1e30;
	boolean stem1Found = false;
	boolean stem2Found = false;
	boolean reverseMode = false; 
	boolean reverseMode2 = false;
	Object3DLinkSetBundle bestStem1Bundle = null;
	Object3DLinkSetBundle bestStem2Bundle = null;
	KissingLoop3D kissingLoop = null;
	for (int i = 0; i < axialSteps; ++i) {
	    double axialAngle = i * (2.0 * Math.PI) / axialSteps;
	    log.fine("Working on kissing loop axial angle: " + (180.0*axialAngle/Math.PI));
	    kissingLoop = (KissingLoop3D)(kissingLoops[0].cloneDeep());
	    optimizeKissingLoopPosition3(branch1, branch2, kissingLoop, axialAngle);
	    if (i == 0) {
		reverseMode = branch1.distance(kissingLoop.getBranch(0)) < branch1.distance(kissingLoop.getBranch(1)) ? false : true;
		reverseMode2 = branch2.distance(kissingLoop.getBranch(1)) < branch2.distance(kissingLoop.getBranch(0)) ? false : true;
	    }
	    // TODO : not cleanly programmed, but reverseMode should not vary with iteration
	    
	    // add interpolating stems to fit kissing loop:

	    Object3DLinkSetBundle stem1Bundle;
	    RnaStrand strand;
	    int[] seqIds = kissingLoop.getIncomingSeqIds();
	    double totFitScore = 0.0;
	    assert !kissingLoop.getBranch(0).isProbablyEqual(kissingLoop.getBranch(1));
	    try {
		if (!reverseMode) {
		    stem1Bundle =  ConnectJunctionTools.generateConnectingStem(branch1, 
									       kissingLoop.getBranch(0),
									       c1, c2,
									       baseName + "_is1",
									       fitParameters,
									       nucleotideDB,
									       connectionAlgorithm, -1);
		}
		else {
		    stem1Bundle =  ConnectJunctionTools.generateConnectingStem(branch1, 
									       kissingLoop.getBranch(1), c1, c2,
									       baseName + "_is1",
									       fitParameters,
									       nucleotideDB,
									       connectionAlgorithm, -1);
		}
	    }
	    catch (RuntimeException e) {
		log.severe(NEWLINE + NEWLINE + "KissingLoopTools Exception: " + e.getMessage()); stem1Bundle = null; 
	    }
	    double totFitScoreTerm1 = 1e30;
	    double totFitScoreTerm2 = 1e30;
	    if (stem1Bundle != null) {
		Object3DSet stemSet = Object3DTools.collectByClassName(stem1Bundle.getObject3D(), "RnaStem3D");
		assert stemSet.size() == 1;
		stem1Found = true;
		RnaStem3D stem = (RnaStem3D)(stemSet.get(0));
		log.fine("Kissing loop stem 1 found: " + stem);
		Properties properties = stem.getProperties();
		assert properties != null;
		String optScore = properties.getProperty("fit_score");
		assert optScore != null;
		log.fine("fit_score 1: " + optScore);
		totFitScoreTerm1 = 0.5*Double.parseDouble(optScore);
		// all residue get "adhoc" property because sequence is not optimized yet 
		Object3DTools.setRecursiveProperty(stem.getStrand1(), SequenceStatus.name, SequenceStatus.adhoc, "Nucleotide3D");
		Object3DTools.setRecursiveProperty(stem.getStrand2(), SequenceStatus.name, SequenceStatus.adhoc, "Nucleotide3D");
	    }
	    else {
		log.info("Stem1Bundle was null!");
	    }

	    Object3DLinkSetBundle stem2Bundle;
	    if (!reverseMode2) {
		stem2Bundle =  ConnectJunctionTools.generateConnectingStem(branch2, 
									   kissingLoop.getBranch(1), c1, c2,
									   baseName + "_is2",
									   fitParameters,
									   nucleotideDB,
									   connectionAlgorithm, -1);
	    }
	    else {
		stem2Bundle =  ConnectJunctionTools.generateConnectingStem(branch2, 
									   kissingLoop.getBranch(0), c1, c2,
									   baseName + "_is2",
									   fitParameters,
									   nucleotideDB,
									   connectionAlgorithm, -1);
	    }
	    if (stem2Bundle != null) {
		Object3DSet stemSet = Object3DTools.collectByClassName(stem2Bundle.getObject3D(), "RnaStem3D");
		assert stemSet.size() == 1;
		stem2Found = true;
		RnaStem3D stem = (RnaStem3D)(stemSet.get(0));
		log.fine("Kissing loop stem 2 found: " + stem);
		Properties properties = stem.getProperties();
		assert properties != null;
		String optScore = properties.getProperty("fit_score");
		assert optScore != null;
		log.fine("fit_score 2: " + optScore);
		totFitScoreTerm2 = 0.5*Double.parseDouble(optScore);
		// all residue get "adhoc" property because sequence is not optimized yet 
		Object3DTools.setRecursiveProperty(stem.getStrand1(), SequenceStatus.name, SequenceStatus.adhoc, "Nucleotide3D");
		Object3DTools.setRecursiveProperty(stem.getStrand2(), SequenceStatus.name, SequenceStatus.adhoc, "Nucleotide3D");
	    }
	    else {
		log.info("Stem2Bundle was null!");
	    }
	    totFitScore = totFitScoreTerm1 + totFitScoreTerm2; // add fitting scores of two stems
	    log.fine("Total fitting score of this axial angle: " + totFitScore);
	    if ((i == 0) || (totFitScore < bestFitScore)) {
		log.info("Saving so far best stem / kissing loop solution!");
		bestFitScore = totFitScore;
		bestStem1Bundle = stem1Bundle;
		bestStem2Bundle = stem2Bundle;
	    }

	} // loop over axialStep
	if (!stem1Found) {
	    log.warning("No solution for stem 1 connecting to kissing loop found!");
	}
	if (!stem2Found) {
	    log.warning("No solution for stem 2 connecting to kissing loop found!");
	}
	log.info("Best fitting score found: " + bestFitScore);

	assert kissingLoop != null;
	// assert bestStem1Bundle != null;
	// assert bestStem2Bundle != null;

	if (bestStem1Bundle != null) {
	    Object3DSet stemSet = Object3DTools.collectByClassName(bestStem1Bundle.getObject3D(), "RnaStem3D");
	    assert stemSet.size() == 1;
	    RnaStem3D stem = (RnaStem3D)(stemSet.get(0));
	    RnaStrand strand = (RnaStrand)(branch1.getOutgoingStrand());
	    strand.append(stem.getStrand1());
	    if (!reverseMode) {
		strand.append(kissingLoop.getBranch(0).getIncomingStrand());
	    }
	    else {
		strand.append(kissingLoop.getBranch(1).getIncomingStrand());
	    }
	    strand.append(stem.getStrand2());
	    resultLinks.merge(bestStem1Bundle.getLinks());
	}
	else {
	    log.info("bestStem1Bundle was null!");
	    // assert !stem1Found;
	}
	if (bestStem2Bundle != null) {
	    Object3DSet stemSet = Object3DTools.collectByClassName(bestStem2Bundle.getObject3D(), "RnaStem3D");
	    assert stemSet.size() == 1;
	    RnaStem3D stem = (RnaStem3D)(stemSet.get(0));
	    RnaStrand strand = (RnaStrand)(branch2.getOutgoingStrand());
	    strand.append(stem.getStrand1());
	    if (!reverseMode2) {
		strand.append(kissingLoop.getBranch(1).getIncomingStrand());
	    }
	    else {
		strand.append(kissingLoop.getBranch(0).getIncomingStrand());
	    }
	    strand.append(stem.getStrand2());
	    resultLinks.merge(bestStem2Bundle.getLinks());
	}
	else {
	    log.info("bestStem2Bundle was null!");
	    // assert !stem2Found;
	}

	return new SimpleObject3DLinkSetBundle(resultTree, resultLinks);
    }

}
