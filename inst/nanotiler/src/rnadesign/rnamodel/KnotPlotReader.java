
/** Factory class is responsible for reading/parsing a data file and creating
 *  a corresponding Object3DGraph
 */
package rnadesign.rnamodel;

import java.awt.Color;
import java.io.*;

import sequence.*;
import tools3d.Vector3D;
import tools3d.objects3d.*;
import java.util.logging.*;

/**
 * @author Eckart Bindewald
 *
 */
public class KnotPlotReader implements Object3DFactory {

    private static Logger log = Logger.getLogger("NanoTiler_debug");	

    private int formatMode = Object3DFormats.LISP_FORMAT;

    /** Constructor needs a buffered reader (provides methods for linewise reading) */
    public KnotPlotReader() {
    }
    
    /** reads object and links. TODO : Not yet implemented! */
    public Object3DLinkSetBundle readBundle(InputStream is) throws Object3DIOException {
	Object3D obj = readAnyObject3D(is);
	// add links:
	LinkSet links = new SimpleLinkSet();
	for (int i = 1; i < obj.size(); ++i) {
	    links.add(new SimpleLink(obj.getChild(i-1), obj.getChild(i)));
	}
	if (obj.size() > 2) {
	    links.add(new SimpleLink(obj.getChild(0), obj.getChild(obj.size()-1)));
	}
	return new SimpleObject3DLinkSetBundle(obj, links);
    }

    /* (non-Javadoc)
     * @see rnamodel.Object3DGraphFactory#createObject3DGraph()
     */
    public Object3D readAnyObject3D(InputStream is) throws Object3DIOException {
	log.fine("Starting readAnyObject3D!");
	DataInputStream dis = new DataInputStream(is);
	Object3D root = new SimpleObject3D();
	root.setName("KnotPlotImport");
	int i = 0;
	while (i < 1000000) {
	    try {
		Vector3D pos = readVector3D(dis);
		Object3D child = new SimpleObject3D();
		child.setPosition(pos);
		child.setName("p" + i);
		i++;
		root.insertChild(child);
	    }
	    catch (Object3DIOException e) {
		break;
	    }
	}

	return root;
    }
    

    /** reads and creates Vector3D in format (Vector3D x y z ) */
    public static Vector3D readVector3D(DataInputStream dis) throws Object3DIOException {
	log.fine("starting readVector!");	
	String word = readWord(dis);
	double x = 0;
	double y = 0;
	double z = 0;
	try {
	    x = Double.parseDouble(word);
	}
	catch (NumberFormatException e) {
	    throw new Object3DIOException("Could not parse x coordinate for Vector3D " + word);
	}
	word = readWord(dis);
	try {
	    y = Double.parseDouble(word);
	}
	catch (NumberFormatException e) {
	    throw new Object3DIOException("Could not parse y coordinate for Vector3D " + word);
	}	
	word = readWord(dis);
	try {
	    z = Double.parseDouble(word);
	}
	catch (NumberFormatException e) {
	    throw new Object3DIOException("Could not parse z coordinate for Vector3D " + word);
	}
	log.fine("ending readVector!");
	return new Vector3D(x, y, z);	
    }

    /** reads a single word from data stream */
    public static String readWord(DataInputStream dis) {
	String s = new String("");
	char c = ' ';
	
	// first skip white space
	do {
			try {
			   c = (char)dis.readByte();
			}
			catch (IOException e) {
				break; // end of file reached
			}
		}
		while (Character.isWhitespace(c));
		
		if (!Character.isWhitespace(c)) {
			s = s + c;
		}
		else {
			log.fine("found word: " + s);
			return s;
		}
		
		while (true) {
			try {
			   c = (char)dis.readByte();
			 if (!Character.isWhitespace(c)) {
				 s = s + c;
			 }
			 else {
				 break;
			 }
			}
			catch (IOException e) {
				break; // end of file reached
			}
		}
		log.fine("found word: " + s);
		return s;
	}

}
