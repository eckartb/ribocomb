package rnadesign.rnamodel;

import rnasecondary.*;
import numerictools.IntegerTools;
import numerictools.IntegerArrayTools;
import tools3d.objects3d.*;
import java.util.*;

/** Creates Base pair classification according to 12 Leontis-Westhof classes. */
public class LWBasePairClassifier implements  BasePairClassifier {

    HydrogenBondFinder bondFinder = new SimpleHydrogenBondFinder();

    public LWBasePairClassifier() {
    }

    public BasePairInteractionType classifyBasePair(Nucleotide3D n1, Nucleotide3D n2) throws RnaModelException {
	LinkSet hBonds = bondFinder.find(n1, n2);
	BasePairInteractionType interaction = classifyBasePair(n1, n2, hBonds);
	return interaction;
    }

    /** Returns true of false according to whether nucleotides glycosylic bond is in cis or trans sensu Leontis/Westhof */
    public boolean isCis(Nucleotide3D n1, Nucleotide3D n2) throws RnaModelException {
	double angle = NucleotideTools.computeGlycosilicBondAngle(n1, n2);
	double cutoff = Math.PI/2.0;
	boolean result = false;
	if (angle < cutoff) {
	    result = true;
	}
	// System.out.println("The angle between the glycosilic bonds of " + n1.getFullName() + " and " + n2.getFullName() + " is " + angle * (360.0/(2*Math.PI)) + " cis: " + result);
	return result;
    }

    private BasePairInteractionType classifyBasePair(Nucleotide3D n1, Nucleotide3D n2, LinkSet hBonds) throws RnaModelException {
	// System.out.println("LWBasePairClassifier.classifyBasePair: " + hBonds.size() + " hydrogen bonds detected between " + n1.getFullName() + " and " + n2.getFullName());
	int[] edgeVotes1 = new int[3];
	int[] edgeVotes2 = new int[3];// careful: assumes constants are in certain range
	char c1 = n1.getSymbol().getCharacter(); // a 'G' for a "G" nucleotide (getName() would have return "G10" for one-based position 10 
	char c2 = n2.getSymbol().getCharacter(); // a 'G' for a "G" nucleotide (getName() would have return "G10" for one-based position 10 
	for (int i = 0; i < hBonds.size(); ++i) {
	    // System.out.println("Analyzing detected hydrogen bond: " + hBonds.get(i).toString());
	    Link hBondOrig = hBonds.get(i);
	    Link hBond = hBondOrig;
	    if ((hBond.getObj1().getParent() == n2) && (hBond.getObj2().getParent() == n1)) {
		hBond = new SimpleLink(hBond.getObj2(), hBond.getObj1()); // flip order
	    }
	    // System.out.println("Analyzing bond: " + hBond);
	    if (! ((hBond.getObj1().getParent() == n1) && (hBond.getObj2().getParent() == n2)))  {
		System.out.println("Strange: hydrogen bond does not match given nucleotides. Skipping.");
		continue;
	    }
	    for (int j = LWEdge.WATSON_CRICK; j <= LWEdge.SUGAR; ++j) {
		int jj = j - LWEdge.WATSON_CRICK;
		switch (j) {
		case LWEdge.WATSON_CRICK:
 		    if (NucleotideTools.isWatsonCrickEdge(c1, hBond.getObj1().getName())) {
			// System.out.println("Atom " + hBond.getObj1().getName() + " is part of W edge in first nucleotide " + n1.getFullName());
			edgeVotes1[jj]++;
		    } else {
			// System.out.println("Atom " + hBond.getObj1().getName() + " is NOT part of W edge in first nucleotide " + n1.getFullName());
		    }
 		    if (NucleotideTools.isWatsonCrickEdge(c2, hBond.getObj2().getName())) {
			// System.out.println("Atom " + hBond.getObj2().getName() + " is part of W edge in second nucleotide " + n2.getFullName());
			edgeVotes2[jj]++;
		    } else {
			// System.out.println("Atom " + hBond.getObj2().getName() + " is NOT part of W edge in second nucleotide " + n2.getFullName());
		    }
		    break;
		case LWEdge.HOOGSTEEN:
 		    if (NucleotideTools.isHoogsteenEdge(c1, hBond.getObj1().getName())) {
			// System.out.println("Atom " + hBond.getObj1().getName() + " is part of H edge in first nucleotide " + n1.getFullName());
			edgeVotes1[jj]++;
		    } else {
			// System.out.println("Atom " + hBond.getObj1().getName() + " is NOT part of H edge in first nucleotide " + n1.getFullName());
		    }
 		    if (NucleotideTools.isHoogsteenEdge(c2, hBond.getObj2().getName())) {
			// System.out.println("Atom " + hBond.getObj2().getName() + " is part of H edge in second nucleotide " + n2.getFullName());
			edgeVotes2[jj]++;
		    } else {
			// System.out.println("Atom " + hBond.getObj2().getName() + " is NOT part of H edge in second nucleotide " + n2.getFullName());
		    }
		    break;
		case LWEdge.SUGAR:
 		    if (NucleotideTools.isSugarEdge(c1, hBond.getObj1().getName())) {
			// System.out.println("Atom " + hBond.getObj1().getName() + " is part of S edge in first nucleotide " + n1.getFullName());
			edgeVotes1[jj]++;
		    } else {
			// System.out.println("Atom " + hBond.getObj1().getName() + " is NOT part of S edge in first nucleotide " + n1.getFullName());
		    }
 		    if (NucleotideTools.isSugarEdge(c2, hBond.getObj2().getName())) {
			// System.out.println("Atom " + hBond.getObj2().getName() + " is part of S edge in second nucleotide " + n2.getFullName());
			edgeVotes2[jj]++;
		    } else {
			// System.out.println("Atom " + hBond.getObj2().getName() + " is NOT part of S edge in second nucleotide " + n2.getFullName());
		    }
		    break;
		default:
		    System.out.println("Cannot classify edge for hydrogen bond: " + hBond);
		    assert(false);
		}

	    }
	}
	// System.out.println("HB Constants: " + LWEdge.WATSON_CRICK + " " + LWEdge.HOOGSTEEN + " " + LWEdge.SUGAR);
	// System.out.println("Voting results:");
	// System.out.print("First base: ");
	// IntegerArrayTools.writeArray(System.out, edgeVotes1);
	// System.out.print("Second base: ");
	// IntegerArrayTools.writeArray(System.out, edgeVotes2);
	int[] maxIds1 = IntegerTools.whichMax(edgeVotes1);
	int[] maxIds2 = IntegerTools.whichMax(edgeVotes2);
	if (edgeVotes1[maxIds1[0]] < 2 || edgeVotes2[maxIds2[0]] < 2) {
	    return null; // less than two hydrogen bonds
	}
	boolean cis = isCis(n1, n2);
	int edge1 = maxIds1[0] + LWEdge.WATSON_CRICK;
	int edge2 = maxIds2[0] + LWEdge.WATSON_CRICK;
	
	// System.out.println("Creating interaction with ids " + maxIds1[0] + " " + maxIds2[0] + " " + edge1 + " " + edge2 + " c/t: " + cis);
	assert  LWEdge.WATSON_CRICK == 1;
	return new BasePairInteractionType(edge1, edge2, cis);
    }
    
    public LinkSet classifyBasePairs(Object3DSet bases) throws RnaModelException {
	LinkSet result = new SimpleLinkSet();
	for (int i = 0; i < bases.size(); ++i) {
	    Nucleotide3D n1 = (Nucleotide3D)(bases.get(i));
	    for (int j = i+1; j < bases.size(); ++j) {
		Nucleotide3D n2 = (Nucleotide3D)(bases.get(j));
		if (n1 == n2) {
		    continue;
		}
		if (NucleotideTools.isAdjacentAssigned(n1, n2)) {
		    // System.out.println("Skipping adjacent nucleotides: " + n1.getFullName() + " " + n2.getFullName());
		    continue; // skip cases of same-strand adjacent nucleotides (ignore stacking interactions)
		}
		BasePairInteractionType bp = classifyBasePair((Nucleotide3D)bases.get(i), (Nucleotide3D)bases.get(j));
		if (bp != null) {
		    Interaction interaction = new SimpleInteraction((Nucleotide3D)bases.get(i), (Nucleotide3D)bases.get(j), bp);
		    Link link = new InteractionLinkImp(bases.get(i), bases.get(j), interaction);
		    result.add(link);
		}
	    }
	    
	}
	return result;
    }
	

}