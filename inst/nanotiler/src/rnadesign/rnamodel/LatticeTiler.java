package rnadesign.rnamodel;

import tools3d.*;
import tools3d.objects3d.*;
import symmetry.*;

public interface LatticeTiler {

    public Object3DLinkSetBundle generateTiling(Object3DLinkSetBundle unitCell,
						Lattice lattice,
						LatticeSection section,
						String name);

}
