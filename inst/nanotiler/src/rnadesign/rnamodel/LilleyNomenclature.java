package rnadesign.rnamodel;

import java.util.Arrays;
import graphtools.IntegerList;
import java.util.logging.*;
import static rnadesign.rnamodel.PackageConstants.*;

public class LilleyNomenclature implements StrandJunctionNomenclature {

    private boolean loopMode = true;
    
    private static Logger log = Logger.getLogger(LOGGER_NAME);

    public LilleyNomenclature() {

    }
    
    public boolean isLoopMode() { return this.loopMode; }

    /** returns sum of loop lengths */
    public static int computeLoopLengthSum(StrandJunction3D junction) {
	int sum = 0;
	for (int i = 0; i < junction.getStrandCount(); ++i) {
	    sum += junction.getLoopLength(i);
	}
	return sum;
    }

    private int compareTo(int[] ary1, int[] ary2) {
	assert ary1.length == ary2.length;
	for (int i = 0; i < ary1.length; ++i) {
	    if (ary1[i] < ary2[i]) {
		return -1;
	    }
	    else if (ary2[i] < ary1[i]) {
		return +1;
	    }

	}
	return 0; 
    }
    
    /** like lexigraphical comparison: rotate loop length such that smallest loops are first */
    private int[] rotateToSmallest(int[] loopLengths) {
	IntegerList list = new IntegerList(loopLengths);
	IntegerList bestList = new IntegerList(loopLengths);
	for (int i = 1; i < list.size(); ++i) {
	    IntegerList newList = list.rotate(i);
	    if (newList.compareTo(bestList) < 0) {
		bestList = newList;
	    }
	}
	return bestList.toArray();
    }

    /** returns next strand following connectivity
     * BUGGY*/
    private int getConsecutiveStrand(StrandJunction3D junction, int strandId) {
	log.warning("Untrustworthy method used: LilleyNomenClature.getConsecutiveStrand");
	if (junction.isKissingLoop()) {
	    ++strandId;
	    if (strandId >= junction.getStrandCount()) {
		strandId = 0;
	    }
	    return strandId;
	}
	int outBranchId = junction.getOutgoingBranchId(strandId);
	assert outBranchId < junction.getBranchCount();
	int[] incomingSeqIds = junction.getIncomingSeqIds();
	int result = incomingSeqIds[outBranchId];
	assert result < junction.getStrandCount();
	return result;
    }

    /** returns next strand following connectivity.
     * TODO : not very elegant, slow
     */
    private int getConsecutiveBranchDescriptor(StrandJunction3D junction, int branchId) {
	if (junction.isKissingLoop()) {
	    ++branchId;
	    if (branchId >= junction.getBranchCount()) {
		branchId = 0;
	    }
	    return branchId;
	}
	BranchDescriptor3D bd = junction.getBranch(branchId);
	NucleotideStrand inStrand = bd.getIncomingStrand();
	for (int i = 0; i < junction.getBranchCount(); ++i) {
	    if (junction.getBranch(i).getOutgoingStrand().isProbablyEqual(inStrand)) {
		return i;
	    }
	}
	assert false; // should never be here
	return -1;
    }

    /** Generates a name, like for example "2HS4H" according to some nomenclature */
    public String generateNomenclature(StrandJunction3D junction) {
	assert junction.getBranchCount() == junction.getStrandCount();
	int branchCount = junction.getBranchCount();
	if (!loopMode) {
	    return "" + branchCount + "H";
	}
	int [] loopLengths = new int[junction.getStrandCount()];
	int currentBranchDescriptor = 0;
	int pc = 0;
	do {
	    BranchDescriptor3D branch = junction.getBranch(currentBranchDescriptor);
	    NucleotideStrand strand = branch.getIncomingStrand();
	    int strandIndex = junction.findStrandIndex(strand); // TODO slow!!!
	    assert strandIndex >= 0;
	    assert strandIndex < junction.getStrandCount();
	    loopLengths[strandIndex] = junction.getLoopLength(strandIndex);
	    currentBranchDescriptor = getConsecutiveBranchDescriptor(junction, currentBranchDescriptor); // get next connected strand
	    pc++;
	    assert pc < 1000; // avoid endless loop
	}
	while (currentBranchDescriptor != 0); // careful: endless loop if not circular
	// Arrays.sort(loopLengths);  // wrong: this would confuse 
	loopLengths = rotateToSmallest(loopLengths);
	// count number of zeros:
	int zeroCount = 0;
	StringBuffer buf = new StringBuffer();
	int helixCount = 1;
	for (int i = 0; i < loopLengths.length; ++i) {
	    if (loopLengths[i] > 0) {
		if (helixCount > 1) {
		    buf.append(helixCount + "H" + "S" + loopLengths[i]);
		}
		else {
		    buf.append("HS" + loopLengths[i]);
		}
		helixCount = 1;
	    }
	    else {
		++helixCount;
	    }
	}
	return buf.toString();
    }

    public void setLoopMode(boolean b) { this.loopMode = b; }

}
