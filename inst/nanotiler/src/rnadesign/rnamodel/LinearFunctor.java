package rnadesign.rnamodel;

import generaltools.DoubleFunctor;

/** defines class providing real valued function */
public class LinearFunctor implements DoubleFunctor {

    private double a;
    private double b;

    public LinearFunctor(double a, double b) {
	this.a = a;
	this.b = b;
    }

    /** function that maps real value to another real value */
    public double doubleFunc(double value) {
	return (a * value) + b;
    }

}
