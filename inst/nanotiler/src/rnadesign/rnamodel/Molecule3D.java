/** 
 * This interface describes the concept of an molecule.
 * A molecule is a container of atoms.
 */
package rnadesign.rnamodel;
import tools3d.objects3d.*;
import java.util.List;

/**
 * @author Eckart Bindewald
 *
 */
public interface Molecule3D extends Object3D {

    /** returns n'th nucleotide */
    public Atom3D getAtom(int n);

    /** returns number of defined atoms.*/
    public int getAtomCount();

    /** Returns order of covalent bond or zero if not bonded. TODO: implement using links and not distances */
    public int getCovalentBondOrder(Atom3D atom1, Atom3D atom2) throws RnaModelException;

    /** returns list of atoms that are covalently bonded with this atom */
    public List<Atom3D> getCovalentlyBondedAtoms(Atom3D atom);

}
