package rnadesign.rnamodel;

import generaltools.PropertyTools;
import tools3d.objects3d.*;
import tools3d.Vector3D;
import tools3d.Vector3DTools;
import java.util.logging.*;
import java.util.Properties;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

public class MoleculeTools {

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    public static Vector3D averageAtomPosition(Object3D molecule) {
	Object3DSet oset = Object3DTools.collectByClassName(molecule, "Atom3D");
	return Object3DSetTools.centerOfMass(oset);
    }

    public static Properties characterize(Object3D molecule) {
	Properties result = null;
	if (molecule.getProperties()!=null) {
	    result = (Properties)(molecule.getProperties());
	}
	else {
	    result = new Properties();
	}
	Object3DSet oset = Object3DTools.collectByClassName(molecule, "Atom3D");
	Object3DSet strandSet = Object3DTools.collectByClassName(molecule, "RnaStrand");
	Object3DSet residueSet = Object3DTools.collectByClassName(molecule, "Nucleotide3D");
	Vector3D[] positions = Object3DSetTools.getCoordinates(oset);
	Vector3D centerGravity = Vector3DTools.computeCenterGravity(positions);
	double[] masses = new double[positions.length];
	masses[0] = 1.0 / (double)masses.length;
	for (int i = 1; i < masses.length; ++i) {
	    masses[i] = masses[0];
	}
	double[] radii = Vector3DTools.getRadiiOfGyration(positions, masses);
	double aniso = 0.0;
	assert radii[0] >= radii[2];
	if (radii[2] > 0.0) {
	    aniso = radii[0] / radii[2];
	}
	double volApprox = (4.0/3.0) * Math.PI * radii[0] * radii[1] * radii[2];
	result.setProperty("number_atoms", "" + oset.size());
	result.setProperty("gyrradius_1", ""+ radii[0]);
	result.setProperty("gyrradius_2", "" + radii[1]);
	result.setProperty("gyrradius_3", "" + radii[2]);
	result.setProperty("gyr_aniso", "" +aniso);
	result.setProperty("gyrradius_volume", "" + volApprox); // ellipsoid volume 4/3 PI * r1 * r2 * r3
	result.setProperty("number_rnastrands", "" + strandSet.size());
	result.setProperty("number_nucleotides", "" + residueSet.size());
	result.setProperty("gravcentx", "" + centerGravity.getX());
	result.setProperty("gravcenty", "" + centerGravity.getY());
	result.setProperty("gravcentz", "" + centerGravity.getZ());
	Properties molProp = molecule.getProperties();
	if (molProp != null) {
	    PropertyTools.mergeProperties(result,molProp,Object3DTools.getFullName(molecule) + ".");
	}
	// TODO : activate code with more control
// 	for (int i = 0; i < molecule.size(); ++i) {
// 	    molProp = molecule.getChild(i).getProperties();
// 	    if (molProp != null) {
// 		PropertyTools.mergeProperties(result,molProp,Object3DTools.getFullName(molecule.getChild(i)) + ".");
// 	    }

// 	}

	return result;
    }

    /** returns all atoms that have to be rotated if one rotates atom2 around bond from atom1. TODO: implement */
    public static Object3DSet collectRotationSet(Molecule3D molecule, Atom3D atom1, Atom3D atom2) {
	// log.info("Starting collectRotationSet!");
	Object3DSet result = new SimpleObject3DSet();
	// use some sort of spanning tree!
	Set<Atom3D> expandSet = new HashSet<Atom3D>();
	expandSet.add(atom2);
	Set<Atom3D> resultSet = new HashSet<Atom3D>();
	while (expandSet.size() > 0) {
	    // log.info("Size of expand and result sets: " + expandSet.size() + " " + result.size());
	    Iterator<Atom3D> atomIter = expandSet.iterator();
	    assert atomIter.hasNext();
	    Atom3D atom = atomIter.next();
	    expandSet.remove(atom);
	    if ((atom != atom1) || (!resultSet.contains(atom))) {
		resultSet.add(atom);
		List<Atom3D> connectedAtoms = molecule.getCovalentlyBondedAtoms(atom);
		assert connectedAtoms.size() < 6;
		for (int i = 0; i < connectedAtoms.size(); ++i) {
		    Atom3D connAtom = connectedAtoms.get(i);
		    if (!resultSet.contains(connAtom)) {
			expandSet.add(connAtom);
		    }
		}
	    }
	}
	Iterator<Atom3D> atomIter = resultSet.iterator();
	while (atomIter.hasNext()) {
	    result.add(atomIter.next());
	}
	// log.info("Finished collectRotationSet with size: " + result.size());
	return result;
    }

    /** rotates part of molecule around bond pointing from atom1 to atom2, using angle */
    public static void rotateAroundBond(Molecule3D molecule, Atom3D atom1, Atom3D atom2, double angle) throws RnaModelException {
	assert atom1.distance(atom2) > 0;
	int bondOrder = molecule.getCovalentBondOrder(atom1, atom2); 
	if (bondOrder == 0) {
	    log.info("Trying to rotate around two atoms that are not bonded: " + atom1 + " " + atom2);
	}
	assert bondOrder > 0;
	Object3DSet rotationSet = collectRotationSet(molecule, atom1, atom2);
	Object3DTools.rotateSet(rotationSet, atom2.getPosition().minus(atom1.getPosition()), atom2.getPosition(), angle);
    }

}
