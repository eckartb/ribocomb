package rnadesign.rnamodel;

import java.util.logging.*;
import java.util.Properties;
import java.util.Random;
import generaltools.Randomizer;
import numerictools.*;
import tools3d.*;
import tools3d.objects3d.*;
import static rnadesign.rnamodel.PackageConstants.*;
import static rnadesign.rnamodel.RnaConstants.*;


/** Class that computes for a given pair of branch descriptors an optimized interpolating branch descritpor */
public class MonteCarloBranchDescriptorOptimizer extends AbstractBranchDescriptorOptimizer {

    private static Random rnd = Randomizer.getInstance();

    public static final double HELIX_RISE_TOLERANCE = 0.5;
    public static final double HELIX_STEP_DEFAULT = 0.1;
    public static final double TRANSLATION_STEP_DEFAULT = 0.5;
    public static final double ANGLE_STEP_DEFAULT = DEG2RAD * 5.0;
    private static final double HELIX_CONSTANT = 38.1356601; // unit: Angstroem squared; formula derived from Aalberts 2005
    public static final String CLASS_NAME = "MonteCarloBranchDescriptorOptimizer";

    private FitParameters prm;
    private OrientableModifier mutator = new GaussianOrientableMutator(TRANSLATION_STEP_DEFAULT,
								       ANGLE_STEP_DEFAULT);

    public MonteCarloBranchDescriptorOptimizer(FitParameters prm) { 
	this.prm = prm;
    }

    public String getClassName() { return CLASS_NAME; }

    private void mutateHelixParameters(BranchDescriptor3D b) {
	HelixParameters prm = b.getHelixParameters();
	double stretch = 1.0 + ( rnd.nextGaussian() * HELIX_STEP_DEFAULT);
	double hNew = prm.getRise() * stretch;
	if (Math.abs(HELIX_RISE - hNew) > HELIX_RISE_TOLERANCE) {
	    return; // do nothing, error too larget
	}
	// double newR = 1.782535 * Math.sqrt(A- (hNew * hNew));
	double newNt = 2.0 * Math.PI * prm.getRadius() / Math.sqrt(HELIX_CONSTANT - (hNew * hNew));
	prm.setRise(hNew);
	prm.setBasePairsPerTurn(newNt);
	b.setHelixParameters(prm);
    }

    private void mutateBranchDescriptor(BranchDescriptor3D b) {
	// mutate position
	mutator.translate(b);
	// mutate orientation
	mutator.rotate(b);
	// mutate helix parameters
	// mutateHelixParameters(b);
    }

    /** optimize stem so that it fits as good as possible with junction branch descriptors 1 and 2.
     *  Careful: junction branch descriptors are assumed to have the offset 1 and -1,
     *  while the result branch descriptor has the residue offset zero.
     *  Using equations from Aalberts 2005, compare also MathWorld page on helices.
     */
    public BranchDescriptor3D optimize(RnaStem3D stem,
				       BranchDescriptor3D branch1,
				       BranchDescriptor3D branch2) {
	log.info("Starting optimizeBranchDescriptor!");
	assert branch1.isValid();
	assert branch2.isValid();
	int bestAngle = 0;
	double rmsLimit = prm.getRmsLimit();
	log.info("Starting MonteCarloBranchDescriptorOptimizer with rms limit: " + rmsLimit);
	double bestError = rmsLimit + 0.0001;
	// check in one degree steps:
	// obtain initial solution:
	FitParameters prmInit = new FitParameters(100.0, Math.PI); // very high limits for initial fit
	prmInit.setRmsLimit(30.0);
	BranchDescriptorOptimizer stretchOptimizer = BranchDescriptorOptimizerFactory.generate(BranchDescriptorOptimizerFactory.STRETCH_OPTIMIZER,
											       prmInit);
	// obtain initial solution:
	BranchDescriptor3D b = null;
	try {
	    b = stretchOptimizer.optimize(stem, branch1, branch2);
	}
	catch (FittingException fe) {
	    if (b == null) {
		log.info("Stretch optimizer could not find suitable initial stem solution!");
		return null;
	    }
	}
	if (b == null) {
	    log.info("Stretch optimizer could not find suitable initial stem solution!");
	    return null;
	}
	assert(b != null);
	for (int iter = 0; iter < prm.getIterMax(); ++iter) {
	    BranchDescriptor3D bSafe = (BranchDescriptor3D)(b.cloneDeep()); // TODO : much better: optim. coord sys
	    mutateBranchDescriptor(b); 
	    double errorVal = computeBranchDescriptorError(stem.getLength(), branch1, branch2, b); // 
	    if (errorVal < bestError) {
		log.info("Iteration: " + iter + " : New solution in MC optimization of stem: " +  errorVal);
		bestError = errorVal;
	    }
	    else {
		// undo operation:
		b = bSafe;
	    }
	}

	if (bestError > rmsLimit) {
	    log.info("Could not find stem solution: " + bestError);
	    return null; // NO SUITABLE solution found
	}
	else {
	    log.info("Could find stem solution: " + bestError);
	}
	Properties prop = b.getProperties();
	if (prop == null) {
	    prop = new Properties();
	    b.setProperties(prop);
	}
	prop.setProperty("fit_score", "" + bestError); // used later for evaluating fit of axial kissing loop!	
	// prop.setProperty("stretch_factor", ""+ bestStretch); // used later for evaluating fit of axial kissing loop!
	assert(b.getProperties() != null);
	log.info("Finished optimizeBranchDescriptor!");
	return b;
    }


    
}
