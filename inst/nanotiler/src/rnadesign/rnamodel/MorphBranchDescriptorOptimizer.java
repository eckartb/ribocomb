package rnadesign.rnamodel;

import java.util.logging.*;
import java.util.Properties;
import java.util.Random;
import generaltools.Randomizer;
import numerictools.*;
import tools3d.*;
import tools3d.objects3d.*;
import static rnadesign.rnamodel.PackageConstants.*;
import static rnadesign.rnamodel.RnaConstants.*;


/** Class that computes for a given pair of branch descriptors an optimized interpolating branch descritpor */
public class MorphBranchDescriptorOptimizer extends AbstractBranchDescriptorOptimizer {

    private static Random rnd = Randomizer.getInstance();
    private double annealFactor = 0.95;

//     public static final double HELIX_RISE_TOLERANCE = 0.5;
//     public static final double HELIX_STEP_DEFAULT = 0.1;
    public static final double TRANSLATION_STEP_DEFAULT = 0.1;
    public static final double ANGLE_STEP_DEFAULT = DEG2RAD * 0.5;
    // private static final double HELIX_CONSTANT = 38.1356601; // unit: Angstroem squared; formula derived from Aalberts 2005
    public static final String CLASS_NAME = "MorphCarloBranchDescriptorOptimizer";
    private double translationStepLimit = 0.001;
    private double angleStepLimit = 0.001;
    private double angleWeight = 1.0;
    private double shiftWeight = 1.0; // used for deformation score

    private FitParameters prm;
    private OrientableModifier mutator = new GaussianOrientableMutator(TRANSLATION_STEP_DEFAULT,
								       ANGLE_STEP_DEFAULT);

    public MorphBranchDescriptorOptimizer(FitParameters prm) { 
	this.prm = prm;
    }

    public String getClassName() { return CLASS_NAME; }

    private void mutateHelixParameters(HelixParameters prm) {
// 	double stretch = 1.0 + ( rnd.nextGaussian() * HELIX_STEP_DEFAULT);
// 	double hNew = prm.getRise() * stretch;
// 	if (Math.abs(HELIX_RISE - hNew) > HELIX_RISE_TOLERANCE) {
// 	    return; // do nothing, error too larget
// 	}
// 	// double newR = 1.782535 * Math.sqrt(A- (hNew * hNew));
// 	double newNt = 2.0 * Math.PI * prm.getRadius() / Math.sqrt(HELIX_CONSTANT - (hNew * hNew));
// 	prm.setRise(hNew);
// 	prm.setBasePairsPerTurn(newNt);
// 	b.setHelixParameters(prm);
	CoordinateSystem def = prm.getDeformation(); // deformation coordinate sytem: describes deviation per base pair
	assert def != null;	
	mutator.rotate(def);
	mutator.translate(def);
    }

//     private void mutateBranchDescriptor(BranchDescriptor3D b) {
// 	// mutate position
// 	// mutator.translate(b);
// 	// mutate orientation
// 	// mutator.rotate(b);
// 	// mutate helix parameters
// 	mutateHelixParameters(b);
//     }

    public double scoreHelixDeformation(HelixParameters hpar) {
	CoordinateSystem defCoord = hpar.getDeformation();
	AxisAngle axisAngle = new AxisAngle(new Matrix3D(defCoord.generateMatrix4D()));
	double angle = axisAngle.getAngle();
	double shift = defCoord.getPosition().length();
	double cosang = 0.5*(1.0-Math.cos(angle));
	double result = angleWeight * cosang  + shiftWeight * shift;
	return result;    
    }

    /** optimize stem so that it fits as good as possible with junction branch descriptors 1 and 2.
     *  Careful: junction branch descriptors are assumed to have the offset 1 and -1,
     *  while the result branch descriptor has the residue offset zero.
     *  Using equations from Aalberts 2005, compare also MathWorld page on helices.
     */
    public BranchDescriptor3D optimize(RnaStem3D stem,
				       BranchDescriptor3D branch1,
				       BranchDescriptor3D branch2) {
	log.info("Starting optimizeBranchDescriptor!");
	assert branch1.isValid();
	assert branch2.isValid();
	int bestAngle = 0;
	double deformScore = 999.9;
	double rmsLimit = prm.getRmsLimit();
	log.info("Starting optimizeStretchedBranchDescriptor with rms limit: " +
		 rmsLimit);
	double bestError = rmsLimit + 0.0001;
	// check in one degree steps:
	// obtain initial solution:
	FitParameters prmInit = new FitParameters(100.0, Math.PI); // very high limits for initial fit
	// obtain initial solution:
	BranchDescriptor3D b = propagateBranchDescriptor(stem, branch1);
	HelixParameters hprm = b.getHelixParameters();
	HelixParameters hBest = new HelixParameters(b.getHelixParameters()); 
	if (hprm.getDeformation() == null) {
	    hprm.initDeformation();
	}
	assert(b != null);
	for (int iter = 0; iter < prm.getIterMax(); ++iter) {
	    HelixParameters hSafe = new HelixParameters(b.getHelixParameters()); // TODO : much better: optim. coord sys
	    mutateHelixParameters(b.getHelixParameters()); 
	    double errorVal = computeBranchDescriptorError(stem.getLength(), branch1, branch2, b); // 
	    // log.info("Iteration: " + iter + " score: " + errorVal);
	    if (errorVal < bestError) {
		log.fine("Iteration: " + iter + " : New solution in MC optimization of stem: " +  errorVal + " with mutator: " 
			 + mutator);
		bestError = errorVal; // keep best solution
		hBest = new HelixParameters(b.getHelixParameters()); 
		mutator.scaleTranslationStep(annealFactor);
		mutator.scaleAngleStep(annealFactor);
		if ((mutator.getTranslationStep() < translationStepLimit) 
		    && (mutator.getAngleStep() < angleStepLimit)) {
		    break;
		}
	    }
	    else {
		// undo operation:
		b.setHelixParameters(hSafe);
	    }
	}

	if (bestError > rmsLimit) {
	    log.info("Could not find stem solution: " + bestError);
	    return null; // NO SUITABLE solution found
	}
	else {
	    deformScore = scoreHelixDeformation(hBest); 
	    log.info("Could find stem solution: " + bestError + " deformation score: " + deformScore);
	    log.info("Best found helix parameters: " + b.getHelixParameters());
	}
	Properties prop = b.getProperties();

	if (prop == null) {
	    prop = new Properties();
	    b.setProperties(prop);
	}
	prop.setProperty("fit_score", "" + bestError); // used later for evaluating fit of axial kissing loop!	
	prop.setProperty("deform_score", "" + deformScore);
	// prop.setProperty("stretch_factor", ""+ bestStretch); // used later for evaluating fit of axial kissing loop!
	assert(b.getProperties() != null);
	log.info("Finished optimizeBranchDescriptor!");
	return b;
    }


    
}
