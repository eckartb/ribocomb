package rnadesign.rnamodel;

import java.util.*;
import java.io.FileInputStream;
import java.io.IOException;

import sequence.Sequence;
import generaltools.ConstraintDouble;
import generaltools.SimpleConstraintDouble;
import tools3d.CoordinateSystem;
import tools3d.Vector3D;
import tools3d.SuperpositionResult;
import tools3d.objects3d.*;


/** scores helix placement based on adherence to possible motifs*/
public class MotifConstraintLink extends SimpleLink{

    private int length;
    private Sequence seq;
    
    //TODO: make user input
    private double angleWeight = 10.0;
    private double rms = 5.0;
    private int solutionMax = 10;
    private List<String> dbFileNames;
    private SingleStrandMotifFinder finder;

    public MotifConstraintLink(Object3D o1, Object3D o2, int length, Sequence seq, List<String> dbFileNames) {
	super(o1, o2);
  this.length = length;
  this.seq = seq;  
  
  List<Object3DLinkSetBundle> bundleList = new ArrayList<Object3DLinkSetBundle>();
  for(String fileName : dbFileNames){
    try{
      RnaPdbRnaviewReader reader = new RnaPdbRnaviewReader();
      FileInputStream fis = new FileInputStream(fileName);
      Object3DLinkSetBundle bundle = reader.readBundle(fis);
      bundleList.add(bundle);
      
    } catch (IOException ioe){
      System.out.println("Error reading from file: " + fileName);
    }
  }
  
  SingleStrandMotifFinder finder = new SingleStrandMotifFinder(bundleList);
  finder.setRms(this.rms);
  finder.setLenMin(this.length);
  finder.setLenMax(this.length);
  finder.setSolutionMax(this.solutionMax);
  finder.setAngleWeight(this.angleWeight);
  
  this.finder = finder;
    }

    public double computeScore(CoordinateSystem cs1, CoordinateSystem cs2) {
      
      Nucleotide3D obj1 = (Nucleotide3D)getObj1();
      Nucleotide3D obj2 = (Nucleotide3D)getObj2();
      Object3DSet tripod1 = null;
      Object3DSet tripod2 = null;
      
      try{
        tripod1 = NucleotideDBTools.extractAtomTripodBp( obj1);
      	tripod2 = NucleotideDBTools.extractAtomTripodBp( obj2);
      } catch (RnaModelException rme){
        System.out.println( rme + " Objects " + obj1 + " and " + obj2 + "do not have atom tripods.");
      }
      
      Vector3D[] coord1 = Object3DSetTools.getCoordinates(tripod1);
    	Vector3D[] coord2 = Object3DSetTools.getCoordinates(tripod2);
      Vector3D[] newCoord1 = new Vector3D[ coord1.length ];
      Vector3D[] newCoord2 = new Vector3D[ coord2.length ];

      //shift coordinates of atoms in tripod
      for(int i=0; i<coord1.length; i++){
    	   newCoord1[i] = cs1.activeTransform( coord1[i] );
       }
      for(int i=0; i<coord2.length; i++){
     	   newCoord2[i] = cs2.activeTransform( coord2[i] );
      }
      
      List<Object3DLinkSetBundle> solutions = new ArrayList<Object3DLinkSetBundle>();
      solutions = finder.findSingleStrandBridge(obj1, obj2, newCoord1, newCoord2, this.length);
      
      assert solutions != null;
      System.out.println("Total number of found bridging fragments: " + solutions.size());
  		if (solutions.size() > 0) {
  		  Object3D motif = solutions.get(0).getObject3D();
  		  System.out.println(motif.getProperties());
        double bestRms = Double.parseDouble( solutions.get(0).getObject3D().getProperty("Rms") );
        for ( int i=0; i<solutions.size(); ++i ){
           double rms = Double.parseDouble( solutions.get(i).getObject3D().getProperty("Rms") );
           System.out.println("Rms " + i + " : " + rms);
           if (rms<bestRms){
             bestRms=rms;
          }
        }
        return bestRms;
      }
      
      System.out.println("No solutions found.");
      return 1000.0;
    }

    /** returns constraint */
    public int getLength() { return length; }

    public Sequence getSeq() { return seq; }

    public String toString() {
	String result = "(ConstraintLink " + getName() + " " + getName1() + " " + getName2() + " " + getSeq() + " " + getLength() + " )";
	return result;
    }

}
