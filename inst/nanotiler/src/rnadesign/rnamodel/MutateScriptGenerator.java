package rnadesign.rnamodel;

import java.util.*;
import java.util.logging.*;
import tools3d.Vector3D;
import tools3d.objects3d.*;
import tools3d.symmetry2.*;
import rnadesign.rnamodel.*;
import rnadesign.rnacontrol.*;
import rnasecondary.*;
import sequence.*;

import static rnadesign.rnacontrol.PackageConstants.*;

public class MutateScriptGenerator implements ScriptGenerator {

    // private Object3D graphRoot;
    // private Object3DSet vertices;
    // private LinkSet links;
    private double distMin = 6.0;
    private double distMax = 12.0;
    private int[] helixLengths;
    private List<String> helixNames;
    private int[] origHelixIndices; // for specifying which helices are the original of the symmetry copies
    private double kt = 10.0;
    private String name;
    
    private SecondaryStructure currentStructure;
    private SecondaryStructure targetStructure;
    private String[] sequenceNames;
    // private List<SymEdgeConstraint> symConstraints;
    private static Logger log = Logger.getLogger(LOGFILE_DEFAULT);
    
    List<String> constraints = new ArrayList<String>();

    public MutateScriptGenerator(SecondaryStructure currentStructure, SecondaryStructure targetStructure, String[] sequenceNames, String name, Properties params) {
    
	assert currentStructure != null;
	assert targetStructure != null;
	this.currentStructure = currentStructure;
	this.targetStructure = targetStructure;
	this.sequenceNames = sequenceNames;
	this.name = name;
	System.out.println("Starting MutateScriptGenerator with objects:");
	parseParams(params);
    }

    private void parseParams(Properties params) {
	if (params.getProperty("min") != null) {
	    this.distMin = Double.parseDouble(params.getProperty("min"));
	}
	if (params.getProperty("max") != null) {
	    this.distMax = Double.parseDouble(params.getProperty("max"));
	}
    }

    public String generate() {
	StringBuffer buf = new StringBuffer();
	buf.append("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb" + NEWLINE + "loadbasepairs ${bpdb}" + NEWLINE );

	int[] perms = SecondaryStructureTools.findSequencePermutation(currentStructure, targetStructure);
	
	buf.append("mutate force=true rms=10 ");
	int seqSize = currentStructure.getSequenceCount();
    for(int i=0; i<seqSize; i++){
    	
    	Sequence targetSequence = targetStructure.getSequence( perms[i] );
    	
    	buf.append( sequenceNames[i] + ":" );
    
    	int resSize = currentStructure.getSequence(i).size();
    	for(int j=0; j<resSize; j++){
			buf.append(  targetSequence.getResidue(j).getSymbol().getCharacter() + "" + (j+1) + "," );
		}
		
		buf.append(" ");
    }
    
    buf.append( NEWLINE );
    
	return buf.toString();
    }
    



    

}
    
