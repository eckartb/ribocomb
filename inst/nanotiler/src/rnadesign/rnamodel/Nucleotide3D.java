package rnadesign.rnamodel;

import java.io.InputStream;
import generaltools.MalformedInputException;
import sequence.*;
import tools3d.*;
import tools3d.objects3d.*;
import java.util.logging.*;
import java.util.List;
import java.util.ArrayList;
import rnadesign.rnamodel.NucleotideTools;

import static rnadesign.rnamodel.PackageConstants.*;

/** represents RNA or DNA base */
public class Nucleotide3D extends SimpleObject3D implements Residue3D {

    // private Sequence sequence;

    // private int pos; // position in sequence (n'th nucleotide) // computed on the fly because difficult to synchronize and keep updated

    private int assignedNumber;

    private String assignedName = "";

    public static final String CLASS_NAME = "Nucleotide3D";

    private LetterSymbol symbol;

    private static Logger log = Logger.getLogger("NanoTiler.debug");

    /** please use with care. object is not valid, but still useful for nucleotide */
    public Nucleotide3D() {
	assignedNumber = 0;
	// pos = 0;
    }

    public Nucleotide3D(LetterSymbol symbol, Vector3D position) {
	super(position);
	this.symbol = symbol;
	// this.sequence = sequence;
	// this.pos = pos;
	this.assignedNumber = 0;
    }

    public Object clone() {
	assert false; // implementation not correct
	return new Nucleotide3D(symbol, getRelativePosition());
    }

    /** "deep" clone not including parent and children: every object is newly generated!
     */
    public Object cloneDeepThis() {
	Nucleotide3D obj = new Nucleotide3D();
	obj.assignedNumber = this.assignedNumber;
	obj.copyDeepThisCore(this);
	// obj.sequence = this.sequence; // TODO not necessary if cloning strand: each nucleotide is pointing to same sequence! :-(
	// obj.pos = pos;
	obj.symbol = (LetterSymbol)(symbol.clone());
	return obj; // TODO : test if ok!
    }

    /** returns assigned number (like residue number occuring in PDB file */
    public int getAssignedNumber() { return assignedNumber; }

    public String getAssignedName() { return assignedName; }

    /** returns number of child nodes that are atoms */
    public int getAtomCount() {
	return size(); // TODO !!!
    }

    /** returns n'th atom child */
    public Atom3D getAtom(int n) {
	if (! (getChild(n) instanceof Atom3D)) {
	    log.warning("Child " + n + " of nucleotide " + getFullName() + " is not of class Atom3D: " 
			+ getChild(n));
	    return null;
	}
	return (Atom3D)(getChild(n)); // TODO !!!
    }

    public String getClassName() { return CLASS_NAME; }

    /** returns order of covalent bond or zero if atoms not bonded. TODO: fully implement higher order bonds */
    public int getCovalentBondOrder(Atom3D atom1, Atom3D atom2) {
	try {
	    boolean flag = NucleotideTools.isCovalentlyBonded(this, atom1.getName(), atom2.getName());
	    return flag ? 1 : 0;
	}
	catch(RnaModelException rne) {
	    log.info("RnaModelException: " + rne.getMessage());
	}
	return 0;
    }

    /** returns list of atoms that are covalently bonded with this atom */
    public List<Atom3D> getCovalentlyBondedAtoms(Atom3D atom) {
	List<Atom3D> resultAtoms = new ArrayList<Atom3D>();
	Nucleotide3D res = (Nucleotide3D)(atom.getParent());
	for (int j = 0; j < res.size(); ++j) {
	    Atom3D atomOther = (Atom3D)(getChild(j));
	    if ((atom != atomOther) && (getCovalentBondOrder(atom, atomOther) > 0)) {
		resultAtoms.add(atomOther);
	    }
	}
	return resultAtoms;
    }

    public Object getParentObject() {
	return getParent(); 
    }

    public LetterSymbol getSymbol() {
	return symbol;
    }

    /** returns true if residue not equal "X" */
    public boolean isKnownResidue() {
	char c = getName().charAt(0);
	return (c != 'X') && (c != 'N');
    }

    public void insertChild(Object3D child) {
	if (! (child instanceof Atom3D)) {
	    log.warning("Adding class other than Atom3D to object of class Nucleotide3D: " + getFullName() + " " +
			child.toString());
	}
	super.insertChild(child);
    }

    public boolean isFirst() {
	if ((getParent() == null) || (!(getParent() instanceof BioPolymer))) {
	    return false;
	}
	return getPos() == 0;
    }

    public boolean isLast() {
	if ((getParent() == null) || (!(getParent() instanceof BioPolymer))) {
	    return false;
	}
	return getPos() + 1 == ((BioPolymer)(getParent())).getResidueCount();
    }

    public boolean isTerminal() { return isFirst() || isLast(); }
    
    /** returns true if part of same sequence */
    public boolean isSameSequence(Residue other) {
	if (other instanceof Nucleotide3D) {
	    Nucleotide3D otherNuc = (Nucleotide3D)other;
	    assert getParent() != null;
	    assert otherNuc.getParent() != null;
	    assert getParent() instanceof BioPolymer;
	    assert otherNuc.getParent() instanceof BioPolymer;
	    return (getParent() == otherNuc.getParent());
	}
	return false;
    }

    public void setSymbol(LetterSymbol symbol) {
	this.symbol = symbol;
    }

    // public Sequence getSequence() { return sequence; }

    /** Return position in sequence. 
     * TODO : currently only the index of the child is counted - could lead to wrong results if non-nucleotides are inserted as siblings.
     */
    public int getPos() { 
	return getSiblingId(); // compute index on the fly
	// return pos; 
    }

    public void mutateResidue(Residue residue, int position) {
	assert false; // currently  not supported
	throw new RuntimeException("Internal error: mutation of Nucleotide3D currently not supported!");
    }

    public void read(InputStream is) {
	// TODO
    }

    public void setParentObject(Object obj) {
	if (obj instanceof Object3D) {
	    super.setParent((Object3D)obj);
	}
	throw new RuntimeException("Parent of Nucleotide3D class must be an Object3D");
    }

    public void setPos(int pos) { 
	log.warning("setPos ignored in Nucleotide3D");
    }

    public void setAssignedName(String name) { this.assignedName = name; }

    /** sets assigned number (like residue number occuring in PDB file */
    public void setAssignedNumber(int number) { this.assignedNumber = number; }

    // public void setSequence(Sequence s) { this.sequence = sequence; }

    /** returns position of first occurence of digit */
    private int findDigitPos(String name) {
	int digitPos = 0;
	for (int i = 0; i < name.length(); ++i) {
	    if (Character.isDigit(name.charAt(i))) {
		return i;
	    }
	}
	return -1;
    }

    /** If given residue with name G11, return 11 */
    public int getResidueNumberInName() {
	String name = getName();
	int digitPos = findDigitPos(name);
	if (digitPos < 0) {
	    return -1;
	}
	String s = name.substring(digitPos, name.length());
	return Integer.parseInt(s);
    }

    /** Returns residue number as given in PDB file, or default if not defined. */
    public int getResidueNumberInPdb() {
	String prop = getProperty(PDB_RESIDUE_ID);
	if (prop == null) {
	    return getPos()+1;
	}
	return Integer.parseInt(prop);
    }

    /** If given residue with name G11 and number 3, gives new name G3 */
    public void setResidueNumberInName(int number) {
	String name = getName();
	int digitPos = findDigitPos(name);
	String prefix = name.substring(0, digitPos);
	setName(prefix + number);
    }

    public String toString() {
	String s = new String();
	s = "(" + getClassName() + " " + symbol + " " + toStringBody() + " )";
	return s;
    }

}
