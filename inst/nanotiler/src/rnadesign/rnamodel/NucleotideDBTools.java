package rnadesign.rnamodel;

import java.io.*;
import sequence.LetterSymbol;
import tools3d.objects3d.*;
import tools3d.*;
import chemistrytools.DefaultChemicalElement;

import java.util.logging.*;

/** helper methods for database of nucleotide coordinates */
public class NucleotideDBTools {

    private static Logger log = Logger.getLogger(PackageConstants.LOGFILE_DEFAULT);

    /** names of atoms to be superposed. Careful: number can be larger than three inspite of the name "tripod" */
    // public static String[] tripodNames = {"P", "O5*", "O3*", "C3*", "C1*"};
    public static final String[] tripodNames = {"P", "O3*", "O5*"};
    public static final String[] multipodNames = {"P", "O3*", "O5*", "C1*", "C5" , "N3", "N1", "C4" }; // last two atoms connectivity depends on nucleotide!
    public static final String[] tripodNamesBp = {"P", "O3*"}; // for base pair, use only 2 atoms per nucleotide (will be 2x2 = 4 atoms for base pair)
    public static final String[] phosphorNeighbors = {"O5*", "C5*"};
    public static final String phosphorName = "P";
    public static boolean correctionMode = false; // correction is alredy in new parameter file
    public static final Vector3D gCorrection = new Vector3D(0.35, -0.45, 0.0);
    public static final Vector3D cCorrection = new Vector3D(0.35, 0.0, 0.0);
    public static final Vector3D auCorrection = new Vector3D(0.35, -0.365, 0.5225);
    public static final Vector3D uCorrection = new Vector3D(-0.125, -0.365, 0.45);
    public static final Matrix3D auCorrectionMatrix = Matrix3D.rotationMatrix(new Vector3D(1.0, 0.0, 0.0), Math.toRadians(6.75));
    public static final Matrix3D uCorrectionMatrix = Matrix3D.rotationMatrix(new Vector3D(1.0, 0.0, 0.0), Math.toRadians(7.4));
    public static final Matrix3D uCorrectionMatrix2 = Matrix3D.rotationMatrix(new Vector3D(0.0, 1.0, 0.0), Math.toRadians(-0.65));
    public static final Matrix3D auCorrectionMatrix2 = Matrix3D.rotationMatrix(new Vector3D(0.0, 1.0, 0.0), Math.toRadians(1));
    public static final Matrix3D auCorrectionMatrix3 = Matrix3D.rotationMatrix(new Vector3D(0.0, 0.0, 1.0), Math.toRadians(0));
    public static final Matrix3D uCorrectionMatrix3 = Matrix3D.rotationMatrix(new Vector3D(0.0, 0.0, 1.0), Math.toRadians(0));
    /** workaround: add phosphor atom ("P") that is often missing in first nucleotide of strand. It is needed for superposing nucleotides. */
    public static void addMissingPhosphor(Nucleotide3D obj) throws RnaModelException {
	if (obj.getIndexOfChild(phosphorName) >= 0) {
	    return;
	}
	if (!StructureQualityTools.containsChildren(obj, phosphorNeighbors)) {
	    throw new RnaModelException("Could not find backbone atoms needed for computed extrapolated position of new phosphor atom: O5* and C5*: " 
					 + obj.getName());
	}
	Vector3D p1 = obj.getChild(phosphorNeighbors[0]).getPosition();
	Vector3D p2 = obj.getChild(phosphorNeighbors[1]).getPosition();
	Vector3D dir = p1.minus(p2);
	dir.normalize();
	dir.scale(1.5);
	Vector3D newPos = p1.plus(dir); // TODO : 120 degree angle is much better than 180 ...
	Atom3D phosphor = new Atom3D(newPos);

	phosphor.setName(phosphorName);
	phosphor.setElement(new DefaultChemicalElement(phosphorName));
	obj.insertChild(phosphor, 0); // add phosphor to be first atom
	assert (obj.getIndexOfChild(phosphorName) == 0);
    }

    /** prepares names */
    public static Object3D prepareDB(Object3D obj) {
	Object3D dbObj = new SimpleObject3D();
	dbObj.setName("nucleotideDB");
	for (int i = 0; i < obj.size(); ++i) {
	    Object3D strand = obj.getChild(i);
	    for (int j = 0; j < strand.size(); ++j) {
		Object3D mol = strand.getChild(j);
		if (mol instanceof Nucleotide3D) {
		    // use "U" instead of "U3" etc
		    mol.setName("" + mol.getName().charAt(0));
		    if (correctionMode) {
			if (mol.getName().equals("G")) {
			    mol.translate(gCorrection); // workaround correction, just for debugging
			}
			if (mol.getName().equals("C")) {
			    mol.translate(cCorrection); // workaround, just for debugging
			}
			if (mol.getName().equals("A")) {
			    mol.translate(auCorrection); // workaround correction, just for debugging
			    mol.rotate(Vector3D.ZVEC,auCorrectionMatrix);
			    mol.rotate(Vector3D.ZVEC,auCorrectionMatrix2);
			    mol.rotate(Vector3D.ZVEC,auCorrectionMatrix3);
			}
			if (mol.getName().equals("U")) {
			    mol.translate(uCorrection); // workaround, just for debugging
			    mol.rotate(Vector3D.ZVEC,uCorrectionMatrix);
			    mol.rotate(Vector3D.ZVEC,uCorrectionMatrix2);
			    mol.rotate(Vector3D.ZVEC,uCorrectionMatrix3);
			}
			// write tmp file of corrected orientations:
			String outFileName = mol.getName() + ".pdb";
			try {
			    FileOutputStream fos = new FileOutputStream(outFileName);
			    GeneralPdbWriter writer = new GeneralPdbWriter();
			    writer.write(fos, mol);
			    fos.close();
			} catch (IOException ioe) {
			    System.out.println("Error writing to tmp file: " + outFileName 
					       + " : " + ioe.getMessage());
			}
		    }
		    dbObj.insertChild(mol);
		}
	    }
	}
	return dbObj;
    }

    /** returns residue with same symbol */
    public static int findResidueIndex(Object3D nucleotideDB,
			 LetterSymbol symbol) {
	assert symbol != null;
	for (int i = 0; i < nucleotideDB.size(); ++i) {
	    LetterSymbol dbSymbol = ((Residue3D)(nucleotideDB.getChild(i))).getSymbol();
	    if (dbSymbol.equals(symbol)) {
		return i;
	    }
	}
	return -1; // not found!
    }

    private static Object3D getChild(Object3D root, String name) throws RnaModelException {
	int idx = root.getIndexOfChild(name);
	if ((idx < 0) || (idx >= root.size())) {
	    throw new RnaModelException("Object " + root.getName() + " does not contain atom with name: " + 
					name);
	}
	return root.getChild(idx);
    }

    
    /** returns true, if all required "tripod" atoms are found. */
    public static boolean hasTripodAtoms(Object3D nuc) {
	for (int i = 0; i < tripodNames.length; ++i) {
	    if (nuc.getIndexOfChild(tripodNames[i]) < 0) {
		return false;
	    }
	}
	return true;
    }
    
    /** extracts three atoms representating a orientation */
    public static Object3DSet extractAtomTripod(Nucleotide3D nuc) throws RnaModelException {
	Object3DSet result = new SimpleObject3DSet();
	for (int i = 0; i < tripodNames.length; ++i) {
	    if (nuc.getIndexOfChild(tripodNames[i]) < 0) {
		throw new RnaModelException("No atom of type " + tripodNames[i] + " found in nucleotide: " + nuc);
	    }
	    result.add(getChild(nuc, tripodNames[i]));
	}
	return result;
    }

    /** extracts three atoms representating a orientation */
    public static Object3DSet extractAtomMultipod(Nucleotide3D nuc) throws RnaModelException {
	Object3DSet result = new SimpleObject3DSet();
	for (String s: multipodNames) {
	    if (nuc.getIndexOfChild(s) < 0) {
		throw new RnaModelException("No atom of type " + s + " found in nucleotide: " + nuc);
	    }
	    result.add(getChild(nuc, s));
	}
	return result;
    }

    /** extracts three atoms representating a orientation */
    public static Object3DSet extractAtomTripodBp(Nucleotide3D nuc) throws RnaModelException {
	Object3DSet result = new SimpleObject3DSet();
	for (int i = 0; i < tripodNamesBp.length; ++i) {
	    if (nuc.getIndexOfChild(tripodNamesBp[i]) < 0) {
		// check for special case of missing phosphor at first position:
		if (tripodNamesBp[i].equals(phosphorName) && NucleotideTools.is5PrimeNucleotide(nuc)) {
		    log.warning("Adding missing phosphor atom to nucleotide: " + Object3DTools.getFullName(nuc));
		    addMissingPhosphor(nuc);
		}
		else {
		    throw new RnaModelException("No atom of type " + tripodNames[i] + " found in nucleotide: " + nuc);
		}
	    }
	    assert (nuc.getIndexOfChild(tripodNamesBp[i]) >= 0);
	    result.add(getChild(nuc, tripodNamesBp[i]));
	}
// 	result.add(getChild(nuc,"P"));
// 	result.add(getChild(nuc,"O3*"));
// 	result.add(getChild(nuc,"C1*"));
	return result;
    }

    /** returns direction vector defined through atom positions of residue and subsequent residue and atom name */
    public static Vector3D getForwardDirectionVector(Residue3D residue, String atomName) throws RnaModelException {
	Object3D atom1 = residue.getChild(atomName);
	if (atom1 == null) {
	    throw new RnaModelException("Could not find atom " + atomName + " in residue " + residue.getFullName());
	}
	BioPolymer strand = (BioPolymer)(residue.getParent());
	if (strand == null) {
	    throw new RnaModelException("Nucleotide has no biopolymer as parent structure: " + residue.getFullName());
	}
	int pos = residue.getPos();
	if ((pos + 1) >= strand.getResidueCount()) {
	    throw new RnaModelException("Residue index too large for obtaining subsequence residue: " + residue.getFullName());
	}
	Residue3D newResidue = strand.getResidue3D(pos+1);
	Object3D atom2 = newResidue.getChild(atomName);
	if (atom2 == null) {
	    throw new RnaModelException("Could not find atom " + atomName + " in residue " + residue.getFullName());
	}
	return atom2.getPosition().minus(atom1.getPosition());
    }

    /** returns direction vector defined through atom positions of residue and previous residue and atom name */
    public static Vector3D getBackwardDirectionVector(Residue3D residue, String atomName) throws RnaModelException {
	Object3D atom1 = residue.getChild(atomName);
	if (atom1 == null) {
	    throw new RnaModelException("Could not find atom " + atomName + " in residue " + residue.getFullName());
	}
	BioPolymer strand = (BioPolymer)(residue.getParent());
	if (strand == null) {
	    throw new RnaModelException("Nucleotide has no biopolymer as parent structure: " + residue.getFullName());
	}
	int pos = residue.getPos();
	if ((pos - 1) < 0) {
	    throw new RnaModelException("Residue index too small for obtaining subsequence residue: " + residue.getFullName());
	}
	Residue3D newResidue = strand.getResidue3D(pos-1);
	Object3D atom2 = newResidue.getChild(atomName);
	if (atom2 == null) {
	    throw new RnaModelException("Could not find atom " + atomName + " in residue " + residue.getFullName());
	}
	return atom2.getPosition().minus(atom1.getPosition());
    }

    /** reads nucleotide database from file name */
    public static Object3D readNucleotideDB(InputStream is) throws IOException {
	Object3DFactory reader = new RnaPdbRnaviewReader();
	Object3DLinkSetBundle bundle = reader.readBundle(is);
	Object3D nucleotideDBTmp = bundle.getObject3D();
	Object3D nucleotideDB = NucleotideDBTools.prepareDB(nucleotideDBTmp);
	assert validateNucleotideDB(nucleotideDB);
	return nucleotideDB;
    }

    /** Checks if nucleotide DB is well defined. To do: improved! */
    public static boolean validateNucleotideDB(Object3D root) {
	for (int i = 0; i < root.size(); ++i) {
	    Object3D obj = root.getChild(i);
	    if (!(obj instanceof Nucleotide3D) ) {
		System.out.println("Strange object in nucleotide 3D:");
		System.out.println(obj.infoString());
		return false;
	    }
	    Nucleotide3D nuc = (Nucleotide3D)obj;
	    if (nuc.getIndexOfChild("P") < 0) {
		System.out.println("Bad nucleotide:");
		System.out.println(nuc.toString());
		return false; // must contain phosphate
	    }
	}
	return true;
    }


}
