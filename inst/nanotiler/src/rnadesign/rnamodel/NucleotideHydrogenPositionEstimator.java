package rnadesign.rnamodel;

import java.util.*;
import tools3d.objects3d.Object3D;
import tools3d.Vector3D;

class NucleotideHydrogenPositionEstimator implements HydrogenPositionEstimator {

    public static final double H_BOND_LENGTH = 1.0;

    public static final double ADJACENCY_CUTOFF = 3.0;

    boolean validateHydrogenPositionEstimate(Atom3D donor, Vector3D estimate) {
	Nucleotide3D nuc = (Nucleotide3D)(donor.getParent());
	double d0 = estimate.distance(donor.getPosition());
	if (d0 > 2.0) { // too large
	    return false;
	}
	for (int i = 0; i < nuc.size(); ++i) {
	    Object3D obj = nuc.getChild(i);
	    if (obj instanceof Atom3D) {
		Atom3D a = (Atom3D)obj;
		if (obj != donor && (!"H".equals(a.getElement().getShortName()))) { // ignore existing hydrogen atoms
		    double d = estimate.distance(obj.getPosition());
		    if (d < d0) {
			System.out.println("Warning: atom " + obj.getName() + " " + obj.getPosition() + " is with distance " + d + " closer to hydrogen bond position than " + donor.getName() + " in nucleotide " + nuc.getFullName());
			return false;
		    }
		}
	    }
	}
	return true; // no other atom has closer distance to hydrogen atom
    }

    /** For a given donor atom, estimates the position of a hydrogen */
    public Vector3D estimate(Atom3D donor) throws RnaModelException {
	// System.out.println("Starting estimate for atom " + donor.getFullName());
	assert donor.getParent() instanceof Nucleotide3D;
	Nucleotide3D nuc = (Nucleotide3D)(donor.getParent());
	char nChar = nuc.getSymbol().getCharacter();
	if (!NucleotideTools.isHDonor(nChar, donor.getName())) {
	    throw new RnaModelException("Provided atom cannot act as a hydrogen donor in a hydrogen bond.");
	}
	String[] adjacents = NucleotideTools.getAdjacentAtomNames(donor.getName());
	List<String> ladj = new ArrayList<String>();
	for (int i = 0; i < adjacents.length; ++i) {
	    int idx = nuc.getIndexOfChild(adjacents[i]);
	    if (idx >= 0) {
		Object3D a = nuc.getChild(idx);
		if (a.distance(donor) < ADJACENCY_CUTOFF) {
		    ladj.add(adjacents[i]);
		}
	    }
	}
	adjacents = new String[ladj.size()];
	// System.out.println("Adjacent atoms for " + nuc.getParent().getName() + " " + nChar + " " + nuc.getAssignedNumber() + " " + (nuc.getPos()+1) + " " + nuc.getName() + " " + donor.getName() + " : ");
	for (int i = 0; i < adjacents.length; ++i) {
	    adjacents[i] = ladj.get(i);
	    // System.out.print(" " + adjacents[i] + " " + nuc.getChild(adjacents[i]).getPosition());
	}
	// System.out.println();
	Vector3D result = donor.getPosition();
	Vector3D dir = null;
	if (adjacents.length == 1) {
	    int idx = nuc.getIndexOfChild(adjacents[0]);
	    if (idx < 0) {
		throw new RnaModelException("Nucleotide does not possess atom of type " + adjacents[0]);
	    }
	    Object3D adj = nuc.getChild(idx);
	    dir = donor.getPosition().minus(adj.getPosition());
	    if (dir.length() == 0.0) {
		throw new RnaModelException("Zero distance encountered between hydrogen bond donor and adacent atom.");
	    }
	} else if (adjacents.length == 2) {
	    int idx = nuc.getIndexOfChild(adjacents[0]);
	    if (idx < 0) {
		throw new RnaModelException("Nucleotide does not possess atom of type " + adjacents[0]);
	    }
	    Object3D adj = nuc.getChild(idx);
	    assert(adj instanceof Atom3D);
	    int idx2 = nuc.getIndexOfChild(adjacents[1]);
	    if (idx2 < 0) {
		throw new RnaModelException("Nucleotide does not possess atom of type " + adjacents[1]);
	    }
	    Object3D adj2 = nuc.getChild(idx2);
	    assert(adj2 instanceof Atom3D);
	    Vector3D dir1 = donor.getPosition().minus(adj.getPosition());
	    if (dir1.length() == 0.0) {
		throw new RnaModelException("Zero distance encountered between hydrogen bond donor and adacent atom.");
	    }
	    dir1.normalize();
	    Vector3D dir2 = donor.getPosition().minus(adj2.getPosition());
	    if (dir2.length() == 0.0) {
		throw new RnaModelException("Zero distance encountered between hydrogen bond donor and adacent atom.");
	    }
	    dir2.normalize();
	    dir = dir1.plus(dir2);
	} else {
	    System.out.println("Strange, there are too many covalently bond adjacent atoms " + adjacents.length);
	}
	assert dir != null;
	dir.normalize();
	dir.scale(H_BOND_LENGTH);
	result.add(dir);
	if (!validateHydrogenPositionEstimate(donor, result)) {
	    System.out.println("Warning: Bad hydrogen bond estimation: " + donor.getFullName() + " " + donor.getPosition() + " H: " + result);
	}
	//	assert (validateHydrogenPositionEstimate(donor, result));
	return result;
    }

}