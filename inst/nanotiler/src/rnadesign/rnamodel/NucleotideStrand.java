package rnadesign.rnamodel;

public interface NucleotideStrand extends BioPolymer {

    /** returns highest index used in nucleotide name */
    int findHighestNameIndex();
    
}
