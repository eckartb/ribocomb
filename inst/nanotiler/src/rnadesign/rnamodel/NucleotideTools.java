package rnadesign.rnamodel;

import tools3d.Vector3D;
import tools3d.objects3d.*;
import rnadesign.rnacontrol.SimpleLinkController;
import chemistrytools.*;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;

import java.util.logging.*;
public class NucleotideTools {

    private static Logger log = Logger.getLogger(PackageConstants.LOGFILE_DEFAULT);

    public static final double HYDROGEN_BOND_DIST_MAX = 1.2;
    public static final String[] backboneNames = { "P", "O1P", "O2P", "O5*", "C5*", "C4*", "C3*", "O3*","O4*","C2*","C1*","O2*"};
    public static final String[] backboneAngleNames = { "P", "O5*", "C5*", "C4*", "C3*", "O3*"  };
    public static final String[] sugarNames = { "N1", "C2", "O2", "N3", "C4","O4","C5","C6","N4","N9","C8","N7","O6","N2","N6"}; // TODO verify

    public static final String[] H_DONORS_A = { "N6", "O2*"}; // add Hoogsteen, Sugar
    public static final String[] H_DONORS_C = { "N4", "O2*"};
    public static final String[] H_DONORS_G = { "N1", "N2", "O2*"};
    public static final String[] H_DONORS_U = { "N3", "O2*"};

    public static final String[] H_ACCEPTORS_A = { "N1", "N3", "N7" };
    public static final String[] H_ACCEPTORS_C = { "O2", "N3"};
    public static final String[] H_ACCEPTORS_G = { "N3", "O6", "N7"};
    public static final String[] H_ACCEPTORS_U = { "O2", "O4"};

    public static final String[] EDGE_WATSONCRICK_A = { "N1", "N6" }; // C2 and C6 are not included
    public static final String[] EDGE_WATSONCRICK_C = { "O2", "N3", "N4" }; // C2 and C6 are not included
    public static final String[] EDGE_WATSONCRICK_G = { "N1", "N2" , "O6"}; // C atoms are not included
    public static final String[] EDGE_WATSONCRICK_U = { "O2", "N3", "O4" }; // C atoms are not included

    public static final String[] EDGE_HOOGSTEEN_A = { "N6", "N7" }; // C2 and C6 are not included
    public static final String[] EDGE_HOOGSTEEN_C = { "N4" }; // C2 and C6 are not included
    public static final String[] EDGE_HOOGSTEEN_G = { "O6", "N7"}; // C atoms are not included
    public static final String[] EDGE_HOOGSTEEN_U = { "O4" }; // C atoms are not included

    public static final String[] EDGE_SUGAR_A = { "N3", "O2*" }; // C2 and C6 are not included
    public static final String[] EDGE_SUGAR_C = { "O2", "O2*"}; // C2 and C6 are not included
    public static final String[] EDGE_SUGAR_G = { "N2", "N3", "O2*"}; // C atoms are not included; N2 is also part of WC edge
    public static final String[] EDGE_SUGAR_U = { "O2", "O2*"}; // C atoms are not included

    public static final String[] purineTripodNames = {"N9", "C4", "C8"};
    public static final String[] pyrimidineTripodNames = {"N1", "C2", "C6"};

    public static Map<String,String[]> nucleotideMap;

    public static Object3DSet findHDonors(Nucleotide3D nuc) throws RnaModelException {
	String[] names = null;
	char nName = nuc.getSymbol().getCharacter(); // 1-character symbol of nucleotide
	if (nName == 'A') {
	    names = H_DONORS_A;
	} else if (nName == 'C') {
	    names = H_DONORS_C;
	} else if (nName == 'G') {
	    names = H_DONORS_G;
	} else if (nName == 'U') {
	    names = H_DONORS_U;
	} else {
	    throw new RnaModelException("Unssuported nucleotide type: " + nName + " for " + nuc.getFullName());
	}
	Object3DSet result = new SimpleObject3DSet();
	for (int i = 0; i < names.length; ++i) {
	    int idx = nuc.getIndexOfChild(names[i]);
	    if (idx >= 0) {
		result.add(nuc.getChild(idx));
	    } else {
		throw new RnaModelException("Missing atom " + names[i] + " in nucleotide " + nuc.getFullName());
	    }
	}
	return result;
    }

    /** Determine if two blunt-end base pairs are adjacent.
     * @param bp1_1 One nucleotide belonging to first base pair. 
     * @param bp1_2 second nucleotide belonging to first base pair.
     * @param bp2_1 One nucleotide belonging to first base pair. 
     * @param bp2_2 second nucleotide belonging to first base pair.
     */
    public static boolean isAdjacentBluntBasePairPair(Nucleotide3D bp1_1, Nucleotide3D bp1_2, Nucleotide3D bp2_1, Nucleotide3D bp2_2, String fivePrimeAtom, String threePrimeAtom, double cutoff) {
	assert bp1_1 != bp1_2;
	assert bp1_1 != bp2_1;
	assert bp1_1 != bp2_2;
	assert bp1_2 != bp2_1;
	assert bp1_2 != bp2_2;
	assert bp2_1 != bp2_2;
	assert bp1_1.isTerminal();
	assert bp1_2.isTerminal();
	assert bp2_1.isTerminal();
	assert bp2_2.isTerminal();

	if (bp1_1.isFirst() && bp1_2.isLast()) {
	    // normal case
	} else if (bp1_1.isLast() && bp1_2.isFirst()) {
	    Nucleotide3D b = bp1_1;
	    bp1_1 = bp1_2; // swap
	    bp1_2 = b; 
	} else {
	    assert false; // should never occur!
	}
	if (bp2_1.isFirst() && bp2_2.isLast()) {
	    // normal case
	} else if (bp2_1.isLast() && bp2_2.isFirst()) {
	    Nucleotide3D b = bp2_1;
	    bp2_1 = bp2_2; // swap
	    bp2_2 = b; 
	} else {
	    assert false; // should never occur!
	}
	assert bp1_1.isFirst();
	assert bp1_2.isLast();
	assert bp2_1.isFirst();
	assert bp2_2.isLast();
	assert bp1_1.getIndexOfChild(fivePrimeAtom) >= 0;
	assert bp1_2.getIndexOfChild(threePrimeAtom) >= 0;
	assert bp2_1.getIndexOfChild(fivePrimeAtom) >= 0;
	assert bp2_2.getIndexOfChild(threePrimeAtom) >= 0;
	if (bp1_2.getChild(threePrimeAtom).distance(bp2_1.getChild(fivePrimeAtom)) > cutoff) {
	    return false;
	}
	if (bp1_1.getChild(fivePrimeAtom).distance(bp2_2.getChild(threePrimeAtom)) > cutoff) {
	    return false;
	}
	return true;
    }

    public static Object3DSet findHAcceptors(Nucleotide3D nuc) throws RnaModelException {
	String[] names = null;
	char nName = nuc.getSymbol().getCharacter(); // 1-character symbol of nucleotide
	if (nName == 'A') {
	    names = H_ACCEPTORS_A;
	} else if (nName == 'C') {
	    names = H_ACCEPTORS_C;
	} else if (nName == 'G') {
	    names = H_ACCEPTORS_G;
	} else if (nName == 'U') {
	    names = H_ACCEPTORS_U;
	} else {
	    throw new RnaModelException("Unssuported nucleotide type: " + nName + " for " + nuc.getFullName());
	}
	Object3DSet result = new SimpleObject3DSet();
	for (int i = 0; i < names.length; ++i) {
	    int idx = nuc.getIndexOfChild(names[i]);
	    if (idx >= 0) {
		result.add(nuc.getChild(idx));
	    } else {
		throw new RnaModelException("Missing atom " + names[i] + " in nucleotide " + nuc.getFullName());
	    }
	}
	return result;
    }

    public static boolean isHDonor(char nName, String atomName) throws RnaModelException {
	String[] names = null;
	if (nName == 'A') {
	    names = H_DONORS_A;
	} else if (nName == 'C') {
	    names = H_DONORS_C;
	} else if (nName == 'G') {
	    names = H_DONORS_G;
	} else if (nName == 'U') {
	    names = H_DONORS_U;
	} else {
	    throw new RnaModelException("Unsupported nucleotide name " + nName);
	}
	for (int i = 0; i < names.length; ++i) {
	    if (names[i].equals(atomName)) {
		return true;
	    }
	}
	return false;
    }

    public static boolean isHAcceptor(char nName, String atomName) throws RnaModelException {
	String[] names = null;
	if (nName == 'A') {
	    names = H_ACCEPTORS_A;
	} else if (nName == 'C') {
	    names = H_ACCEPTORS_C;
	} else if (nName == 'G') {
	    names = H_ACCEPTORS_G;
	} else if (nName == 'U') {
	    names = H_ACCEPTORS_U;
	} else {
	    throw new RnaModelException("Unsupported nucleotide name " + nName);
	}
	for (int i = 0; i < names.length; ++i) {
	    if (names[i].equals(atomName)) {
		return true;
	    }
	}
	return false;
    }

    public static boolean isWatsonCrickEdge(char nName, String atomName) throws RnaModelException {
	String[] names = null;
	if (nName == 'A') {
	    names = EDGE_WATSONCRICK_A;
	} else if (nName == 'C') {
	    names = EDGE_WATSONCRICK_C;
	} else if (nName == 'G') {
	    names = EDGE_WATSONCRICK_G;
	} else if (nName == 'U') {
	    names = EDGE_WATSONCRICK_U;
	} else {
	    throw new RnaModelException("Unsupported nucleotide name " + nName);
	}
	for (int i = 0; i < names.length; ++i) {
	    if (names[i].equals(atomName)) {
		return true;
	    }
	}
	return false;
    }

    public static boolean isHoogsteenEdge(char nName, String atomName) throws RnaModelException {
	String[] names = null;
	if (nName == 'A') {
	    names = EDGE_HOOGSTEEN_A;
	} else if (nName == 'C') {
	    names = EDGE_HOOGSTEEN_C;
	} else if (nName == 'G') {
	    names = EDGE_HOOGSTEEN_G;
	} else if (nName == 'U') {
	    names = EDGE_HOOGSTEEN_U;
	} else {
	    throw new RnaModelException("Unsupported nucleotide name " + nName);
	}
	for (int i = 0; i < names.length; ++i) {
	    if (names[i].equals(atomName)) {
		return true;
	    }
	}
	return false;
    }

    public static boolean isSugarEdge(char nName, String atomName) throws RnaModelException {
	String[] names = null;
	if (nName == 'A') {
	    names = EDGE_SUGAR_A;
	} else if (nName == 'C') {
	    names = EDGE_SUGAR_C;
	} else if (nName == 'G') {
	    names = EDGE_SUGAR_G;
	} else if (nName == 'U') {
	    names = EDGE_SUGAR_U;
	} else {
	    throw new RnaModelException("Unsupported nucleotide name " + nName);
	}
	for (int i = 0; i < names.length; ++i) {
	    if (names[i].equals(atomName)) {
		return true;
	    }
	}
	return false;
    }

    public static boolean isPhosphate(Atom3D atom) {
	return AtomTools.getElementChar(atom) == 'P';
    }

    public static String getBackboneAngleAtomName1(int angleId) {
	return backboneAngleNames[angleId/2];
    }

    public static String getBackboneAngleAtomName2(int angleId) {
	return (backboneAngleNames[1 + (angleId/2)] );
    }

    /** Returns approximate direction vector of backbone. Avoid usage of Phosphor, because it is sometimes missing. */
    public static Vector3D getBackboneDirection(Nucleotide3D nuc) throws RnaModelException {
	Object3D obj1 = nuc.getChild("C5*");
	Object3D obj2 = nuc.getChild("O3*");
	if ((obj1 == null) || (obj2 == null)) {
	    throw new RnaModelException("Could not find atoms C5* or O3* in " + nuc.getFullName());
	}
	return obj2.getPosition().minus(obj1.getPosition());
    }

    /** Returns bending angle between two residues */
    public static double getBackboneAngle(Nucleotide3D n1, Nucleotide3D n2) throws RnaModelException {
	return getBackboneDirection(n1).angle(getBackboneDirection(n2));
    }

    /** returns true if sugar atom */
    public static boolean isSugarAtom(Atom3D atom) {
	String name = atom.getName();
	char lastChar = name.charAt(name.length()-1);
	return ( lastChar == '*' ) || ( lastChar == '\'' );
    }
    
    public static boolean isCovalentlyBonded(Nucleotide3D n, Atom3D atom1, Atom3D atom2) {
	//assert (n == atom1.getParent()) && (n == atom2.getParent());

	boolean result = false;
	try {
	    result = isCovalentlyBonded(n,atom1.getName(),atom2.getName());
	}
	catch (RnaModelException ex) {
	    assert false; // should never happen
	}
	return result;
    }

    public static boolean isCovalentlyBonded(Nucleotide3D n, String atomName1, String atomName2) 
    	throws RnaModelException {
	int id1 = n.getIndexOfChild(atomName1);
    	if(id1 < 0) {
    		throw new RnaModelException(atomName1 + " is not a member of " + n);
	}
	int id2 = n.getIndexOfChild(atomName2);
    	if(id2 < 0) {
    		throw new RnaModelException(atomName2 + " is not a member of " + n);
	}
	Atom3D a1 = (Atom3D)(n.getChild(id1));
    	Atom3D a2 = (Atom3D)(n.getChild(id2));
    	if(a1.getLinks().find(a1, a2) != null) {
    		return true;
	}

     	if(a1.getPosition().distance(a2.getPosition()) > 3.0) {
	    return false;
	}
	if(nucleotideMap==null){	
	    generateNucleotideMap();
	}

	if(nucleotideMap.containsKey(atomName1) &&  nucleotideMap.containsKey(atomName2)){
	    String[] bondedAtoms = (String[])nucleotideMap.get(atomName1);

	    for(int i=0;i<bondedAtoms.length;i++){
		if(bondedAtoms[i].equals(atomName2)){
		    return true;
		}
	    }
	} // check for hydrogens without mapping, just distance:
	else 
{	    boolean h1Flag = AtomTools.isHydrogen(a1);
	    boolean h2Flag = AtomTools.isHydrogen(a2);
	    if ((h1Flag ^ h2Flag) && (a1.distance(a2) < HYDROGEN_BOND_DIST_MAX)) { // exclusive OR test!
		return true;
	    } 
	}

    	return false;
    }

    public static void addCovalentAtomLinks(Nucleotide3D nuc1, Nucleotide3D nuc2,  LinkSet links){
	    for(int i = 0; i < nuc1.getAtomCount();i++){
		Atom3D atom1 = nuc1.getAtom(i);
		for(int n=i+1; n < nuc1.getAtomCount(); n++){
		    Atom3D atom2 = nuc1.getAtom(n);
		    if(links.find(atom1,atom2) == null){
			if(isCovalentlyBonded(nuc1,atom1,atom2)){
			    Link link = new SimpleLink(atom1, atom2);
			    if(!links.contains(link)){
				links.add(link);
			    }
			}
		    }
		}
	    }
	    if(nuc1.getPos() > 0){
		Object3D obj1 = nuc1.getChild("P");
		Object3D obj2 = nuc2.getChild("O3*");
		if(obj1 != null  && obj2 != null){
		    if(links.find(obj1,obj2) == null){
			Link specialLink = new SimpleLink(obj1,obj2);
			links.add(specialLink);
		    }
		}
	    }
    }

    /** Generates links between atoms without checking if links already exist */
    public static void addCovalentAtomLinksFast(Nucleotide3D nuc1, Nucleotide3D nuc2,  LinkSet links){
	    for(int i = 0; i < nuc1.getAtomCount();i++){
		Atom3D atom1 = nuc1.getAtom(i);
		for(int n=i+1; n < nuc1.getAtomCount(); n++){
		    Atom3D atom2 = nuc1.getAtom(n);
		    if(isCovalentlyBonded(nuc1,atom1,atom2)){
			Link link = new SimpleLink(atom1, atom2);
			if(!links.contains(link)){
			    links.add(link);
			}
		    }
		}
	    }
	    if(nuc1.getPos() > 0){
		Object3D obj1 = nuc1.getChild("P");
		Object3D obj2 = nuc2.getChild("O3*");
		if(obj1 != null  && obj2 != null){
		    if(links.find(obj1,obj2) == null){
			Link specialLink = new SimpleLink(obj1,obj2);
			links.add(specialLink);
		    }
		}
	    }
    }

    public static void addCovalentAtomLinks(Nucleotide3D nuc1, LinkSet links){
	int pos1 = nuc1.getPos();
	if(pos1>0){
	    Nucleotide3D nuc2 = (Nucleotide3D)nuc1.getParent().getChild(pos1-1);
	    addCovalentAtomLinks(nuc1, nuc2, links);
	    return;
	}

	for(int i = 0; i < nuc1.getAtomCount();i++){
	    Atom3D atom1 = nuc1.getAtom(i);
	    for(int n=i+1; n < nuc1.getAtomCount(); n++){
		Atom3D atom2 = nuc1.getAtom(n);
		if(links.find(atom1,atom2) == null){
		    if(isCovalentlyBonded(nuc1,atom1,atom2)){
			Link link = new SimpleLink(atom1, atom2);
		        links.add(link);
		    }			
		}
	    }
	}
    }

    /** Generates links between atoms without checking if links already exist */
    public static void addCovalentAtomLinksFast(Nucleotide3D nuc1, LinkSet links){
	int pos1 = nuc1.getPos();
	if(pos1>0){
	    Nucleotide3D nuc2 = (Nucleotide3D)nuc1.getParent().getChild(pos1-1);
	    addCovalentAtomLinks(nuc1, nuc2, links);
	    return;
	}

	for(int i = 0; i < nuc1.getAtomCount();i++){
	    Atom3D atom1 = nuc1.getAtom(i);
	    for(int n=i+1; n < nuc1.getAtomCount(); n++){
		Atom3D atom2 = nuc1.getAtom(n);
		if(isCovalentlyBonded(nuc1,atom1,atom2)){
		    Link link = new SimpleLink(atom1, atom2);
		    links.add(link);
		}			
	    }
	}
    }

    public static LinkSet getCovalentAtomLinks(Nucleotide3D nuc1){
	LinkSet links = new SimpleLinkController();
	addCovalentAtomLinks(nuc1,links);
	return links;
    }

    /*creates a hash table based on the nanotiler naming standard for nucleotides*/   
    private static void generateNucleotideMap(){

	nucleotideMap = new HashMap<String, String[]>();
	//add names according to nucleotide
	nucleotideMap.put("N9",new String[]{"C8","C4","C1*"});
	nucleotideMap.put("C8",new String[]{"N7","N9"});
	nucleotideMap.put("N7",new String[]{"C8","C5"});
	nucleotideMap.put("C5",new String[]{"N7","C4","C6"});
	nucleotideMap.put("N1",new String[]{"C6","C2","C1*"});
	nucleotideMap.put("N3",new String[]{"C2","C4"});
	nucleotideMap.put("C4",new String[]{"N3","C5","N9","N4","O4"});
	nucleotideMap.put("C6",new String[]{"C5","N6","N1","O6"});
	nucleotideMap.put("N6",new String[]{"C6"});
	nucleotideMap.put("C2",new String[]{"N1","N3","N2","O2"});
	nucleotideMap.put("O6",new String[]{"C6"});
	nucleotideMap.put("N2",new String[]{"C2"});
	nucleotideMap.put("C5",new String[]{"C6","C4"});
	nucleotideMap.put("O2",new String[]{"C2"});
	nucleotideMap.put("N4",new String[]{"C4"});
	nucleotideMap.put("O4",new String[]{"C4"});
	//add names for backbone
	nucleotideMap.put("O1P",new String[]{"P"});
	nucleotideMap.put("P",new String[]{"O1P","O5*","O2P"});
	nucleotideMap.put("O2P",new String[]{"P"});
	nucleotideMap.put("O5*",new String[]{"P","C5*"});
	nucleotideMap.put("C5*",new String[]{"O5*","C4*"});
	nucleotideMap.put("C4*",new String[]{"C5*","O4*","C3*"});
	nucleotideMap.put("C3*",new String[]{"C4*","O3*","C2*"});
	nucleotideMap.put("O3*",new String[]{"C3*"});
	nucleotideMap.put("C2*",new String[]{"C3*","O2*","C1*"});
	nucleotideMap.put("O2*",new String[]{"C2*"});
	nucleotideMap.put("C1*",new String[]{"N1","N9","O4*","C2*"});
	nucleotideMap.put("O4*",new String[]{"C1*","C4*"});

    }

    /** checks if RNA backbone. Overlap with isSugarAtom */
    public static boolean isBackboneAtom(Atom3D atom) {
	String name = atom.getName();
	for (int i = 0; i < backboneNames.length; ++i) {
	    if (backboneNames[i].equals(name)) {
		return true;
	    }
	}
	return false;
    }
    
    public static String[] getAdjacentAtomNames(String atomName) {
	if(nucleotideMap==null){	
	    generateNucleotideMap();
	}
	if (nucleotideMap.containsKey(atomName)) {
	    return nucleotideMap.get(atomName);
	}
	return null;
    }

    /** Tests of nucleotide base. returns true, if not part of sugar or backbone. Assumes that it is part of
     * the nucleotide at all. 
     */
    public static boolean isBaseAtom(Atom3D atom) {
	return ! (isSugarAtom(atom) || isBackboneAtom(atom));
    }

    /** returns true if nucleotide is first in strand */
    public static boolean is5PrimeNucleotide(Nucleotide3D nuc) {
	if (nuc.getParent() == null) {
	    return false;
	}
	return nuc.getSiblingId() == 0;
    }

    public static boolean isAdjacentInternal(Nucleotide3D n1, Nucleotide3D n2) {
	return (n1.getParent() == n2.getParent()) && (Math.abs(n1.getPos()-n2.getPos()) == 1);
    }

    public static boolean isAdjacentAssigned(Nucleotide3D n1, Nucleotide3D n2) {
	return (n1.getParent() == n2.getParent()) && (Math.abs(n1.getAssignedNumber()-n2.getAssignedNumber()) == 1);
    }

    /** returns true if nucleotide is last in strand */
    public static boolean is3PrimeNucleotide(Nucleotide3D nuc) {
	if (nuc.getParent() == null) {
	    return false;
	}
	return nuc.getSiblingId() == (nuc.getParent().size()-1);
    }

    /** returns true if nucleotide is first in strand */
    public static boolean isAtStrandEnd(Nucleotide3D nuc) {
	return is5PrimeNucleotide(nuc) || is3PrimeNucleotide(nuc);

    }

    /** Adds a mising O3* atom */
    public static boolean addMissingO3Prime(Nucleotide3D nuc) throws RnaModelException {
	if (nuc.getIndexOfChild("O3*") >= 0) {
	    return false;
	}
	if ((nuc.getIndexOfChild("C3*") < 0) || (nuc.getIndexOfChild("C4*") < 0)
	    || (nuc.getIndexOfChild("C2*") < 0)) {
	    throw new RnaModelException("Cannot add O3*, because on of the following atoms is missing too: C3*, C2*, C4* ." + nuc.toString());
	}
	Vector3D vc2 = nuc.getChild("C2*").getPosition();
	Vector3D vc3 = nuc.getChild("C3*").getPosition();
	Vector3D vc4 = nuc.getChild("C4*").getPosition();
	Vector3D dir1 = vc3.minus(vc4);
	Vector3D dir2 = vc3.minus(vc2);
	Vector3D dir = dir1.plus(dir2);
	if (dir.length() == 0) {
	    throw new RnaModelException("Error in adding O3*: zero length direction vector!");
	}
	dir.normalize();
	dir.scale(1.43); // single bond C-O bond length (http://www.science.uwaterloo.ca/~cchieh/cact/c120/bondel.html)
	assert(Math.abs(dir.length() - 1.43) < 0.001);
	Vector3D pos = vc3.plus(dir);
	Atom3D o3prime = new Atom3D(pos);
	o3prime.setElement(PeriodicTableImp.generateDefaultChemicalElementByValency(2));
	o3prime.setName("O3*");
	nuc.insertChild(o3prime);
	assert (o3prime.getPosition().minus(vc3).length() < 2.0);
	return true;
    }

    /** removes O1P AND O2P and P atom or synonyms
     * @param nuc Nucleotide to be working on. 
     * @returns function returns true if atoms were removed, false, otherwise*/
    public static boolean removePhosphateGroup(Nucleotide3D nuc) {
	boolean result = false;
	if (nuc.getIndexOfChild("P") >= 0) {
	    result = true;
	    nuc.removeChild(nuc.getIndexOfChild("P"));
	}
	if (nuc.getIndexOfChild("O1P") >= 0) {
	    result = true;
	    nuc.removeChild(nuc.getIndexOfChild("O1P"));
	}
	if (nuc.getIndexOfChild("OP1") >= 0) {
	    result = true;
	    nuc.removeChild(nuc.getIndexOfChild("OP1"));
	}
	if (nuc.getIndexOfChild("O2P") >= 0) {
	    result = true;
	    nuc.removeChild(nuc.getIndexOfChild("OP2"));
	}
	if (nuc.getIndexOfChild("OP2") >= 0) {
	    result = true;
	    nuc.removeChild(nuc.getIndexOfChild("OP2"));
	}
	return result;
    }

    /** adds O1P AND O2P to P atom or nothing */
    public static boolean addMissingOP(Nucleotide3D nuc) throws RnaModelException {
	log.info("Trying to add O1P, O2P to nucleotide: " + nuc.toString());
	if ((nuc.getIndexOfChild("O1P") >= 0) || (nuc.getIndexOfChild("OP1") >= 0)) {
	    return false;
	}
	if ((nuc.getIndexOfChild("O2P") >= 0) || (nuc.getIndexOfChild("OP2") >= 0))  {
	    return false;
	}
	if (nuc.getSiblingId() < 1) {
	    return false;
	}
	Nucleotide3D prevNuc = (Nucleotide3D)(nuc.getParent().getChild(nuc.getSiblingId() - 1));
	if ((nuc.getIndexOfChild("P") < 0) || (nuc.getIndexOfChild("O5*") < 0)
	    || (prevNuc.getIndexOfChild("O3*") < 0)) {
	    throw new RnaModelException("Cannot add O1P, O2P, because on of the following atoms is missing too: P, O5*, O3* ." + nuc.toString());
	}
	Vector3D vc2 = prevNuc.getChild("O3*").getPosition();
	Vector3D vc3 = nuc.getChild("P").getPosition();
	Vector3D vc4 = nuc.getChild("O5*").getPosition();
	Vector3D vcOrth = vc2.cross(vc4);
	if (vcOrth.length() == 0.0) {
	    throw new RnaModelException("Orthogonal vector could not be normalized!");
	}
	vcOrth.normalize();
	Vector3D dir1 = vc3.minus(vc4);
	Vector3D dir2 = vc3.minus(vc2);
	Vector3D dir = dir1.plus(dir2);
	if (dir.length() == 0) {
	    throw new RnaModelException("Error in adding O3*: zero length direction vector!");
	}
	dir.normalize();
	double bLength = 1.4;
	double tetAng = Math.toRadians(0.5 * 109.0);
	dir.scale(Math.cos(tetAng) * bLength);
	vcOrth.scale(Math.sin(tetAng) * bLength);
	Vector3D pos = vc3.plus(dir).plus(vcOrth);
	Vector3D pos2 = vc3.plus(dir).minus(vcOrth);
	Atom3D o1p = new Atom3D(pos);
	Atom3D o2p = new Atom3D(pos2);
	o1p.setElement(PeriodicTableImp.generateDefaultChemicalElementByValency(2));
	o1p.setName("O1P");
	o2p.setName("O2P");
	nuc.insertChild(o1p);
	nuc.insertChild(o2p);
	log.info("Added missing atoms: " + o1p.toString() + " " + o2p.toString() + " to residue: " 
		 + nuc.toString());
	assert (o1p.getPosition().minus(vc3).length() < 2.0);
	assert (o2p.getPosition().minus(vc3).length() < 2.0);
	return true;
    }

    public static List<String> findMissingAtoms(Nucleotide3D nuc) {
	List<String> result = new ArrayList<String>();
	for (String name : backboneNames) {
	    if ((nuc.getIndexOfChild(name) < 0)) {
		result.add(name);
	    }
	}
	return result;
    }
    
    public static boolean isPurine(Nucleotide3D nuc) {
	return nuc.getIndexOfChild("N9") >= 0;
    }

    public static Atom3D getGlycosilicBondAtom (Nucleotide3D nuc) throws RnaModelException {
	if (isPurine(nuc)) {
	    if (nuc.getIndexOfChild("N9") < 0) {
		throw new RnaModelException("Cannot find N9 atom in nucleotide " + nuc.getFullName());
	    }
	    return (Atom3D)(nuc.getChild("N9"));
	}
	if (nuc.getIndexOfChild("N1") < 0) {
	    throw new RnaModelException("Cannot find N1 atom in nucleotide " + nuc.getFullName());
	}
	return (Atom3D)(nuc.getChild("N1"));
    }

    public static Vector3D computeGlycosilicBondVector (Nucleotide3D nuc) throws RnaModelException {
	Atom3D nglyc = getGlycosilicBondAtom(nuc);
	int idx2 = nuc.getIndexOfChild("C1*"); // TODO careful with prime versus star
	if (idx2 < 0) {
	    throw new RnaModelException("Cannot find N1 C1* in nucleotide " + nuc.getFullName());
	}
	Atom3D c1 = (Atom3D)( nuc.getChild(idx2));
	Vector3D result = nglyc.getPosition().minus(c1.getPosition());
	return result;
    }

    public static double computeGlycosilicBondAngle (Nucleotide3D nuc1, Nucleotide3D nuc2) throws RnaModelException {
	Vector3D g1 = computeGlycosilicBondVector(nuc1);
	Vector3D g2 = computeGlycosilicBondVector(nuc2);
	return g1.angle(g2);	
    }

    /** Computes plane normal using 3 points on plane. Use p1 as offset, p2-p1 and p3-p1 as direction vectors. Not normalized. */
    private static Vector3D computeBasePlaneNormal(Vector3D p1, Vector3D p2, Vector3D p3) {
	Vector3D v1 = p2.minus(p1);
	Vector3D v2 = p3.minus(p1);
	Vector3D result = v1.cross(v2);
	return result;
    }

    /** Uses plane offset: position of atom returned by getGlycoslicBondAtom. Returns plane normal. Vector points in direction
     * backbone of strand  */
    public static Vector3D computeBasePlaneNormal(Nucleotide3D nuc, String[] atomNames) throws RnaModelException{
	assert (atomNames.length == 3);
	Vector3D result = null;
	try {
	    Object3DSet atoms = Object3DSetTools.getChildren(nuc, atomNames);
	    assert(atoms.size() == 3);
	    result = computeBasePlaneNormal(atoms.get(0).getPosition(), 
					    atoms.get(1).getPosition(),
					    atoms.get(2).getPosition());
	} catch (Object3DException oe) {
	    throw new RnaModelException(oe.getMessage());
	}
	assert result != null;
	if (result.length() == 0.0) {
	    throw new RnaModelException("Could not compute normal vector of base (has zero length): " + nuc.getFullName());
	}
	Vector3D backboneDir = getBackboneDirection(nuc);
	if (backboneDir.dot(result) < 0.0) {
	    result.scale(-1.0);
	}
	result.normalize();
	assert(result.length() > 0.99 && result.length() < 1.01);
	return result;
    }

    /** Uses plane offset: position of atom returned by getGlycosilicBondAtom. Returns plane normal */
    public static Vector3D computeBasePlaneNormal(Nucleotide3D nuc) throws RnaModelException {
	return isPurine(nuc) ? computeBasePlaneNormal(nuc, purineTripodNames) : computeBasePlaneNormal(nuc, pyrimidineTripodNames);
    }

}
