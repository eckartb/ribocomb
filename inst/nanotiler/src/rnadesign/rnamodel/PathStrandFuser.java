package rnadesign.rnamodel;

import generaltools.ResultWorker;
import generaltools.ScoreWrapper;
import java.io.PrintStream;
import java.util.*;
import java.util.logging.*;
import tools3d.objects3d.*;
import tools3d.Vector3D;

/** Algorithm for automated fusing of RNA strands disregarding junctions */
public class PathStrandFuser extends ResultWorker {

    /** Check if strands belong to different parents: */
    public static final int AVOID_NONE = 0;
    public static final int AVOID_SAME_PARENT = 1;

    /** Reassign nick for circular strands: */
    public static final int NICK_NONE = 0;
    public static final int NICK_CIRCULAR = 1;

    /** For specified helix end, which strand ends to extend: */
    public static final int EXTEND_OUTGOING = 1;
    public static final int EXTEND_INCOMING = 2;
    public static final int EXTEND_BOTH = 3;

    private Logger log = Logger.getLogger(PackageConstants.LOGFILE_DEFAULT);

    // some helper words used to indicate status of strands in properties object
    public static final int STRAND_FUSION_MAX = 20;
    private static final double STRAND_DIST_MAX = 7.0; // maxmum distance between connecting strands running i
    private static final double STRAND_ANGLE_MAX = 0.75 * Math.PI; // minimum angle between connecting strands running i
    private static final double ANGLE_PENALTY = 50.0; // bad angle between connecting strands is penalized by this "distance" 
    private static final double STRAND_CONNECTION_SCORE_CUTOFF = 50.0;
    private static final String TMP_TOKEN = "__RINGFUSE_STATUS";
    private static final String TMP_TOKEN_OCCUPIED = "occupied";
    // private double distanceCutoff = 10.0; // max distance between two residues to be fused
    public int mode = AVOID_SAME_PARENT;
    public int nickMode = NICK_CIRCULAR;
    public int extendMode = EXTEND_BOTH; // EXTEND_OUTGOING;
    private double scoreCutoff = STRAND_CONNECTION_SCORE_CUTOFF;
    private static boolean debugMode = true;
    private List<RnaStrand> strands = new ArrayList<RnaStrand>();
    public int xlen = 0; // used for moving nick in case of circular strands
    // private List<RnaStrand> fusedStrands; 
    // private Object3D loops = new SimpleObject3D("loops");
    private RnaStrand resultStrand = null;
    private List<RnaStrand> deletableStrands = new ArrayList<RnaStrand>();
    public PathStrandFuser(List<RnaStrand> strands) {
	this.strands.addAll(strands); // new container
	// this.strands = cleanStrands(this.strands); // FIXIT
	log.info("Initial strands: " + strands.size() + " cleaned version: " + this.strands.size());
    }

    public RnaStrand getResultStrand() { return resultStrand; }

    public List<RnaStrand> getDeletableStrands() { return deletableStrands; }

    public void runInternal() {
	System.out.println("# Starting PathStrandFuser.runInternal: automatic strand fusing for " + strands.size() + " strands and potential circular shift by " + xlen);
	deletableStrands.clear();
	resultStrand = (RnaStrand)(strands.get(0).cloneDeep());
	int nres0 = resultStrand.getResidueCount();
	deletableStrands.add(strands.get(0));
	String paths = "";
	for (int i = 0; i < strands.size(); ++i) {
	    paths = paths + " " + strands.get(i).getFullName();
	}
	System.out.println("# all strands considered for fusing: " + paths);
        Properties properties = new Properties();
	// for each junction class: pick first element
	boolean[] flags = new boolean[strands.size()];
	for (int i = 0; i < flags.length; ++i) {
	    flags[i] = true;
	}
	flags[0] = false; // strands 0 has already been added
	int bestI = 0;
	int lastI = 0; // last strand id to connect to
	// for (int i = 0; i < 1; ++i) { // i is 0: start always with first strand in list
	int i = 0;
	    boolean ok = true;
	    double bestScore = 1e10;
	    int bestJ = -1;
	    int round = 0;
	    while (ok) {
		++round;
		System.out.println("# PathStrandFuser.runInternal round " + round);
		ok = false;
		bestJ = -1;
		bestScore = 1e10;
		for (int j = 1; j < strands.size(); ++j) {
		    System.out.println("# PathStrandFuser.runInternal round " + round + " checking strand " + (j+1));
		    if (!flags[j]) {
			System.out.println("Strand " + strands.get(j).getName() + " is not available.");
			continue;
		    }
		    if (mode == AVOID_SAME_PARENT) {
			if (strands.get(lastI).getParent() != null
			    && strands.get(j).getParent() != null) {
			    // check if reference is same
			    if (strands.get(i).getParent().isProbablyEqual(strands.get(j).getParent())) {
				continue;
			    }
			}
		    }
		    try {
			double score = scoreStrandConnectivity(resultStrand, strands.get(j));
			System.out.println("# score for strand " + (j+1) 
					   + " : " + score + " : " + strands.get(j).getFullName() + " previous: " + strands.get(lastI).getFullName());
			if (score > scoreCutoff) {
			    continue; // ignore solution
			}
			if (score < bestScore) {
			    bestScore = score;
			    bestJ = j;
			    ok = true; // found at least one possibility
			}
		    }
		    catch (RnaModelException rne ) {
			// do nothing 
		    }
		}
		result = properties;
		if (bestScore < scoreCutoff && bestJ > 0) {
		    flags[bestJ] = false; // do not use again
		    log.info("Fusing strands " + strands.get(lastI).getFullName() + " and " + strands.get(bestJ).getFullName());
		    int prevlen = resultStrand.size();
		    System.out.println("# lengths of strands 1 and " + (bestJ+1) 
				       + " before fusing: " + resultStrand.size() 
				       + " " + strands.get(bestJ).size());
		    resultStrand.append(strands.get(bestJ)); // remove second strand
		    deletableStrands.add(strands.get(bestJ));
		    int newlen = resultStrand.size();
		    System.out.println("# lengths of strands 1 and " + (bestJ+1) 
				       + " after fusing: " + resultStrand.size() 
				       + " " + strands.get(bestJ).size());
		    if (newlen <= prevlen) { // object must have changed
			System.out.println("Warning: could not extend strand. Internal error!");
		    }
		    lastI = bestJ;
		} else {
		    System.out.println("Could not find any candidates for fusing strands: " + bestScore + " with cutoff " + scoreCutoff);
		}
	    }// while ok


	    if (extendMode == EXTEND_BOTH || extendMode == EXTEND_INCOMING) {
		lastI = 0;
		System.out.println("# PathStrandFuser.runInternal - starting reverse mode");
		ok = true;
	    while (ok) {
		++round;
		System.out.println("# PathStrandFuser.runInternal - incoming strand - round " + round);
		ok = false;
		bestJ = -1;
		bestScore = 1e10;
		for (int j = 1; j < strands.size(); ++j) {
		    System.out.println("# PathStrandFuser.runInternal round " + round + " checking strand " + (j+1));
		    if (!flags[j]) {
			System.out.println("Strand " + strands.get(j).getName() + " is not available.");
			continue;
		    }
		    if (mode == AVOID_SAME_PARENT) {
			if (strands.get(lastI).getParent() != null
			    && strands.get(j).getParent() != null) {
			    // check if reference is same
			    if (strands.get(i).getParent().isProbablyEqual(strands.get(j).getParent())) {
				continue;
			    }
			}
		    }
		    try {
			double score = scoreStrandConnectivity(strands.get(j),resultStrand);
			System.out.println("# reverse score for strand " + (j+1) 
					   + " : " + score + " : " + strands.get(j).getFullName() + " previous: " + strands.get(lastI).getFullName());
			if (score > scoreCutoff) {
			    continue; // ignore solution
			}
			if (score < bestScore) {
			    bestScore = score;
			    bestJ = j;
			    ok = true; // found at least one possibility
			}
		    }
		    catch (RnaModelException rne ) {
			// do nothing 
		    }
		}
		result = properties;
		if (bestScore < scoreCutoff && bestJ > 0) {
		    flags[bestJ] = false; // do not use again
		    log.info("Fusing strands " + strands.get(lastI).getFullName() + " and " + strands.get(bestJ).getFullName());
		    int prevlen = resultStrand.size();
		    System.out.println("# lengths of strands 1 and " + (bestJ+1) 
				       + " before fusing: " + resultStrand.size() 
				       + " " + strands.get(bestJ).size());
		    resultStrand.prepend(strands.get(bestJ)); // remove second strand
		    deletableStrands.add(strands.get(bestJ));
		    int newlen = resultStrand.size();
		    System.out.println("# lengths of strands 1 and " + (bestJ+1) 
				       + " after fusing: " + resultStrand.size() 
				       + " " + strands.get(bestJ).size());
		    if (newlen <= prevlen) { // object must have changed
			System.out.println("Warning: could not extend strand. Internal error!");
		    }
		    lastI = bestJ;
		} else {
		    System.out.println("Could not find any candidates for fusing strands: " + bestScore + " with cutoff " + scoreCutoff);
		}
	    }// while ok

	    } // if extendMode == EXTEND_BOTH etc.
	    

	    // } // old for loop over i
	    if (nickMode == NICK_CIRCULAR) {
		try {
		double circScore = scoreStrandConnectivity(
		   strands.get(lastI),resultStrand);
		if (circScore < scoreCutoff) {
		    System.out.println("# Trying to apply circular reassignment to sequence by " + nres0 + xlen + " residues.");
		    int resCount = resultStrand.getResidueCount();
		    RnaStrand tailSeq = (RnaStrand)(resultStrand.split(nres0 + xlen,resultStrand.getName() + "_circ"));
		    System.out.println("# temporatory sequence lengths: " + resultStrand.getResidueCount() + " tail: " +  tailSeq.getResidueCount());
		    if (resultStrand.getResidueCount() != (nres0+xlen)) {
			System.out.println("WARNING: Internal error in in PathStrandFuser: residue count does not match!");
		    }
		    tailSeq.append(resultStrand);
		    if (resCount != tailSeq.getResidueCount()) {
			System.out.println("WARNING: Internal error in PathStrandFuser - sequence length not constant");
		    } else {
			resultStrand = tailSeq;
		    }
		}
		} catch (RnaModelException rme) {
		    System.out.println("# exception while trying to apply circular reasssignment to sequence: " + rme.getMessage());
		}
	    }
	log.info("Finished PathStrandFused.runInternal!");
    }

    /** checks if strand is occupied */
    private boolean checkStrandAvailable(NucleotideStrand strand) {
	return strand.getProperty(TMP_TOKEN) != TMP_TOKEN_OCCUPIED;
    }

    public void setScoreCutoff(double value) { scoreCutoff = value; }

    /** marks strand as occupied */
    private void setStrandOccupied(NucleotideStrand strand) {
	strand.setProperty(TMP_TOKEN, TMP_TOKEN_OCCUPIED);
    }

    /** Scores how well a start strand can be appended with end strand. Zero: perfect score */
    private double scoreStrandConnectivity(RnaStrand startStrand, RnaStrand endStrand) throws RnaModelException {
	Nucleotide3D startStrandResidue = (Nucleotide3D)(startStrand.getResidue3D(startStrand.getResidueCount()-1));
	Nucleotide3D endStrandResidue = (Nucleotide3D)(endStrand.getResidue3D(0));
	Object3D ao21 = startStrandResidue.getChild("O2*");
	Object3D ap2 = endStrandResidue.getChild("P");
	if (ap2 == null) {
	    NucleotideDBTools.addMissingPhosphor(endStrandResidue);
	    ap2 = endStrandResidue.getChild("P");
	    assert ap2 != null; // phosphor was added even if this is not physical
	}
	if (ao21 == null) {
	    throw new RnaModelException("Could not find atom O2* in residue: " + startStrandResidue.getFullName());
	}
	double distance = ao21.distance(ap2);
	// check angles:
	Vector3D v1 = NucleotideDBTools.getBackwardDirectionVector(startStrandResidue, "C4*");
	Vector3D v2 = NucleotideDBTools.getForwardDirectionVector(endStrandResidue, "C4*");
	double angle = Math.PI - v1.angle(v2); // forward and backward direction go in opposite directions, compensate for that
	if (distance > STRAND_DIST_MAX) {
	    distance += scoreCutoff + 1.0; // cannot be bridged
	}
	if (angle > STRAND_ANGLE_MAX) { // 
	    distance += ANGLE_PENALTY;
	}
	return distance;
    }


}
