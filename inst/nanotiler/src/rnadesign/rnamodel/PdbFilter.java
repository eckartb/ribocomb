package rnadesign.rnamodel;

import java.io.*;
import tools3d.objects3d.*;

public class PdbFilter {

    public static void main(String[] args) {
	String fileInName = args[0];
	String fileOutName = args[1];
	GeneralPdbWriter writer = new GeneralPdbWriter();
	RnaPdbRnaviewReader reader = new RnaPdbRnaviewReader();
	try {
	    FileInputStream fis = new FileInputStream(fileInName);
	    Object3DLinkSetBundle bundle = reader.readBundle(fis);
	    fis.close();
	    FileOutputStream fos = new FileOutputStream(fileOutName);
	    writer.write(fos, bundle.getObject3D());
	    fos.close();
	}
	catch (IOException ioe) {
	    System.out.println("IO Error : " + ioe.getMessage());
	}
    }

}
