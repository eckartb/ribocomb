
/** Factory class is responsible for reading/parsing a data file and creating
 *  a corresponding Object3DGraph
 */
package rnadesign.rnamodel;

import java.awt.Color;
import java.io.*;
import java.util.logging.Logger;

import sequence.*;
import tools3d.*;
import tools3d.objects3d.*;

/**
 * @author Eckart Bindewald
 *
 */
public class PointSetReader implements Object3DFactory {

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    public static CoordinateSystem3D debugCs;
    public static Object3D debugPoint;

    private int formatMode = Object3DFormats.LISP_FORMAT;

    /** Constructor needs a buffered reader (provides methods for linewise reading) */
    public PointSetReader() { }

    /** reads object and links. TODO : Not yet implemented! */
    public Object3DLinkSetBundle readBundle(InputStream is) throws Object3DIOException {
	Object3D obj = readAnyObject3D(is);
	// add links:
	LinkSet links = readLinks(is, obj);
	readAndAddTransformations(is, obj, links);
	log.info("Read " + obj.size() + " objects and " + links.size() + " links.");
	return new SimpleObject3DLinkSetBundle(obj, links);
    }

    /* reads objects as a list of points
     */
    public Object3D readAnyObject3D(InputStream is) throws Object3DIOException {
	log.fine("Starting readAnyObject3D!");
	DataInputStream dis = new DataInputStream(is);
	Object3D root = new SimpleObject3D();
	root.setName("PointSetImport");
	String word = readWord(dis);
	int numEntries = Integer.parseInt(word);
	log.fine("reading " + numEntries + " entries.");
	for (int i = 0; i < numEntries; ++i) {
	    readWord(dis); // overread first counter
	    Vector3D pos = readVector3D(dis);
	    log.fine("coordinates of point " + (i+1) + " : " + pos);
	    Object3D child = new SimpleObject3D();
	    child.setPosition(pos);
	    child.setName("p" + i);
	    root.insertChild(child);
	}
	return root;
    }

    /* reads links. Requires that objects are already defined!
     */
    public LinkSet readLinks(InputStream is, Object3D root) throws Object3DIOException {
	log.fine("Starting readAnyObject3D!");
	DataInputStream dis = new DataInputStream(is);
	LinkSet links = new SimpleLinkSet();
	root.setName("ImportedLinks");
	String word = readWord(dis);
	int numEntries = Integer.parseInt(word);
	for (int i = 0; i < numEntries; ++i) {
	    boolean silentMode = false;
	    String word1 = readWord(dis);
	    String word2 = readWord(dis);
	    int id1 = Integer.parseInt(word1);
	    int id2 = Integer.parseInt(word2);
	    if (id1 < 0) { // indicate "silent" links by negative indices
		silentMode = true;
		id1 *= -1;
	    }
	    log.fine("Creating links between objects : " + id1 + " " + id2);
	    --id1;
	    --id2; // internal counting
	    if ((id1 >= root.size()) || (id2 >= root.size())) {
		throw new Object3DIOException("Error reading point set: Point index larger than number of defined points!");
	    }
	    Link link = new SimpleLink(root.getChild(id1), root.getChild(id2));
	    if (silentMode) {
		link.setProperty("silent", "true");
	    }
	    links.add(link);
	}
	return links;
    }

    /* reads links. Requires that objects are already defined!
     */
    public void readAndAddTransformation(InputStream is, Object3D points, LinkSet links) throws Object3DIOException {
	try {
	DataInputStream dis = new DataInputStream(is);
	int numVals = 9;
	double[] vals = new double[numVals];
	String name = readWord(dis);
	for (int i = 0; i < numVals; ++i) {
	    vals[i] = Double.parseDouble(readWord(dis));
	}
	Vector3D basePos = new Vector3D(vals[0], vals[1], vals[2]);
	Vector3D x = new Vector3D(vals[3], vals[4], vals[5]);
	Vector3D y = new Vector3D(vals[6], vals[7], vals[8]);
	int numObjects = Integer.parseInt(readWord(dis));
	CoordinateSystem3D cs = new CoordinateSystem3D(basePos, x, y);
	this.debugCs = (CoordinateSystem3D)(cs.cloneDeep());
	log.fine("Read coordinate system: " + cs.toString());
	log.fine("Stored debug coordinate system: " + debugCs.toString());
	cs.setName(name);
	for (int i =0; i < numObjects; ++i) {
	    int objectId = Integer.parseInt(readWord(dis)) - 1; // internalcounting starts at zero
	    points.getChild(objectId).insertChild((Object3D)(cs.cloneDeep()));
	    debugPoint = points.getChild(objectId);
	}
	int numLinks = Integer.parseInt(readWord(dis));
	for (int i =0; i < numLinks; ++i) {
	    int linkId = Integer.parseInt(readWord(dis)) - 1; // internalcounting starts at zero
	    links.get(linkId).setProperty("symmetry:"+name, "1"); // currently: apply symmetry once
	}
	} catch (NumberFormatException nfe) {
	    throw new Object3DIOException("Number format exception while reading points format file: " + nfe.getMessage());
	}
    }

    /* reads links. Requires that objects are already defined!
     */
    public void readAndAddTransformations(InputStream is, Object3D points, LinkSet links) throws Object3DIOException {
	log.fine("Starting readAnyObject3D!");
	DataInputStream dis = new DataInputStream(is);
	String word = "";
	word = readWord(dis);
	if (word.length() == 0) {
	    log.fine("No symmetry transformations defined. Must be old-style points file.");
	    return;
	}
	int numEntries = Integer.parseInt(word);
	for (int i = 0; i < numEntries; ++i) {
	    readAndAddTransformation(is, points, links);
	}
    }    

    /** reads and creates Vector3D in format (Vector3D x y z ) */
    public static Vector3D readVector3D(DataInputStream dis) throws Object3DIOException {
	log.fine("starting readVector!");	
	String word = readWord(dis);
	double x = 0;
	double y = 0;
	double z = 0;
	try {
	    x = Double.parseDouble(word);
	}
	catch (NumberFormatException e) {
	    throw new Object3DIOException("Could not parse x coordinate for Vector3D " + word);
	}
	word = readWord(dis);
	try {
	    y = Double.parseDouble(word);
	}
	catch (NumberFormatException e) {
	    throw new Object3DIOException("Could not parse y coordinate for Vector3D " + word);
	}	
	word = readWord(dis);
	try {
	    z = Double.parseDouble(word);
	}
	catch (NumberFormatException e) {
	    throw new Object3DIOException("Could not parse z coordinate for Vector3D " + word);
	}
	log.fine("ending readVector!");
	return new Vector3D(x, y, z);	
    }

    /** reads a single word from data stream */
    public static String readWord(DataInputStream dis) {
	String s = new String("");
	char c = ' ';
	
	// first skip white space
	do {
	    try {
		c = (char)dis.readByte();
	    }
	    catch (IOException e) {
		break; // end of file reached
	    }
	}
	while (Character.isWhitespace(c));
	
	if (!Character.isWhitespace(c)) {
	    s = s + c;
	}
	else {
	    log.finest("found word: " + s);
	    return s;
	}
	
	while (true) {
	    try {
		c = (char)dis.readByte();
		if (!Character.isWhitespace(c)) {
		    s = s + c;
		}
		else {
		    break;
		}
	    }
	    catch (IOException e) {
		break; // end of file reached
	    }
	}
	log.finest("found word: " + s);
	return s;
    }

}
