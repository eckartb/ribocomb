package rnadesign.rnamodel;

import java.io.DataInputStream;
import java.io.InputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import org.testng.annotations.Test;

import tools3d.Point;
import tools3d.Vector3D;
import tools3d.objects3d.Link;
import tools3d.objects3d.LinkSet;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DFactory;
import tools3d.objects3d.Object3DFormats;
import tools3d.objects3d.Object3DIOException;
import tools3d.objects3d.Object3DLinkSetBundle;
import tools3d.objects3d.RotationInfo;
import tools3d.objects3d.SimpleLink;
import tools3d.objects3d.SimpleLinkSet;
import tools3d.objects3d.SimpleObject3D;
import tools3d.objects3d.SimpleObject3DLinkSetBundle;

/**
 * NEW .points2 FORMATTING
 *
 * POINTS #
 * 1 x y z
 * n x y z
 * LINKS #
 * 1 n
 * PLACE #
 * n (which points to place junctions on)
 * ROTATE #
 * 1 n x y z x y z
 * (point junction was placed on, point to rotate junction to, coordinates of line to rotate around)
 *
 * ex. square
 * POINTS 4
 * 1 0 0 0
 * 2 100 0 0
 * 3 100 100 0
 * 4 0 100 0
 * LINKS 4
 * 1 2
 * 2 3
 * 3 4
 * 4 1
 * PLACE 1
 * 1
 * ROTATE 3
 * 1 2 50 50 0
 * 1 3 50 50 0
 * 1 4 50 50 0
 */

/**
 * Reads a .points2 file and turns it into data the program can use.
 *
 * @author Christine Viets
 */
public class PointSetReader3 implements Object3DFactory {

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    /** Constructor needs a buffered reader (provides methods for linewise reading) */
    public PointSetReader3() { }

    public RotationInfo readPoints2(InputStream is) throws Object3DIOException {
	DataInputStream dis = new DataInputStream(is);
	Object3D root = readAnyObject3D(is);
	LinkSet links = readLinks(is, root);
	readWord(dis); // Should be "PLACE".
	int numPlace = Integer.parseInt(readWord(dis));
	// int[] placeArray = new int[numPlace];
	for (int i = 0; i < numPlace; i++) {
	    // int j = (Integer.parseInt(readWord(dis)) - 1);
	    // placeArray[i] = j;
	    root.getChild((Integer.parseInt(readWord(dis)) - 1)).setProperty("place", "true");
	}
	readWord(dis); // Should be "ROTATE"
	int numRotate = Integer.parseInt(readWord(dis));
	// ArrayList<Rotation3D> rotateList = new ArrayList<Rotation3D>();
	for (int i = 0; i < numRotate; i++) {
	    int id1 = (Integer.parseInt(readWord(dis)) - 1);
	    int id2 = (Integer.parseInt(readWord(dis)) - 1);
	    Object3D child1 = root.getChild(id1);
	    Object3D child2 = root.getChild(id2);
	    Vector3D axis = readVector3D(dis);
	    if (child2.getProperty("place") != "true") {
		child2.setProperty("rotateFrom", "" + id1);
		assert child2.getProperty("rotateFrom").equals("" + id1);
		child2.setProperty("axisX", "" + axis.getX());
		assert child2.getProperty("axisX").equals("" + axis.getX());
		child2.setProperty("axisY", "" + axis.getY());
		assert child2.getProperty("axisY").equals("" + axis.getY());
		child2.setProperty("axisZ", "" + axis.getZ());
		assert child2.getProperty("axisZ").equals("" + axis.getZ());
		child2.setProperty("angle", "" + getAngle(axis, child1.getPosition(), child2.getPosition()));
		assert child2.getProperty("angle").equals("" + getAngle(axis, child1.getPosition(), child2.getPosition()));
	    }
	    // Rotation3D rotation = new Rotation3D(id1, id2, child1,
	    // child2, axis);
	    // rotateList.add(rotation);
	}
	return new RotationInfo(root, links); // , placeArray, rotateList);
    }

    private double getAngle(Vector3D axisOrig,
			    Vector3D child1Orig,
			    Vector3D child2Orig) {
	Vector3D axis = new Vector3D(axisOrig);
	Vector3D child1 = new Vector3D(child1Orig);
	Vector3D child2 = new Vector3D(child2Orig);
	double angle;
	if (!axis.isOrigin()) {
	    child1 = child1.minus(axis);
	    child2 = child2.minus(axis);
	    axis = axis.minus(axis);
	}
	assert axis.isOrigin();
	assert child1.length() > 0;
	assert child2.length() > 0;
	angle = child1.angle(child2);
	assert angle > 0;
	return angle;
    }

    public Object3D readAnyObject3D(InputStream is) throws Object3DIOException {
	DataInputStream dis = new DataInputStream(is);
	Object3D root = new SimpleObject3D();
	readWord(dis); // Should be "POINTS"
	int numPoints = Integer.parseInt(readWord(dis));
	log.info("Reading " + numPoints + " points.");
	for (int i = 0; i < numPoints; i++) {
	    // Point p = readPoint(dis);
	    Vector3D position = readVector3DIndex(dis);
	    log.fine("Coordinates of point " + (i + 1) + ": " + position);
	    Object3D child = new SimpleObject3D();
	    child.setPosition(position);
	    child.setName("p" + i);
	    root.insertChild(child);
	}
	return root;
    }

    @Test public void testReadAnyObject3D() {
    }

    /** Method not used in this class. */
    public Object3DLinkSetBundle readBundle(InputStream is) throws Object3DIOException {
	assert false;
	return new SimpleObject3DLinkSetBundle();
    }

    public LinkSet readLinks(InputStream is, Object3D root) throws Object3DIOException {
	DataInputStream dis = new DataInputStream(is);
	LinkSet links = new SimpleLinkSet();
	root.setName("PointSet3Import");
	readWord(dis); // Should be "LINKS"
	int numLinks = Integer.parseInt(readWord(dis));
	for (int i = 0; i < numLinks; i++) {
	    int id1 = (Integer.parseInt(readWord(dis)) - 1);
	    int id2 = (Integer.parseInt(readWord(dis)) - 1);
	    links.add(new SimpleLink(root.getChild(id1), root.getChild(id2)));
	    //TODO: IMPORTANT: add links in the object itself
	}
	return links;
    }

    /** reads and creates Vector3D in format (Vector3D x y z ) */
    public static Vector3D readVector3DIndex(DataInputStream dis) throws Object3DIOException {
	int index = Integer.parseInt(readWord(dis));
	double x = 0;
	double y = 0;
	double z = 0;
	String word = readWord(dis);
	try { x = Double.parseDouble(word); }
	catch (NumberFormatException e) { throw new Object3DIOException("Could not parse x coordinate for Vector3D " + word); }
	word = readWord(dis);
	try {
	    y = Double.parseDouble(word);
	}
	catch (NumberFormatException e) {
	    throw new Object3DIOException("Could not parse y coordinate for Vector3D " + word);
	}	
	word = readWord(dis);
	try {
	    z = Double.parseDouble(word);
	}
	catch (NumberFormatException e) {
	    throw new Object3DIOException("Could not parse z coordinate for Vector3D " + word);
	}
	return new Vector3D(x, y, z, index);
    }

    /** reads and creates Vector3D in format (Vector3D x y z ) */
    public static Vector3D readVector3D(DataInputStream dis) throws Object3DIOException {
	double x = 0;
	double y = 0;
	double z = 0;
	String word = readWord(dis);
	try { x = Double.parseDouble(word); }
	catch (NumberFormatException e) { throw new Object3DIOException("Could not parse x coordinate for Vector3D " + word); }
	word = readWord(dis);
	try {
	    y = Double.parseDouble(word);
	}
	catch (NumberFormatException e) {
	    throw new Object3DIOException("Could not parse y coordinate for Vector3D " + word);
	}	
	word = readWord(dis);
	try {
	    z = Double.parseDouble(word);
	}
	catch (NumberFormatException e) {
	    throw new Object3DIOException("Could not parse z coordinate for Vector3D " + word);
	}
	return new Vector3D(x, y, z);
    }


    public static Point readPoint(DataInputStream dis) throws Object3DIOException {
	String name = readWord(dis);
	double x = 0.0;
	double y = 0.0;
	double z = 0.0;
	String word = readWord(dis); // Read the x value of the Point.
	try { // Try to convert it to a Double.
	    x = Double.parseDouble(word);
	}
	catch (NumberFormatException e) {
	    throw new Object3DIOException("Could not parse x coordinate for Point " + word);
	}
	word = readWord(dis); // Read the y value of the Point.
	try { // Try to convert it to a Double.
	    y = Double.parseDouble(word);
	}
	catch (NumberFormatException e) {
	    throw new Object3DIOException("Could not parse y coordinate for Point " + word);
	}
	word = readWord(dis); // Read the z value of the Point.
	try { // Try to convert it to a Double.
	    z = Double.parseDouble(word);
	}
	catch (NumberFormatException e) {
	    throw new Object3DIOException("Could not parse z coordinate for Point " + word);
	}
	Point point = new Point(name, x, y, z);
	return point;
    }
    
    @Test public void testReadPoint() {
    }
    
    /** reads a single word from data stream */
    public static String readWord(DataInputStream dis) {
	String s = new String("");
	char c = ' ';
	
	// Skip white space.
	do {
	    try {
		c = (char)dis.readByte();
	    }
	    catch (IOException e) {
		break; // End of file reached.
	    }
	}
	while (Character.isWhitespace(c));
	
	if (!Character.isWhitespace(c)) {
	    s = s + c;
	}
	else {
	    log.fine("found word: " + s);
	    return s;
	}
	
	while (true) {
	    try {
		c = (char)dis.readByte();
		if (!Character.isWhitespace(c)) {
		    s = s + c;
		}
		else {
		    break;
		}
	    }
	    catch (IOException e) {
		break; // End of file reached.
	    }
	}
	log.fine("found word: " + s);
	return s;
    }

}

    /*
      public void readPoints(DataInputStream dis,
      Junction j,
      Object3D root) throws Object3DIOException {
      assert root.size() == 0;
      int numPoints = Integer.parseInt(readWord(dis)); // Read the number of Points.
      assert numPoints > 0;
      String centerPoint = readWord(dis); // Read the name of the center point of the Junction.
      for( int i = 0; i < numPoints; i++ ) {
      Point point = readPoint(dis);
      if (point.getName().equals(centerPoint)) { // If the Point's name is the same as the center point's name,
      point.centerPoint(true); // Set the center point flag.
      }
      j.addPoint(point);
      Object3D newPoint = new SimpleObject3D();
      newPoint.setPosition(point.getVector3D());
      newPoint.setName(point.getName());
      newPoint.setFinalPoint(point.getFinalPoint()); // Set whether this point is final or overwritten.
      root.insertChild(newPoint);
      }
      assert j.getNumPoints() == numPoints;
      assert root.size() == numPoints;
      }
    */
    
    /*
      public void readSymmetries(DataInputStream dis) throws Object3DIOException {
      int numSymmetries = Integer.parseInt(readWord(dis)); // Read the number of Symmetries.
      assert(numSymmetries > 0);
      symmetriesVector = new Vector<Symmetry>(numSymmetries);
      for( int i = 0; i < numSymmetries; i++ ) {
      Symmetry tmpSymmetry = new Symmetry();
      tmpSymmetry.setName(readWord(dis));
      String word = readWord(dis); // Read the type of the Symmetry.
      try { // If the type is an int,
      tmpSymmetry.setType(Integer.parseInt(word));
      }
      catch(NumberFormatException e) { // If type is a String,
      tmpSymmetry.setType(word);
      }
      word = readWord(dis); // Read "JUNCTIONS".
      int numJuncRot = Integer.parseInt(readWord(dis)); // Read the number of Junctions to rotate.
      Junction symJunc = new Junction();
      for( int j = 0; j < numJuncRot; j++ ) {
      word = readWord(dis); // Read the name of the Junction to be rotated.
      for (int k = 0; k < junctionsVector.size(); k++) {
      if (((Junction)junctionsVector.get(k)).getName().equals(word)) { //If the name of the Junction to be rotated is the same as the Junction,
      assert junctionsVector.get(k).getNumPoints() > 0;
      symJunc = (Junction)((Junction)junctionsVector.get(k)).cloneDeep();
      assert symJunc.getNumPoints() > 0;
      k = junctionsVector.size(); // Quit for loop.
      }
      else{
      log.warning( "Check: names are not equal: " + junctionsVector.get(k).getName() + " " + word );
      }
      }
      assert symJunc.getNumPoints() > 0;
      tmpSymmetry.addJunction( j, symJunc );
      }
      tmpSymmetry.setStartPoint(readPoint(dis));
      tmpSymmetry.setEndPoint(readPoint(dis));
      word = readWord(dis); // Read "LINKS".
      int numSymLinks = Integer.parseInt(readWord(dis));
      for (int j = 0; j < numSymLinks; j++) {
      Junction junc = tmpSymmetry.getJunction(readWord(dis));
      Point point = junc.getPoint(readWord(dis));
      Junction junc_sym = tmpSymmetry.getJunction(readWord(dis));
      Point point_sym = junc_sym.getPoint(readWord(dis));
      SymmetryLink link = new SymmetryLink(junc, point, junc_sym, point_sym, tmpSymmetry);
      tmpSymmetry.addLink(j, link);
      }
      Symmetry symmetry = (Symmetry)tmpSymmetry.cloneDeep();
      log.info(symmetry.toString());
      symmetriesVector.add(i, symmetry);
      }
      assert symmetriesVector.size() == numSymmetries;
      }
    */
      
    /*
      public void readVectors(int j,
      Junction junc,
      DataInputStream dis) throws Object3DIOException {
      int numVectors = Integer.parseInt(readWord(dis)); // Read the number of Vector2s.
      assert numVectors > 0;
      Point p = junc.getPoint(readWord(dis)); // Read which Point the Vector2s originate from.
      for( int i = 0; i < numVectors; i++ ) {
      Point v = readPoint(dis); // Read the Vector2 Point.
      p.addNeighbor("vector", v); // Add the neighbor to the Point.
      junc.addVector(new Vector2(p, v));
      }
      assert junc.getNumVectors() == numVectors;
      }
    */

    /*
      public void readJunctions(DataInputStream dis,
      Object3D root) throws Object3DIOException {
      int numJunctions = Integer.parseInt(readWord(dis));
      assert numJunctions > 0;
      junctionsVector = new Vector<Junction>(numJunctions);
      String[] junctionNamesArray = new String[numJunctions]; // TODO: Remove?
      readJunctionNames(numJunctions, dis, junctionNamesArray);
      for( int i = 0; i < numJunctions; i++ ) {
      Junction junction = new Junction();
      junction.setName(junctionNamesArray[i]);
      String word = readWord(dis); // Read "POINTS".
      assert junction.getNumPoints() == 0;
      readPoints(dis, junction, root);
      assert junction.getNumPoints() > 0;
      word = readWord(dis); // Read "LINKS".
      assert junction.getNumLinks() == 0;
      readLinks(dis, junction, root);
      assert junction.getNumLinks() > 0;
      word = readWord(dis); // Read "VECTORPOINTS".
      int numVectorPoints = Integer.parseInt(readWord(dis)); // Read the number of vector points.
      assert(numVectorPoints > 0); // Can't there be zero vector points? TODO : check
      for( int k = 0; k < numVectorPoints; k++ ) {
      word = readWord(dis); // Read "VECTORS".
      assert junction.getNumVectors() == 0;
      readVectors(k, junction, dis);
      assert junction.getNumVectors() > 0;
      }
      junctionsVector.add(i, junction);
      }
      }
    */

    /*
      public void readLinks(DataInputStream dis,
      Junction junction,
      Object3D root) throws Object3DIOException {
      int numLinks = Integer.parseInt(readWord(dis)); // Read the number of Links.
      assert numLinks > 0;
      for( int i = 0; i < numLinks; i++ ) {
      String word1 = readWord(dis); // Read the name of the first Point.
      String word2 = readWord(dis); // Read the name of the second Point.
      Point p1 = junction.getPoint(word1); // Find the Point with the first name.
      Point p2 = junction.getPoint(word2); // Find the Point with the second name.
      p1.addNeighbor(p2); // Add a neighbor to the Points.
      Link link = new SimpleLink("link_" + i, p1, p2); //, keep);
      junction.addLink(link);
      }
      }
    */

