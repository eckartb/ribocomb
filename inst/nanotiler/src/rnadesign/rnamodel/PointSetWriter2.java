package rnadesign.rnamodel;
/** writes scence graph
 * 
 */

import tools3d.objects3d.*;
import tools3d.Vector3D;
import java.io.*;

/**
 * @author Eckart Bindewald
 *
 */
public class PointSetWriter2 {

    public PointSetWriter2() { }
	
    public void write(OutputStream os, Object3DSet objects, LinkSet links) throws IOException {
	PrintStream ps = new PrintStream(os);
	ps.println("" + objects.size());
	for (int i = 0; i < objects.size(); ++i) {     
	    Object3D obj = objects.get(i);
	    Vector3D pos = obj.getPosition();
	    ps.println("" + (i+1) + " " + pos.getX() + " " + pos.getY() + " " + pos.getZ());
	}
	StringBuffer buf = new StringBuffer();
	int count = 0;
	for (int i = 0; i < links.size(); ++i) {
	    Link link = links.get(i);
	    Object3D obj1 = link.getObj1();
	    Object3D obj2 = link.getObj2();
	    int id1 = objects.indexOf(obj1);
	    int id2 = objects.indexOf(obj2);
	    if (id1 >= 0 && id2 >= 0) {
		++count;
		buf.append("" + (id1+1) + " " + (id2+1) + PackageConstants.ENDL);
	    }
	}
	ps.println("" + count);
	ps.println(buf.toString());
    }
	
}
