/** 
 * This interface describes the concept of an RNA strand.
 * An RNA strand is a Object3D with one and only one
 * sequence defined.
 */
package rnadesign.rnamodel;
import sequence.Sequence;
import tools3d.Vector3D;
import tools3d.objects3d.*;

/**
 * @author Eckart Bindewald
 *
 */
public interface ProteinStrand extends BioPolymer {

}
