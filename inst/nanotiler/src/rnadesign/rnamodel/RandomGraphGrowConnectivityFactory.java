package rnadesign.rnamodel;

import java.util.Random;
import generaltools.Randomizer;
import tools3d.objects3d.*;

/** for a given graph, return random grow commands that might generatate this graph */
public class RandomGraphGrowConnectivityFactory extends GraphGrowConnectivityFactory  {

    private static Random rnd = Randomizer.getInstance();
    private FragmentGridTiler fgTiler;
    public RandomGraphGrowConnectivityFactory(Object3DLinkSetBundle graph,
					      StrandJunctionDB junctionDB,
					      StrandJunctionDB kissingLoopDB) {
	super(graph, junctionDB, kissingLoopDB);
	this.fgTiler = new FragmentGridTiler(junctionDB, kissingLoopDB);
    }

    /** UNFINISHED IMPLEMENTATION: Returns true if more grow connectivities can be generated */
    public boolean hasNext() { assert false; return false; }
    
    /** returns "next" growconnectivity that might be compatible with graph */
    public GrowConnectivity next() throws FittingException { 
	// determine which junctions to use
	int startVertexId = rnd.nextInt(vertices.size());
	// add links between junctions
	Object3D vertex = vertices.get(startVertexId);
	// StrandJunction3D firstJunction = 
	assert false;
	return null; 
    }

}
