package rnadesign.rnamodel;

import generaltools.ApplicationBugException;
import rnasecondary.*;
import sequence.*;
import tools3d.Vector3D;
import tools3d.objects3d.*;
import graphtools.*;
import java.util.List;
import java.util.ArrayList;
import java.util.logging.*;

/** generate tiling of RnaStrand objects and RnaStem3D objects 
 * tracing given set of objects 
 */
public class RandomGridTiler extends AbstractGridTiler implements GridTiler {

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    private static RnaPhysicalProperties rpp = new RnaPhysicalProperties(); // mainly used as shortcut

    /** number of residues per loop. Important variable */
    private static int offset = 2; // 2 * offset == residuePerLoopCount

    /** contains database of all used nucleotides */
    private Object3D nucleotideDB;

    /** public constructor needs nucleotide db */
    public RandomGridTiler(Object3D nucleotideDB) {
	this.nucleotideDB = nucleotideDB;
    }

    /** generates ALL possible tilings as a set of set of paths! */
    public IntegerListList generateRandomPathSet(GraphBase graph, boolean onlyFirstPathModea) {
	DnaJunctionSet junctionSet = DnaJunctionSetTools.generateRandomJunctionSet(graph);
	IntegerListList newPaths = DnaJunctionSetTools.generatePaths(graph, junctionSet);
	return newPaths;
    }

    /** return score for path. The lower the better. Use length (number of nodes) as score */
    public double computePathScore(IntegerList path) {
	return  (double)(path.size());
    }

    /** return score for path set. The lower the better. Use worst score of each of its path */
    public double computePathSetScore(IntegerListList pathSet) {
	// simple : find length of longest path
	if ((pathSet == null) || (pathSet.size() == 0)) {
	    return 1000;
	}
	double worstPathScore = computePathScore(pathSet.get(0));
	for (int i = 1; i < pathSet.size(); ++i) {
	    double score = computePathScore(pathSet.get(i));
	    if (score > worstPathScore) {
		worstPathScore = score;
	    }
	}
	return worstPathScore;
    }


    /** returns first path set with best score according to scoring function */
    public int findBestPathSet(IntegerListListList pathSetSet) {
	int bestPathSetId = 0;
	double bestPathSetScore = computePathSetScore(pathSetSet.get(0));
	for (int i = 1; i < pathSetSet.size(); ++i) {
	    double score =  computePathSetScore(pathSetSet.get(i));
	    if (score < bestPathSetScore) {
		bestPathSetScore = score;
		bestPathSetId =i;
	    }
	}
	return bestPathSetId;
    }

    /** Returns set of strand that trace the given set of objects for given set of paths.
     * Each path corresponds to one RNA strand. 
     */
    public Object3DLinkSetBundle generateTiling(Object3DSet objectSet, 
						LinkSet links, 
						IntegerListList circularPaths, 
						String baseName, char sequenceChar) {
 	log.fine("Starting generateTiling(objectSet...)");
 	log.fine("Using path set: " + circularPaths);
	// first generate graph
	GraphBase graph = GridPathTools.generateGraph(objectSet, links);
	Object3D resultObjects = new SimpleObject3D();
	Object3DSet strandSet = new SimpleObject3DSet();
	LinkSet newLinkSet = new SimpleLinkSet();
	resultObjects.setName("tilingroot");
	// generate a strand for each circular path
	log.fine("generating strand for each path out of " + circularPaths.size());
	for (int i = 0; i < circularPaths.size(); ++i) {
	    String strandName = baseName + "_s" + (i+1); // like grid.1, grid.2 etc : different name for each strand
	    Object3DLinkSetBundle bundle = generateStrand(circularPaths.get(i), objectSet, strandName, sequenceChar,
							  2*offset);
	    RnaStrand strand = (RnaStrand)(bundle.getObject3D());
	    LinkSet tmpLinks = bundle.getLinks();
	    log.fine("Strand " + strand.getName() + " with size " + strand.getResidueCount() + " " + strand.size()
			       + " generated. Number of new links: " + tmpLinks.size());
	    resultObjects.insertChild(strand);
	    strandSet.add(strand);
	    newLinkSet.merge(tmpLinks);
	    log.fine("New link set size after merging: " + newLinkSet.size());
	}
	// generate a Stem3D for each link
	log.fine("generating Stem3D for each link");
	for (int i = 0; i < links.size(); ++i) {
	    String stemName = baseName + "_h" + (i+1);
	    Object3DLinkSetBundle stemBundle = generateStem3D(circularPaths, objectSet, strandSet, links.get(i), stemName,
							      nucleotideDB, offset);
	    if (stemBundle != null) {
		resultObjects.insertChild(stemBundle.getObject3D());
		newLinkSet.merge(stemBundle.getLinks());
	    }
	    else {
		log.fine("Warning: generated stem was null, not adding! Link index: " + i);
	    }
	}
	// interpolate strands:
	for (int i = 0; i < strandSet.size(); ++i) {
	    Rna3DTools.interpolateNonStems((RnaStrand)(strandSet.get(i)), resultObjects);
	}
	Object3DLinkSetBundle resultBundle = new SimpleObject3DLinkSetBundle(resultObjects, newLinkSet);
	log.fine("Finished generateTiling(1)! " + resultBundle.getObject3D().size() + " "
			   + resultBundle.getLinks().size());
	return resultBundle;

    }

    public static Object3DSet generateObjectSet(Object3D objects, LinkSet links) {
	Object3DSet objectSet = new SimpleObject3DSet(objects);
	GraphBase graph = GridPathTools.generateGraph(objectSet, links);
	for (int i = objectSet.size()-1; i >= 0; --i) {
	    Object3D obj = objectSet.get(i);
	    if (graph.getConnections(i).size() < 2) {
		log.fine("Removing object " + i + " from set!");
		objectSet.remove(obj); // remove root object
	    }
	}
	return objectSet;
    }

    /** returns set of strand that trace the given set of objects */
    public Object3DLinkSetBundle generateTiling(Object3D objects, LinkSet links, String baseName,
						char sequenceChar, boolean onlyFirstPathMode ) {
	log.fine("Starting generateTiling!");
	Object3DSet objectSet = generateObjectSet(objects, links);
	log.fine("Using object3dset with " + objectSet.size() + " objects!");
	// first generate graph
	GraphBase graph = GridPathTools.generateGraph(objectSet, links);
	// obtain circular paths:
	log.fine("Starting generatePathSets!");
	IntegerListList randomPathSet = generateRandomPathSet(graph, onlyFirstPathMode);
	log.fine("Finished generateRandomPathSet: " + randomPathSet.size() + " " + randomPathSet);
	Object3DLinkSetBundle bundle = generateTiling(objectSet, links, randomPathSet, baseName, 
						      sequenceChar);
	log.fine("Finished generateTiling with " 
		 + bundle.getObject3D().size() + " objects and " 
		 + bundle.getLinks().size() + " links.");
	return bundle;
    }

}
