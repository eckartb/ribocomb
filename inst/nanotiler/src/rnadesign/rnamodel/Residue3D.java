package rnadesign.rnamodel;

import java.io.InputStream;
import generaltools.MalformedInputException;
import sequence.*;
import tools3d.*;
import tools3d.objects3d.*;

/** represents RNA or DNA base or even Amino acide that is part of a sequence */
public interface Residue3D extends Object3D, Residue, Molecule3D {

    /** returns sequence it belongs to */
    // public Sequence getSequence();

    /** returns position in sequence (zero-based counting) */
    public int getPos();

    /** Returns residue number as given in PDB file, or default if not defined. */
    public int getResidueNumberInPdb();

    /** If first residue in sequence */
    public boolean isFirst();

    /** If last residue in sequence */
    public boolean isLast();
    
    /** iff first or last residue in sequence */
    public boolean isTerminal();

    /** sets position in sequence */
    public void setPos(int pos);

    /** returns true if residue not equal "X" */
    public boolean isKnownResidue();

    /** sets sequence */
    // public void setSequence(Sequence s);

}
