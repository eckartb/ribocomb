package rnadesign.rnamodel;

import generaltools.ScoreWrapper;
import generaltools.ResultWorker;
import java.util.*;
import tools3d.objects3d.*;
import java.util.logging.*;
import static rnadesign.rnamodel.AI3DTools.*;
import static rnadesign.rnamodel.PackageConstants.*;

/** Interface for classes computing link sets */
public class RingFixLinkGenerator extends ResultWorker implements LinkGenerator {

    private static Logger log = Logger.getLogger(LOGFILE_DEFAULT);

    private double constraintRms = 10.0;
    private List<StrandJunction3D> junctions;
    private List<List<StrandJunction3D> > junctionClasses;
    private int helixLengthMax = 30;

    public RingFixLinkGenerator(List<StrandJunction3D> junctions) {
	this.junctions = junctions;
	assert this.junctions != null;
    }

    public RingFixLinkGenerator(Object3DSet junctionSet) {
	this.junctions = new ArrayList<StrandJunction3D>();
	for (int i = 0; i < junctionSet.size(); ++i) {
	    Object3D obj = junctionSet.get(i);
	    if (obj instanceof StrandJunction3D) {
		this.junctions.add((StrandJunction3D)obj);
	    }
	}
    }

    public LinkSet getLinks() {
	return (LinkSet)(getResult());
    }

    protected void runInternal() {
	System.out.println("Starting RingFixLinkGenerator.runInternal!");
	assert junctions != null;
	this.junctionClasses = AI3DTools.classifyJunctions(this.junctions);
	this.result = findHelixConstraints();
	assert getLinks() != null;
	System.out.println("Finished RingFixLinkGenerator.runInternal!");
    }

    /** returns helix constraint wrapped in ScoreWrapper constaining a score */
    private ScoreWrapper generateRawConstraint(BranchDescriptor3D b1, BranchDescriptor3D b2, double rms) {
	log.info("Starting generateRawConstraint(1) with " + b1.getFullName()
			   + " " + b2.getFullName());
	int helixLength = ConnectJunctionTools.computeBestStemLength(b1, b2);
	if (helixLength > helixLengthMax) {
	    return null;
	}
	log.info("continuing generateRawConstraint(1) with " + b1.getFullName()
			   + " " + b2.getFullName());
	return generateRawConstraint(b1, b2, rms, helixLength);
    }

    /** returns helix constraint wrapped in ScoreWrapper containing a score */
    private ScoreWrapper generateRawConstraint(BranchDescriptor3D b1, BranchDescriptor3D b2, double rms,
					       int helixLength) {
	log.info("Starting generateRawConstraint(2) with " + b1.getFullName()
			   + " " + b2.getFullName());
	if (b1.getParent() == b2.getParent()) {
	    log.info("Internal error: branch descriptors have same parent node: "
			       + b1.getFullName() + " "
			       + b1.getParent().getFullName()
			       + NEWLINE
			       + b2.getFullName() + " "
			       + b2.getParent().getFullName());
	}
	assert b1.getParent() != b2.getParent(); // must belog to different junction
	HelixConstraintLink constraint = new SimpleHelixConstraintLink(b1, b2, helixLength, helixLength, rms);
	double score = HelixOptimizer.computeBranchDescriptorError(b1, b2,
			   CoordinateSystem3D.CARTESIAN, CoordinateSystem3D.CARTESIAN, helixLength);
	ScoreWrapper wrap = new ScoreWrapper(constraint, score);
	return wrap;
    }

    /** generates a all vs all list of helix constraints */
    private List<ScoreWrapper> generateRawConstraints(StrandJunction3D junction1, StrandJunction3D junction2) {
	assert junction1 != junction2;
	List<ScoreWrapper> rawConstraints = new ArrayList<ScoreWrapper>();
	for (int i = 0; i < junction1.getBranchCount(); ++i) {
	    for (int j = 0; j < junction2.getBranchCount(); ++j) {
		assert junction1.getBranch(i) != junction2.getBranch(j);
		assert junction1 != junction2;
		if (junction1.getBranch(i).getParent() == junction2.getBranch(j).getParent()) {
		    log.info("Internal error: branch descriptors have same parent node: "
				+ junction1.getFullName() + " " + junction1.getBranch(i).getFullName() + " "
				+ junction1.getBranch(i).getParent().getFullName()
				+ NEWLINE
				+ junction2.getFullName() + " " + junction2.getBranch(j).getFullName() + " "
				+ junction1.getBranch(j).getParent().getFullName());
		    assert junction1.getBranch(i).getParent() == junction2.getBranch(j).getParent();
		}
		else if (junction1.getBranch(i).getParent() != junction2.getBranch(j).getParent()) {
		    log.info("Everything ok using " + junction1.getFullName() + " " + junction2.getFullName());
		}
		assert junction1.isValid();
		assert junction2.isValid();
		assert junction1.getBranch(i).getParent() != junction2.getBranch(j).getParent();
		assert junction1.getBranch(i).getParent() == junction1;
		assert junction2.getBranch(j).getParent() == junction2;
		assert junction1 != junction2;
		log.info("Starting generateRawConstraint with " + junction1.getBranch(i).getFullName()
				   + " " + junction2.getBranch(j).getFullName());
		ScoreWrapper wrap = generateRawConstraint(junction1.getBranch(i), junction2.getBranch(j), constraintRms);
		if (wrap != null) {
		    rawConstraints.add(wrap);
		}
	    }
	}
	return rawConstraints;
    }

    private List<HelixConstraintLink> generateSortedRawConstraints(List<StrandJunction3D> junctions) {
	List<ScoreWrapper> tmpResult = new ArrayList<ScoreWrapper>();
	for (int i = 0; i < junctions.size(); ++i) {
	    for (int j = i + 1; j < junctions.size(); ++j) {
		List<ScoreWrapper> tmpResult2 = generateRawConstraints(junctions.get(i), junctions.get(j));
		if (tmpResult2.size() > 0) {
		    Collections.sort(tmpResult2);
		    tmpResult.add(tmpResult2.get(0));
		}
	    }
	}
	Collections.sort(tmpResult);
	List<HelixConstraintLink> result = new ArrayList<HelixConstraintLink>();
	for (Object obj : tmpResult) {
	    HelixConstraintLink constraint = (HelixConstraintLink)(((ScoreWrapper)(obj)).getObject());
	    result.add(constraint);
	}
	return result;
    }

    /** Generates set of helix constraints */
    private LinkSet findHelixConstraints() {
	assert junctionClasses != null;
	LinkSet helixConstraints = new SimpleLinkSet();
	// sort such that best constraints are first:
	List<HelixConstraintLink> rawConstraints = generateSortedRawConstraints(junctions);
	for (Link link : rawConstraints) {
	    assert link instanceof HelixConstraintLink;
	    HelixConstraintLink constraintLink = (HelixConstraintLink)link;
	    if (constraintAvailable(constraintLink)) {
		// places this constraints but also all equivalent constraints:
		placeEquivalentConstraints(constraintLink, helixConstraints, rawConstraints); 
	    }
	}	
	return helixConstraints;
    }

    /** Returns true if both branch descriptors are still "free" */
    private boolean constraintAvailable(HelixConstraintLink constraint) {
	BranchDescriptor3D b1 = (BranchDescriptor3D)(constraint.getObj1());
	BranchDescriptor3D b2 = (BranchDescriptor3D)(constraint.getObj2());
	return b1.getProperty(TMP_TOKEN) == null && b2.getProperty(TMP_TOKEN) == null;
    }

    /** Adds a single constraint to link set and marks corresponding branch descriptors as occupied */
    private void placeConstraint(HelixConstraintLink constraint, LinkSet helixConstraints) {
	if (constraintAvailable(constraint)) {
	    constraint.getObj1().setProperty(TMP_TOKEN, TMP_TOKEN_PLACED);
	    constraint.getObj2().setProperty(TMP_TOKEN, TMP_TOKEN_PLACED);
	    helixConstraints.add(constraint);
	}
    }

    /** Returns true if both branch descriptors are still "free" */
    private List<HelixConstraintLink> generateSortedEquivalentConstraints(HelixConstraintLink constraint) {
	assert constraint.getBasePairMin() == constraint.getBasePairMax();
	BranchDescriptor3D b1 = (BranchDescriptor3D)(constraint.getObj1());
	BranchDescriptor3D b2 = (BranchDescriptor3D)(constraint.getObj2());
	StrandJunction3D j1 = (StrandJunction3D)(b1.getParent());
	StrandJunction3D j2 = (StrandJunction3D)(b2.getParent());
	int id1 = j1.getIndexOfChild(b1);
	int id2 = j2.getIndexOfChild(b2);
	int class1 = Integer.parseInt(j1.getProperty(TMP_TOKEN_JUNCTIONCLASS));
	int class2 = Integer.parseInt(j2.getProperty(TMP_TOKEN_JUNCTIONCLASS));
	assert b1.getProperty(TMP_TOKEN) == null && b2.getProperty(TMP_TOKEN) == null;
	List<ScoreWrapper> rawConstraints = new ArrayList<ScoreWrapper>();
	for (int i = 0; i < junctionClasses.get(class1).size(); ++i) {
	    StrandJunction3D junction1 = junctionClasses.get(class1).get(i);
	    for (int j = 0; j < junctionClasses.get(class2).size(); ++j) { // duplicate counting is harmless: occupancy takes care of it
		StrandJunction3D junction2 = junctionClasses.get(class2).get(j);
		if (junction1 == junction2) {
		    continue; // no reason to have constraint between same junction
		}
		assert junction1 != junction2;
		assert junction1.getChild(id1) != junction2.getChild(id2);
		rawConstraints.add(generateRawConstraint((BranchDescriptor3D)(junction1.getChild(id1)),
							 (BranchDescriptor3D)(junction2.getChild(id2)),
							 constraintRms, constraint.getBasePairMin()));
	    }
	}
	Collections.sort(rawConstraints);
	List<HelixConstraintLink> result = new ArrayList<HelixConstraintLink>();
	for (ScoreWrapper wrap : rawConstraints) {
	    result.add((HelixConstraintLink)(wrap.getObject()));
	}
	return result;
    }

    /** Returns true if both branch descriptors are still "free" */
    private void placeEquivalentConstraints(HelixConstraintLink constraint, LinkSet helixConstraints, List<HelixConstraintLink> rawConstraints) {
	assert (constraintAvailable(constraint));
	List<HelixConstraintLink> equivConstraints = generateSortedEquivalentConstraints(constraint);
	for (HelixConstraintLink c  : equivConstraints) {
	    placeConstraint(c,  helixConstraints);
	}
    }

    /** Returns true iff both constraint point two equivalent branch descriptors of junctions of same class, order dependent. Careful: the junctions must really have equivalent composition and order to be considered equivalent; no child objects should be added to one of them. */
    private boolean checkConstraintEquivalenceInternal(HelixConstraintLink constraint1, HelixConstraintLink constraint2) {
	BranchDescriptor3D b11 = (BranchDescriptor3D)(constraint1.getObj1());
	BranchDescriptor3D b12 = (BranchDescriptor3D)(constraint1.getObj2());
	BranchDescriptor3D b21 = (BranchDescriptor3D)(constraint2.getObj1());
	BranchDescriptor3D b22 = (BranchDescriptor3D)(constraint2.getObj2());
	StrandJunction3D j11 = (StrandJunction3D)(b11.getParent());
	StrandJunction3D j12 = (StrandJunction3D)(b12.getParent());
	StrandJunction3D j21 = (StrandJunction3D)(b21.getParent());
	StrandJunction3D j22 = (StrandJunction3D)(b22.getParent());
	return j11.getProperty(TMP_TOKEN_JUNCTIONCLASS).equals(j21.getProperty(TMP_TOKEN_JUNCTIONCLASS))
	    && j12.getProperty(TMP_TOKEN_JUNCTIONCLASS).equals(j22.getProperty(TMP_TOKEN_JUNCTIONCLASS))
	    && (j11.getIndexOfChild(b11) == j21.getIndexOfChild(b21))
	    && (j12.getIndexOfChild(b12) == j22.getIndexOfChild(b22));
    }

    /** Returns true iff both constraint point two equivalent branch descriptors of junctions of same class, order independent. Careful: the junctions must really have equivalent composition and order to be considered equivalent; no child objects should be added to one of them. */
    private boolean checkConstraintEquivalence(HelixConstraintLink constraint1, HelixConstraintLink constraint2) {
	boolean result = checkConstraintEquivalenceInternal(constraint1, constraint2);
	if (result) {
	    return true;
	}
	HelixConstraintLink swapped = (HelixConstraintLink)(constraint2.clone());
	swapped.swap();
	return checkConstraintEquivalenceInternal(constraint1, swapped); // order should not matter

    }

}
