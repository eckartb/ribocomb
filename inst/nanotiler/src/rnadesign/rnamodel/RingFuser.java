package rnadesign.rnamodel;

import generaltools.ScoreWrapper;
import java.io.PrintStream;
import java.util.*;
import java.util.logging.*;
import tools3d.objects3d.*;
import tools3d.Vector3D;

/** Algorithm for automated fusing of RNA strands */
public class RingFuser implements Runnable {

    private Logger log = Logger.getLogger(PackageConstants.LOGFILE_DEFAULT);

    // some helper words used to indicate status of strands in properties object
    private static final double STRAND_ANGLE_MAX = 0.5 * Math.PI; // minimum angle between connecting strands running i
    private static final double ANGLE_PENALTY = 50.0; // bad angle between connecting strands is penalized by this "distance" 
    private static final double STRAND_CONNECTION_SCORE_CUTOFF = 20.0;
    private static final String TMP_TOKEN = "__RINGFUSE_STATUS";
    private static final String TMP_TOKEN_OCCUPIED = "occupied";
    private static final String TMP_TOKEN_JUNCTIONCLASS = "__RINGFUSE_JUNCTION_CLASS";
    private double distanceCutoff = 10.0; // max distance between two residues to be fused
    private List<List<StrandJunction3D> > junctionClasses;
    private static boolean debugMode = true;
    private List<StrandJunction3D> junctions = new ArrayList<StrandJunction3D>();
    private List<RnaStrand> strands = new ArrayList<RnaStrand>();
    private List<RnaStrand> fusedStrands; 

    public RingFuser(List<StrandJunction3D> junctions, List<RnaStrand> strands) {
	this.junctions.addAll(junctions);
	this.strands.addAll(strands); // new container
	this.junctionClasses = AI3DTools.classifyJunctions(junctions);
	this.strands = cleanStrands(this.strands);
	log.info("Initial strands: " + strands.size() + " cleaned version: " + this.strands.size());
    }

    public RingFuser(Object3DSet junctionSet, Object3DSet strandSet) {
	for (int i = 0; i < junctionSet.size(); ++i) {
	    Object3D obj = junctionSet.get(i);
	    if (obj instanceof StrandJunction3D) {
		this.junctions.add((StrandJunction3D)obj);
	    }
	    else {
		assert false;
	    }
	}
	for (int i = 0; i < strandSet.size(); ++i) {
	    Object3D obj = strandSet.get(i);
	    if (obj instanceof RnaStrand) {
		this.strands.add((RnaStrand)obj);
	    }
	    else {
		log.severe("Internal error in RingFuse: 3D object is not RNA strand: " + obj.getFullName() + " " + obj.getClassName());
		assert false;
	    }
	}
	this.junctionClasses = AI3DTools.classifyJunctions(junctions);
	this.strands = cleanStrands(this.strands);
	log.info("Initial strands: " + strandSet.size() + " cleaned version: " + this.strands.size());
    }

    private boolean isDuplicateStrand(RnaStrand strand, List<RnaStrand> strands) {
	for (RnaStrand strand2 : strands) {
	    if (strand2 == strand) {
		continue;
	    }
	    if (AI3DTools.checkStrandDuplicate(strand, strand2, 2.0, "C4*")) {
		log.info("Strand duplicate found: " + strand.getFullName() + " " + strand.sequenceString()
			 + strand2.getFullName() + " " + strand2.sequenceString());
		return true;
	    }
	}
	return false;
    }

    /** removes duplicate strands from set of strands to be evaluated */
    private List<RnaStrand> cleanStrands(List<RnaStrand> strands) {
	List<RnaStrand> result = new ArrayList<RnaStrand>();
	for (RnaStrand strand : strands) {
	    if ((findJunctionClass(strand) >= 0) || (!isDuplicateStrand(strand,strands))) {
		result.add(strand); // only delete if not part of junction
	    }
	}
	return result;
    }

    /** Returns order of precedence for junction classes */
    static int[] computeJunctionOrder(List<List<StrandJunction3D> > junctionClasses) {
	int[] result = new int[junctionClasses.size()];
	for (int i = 0; i < result.length; ++i) {
	    result[i] = i;
	}
	return result;
    }

    private void printJunctionClasses(PrintStream ps) {
	for (int i = 0; i < junctionClasses.size(); ++i) {
	    ps.print("Class " + (i+1) + " ");
	    for (int j = 0; j < junctionClasses.get(i).size(); ++j) {
		ps.print(junctionClasses.get(i).get(j).getFullName() + " ");
	    }
	    ps.println();
	}
    }

    public void run() {
	log.info("Starting automatic ring fusing!");
	this.fusedStrands = new ArrayList<RnaStrand>(); // resulting fused strands
	if (debugMode) {
	    log.info("Junction classes: ");
	    printJunctionClasses(System.out);
	}
	// sort junction classes such that kissing loops and then highest order junctions come first
	int[] junctionOrder = computeJunctionOrder(junctionClasses);
	// for each junction class: pick first element
	int[] strandUsed = new int[strands.size()];
	for (int jClass : junctionOrder) {
	    // for each class: loop over each junction of each class
	    for (int i = 0; i < junctionClasses.get(jClass).size(); ++i) {
		StrandJunction3D junction = junctionClasses.get(jClass).get(i);
		// for each junction: loop over each strand of each junction
		for (int j = 0; j < junction.getStrandCount(); ++j) {
		    NucleotideStrand strand = junction.getStrand(j);
		    if (!checkStrandAvailable(strand)) {
			continue;
		    }
		    int strandId = strands.indexOf(strand);
		    assert strandId >= 0; // must be found!
		    List<Integer> fusionIds = generateFusionStrandIds(strands, strandId);
		    fusedStrands.add(applyStrandFusion(fusionIds));
		}
	    }
	}
	log.info("Finished automatic ring fusing!");
    }

    /** fuse strand with these ids */
    private RnaStrand applyStrandFusion(List<Integer> strandIds) {
	assert strandIds != null && strandIds.size() > 0;
	RnaStrand newStrand = (RnaStrand)(strands.get(strandIds.get(0)).cloneDeep());
	log.info("Starting sequence path with " + newStrand.getFullName() + " strand id: " + (strandIds.get(0)+1) + " junction class: " + (findJunctionClass(strands.get(strandIds.get(0)))+1)) ;
	setStrandOccupied(strands.get(strandIds.get(0))); // make sure it is not used again
	assert !checkStrandAvailable(strands.get(strandIds.get(0)));
	int count = strands.get(strandIds.get(0)).getResidueCount();
	for (int i = 1; i < strandIds.size(); ++i) {
	    RnaStrand tmpStrand = strands.get(strandIds.get(i));
	    RnaStrand newStrand2 = (RnaStrand)(tmpStrand.cloneDeep());
	    setStrandOccupied(tmpStrand);
	    assert !checkStrandAvailable(tmpStrand);
	    count += strands.get(strandIds.get(i)).getResidueCount();
	    log.info("Fusing strand " + newStrand.getFullName() + " " + newStrand.sequenceString()
		     + " with " + newStrand2.getFullName() + " " + newStrand2.sequenceString() + " strand id: " + (strandIds.get(i)+1) + " junction class: " + (findJunctionClass(tmpStrand)+1)) ;
	    newStrand.append(newStrand2);
	}
	assert newStrand.getResidueCount() == count; // must be sum of all residues
	return newStrand;
    }

    /** checks if strand is occupied */
    private boolean checkStrandAvailable(NucleotideStrand strand) {
	return strand.getProperty(TMP_TOKEN) != TMP_TOKEN_OCCUPIED;
    }

    /** marks strand as occupied */
    private void setStrandOccupied(NucleotideStrand strand) {
	strand.setProperty(TMP_TOKEN, TMP_TOKEN_OCCUPIED);
    }

    /** returns internal class id of classified junction */
    private int findJunctionClass(StrandJunction3D junction) {
	for (int i = 0; i < junctionClasses.size(); ++i) {
	    if (junctionClasses.get(i).contains(junction)) {
		return i;
	    }
	}
	return -1 ; // no class found
    }

    /** Returns junction that contains strand or null if not found */
    private StrandJunction3D findJunction(RnaStrand strand) {
	return (StrandJunction3D)(Object3DTools.findAncestor(strand, junctions)); // returns junction or kissing loop that is parent or ancestor node
    }

    /** Returns id of junction class to which strand is part of */
    private int findJunctionClass(RnaStrand strand) {
	StrandJunction3D junction = findJunction(strand);
	if (junction == null) {
	    log.fine("Strand " + strand.getFullName() + " does not belong to a junction.");
	    return -1; // no junction found, this should be a strand belonging to a helix
	}
        int result = findJunctionClass(junction);
	assert result >= 0; // must be found!
	return result;
    }

    /** Scores how well a start strand can be appended with end strand. Zero: perfect score */
    private double scoreStrandConnectivity(RnaStrand startStrand, RnaStrand endStrand) throws RnaModelException {
	Nucleotide3D startStrandResidue = (Nucleotide3D)(startStrand.getResidue3D(startStrand.getResidueCount()-1));
	Nucleotide3D endStrandResidue = (Nucleotide3D)(endStrand.getResidue3D(0));
	Object3D ao21 = startStrandResidue.getChild("O2*");
	Object3D ap2 = endStrandResidue.getChild("P");
	if (ap2 == null) {
	    NucleotideDBTools.addMissingPhosphor(endStrandResidue);
	    ap2 = endStrandResidue.getChild("P");
	    assert ap2 != null; // phosphor was added even if this is not physical
	}
	if (ao21 == null) {
	    throw new RnaModelException("Could not find atom O2* in residue: " + startStrandResidue.getFullName());
	}
	double distance = ao21.distance(ap2);
	// check angles:
	Vector3D v1 = NucleotideDBTools.getBackwardDirectionVector(startStrandResidue, "C4*");
	Vector3D v2 = NucleotideDBTools.getForwardDirectionVector(endStrandResidue, "C4*");
	double angle = Math.PI - v1.angle(v2); // forward and backward direction go in opposite directions, compensate for that
	if (angle > STRAND_ANGLE_MAX) { // 
	    distance += ANGLE_PENALTY;
	}
	return distance;
    }

    /** return all the junction classes visited by this path */
    List<Integer> findJunctionClassIds(List<RnaStrand> strands, List<Integer> sofarStrandIds) {
	List<Integer> junctionIds = new ArrayList<Integer>();
	for (Integer i : sofarStrandIds) {
	    int junctionId = findJunctionClass(strands.get(i));
	    junctionIds.add(junctionId); // can also contain -1 if strand does not belong to junction
	}
	return junctionIds;
    }

    /** For strand with id "id", return index of strand that can be connected */
    int findNextStrandId(List<RnaStrand> strands, List<Integer> sofarStrandIds) {
	assert sofarStrandIds.size() > 0;
	int id = sofarStrandIds.get(sofarStrandIds.size()-1); // get last id, using auto-unboxing
	StrandJunction3D junction = findJunction(strands.get(id));
	List<Integer> junctionClassIds = findJunctionClassIds(strands, sofarStrandIds);
	// search for closest 5' end that does not belong to current junction
	List<ScoreWrapper> rankedList = new ArrayList<ScoreWrapper>();
	for (int i = 0; i < strands.size(); ++i) {
	    if (i == id) {
		continue;
	    }
	    if (sofarStrandIds.contains(i)) { // use auto-boxing
		continue; // check if used in this path
	    }
	    if (!checkStrandAvailable(strands.get(i))) {
		continue; // check if not used at all
	    }
	    StrandJunction3D junction2 = findJunction(strands.get(i));
	    int junctionClass2 = -1;
	    if (junction2 != null) {
		junctionClass2 = findJunctionClass(junction2);
		assert junctionClass2 >= 0;
		if (junctionClassIds.contains(junctionClass2)) {
		    continue; // this junction class is already being used in this path
		}
		else {
		    log.fine("Junction class of " + strands.get(i).getFullName() + " " + junctionClass2 + " is not being used so far in strands: "
			     + sofarStrandIds + " and classes: " + junctionClassIds);
		}
	    }
	    try {
		double score = scoreStrandConnectivity(strands.get(id), strands.get(i));
		if (score < STRAND_CONNECTION_SCORE_CUTOFF) {
		    ScoreWrapper wrap = new ScoreWrapper(new Integer(i), score); // remember id of strand
		    rankedList.add(wrap);
		}
	    }
	    catch (RnaModelException e) {
		log.warning("Bad nucleotide for strand fusing encountered: " + e.getMessage());
	    }
	}
	if (rankedList.size() == 0) {
	    return -1;
	}
	Collections.sort(rankedList);
	Integer strandIndexInt = (Integer)(rankedList.get(0).getObject());
	int result = strandIndexInt.intValue();
	assert checkStrandAvailable(strands.get(result));
	assert (findJunctionClass(strands.get(result)) < 0) || (!junctionClassIds.contains(findJunctionClass(strands.get(result)))); // junction class has never been used before
	assert rankedList.get(0).getScore() <= STRAND_CONNECTION_SCORE_CUTOFF;
	return result;
    }

    /** Returns indices of strands that can be fused if one follows the start strand from 5' to 3' end */
    private List<Integer> generateFusionStrandIds(List<RnaStrand> strands, int currentId) {
	List<Integer> result = new ArrayList<Integer>();
	// get junction class of strand:
	int startJunctionClass = findJunctionClass(strands.get(currentId));
	result.add(currentId); // one of my first examples of "auto-boxing" ! :-)
	while ((currentId = findNextStrandId(strands, result)) >= 0) {
	    result.add(currentId);
	}
	return result;
    }

    /** Returns result */
    public List<RnaStrand> getFusedStrands() {
	if (fusedStrands == null) {
	    run(); // lazy evaluation
	}
	return fusedStrands;
    }

}
