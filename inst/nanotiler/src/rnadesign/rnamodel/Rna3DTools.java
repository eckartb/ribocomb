package rnadesign.rnamodel;

import java.util.logging.Logger;

import generaltools.ApplicationBugException;
import generaltools.ConstraintDouble;
import generaltools.SimpleConstraintDouble;
import numerictools.PolarTools;
import rnasecondary.Interaction;
import rnasecondary.InteractionType;
import rnasecondary.RnaInteractionType;
import rnasecondary.SimpleInteraction;
import rnasecondary.SimpleStem;
import rnasecondary.Stem;
import sequence.Alphabet;
import sequence.DnaTools;
import sequence.LetterSymbol;
import sequence.Sequence;
import sequence.SimpleSequence;
import sequence.UnknownSymbolException;
import tools3d.CoordinateSystem;
import tools3d.LineShape;
import tools3d.Matrix4D;
import tools3d.Vector3D;
import tools3d.objects3d.Link;
import tools3d.objects3d.LinkSet;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DLinkSetBundle;
import tools3d.objects3d.Object3DSet;
import tools3d.objects3d.Object3DTools;
import tools3d.objects3d.SimpleConstraintLink;
import tools3d.objects3d.SimpleLinkSet;
import tools3d.objects3d.SimpleObject3D;
import tools3d.objects3d.SimpleObject3DSet;
import tools3d.objects3d.SimpleObject3DLinkSetBundle;

import static rnadesign.rnamodel.RnaConstants.C4_NORM_POS;
import static rnadesign.rnamodel.RnaConstants.RESIDUE_DIST;
// import static rnadesign.rnamodel.RnaConstants.WATSON_CRICK_DIST_MAX;
// import static rnadesign.rnamodel.RnaConstants.WATSON_CRICK_DIST_MIN;

public class Rna3DTools {

    // public static final ConstraintDouble RNA_WC_CONSTRAINT = new SimpleConstraintDouble(WATSON_CRICK_DIST_MIN,
											//									     WATSON_CRICK_DIST_MAX);

    /** minimum and maximum distances of bases in RNA chain TODO */
    // public static final ConstraintDouble RNA_CHAIN_CONSTRAINT = new SimpleConstraintDouble(WATSON_CRICK_DIST_MIN,
    //									     WATSON_CRICK_DIST_MAX);

    public static double RMS_LARGE_LIMIT = 10.0;

    public static final Vector3D X_VEC = new Vector3D(1.0, 0.0, 0.0);

    public static final Vector3D Z_VEC = new Vector3D(0.0, 0.0, 1.0);

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    /** assume base at position 0,0.0, first base pair middle point is in direction 1,0.0.
     * computes transformed positions given an initial atom coordinate
     */
    public static LineShape computeHelix(int s, Vector3D refPos, HelixParameters prm) {
	double z = prm.rise * s;
	double zDelta = refPos.getZ()-0.0;
	double phiDelta = PolarTools.phiFromXY(refPos.getX(), refPos.getY());
	double r = Math.sqrt(refPos.getX()*refPos.getX() + refPos.getY()*refPos.getY()); // HELIX_RADIUS;
	double nt = prm.basePairsPerTurn;
	double h = prm.rise;;
	double hoff = prm.offset; // HELIX_OFFSET;
	double phiOff = prm.phaseOffset; // HELIX_PHASE_OFFSET;
	double a1 = (2.0*Math.PI)*s/nt + phiDelta; // angle of base 1
	double a2 = a1 + phiOff - (2*phiDelta); // angle of base 2, -2phiDelta to compensate for added phiDelta in a1
	double b1x = r * Math.cos(a1);
	double b1y = r * Math.sin(a1);
	double b2x = r * Math.cos(a2);
	double b2y = r * Math.sin(a2);
	Vector3D pos1 = new Vector3D(b1x, b1y, z + zDelta); // center of base 1
	Vector3D pos2 = new Vector3D(b2x, b2y, z + hoff -zDelta); // center of base 2
	if ((s>0) && (prm.getDeformation() != null)) { // apply s times the deformation matrix
	    Matrix4D m = prm.getDeformation().generateMatrix4D();
	    pos1 = m.multiply(pos1, s);
	    pos2 = m.multiply(pos2, s);
	}
	return new LineShape(pos1, pos2);
    }

    /** computes start and stop position of n-th base pair when helix was moved like coordinate system cs,
     * allows for non-standard helix parameters.
     */
    public static LineShape computeHelix(int n, Vector3D refPos, HelixParameters prm, CoordinateSystem cs) {
	LineShape line = computeHelix(n, refPos, prm); // positions for "norm" helix
	Vector3D p1 = line.getPosition1();
	Vector3D p2 = line.getPosition2();
	p1 = cs.activeTransform(p1); // transformHelix(p1, cs);
	p2 = cs.activeTransform(p2); // transformHelix(p2, cs);
	return new LineShape(p1, p2);
    }

    /** computes start and stop position of n-th base pair when helix was moved like coordinate system cs,
     * allows for non-standard helix parameters.
     */
    public static LineShape computeHelix(int n, CoordinateSystem cs, HelixParameters prm) {
        return computeHelix(n, C4_NORM_POS, prm, cs); // positions for "norm" helix
    }

    /** convinience method to generate "artificial" SimpleRnaStem3D object from ideal helix coodinates. 
     * Needs start position of strand1 (5'end) and direction. 
     * @deprecated
     */
    public static Object3DLinkSetBundle generateRnaStem3D(RnaStrand strand1, 
							  RnaStrand strand2, 
							  int startPos,
							  int stopPos, 
							  int length, 
							  boolean setEqualMode,
							  String stemName,
							  Vector3D strand1BasePos,
							  Vector3D directionOrig) 
      {
      log.severe("Outdated method called: generateRnaStem3D(RnaStrand, RnaStrand, int, int, boolean, String, Vector3D, Vector3D)");
      //	assert false; //TODO: put back when method is no longer called!
      // log.fine("Computing stem3d: " + strand1BasePos + "  " + directionOrig);
      Stem stem = new SimpleStem(startPos, stopPos, length, strand1, strand2);
      if (!stem.isValid()) {
      throw new ApplicationBugException("Invalid stem parameters in RNA3DTools.generateRnaStem3D!");
      }
      Vector3D base = strand1BasePos;
      // log.fine("Getting residue endPos " + (startPos+length-1) + " of strand1 with size " + strand1.getResidueCount());
      Vector3D endPos = strand1.getResidue3D(startPos+ length -1).getPosition();
      Vector3D direction = new Vector3D(directionOrig); // endPos.minus(base);
      direction.normalize();
      // log.fine("Getting residue endPos " + (stopPos) + " of strand2 with size " + strand2.getResidueCount());
      Vector3D base2 = strand2.getResidue3D(stopPos).getPosition();
      Vector3D rpTmp = base2.minus(base);
      Vector3D rp = direction.cross(rpTmp);
      rp.normalize(); // relative vector pointing to first base pair from base (approximately). Also orth. to direction 
      
      RnaStem3D stem3D = new SimpleRnaStem3D(stem); // , l1.getPosition1(), l1.getPosition2(), l2.getPosition1(), l2.getPosition2());
      LinkSet newLinks = new SimpleLinkSet();
      stem3D.setName(stemName);
      InteractionType watsonCrickInteraction = new RnaInteractionType(RnaInteractionType.WATSON_CRICK); // describes Watson-Crick interaction
      InteractionType backboneInteraction = new RnaInteractionType(RnaInteractionType.BACKBONE); // describes backbone interaction
      // add intermediate points:
      for (int i = 0; i < length; ++i) {
	  assert false; 
      LineShape line = null; // TODO replace computeHelix(base, rp, direction, i);
      Object3D objectStrand1 = new SimpleObject3D(line.getPosition1());
      String name = "S1N" + (i + 1);
      objectStrand1.setName(name);
      // Object3D objectStrand2 = new SimpleObject3D(line.getPosition2());
      stem3D.insertChild(objectStrand1);
      Residue3D res1 = strand1.getResidue3D(startPos + i);
      if (setEqualMode) {
      // log.fine("Getting residue " + (startPos+i) + " of strand1 with size " + strand1.getResidueCount());
	  res1.setPosition(objectStrand1.getPosition());
      }
      // }
      // 	for (int i = 0; i < length; ++i) {
      // 	    LineShape line = computeHelix(base, rp, direction, i);
      Object3D objectStrand2 = new SimpleObject3D(line.getPosition2());
      String name2 = "S2N" + (i + 1);
      objectStrand2.setName(name2);
      // Object3D objectStrand2 = new SimpleObject3D(line.getPosition2());
      stem3D.insertChild(objectStrand2);
      Residue3D res2 = strand2.getResidue3D(stopPos - i);
      if (setEqualMode) {
	  
      // log.fine("Getting residue " + (stopPos-i) + " of strand2 with size " + strand2.getResidueCount());
	  res2.setPosition(objectStrand2.getPosition());
      }
      Interaction interaction = new SimpleInteraction(res1, res2, watsonCrickInteraction);
      // 	    Interaction bbInteraction = new SimpleInteraction(res1, res2, backboneInteraction);
      Link link = new InteractionLinkImp(res1, res2, interaction);
      // 	    Link newLink = new InteractionLinkImp(res1, res2, bbInteraction);
      newLinks.add(link);
      // 	    newLinks.add(newLink);
      }	
      stem3D.setStrand1End5Index(0);
      stem3D.setStrand1End3Index(length-1);
      stem3D.setStrand2End5Index(length);
      stem3D.setStrand2End3Index(2*length-1);
      // log.fine("Resulting stem3d: " + stem3D);
      // log.fine("Finished generateRnaStem3D with size: " + stem3D.size() + " and # links: " + newLinks.size());
      return new SimpleObject3DLinkSetBundle(stem3D, newLinks);
      }
    

    /** adds single atom to residue if residue is part of stem. */
    private static void addAtomInStem(Residue3D res,
			       int pos,
			       boolean watsonStrand,
			       BranchDescriptor3D branch,
			       Atom3D atomOrig) {
	Atom3D atom = (Atom3D)(atomOrig.cloneDeep());
	// adds atom other than C4*: if position is 9.9, 0, 0 then the method is equivalent to computeHelix(branch, i+helixOffset)
	LineShape line = computeHelix(pos, atom.getPosition(), branch.getHelixParameters(), branch.getCoordinateSystem());	    
	if (watsonStrand) {
	    atom.setPosition(line.getPosition1());
	}
	else {
	    atom.setPosition(line.getPosition2());
	}
	res.insertChild(atom);
    }

    /** add atom coordinates to residue */
    private static void addAtomsInStem(Residue3D res, int pos, boolean watsonStrand, 
				       BranchDescriptor3D branch, 
				       Object3D nucleotideDB) {
	assert nucleotideDB != null;
	// first try to find correct molecule:
	String molTypeName = "" + res.getSymbol().getCharacter();
	int molIdx = nucleotideDB.getIndexOfChild(molTypeName);
	if (molIdx < 0) {
	    log.severe("Warning: nucleotide type " + molTypeName + " was not defined in database: ");
	    for (int i = 0; i < nucleotideDB.size(); ++i) {
		log.severe(nucleotideDB.getChild(i).getName());
	    }
	    return;
	}
	Nucleotide3D mol = (Nucleotide3D)(nucleotideDB.getChild(molIdx));
	int atomCount = mol.getAtomCount();
	for (int i = 0; i < atomCount; ++i) {
	    addAtomInStem(res, pos, watsonStrand, branch, mol.getAtom(i));
	}
	assert (Object3DTools.collectByClassName(res, "Atom3D").size() > 0); // at least one atom must be placed
    }

    /** convinience method to generate "artificial" SimpleRnaStem3D object from ideal helix coodinates. 
     * Needs start position of strand1 (5'end) and direction. 
     */
    public static Object3DLinkSetBundle generateRnaStem3D(RnaStrand strand1, 
							  RnaStrand strand2, 
							  int startPos,
							  int stopPos, 
							  int length, // length of helix 
							  int helixOffset, // usually 0 (starting directly at branch positions or 1 (starting 1 base pair later)
							  boolean setEqualMode,
							  String stemName,
							  BranchDescriptor3D branch,
							  Object3D nucleotideDB)
    {
	assert nucleotideDB != null; // just for debugging
	// log.fine("Computing stem3d: " + strand1BasePos + "  " + directionOrig);
	Stem stem = new SimpleStem(startPos, stopPos, length, strand1, strand2);
	if (!stem.isValid()) {
	    throw new ApplicationBugException("Invalid stem parameters in RNA3DTools.generateRnaStem3D!");
	}
	RnaStem3D stem3D = new SimpleRnaStem3D(stem); // , l1.getPosition1(), l1.getPosition2(), l2.getPosition1(), l2.getPosition2());
	LinkSet newLinks = new SimpleLinkSet();
	stem3D.setName(stemName);
	InteractionType watsonCrickInteraction = new RnaInteractionType(RnaInteractionType.WATSON_CRICK); // describes Watson-Crick interaction
	InteractionType backboneInteraction = new RnaInteractionType(RnaInteractionType.BACKBONE); // describes backbone interaction
	// add intermediate points:
	for (int i = 0; i < length; ++i) {
	    // LineShape line = computeHelix(base, rp, direction, i);
	    LineShape line = computeHelix(i + helixOffset, branch.getCoordinateSystem(), branch.getHelixParameters());
	    Object3D objectStrand1 = new SimpleObject3D(line.getPosition1());
	    String name = "S1N" + (i + 1);
	    objectStrand1.setName(name);
	    // Object3D objectStrand2 = new SimpleObject3D(line.getPosition2());
	    stem3D.insertChild(objectStrand1);
	    Residue3D res1 = strand1.getResidue3D(startPos + i);
	    if (setEqualMode) {
		// log.fine("Getting residue " + (startPos+i) + " of strand1 with size " + strand1.getResidueCount());
		res1.setPosition(objectStrand1.getPosition());
	    }
	    // insert atoms!
	    if ((nucleotideDB != null) && (res1.getAtomCount() == 0)) {
		// log.fine("Adding atom coordinates to residue: " + res1.getName());
		addAtomsInStem(res1, i+helixOffset, 
			       true, branch, nucleotideDB);
	    }
	    else {
		assert false; // could not place atoms!
	    }
	    Object3D objectStrand2 = new SimpleObject3D(line.getPosition2());
	    String name2 = "S2N" + (i + 1);
	    objectStrand2.setName(name2);
	    // Object3D objectStrand2 = new SimpleObject3D(line.getPosition2());
	    stem3D.insertChild(objectStrand2);
	    Residue3D res2 = strand2.getResidue3D(stopPos - i);
	    if (setEqualMode) {
	// log.fine("Getting residue " + (stopPos-i) + " of strand2 with size " + strand2.getResidueCount());
		res2.setPosition(objectStrand2.getPosition());
	    }
	    if ((nucleotideDB != null) && (res2.getAtomCount() == 0)) {
		// log.fine("Adding atom coordinates to residue: " + res2.getName());
		addAtomsInStem(res2, i+helixOffset, 
			       false, branch, nucleotideDB);
	    }
	    else {
		assert false;
	    }
	    Interaction interaction = new SimpleInteraction(res1, res2, watsonCrickInteraction);
// 	    Interaction bbInteraction = new SimpleInteraction(res1, res2, backboneInteraction);
	    Link link = new InteractionLinkImp(res1, res2, interaction);
// 	    Link newLink = new InteractionLinkImp(res1, res2, bbInteraction);
	    newLinks.add(link);
// 	    newLinks.add(newLink);
	}	
	stem3D.setStrand1End5Index(0);
	stem3D.setStrand1End3Index(length-1);
	stem3D.setStrand2End5Index(length);
	stem3D.setStrand2End3Index(2*length-1);
	// log.fine("Resulting stem3d: " + stem3D);
	// log.fine("Finished generateRnaStem3D with size: " + stem3D.size() + " and # links: " + newLinks.size());

// 	assert(strand1.getResidue3D(startPos).getPosition().distance(branch.computeHelixPosition(0, BranchDescriptor3D.OUTGOING_STRAND)) < RMS_LARGE_LIMIT);
// 	assert(strand2.getResidue3D(stopPos).getPosition().distance(branch.computeHelixPosition(0, BranchDescriptor3D.INCOMING_STRAND)) < RMS_LARGE_LIMIT);
	assert newLinks.size() == stem3D.getLength();
	assert newLinks.get(0) instanceof InteractionLink;
	return new SimpleObject3DLinkSetBundle(stem3D, newLinks);
    }

    /** convinience method to generate "artificial" SimpleRnaStem3D object from ideal helix coodinates. 
     * Needs two branch descriptors. Starts optimization in order to find best fitting stem.
     * Branch descriptors describe OUTSIDE connecting stems: adding one base pair results in inside branch descriptor.
     */
    /*
    public static Object3DLinkSetBundle generateStretchedRnaStem3D(RnaStrand strand1, 
								   RnaStrand strand2, 
								   int startPos,
								   int stopPos, 
								   int length, // length of helix 
								   int helixOffset, // usually 0 (starting directly at branch positions or 1 (starting 1 base pair later) deprecated
								   boolean setEqualMode,
								   String stemName,
								   BranchDescriptor3D branch1,
								   BranchDescriptor3D branch2,
								   double rmsLimit,
								   Object3D nucleotideDB)
    {
	assert branch1.isValid();
	assert branch2.isValid();
	log.fine("Starting RNA3DTools.generateStretchedRnaStem3D");
	// log.fine("Computing stem3d: " + strand1BasePos + "  " + directionOrig);
	Stem stem = new SimpleStem(startPos, stopPos, length, strand1.getSequence(), strand2.getSequence());
	if (!stem.isValid()) {
	    throw new ApplicationBugException("Invalid stem parameters in RNA3DTools.generateRnaStem3D!");
	}
	RnaStem3D stem3D = new SimpleRnaStem3D(stem);
	// computes optimized branch descriptor
	// log.fine("Starting optimizing branch descriptors : branch1: " + branch1 + " branch2: " + branch2);
	BranchDescriptor3D branch = optimizeStretchedBranchDescriptor(stem3D, branch1, branch2, rmsLimit);
	if (branch == null) {
	    return null; // no suitable solution found!
	}
	Properties bProp = branch.getProperties();
	assert bProp != null;
	String fitProperty = bProp.getProperty("fit_score");
	String stretchFactor = bProp.getProperty("stretch_factor");
	assert(stretchFactor != null);
	assert((fitProperty != null) && (fitProperty.length() > 0.0));
	// log.fine("Resulting optimized branch descriptors : branch: " + branch + " fit_score: " + fitProperty);
	Object3DLinkSetBundle resultBundle = generateRnaStem3D(strand1, strand2, startPos, stopPos, length, 0, 
							       setEqualMode, stemName, branch, nucleotideDB);
	Object3D resultTree = resultBundle.getObject3D();
	Object3DSet resultStems = Object3DTools.collectByClassName(resultTree, "RnaStem3D");
	assert resultStems.size() == 1;
	Object3D resultStem = resultStems.get(0);
	Properties prop = resultStem.getProperties();
	if (prop == null) {
	    prop = new Properties();
	}
	prop.setProperty("fit_score", fitProperty); // used later for evaluating fit of axial kissing loop!
	prop.setProperty("stretch_factor", stretchFactor); // used later for evaluating fit of axial kissing loop!
	resultStem.setProperties(prop);
// 	assert(strand1.getResidue3D(startPos).getPosition().distance(branch1.computeHelixPosition(1, BranchDescriptor3D.OUTGOING_STRAND)) < RMS_LARGE_LIMIT);
// 	assert(strand2.getResidue3D(stopPos).getPosition().distance(branch1.computeHelixPosition(1, BranchDescriptor3D.INCOMING_STRAND)) < RMS_LARGE_LIMIT);
// 	assert(strand1.getResidue3D(startPos + length - 1).getPosition().distance(branch2.computeHelixPosition(1, BranchDescriptor3D.INCOMING_STRAND)) < RMS_LARGE_LIMIT);
	// assert(strand2.getResidue3D(stopPos - (length - 1)).getPosition().distance(branch2.computeHelixPosition(1, BranchDescriptor3D.OUTGOING_STRAND)) < RMS_LARGE_LIMIT);
	return resultBundle;
    }
    */

    /** convinience method to generate SimpleRnaStem3D object */
    public static Object3DLinkSetBundle generateRnaStem3D(RnaStrand strand1, 
							  RnaStrand strand2, 
							  int startPos,
							  int stopPos, 
							  int length, 
							  boolean setEqualMode,
							  String stemName) 
    {
	assert false;
	Stem stem = new SimpleStem(startPos, stopPos, length, strand1, strand2);
	if (!stem.isValid()) {
	    throw new ApplicationBugException("Invalid stem parameters in RNA3DTools.generateRnaStem3D!");
	}
	Vector3D base = strand1.getResidue3D(startPos).getPosition();
	Vector3D endPos = strand1.getResidue3D(startPos+ length -1).getPosition();
	Vector3D direction = endPos.minus(base);
	direction.normalize();
	return generateRnaStem3D(strand1, //TODO: uses deprecated method
				 strand2,
				 startPos,
				 stopPos,
				 length,
				 //helixOffset, //TODO: add
				 setEqualMode,
				 stemName, 
				 //branch, //TODO: add
				 //nucleotideDB); //TODO: add
				 base, //TODO: remove
				 direction); //TODO: remove
    }



    /** searches for closest 5' neighbor that is part of a stem */
    static int find5PrimeStem(RnaStrand strand, int pos, Object3DSet stemSet) {
	for (int i = pos - 1; i >= 0; --i) {
	    Nucleotide3D residue = (Nucleotide3D)(strand.getResidue3D(i));
	    boolean result = StemTools.isPartOfStem(residue, stemSet);
	    if (result == true) {
		return i;
	    }
	}
	return -1; // not found
    }

    /** searches for closest 5' neighbor that is part of a stem */
    static int find3PrimeStem(RnaStrand strand, int pos, Object3DSet stemSet) {
	for (int i = pos + 1; i < strand.getResidueCount(); ++i) {
	    Nucleotide3D residue = (Nucleotide3D)(strand.getResidue3D(i));
	    boolean result = StemTools.isPartOfStem(residue, stemSet);
	    if (result == true) {
		return i;
	    }
	}
	return -1; // not found
    }


    /** searches all known stems TODO: no recursive search !*/
    public static Object3DSet collectRnaStrand(Object3D root) {
	Object3DSet stemSet = new SimpleObject3DSet();
	for (int i = 0; i < root.size(); ++i) {
	    Object3D child = root.getChild(i);
	    if (child instanceof RnaStrand) {
		stemSet.add(child);
	    }
	}
	return stemSet;
    }

    /** leaves all residue positions that are involved in a stem as fixed, interpolates the other positions */
    public static void interpolateNonStems(RnaStrand strand, Object3D root) {
	Object3DSet stemSet = StemTools.collectStems3D(root);
	for (int i = 0; i < strand.getResidueCount(); ++i) {
	    Nucleotide3D residue = (Nucleotide3D)(strand.getResidue3D(i)); // returns i'th nucleotide
	    if (!StemTools.isPartOfStem(residue, stemSet)) {
		int pos5 = find5PrimeStem(strand, i, stemSet); // first stem position if one walks towards 5' position
		int pos3 = find3PrimeStem(strand, i, stemSet); // first stem positioon if one walks towards 3' position
		if (pos5 >= 0) {
		    if (pos3 >= 0) {
			Vector3D meanPos = Vector3D.mean(strand.getResidue3D(pos5).getPosition(),
							 strand.getResidue3D(pos3).getPosition());
			residue.setPosition(meanPos);
		    }
		    else { // 5' end has stem, 3' end has none
			Vector3D direction = new Vector3D(0.0, 0.0, 0.0);
			int pos5Count = pos5;
			do {
			    --pos5Count;
			    if (pos5Count >= 0) {
				direction = strand.getResidue3D(pos5).getPosition();
				direction.sub(strand.getResidue3D(pos5Count).getPosition());
			    }
			    else {
				break;
			    }
			}
			while (direction.lengthSquare() == 0.0);
			if (direction.lengthSquare() == 0.0) {
			    // very odd case, no direction could be found:
			    log.severe("Warning: Could not find 5' stem direction!");
			}
			else {
			    direction.normalize();
			    Vector3D newPos = strand.getResidue3D(pos5).getPosition()
				.plus(direction.mul(RESIDUE_DIST * (double)(i - pos5)));
			    residue.setPosition(newPos);
			}
		    }
		}
		else if (pos3 >= 0) { // 3' end has stem, 5' has none
			Vector3D direction = new Vector3D(0.0, 0.0, 0.0);
			int pos3Count = pos3;
			do {
			    ++pos3Count;
			    if (pos3Count < strand.getResidueCount()) {
				direction = strand.getResidue3D(pos3).getPosition();
				direction.sub(strand.getResidue3D(pos3Count).getPosition());
			    }
			    else {
				break;
			    }
			}
			while (direction.lengthSquare() == 0.0);
			if (direction.lengthSquare() == 0.0) {
			    // very odd case, no direction could be found:
			    log.severe("Warning: Could not find 3' stem direction!");
			}
			else {
			    direction.normalize();
			    Vector3D newPos = strand.getResidue3D(pos3).getPosition().plus(
											 direction.mul(RESIDUE_DIST * (double)(pos3-i)));
			    residue.setPosition(newPos);
			}
		}
		// else do nothing (no stems whatsoever)
	    }
	    // if isPartOfStem keep position fixed
	}
    }

    /** generates and inserts "Stem" (two SequenceBindingSite objects and one link */
    /*
      public static void addStem(Sequence s1, Sequence s2, int startPos, int stopPos, int length, LinkSet links) {
      Object o1 = s1.getParentObject();
      Object o2 = s2.getParentObject();
      if ((o1 instanceof RnaStrand) && (o2 instanceof RnaStrand)) {
      RnaStrand p1 = (RnaStrand)o1;
      RnaStrand p2 = (RnaStrand)o2; // parent objects that "possess" the sequence
      int n1  = p1.getResidueCount();
      int n2  = p2.getResidueCount();
      if (((startPos + length - 1)< n1)
      && (stopPos < n2) && ((stopPos - length + 1)>= 0)) {
      for (int i = 0; i < length; ++i) {
      links.add(new SimpleConstraintLink(p1.getResidue3D(startPos+i),p2.getResidue3D(stopPos-i), RNA_WC_CONSTRAINT)); 
      }
      }
      else { // TODO real error thrown
      log.info("Bad stem ids!");
      }
      }
      else {
      log.severe("Sequence objects did not have RnaStrand parents!");
      }
      }
    */

    /** generates RNA strand with one Nucleotide3D object per residue and no atoms */
    public static Object3DLinkSetBundle generateSimpleRnaStrand(String name,
								String seqString, Vector3D pos , Vector3D dir) throws UnknownSymbolException {
// 	log.fine("starting generateSimpleRnaStrand with " + name +
// 			   " " + seqString);
	Alphabet alphabet = DnaTools.AMBIGUOUS_RNA_ALPHABET;
	RnaStrand strand = new SimpleRnaStrand(alphabet);
	strand.setName(name);
	strand.setPosition(pos);
	// log.fine("generated initial strand " +strand.size() + " " + seqString);
	// RnaStrand helpStrand = new SimpleRnaStrand(seqString, name);
	String seqName = name + ".seq";
	Sequence seq = new SimpleSequence(seqString, seqName, alphabet); // strand.getSequence(0);
	// log.fine("Using sequence: " + seq.size() + " " + seq);
	int residueCount = seqString.length();
	for (int i = 0; i < residueCount; ++i) {
	    LetterSymbol symbol = seq.getResidue(i).getSymbol();
	    Nucleotide3D residue = new Nucleotide3D(symbol, pos.plus(dir.mul(RESIDUE_DIST*i))); // seq, i, 
	    // log.fine("Generated nucleotide " + i + " : " + residue);
	    double r = RnaPhysicalProperties.RNA_AVERAGE_BASE_RADIUS;
	    residue.setDimensions(new Vector3D(r, r, r));
	    Character character = new Character(symbol.getCharacter());
	    String resName = "" + character.toString() + (i+1);
	    residue.setName(resName);
	    residue.setBoundingRadius(r);
	    strand.insertChild(residue);
	    // log.fine("added residue! " +strand.size());
	}
	LinkSet links = new SimpleLinkSet();
	InteractionType backboneType = new RnaInteractionType(RnaInteractionType.BACKBONE); // describes backbone interaction
	for (int i = 1; i < strand.size(); ++i) {
	    Interaction backboneInteraction = new SimpleInteraction((Residue3D)(strand.getChild(i-1)), 
								    (Residue3D)(strand.getChild(i)), backboneType);
	    // constraintLink keeps all the links in the frame
//  	    links.add(new SimpleConstraintLink(strand.getChild(i-1),strand.getChild(i), RNA_CHAIN_CONSTRAINT)); 
   	    links.add(new InteractionLinkImp(strand.getChild(i-1),strand.getChild(i), backboneInteraction));
	    // log.fine("links added! " + links.size());
	}
	// log.fine("Finished generateSimpleRnaStrand: " + strand.size() + " " + links.size());
	return new SimpleObject3DLinkSetBundle(strand, links);
    }

    /** leaves residue 0,...,n-1 in strand and returns new strands
     * with residues n, ..., length-1 */
    RnaStrand cutStrand(RnaStrand strand, int n) {
	int origLen = strand.getResidueCount();
	RnaStrand newStrand = new SimpleRnaStrand();
	String name = strand.getName() + "_cut"+n;
	newStrand.setName(name);
	Vector3D newPos = new Vector3D(0,0,0);
	Vector3D oldPos = new Vector3D(0,0,0);
	for (int i = n; i < strand.getResidueCount(); ++i) {
	    newStrand.insertChild(strand.getResidue3D(i));
	    newPos.add(strand.getResidue3D(i).getPosition());
	}
	newPos.scale(1.0/(double)(newStrand.getResidueCount()));
	for (int i = strand.getResidueCount()-1; 
	     i >= 0; --i) {
	    strand.removeChild(i);
	}
	for (int i = 0; i < strand.getResidueCount(); ++i) {
	    oldPos.add(strand.getResidue3D(i).getPosition());
	}
	oldPos.scale(1.0/(double)(strand.getResidueCount()));
	assert origLen == (newStrand.getResidueCount()+strand.getResidueCount());
	return newStrand;
    }

}
