package rnadesign.rnamodel;

import java.util.ResourceBundle;
import tools3d.Vector3D;
import tools3d.Matrix3D;
import tools3d.Matrix4D;

public class RnaConstants {

    private static final ResourceBundle rb = ResourceBundle.getBundle("RnaConstants");

    public static final double ATOM_COLLISION_DISTANCE = Double.parseDouble(rb.getString("ATOM_COLLISION_DISTANCE")); // collision distance for non-hydrogen atoms

    // public static final double HELIX_DELTA_ANGLE = Double.parseDouble(rb.getString("HELIX_DELTA_ANGLE")) * PackageConstants.DEG2RAD; // deprecated wrong: rize in z-coordinate per base-pair in helix

    // public static final double HELIX_CENTER_DIST = Double.parseDouble(rb.getString("HELIX_CENTER_DIST")); // 5.0; // deprecated distance of center of base pair from helix axis

    // public static final double HELIX_BASE_DIST = 5.0; // deprecated distance of centers of bases of base pair

    public static final double HELIX_PHASE_OFFSET = Double.parseDouble(rb.getString("HELIX_PHASE_OFFSET")) * PackageConstants.DEG2RAD; // 93 degree

    public static final double RESIDUE_DIST = Double.parseDouble(rb.getString("RESIDUE_DIST")); // 3.5;

    // public static final double WATSON_CRICK_DIST_MIN = Double.parseDouble(rb.getString("WATSON_CRICK_DIST_MIN")); // 3.0;

    public static final double WATSON_CRICK_DIST = Double.parseDouble(rb.getString("WATSON_CRICK_DIST")); // 4.0;

    // public static final double WATSON_CRICK_DIST_MAX = Double.parseDouble(rb.getString("WATSON_CRICK_DIST_MAX")); // 5.0;

    ////////// values taken from Aalberts 2005, Nucleic Acids Research
    // TODO move to RnaPhysicalParameters
    public static final double HELIX_BASE_PAIRS_PER_TURN = Double.parseDouble(rb.getString("HELIX_BASE_PAIRS_PER_TURN")); // 11.2;

    public static final double HELIX_RADIUS = Double.parseDouble(rb.getString("HELIX_RADIUS")); // 9.9; 

    /** distance between P and O3* atoms of base paired bases */
    public static final double HELIX_BP_P_O3_DIST = Double.parseDouble(rb.getString("HELIX_BP_P_O3_DIST")); // 15.0; 

    /* Standard position of C4' atom in norm helix */
    public static final Vector3D C4_NORM_POS = new Vector3D(HELIX_RADIUS, 0.0, 0.0); 

    /** rize in z-coordinate per base-pair in helix or height per stack */
    public static final double HELIX_RISE = Double.parseDouble(rb.getString("HELIX_RISE")); // 2.7; 

    /** Height of a turn */
    public static final double TURN_HEIGHT = HELIX_RISE * HELIX_BASE_PAIRS_PER_TURN;
    
    /** Distance of helices from n-way junction.  */
    public static final double JUNCTION_OFFSET = HELIX_RADIUS;

    /** vertical offset between complementary strands */
    public static final double HELIX_OFFSET = Double.parseDouble(rb.getString("HELIX_OFFSET")); // -4.2;

    /** vertical offset between complementary strands */

    /** Defines how to tranform one base pair to subsequence one */
    public static final Matrix4D HELIX_MATRIX4D = getHelixPropagationMatrix();

    public static Matrix4D getHelixPropagationMatrix() {
	Vector3D translation = new Vector3D(0, 0, HELIX_RISE);
	double anglePerBp = PackageConstants.DEG2RAD * (360.0/HELIX_BASE_PAIRS_PER_TURN);
	double phi = anglePerBp;
	Matrix3D rotMatrix = new Matrix3D(Math.cos(phi), Math.sin(phi), 0,
            				- Math.sin(phi), Math.cos(phi), 0,
					  0.0, 0.0, 1.0);
	return new Matrix4D(rotMatrix, translation);
    }

}
