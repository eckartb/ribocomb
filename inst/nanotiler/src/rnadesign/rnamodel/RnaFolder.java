package rnadesign.rnamodel;

import java.util.Properties;
import tools3d.objects3d.Object3D;

public interface RnaFolder {

    public Properties fold(Object3D obj);

}
