package rnadesign.rnamodel;

import generaltools.ApplicationBugException;
import generaltools.ConstraintDouble;
import generaltools.DoubleFunctor;
import generaltools.SimpleConstraintDouble;
import tools3d.objects3d.modeling.*;

public class RnaForceFieldFactory implements ForceFieldFactory {

    /** weight of nucleotide pair potential */
    private double nucPairForceFieldWeight = 1.0;
    /** weight of nucleotide stacking potential */
    private double nucLinkForceFieldWeight = 1.0;

    public static final int DEFAULT_FORCEFIELD = 0;

    public static final int FANCY_FORCEFIELD = 1;

    public static final String[] FORCEFIELD_NAMES = { "default", "fancy" };

    private int  forceFieldId = DEFAULT_FORCEFIELD;

    public int getForceFieldId() { return forceFieldId; }

    /** creates force field between bases that only depends on distance */
    private ForceFieldPairElement generateDefaultNucleotideForceFieldPairElement() {
	double sigma = 2.0 * RnaPhysicalProperties.RNA_AVERAGE_BASE_RADIUS;
	DoubleFunctor gaussFunctor = new GaussianFunctor(sigma, 0.0, 1000);
	Nucleotide3D nucleotide = new Nucleotide3D(); // prototype
	ForceFieldPairElement ffPairElement = new SimpleDistanceForceFieldPairElement(nucleotide, gaussFunctor,
						-1, -1);
	return ffPairElement;
    }

    /** creates force field between bases that only depends on distance */
    private ForceField generateDefaultNucleotideLinkForceField() {
	double cd =  RnaPhysicalProperties.RNA_BP_DISTANCE;
	double cdt =  RnaPhysicalProperties.RNA_BP_DISTANCE_TOLERANCE;
	double ca =  RnaPhysicalProperties.RNA_BP_ANGLE; // about 36 degree angle for consecutive base pairs
	double cat =  RnaPhysicalProperties.RNA_BP_ANGLE_TOLERANCE;
	double cb =  RnaPhysicalProperties.RNA_BP_BEND; 
	double cbt =  RnaPhysicalProperties.RNA_BP_BEND_TOLERANCE;
	ConstraintDouble constraint =  new SimpleConstraintDouble(cd-cdt, cd+cdt, 1.0);
	DoubleFunctor functor = new SquareFunctor(0.0, 1.0, 0.0); // x-offs, scale, y-offs: (x-x0)2 + y
	ConstraintDouble angleConstraint = new SimpleConstraintDouble(ca - cat, ca + cat, 1.0);
	DoubleFunctor angleFunctor = new SquareFunctor(0.0, 1.0, 0.0); // x-offs, scale, y-offs: (x-x0)2 + y
	ConstraintDouble bendConstraint =  new SimpleConstraintDouble(cb - cbt, cb + cbt, 1.0);
	DoubleFunctor bendFunctor =  new SquareFunctor(0.0, 1.0, 0.0); // x-offs, scale, y-offs: (x-x0)2 + y
	ForceField ff = new BasePairStackingForceField2(constraint, functor,
							angleConstraint, angleFunctor,
							bendConstraint, bendFunctor);
	return ff;
    }

    private ForceField generateDefaultForceField() {
	CompositeForceField forceField = new SimpleCompositeForceField();
	forceField.addForceField(new ConstraintForceField(1.0), 1.0);
	// forceField.addForceField(new GaussianRepulsionForceField(), 1.0);
	FlexibleForceField flexField = new SimpleFlexibleForceField();
	ForceFieldPairElement nucPairForceField = generateDefaultNucleotideForceFieldPairElement();
	flexField.addForceFieldPairElement(nucPairForceField);
	forceField.addForceField(flexField, nucPairForceFieldWeight);
	ForceField nucLinkForceField = generateDefaultNucleotideLinkForceField();
	forceField.addForceField(nucLinkForceField, nucLinkForceFieldWeight);
	return forceField;
    }

    private ForceField generateFancyForceField() {
	ForceField forceField = new ConstraintForceField(1.0);
	return forceField;
    }

    public ForceField generateForceField() {
	ForceField ff = null;
	switch (forceFieldId) {
	case DEFAULT_FORCEFIELD:
	    ff = generateDefaultForceField();
	    break;
	case FANCY_FORCEFIELD:
	    ff = generateFancyForceField();
	    break;
	default:
	    throw new ApplicationBugException("Internal error in line 20!");
	}
	return ff;
    }

    public String[] getForceFieldNames() { return FORCEFIELD_NAMES; }

    public void setForceFieldId(int n) { this.forceFieldId = n; }

}
