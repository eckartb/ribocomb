package rnadesign.rnamodel;

public class RnaFragment {

    RnaStrand strand;

    int startPos; // inclusive start position

    int stopPos; // inclusive stop position

    public RnaFragment(RnaStrand _strand, int _startPos, int _stopPos) {
	assert (_stopPos >= _startPos);
	strand = _strand;
	startPos = _startPos;
	stopPos = _stopPos;
    }

    public int getStartPos() { return startPos; }

    public int getStopPos() { return stopPos; }

    public int length() {
	return stopPos - startPos + 1;
    }
    
    public String toString() { return "(Fragment " + strand.getFullName() + " " + (startPos+1) + " " + (stopPos+1) + " )"; }

}