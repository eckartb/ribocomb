package rnadesign.rnamodel;

import numerictools.PotentialND;

import tools3d.Vector3D;
import tools3d.objects3d.LinkSet;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DSet;
import java.util.*;

/** potential in n dimensions */
public class RnaIntegerPotential implements PotentialND {

    private LinkSet links;
    private Object3DSet objSet;
    private List<Integer> linkIds1;
    private List<Integer> linkIds2;
    private double turnHeight;
    private double offset;

    public RnaIntegerPotential(Object3DSet objSet, LinkSet links, double turnHeight, double offset) {
	this.links = links;
	this.objSet = objSet;
	this.turnHeight = turnHeight;
	this.offset = offset;
	initIds();

	// some tests:
	assert Math.abs(getDistanceScore(3.0 * turnHeight+2*offset)) < 0.001;
	assert Math.abs(getDistanceScore(3.0 * turnHeight+2*offset + 5.7)-5.7) < 0.001;
	assert Math.abs(getDistanceScore(3.0 * turnHeight+2*offset - 5.7)-5.7) < 0.001;

    }

    private void initIds() {
	assert links != null;
	linkIds1 = new ArrayList<Integer>();
	linkIds2 = new ArrayList<Integer>();
	for (int i = 0; i < links.size(); ++i) {
	    Object3D obj1 = links.get(i).getObj1();
	    Object3D obj2 = links.get(i).getObj2();
	    int id1 = objSet.indexOf(obj1);
	    int id2 = objSet.indexOf(obj2);
	    if ((id1 >= 0) && (id1 < objSet.size()) && (id2 >= 0) && (id2 < objSet.size())) {
		linkIds1.add(id1); // autoboxing
		linkIds2.add(id2);
	    }
	}
    }

    public double[] generateLowPosition() {
	double[] result = new double[getDimension()];
	for (int i = 0; i < objSet.size(); ++i) {
	    Vector3D pos = objSet.get(i).getPosition();
	    result[3 * i] = pos.getX();
	    result[(3 * i) + 1] = pos.getY();
	    result[(3 * i) + 2] = pos.getZ();
	}
	return result;
    }

    public double[] generateHighPosition() { return generateLowPosition(); }

    public int getDimension() { return objSet.size() * 3; }

    public double getValue(double[] position) {
	double sum = 0.0;
	for (int i = 0; i < linkIds1.size(); ++i) {
	    sum += getValue(position, i);
	}
	return sum;
    }

    /** Returns score with respect to n'th link */
    private double getValue(double[] position, int linkId) {
	int objId1 = linkIds1.get(linkId);;
	int objId2 = linkIds2.get(linkId);
	int off1 = objId1 * 3;
	int off2 = objId2 * 3;
	assert off1 != off2;
	Vector3D pos1 = new Vector3D(position[off1], position[off1+1], position[off1+2]);
	Vector3D pos2 = new Vector3D(position[off2], position[off2+1], position[off2+2]);
	double distance = pos1.distance(pos2);
	return getDistanceScore(distance);
    }

    /** Scores deviation from perfect distance. Perfect distance: 2 * offset + N * height */
    public double getDistanceScore(double distance) {
	distance -= 2 * offset;
	if (distance <= turnHeight) {
	    return turnHeight - distance;
	}
	int nTurns = (int)(distance / turnHeight); // integer number of turns
	double result1 = Math.abs(distance - (nTurns * turnHeight));
	int nTurns2 = nTurns + 1;
	double result2 = Math.abs(distance - (nTurns2 * turnHeight));
	if (result1 < result2) {
	    return result1;
	}
	return result2;
    }

}
