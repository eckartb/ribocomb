package rnadesign.rnamodel;

import tools3d.objects3d.*;
import rnasecondary.*;

public class RnaLinkTools {

    /** Returns true if link corresponds to a base pair interaction */
    public static boolean isBasePairLink(Link link) {
	if (! (link instanceof InteractionLink)) {
	    return false;
	}
	InteractionLink interactionLink = (InteractionLink)link;
	Interaction interaction = interactionLink.getInteraction();
	InteractionType interactionType = interaction.getInteractionType();
	if (interactionType instanceof RnaInteractionType) {
	    if (interactionType.getSubTypeId() != RnaInteractionType.BACKBONE) {
		return true;
	    }
	}
	return false;
    }

}
