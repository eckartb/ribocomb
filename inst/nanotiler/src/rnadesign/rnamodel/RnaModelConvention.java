package rnadesign.rnamodel;

import java.io.InputStream;

import generaltools.MalformedInputException;

/** all classes and interfaces of this package must implement these methods
 */
public interface RnaModelConvention {

    /** returns class name. Implementations often return the name
     * of their nearest interface */
    public String getClassName();

    /** reads object */
    public void read(InputStream is) throws MalformedInputException;

    /** writes object to string */
    public String toString();

}
