package rnadesign.rnamodel;

public class RnaModelException extends Exception {

    public RnaModelException(String message) {
	super(message);
    }

    public RnaModelException(Exception e) {
	super(e);
    }

}
