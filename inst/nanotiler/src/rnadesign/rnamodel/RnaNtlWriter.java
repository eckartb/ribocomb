package rnadesign.rnamodel;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.logging.Logger;

import tools3d.objects3d.LinkSet;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DLinkSetBundle;
import tools3d.objects3d.Object3DWriter;

/**
 * @author Christine Viets
 */
public class RnaNtlWriter implements Object3DWriter {

    public static final String NANOTILER_STRING = "nanotiler";
    public static final String NEWLINE = System.getProperty("line.separator");
    public static final String NTL_EXTENSION = ".ntl";
    public static final String PDB_EXTENSION = ".pdb";
    public static String SLASH = File.separator;

    private Logger log = Logger.getLogger("NanoTiler_debug");

    public RnaNtlWriter() { }

    /**
     * Writes junction or kissing loop to file "rnano4/db/nanotiler/....ntl".
     *
     * @param writeDir The directory to write to.
     * @param fileName Something like "/home/you/rnano/db/RnaSpot/6TNA.pdb_kl1"
     * @param junction The junction or kissing loop to be written to a file.
     */
    public void write(String writeDir,
		      String fileName,
		      StrandJunction3D junction) {
	assert writeDir != null;
	assert fileName != null;
	assert junction != null;
	log.info("Writing ntl file for: " + fileName + " to directory: " + writeDir);
	fileName = fileName.substring(fileName.lastIndexOf(SLASH) + 1);
	writeDir = writeDir + SLASH + 
	    fileName.substring(0, fileName.indexOf(PDB_EXTENSION)) + SLASH; // TODO: make sure slashes are correct! Christine
	(new File(writeDir)).mkdir();
	if (fileName.endsWith(PDB_EXTENSION)) {
	    fileName = fileName.substring(0,
					  fileName.lastIndexOf(PDB_EXTENSION));
	}
	if (!fileName.endsWith(NTL_EXTENSION)) {
	    fileName += NTL_EXTENSION;
	}
	fileName = writeDir + fileName;

	try {
	    log.info("Writing file: " + fileName);
	    FileWriter fw = new FileWriter(fileName);
	    BufferedWriter bw = new BufferedWriter(fw);
	    bw.write(NANOTILER_STRING + NEWLINE);
	    bw.write(junction.toString());
	    bw.close();
	    log.info("Finished writing file: " + fileName);
	}
	catch (IOException e) {
	    log.severe("Could not write file: " + fileName);
	}
    }

    /**
     * Files are only here because they are in the class this derives from:
     * they are not used!
     */
    public int getFormatId() { assert false; return 0; }
    
    public void reset() { }

    public void write(OutputStream os, LinkSet links) {	assert false; }

    public void write(OutputStream os, Object3D obj) { assert false; }
    
    public String writeString(Object3D obj) { assert false; return new String(); }

}
