/**
 * Factory class is responsible for reading/parsing a
 * PDB file and creating a corresponding Object3DGraph.
 */
package rnadesign.rnamodel;

import java.awt.Color;
import java.io.DataInputStream;
import java.io.InputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Logger;

import rnasecondary.Interaction;
import rnasecondary.InteractionType;
import rnasecondary.RnaInteractionType;
import rnasecondary.SimpleInteraction;
import sequence.DnaTools;
import sequence.LetterSymbol;
import sequence.SimpleLetterSymbol;
import sequence.UnknownSymbolException;
import tools3d.Vector3D;
import tools3d.objects3d.LinkSet;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DFactory;
import tools3d.objects3d.Object3DIOException;
import tools3d.objects3d.Object3DLinkSetBundle;
import tools3d.objects3d.Object3DSet;
import tools3d.objects3d.Object3DTools;
import tools3d.objects3d.RotationInfo;
import tools3d.objects3d.SimpleLinkSet;
import tools3d.objects3d.SimpleObject3D;
import tools3d.objects3d.SimpleObject3DLinkSetBundle;
import tools3d.objects3d.SimpleObject3DSet;

/**
 * @author Eckart Bindewald
 */
public class RnaPdbReader extends AbstractPdbReader implements Object3DFactory {

    public static final char BAD_RESIDUE_CHAR = 'X';
    // public static final double COLLISION_DISTANCE = 0.7;
    public static final int PRECISION = 3;
    public static final int NO_TRANSLATION = 0;
    public static final int AMBER_TO_STANDARD = 1;

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    public static char defaultStrandName = 'A'; // name of strand instead of "space" character

    private int translationMode = NO_TRANSLATION; // AMBER_TO_STANDARD;

    public boolean debugMode = false;
    public boolean dnaMode = false;

    /** Constructor needs a buffered reader (provides methods for linewise reading) */
    public RnaPdbReader() { }
    
    /** reads object and links. */
    public Object3DLinkSetBundle readBundle(InputStream is) throws Object3DIOException {
	DataInputStream dis = new DataInputStream(is);
// 	if (readWord(dis).equals("nanotiler")) { // The file was written by NanoTiler.
// 	    Object3DLinkSetBundle bundle = reRead(is);
// 	    return bundle;
// 	}
// 	else {
	    if (debugMode) {
		log.fine("starting RnaPdbReader.readBundle!");
	    }
	    Object3D obj = readAnyObject3D(is);
	    LinkSet links = new SimpleLinkSet();
	    // add links:
	    addBackboneLinks(obj, links);
	    addCovalentLinks(obj, links);
	    if (debugMode) {
		log.fine("Finished RnaPdbReader.readBundle: PDB file with " + obj.size() + " strands read: Position: " + obj.getPosition());
	    }
	    return new SimpleObject3DLinkSetBundle(obj, links);
// 	}
    }

    // Christine: TODO: Method not yet implemented!
//     public Object3DLinkSetBundle reRead(InputStream is) throws Object3DIOException {
// 	DataInputStream dis = new DataInputStream(is);
// 	String expected = "(SimpleObject3DLinkSetBundle";
// 	String word = readWord(dis);
// 	if (word != expected) {
// 	    throw new Object3DIOException("reRead: read: " + word + " instead of expected: " + expected);
// 	}
// 	readWord(dis);
// 	//	Object3D obj = reReadAnyObject3D(is);
// 	//	LinkSet links = reReadLinks(is); //?
// 	return new SimpleObject3DLinkSetBundle();
//     }

    /* (non-Javadoc)
     * @see rnamodel.Object3DGraphFactory#createObject3DGraph()
     */
    public Object3D readAnyObject3D(InputStream is) throws Object3DIOException {
	if (debugMode) {
	    log.fine("Starting RnaPdbReader.readAnyObject3D!");
	}
	//	Atom3DSet atom3dSet = new Atom3DSet();
	DataInputStream dis = new DataInputStream(is);
	Object3D root = new SimpleObject3D();
	Object3DSet atomSet = new SimpleObject3DSet();
	root.setName("PdbImport");
	char currentStrandName = '_';
	int currentResidueId = -9999;
	int currentAtomId = -9999;
	int lineCounter = 0;
	NucleotideStrand currentStrand = null;
	Nucleotide3D currentResidue = null;
	int strandCounter = 0;
	String line;
	boolean strandEndFlag = false;
	try {
	    do {
		line = dis.readLine(); // Read new atom.
		++lineCounter;
		if (debugMode) {
		    log.fine("Read line " + lineCounter + ": " + line);
		}
		if (line == null) {
		    if (debugMode) {
			log.fine("Quitting loop because end of file reached! Line number: " + lineCounter);
		    }
		    break;
		}
		if (isTer(line) || isEndMdl(line)) { // Check for TER or ENDMDL keyword.
		    if (debugMode) {
			log.fine("TER or ENDMDL keyword found!");
		    }
		    strandEndFlag = true;
		    continue;
		}
		if (isAtom(line)) {
		    int residueId = getResidueId(line);
		    String residueName = getResidueName(line);
		    if (!isLegalResidueName(residueName)) {
			System.out.println("# Residue name is not legal: " + residueName);
			continue; // skip strange residues like for example amino acids
		    }
		    else {
			// System.out.println("# Residue name is legal: " + residueName);
		    }
		    String atomName = getAtomName(line);
		    char strandName = getStrandName(line);
		    char versionChar = getVersionChar(line);
		    if ((versionChar != ' ') && (versionChar != 'A')) {
			log.fine("Warning: ignoring alternative coordinate version: " + line);
			continue;
		    }
		    Atom3D atom = generateAtomFromPdb(line);
		    if (debugMode) {
			log.fine("Read atom: " + atom.getName() + " with position: " + atom.getPosition());
		    }
		    if (atomSet.size() > 0) {
			double minDist = findMinimumDistance(atom, atomSet);
			if (minDist < COLLISION_DISTANCE) {
			    log.fine("Warning: ignoring atom " + line);
			    continue; // Ignore this atom.
			}
		    }
		    if (currentStrand == null ||
			strandEndFlag ||
			((strandName!=' ') && (strandName != currentStrandName)) ||
			residueId < currentResidueId) {
			// Generate new strand:
			strandEndFlag = false; // Reset strand end flag.
			if (strandName == ' ') {
			    strandName = getDefaultStrandName(strandCounter);
			}
			++strandCounter;
			if (dnaMode) {
			    currentStrand = new SimpleRnaStrand(DnaTools.DNA_ALPHABET);
			}
			else {
			    currentStrand = new SimpleRnaStrand(DnaTools.RNA_ALPHABET);
			}
			currentStrand.setName("" + strandName);
			currentStrand.setProperty("pdb_chain_char", ""+strandName);
			currentStrandName = strandName;
			// currentStrand.setPosition(atom.getPosition());
			root.insertChild(currentStrand);
		    }
		    if ((currentResidue == null) ||
			(currentResidueId != residueId)) {
			// Generate new nucleotide: position is equal to position of first atom.
			try {
			    char c = getResidueCharacter(residueName);
			    LetterSymbol symbol;
			    if (dnaMode) {
				symbol = new SimpleLetterSymbol(c, DnaTools.DNA_ALPHABET);
			    }
			    else {
				symbol = new SimpleLetterSymbol(c, DnaTools.RNA_ALPHABET);
			    }
			    currentResidue = new Nucleotide3D(symbol,
							      // currentStrand.getSequence(),
							      // currentStrand.getResidueCount(),
							      atom.getPosition());
			    currentResidue.setAssignedNumber(currentResidueId);
			    currentResidue.setAssignedName(residueName);
			    String resName = "" + c + residueId;
			    currentResidue.setName(resName);
			    currentStrand.insertChild(currentResidue);
			    currentResidueId = residueId;
			}
			catch (UnknownSymbolException e) {
			    log.fine("Warning: could not interpret nucleotide symbol from line: " + line);
			    // do nothing otherwise
			}
		    }
		    if (debugMode) {
			log.fine("Inserting atom into strand " +
				 strandCounter + " " +
				 currentStrandName + " residue "
				 + currentResidue.getName() + " " +
				 currentResidue.getPos() + " : " + atom);
		    }
		    currentResidue.insertChild(atom);
		    //		    atom3dSet.addAtom(atom);
		    Vector3D newAtomPos = currentResidue.getChild(currentResidue.size() - 1).getPosition();
		    if (atom.getPosition().distance(newAtomPos) >= 0.001) {
			log.fine("Error!!! root: " + root.getPosition() + " strand " + currentStrand.getPosition() + " residue " + currentResidue.getPosition()
					    + " atom: " + atom.getPosition() + " new atom pos: " + newAtomPos);
			assert (atom.getPosition().distance(newAtomPos) < 0.001);
		    }
		    // TODO : cannot handle 
		}
		else {
		    if (debugMode) {
			log.fine("Line is not ATOM : " + line);
		    }
		}
	    }
	    while (line != null);
	}
	catch (IOException exp) {
	    // do nothing
	}
	Object3DTools.balanceTree(root);
	if (debugMode) {
	    log.fine("Finished RnaPdbReader.readAnyObject3D");
	}
	//	StrandJunctionDBTools.atom3dSet.merge(atom3dSet);
	return root;
    }
    
}

