/** Factory class is responsible for reading/parsing a PDB file and creating
 *  a corresponding Object3DGraph
 */
package rnadesign.rnamodel;

import java.io.*;
import java.util.*;
import java.util.logging.*;

import rnasecondary.Interaction;
import rnasecondary.InteractionType;
import rnasecondary.RnaInteractionType;
import rnasecondary.SimpleInteraction;
import sequence.DnaTools;
import sequence.LetterSymbol;
import sequence.SimpleLetterSymbol;
import sequence.UnknownSymbolException;
import tools3d.Vector3D;
import tools3d.objects3d.Link;
import tools3d.objects3d.LinkSet;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DFactory;
import tools3d.objects3d.Object3DIOException;
import tools3d.objects3d.Object3DLinkSetBundle;
import tools3d.objects3d.Object3DSet;
import tools3d.objects3d.Object3DTools;
import tools3d.objects3d.Object3DSetTools;
import tools3d.objects3d.RotationInfo;
import tools3d.objects3d.SimpleLinkSet;
import tools3d.objects3d.SimpleObject3D;
import tools3d.objects3d.SimpleObject3DLinkSetBundle;
import tools3d.objects3d.SimpleObject3DSet;
import launchtools.SimpleQueueManager;
import generaltools.StringTools;

import static rnadesign.rnamodel.PackageConstants.*;

/**
 * @author Eckart Bindewald
 *
 */
public class RnaPdbRnaviewReader extends AbstractPdbReader implements Object3DFactory {

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    public static String nanotilerHome = System.getenv("NANOTILER_HOME");

    public static String RNAVIEW_BIN = nanotilerHome + "/bin/addrnaview";

    public static final int RNAVIEW_STRAND_MAX = 36;

    public static String PDB_REMARKS = "pdb_remarks";

    public static final int WATSON_CRICK_BP = 1; // only consider GC base pairs

    public static final int CANONICAL_BP = 2; // consider GC, AU, GU

    public static final int NON_CANONICAL_BP = 3; // consider also non-canonical base pairs

    public static final int ALL_BP = 4;

    public static final int NOT_STACKED = 5;

    public boolean addRnaviewMode = true; // now we detect base pair types internally

    public boolean allowBackwardJump = false; // true is currently not correctly working!
    
    // Possible solution: NanoTiler command: controller bpallowed notstacked | canonical | wc | all | 
    public int allowedMode = NOT_STACKED; // CANONICAL_BP; // NOT_STACKED; // CANONICAL_BP; // WATSON_CRICK_BP; // NOT_STACKED; // ALL_BP; // NON_CANONICAL_BP; // ALL_BP

    public static boolean debugMode = false; // if true, write even more output and do not delete temporary files

    public boolean dnaMode = false;

    public boolean residueRenumberMode = false; // if true, renumber residues, starting from 1 in consecutive order

    public int verboseLevel = 1;

    /** Constructor needs a buffered reader (provides methods for linewise reading) */
    public RnaPdbRnaviewReader() {
    }

    /** if true, renumbers residues to start from one in consecutive order while reading */
    public boolean getResidueRenumberMode() { return residueRenumberMode; }

    public boolean getAllowBackwardJump() { return this.allowBackwardJump; }

    public boolean getAddRnaviewMode() { return this.addRnaviewMode; }

    /** Sets if RNAview is to be launched if info is missing */
    public void setAddRnaviewMode(boolean b) { 
	this.addRnaviewMode = b;  
    }

    public void setAllowedMode(int mode) { this.allowedMode = mode; }

    public void setAllowBackwardJump(boolean b) { 
	assert !b; 
	this.allowBackwardJump = b;  // b == true is currently not working properly. TODO (consult test case in ImportCommandTest, RingFixConstraintCommandTest)
    }

    /** compares two strand names. Special case of unnamed strand (space character): returns true if renamed to "A" */
    private boolean sameStrandNames(String assignedName, String pdbName) {
	if (" ".equals(pdbName)) {
	    return "A".equals(assignedName);
	}
	return assignedName.equals(pdbName);
    }

    /** Parses RNAVIEW annotation hidden in pdb_remarks properties: returns set of unique strand pairings: pairing c-b will be returned as BC. REMARK must have been cut out already with line.substring(8) command */
    private String parseRnaviewStrandPairing(String line) {

	line = line.trim();
	line = line.replace("\\s{2,}", " "); // translate white space into single spaces
	StringTokenizer tokens = new StringTokenizer(line);
	int count = 0;
	String chainName1 = "";
	String chainName2 = "";
	int res1 = 0;
	int res2 = 0;
	try {
	while (tokens.hasMoreTokens()) {
	    String token = tokens.nextToken();
	    // log.fine("Current token :" + token + ":");
	    // intClass = token;
// 	    if (count == 0) {
// 		String[] words = token.split("_");
// 		if (words.length != 2) {
// 		    continue;
// 		}
// 		words[1] = words[1].substring(0, words.length-1); // delete last comma
// 		res1 = Integer.parseInt(words[0]) - 1;
// 		res2 = Integer.parseInt(words[1]) - 1;
// 	    }
	    if (count == 1) {
		if (token.length() == 2) {
		    chainName1 = token.substring(0, 1);
		}
		else if (token.length() == 1) {
		    chainName1 = "A"; // empty
		}
		else {
		    log.warning("Weird line, no first chain name extracted: " + line);
		    return null;
		}
	    }
	    if (count == 5) {
		if (token.length() == 2) {
		    chainName2 = token.substring(0, 1);
		}
		else if (token.length() == 1) {
		    chainName2 = "A"; // empty
		}
		else {
		    log.warning("Weird line, no second chain name extracted: " + line);
		    return null;
		}
	    }
 	    if (count == 2) {
 		res1 = Integer.parseInt(token) - 1; // -1 : internal counting 
 	    }
 	    if (count == 4) {
 		res2 = Integer.parseInt(token) - 1; 
 	    }
	    ++count;
	}
	}
	catch (NumberFormatException nfe) {
	    log.info("Could not interpret RNAview line: " + line);
	    return null; // must have been bad line
	}
	if ((chainName1 == null) || (chainName2 == null)) {
	    return null;
	}
	if ((chainName1.length() != 1) || (chainName2.length() != 1)) {
	    return null;
	}
	SortedSet<Character> sortedSet = new TreeSet<Character>();
	sortedSet.add(new Character(chainName1.charAt(0)));
	sortedSet.add(new Character(chainName2.charAt(0)));
	Iterator<Character> iterator = sortedSet.iterator();
	String result = "";
	while (iterator.hasNext()) {
	    result = result + iterator.next();
	}
	if (result.length() == 1) {
	    result = result + result; // convert "A" to "AA"
	}
	return result;
    }

    /** Parses RNAVIEW annotation hidden in pdb_remarks properties: returns set of unique strand pairings */
    private Set<String> parseRnaviewStrandPairings(String[] lines) {
	Set<String> result = new HashSet<String>();
	int mode = 0;
	for (int i = 0; i < lines.length; ++i) {
	    if (lines[i].length() < 10) {
		continue;
	    }
	    if (!lines[i].startsWith("REMARK")) {
		continue;
	    }
	    String line = lines[i].substring(8);
	    if (line.startsWith("BEGIN_base-pair")) {
		mode = 1;
	    }
	    else if (line.startsWith("END_base-pair")) {
		break;
	    }
	    if (mode == 1) {
		String strandPairing = parseRnaviewStrandPairing(line);
		if (strandPairing != null) {
		    result.add(strandPairing);
		}
	    }
	}
	return result;
    }

    /** Parses RNAVIEW annotation hidden in pdb_remarks properties */
    private LinkSet parseRnaviewLinks(String[] lines, 
				      NucleotideStrand strand, NucleotideStrand strand2,
				      int strand1OffStart, int strand1OffEnd,
				      int strand2OffStart, int strand2OffEnd) {
	assert lines != null;
	assert strand != null;
	LinkSet links = new SimpleLinkSet();
	// String[] lines = rnaviewText.split("\\n"); // use newline as reg exp splitter // TODO: should be NEWLINE?
	if (lines == null) {
	    log.warning("Object3D " + strand.getName() + " has no muliple lines of RNAVIEW properties defined.");
	    return links;
	}
	log.finest("Parsing " + lines.length + " RNAVIEW info lines." + NEWLINE);
	int i = 0;
	// search for start keyword: BEGIN_base-pair
	for (i = 0; i < lines.length; ++i) {
	    if (lines[i].length() < 10) {
		continue;
	    }
	    String line = lines[i].substring(8); // skip keyword "REMARK", so that parsing is done on pure RNAVIEW output
	    if (line.startsWith("BEGIN_base-pair")) {
		++i;
		break;
	    }
	}
	if (i >= lines.length) {
	    log.warning("Could not find RNAVIEW keyword BEGIN_base-pair");
	    return links;
	}
	// parsing actual content
	int lineCounter = 0;
	for (; i < lines.length; ++i) {
	    ++lineCounter;
	    if (lines[i].length() < 10) {
		log.finest("RNAVIEW ine too short, skipping: " + lines[i]);
		continue;
	    }
	    String line = lines[i].substring(8); // skip keyword "REMARK", so that parsing is done on pure RNAVIEW output
	    if (line.startsWith("END_base-pair")) {
		break; // quit loop if final keyword matches
	    }
	    Link newLink = parseRnaviewLine(line, strand, strand2,
					    strand1OffStart, strand1OffEnd,
					    strand2OffStart, strand2OffEnd);
	    if (newLink != null) {
		links.add(newLink);
	    }
	    else {
		log.fine("No proper Watson-Crick pair in line: " + line);
	    }
	}
	log.finest("Anlyzed " + lineCounter + " RNAVIEW lines.");
	if (i >= lines.length) {
	    log.warning("Could not find RNAVIEW keyword END_base-pair");
	    return links;
	}
	
	return links;
    }

    /** parses an RNAVIEW output line, given a corresponding 3D Object */
    Link parseRnaviewLine(String line, NucleotideStrand strand, NucleotideStrand strand2,
			  int strand1OffStart, int strand1OffEnd,
			  int strand2OffStart, int strand2OffEnd) {
	assert line != null;
	assert strand != null;
	// log.info("Starting RnaPdbRnaviewRead.parseRnaviewLine: Parsing line: " + line + " for strands " + strand.getName() + " " + strand2.getName());
	line = line.trim();
	line = line.replace("\\s{2,}", " "); // translate white space into single spaces
	boolean sameNameMode = (strand != strand2) && (strand.getProperty(PDB_CHAIN_CHAR) != null)
	    && (strand.getProperty(PDB_CHAIN_CHAR).equals(strand2.getProperty(PDB_CHAIN_CHAR)))
	    && (strand1OffStart == strand2OffStart);
	StringTokenizer tokens = new StringTokenizer(line);
// 	if (words.length < 8) {
// 	    log.info("Skipping weird line with " + words.length + " tokens: " + line);
// 	    return null;
// 	}	
	String intClass = "";
// 	System.out.println("Parsed words: ");
// 	for (int i = 0; i < words.length; ++i) {
// 	    System.out.print(words[i] + ":");
// 	}
// 	System.out.println();
	// classes according to Leontis and Westhof
	int count = 0;
	int res1 = 0;
	int res2 = 0;
	String chainName1 = "" + getDefaultStrandName(0);
	String chainName2 = "" + getDefaultStrandName(0);
	String[] words = line.split(" ");
	if (words.length < 5) {
	    log.warning("Strange line (less then 5 words: " + line);
	    return null;
	}
	String[] numberWords = words[0].split("_");
	if (numberWords.length != 2) {
	    log.warning("Strange RNAview word: " + words[0] + " in line: " + line);
	    return null;
	}
	int absNumber1 = Integer.parseInt(numberWords[0])-1;
	int absNumber2 = Integer.parseInt(numberWords[1].replaceAll(",",""))-1;
	if ((absNumber1 < strand1OffStart) || (absNumber1 >= strand1OffEnd)
	    || (absNumber2 < strand2OffStart) || (absNumber2 >= strand2OffEnd)) {
 	    log.fine("Non-matching indices: " + strand1OffStart + " " + strand1OffEnd + " " + strand2OffStart + " " + strand2OffEnd
 			       + " for residues in line:  " 
 			       + absNumber1 + " " + absNumber2 + " " + line + " " + strand.getFullName() + " " + strand2.getFullName());
	    return null; // residue indices out of range: must be different strand with possibly same name
	}
	while (tokens.hasMoreTokens()) {
	    String token = tokens.nextToken();
	    // log.info("Current token :" + token + ":");
	    intClass = token;
// 	    if (count == 0) {
// 		String[] words = token.split("_");
// 		if (words.length != 2) {
// 		    continue;
// 		}
// 		words[1] = words[1].substring(0, words.length-1); // delete last comma
// 		res1 = Integer.parseInt(words[0]) - 1;
// 		res2 = Integer.parseInt(words[1]) - 1;
// 	    }
	    if (count == 1) {
		if (token.length() == 2) {
		    chainName1 = token.substring(0, 1);
		}
		else if (token.length() == 1) {
		    chainName1 = "A"; // empty name
		}
		else {
		    log.warning("Weird line, no first chain name extracted: " + line);
		    return null;
		}
	    }
	    if (count == 5) {
		if (token.length() == 2) {
		    chainName2 = token.substring(0, 1);
		}
		else if (token.length() == 1) {
		    chainName2 = "A"; // empty name
		}
		else {
		    log.warning("Weird line, no second chain name extracted: " + line);
		    return null;
		}
	    }
 	    if (count == 2) {
 		res1 = Integer.parseInt(token); //  - 1; // -1 : internal counting 
 	    }
 	    if (count == 4) {
 		res2 = Integer.parseInt(token); // - 1; 
 	    }
	    ++count;
	}
	if (! (sameStrandNames(chainName1,strand.getProperty(PDB_CHAIN_CHAR))
	       && sameStrandNames(chainName2, strand2.getProperty(PDB_CHAIN_CHAR) ) ) ) {
	    log.fine("RNA strand names do not match:" + chainName1 + ":" + strand.getProperty(PDB_CHAIN_CHAR)
		     + "|" + chainName2 + ":" + strand2.getProperty(PDB_CHAIN_CHAR) + ":line: " + line);
	    // assert false; // should not happen!
	    return null;
	}
	if (isAllowedClass(intClass)) { //  && (res1 <= strand.getResidueCount()) && (res2 <= strand2.getResidueCount())) {
	    // new watson crick pair found
	    Residue3D residue1 = strand.getResidueByPdbId(res1); // AssignedNumber(res1); // getResidue3D(res1); // 
	    Residue3D residue2 = strand2.getResidueByPdbId(res2); // AssignedNumber(res2); // getResidue3D(res2); // strand2.
	    if (residue1 == null) {
		String debugResult = "";
		// log.info("Could not find residue number in strand1 " + res1);
		for (int i = 0; i < strand.getResidueCount(); ++i) {
		    debugResult = debugResult + " " + strand.getResidue(i).getProperty(PDB_RESIDUE_ID);
		    // getAssignedNumber();
		}
		log.info("Could not find residue with pdb number: " + res1 + " in: " + debugResult);
		return null;
	    }
	    if (residue2 == null) {
		String debugResult = "";
		// log.info("Could not find residue number in strand2 " + res2);
		for (int i = 0; i < strand2.getResidueCount(); ++i) {
		    debugResult = debugResult + " " + strand2.getResidue(i).getProperty(PDB_RESIDUE_ID); // getAssignedNumber();
		}
		log.info("Could not find residue with pdb number: " + res2 + " in: " + debugResult);
		return null;
	    }
 	    log.fine("Generating link between residues " + (residue1.getFullName() + " " + residue1.getPos()+1) + " ( " + res1 + " ) and " 
 		     + residue2.getFullName() + " " + (residue2.getPos() + 1) + " ( " + res2 + " ) ");
	    InteractionType interactionType = new RnaInteractionType(RnaInteractionType.WATSON_CRICK);
	    Interaction interaction = new SimpleInteraction(residue1, residue2, interactionType);
	    return new InteractionLinkImp(residue1, residue2, interaction); // TODO add Watson Crick type info
	}
	log.fine("parseRnaviewLine failed for line: " + line);
	return null;
    }

    /** returns true if hydrogen bond class is allowed */
    boolean isAllowedClass (String intClass) {
	if ((intClass == null) || (intClass.length() == 0)) {
	    return false;
	}
	switch (allowedMode) {
	case CANONICAL_BP: return intClass.equals("XX") || intClass.equals("XIX") || intClass.equals("XXVIII");
	case WATSON_CRICK_BP:  return intClass.equals("XX") || intClass.equals("XIX");
	case NON_CANONICAL_BP: return intClass.charAt(0) == 'X';
	case NOT_STACKED: return !(intClass.equals("stacked") 
				   || intClass.equals("!(s_s)") || intClass.equals("n/a"));
	case ALL_BP: return true; // allow all base pairs found by RNAVIEW
	default: assert false; // unknown allowedMode
	}
	return false;
    }

    /** extracts one-letter PDB chain name from strand, and returns it concatenated and sorted in one string */
    String getRnaviewStrandPairing(NucleotideStrand strand1, NucleotideStrand strand2) {
	String chainName1 = strand1.getProperty(PDB_CHAIN_CHAR);
	String chainName2 = strand2.getProperty(PDB_CHAIN_CHAR);
	if ((chainName1.length() > 1) && (chainName2.charAt(0) == '_')) {
	    chainName1 = chainName1.substring(1); // skip leading underscore
	}
	if ((chainName2.length() > 1) && (chainName2.charAt(0) == '_')) {
	    chainName2 = chainName2.substring(1); // skip leading underscore
	}
	log.fine("Using chain names for strand pairings: " 
		 + strand1.getFullName() + " "
		 + chainName1 
		 + strand2.getFullName() + " "
		 + " " + chainName2);
	if ((chainName1 == null) || (chainName2 == null) || (chainName1.length() != 1)
	    || (chainName2.length() != 1)) {
	    return null;
	}
	char c1 = chainName1.charAt(0);
	char c2 = chainName2.charAt(0);
	if (c1 == ' ') {
	    c1 = 'A';
	}
	if (c2 == ' ') {
	    c2 = 'A'; // convert space to A
	}
	String strandPairing = "";
	if (c1  <= c2 ) {
	    strandPairing = strandPairing + c1 + c2;
	}
	else {
	    strandPairing = strandPairing + c2 + c1;
	}
	return strandPairing;
    }

    /** Returns true if REMARK lines contain RNAview info */
    private boolean containsRnaviewInfo(String[] lines) {
	if ((lines == null) || (lines.length == 0)) {
	    return false;
	}
	boolean found1 = false;
	boolean found2 = false;
	for (String s : lines) {
	    if (s.length() < 10) {
		continue;
	    }
	    if (!s.startsWith("REMARK")) {
		continue;
	    }
	    String line = s.substring(8);
	    if (line.startsWith("BEGIN_base-pair")) {
		found1 = true;
	    }
	    else if (found1 && line.startsWith("END_base-pair")) {
		found2 = true;
		break;
	    }
	}
	return found1 && found2; // both keyword lines must be found
    }

    /** Returns true iff string contatins RNAview generated info */
    private boolean containsRnaviewInfo(String remarks) {
	if ((remarks == null) || (remarks.length() == 0)) {
	    return false;
	}
	return containsRnaviewInfo(remarks.split("\\n")); // split to lines. No Windows compatible
    }

    /** Returns true iff property file contatins RNAview generated info */
    private boolean containsRnaviewInfo(Properties properties) {
	if (properties == null) {
	    return false;
	}
	return containsRnaviewInfo(properties.getProperty(PDB_REMARKS));
    }

    /** Copies input to output stream. From: http://www.rgagnon.com/javadetails/java-0064.html */
    public static void fileCopy(InputStream is, OutputStream os) throws IOException {
	try {
	    byte[] buf = new byte[1024];
	    int i = 0;
	    while ((i = is.read(buf)) != -1) {
		os.write(buf, 0, i);
	    }
	} 
	catch (IOException e) {
	    throw e;
	}
	finally {
	    if (is != null) is.close();
	    if (os != null) os.close();
	}
    }

    /** Returns number of RNA strands */
    int countStrands(Object3D root) {
	Object3DSet strands = Object3DTools.collectByClassName(root, "RnaStrand");
	return strands.size();
    }

    /** Returns if all strand names are simple and without duplicates */
    boolean checkStrandNamesSimple(Object3D root) {
	Set<String> strandNames = new HashSet<String>();
	Object3DSet strands = Object3DTools.collectByClassName(root, "RnaStrand");
	for (int i = 0; i < strands.size(); ++i) {
	    String name = strands.get(i).getName();
	    assert name != null;
	    if (name.length() != 1) {
		return false; // must be one letter, use "_" for non-named strands
	    }
	    strandNames.add(name);
	}
	// if no duplicates, the sizes of hashset and list will be the same:
	return strandNames.size() == strands.size();
    }
    
    /** reads object and links. */
    public Object3DLinkSetBundle readBundle(InputStream isOrig) throws Object3DIOException {
	log.info("starting RnaPdbRnaviewReader.readBundle!");
	String fileName = null;
	File tmpFile = null; // only use for adding RNAview info
	InputStream is = isOrig;
	Object3D obj = readAnyObject3D(is);
	LinkSet links = new SimpleLinkSet();
	Properties prop = obj.getProperties();
	String[] remarkLines = new String[0];
	if (debugMode) {
	    Object3DSet strandSet = Object3DTools.collectByClassName(obj, "RnaStrand");
	    System.out.println("Initially read strands:");
	    for (int i = 0; i < strandSet.size(); ++i) {
		System.out.println(strandSet.get(i).getFullName());
	    }
	}
	if (addRnaviewMode && (!containsRnaviewInfo(obj.getProperties()))) {
	    if (debugMode) {
		log.info("Adding RNAview info...");
	    }
	    int strandCount = countStrands(obj);
		if (strandCount > RNAVIEW_STRAND_MAX) {
		    throw new Object3DIOException("Cannot add RNAview info, because more than " 
						  + RNAVIEW_STRAND_MAX
						  + " strands detected: " + strandCount);
		}
		try {
		    // workaround, because of scripting for RNAview we need a filename 
		    tmpFile = File.createTempFile("nanotiler", ".pdb");
		    fileName = tmpFile.getAbsolutePath();	    
		    if (!debugMode) {
			tmpFile.deleteOnExit();
		    }
		    FileOutputStream fos = new FileOutputStream(tmpFile);
		    boolean simpleStrands = checkStrandNamesSimple(obj);
		    GeneralPdbWriter writer = new GeneralPdbWriter();
		    if (!simpleStrands) { // use renumbering of strand names in case of duplicate or non-trivial strand names
			log.warning("Duplicate or long strand names detected! Re-reading using renamed strands...");
			writer.setOriginalMode(GeneralPdbWriter.RESIDUE_RECOUNTED_NUMBER);
		    }
		    else {
			log.fine("All strand names are simple, no renaming necessary.");
		    }
		    log.fine("Writing structure in PDB format to temporary file: " + fileName);
		    writer.write(fos, obj); // write to file again!
		    fos.close();
		    // check if non-trivial or duplicate strand names with more than one letter 
		    assert fileName != null;
		    String mode = "n"; // mode "n" means that the PDB content is not repeated
		    // 		if (!simpleStrands) { // renumbering mode not necessary anymore, already taken case of by GeneralPdbWriter
		    // 		    mode = mode + "r"; # also perform renumbering of strands
		    // 		}
		    String command = RNAVIEW_BIN + " " + fileName + " " + mode;
		    log.fine("Launching command: " + command);
		    List<String> linesList = SimpleQueueManager.shell(command);
		    assert linesList != null;
		    remarkLines = linesList.toArray(remarkLines);
		    String remarkLinesString = StringTools.convertArrayToString(remarkLines);
		    assert remarkLines.length == linesList.size();
		    is = new FileInputStream(tmpFile);
		    obj = readAnyObject3D(is);  // read again, this time from temporary file
		    obj.setProperty(PDB_REMARKS, remarkLinesString);
		    is.close();
		    if (!checkStrandNamesSimple(obj)) {
			// this time, the strand names should be simple
			log.severe("Could not generate simple strand names even! Maybe more than 52 strands? RNAview base pair info might not be correctly assigned!");
		    }
		    if (debugMode) {
			log.info("Added base pair info by launching RNAview!");
			for (String s : remarkLines) {
			    System.out.println(s);
			}
		    }
		}
		catch (IOException ioe) {
		    throw new Object3DIOException("Error in readBundle, trying to supplement RNAview info: " + ioe.getMessage());
		}		
	}
	else {
	    if (prop != null) {
		String remarks = prop.getProperty(PDB_REMARKS); // find rnaview links in "pdb_remarks" property
		if (remarks != null) {
		    remarkLines = remarks.split("\\n"); // use newline as reg exp splitter // TODO: should be NEWLINE?
		}
	    }
	}
	if (addRnaviewMode && containsRnaviewInfo(remarkLines)) {
	    if (debugMode) {
		log.info("Using RNAview info...");
	    }
		Set<String> rnaviewStrandPairings = parseRnaviewStrandPairings(remarkLines);
		// assert rnaviewStrandPairings.size() > 0;
		// log.info("Rnaview strand pairings: ");
		// 		Iterator<String> iter = rnaviewStrandPairings.iterator();
		// 		while (iter.hasNext()) {
		// 		    log.info(iter.next());
		// 		}
		Object3DSet strandSet = Object3DTools.collectByClassName(obj, "RnaStrand");
		int[] sizeOffsets = Object3DSetTools.generateSizeOffsets(strandSet);
		if (verboseLevel > 0) {
		    log.finest("Parsing info about " + strandSet.size() + " strands.");
		}
		for (int i = 0; i < strandSet.size(); ++i) {
		    NucleotideStrand strand = (NucleotideStrand)(strandSet.get(i));
		    log.fine("Parsing RNAVIEW info for strand " + strand.getName() + " with "
			     + strand.getResidueCount() + " residues.");
		    if (strand.size() == 0) {
			throw new Object3DIOException("Strand " + strand.getFullName() + " has zero size.");
		    }
		    assert strand.size() > 0;
		    assert strand.getResidueCount() > 0;
// 		    LinkSet rnaviewLinks = parseRnaviewLinks(remarks, strand, strand); // parse text with respect to strand
// 		    links.merge(rnaviewLinks);
		    for (int j = 0; j < strandSet.size(); ++j) {
			NucleotideStrand strand2 = (NucleotideStrand)(strandSet.get(j));
			log.fine("Parsing RNAVIEW info for strand " + strand.getName() + " with "
				   + strand.getResidueCount() + " residues and "
				   + strand2.getName() + " with " + strand2.getResidueCount() + " residues.");
			if (strand2.size() == 0) {
			    throw new Object3DIOException("Strand2 " + strand2.getFullName() + " has zero size.");
			}
			assert strand2.size() > 0;
			assert strand2.getResidueCount() > 0;
			log.finest("Checking between strands (1) " + strand.getName() + " " + strand2.getName());
			String strandPairing = getRnaviewStrandPairing(strand, strand2);
			if (strandPairing == null) {
			    log.warning("Could not extract PDB - Rnaview strand pairing for " + strand.getFullName() 
					+ " and " + strand2.getFullName());
			    continue;
			}
			if (! rnaviewStrandPairings.contains(strandPairing)) {
			    log.fine("Ignoring non-existent strand pairing: " + strandPairing);
			    continue;
			}
			else {
			    // log.info("Working on existent strand pairing: " + strandPairing);
			}
			LinkSet rnaviewLinks = parseRnaviewLinks(remarkLines, strand, strand2,
								 sizeOffsets[i], sizeOffsets[i+1],
								 sizeOffsets[j], sizeOffsets[j+1]); // parse text with respect to strand pair
			log.fine("Found " + rnaviewLinks.size() + " links between strands "
				 + (i+1) + " " + strand.getName() + " " + strand.getProperty(PDB_CHAIN_CHAR) + " and "
				 + (j+1) + " " + strand2.getName() + " " + strand2.getProperty(PDB_CHAIN_CHAR));
			links.merge(rnaviewLinks);
			rnaviewLinks.clear();
			// try reverse order
// 			log.finest("Checking between strands (2) " + strand2.getName() + " " + strand.getName());
// 			rnaviewLinks = parseRnaviewLinks(remarks, strand2, strand); 
// 			links.merge(rnaviewLinks);
		    }
		}
	}
	else {
	    log.info("Internal analysis of atomic structure in order to detect base pairs...");
	    Object3DSet nucleotides = Object3DTools.collectByClassName(obj, "Nucleotide3D");
	    BasePairClassifier bpFinder = new LWBasePairClassifier();
	    try {
		LinkSet basePairs = bpFinder.classifyBasePairs(nucleotides);
		log.info("Detected " + basePairs.size() + " base pairs.");
		if (verboseLevel > 0) {
		    System.out.println(BasePairTools.toString(basePairs));
		}
		links.merge(basePairs);
	    } catch (RnaModelException rme) {
		log.warning("Error while detecting base pairs in structure. No base pairs are being detected. Error: " + rme.getMessage());
	    }
	}
	if (addRnaviewMode && (tmpFile != null) && (!debugMode)) {
	    assert fileName != null;
	    if (debugMode) {
		log.info("Removing temporary file " + fileName);
	    }
	    tmpFile.delete();
	}
	// add backbone links:
	// addBackboneLinks(obj, links);
	if (debugMode) {
	    log.info("Adding covalent links...");
	}
	addCovalentLinks(obj, links);

	if (debugMode) {
	    log.info("Finished RnaPdbRnaviewReader.readBundle: PDB file with " + obj.size() + " strands read: Position: " + obj.getPosition());
	}
	return new SimpleObject3DLinkSetBundle(obj, links);
    }

    /* Central method that reads the body of the PDB file and creates strands, nucleotides and atoms 
     * @see rnamodel.Object3DGraphFactory#createObject3DGraph()
     */
    public Object3D readAnyObject3D(InputStream is) throws Object3DIOException {
	if (debugMode) {
	    log.info("Starting RnaPdbRnaviewReader.readAnyObject3D! Renumber-mode: " + residueRenumberMode);
	}
	//	Atom3DSet atom3dSet = new Atom3DSet();
	DataInputStream dis = new DataInputStream(is);
	Object3D root = new SimpleObject3D();
	Object3DSet atomSet = new SimpleObject3DSet();
	root.setName("PdbImport");
	String currentStrandName = "_";
	int currentResidueId = -9999;
	int currentPdbResidueId = -9999;
	if (residueRenumberMode) {
	    currentResidueId = 0;
	}
	int currentAtomId = -9999;
	int lineCounter = 0;
	NucleotideStrand currentStrand = null;
	Nucleotide3D currentResidue = null;
	int strandCounter = 0;
	String line;
	boolean strandEndFlag = false;
	StringBuffer remarkBuf = new StringBuffer();
	char tabooResidueVersionChar = ' ';
	String strandNameMapFrom = "";
	String strandNameMapTo = "";
	String strandNameOrig = "undefined";
	String strandNameOrigOld = "undefined2";
	int mappedId = -1; // workaround for dealing with backwards jumps in residue indices
	Set<String> knownErrors = new HashSet<String>(); // makes sure each error is shown only once
	try {
	    do {
		line = dis.readLine(); // Read new atom.
		++lineCounter;
		if (debugMode) {
		    log.fine("Read line " + lineCounter + ": " + line);
		}
		if (line == null) {
		    if (debugMode) {
			log.info("Quitting loop because end of file reached! Line number: " + lineCounter);
		    }
		    break;
		}
		if (isTer(line) || isEndMdl(line)) { // Check for TER or ENDMDL keyword.
		    if (debugMode) {
			log.fine("TER or ENDMDL keyword found!");
		    }
		    strandEndFlag = true;
		    continue;
		}
		if (isRemark(line)) {
		    remarkBuf.append(line + NEWLINE);
		}
		if (isAtom(line)) {
		    if (debugMode) {
			log.fine("Recognized atom in current line!");
		    }
		    int residueId = getResidueId(line);
		    int pdbResidueId = getResidueId(line);
		    if (residueRenumberMode) {
			if (pdbResidueId != currentPdbResidueId) {
			    residueId = currentResidueId + 1;
			    log.fine("Renumber mode: setting new current residue to " 
				     + residueId + " instead of " + pdbResidueId);
			}
			else {
			    residueId = currentResidueId;
			}
		    }
		    String residueName = getResidueName(line);
// 		    if (!isLegalResidueName(residueName)) {
// 			continue; // skip strange residues like for example amino acids
// 		    }
		    String atomName = getAtomName(line);
		    String strandName = "" + getStrandName(line);
		    log.fine("Currently reading strand name: " + strandName);
// 		    if ((strandName.length() == 1) && Character.isDigit(strandName.charAt(0))) {
// 			strandName = "_" + strandName; // convert "2" to "_2";
// 		    }
		    strandNameOrigOld = strandNameOrig;
		    strandNameOrig = new String(strandName);
		    if (strandNameMapFrom.equals(strandName) && (strandNameMapTo.length() > 0)) {
			strandName = strandNameMapTo;
		    }
		    char versionChar = getVersionChar(line);
		    char residueVersionChar = getResidueVersionChar(line);
		    if ((tabooResidueVersionChar != ' ') && (residueVersionChar == tabooResidueVersionChar)) {
			log.warning("Warning: ignoring alternative residue version: " + line);
		    }
// 		    if ((versionChar != ' ') && (versionChar != 'A')) {
// 			log.info("Warning: ignoring alternative coordinate version: " + line);
// 			continue;
// 		    }
		    Atom3D atom = generateAtomFromPdb(line);
		    if (debugMode) {
			log.fine("Read atom: " + atom.getName() + " with position: " + atom.getPosition());
		    }
		    if (atomSet.size() > 0) {
			double minDist = findMinimumDistance(atom, atomSet);
			if (minDist < COLLISION_DISTANCE) {
			    log.info("Warning: colliding atom " + line);
			    // continue; // Ignore this atom.
			}
		    }
		    // start a new strand if
		    // 1. residue must be nucleotide
		    // 2. either: no strand exists
		    //     or TER 
		    //     or change of strand name
		    //     or residue id is suddenly smaller than before
		    if (!isLegalResidueName(residueName)) {
			String combined = residueName + "_" + residueId;
			if (!knownErrors.contains(combined)) {
			    log.info("Not legal RNA residue name: " + residueName + " " + residueId);
			    knownErrors.add(combined);
			}
			continue;
		    }
		    if (((residueId < currentResidueId) && (strandName.equals(currentStrandName)) && allowBackwardJump)) {
			if (mappedId < 0) {
			    mappedId = residueId;
			    residueId = currentResidueId + 1;
			    // log.info("Backward jump detected (1)! Adjusting residue names: " + residueId + " " + residueName);
			}
			else if (residueId != mappedId) { // new residue
			    mappedId = residueId;
			    residueId = currentResidueId + 1;
			    // log.info("Backward jump detected (2)! Adjusting residue names: " + residueId + " " + residueName);
			}
			else {
			    residueId = currentResidueId; // mapped id not changed
			    // log.info("Backward jump detected (3)! Adjusting residue names: " + residueId + " " + residueName);
			}
			residueName = "" + residueName.charAt(0) + residueId;
			    log.info("Backward jump detected! Adjusting residue names: " + residueId + " " + residueName 
				     + " strand name (orig): " + strandNameOrig + " current strand name: " + currentStrandName);

		    }
		    else {
			mappedId = -1; // no mapping
		    }
  		    if (isLegalResidueName(residueName) && (currentStrand == null || strandEndFlag  || ((!strandName.equals(" "))
  					&& (!strandName.equals(currentStrandName) ) ) || ((residueId < currentResidueId) && (!allowBackwardJump)) ) ) {
//  		    if (isLegalResidueName(residueName) 
// 			&& (currentStrand == null || strandEndFlag  || (!strandNameOrig.equals(strandNameOrigOld))
// 			    || ((residueId < currentResidueId) && (!allowBackwardJump) ) ) ) {
			if (debugMode) {
			    if (residueId < currentResidueId) {
				log.info("Detected backwards jump in residue ids! Starting new chain: " 
					 +  residueId + " " + currentResidueId);
			    }
			    if (currentStrand == null) {
				log.info("Current strand is null");
			    }
			    log.fine("Strand end flag, strand name, current strand name: " + strandEndFlag + " : " + strandName + " : " + currentStrandName
				     + " : " + residueId + " : " + currentResidueId + " : " + allowBackwardJump);
			}
			mappedId = -1; // resetting backwards jump workaround
			String tmp = "";
			if (currentStrand == null) {
			    tmp = "null";
			}
			else {
			    tmp = currentStrand.getName();
			}
			log.fine("Starting a new strand for line: " + line);

			log.fine(":" + tmp + ":" + strandEndFlag + ":" + strandName + ":" + currentStrandName 
				 + ":" + residueId + ":" + currentResidueId);
			// Generate new strand:
			if (strandName.equals(" ")) {
			    strandName = "" + getDefaultStrandName(strandCounter);
			}
			boolean renameOccured = false;
			while ((root.getIndexOfChild(strandName) >= 0)) {
			    renameOccured = true;
			    strandName = getHigherOrderStrandName(strandName);// convert B to B2; B2 to B3, C5 to C6 etc

			    strandNameMapTo = strandName;
			    strandNameMapFrom = strandNameOrig;
			}
			if (renameOccured) {
			    log.info("Strand with name " + strandNameOrig + " already existed: " + Object3DTools.generateChildNames(root) + " renaming to strand: " + strandName);
			}
			strandEndFlag = false; // Reset strand end flag.
			++strandCounter;
			if (dnaMode) {
			    currentStrand = new SimpleRnaStrand(DnaTools.DNA_ALPHABET);
			}
			else {
			    currentStrand = new SimpleRnaStrand(DnaTools.RNA_ALPHABET);
			}
			currentStrand.setName(strandName);
			currentStrand.setProperty(PDB_CHAIN_CHAR, "" + strandNameOrig.substring(strandNameOrig.length()-1));
			currentStrandName = new String(strandName);
			// currentStrand.setPosition(atom.getPosition());
			if (root.getIndexOfChild(currentStrand.getName()) >= 0) {
			    throw new Object3DIOException("Strand with this name already exists: " + currentStrand.getName());
			}
			else {
			    if (root.size() > 0) {
				RnaStrand lastStrand = (RnaStrand)(root.getChild(root.size()-1));
				if (lastStrand.getResidueCount() == 0) {
				    throw new Object3DIOException("Last strand had zero residues: " + lastStrand.getFullName());
				}
			    }
			    root.insertChild(currentStrand);
			    log.fine("Adding strand with name: " + currentStrand.getFullName());
			}
		    }
		    if (currentStrand == null) {
			log.warning("Strand not generated yet! Ignoring atom!");
			continue; // no open strand yet
		    }
		    if ((currentResidue == null) || (currentResidueId != residueId) || (currentStrand.getResidueCount() == 0)) {
			// Generate new nucleotide: position is equal to position of first atom.
			// make sure there is more than one atom in previous nucleotide
			if ((currentStrand.size() > 0) && (currentStrand.getResidue3D(currentStrand.getResidueCount()-1).size() < 2)) {
			    Residue3D residue = currentStrand.getResidue3D(currentStrand.getResidueCount()-1);
			    System.err.println("Problem in line: " + line);
			    System.err.println("Weird nucleotide: " + residue.getFullName() + " " + residue);
			    System.err.println("Current and actual residue ids: " + currentResidueId + " " + residueId);
			    if (currentResidue == null) {
				System.err.println("Current residue is null!");
			    }
			}
			assert currentStrand.size() == 0 || currentStrand.size() == 1 
			    || currentStrand.getResidue3D(currentStrand.getResidueCount()-1).size() > 1;
			try {
			    assert getResidueCharacter("U9") == 'U';
			    char c = getResidueCharacter(residueName);
			    assert c != 'B'; // TODO: just for debugging
			    if (c == 'X') {
				log.severe("Could not parse residueName " + residueName);
			    }
			    // assert c != 'X';
			    LetterSymbol symbol;
			    if (dnaMode) {
				symbol = new SimpleLetterSymbol(c, DnaTools.DNA_ALPHABET);
			    }
			    else {
				symbol = new SimpleLetterSymbol(c, DnaTools.RNA_ALPHABET);
			    }
			    assert symbol.getCharacter() != 'B';
			    currentResidue = new Nucleotide3D(symbol,
							      // currentStrand.getSequence(), 
							      // currentStrand.getResidueCount(),
							      atom.getPosition());
			    // log.info("Setting assigned number to " + residueId + " using line: " + line);
			    currentResidue.setAssignedNumber(residueId);
			    currentResidue.setAssignedName(residueName);
			    currentResidue.setProperty(PDB_RESIDUE_ID, "" + pdbResidueId);
			    String resName = "" + c + residueId;
			    currentResidue.setName(resName);
			    // log.info("Setting assigned number of " + resName + " to " + residueId);
			    if (currentStrand.getIndexOfChild(currentResidue.getName()) >= 0) {
				log.info("Ignoring duplicate residue: " + currentResidue.getName() + residueVersionChar
					 + " " + line);
				tabooResidueVersionChar = residueVersionChar;
				continue;
			    }
			    else {
				tabooResidueVersionChar = ' '; // no taboo residue
			    }
			    currentStrand.insertChild(currentResidue);
			    currentResidueId = residueId;
			    currentPdbResidueId = pdbResidueId;
			    log.fine("Added nucleotide: " + currentResidue.getFullName() + " " + (currentResidue.getPos() + 1)
				     + " " + currentResidue.getAssignedNumber() + " " + currentResidue.getProperty(PDB_RESIDUE_ID));
			}
			catch (UnknownSymbolException e) {
			    log.info("Warning: could not interpret nucleotide symbol from line: " + line);
			    // do nothing otherwise
			}
		    }
		    assert line != null;
		    if (atom == null) {
			log.warning("No atom defined for line: " + line);
			continue;
		    }
		    if (currentResidue == null) {
			log.warning("No current residue defined for line: " + line);
			continue;
		    }
		    if (currentResidue.getIndexOfChild(atom.getName()) >= 0) {
			log.warning("Ignoring duplicate atom: " + atom.getName() + " for residue " + currentResidue.getFullName());
			continue;
		    }
		    if (debugMode) {
			log.fine("Inserting atom into strand " +
				 strandCounter + " " +
				 currentStrandName + " residue "
				 + currentResidue.getFullName() + " " +
				 currentResidue.getPos() + " : " + atom);
		    }
		    currentResidue.insertChild(atom);
		    //		    atom3dSet.addAtom(atom);
		    Vector3D newAtomPos = currentResidue.getChild(currentResidue.size() - 1).getPosition();
		    if (atom.getPosition().distance(newAtomPos) >= 0.001) {
			log.info("Error!!! root: " + root.getPosition() + " strand " + currentStrand.getPosition() + " residue " + currentResidue.getPosition()
					    + " atom: " + atom.getPosition() + " new atom pos: " + newAtomPos);
			assert (atom.getPosition().distance(newAtomPos) < 0.001);
		    }
		    // TODO : cannot handle 
		}
		else {
		    if (debugMode) {
			log.fine("Line is not ATOM : " + line);
		    }
		}
	    }
	    while (line != null);
	}
	catch (IOException ioe) {
	    if (ioe instanceof Object3DIOException) {
		throw (Object3DIOException)ioe;
	    } else {
		throw new Object3DIOException(ioe);
	    }
	}
	Object3DTools.balanceTree(root);
	//	StrandJunctionDBTools.atom3dSet.merge(atom3dSet);
	if (remarkBuf.length() > 0) {
	    Properties prop = root.getProperties();
	    if (prop == null) {
		prop = new Properties();
	    }
	    prop.setProperty(PDB_REMARKS, remarkBuf.toString());
	    if (debugMode) {
		log.info("Set object " + root.getName() + " remark text to: " + prop.getProperty("pdb_remarks"));
	    }
	    root.setProperties(prop);
	}
	if (debugMode) {
	    log.info("Finished RnaPdbRnaviewReader.readAnyObject3D. Number of read objects: " + (new SimpleObject3DSet(root)).size());
	}
	return root;
    }

    /** if true, renumbers residues to start from one in consecutive order while reading */
    public void setResidueRenumberMode(boolean flag) {
	this.residueRenumberMode = flag;
    }
    

}
