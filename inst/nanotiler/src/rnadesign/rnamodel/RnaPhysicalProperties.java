package rnadesign.rnamodel;

public class RnaPhysicalProperties {

    public static final double DEG2RAD =  Math.PI / 180.0;
    public static final double RAD2DEG = 180.0 / Math.PI;
    public static final double RNA_SINGLESTRAND_LENGTH_PER_RESIDUE = 5.0;
    public static final double RNA_DOUBLESTRAND_LENGTH_PER_RESIDUE = 3.0;
    public static final double RNA_SINGLESTRAND_WIDTH = 5.0;
    public static final double RNA_AVERAGE_BASE_RADIUS = 3.0;
    public static final double RNA_DOUBLESTRAND_WIDTH = 10.0;

    public static final double RNA_BP_DISTANCE = 3.5;
    public static final double RNA_BP_DISTANCE_TOLERANCE = 0.5;

    public static final double RNA_BP_ANGLE = 36.0 * DEG2RAD;
    public static final double RNA_BP_ANGLE_TOLERANCE = 5 * DEG2RAD;

    public static final double RNA_BP_BEND = 0.0;
    public static final double RNA_BP_BEND_TOLERANCE = 5.0 * DEG2RAD;

    public static double computeSingleStrandLength(int nResidues) {
	return nResidues * RNA_SINGLESTRAND_LENGTH_PER_RESIDUE;
    }

    public static double getSingleStrandWidth() {
	return RNA_SINGLESTRAND_WIDTH;
    }

    public static double computeDoubleStrandLength(int nResidues) {
	return nResidues * RNA_DOUBLESTRAND_LENGTH_PER_RESIDUE;
    }

    public static double getDoubleStrandWidth() {
	return RNA_DOUBLESTRAND_WIDTH;
    }

    public static double getAverageBaseRadius() {
	return RNA_AVERAGE_BASE_RADIUS;
    }

}
