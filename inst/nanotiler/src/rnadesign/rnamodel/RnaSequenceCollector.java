/**
 * 
 */
package rnadesign.rnamodel;

import java.util.ArrayList;
import java.util.List;

import sequence.Sequence;
import sequence.SequenceContainer;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DAction;

/** Collects all Rna sequences of one node in tree
 * @author Eckart Bindewald
 *
 */
public class RnaSequenceCollector implements Object3DAction, SequenceContainer {
	
    private List<Sequence> list;
	
    public RnaSequenceCollector() {
	list = new ArrayList<Sequence>();
    }
	
    public void act(Object3D obj) {
	if (obj instanceof SequenceObject3D) {
	    SequenceObject3D rna = (SequenceObject3D)obj;
	    for (int i = 0; i < rna.getSequenceCount(); ++i) {
		addSequence(rna.getSequence(i));
	    }
	}
    }

	/** gets n'th collected sequence */
    public void addSequence(Sequence s) {
	list.add(s);
    }
	
    public void clear() {
	list.clear();
    }
    
    /** gets n'th collected sequence */
	public Sequence getSequence(int n) {
	    if (n < getSequenceCount()) {
		return (Sequence)(list.get(n));
		}
		return null;
	}
	
	public Sequence getSequence(String name) {
		int id = getSequenceId(name);
		if (id >= 0) {
		   return getSequence(id);
		}
		return null;
    }
	
	 /** gets n'th collected sequence */	
	public Sequence get(int n) {
		return getSequence(n);
	}
	
	/** returns number of collected sequences so far */
	public int getSequenceCount() {
	    return list.size();
	}
	
	 /** returns index of sequence , -1 if not found; TODO: improve speed of implementation */
    public int getSequenceId(String name) {
	   for (int i = 0; i < list.size(); ++i) {
	   	if (list.get(i).getName().equals(name)) {
	   		return i;
	   	}
	   }
	   return -1;		
	}

	/** returns number of collected sequences so far */
	public int size() {
		return getSequenceCount();
	}
	
}
