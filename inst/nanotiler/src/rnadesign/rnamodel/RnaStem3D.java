package rnadesign.rnamodel;

import rnasecondary.Stem;
import tools3d.Vector3D;
import tools3d.objects3d.Object3D;

/** interface that represents 3D object that is an RNA stem.
 *    The idea is, that this class is only used as a supplement,
 *    to improve visualization and structure optimization.
 *    The basic information is stored in a set of RNA strands
 *    and a set of residue interactions (base pairs)
 */
public interface RnaStem3D extends Object3D {

    /** generates stem with identities of strand 1 and 2 reversed */
    public RnaStem3D generateReverseStem();

    public int getStrand1End5Index();

    public int getStrand1End3Index();

    public int getStrand2End5Index();

    public int getStrand2End3Index();

    public void setStrand1End5Index(int n);

    public void setStrand1End3Index(int n);

    public void setStrand2End5Index(int n);

    public void setStrand2End3Index(int n);

    /** average between strand1End5 and strand2End3 */
    public Vector3D getCenter1Position();

    /** average between strand2End5 and strand1End3 */
    public Vector3D getCenter2Position();

    public NucleotideStrand getStrand1();

    public NucleotideStrand getStrand2();

    /** returns length of stem (number of base pairs) */
    public int getLength();

    /** return stem info object */
    public Stem getStemInfo();

    public void setStemInfo(Stem stem);

    /** returns n'th residue of strand 1 of stem (counting wrt to stem, 0,.., n-1) */
    public Nucleotide3D getStartResidue(int n);

    /** returns n'th residue of strand 2 of stem (counting wrt to stem, 0, ..., n-1) */
    public Nucleotide3D getStopResidue(int n);

}
