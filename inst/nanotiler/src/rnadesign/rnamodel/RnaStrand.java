/** 
 * This interface describes the concept of an RNA strand.
 * An RNA strand is a Object3D with one and only one
 * sequence defined.
 */
package rnadesign.rnamodel;

/**
 * @author Eckart Bindewald
 * Extends BioPolymer. Mainly used to distinguish RNA, DNA and Proteins
 */
public interface RnaStrand extends NucleotideStrand {

}
