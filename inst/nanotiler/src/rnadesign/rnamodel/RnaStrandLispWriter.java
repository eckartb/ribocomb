/** writes scence graph
 */
package rnadesign.rnamodel;

import java.io.OutputStream;

import tools3d.objects3d.LinkSet;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DAbstractLispWriter;

/**
 * @author Eckart Bindewald
 *
 */
public class RnaStrandLispWriter extends Object3DAbstractLispWriter {
    
    public void write(OutputStream os, Object3D tree) { }

    public void write(OutputStream os, LinkSet links) { }
   
    /** write RnaStrand object to string */
    public String writeString(Object3D tree) {
	assert (tree instanceof RnaStrand);
	StringBuffer buf = new StringBuffer();
	RnaStrand strand = (RnaStrand)tree;
	buf.append(strand.toString());
	return buf.toString();
    }

	
}
