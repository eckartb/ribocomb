package rnadesign.rnamodel;

import generaltools.TestTools;
import sequence.LetterSymbol;
import tools3d.*;
import tools3d.objects3d.*;
import java.util.logging.*;
import java.io.PrintStream;
import java.util.*;
import java.io.FileInputStream;
import java.io.IOException;
import rnasecondary.*;
import sequence.Residue;
import sequence.LetterSymbol;
import sequence.SimpleLetterSymbol;
import sequence.UnknownSymbolException;
import sequence.DnaTools;

import rnadesign.rnacontrol.SimpleLinkController;
import rnadesign.rnacontrol.LinkController; // TODO : not clean to use controller in model
import rnadesign.rnacontrol.BasePairController;
import rnadesign.rnacontrol.SimpleBasePairController;

import org.testng.annotations.*;
import java.util.ResourceBundle;

import static rnadesign.rnamodel.PackageConstants.*;

public class RnaStrandTools {

    private static Logger log = Logger.getLogger(LOGGER_NAME);
    
    /** mutates strand residue n to specified residue */
    public static void mutateResidue(RnaStrand strand,
				     Nucleotide3D nucleotide,
				     int n,
				     double rmsLimit,
				     LinkSet links) throws FittingException, RnaModelException {
	int origSize = strand.getResidueCount();
	log.fine("Changing residue " + strand.getResidue(n).getSymbol() + " " + (n+1)
		 + " to " + nucleotide.getSymbol());
	Nucleotide3D newRes = (Nucleotide3D)(nucleotide.cloneDeep());
	Nucleotide3D oldRes = (Nucleotide3D)(strand.getResidue(n));
	newRes.setName(swapResidueNames(newRes.getName(), oldRes.getName()));
	Object3DSet oldTripod = NucleotideDBTools.extractAtomTripod(oldRes);
	Object3DSet newTripod = NucleotideDBTools.extractAtomTripod(newRes);
	Vector3D[] oldCoord = Object3DSetTools.getCoordinates(oldTripod);
	Vector3D[] newCoord = Object3DSetTools.getCoordinates(newTripod);
	// superpose:
	Superpose superposer = new KabschSuperpose(); // MCSuperpose();
	SuperpositionResult supResult = superposer.superpose(oldCoord, newCoord);
	if (supResult.getRms() > rmsLimit) {
	    throw new FittingException("Could not place mutated residue with RMS smaller than " 
					+ rmsLimit + " : " + supResult.getRms());
	}
	supResult.applyTransformation(newRes);	
	if (oldRes.getProperties() != null) {
	    newRes.setProperties((Properties)(oldRes.getProperties().clone()));
	}
	links.replaceObjectInLinks(strand.getResidue3D(n), newRes);
	strand.replaceChild(n, newRes); // central command: replaces nucleotide!
	assert newRes.getParent() instanceof BioPolymer;
	assert oldRes.getParent() == null;
	assert strand.getResidue(n).getSymbol().equals(newRes.getSymbol()); // successfully mutated!	
	assert strand.getResidueCount() == origSize; // number of residues has not changed
	NucleotideTools.addCovalentAtomLinks(newRes,links);
	log.info("Changed residue " + oldRes.getSymbol() + " " + (n+1)
		 + " to " + strand.getResidue(n).getSymbol());
    }

    /** mutates strand residue n to specified residue */
    public static void mutateResidue(RnaStrand strand,
				     LetterSymbol symbol,
				     int n,
				     Object3D nucleotideDB,
				     double rmsLimit,
				     LinkSet links) throws FittingException, RnaModelException {
	int dbId = NucleotideDBTools.findResidueIndex(nucleotideDB, symbol);
	if (dbId < 0) { // not found!
	    throw new RnaModelException("Could not find nucleotide in database: " + symbol);
	}
	Nucleotide3D dbNuc = (Nucleotide3D)(nucleotideDB.getChild(dbId));
	mutateResidue(strand, dbNuc, n, rmsLimit, links);
    }

    public static void mutateResidueIfNecessary(RnaStrand strand,
						LetterSymbol symbol,
						int n,
						Object3D nucleotideDB,
						double rmsLimit,
						LinkSet links) throws FittingException, RnaModelException {
	if (strand.getResidue(n).getSymbol().getCharacter() != symbol.getCharacter()) {
	    mutateResidue(strand, symbol, n, nucleotideDB, rmsLimit, links);
	}
    }

    private static String swapResidueNames(String newResidueOrigName, String oldResidueName) {
	return "" + newResidueOrigName.charAt(0) + oldResidueName.substring(1);
    }

    /** counts collisions between nucleotides ignoring backbone and sugar groups */
    private static int countBaseCollisions(Nucleotide3D res1,
					   Nucleotide3D res2,
					   double collDist) {
	int count = 0;
	for (int i = 0; i < res1.getAtomCount(); ++i) {
	    Atom3D atom1 = res1.getAtom(i);
	    if ((!AtomTools.isHydrogen(atom1)) && NucleotideTools.isBaseAtom(atom1)) {
		for (int j = 0; j < res2.getAtomCount(); ++j) {
		    Atom3D atom2 = res2.getAtom(j);
		    if ((!AtomTools.isHydrogen(atom2)) && NucleotideTools.isBaseAtom(atom2)) {
			if (atom1.distance(atom2) < collDist) {
			    ++count;
			}
		    }
		}
	    }
	}
	return count;
    }

    /**  returns collisions between base n and neighboring residues */
    private static int countBaseCollisions(Nucleotide3D newRes,
					   RnaStrand strand,
					   int n,
					   double collDist) {
	int result = 0;
	if (n > 0) {
	    result += countBaseCollisions(newRes, (Nucleotide3D)(strand.getResidue(n-1)), collDist);
	}
	if (n < (strand.getResidueCount()-1)) {
	    result += countBaseCollisions(newRes, (Nucleotide3D)(strand.getResidue(n+1)), collDist);
	}
	return result;
    }

    /** mutates strand residue n to specified residue */
    private static void mutateBasePair(RnaStrand strand1,
				       Nucleotide3D nucleotide1,
				       int n1,
				       RnaStrand strand2,
				       Nucleotide3D nucleotide2,
				       int n2,
				       double rmsLimit,
				       LinkSet links) throws FittingException, RnaModelException {
	int origSize = strand1.getResidueCount();
	log.fine("Changing residue " + strand1.getResidue(n1).getSymbol() + " " + (n1+1)
		 + " to " + nucleotide1.getSymbol() + " and " 
		 + strand2.getResidue(n2).getSymbol() + " " + (n2+1)
		 + " to " + nucleotide2.getSymbol() );
	Nucleotide3D newRes1 = (Nucleotide3D)(nucleotide1.cloneDeep());
	Nucleotide3D newRes2 = (Nucleotide3D)(nucleotide2.cloneDeep());
	Nucleotide3D oldRes1 = (Nucleotide3D)(strand1.getResidue(n1));
	Nucleotide3D oldRes2 = (Nucleotide3D)(strand2.getResidue(n2));
	newRes1.setName(swapResidueNames(newRes1.getName(), oldRes1.getName())); // G10 mutated using C5 becomes C10
	newRes2.setName(swapResidueNames(newRes2.getName(), oldRes2.getName()));
	Object3DSet oldTripod1 = NucleotideDBTools.extractAtomTripodBp(oldRes1);
	Object3DSet oldTripod2 = NucleotideDBTools.extractAtomTripodBp(oldRes2);
	Object3DSet newTripod1 = NucleotideDBTools.extractAtomTripodBp(newRes1);
	Object3DSet newTripod2 = NucleotideDBTools.extractAtomTripodBp(newRes2);
	Vector3D[] oldCoord1 = Object3DSetTools.getCoordinates(oldTripod1);
	Vector3D[] newCoord1 = Object3DSetTools.getCoordinates(newTripod1);
	Vector3D[] oldCoord2 = Object3DSetTools.getCoordinates(oldTripod2);
	Vector3D[] newCoord2 = Object3DSetTools.getCoordinates(newTripod2);
	Vector3D[] oldCoord = new Vector3D[oldCoord1.length + oldCoord2.length];
	Vector3D[] newCoord = new Vector3D[newCoord1.length + newCoord2.length];
	for (int i = 0; i < newCoord1.length; ++i) {
	    newCoord[i] = newCoord1[i];
	    oldCoord[i] = oldCoord1[i];
	}
	for (int i = 0; i < newCoord2.length; ++i) {
	    newCoord[i+newCoord1.length] = newCoord2[i];
	    oldCoord[i+oldCoord1.length] = oldCoord2[i];
	}
	// superpose:
	Superpose superposer = new KabschSuperpose(); // MCSuperpose();
	SuperpositionResult supResult = superposer.superpose(oldCoord, newCoord);
	if (supResult.getRms() > rmsLimit) {
	    throw new FittingException("Could not place mutated residue with RMS smaller than " 
					+ rmsLimit + " : " + supResult.getRms());
	}
	supResult.applyTransformation(newRes1);	
	supResult.applyTransformation(newRes2);	
	// count collisions between bases, ignoring sugar groups and backbone collisions because they are less likely
	int countColl = countBaseCollisions(newRes1, strand1, n1, AtomTools.defaultCollisionDistance)
	              + countBaseCollisions(newRes2, strand2, n2, AtomTools.defaultCollisionDistance);
	if (countColl > 0) {
	    System.out.println("Found " + countColl + " collisions for new base pair!");
	    throw new FittingException("Could not place mutated residue because of base collisions!");
	}
	assert oldRes1 != null;
	assert oldRes2 != null;
	if (oldRes1.getProperties() != null) {
	    newRes1.setProperties((Properties)(oldRes1.getProperties().clone()));
	}
	if (oldRes2.getProperties() != null) {
	    newRes2.setProperties((Properties)(oldRes2.getProperties().clone()));
	}
	links.replaceObjectInLinks(strand1.getResidue3D(n1), newRes1);
	links.replaceObjectInLinks(strand2.getResidue3D(n2), newRes2);
	strand1.replaceChild(n1, newRes1); // central command: replaces nucleotide!
	strand2.replaceChild(n2, newRes2); // central command: replaces nucleotide!
	///////////////////////////////////
	//System.out.println("/////////////////Adding New Nucleotide Links////////////////////////");
	NucleotideTools.addCovalentAtomLinks(newRes1, links);
	NucleotideTools.addCovalentAtomLinks(newRes2, links);

	assert newRes1.getParent() instanceof BioPolymer;
	assert oldRes1.getParent() == null;
	assert strand1.getResidue(n1).getSymbol().equals(newRes1.getSymbol()); // successfully mutated!	
	assert strand2.getResidue(n2).getSymbol().equals(newRes2.getSymbol()); // successfully mutated!	

	assert strand1.getResidueCount() == origSize; // number of residues has not changed
    }

    /** Find transformation to superimporse strand1.getResidue(n1) to nucleotide1 and strand2.getResidue(n2) to nucleotide 2*/
    private static void superimposeBasePair(RnaStrand strand1,
				       Nucleotide3D nucleotide1,
				       int n1,
				       RnaStrand strand2,
				       Nucleotide3D nucleotide2,
				       int n2,
				       double rmsLimit,
				       LinkSet links) throws FittingException, RnaModelException {
	int origSize = strand1.getResidueCount();
	log.fine("Finding transformation to go from " + strand1.getResidue(n1).getSymbol() + " " + (n1+1)
		 + " to " + nucleotide1.getSymbol() + " and " 
		 + strand2.getResidue(n2).getSymbol() + " " + (n2+1)
		 + " to " + nucleotide2.getSymbol() );
	Nucleotide3D newRes1 = (Nucleotide3D)(nucleotide1.cloneDeep());
	Nucleotide3D newRes2 = (Nucleotide3D)(nucleotide2.cloneDeep());
	Nucleotide3D oldRes1 = (Nucleotide3D)(strand1.getResidue(n1));
	Nucleotide3D oldRes2 = (Nucleotide3D)(strand2.getResidue(n2));
	newRes1.setName(swapResidueNames(newRes1.getName(), oldRes1.getName())); // G10 mutated using C5 becomes C10
	newRes2.setName(swapResidueNames(newRes2.getName(), oldRes2.getName()));
	Object3DSet oldTripod1 = NucleotideDBTools.extractAtomTripodBp(oldRes1);
	Object3DSet oldTripod2 = NucleotideDBTools.extractAtomTripodBp(oldRes2);
	Object3DSet newTripod1 = NucleotideDBTools.extractAtomTripodBp(newRes1);
	Object3DSet newTripod2 = NucleotideDBTools.extractAtomTripodBp(newRes2);
	Vector3D[] oldCoord1 = Object3DSetTools.getCoordinates(oldTripod1);
	Vector3D[] newCoord1 = Object3DSetTools.getCoordinates(newTripod1);
	Vector3D[] oldCoord2 = Object3DSetTools.getCoordinates(oldTripod2);
	Vector3D[] newCoord2 = Object3DSetTools.getCoordinates(newTripod2);
	Vector3D[] oldCoord = new Vector3D[oldCoord1.length + oldCoord2.length];
	Vector3D[] newCoord = new Vector3D[newCoord1.length + newCoord2.length];
	for (int i = 0; i < newCoord1.length; ++i) {
	    newCoord[i] = newCoord1[i];
	    oldCoord[i] = oldCoord1[i];
	}
	for (int i = 0; i < newCoord2.length; ++i) {
	    newCoord[i+newCoord1.length] = newCoord2[i];
	    oldCoord[i+oldCoord1.length] = oldCoord2[i];
	}
	// superpose:
	Superpose superposer = new KabschSuperpose(); // MCSuperpose();
	SuperpositionResult supResult = superposer.superpose(oldCoord, newCoord);
	if (supResult.getRms() > rmsLimit) {
	    throw new FittingException("Could not place mutated residue with RMS smaller than " 
					+ rmsLimit + " : " + supResult.getRms());
	}
	supResult.applyTransformation(newRes1);	
	supResult.applyTransformation(newRes2);	
	// count collisions between bases, ignoring sugar groups and backbone collisions because they are less likely
	int countColl = countBaseCollisions(newRes1, strand1, n1, AtomTools.defaultCollisionDistance)
	              + countBaseCollisions(newRes2, strand2, n2, AtomTools.defaultCollisionDistance);
	if (countColl > 0) {
	    System.out.println("Found " + countColl + " collisions for new base pair!");
	    throw new FittingException("Could not place mutated residue because of base collisions!");
	}
	assert oldRes1 != null;
	assert oldRes2 != null;
	if (oldRes1.getProperties() != null) {
	    newRes1.setProperties((Properties)(oldRes1.getProperties().clone()));
	}
	if (oldRes2.getProperties() != null) {
	    newRes2.setProperties((Properties)(oldRes2.getProperties().clone()));
	}
	links.replaceObjectInLinks(strand1.getResidue3D(n1), newRes1);
	links.replaceObjectInLinks(strand2.getResidue3D(n2), newRes2);
	strand1.replaceChild(n1, newRes1); // central command: replaces nucleotide!
	strand2.replaceChild(n2, newRes2); // central command: replaces nucleotide!
	///////////////////////////////////
	//System.out.println("/////////////////Adding New Nucleotide Links////////////////////////");
	NucleotideTools.addCovalentAtomLinks(newRes1, links);
	NucleotideTools.addCovalentAtomLinks(newRes2, links);

	assert newRes1.getParent() instanceof BioPolymer;
	assert oldRes1.getParent() == null;
	assert strand1.getResidue(n1).getSymbol().equals(newRes1.getSymbol()); // successfully mutated!	
	assert strand2.getResidue(n2).getSymbol().equals(newRes2.getSymbol()); // successfully mutated!	

	assert strand1.getResidueCount() == origSize; // number of residues has not changed
    }

    /** mutates base pair  to specified residue */
    public static void mutateBasePair(InteractionLink oldBasePairLink,
				      InteractionLink newBasePairLink,
				      double rmsLimit,
				      LinkSet links) throws RnaModelException, FittingException {
	Object3D objOld1 = oldBasePairLink.getObj1();
	Object3D objOld2 = oldBasePairLink.getObj2();
	Object3D objNew1 = newBasePairLink.getObj1();
	Object3D objNew2 = newBasePairLink.getObj2();
	if (! (objOld1 instanceof Nucleotide3D) ) {
	    throw new RnaModelException("Object " + objOld1.getName() + " is not a nucleotide object!");
	}
	if (! ( objOld2 instanceof Nucleotide3D) ) {
	    throw new RnaModelException("Object " + objOld2.getName() + " is not a nucleotide object!");
	}
	Nucleotide3D oldNuc1 = (Nucleotide3D)objOld1;
	Nucleotide3D oldNuc2 = (Nucleotide3D)objOld2;
	Nucleotide3D newNuc1 = (Nucleotide3D)objNew1;
	Nucleotide3D newNuc2 = (Nucleotide3D)objNew2;
	RnaStrand strand1 = null;
	RnaStrand strand2 = null;
	if (! (oldNuc1.getParent() instanceof RnaStrand) ) {
	    throw new RnaModelException("Nucleotide does not have RNA strand as parent object: " + oldNuc1.getName());
	}
	if (! (oldNuc2.getParent() instanceof RnaStrand) ) {
	    throw new RnaModelException("Nucleotide does not have RNA strand as parent object: " + oldNuc2.getName());
	}
	strand1 = (RnaStrand)(oldNuc1.getParent());
	strand2 = (RnaStrand)(oldNuc2.getParent());
	int n1 = oldNuc1.getPos();
	int n2 = oldNuc2.getPos();
// 	int dbId = NucleotideDBTools.findResidueIndex(nucleotideDB, symbol);
// 	if (dbId < 0) { // not found!
// 	    throw new RnaModelException("Could not find nucleotide in database: " + symbol);
// 	}
// 	Nucleotide3D dbNuc = (Nucleotide3D)(nucleotideDB.getChild(dbId));
	mutateBasePair(strand1, newNuc1, n1, strand2, newNuc2, n2, rmsLimit, links);
    }


    /** mutates base pair  to specified residue */
    public static void mutateBasePair(InteractionLink oldBasePairLink,
				      LetterSymbol symbol1,
				      LetterSymbol symbol2,
				      RnaInteractionType interactionType,
				      LinkSet basePairDB,
				      double rmsLimit,
				      LinkSet links) throws RnaModelException, FittingException {
	// find correct nucleotides:
	boolean interactionFound = false;
	for (int i = 0; i < basePairDB.size(); ++i) {
	    InteractionLink link = (InteractionLink)(basePairDB.get(i));
	    LetterSymbol s1 = ((Nucleotide3D)(link.getObj1())).getSymbol();
	    LetterSymbol s2 = ((Nucleotide3D)(link.getObj2())).getSymbol();
	    if ((s1.getCharacter() == symbol1.getCharacter())
		&& (s2.getCharacter() == symbol2.getCharacter())) {
		RnaInteractionType dbInteraction = (RnaInteractionType)(link.getInteraction().getInteractionType());
		if (dbInteraction.getSubTypeId() == interactionType.getSubTypeId()) {
		    interactionFound = true;
		    try {
			mutateBasePair(oldBasePairLink, link, rmsLimit, links);
			return; 		    // correct interaction found!
		    }
		    catch (FittingException fe) {
			// do not return, keep trying
		    }
		}
	    }
	}
	if (interactionFound) {
	    throw new FittingException("Could not find fitting " + interactionType + " " + symbol1 + " " + symbol2
					+ " in base pair database!");
	}
	else {
	    throw new RnaModelException("Could not find " + interactionType + " " + symbol1 + " " + symbol2
					+ " in base pair database!");
	}
    }    

    /** mutates base pair  to specified residue */
    public static void mutateBasePair(Interaction oldBasePairInteraction,
				      char c1, // new characters specififying to what to mutate into
				      char c2,
				      // RnaInteractionType interactionType, : assume Watson Crick interaction
				      LinkSet basePairDB,
				      double rmsLimit,
				      LinkSet links) throws RnaModelException, FittingException, UnknownSymbolException {
	// first find residues and link corresponding to interaction:
	Residue res1 = oldBasePairInteraction.getResidue1();
	Residue res2 = oldBasePairInteraction.getResidue2();
	LetterSymbol s1 = new SimpleLetterSymbol(c1, DnaTools.AMBIGUOUS_RNA_ALPHABET);
	LetterSymbol s2 = new SimpleLetterSymbol(c2, DnaTools.AMBIGUOUS_RNA_ALPHABET);
	// find link corresponding to interaction:
	Link oldBasePairLink = links.find((Object3D)res1, (Object3D)res2); // TODO : what if there are several links?
	if ((oldBasePairLink == null) || (! (oldBasePairLink instanceof InteractionLink))) {
	    throw new RnaModelException("Could not find base pair link for residues " + res1 + " " + res2);
	}
	RnaInteractionType interactionType = new RnaInteractionType(RnaInteractionType.WATSON_CRICK);
	mutateBasePair((InteractionLink)oldBasePairLink, s1, s2, interactionType, basePairDB, rmsLimit, links);

    }

    /** writes interaction set, assumes that it is watson-crick interaction */
    private void writeInteractions(PrintStream ps,
				   InteractionSet hbondInteractions) {
	for (int i = 0; i < hbondInteractions.size(); ++i) {
	    Interaction interaction = hbondInteractions.get(i);
	    Residue res1 = interaction.getResidue1();
	    Residue res2 = interaction.getResidue2();
	    System.out.println("Watson Crick interactions: " + (i+1) + " " 
			       + res1.getSymbol() + (res1.getPos()+1) + " : " 
			       + res2.getSymbol() + (res2.getPos()+1));
	}

    }

    /** tests mutating a base pair in an RNA structure. Checks for problems with structure like gaps in the backbone */
    @Test ( groups={"slow"} )
    public void testMutateBasePair() {
	log.info(TestTools.generateMethodHeader("testMutateBasePair"));
	final String filename1 = "../test/fixtures/1BGZ.rnaview.pdb";
	// final String basePairDBName = "../resources/157D_rnaview_edit.pdb";
	final String basePairDBName = "../resources/2J00_A_rnaview.pdb"; // complete 16S ribosome!
	// read structure:
	Object3DFactory reader = new RnaPdbRnaviewReader();
	Object3DLinkSetBundle bundle = null;
	final double gapCutoff = 1.9; // maximum bond distance
	try {
	    FileInputStream fis = new FileInputStream(filename1);
	    bundle = reader.readBundle(fis);
	}
	catch (IOException ioe) {
	    System.out.println("IO error: " + ioe.getMessage());
	    assert false;
	}
	assert bundle != null;
	// find strands:
	Object3DSet strandSet = Object3DTools.collectByClassName(bundle.getObject3D(), "RnaStrand");
	for (int i = 0; i < strandSet.size(); ++i) {
	    RnaStrand strand = (RnaStrand)(strandSet.get(i));
	    int numGaps = StructureQualityTools.countBackboneGaps(strand, gapCutoff);
	    System.out.println("Number of backbone gaps in strand: " + strand.getName() + " : " + numGaps);
	}
	int allGaps = StructureQualityTools.countAllBackboneGaps(bundle.getObject3D(), gapCutoff);
	System.out.println("Total number of backbone gaps: " + allGaps);
	// workaround to have toPrettyString available:
	LinkController linkController = new SimpleLinkController(bundle.getLinks());
	System.out.println("Links: " + linkController.toPrettyString());
	InteractionSet hbondInteractions = linkController.getHydrogenBondInteractions();
	writeInteractions(System.out, hbondInteractions);
	// read base pair controller:
	System.out.println("Reading base pair database: " + basePairDBName);
	BasePairController basePairDB = new SimpleBasePairController();
	try {
	    FileInputStream fis = new FileInputStream(basePairDBName);
	    basePairDB.read(fis);
	}
	catch(IOException ioe) {
	    System.out.println("IO error reading base pair database: " + ioe.getMessage());
	    assert false;
	}
	System.out.println("Number of base pairs in database: " + basePairDB.size());
	// mutate second base pair into A-U:
	double mutationRmsLimit = 0.28;
	double mutationRmsLimit2 = 0.35;
	int mutationMistakeCounter = 0;
	try {
	    System.out.println("mutating GC into AU:");
	    mutateBasePair(hbondInteractions.get(1), 'A', 'U', basePairDB, mutationRmsLimit, linkController);
	    int allGapsNew = StructureQualityTools.countAllBackboneGaps(bundle.getObject3D(), gapCutoff);
	    System.out.println("Total number of backbone gaps after performing mutation: " + allGapsNew);
	    System.out.println("mutating AU into CG:");
	    mutateBasePair(hbondInteractions.get(3), 'C', 'G', basePairDB, mutationRmsLimit, linkController);
	    allGapsNew = StructureQualityTools.countAllBackboneGaps(bundle.getObject3D(), gapCutoff);
	    System.out.println("Total number of backbone gaps after performing mutation: " + allGapsNew);
	    System.out.println("Hardest case: mutate very first base pair frmo GC to AU:");
	    mutateBasePair(hbondInteractions.get(0), 'U', 'A', basePairDB, mutationRmsLimit2, linkController);
	    allGapsNew = StructureQualityTools.countAllBackboneGaps(bundle.getObject3D(), gapCutoff);
	    System.out.println("Total number of backbone gaps after performing mutation: " + allGapsNew);
	}
	catch (RnaModelException rme) {
	    System.out.println("Rna model exception while mutating base pair: " + rme.getMessage());
	    assert false;
	}
	catch (FittingException fe) {
	    System.out.println("Fitting-exception while mutating base pair: " + fe.getMessage());
	    assert false;
	}
	catch (UnknownSymbolException use) {
	    System.out.println("Unknown symbol exception while mutating base pair: " + use.getMessage());
	    assert false;
	}
	// exporting structure:
	GeneralPdbWriter writer = new GeneralPdbWriter();
	System.out.println("Mutated structure:");
	writer.write(System.out, bundle.getObject3D());
	int allGapsNew = StructureQualityTools.countAllBackboneGaps(bundle.getObject3D(), gapCutoff);
	System.out.println("Total number of backbone gaps after performing mutation: " + allGapsNew);
	if (allGapsNew > allGaps) {
	    System.out.println("New gaps introduced due to mutation! Mutation test failed.");
	    mutationMistakeCounter += allGapsNew;
	}
	// make second mutation
	if (mutationMistakeCounter > 0) {
	    assert false;
	}
	// System.out.println("Finished testMutateBasePair");
	log.info(TestTools.generateMethodHeader("testMutateBasePair"));
    }

    /** If pure double-helix, all residues must be connected by a Watson-Crick pair */
    public static boolean isPureHelix(NucleotideStrand strand1, NucleotideStrand strand2,
				      LinkSet links) {
	assert strand1 != null && strand2 != null;
	if (strand1.getResidueCount() != strand2.getResidueCount()) {
	    return false;
	}
	int n = strand1.getResidueCount();
	for (int i = 0; i < strand1.getResidueCount(); ++i) {
	    assert (n - i -1) >= 0;
	    if (links.getLinkNumber(strand1.getResidue3D(i), strand2.getResidue3D(n-i-1)) == 0) {
		return false;
	    }
	}
	return true;
    }

    /** Finds RNA strands, that are purelely involved in a double-helix with another strand */
    public static Object3DSet findPureHelicalStrands(Object3D node, LinkSet links) {
	List<Object3D> strands = Object3DTools.collectByClassName(node, "RnaStrand").getAsList();
	Object3DSet result = new SimpleObject3DSet();
	for (int i = 0; i < strands.size(); ++i) {
	    for (int j = i+1; j < strands.size(); ++j) {
		if (RnaStrandTools.isPureHelix((NucleotideStrand)(strands.get(i)), (NucleotideStrand)(strands.get(j)), links)) {
		    result.add(strands.get(i));
		    result.add(strands.get(j));
		    break; // only one helix partner reasonable
		}
	    }
	}
	return result;
    }

    /** Returns true if this link represents a base pairing interaction */
    public static boolean isRnaBasePairLink(Link link) {
	if (link instanceof InteractionLink) {
	    Interaction interaction = ((InteractionLink)link).getInteraction();
	    InteractionType interactionType = interaction.getInteractionType();
	    if (interactionType instanceof RnaInteractionType) {
		if ((interactionType.getSubTypeId() != RnaInteractionType.BACKBONE)
		    && (interactionType.getSubTypeId() != RnaInteractionType.NO_INTERACTION)) {
		    log.info("Found base pair type: " + interactionType.getSubTypeId());
		    return true;
		}
	    }
	}
	return false;
    }


    /** Removes bases that are at the tails of the strand and that are not involved in base pairing */
    public static void trimUnpaired(NucleotideStrand strand,
			     LinkSet links) {
	OUTER:
	for (int i = strand.size()-1; i >= 0; --i) {
	    LinkSet group = links.findLinks(strand.getResidue3D(i));
	    boolean found = false;
	    for (int j = 0; j < group.size(); ++j) {
		if (isRnaBasePairLink(group.get(j))) {
		    found = true;
		    log.info("Found base pair interaction for residue: " + strand.getResidue3D(i).getFullName());
		    break OUTER; // quit
		}
	    }
	    if (!found) {
		log.info("Removing unpaired nucleotide " + strand.getChild(i).getFullName());
		strand.removeChild(i);
	    }
	}
	List<Integer> toBeRemoved = new ArrayList<Integer>();
	OUTER2:
	for (int i = 0; i < strand.getResidueCount(); ++i) {
	    LinkSet group = links.findLinks(strand.getResidue3D(i));
	    boolean found = false;
	    for (int j = 0; j < group.size(); ++j) {
		if (isRnaBasePairLink(group.get(j))) {
		    log.info("Found base pair interaction for residue: " + strand.getResidue3D(i).getFullName());
		    found = true;
		    break OUTER2; // quit
		}
	    }
	    if (!found) {
		log.info("Removing unpaired nucleotide " + strand.getChild(i).getFullName());
		toBeRemoved.add(i); // autoboxing
	    }
	}
	for (int i = toBeRemoved.size()-1; i >= 0; --i) {
	    log.info("Removing unpaired nucleotide " + strand.getChild(i).getFullName());
	    strand.removeChild(i);
	}
    }

}
