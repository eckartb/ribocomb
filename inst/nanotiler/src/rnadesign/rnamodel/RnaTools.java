package rnadesign.rnamodel;

import java.util.Properties;
import tools3d.*;
import tools3d.objects3d.*;

/** general helper methods for handling RNA objects with scene graphs */
public class RnaTools {

    public static final String[] RNA_CLASSNAMES = { "Atom3D","RnaStrand", "StrandJunction3D", "BranchDescriptor3D",
						    "Nucleotide3D","Object3D" };
    public static String rnaDisplaySelectionType="Atom3D";

    /** returns report of closest object of certain class 
     */
    public static Properties reportClosestRnaObject(Object3D root, Vector3D position, String className) {
	Properties properties = new Properties();
	Object3D obj = Object3DTools.findClosestByClassName(root, position, className);
	if (obj != null) {
	    properties.setProperty(className, obj.getFullName());
	}
	return properties;
    }

    /** returns closest object of certain class 
     */
    public static Properties reportClosestRnaObjects(Object3D root, Vector3D position) {
	Properties properties = new Properties();
	for (int i = 0; i < RNA_CLASSNAMES.length; ++i) {
	    properties.putAll(reportClosestRnaObject(root, position, RNA_CLASSNAMES[i]));
	}
	return properties;
    }

    /** returns RNA_CLASSNAMES */
    public static String[] getRnaClassNames(){
	return RNA_CLASSNAMES;
    }

    /** sets which type of object is selected when the mouse
     *  is clicked (atom3d, nucleotide, etc...)*/
    public static void setRnaDisplaySelectionType(String type){
	boolean validType = false;
	for(int i=0;i<RNA_CLASSNAMES.length;i++){
	    if(type.equals(RNA_CLASSNAMES[i]))
		validType = true;
	}
	if(validType)
	    rnaDisplaySelectionType = type;
    }
    /** get rnaDisplaySelectionType */
    public static String getRnaDisplaySelectionType(){
	return rnaDisplaySelectionType;
    }

    /** used to find the selected object based on the selection type specified
     *  in the display options menu*/
    public static Object3D findTrueSelectedObject(Object3D selectedObject){
	Object3D trueSelectedObject =  Object3DTools.findClosestByClassName(Object3DTools.findRoot(selectedObject), selectedObject.getPosition(), getRnaDisplaySelectionType());

	return trueSelectedObject;
    }


}
