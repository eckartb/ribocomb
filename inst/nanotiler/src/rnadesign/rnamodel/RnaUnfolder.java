package rnadesign.rnamodel;

import java.util.Properties;
import java.util.logging.*;
import tools3d.*;
import tools3d.objects3d.*;
import java.util.Random;
import generaltools.Randomizer;

/** Unfolds a set of Rna strands by mutating backbone torsion angles and avoiding new collisions */
public class RnaUnfolder implements RnaFolder {

    private int stepMax = 100;
    public static final double RESIDUE_DIST_CUTOFF = 30.0;
    public static final double ATOM_DIST_COLLISION = 0.9;
    private static Random rnd = Randomizer.getInstance();
    private static final double ANGLE_STEP = PackageConstants.DEG2RAD * 10.0;
    private static Logger log = Logger.getLogger(PackageConstants.LOGGER_NAME);

    private int countCollisions(Object3DSet strandSet) {
	int collisionCounter = 0;
	for (int i = 0; i < strandSet.size(); ++i) {
	    RnaStrand strandI = (RnaStrand)(strandSet.get(i));
	    for (int k = 0; k < strandI.getResidueCount(); ++k) {
		Nucleotide3D res1 = (Nucleotide3D)(strandI.getResidue3D(k));
		for (int j = i; j < strandSet.size(); ++j) {
		    RnaStrand strandJ = (RnaStrand)(strandSet.get(j));
		    for (int m = 0;  m < strandJ.getResidueCount(); ++m) {
			if ((j == i) && (m <= k)) {
			    continue;
			} 
			Nucleotide3D res2 = (Nucleotide3D)(strandJ.getResidue3D(m));
			if (res1.distance(res2) > RESIDUE_DIST_CUTOFF) {
			    continue; // skip to next residue, they are too far apart!
			}
			for (int p = 0; p < res1.getAtomCount(); ++p) {
			    Atom3D atom1 = res1.getAtom(p);
			    for (int q = 0; q < res2.getAtomCount(); ++q) {
				Atom3D atom2 = res2.getAtom(q);
				if (atom1.distance(atom2) < ATOM_DIST_COLLISION) {
				    ++collisionCounter;
				}
			    }
			}
		    }
		}
	    }
	}
	return collisionCounter;
    }

    public boolean mutationStep(Object3DSet strandSet, int initialCollisions) {
	int strandId = rnd.nextInt(strandSet.size());
	RnaStrand strand = (RnaStrand)(strandSet.get(strandId));
	boolean result = true;
	int resId = rnd.nextInt(strand.size());
	int angleId = rnd.nextInt(5);
	while (angleId == 3) { // 
	    angleId = rnd.nextInt(5);
	}
	double angle = ANGLE_STEP * rnd.nextGaussian();
	try {
	    strand.rotateAroundBackboneAngle(resId, angleId, angle);
	}
	catch (RnaModelException rne) {
	    log.info("Could not rotate!");
	}
	int collisions = countCollisions(strandSet);
	if ((!result) || (collisions > initialCollisions)) {
	    // undo step
	    try {
		strand.rotateAroundBackboneAngle(resId, angleId, -angle);
	    }
	    catch (RnaModelException rne) {
		log.warning("Could not rotate back!");
	    }
	    result = false;
	}
	assert countCollisions(strandSet) == initialCollisions;
	return result;
    }

    public Properties fold(Object3D obj) {
	Properties properties = new Properties();
	Object3DSet strandSet = Object3DTools.collectByClassName(obj, "RnaStrand");
	int initialCollisionCount =  countCollisions(strandSet);
	int acceptCount = 0;
	for (int i = 0; i < stepMax; ++i) {
	    // log.info("Working on step unfolding step : " + (i+1) + " accepted so far: " + acceptCount);
	    boolean accepted = mutationStep(strandSet, initialCollisionCount);
	    if (accepted) {
		++acceptCount;
	    }
	}
	properties.setProperty("number_steps", "" + stepMax);
	properties.setProperty("number_accept", "" + acceptCount);
	return properties;
    }

}
