/**
 * 
 */
package rnadesign.rnamodel;

import java.util.ArrayList;
import java.util.List;

import sequence.Sequence;
import tools3d.objects3d.Object3D;

/** Collects all Rna sequences of one node in tree
 * @author Eckart Bindewald
 *
 */

//CV - somewhere the sequence is getting added as Sequence seq RNA_A NNNNNNNNNN) and seq is its name. Its name should be RNA_A. I need to find where that happens and change it.
public class SecondaryStructureSequenceCollector extends SequenceCollector {
	
    private List<Sequence> list; //list of collected sequences
	
    public SecondaryStructureSequenceCollector() {
	list = new ArrayList<Sequence>(); //list of collected sequences is cleared
    }
	
    public void act(Object3D obj) {
	if (obj instanceof BioPolymer && secondaryStructureSequence(obj)) {
	    BioPolymer strand = (BioPolymer)obj; // could be RNA, DNA, Protein
	    addSequence(strand); 
	}
    }

	/** gets n'th collected sequence */
    public void addSequence(Sequence s) {
	list.add(s);
    }
	
    public void clear() {
	list.clear();
    }
    
    /** gets n'th collected sequence */
	public Sequence getSequence(int n) {
	    if (n < getSequenceCount()) {
		return (Sequence)(list.get(n));
		}
		return null;
	}
	
	/** returns number of collected sequences so far */
	public int getSequenceCount() {
	    return list.size();
	}

    /**
     * Determine if the sequence is part of a junction or part
     * of a normal strand. A sequence is part of a junction
     * if it has a parent node that is a junction.
     * @param o Determine if this sequence object is part of a junction
     * @return Returns false if the object <code>o</code> is part
     * of a junction and true if otherwise.
     */
    private static boolean secondaryStructureSequence(Object3D o) {
	if(o.getParent() == null)
	    return true;

	if(o.getParent() instanceof StrandJunction3D)
	    return false;

	return secondaryStructureSequence(o.getParent());

    }

	
}
