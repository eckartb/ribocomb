/** This class implements concept of an "binding site"
 *  A binding site as a position and orientation in space (thus Object3)
 *  it optionally can have a part of a sequence associated with it
 *  The parent object is the object the binding site belongs to
 */

package rnadesign.rnamodel;

import sequence.SequenceSubset;
import tools3d.objects3d.Object3D;

public interface SequenceBindingSite extends Object3D {

    /** returns sequence subset: has reference of full sequence plus its subset */
    public SequenceSubset getBindingSequence();

    /** sets sequence
     * @param seq sequence to be set (whole sequence, including non-binding part
     * @param minId  minimum nucleotide position (5' end). Counting starts from zero.
     * @param maxId  maximum nucleotide id involding in binding PLUS ONE
     */ 
    /*
      public void setBindingSequence(Sequence seq, int minId, int maxId);
    */

    public void setBindingSequence(SequenceSubset bindindSequence);
    
}
