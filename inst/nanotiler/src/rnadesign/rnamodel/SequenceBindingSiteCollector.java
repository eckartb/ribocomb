/**
 * 
 */
package rnadesign.rnamodel;

import java.util.ArrayList;
import java.util.List;

import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DAction;
import tools3d.objects3d.Object3DActionVisitor;

/** Collects all Rna binding sites of one node in tree
 * @author Eckart Bindewald
 *
 */
public class SequenceBindingSiteCollector implements Object3DAction {
	
	private List<Object3D> list;
	
	public SequenceBindingSiteCollector() {
		list = new ArrayList<Object3D>();
	}
	
	public void act(Object3D obj) {
	    if (obj instanceof SequenceBindingSite) {
		list.add(obj);
	    }
	}
	
	/** gets n'th collected binding site */
	public SequenceBindingSite get(int n) {
	    if (n < size()) {
		return (SequenceBindingSite)(list.get(n));
	    }
	    return null;
	}
	
	/** returns number of collected sequences so far */
	public int size() {
		return list.size();
	}

    /** traverses tree and fixes all names (replaces with references to objects) */
    public static void actAll(Object3D root, SequenceBindingSiteCollector collector) {
	Object3DActionVisitor visitor = new Object3DActionVisitor(root, collector); 
	visitor.nextToEnd();
    }
    
}
