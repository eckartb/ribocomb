/**
 * 
 */
package rnadesign.rnamodel;

import java.util.ArrayList;
import java.util.List;

import sequence.Sequence;
import sequence.SequenceContainer;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DAction;

/** Collects all Rna sequences of one node in tree
 * @author Eckart Bindewald
 *
 */

//CV - somewhere the sequence is getting added as Sequence seq RNA_A NNNNNNNNNN) and seq is its name. Its name should be RNA_A. I need to find where that happens and change it.
public class SequenceCollector implements Object3DAction, SequenceContainer {
	
    private List<Sequence> list; //list of collected sequences
	
    public SequenceCollector() {
	list = new ArrayList<Sequence>(); //list of collected sequences is cleared
    }
	
    public void act(Object3D obj) {
	if (obj instanceof SequenceObject3D) {
	    SequenceObject3D rna = (SequenceObject3D)obj;
	    for (int i = 0; i < rna.getSequenceCount(); ++i) {
		addSequence(rna.getSequence(i)); //adds the rna to sequences
	    }
	}
	else if (obj instanceof BioPolymer) {
	    BioPolymer strand = (BioPolymer)obj; // could be RNA, DNA, Protein
	    addSequence(strand); 
	}
    }

	/** gets n'th collected sequence */
    public void addSequence(Sequence s) {
	list.add(s);
    }
	
    public void clear() {
	list.clear();
    }
    
    public Sequence getSequence(String name) {
		int id = getSequenceId(name);
		if (id >= 0) {
		   return getSequence(id);
		}
		return null;
    }
    
    /** gets n'th collected sequence */
	public Sequence getSequence(int n) {
	    if (n < getSequenceCount()) {
		return (Sequence)(list.get(n));
		}
		return null;
	}
	
	public Sequence get(int n){
		return getSequence(n);
	}
	
	public int getSequenceId(String name) {
	   for (int i = 0; i < list.size(); ++i) {
	   	if (list.get(i).getName().equals(name)) {
	   		return i;
	   	}
	   }
	   return -1;		
	}
	
	/** returns number of collected sequences so far */
	public int getSequenceCount() {
	    return list.size();
	}

	/** returns number of collected sequences so far */
	public int size() {
	    return list.size();
	}
	
	

	
}
