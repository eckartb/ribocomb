/** 
 * This interface describes the concept of an RNA building block.
 * A building block (like a stem, or a loop) consist of a set of 
 * Sequences, a set of Connectors and internal properties like 
 * stems formed between the sequences
 */
package rnadesign.rnamodel;

import sequence.SequenceContainer;
import tools3d.Vector3D;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.SimpleObject3D;

/**
 * @author Eckart Bindewald
 */
public interface SequenceObject3D extends Object3D, SequenceContainer {

    /** returns position of position pos of n'th sequence */
    public Vector3D getResiduePosition(int seq, int pos);

}
