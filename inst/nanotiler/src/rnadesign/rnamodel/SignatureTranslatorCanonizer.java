package rnadesign.rnamodel;

import generaltools.AlgorithmFailureException;
import java.io.*;
import tools3d.Vector3D;
import tools3d.objects3d.*;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.*;
import launchtools.*;

import org.testng.annotations.*;
import java.util.ResourceBundle;

import static rnadesign.rnamodel.PackageConstants.*;

/** Uses "tra" program from Signature Translator package
 * @see http://www.cs.sandia.gov/~jfaulon
 */
public class SignatureTranslatorCanonizer implements GraphCanonizer {

    private String nameBase = "signature_graph";
    private static ResourceBundle rb = ResourceBundle.getBundle("NanoTiler");
    private static Logger log = Logger.getLogger(LOGFILE_DEFAULT);
    private boolean debugMode = false;
    private String programName = "tra"; // rb.getString("signature");

    public SignatureTranslatorCanonizer() { }

    public String generateCanonizedRepresentation(Object3DLinkSetBundle graph) throws AlgorithmFailureException {
	// translate to molecule
	Object3DLinkSetBundle moleculeBundle = AtomTools.convertGraphToMolecule(graph.getObject3D(), 
										graph.getLinks(), nameBase);
	// write to "polygraf" file (ending .bgf)
	HeteroAtomPdbWriter writer = new HeteroAtomPdbWriter();
	List<String> resultLines;
	try {
	    File tmpFile = File.createTempFile("signature",".bgf");
	    if (!debugMode) {
		tmpFile.deleteOnExit();
	    }
	    String fileName = tmpFile.getAbsolutePath();
	    FileOutputStream fos = new FileOutputStream(fileName);
	    PrintStream ps = new PrintStream(fos);
	    String content = writer.toString(moleculeBundle);
	    ps.println(content);
	    fos.close();
	    if (debugMode) {
		log.info("wrote temporary polygraf file: " + content);
	    }
	    String tmpFileNameBase = fileName.substring(0, fileName.length()-4); // remove trailing ".bgf" ending
	    // launch "tra" program
	    String commandString = programName + " " + tmpFileNameBase + " polygraf canonize"; 
	    log.fine("Launching command with string: " + commandString);
	    String[] commandWords = commandString.split(" ");
	    // generate command
	    RunCommand command = new SimpleRunCommand(commandWords);
	    // create queue manager (singleton pattern)
	    QueueManager queueManager = SimpleQueueManager.getInstance();
	    // generate job
	    Job job = queueManager.createJob(command);
	    // add listener to job
	    // launch command
	    queueManager.submit(job);	    	
	    // return results that launched program wrote to standard output:
	    resultLines = job.getRunInfo().getLines();
	    if (!debugMode) { // remove tmp files
		boolean deletionResult = tmpFile.delete(); // remove file again
		if (!deletionResult) {
		    log.warning("Could not delete file: " + fileName);
		}
		assert !tmpFile.exists();
		File file2 = new File(tmpFileNameBase + "_out.bgf");
		if (file2.exists()) {
		    boolean deletionResult2 = file2.delete();
		    assert deletionResult2;
		}
		else {
		    log.warning("Could not find signature result file: " + file2.getAbsolutePath());
		}
	    }
	}
	catch (IOException ioe) {
	    throw new AlgorithmFailureException(ioe.getMessage());
	}
	if (resultLines == null) {
	    throw new AlgorithmFailureException("No output from signature translation program received!");
	}
	StringBuffer buf = new StringBuffer();
	for (int i = 0; i < resultLines.size(); ++i) {
	    buf.append(resultLines.get(i) + NEWLINE);
	}
	String result = buf.toString().trim();
	assert result != null;
	if (result.length() == 0) {
	    throw new AlgorithmFailureException("Output from signature translation program has zero length! Maybe signature generater help program is not installed?");
	}
	result = processRawCanonizedString(result);
	assert result != null;
	if (result.length() == 0) {
	    throw new AlgorithmFailureException("Processed output from signature translation program has zero length! Maybe signature generater help program is not installed?");
	}
	return result;
    }

    /** replaces newline with semicolon, space with percent */
    String processRawCanonizedString(String canonicString) {
	canonicString = canonicString.replace('\n', ';');
	canonicString = canonicString.replace(' ', '%'); // make sure no spaces are in string
	if (canonicString.charAt(canonicString.length()-1) == ';') { // remove trailing ';'
	    canonicString = canonicString.substring(0, canonicString.length()-1);
	}
	StringBuffer buf = new StringBuffer();
	String[] words = canonicString.split(";");
	for (String word : words) {
	    if (word.indexOf("[") >= 0) {
		buf.append(word);
		buf.append(";");
	    }
	}
	String result = buf.toString();
	if ((result.length() > 0) && (result.charAt(result.length()-1) == ';')) {
	    result = result.substring(0, result.length()-1);
	}
	return result;
    }

    @Test(groups={"new"})
    public void testGenerateCanonizedRepresentation() {
	// generate test graph
	Object3D rawGraph = new SimpleObject3D();
	rawGraph.setName("test_graph");
	Object3D p1 = new SimpleObject3D(new Vector3D(0, 0, 0));
	Object3D p2 = new SimpleObject3D(new Vector3D(100, 0, 0));
	Object3D p3 = new SimpleObject3D(new Vector3D(0, 100, 0));
	Object3D p4 = new SimpleObject3D(new Vector3D(0, 0, 100));
	LinkSet links = new SimpleLinkSet();
	p1.setName("p1");
	p2.setName("p2");
	p3.setName("p3");
	p3.setProperty("atom_element", "S");
	links.add(new SimpleLink(p1, p2));
	links.add(new SimpleLink(p2, p3));
	links.add(new SimpleLink(p1, p3));
	rawGraph.insertChild(p1);
	rawGraph.insertChild(p2);
	rawGraph.insertChild(p3);
	Object3DLinkSetBundle fullGraph = new SimpleObject3DLinkSetBundle(rawGraph, links);
	String correctAnswer1 = "[s_]([o_]([o_,1])[o_,1])";
	String correctAnswer2 = "[s_]([n_]([n_,1][o_,2])[n_,1]([o_,2]))";
	GraphWriter writer = new GraphWriter();
	System.out.println("Calling canonizer for graph: " + writer.toString(fullGraph));
	try {
	    String result = generateCanonizedRepresentation(fullGraph);
	    System.out.println("The canonized output is: " + result);
	    assert correctAnswer1.equals(result);
	}
	catch(AlgorithmFailureException afe) {
	    System.out.println("Error canonizing graph: " + afe.getMessage());
	    assert false;
	}
	rawGraph.insertChild(p4);
	links.add(new SimpleLink(p1, p4));
	links.add(new SimpleLink(p2, p4));
	System.out.println("Calling canonizer for graph: " + writer.toString(fullGraph));
	try {
	    String result = generateCanonizedRepresentation(fullGraph);
	    System.out.println("The canonized output is: " + result);
	    assert correctAnswer2.equals(result);
	}
	catch(AlgorithmFailureException afe) {
	    System.out.println("Error canonizing graph: " + afe.getMessage());
	    assert false;
	}

    }


    @Test(groups={"new"})
    public void testGenerateCanonizedRepresentation2() {
	// generate test graph
	Object3D rawGraph = new SimpleObject3D();
	rawGraph.setName("test_graph");
	Object3D p1 = new SimpleObject3D(new Vector3D(0, 0, 0));
	Object3D p2 = new SimpleObject3D(new Vector3D(100, 0, 0));
	Object3D p3 = new SimpleObject3D(new Vector3D(0, 100, 0));
	Object3D p4 = new SimpleObject3D(new Vector3D(0, 0, 100));
	LinkSet links = new SimpleLinkSet();
	p1.setName("p1");
	p2.setName("p2");
	p3.setName("p3");
	p3.setName("p4");
	p2.setProperty("atom_element", "S");
	p4.setProperty("atom_element", "S");
	links.add(new SimpleLink(p1, p2));
	links.add(new SimpleLink(p2, p3));
	links.add(new SimpleLink(p3, p4));
	links.add(new SimpleLink(p1, p4));
	rawGraph.insertChild(p1);
	rawGraph.insertChild(p2);
	rawGraph.insertChild(p3);
	rawGraph.insertChild(p4);
	Object3DLinkSetBundle fullGraph = new SimpleObject3DLinkSetBundle(rawGraph, links);
	String correctAnswer1 = "[s_]([o_]([o_,1])[o_,1])";
	GraphWriter writer = new GraphWriter();
	System.out.println("Calling canonizer for graph: " + writer.toString(fullGraph));
	try {
	    String result = generateCanonizedRepresentation(fullGraph);
	    System.out.println("The canonized output is: " + result);
	    // assert correctAnswer1.equals(result);
	    p2.setProperty("atom_element", "O");
	    p4.setProperty("atom_element", "O");
	    p1.setProperty("atom_element", "S");
	    p3.setProperty("atom_element", "S");
	    String result2 = generateCanonizedRepresentation(fullGraph);
	    System.out.println("The canonized output of the relable graph is: " + result2);
	    assert result.equals(result2);
	}
	catch(AlgorithmFailureException afe) {
	    System.out.println("Error canonizing graph: " + afe.getMessage());
	    assert false;
	}

    }

    @Test(groups={"new"})
    public void testGenerateCanonizedRepresentation3() {
	// generate test graph
	Object3D rawGraph = new SimpleObject3D();
	rawGraph.setName("test_graph");
	Object3D p1 = new SimpleObject3D(new Vector3D(0, 0, 0));
	Object3D p2 = new SimpleObject3D(new Vector3D(0, 0, 0));
	Object3D p3 = new SimpleObject3D(new Vector3D(0, 0, 0));
	Object3D p4 = new SimpleObject3D(new Vector3D(0, 0, 0));
	LinkSet links = new SimpleLinkSet();
	p1.setName("p1");
	p2.setName("p2");
	p3.setName("p3");
	p3.setName("p4");
	p2.setProperty("atom_element", "S");
	p4.setProperty("atom_element", "S");
	links.add(new SimpleLink(p1, p2));
	links.add(new SimpleLink(p2, p3));
	links.add(new SimpleLink(p3, p4));
	links.add(new SimpleLink(p1, p4));
	rawGraph.insertChild(p1);
	rawGraph.insertChild(p2);
	rawGraph.insertChild(p3);
	rawGraph.insertChild(p4);
	Object3DLinkSetBundle fullGraph = new SimpleObject3DLinkSetBundle(rawGraph, links);
	String correctAnswer1 = "[s_]([o_]([o_,1])[o_,1])";
	GraphWriter writer = new GraphWriter();
	System.out.println("Calling canonizer for graph: " + writer.toString(fullGraph));
	try {
	    String result = generateCanonizedRepresentation(fullGraph);
	    System.out.println("The canonized output is: " + result);
	    // assert correctAnswer1.equals(result);
	    p2.setProperty("atom_element", "O");
	    p4.setProperty("atom_element", "O");
	    p1.setProperty("atom_element", "S");
	    p3.setProperty("atom_element", "S");
	    String result2 = generateCanonizedRepresentation(fullGraph);
	    System.out.println("The canonized output of the relable graph is: " + result2);
	    assert result.equals(result2);
	}
	catch(AlgorithmFailureException afe) {
	    System.out.println("Error canonizing graph: " + afe.getMessage());
	    assert false;
	}

    }


}
