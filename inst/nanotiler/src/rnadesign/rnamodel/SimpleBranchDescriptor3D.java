package rnadesign.rnamodel;

import java.util.ArrayList;
import java.util.List;

import tools3d.CoordinateSystem;
import tools3d.LineShape;
import tools3d.Vector3D;
import tools3d.objects3d.CoordinateSystem3D;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DTools;
import tools3d.objects3d.SimpleObject3D;

import static rnadesign.rnamodel.RnaConstants.HELIX_BASE_PAIRS_PER_TURN;
import static rnadesign.rnamodel.RnaConstants.HELIX_RISE;

public class SimpleBranchDescriptor3D extends SimpleObject3D implements BranchDescriptor3D {
    
    private NucleotideStrand incomingStrand;
    private NucleotideStrand outgoingStrand;
    private HelixParameters helixParameters;
    private int incomingStartIndex = 0;
    private int outgoingStopIndex = 0;    
    private int offset = 0;

    public static final String CLASS_NAME = "BranchDescriptor3D";
    public static final String COORD_NAME = "coordinate-system";
    public static final String BASE_NAME = "base";
    //    public static final String DIR_NAME = "dir";
    public static final double ANGLE_DIFF_MAX = 89.0; // TODO !!! 40.0; // maximum discrepancy between ideal direction and found direction in degree

    /** default constructor is used internally only for clone methods */
    public SimpleBranchDescriptor3D() {
	helixParameters = new HelixParameters(); // set default helix parameters
    }

    /** Construct branch descriptor without RNA strands but with orientation. Use with care, object will not validate ! */
    public SimpleBranchDescriptor3D(CoordinateSystem3D coordinateSystem) {
	helixParameters = new HelixParameters(); // set default helix parameters
	CoordinateSystem3D lcs = new CoordinateSystem3D(coordinateSystem);
	lcs.setName(COORD_NAME);
	setPosition(coordinateSystem.getPosition());
	insertChild(lcs);
    }

    /** constructs object.
     * @param incomingStrand: strand that points towards junction (away from helix center)
     * @param outgoingStrand: strand that points away from junction (into helix center)
     * @param incomingIndex    : index of reference nucleotide of incoming strand (strand running towards junction)
     * @param outgoingIndex    : index of reference nucleotide of outgoing strand (strand running away from junction)
     * @param coordinateSystem : defines helix end by z-axis pointing towards helix, zero-point being base of helix,
     *                           x-axis pointing from base to first nucleotide, y-axis given through x cross y = z
     */
    public SimpleBranchDescriptor3D(NucleotideStrand incomingStrand,
				    NucleotideStrand outgoingStrand,
				    int incomingIndex,
				    int outgoingIndex,
				    CoordinateSystem coordinateSystem) { 				   
	super();
	assert(coordinateSystem.isValid());
	helixParameters = new HelixParameters(); // set default helix parameters
	this.incomingStrand = incomingStrand;
	this.outgoingStrand = outgoingStrand;
	this.incomingStartIndex = incomingIndex;
	this.outgoingStopIndex = outgoingIndex;
	String myName = incomingStrand.getName() + "_" + (incomingIndex+1) + "_" + outgoingStrand.getName() + "_" + (outgoingIndex+1);
	setName(myName);

	// System.out.println();
	// try {
	Vector3D pIn = incomingStrand.getResidue3D(incomingIndex).getPosition(); // TODO : problem : position of residue not entirely defined! Prefer reference atom
	Vector3D pOut = outgoingStrand.getResidue3D(outgoingIndex).getPosition();
	    // }
// 	catch (IndexOutOfBoundsException e) {
// 	    incomingIndex = incomingIndex - 1;
// 	    this.incomingStartIndex = incomingIndex;
// 	    outgoingIndex = outgoingIndex - 1;
// 	    this.outgoingStopIndex = outgoingIndex;
// 	    Vector3D pIn = incomingStrand.getResidue3D(incomingIndex).getPosition();
// 	    Vector3D pOut = outgoingStrand.getResidue3D(outgoingIndex).getPosition();
// 	} //TODO: remove, just for debugging! Christine
	Vector3D newPos = coordinateSystem.getPosition(); // Vector3D.average(inObj.getPosition(), outObj.getPosition());
	setIsolatedPosition(newPos);
	// Object3DTools.balanceTree(this); // set this position to average of child positions
	// coordinateSystemId = this.size(); // current number of children is id of coordinate System. Careful: this can change
	CoordinateSystem3D lcs = new CoordinateSystem3D(coordinateSystem);
	lcs.setName(COORD_NAME);
	insertChild(lcs);
	if (!isValid()) {
	    System.out.println(" Invalid branch descriptor: " + toString());
	    assert(isValid()); // every generate branch descriptor object should be "valid" (consistent, error free)
	}
    }

    /** "deep" clone not including parent or children: every object is newly generated!
     */
    public Object cloneDeepThis() {
	SimpleBranchDescriptor3D obj = new SimpleBranchDescriptor3D();
// 	obj.setParent(getParent());
// 	obj.setPosition(getPosition());
	obj.copyDeepThisCore(this); // implementation from SimpleObject3D
	obj.helixParameters = new HelixParameters(helixParameters);
	obj.incomingStartIndex = incomingStartIndex;
	obj.outgoingStopIndex = outgoingStopIndex;
	obj.incomingStrand = (NucleotideStrand)(incomingStrand.cloneDeep()); // problematic!!!
	obj.offset = offset;
	if (!isSingleSequence()) {
	    obj.outgoingStrand = (NucleotideStrand)(outgoingStrand.cloneDeep());
	}
	else {
	    obj.outgoingStrand = obj.incomingStrand;
	}
	return obj; // TODO : test if ok!
    }

    /** computes idealized position of reference atom of base of n'th basePair belonging to strand==OUTGOING_STRAND, or strand==INCOMING_STRAND.
     * Remember: outgoing strand leaves the junction, but points *towards* the stem. */
    public Vector3D computeHelixPosition(int basePair, int strand) {
	CoordinateSystem cs = getCoordinateSystem();
	LineShape line = Rna3DTools.computeHelix(basePair, cs, helixParameters);
	Vector3D result = null;
	switch (strand) {
	case OUTGOING_STRAND: 
	    result = line.getPosition1();
	    break;
	case INCOMING_STRAND:
	    result = line.getPosition2();
	    break;
	default:
	    assert false; // internal error, should never happen
	}
	return result;
    }

    /** relies on child node called "base". Describes center of helix. */
    public Vector3D getBasePosition() {
	return getPosition();
    }

    public String getClassName() { return CLASS_NAME; }

    /** returns coordinate system object */
    public CoordinateSystem getCoordinateSystem() {
	Object3D child = getChild(COORD_NAME);
	if (child == null) {
	    assert false;
	    return null;
	}
	return (CoordinateSystem3D)(child);
    }

    /** relies on child node called "dir" */
    public Vector3D getDirection() {
// 	int idx = getIndexOfChild(DIR_NAME);
// 	assert (idx >= 0);
// 	return getChild(idx).getRelativePosition();
	return getCoordinateSystem().getZ(); // z-coordinate of coordinate system is new direction of helix
    }

    /** returns used helix parameters */
    public HelixParameters getHelixParameters() { return helixParameters; }

    public NucleotideStrand getIncomingStrand() { return incomingStrand; }

    public NucleotideStrand getOutgoingStrand() { return outgoingStrand; }

    public int getIncomingIndex() { return incomingStartIndex; }

    public int getOutgoingIndex() { return outgoingStopIndex; }

    /** returns name of "in" object (position of incoming strand) */
    // public String getInName() { return inName; }

    /** returns offfset */
    public int getOffset() { return this.offset; }

    /** returns name of "out" object (position of outgoing strand) */
    // public String getOutName() { return outName; }

    // private Object3D getOutObject() { return getChild(outName); }

    // private Object3D getInObject() { return getChild(inName); }

    /** propagate branch descriptor along ideal helix. The offset "off" describes the number of base pairs to move.
     * @return Returns clone propagated coordinate system. */
    /* public static CoordinateSystem generatePropagatedCoordinateSystem(CoordinateSystem csOrig, HelixParameters helixParameters,
								      int offset) {
	// CoordinateSystem3D cs = new CoordinateSystem3D(getCoordinateSystem());
	// move cs according to offset:
	cs.translate(cs.getZ().mul(helixParameters.getRise() * offset));
	// rotate coordinate system:
	double ang = ((2.0 * Math.PI) / helixParameters.getBasePairsPerTurn()) * offset;
	cs.rotate(cs.getZ(), ang);
	return cs;
	} */

    /** propagate branch descriptor along ideal helix. The offset "off" describes the number of base pairs to move.
     * @return Returns clone propagated coordinate system. */
    public static CoordinateSystem3D generatePropagatedCoordinateSystem(CoordinateSystem3D csOrig, HelixParameters helixParameters,
								      int offset) {
	// CoordinateSystem3D cs = new CoordinateSystem3D(getCoordinateSystem());
	// move cs according to offset:
	CoordinateSystem3D cs = (CoordinateSystem3D)(csOrig.cloneDeep());
	cs.translate(cs.getZ().mul(helixParameters.getRise() * offset));
	// rotate coordinate system:
	double ang = ((2.0 * Math.PI) / helixParameters.getBasePairsPerTurn()) * offset;
	cs.rotate(cs.getZ(), ang);
	return cs;
    }

    /** propagate branch descriptor along ideal helix. The offset "off" describes the number of base pairs to move.
     * @return Returns clone propagated coordinate system. */
    public CoordinateSystem generatePropagatedCoordinateSystem(int offset) {
	CoordinateSystem3D cs = new CoordinateSystem3D(getCoordinateSystem());
	return generatePropagatedCoordinateSystem(cs, helixParameters, offset);
	// move cs according to offset:
	// cs.translate(cs.getZ().mul(helixParameters.getRise() * offset));
	// rotate coordinate system:
	// double ang = ((2.0 * Math.PI) / helixParameters.getBasePairsPerTurn()) * offset;
	// cs.rotate(cs.getZ(), ang);
	// return cs;
    }

    /** propagate branch descriptor along ideal helix. The offset "off" describes the number of base pairs to move. */
    public void propagate(int offset) {
	CoordinateSystem cs =  getCoordinateSystem();
	// move cs according to offset:
	cs.translate(cs.getZ().mul(HELIX_RISE * offset));
	// rotate coordinate system:
	double ang = ((2.0 * Math.PI) / HELIX_BASE_PAIRS_PER_TURN) * offset;
	cs.rotate(cs.getZ(), ang);
	incomingStartIndex += offset;
	outgoingStopIndex -= offset;
    }

    /** relies on child node called "base". Describes center of helix. */
    public void setBasePosition(Vector3D p) {
	setPosition(p);
    }

    /** sets coordinate system object by copying information. */
    public void setCoordinateSystem(CoordinateSystem other) {
	setPosition(other.getPosition()); // TODO : test! EB 2008
	CoordinateSystem cs = getCoordinateSystem();
	cs.copy(other);
    }

    /** Sets helix parameters */
    public void setHelixParameters(HelixParameters prm) {
	this.helixParameters = prm;
    }

    /** returns true if incoming strand is equal to outgoing strand */
    public boolean isSingleSequence() {
	// return incomingStrand.isProbablyEqual(outgoingStrand); // TODO : slow!!!
	return isStrandPairProbablyEqual(incomingStrand, outgoingStrand);
    }

    /** fast check if strands are probably the same */
    private boolean isStrandPairProbablyEqual(BioPolymer strand1, 
					      BioPolymer strand2) {
	if (strand1 == strand2) {
	    return true;
	}
	if (! strand1.getName().equals(strand2.getName())) {
	    return false;
	}
	if (strand1.size() != strand2.size()) {
	    return false;
	}
	if ((strand1.size() == 0) || (strand1.getChild(0).distance(strand2.getChild(0)) < 0.01)) {
	    return true;
	}
	// check all positions:
	// 	for (int i = 0; i < strand1.getResidueCount(); ++i) {
	// 	    if (! isResiduePairProbablyEqual(strand1.getResidue3D(i),
	// 					     strand2.getResidue3D(i))) {
	// 		return false;
	// 	    }
	// 	}
	return false;
    }

    /** fast check if residues are probably the same. Does not check every atom to save time */
    private boolean isResiduePairProbablyEqual(Residue3D residue1, 
					       Residue3D residue2) {
	// check if same type of residue
	if (!residue1.getSymbol().equals(residue2.getSymbol())) {
	    return false;
	}
	// check if same number of child nodes
	if (residue1.size() != residue2.size()) {
	    return false;
	}
	if (residue1.getPosition().distance(residue2.getPosition()) > DISTANCE_TOLERANCE) {
	    return false;
	}
	return true;
    }

    public boolean isValid() {
	// check if these 3 important child objects exist:
	// 	if (getIndexOfChild(inName) < 0) {
	// 	    return false;
	// 	}
	// 	if (getIndexOfChild(outName) < 0) {
	// 	    return false;
	// 	}
	// 	Vector3D dir = getDirection();
	// 	Vector3D dir2 = getOutObject().getRelativePosition().minus(getInObject().getRelativePosition());
	// 	// angle between those two vectors must be approximately 90 degree:
	// 	double angleDiff = Math.abs((180.0*dir.angle(dir2)/Math.PI)-90.0);
	// 	if (angleDiff > ANGLE_DIFF_MAX) {
	// 	    return false;
	// 	}
	CoordinateSystem cs = getCoordinateSystem();
	if ( cs == null) {
	    System.out.println("Warning: BranchDescriptor coordinate system undefined.");
	    return false; // no coordinate system defined
	}
	// position of coordinate system must be equal to position of branch descriptor
	if (cs.getPosition().distance(getPosition()) > 0.01) {
	    System.out.println("Warning: BranchDescriptor coordinate system position inconsistent.");
	    return false;
	}
	// check for Watson Crick pairs:
	char c1 = getIncomingStrand().getResidue(getIncomingIndex()).getSymbol().getCharacter();
	char c2 = getOutgoingStrand().getResidue(getOutgoingIndex()).getSymbol().getCharacter();
// 	if (! (DnaTools.isWatsonCrick(c1, c2) || DnaTools.isWobble(c1, c2)) ) {
// 	    System.out.println("Warning: BranchDescriptor uses non-watson-crick pairing: " + c1 + " " + c2 
// 			       + " descriptor: " + toString());
// 	    // return false;
// 	}
	return true;
    }

    public void setIncomingIndex(int n) { this.incomingStartIndex = n; }

    public void setIncomingStrand(NucleotideStrand strand) { this.incomingStrand = strand; }

    /** how for "into the helix" are the reference nucleotides? Typically 2 base pairs */
    public void setOffset(int offset) { this.offset = offset; }

    public void setOutgoingIndex(int n) { this.outgoingStopIndex = n; }

    public void setOutgoingStrand(NucleotideStrand strand) { this.outgoingStrand = strand; }

    public String infoString() {
	String result = getClassName() + " " + 
	    getName() + " " + getPosition() + " dir: " + getDirection();
	if (incomingStrand != null) {
	    result = result + " incoming: " + (incomingStartIndex +1) + " " 
		+ Object3DTools.getFullName(incomingStrand.getResidue3D(incomingStartIndex)) + " ";
	};
	if (outgoingStrand != null) {
	    result = " outgoing: " + (outgoingStopIndex +1) + " " +
		Object3DTools.getFullName(outgoingStrand.getResidue3D(outgoingStopIndex)) + " "
		+ " position: " + getPosition();
	}
	return result;
    }

    public String toString() {
	String result = "(" + getClassName() + " " + 
	    getName() + " " + getIncomingStrand().toString() + 
	    " " + incomingStartIndex + " " + 
	    getOutgoingStrand().toString() + " " + 
	    outgoingStopIndex + " " + getPosition() + 
	    " (children " + size() + " ";
	//+ getChild(COORD_NAME) + " " //don't need toString coordinate system because it's a child...
	//	String result = "(" + getClassName() + " " + getName() + " " + Object3DTools.getFullName(incomingStrand) + " "
	//	    + (incomingStartIndex + 1) + " " + Object3DTools.getFullName(outgoingStrand) + " " + (outgoingStopIndex + 1) 
	//	    + " " + getPosition() + " (children " + size() + " ";
	for (int i = 0; i < size(); ++i) {
	    result = result + getChild(i).toString() + " ";
	}
	result = result + " ) )";
	return result;
    }

}
