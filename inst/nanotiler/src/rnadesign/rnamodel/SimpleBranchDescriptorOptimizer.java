package rnadesign.rnamodel;

import java.util.Properties;
import java.util.logging.Logger;

import static rnadesign.rnamodel.PackageConstants.DEG2RAD;
import static rnadesign.rnamodel.PackageConstants.RAD2DEG;

/** Class that computes for a given pair of branch descriptors an optimized interpolating branch descritpor */
public class SimpleBranchDescriptorOptimizer extends AbstractBranchDescriptorOptimizer {

    public static final String CLASS_NAME = "SimpleBranchDescriptorOptimizer";
    private double rmsLimit;

    public SimpleBranchDescriptorOptimizer(FitParameters limits) { this.rmsLimit = limits.getRmsLimit(); }

    public String getClassName() { return CLASS_NAME; }

    /** optimize stem so that it fits as good as possible with junction branch descriptors 1 and 2.
     *  Careful: junction branch descriptors are assumed to have the offset 1 and -1,
     *  while the result branch descriptor has the residue offset zero.
     * TODO : verify due to changes made in BranchDescriptor3D
     */
    public BranchDescriptor3D optimize(RnaStem3D stem,
				       BranchDescriptor3D branch1,
				       BranchDescriptor3D branch2) throws FittingException {
	return optimize(stem, branch1, branch2, this.rmsLimit);
    }


    /** optimize stem so that it fits as good as possible with junction branch descriptors 1 and 2.
     *  Careful: junction branch descriptors are assumed to have the offset 1 and -1,
     *  while the result branch descriptor has the residue offset zero.
     * TODO : verify due to changes made in BranchDescriptor3D
     */
    private static BranchDescriptor3D optimize(RnaStem3D stem,
					       BranchDescriptor3D branch1,
					       BranchDescriptor3D branch2,
					       double rmsLimit) throws FittingException {
	log.info("Starting SimpleBranchDescriptor.optimize!");
	assert branch1.isValid();
	assert branch2.isValid();
	int bestAngle = 0;
	double bestError = rmsLimit + 0.0001;
	// check in one degree steps:
	for (int a = 0; a < 360; a++) {
	    log.info("Working on angle " + a);
	    try {
		BranchDescriptor3D b = interpolateBranchDescriptor(stem, branch1, branch2, DEG2RAD * a);
		double errorVal = computeBranchDescriptorError(stem.getLength(), branch1, branch2, b); // 
		log.info("Optimizing stem: " + a + " " + errorVal);
		if (errorVal > rmsLimit) {
		    continue;
		}
		else if (errorVal < bestError) {
		    bestError = errorVal;
		    bestAngle = a;
		}
	    }
	    catch (FittingException fe) {
		log.info("could not get good helix fit for angle: " + a);
	    }
	}
	if (bestError > rmsLimit) {
	    log.info("The helix fit of " + bestError + " is higher than the acceptable limit of " + rmsLimit);
	    return null; // NO SUITABLE solution found
	}
	BranchDescriptor3D bBest = interpolateBranchDescriptor(stem, branch1, branch2, RAD2DEG * bestAngle);
	Properties prop = bBest.getProperties();
	if (prop == null) {
	    prop = new Properties();
	    bBest.setProperties(prop);
	}
	log.info("Optimized stem: " + bestAngle + " " + bestError);
	prop.setProperty("fit_score", ""+bestError); // used later for evaluating fit of axial kissing loop!	
	assert(bBest.getProperties() != null);
	return bBest;
    }


    /** optimize stem so that it fits as good as possible with junction branch descriptors 1 and 2.
     *  Careful: junction branch descriptors are assumed to have the offset 1 and -1,
     *  while the result branch descriptor has the residue offset zero.
     * TODO : verify due to changes made in BranchDescriptor3D
     */
    /*
    private static BranchDescriptor3D optimizeBranchDescriptorOld(RnaStem3D stem,
								  BranchDescriptor3D branch1,
								  BranchDescriptor3D branch2,
								  double rmsLimit) {
	// log.fine("Starting optimizeBranchDescriptor!");
	assert branch1.isValid();
	assert branch2.isValid();
	PotentialND stemPotential = new StemBranchDescriptorPotential(stem.getLength(), branch1, branch2);
	double[] startVector = stemPotential.generateLowPosition();
	// 	log.fine("Starting optimization with: ");
	// 	DoubleArrayTools.writeArray(System.out, startVector);
	PotentialNDOptimizer optimizer = new MonteCarloOptimizer();
	optimizer.setVerboseLevel(0);
	OptimizationNDResult result = optimizer.optimize(stemPotential, startVector);
	// log.fine("Finished optimization with: " + result.getBestValue());
	// DoubleArrayTools.writeArray(System.out, result.getBestPosition());
	if (result.getBestValue() > rmsLimit) {
	    log.fine("optimizeBranchDescriptor: Found solution is not sufficient: " + result.getBestValue()); 
	    return null; // no suitable solution found!
	}
	// 	double[] baseAndDirection = result.getBestPosition();
	CoordinateSystem cs = new CoordinateSystem3D(result.getBestPosition());
	NucleotideStrand strand1 = stem.getStrand1();
	NucleotideStrand strand2 = stem.getStrand2();
	int offs = 0; // TODO verify
	BranchDescriptor3D branch = null; 
	// new SimpleBranchDescriptor3D(strand2, strand1, 
	// 								 stem.getStemInfo().getStopPos()-offs, 
	// 								 stem.getStemInfo().getStartPos()+offs,
	// 								 offs, cs);	    
	// code is similar to BranchDescriptorTools.generateBranchDescriptors

	// 	Vector3D basePos = new Vector3D(baseAndDirection[0], baseAndDirection[1], baseAndDirection[2]);
	// 	Vector3D fivePos = new Vector3D(baseAndDirection[3], baseAndDirection[4], baseAndDirection[5]);
	// 	Vector3D dir = new Vector3D(baseAndDirection[6], baseAndDirection[7], baseAndDirection[8]);
	// 	dir.sub(basePos);
	// 	assert dir.length() > 0.0;
	// dir.normalize();
	// 	log.fine("Generated branch descriptor positions: " + basePos + " ; " + fivePos + " ; "
	// 			   + dir);
	// Vector3D basePosFront = basePos; // .plus(dir.mul(RNA3DTools.HELIX_RISE * offs)); // the center is shifted 
	// 	log.fine("Generated branch descriptor positions (shifted): " + basePos + " ; " + fivePos + " ; "
	// 			   + dir + " " + basePosFront);
	// start of stop residue must be close to center of helix
	// 	log.fine("New Base" + basePosFront + " 5' start orig: " + branch.getOutObject().getPosition()
	// 			   + " dist: " + basePosFront.distance(branch.getOutObject().getPosition()));
	// 	log.fine("New Base" + basePosFront + " 5' stop orig: " + branch.getInObject().getPosition()
	// 			   + " dist: " + basePosFront.distance(branch.getInObject().getPosition()));
	// 	assert basePosFront.distance(branch.getOutObject().getPosition()) < HELIX_CENTER_ASSERTION_DIST;
	// 	assert basePosFront.distance(branch.getInObject().getPosition()) < HELIX_CENTER_ASSERTION_DIST;
	// branch.setIsolatedPosition(basePosFront);
	// log.fine("Setting direction vector1: " + dir);
	// branch.setDirection(dir);
	Properties prop = branch.getProperties();
	if (prop == null){
	    prop = new Properties();
	}
	prop.setProperty("fit_score", ""+result.getBestValue());
	branch.setProperties(prop);
	// TODO : verify if below code is correct and if it is even necessary
	// LineShape line = computeHelix(cs.getPosition(), cs.getPosition().plus(cs.getX()), cs.getZ(), 0); // compute ideal helix for first base pair
	// 	Object3D outObject = branch.getOutObject();
	// 	Object3D inObject = branch.getInObject();
	// outObject.setPosition(fivePos); 
	// inObject.setPosition(line.getPosition2());
	assert (branch.isValid());
	// log.fine("Finished optimizeBranchDescriptor!");
	assert branch.getProperties() != null;
	return branch;
    }
    */

}
