package rnadesign.rnamodel;

import generaltools.AlgorithmFailureException;
import tools3d.objects3d.*;
import tools3d.Vector3D;
import java.util.*;
import java.util.logging.*;

public class SimpleConnectivityGenerator implements ConnectivityGenerator {

    private static Logger log = Logger.getLogger(PackageConstants.LOGFILE_DEFAULT);
    List<DBElementDescriptor> buildingBlocks; // total set of allowed building blocks
    List<List<Vector3D> > buildingBlocksHelixDirections; // for each building block store all z-directions of all helices
    List<List<Integer> > buildingBlockIndices;
    List<Object3D> vertices;
    LinkSet links;
    String topology = null;
    int[] vertexOrders;
    int numGenerations = 5;
    int helixLengthVariation = 4; // amound by which helices are shortened and lengthened
    int buildingBlockCountMax = 10; // number of non-equivalent building blocks
    int connectionCountMax = 10; // number of non-equivalent connections

    public SimpleConnectivityGenerator(List<Object3D> vertices, LinkSet links,
				       List<DBElementDescriptor> buildingBlocks,
				       List<List<Integer> > buildingBlockIndices,
				       int numGenerations) {
	assert vertices != null && vertices.size() > 0;
	assert links != null && links.size() > 0;
	assert buildingBlocks != null && buildingBlockIndices != null;
	assert buildingBlockIndices.size() == vertices.size();
	assert buildingBlocks.size() > 0;
	this.vertices = vertices;
	this.links = links;
	this.buildingBlocks = buildingBlocks;
	this.buildingBlockIndices = buildingBlockIndices;
	this.numGenerations = numGenerations;
	initVertexOrders();
    }
    
    public SimpleConnectivityGenerator(Object3D root, LinkSet linksOrig,
				       List<DBElementDescriptor> buildingBlocks,
				       List<List<Integer> > buildingBlockIndices,
				       int numGenerations) {	
	log.warning("Constructor method not save because order of elements below root not determined.");
	this.numGenerations = numGenerations;
	this.links =new SimpleLinkSet();
	for (int i = 0; i < linksOrig.size(); ++i) {
	    links.add(linksOrig.get(i));
	}
	links.removeBadLinks(root); // remove links that connect other objects
	this.vertices = new ArrayList<Object3D>();
	Object3DSet oset = new SimpleObject3DSet(root);
	for (int i = 0; i < oset.size(); ++i) {
	    if (links.getLinkOrder(oset.get(i)) > 0) {
		vertices.add(oset.get(i));
	    }
	}
	this.buildingBlocks = buildingBlocks;
	this.buildingBlockIndices = buildingBlockIndices;
	assert vertices != null; //  && vertices.size() > 0;
	assert links != null; //  && links.size() > 0;
	assert buildingBlocks != null && buildingBlockIndices != null;
	assert buildingBlockIndices.size() == vertices.size();
	assert buildingBlocks.size() > 0;
	initVertexOrders();
	initTopology();
    }

    public void setTopology(String s) {
	this.topology = s;
    }

    public String getTopology() {
	return topology;
    }

    public int getBuildingBlockCountMax() { return buildingBlockCountMax; }

    public int getConnectionCountMax() { return connectionCountMax; }

    /** returns order (number of edges/links) of n'th vertex */
    public int getVertexOrder(int n) {
	assert vertexOrders != null && vertexOrders.length == vertices.size();
	assert (n>= 0) && (n < vertexOrders.length);
	return vertexOrders[n];
    }

    public int getHelixLengthVariation() { return this.helixLengthVariation; }

    public int getNumGenerations() { return numGenerations; }

    public void setNumGenerations(int n) { this.numGenerations = n; }

    private void initTopology() {
	if (vertices == null || (vertices.size() < 1)) {
	    log.info("No target topology set!");
	    setTopology("");
	    return;
	}
	SignatureTranslatorCanonizer canonizer = new SignatureTranslatorCanonizer();
	Object3D root = new SimpleObject3D("root");
	for (Object3D vertex : vertices) {
	    root.insertChild(vertex);
	}
	Object3DLinkSetBundle bundle = new SimpleObject3DLinkSetBundle(root, links);
	try {
	    String topo = canonizer.generateCanonizedRepresentation(bundle);
	    log.info("Setting target topology to " + topo);
	    setTopology(topo);
	}
	catch (AlgorithmFailureException afe) {
	    log.severe("Error canonizing graph : " + bundle);
	}
	log.info("Set graph topology to " + getTopology());
    }

    private void initVertexOrders() {
	this.vertexOrders = new int[vertices.size()];
	for (int i = 0; i < vertexOrders.length; ++i) {
	    vertexOrders[i] = links.getLinkOrder(vertices.get(i));
	}
    }

    public void setHelixLengthVariation(int n) { this.helixLengthVariation = n; }

    public Iterator<GrowConnectivity> iterator() { 
	// SimpleConnectivityIterator iter = new SimpleConnectivityIterator(this); 
	SmallConnectivityIterator iter = new SmallConnectivityIterator(this); 
	assert iter.validate();
	return iter;
    }

    public List<DBElementDescriptor> getBuildingBlocks() { return buildingBlocks; }

    public List<List<Vector3D> > getBuildingBlocksHelixDirections() { return buildingBlocksHelixDirections; }

    /** Returns list of list, each sublist i containing indicies of building blocks (relative to getBuildingBlocks list) that are allowd at vertex i 
     * (index i with respect to getVertices */
    public List<List<Integer> > getBuildingBlockIndices() { return buildingBlockIndices; }

    public List<Object3D> getVertices() { return vertices; }

    public LinkSet getLinks() { return links; }

    public void setBuildingBlockCountMax(int n) { this.buildingBlockCountMax = n; }

    public void setConnectionCountMax(int n) { this.connectionCountMax = n; }
 
}
