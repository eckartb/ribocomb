package rnadesign.rnamodel;

import java.util.logging.Logger;

import graphtools.GraphBase;
import graphtools.GraphBaseOrderSet;
import graphtools.GraphBaseOrderSetTools;
import graphtools.IntegerListList;
import graphtools.SimpleGraphBaseOrderSet;
import tools3d.objects3d.LinkSet;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DLinkSetBundle;
import tools3d.objects3d.Object3DSet;
import tools3d.objects3d.SimpleLinkSet;
import tools3d.objects3d.SimpleObject3D;
import tools3d.objects3d.SimpleObject3DLinkSetBundle;
import tools3d.objects3d.SimpleObject3DSet;

/** generate tiling of RnaStrand objects and RnaStem3D objects 
 * tracing given set of objects 
 */
public class SimpleGridTiler extends AbstractGridTiler implements GridTiler {

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    private int offset = 2; // 2 * offset == residuePerLoop 

    private Object3D nucleotideDB;

    public SimpleGridTiler(Object3D nucleotideDB) {
	this.nucleotideDB = nucleotideDB;
    }

    /** returns set of strand that trace the given set of objects */
    public Object3DLinkSetBundle generateTiling(Object3D objects, LinkSet links, String baseName, char defaultSequenceChar,
						boolean onlyFirstPathMode) {
	log.fine("Starting generateTiling!");
	Object3DSet objectSet = new SimpleObject3DSet(objects);
	// first generate graph
	GraphBase graph = GridPathTools.generateGraph(objectSet, links);
	// obtain circular paths:
	GraphBaseOrderSet graphOrderSet = new SimpleGraphBaseOrderSet(graph, 4);
	IntegerListList circularPaths = GraphBaseOrderSetTools.getUniqueCyclicalPaths(graphOrderSet);
	Object3D resultObjects = new SimpleObject3D();
	Object3DSet strandSet = new SimpleObject3DSet();
	LinkSet newLinks = new SimpleLinkSet();
	resultObjects.setName("tilingroot");
	// generate a strand for each circular path
	log.fine("generating strand for each circular path out of " + circularPaths.size());
	for (int i = 0; i < circularPaths.size(); ++i) {
	    String strandName = baseName + ".strand" + (i+1); // like grid.1, grid.2 etc : different name for each strand
	    Object3DLinkSetBundle bundle = generateStrand(circularPaths.get(i), objectSet, strandName, defaultSequenceChar, 2*offset);
	    RnaStrand strand = (RnaStrand)(bundle.getObject3D());
	    log.fine("Strand " + strand.getName() + " with size " + strand.getResidueCount() + " " + strand.size()
			       + " generated.");
	    resultObjects.insertChild(strand);
	    strandSet.add(strand);
	    newLinks.merge(bundle.getLinks());
	}
	// generate a Stem3D for each link
	log.fine("generating Stem3D for each link");
	for (int i = 0; i < links.size(); ++i) {
	    String stemName = baseName + ".stem" + (i+1);
	    Object3DLinkSetBundle stemBundle = generateStem3D(circularPaths, objectSet, strandSet, links.get(i), stemName, nucleotideDB, offset);
	    if (stemBundle != null) {
		resultObjects.insertChild(stemBundle.getObject3D());
		newLinks.merge(stemBundle.getLinks());
	    }
	}
	// interpolate strands:
	for (int i = 0; i < strandSet.size(); ++i) {
	    Rna3DTools.interpolateNonStems((RnaStrand)(strandSet.get(i)), resultObjects);
	}

	// so far no new links except the new stems
	Object3DLinkSetBundle resultBundle = new SimpleObject3DLinkSetBundle(resultObjects, newLinks);
	log.fine("Finished generateTiling!");
	return resultBundle;
    }

}
