package rnadesign.rnamodel;

import java.util.Properties;
import tools3d.objects3d.Link;
import tools3d.objects3d.SimpleLink;
import tools3d.objects3d.SymmetryLinker;
import tools3d.objects3d.Object3D;

public class SimpleHelixConstraintLink extends SimpleLink implements HelixConstraintLink, SymmetryLinker {

    private int basePairMin;
    private int basePairMax;
    private double rms;
    private int symId1 = 0;
    private int symId2 = 0;

    /** constructor with added parameters. Linked objects must be from class BranchDescriptor3D. */
    public SimpleHelixConstraintLink(BranchDescriptor3D o1, BranchDescriptor3D o2, int basePairMin, int basePairMax, double rms) {
	super(o1, o2);
        setName("hclink");
	this.basePairMin = basePairMin;
	this.basePairMax = basePairMax;
	this.rms = rms;
	assert getName() != null;
    }

    public Object clone() {
	SimpleHelixConstraintLink l = new SimpleHelixConstraintLink((BranchDescriptor3D)getObj1(),
								    (BranchDescriptor3D)getObj2(),
								    getBasePairMin(), getBasePairMax(), getRms());
        assert(l != null);
	// assert (getName() != null);
	if (getName() != null) {
	    l.setName(new String(getName()));
	} else {
	    l.setName(null);
	}
	l.setTypeName(new String(getTypeName()));
	l.setSymId1(this.symId1);
	l.setSymId2(this.symId2);
	if (getProperties() != null) {
	    l.setProperties(getProperties());
	}
	return l;
    }

    public Object clone(Object3D newObj1, Object3D newObj2) {
	SimpleHelixConstraintLink l = (SimpleHelixConstraintLink)clone();
	l.setObj1(newObj1);
	l.setObj2(newObj2);
	return l;
    }

    public double getRms() { return rms; }

    public void setRms(double rms) { this.rms = rms; }

    public int getBasePairMin() { return basePairMin; }

    public int getBasePairMax() { return basePairMax; }

    public int getSymId1() { return symId1; }

    public int getSymId2() { return symId2; }

    public void setBasePairMin(int n) { basePairMin = n; }

    public void setBasePairMax(int n) { basePairMax = n; }
    
    public String getTypeName() {
    	return "SimpleHelixConstraintLink";
    }

    public void setSymId1(int id) { symId1 = id; }

    public void setSymId2(int id) { symId2 = id; }

}
