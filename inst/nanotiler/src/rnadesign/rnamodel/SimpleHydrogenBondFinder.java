package rnadesign.rnamodel;

import rnasecondary.*;

import tools3d.objects3d.*;
import tools3d.objects3d.LinkSet;

public class SimpleHydrogenBondFinder implements HydrogenBondFinder {

    double scoreCutoff = 0.5;
    HydrogenBondScorer hbScorer = new DistanceHydrogenBondScorer(); // TODO improve
    HydrogenPositionEstimator hbEstimator = new NucleotideHydrogenPositionEstimator();

    public LinkSet find(Nucleotide3D n1, Nucleotide3D n2) throws RnaModelException {
	Object3DSet hDonors1 = NucleotideTools.findHDonors(n1);
	assert(hDonors1.size() > 0);
	Object3DSet hAcceptors1 = NucleotideTools.findHAcceptors(n1);
	Object3DSet hDonors2 = NucleotideTools.findHDonors(n2);
	assert(hAcceptors1.size() > 0);
	assert(hDonors2.size() > 0);
	Object3DSet hAcceptors2 = NucleotideTools.findHAcceptors(n2);
	assert(hAcceptors2.size() > 0);
	LinkSet hBonds12 = HydrogenBondTools.findHydrogenBonds(hDonors1, hAcceptors2, hbScorer, hbEstimator, scoreCutoff);
	LinkSet hBonds21 = HydrogenBondTools.findHydrogenBonds(hDonors2, hAcceptors1, hbScorer, hbEstimator, scoreCutoff);
	LinkSet hBonds = hBonds12;
	hBonds.merge(hBonds21);
	// System.out.println("Number of found hydrogen bonds: " + hBonds12.size() + " " + hBonds21.size() + " " + hBonds.size());
	return hBonds;
    }

}