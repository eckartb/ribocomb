package rnadesign.rnamodel;

import tools3d.objects3d.SimpleObject3D;

/** 
 * This interface describes the concept of a general molecule.
 * A molecule is a container of atoms.
 */
public class SimpleMolecule3D extends SimpleObject3D {

    /** returns n'th atom. TODO: not clean implementation yet! */
    public Atom3D getAtom(int n) { 
	return (Atom3D)getChild(n);
    }

    /** returns number of defined atoms. TODO: not clean implementation yet*/
    public int getAtomCount() { return size(); }

}
