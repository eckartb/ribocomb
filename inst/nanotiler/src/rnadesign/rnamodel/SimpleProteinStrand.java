/** This class implements concept of an RNA builing block.
 *  Special requiremnt: the number of child Object3D's must equal the number of RNAConnections
 * 
 */

package rnadesign.rnamodel;

import java.util.List;
import java.util.ArrayList;
import sequence.Alphabet;
import sequence.LetterSymbol;
import sequence.ProteinTools;
import sequence.Residue;
import sequence.Sequence;
import sequence.SimpleResidue;
import sequence.SimpleSequence;
import sequence.UnknownSymbolException;
import tools3d.Vector3D;
import tools3d.Matrix3DTools;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.SimpleObject3D;

public class SimpleProteinStrand extends AbstractBiopolymer implements ProteinStrand {

    public static final String CLASS_NAME = "ProteinStrand";
    private static Alphabet alphabet = ProteinTools.PROTEIN_ALPHABET;
    private double weight = 1.0;
    // private Sequence sequence;
	
    /** default constructor */
    public SimpleProteinStrand() {
	super();
    }

    /** default constructor */
    public SimpleProteinStrand(Alphabet alphabet) {
	super();
	this.alphabet = alphabet;
    }

    /** default constructor */
//     public SimpleProteinStrand(String s, String name) throws UnknownSymbolException {
// 	super();
// 	setName(name);
// 	String seqName = getName() + ".s";
// 	Alphabet alphabet = ProteinTools.PROTEIN_ALPHABET;
// 	this.sequence = new SimpleSequence(s, seqName, alphabet);
// 	this.sequence.setParent(this);
//     }

    public Object cloneDeep(int startPos, int stopPos) {
	assert false; // TODO not yet implemented
	return null;
    }

    /** returns alphabet. */
    public Alphabet getAlphabet() { return this.alphabet; }

    /** returns n'th atom */
    public Atom3D getAtom(int n) {
	assert false;
	log.severe("getAtom method not yet implemented!");
	return null;
    }

    /** returns total number of atoms. TODO: use caching */
    public int getAtomCount() {
	int sum = 0;
	int resCount = getResidueCount();
	for (int i = 0; i < resCount; ++i) {
	    sum += getResidue3D(i).getAtomCount();
	}
	return sum;
    }

    public String getClassName() { return CLASS_NAME; }

    /** returns order of covalent bond or -1 if not connected */
    public int getCovalentBondOrder(Atom3D atom1, Atom3D atom2) {
	assert false;
	return -1;
    }

    /** returns list of atoms that are covalently bonded with this atom. TODO: not yet implemented */
    public List<Atom3D> getCovalentlyBondedAtoms(Atom3D atom) {
	assert false;
	return null;
    }

    public Object getParentObject() { return getParent(); }

    // public Vector3D getResidueCount() {
    // return size(); // TODO : fix inaccurate implementation
    // }

    /** returns central position of n'th residue */
    public Vector3D getResiduePosition(int n) throws IndexOutOfBoundsException {
	if (n < getResidueCount()) {
	    Residue3D residue = getResidue3D(n);
	    return residue.getPosition();
	}
	// no residue data available, assume dum model: sequences grow in z-direction in standard orientation:
	Vector3D pos = new Vector3D();
	pos.copy(getPosition());
	int nn = getResidueCount();
	int nHalf = nn / 2; // 
	double z = pos.getZ() + ((n - nHalf) * RnaPhysicalProperties.RNA_SINGLESTRAND_LENGTH_PER_RESIDUE);
	pos.setZ(z);
	return Matrix3DTools.rotate(pos, getRotationAxis(), getRotationAngle());
    }

//     public void addSequence(Sequence s) {
// 	sequence = s;
//     }

    /** Needed to implement sequence interface */
    public void addResidue(Residue residue) {
	if (residue instanceof Nucleotide3D) {
	    insertChild((Nucleotide3D)residue);
	}
	else {
	    assert false;
	    throw new RuntimeException("SimpleRnaStrand.addResidue: added residue must be of type Nucleotid3D");
	}
    }


    /** append polymer. TODO : not yet implemented */
    public void append(BioPolymer polymer) { 
	assert false;
    }

    public double getWeight() { return this.weight; }

    /** append polymer at beginning. TODO : not yet implemented */
    public void prepend(BioPolymer polymer) { 
	assert false;
    }

    /** append polymer. TODO : not yet implemented */
    public void appendAndDelete(BioPolymer polymer) { 
	assert false;
    }

    /** append polymer at beginning. TODO : not yet implemented */
    public void prependAndDelete(BioPolymer polymer) { 
	assert false;
    }

    public void clear() {
	super.clear();
	// sequence = null; // TODO new SimpleSequence();
    }

    /** returns residue whose assigned number is equal to n.
     * TODO : implement faster */
    public Residue3D getResidueByAssignedNumber(int n) {
	for (int i = 0; i < size(); ++i) {
	    Object3D child = getChild(i);
	    if (child instanceof Residue3D) {
		Residue3D residue = (Residue3D)child;
		if (residue.getAssignedNumber() == n) {
		    return residue;
		}
	    }
	}
	return null; // not found
    }
   
    /** returns number of children that are of type Nucleotide3D.
     * TODO : implement correctly!
     */
    public int getResidueCount() { return size(); }

    /** returns child of type Nucleotide3D.
     * TODO implement correctly
     */
    public Residue3D getResidue3D(int n) { return (AminoAcid3D)(getChild(n)); }

    /** returns child of type Nucleotide3D.
     * TODO implement correctly
     */
    public Residue getResidue(int n) { return (AminoAcid3D)(getChild(n)); }

    /** returns residue whose assigned number is equal to n.
     * TODO : implement faster */
    public Residue3D getResidueByPdbId(int n) {
	String ns = new Integer(n).toString();
	int resCount = getResidueCount();
	for (int i = 0; i < resCount; ++i) {
	    Residue3D residue = getResidue3D(i);
	    if (ns.equals(residue.getProperty(PackageConstants.PDB_RESIDUE_ID))) {
		return residue;
	    }
	}
	return null; // not found
    }

    public String getShapeName() { return "RnaStrand"; }

    /** returns n'th sequence */
//     public Sequence getSequence(int n) throws IndexOutOfBoundsException {
// 	return sequence;
//     }
	   

//     public int getSequenceCount() {
// 	return 1;
//     }

    /** if AminoAcid3D was inserted, add symbol to sequence */
    public void insertChild(Object3D child) {
	assert child instanceof AminoAcid3D;
	super.insertChild(child);
// 	if (child instanceof AminoAcid3D) {
// 	    AminoAcid3D nuc = (AminoAcid3D)child;
// 	    LetterSymbol symbol = nuc.getSymbol();
// 	    int pos = getResidueCount();
// 	    Residue residue = new SimpleResidue(symbol, pos);
// 	    sequence.addResidue(residue);
// 	}
    }

    // public void setSequence(Sequence sequence) { this.sequence = sequence; }

    public void setParent(Object obj) {
	if (obj instanceof Object3D) {
	    super.setParent((Object3D)obj);
	}
	assert false;
	throw new RuntimeException("Internal error: only object3d objects can be added here.");
    }

    public void setWeight(double weight) { this.weight = weight; }

    /** rotates part of molecule around bond pointing from atom1 to atom2, using angle */
    public void rotateAroundBackboneAngle(int residue, int angleId, double angle) {
	assert false; // TODO : not yet implemented
    }

    /** Shortens current sequence to have length position (residues 0..position-1), and returns new sequence with positions position...length-1.
     * No cloning of residues is being done, just reorganizing. */
    public BioPolymer split(int position, String newName) {
	int oldLength = getResidueCount();
	String oldSequence = sequenceString();
	SimpleProteinStrand newStrand = new SimpleProteinStrand(getAlphabet());
	newStrand.setName(newName);
	split(position, newStrand);
	assert (getResidueCount() + newStrand.getResidueCount()) == oldLength;
	assert oldSequence.equals(sequenceString() + newStrand.sequenceString());
	return newStrand;
    }

    /** standard output method */
    public String toString() {
	String result = "(ProteinStrand " + toStringBody() + sequenceString();
	result = result + " )";
	return result;
    }
    
}
