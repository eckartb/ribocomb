package rnadesign.rnamodel;

import rnasecondary.Stem;
import tools3d.Vector3D;
import tools3d.objects3d.SimpleObject3D;

/** interface that represents 3D object that is an RNA stem.
 *    The idea is, that this class is only used as a supplement,
 *    to improve visualization and structure optimization.
 *    The basic information is stored in a set of RNA strands
 *    and a set of residue interactions (base pairs)
 */
public class SimpleRnaStem3D extends SimpleObject3D implements RnaStem3D {

    public int strand1_end5_index = 0;
    public int strand1_end3_index = 1;
    public int strand2_end5_index = 2;
    public int strand2_end3_index = 3;

    public static final String STRAND1_END5_NAME = "STRAND1_END5";
    public static final String STRAND1_END3_NAME = "STRAND1_END3";
    public static final String STRAND2_END5_NAME = "STRAND2_END5";
    public static final String STRAND2_END3_NAME = "STRAND2_END3";

    public static String CLASS_NAME = "RnaStem3D";

    private Stem stemInfo;

    /** used for clone method */
    public SimpleRnaStem3D() { }

    public SimpleRnaStem3D(Stem stem) {
	this.stemInfo = stem;
	// maybe generate the "strand1End5Pos" etc information below somehow
    }

    public SimpleRnaStem3D(Stem stem,
			   Vector3D pos) {
// 			   Vector3D strand1End5Pos,
// 			   Vector3D strand1End3Pos,
// 			   Vector3D strand2End5Pos, // TODO : not entirely sure, if this is 5' or 3' end of strand 2 ... :-(
// 			   Vector3D strand2End3Pos) {
	this.stemInfo = stem;
	this.setPosition(pos);

// 	Vector3D p = strand1End5Pos;
// 	p.add(strand1End3Pos);
// 	p.add(strand2End5Pos);
// 	p.add(strand2End3Pos);
// 	p.scale(0.25);

// 	Object3D strand1End5Child = new SimpleObject3D();
// 	strand1End5Child.setName(STRAND1_END5_NAME);
// 	strand1End5Child.setPosition(strand1End5Pos);
// 	insertChild(strand1End5Child);
// 	Object3D strand1End3Child = new SimpleObject3D();
// 	strand1End3Child.setName(STRAND1_END3_NAME);
// 	strand1End3Child.setPosition(strand1End3Pos);
// 	insertChild(strand1End3Child);
// 	Object3D strand2End5Child = new SimpleObject3D();
// 	strand2End5Child.setName(STRAND2_END3_NAME);
// 	strand2End5Child.setPosition(strand1End5Pos);
// 	insertChild(strand2End5Child);
// 	Object3D strand2End3Child = new SimpleObject3D();
// 	strand2End3Child.setName(STRAND2_END3_NAME);
// 	strand2End3Child.setPosition(strand2End3Pos);
// 	insertChild(strand2End3Child);
    }

    /** "deep" clone not including parent and children: every object is newly generated!
     */
    public Object cloneDeepThis() {
	SimpleRnaStem3D obj = new SimpleRnaStem3D();
	obj.copyDeepThisCore(this);
	obj.strand1_end5_index = this.strand1_end5_index;
	obj.strand1_end3_index = this.strand1_end3_index;
	obj.strand2_end5_index = this.strand2_end5_index;
	obj.strand2_end3_index = this.strand2_end3_index;
	obj.stemInfo = (Stem)(this.stemInfo.clone());
	return obj; // TODO : test if ok!
    }


    /** generates stem with identities of strand 1 and 2 reversed */
    public RnaStem3D generateReverseStem() {
	return new SimpleRnaStem3D(this.stemInfo.generateReverseStem(), this.getPosition());
    }

    /** returns length of stem (number of base pairs) */
    public int getLength() { return stemInfo.size(); }

    public int getStrand1End5Index() { return strand1_end5_index; }

    public int getStrand1End3Index()  { return strand1_end3_index; }

    public int getStrand2End5Index( )  { return strand2_end5_index; }

    public int getStrand2End3Index( )  { return strand2_end3_index; }

    /** average between strand1End5 and strand2End3 */
    public Vector3D getCenter1Position() {
	Vector3D sum = getChild(getStrand1End5Index()).getPosition();
	sum.add(getChild(getStrand2End3Index()).getPosition());
	sum.scale(0.5);
	return sum;
    }

    public String getClassName() { return CLASS_NAME; }

    /** average between strand2End5 and strand1End3 */
    public Vector3D getCenter2Position() {
	Vector3D sum = getChild(getStrand1End3Index()).getPosition();
	sum.add(getChild(getStrand2End5Index()).getPosition());
	sum.scale(0.5);
	return sum;
    }
    
    public NucleotideStrand getStrand1() { 
	NucleotideStrand strand = (NucleotideStrand)(stemInfo.getSequence1()); // .getParentObject());
	return strand;
    }

    public NucleotideStrand getStrand2() {
	NucleotideStrand strand = (NucleotideStrand)(stemInfo.getSequence2()); // .getParent());
	return strand;
    }

    /** returns n'th residue of strand 1 of stem (counting wrt to stem, 0,.., n-1) */
    public Nucleotide3D getStartResidue(int n) {
	return (Nucleotide3D)(getStrand1().getResidue(stemInfo.getStartPos(n).getPos()));
    }

    /** returns n'th residue of strand 2 of stem (counting wrt to stem, 0, ..., n-1) */
    public Nucleotide3D getStopResidue(int n) {
	return (Nucleotide3D)(getStrand2().getResidue(stemInfo.getStopPos(n).getPos()));
    }

    public Stem getStemInfo() { 
	return stemInfo;
    }

    public void setStemInfo(Stem stem) {
	this.stemInfo = stem;
    }

    public void setStrand1End5Index(int n) { strand1_end5_index = n; }

    public void setStrand1End3Index(int n) { strand1_end3_index = n; }

    public void setStrand2End5Index(int n) { strand2_end5_index = n; }

    public void setStrand2End3Index(int n) { strand2_end3_index = n; }

    public String toString() {
	String result = new String();
	result = "(" + getClassName() + " "  + toStringBody() + " )";
	return result;
    }

    public String toStringBody() {
	String result = new String();
	assert stemInfo != null;
	assert getStrand1() != null;
	assert getStrand2() != null;
	result = getName() + " " + getStrand1().getName() + " " + getStrand2().getName() + " ";
	result = result + getRelativePosition().toString(); // write relative position vector
	result = result + " " + stemInfo.toString();
// 	result = result + " (children " + size();
// 	for (int i = 0; i < size(); ++i) {
// 	    result = result + " " + getChild(i).toString();
// 	}
// 	result = result + " ) ";
	return result;
    }

}
