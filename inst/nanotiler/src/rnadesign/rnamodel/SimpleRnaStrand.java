/** This class implements concept of an RNA builing block.
 *  Special requiremnt: the number of child Object3D's must equal the number of RNAConnections
 * 
 */
package rnadesign.rnamodel;

import java.util.logging.*;
import java.util.ArrayList;
import java.util.List;
import sequence.Alphabet;
import sequence.DnaTools;
import sequence.LetterSymbol;
import sequence.Residue;
import sequence.Sequence;
import sequence.SimpleResidue;
import sequence.SimpleSequence;
import sequence.UnknownSymbolException;
import tools3d.Matrix3DTools;
import tools3d.Vector3D;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DSet;
import tools3d.objects3d.SimpleObject3DSet;
import tools3d.objects3d.Object3DTools;
import tools3d.objects3d.SimpleObject3D;

import static rnadesign.rnamodel.PackageConstants.*;

public class SimpleRnaStrand extends AbstractBiopolymer implements RnaStrand {

    public static final char DEFAULT_CHAIN_CHARACTER = 'A';

    private Alphabet alphabet = DnaTools.AMBIGUOUS_RNA_ALPHABET;	
    private double weight = 1.0;
    public static final String CLASS_NAME = "RnaStrand";
    private Logger log = Logger.getLogger(LOGGER_NAME);

    // private Sequence sequence;

    private char chainCharacter = DEFAULT_CHAIN_CHARACTER;
	
    /** default constructor */
    public SimpleRnaStrand() {
	super();
 	this.setName("unnamed");
    }

    /** default constructor */
    public SimpleRnaStrand(Alphabet alphabet) {
	super();
	this.alphabet = alphabet;

    }


    /** Needed to implement sequence interface */
    public void addResidue(Residue residue) {
	if (residue instanceof Nucleotide3D) {
	    insertChild((Nucleotide3D)residue);
	}
	else {
	    assert false;
	    throw new RuntimeException("SimpleRnaStrand.addResidue: added residue must be of type Nucleotid3D");
	}
    }

    /** returns n'th atom */
    public Atom3D getAtom(int n) {
	assert false;
	log.severe("getAtom method not yet implemented!");
	return null;
    }

    /** returns total number of atoms. TODO: use caching */
    public int getAtomCount() {
	int sum = 0;
	int resCount = getResidueCount();
	for (int i = 0; i < resCount; ++i) {
	    sum += getResidue3D(i).getAtomCount();
	}
	return sum;
    }

    /** Returns order of covalent bond or zero if not bonded. TODO: implement using links and not distances */
    public int getCovalentBondOrder(Atom3D atom1, Atom3D atom2) throws RnaModelException {
	Nucleotide3D res1 = (Nucleotide3D)(atom1.getParent());
	Nucleotide3D res2 = (Nucleotide3D)(atom2.getParent());
	assert res1.getParent() == this;
	assert res2.getParent() == this;
	if (res1 == res2) { // same residue!
	    boolean flag = NucleotideTools.isCovalentlyBonded(res1, atom1.getName(), atom2.getName());
	    if (!flag) {
		return 0;
	    }
	    else {
		return 1;
	    }
	}
	else {
	    int posDiff = res2.getPos() - res1.getPos();
	    assert posDiff != 0;
	    if (Math.abs(posDiff) > 1) {
		return 0;
	    }
	    if ((posDiff == 1) && ((atom1.getName().equals("O3*") || (atom1.getName().equals("O3'"))) && atom2.getName().equals("P"))) {
		return 1;
	    }
	    if ((posDiff == -1) && ((atom2.getName().equals("O3*") || (atom2.getName().equals("O3'"))) && atom1.getName().equals("P"))) {
		return 1;
	    }
	}
	return 0; // no connection found possible
    }

    /** Returns order of covalent bond or zero if not bonded. TODO: implement using links and not distances */
    public Object3DSet collectRotationSet(Atom3D atom1, Atom3D atom2) throws RnaModelException {
	Nucleotide3D res1 = (Nucleotide3D)(atom1.getParent());
	Nucleotide3D res2 = (Nucleotide3D)(atom2.getParent());
	assert res1.getParent() == this;
	assert res2.getParent() == this;
	Object3DSet result = new SimpleObject3DSet();
	if (res1 == res2) { // same residue!
	    result = MoleculeTools.collectRotationSet(res1, atom1, atom2);
	    // add all downstream atoms:
	    for (int i = res1.getPos()+1; i < getResidueCount(); ++i) {
		result.merge(Object3DTools.collectByClassName(getResidue3D(i), "Atom3D"));
	    }
	}
	else {
	    log.severe("Sorry, no backbone torsion angle rotations are currently only allowed for atoms of same residue!");
	    assert false;
	}
	return result;
    }


    /** returns list of atoms that are covalently bonded with this atom */
    public List<Atom3D> getCovalentlyBondedAtoms(Atom3D atom) {
	// log.info("Starting getCovalentlyBondedAtoms");
	List<Atom3D> resultAtoms = new ArrayList<Atom3D>();
	Nucleotide3D res = (Nucleotide3D)(atom.getParent());
	int pos = res.getPos();
	int posStart = pos - 1;
	if (posStart < 0) {
	    posStart = 0;
	}
	int posEnd = pos + 1;
	if (posEnd >= getResidueCount()) {
	    posEnd = getResidueCount()-1;
	}
	for (int i = posStart; i <= posEnd; ++i) {
	    Nucleotide3D resOther = (Nucleotide3D)(getChild(i));
	    for (int j = 0; j < resOther.size(); ++j) {
		Atom3D atomOther = (Atom3D)(resOther.getChild(j));
		try {
		    if ((atom != atomOther) && (getCovalentBondOrder(atom, atomOther) > 0)) {
			resultAtoms.add(atomOther);
		    }
		}
		catch (RnaModelException rne) {
		    log.fine("RNA model exception in getCovelentlyBondedAtoms " + atom.getName());
		}
	    }
	}
	// log.info("Finished getCovalentlyBondedAtom: " + atom + " " + resultAtoms.size());
	assert resultAtoms.size() < 6;
	return resultAtoms;
    }

    public double getWeight() { return this.weight; }

    /** returns highest index used in residue names. Assumes naming convention like G10, A11, C12, G13 etc */
    public int findHighestNameIndex() {
	int resCount = getResidueCount();
	int highest = 0;
	for (int i = 0; i < resCount; ++i) {
	    try {
		int newNum = Integer.parseInt(getChild(i).getName().substring(1));
		if (newNum > highest) {
		    highest = newNum;
		}
	    }
	    catch (NumberFormatException e) {
		log.fine("Ignoring name " + getChild(i).getName() + " for finding highest residue index.");
	    }
	}
	return highest;
    }

    /** appends other polymer to end. Adjusts names of nucleotides. Assumes name like G10, A11 etc. */
    public void append(BioPolymer other) {
	assert other instanceof NucleotideStrand;
	assert other != this;
	log.fine("starting SimpleRnaStrand.append");
	NucleotideStrand strand = (NucleotideStrand)other;
	int currIndex = findHighestNameIndex() + 1;
	for (int i = 0; i < strand.getResidueCount(); ++i) {
	    Residue3D newRes = strand.getResidue3D(i);
	    String newName = newRes.getName();
	    newName = "" + newName.charAt(0) + currIndex;
	    newRes.setName(newName);
	    assert newRes.size() > 0; // must be more than one atom
	    insertChild(newRes);
	    currIndex++;
	}
	log.fine("finished SimpleRnaStrand.append");
    }

    /** appends other polymer to end. Adjusts names of nucleotides. Assumes name like G10, A11 etc. */
    public void appendAndDelete(BioPolymer other) {
	assert other instanceof NucleotideStrand;
	assert other != this;
	log.fine("starting SimpleRnaStrand.append " + other.getFullName() + " to " + getFullName());
	NucleotideStrand strand = (NucleotideStrand)other;
	int currIndex = findHighestNameIndex() + 1;
// 	StringBuffer buf = new StringBuffer();	
// 	for (int i = 0; i < size(); ++i) {
// 	    buf.append(getChild(i).getName() + " ");
// 	}
// 	log.info("Child names of current strand before appending: " + buf.toString());
	for (int i = 0; i < strand.getResidueCount(); ++i) {
	    Residue3D newRes = strand.getResidue3D(i);
	    String newName = newRes.getName();
	    newName = "" + newName.charAt(0) + currIndex;
	    log.finest("Trying to insert new residue with name " + newName);
	    newRes.setName(newName);
	    assert newRes.size() > 0; // must be more than one atom
	    insertChild(newRes);
	    currIndex++;
	}
	if (other.getParent() != null) {
	    other.getParent().removeChild(other);
	}
	log.fine("finished SimpleRnaStrand.append");
    }

    /** appends other polymer to end */
    public void prepend(BioPolymer other) {
	assert other instanceof NucleotideStrand;
	log.fine("started SimpleRnaStrand.prepend");
	NucleotideStrand strand = (NucleotideStrand)other;
	int currIndex = strand.findHighestNameIndex() + 1;
	for (int i = 0; i < getResidueCount(); ++i) {
	    String newName = getResidue3D(i).getName();
	    newName = "" + newName.charAt(0) + currIndex;
	    assert getResidue3D(i).size() > 0; // must be more than one atom
	    getResidue3D(i).setName(newName);
	    currIndex++;
	}
	for (int i = strand.getResidueCount()-1; i >= 0; --i) {
	    insertChild(strand.getResidue3D(i), 0); // insert at beginning
	}
	log.fine("finished SimpleRnaStrand.prepend");
    }

    /** appends other polymer to end */
    public void prependAndDelete(BioPolymer other) {
	assert other instanceof NucleotideStrand;
	log.fine("started SimpleRnaStrand.prepend");
	NucleotideStrand strand = (NucleotideStrand)other;
	int currIndex = strand.findHighestNameIndex() + 1;
	for (int i = 0; i < getResidueCount(); ++i) {
	    String newName = getResidue3D(i).getName();
	    newName = "" + newName.charAt(0) + currIndex;
	    assert getResidue3D(i).size() > 0; // must be more than one atom
	    getResidue3D(i).setName(newName);
	    currIndex++;
	}
	for (int i = strand.getResidueCount()-1; i >= 0; --i) {
	    insertChild(strand.getResidue3D(i), 0); // insert at beginning
	}
	if (other.getParent() != null) {
	    other.getParent().removeChild(other);
	}
	log.fine("finished SimpleRnaStrand.prepend");
    }

    /** "deep" clone not including parent and children: every object is newly generated!
     */
    public Object cloneDeepThis() {
	SimpleRnaStrand obj = new SimpleRnaStrand();
	obj.copyDeepThisCore(this);
	// obj.sequence = (Sequence)(sequence.clone());
	obj.chainCharacter = chainCharacter;
	obj.alphabet = alphabet;
	obj.weight = weight;
	return obj; // TODO : test if ok!
    }

    /** returns cloned subsequence from startPos (inclusive) to stopPos (exclusive) */
    public Object cloneDeep(int startPos, int stopPos) {
	SimpleRnaStrand strand = (SimpleRnaStrand) (cloneDeepThis());
	for (int i = startPos; i < stopPos; ++i) {
	    strand.insertChild((Object3D)(getChild(i).cloneDeep()));
	}
	return strand;
// 	for (int i = strand.getResidueCount()-1; i >= stopPos; --i) {
// 	    strand.removeChild(i); // remove i-th residue
// 	}
// 	for (int i = startPos-1; i >= 0; --i) {
// 	    strand.removeChild(i); // remove i-th residue
// 	}
// 	assert strand.getResidueCount() == (stopPos - startPos);
// 	return strand;
    }

    /** "deep" clone including children: every object is newly generated!
     * this is a specialized version such that each child Nucleotide3D
     * points to the same sequence object. */
//     public Object cloneDeep() {
// 	SimpleRnaStrand newObj = (SimpleRnaStrand)(cloneDeepThis());
// 	for (int i = 0; i < size(); ++i) {
// 	    Object3D childObj = (Object3D)(getChild(i).cloneDeep());
// 	    // should already be done by insertChild:
// // 	    if (childObj instanceof Nucleotide3D) {
// // 		Nucleotide3D nuc = (Nucleotide3D)childObj;
// // 		nuc.setSequence(newObj.getSequence()); // all point to same sequence
// // 		newObj.insertChild(nuc);
// // 	    }
// // 	    else {
// 	    newObj.insertChild(childObj);
// 		// }
// 	}
// 	return newObj;
//     }

    /** returns for example DEFAULT_CHAIN_CHARACTER for chain A (corresponds to PDB file entry) */
    public char getChainCharacter() { return this.chainCharacter; }

    public String getClassName() { return CLASS_NAME; }

    /** returns central position of n'th residue */
    public Vector3D getResiduePosition(int n) throws IndexOutOfBoundsException {
	if (n < getResidueCount()) {
	    Residue3D residue = getResidue3D(n);
	    return residue.getPosition();
	}
	// no residue data available, assume dum model: sequences grow in z-direction in standard orientation:
	Vector3D pos = new Vector3D();
	pos.copy(getPosition());
	int nn = size(); // getSequence().size();
	int nHalf = nn / 2; // 
	double z = pos.getZ() + ((n - nHalf) * RnaPhysicalProperties.RNA_SINGLESTRAND_LENGTH_PER_RESIDUE);
	pos.setZ(z);
	return Matrix3DTools.rotate(pos, getRotationAxis(), getRotationAngle());
    }

    public void clear() {
	super.clear();
	chainCharacter = DEFAULT_CHAIN_CHARACTER;
	// sequence.clear(); // keeps sequence name and alphabet // new SimpleSequence(); // null; // TODO new SimpleSequence();
    }
   
    public Alphabet getAlphabet() { return this.alphabet; }

    public Object getParentObject() { return getParent(); }

    /** returns number of children that are of type Nucleotide3D.
     * TODO : implement correctly!
     */
    public int getResidueCount() { 
	return size(); 
    }

    /** returns child of type Nucleotide3D.
     * TODO implement correctly
     */
    public Residue3D getResidue3D(int n) { return (Nucleotide3D)(getChild(n)); }

    /** used to implement Sequence interface */
    public Residue getResidue(int n) { return getResidue3D(n); }

    /** returns residue whose assigned number is equal to n.
     * TODO : implement faster */
    public Residue3D getResidueByAssignedNumber(int n) {
	int resCount = getResidueCount();
	for (int i = 0; i < resCount; ++i) {
	    Residue3D residue = getResidue3D(i);
	    if (residue.getAssignedNumber() == n) {
		return residue;
	    }
	}
	return null; // not found
    }

    /** returns residue whose assigned number is equal to n.
     * TODO : implement faster */
    public Residue3D getResidueByPdbId(int n) {
	String ns = new Integer(n).toString();
	int resCount = getResidueCount();
	for (int i = 0; i < resCount; ++i) {
	    Residue3D residue = getResidue3D(i);
	    if (ns.equals(residue.getProperty(PDB_RESIDUE_ID))) {
		return residue;
	    }
	}
	return null; // not found
    }

    public String getShapeName() { return "RnaStrand"; }

    /** if Nucleotide3D was inserted, add symbol to sequence */
    public void insertChild(Object3D child) {
	assert child instanceof Nucleotide3D;
	super.insertChild(child);
// 	if (child instanceof Nucleotide3D) {
// 	    Nucleotide3D nuc = (Nucleotide3D)child;
// 	    LetterSymbol symbol = nuc.getSymbol();
// 	    // int pos = sequence.size(); // position equals to number of residues defined so far in sequence
// 	    // Residue residue = new SimpleResidue(symbol, sequence, pos);
// 	    sequence.addResidue(nuc);
// 	    // nuc.setPos(pos); // already done by addResidue method
// 	    // nuc.setSequence(sequence);
// 	    assert nuc.getSequence() == sequence;
// 	    assert nuc.getPos() == (sequence.size() - 1);
// 	}
    }

    /** fast and approximate check if two objects describe the same content. Can be true even after cloning.
     * TODO : improve */
    public boolean isProbablyEqual(Sequence seq) {
	if (seq instanceof Object3D) {
	    return isProbablyEqual((Object3D)seq);
	}
	return false;
    }

    /** fast and approximate check if two objects describe the same content. Can be true even after cloning.
     * TODO : improve */
    public boolean isProbablyEqual(Object3D obj) {
	if (obj == this) {
	    return true;
	}
	if (!(obj.getClassName().equals(getClassName()) && obj.getName().equals(getName())) ) {
	    return false;
	}
	double dist = distance(obj);
	if (dist > 0.001) {
	    return false; // allow some numerical inaccuracy
	}
	if (size() != obj.size()) {
	    // must have same number of children
	    return false;
	}
	for (int i = 0; i < size(); ++i) {
	    // change: not recursive!
	    if (!(isProbablyEqual(getChild(i), obj.getChild(i)))) {
		return false;
	    }
	}
	return true;
    }

    private boolean isProbablyEqual(Object3D obj1, Object3D obj2) {
	if ((obj1 instanceof Residue3D) && (obj2 instanceof Residue3D)) {
	    Residue3D res1 = (Residue3D)obj1;
	    Residue3D res2 = (Residue3D)obj2;
	    if (!(res1.getSymbol().equals(res2.getSymbol()))) {
		return false; // different sequence
	    }
	    if (res1.size() != res2.size()) {
		return false;
	    }
	    return res1.distance(res2) < 0.001; // distance cutoff
	}
	return obj1.isProbablyEqual(obj2);
    }

    /** rotates part of molecule around bond pointing from atom1 to atom2, using angle */
    public void rotateAroundBackboneAngle(int residue, int angleId, double angle) throws RnaModelException {
	String atomName1 = NucleotideTools.getBackboneAngleAtomName1(angleId);
	String atomName2 = NucleotideTools.getBackboneAngleAtomName2(angleId);
	Atom3D atom1 = (Atom3D)((getResidue3D(residue)).getChild(atomName1));
	Atom3D atom2 = (Atom3D)((getResidue3D(residue)).getChild(atomName2));
	if ((atom1 == null) || (atom2 == null)) {
	    throw new RnaModelException("Could not find atoms with names " + atomName1 + " " + atomName2
					+ " for residue " + (residue+1));
	}
	int bondOrder = getCovalentBondOrder(atom1, atom2); 
	if (bondOrder == 0) {
	    log.info("Trying to rotate around two atoms that are not bonded: " + atom1 + " " + atom2);
	    return;
	}
	// 	MoleculeTools.rotateAroundBond(this, atom1, atom2, angle);
	Object3DSet rotationSet = collectRotationSet(atom1, atom2);
	Object3DTools.rotateSet(rotationSet, atom2.getPosition().minus(atom1.getPosition()), atom2.getPosition(), angle);
    }


    /** sets for example 'A' for chain A (corresponds to PDB file entry) */
    public void setChainCharacter(char chainChar) {
	this.chainCharacter = chainChar;
    }

    public void setParent(Object obj) {
	if (obj instanceof Object3D) {
	    super.setParent((Object3D)obj);
	}
	assert false;
	throw new RuntimeException("Internal error: only object3d objects can be added here.");
    }

    public void setWeight(double weight) { this.weight = weight; }

    /** Shortens current sequence to have length position (residues 0..position-1), and returns new sequence with positions position...length-1.
     * No cloning of residues is being done, just reorganizing. */
    public BioPolymer split(int position, String newName) {
	int oldLength = getResidueCount();
	String oldSequence = sequenceString();
	SimpleRnaStrand newStrand = new SimpleRnaStrand(getAlphabet());
	newStrand.setName(newName);
	split(position, newStrand);
	assert (getResidueCount() + newStrand.getResidueCount()) == oldLength;
	assert oldSequence.equals(sequenceString() + newStrand.sequenceString());
	return newStrand;
    }

    /** standard output method */
    public String toString() {
	String result = "(RnaStrand " + toStringBody() + sequenceString();
	result = result + " )";
	return result;
    }
    
}
