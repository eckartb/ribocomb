/** This class implements concept of an "binding site"
 *  A binding site as a position and orientation in space (thus Object3)
 *  it optionally can have a part of a sequence associated with it
 *  The parent object is the object the binding site belongs to
 */

package rnadesign.rnamodel;

import sequence.SequenceSubset;
import sequence.SimpleSequenceSubset;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.SimpleObject3D;

public class SimpleSequenceBindingSite extends SimpleObject3D implements SequenceBindingSite {

    private SequenceSubset bindingSequence;

    /** default constructor */
    public SimpleSequenceBindingSite() {
	super();
	bindingSequence = new SimpleSequenceSubset();
    }


    /** default constructor */
    public SimpleSequenceBindingSite(SequenceSubset sequenceSubset) {
	super();
	this.bindingSequence = sequenceSubset;
    }

    /** default constructor */
    public SimpleSequenceBindingSite(Object3D obj, SequenceSubset sequenceSubset) {
	super(obj);
	this.bindingSequence = sequenceSubset;
    }

    public SequenceSubset getBindingSequence() {
	return this.bindingSequence;
    }

    public void setBindingSequence(SequenceSubset bindingSequence) {
	this.bindingSequence = bindingSequence;
    }

    /** sets sequence
     * @param seq sequence to be set (whole sequence, including non-binding part
     * @param minId  minimum nucleotide position (5' end). Counting starts from zero.
     * @param maxId  maximum nucleotide id involding in binding PLUS ONE
     */ 
    /*
    public void setSequence(String seq, int minId, int maxId) {
	sequence = seq;
	sequenceIdMin = minId;
	sequenceIdMax = maxId;
	update();
    }
    */

    /* public void update() {
	String seq = getSequence().toSequenceString();
	seq = seq.substring(minId, maxId-minId);
	String subsetName = seq.getName() + "." + (sequenceMinId+1) + "-" + (sequenceMaxId+1);
	sequenceSubset = new SimpleSequence("", subsetName, seq.getAlphabet());
	for (int i = 0; i < seq.size(); ++i) {
	    sequenceSubset.add(seq.getResidue(i));
	}
    }
    */

    /** standard output method */
    public String toString() {
	String result = "(Binding " + toStringBody() + bindingSequence.toString();
	return result;
    }


}
