package rnadesign.rnamodel;

import generaltools.ResultWorker;
import generaltools.ScoreWrapper;
import java.io.PrintStream;
import java.util.*;
import java.util.logging.*;
import tools3d.objects3d.*;
import tools3d.Vector3D;

/** Algorithm for automated fusing of RNA strands disregarding junctions */
public class SimpleStrandFuser extends ResultWorker {

    private Logger log = Logger.getLogger(PackageConstants.LOGFILE_DEFAULT);

    // some helper words used to indicate status of strands in properties object
    public static final int STRAND_FUSION_MAX = 20;
    private static final double STRAND_DIST_MAX = 7.0; // minimum angle between connecting strands running i
    private static final double STRAND_ANGLE_MAX = 0.75 * Math.PI; // minimum angle between connecting strands running i
    private static final double ANGLE_PENALTY = 50.0; // bad angle between connecting strands is penalized by this "distance" 
    private static final double STRAND_CONNECTION_SCORE_CUTOFF = 50.0;
    private static final String TMP_TOKEN = "__RINGFUSE_STATUS";
    private static final String TMP_TOKEN_OCCUPIED = "occupied";
    // private double distanceCutoff = 10.0; // max distance between two residues to be fused
    private double scoreCutoff = STRAND_CONNECTION_SCORE_CUTOFF;
    private static boolean debugMode = true;
    private List<RnaStrand> strands = new ArrayList<RnaStrand>();
    // private List<RnaStrand> fusedStrands; 
    // private Object3D loops = new SimpleObject3D("loops");
    
    public SimpleStrandFuser(List<RnaStrand> strands) {
	this.strands.addAll(strands); // new container
	// this.strands = cleanStrands(this.strands); // FIXIT
	log.info("Initial strands: " + strands.size() + " cleaned version: " + this.strands.size());
    }

    /** Return bridging loops */
    // public Object3D getLoops() { return loops; }

    public SimpleStrandFuser(Object3DSet strandSet) {
	for (int i = 0; i < strandSet.size(); ++i) {
	    Object3D obj = strandSet.get(i);
	    if (obj instanceof RnaStrand) {
		this.strands.add((RnaStrand)obj);
	    }
	    else {
		log.severe("Internal error in RingFuse: 3D object is not RNA strand: " + obj.getFullName() + " " + obj.getClassName());
		assert false;
	    }
	}
	// this.strands = cleanStrands(this.strands); // FIXIT
	log.info("Initial strands: " + strandSet.size() + " cleaned version: " + this.strands.size());
    }

    public void runInternal() {
	log.info("Starting automatic strand fusing for " + strands.size() + " strands !");
        Properties properties = new Properties();
	// for each junction class: pick first element
	double bestScore = 1e10;
	int bestI = 0;
	int bestJ = 0;
	for (int i = 0; i < strands.size(); ++i) {
	    if (!checkStrandAvailable(strands.get(i))) {
		log.info("Strand " + strands.get(i).getName() + " is not available.");
		continue;
	    }
	    for (int j = 0; j < strands.size(); ++j) {
		if (i == j) {
		    continue; // do not allow fusing with itself!
		}
		if (!checkStrandAvailable(strands.get(j))) {
		    log.info("Strand " + strands.get(j).getName() + " is not available.");
		    continue;
		}
		try {
		    double score = scoreStrandConnectivity(strands.get(i), strands.get(j));
		    if (score < bestScore) {
			bestScore = score;
			bestI = i;
			bestJ = j;
		    }
		}
		catch (RnaModelException rne ) {
		    // do nothing 
		}
	    }
            result = properties;
	}
	if (bestScore < scoreCutoff) {
	    properties.setProperty("fused_strands", "true");
	    properties.setProperty("strand1", strands.get(bestI).getName());
	    properties.setProperty("strand2", strands.get(bestJ).getName());
	    properties.setProperty("score", "" + bestScore);
	    log.info("Fusing strands " + strands.get(bestI).getFullName() + " and " + strands.get(bestJ).getFullName());
	    strands.get(bestI).appendAndDelete(strands.get(bestJ)); // remove second strand
	} else {
	    log.info("Could not find any candidates for fusing strands: " + bestScore);
	}
	log.info("Finished SimpleStrandFused.runInternal!");
    }


    /** checks if strand is occupied */
    private boolean checkStrandAvailable(NucleotideStrand strand) {
	return strand.getProperty(TMP_TOKEN) != TMP_TOKEN_OCCUPIED;
    }

    public void setScoreCutoff(double value) { scoreCutoff = value; }

    /** marks strand as occupied */
    private void setStrandOccupied(NucleotideStrand strand) {
	strand.setProperty(TMP_TOKEN, TMP_TOKEN_OCCUPIED);
    }

    /** Scores how well a start strand can be appended with end strand. Zero: perfect score */
    private double scoreStrandConnectivity(RnaStrand startStrand, RnaStrand endStrand) throws RnaModelException {
	Nucleotide3D startStrandResidue = (Nucleotide3D)(startStrand.getResidue3D(startStrand.getResidueCount()-1));
	Nucleotide3D endStrandResidue = (Nucleotide3D)(endStrand.getResidue3D(0));
	Object3D ao21 = startStrandResidue.getChild("O2*");
	Object3D ap2 = endStrandResidue.getChild("P");
	if (ap2 == null) {
	    NucleotideDBTools.addMissingPhosphor(endStrandResidue);
	    ap2 = endStrandResidue.getChild("P");
	    assert ap2 != null; // phosphor was added even if this is not physical
	}
	if (ao21 == null) {
	    throw new RnaModelException("Could not find atom O2* in residue: " + startStrandResidue.getFullName());
	}
	double distance = ao21.distance(ap2);
	// check angles:
	Vector3D v1 = NucleotideDBTools.getBackwardDirectionVector(startStrandResidue, "C4*");
	Vector3D v2 = NucleotideDBTools.getForwardDirectionVector(endStrandResidue, "C4*");
	double angle = Math.PI - v1.angle(v2); // forward and backward direction go in opposite directions, compensate for that
	if (distance > STRAND_DIST_MAX) {
	    distance += scoreCutoff + 1.0; // cannot be bridged
	}
	if (angle > STRAND_ANGLE_MAX) { // 
	    distance += ANGLE_PENALTY;
	}
	return distance;
    }


}
