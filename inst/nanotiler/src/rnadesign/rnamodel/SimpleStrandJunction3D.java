
package rnadesign.rnamodel;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import graphtools.*;
import sequence.SequenceStatus;
import tools3d.Vector3D;
import tools3d.objects3d.Link;
import tools3d.objects3d.LinkSet;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DSet;
import tools3d.objects3d.Object3DTools;
import tools3d.objects3d.Object3DWriter;
import tools3d.objects3d.SimpleObject3D;
import tools3d.objects3d.SimpleObject3DSet;

/** Describes concept on n-way junction of strand.
 * Each arm of the junction corresponds to one or two sequences. 
 * There is no function "addBranch". Instead one defines
 * new branches by adding them with "insertChild"
 */
public class SimpleStrandJunction3D extends SimpleObject3D implements StrandJunction3D {

    public static final double BRANCH_MEET_DIST = 30.0;
    public static final String NEWLINE = System.getProperty("line.separator");

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    private int branchCount = 0;

    /** if true, junction possesses internal helices. Flag has to be kept in sync */
    private boolean internalHelices = false; 

    /** returns for n'th sequence the branch id for which it is the incoming sequence. */
    private int[] incomingBranchIds = new int[BRANCH_MAX];    
    /** returns for n'th sequence the branch id for which it is the outgoing sequence. */
    private int[] outgoingBranchIds = new int[BRANCH_MAX];
    /** index of incoming sequence of n'th branch */
    private int[] incomingSeqIds = new int[BRANCH_MAX];
    /** index of outgoing sequence of n'th branch */
    private int[] outgoingSeqIds = new int[BRANCH_MAX];

    public static final String CLASS_NAME = "StrandJunction3D";

    /** default constructor is just used internally for methods like "cloneDeep" */
    public SimpleStrandJunction3D() { }

    public SimpleStrandJunction3D(Object3DSet branchesOrig) {
	assert (branchesOrig != null) && (branchesOrig.size() > 0);
	// log.info("Starting SimpleStrandJunction3D(Object3DSet). Branches: " + branchesOrig.size());
	Vector3D pos = new Vector3D(0, 0, 0);
	for (int i = 0; i < branchesOrig.size(); ++i) {
	    BranchDescriptor3D branch = (BranchDescriptor3D)(((BranchDescriptor3D)(branchesOrig.get(i))).cloneDeep());
	    // search if incoming or outgoing sequence of branch descriptor is alrady known.
	    // if yes, overwrite with already known sequence
	    // BranchDescriptor3D branch = (BranchDescriptor3D)(branchesOrig.get(i)); 
	    assert (!branch.isSingleSequence()) || (branchesOrig.size() == 1);
	    // log.fine("branch descriptor position " + (i+1) + " : " + branch.getPosition());
	    pos.add(branch.getPosition());
	    // branches.add(branch);
	    insertChild(branch);
	    // addStrands(branch);
	    // 	    addIncomingStrand(branch.getIncomingStrand(), getBranchCount()-1);
	    // 	    addOutgoingStrand(branch.getOutgoingStrand(), getBranchCount()-1);
	}
	pos.scale(1.0 / (double)(branchesOrig.size()));
	// TODO : better: center of mass of all atoms in middle area
	// cloneStrands(); // use only clone copy of strands // not necessary because BranchDescriptors are already cloned
// 	Vector3D linePos = StrandJunctionTools.computeJunctionCenter(this); // compute medium position where lines meet
// 	if (linePos.distance(pos) < BRANCH_MEET_DIST) {
// 	    log.finest("BranchDescriptor meet position was plausible: " + linePos 
// 			       + " using nonetheless average BranchDescriptor position: " + pos);
// 	    setIsolatedPosition(linePos);
// 	    // Object3DTools.balanceTree(this); // set position to center of mass
// 	}
// 	else {
	    // Object3DTools.balanceTree(this); // dangerous! Might mess up direction of branch descriptors
	setIsolatedPosition(pos); // set position to center of mass
	// 	    log.fine("Warning: BranchDescriptor meet position was not plausible: " + linePos 
	//        + " using instead average BranchDescriptor position: " + getPosition());
	// }
	// avoid setPosition method which in turn calls new version of translate
	// pos = pos.minus(getPosition());
	// setPosition(pos); // 
	// 	setRelativePosition(getRelativePosition().plus(pos));
	// 	Vector3D negShift = pos.mul(-1.0); // compensate shift for child nodes
	// 	for (int i = 0; i < size(); ++i) {
	// 	    Object3D child = getChild(i);
	// 	    child.translate(negShift); // translate in opposite direction. Relatively cheap method
	// 	}
// 	log.fine("Quitting SimpleStrandJunction3D(Object3DSet): " 
// 			   + getPosition() + " " + size() + " " + getBranchCount() + " " + getStrandCount());
	// assert isValid(); // checks if for each strand is the incoming strand of one branch and the outgoing strand of another branch
	setFragmentStatus();
	assert checkChildrenUnique();
    }

    /** Possibility to store additional strands, branch descriptors and other objects without interfering with core class definition.
     * additional objects are stored as child nodes of group node with name "additional".
     */
    public SimpleStrandJunction3D(Object3DSet branchesOrig, Object3DSet additionalObjects) {
	this(branchesOrig);
	Object3D additionalNode = new SimpleObject3D();
	additionalNode.setName("additional");
	additionalNode.setPosition(this.getPosition());
	for (int i = 0; i < additionalObjects.size(); ++i) {
	    additionalNode.insertChild(additionalObjects.get(i));
	}
	insertChild(additionalNode);
    }

    /** Generates a list of junctions with smaller order */
    public Object3DSet cloneDown(int junctionOrder) {
 	assert junctionOrder < getBranchCount();
	Object3DSet result = new SimpleObject3DSet();
	IntegerPermutator permGen = new IntegerArrayIncreasingGenerator(junctionOrder, getBranchCount());
	Object3DSet bSet = new SimpleObject3DSet();
	Object3D additional = getChild("additional");
	do {
	    int[] perm = permGen.get();
	    assert(perm.length == junctionOrder); // not all branch descriptor indices are used for current junction
	    bSet.clear();
	    for (int i = 0; i < junctionOrder; ++i) {
		bSet.add(getBranch(perm[i]));
	    }

	    Object3D currentAdditional = getChild("additional");
	    Object3D newAdditional = new SimpleObject3D();
	    if (currentAdditional != null) {
		for (int i = 0; i < currentAdditional.size(); ++i) {
		    newAdditional.insertChild((Object3D)(currentAdditional.getChild(i).cloneDeep()));
		}
	    }
	    SimpleStrandJunction3D newJunction = new SimpleStrandJunction3D(bSet);
	    // check which strands have been forgotten; they are added as child nodes of "additiona" node:
	    for (int i = 0; i < getStrandCount(); ++i) {
		if (newJunction.findStrandIndex(getStrand(i)) < 0) {
		    newAdditional.insertChild((Object3D)(getStrand(i).cloneDeep()));
		}
	    }
	    if (newAdditional.size() > 0) {
		log.info("Adding additional cloned child nodes to junction " + newJunction.getFullName() + " : " + newAdditional.size());
		newJunction.insertChild(newAdditional);
	    }
	    result.add(newJunction);
	} while (permGen.hasNext() && permGen.inc());
	return result;
    }

    public static void setFragmentStatus(NucleotideStrand strand) {
	for (int i = 0; i < strand.getResidueCount(); ++i) {
	    strand.getResidue(i).setProperty(SequenceStatus.name, SequenceStatus.fragment);
	}
    }

    /** sets sequence_status status of all residues to "fragment" */
    private void setFragmentStatus() {
	for (int i = 0; i < getStrandCount(); ++i) {
	    NucleotideStrand strand = getStrand(i);
	    setFragmentStatus(strand);
	}
    }

    /** check if incoming and outgoing strands already exist. Add them as child nodes if they do not exist yet.
     * TODO : verify for complication.
     */
    private void addStrands(BranchDescriptor3D branch) {
	assert branch.isValid();
	// find out index of branch:
	// int branchIndex = getIndexOfChild(branch);
	int branchIndex = getChildClassCounter(branch); // big change! Check if not bugs where introduced.
	assert(branchIndex < getBranchCount());
	assert branchIndex >= 0; // must be found
	NucleotideStrand incoming = branch.getIncomingStrand();
	NucleotideStrand outgoing = branch.getOutgoingStrand();
	int inId = findStrandIndex(incoming);
	if (inId < 0) { // strand not found
	    insertChild(incoming);
	    inId = size()-1;
	}
	else {
	    // adjust incoming strand of branch descriptor:
	    branch.setIncomingStrand(getStrand(inId));
	}
	incomingBranchIds[inId] = branchIndex; 
	incomingSeqIds[branchIndex] = inId;
	int outId = findStrandIndex(outgoing);
	if (outId < 0) { // strand not found
	    insertChild(outgoing);
	    outId = size()-1;
	}
	else {
	    // adjust incoming strand of branch descriptor:
	    branch.setOutgoingStrand(getStrand(outId));
	}
	outgoingBranchIds[outId] = branchIndex; 
    	outgoingSeqIds[branchIndex] = outId;
    }

    /** returns true, if all branch descriptor occupy a free "corridor" with a certain radius.
     * If supplied radius is smaller or equal zero, true if returned. */
    public boolean corridorCheck(CorridorDescriptor corridorDescriptor) {
	double corridorRadius = corridorDescriptor.radius;
	double corridorStart = corridorDescriptor.start;
	if (corridorRadius <= 0.0) {
	    return true;
	}
	Object3DSet atomSet = Object3DTools.collectByClassName(this, "Atom3D"); // obtain all atoms
	assert atomSet.size() > 0;
	for (int i = 0; i < getBranchCount(); ++i) {
	    BranchDescriptor3D branch = getBranch(i);
	    if (! Object3DTools.corridorCheck(branch.getPosition(), branch.getDirection(), corridorRadius, corridorStart, atomSet)) {
		return false;
	    }
	}
	return true; 
    }

    /** Returns true iff junction contains two strands as kissing loop */
    public boolean isKissingLoop() { return false; }

    /** updates indices incomingBranchIds, incomingSeqIds etc TODO */
    private void updateIndices() {
	log.fine("updateIndices not yet implemented !!!");
    }

    /** appends strand m to strand n. Strand m gets discarded. 
     * Indeces of branch descriptors are adjusted.
     * It is the users responsisbility to make sure that 
     * it makes sense to fuse strands n and m. */
    public void fuseStrands(int n, int m) {
	log.fine("Starting SimpleStrandJunction3D.fuseStrands");
	assert n < getStrandCount();
	assert m < getStrandCount();
	int strandCountOrig = getStrandCount();
	NucleotideStrand strand1 = getStrand(n);
	int size1 = strand1.size();
	NucleotideStrand strand2 = getStrand(m);
	strand1.append(strand2);
	super.removeChild(strand2);
	// hard part: adjust branch descriptors:
	int branchCount = getBranchCount();
	log.fine("strand2: " + strand2 + NEWLINE + "strand1: " + strand1);
	for (int i = 0; i < branchCount; ++i) {
	    BranchDescriptor3D branch = getBranch(i);
	    if (branch.getOutgoingStrand() == strand2) {
		branch.setOutgoingStrand(strand1);
		branch.setOutgoingIndex((branch.getOutgoingIndex() + size1));
	    }
	    if (branch.getIncomingStrand() == strand2) {
		branch.setIncomingStrand(strand1);
		branch.setIncomingIndex((branch.getIncomingIndex() + size1));
	    }
	}
	updateIndices(); // updates helper indices
    }
    

//     private void addIncomingStrand(NucleotideStrand strand, int branchIndex) {
// 	int id = findStrandIndex(strand);
// 	// 	if (id < 0) {
// 	// 	    strands.add(strand);
// 	// 	    id = strands.size()-1;
// 	// 	}
// 	incomingBranchIds[id] = branchIndex; 
// 	incomingSeqIds[branchIndex] = id;
//     }

//     private void addOutgoingStrand(NucleotideStrand strand, int branchIndex) {
// 	int id = findStrandIndex(strand);
// 	if (id < 0) {
// 	    strands.add(strand);
// 	    id = strands.size()-1;
// 	}
// 	outgoingBranchIds[id] = branchIndex; 
//     	outgoingSeqIds[branchIndex] = id;
//     }

    /** performs deep copy of strands */
//     private void cloneStrands() {
// 	for (int i = 0; i < strands.size(); ++i) {
// 	    strands.set(i, getStrand(i).cloneDeep());
// 	}
//     }

    /** "deep" clone including children: every object is newly generated!
     * TODO
     */
    public Object cloneDeepThis() {
	SimpleStrandJunction3D obj = new SimpleStrandJunction3D();
	obj.copyDeepThisCore(this);
	obj.branchCount = branchCount;
	obj.internalHelices = internalHelices;
	obj.incomingBranchIds = new int[BRANCH_MAX];
	obj.outgoingBranchIds = new int[BRANCH_MAX];
	obj.incomingSeqIds = new int[BRANCH_MAX];
	obj.outgoingSeqIds = new int[BRANCH_MAX];
	for (int i = 0; i < BRANCH_MAX; ++i) {
	    obj.incomingBranchIds[i] = incomingBranchIds[i];
	    obj.outgoingBranchIds[i] = outgoingBranchIds[i];
	    obj.incomingSeqIds[i] = incomingSeqIds[i];
	    obj.outgoingSeqIds[i] = outgoingSeqIds[i];
	}

	// 	obj.set Name(getName());
	// 	obj.setParent(getParent());
	// 	obj.setPosition(getPosition());
	// 	for (int i = 0; i < getBranchCount(); ++i) {
	// 	    BranchDescriptor3D branch = (BranchDescriptor3D)(getBranch(i).cloneDeep()); // clone new branch and sequence!
	// 	    // obj.branches.add(branch);
	// 	    // obj.insertChild(branch);
	// 	    obj.addIncomingStrand(branch.getIncomingStrand(), getBranchCount()-1);
	// 	    obj.addOutgoingStrand(branch.getOutgoingStrand(), getBranchCount()-1);
	// 	}
	
	// do not copy arrays (incomingBranchIds etc). Rely on them being updated in the 
	// specialized version of insertChild

	return obj; // TODO : test if ok!
    }

    /** "deep" clone including children: every object is newly generated! */
     public Object cloneDeep() {
 	SimpleStrandJunction3D newObj = (SimpleStrandJunction3D)(cloneDeepThis());

	newObj.setFilename(getFilename());

 	for (int i = 0; i < size(); ++i) {
 	    newObj.insertRawChild((Object3D)(getChild(i).cloneDeep())); // use raw version
 	}
	NucleotideStrand[] strands = getStrands();
	int branchCount = getBranchCount();
	newObj.branchCount = getBranchCount();
	// the following code makes sure that the new branch descriptors do not have their own copies of the strands
	// but use the strands defined in the new junction
	for (int i = 0; i < branchCount; ++i) {
	    // find which strand 
	    BranchDescriptor3D branch = getBranch(i);
	    BranchDescriptor3D newBranch = newObj.getBranch(i);
	    NucleotideStrand inStrand = branch.getIncomingStrand();
	    NucleotideStrand outStrand = branch.getOutgoingStrand();
	    for (int j = 0; j < strands.length; ++j) {
		if (inStrand == strands[j]) {
		    newBranch.setIncomingStrand(newObj.getStrand(j));
		}
		if (outStrand == strands[j]) {
		    newBranch.setOutgoingStrand(newObj.getStrand(j));
		}
	    }
	}
 	return newObj;
     }

    /** return index of strand if found, -1 otherwise */
    public int findStrandIndex(NucleotideStrand strand) {
	int sc = getStrandCount();
	for (int i = 0; i < sc; ++i) {
	    if (getStrand(i).isProbablyEqual(strand)) { // fast check
		return i;
	    }
	}
	return -1;
    }

//     /** return index of strand if found, -1 otherwise */
//     private void consolidateStrands() {
// 	int bc = getBranchCount();
// 	for (int i = 0; i < bc; ++i) {
// 	    for (int j = i+1; j < getBr
// 		if (getStrand(i).isProbablyEqual(strand)) { // fast check
// 		    return i;
// 		}
// 	    }
// 	}
// 	return -1;
//     }

    /** returns number of branches */
    public int getBranchCount() { 
	assert branchCount == getChildCount("BranchDescriptor3D");
	return branchCount;
    }

    public String getClassName() { return CLASS_NAME; }

    /** index of incoming sequence of n'th branch */
    public int[] getIncomingSeqIds() { 
	return incomingSeqIds; 
    }

    public boolean hasInternalHelices() { return internalHelices; }

    /** length of loop of strand id */
    public int getLoopLength(int strandId) {
	int inBId = getIncomingBranchId(strandId); // return branch descriptor id of 
	int outBId = getOutgoingBranchId(strandId);
	BranchDescriptor3D inBranch = getBranch(inBId);
	BranchDescriptor3D outBranch = getBranch(outBId);
	int inId = inBranch.getIncomingIndex() + inBranch.getOffset() + 1; // points to first nucl. of loop
	int outId = outBranch.getOutgoingIndex() - outBranch.getOffset() - 1; // points to last nucleotid of loop
	int diff = outId - inId + 1;
	// diff -= getBranch(inBId).getOffset() - getBranch(outBId).getOffset(); // offset; // should be used, but not part of branch descriptor anymore
	// log.warning("Warning: no branch descriptor offset used for getLoopLength");
	if (diff < 0) {
	    log.fine("Warning: bad loop length indices: " + inBId + " " + outBId + " strand: "
			+ strandId); 
	    // + getStrand(strandId));
	}
	// assert diff >= 0;
	return diff;
    }

    /** index of outgoing sequence of n'th branch */
    public int[] getOutgoingSeqIds() { 
	return outgoingSeqIds; 
    }

    /** returns number of defined strands */
    public int getStrandCount() { 
	// log.fine("Calling slow version of getStrandCount");
	int counter = 0;
	for (int i = 0; i < size(); ++i) {
	    if (getChild(i) instanceof NucleotideStrand) {
		++counter;
	    }
	}
	return counter;
	// return strands.size(); 
    }
    
    /** returns descriptor for n'th branch */
    public BranchDescriptor3D getBranch(int n) { 
	int idx = getIndexOfChild(n, "BranchDescriptor3D");
	assert (idx >= 0);
	BranchDescriptor3D result = (BranchDescriptor3D)(getChild(idx));
	assert result.getParent() == this;
	return result;
	// return (BranchDescriptor3D)(branches.get(n)); 
    }
    
    /** returns n'th strand. */
    public NucleotideStrand getStrand(int n) { 
	// return (NucleotideStrand)(strands.get(n)); 
	int counter = -1;
	for (int i = 0; i < size(); ++i) {
	    if (getChild(i) instanceof NucleotideStrand) {
		++counter;
		if (counter == n) {
		    return (NucleotideStrand)(getChild(i));
		}
	    }
	}
	// int idx = getIndexOfChild(n, "NucleotideStrand3D"); // avoid, because if would not work with RnaStrand etc
	assert false; // no strand found!
	return null;
    }

    /** returns references to all strands. */
    public NucleotideStrand[] getStrands() { 
	int strandCount = getStrandCount();
	NucleotideStrand[] strands = new NucleotideStrand[strandCount];
	for (int i = 0; i < strandCount; ++i) {
	    strands[i] = getStrand(i);
	}
	assert strands.length == getStrandCount();
	return strands;
    }
    
    /** returns for n'th sequence the branch id for which it is the
     * incoming sequence.
     */
    public int getIncomingBranchId(int n) {
	assert (n < incomingBranchIds.length);
	assert (incomingBranchIds[n] < getBranchCount());
	assert (getBranch(incomingBranchIds[n]) != null);
	return incomingBranchIds[n]; 
    }

    /** returns for n'th sequence the branch id for which it is the
     * incoming sequence.
     */
    public int[] getIncomingBranchIds() { return incomingBranchIds; }

    /** returns for n'th sequence the branch id for which it is the
     * outgoing sequence.
     */
    public int getOutgoingBranchId(int n) { 
// 	assert (n < outgoingBranchIds.length);
// 	if (outgoingBranchIds[n] >- getBranchCount()) {
// 	    log.fine("Internal error in line 358: " 
// 		       + n + " "
// 		       + outgoingBranchIds[n] + " "
// 		       + getBranchCount());
// 	    for (int i = 0; i < outgoingBranchIds.length; ++i) {
// 		log.finest(outgoingBranchIds[i] + " ");
// 	    }
// 	    log.fine("");
// 	    log.fine("Number of children: " + size());
// 	    for (int i = 0; i < size(); ++i) {
// 		log.fine(getChild(i).getClassName() + " " + getChild(i).getName());
// 	    }
// 	}
// 	assert (outgoingBranchIds[n] < getBranchCount());
// 	assert (getBranch(outgoingBranchIds[n]) != null);
	return outgoingBranchIds[n]; }

    /** returns for n'th sequence the branch id for which it is the
     * outgoing sequence.
     */
    public int[] getOutgoingBranchIds() { return outgoingBranchIds; }

    /** use original method from super class */
    public void insertRawChild(Object3D obj) {
	if (obj instanceof BranchDescriptor3D) {
	    ++branchCount;
	}
	super.insertChild(obj);
    }

    public void insertChild(Object3D obj) {
	for (int i = 0; i < size(); ++i) {
	    if (getChild(i).isProbablyEqual(obj)) {
		assert false; // assertion violated!
	    }
	}
	super.insertChild(obj);
	if (obj instanceof BranchDescriptor3D) {
	    ++branchCount;
	    BranchDescriptor3D branch = (BranchDescriptor3D)obj;
	    assert branch.isValid();
	    addStrands(branch);
	}
    }

    public void removeChild(Object3D obj) {
	if (obj instanceof BranchDescriptor3D) {
	    log.warning("Sorry, removing helix descriptors from junctions is currently not allowed.");
	    return;
	}
	super.removeChild(obj);
	if (obj instanceof BranchDescriptor3D) {
	    --branchCount;
	    assert false; // implementation not complete yet
	}
    }

    /** returns true if a strand is going from 
     * incoming branchid to outgoing branchid.
     * True only if directionality correct.
     */
    public boolean isConnected(int incomingBranchId, int outgoingBranchId) { 
	return incomingSeqIds[incomingBranchId] == outgoingSeqIds[outgoingBranchId];
    }

    /** A junction is regular if and only if getBranchCount() == getStrandCount() */
    public boolean isRegular() { return getBranchCount() == getStrandCount(); }

    /** checks if defined and consistent */
    public boolean isValid() {
	if (!super.isValid()) {
	    return false;
	}
	int branchCount = getBranchCount();
	int strandCount = getStrandCount();
	if ((branchCount == 0) || (strandCount == 0)) {
	    return false;
	}
	// number of branch descriptors must be equal to the number of strands:
	if (branchCount != strandCount) {
	    log.info("Warning: BranchCount is not equal Strand count: " + getBranchCount() + " "
			       + getStrandCount());
	    return false;
	}
	// each strand is the outgoing (and also incoming) strand of one and only one branch descriptor
	log.fine("starting SimpleStrandJunction3D.isValid with junction of size: " + branchCount);
	for (int i = 0; i < branchCount; ++i) {
	    BranchDescriptor3D branch = getBranch(i);
	    if (!branch.isValid()) {
		return false;
	    }
	    // write branch descriptor
// 	    String branchPdb = toPdb(branch, i, writer);
// 	    result = result + branchPdb;
	    NucleotideStrand strand = branch.getIncomingStrand();
	    int startId = branch.getIncomingIndex();
	    // seach for closest stop id:
	    int bestId = 0;
	    int bestDiff = 10000;
	    boolean found = false;
	    // TODO : the following code should be done by BranchDescriptor
	    for (int j = 0; j < branchCount; ++j) {
		if (i == j) {
		    continue;
		}
		BranchDescriptor3D branch2 = getBranch(j);
		if (branch2.getOutgoingStrand() != strand) {
		    continue;
		}
		int stopId = branch2.getOutgoingIndex();
		if (stopId <= startId) {
		    continue;
		}
		int diff = stopId - startId;
		if (diff < bestDiff) {
		    bestDiff = diff;
		    bestId = j;
		    found = true;
		}
	    }
	    if (!found) {
		log.fine("Warning: bad strand " + i);
		Object3DWriter writer = new StrandJunction3DLispWriter();
		log.fine("SimpleStandJunction3D is not valid!");
		// 		log.fine(writer.writeString(this));
		return false;
	    }
	}
	for (int i = 0; i < getStrandCount(); ++i) {
	    if (getLoopLength(i) < 0) {
		return false;
	    }
	}
	return true;
    }

    /** rotates this object and all child nodes around center
     * rotation-axis and angle
     */
//     public void rotate(Vector3D center, 
// 		       Vector3D axis, double angle) {
// 	super.rotate(center, axis, angle);
// 	for (int i = 0; i < getStrandCount(); ++i) {
// 	    NucleotideStrand strand = getStrand(i);
// 	    strand.rotate(center, axis, angle);
// 	}	
// 	for (int i = 0; i < getBranchCount(); ++i) {
// 	    NucleotideStrand branch = getBranch(i);
// 	    branch.rotate(center, axis, angle);
// 	}	
//    }

    /** Translate object. */
//     public void translate(Vector3D vec) {
// 	super.translate(vec);
// 	for (int i = 0; i < getStrandCount(); ++i) {
// 	    NucleotideStrand strand = getStrand(i);
// 	    strand.translate(vec);
// 	}
// 	for (int i = 0; i < getBranchCount(); ++i) {
// 	    BranchDescriptor3D branch = getBranch(i);
// 	    branch.translate(vec);
// 	}
//     }

    /** returns index of link that links two branch descriptors between junctions. 
     * TODO : careful: assumes only one link leading from one junction to next!
     */
    public int findBranchConnectionLink(StrandJunction3D junction, LinkSet links) {
	int branchCount = getBranchCount();
	for (int i = 0; i < branchCount; ++i) {
	    BranchDescriptor3D branch1 = getBranch(i);
	    for (int j = 0; j < branchCount; ++j) {
		BranchDescriptor3D branch2 = junction.getBranch(j);
		for (int k = 0; k < links.size(); ++k) {
		    Link link = links.get(k);
		    if (link.isLinked(branch1, branch2)) {
			return k;
		    }
		}
	    }
	}
	return -1;
    }
    
    public void setInternalHelices(boolean flag) { this.internalHelices = flag; }    

    public String infoString() {
	StringBuffer buf = new StringBuffer();
	if (isKissingLoop()) {
	    buf.append("kl ");
	}
	else {
	    buf.append("j ");
	}
	buf.append("" + getStrandCount() + " ");
	for (int i = 0; i < getStrandCount(); ++i) {
	    buf.append(getStrand(i).sequenceString() + " | ");
	}
	return buf.toString();
    }

}
