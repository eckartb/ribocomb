package rnadesign.rnamodel;

import java.io.DataInputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.IOException;
import java.util.*;
import java.util.logging.*;

import tools3d.*;
import tools3d.objects3d.LinkSet;
import tools3d.objects3d.CoordinateSystem3D;
import tools3d.objects3d.Object3DFactory;
import tools3d.objects3d.Object3DIOException;
import tools3d.objects3d.Object3DWriter;
import tools3d.objects3d.SimpleLinkSet;

import static rnadesign.rnamodel.RnaConstants.*;
import static rnadesign.rnamodel.PackageConstants.*;

/** implements concept of database of strand junctions */
public class SimpleStrandJunctionDB implements StrandJunctionDB {

    public static final String NEWLINE = System.getProperty("line.separator");
    private int junctionType = DBElementDescriptor.UNKNOWN_TYPE;
    private StrandJunction3D[][] strandJunctions;
    private List<HashMap<String,  Set<Integer>>> hashes; // used to retrieve junction based on hash: for each junction order, a different hash map, with hash as key and index of that junction as value
    private List<List<List<Matrix4D>>> junctionTransformations;
    private LinkSet[][] strandJunctionLinkSets;
    private JunctionHashGenerator hashGenerator = new JunctionHashGenerator();
    private Object3DFactory reader = new GeneralRnaReader();
    private Object3DWriter writer = new StrandJunction3DLispWriter();
    private static Logger log = Logger.getLogger(LOGFILE_DEFAULT);
    private String name = "junctionDB";

    public SimpleStrandJunctionDB() { assert false; };

    /** Construct object with specified maximum junction order. If user defines numOrders = n, than 0... n-1 are defined orders */
    public SimpleStrandJunctionDB(int numOrders) { 
	strandJunctions = new StrandJunction3D[numOrders][0]; 
	strandJunctionLinkSets = new SimpleLinkSet[numOrders][0];
	hashes = new ArrayList<HashMap<String, Set<Integer>>>();
	junctionTransformations = new ArrayList<List<List<Matrix4D> > >();
	for (int i = 0; i < numOrders; ++i) {
	    hashes.add(new HashMap<String, Set<Integer>>());
	    junctionTransformations.add(new ArrayList<List<Matrix4D> >());
	}
    };

    /** remove all data */
    public void clear() { 
	strandJunctions = null; 
	strandJunctionLinkSets = null;
    }

    /** adds a new junction to database */
    public synchronized void addJunction(StrandJunction3D junction,
					 LinkSet links) {

	int order = junction.getBranchCount();
	if (order >= size()) {
	    log.info("Warning: not adding junction because of too high order: " + order + " >= " + size());
	    return;
	}
	StrandJunction3D[] sofarJunctions = getJunctions(order);
	StrandJunction3D[] tmpJunctions = new StrandJunction3D[sofarJunctions.length + 1];
	for (int i = 0; i < sofarJunctions.length; ++i) { // TODO: not very elegant, use Java Container instead
	    tmpJunctions[i] = sofarJunctions[i];
	}
	tmpJunctions[tmpJunctions.length-1] = junction; // set last element to new junction
	strandJunctions[order] = tmpJunctions; // use new lengthened array!
	List<BranchDescriptor3D> branches = new ArrayList<BranchDescriptor3D>();
	for (int i = 0; i < junction.getBranchCount();++i) {
	    branches.add(junction.getBranch(i));
	}
	try {
	    assert branches != null;
	    assert branches.size() > 1;
	    List<String> junctionHashes = hashGenerator.generateCompleteHashes(branches);
	    for (String s : junctionHashes) {
		Set<Integer> l = hashes.get(order).get(s);
		if (l == null) {
		    l = new HashSet<Integer>();
		    hashes.get(order).put(s, l);
		}
	    l.add(tmpJunctions.length-1); // index of last added junction
	    List<Matrix4D> normTrafos = hashGenerator.generateCompleteNormalizedTransformations(junction);
	    // junctionHashes.get(order).add(normTrafos); // TODO
	}
	log.fine("Adding links into Junction DB not yet implemented!");
	} 
	catch (java.awt.geom.NoninvertibleTransformException niv) {
	    System.out.println(niv.getMessage());
	    System.exit(1);
	}
// 	LinkSet[] sofarLinkSets = getStrandJunctionLinkSets(order);
// 	LinkSet[] tmpLinkSets = new LinkSet[sofarLinkSets.length + 1];
// 	for (int i = 0; i < sofarLinkSets.length; ++i) {
// 	    tmpLinkSets[i] = sofarLinkSets[i];
// 	}
// 	tmpLinkSets[tmpLinkSets.length-1] = links; // set last element to new junction
// 	strandJunctionLinkSets[order] = tmpLinkSets; // use new lengthened array!
    }

    /** returns add junctions of specified order. Example: for 3-way junctions the order is 3. */
    public StrandJunction3D[] getJunctions(int order) {
	if ((strandJunctions == null) || (order < 0) || (order >= strandJunctions.length) ) {
	    return new StrandJunction3D[0];
	}
	return strandJunctions[order];
    }

    public String getName() { return name; }

    /** returns add junctions of specified order. Example: for 3-way junctions the order is 3. */
    public LinkSet[] getStrandJunctionLinkSets(int order) {
	if ((strandJunctionLinkSets == null) || (order < 0) || (order >= strandJunctionLinkSets.length) ) {
	    return new LinkSet[0];
	}
	return strandJunctionLinkSets[order];
    }

    /** returns n'th junction of specified order. Example: for 3-way junctions the order is 3. */
    public StrandJunction3D getJunction(int order, int n) {
	if ((strandJunctions == null) || (order < 0) || (order >= strandJunctions.length)
	    || (n >= strandJunctions[order].length)) {
	    return null;
	}
	return strandJunctions[order][n];
    }

    /** returns array with indices of all junctions of this order, that are 
     * superposable withing this DRMS cutoff */
    public int[] getSuperposableJunctions(int order, int n, double rmsLimit) {
	List<Integer> result = new ArrayList<Integer>();
	StrandJunction3D jnc = getJunction(order,n);
	StrandJunction3D[] otherJunctions = getJunctions(order);
	JunctionSimilarity similarityGenerator = new JunctionSimilarity();
	for (int i = 0; i < otherJunctions.length; ++i) {
	    if (i == n) {
		continue;
	    }
	    Properties superposProperties = similarityGenerator.similarity(jnc, otherJunctions[i]);
	    if (superposProperties != null) {
		String rmsString = superposProperties.getProperty(JunctionSimilarity.DRMS);
		if (rmsString != null) {
		    double rms = Double.parseDouble(rmsString);
		    if (rms <= rmsLimit) {
			result.add(new Integer(i));
		    }
		}
	    }
	}
	int[] finalResult = new int[result.size()];
	for (int i = 0; i < finalResult.length; ++i) {
	    finalResult[i] = result.get(i).intValue();
	}
	return finalResult;
    }

    /** returns array with indices of all junctions of this order, that are similar based on hash function */
    public Set<Integer> findSimilarJunctions(List<BranchDescriptor3D> branches) {
	System.out.println(hashesToString());
	List<CoordinateSystem> transforms = new ArrayList<CoordinateSystem>();
	CoordinateSystem3D cs = new CoordinateSystem3D(new Vector3D(0,0,0), new Vector3D(1.0, 0, 0), new Vector3D(0,1,0));
	for (int i = 0; i < branches.size(); ++i) { // slow, not 
	    transforms.add(cs);
	}
	return findSimilarJunctions(branches, transforms);
    }

    /** returns array with indices of all junctions of this order, that are similar based on hash function.
     * Before query, all helix descriptors are transformed using the provided active transformations. */
    public Set<Integer> findSimilarJunctions(List<BranchDescriptor3D> branches, List<CoordinateSystem> activeTransforms) {
	assert(branches.size() == activeTransforms.size());
	int order = branches.size(); // junction order
	System.out.println(hashesToString());
	Set<Integer> result = new HashSet<Integer>();
	try {
	    List<String> junctionHashes = hashGenerator.generateCompleteHashes(branches, activeTransforms);
	    for (int i = 0; i < junctionHashes.size(); ++i) {
		Set<Integer> set = hashes.get(order).get(junctionHashes.get(i));
		Iterator<Integer> it = set.iterator();

		while (it.hasNext()) {
		    System.out.print("" + it.next());
		}
		System.out.println("");
		result.addAll(set);
	    }
	} catch (java.awt.geom.NoninvertibleTransformException nit) {
	    log.warning("Could not generate hash codes for junction!");
	}
	return result;
    }

    /** returns array with indices of all junctions of this order, that are similar based on hash function.
     * Before query, all helix descriptors are transformed using the provided active transformations. */
    public Map<Integer, Double> findAndScoreSimilarJunctions(List<BranchDescriptor3D> branches) {
	int order = branches.size(); // junction order
	System.out.println(hashesToString());
	HashMap<Integer, Double> result = new HashMap<Integer, Double>();
	try {
	    List<Matrix4D> normTrafos = hashGenerator.generateCompleteNormalizedTransformations(branches); // normalized transformations
	    List<String> junctionHashes = hashGenerator.generateCompleteHashes(branches);
	    for (int i = 0; i < junctionHashes.size(); ++i) {
		Set<Integer> set = hashes.get(order).get(junctionHashes.get(i));
		Iterator<Integer> it = set.iterator();
		while (it.hasNext()) {
		    int id = it.next(); // id of next junction
		    StrandJunction3D junc = getJunction(order, id);
		    List<Matrix4D> currTrafos = hashGenerator.generateCompleteNormalizedTransformations(junc);
		    double score = hashGenerator.scoreNormalizedTransformations(normTrafos, currTrafos);
		    result.put(id, score);
		}
		// result.addAll(set);
	    }
	} catch (java.awt.geom.NoninvertibleTransformException nit) {
	    log.warning("Could not generate hash codes for junction!");
	}
	return result;
    }

    /** returns array with indices of all junctions of this order, that are similar based on hash function.
     * Before query, all helix descriptors are transformed using the provided active transformations. */
    public Map<Integer, Double> findAndScoreSimilarJunctions(List<BranchDescriptor3D> branches, List<CoordinateSystem> activeTransforms) {
	assert(branches.size() == activeTransforms.size());
	int order = branches.size(); // junction order
	System.out.println(hashesToString());
	HashMap<Integer, Double> result = new HashMap<Integer, Double>();
	try {
	    List<Matrix4D> normTrafos = hashGenerator.generateCompleteNormalizedTransformations(branches, activeTransforms); // normalized transformations
	    List<String> junctionHashes = hashGenerator.generateCompleteHashes(branches, activeTransforms);
	    for (int i = 0; i < junctionHashes.size(); ++i) {
		Set<Integer> set = hashes.get(order).get(junctionHashes.get(i));
		if (set != null) {
		    Iterator<Integer> it = set.iterator();
		    assert (it != null);
		    while (it.hasNext()) {
			int id = it.next(); // id of next junction
			StrandJunction3D junc = getJunction(order, id);
			List<Matrix4D> currTrafos = hashGenerator.generateCompleteNormalizedTransformations(junc);
			double score = hashGenerator.scoreNormalizedTransformations(normTrafos, currTrafos);
			result.put(id, score);
		    }
		} else {
		    System.out.println("No similar junctions found for hash " + (i+1) + " : " + junctionHashes.get(i));
		}
		// result.addAll(set);
	    }
	} catch (java.awt.geom.NoninvertibleTransformException nit) {
	    log.warning("Could not generate hash codes for junction!");
	}
	return result;
    }

    /** returns array with indices of all junctions of this order.
     * Before query, all helix descriptors are transformed using the provided active transformations. */
    public List<Double> scoreAllJunctions(List<BranchDescriptor3D> branches, List<CoordinateSystem> activeTransforms) {
	assert(branches.size() == activeTransforms.size());
	int order = branches.size(); // junction order
	// log.info("Scoring all junctions of order " + order + " number of junctions : " + size(order) + " hashes: " + hashesToString());
	List<Double> result = new ArrayList<Double>();
	try {
	    List<Matrix4D> normTrafos = hashGenerator.generateCompleteNormalizedTransformations(branches, activeTransforms); // normalized transformations
	    // List<String> junctionHashes = hashGenerator.generateCompleteHashes(branches, activeTransforms);
	    for (int id = 0; id < size(order); ++id) {
		// int id = it.next(); // id of next junction
		StrandJunction3D junc = getJunction(order, id);
		List<Matrix4D> currTrafos = hashGenerator.generateCompleteNormalizedTransformations(junc);
		double score = hashGenerator.scoreNormalizedTransformations(normTrafos, currTrafos);
		result.add(score);
	    }
	    // } else {
	    // System.out.println("No similar junctions found for hash " + (i+1) + " : " + junctionHashes.get(i));
	    // }
		// result.addAll(set);
	    // }
	} catch (java.awt.geom.NoninvertibleTransformException nit) {
	    log.warning("Could not generate hash codes for junction!");
	}
	return result;
    }

    public void setJunctionType(int type) { 
	assert (type >= 0);
	assert (type < DBElementDescriptor.JUNCTION_TYPE_MAX);
	junctionType = type;
    }
    
    /** returns array with indices of all junctions of this order.
     * Before query, all helix descriptors are transformed using the provided active transformations. */
    public List<Double> scoreAllJunctions(List<BranchDescriptor3D> branches) {
	int order = branches.size(); // junction order
	System.out.println(hashesToString());
	List<Double> result = new ArrayList<Double>();
	try {
	    List<Matrix4D> normTrafos = hashGenerator.generateCompleteNormalizedTransformations(branches); // normalized transformations
	    // List<String> junctionHashes = hashGenerator.generateCompleteHashes(branches, activeTransforms);
	    for (int id = 0; id < size(order); ++id) {
		// int id = it.next(); // id of next junction
		StrandJunction3D junc = getJunction(order, id);
		List<Matrix4D> currTrafos = hashGenerator.generateCompleteNormalizedTransformations(junc);
		double score = hashGenerator.scoreNormalizedTransformations(normTrafos, currTrafos);
		result.add(score);
	    }
	    // } else {
	    // System.out.println("No similar junctions found for hash " + (i+1) + " : " + junctionHashes.get(i));
	    // }
		// result.addAll(set);
	    // }
	} catch (java.awt.geom.NoninvertibleTransformException nit) {
	    log.warning("Could not generate hash codes for junction!");
	}
	return result;
    }

    /** returns array with similarity scores (0.0: identitical 3D transformations). 
     * The junctionIds are typically generated by function findSimilarJunctions. */
//     public double scoreSimilarJunction(List<BranchDescriptor3D> branches, int junctionId) {
// 	assert false; // not yet implemented
// 	return null;
//     }

    /** returns array with similarity scores (0.0: identitical 3D transformations). 
     * The junctionIds are typically generated by function findSimilarJunctions. */
    /*
    public double[] scoreSimilarJunctions(List<BranchDescriptor3D> branches, int[] junctionIds) {
	double[] result = new double[branches.size()];
	for (int i = 0; i < branches.size(); ++i) {
	    result[i] = scoreSimilarJunction(branches.get(i), junctionIds[i]);
	}
	return result;
	} */

    /** returns array with similarity scores (0.0: identitical 3D transformations). 
     * The junctionIds are typically generated by function findSimilarJunctions. */
    // public double[] scoreSimilarJunctions(List<BranchDescriptor3D> branches, Set<Integer> junctionIds) {
// 	double[] result = new double[branches.size()];
// 	Iterator<Integer> it = junctionIds.iterator();
// 	int i = 0;
// 	while (it.hasNext()) {
// 	    result[i++] = scoreSimilarJunction(branches.get(i), it.next());
// 	}
// 	return result;
//     }

    /** returns n'th junction of specified order. Example: for 3-way junctions the order is 3. */
    public LinkSet getJunctionLinkSet(int order, int n) {
	if ((strandJunctionLinkSets == null) || (order < 0) || (order >= strandJunctionLinkSets.length)
	    || (n >= strandJunctionLinkSets[order].length)) {
	    return null;
	}
	return strandJunctionLinkSets[order][n];
    }
    
    /** returns total number of defined junctions. */
    public int getJunctionCount() {
	log.fine("SimpleStrandJunctionDB: Called getJunctionCount");
	try {
	    int sum = 0;
	    log.fine("Size: " + size());
	    log.fine("strandJunctions: " + strandJunctions);
	    log.fine("strandJunctions.length: " + strandJunctions.length);
	    for (int i = 0; i < size(); ++i) {
		sum += size(i);
		log.fine("Size(i): " + size(i));
		log.fine("Sum: " + sum);
	    }
	    return sum;
	}
	catch (NullPointerException e) {
	    log.info("Junction DB does not contain junctions yet.");
	    return 0;
	}
    }
    
    /** returns true if database is defined */
    public boolean isValid() { 
	return (strandJunctions != null) && (strandJunctions.length > 0);
    }

    /** Scores how well a loop would fit into a scaffold */
    public static Properties scoreJunctionLoopFit(BranchDescriptor3D bScaff1, BranchDescriptor3D bScaff2,
						  BranchDescriptor3D bLoop1, BranchDescriptor3D bLoop2,
						  int n1, int n2, double rotationWeight) {
	Matrix4D targetTrafo = BranchDescriptorTools.computeBranchDescriptorFlippedTransformation(bScaff1, bScaff2);
	log.fine("Trying to bridge: " + bScaff1.getFullName() + " " + bScaff2.getFullName() + " " + targetTrafo);
	log.fine("Trying to bridge from scaff1: " + bScaff1.getCoordinateSystem() + " " + bScaff1.getCoordinateSystem().generateMatrix4D());
	log.fine("Trying to bridge from scaff2: " + bScaff2.getCoordinateSystem() + " " + bScaff2.getCoordinateSystem().generateMatrix4D());
	log.fine("Trying to bridge from loop1: " + bLoop1.getCoordinateSystem() + " " + bLoop1.getCoordinateSystem().generateMatrix4D());
	log.fine("Trying to bridge from loop2: " + bLoop2.getCoordinateSystem() + " " + bLoop2.getCoordinateSystem().generateMatrix4D());
	Matrix4D bLoop1Flip = BranchDescriptorTools.computeBranchDescriptorInvertedTransformation(bLoop1.getCoordinateSystem().generateMatrix4D(),0);
	Matrix4D trans = bLoop1Flip.inverse().multiply(bLoop2.getCoordinateSystem().generateMatrix4D());
	Matrix4D trans2 = HELIX_MATRIX4D.pow(n1+1).multiply(trans).multiply(HELIX_MATRIX4D.pow(n2+1)); // +1: indicates propagation by one base pair
	log.fine("Found bridge: " + bScaff1.getFullName() + " " + bScaff2.getFullName() + " " + trans + "\ntwo empty bp shifts:\n" + trans2);
	double score = Matrix4DTools.scoreFit(targetTrafo, trans2, rotationWeight);
	return Matrix4DTools.generateMatrix4DFitResult(score, n1, n2);
    }

    /** Scores how well a junction would fit into target loop */
    Properties scoreJunctionLoopFit(StrandJunction3D junction, int b1, int b2, 
				    int n1Min, int n1Max, int n2Min, int n2Max,
				    Matrix4D targetTrafo) {
	BranchDescriptor3D branch1 = junction.getBranch(b1);
	BranchDescriptor3D branch2 = junction.getBranch(b2);
	// flip first branch descriptor, overall path (after inverse) points from b1 to b2 
	Matrix4D queryTrafo = BranchDescriptorTools.computeBranchDescriptorFlippedTransformation(branch2, branch1).inverse();
	log.fine("Starting to rank loop fittings for brand descriptors " 
		 + junction.getBranch(b1).getFullName() + " "
		 + " and " + junction.getBranch(b2).getFullName());
	log.fine("cs1: " + branch1.getCoordinateSystem().toString());
	log.fine("cs2: " + branch2.getCoordinateSystem().toString());
	log.fine("query transformation: " + queryTrafo);
	// add one in helix propagation matrix: correponds to "next" base pair 
	Properties result = Matrix4DTools.scoreFit(targetTrafo, queryTrafo, RnaConstants.HELIX_MATRIX4D, RnaConstants.HELIX_MATRIX4D, n1Min+1, n1Max+1, n2Min+1, n2Max+1); 
	assert result != null;
	result.setProperty("branch_id1", "" + b1);
	result.setProperty("branch_id2", "" + b2);
	return result;
    }


    /** ranks 2D junctions for interpolating loops defined by outside stems b1 and b2 pointing inwards (towards loop) */
    public List<Properties> rankLoopFits(BranchDescriptor3D b1, BranchDescriptor3D b2,
			 int n1Min, int n1Max, int n2Min, int n2Max) {
	// invert orientations of branch descriptor of outside frame to be fitted:
	Matrix4D targetTrafo = BranchDescriptorTools.computeBranchDescriptorFlippedTransformation(b1, b2);
	log.fine("Starting to rank loop fittings for brand descriptors " + b1.getFullName() + " "
		 + " and " + b2.getFullName() );
	log.fine("cs1: " + b1.getCoordinateSystem().toString());
	Matrix4D mh2a = b2.getCoordinateSystem().generateMatrix4D();
	Matrix4D mh2b = BranchDescriptorTools.computeBranchDescriptorInvertedTransformation(mh2a,1);
	Matrix4D mh2c = BranchDescriptorTools.computeBranchDescriptorInvertedTransformation(mh2b, 1);
	assert (mh2a.minus(mh2c)).norm() < 0.1; // inversion applied twice should be original
	log.fine("cs2: " + b2.getCoordinateSystem().toString() + " ; flipped: "

		 + mh2b + " " + (new CoordinateSystem3D(mh2b)).toString());
	log.fine("target transformation: " + targetTrafo);
	List<Properties> result = new ArrayList<Properties>();
	int order = 2;
	int bId1 = 0;
	int bId2 = 1; // currently search only 2-way junctions
	double bestScore = 1e30;
	int bestId = 0;
	for (int i = 0; i < size(order); ++i) { // loop over all 2-way junctions
	    StrandJunction3D junction = getJunction(order, i);
	    Properties properties = scoreJunctionLoopFit(junction, bId1, bId2, n1Min, n1Max, n2Min, n2Max, targetTrafo);
	    properties.setProperty("junction_id", "" + i);
	    properties.setProperty("junction_order", "" + order);
	    properties.setProperty("junction_direction", "forward");
	    properties.setProperty("junction_type", junction.getClassName());
	    double score = Double.parseDouble(properties.getProperty("score"));
	    result.add(properties);
	    if (score < bestScore) {
		bestId = result.size()-1;
		bestScore = score;
	    }
	    Properties properties2 = scoreJunctionLoopFit(junction, bId2, bId1, n1Min, n1Max, n2Min, n2Max, targetTrafo); // try fitting other way around
	    properties2.setProperty("junction_id", "" + i);
	    properties2.setProperty("junction_order", "" + order);
	    properties2.setProperty("junction_type", junction.getClassName());
	    properties2.setProperty("junction_direction", "backward");
	    result.add(properties2);
	    score = Double.parseDouble(properties2.getProperty("score"));
	    if (score < bestScore) {
		bestId = result.size()-1;
		bestScore = score;
	    }
	}
	if (bestId != 0) {
	    log.fine("Swapping! 0 and " + bestId);
	    // swap such that first junction is best fit:
	    Properties tmpProperties = result.get(0);
	    result.set(0, result.get(bestId));
	    result.set(bestId, tmpProperties);
	}
	return result;
    }

    /** reads dababase for junctions of size n from stream. */
    private void readDimension(InputStream is, int n) throws IOException {
	DataInputStream dis = new DataInputStream(is);
	String line = dis.readLine();
	line = line.trim();
	int numEntries = Integer.parseInt(line);
	strandJunctions[n] = new StrandJunction3D[numEntries];
	// read each junction
	for (int i = 0; i < numEntries; ++i) {
	    try {
		strandJunctions[n][i] = (StrandJunction3D)(reader.readAnyObject3D(dis));
	    }
	    catch (Object3DIOException e) {
		clear();
		throw new IOException("StrandJunction3D parsing error: entry " + (i+1) + " of dimension " + n
				      + " ; " + e.getMessage());
	    }
	}
    }

    /** reads dababase from stream.  */ 
    public void read(InputStream is) throws IOException {
	DataInputStream dis = new DataInputStream(is);
	// read in number of dimensions filled (+1):
	String line = dis.readLine();
	line = line.trim();
	int numDim = Integer.parseInt(line);
	strandJunctions = new StrandJunction3D[numDim][0];
	// read each dimension:
	for (int dim = 0; dim < numDim; ++dim) {
	    readDimension(dis, dim);
	}
    }

    /** Retrieves all database elements compatible with this descriptor as one list */
    public List<StrandJunction3D> retrieveJunctions(DBElementDescriptor dbe) {
	StrandJunction3D[] jnc = getJunctions(dbe.getOrder());
	List<StrandJunction3D> result = new ArrayList<StrandJunction3D>();
	if (jnc != null) {
	    if ((dbe.getId() >= 0) && (dbe.getId() < jnc.length)) {
		result.add(jnc[dbe.getId()]);
	    }
	    else {
		// no id specified, use all junctions of this type:
		for (StrandJunction3D jc : jnc) {
		    result.add(jc);
		}
	    }
	}
	return result;
    }

    /** Retrieves all database elements compatible as one list */
    public List<StrandJunction3D> retrieveAllJunctions() {
	List<StrandJunction3D> result = new ArrayList<StrandJunction3D>();
	for (int order = 0; order < strandJunctions.length; ++order) {
// 	    int id = 1;
// 	    int type = DBElementDescriptor.JUNCTION_TYPE;
// 	    int descriptorId = -1;
// 	    DBElementDescriptor dbe = new DBElementDescriptor(order, id, type, descriptorId); // no id, type or descriptorId specified
	    for (int id = 0; id < strandJunctions[order].length; ++id) {
		result.add(strandJunctions[order][id]);
	    }
	}
	if (result.size() != getJunctionCount()) { // all elements must be there
	    log.severe("Internal error in method retrieve all junctions: " + result.size() + " " + getJunctionCount());
	}
	// assert result.size() == getJunctionCount(); // all elements must be there
	return result;
    }

    /** Retrieves all database elements compatible as one list */
    public List<DBElementDescriptor> retrieveAllDescriptors() {
	List<DBElementDescriptor> result = new ArrayList<DBElementDescriptor>();
	int descriptorId = 0;
	for (int order = 0; order < strandJunctions.length; ++order) {
// 	    int id = 1;
// 	    int type = DBElementDescriptor.JUNCTION_TYPE;
// 	    int descriptorId = -1;
// 	    DBElementDescriptor dbe = new DBElementDescriptor(order, id, type, descriptorId); // no id, type or descriptorId specified
	    for (int id = 0; id < strandJunctions[order].length; ++id) {
		result.add(new DBElementDescriptor(order, id, junctionType, descriptorId++));
	    }
	}
	assert(result.size() == getJunctionCount());
	return result;
    }

    /** returns number of defined dimensions */
    public int size() { 
	if (strandJunctions == null) {
	    return 0;
	}
	return strandJunctions.length;
    }

    /** returns number of entries of dimension n */
    public int size(int n) { 
	if ((strandJunctions == null) || (n >= strandJunctions.length)) {
	    return 0;
	}
	return strandJunctions[n].length;
    }

    /** returns info string */
    public String infoString() {
	if (!isValid()) {
	    return "No junctions defined in database!";
	}
	// NanoTiler.numberOfKissingLoops = 0;
	StringBuffer buf = new StringBuffer();
	for (int i = 2; i < strandJunctions.length; ++i) {
	    // NanoTiler.numberOfKissingLoops += size(i);
	    buf.append("Junctions of order " + i + ": " + size(i) + NEWLINE);
//  	    for (int j = 0; j < strandJunctions[i].length; ++j) {
//  		buf.append("Junction " + i + " " + (j+1) + " : " + NEWLINE);
//  		// Object3DTools.printTree(buf, getJunction(i,j));
//  		buf.append(NEWLINE);
//  	    }
	}
	return buf.toString();
    }

    public String hashesToString() {
	StringBuffer buf = new StringBuffer();
	for (int i = 0; i < hashes.size(); ++i) {
	    if (size(i) > 0) {
		buf.append("# junctions of order " + i + ":" + NEWLINE);
		buf.append(hashes.get(i).toString() + NEWLINE);		
	    }
	}
        return buf.toString();
    }

    public void setName(String s) { this.name = s; }

    /** string representation */
    public String toString() {
	StringBuffer buf = new StringBuffer();
	buf.append("" + size() + NEWLINE); // number of entries
	for (int i = 0; i < size(); ++i) {
	    buf.append("" + strandJunctions[i].length + NEWLINE);
	    for (int j = 0; j < strandJunctions[i].length; ++j) {
		buf.append(writer.writeString(strandJunctions[i][j]));
	    }
	}
	return buf.toString();
    }

}
