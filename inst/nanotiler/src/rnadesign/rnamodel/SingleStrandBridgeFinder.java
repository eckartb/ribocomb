package rnadesign.rnamodel;

import tools3d.*;
import tools3d.objects3d.*;
import java.util.*;
import java.util.logging.*;

import static rnadesign.rnamodel.PackageConstants.*;

/** Interface for classes that find 3D bridges between two objects */
public class SingleStrandBridgeFinder implements BridgeFinder {

    public static final int FRAGMENT_DIST_MAX = 40;

    private static Logger log = Logger.getLogger(LOGGER_NAME);
    public static final double SOLUTION_MAX_FACTOR = 5.0; // multiply this value with solutionMax to determine quitting of search
    private List<Object3DLinkSetBundle> bundleList;
    int minBridge = 1; // length 0 is pointless
    int maxBridge = 10; //  maximum number of bridging residues
    double rms = 3.0;
    double angleWeight = 1.0;
    double angleBendLimit = Math.toRadians(180); // switched off; was: Math.toRadians(120.0); // no U-turns, thus not allowing bridges making hairpin
    double angleBendLimitDist = 50.0; // 11.0; // indication of base pairing distance
    double collisionDistance = 1.0;
    int solutionMax = 10;
    boolean shortestMode = false;
    List<List<List<RnaFragment> > > fragments; // for a given fragment length and distance in Angstroem (integer) from O3' of upstream adjacent residue and P atom to downstream adjacent residue
    Object3DSet strands; // stores references to all strands of bundleList
    String upAnchorAtom = "O3*";
    String downAnchorAtom = "P";

    public SingleStrandBridgeFinder(List<Object3DLinkSetBundle> bundleList) {
	this.bundleList = bundleList;
	init();
    }
    
//     public SingleStrandBridgeFinder() {
// 	this.bundleList = new ArrayList<Object3DLinkSetBundle>();
//     }

    public List<Object3DLinkSetBundle> findBridge(Object3D obj1, Object3D obj2) {
	assert (obj1 != null) || (obj2 != null);
	List<Object3DLinkSetBundle> result = new ArrayList<Object3DLinkSetBundle>();
	for(Object3DLinkSetBundle bundle : bundleList) {
	    List<Object3DLinkSetBundle> tmpResult = null;
	    if ((obj1 != null) && (obj2 != null)) {
		tmpResult = findBridgeInBundle(obj1, obj2, bundle);
	    }
	    else if (obj1 != null) {
		tmpResult = findBridgeHeadInBundle(obj1, bundle, maxBridge);
	    }
	    else {
		assert obj2 != null;
		tmpResult = findBridgeHeadInBundle(obj2, bundle, maxBridge);
	    }
	    if (tmpResult != null) {
		result.addAll(tmpResult);
	    }
	}
	return result;
    }

    public List<Object3DLinkSetBundle> findBridgeHeadInBundle(Object3D obj1, Object3DLinkSetBundle bundle, int len) {
	log.fine("Started single strand bridge head finder: " + obj1.getFullName() + " using length " + len);
	if (!(obj1 instanceof Nucleotide3D)) {
	    log.warning("Single strand bridge search only defined between nucleotides: " + obj1.getFullName());
	    return null;
	} 
	if (obj1.getParent() == null) {
	    log.warning("Residue must be child node of nucleotide strand!");
	    return null;
	}
	Nucleotide3D nuc1 = (Nucleotide3D) obj1;
	boolean appendAtEnd = true;
	if (nuc1.getSiblingId() == 0) {
	    appendAtEnd = false;
	}
	else if (nuc1.getSiblingId() != (nuc1.getParent().size()-1)) {
	    log.warning("Nucleotide must be first or last residue of strand in order to be bridge head use for extension.");
	    return null;
	}
	List<Object3DLinkSetBundle> result =new ArrayList<Object3DLinkSetBundle>();
	Object3D dbObj = bundle.getObject3D();
	Object3DSet strands = Object3DTools.collectByClassName(dbObj, "RnaStrand");
	for (int i = 0; i < strands.size(); ++i) {
	    List<Object3DLinkSetBundle> tmpResult = findSingleStrandBridgeHead(nuc1, (RnaStrand)(strands.get(i)), len, appendAtEnd);
	    result.addAll(tmpResult);
	}
	return result;
    }

    public List<Object3DLinkSetBundle> findBridgeInBundle(Object3D obj1, Object3D obj2, Object3DLinkSetBundle bundle) {
	log.info("Started single strand bridge finder: " + obj1.getFullName() + " " + obj2.getFullName() + " distance: " + obj1.distance(obj2));
	if (!(obj1 instanceof Nucleotide3D)) {
	    log.warning("Single strand bridge search only defined between nucleotides: " + obj1.getFullName() + " " + obj2.getFullName());
	    return null;
	} 
	if (!(obj2 instanceof Nucleotide3D)) {
	    log.warning("Single strand bridge search only defined between nucleotides: " + obj1.getFullName() + " " + obj2.getFullName());
	    return null;
	} 
	Nucleotide3D nuc1 = (Nucleotide3D) obj1;
	Nucleotide3D nuc2 = (Nucleotide3D) obj2;
	List<Object3DLinkSetBundle> result =new ArrayList<Object3DLinkSetBundle>();
	if (!validate(nuc1, nuc2)) {
	    log.info("Nucleotides " + nuc1.getFullName() + " " + nuc2.getFullName() + " not bridgeable!");
	    return result;
	}

 	for (int bridgeLength = minBridge; bridgeLength <= maxBridge; ++bridgeLength) {
 	    if (bridgeLength == 0) {
 		continue; // no search necessary
 	    }
 	    log.info("Searching for bridge of length " + bridgeLength);
 	    List<Object3DLinkSetBundle> tmpResult = findSingleStrandBridge(nuc1, nuc2, bridgeLength);
	    if ((tmpResult != null) && (tmpResult.size() > 0)) {
		result.addAll(tmpResult);
		if (shortestMode) {
		    break; // quit after solutions were found for this length
		}
	    }
 	}

	return result;
    }

    public double getRms() { return this.rms; }

    private void init() {
	initStrands();
	initFragments();
    }

    private void initStrands() {
	strands = new SimpleObject3DSet();
	for (int i = 0; i < bundleList.size(); ++i) {
	    Object3DLinkSetBundle bundle = bundleList.get(i);
	    Object3D dbObj = bundle.getObject3D();
	    Object3DSet strands2 = Object3DTools.collectByClassName(dbObj, "RnaStrand");
	    strands.merge(strands2);
	}
    }

    private void initFragments(int bridgeLength, RnaStrand strand) {
	assert bridgeLength < fragments.size();
	int residueCount = strand.getResidueCount();
	int anchorStartMax = residueCount - bridgeLength - 2;
	for (int anchorStart = 0; anchorStart < anchorStartMax ; ++anchorStart) {
	    int anchorStop = anchorStart + bridgeLength + 1;
	    assert anchorStop < strand.getResidueCount();
	    assert anchorStart < strand.getResidueCount() - bridgeLength - 2;
	    Nucleotide3D anchor1 = (Nucleotide3D)(strand.getResidue3D(anchorStart));
	    Nucleotide3D anchor2 = (Nucleotide3D)(strand.getResidue3D(anchorStop));
	    int dist = computeHashDistance(anchor1, anchor2);
	    // int id1 = anchor1.getIndexOfChild(upAnchorAtom);
	    // int id2 = anchor2.getIndexOfChild(downAnchorAtom);
	    if (dist >= 0.0) {
		RnaFragment fragment = new RnaFragment(strand, anchorStart, anchorStop);		    
		assert fragment.length() == bridgeLength + 2;
		if (dist < fragments.get(bridgeLength).size()) {
		    fragments.get(bridgeLength).get(dist).add(fragment);
		}
	    } // otherwise: atoms missing, this is not a good anchor region
	}
    }

    private void initFragments() {
	assert strands != null;
	fragments = new ArrayList<List<List<RnaFragment> > >();
	for (int i = 0; i <= maxBridge; ++i) {
	    ArrayList<List<RnaFragment> > frags = new ArrayList<List<RnaFragment> >();
	    for (int j = 0; j < FRAGMENT_DIST_MAX+1; ++j) {
		frags.add(new ArrayList<RnaFragment>());
	    }
	    fragments.add(frags);
	}

	for (int bridgeLength = minBridge; bridgeLength <= maxBridge; ++bridgeLength) {
	    for (int j = 0; j < strands.size(); ++j) {
		RnaStrand strand = (RnaStrand)(strands.get(j));
		initFragments(bridgeLength, strand);
	    }
	}
    }

    public void setRms(double rms) { this.rms = rms; }

    /** Actual search algorithm. Could be much more efficient with geometric hashing! */
//     public List<Object3DLinkSetBundle> findSingleStrandBridge(Nucleotide3D obj1, Nucleotide3D obj2, RnaStrand strand) {
// 	List<Object3DLinkSetBundle> result =new ArrayList<Object3DLinkSetBundle>();
// 	for (int bridgeLength = minBridge; bridgeLength <= maxBridge; ++bridgeLength) {
// 	    if (bridgeLength == 0) {
// 		continue; // no search necessary
// 	    }
// 	    log.info("Searching for bridge of length " + bridgeLength);
// 	    List<Object3DLinkSetBundle> tmpResult = findSingleStrandBridge(obj1, obj2, strand, bridgeLength);
// 	    if ((tmpResult != null) && (tmpResult.size() > 0)) {
// 		result.addAll(tmpResult);
// 		if (shortestMode) {
// 		    break; // quit after solutions were found for this length
// 		}
// 	    }
// 	}
// 	return result;
//     }

    int computeHashDistance(Nucleotide3D anchor1, Nucleotide3D anchor2) {
	int id1 = anchor1.getIndexOfChild(upAnchorAtom);
	int id2 = anchor2.getIndexOfChild(downAnchorAtom);
	if ((id1 >= 0) && (id2 >= 0)) {		   
	    return (int)(anchor1.getChild(id1).distance(anchor2.getChild(id2)));
	}
	return - 1;  // anchor atoms not found
    }

    /** Actual search algorithm. Could be much more efficient with geometric hashing! */
    public List<Object3DLinkSetBundle> findSingleStrandBridge(Nucleotide3D obj1, Nucleotide3D obj2, int bridgeLength) {
	List<Object3DLinkSetBundle> result = new ArrayList<Object3DLinkSetBundle>();
	Object3D root = Object3DTools.findRoot(obj1); // used for collision check
	Object3D root2 = Object3DTools.findRoot(obj2); // must have same root
	int hashDistance = computeHashDistance(obj1, obj2);
	if (hashDistance < 0 || bridgeLength >= fragments.size() || hashDistance >= fragments.get(bridgeLength).size()) {
	    log.warning("Could not find bridging fragments.");
	    return result; 
	}
	assert (hashDistance >= 0);
	List<RnaFragment> frags = fragments.get(bridgeLength).get(hashDistance); // obtain list of all applicable fragments
	// obtain fragments corresponding to neighboring hash values
	if (hashDistance > 0) {
	    frags.addAll(fragments.get(bridgeLength).get(hashDistance-1)); // obtain list of all applicable fragments
	}
	if ((hashDistance + 1) < fragments.get(bridgeLength).size()) {
	    frags.addAll(fragments.get(bridgeLength).get(hashDistance+1)); // obtain list of all applicable fragments
	}

	// for (int i = 0; i < strand.getResidueCount(); ++i) { // most time-consuming step: linear search ... replace with faster algorithm
	log.info("Found " + frags.size() + " potential fragments!");
	for (int i = 0; i < frags.size(); ++i) { // most time-consuming step: linear search ... replace with faster algorithm

	    RnaFragment frag = frags.get(i);
	    log.fine("Trying fragment " + (i+1) + " : " + frag.toString());
	    assert frag.length() == (bridgeLength + 2);
	    int startPos = frag.startPos;
	    int stopPos = frag.stopPos; // i + bridgeLength + 1;
	    RnaStrand strand = frag.strand;
	    if (stopPos >= strand.getResidueCount()) {
		break;
	    }
	    Nucleotide3D otherStart = (Nucleotide3D)(strand.getResidue3D(startPos));
	    Nucleotide3D otherStop = (Nucleotide3D)(strand.getResidue3D(stopPos));
	    try {
		SuperpositionResult supResult = scoreSuperposition(obj1, obj2, otherStart, otherStop);
		if ((supResult != null) && (supResult.getRms() < rms)) {
		    // clone bridge:
		    Object3D bridgePart = (Object3D)(strand.cloneDeep(startPos+1, stopPos)); // do not include anchors ... Also, second index is 1-based (non-inclusive)
		    assert bridgePart.size() == bridgeLength;
		    supResult.applyTransformation(bridgePart);	
		    // count collisions:
		    int collCount = 0;
		    if (collisionDistance > 0.0) {
			collCount = AtomTools.countExternalCollisions(bridgePart, root, collisionDistance);
			if (root != root2) {
			    collCount += AtomTools.countExternalCollisions(bridgePart, root2, collisionDistance);
			}
		    }
		    if (collCount == 0) {
			// now check distance between atoms:
			double d1 = obj1.getChild("O3*").distance(bridgePart.getChild(0).getChild("P"));
			double d2 = obj2.getChild("P").distance(bridgePart.getChild(bridgePart.size()-1).getChild("O3*"));
			// workaround: later we will sort these fragments
			if (d1 <= rms && d2 <= rms) {
			    bridgePart.setZBufValue(supResult.getRms());
			    log.fine("Adding bridge fragment with RMS " + supResult.getRms() + " and O3*-P distances: " + d1 + " " + d2);
			    result.add(new SimpleObject3DLinkSetBundle(bridgePart, new SimpleLinkSet()));
			}
		    }
		} else {
		    log.fine("Bad superposition: " + supResult);
		}
	    }
	    catch (RnaModelException rne) {
		log.warning("Could not superpose " + obj1.getFullName() + " " + obj2.getFullName() + " with " 
			    + otherStart.getFullName() + " " + otherStop.getFullName());
	    }
	}
	return result;
    }

    /** Actual search algorithm. Could be much more efficient with geometric hashing! */
    private List<Object3DLinkSetBundle> findSingleStrandBridgeHead(Nucleotide3D obj1, RnaStrand strand, int bridgeLength,
								   boolean appendAtEnd) {
	List<Object3DLinkSetBundle> result = new ArrayList<Object3DLinkSetBundle>();
	Object3D root = Object3DTools.findRoot(obj1); // used for collision check
	for (int i = 0; i < strand.getResidueCount(); ++i) {
	    int startPos = i;
	    int stopPos = i + bridgeLength + 1;
	    if (stopPos >= strand.getResidueCount()) {
		break;
	    }
	    Nucleotide3D otherStart = (Nucleotide3D)(strand.getResidue3D(startPos));
	    Nucleotide3D otherStop = (Nucleotide3D)(strand.getResidue3D(stopPos));
	    try {
		SuperpositionResult supResult = null;
		if (appendAtEnd) {
		    supResult = scoreHeadSuperposition(obj1, otherStart); // obj1 is end of strand, superpose with beginning of fragment
		}
		else {
		    supResult = scoreHeadSuperposition(obj1, otherStop); // obj1 is begging of strand, superpose with END of fragment
		}
		if ((supResult != null) && (supResult.getRms() < rms)) {
		    // clone bridge:
		    Object3D bridgePart = (Object3D)(strand.cloneDeep(startPos+1, stopPos)); // do not include anchors
		    supResult.applyTransformation(bridgePart);	
		    // count collisions:
		    int collCount = 0;
		    if (collisionDistance > 0.0) {
			collCount = AtomTools.countExternalCollisions(bridgePart, root, collisionDistance);
		    }
		    if (collCount == 0) {
			// workaround: later we will sort these fragments
			bridgePart.setZBufValue(supResult.getRms());
			log.info("Adding bridge head fragment with rms " + supResult.getRms());
			result.add(new SimpleObject3DLinkSetBundle(bridgePart, new SimpleLinkSet()));
			if (result.size() >= (SOLUTION_MAX_FACTOR * solutionMax)) {
			    log.info("Found " + result.size() + " solutions. Quitting bridge head search.");
			    break;
			}
		    }
		}
	    }
	    catch (RnaModelException rne) {
		log.warning("Could not superpose " + obj1.getFullName() + " with " 
			    + otherStart.getFullName());
	    }
	}
	return result;
    }
    
    /** Actual search algorithm. Could be much more efficient with geometric hashing! */
    public SuperpositionResult scoreSuperposition(Nucleotide3D oldRes1, Nucleotide3D oldRes2, Nucleotide3D newRes1, Nucleotide3D newRes2) throws RnaModelException {
	Object3DSet oldTripod1 = NucleotideDBTools.extractAtomTripodBp(oldRes1);
	Object3DSet oldTripod2 = NucleotideDBTools.extractAtomTripodBp(oldRes2);
	Object3DSet newTripod1 = NucleotideDBTools.extractAtomTripodBp(newRes1);
	Object3DSet newTripod2 = NucleotideDBTools.extractAtomTripodBp(newRes2);
	Vector3D[] oldCoord1 = Object3DSetTools.getCoordinates(oldTripod1);
	Vector3D[] newCoord1 = Object3DSetTools.getCoordinates(newTripod1);
	Vector3D[] oldCoord2 = Object3DSetTools.getCoordinates(oldTripod2);
	Vector3D[] newCoord2 = Object3DSetTools.getCoordinates(newTripod2);
	Vector3D[] oldCoord = new Vector3D[oldCoord1.length + oldCoord2.length];
	Vector3D[] newCoord = new Vector3D[newCoord1.length + newCoord2.length];
	for (int i = 0; i < newCoord1.length; ++i) {
	    newCoord[i] = newCoord1[i];
	    oldCoord[i] = oldCoord1[i];
	}
	for (int i = 0; i < newCoord2.length; ++i) {
	    newCoord[i+newCoord1.length] = newCoord2[i];
	    oldCoord[i+oldCoord1.length] = oldCoord2[i];
	}
	if (Vector3DTools.computeDrms(newCoord, oldCoord) > rms) {
	    return null; // Distance RMS too bad, no superposition needed
	}
	// superpose:
	Superpose superposer = new KabschSuperpose(); // new MCSuperpose();
	SuperpositionResult supResult = superposer.superpose(oldCoord, newCoord);
	return supResult;
    }

    /** Actual search algorithm for one-sided bridge (a bridge head or extension). Could be much more efficient with geometric hashing! */
    public SuperpositionResult scoreHeadSuperposition(Nucleotide3D oldRes1, Nucleotide3D newRes1) throws RnaModelException {
	Object3DSet oldTripod1 = NucleotideDBTools.extractAtomTripodBp(oldRes1);
	Object3DSet newTripod1 = NucleotideDBTools.extractAtomTripodBp(newRes1);
	Vector3D[] oldCoord1 = Object3DSetTools.getCoordinates(oldTripod1);
	Vector3D[] newCoord1 = Object3DSetTools.getCoordinates(newTripod1);
	if (Vector3DTools.computeDrms(newCoord1, oldCoord1) > rms) {
	    return null; // Distance RMS too bad, no superposition needed
	}
	// superpose:
	Superpose superposer = new KabschSuperpose(); // new MCSuperpose();
	SuperpositionResult supResult = superposer.superpose(oldCoord1, newCoord1);
	return supResult;
    }

    public void setAngleWeight(double x) { this.angleWeight = x; }

    public int getLenMin() { return this.minBridge; }

    public int getLenMax() { return this.maxBridge; }

    public void setLenMin(int n) { this.minBridge = n; }

    public void setLenMax(int n) { this.maxBridge = n; }

    public void setSolutionMax(int n) { this.solutionMax = n; }

    public boolean validate(Nucleotide3D n1, Nucleotide3D n2) {
	boolean result = false;
	try {
	    double angle = NucleotideTools.getBackboneAngle(n1, n2);
	    double dist = n1.getChild("C1*").distance(n2.getChild("C1*"));
	    if ((angle < angleBendLimit) 
		&& (dist < angleBendLimitDist)) {
		result = true; // everything OK;
		log.info("Single-Strand-Bridge validation result for nucleotides with distance and angle: " + dist + " " + Math.toDegrees(angle) + " : " + result + " " + n1.getFullName() + " "
			 + n2.getFullName());
	    } else {
		log.fine("Problematic single-Strand-Bridge validation result for nucleotides with distance and angle: " + dist + " " + Math.toDegrees(angle) + " : " + result + " " + n1.getFullName() + " "
			 + n2.getFullName());
	    }
	}
	catch (RnaModelException rme) {
	    log.info("Model exception while trying to validate bridge anchor residues: " + rme.getMessage());
	    return false; // probably atoms missing
	}
	return result;
    }


}
