package rnadesign.rnamodel;

import graphtools.*;
import tools3d.objects3d.*;
import tools3d.Vector3D;
import java.math.BigInteger;
import java.math.BigDecimal;
import java.util.*;
import java.util.logging.*;
import generaltools.CollectionTools;

import org.testng.annotations.*;

public class SmallConnectivityIterator implements Iterator<GrowConnectivity> {

    boolean initial = true;
    public static int DEBUG_OUTPUT_INTERVALL = 0; // how often is debug output written
    private static Logger log = Logger.getLogger(PackageConstants.LOGFILE_DEFAULT);
    private double angleTolerance = -1.0; // if greater zero, allow this angle difference between the helices of the physical model and the links 
    /** Container for all permanent data like building block sets, vertices, edges etc */
    private ConnectivityGenerator connGenerator;
    /** Length offset (number - helixLengthVariation) of helix corresponding to n'th edge (=link) */
    private IntegerPermutator helixLengths;
    private BipartiteGraphPermutator bipartite;
    /** For n'th vertex, what building block is current? */
    // private IntegerArrayGenerator vertexIds;
    /** one permutation generator for each vertex. Determines the correspondence between helices of a junction and the edges of a vertex.
     * Rotating through all permutations at a certain vertex corresponds to superposing the junction in all possible ways to a vertex. */
    // private PermutationGenerator[] helixPermutations; 

    /** Master list that keeps track of all combinations */
    private IntegerPermutatorList permutatorList;
    // private int buildingBlockCountMax = 10; // number of non-equivalent building blocks
    // private int connectionCountMax = 10; // number of non-equivalent connections
    private boolean debugMode = false; // true;
    private boolean smartConnectionCheckMode = true;
    private boolean noSkipMode = false;

    private int connAvail = 0; // add all helices of all building blocks
    private int connMax = 0;
    private int[] connectionBBIds;
    private int[] connectionHelixIds;

    /** The constructor is not public on purpose */
    SmallConnectivityIterator(ConnectivityGenerator connGenerator) {
	this.connGenerator = connGenerator;
	this.connMax = connGenerator.getConnectionCountMax();
	init();
	assert validate();
    }

    // public int getBuildingBlockCountMax() { return buildingBlockCountMax; }

    // public int getConnectionCountMax() { return connectionCountMax; }

    private void initConnections() {
	// count connections:
	List<DBElementDescriptor> bbDescs = generateBuildingBlocks();
	int orderSum = 0; // sum of all helices of all building blocks
	for (DBElementDescriptor d : bbDescs) {
	    orderSum += d.getOrder();
	}
	this.connAvail = orderSum;
	if (2*connMax > connAvail) {
	    connMax = connAvail/2;
	}
	this.connectionBBIds = new int[orderSum];
	this.connectionHelixIds = new int[orderSum];
	int pc = 0;
	for (int i = 0; i < bbDescs.size(); ++i) {
	    DBElementDescriptor dbe = bbDescs.get(i);
	    int order = dbe.getOrder();
	    for (int j = 0; j < order; ++j) {
		connectionBBIds[pc] = i;
		connectionHelixIds[pc] = j;
		++pc;
	    }
	}
    }

    private void init() {
	log.info("Starting SmallConnectivityIterator.init()...");
	initConnections();
	helixLengths = new IntegerArrayGenerator(connMax,
						 2 * connGenerator.getHelixLengthVariation() + 1);
// 	helixLengths = new MappedMaxDiffIntegerPermutator(connGenerator.getLinks().size(),
// 							  0, 2 * connGenerator.getHelixLengthVariation() + 1,
// 							  connGenerator.getConnectionCountMax());
	assert helixLengths.validate();
	// adding constraint! Do not allow more than this many different helix lengths
	// IntegerArrayConstraint constraint = new IntegerArrayMaxDifferentConstraint(connGenerator.getConnectionCountMax());
	// helixLengths.addConstraint(constraint);

// 	int[] bbIdSizes = new int[connGenerator.getVertices().size()];
// 	for (int i = 0; i < connGenerator.getBuildingBlockIndices().size(); ++i) {
// 	    bbIdSizes[i] = connGenerator.getBuildingBlockIndices().get(i).size(); // number of building blocks allowed at i'th vertex
// 	}
	// vertexIds = new IntegerArrayGenerator(bbIdSizes);
	// adding constraint for maximum number of different building blocks
	// vertexIds.addConstraint(new IntegerArrayMaxDifferentConstraint(connGenerator.getBuildingBlockCountMax()));
	// assert vertexIds.validate();
// 	helixPermutations = new PermutationGenerator[vertexIds.size()];
// 	for (int i = 0; i < helixPermutations.length; ++i) {
// 	    // find out how many helices / edges are connected to a vertex:
// 	    int numVertexHelices = connGenerator.getLinks().getLinkOrder(connGenerator.getVertices().get(i));
// 	    assert numVertexHelices > 0; // if zero, something went wrong with graph
// 	    helixPermutations[i] = new PermutationGenerator(numVertexHelices);
// 	    assert helixPermutations[i].validate();
// 	}
	permutatorList = new IntegerPermutatorList();
 	permutatorList.add(helixLengths);
	assert (connAvail >= 2*connMax);
	this.bipartite = new BipartiteGraphPermutator(connAvail, connMax);
	permutatorList.add(bipartite);
// 	for (PermutationGenerator p : helixPermutations) {
// 	    permutatorList.add(p);
// 	}
	// permutatorList.add(vertexIds);
	assert permutatorList.validate();
	if (debugMode) {
	    // 	    log.info("Using constraint: " + constraint);
	    log.info("SmallConnectivityIterator:Test-iterating through permutations: " + permutatorList);
	    long count = 1;
	    do {
		++count;
		log.info("Iterator: " + count + " : " + permutatorList.toString());
		assert permutatorList.validate();
	    }
	    while (permutatorList.hasNext() && (permutatorList.inc()));
	    log.info("Counted helix length combinations: " + count);
	    permutatorList.reset();
	    log.info("Status after reset: " + permutatorList);
	    assert permutatorList.hasNext();
	    log.info("Now starting self-test of iterator: ");
	    permutatorList.testCurrentTotal();
	    assert !permutatorList.hasNext();
	    log.info("Successful self-testing of iterator!");
	    permutatorList.reset();
	}
	assert validate();
	log.info("Finished SmallConnectivityIterator.init()");
    }

    /** Returns index of object, testing for == operator */
//     private int indexOf(List<? extends Object> list, Object obj) {
// 	for (int i = 0; i < list.size(); ++i) {
// 	    if (list.get(i) == obj) {
// 		return i;
// 	    }
// 	}
// 	return -1;
//     }

    /** For a given distance, returns ideal number of base pairs */
    private int estimateNumberBasePairs(double distance) {
	return (int)(distance / RnaConstants.HELIX_RISE + 0.5);
    }

    int countIdenticalDuplicates(List list) {
	int count = 0;
	for (int i = 0; i < list.size(); ++i) {
	    for (int j = i+1; j < list.size(); ++j) {
		if (list.get(i) == list.get(j)) {
		    ++count;
		}
	    }
	}
	return count;
    }

    /** Generates the n'th connection corresponding to n'th edge in graph */
    /*
    private DBElementConnectionDescriptor generateConnection(int n, List<DBElementDescriptor> buildingBlocks) {
	assert n < connGenerator.getLinks().size();
	Link link = connGenerator.getLinks().get(n);
	// find index of vertex:
	Object3D obj1 = link.getObj1();
	Object3D obj2 = link.getObj2();
	List<? extends Object3D> vertices = connGenerator.getVertices();
	assert countIdenticalDuplicates(vertices) == 0;
	int id1 = CollectionTools.indexOfIdentical(vertices, obj1);
	int id2 = CollectionTools.indexOfIdentical(vertices, obj2);
	log.fine("Attempting to generating connection between " + obj1.getFullName() + " " + obj2.getFullName() + " " + n
		 + " number of building blocks: " + buildingBlocks.size() + " bb ids: " + id1 + " " + id2);
	assert id1 >= 0;
	assert id2 >= 0;
	assert id1 != id2;
	assert id1 < vertexIds.size();
	assert id2 < vertexIds.size();
	assert vertices.size() == vertexIds.size();
	assert vertices.size() == buildingBlocks.size();
	int bbid1 = id1; // vertexIds.get()[id1]; // id of building block
	int bbid2 = id2; // vertexIds.get()[id2];
	DBElementDescriptor dbe1 = buildingBlocks.get(bbid1); // there should be one building block defined for each vertex
	DBElementDescriptor dbe2 = buildingBlocks.get(bbid2);
	int bpIdeal = estimateNumberBasePairs(obj1.distance(obj2));
	int bpOffset = helixLengths.get()[n] - connGenerator.getHelixLengthVariation();
	int bp = bpIdeal + bpOffset;
	if (bp < 0) {
	    return null; // cannot be generated, helix would have negative length
	}
	int linkRank1 = connGenerator.getLinks().getLinkRank(obj1, link); // would this edge be counted as edge 0, 1, ... or n ?
	int linkRank2 = connGenerator.getLinks().getLinkRank(obj2, link); // would this edge be counted as edge 0, 1, ... or n ?
	assert linkRank1 >= 0;
	assert linkRank2 >= 0; // must be found at all
	assert linkRank1 < helixPermutations[id1].size();
	assert linkRank2 < helixPermutations[id2].size();
	int helixId1 = helixPermutations[id1].get()[linkRank1];
	int helixId2 = helixPermutations[id2].get()[linkRank2];
	return new DBElementConnectionDescriptor(dbe1, dbe2, helixId1, helixId2, bp);
    }
    */

    /** Generates the n'th connection corresponding to n'th edge in graph */
    private DBElementConnectionDescriptor generateConnection(int bbid1, int bbid2, int helixId1, int helixId2, int bp,
							     List<DBElementDescriptor> buildingBlocks) {
	DBElementDescriptor dbe1 = buildingBlocks.get(bbid1); // there should be one building block defined for each vertex
	DBElementDescriptor dbe2 = buildingBlocks.get(bbid2);
	return new DBElementConnectionDescriptor(dbe1, dbe2, helixId1, helixId2, bp);
    }


    /** Returns total number of steps */
    public BigInteger getTotal() { 
	return permutatorList.getTotal();
    }

    /** Returns direction of certain helix of certain building block */
    private Vector3D getHelixDirection(int dbElementId, int helixId) {
	return connGenerator.getBuildingBlocksHelixDirections().get(dbElementId).get(helixId);
    }

    /** Computues angle between two vectors */
    private double computeAngle(int dbElementId, int helixId1, int helixId2) {
	double angle = getHelixDirection(dbElementId, helixId1).angle(getHelixDirection(dbElementId, helixId2));
	return angle;
    }

    private double computeAngle(DBElementConnectionDescriptor conn1, DBElementConnectionDescriptor conn2) {
	// find common building block:
	if (conn1.getDescriptor1() == conn2.getDescriptor1()) {
	    int dbId = CollectionTools.indexOf(connGenerator.getBuildingBlocks(),conn1.getDescriptor1());
	    assert false;
	    return computeAngle(dbId, conn1.getHelixendId1(), conn2.getHelixendId1()); // contains error!
	}
	else if (conn1.getDescriptor1() == conn2.getDescriptor2()) {
	    int dbId = CollectionTools.indexOf(connGenerator.getBuildingBlocks(),conn1.getDescriptor1());
	    assert false;
	    return computeAngle(dbId, conn1.getHelixendId1(), conn2.getHelixendId1()); // contains error!
	}
	assert false;
	return -1; // should never be here
    }

    public boolean validateAngle(DBElementConnectionDescriptor newConn,
				 DBElementConnectionDescriptor sofarConn,
				 double linkAngle,
				 double angleTolerance) {
	assert newConn != null && sofarConn != null;
	double angle = computeAngle(newConn, sofarConn);
	return (angleTolerance <= 0.0) || (Math.abs(angle - linkAngle) <= angleTolerance);
    }

    // @Test(groups={"new"})
    public void testValidateAngle1() {
	DBElementConnectionDescriptor newConn = null;
	DBElementConnectionDescriptor sofarConn = null;
	double linkAngle = 0.1;
	double angleTolerance = Math.toRadians(20.0);
	assert !validateAngle(newConn, sofarConn, linkAngle, angleTolerance);
    }

    /** Generates list of connections, one connection for each link */
    private List<DBElementConnectionDescriptor> generateConnections(List<DBElementDescriptor> buildingBlocks) {
	List<DBElementConnectionDescriptor> result = new ArrayList<DBElementConnectionDescriptor>();
	// int numConn = connGenerator.getConnectionCountMax();
	int[] bipartiteConn = bipartite.get();
	int[] helLen = helixLengths.get();
	// int[] linkIds = new int[connGenerator.getLinks().size()]; // stores for each connection to which link it belongs
	for (int i = 0; i < connMax; ++i) {
	    int c1 = bipartiteConn[2*i];
	    int c2 = bipartiteConn[2*i+1];
	    int bb1 = connectionBBIds[c1];
	    int bb2 = connectionBBIds[c2];
	    int h1 = connectionHelixIds[c1];
	    int h2 = connectionHelixIds[c2];
	    int bp = helLen[i];
	    DBElementConnectionDescriptor newConn = generateConnection(bb1, bb2, h1, h2, bp, buildingBlocks);
	    if (newConn == null) { // one connection could not be generated, so quite the whole method
		return null;
	    }
	    assert newConn != null;
	    result.add(newConn);
	}
	// assert result != null && (!result.contains(null));
	return result;
    }

    /** Generates set of used building blocks. If for example a total number of one building block is defined for a graph
     * of three vertices, this method returns a list of 3 copies of the building block descriptor for the only building block in the total set.
     * There is always one building block for each graph vertex returned */
    private List<DBElementDescriptor> generateBuildingBlocks() {
	List<DBElementDescriptor> bb = connGenerator.getBuildingBlocks(); // total set of building blocks
// 	List<DBElementDescriptor> result = new ArrayList<DBElementDescriptor>();
// 	for (int i = 0; i < vertexIds.size(); ++i) {
// 	    DBElementDescriptor dbe = (DBElementDescriptor)((bb.get(vertexIds.get()[i])).clone());
// 	    if (dbe.getOrder() != connGenerator.getVertexOrder(i)) {
// 		log.info("Skipping building block generation because building block order does not match vertex order.");
// 		assert false; // should never happen!?
// 		return null;
// 	    }
// 	    dbe.setDescriptorId(i); // make them distinguishable TODO : should that be i+1?
// 	    result.add(dbe);
// 	}
// 	assert result.size() == vertexIds.size();
	// assert result != null && (!result.contains(null));
	return bb;
    }
    
    public boolean hasNext() { return permutatorList.hasNext(); }

    public GrowConnectivity getGrowConnectivity() {
	log.info("Starting getGrowConnectivity");
	List<DBElementDescriptor> buildingBlocks = generateBuildingBlocks();
	if (buildingBlocks == null) {
	    return null; // simple building block check failed, no building blocks could be generated
	}
	List<DBElementConnectionDescriptor> connections = generateConnections(buildingBlocks);
	if (connections == null || buildingBlocks.contains(null) || connections.contains(null)) {
	    return null;
	}
	GrowConnectivity growConn =  new GrowConnectivity(buildingBlocks, connections, null, connGenerator.getNumGenerations());
	String topol = connGenerator.getTopology();
	if ((topol != null) && (topol.length() > 0)) {
	    growConn.addTopology(topol);
	}
	// 	growConn.setGraphFlag(true); // the whole point of "graph flag" : no infinite growing, but one-to-one correspondence between nodes
	// 	assert growConn.isGraphFlag();
	if (!validateConnectivity(growConn)) {
	    if (initial) {
		initial = false;
		try {
		    inc();
		}
		catch (IllegalAccessException e) {
		    return null; // could not find solution
		}
		return getGrowConnectivity();
	    }
	    else {
		return null; // could not find any further solutions
	    }
	}
	assert validateConnectivity(growConn);
	log.info("Generated grow connectivity corresponding to permutator status: " + permutatorList.toString());
	return growConn;
    }

    public GrowConnectivity next() {
	if (!hasNext()) {
	    return null;
	}
	try {
	    inc();
	}
	catch (IllegalAccessException e) {
	    return null; // TODO : not nice case: hasNext() was probably true and yet we return null TODO
	}
	return getGrowConnectivity();
    }

    /** Increases iteration. Should only be called if it is clear the in can be increased! */
    private void inc() throws IllegalAccessException {
	assert hasNext();
	GrowConnectivity connectivity = null;
	int incCounter = 1;
	int connectionCountMax = connMax; // connGenerator.getConnectionCountMax();
	int buildingBlockCountMax = connGenerator.getBuildingBlockCountMax();
	do {
	    log.info("another do iteration in SmallConnectivityIterator.inc()...");
	    if (!permutatorList.hasNext()) {
		throw new IllegalAccessException("No increasing possible!");
	    }
	    if ((DEBUG_OUTPUT_INTERVALL > 0) && ((incCounter++ % DEBUG_OUTPUT_INTERVALL) == 0)) {
		log.info("Increasing building block connectivity from (" + incCounter++ 
			 + "): " + getGrowConnectivity());
	    }
	    permutatorList.inc(); // increase unified counters
	    if (permutatorList.validate() ) {
		log.info("Trying grow connectivity corresponding to permutator status: " + permutatorList.toString());
		connectivity = getGrowConnectivity();
		if (connectivity != null) {
		    if (connectivity.getNonEquivalentBuildingBlockCount() > buildingBlockCountMax) {
			log.info("Skipping to next building block during increment");
			assert false;
			// skipToNextBuildingBlock();
		    }
		    if (connectivity.getNonEquivalentConnectionCount() > connectionCountMax) {
			log.info("Skipping to next connection during increment");
			assert false;
			// skipToNextConnection();
		    }
		}
	    }
	}
	while (permutatorList.hasNext() && (connectivity == null) || (!validateConnectivity(connectivity))); // check if "symmetry" constraints are ok
	if ((connectivity == null) || (! validateConnectivity(connectivity))) {
	    throw new IllegalAccessException("SmallConnectivityIterator.inc: Could not find solution!");
	}
    }

    /** Returns true if connectivity has allowed number of non-equivalent building blocks and connections */
    public boolean validateConnectivity(GrowConnectivity connectivity) {
	if (connectivity == null || connectivity.getConnections() == null || connectivity.getBuildingBlocks() == null) {
	    return false;
	}
	boolean check1 = !connectivity.getConnections().contains(null);
	boolean check2 = !connectivity.getBuildingBlocks().contains(null);
	int connectionCountMax = connMax; // connGenerator.getConnectionCountMax();
	int buildingBlockCountMax = connGenerator.getBuildingBlockCountMax();
	if (!check1) {
	    return false;
	}
	if (!check2) {
	    return false;
	}
	log.fine("Validating connectivity: " + connectivity.getNonEquivalentBuildingBlockCount() + " "
		 + buildingBlockCountMax + " "
		 + connectivity.getNonEquivalentConnectionCount() + " " + connectionCountMax);
        boolean result = (connectivity.getNonEquivalentBuildingBlockCount() <= buildingBlockCountMax)
	    && (connectivity.getNonEquivalentConnectionCount() <= connectionCountMax);
	if (!result) {
	    log.fine("Invalid connectivity encountered: " + connectivity 
		     + " non-equiv blocks: " + connectivity.getNonEquivalentBuildingBlockCount() 
		     + " non-equiv conn: " + connectivity.getNonEquivalentConnectionCount());
	}
	else {
	    log.fine("Valid connectivity encountered: " + connectivity 
		     + " non-equiv blocks: " + connectivity.getNonEquivalentBuildingBlockCount() 
		     + " non-equiv conn: " + connectivity.getNonEquivalentConnectionCount());
	}
	return result;
    }

    // public void setBuildingBlockCountMax(int n) { this.buildingBlockCountMax = n; }

    // public void setConnectionCountMax(int n) { this.connectionCountMax = n; }
 
    public boolean validate() {
	if (permutatorList == null) {
	    return false;
	}
	return permutatorList.validate();
    }

    /** Removal of last element. Not implemented. */
    public void remove() { 
	assert false;
    }

    /** reset all iterators */
    public void reset() {
	permutatorList.reset();
	assert validate();
// 	helixLengths.reset();
// 	vertexIds.reset();
// 	for (PermutationGenerator p : helixPermutations) {
// 	    p.reset();
// 	}
    }

    /** Skips to next building block. Returns true if successful */
    /*
    public boolean skipToNextBuildingBlock() throws IllegalAccessException {
	log.info("Starting skipToNextBuildingBlock");
	if (noSkipMode) {
	    log.info("Deactivated skipToNextBuildingBlock!");
	    return false; // no skipping
	}
	GrowConnectivity connectivity = null;
	int skipN = 1 + helixPermutations.length; // helix lengths (1) + helix Permutations
	int incCounter = 1;
	do {
	    log.info("another do iteration in SmallConnectivityIterator.inc()...");
	    if (!permutatorList.hasNext()) {
		throw new IllegalAccessException("No increasing possible!");
	    }
	    if ((DEBUG_OUTPUT_INTERVALL > 0) && ((incCounter++ % DEBUG_OUTPUT_INTERVALL) == 0)) {
		log.info("Increasing building block connectivity from (" + incCounter++ 
			 + "): " + getGrowConnectivity());
	    }
	    permutatorList.skip(skipN); // increase unified counters
	    log.info("Skip: Trying grow connectivity corresponding to permutator status: " + permutatorList.toString());
	    connectivity = getGrowConnectivity();
	}
	while ((connectivity == null) || (!validateConnectivity(connectivity))); // check if "symmetry" constraints are ok
	assert (connectivity != null) && validateConnectivity(connectivity);
	log.info("Finished skipToNextBuildingBlock");
	return true;
    }
    */

    /** Skips to next building block. Returns true if successful */
    /*
    public boolean skipToNextConnection() throws IllegalAccessException {
	log.info("Starting skipToNextConnection");
	if (noSkipMode) {
	    log.info("Deactivated skipToNextConnection!");
	    return false; // no skipping
	}
	GrowConnectivity connectivity = null;
	int skipN = 1; // helix lengths (1) are skipped
	int incCounter = 1;
	do {
	    log.info("another do iteration in SmallConnectivityIterator.inc()...");
	    if (!permutatorList.hasNext()) {
		throw new IllegalAccessException("No increasing possible!");
	    }
	    if ((DEBUG_OUTPUT_INTERVALL > 0) && ((incCounter++ % DEBUG_OUTPUT_INTERVALL) == 0)) {
		log.info("Increasing building block connectivity from (" + incCounter++ 
			 + "): " + getGrowConnectivity());
	    }
	    permutatorList.skip(skipN); // increase unified counters
	    log.info("Skip: Trying grow connectivity corresponding to permutator status: " + permutatorList.toString());
	    connectivity = getGrowConnectivity();
	}
	while ((connectivity == null) || (!validateConnectivity(connectivity))); // check if "symmetry" constraints are ok
	assert (connectivity != null) && validateConnectivity(connectivity);
	log.info("Finished skipToNextConnection");
	return true;
    }
    */

}
