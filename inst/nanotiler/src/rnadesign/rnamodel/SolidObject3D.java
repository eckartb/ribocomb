package rnadesign.rnamodel;

import tools3d.Vector3D;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.SimpleObject3D;
import chemistrytools.MaterialDescriptor;

public class SolidObject3D extends SimpleObject3D {

    private MaterialDescriptor material;
    
    public SolidObject3D() {
	super();
    }

    public SolidObject3D(Vector3D pos) {
	super(pos);
    }

    public SolidObject3D(Object3D obj) {
	super(obj);
    }

    public MaterialDescriptor getMaterial() { return material; }

    public void setMaterial(MaterialDescriptor material) { 
	this.material = material;
    }

    

}
