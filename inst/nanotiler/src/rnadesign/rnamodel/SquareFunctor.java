package rnadesign.rnamodel;

import generaltools.DoubleFunctor;

/** defines class providing real valued function */
public class SquareFunctor implements DoubleFunctor {

    private double xoffs;
    private double yoffs;
    private double scale;

    public SquareFunctor(double xoffs, double scale, double yoffs) {
	this.xoffs=xoffs;
	this.scale=scale;
	this.yoffs=yoffs;
    }

    /** function that maps real value to another real value */
    public double doubleFunc(double value) {
	double term = (value-xoffs) *scale;
	return (term*term) + yoffs;
    }

}
