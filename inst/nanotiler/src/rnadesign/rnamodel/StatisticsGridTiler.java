package rnadesign.rnamodel;

public interface StatisticsGridTiler extends GridTiler {

    TilingStatistics getJunctionStatistics();

    TilingStatistics getKissingLoopStatistics();

    TilingStatistics getStemStatistics();

}
