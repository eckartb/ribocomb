package rnadesign.rnamodel;

import numerictools.PotentialND;
import rnasecondary.Stem;
import tools3d.CoordinateSystem;
import tools3d.LineShape;
import tools3d.Vector3D;
import tools3d.objects3d.CoordinateSystem3D;

import static rnadesign.rnamodel.BranchDescriptorTools.REF_ATOM_NAME;
import static rnadesign.rnamodel.BranchDescriptorTools.REF_ATOM_NAME_ALT;

public class StemBasePotential implements PotentialND {

    private RnaStem3D stem;
    private Stem stemInfo;
    private static final double ERROR_VALUE = 9999.9;
    private HelixParameters helixParameters = new HelixParameters(); // default helix parameters can be changed

    public StemBasePotential(RnaStem3D stem) { this.stem = stem; this.stemInfo = stem.getStemInfo(); }
    
    /** returns fit of stem base to helix data.
     * TODO : verify! */
    public double computeHelixFit(CoordinateSystem cs) throws FittingException {
	assert (stemInfo.size() > 0);
	double rms = 0;
// 	Vector3D direction = base3.minus(base5);
// 	// 	log.fine("starting computeHelixFit with " + base5 + " " + base3 + " " + direction);
// 	direction.normalize();
// 	// compute branch descriptor:
// 	// strand 1 is "outgoing strand; strand 2 is "incoming strand" !
// 	BranchDescriptor3D branch = new SimpleBranchDescriptor3D(stem.getStrand2(), stem.getStrand1(),
// 								 stem.getStrand2End3Index(), stem.getStrand1End5Index(), 0); // TODO verify zero offset
// 	branch.setIsolatedPosition(base5);
// 	branch.setDirection(direction);
	// log.fine("Computing stem for help branch: " + branch.getBasePosition() + " " + branch.getDirection()); 
	int count = 0;
	int step = (stemInfo.size() - 1)/2; // only check first, middle and last base pair!
	if (step < 1) {
	    step = 1;
	}
	for (int i = 0; i < stemInfo.size(); i+= step) {
	    LineShape line = Rna3DTools.computeHelix(i, cs, helixParameters);
	    Vector3D p1 = line.getPosition1();
	    Vector3D p2 = line.getPosition2();
	    String usedRefName1 = REF_ATOM_NAME;
	    String usedRefName2 = REF_ATOM_NAME;
	    if (stem.getStartResidue(i).getIndexOfChild(usedRefName1) < 0) {
		usedRefName1 = REF_ATOM_NAME_ALT; // AMBER alternative
	    }
	    if (stem.getStopResidue(i).getIndexOfChild(usedRefName2) < 0) {
		usedRefName2 = REF_ATOM_NAME_ALT; // AMBER alternative
	    }
	    if (stem.getStartResidue(i).getIndexOfChild(usedRefName1) < 0) {
		throw new FittingException("Could not find atom with name: " + REF_ATOM_NAME + " or "
					   + REF_ATOM_NAME_ALT + " in residue " + stem.getStartResidue(i).getName());
	    }
	    if (stem.getStopResidue(i).getIndexOfChild(usedRefName2) < 0) {
		throw new FittingException("Could not find atom with name: " + REF_ATOM_NAME + " or "
					   + REF_ATOM_NAME_ALT + " in residue " + stem.getStopResidue(i).getName());
	    }

	    Vector3D p1Orig = stem.getStartResidue(i).getChild(usedRefName1).getPosition();
	    Vector3D p2Orig = stem.getStopResidue(i).getChild(usedRefName2).getPosition();
	    rms += (p1.distanceSquare(p1Orig) + p2.distanceSquare(p2Orig));
	    // debug info:
// 	    if ((i == 0) || (i == (stemInfo.size()-1))) {
// 		log.fine("Projected base pair " + (i+1) + " rms: " + term + " tot-rms: " + (rms/(i+1)) + " " 
// 				   + p1 + " ( " + p1Orig + " ) " + p2 + " ( " + p2Orig + " ) ");
// 	    }
	    count++;
	}
	rms /= (2.0 * count); // compute average
	rms = Math.sqrt(rms); 
	return rms;
    }

    // dimensionality of vector to be optimized
    public int getDimension() { return CoordinateSystem3D.ARRAY_DIM; }

    public double[] generateLowPosition() {
	assert false; // TODO : NOT YET IMPLEMENTED
	return null;
    }

    public double[] generateHighPosition() {
	assert false; // TODO : NOT YET IMPLEMENTED
	return null;
    }

    public HelixParameters getHelixParameters() { return helixParameters; }

    /** returns fit of stem base to helix data. Workaround: changing input parameters to "fix" rotation matrix. */
    public double getValue(double[] v) {
// 	Vector3D base5 = new Vector3D(v[0], v[1], v[2]);
// 	Vector3D base3 = new Vector3D(v[3], v[4], v[5]);
	CoordinateSystem3D.renormalize(v);
	CoordinateSystem cs = new CoordinateSystem3D(v);
// 	if (base5.minus(base3).length() == 0.0) {
// 	    return 1000.0; // penalty
// 	}
	double value = 0.0;
	try {
	    value = computeHelixFit(cs); // base5, base3);
	}
	catch (FittingException fe) {
	    value = ERROR_VALUE;
	}
	return value;
    }

    public void setHelixParameters(HelixParameters other) { this.helixParameters = other; }

}

/* removed from class
    / returns fit of stem base to helix data.
      TODO : verify! /
//     public double computeHelixFit(Vector3D base5, Vector3D base3) {
// 	double rms = 0;
// 	Vector3D direction = base3.minus(base5);
// 	// 	log.fine("starting computeHelixFit with " + base5 + " " + base3 + " " + direction);
// 	direction.normalize();
// 	// compute branch descriptor:
// 	// strand 1 is "outgoing strand; strand 2 is "incoming strand" !
// 	BranchDescriptor3D branch = new SimpleBranchDescriptor3D(stem.getStrand2(), stem.getStrand1(),
// 								 stem.getStrand2End3Index(), stem.getStrand1End5Index(), 0); // TODO verify zero offset
// 	branch.setIsolatedPosition(base5);
// 	branch.setDirection(direction);
// 	// log.fine("Computing stem for help branch: " + branch.getBasePosition() + " " + branch.getDirection()); 
// 	for (int i = 0; i < stemInfo.size(); ++i) {
// 	    LineShape line = Rna3DTools.computeHelix(branch, i);
// 	    Vector3D p1 = line.getPosition1();
// 	    Vector3D p2 = line.getPosition2();
// 	    Vector3D p1Orig = stem.getStartResidue(i).getChild(BranchDescriptorTools.REF_ATOM_NAME).getPosition();
// 	    Vector3D p2Orig = stem.getStopResidue(i).getChild(BranchDescriptorTools.REF_ATOM_NAME).getPosition();
// 	    double term = 0.5 * (p1.distance(p1Orig) + p2.distance(p2Orig));
// 	    rms += term;
// 	    // debug info:
// // 	    if ((i == 0) || (i == (stemInfo.size()-1))) {
// // 		log.fine("Projected base pair " + (i+1) + " rms: " + term + " tot-rms: " + (rms/(i+1)) + " " 
// // 				   + p1 + " ( " + p1Orig + " ) " + p2 + " ( " + p2Orig + " ) ");
// // 	    }
// 	}
// 	rms /= stemInfo.size(); // compute average
// 	return rms;
//     }

*/
