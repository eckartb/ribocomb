package rnadesign.rnamodel;

import tools3d.*;
import numerictools.*;

import static rnadesign.rnamodel.RnaConstants.*;

/** how well can 2 branch descriptors be interpolated by a helix of give length? */
public class StemBranchDescriptorPotential implements PotentialND {


    private int stemLength; 
    private Vector3D firstStartPosition; // 5' end of strand 1
    private Vector3D firstStopPosition; // matching with firstStartPosition
    private Vector3D lastStartPosition; // 3' end of strand 1
    private Vector3D lastStopPosition; // matching with lastStartPosition

    public static final int DIMENSION = 9;

    StemBranchDescriptorPotential( int stemLength,
				   BranchDescriptor3D branch1,
				   BranchDescriptor3D branch2) {
	assert branch1.isValid();
	assert branch2.isValid();
	assert false; // not valid code anymore, need replacement for branch1.getInObject().getPosition
	this.stemLength = stemLength;
// 	this.firstStartPosition = branch1.getOutObject().getPosition();
// 	this.firstStopPosition = branch1.getInObject().getPosition();
// 	this.lastStartPosition = branch2.getInObject().getPosition();
// 	this.lastStopPosition = branch2.getOutObject().getPosition();
	assert isValid();
    }

    /** computes approximate maximum TODO */
    public double[] generateHighPosition() {
	assert false;
	return null;
    }

    /** computes approximate minimum */
    public double[] generateLowPosition() {
	assert firstStopPosition != null;
	assert firstStartPosition != null;
	double[] result = new double[DIMENSION];
	firstStopPosition.setArray(result, 0);
	firstStartPosition.setArray(result, 3);
	lastStartPosition.setArray(result, 6);
	return result;
    }

    public int getDimension() { return DIMENSION; }

    /** converts branch descriptor and stem length to internal vector representation */
    public static double[] convertBranchDescriptor(BranchDescriptor3D b, int len) {
	double[] result = new double[9];
	Vector3D pos = b.getPosition();
	// Vector3D outPos = b.getOutObject().getPosition();
	assert false; 
	Vector3D outPos = null; // TODO : not valid code anymore; find replacement
	Vector3D dir = b.getDirection();
	Vector3D farPos = pos.plus(dir.mul(len * HELIX_RISE));
	pos.setArray(result, 0);
	outPos.setArray(result, 3);
	farPos.setArray(result, 6);
	return result;
    }

    /** expects 9-dimensional vector that represent coordinates of stem base, 5' start position and stem direction.
     * Computes fit to given branch descriptors. */
    public double getValue(double[] v) {
	assert v.length == getDimension();
	Vector3D basePos = new Vector3D(v[0], v[1], v[2]);
	Vector3D fivePos = new Vector3D(v[3], v[4], v[5]);
	Vector3D direction = new Vector3D(v[6], v[7], v[8]).minus(basePos);
	if (direction.length() == 0.0) {
	    direction.set(0, 0, 1); // set to arbitrary value if zero
	}
	else {
	    direction.normalize();
	}
	assert false; // TODO : fix below code so it does not use anymore deprecated methods
	LineShape firstPair = null; // RNA3DTools.computeHelix(basePos, fivePos, direction, -1); // one residue prior to helix must match!
	LineShape lastPair = null; //  RNA3DTools.computeHelix(basePos, fivePos, direction, stemLength); // one residue prior to helix must match!
	double term11 = (firstPair.getPosition1().minus(firstStartPosition)).lengthSquare();
	double term12 = (firstPair.getPosition2().minus(firstStopPosition)).lengthSquare();
	double term21 = (lastPair.getPosition1().minus(lastStartPosition)).lengthSquare();
	double term22 = (lastPair.getPosition2().minus(lastStopPosition)).lengthSquare();
	double result = Math.sqrt(0.25* (term11 + term12 + term21 + term22));
	return result;
    }

    /** returns true if current object is well defined. */
    public boolean isValid() {
	return 
		(firstStartPosition != null) && (firstStopPosition != null) 
	    && (lastStartPosition != null) && (lastStopPosition != null);
    }

}
