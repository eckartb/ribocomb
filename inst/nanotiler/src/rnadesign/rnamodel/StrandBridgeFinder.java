package rnadesign.rnamodel;

import java.util.List;
import tools3d.objects3d.*;

/** Interface for classes that find 3D bridges between two objects */
public interface StrandBridgeFinder {

    List<Object3DLinkSetBundle> findBridges(Object3DSet strands);

    void setRms(double rms);

    void setAngleWeight(double weight);

    void setLenMax(int n);

}
