package rnadesign.rnamodel;

import generaltools.ScoreWrapper;
import java.io.PrintStream;
import java.util.*;
import java.util.logging.*;
import tools3d.objects3d.*;
import tools3d.Vector3D;

/** Algorithm for automated fusing of RNA strands discregarding junctions */
public class StrandFuser implements Runnable {

    private Logger log = Logger.getLogger(PackageConstants.LOGFILE_DEFAULT);

    // some helper words used to indicate status of strands in properties object
    public static final int STRAND_FUSION_MAX = 20;
    private static final double STRAND_ANGLE_MAX = Math.PI; // 0.75 * Math.PI; // minimum angle between connecting strands running i
    private static final double ANGLE_PENALTY = 50.0; // bad angle between connecting strands is penalized by this "distance" 
    private static final double STRAND_CONNECTION_SCORE_CUTOFF = 50.0;
    private static final String TMP_TOKEN = "__RINGFUSE_STATUS";
    private static final String TMP_TOKEN_OCCUPIED = "occupied";
    // private double distanceCutoff = 10.0; // max distance between two residues to be fused
    private double scoreCutoff = STRAND_CONNECTION_SCORE_CUTOFF;
    private static boolean debugMode = true;
    private List<RnaStrand> strands = new ArrayList<RnaStrand>();
    private List<RnaStrand> fusedStrands; 
    // private Object3D loops = new SimpleObject3D("loops");
    private SingleStrandBridgeFinder singleStrandBridger;
    
    public StrandFuser(List<RnaStrand> strands, SingleStrandBridgeFinder singleStrandBridger) {
	this.singleStrandBridger = singleStrandBridger;
	this.strands.addAll(strands); // new container
	this.strands = cleanStrands(this.strands);
	log.info("Initial strands: " + strands.size() + " cleaned version: " + this.strands.size());
    }

    /** Return bridging loops */
    // public Object3D getLoops() { return loops; }

    public StrandFuser(Object3DSet strandSet, SingleStrandBridgeFinder singleStrandBridger) {
	this.singleStrandBridger = singleStrandBridger;
	for (int i = 0; i < strandSet.size(); ++i) {
	    Object3D obj = strandSet.get(i);
	    if (obj instanceof RnaStrand) {
		this.strands.add((RnaStrand)obj);
	    }
	    else {
		log.severe("Internal error in RingFuse: 3D object is not RNA strand: " + obj.getFullName() + " " + obj.getClassName());
		assert false;
	    }
	}
	this.strands = cleanStrands(this.strands);
	log.info("Initial strands: " + strandSet.size() + " cleaned version: " + this.strands.size());
    }

    private boolean isDuplicateStrand(RnaStrand strand, List<RnaStrand> strands) {
	for (RnaStrand strand2 : strands) {
	    if (strand2 == strand) {
		continue;
	    }
	    if (AI3DTools.checkStrandDuplicate(strand, strand2, 2.0, "C4*")) {
		log.info("Strand duplicate found: " + strand.getFullName() + " " + strand.sequenceString()
			 + strand2.getFullName() + " " + strand2.sequenceString());
		return true;
	    }
	}
	return false;
    }

    /** removes duplicate strands from set of strands to be evaluated */
    private List<RnaStrand> cleanStrands(List<RnaStrand> strands) {
	return strands;
// 	List<RnaStrand> result = new ArrayList<RnaStrand>();
// 	for (RnaStrand strand : strands) {
// 	    if ((findJunctionClass(strand) >= 0) || (!isDuplicateStrand(strand,strands))) {
// 		result.add(strand); // only delete if not part of junction
// 	    }
// 	}
// 	return result;
    }

    public void run() {
	log.info("Starting automatic strand fusing for " + strands.size() + " strands !");
	this.fusedStrands = new ArrayList<RnaStrand>(); // resulting fused strands
	// for each junction class: pick first element
	int[] strandUsed = new int[strands.size()];
	for (int j = 0; j < strands.size(); ++j) {
	    NucleotideStrand strand = strands.get(j);
	    if (!checkStrandAvailable(strand)) {
		continue;
	    }
	    int strandId = strands.indexOf(strand);
	    assert strandId >= 0; // must be found!
	    List<Integer> fusionIds = generateFusionStrandIds(strands, strandId, scoreCutoff);
	    if (fusionIds.size() > 1) { // must be at least two strands
		fusedStrands.add(applyStrandFusion(fusionIds));
	    } else {
		log.info("No strand available for fusing with strand " + (j+1) + " : " + strand.getFullName());
	    }
	}
	log.info("Finished automatic strand fusing! Number of strands in result: " + fusedStrands.size());
    }

    /** fuse strand with these ids */
    private RnaStrand applyStrandFusion(List<Integer> strandIds) {
	log.info("Starting applyStrandFusion with " + strandIds.size() + " strands!");
	assert strandIds.size() < STRAND_FUSION_MAX; // do not be ridiculous
	assert strandIds != null && strandIds.size() > 0;
	assert strandIds.size() == 1 || (!strandIds.get(0).equals(strandIds.get(1)));
	RnaStrand newStrand = (RnaStrand)(strands.get(strandIds.get(0))); // .cloneDeep());
	String outs = "";
	StringBuffer buf = new StringBuffer();
	for (Integer ii : strandIds) {
	    System.out.println("Id: " + (ii + 1) + " size: " + buf.length());
	    buf.append("" + (ii.intValue()+1) + " ");
	}
	outs = buf.toString();
	log.info("Applying strand fusing to sequence path with " + strands.get(strandIds.get(0)).getFullName() 
		 + " strand ids: " + outs );
	setStrandOccupied(strands.get(strandIds.get(0))); // make sure it is not used again
	assert !checkStrandAvailable(strands.get(strandIds.get(0)));
	int count = strands.get(strandIds.get(0)).getResidueCount();
	for (int i = 1; i < strandIds.size(); ++i) {
	    RnaStrand tmpStrand = strands.get(strandIds.get(i));
	    assert tmpStrand != strands.get(strandIds.get(i-1));
	    // RnaStrand newStrand2 = (RnaStrand)(tmpStrand.cloneDeep());
	    setStrandOccupied(tmpStrand);
	    assert !checkStrandAvailable(tmpStrand);
	    count += strands.get(strandIds.get(i)).getResidueCount();
	    log.info("Fusing strand " + newStrand.getFullName() + " " + newStrand.sequenceString()
		     + " with " + tmpStrand.getFullName() + " " + tmpStrand.sequenceString() + " strand id: " + (strandIds.get(i)+1));
	    List<Object3DLinkSetBundle> fragments = null;
	    if ((singleStrandBridger.getRms() > 0.0) && (singleStrandBridger.getLenMax() > 0)) {
		fragments = singleStrandBridger.findBridge(newStrand.getResidue3D(newStrand.getResidueCount()-1),
							   tmpStrand.getResidue3D(0));
		if ((fragments != null) && (fragments.size() > 0)) {
		    RnaStrand loop = (RnaStrand)(fragments.get(0).getObject3D());
		    log.info("Found fragment between strands " + newStrand.getFullName() + " and " + tmpStrand.getFullName()
			     + loop.getFullName() + " " + loop.getResidueCount());
		    newStrand.append(loop); // do not delete fragment from database, appendAndDelete would be a mean bug!
		    count += loop.getResidueCount();
		}
		else {
		    log.info("No fragment found between strands " + newStrand.getFullName() + " and " + tmpStrand.getFullName());
		}		
	    }
	    newStrand.appendAndDelete(tmpStrand); // remove strand2
	}
	assert newStrand.getResidueCount() == count; // must be sum of all residues
	return newStrand;
    }

    /** checks if strand is occupied */
    private boolean checkStrandAvailable(NucleotideStrand strand) {
	return strand.getProperty(TMP_TOKEN) != TMP_TOKEN_OCCUPIED;
    }

    public void setScoreCutoff(double value) { scoreCutoff = value; }

    /** marks strand as occupied */
    private void setStrandOccupied(NucleotideStrand strand) {
	strand.setProperty(TMP_TOKEN, TMP_TOKEN_OCCUPIED);
    }

    /** Scores how well a start strand can be appended with end strand. Zero: perfect score */
    private double scoreStrandConnectivity(RnaStrand startStrand, RnaStrand endStrand) throws RnaModelException {
	Nucleotide3D startStrandResidue = (Nucleotide3D)(startStrand.getResidue3D(startStrand.getResidueCount()-1));
	Nucleotide3D endStrandResidue = (Nucleotide3D)(endStrand.getResidue3D(0));
	Object3D ao21 = startStrandResidue.getChild("O2*");
	Object3D ap2 = endStrandResidue.getChild("P");
	if (ap2 == null) {
	    NucleotideDBTools.addMissingPhosphor(endStrandResidue);
	    ap2 = endStrandResidue.getChild("P");
	    assert ap2 != null; // phosphor was added even if this is not physical
	}
	if (ao21 == null) {
	    throw new RnaModelException("Could not find atom O2* in residue: " + startStrandResidue.getFullName());
	}
	double distance = ao21.distance(ap2);
	// check angles:
	Vector3D v1 = NucleotideDBTools.getBackwardDirectionVector(startStrandResidue, "C4*");
	Vector3D v2 = NucleotideDBTools.getForwardDirectionVector(endStrandResidue, "C4*");
	double angle = Math.PI - v1.angle(v2); // forward and backward direction go in opposite directions, compensate for that
	if (angle > STRAND_ANGLE_MAX) { // 
	    distance += ANGLE_PENALTY;
	}
	if (!singleStrandBridger.validate(startStrandResidue, endStrandResidue)) {
	    distance += scoreCutoff + 1.0; // cannot be bridged
	}
	return distance;
    }

    /** For strand with id "id", return index of strand that can be connected */
    int findNextStrandId(List<RnaStrand> strands, List<Integer> sofarStrandIds, double scoreCutoff) {
	assert sofarStrandIds.size() > 0;
	int id = sofarStrandIds.get(sofarStrandIds.size()-1); // get last id, using auto-unboxing
	List<ScoreWrapper> rankedList = new ArrayList<ScoreWrapper>();
	for (int i = 0; i < strands.size(); ++i) {
	    if (i == id) {
		continue;
	    }
	    if (sofarStrandIds.contains(i)) { // use auto-boxing
		continue; // check if used in this path
	    }
	    if (!checkStrandAvailable(strands.get(i))) {
		continue; // check if not used at all
	    }

	    try {
		double score = scoreStrandConnectivity(strands.get(id), strands.get(i));
        	log.info("Working on connectivity for strand " + (i+1) + " : " + score);
		if (score < scoreCutoff) {
		    ScoreWrapper wrap = new ScoreWrapper(new Integer(i), score); // remember id of strand
		    rankedList.add(wrap);
		}
	    }
	    catch (RnaModelException e) {
		log.warning("Bad nucleotide for strand fusing encountered: " + e.getMessage());
	    }
	}
	if (rankedList.size() == 0) {
	    return -1;
	}
	try {
	    Collections.sort(rankedList); // somehow the compiler gives a warning here... FIXIT
	}
	catch (ClassCastException ce) {
	    log.warning("Internal error: " + ce.getMessage());
	    System.exit(-1); // internal error
	}
	catch (UnsupportedOperationException uoe) {
	    log.warning("Internal error: " + uoe.getMessage());
	    System.exit(-1); // internal error	    
	}
	Integer strandIndexInt = (Integer)(rankedList.get(0).getObject()); // get best next strand
	int result = strandIndexInt.intValue();
	assert checkStrandAvailable(strands.get(result));
	assert rankedList.get(0).getScore() <= STRAND_CONNECTION_SCORE_CUTOFF;
	assert !(sofarStrandIds.contains(strandIndexInt));
	return result;
    }

    /** Returns indices of strands that can be fused if one follows the start strand from 5' to 3' end */
    private List<Integer> generateFusionStrandIds(List<RnaStrand> strands, int currentId, double scoreCutoff) {
	List<Integer> result = new ArrayList<Integer>();
	// get junction class of strand:
	result.add(currentId); // one of my first examples of "auto-boxing" ! :-)
	while ((currentId = findNextStrandId(strands, result, scoreCutoff)) >= 0) {
	    assert (!result.contains(currentId));
	    assert result.size() < 100; // avoid being ridiculous
	    result.add(currentId);
	}
	return result;
    }

    /** Returns result */
    public List<RnaStrand> getFusedStrands() {
	if (fusedStrands == null) {
	    run(); // lazy evaluation
	}
	return fusedStrands;
    }

}
