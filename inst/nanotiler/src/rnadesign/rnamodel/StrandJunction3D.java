package rnadesign.rnamodel;

import tools3d.objects3d.LinkSet;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DSet;

/** Describes concept on n-way junction of strand.
 * Each arm of the junction corresponds to one or two sequences. 
 * There is no function "addBranch". Instead one defines
 * new branches by adding them with "insertChild"
 */
public interface StrandJunction3D extends Object3D {

    public static final int BRANCH_MAX = 100; // maximum number of branches
    public static final double DISTANCE_TOLERANCE = 0.0001;

    public Object3DSet cloneDown(int junctionOrder);

    /** returns true, if all branch descriptor occupy a free "corridor" with a certain radius.
     * If supplied radius is smaller or equal zero, true if returned. */
    public boolean corridorCheck(CorridorDescriptor corridorDescriptor);

    /** return index of strand if found, -1 otherwise */
    public int findStrandIndex(NucleotideStrand strand);

    /** appends strand m to strand n, removes strand m */
    public void fuseStrands(int n, int m);

    /** returns number of branches */
    int getBranchCount();

    /** returns number of defined strands */
    int getStrandCount();
    
    /** returns references to all strands. */
    public NucleotideStrand[] getStrands();

    /** returns descriptor for n'th branch */
    BranchDescriptor3D getBranch(int n);

    /** index of incoming sequence of n'th branch */
    int[] getIncomingSeqIds();

    /** index of outgoing sequence of n'th branch */
    int[] getOutgoingSeqIds();
    
    /** returns n'th strand. */
    NucleotideStrand getStrand(int n);

    /** returns loop length of n'th strand */
    int getLoopLength(int n);
    
    /** returns for n'th sequence the branch id for which it is the
     * incoming sequence.
     */
    int getIncomingBranchId(int n);

    /** returns for all sequences the branch descriptor ids for which it is the
     * incoming sequence.
     */
    int[] getIncomingBranchIds();

    /** returns for n'th sequence the branch descriptor ids for which it is the
     * outgoing sequence.
     */
    int getOutgoingBranchId(int n);

    /** returns sequence the branch id for which it is the
     * outgoing sequence.
     */
    int[] getOutgoingBranchIds();

    /** returns true, if connecting internal "loops" are paired in internal helices. 
     * This is a simple flag. The precice definition (should single base pairs be counted as a helix? Usually not) is 
     * up to the programmer / generating method. The default value is false and is not changing unless being set with setInternalHelices(). 
     */
    boolean hasInternalHelices();

    /** returns true if a strand is going from 
     * incoming branchid to outgoing branchid.
     * True only if directionality correct.
     */
    boolean isConnected(int incomingBranchId, int outgoingBranchId);

    /** Returns true iff junction contains two strands as kissing loop */
    boolean isKissingLoop();
    
    /** A junction is regular if and only if getBranchCount() == getStrandCount() */
    boolean isRegular();

    /** returns true if consistent state. */
    boolean isValid();

    /** returns index of link that links two branch descriptors between junctions. 
     * TODO : careful: assumes only one link leading from one junction to next!
     */
    int findBranchConnectionLink(StrandJunction3D junction, LinkSet links);

    /** set to true, if junction containts internal helices */
    void setInternalHelices(boolean flag);
    
}
