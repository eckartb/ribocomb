package rnadesign.rnamodel;

import java.io.OutputStream;

import tools3d.objects3d.LinkSet;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DLispWriterTools;
import tools3d.objects3d.Object3DWriter;
import tools3d.objects3d.Object3DWriterTools;
import tools3d.objects3d.SimpleObject3D;

/** Writes StrandJunction3D object in Lisp format.
 * @author Eckart Bindewald
 *
 */
public class StrandJunction3DLispWriter implements Object3DWriter {

    private int formatId = LISP_FORMAT;

    private Object3DWriter branchWriter = new BranchDescriptor3DLispWriter();
    private Object3DWriter strandWriter = new RnaStrandLispWriter();
    private Object3DWriterTools tools = new Object3DLispWriterTools();
	
    public int getFormatId() { return formatId; }

    public void write(OutputStream os, Object3D tree) {
	assert false;
	// not yet implemented
    }

    public void write(OutputStream os, LinkSet links) {
	assert false;
	// not yet implemented	
    }   

    /** writes all strands */
    private String writeBranches(StrandJunction3D junc) {
	StringBuffer buf = new StringBuffer();
	final String name = "branches";
	for (int i = 0; i < junc.getBranchCount(); ++i) {
	    buf.append(" " + branchWriter.writeString(junc.getBranch(i)));
	}
	return tools.encloseBody(name, buf.toString());
    }

    /** writes all strands */
    private String writeStrands(StrandJunction3D junc) {
	StringBuffer buf = new StringBuffer();
	final String name = "branches";
	for (int i = 0; i < junc.getStrandCount(); ++i) {
	    buf.append(" " + strandWriter.writeString(junc.getStrand(i)));
	}
	return tools.encloseBody(name, buf.toString());
    }

    public String writeString(Object3D tree) {
	assert (tree instanceof StrandJunction3D);
	StrandJunction3D junc = (StrandJunction3D)tree;
	StringBuffer buf = new StringBuffer();
	buf.append(tools.getHeader(junc.getClassName()));
		   
	buf.append(SimpleObject3D.toStringBody(junc)); // write basics of object TODO : not format independent!
	buf.append(" " + tools.writeIntArray(junc.getIncomingBranchIds()));
	buf.append(" " + tools.writeIntArray(junc.getOutgoingBranchIds()));
	buf.append(" " + tools.writeIntArray(junc.getIncomingSeqIds()));
	buf.append(" " + tools.writeIntArray(junc.getOutgoingSeqIds()));
	// write defined Branches:
	buf.append(" " + writeBranches(junc));
	// write defined strands:
	buf.append(" " + writeStrands(junc));

	buf.append(tools.getFooter(junc.getClassName()));

	return buf.toString();
    }
	
}
