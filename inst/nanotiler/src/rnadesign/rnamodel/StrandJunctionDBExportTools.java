package rnadesign.rnamodel;

import java.util.Arrays;

import tools3d.CoordinateSystem;
import tools3d.Vector3D;
import tools3d.objects3d.Object3DTools;

import static rnadesign.rnamodel.PackageConstants.NEWLINE;

public class StrandJunctionDBExportTools {

    /** Returns sorted helix angles */
    public static double[] generateJunctionFingerPrint(StrandJunction3D junction) {
	int size = junction.getBranchCount();
	int num = (size * (size-1)) / 2;
	double[] result = new double[num];
	int pc = 0;
	for (int i = 0; i < junction.getBranchCount(); ++i) {
	    for (int j = i+1; j < junction.getBranchCount(); ++j) {
		result[pc++] = junction.getBranch(i).getDirection().angle(
									  junction.getBranch(j).getDirection());
	    }
	}
	Arrays.sort(result);
	return result;
    }

    /** Returns sorted helix angles in text format */
    private static String generateJunctionFingerPrintText(StrandJunction3D junction) {
	double[] angles = generateJunctionFingerPrint(junction);
	StringBuffer buf = new StringBuffer();
	for (int i = 0; i < angles.length; ++i) {
	    double degrees = Math.toDegrees(angles[i]);
	    int degreesInt = (int)(degrees + 0.5); // add 0.5, round down
	    buf.append("" + degreesInt);
	    if ((i+1) < angles.length) {
		buf.append(":");
	    }
	}
	return buf.toString();
    }

    private static String generateVectorDBInfo(Vector3D pos, String nameBase) {
	return nameBase + "_X = " + pos.getX() + NEWLINE
	    + nameBase + "_Y = " + pos.getY() + NEWLINE
	    + nameBase + "_Z = " + pos.getZ() + NEWLINE;
    }

    /** writes property file like info about i'th strand */
    private  static String generateStrandDBInfo(StrandJunction3D junction, int i) {
 	StringBuffer buf = new StringBuffer();
	buf.append("## Properties of strand " + (i + 1) + NEWLINE);
	String base = "S" + (i + 1) + "_";
	NucleotideStrand strand = junction.getStrand(i);
	int b1Id = junction.getIncomingBranchId(i);
	int b2Id = junction.getOutgoingBranchId(i);
	BranchDescriptor3D branchIn = junction.getBranch(b1Id);
	BranchDescriptor3D branchOut = junction.getBranch(b2Id);
	int fiveId = branchIn.getIncomingIndex();
	int threeId = branchOut.getOutgoingIndex();
	int inOffset = branchIn.getOffset();
	int outOffset = branchOut.getOffset();
	int fiveId2 = fiveId + inOffset;
	int threeId2 = threeId - outOffset;
	String seqString = junction.getStrand(i).sequenceString();
	assert fiveId <= threeId + 1;
	assert threeId+1 <= seqString.length();
	assert fiveId >= 0;
	seqString = seqString.substring(fiveId, threeId+1); // use one used substring
	int lastIndexTmp = seqString.length()-outOffset;
	String seqString2 = "";
	if ((lastIndexTmp >= 0) && (lastIndexTmp > inOffset) && (inOffset < seqString.length()) && (lastIndexTmp <= seqString.length())) {
	    seqString2 = seqString.substring(inOffset, lastIndexTmp);
	}
	Residue3D fiveResidue = strand.getResidue3D(fiveId);
	Residue3D threeResidue = strand.getResidue3D(threeId);
	Residue3D fiveResidue2 = strand.getResidue3D(fiveId2);
	Residue3D threeResidue2 = strand.getResidue3D(threeId2);
	// original names:
	String nameIn = fiveResidue.getName().substring(1); // ignore first sequence letter
	String nameOut = threeResidue.getName().substring(1); // ignore first sequence letter
	String nameIn2 = fiveResidue2.getName().substring(1); // ignore first sequence letter
	String nameOut2 = threeResidue2.getName().substring(1); // ignore first sequence letter

	String strandChar = Object3DTools.getProperty(strand, "pdb_chain_char");
	if (strandChar == null) {
	    strandChar = "A";
	}

	buf.append(base + "NAME_ID5 =  " + nameIn + NEWLINE);
	buf.append(base + "NAME_ID3 = " + nameOut + NEWLINE);

	buf.append(base + "NUM_ID5_ORIG =  " + fiveResidue.getAssignedNumber() + NEWLINE);
	buf.append(base + "NUM_ID3_ORIG = " + threeResidue.getAssignedNumber() + NEWLINE);

	buf.append(base + "NAME2_ID5 =  " + nameIn2 + NEWLINE);
	buf.append(base + "NAME2_ID3 = " + nameOut2 + NEWLINE);

	buf.append(base + "NUM2_ID5_ORIG =  " + fiveResidue2.getAssignedNumber() + NEWLINE);
	buf.append(base + "NUM2_ID3_ORIG = " + threeResidue2.getAssignedNumber() + NEWLINE);

	buf.append(base + "SQ2 = " + seqString2 + NEWLINE);
	buf.append(base + "SQLEN2 = " + seqString2.length() + NEWLINE);

	buf.append(base + "SQ = " + seqString + NEWLINE);
	buf.append(base + "SQLEN = " + seqString.length() + NEWLINE);
	
	buf.append(base + "CHAR = " + strandChar + NEWLINE);

	return buf.toString();
    }

    /** writes property file like info about branch descriptor */
    private static String generateBranchDBInfo(StrandJunction3D junction, int id) {
	StringBuffer buf = new StringBuffer();
	buf.append("## Properties of helix " + (id + 1) + NEWLINE);
	BranchDescriptor3D branch = junction.getBranch(id);
// 	int[] incomingSeqIds = junction.getIncomingSeqIds();
// 	int[] outgoingSeqIds = junction.getOutgoingSeqIds();
// 	int inSeqId = incomingSeqIds[id]; // what id has the incoming sequence (is it the first, second, third sequence)?
// 	assert inSeqId >= 0;
// 	if (inSeqId >= junction.getStrandCount()) {
// 	    System.out.println("Weird incoming sequence id array: " );
// 	    IntegerArrayTools.writeArray(System.out, incomingSeqIds);
// 	}
// 	assert inSeqId < junction.getStrandCount();
// 	int outSeqId = outgoingSeqIds[id];
// 	assert outSeqId >= 0;
// 	assert outSeqId < junction.getStrandCount();
	String hBase = "H" + (id + 1) + "_";
	int incIndex = branch.getIncomingIndex(); // residue of start of helix of incoming strand
	int outIndex = branch.getOutgoingIndex(); // residue of start of helix of outgoing strand
	int incIndex2 = incIndex + branch.getOffset(); // residue of start of helix of incoming strand
	assert incIndex2 < branch.getIncomingStrand().getResidueCount();
	int outIndex2 = outIndex - branch.getOffset(); // residue of start of helix of outgoing strand
	assert outIndex2 >= 0;
	assert outIndex2 < branch.getOutgoingStrand().getResidueCount();
	int incIndex2Orig = branch.getIncomingStrand().getResidue(incIndex2).getAssignedNumber(); 
	int outIndex2Orig = branch.getOutgoingStrand().getResidue(outIndex2).getAssignedNumber(); 

	String incStrandChar = Object3DTools.getProperty(branch.getIncomingStrand(), "pdb_chain_char");
	if (incStrandChar == null) {
	    incStrandChar = "A";
	}
	String outStrandChar = Object3DTools.getProperty(branch.getOutgoingStrand(), "pdb_chain_char");
	if (outStrandChar == null) {
	    outStrandChar = "A";
	}

	buf.append(hBase + "INID = " + (incIndex+1) + NEWLINE);
	buf.append(hBase + "OUTID = " + (outIndex+1) + NEWLINE);
	buf.append(hBase + "INID2 = " + (incIndex2+1) + NEWLINE);
	buf.append(hBase + "OUTID2 = " + (outIndex2+1) + NEWLINE);
	buf.append(hBase + "INID2_ORIG = " + incIndex2Orig + NEWLINE);
	buf.append(hBase + "OUTID2_ORIG = " + outIndex2Orig + NEWLINE);
	buf.append(hBase + "INSEQ_CHAR = " + incStrandChar + NEWLINE);
	buf.append(hBase + "OUTSEQ_CHAR = " + outStrandChar + NEWLINE);
	// index of incoming sequence
	// buf.append(hBase + "INSQID = " + (inSeqId+1) + NEWLINE);
	// index of outgoing sequence
	// buf.append(hBase + "OUTSQID = " + (outSeqId+1) + NEWLINE);
	// index of incoming strand: 
	// int incSeqId = junction.getIncomingSeqIds()[id]; 
	buf.append("# Position:" + NEWLINE);
	String posBase = hBase + "P";
	buf.append(generateVectorDBInfo(branch.getPosition(), posBase));
	buf.append("# Orientation:" + NEWLINE);
	CoordinateSystem cs = branch.getCoordinateSystem();
	String dirBase = hBase + "DX";
	buf.append(generateVectorDBInfo(cs.getX(), dirBase));
	dirBase = hBase + "DY";
	buf.append(generateVectorDBInfo(cs.getY(), dirBase));
	dirBase = hBase + "DZ"; // this is the z-axis of the helix
	buf.append(generateVectorDBInfo(cs.getZ(), dirBase));
	return buf.toString();
    }


    /** writes property file like info about branch descriptor. Package visibility */
    static String generateBranchDBInfo(BranchDescriptor3D branch, int id) {
	StringBuffer buf = new StringBuffer();
	buf.append("## Properties of helix " + (id + 1) + NEWLINE);
// 	int[] incomingSeqIds = junction.getIncomingSeqIds();
// 	int[] outgoingSeqIds = junction.getOutgoingSeqIds();
// 	int inSeqId = incomingSeqIds[id]; // what id has the incoming sequence (is it the first, second, third sequence)?
// 	assert inSeqId >= 0;
// 	if (inSeqId >= junction.getStrandCount()) {
// 	    System.out.println("Weird incoming sequence id array: " );
// 	    IntegerArrayTools.writeArray(System.out, incomingSeqIds);
// 	}
// 	assert inSeqId < junction.getStrandCount();
// 	int outSeqId = outgoingSeqIds[id];
// 	assert outSeqId >= 0;
// 	assert outSeqId < junction.getStrandCount();
	String hBase = "H" + (id + 1) + "_";
	int incIndex = branch.getIncomingIndex(); // residue of start of helix of incoming strand
	int outIndex = branch.getOutgoingIndex(); // residue of start of helix of outgoing strand
	int incIndex2 = incIndex + branch.getOffset(); // residue of start of helix of incoming strand
	assert incIndex2 < branch.getIncomingStrand().getResidueCount();
	int outIndex2 = outIndex - branch.getOffset(); // residue of start of helix of outgoing strand
	assert outIndex2 >= 0;
	assert outIndex2 < branch.getOutgoingStrand().getResidueCount();
	int incIndex2Orig = branch.getIncomingStrand().getResidue(incIndex2).getAssignedNumber(); 
	int outIndex2Orig = branch.getOutgoingStrand().getResidue(outIndex2).getAssignedNumber(); 

	String incStrandChar = Object3DTools.getProperty(branch.getIncomingStrand(), "pdb_chain_char");
	if (incStrandChar == null) {
	    incStrandChar = "A";
	}
	String outStrandChar = Object3DTools.getProperty(branch.getOutgoingStrand(), "pdb_chain_char");
	if (outStrandChar == null) {
	    outStrandChar = "A";
	}

	buf.append(hBase + "INID = " + (incIndex+1) + NEWLINE);
	buf.append(hBase + "OUTID = " + (outIndex+1) + NEWLINE);
	buf.append(hBase + "INID2 = " + (incIndex2+1) + NEWLINE);
	buf.append(hBase + "OUTID2 = " + (outIndex2+1) + NEWLINE);
	buf.append(hBase + "INID2_ORIG = " + incIndex2Orig + NEWLINE);
	buf.append(hBase + "OUTID2_ORIG = " + outIndex2Orig + NEWLINE);
	buf.append(hBase + "INSEQ_CHAR = " + incStrandChar + NEWLINE);
	buf.append(hBase + "OUTSEQ_CHAR = " + outStrandChar + NEWLINE);
	// index of incoming sequence
	// buf.append(hBase + "INSQID = " + (inSeqId+1) + NEWLINE);
	// index of outgoing sequence
	// buf.append(hBase + "OUTSQID = " + (outSeqId+1) + NEWLINE);
	// index of incoming strand: 
	// int incSeqId = junction.getIncomingSeqIds()[id]; 
	buf.append("# Position:" + NEWLINE);
	String posBase = hBase + "P";
	buf.append(generateVectorDBInfo(branch.getPosition(), posBase));
	buf.append("# Orientation:" + NEWLINE);
	CoordinateSystem cs = branch.getCoordinateSystem();
	String dirBase = hBase + "DX";
	buf.append(generateVectorDBInfo(cs.getX(), dirBase));
	dirBase = hBase + "DY";
	buf.append(generateVectorDBInfo(cs.getY(), dirBase));
	dirBase = hBase + "DZ"; // this is the z-axis of the helix
	buf.append(generateVectorDBInfo(cs.getZ(), dirBase));
	return buf.toString();
    }


    /** writes property file like info about junction */
    public static String generateJunctionDBInfo(StrandJunction3D junction) {
	StringBuffer buf = new StringBuffer();
	buf.append("#############  Start of Description of junction: " + junction.getName() + NEWLINE);
	int numBranches = junction.getBranchCount();
	int numStrands =  junction.getStrandCount();
	StrandJunctionNomenclature lilley = new LilleyNomenclature();
	buf.append("# Number of strands:" + NEWLINE + "NUM_STRANDS = " + numStrands + NEWLINE);
	buf.append("LILLEY = " + lilley.generateNomenclature(junction)
		   + NEWLINE); 
	buf.append("FINGERPRINT = " + generateJunctionFingerPrintText(junction) + NEWLINE);
	buf.append("LOOP_LENGTH_SUM = " + LilleyNomenclature.computeLoopLengthSum(junction) + NEWLINE);
	buf.append("KISSING_LOOP = ");
	if (junction.isKissingLoop()) {
	    buf.append("TRUE" + NEWLINE);
	}
	else {
	    buf.append("FALSE" + NEWLINE);
	}
	buf.append("INTERNAL_HELICES = ");
	buf.append("NULL" + NEWLINE); // TODO: currently not functional, putting out "NULL" instead of TRUE or FALSE
// 	if (junction.hasInternalHelices()) {
// 	    buf.append("TRUE" + NEWLINE);
// 	}
// 	else {
// 	    buf.append("FALSE" + NEWLINE);
// 	}
	for (int i = 0; i < numStrands; ++i) {
	    buf.append(generateStrandDBInfo(junction, i));
	}
	for (int i = 0; i < numBranches; ++i) {
	    buf.append(generateBranchDBInfo(junction, i));
	}
	buf.append("#############  END of Description of junction: " + junction.getName() + NEWLINE);
	return buf.toString();
    }
    
}
