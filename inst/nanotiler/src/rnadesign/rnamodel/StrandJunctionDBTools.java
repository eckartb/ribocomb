package rnadesign.rnamodel;

import generaltools.CollectionTools;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;

import sequence.Alphabet;
import sequence.Sequence;
import sequence.SequenceTools;
import sequence.SimpleAlphabet;
import sequence.SimpleLetterSymbol;
import sequence.SimpleSequence;
import sequence.UnknownSymbolException;
import tools3d.Vector3D;
import tools3d.objects3d.CoordinateSystem3D;
import tools3d.objects3d.LinkSet;
import tools3d.objects3d.SimpleLinkSet;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DFactory;
import tools3d.objects3d.Object3DLinkSetBundle;
import tools3d.objects3d.Object3DSet;
import tools3d.objects3d.Object3DTools;
import tools3d.objects3d.SimpleObject3D;
import tools3d.objects3d.SimpleObject3DSet;
import tools3d.objects3d.modeling.CollisionForceField;
import tools3d.objects3d.modeling.ForceField;

import static rnadesign.rnamodel.PackageConstants.ENDL;
import static rnadesign.rnamodel.PackageConstants.NEWLINE;
import static rnadesign.rnamodel.PackageConstants.SLASH;

public class StrandJunctionDBTools {
    public static final String NTL_EXTENSION = ".ntl";
    private static final double stemFitRmsTolerance = 3.0; // 2.5;
    public static final double JUNCTION_DIST_MAX = 20.0;
    public static final double KISSINGLOOP_DIST_MAX = 20.0;
    public static final boolean SUBJUNCTION_ALLOWED = true; // was set to false for RNAJunction database!
    public static boolean regularJunctionMode = false; // if true, all junctions must have same number of strands than branches (form connected circle)
    // TODO : clarify below variable
    public static final double COLLINEAR_LIMIT_RAD = Math.PI; // (Math.PI/180.0)* 90.0; // 45.0; // max angle between rel branch position and branch direction TODO : change so that value can be adjusted
    //     public static String debugFileName = "/tmp/nanotiler_debug_testfile.txt";
    //     public static String debugFileName2 = "/tmp/nanotiler_debug_testfile1.txt";

    // private static Logger log = Logger.getLogger("global");
    // The below logger is initialized in NanoTiler.createAndShowGUI
    private static Logger log = Logger.getLogger("NanoTiler_debug");

    //    public static Atom3DSet atom3dSet = new Atom3DSet();

    /** returns true if smaller strand compared to strand that is extended by offset nucleotides */
    private static boolean isSubStrand(NucleotideStrand smallStrand, NucleotideStrand largeStrand, int offset) {
	if (smallStrand.getResidueCount() + 2*offset != largeStrand.getResidueCount()) {
	    return false;
	}
	String largeSeqCut = largeStrand.sequenceString().substring(offset, largeStrand.sequenceString().length() - offset);
	return smallStrand.sequenceString().equals(largeSeqCut); // only sequence comparison, better: 3D! TODO
    }

    /** return index of smaller strand compared to strand that is extended by offset nucleotides */
    private static int findSubStrandIndex(StrandJunction3D junction, NucleotideStrand strand, int offset) {
	for (int i = 0; i < junction.getStrandCount(); ++i) {
	    if (isSubStrand(junction.getStrand(i), strand, offset)) {
		return i;
	    }
	}
	return -1;
    }

    /** Returns true iff junction2 is subjunction of junction1 */
    private static boolean isSubJunction(StrandJunction3D junction1, StrandJunction3D junction2, int offset) {
	if (junction1.getStrandCount() != junction2.getStrandCount()) {
	    return false;
	}
	for (int i = 0; i < junction1.getStrandCount(); ++i) {
	    int id = findSubStrandIndex(junction2, junction1.getStrand(i), offset);
	    if (id < 0) {
		return false;
	    }
	}
	return true;
    }

    private static int findSubJunction(StrandJunction3D junction, Object3D junctions, int offset) {
	log.fine("Starting findSubJunction " + junction.getFullName() + " " + offset + " " + junction.infoString());
	for (int i = 0; i < junctions.size(); ++i) {
	    if (junctions.getChild(i) instanceof StrandJunction3D) {
		StrandJunction3D junction2 = (StrandJunction3D)(junctions.getChild(i));
		if (isSubJunction(junction, junction2, offset)) {
		    log.fine("Subjunction found: " + junction2.infoString());
		    return i;
		}
	    }
	}
	log.fine("Finished findSubJunction " + junction.getFullName() + " " + offset + " : -1");
	return -1;
    }

    /** returns true if junction is plausible */
    public static boolean verifyJunction(StrandJunction3D junction,
					 Object3D tree, Object3D junctionsZero, int offset) {

	// File file = new File("/tmp/nanotiler_debug_testfile.txt");
	// 	try {
	// 	    FileOutputStream fos = new FileOutputStream(file, true);
	// 	    PrintWriter out = new PrintWriter(fos);
	if (junction.getStrandCount() != junction.getBranchCount()) {
	    log.info("Strand count is not equal to branch count " + junction.getFullName());
	    if (regularJunctionMode) {
		return false;
	    }
	}
	// test if all strands unique:
	List<NucleotideStrand> strandSet = new ArrayList<NucleotideStrand>();
	for (int i = 0; i < junction.getStrandCount(); ++i) {
	    NucleotideStrand strand = junction.getStrand(i);
	    log.fine("Testing junction sequence: " + strand.sequenceString());
	    if (CollectionTools.containsIdentical(strandSet, strand) != null) {
		log.info("Junction does not validate because it containts identical strands: " + junction.getFullName());
		return false; // all strands should be different entities
	    }
	    for (NucleotideStrand strand2 : strandSet) {
		if (strand2.isProbablyEqual(strand)) {
		    log.info("Junction does not validate because it containts probably equal strands: " + junction.getFullName());
		    return false; // no duplicates allowed
		}
	    }
	    strandSet.add(strand);

	}
	for (int i = 0; i < junction.getBranchCount(); ++i) {
		BranchDescriptor3D branch = junction.getBranch(i);
		log.fine("Testing branch " + (i+1) + " " + branch.getPosition() + " : " + branch.getDirection() 
			 + " " + branch.getIncomingStrand().sequenceString() + " " + branch.getOutgoingStrand().sequenceString());
		double ang = branch.getRelativePosition().angle(branch.getDirection());
		// TODO: return false: for right now, let all angles pass: make the limit less when nanotiler is working well.
		if (ang > COLLINEAR_LIMIT_RAD) {
		    // 		    log.fine("ang: " + ang);
		    // 		    out.println("ang: " + ang + " > COLLINEAR_LIMIT_RAD");
		    // 		    out.close();
		    log.fine("ang: " + ang + " > COLLINEAR_LIMIT_RAD");
		    //		    return false; // branch descriptor direction is not collinear
		}
		if (branch.getIncomingStrand() == branch.getOutgoingStrand()) {
		    log.info("Junction does not validate because an incoming strand is equal to an outgoing strand: " + junction.getFullName());
		    return false; // something went wrong, the strands should only be equal for a kissing loop
		}
	    }
	    Object3DSet atomSet = Object3DTools.collectByClassName(tree, "Atom3D"); // get all atoms
	    if (atomSet.size() == 0) {
		log.info("Junction does not validate because it does not contain any atoms: " + junction.getFullName());
		return false;
	    }
	    Vector3D pos = junction.getPosition();
	    double dMin = atomSet.get(0).getPosition().distance(pos);
	    for (int i = 1; i < atomSet.size(); ++i) {
		Object3D obj = atomSet.get(i);
		double d = obj.getPosition().distance(pos);
		if (d < dMin) {
		    dMin = d;
		}
	    }
	    if (dMin > JUNCTION_DIST_MAX) {
		//TODO: return false again once nanotiler is working well.
		log.fine("dMin > JUNCTION_DIST_MAX");
		//		return false;
	    }
	    // check if steric clash
	    boolean clashFree = StrandJunctionTools.stericalCheck(junction);
	    if (!clashFree) {
		log.info("Clash detected! Omitting junction: " + junction.getFullName() );
		return false;
	    }
	    // must have corresponding junction with zero offset
	    if ((!SUBJUNCTION_ALLOWED) && (junctionsZero != null)) {
		if (findSubJunction(junction, junctionsZero, offset) < 0) {
		    log.info("sub-junction detected! Omitting junction: " + junction.getFullName() );
		    return false;
		}
	    }
	    return true;
    }

    /** returns true if junction is plausible
     * TODO : sterical check */
    public static boolean verifyKissingLoop(KissingLoop3D kissingLoop,
					    Object3D tree) {

	// File file = new File("/tmp/nanotiler_debug_testfile.txt");
	// try {
	// 	    FileOutputStream fos = new FileOutputStream(file, true);
	// 	    PrintWriter out = new PrintWriter(fos);
	  
	for (int i = 0; i < kissingLoop.getBranchCount(); ++i) {
	    BranchDescriptor3D branch = kissingLoop.getBranch(i);
	    double ang = branch.getRelativePosition().angle(branch.getDirection());
	    if (ang > COLLINEAR_LIMIT_RAD) {
		log.fine("ang: " + ang + " > COLLINEAR_LIMIT_RAD");
		// out.println("ang: " + ang + " > COLLINEAR_LIMIT_RAD");
		// out.close();
		return false; // branch descriptor direction is not collinear
	    }
	}
	Object3DSet atomSet = Object3DTools.collectByClassName(tree, "Atom3D"); // get all atoms
	if (atomSet.size() == 0) {
	    // 		out.println("atomSet.size() == 0");
	    // 		out.close();
	    log.fine("atomSet.size() == 0");
	    return false;
	}
	Vector3D pos = kissingLoop.getPosition();
	double dMin = atomSet.get(0).getPosition().distance(pos);
	for (int i = 1; i < atomSet.size(); ++i) {
	    Object3D obj = atomSet.get(i);
	    double d = obj.getPosition().distance(pos);
	    if (d < dMin) {
		dMin = d;
	    }
	}
	if (dMin > KISSINGLOOP_DIST_MAX) {
	    // 		out.println("dMin > KISSINGLOOP_DIST_MAX");
	    // 		out.close();
	    log.fine("dMin > KISSINGLOOP_DIST_MAX");
	    return false;
	}
	// check if steric clash
	// 	boolean clashFree = StrandJunctionTools.stericalCheck(kissingLoop);
	// 	if (!clashFree) {
	// 	    log.fine("Clash detected! Omitting kissing loop!");
	// 	    return false;
	// 	}
	// 	    out.close();
	// 	}
	// 	catch(IOException ioe) {}
	return true;
    }

    /** reads a set of PDB files, parses them to junctions and adds this to the database */
    public static void parsePdbFilesToDB(StrandJunctionDB junctionDB, 
					 StrandJunctionDB kissingLoopDB,
					 String fileName,
					 int cloneDownSize,
					 String readDir,
					 String writeDir,
					 boolean noWriteFlag,
					 boolean rnaviewMode,
					 boolean randomMode,
					 int branchDescriptorOffset,
					 int stemLengthMin,
					 CorridorDescriptor corridorDescriptor,
					 int loopLengthSumMax) throws IOException {
	assert (fileName != null) && (fileName.length() > 0);
	assert rnaviewMode;
	assert branchDescriptorOffset >= 0;
	String idname = fileName.substring(0,4);
	Date startDate = new Date();
        long startTime = new Long(startDate.getTime());
	log.info("Starting StrandJunctionDBTools.parsePdbFilesToDB(2)");
	ForceField collisionDetector = new CollisionForceField();
	String frontChar = fileName.substring(0,1);
	if ( (!frontChar.equals(SLASH)) &&
	     (!frontChar.equals(".")) &&
	     (readDir != null) && 
	     (readDir.length() > 0) ) {
	    fileName = readDir + SLASH + fileName;
	}
	log.info("Processing file : " + fileName);
	int id = 0; // Unique id assigned to each junction in the file
	// Check if file was generated by NanoTiler
	if (fileName.endsWith(NTL_EXTENSION)) {
	    log.fine("NanoTiler generated file found: " + fileName);
	    StrandJunction3D ntlFile = readNtl(fileName);
	    LinkSet links = new SimpleLinkSet(); // TODO : read links also!?
	    ntlFile.setFilename(idname + intToChar(id));
	    id++;
	    if (ntlFile instanceof KissingLoop3D) {
		kissingLoopDB.addJunction(ntlFile, links);
	    }
	    else {
		junctionDB.addJunction(ntlFile, links);
	    }
	}
	else {
// 	    String writeFileName = fileName;
// 	    if (!noWriteFlag && (writeDir != null) && (writeFileName != null)) {
// 		(new File(writeDir)).mkdir();
// 		writeFileName = writeFileName.substring(0, writeFileName.indexOf(".pdb"));
// 		writeFileName = writeDir + SLASH + writeFileName + SLASH + writeFileName + ".ntl";
// 	    }
	    
	    InputStream is = new FileInputStream(fileName);
	    
	    Object3DFactory reader;
	    if (rnaviewMode) {
		reader = new RnaPdbRnaviewReader();
	    }
	    else {
		log.severe("Only reading of RNAview augmented PDB files is currently supported.");
		assert false; // not supported anymore
		reader = new RnaPdbReader();
	    }
	    Object3DLinkSetBundle bundle = reader.readBundle(is);
	    Object3D root = bundle.getObject3D();
	    LinkSet links = bundle.getLinks();
	    LinkSet emptyLinks = new SimpleLinkSet();
// 	    log.fine("Number of read links: " + links.size());
// 	    for (int i = 0; i < links.size(); ++i) {
// 		System.out.println("" + links.get(i));
// 	    }
	    if (randomMode) {
		log.fine("NOT randomizing orientation of object tree: " + root.getName());
		// Object3DTools.randomizeOrientation(root);
	    }
	    Object3DSet atomSet = Object3DTools.collectByClassName(root, "Atom3D"); // get all atoms
	    log.fine(" " + atomSet.size() + " atoms found in PDB file.");
	    double collisionEnergy = collisionDetector.energy(atomSet, links);
	    if (collisionEnergy > 0.0) {
		log.warning("Collisions detected in structure: " + fileName);
	    }
	    else {
		log.fine("No collisions detected!");
	    }
	    Object3DLinkSetBundle stemBundle = null;
	    if (rnaviewMode) {
		stemBundle = StemTools.generateStemsFromLinks(root, links, stemLengthMin);
		log.fine("Generated " + stemBundle.getObject3D().size() + " stems using a minimum length cutoff " + stemLengthMin);
	    }
	    else {
		assert false;
		stemBundle = StemTools.generateStems(root);
	    }
	    Object3D stemRoot = stemBundle.getObject3D();
	    Object3DTools.balanceTree(stemRoot);
	    if (stemRoot.size() > 0) {
		log.fine("StrandJunctionDBTools: Generating junctions from " + stemRoot.size() + " stems.");
		Object3D junctions = BranchDescriptorTools.generateJunctions(stemRoot, "dummy", 
			     branchDescriptorOffset, corridorDescriptor, stemFitRmsTolerance, loopLengthSumMax);
		Object3D junctionsZero = null;
		// for verifying junctions: must work also for zero offset

	
		if (branchDescriptorOffset != 0) {
		    junctionsZero = BranchDescriptorTools.generateJunctions(stemRoot, "dummy",
									    0, corridorDescriptor, stemFitRmsTolerance, loopLengthSumMax);
		    for (int j = junctionsZero.size()-1; j >= 0; --j) {
			if (!verifyJunction((StrandJunction3D)(junctionsZero.getChild(j)), root, null, 0)) {
			    junctionsZero.removeChild(j);
			}
		    }
		}
		log.fine("Number of detected junctions in structure: " + junctions.size());
		for (int j =0; j < junctions.size(); ++j) {
		    StrandJunction3D junction = (StrandJunction3D)(junctions.getChild(j));
		    String junctionName = fileName + "_junc" + (j+1); // do not lose file name info (with for example PDB identifier)
		    if (cloneDownSize < 1) {
			// junction.setName(junctionName);
			junction.setProperty("filename_orig", fileName); // orginal final name of PDB file
			junction.setProperty("filename", junctionName);
			
			if (!verifyJunction(junction, root, junctionsZero, branchDescriptorOffset)) {
			    log.info("junction " + junctionName + " with position: " + junction.getPosition()
				     + " is not usable!");
			}
			else {
			    log.fine("junction of order " + junction.getBranchCount() + " and name " + junctionName + " is usable. Adding junction!");
			    RnaNtlWriter ntlWriter = new RnaNtlWriter();
			    if ((writeDir != null) && (!writeDir.equals(""))) {
				ntlWriter.write(writeDir, junctionName, junction); // Write the junction to a file.
			    }

			    junction.setFilename(idname + intToChar(id));
			    id++;

			    junctionDB.addJunction(junction, emptyLinks);
			}
		    } else { // generate from a junction of order m a set of junctions of order n < m
			Object3DSet smallerJunctions = junction.cloneDown(cloneDownSize);
			for (int i = 0; i < smallerJunctions.size(); ++i) {
			    StrandJunction3D sJunction = (StrandJunction3D)(smallerJunctions.get(i));
			    assert (sJunction.getBranchCount() == cloneDownSize);
			    String sJunctionName = junctionName + "_" + (i + 1);
			    sJunction.setProperty("filename_orig", fileName); // orginal final name of PDB file
			    sJunction.setProperty("filename", junctionName);
			    sJunction.setName(sJunctionName);
			    if (!verifyJunction(sJunction, root, junctionsZero, branchDescriptorOffset)) {
				log.info("junction " + junctionName + " with position: " + junction.getPosition()
					 + " is not usable!");
			    }
			    else {
				junction.setFilename(idname + intToChar(id));
				id++;
				log.info("Adding Junction!");
				junctionDB.addJunction(sJunction, emptyLinks);
			    }
			}
		    }
		}
		Object3D kissingLoops = BranchDescriptorTools.generateKissingLoops(stemRoot, "dummy", 
										   branchDescriptorOffset, stemFitRmsTolerance);
		LinkSet tmpLinks = new SimpleLinkSet(); // TODO : no links read read
		log.fine("Number of detected kissing loops in structure: " + kissingLoops.size());
		for (int j =0; j < kissingLoops.size(); ++j) {
		    KissingLoop3D kissingLoop = (KissingLoop3D)(kissingLoops.getChild(j));
		    String name = fileName + "_kl" + (j+1); // do not lose file name info (with for example PDB identifier)
		    kissingLoop.setName(name);
		    kissingLoop.setProperty("filename_orig", fileName);
		    kissingLoop.setProperty("filename", name);
		    log.fine("Adding kissing loop " + kissingLoop.getName() + " with position: " + kissingLoop.getPosition());
		    if (!verifyKissingLoop(kissingLoop, root)) { 
			log.info("kissing loop " + kissingLoop.getName() + " with position: " + kissingLoop.getPosition()
				 + " is not usable!");
		    }
		    else {
			log.fine("kissing loop " + kissingLoop.getName() + " was usable");
			String kissingLoopName = new String();
			RnaNtlWriter ntlWriter = new RnaNtlWriter();
			if ((writeDir != null) && (!writeDir.equals(""))) {
			    ntlWriter.write(writeDir, kissingLoop.getName(), kissingLoop);
			}
			log.fine("Adding kissing loop. Number of current entries: " + kissingLoopDB.size(2));
			
			kissingLoop.setFilename(idname + intToChar(id));
			id++;
			
			kissingLoopDB.addJunction(kissingLoop, tmpLinks);
			log.fine("Added kissing loop. Number of current entries: " + kissingLoopDB.size(2));
			assert kissingLoopDB.size(2) > 0; // must have been added
		    }
		}
	    }
	    else {
		log.info("Could not find any stems using stem length min " + stemLengthMin + " and links: " );
		for (int i = 0; i < links.size(); ++i) {
		    if (RnaLinkTools.isBasePairLink(links.get(i))) {
			System.out.println("" + (i+1) + links.get(i).toString());
		    }
		}
	    }
	    is.close();
	}	
	Date endDate = new Date();
	long endTime = new Long(endDate.getTime());
	Long totalMilli = new Long(endTime - startTime);
	int totalSec = (totalMilli.intValue() / 1000);
	int totalMin = (totalSec / 60);
	totalSec -= (totalMin * 60);
	int totalHour = (totalMin / 60);
	totalMin -= (totalHour * 60);
	String timeString = "";
	if (totalHour < 10) {
	    timeString += "0";
	}
	timeString += totalHour + ":";
	if (totalMin < 10) {
	    timeString += "0";
	}
	timeString += totalMin + ":";
	if (totalSec < 10) {
	    timeString += "0";
	}
	timeString += totalSec;
	log.fine(NEWLINE + NEWLINE + "parsePdbFilesToDB(2) took " + timeString + NEWLINE);
	log.fine("parsePdbFilesToDB (2) ended");
    } // parsePdbFilesToDB, no check


    /** reads a set of PDB files, parses them to junctions and adds this to the database, with IntHelicesCheck option */
    public static void parsePdbFilesToDB(StrandJunctionDB junctionDB, 
					 StrandJunctionDB kissingLoopDB,
					 String fileName,
					 int cloneDownSize,
					 String readDir,
					 String writeDir,
					 boolean noWriteFlag,
					 boolean rnaviewMode,
					 boolean randomMode,
					 int branchDescriptorOffset,
					 int stemLengthMin,
					 CorridorDescriptor corridorDescriptor,
					 int loopLengthSumMax,
					 boolean IntHelicesCheck ) throws IOException {
	assert (fileName != null) && (fileName.length() > 0);
	assert rnaviewMode;
	assert branchDescriptorOffset >= 0;

	String idname = fileName.substring(0,4);

	Date startDate = new Date();
        long startTime = new Long(startDate.getTime());
	log.info("Starting StrandJunctionDBTools.parsePdbFilesToDB");
	ForceField collisionDetector = new CollisionForceField();
	String frontChar = fileName.substring(0,1);
	if ( (!frontChar.equals(SLASH)) &&
	     (!frontChar.equals(".")) &&
	     (readDir != null) && 
	     (readDir.length() > 0) ) {
	    fileName = readDir + SLASH + fileName;
	}

	log.info("Processing file : " + fileName);
	
	int id = 0; // Unique id assigned to each junction in the file

	// Check if file was generated by NanoTiler
	if (fileName.endsWith(NTL_EXTENSION)) {
	    log.fine("NanoTiler generated file found: " + fileName);
	    StrandJunction3D ntlFile = readNtl(fileName);
	    LinkSet links = new SimpleLinkSet(); // TODO : read links also!?

	    ntlFile.setFilename(idname + intToChar(id));
	    id++;

	    if (ntlFile instanceof KissingLoop3D) {
		kissingLoopDB.addJunction(ntlFile, links);
	    }
	    else {
		junctionDB.addJunction(ntlFile, links);
	    }
	}
	else {
// 	    String writeFileName = fileName;
// 	    if (!noWriteFlag && (writeDir != null) && (writeFileName != null)) {
// 		(new File(writeDir)).mkdir();
// 		writeFileName = writeFileName.substring(0, writeFileName.indexOf(".pdb"));
// 		writeFileName = writeDir + SLASH + writeFileName + SLASH + writeFileName + ".ntl";
// 	    }
	    
	    InputStream is = new FileInputStream(fileName);
	    
	    Object3DFactory reader;
	    if (rnaviewMode) {
		reader = new RnaPdbRnaviewReader();
	    }
	    else {
		log.severe("Only reading of RNAview augmented PDB files is currently supported.");
		assert false; // not supported anymore
		reader = new RnaPdbReader();
	    }
	    Object3DLinkSetBundle bundle = reader.readBundle(is);
	    Object3D root = bundle.getObject3D();
	    LinkSet links = bundle.getLinks();
	    LinkSet emptyLinks = new SimpleLinkSet();
// 	    log.fine("Number of read links: " + links.size());
// 	    for (int i = 0; i < links.size(); ++i) {
// 		System.out.println("" + links.get(i));
// 	    }
	    if (randomMode) {
		log.fine("NOT randomizing orientation of object tree: " + root.getName());
		// Object3DTools.randomizeOrientation(root);
	    }
	    Object3DSet atomSet = Object3DTools.collectByClassName(root, "Atom3D"); // get all atoms
	    log.fine(" " + atomSet.size() + " atoms found in PDB file.");
	    double collisionEnergy = collisionDetector.energy(atomSet, links);
	    if (collisionEnergy > 0.0) {
		log.warning("Sorry, collisions detected in structure: " + fileName);
	    }
	    else {
		log.fine("No collisions detected!");
	    }
	    Object3DLinkSetBundle stemBundle = null;
	    if (rnaviewMode) {
		stemBundle = StemTools.generateStemsFromLinks(root, links, stemLengthMin );
		log.fine("Generated " + stemBundle.getObject3D().size() + " stems using a minimum length cutoff " + stemLengthMin);
	    }
	    else {
		assert false;
		stemBundle = StemTools.generateStems(root);
	    }
	    Object3D stemRoot = stemBundle.getObject3D();
	    Object3DTools.balanceTree(stemRoot);
	    if (stemRoot.size() > 0) {
		log.fine("StrandJunctionDBTools: Generating junctions from " + stemRoot.size() + " stems.");
		Object3D junctions = BranchDescriptorTools.generateJunctions(stemRoot, "dummy", 
									     branchDescriptorOffset, corridorDescriptor, stemFitRmsTolerance, loopLengthSumMax, IntHelicesCheck);
		Object3D junctionsZero = null;
		// for verifying junctions: must work also for zero offset

	
		if (branchDescriptorOffset != 0) {
		    junctionsZero = BranchDescriptorTools.generateJunctions(stemRoot, "dummy",
									    0, corridorDescriptor, stemFitRmsTolerance, loopLengthSumMax);
		    for (int j = junctionsZero.size()-1; j >= 0; --j) {
			if (!verifyJunction((StrandJunction3D)(junctionsZero.getChild(j)), root, null, 0)) {
			    junctionsZero.removeChild(j);
			}
		    }
		}
		log.fine("Number of detected junctions in structure: " + junctions.size());
		for (int j =0; j < junctions.size(); ++j) {
		    StrandJunction3D junction = (StrandJunction3D)(junctions.getChild(j));
		    String junctionName = fileName + "_junc" + (j+1); // do not lose file name info (with for example PDB identifier)
		    if (cloneDownSize < 1) {
			// junction.setName(junctionName);
			junction.setProperty("filename_orig", fileName); // orginal final name of PDB file
			junction.setProperty("filename", junctionName);
			
			if (!verifyJunction(junction, root, junctionsZero, branchDescriptorOffset)) {
			    log.info("junction " + junctionName + " with position: " + junction.getPosition()
				     + " is not usable!");
			}
			else {
			    log.fine("junction of order " + junction.getBranchCount() + " and name " + junctionName + " is usable. Adding junction!");
			    RnaNtlWriter ntlWriter = new RnaNtlWriter();
			    if ((writeDir != null) && (!writeDir.equals(""))) {
				ntlWriter.write(writeDir, junctionName, junction); // Write the junction to a file.
			    }

			    junction.setFilename(idname + intToChar(id));
			    id++;

			    junctionDB.addJunction(junction, emptyLinks);
			}
		    } else { // generate from a junction of order m a set of junctions of order n < m
			Object3DSet smallerJunctions = junction.cloneDown(cloneDownSize);
			for (int i = 0; i < smallerJunctions.size(); ++i) {
			    StrandJunction3D sJunction = (StrandJunction3D)(smallerJunctions.get(i));
			    assert (sJunction.getBranchCount() == cloneDownSize);
			    String sJunctionName = junctionName + "_" + (i + 1);
			    sJunction.setProperty("filename_orig", fileName); // orginal final name of PDB file
			    sJunction.setProperty("filename", junctionName);
			    sJunction.setName(sJunctionName);
			    if (!verifyJunction(sJunction, root, junctionsZero, branchDescriptorOffset)) {
				log.info("junction " + junctionName + " with position: " + junction.getPosition()
					 + " is not usable!");
			    }
			    else {
				junction.setFilename(idname + intToChar(id));
				id++;
				log.info("Adding Junction!");
				junctionDB.addJunction(sJunction, emptyLinks);
			    }
			}
		    }
		}
		Object3D kissingLoops = BranchDescriptorTools.generateKissingLoops(stemRoot, "dummy", 
										   branchDescriptorOffset, stemFitRmsTolerance,IntHelicesCheck);
		LinkSet tmpLinks = new SimpleLinkSet(); // TODO : no links read read
		log.fine("Number of detected kissing loops in structure: " + kissingLoops.size());
		for (int j =0; j < kissingLoops.size(); ++j) {
		    KissingLoop3D kissingLoop = (KissingLoop3D)(kissingLoops.getChild(j));
		    String name = fileName + "_kl" + (j+1); // do not lose file name info (with for example PDB identifier)
		    kissingLoop.setName(name);
		    kissingLoop.setProperty("filename_orig", fileName);
		    kissingLoop.setProperty("filename", name);
		    log.fine("Adding kissing loop " + kissingLoop.getName() + " with position: " + kissingLoop.getPosition());
		    if (!verifyKissingLoop(kissingLoop, root)) { 
			log.info("kissing loop " + kissingLoop.getName() + " with position: " + kissingLoop.getPosition()
				 + " is not usable!");
		    }
		    else {
			log.fine("kissing loop " + kissingLoop.getName() + " was usable");
			String kissingLoopName = new String();
			RnaNtlWriter ntlWriter = new RnaNtlWriter();
			if ((writeDir != null) && (!writeDir.equals(""))) {
			    ntlWriter.write(writeDir, kissingLoop.getName(), kissingLoop);
			}
			log.fine("Adding kissing loop. Number of current entries: " + kissingLoopDB.size(2));
			
			kissingLoop.setFilename(idname + intToChar(id));
			id++;
			
			kissingLoopDB.addJunction(kissingLoop, tmpLinks);
			log.fine("Added kissing loop. Number of current entries: " + kissingLoopDB.size(2));
			assert kissingLoopDB.size(2) > 0; // must have been added
		    }
		}
	    }
	    else {
		log.info("Could not find any stems using stem length min " + stemLengthMin + " and links: " );
		for (int i = 0; i < links.size(); ++i) {
		    if (RnaLinkTools.isBasePairLink(links.get(i))) {
			System.out.println("" + (i+1) + links.get(i).toString());
		    }
		}
	    }
	    is.close();
	}	
	Date endDate = new Date();
	long endTime = new Long(endDate.getTime());
	Long totalMilli = new Long(endTime - startTime);
	int totalSec = (totalMilli.intValue() / 1000);
	int totalMin = (totalSec / 60);
	totalSec -= (totalMin * 60);
	int totalHour = (totalMin / 60);
	totalMin -= (totalHour * 60);
	String timeString = "";
	if (totalHour < 10) {
	    timeString += "0";
	}
	timeString += totalHour + ":";
	if (totalMin < 10) {
	    timeString += "0";
	}
	timeString += totalMin + ":";
	if (totalSec < 10) {
	    timeString += "0";
	}
	timeString += totalSec;
	log.fine(NEWLINE + NEWLINE + "parsePdbFilesToDB(2) took " + timeString + NEWLINE);
	log.fine("parsePdbFilesToDB (2) ended");
    } // parsePdbFilesToDB,with intHelixCheck option.

    /** reads a set of PDB files, parses them to general motifs (class GeneralHelixMotif3D
     * and adds this to the database, with IntHelicesCheck option */
    public static void parsePdbFilesToMotifDB(StrandJunctionDB motifDB,
					      String fileName,
					      int cloneDownSize,
					      String readDir,
					      String writeDir,
					      boolean noWriteFlag,
					      boolean rnaviewMode,
					      boolean randomMode,
					      int branchDescriptorOffset,
					      int stemLengthMin,
					      CorridorDescriptor corridorDescriptor,
					      int loopLengthSumMax,
					      boolean IntHelicesCheck ) throws IOException {
	assert (fileName != null) && (fileName.length() > 0);
	assert (motifDB != null);
	assert rnaviewMode;
	assert branchDescriptorOffset >= 0;
	boolean added = false;
	String idname = fileName.substring(0,4);
	Date startDate = new Date();
        long startTime = new Long(startDate.getTime());
	log.info("Starting StrandJunctionDBTools.parsePdbFilesToMotifDB");
	ForceField collisionDetector = new CollisionForceField();
	String frontChar = fileName.substring(0,1);
	if ( (!frontChar.equals(SLASH)) &&
	     (!frontChar.equals(".")) &&
	     (readDir != null) && 
	     (readDir.length() > 0) ) {
	    fileName = readDir + SLASH + fileName;
	}
	log.info("Processing file : " + fileName);
	int id = 0; // Unique id assigned to each junction in the file
	    InputStream is = new FileInputStream(fileName);	    
	    Object3DFactory reader;
	    if (rnaviewMode) {
		reader = new RnaPdbRnaviewReader();
	    }
	    else {
		log.severe("Only reading of RNAview augmented PDB files is currently supported.");
		assert false; // not supported anymore
		reader = new RnaPdbReader();
	    }
	    Object3DLinkSetBundle bundle = reader.readBundle(is);
	    Object3D root = bundle.getObject3D();
	    LinkSet links = bundle.getLinks();
	    LinkSet emptyLinks = new SimpleLinkSet();
	    if (randomMode) {
		log.fine("NOT randomizing orientation of object tree: " + root.getName());
		// Object3DTools.randomizeOrientation(root);
	    }
	    Object3DSet atomSet = Object3DTools.collectByClassName(root, "Atom3D"); // get all atoms
	    Object3DSet additional = new SimpleObject3DSet();
	    additional.add(root); // add structure; it will be added to the motif junction
	    log.info(" " + atomSet.size() + " atoms found in PDB file.");
	    double collisionEnergy = collisionDetector.energy(atomSet, links);
	    if (collisionEnergy > 0.0) {
		log.warning("Sorry, collisions detected in structure: " + fileName);
	    }
	    else {
		log.fine("No collisions detected!");
	    }
	    Object3DLinkSetBundle stemBundle = null;
	    if (rnaviewMode) {
		stemBundle = StemTools.generateStemsFromLinks(root, links, stemLengthMin );
		log.fine("Generated " + stemBundle.getObject3D().size() + " stems using a minimum length cutoff " + stemLengthMin);
	    }
	    else {
		assert false;
		stemBundle = StemTools.generateStems(root);
	    }
	    Object3D stemRoot = stemBundle.getObject3D();
	    Object3DTools.balanceTree(stemRoot);
	    if (stemRoot.size() > 0) {
		log.info("StrandJunctionDBTools: Generating motifs from " + stemRoot.size() + " stems.");
		Object3DSet stemSet = Object3DTools.collectByClassName(stemRoot, "RnaStem3D");
		log.info("Identified " + stemSet.size() + " helices.");
		Object3DSet branchDescriptors = BranchDescriptorTools.generateBranchDescriptors(stemSet, branchDescriptorOffset, stemFitRmsTolerance);
		if (branchDescriptors.size() > 0) {
		    log.info("Identified " + branchDescriptors.size() + " helix ends.");
		    Object3DSet junctions = BranchDescriptorTools.generateGeneralMotifs(branchDescriptors, corridorDescriptor, additional);
		    log.info("Identified " + junctions.size() + " motifs.");
		    for (int j = 0; j < junctions.size(); ++j) {
			StrandJunction3D junction = (StrandJunction3D)(junctions.get(j));
			log.info("Adding junction of order " + junction.getBranchCount());
			String junctionName = fileName + "_junc" + (j+1); // do not lose file name info (with for example PDB identifier)
			// junction.setName(junctionName);
			junction.setProperty("filename_orig", fileName); // orginal final name of PDB file
			junction.setProperty("filename", junctionName);		    
			motifDB.addJunction(junction, emptyLinks);
			added = true; // a motif has been added
		    }
		} else {
		    log.info("No helix ends were identified that could be used for motif generation.");
		}
	    } else {
		log.info("No helices were identified that could be used for motif generation.");
	    }
	    if (!added) {
		log.warning("No structure has been added to the motif database.");
	    }
	    log.info("Finished StrandJunctionDBTools.parsePdbFilesToMotifDB");
    }

    /** Conver 0 to a, 1 to b, 2 to c, etc.*/
    private static String intToChar(int i) {
	String[] charVals = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
			      "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };
	int rem = i % 26;
	return charVals[rem];
    }


    /** reads a set of PDB files, parses them to junctions and adds this to the database */
    public static void parsePdbFilesToDB(StrandJunctionDB junctionDB, 
					 StrandJunctionDB kissingLoopDB,
					 String[] fileNames,
					 int[] cloneDownSizes,
					 String readDir,
					 String writeDir,
					 boolean noWriteFlag,
					 boolean rnaviewMode,
					 boolean randomMode,
					 int branchDescriptorOffset,
					 int stemLengthMin,
					 CorridorDescriptor corridorDescriptor,
					 int loopLengthSumMax) {
	log.warning("This version of the parsePdbFilesToDB method is deprecated!");
	assert false;
	assert fileNames != null;
	log.fine("Starting StrandJunctionDBTools.parsePdbFilesToDB(1)!");
	Date startDate = new Date();
	long startTime = new Long(startDate.getTime());
	ForceField collisionDetector = new CollisionForceField();
	if (fileNames == null) {
	    log.warning("No filenames specified for reading junction database!");
	    return;
	}
	for (int i = 0; i < fileNames.length; ++i) {
	    assert fileNames[i] != null;
	    assert fileNames[i].length() != 0;
	    int cloneDownSize = 0;
	    if (cloneDownSizes != null) {
		assert(i < cloneDownSizes.length);
		cloneDownSize = cloneDownSizes[i];
	    }
	    try {
		parsePdbFilesToDB(junctionDB, 
				  kissingLoopDB,
				  fileNames[i],
				  cloneDownSize,
				  readDir,
				  writeDir,
				  noWriteFlag,
				  rnaviewMode,
				  randomMode,
				  branchDescriptorOffset,
				  stemLengthMin,
				  corridorDescriptor,
				  loopLengthSumMax);
	    }
	    catch (IOException exp) {
		log.severe("Error: " + exp.getMessage() + " using input file: " + fileNames[i]);
	    }
	    
	} // for (int i ...
	log.fine("Generated junction database info: ");
	log.fine(junctionDBInfo(junctionDB)); 
	log.fine(junctionDB.infoString()); // list of all junctions
	Date endDate = new Date();
	long endTime = new Long(endDate.getTime());
	Long totalMilli = new Long(endTime - startTime);
	int totalSec = (totalMilli.intValue() / 1000);
	int totalMin = (totalSec / 60);
	totalSec -= (totalMin * 60);
	int totalHour = (totalMin / 60);
	totalMin -= (totalHour * 60);
	String timeString = "";
	if (totalHour < 10) {
	    timeString += "0";
	}
	timeString += totalHour + ":";
	if (totalMin < 10) {
	    timeString += "0";
	}
	timeString += totalMin + ":";
	if (totalSec < 10) {
	    timeString += "0";
	}
	timeString += totalSec;
	log.fine(NEWLINE + NEWLINE + "parsePdbFilesToDB(1) took " + timeString + NEWLINE);
	log.fine("parsePdbFilesToDB(1) ended");
    } // parsePdbFilesToDB

  /** reads a set of PDB files, parses them to junctions and adds this to the database */
    public static void parsePdbFilesToDB(StrandJunctionDB junctionDB, 
					 StrandJunctionDB kissingLoopDB,
					 StrandJunctionDB motifDB,
					 String[] fileNames,
					 int[] cloneDownSizes,
					 String readDir,
					 String writeDir,
					 boolean noWriteFlag,
					 boolean rnaviewMode,
					 boolean randomMode,
					 int branchDescriptorOffset,
					 int stemLengthMin,
					 CorridorDescriptor corridorDescriptor,
					 int loopLengthSumMax,
					 boolean intHelix,
					 boolean motifMode) {
	assert fileNames != null;
	log.info("Starting StrandJunctionDBTools.parsePdbFilesToDB(1) with motif mode " + motifMode + "!");
	Date startDate = new Date();
	long startTime = new Long(startDate.getTime());
	ForceField collisionDetector = new CollisionForceField();
	if (fileNames == null) {
	    log.warning("No filenames specified for reading junction database!");
	    return;
	}
	for (int i = 0; i < fileNames.length; ++i) {
	    assert fileNames[i] != null;
	    assert fileNames[i].length() != 0;
	    int cloneDownSize = 0;
	    if (cloneDownSizes != null) {
		assert(i < cloneDownSizes.length);
		cloneDownSize = cloneDownSizes[i];
	    }
	    try {
		if (!motifMode) {
		    parsePdbFilesToDB(junctionDB, 
				  kissingLoopDB,
				  fileNames[i],
				  cloneDownSize,
				  readDir,
				  writeDir,
				  noWriteFlag,
				  rnaviewMode,
				  randomMode,
				  branchDescriptorOffset,
				  stemLengthMin,
				  corridorDescriptor,
				  loopLengthSumMax, intHelix);
		} else {
		    parsePdbFilesToMotifDB(motifDB, 
				  fileNames[i],
				  cloneDownSize,
				  readDir,
				  writeDir,
				  noWriteFlag,
				  rnaviewMode,
				  randomMode,
				  branchDescriptorOffset,
				  stemLengthMin,
				  corridorDescriptor,
				  loopLengthSumMax, intHelix);
		}
	    }
	    catch (IOException exp) {
		log.severe("Error: " + exp.getMessage() + " using input file: " + fileNames[i]);
	    }
	    
	} // for (int i ...
	log.fine("Generated junction database info: ");
	log.fine(junctionDBInfo(junctionDB)); 
	log.fine(junctionDB.infoString()); // list of all junctions
	Date endDate = new Date();
	long endTime = new Long(endDate.getTime());
	Long totalMilli = new Long(endTime - startTime);
	int totalSec = (totalMilli.intValue() / 1000);
	int totalMin = (totalSec / 60);
	totalSec -= (totalMin * 60);
	int totalHour = (totalMin / 60);
	totalMin -= (totalHour * 60);
	String timeString = "";
	if (totalHour < 10) {
	    timeString += "0";
	}
	timeString += totalHour + ":";
	if (totalMin < 10) {
	    timeString += "0";
	}
	timeString += totalMin + ":";
	if (totalSec < 10) {
	    timeString += "0";
	}
	timeString += totalSec;
	log.fine(NEWLINE + NEWLINE + "parsePdbFilesToDB(1) took " + timeString + NEWLINE);
	log.fine("parsePdbFilesToDB(1) ended");
    } // parsePdbFilesToDB


    /**
     * Reads a "ntl" file
     * Needs to be tested more: not sure if there are things it cannot read.
     *
     * @author Christine Viets
     *
     * @param fileName The "ntl" file to read.
     */
    public static StrandJunction3D readNtl(String fileName) {
	assert fileName.endsWith(NTL_EXTENSION);
	try {
	    InputStream is = new FileInputStream(fileName);
	    DataInputStream dis = new DataInputStream(is);
	    assert readWord(dis).equals("nanotiler");
	    String word = readWord(dis);
	    if (word.equals("(StrandJunction3D")) {
		String junctionName = readWord(dis);
		word = readWord(dis); // Should read "(v3" for Vector3D position.
		assert word.equals("(v3");
		Vector3D junctionPos = new Vector3D();
		junctionPos.setX(Double.parseDouble(readWord(dis)));
		junctionPos.setY(Double.parseDouble(readWord(dis)));
		junctionPos.setZ(Double.parseDouble(readWord(dis)));
		assert readWord(dis).equals(")"); // )Vector3D
		
		/* Read junction children. */
		assert readWord(dis).equals("(children");
		int junctionChildren = Integer.parseInt(readWord(dis));
		Object3DSet branches = new SimpleObject3DSet();
		for (int i_A = 0; i_A < junctionChildren; i_A++) {
		    String junctionChild = readWord(dis);
		    if (junctionChild.equals("(BranchDescriptor3D")) {
			String branchName = readWord(dis);
			
			/* Read incoming strand. */
			assert readWord(dis).equals("(RnaStrand");
			RnaStrand incoming = new SimpleRnaStrand();
			incoming.setName(readWord(dis));
			assert readWord(dis).equals("(v3");
			Vector3D incomingPos = new Vector3D();
			incomingPos.setX(Double.parseDouble(readWord(dis)));
			incomingPos.setY(Double.parseDouble(readWord(dis)));
			incomingPos.setZ(Double.parseDouble(readWord(dis)));
			incoming.setPosition(incomingPos);
			assert readWord(dis).equals(")"); // )Vector3D
			assert readWord(dis).equals("(children");
			int incomingChildren = Integer.parseInt(readWord(dis));
			assert incoming != null;
			for (int i_B = 0; i_B < incomingChildren; i_B++) {
			    assert incoming != null;
			    assert readWord(dis).equals("(Nucleotide3D");
			    Nucleotide3D nucleotide = new Nucleotide3D();
			    char symbol = readWord(dis).charAt(0);
			    String alphaString = readWord(dis);
			    int alphaType = SequenceTools.UNKNOWN_SEQUENCE;
			    if (alphaString.equals("DNA")) {
				alphaType = SequenceTools.DNA_SEQUENCE;
			    }
			    else if (alphaString.equals("DNA_A")) {
				alphaType = SequenceTools.AMBIGUOUS_DNA_SEQUENCE;
			    }
			    else if (alphaString.equals("RNA")) {
				alphaType = SequenceTools.RNA_SEQUENCE;
			    }
			    else if (alphaString.equals("RNA_A")) {
				alphaType = SequenceTools.AMBIGUOUS_RNA_SEQUENCE;
			    }
			    try {
				nucleotide.setSymbol(new SimpleLetterSymbol(symbol, new SimpleAlphabet(alphaType)));
			    }
			    catch (UnknownSymbolException e) {
				log.severe("Generated UnknownSymbolException while reading NanoTiler-generated file!");
			    }
			    nucleotide.setName(readWord(dis));
			    assert readWord(dis).equals("(v3");
			    Vector3D nucleotidePos = new Vector3D();
			    nucleotidePos.setX(Double.parseDouble(readWord(dis)));
			    nucleotidePos.setY(Double.parseDouble(readWord(dis)));
			    nucleotidePos.setZ(Double.parseDouble(readWord(dis)));
			    nucleotide.setPosition(nucleotidePos);
			    assert readWord(dis).equals(")"); // )Vector3D
			    assert readWord(dis).equals("(children");
			    int nucleotideChildren = Integer.parseInt(readWord(dis));
			    for (int i_C = 0; i_C < nucleotideChildren; i_C++) {
				assert incoming != null;
				assert readWord(dis).equals("(Atom3D");
				Atom3D atom = new Atom3D();
				atom.setName(readWord(dis));
				assert readWord(dis).equals("(v3");
				Vector3D atomPos = new Vector3D();
				atomPos.setX(Double.parseDouble(readWord(dis)));
				atomPos.setY(Double.parseDouble(readWord(dis)));
				atomPos.setZ(Double.parseDouble(readWord(dis)));
				atom.setPosition(atomPos);
				assert readWord(dis).equals(")"); // )Vector3D
				assert readWord(dis).equals("(children");
				assert Integer.parseInt(readWord(dis)) == 0; // No children.
				assert readWord(dis).equals(")"); // )Children
				assert readWord(dis).equals(")"); // )Atom3D
				nucleotide.insertChild(atom);
			    }
			    assert readWord(dis).equals(")"); // )Nucleotide3D children
			    assert readWord(dis).equals(")"); // )Nucleotide3D
			    incoming.insertChild(nucleotide);
			}
			assert incoming != null;
			assert readWord(dis).equals(")"); // )RnaStrand children
			assert readWord(dis).equals("(Sequence");
			String incSequenceName = readWord(dis);
			String incTypeString = readWord(dis);
			int incSequenceType = SequenceTools.UNKNOWN_SEQUENCE;
			if (incTypeString.equals("DNA")) {
			    incSequenceType = SequenceTools.DNA_SEQUENCE;
			}
			else if (incTypeString.equals("DNA_A")) {
			    incSequenceType = SequenceTools.AMBIGUOUS_DNA_SEQUENCE;
			}
			else if (incTypeString.equals("RNA")) {
			    incSequenceType = SequenceTools.RNA_SEQUENCE;
			}
			else if (incTypeString.equals("RNA_A")) {
			    incSequenceType = SequenceTools.AMBIGUOUS_RNA_SEQUENCE;
			}
			String incSequenceString = readWord(dis);
			Alphabet incSequenceAlphabet = new SimpleAlphabet(incSequenceType);
			Sequence incSequence;
			try {
			    incSequence = new SimpleSequence(incSequenceString, incSequenceName, incSequenceAlphabet);
			}
			catch (UnknownSymbolException e) {
			    log.severe("UnknownSymbolException generated when reading NanoTiler generated junction.");
			}
			assert readWord(dis).equals(")"); // )Sequence
			assert readWord(dis).equals(")"); // )RnaStrand
			int incomingIndex = Integer.parseInt(readWord(dis));
			log.fine("Finished incoming strand.");
			assert incoming != null;
			
			/* Read outgoing strand. */
			assert readWord(dis).equals("(RnaStrand");
			RnaStrand outgoing = new SimpleRnaStrand();
			outgoing.setName(readWord(dis));
			assert readWord(dis).equals("(v3");
			Vector3D outgoingPos = new Vector3D();
			outgoingPos.setX(Double.parseDouble(readWord(dis)));
			outgoingPos.setY(Double.parseDouble(readWord(dis)));
			outgoingPos.setZ(Double.parseDouble(readWord(dis)));
			outgoing.setPosition(outgoingPos);
			assert readWord(dis).equals(")"); // )Vector3D
			assert readWord(dis).equals("(children");
			int outgoingChildren = Integer.parseInt(readWord(dis));
			for (int i_B = 0; i_B < outgoingChildren; i_B++) {
			    assert readWord(dis).equals("(Nucleotide3D");
			    Nucleotide3D nucleotide = new Nucleotide3D();
			    nucleotide.setName(readWord(dis));
			    char symbol = readWord(dis).charAt(0);
			    String alphaString = readWord(dis);
			    int alphaType = SequenceTools.UNKNOWN_SEQUENCE;
			    if (alphaString.equals("DNA")) {
				alphaType = SequenceTools.DNA_SEQUENCE;
			    }
			    else if (alphaString.equals("DNA_A")) {
				alphaType = SequenceTools.AMBIGUOUS_DNA_SEQUENCE;
			    }
			    else if (alphaString.equals("RNA")) {
				alphaType = SequenceTools.RNA_SEQUENCE;
			    }
			    else if (alphaString.equals("RNA_A")) {
				alphaType = SequenceTools.AMBIGUOUS_RNA_SEQUENCE;
			    }
			    try {
				nucleotide.setSymbol(new SimpleLetterSymbol(symbol, new SimpleAlphabet(alphaType)));
			    }
			    catch (UnknownSymbolException e) {
				log.severe("Generated UnknownSymbolException while reading NanoTiler-generated file!");
			    }
			    assert readWord(dis).equals("(v3");
			    Vector3D nucleotidePos = new Vector3D();
			    nucleotidePos.setX(Double.parseDouble(readWord(dis)));
			    nucleotidePos.setY(Double.parseDouble(readWord(dis)));
			    nucleotidePos.setZ(Double.parseDouble(readWord(dis)));
			    nucleotide.setPosition(nucleotidePos);
			    assert readWord(dis).equals(")"); // )Vector3D
			    assert readWord(dis).equals("(children");
			    int nucleotideChildren = Integer.parseInt(readWord(dis));
			    for (int i_C = 0; i_C < nucleotideChildren; i_C++) {
				assert readWord(dis).equals("(Atom3D");
				Atom3D atom = new Atom3D();
				atom.setName(readWord(dis));
				assert readWord(dis).equals("(v3");
				Vector3D atomPos = new Vector3D();
				atomPos.setX(Double.parseDouble(readWord(dis)));
				atomPos.setY(Double.parseDouble(readWord(dis)));
				atomPos.setZ(Double.parseDouble(readWord(dis)));
				atom.setPosition(atomPos);
				assert readWord(dis).equals(")"); // )Vector3D
				assert readWord(dis).equals("(children");
				assert Integer.parseInt(readWord(dis)) == 0; // No children.
				assert readWord(dis).equals(")"); // )Children
				assert readWord(dis).equals(")"); // )Atom3D
				nucleotide.insertChild(atom);
			    }
			    assert readWord(dis).equals(")"); // )Nucleotide3D children
			    assert readWord(dis).equals(")"); // )Nucleotide3D
			    outgoing.insertChild(nucleotide);
			}
			assert readWord(dis).equals(")"); // )RnaStrand children
			assert readWord(dis).equals("(Sequence");
			String outSequenceName = readWord(dis);
			String outTypeString = readWord(dis);
			int outSequenceType = SequenceTools.UNKNOWN_SEQUENCE;
			if (outTypeString.equals("DNA")) {
			    outSequenceType = SequenceTools.DNA_SEQUENCE;
			}
			else if (outTypeString.equals("DNA_A")) {
			    outSequenceType = SequenceTools.AMBIGUOUS_DNA_SEQUENCE;
			}
			else if (outTypeString.equals("RNA")) {
			    outSequenceType = SequenceTools.RNA_SEQUENCE;
			}
			else if (outTypeString.equals("RNA_A")) {
			    outSequenceType = SequenceTools.AMBIGUOUS_RNA_SEQUENCE;
			}
			String outSequenceString = readWord(dis);
			Alphabet outSequenceAlphabet = new SimpleAlphabet(outSequenceType);
			Sequence outSequence;
			try {
			    outSequence = new SimpleSequence(outSequenceString, outSequenceName, outSequenceAlphabet);
			}
			catch (UnknownSymbolException e) {
			    log.severe("UnknownSymbolException generated when reading NanoTiler generated junction.");
			}
			assert readWord(dis).equals(")"); // )Sequence
			assert readWord(dis).equals(")"); // )RnaStrand
			int outgoingIndex = Integer.parseInt(readWord(dis));
			log.fine("Finished outgoing strand.");
			
			Vector3D branchPosition = new Vector3D();
			assert readWord(dis).equals("(v3"); // Branch position.
			branchPosition.setX(Double.parseDouble(readWord(dis)));
			branchPosition.setY(Double.parseDouble(readWord(dis)));
			branchPosition.setZ(Double.parseDouble(readWord(dis)));
			assert readWord(dis).equals(")"); // )Vector3D
			
			assert readWord(dis).equals("(children"); // Branch children.
			int branchChildren = Integer.parseInt(readWord(dis));
			CoordinateSystem3D coordinateSystem = new CoordinateSystem3D(Vector3D.ZVEC);
			for (int i_D = 0; i_D < branchChildren; i_D++) {
			    String className = readWord(dis);
			    if (className.equals("(CoordinateSystem3D")) {
				coordinateSystem.setName(readWord(dis));
				Vector3D coordinatePos = new Vector3D();
				assert readWord(dis).equals("(v3");
				coordinatePos.setX(Double.parseDouble(readWord(dis)));
				coordinatePos.setY(Double.parseDouble(readWord(dis)));
				coordinatePos.setZ(Double.parseDouble(readWord(dis)));
				assert readWord(dis).equals(")"); // )Vector3D
				coordinateSystem.setPosition(coordinatePos);
				int coordinateChildren = Integer.parseInt(readWord(dis));
				for (int i_E = 0; i_E < coordinateChildren; i_E++) {
				    Vector3D childPos = new Vector3D();
				    assert readWord(dis).equals("(v3");
				    childPos.setX(Double.parseDouble(readWord(dis)));
				    childPos.setY(Double.parseDouble(readWord(dis)));
				    childPos.setZ(Double.parseDouble(readWord(dis)));
				    assert readWord(dis).equals(")"); // )Vector3D
				    childPos = childPos.plus(branchPosition); // When child is inserted, the parent position is subtracted.
				    Object3D child = new SimpleObject3D(childPos);
				    coordinateSystem.insertChild(child);
				}
				assert readWord(dis).equals(")"); // )CoordinateSystem3D
			    }
			    else {
				log.severe("NanoTiler does not know how to read: " + 
					   className + " as a child of a BranchDescriptor3D");
				assert false;
			    }
			}
			assert readWord(dis).equals(")"); // )children
			assert readWord(dis).equals(")"); // )BranchDescriptor3D
			BranchDescriptor3D branch = new SimpleBranchDescriptor3D(incoming, 
										 outgoing, 
										 incomingIndex, 
										 outgoingIndex, 
										 coordinateSystem);
 			branch.setName(branchName);
			branches.add(branch);
		    }
		    else if (junctionChild.equals("(RnaStrand")) {
			/* When the junction is created, the strands 
			   of each branch are added as children. 
			   There is no need to add them again. 
			   Just read over the RnaStrand. */
			log.fine("Reading over RnaStrand child."); //TODO: make faster: if you don't need the info, why read it over at all? Or just don't include it in the file!
			word = readWord(dis); // name or )RnaStrand
			if (word.equals(")")) { }
			else { // outdated: NanoTiler does not write this anymore. Once old files are no longer used, remove! Christine
			    readWord(dis); // (v3
			    readWord(dis); // x
			    readWord(dis); // y
			    readWord(dis); // z
			    readWord(dis); // )Vector3D
			    readWord(dis); // (children
			    int numChildren1 = Integer.parseInt(readWord(dis));
			    for (int i_1A = 0; i_1A < numChildren1; i_1A++) {
				readWord(dis); // (Nucleotide3D
				readWord(dis); // name
				readWord(dis); // symbol
				readWord(dis); // alphabet
				readWord(dis); // (v3
				readWord(dis); // x
				readWord(dis); // y
				readWord(dis); // z
				readWord(dis); // )Vector3D
				readWord(dis); // (children
				int numChildren2 = Integer.parseInt(readWord(dis));
				for (int i_1B = 0; i_1B < numChildren2; i_1B++) {
				    readWord(dis); // (Atom3D
				    readWord(dis); // name
				    readWord(dis); // (v3
				    readWord(dis); // x
				    readWord(dis); // y
				    readWord(dis); // z
				    readWord(dis); // )Vector3D
				    readWord(dis); // (children
				    readWord(dis); // numChildren
				    readWord(dis); // )children
				    readWord(dis); // )Atom3D
				}
				readWord(dis); // )Nucleotide3D children
				readWord(dis); // )Nucleotide3D
			    }
			    readWord(dis); // )RnaStrand children
			    readWord(dis); // (Sequence
			    readWord(dis); // name
			    readWord(dis); // type
			    readWord(dis); // sequence
			    readWord(dis); // )Sequence
			    readWord(dis); // )RnaStrand
			}
		    }
		    else {
			log.severe("NanoTiler does not know how to read: " + junctionChild);
			assert false;
		    }
		}
		
		StrandJunction3D junction = new SimpleStrandJunction3D(branches);
		junction.setName(junctionName);
		junction.setPosition(junctionPos);
		//	    junctionDB.addJunction(junction);
		is.close();
		return junction;
		
	    }
	    else if (word.equals("(KissingLoop3D")) {
		String kissingLoopName = readWord(dis);
		word = readWord(dis); // Should read "(v3" for Vector3D position.
		assert word.equals("(v3");
		Vector3D kissingLoopPos = new Vector3D();
		kissingLoopPos.setX(Double.parseDouble(readWord(dis)));
		kissingLoopPos.setY(Double.parseDouble(readWord(dis)));
		kissingLoopPos.setZ(Double.parseDouble(readWord(dis)));
		assert readWord(dis).equals(")"); // )Vector3D
		
		/* Read kissing loop children. */
		assert readWord(dis).equals("(children");
		int kissingLoopChildren = Integer.parseInt(readWord(dis));
		Object3DSet branches = new SimpleObject3DSet();
		for (int i_A = 0; i_A < kissingLoopChildren; i_A++) {
		    String kissingLoopChild = readWord(dis);
		    if (kissingLoopChild.equals("(BranchDescriptor3D")) {
			String branchName = readWord(dis);
			
			/* Read incoming strand. */
			assert readWord(dis).equals("(RnaStrand");
			RnaStrand incoming = new SimpleRnaStrand();
			incoming.setName(readWord(dis));
			assert readWord(dis).equals("(v3");
			Vector3D incomingPos = new Vector3D();
			incomingPos.setX(Double.parseDouble(readWord(dis)));
			incomingPos.setY(Double.parseDouble(readWord(dis)));
			incomingPos.setZ(Double.parseDouble(readWord(dis)));
			incoming.setPosition(incomingPos);
			assert readWord(dis).equals(")"); // )Vector3D
			assert readWord(dis).equals("(children");
			int incomingChildren = Integer.parseInt(readWord(dis));
			assert incoming != null;
			for (int i_B = 0; i_B < incomingChildren; i_B++) {
			    assert incoming != null;
			    assert readWord(dis).equals("(Nucleotide3D");
			    Nucleotide3D nucleotide = new Nucleotide3D();
			    char symbol = readWord(dis).charAt(0);
			    String alphaString = readWord(dis);
			    int alphaType = SequenceTools.UNKNOWN_SEQUENCE;
			    if (alphaString.equals("DNA")) {
				alphaType = SequenceTools.DNA_SEQUENCE;
			    }
			    else if (alphaString.equals("DNA_A")) {
				alphaType = SequenceTools.AMBIGUOUS_DNA_SEQUENCE;
			    }
			    else if (alphaString.equals("RNA")) {
				alphaType = SequenceTools.RNA_SEQUENCE;
			    }
			    else if (alphaString.equals("RNA_A")) {
				alphaType = SequenceTools.AMBIGUOUS_RNA_SEQUENCE;
			    }
			    try {
				nucleotide.setSymbol(new SimpleLetterSymbol(symbol, new SimpleAlphabet(alphaType)));
			    }
			    catch (UnknownSymbolException e) {
				log.severe("Generated UnknownSymbolException while reading NanoTiler-generated file!");
			    }
			    nucleotide.setName(readWord(dis));
			    assert readWord(dis).equals("(v3");
			    Vector3D nucleotidePos = new Vector3D();
			    nucleotidePos.setX(Double.parseDouble(readWord(dis)));
			    nucleotidePos.setY(Double.parseDouble(readWord(dis)));
			    nucleotidePos.setZ(Double.parseDouble(readWord(dis)));
			    nucleotide.setPosition(nucleotidePos);
			    assert readWord(dis).equals(")"); // )Vector3D
			    assert readWord(dis).equals("(children");
			    int nucleotideChildren = Integer.parseInt(readWord(dis));
			    for (int i_C = 0; i_C < nucleotideChildren; i_C++) {
				assert incoming != null;
				assert readWord(dis).equals("(Atom3D");
				Atom3D atom = new Atom3D();
				atom.setName(readWord(dis));
				assert readWord(dis).equals("(v3");
				Vector3D atomPos = new Vector3D();
				atomPos.setX(Double.parseDouble(readWord(dis)));
				atomPos.setY(Double.parseDouble(readWord(dis)));
				atomPos.setZ(Double.parseDouble(readWord(dis)));
				atom.setPosition(atomPos);
				assert readWord(dis).equals(")"); // )Vector3D
				assert readWord(dis).equals("(children");
				assert Integer.parseInt(readWord(dis)) == 0; // No children.
				assert readWord(dis).equals(")"); // )Children
				assert readWord(dis).equals(")"); // )Atom3D
				nucleotide.insertChild(atom);
			    }
			    assert readWord(dis).equals(")"); // )Nucleotide3D children
			    assert readWord(dis).equals(")"); // )Nucleotide3D
			    incoming.insertChild(nucleotide);
			}
			assert incoming != null;
			assert readWord(dis).equals(")"); // )RnaStrand children
			assert readWord(dis).equals("(Sequence");
			String incSequenceName = readWord(dis);
			String incTypeString = readWord(dis);
			int incSequenceType = SequenceTools.UNKNOWN_SEQUENCE;
			if (incTypeString.equals("DNA")) {
			    incSequenceType = SequenceTools.DNA_SEQUENCE;
			}
			else if (incTypeString.equals("DNA_A")) {
			    incSequenceType = SequenceTools.AMBIGUOUS_DNA_SEQUENCE;
			}
			else if (incTypeString.equals("RNA")) {
			    incSequenceType = SequenceTools.RNA_SEQUENCE;
			}
			else if (incTypeString.equals("RNA_A")) {
			    incSequenceType = SequenceTools.AMBIGUOUS_RNA_SEQUENCE;
			}
			String incSequenceString = readWord(dis);
			Alphabet incSequenceAlphabet = new SimpleAlphabet(incSequenceType);
			Sequence incSequence;
			try {
			    incSequence = new SimpleSequence(incSequenceString, incSequenceName, incSequenceAlphabet);
			}
			catch (UnknownSymbolException e) {
			    log.severe("UnknownSymbolException generated when reading NanoTiler generated kissing loop.");
			}
			assert readWord(dis).equals(")"); // )Sequence
			assert readWord(dis).equals(")"); // )RnaStrand
			int incomingIndex = Integer.parseInt(readWord(dis));
			log.fine("Finished incoming strand.");
			assert incoming != null;
			
			/* Read outgoing strand. */
			assert readWord(dis).equals("(RnaStrand");
			RnaStrand outgoing = new SimpleRnaStrand();
			outgoing.setName(readWord(dis));
			assert readWord(dis).equals("(v3");
			Vector3D outgoingPos = new Vector3D();
			outgoingPos.setX(Double.parseDouble(readWord(dis)));
			outgoingPos.setY(Double.parseDouble(readWord(dis)));
			outgoingPos.setZ(Double.parseDouble(readWord(dis)));
			outgoing.setPosition(outgoingPos);
			assert readWord(dis).equals(")"); // )Vector3D
			assert readWord(dis).equals("(children");
			int outgoingChildren = Integer.parseInt(readWord(dis));
			for (int i_B = 0; i_B < outgoingChildren; i_B++) {
			    assert readWord(dis).equals("(Nucleotide3D");
			    Nucleotide3D nucleotide = new Nucleotide3D();
			    nucleotide.setName(readWord(dis));
			    char symbol = readWord(dis).charAt(0);
			    String alphaString = readWord(dis);
			    int alphaType = SequenceTools.UNKNOWN_SEQUENCE;
			    if (alphaString.equals("DNA")) {
				alphaType = SequenceTools.DNA_SEQUENCE;
			    }
			    else if (alphaString.equals("DNA_A")) {
				alphaType = SequenceTools.AMBIGUOUS_DNA_SEQUENCE;
			    }
			    else if (alphaString.equals("RNA")) {
				alphaType = SequenceTools.RNA_SEQUENCE;
			    }
			    else if (alphaString.equals("RNA_A")) {
				alphaType = SequenceTools.AMBIGUOUS_RNA_SEQUENCE;
			    }
			    try {
				nucleotide.setSymbol(new SimpleLetterSymbol(symbol, new SimpleAlphabet(alphaType)));
			    }
			    catch (UnknownSymbolException e) {
				log.severe("Generated UnknownSymbolException while reading NanoTiler-generated file!");
			    }
			    assert readWord(dis).equals("(v3");
			    Vector3D nucleotidePos = new Vector3D();
			    nucleotidePos.setX(Double.parseDouble(readWord(dis)));
			    nucleotidePos.setY(Double.parseDouble(readWord(dis)));
			    nucleotidePos.setZ(Double.parseDouble(readWord(dis)));
			    nucleotide.setPosition(nucleotidePos);
			    assert readWord(dis).equals(")"); // )Vector3D
			    assert readWord(dis).equals("(children");
			    int nucleotideChildren = Integer.parseInt(readWord(dis));
			    for (int i_C = 0; i_C < nucleotideChildren; i_C++) {
				assert readWord(dis).equals("(Atom3D");
				Atom3D atom = new Atom3D();
				atom.setName(readWord(dis));
				assert readWord(dis).equals("(v3");
				Vector3D atomPos = new Vector3D();
				atomPos.setX(Double.parseDouble(readWord(dis)));
				atomPos.setY(Double.parseDouble(readWord(dis)));
				atomPos.setZ(Double.parseDouble(readWord(dis)));
				atom.setPosition(atomPos);
				assert readWord(dis).equals(")"); // )Vector3D
				assert readWord(dis).equals("(children");
				assert Integer.parseInt(readWord(dis)) == 0; // No children.
				assert readWord(dis).equals(")"); // )Children
				assert readWord(dis).equals(")"); // )Atom3D
				nucleotide.insertChild(atom);
			    }
			    assert readWord(dis).equals(")"); // )Nucleotide3D children
			    assert readWord(dis).equals(")"); // )Nucleotide3D
			    outgoing.insertChild(nucleotide);
			}
			assert readWord(dis).equals(")"); // )RnaStrand children
			assert readWord(dis).equals("(Sequence");
			String outSequenceName = readWord(dis);
			String outTypeString = readWord(dis);
			int outSequenceType = SequenceTools.UNKNOWN_SEQUENCE;
			if (outTypeString.equals("DNA")) {
			    outSequenceType = SequenceTools.DNA_SEQUENCE;
			}
			else if (outTypeString.equals("DNA_A")) {
			    outSequenceType = SequenceTools.AMBIGUOUS_DNA_SEQUENCE;
			}
			else if (outTypeString.equals("RNA")) {
			    outSequenceType = SequenceTools.RNA_SEQUENCE;
			}
			else if (outTypeString.equals("RNA_A")) {
			    outSequenceType = SequenceTools.AMBIGUOUS_RNA_SEQUENCE;
			}
			String outSequenceString = readWord(dis);
			Alphabet outSequenceAlphabet = new SimpleAlphabet(outSequenceType);
			Sequence outSequence;
			try {
			    outSequence = new SimpleSequence(outSequenceString, outSequenceName, outSequenceAlphabet);
			}
			catch (UnknownSymbolException e) {
			    log.severe("UnknownSymbolException generated when reading NanoTiler generated kissing loop.");
			}
			assert readWord(dis).equals(")"); // )Sequence
			assert readWord(dis).equals(")"); // )RnaStrand
			int outgoingIndex = Integer.parseInt(readWord(dis));
			log.fine("Finished outgoing strand.");
			
			Vector3D branchPosition = new Vector3D();
			assert readWord(dis).equals("(v3"); // Branch position.
			branchPosition.setX(Double.parseDouble(readWord(dis)));
			branchPosition.setY(Double.parseDouble(readWord(dis)));
			branchPosition.setZ(Double.parseDouble(readWord(dis)));
			assert readWord(dis).equals(")"); // )Vector3D
			
			assert readWord(dis).equals("(children"); // Branch children.
			int branchChildren = Integer.parseInt(readWord(dis));
			CoordinateSystem3D coordinateSystem = new CoordinateSystem3D(Vector3D.ZVEC);
			for (int i_D = 0; i_D < branchChildren; i_D++) {
			    String className = readWord(dis);
			    if (className.equals("(CoordinateSystem3D")) {
				coordinateSystem.setName(readWord(dis));
				Vector3D coordinatePos = new Vector3D();
				assert readWord(dis).equals("(v3");
				coordinatePos.setX(Double.parseDouble(readWord(dis)));
				coordinatePos.setY(Double.parseDouble(readWord(dis)));
				coordinatePos.setZ(Double.parseDouble(readWord(dis)));
				assert readWord(dis).equals(")"); // )Vector3D
				coordinateSystem.setPosition(coordinatePos);
				int coordinateChildren = Integer.parseInt(readWord(dis));
				for (int i_E = 0; i_E < coordinateChildren; i_E++) {
				    Vector3D childPos = new Vector3D();
				    assert readWord(dis).equals("(v3");
				    childPos.setX(Double.parseDouble(readWord(dis)));
				    childPos.setY(Double.parseDouble(readWord(dis)));
				    childPos.setZ(Double.parseDouble(readWord(dis)));
				    assert readWord(dis).equals(")"); // )Vector3D
				    childPos = childPos.plus(branchPosition); // When child is inserted, the parent position is subtracted.
				    Object3D child = new SimpleObject3D(childPos);
				    coordinateSystem.insertChild(child);
				}
				assert readWord(dis).equals(")"); // )CoordinateSystem3D
			    }
			    else {
				log.severe("NanoTiler does not know how to read: " + 
					   className + " as a child of a BranchDescriptor3D");
				assert false;
			    }
			}
			assert readWord(dis).equals(")"); // )children
			assert readWord(dis).equals(")"); // )BranchDescriptor3D
			BranchDescriptor3D branch = new SimpleBranchDescriptor3D(incoming, 
										 outgoing, 
										 incomingIndex, 
										 outgoingIndex, 
										 coordinateSystem);
			branch.setName(branchName);
			branches.add(branch);
		    }
		    else if (kissingLoopChild.equals("(RnaStrand")) {
			/* When the kissing loop is created, the strands 
			   of each branch are added as children. 
			   There is no need to add them again. 
			   Just read over the RnaStrand. */
			log.fine("Reading over RnaStrand child.");
			word = readWord(dis); // name or )RnaStrand
			if (word.equals(")")) { }
			else { // TODO: outdated, remove once old files are rewritten! Christine
			    readWord(dis); // (v3
			    readWord(dis); // x
			    readWord(dis); // y
			    readWord(dis); // z
			    readWord(dis); // )Vector3D
			    readWord(dis); // (children
			    int numChildren1 = Integer.parseInt(readWord(dis));
			    for (int i_1A = 0; i_1A < numChildren1; i_1A++) {
				readWord(dis); // (Nucleotide3D
				readWord(dis); // name
				readWord(dis); // symbol
				readWord(dis); // alphabet
				readWord(dis); // (v3
				readWord(dis); // x
				readWord(dis); // y
				readWord(dis); // z
				readWord(dis); // )Vector3D
				readWord(dis); // (children
				int numChildren2 = Integer.parseInt(readWord(dis));
				for (int i_1B = 0; i_1B < numChildren2; i_1B++) {
				    readWord(dis); // (Atom3D
				    readWord(dis); // name
				    readWord(dis); // (v3
				    readWord(dis); // x
				    readWord(dis); // y
				    readWord(dis); // z
				    readWord(dis); // )Vector3D
				    readWord(dis); // (children
				    readWord(dis); // numChildren
				    readWord(dis); // )children
				    readWord(dis); // )Atom3D
				}
				readWord(dis); // )Nucleotide3D children
				readWord(dis); // )Nucleotide3D
			    }
			    readWord(dis); // )RnaStrand children
			    readWord(dis); // (Sequence
			    readWord(dis); // name
			    readWord(dis); // type
			    readWord(dis); // sequence
			    readWord(dis); // )Sequence
			    readWord(dis); // )RnaStrand
			}
		    }
		    else if (kissingLoopChild.equals("(BranchDescriptor3D")) {
			log.severe("NanoTiler does not know how to read a BranchDescriptor child yet!");
		    }
		    else {
			log.severe("NanoTiler does not know how to read: " + kissingLoopChild);
			assert false;
		    }
		}
		
		StrandJunction3D kissingLoop = new KissingLoop3D(branches);
		kissingLoop.setName(kissingLoopName);
		kissingLoop.setPosition(kissingLoopPos);
		is.close();
		return kissingLoop;
	    }
	    else {
		log.severe("NanoTiler does not know how to read this: " + word);
		assert false;
		is.close();
	    }
	}
	catch (IOException e) {
	    log.severe("Could not read ntl file: " + fileName + " " + e.getMessage());
	}
	return new SimpleStrandJunction3D();
    }

    /** generates info text for junction database */
    public static String junctionDBInfo(StrandJunctionDB junctionDB) {
	StringBuffer buf = new StringBuffer();
	buf.append("Orders: " + junctionDB.size() + ENDL);
	for (int i = 2; i < junctionDB.size(); ++i) {
	    buf.append("order: " + i + " : " + junctionDB.size(i) + ENDL);
	}
	buf.append(junctionDBGeomInfo(junctionDB));
	return buf.toString();
    }

    /** generates info text for junction database */
    public static String junctionDBGeomInfo(StrandJunctionDB junctionDB) {
	StringBuffer buf = new StringBuffer();
	buf.append("Orders: " + junctionDB.size() + ENDL);
	for (int i = 2; i < junctionDB.size(); ++i) {
	    buf.append("order: " + i + " : " + junctionDB.size(i) + ENDL);
	    StrandJunction3D[] junctions = junctionDB.getJunctions(i);
	    if (junctions == null) {
		continue;
	    }
	    for (int j = 0; j < junctions.length; ++j) {
		log.fine("Junction: " + (i+1) + " " + (j+1));
		buf.append(StrandJunctionTools.writeJunctionGeomInfo(junctions[j]));
	    }

	}
	return buf.toString();
    }

    /** reads a single word from data stream */
    public static String readWord(DataInputStream dis) {
	String s = new String("");
	char c = ' ';
	
	// first skip white space
	do {
	    try {
		c = (char)dis.readByte();
	    }
	    catch (IOException e) {
		break; // end of file reached
	    }
	}
	while (Character.isWhitespace(c));
	
	if (!Character.isWhitespace(c)) {
	    s = s + c;
	}
	else {
	    log.fine("found word: " + s);
	    return s;
	}
	
	while (true) {
	    try {
		c = (char)dis.readByte();
		if (!Character.isWhitespace(c)) {
		    s = s + c;
		}
		else {
		    break;
		}
	    }
	    catch (IOException e) {
		break; // end of file reached
	    }
	}
	log.fine("found word: " + s);
	return s;
    }
    
}
