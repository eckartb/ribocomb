package rnadesign.rnamodel;

public interface StrandJunctionNomenclature {


    /** Generates a name, like for example "2HS4H" according to some nomenclature */
    String generateNomenclature(StrandJunction3D junction);

}
