package rnadesign.rnamodel;

import chemistrytools.ChemicalElement;

import java.util.*;
import java.util.logging.*;
import rnasecondary.Interaction;
import rnasecondary.InteractionType;
import rnasecondary.RnaInteractionType;
import rnasecondary.SimpleInteraction;
import tools3d.*;
import tools3d.numerics3d.*;
import tools3d.objects3d.*;

import static rnadesign.rnamodel.PackageConstants.NEWLINE;

public class StrandJunctionTools {
    
    /** extends strand by this many residues */
    public static final int PDB_STRAND_OFFSET = 2;
    public static final double RAD2DEG = 180.0 / Math.PI;

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    /** returns transformation defined by a two-way junction (junction can be of higher order though */
    public Matrix4D computeTransformation(StrandJunction3D junction, int branchId1, int branchId2) {
	assert false; // TODO to be implemented
	return null;
    }

    /** Finds and generates for an arbitray set of objects
     *  a set of NucleotideStrand junctions.
     * In current form it requires each branch to correspond to a stem.
     * TODO : not yet implemented!
     */
    public static Object3DSet generateStrandJunctions(Object3D root) {
	Object3DSet result = new SimpleObject3DSet();
	assert false; // should never be here!
	// TODO
	return result;
    }

    /** generates Watson-Crick interaction links between ends of connector helices */
    static LinkSet generateJunctionBranchLinks(StrandJunction3D junction, int branchId) {
	LinkSet result = new SimpleLinkSet();
	BranchDescriptor3D bd = junction.getBranch(branchId);
	NucleotideStrand inStrand = bd.getIncomingStrand();
	NucleotideStrand outStrand = bd.getOutgoingStrand();
	// TODO : non-standard base pairs are wrongly classified as Watson-Crick
	InteractionType interactionType = new RnaInteractionType(RnaInteractionType.WATSON_CRICK);	
	for (int i = 0; i <= bd.getOffset(); ++i) {
	    int inId = bd.getIncomingIndex() + i;
	    int outId = bd.getOutgoingIndex() -i;
	    assert inId < inStrand.getResidueCount();
	    Residue3D inRes = inStrand.getResidue3D(inId);
	    assert outId < outStrand.getResidueCount();
	    Residue3D outRes = outStrand.getResidue3D(outId);
	    Interaction interaction = new SimpleInteraction(inRes, outRes, interactionType);
	    Link link = new InteractionLinkImp(inRes, outRes, interaction);
	    result.add(link);
	}
	return result;
    }

    
    static LinkSet generateJunctionLinks(StrandJunction3D junction) {
	LinkSet result = new SimpleLinkSet();
	if (junction == null) {
	    return result;
	}
	for (int i = 0; i < junction.getBranchCount(); ++i) {
	    result.merge(StrandJunctionTools.generateJunctionBranchLinks(junction, i));
	}
	return result;
    }

    /** Sets a certain helix and to "used" status. */
    public static void setHelixEndUsed(Object3D object, String helixendName) {
	assert (isHelixEndFree(object, helixendName) );
	String usedHelixends = object.getProperty("used_helixends");
	if ((usedHelixends == null) || (usedHelixends.length() == 0)) {
	    object.setProperty("used_helixends", helixendName);
	}
	else {
	    object.setProperty("used_helixends", usedHelixends + ";" + helixendName);
	}
    }

    /** sets n'th helix end to occupied */
    public static void setHelixendUsed(StrandJunction3D junction, int id) {
	setHelixEndUsed(junction, "(hxend)"+(id+1));
    }

    /** returns true, if for a certain object (junction, kissing loop), a helix end is not been connected to yet.
     * Used so far only for growBuildingBlock methods */
    public static boolean isHelixEndFree(Object3D object, String helixendName) {
	assert helixendName != null;
	String usedHelixends = object.getProperty("used_helixends");
	if ((usedHelixends == null) || (usedHelixends.length() == 0)) {
	    return true;
	}
	String[] words = usedHelixends.split(";");
	for (int i = 0; i < words.length; ++i) {
	    if (helixendName.equals(words[i])) {
		return false; // helix end is already used!
	    }
	}
	return true;
    }

    /** returns true, if n'th helix end is free */
    public static boolean isHelixEndFree(StrandJunction3D junction, int id) {
	return (isHelixEndFree(junction, "(hxend)" + (id+1)));
    }

    /** returns true, if the junction has at least one helix end that is not occupied yet */
    public static boolean hasFreeHelixEnds(Object3D obj) {
	if (! (obj instanceof StrandJunction3D)) {
	    return false;
	}
	StrandJunction3D junction = (StrandJunction3D)obj;
	String usedHelixends = junction.getProperty("used_helixends");
	if ((usedHelixends == null) || (usedHelixends.length() == 0)) {
	    return true;
	}
	String[] words = usedHelixends.split(";");
	return (words.length < junction.getBranchCount());
    }

    /** generates a graph node for each unoccupied helix end */
    private static Object3DSet generateFreeHelixGraphEnds(StrandJunction3D junction, double distance, Object3D cloneObject) {
	Object3DSet resultSet = new SimpleObject3DSet();
	Vector3D origPos = junction.getPosition();
	for (int i = 0; i < junction.getBranchCount(); ++i) {
	    if (isHelixEndFree(junction, i)) { // test words like "(hxend)1", "(hxend)2" etc
		Vector3D direction = junction.getBranch(i).getDirection();
		Vector3D offset = direction.mul(distance);
		Object3D newObj = (Object3D)(cloneObject.cloneDeep());
		newObj.setName(junction.getName() + "_hxextension" + (i+1));
		newObj.setPosition(origPos.plus(offset));
		resultSet.add(newObj);
	    }
	}
	return resultSet;
    }

    /** Extracts graph from object3d structure : each node corresponds to one junction */
    public static Object3DLinkSetBundle extractJunctionGraph(Object3D root, LinkSet links, String nameBase,
							     Object3DSet tabooSet) {
	Object3DSet junctionSet = Object3DTools.collectByClassName(root, "StrandJunction3D");
	for (int i = junctionSet.size()-1; i >= 0; --i) {
	    assert junctionSet.get(i) != null;
	    if (tabooSet.contains(junctionSet.get(i))) {
		junctionSet.remove(junctionSet.get(i));
	    }
	}
	Object3DSet kissingSet = Object3DTools.collectByClassName(root, "KissingLoop3D");
	for (int i = kissingSet.size()-1; i >= 0; --i) {
	    if (tabooSet.contains(kissingSet.get(i))) {
		kissingSet.remove(kissingSet.get(i));
	    }
	}
	junctionSet.merge(kissingSet); // add kissing loops
	if (junctionSet.size() < 1) {
	    return new SimpleObject3DLinkSetBundle();
	}
	Object3DSet allFreeEnds = new SimpleObject3DSet();
	Object3D resultObj = new SimpleObject3D(Object3DSetTools.centerOfMass(junctionSet));	
	LinkSet resultLinks = new SimpleLinkSet();
	resultObj.setName(nameBase);
	String nameBase2 = "p";
	double freeEndDistance = 20.0; // default distance for free helix ends
	int pc = 0; // counts added points
	for (int i = 0; i < junctionSet.size(); ++i) {
	    StrandJunction3D junction = (StrandJunction3D)(junctionSet.get(i));
	    Object3D obj = new SimpleObject3D(junction.getPosition());
	    obj.setName(nameBase2 + (++pc));
	    if (junctionSet.get(i).getClassName().equals("StrandJunction3D")) {
		obj.setProperty("graph_color", "j");
	    }
	    else if (junctionSet.get(i).getClassName().equals("KissingLoop3D")) {
		obj.setProperty("graph_color", "k");
		obj.setProperty("atom_element", "S"); // workaround: normal two-way junctions are represented by oxygen "O" in canonizer,
		// use sulphur "S" as alternative to indicate kissing loop
	    }
	    resultObj.insertChild(obj);
	    // generates representation of free helix ends:
	    Object3DSet freeHelixGraphEnds = generateFreeHelixGraphEnds(junction, freeEndDistance, obj);
	    for (int j = 0; j < freeHelixGraphEnds.size(); ++j) { // add links to parent junction
		resultLinks.add(new SimpleLink(obj, freeHelixGraphEnds.get(j)));
	    }
	    allFreeEnds.merge(freeHelixGraphEnds);
	}
	for (int i = 0; i < allFreeEnds.size(); ++i) {
	    Object3D newObj = allFreeEnds.get(i);
	    newObj.setName(nameBase2 + (++pc));
	    resultObj.insertChild(newObj);
	}
	for (int i = 0; i < junctionSet.size(); ++i) {
	    for (int j = i+1; j < junctionSet.size(); ++j) {
		int linkNumber = links.getLinkNumber(junctionSet.get(i), junctionSet.get(j));
		if (linkNumber > 0) {
		    resultLinks.add(new SimpleLink(resultObj.getChild(i), resultObj.getChild(j)));
		}
	    }
	}
	return new SimpleObject3DLinkSetBundle(resultObj, resultLinks);
    }


    /** Extracts graph from object3d structure : each node corresponds to one junction */
    public static Object3DLinkSetBundle extractJunctionGraph(Object3D root, LinkSet links, String nameBase) {
	Object3DSet tabooSet = new SimpleObject3DSet();
	return extractJunctionGraph(root, links, nameBase, tabooSet); 
    }

    /** fuses strands of junctions if possible. Careful: use must determine if that is reasonable,
     * usually only if the strands were extended by kissing loops. 
     */
    public static void fuseStrands(StrandJunction3D junction, double residueCutoffDist) {
	boolean isOk = false;
	while (!isOk) {
	    isOk = true;
	    NucleotideStrand[] strands = junction.getStrands();
	    assert strands.length == junction.getStrandCount();
	    log.fine("Strand count: " + strands.length + " " + junction.getStrandCount());
	    for (int i = 0; i < strands.length; ++i) {
		for (int j = i+1; j < strands.length; ++j) {
		    // check if strands i and j can be fused:
		    double dist1 = strands[i].getResidue3D(strands[i].getResidueCount()-1).distance(strands[j].getResidue3D(0));
		    if (dist1 <= residueCutoffDist) {
			log.fine("Trying to fuse strands: " + (i+1) + " " + (j+1));
			junction.fuseStrands(i,j);
			isOk = false;
			break;
		    }
		    else {
			double dist2 = strands[j].getResidue3D(strands[j].getResidueCount()-1).distance(strands[i].getResidue3D(0));
			if (dist2 <= residueCutoffDist) {
			    log.fine("Trying to fuse strands: " + (j+1) + " " + (i+1));
			    junction.fuseStrands(j,i);
			    isOk = false;
			    break;
			}
		    }
		}
		if (!isOk) {
		    break;
		}
	    }
	}
    }

    /** places strands as children. TODO : test / verify! maybe only subset of strand should be used!? */
    public static void placeStrands(StrandJunction3D junction, Object3D obj) {
// 	for (int i = 0; i < junction.getStrandCount(); ++i) {
// 	    NucleotideStrand strand = junction.getStrand(i);
// 	    obj.insertChild(strand);
// 	}

	Set<NucleotideStrand> strandSet = new HashSet<NucleotideStrand>();
	for (int i = 0; i < junction.getBranchCount(); ++i) {
	    BranchDescriptor3D branch = junction.getBranch(i);
	    strandSet.add(branch.getIncomingStrand());
	    strandSet.add(branch.getOutgoingStrand());
	}
	Iterator it = strandSet.iterator();
	while (it.hasNext()) {
	    NucleotideStrand strand = (NucleotideStrand)(it.next());
	    obj.insertChild(strand);
	}

	/*
	char strandChar = 'A';
	log.fine("starting StandJunctionTools.placeStrands with size junction: " + junction.getBranchCount());

	for (int i = 0; i < junction.getBranchCount(); ++i) {
	    BranchDescriptor3D branch = junction.getBranch(i);
	    // write branch descriptor
// 	    String branchPdb = toPdb(branch, i, writer);
// 	    result = result + branchPdb;
	    NucleotideStrand strand = branch.getIncomingStrand();
	    int startId = branch.getIncomingIndex();
	    // seach for closest stop id:
	    int bestId = 0;
	    int bestDiff = 10000;
	    boolean found = false;
	    // TODO : the following code should be done by BranchDescriptor
	    for (int j = 0; j < junction.getBranchCount(); ++j) {
		if (i == j) {
		    continue;
		}
		BranchDescriptor3D branch2 = junction.getBranch(j);
		if (branch2.getOutgoingStrand() != strand) {
		    continue;
		}
		int stopId = branch2.getOutgoingIndex();
		if (stopId <= startId) {
		    continue;
		}
		int diff = stopId - startId;
		if (diff < bestDiff) {
		    bestDiff = diff;
		    bestId = j;
		    found = true;
		}
	    }
	    if (!found) {
		log.fine("Warning: bad strand " + i);
		continue; // bad strand
	    }
	    BranchDescriptor3D branch3 = junction.getBranch(bestId);
	    int stopId = branch3.getOutgoingIndex();
	    NucleotideStrand strandClone = (NucleotideStrand)(strand.cloneDeep());
	    strandClone.setName(""+strandChar);
	    for (int ii = strandClone.size()-1; ii > stopId + 1; --ii) {
		strandClone.removeChild(ii); // TODO : check if sequence gets adjusted properly!
	    } 
	    for (int ii = startId-1; ii >= 0; --ii) {
		strandClone.removeChild(ii);
	    }
	    log.fine("Placing truncated strand clone: " + strandClone.size());
	    obj.insertChild(strandClone);
// 	    String tmpString = writer.writeStrand(strand, strandChar, startId, stopId + 1);
// 	    log.fine("Writing strand: " + strandChar + " " + (startId+1) + " " 
// 			       + stopId);

// 	    result = result + tmpString;
	    int strandCharId = (int)(strandChar);
	    ++strandCharId;
	    strandChar = (char)strandCharId;
	}
	*/

    }

    /** writes branch descriptor to PDB file */
    private static String toPdb(BranchDescriptor3D branchDescriptor, int n, 
				GeneralPdbWriter writer) {
	StringBuffer result = new StringBuffer();
	writer.incStrandChar();
	Atom3D dummyAtom = new Atom3D();
	String atomName = "p" + (n+1); // b1, b2 etc ...
	String residueName = "b" + (n+1); // b1, b2 etc ...
	dummyAtom.setName(atomName);
	dummyAtom.setPosition(branchDescriptor.getPosition());
	result.append(writer.writeHeteroAtom(dummyAtom, residueName, writer.getStrandChar(), 1) + NEWLINE); // TODO not clean
	dummyAtom.setPosition(branchDescriptor.getPosition().plus(branchDescriptor.getDirection().mul(3.0)));
	dummyAtom.setName("dir");
	result.append(writer.writeHeteroAtom(dummyAtom, residueName, writer.getStrandChar(), 1) + NEWLINE); // TODO not clean
	for (int i = 0; i < 4; ++i) {
	    // write idealized helix position of outgoing strand
	    dummyAtom = new Atom3D();
	    atomName = "o" + (i+1); // b1, b2 etc ...
	    dummyAtom.setName(atomName);
	    dummyAtom.setPosition(branchDescriptor.computeHelixPosition(i, BranchDescriptor3D.OUTGOING_STRAND));
	    result.append(writer.writeHeteroAtom(dummyAtom, residueName, writer.getStrandChar(), 1) + NEWLINE); // TODO not clean	    

	    // write idealized helix position of incoming strand	    
	    dummyAtom = new Atom3D();
	    atomName = "i" + (i+1); // b1, b2 etc ...
	    dummyAtom.setName(atomName);
	    dummyAtom.setPosition(branchDescriptor.computeHelixPosition(i, BranchDescriptor3D.INCOMING_STRAND));
	    result.append(writer.writeHeteroAtom(dummyAtom, residueName, writer.getStrandChar(), 1) + NEWLINE); // TODO not clean	    
	}
	result.append("TER" + NEWLINE);
	return result.toString();
    }

    /** returns chain id that was assigned by PDB file or default character */
    public static char getOriginalStrandChar(NucleotideStrand strand, char defaultChar) {
	Properties properties = strand.getProperties();
	if (properties != null) {
	    String chainString = properties.getProperty("pdb_chain_char");
	    if (chainString == null) {
		log.warning("Strand with name " + strand.getName() + " has no property pdb_chain_char");
	    }
	    if ((chainString != null) && (chainString.length() ==1)) {
		return chainString.charAt(0);
	    }
	}
	return defaultChar;
    }

    /* 
    public static String toPdb(StrandJunction3D junction) {
    char strandChar = 'A';
    String result = "";
    GeneralPdbWriter writer = new GeneralPdbWriter();
    writer.setOriginalMode(true);
    log.info("starting StandJunctionTools.toPdb with size junction: " + junction.getBranchCount());
    for (int i = 0; i < junction.getBranchCount(); ++i) {
    BranchDescriptor3D branch = junction.getBranch(i);
	    // write branch descriptor
	    String branchPdb = toPdb(branch, i, writer);
	    result = result + branchPdb;
	    NucleotideStrand strand = branch.getIncomingStrand();
	    int startId = branch.getIncomingIndex();
	    // seach for closest stop id:
	    int bestId = 0;
	    int bestDiff = 10000;
	    boolean found = false;
	    // TODO : the following code should be done by BranchDescriptor
	    for (int j = 0; j < junction.getBranchCount(); ++j) {
		if (i == j) {
		    continue;
		}
		BranchDescriptor3D branch2 = junction.getBranch(j);
		if (branch2.getOutgoingStrand() != strand) {
		    continue;
		}
		int stopId = branch2.getOutgoingIndex();
		if (stopId <= startId) {
		    continue;
		}
		int diff = stopId - startId;
		if (diff < bestDiff) {
		    bestDiff = diff;
		    bestId = j;
		    found = true;
		}
	    }
	    if (!found) {
		log.fine("Warning: bad strand " + i);
		continue; // bad strand
	    }
	    BranchDescriptor3D branch3 = junction.getBranch(bestId);
	    int stopId = branch3.getOutgoingIndex();
	    int pdbStartId = startId - PDB_STRAND_OFFSET;
	    int pdbStopId = stopId + PDB_STRAND_OFFSET;
	    if (pdbStartId < 0) {
		pdbStartId = 0;
	    }
	    if (pdbStopId >= strand.getResidueCount()) {
		pdbStopId = strand.getResidueCount() - 1; // needs one index larger than last residue
	    }
	    String tmpString = writer.writeStrand(strand, getOriginalStrandChar(strand, strandChar),
						  pdbStartId, pdbStopId);
	    log.info("Writing strand: " + strandChar + " " + (pdbStartId+1) + " " 
		     + pdbStopId);
	    
	    result = result + tmpString;
	    int strandCharId = (int)(strandChar);
	    ++strandCharId;
	    strandChar = (char)strandCharId;
	}
	log.info("finishing StandJunctionTools.toPdb with size junction: " + junction.getBranchCount());
	return result;
    }
    */

    /** writes junction to pdb file, including information about fitted helices */
    public static String toPdb(StrandJunction3D junction, boolean originalMode) {
	char strandChar = 'A';
	GeneralPdbWriter writer = new GeneralPdbWriter();
	writer.setOriginalMode(GeneralPdbWriter.RESIDUE_PDB_NUMBER);
	log.info("starting StandJunctionTools.toPdb with size junction: " + junction.getBranchCount());
	StringBuffer buf = new StringBuffer();
	for (int i = 0; i < junction.getBranchCount(); ++i) {
	    buf.append(toPdb(junction.getBranch(i), i, writer));
	}
	for (int i = 0; i < junction.getStrandCount(); ++i) {
	    NucleotideStrand strand = junction.getStrand(i);
	    if (originalMode) {
		strandChar = strand.getName().charAt(0);
	    }
	    buf.append(writer.writeStrand(strand, getOriginalStrandChar(strand, strandChar)));
	    log.info("Writing strand: " + strandChar);
	    int strandCharId = (int)(strandChar);
	    ++strandCharId;
	    strandChar = (char)strandCharId;
	}
	log.info("finishing StandJunctionTools.toPdb with size junction: " + junction.getBranchCount());
	return buf.toString();
    }


    /** defines a line with position and direction of BranchDescriptor */
    private static Line computeLine(BranchDescriptor3D branch) {
// 	int idx = branch.getIndexOfChild("dir");
// 	assert idx >= 0 && idx < branch.size(); // must exist!
	Vector3D pos = branch.getPosition();
	Vector3D dir = branch.getDirection();
	return new Line(pos, dir);
    }

    /** defines a line with position and direction of BranchDescriptor. Branch descriptor is moved first by active transformation. */
    private static Line computeLine(BranchDescriptor3D branch, CoordinateSystem activeTransformation) {
// 	int idx = branch.getIndexOfChild("dir");
// 	assert idx >= 0 && idx < branch.size(); // must exist!
	Vector3D pos = activeTransformation.activeTransform3(branch.getPosition());
	Vector3D dir = activeTransformation.activeTransform3(branch.getDirection());
	return new Line(pos, dir);
    }

    /** defines a line with position and direction of a */
    private static Line computeLine(CoordinateSystem branch) {
// 	int idx = branch.getIndexOfChild("dir");
// 	assert idx >= 0 && idx < branch.size(); // must exist!
	Vector3D pos = branch.getPosition();
	Vector3D dir = branch.getZ();
	return new Line(pos, dir);
    }

    /** asssume center where all branch descriptor directions meet */
    public static Vector3D computeJunctionCenter(StrandJunction3D junction) throws java.awt.geom.NoninvertibleTransformException {
	Line[] lines = new Line[junction.getBranchCount()];
	for (int i = 0; i < junction.getBranchCount(); ++i) {
	    lines[i] = computeLine(junction.getBranch(i));
	}
	return Geom3DTools.computeLinesMedium(lines); // computes best point where lines meet
    }

    /** asssume center where all branch descriptor directions meet */
    public static Vector3D computeJunctionCenter(List<BranchDescriptor3D> helixDescriptors) throws java.awt.geom.NoninvertibleTransformException {
	Line[] lines = new Line[helixDescriptors.size()];
	for (int i = 0; i < helixDescriptors.size(); ++i) {
	    lines[i] = computeLine(helixDescriptors.get(i));
	}
	return Geom3DTools.computeLinesMedium(lines); // computes best point where lines meet
    }

    /** asssume center where all branch descriptor directions meet */
    public static Vector3D computeJunctionCenter(List<BranchDescriptor3D> helixDescriptors, List<CoordinateSystem> activeTransformations) throws java.awt.geom.NoninvertibleTransformException {
	Line[] lines = new Line[helixDescriptors.size()];
	for (int i = 0; i < helixDescriptors.size(); ++i) {
	    lines[i] = computeLine(helixDescriptors.get(i), activeTransformations.get(i));
	}
	return Geom3DTools.computeLinesMedium(lines); // computes best point where lines meet
    }

    /** Asssume center where z-axis of all coordinat systems meet */
    public static Vector3D computeCSJunctionCenter(List<CoordinateSystem> helixDescriptors) throws java.awt.geom.NoninvertibleTransformException {
	Line[] lines = new Line[helixDescriptors.size()];
	for (int i = 0; i < helixDescriptors.size(); ++i) {
	    lines[i] = computeLine(helixDescriptors.get(i));
	}
	return Geom3DTools.computeLinesMedium(lines); // computes best point where lines meet
    }

    /** Asssume center where z-axis of all coordinat systems meet */
    public static Vector3D computeCS3DJunctionCenter(List<CoordinateSystem3D> helixDescriptors) throws java.awt.geom.NoninvertibleTransformException {
	Line[] lines = new Line[helixDescriptors.size()];
	for (int i = 0; i < helixDescriptors.size(); ++i) {
	    lines[i] = computeLine(helixDescriptors.get(i));
	}
	return Geom3DTools.computeLinesMedium(lines); // computes best point where lines meet
    }

    /** returns true if all branch descriptors can be extended without clashing with junction atoms */
    private static boolean stericalCheck(StrandJunction3D junction, int n) {
	BranchDescriptor3D branch = junction.getBranch(n); // obtain n'th branch descriptor
	assert branch.isValid();
	Vector3D base = branch.getPosition();
	Vector3D dir = branch.getDirection();
	dir.normalize();
	for (int i = 0; i < junction.getStrandCount(); ++i) {
	    NucleotideStrand strand = junction.getStrand(i);
	    for (int j = 0; j < strand.getResidueCount(); ++j) {
		Residue3D residue = strand.getResidue3D(j);
		for (int k = 0; k < residue.size(); ++k) {
		    Atom3D atom = residue.getAtom(k);
		    Vector3D v = atom.getPosition();
		    Vector3D vRel = v.minus(base);
		    double projection = dir.dot(vRel);
		    if (projection > 15.0) {
			double dist = Math.abs(Geom3DTools.computeDistanceToLine(v, base, dir));
			if (dist < 10.0) {
			    log.fine("Problem in junction detected! "
				     + projection + " " + dist + " " 
				     + junction.getName() + " branch: " + (n+1) + " " 
				     + branch.getName() + " " + branch.getPosition() + " " + branch.getDirection() + " strand: " 
				     + (i+1) + " " + strand.getName() + " " + strand.getPosition() + " residue: "  
				     + (j+1) + " " + residue.getName() + " " + residue.getPosition() + " atom: " 
				     + (k+1) + " " + atom.getName() + " " + atom.getPosition());
			    return false; // atom in forbidden corridor!
			}
		    }
		}
	    }
	}
	return true; // everything ok
    }


    /** returns true if all branch descriptors can be extended without clashing with junction atoms */
    public static boolean stericalCheck(StrandJunction3D junction) {
 	for (int i = 0; i < junction.getBranchCount(); ++i) {
 	    if (!stericalCheck(junction, i)) { // i-th branch is bad
 		return false;
 	    }
 	}
 	return true;
    }

    /** writes relative positions of branches */
    public static String writeJunctionGeomInfo(StrandJunction3D junction) {
	StringBuffer buf = new StringBuffer();
	buf.append(junction.getName() + " " + junction.getBranchCount());
	for (int i = 0; i < junction.getBranchCount(); ++i) {
	    buf.append(junction.getBranch(i).getRelativePosition());
	}
	return buf.toString();
    }

    public static String generateJunctionAngleInfo(StrandJunction3D junction) {
	StringBuffer buf = new StringBuffer();

	buf.append("#############  Start of Description of junction: " + junction.getName() + NEWLINE);

	int numBranches = junction.getBranchCount();
	int numStrands =  junction.getStrandCount();
	buf.append("Number of strands: " + numStrands + NEWLINE);
	for (int i = 0; i < numStrands; ++i) {
	    buf.append("Strand " + (i+1) + " : ");
	    NucleotideStrand strand = junction.getStrand(i);
	    int b1Id = junction.getIncomingBranchId(i);
	    int b2Id = junction.getOutgoingBranchId(i);
	    int fiveId = junction.getBranch(b1Id).getIncomingIndex();
	    int threeId = junction.getBranch(b2Id).getOutgoingIndex();
	    String seqString = junction.getStrand(i).sequenceString();
	    // original names:
	    String nameIn = strand.getResidue3D(fiveId).getName();
	    String nameOut = strand.getResidue3D(threeId).getName();
	    log.fine("namein: " + nameIn + " " + fiveId + " " 
		     + "nameout: " + nameOut + " " + threeId + " "
		     + "complete sequence: " + seqString);
	    seqString = seqString.substring(fiveId, threeId+1); // use one used substring
	    buf.append(nameIn + "," + nameOut + " " + seqString + NEWLINE);
	}
	buf.append("Number of stems: " + numBranches + NEWLINE);
	for (int i = 0; i < numBranches; ++i) {
	    buf.append("BranchDescriptor for Stem " + (i+1) + " : " 
		       + junction.getBranch(i).infoString() + NEWLINE);
	}
	for (int i = 0; i < numBranches; ++i) {
	    BranchDescriptor3D branch1 = junction.getBranch(i);
	    for (int j = i+1; j < numBranches; ++j) {
		BranchDescriptor3D branch2 = junction.getBranch(j);
		buf.append("Angle between stems " + (i+1) + " and "
			   + (j+1) + " : " + RAD2DEG * BranchDescriptorTools.computeBranchDescriptorAngle(branch1, branch2) + NEWLINE); 
	    }
	    
	}

	buf.append("#############  END of Description of junction: " + junction.getName() + NEWLINE);

	return buf.toString();
    }


}
