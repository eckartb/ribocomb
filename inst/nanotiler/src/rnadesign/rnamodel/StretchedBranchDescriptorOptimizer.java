package rnadesign.rnamodel;

import java.util.Properties;
import java.util.logging.Logger;

/** Class that computes for a given pair of branch descriptors an optimized interpolating branch descritpor */
public class StretchedBranchDescriptorOptimizer extends AbstractBranchDescriptorOptimizer {

    public static final String CLASS_NAME = "StretchedBranchDescriptorOptimizer";

    public static final double RMS_LIMIT_DEFAULT = 100.0; // allow initial very high solutino

    private double rmsLimit = 5.0;

    private double stretchMaxFactor = 1.0; // no stretch! 1.2; // 1.05; // this value determines how far to deviate from ideal helix parameters. initially: 1.5  

    private double stretchStep = 0.02;

    public StretchedBranchDescriptorOptimizer() { this.rmsLimit = RMS_LIMIT_DEFAULT; }

    public StretchedBranchDescriptorOptimizer(FitParameters limits) { this.rmsLimit = limits.getRmsLimit(); }

    public String getClassName() { return CLASS_NAME; }

    /** optimize stem so that it fits as good as possible with junction branch descriptors 1 and 2.
     *  Careful: junction branch descriptors are assumed to have the offset 1 and -1,
     *  while the result branch descriptor has the residue offset zero.
     *  Using equations from Aalberts 2005, compare also MathWorld page on helices.
     */
    private BranchDescriptor3D optimize(RnaStem3D stem,
					BranchDescriptor3D branch1,
					BranchDescriptor3D branch2,
					double rmsLimit) throws FittingException {
	log.info("Starting StretchedBranchDescriptorOptimizer.optimize with rmsLimit: " + rmsLimit + " !");
	assert branch1.isValid();
	assert branch2.isValid();
	int bestAngle = 0;
	double bestError = rmsLimit + 0.0001;
	// check in one degree steps:
	BranchDescriptor3D b = null;
	try {
	    b = interpolateBranchDescriptor(stem, branch1, branch2, 0.0);
	}
	catch (FittingException fe) {
	    log.info("Could not generate result from interpolateBranchDescriptor: " + fe.getMessage());
	    throw fe;
	}
	assert b != null;
	HelixParameters prmOrig = branch1.getHelixParameters();
	HelixParameters prm = new HelixParameters(prmOrig);
	double hOrig = prmOrig.rise;
	double bestH = hOrig;
	double bestNt = prm.basePairsPerTurn;
	double bestStretch = 1.0;
	double A = 38.1356601; // unit: Angstroem squared; formula derived from Aalberts 2005
	for (double stretch = 1.0; stretch <= stretchMaxFactor; stretch += stretchStep) {
	    for (int flag = -1; flag <= 1; flag += 2) {
		assert flag != 0;
		double stretchUse = stretch;
		if (flag > 0) {
		    stretchUse = 1.0 / stretch; // try stretching as well as compressing
		}
		double hNew = hOrig * stretchUse;
		// double newR = 1.782535 * Math.sqrt(A- (hNew * hNew));
		double newNt = 2.0 * Math.PI * prm.radius / Math.sqrt(A - (hNew * hNew));
		prm.rise = hNew;
		prm.basePairsPerTurn = newNt;
		b.setHelixParameters(prm);
		double errorVal = computeBranchDescriptorError(stem.getLength(), branch1, branch2, b); // 
		if (errorVal < bestError) {
		    log.info("New better solution for stem helix rise, bp per turn: " + hNew + " " + newNt + " error: " +  errorVal + " stretch: " + stretchUse);
		    bestError = errorVal;
		    bestH = hNew;
		    bestNt = newNt;
		    bestStretch = stretchUse;
		}
		else {
		    log.info("Error value too large (stretch)" + errorVal);
		}
	    }
	}
	if (bestError > rmsLimit) {
	    log.info("Could not find stem solution: " + bestError + " " + rmsLimit);
	    return null; // NO SUITABLE solution found
	}
	else {
	    log.info("Could find stem solution: " + bestError + " with stretch: " + bestStretch);
	}
	prm.rise = bestH;
	prm.basePairsPerTurn = bestNt;
	b.setHelixParameters(prm);
	Properties prop = b.getProperties();
	if (prop == null) {
	    prop = new Properties();
	    b.setProperties(prop);
	}
	log.info("Optimized stem helix rise: " + bestH + " " + bestError + " stretch: " + bestStretch);
	prop.setProperty("fit_score", "" + bestError); // used later for evaluating fit of axial kissing loop!	
	prop.setProperty("stretch_factor", ""+ bestStretch); // used later for evaluating fit of axial kissing loop!
	assert(b.getProperties() != null);
	return b;
    }

    public double getStretchMaxFactor() { return stretchMaxFactor; }

    public void setStretchMaxFactor(double x) { this.stretchMaxFactor = x; }

    public BranchDescriptor3D optimize(RnaStem3D stem,
				       BranchDescriptor3D branch1,
				       BranchDescriptor3D branch2) throws FittingException {
	log.fine("starting StretchBranchDescriptorOptimizer.optimize(stem, branch1, branch2) with rms limit: " + this.rmsLimit);
	return optimize(stem, branch1, branch2, this.rmsLimit);
    }
    
    public void setRmsLimit(double rmsLimit) { this.rmsLimit = rmsLimit; }

}
