package rnadesign.rnamodel;

import tools3d.objects3d.*;

public class StructureQualityTools {

    /** regular names of backbone atoms of one nucleotide */
    public static final String[] backboneNames = {"P", "O5*", "C5*", "C4*", "C3*", "O3*"};
    public static final String[] backboneNames5 = {"O5*", "C5*", "C4*", "C3*", "O3*"}; // special case of nucleotides at 5' end
    public static final String[] backboneNames3 = backboneNames;

    /** returns true if all child nodes exist */
    public static boolean containsChildren(Object3D obj, String[] names) {
	for (int i = 0; i < names.length; ++i) {
	    if (obj.getIndexOfChild(names[i]) < 0) {
		return false;
	    }
	}
	return true;
    }

    /** returns maximum gap distance within one nucleotide. 
     * Not checking bond distances to neighboring nucleotides.
     */
    private static double findMaximumBackboneGap(Nucleotide3D nuc) throws RnaModelException {
	String[] usedBackboneNames = backboneNames;
	if (NucleotideTools.is5PrimeNucleotide(nuc)) {
	    usedBackboneNames = backboneNames5;
	}
	if (!containsChildren(nuc, usedBackboneNames)) {
	    throw new RnaModelException("Missing backbone atoms in nucleotide " + nuc.getName());
	}
	double maxDist = 0.0;
	for (int i = 1; i < usedBackboneNames.length; ++i) {
	    double dist = nuc.getChild(usedBackboneNames[i-1]).distance(nuc.getChild(usedBackboneNames[i]));
	    if (dist > maxDist) {
		maxDist = dist;
	    }
	}
	return maxDist;
    }

    /** throws exception if child not does not exist */
    public static void assertChild(Object3D obj, String name) throws RnaModelException {
	if (obj.getIndexOfChild(name) < 0) {
	    throw new RnaModelException("Could not find child " + name + " in object " + obj.getClassName() + " " + obj.getName());
	}
    }

    /** returns gap distance between O3* - P distance of two nucleotides.
     */
    private static double getInterNucleotideBackboneGap(Nucleotide3D nuc1, Nucleotide3D nuc2) throws RnaModelException {
	assertChild(nuc1, backboneNames[backboneNames.length-1]);
	assertChild(nuc2, backboneNames[0]);
	double dist = nuc1.getChild(backboneNames[backboneNames.length-1]).distance(nuc2.getChild(backboneNames[0]));
	return dist;
    }

    /** counts the number of gaps in backbone of Rna strand */
    public static int countBackboneGaps(RnaStrand strand, double cutoff) {
	int count = 0;
	double gapMax;
	for (int i = 0; i < strand.getResidueCount(); ++i) {
	    try {
		gapMax = findMaximumBackboneGap((Nucleotide3D)(strand.getResidue3D(i)));
		if (gapMax > cutoff) {
		    ++count;
		}
	    }
	    catch (RnaModelException rme) {
		System.out.println("Count as gap! Rna model exception in nucleotide " + strand.getResidue3D(i).getName() + " : " 
				   + rme.getMessage());
		++count; // count as gap
	    }
	    if (i > 0) {
		try {
		    gapMax = getInterNucleotideBackboneGap((Nucleotide3D)(strand.getResidue(i-1)),
							   (Nucleotide3D)(strand.getResidue(i)));
		    if (gapMax > cutoff) {
			++count;
		    }
		}
		catch (RnaModelException rme) {
		    System.out.println("Count as gap! Rna model exception in nucleotides " 
				       + strand.getResidue3D(i-1).getName() + " and " + strand.getResidue3D(i).getName() + " : " 
				       + rme.getMessage());
		    ++count; // count as gap
		}
	    }
	}
	return count;
    }

    /** counts the number of gaps in backbone of all Rna strand */
    public static int countAllBackboneGaps(Object3D root, double cutoff) {
	int count = 0;
	Object3DSet strandSet = Object3DTools.collectByClassName(root, "RnaStrand");
	for (int i = 0; i < strandSet.size(); ++i) {
	    RnaStrand strand = (RnaStrand)(strandSet.get(i));
	    int numGaps = StructureQualityTools.countBackboneGaps(strand, cutoff);
	    count += numGaps;
	}
	return count;
    }

}
