/** implements concept of two linked objects */
package rnadesign.rnamodel;

import java.util.List;

import tools3d.objects3d.SimpleLink;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DSet;
import tools3d.objects3d.Object3DSetTools;
import tools3d.Vector3D;
import tools3d.Vector3DTools;
import tools3d.CoordinateSystem;

public class SuperimposeConstraintLink extends SimpleLink {
  
    private int symId1 = 0; // symmetry copy id of object1

    private int symId2 = 0; // symmetry copy id of object2;

    public SuperimposeConstraintLink(Object3D o1, Object3D o2) {
	super(o1, o2);
	
    }
    
    public double computeScore( CoordinateSystem cs1, CoordinateSystem cs2 ){
      
      Object3DSet tripod1 = null;
      Object3DSet tripod2 = null;
      
      try{
        tripod1 = NucleotideDBTools.extractAtomTripodBp( (Nucleotide3D) getObj1() );
        tripod2 = NucleotideDBTools.extractAtomTripodBp( (Nucleotide3D) getObj2() );
      } catch (RnaModelException rme){
        System.out.println( rme + " Objects " + getObj1() + " and " + getObj2() + "do not have atom tripods.");
      }

      Vector3D[] coord1 = Object3DSetTools.getCoordinates(tripod1);
    	Vector3D[] coord2 = Object3DSetTools.getCoordinates(tripod2);
      Vector3D[] newCoord1 = new Vector3D[ coord1.length ];
      Vector3D[] newCoord2 = new Vector3D[ coord2.length ];

      //shift coordinates of atoms in tripod
      for(int i=0; i<coord1.length; i++){
    	   newCoord1[i] = cs1.activeTransform( coord1[i] );
       }
      for(int i=0; i<coord2.length; i++){
     	   newCoord2[i] = cs2.activeTransform( coord2[i] );
      }
      double score = Vector3DTools.computeRms(newCoord1, newCoord2);
      // double sqrScore = Math.pow(score,2);
      return score;
    }

}
	
