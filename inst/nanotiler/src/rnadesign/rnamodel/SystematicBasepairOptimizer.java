package rnadesign.rnamodel;

import java.util.List;
import java.util.Properties;
import tools3d.*;
import tools3d.objects3d.*;
import graphtools.*;

public class SystematicBasepairOptimizer extends BasepairOptimizer {

    private IntegerPermutatorList permutators;

    public SystematicBasepairOptimizer(LinkSet links, List<Object3DSet> objectBlocks, 
				       LinkSet basePairDB,
				       IntegerPermutatorList permutators) {
	super(links, objectBlocks, 0, 0.0, basePairDB, null, null); // FIXIT: replace null with symVdwActive
	assert objectBlocks.size() == permutators.size();
	this.permutators = permutators;
    }

    private void mutateCoordinateSystems(List<CoordinateSystem> optCoordinateSystems) {
	for (int i = 0; i < optCoordinateSystems.size(); ++i) {
	    OrientableModifier mutator = (OrientableModifier)(permutators.get(i)); // example: SystematicMutator
	    String ts = optCoordinateSystems.get(i).toString();
	    mutator.mutate(optCoordinateSystems.get(i));
	    // System.out.println("cs before: " + ts + " after mutation: " + optCoordinateSystems.get(i).toString());
	    // System.out.println("cs " + (i+1) + " : " + optCoordinateSystems.get(i));
	}
    }

    public Properties optimize() {
	Properties properties = new Properties();
	if ((optCoordinateSystems == null) || (optCoordinateSystems.size() == 0)) {
	    String errMsg = "No coordinate systems defined for optimization. Maybe add helix constraints?";
	    log.info(errMsg);
	    properties.setProperty(MESSAGE, errMsg);
	    return properties;
	}
	log.info("Number of relevant coordinate systems: " + origCoordinateSystems.size());
	String s2 = "";
	// mutator.copy(mutatorOrig); // reset mutator in case several optimizations are run
	// 	printCoordinateSystems(origCoordinateSystems);
	// 	printCoordinateSystems(optCoordinateSystems);
	copyCoordinateSystems(optCoordinateSystems, origCoordinateSystems); // copies original to opt coordinate systems
	List<CoordinateSystem> saveCoords = cloneCoordinateSystems(origCoordinateSystems); // original coordinate system
	List<CoordinateSystem> bestCoords = cloneCoordinateSystems(origCoordinateSystems);
	IntegerPermutatorList bestPermutators = null;
	try {
	    double newScore = scoreCoordinateSystems(optCoordinateSystems, properties);
	    double bestScore = newScore;
	    properties.setProperty(START_SCORE,""+ bestScore);
	    int iter = 0;
	    assertMovables();
	    permutators.reset();
	    do {
		optCoordinateSystems = cloneCoordinateSystems(saveCoords); // reset to original orientation
		mutateCoordinateSystems(optCoordinateSystems);
		newScore = scoreCoordinateSystems(optCoordinateSystems, properties);
		if (newScore < bestScore) {
		    bestScore = newScore;
		    bestPermutators = (IntegerPermutatorList)(permutators.clone());
		    copyCoordinateSystems(bestCoords, optCoordinateSystems); // stores best trafos
		    log.info("New best score found: " + bestScore + " at iteration " + (iter + 1));
		    // outputCoordinateSystems(bestCoords);
		    if (newScore < getScoreMinimumLimit()) {
			log.info("Quitting optimization, because score is lower than minimum score cutoff: " + getScoreMinimumLimit());
			break;
		    }
		}
		if ((++iter % outputInterval) == 0) {
		    log.info("Iteration: " + iter + " Current and best score so far: " + newScore + " " + bestScore);			 
		    log.info("Mutators: " + permutators.toString());
		    log.info("First coordinate system: " + optCoordinateSystems.get(0));
		    if (bestPermutators != null) {
			log.info("Best mutators: " + bestPermutators.toString());
		    }
		}
	    }
	    while (permutators.inc());
	    // apply transformation to objects!
	    applyTransformations(bestCoords);
	    properties.setProperty(NUMBER_STEPS, "" + (iter+1));
	    properties.setProperty(FINAL_SCORE, ""+ bestScore);
	    if (bestPermutators != null) {
		properties.setProperty(FINAL_STATUS, ""+ bestPermutators.toString());
	    }
	}
	catch (RnaModelException rme) {
	    properties.setProperty(ERROR, rme.getMessage());
	}
	log.info("Optimization finished: " + properties);
	assertMovables();
	return properties;
    }

}
