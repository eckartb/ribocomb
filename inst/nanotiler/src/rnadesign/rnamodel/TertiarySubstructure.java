package rnadesign.rnamodel;

// import rnadesign.rnacontrol.*;
import tools3d.objects3d.*;
import tools3d.Vector3D;
import sequence.*;
import java.util.*;

public class TertiarySubstructure {

  private Class type;
  private Object3D structure;
  // private Object3DGraphController controller = new Object3DGraphController ();
  
  public TertiarySubstructure (Class t, Object3D s){ //type of motif, tertiary structure of model
    this.type = t;
    this.structure = s;
  }
  
  public Class getType(){
    return type;
  }
  
  /** Obtain all C1* atoms from a structure. Careful with ambiguity of C1* and C1' in PDB structures. */
  public Vector3D [] getCarbonPrimes ()  {
      String [] atomNames = {"C1*"};
      List<Residue3D> residues = AtomTools.generateResidueList(structure);
      assert(residues!=null);
      List<Atom3D> atoms = AtomTools.generateAtomList(residues, atomNames);
      Vector3D[] vec1 = new Vector3D[atoms.size()];  
      for(int i=0; i<atoms.size(); i++){
        vec1[i] = atoms.get(i).getPosition();
      }
      assert(vec1 != null);
    return vec1;
  }
  
  public Object3D getStructure () {
    return structure;
  }
  
}