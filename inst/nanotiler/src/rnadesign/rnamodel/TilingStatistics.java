package rnadesign.rnamodel;

import static rnadesign.rnamodel.PackageConstants.*;

/** stores information about how well a tiling algorithm worked. */
public class TilingStatistics {

    private double averageAngle = 0.0;
    private double averageDistance = 0.0;
    private int numberAngles = 0;
    private int numberDistances = 0;
    private int numberPlaced = 0;   //keeps track of the junctions that have been added or not
    private int numberUnplaced = 0;
    private double sumAngle = 0.0;
    private double sumDistance = 0.0;

    public TilingStatistics() {

    }

    public TilingStatistics(TilingStatistics other) {
	copy(other);
    }

    public Object clone() {
	return new TilingStatistics(this);
    }

    public void copy(TilingStatistics other) {
	this.averageAngle = other.averageAngle;
	this.averageDistance = other.averageDistance;
	this.numberAngles = other.numberAngles;
	this.numberDistances = other.numberDistances;
	this.numberPlaced = other.numberPlaced;
	this.numberUnplaced = other.numberUnplaced;
	this.sumAngle = other.sumAngle;
	this.sumDistance = other.sumDistance;
    }

    public double getAverageAngle() { return this.averageAngle; }
    
    public double getCoverage() {
	if (getNumberTrials() <= 0) {
	    return 0.0;
	}
	return numberPlaced/(double)(getNumberTrials());
    }

    public double getAverageDistance() { return this.averageDistance; }

    public int getNumberPlaced() { return this.numberPlaced; }

    public int getNumberUnplaced() { return this.numberUnplaced; }

    public int getNumberTrials() { return getNumberPlaced() + getNumberUnplaced(); }

    public void setAverageAngle(double a) { this.averageAngle = a; }

    public void setAverageDistance(double d) { this.averageDistance = d; }

    public void updateAverageAngle(double angle) {

	sumAngle += angle;

	numberAngles++;

	averageAngle = sumAngle/numberAngles;

	setAverageAngle(averageAngle);
    } 

    public void updateAverageDistance(double distance) {

	sumDistance += distance;

	numberDistances++;

	averageDistance = sumDistance/numberDistances;

	setAverageDistance(averageDistance);
    } 

    public void updateCoverage(boolean placedFlag) {
	if (placedFlag) {
	    ++numberPlaced;
	}
	else {
	    ++numberUnplaced;
	}
    }

    public String toString() {
	
	String string = new String("");
	string += ENDL + ENDL + "Placed: " + numberPlaced + ENDL + "Unplaced: " + numberUnplaced;
	string += ENDL + "Coverage: " + (int)(getCoverage() * 100) + "%";
	string += ENDL + "Average best angle: " + getAverageAngle();

	return string;
    }

}


