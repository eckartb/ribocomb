package rnadesign.rnamodel;

import numerictools.PotentialND;
import numerictools.IntegerArrayTools;
import generaltools.Randomizer;
import graphtools.PermutationGenerator;
import graphtools.IntegerPermutator;
import tools3d.objects3d.*;
import tools3d.CoordinateSystem;
import tools3d.GeometryTools;
import tools3d.Vector3D;
import tools3d.Vector3DTools;
import tools3d.objects3d.LinkSet;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DSet;
import tools3d.objects3d.SimpleObject3DLinkSetBundle;
import tools3d.objects3d.Object3DLinkSetBundle;
import java.util.*;
import java.util.logging.*;
import tools3d.Matrix3D;
import tools3d.Matrix3DTools;

/** potential in n dimensions */
public class TraceRnaPotential implements PotentialND {

    private Logger log = Logger.getLogger(PackageConstants.LOGGER_NAME);

    private LinkSet links;
    private Object3DSet objSet;
    private int[][] bestPerms; // stores for each vertex the best permutation of 5'/3' end matchings
    private List<List<Integer> > linkIds; // for n'th vertex , list of all links connecting this vertex
    
    private List<BranchDescriptor3D> branchDescriptors;
    private List<Integer> helixLengths;
    private int helixLengthMax = 100;
    private double turnHeight;
    private double offset;

    private double distMax = 7.0;
    private double distMin = 3.0;
    private double collisionDistance = 4.0;
    private double collisionPenalty = 0.0;
    private int bridgeLenMax = 5;
    private int verboseLevel = 1;
    private char c1 = 'G';
    private char c2 = 'C';
    private double crossingDistanceLimit = 7.0; // minimum distance between direct lines connecting strand ends
    private boolean generateBridgesMode = true;

    public TraceRnaPotential(Object3DSet objSet, LinkSet links, double turnHeight, double offset, double distMax, double distMin,
			     int _helixLengthMax) {
	assert validate(objSet, links, turnHeight, offset);
	this.links = links;
	this.objSet = objSet;
	this.turnHeight = turnHeight;
	this.offset = offset;
	this.distMax = distMax;
	this.distMin = distMin;
	this.helixLengthMax = _helixLengthMax;
	init();
	assert getDimension() == links.size();
	assert validate();
    }

    public boolean getGenerateBridgesMode() { return this.generateBridgesMode; }

    public void setGenerateBridgesMode(boolean mode) { this.generateBridgesMode = mode; }

    public int getVerboseLevel() { return verboseLevel; }

    public void setVerboseLevel(int n) { this.verboseLevel = n; }

    /** Check one edge lengths */
    public static boolean validate(double edgeDistance,
				    double turnHeight, double offset) {
	double helLen = edgeDistance - 2 * offset;
	return helLen > RnaConstants.HELIX_RISE;
    }

    /** Check all graph edge lengths */
    public static boolean validate(Object3DSet objSet, LinkSet links,
				   double turnHeight, double offset) {
	for (int i = 0; i < links.size(); ++i) {
	    if (!validate(links.get(i).getObj1().distance(links.get(i).getObj2()), turnHeight, offset)) {
		return false;
	    }
	}
	return true;
    }

    /** Generates RNA according to angles of helix-cylinder representations */
    public Object3DLinkSetBundle generateRna(double[] angles, String baseName, Object3D nucleotideDB,
					     List<Object3DLinkSetBundle> bridgeBundle) {
	assert angles != null && baseName != null && Object3DTools.validateName(baseName) && nucleotideDB != null;
	assert angles.length == branchDescriptors.size();
	log.info("Starting generateRna");
	for (int i = 0; i < objSet.size(); ++i) {
	    log.info(objSet.get(i).toString());
	}
	double value = getValue(angles); // used for updating bestPerm values
	Object3D root = new SimpleObject3D(baseName);
	LinkSet links = new SimpleLinkSet();
	for (int i = 0; i < branchDescriptors.size(); ++i) {
	    String name = baseName + "_" + (i+1);
	    int len = helixLengths.get(i);
	    BranchDescriptor3D bd = branchDescriptors.get(i);
	    log.info("Generating helix for branch descriptor " + (i+1) + " " + bd.getPosition().toString() + " " 
		     + bd.getDirection().toString());
	    Object3DLinkSetBundle helixBundle = ConnectJunctionTools.generateIdealStem(bd, c1, c2, name, nucleotideDB, len);
	    // Matrix3D rotMatrix = Matrix3DTools.rotationMatrix(bd.getDirection(), angles[i]);
	    Object3D obj = helixBundle.getObject3D();
	    obj.rotate(bd.getDirection(), angles[i]);
	    // obj.rotate(obj.getPosition(), rotMatrix); // this command is buggy!
	    // obj.rotate(obj.getPosition(), rotMatrix); // this command is buggy!
	    root.insertChild(helixBundle.getObject3D());
	    links.merge(helixBundle.getLinks());
	}
	if (generateBridgesMode) {
	    Object3DSet strands = Object3DTools.collectByClassName(root, "RnaStrand");
	    SingleStrandBridgeFinder bridgeFinder = new SingleStrandBridgeFinder(bridgeBundle);
	    bridgeFinder.setLenMax(bridgeLenMax);
	    for (int i = 0; i < objSet.size(); ++i) {
		List<Object3DLinkSetBundle> bridges = generateVertexBridges(angles, i, strands, bridgeFinder);
		if (bridges != null) {
		    log.info("Generated " + bridges.size() + " bridges for vertex " + (i+1));
		    for (int j = 0; j < bridges.size(); ++j) {
			root.insertChildSafe(bridges.get(j).getObject3D());
			links.merge(bridges.get(j).getLinks());
		    }
		}
		else {
		    log.info("No bridges could be generated for vertex " + (i+1));
		}
	    }
	}
	log.info("Finished generateRna");
	return new SimpleObject3DLinkSetBundle(root, links);
    }

    /** Returns nucleotide whose first atom is closestt to position pos */
    Nucleotide3D findClosestStrandEnd(Object3DSet strands, Vector3D pos) {
	assert strands != null && strands.size() > 0;
	double bestDist = 99999.8;
	Residue3D result = null;
	for (int i = 0; i < strands.size(); ++i) {
	    RnaStrand strand = (RnaStrand)(strands.get(i));
	    Residue3D nuc = strand.getResidue3D(0);
	    double d = nuc.getChild(0).getPosition().distance(pos);
	    if (d < bestDist) {
		bestDist = d;
		result = nuc;
	    }
	    nuc = strand.getResidue3D(strand.getResidueCount()-1);
	    d = nuc.getChild(0).getPosition().distance(pos);
	    if (d < bestDist) {
		bestDist = d;
		result = nuc;
	    }
	}
	assert result != null;
	return (Nucleotide3D)result;
    }

    /** Returns score with respect to n'th vertex. PosFive: positions of all 5' ends, Posthree: positions of all 3' ends */
	private List<Object3DLinkSetBundle> generateVertexBridges(double[] angles, int vertexId, Object3DSet strands,
								  SingleStrandBridgeFinder bridgeFinder   ) {

	List<Object3DLinkSetBundle> result = new ArrayList<Object3DLinkSetBundle>();

	Vector3D[] posFive = computeFivePrimePositions(angles, vertexId);
	Vector3D[] posThree = computeThreePrimePositions(angles, vertexId);
	int[] perm = bestPerms[vertexId];
	assert posFive.length == posThree.length;
	assert perm.length + 1 == posFive.length;

	for (int i = 1; i < perm.length; ++i) {
	    Nucleotide3D resFive = findClosestStrandEnd(strands, posFive[perm[i-1]]);
	    Nucleotide3D resThree = findClosestStrandEnd(strands, posThree[perm[i]]);
	    // generate bridge
	    List<Object3DLinkSetBundle> bridges = bridgeFinder.findBridge(resFive, resThree);
	    if ((bridges != null) && (bridges.size() > 0)) {
		result.add(bridges.get(0));
	    }
	}

	// connect last helix with two connections:
	Vector3D lastFivePos1 = posFive[perm[perm.length-1]];
	Vector3D firstThreePos1 = posThree[posThree.length-1];
	Vector3D lastFivePos2 = posFive[posFive.length-1];
	Vector3D firstThreePos2 = posThree[perm[0]];

	Nucleotide3D resFiveB = findClosestStrandEnd(strands, lastFivePos1);
	Nucleotide3D resThreeB = findClosestStrandEnd(strands, firstThreePos1);
	List<Object3DLinkSetBundle> bridges = bridgeFinder.findBridge(resFiveB, resThreeB);
	if ((bridges != null) && (bridges.size() > 0)) {
	    result.add(bridges.get(0));
	}

	Nucleotide3D resFiveC = findClosestStrandEnd(strands, lastFivePos2);
	Nucleotide3D resThreeC = findClosestStrandEnd(strands, firstThreePos2);
	bridges = bridgeFinder.findBridge(resFiveB, resThreeB);
	if ((bridges != null) && (bridges.size() > 0)) {
	    result.add(bridges.get(0));
	}

	return result;
    }

    /** Generates link ids corresponding to n'th object */
    List<Integer> generateObjectLinkIds(int n) {
	assert n >= 0 && n < objSet.size();
	List<Integer> result = new ArrayList<Integer>();
	Object3D obj = objSet.get(n);
	for (int i = 0; i < links.size(); ++i) {
	    Link link = links.get(i);
	    int order = link.linkOrder(obj);
	    assert order < 2;
	    if (order == 1) {
		result.add(i);
	    }
	    else {
		assert order == 0;
	    }
	}
	assert result != null;
	return result;
    }

    private void init() {
	assert links != null;
	linkIds = new ArrayList<List<Integer> >();
	for (int i = 0; i < objSet.size(); ++i) {
	    linkIds.add(generateObjectLinkIds(i));
	}
	initHelices();
	bestPerms = new int[objSet.size()][0]; // store for best permutations for each vertex
	assert validate();
    }

    /** initializes helix with respect to n'th graph edge pointing from p1Pos to p2Pos */
    private void initHelix(Vector3D p1Pos, Vector3D p2Pos) {
	Vector3D dir = p2Pos.minus(p1Pos);
	double helLen = dir.length() - 2 * offset;
	assert helLen > 0; // TODO 
	assert dir.length() > 0.0;
	dir.normalize();
	Vector3D base = p1Pos.plus(dir.mul(offset));
	Vector3D y = Vector3DTools.generateRandomOrthogonalDirection(dir);
	Vector3D x = y.cross(dir);
	CoordinateSystem cs = new CoordinateSystem3D(base, x, y);
	BranchDescriptor3D bd = new SimpleBranchDescriptor3D(new CoordinateSystem3D(Vector3D.ZVEC));
	bd.setCoordinateSystem(cs);
	branchDescriptors.add(bd);
	int numBp = SimpleConnectivityIterator.estimateNumberBasePairs(helLen); // number of base pairs
	if (numBp > helixLengthMax) {
	    numBp = helixLengthMax;
	}
	helixLengths.add(numBp);
	log.info("Added length of helix " + (helixLengths.size()) + " : " + numBp);
    }

    /** Initializes n'th helix corresponding to n'th link */
    private void initHelix(int n) {
	Link link = links.get(n);
	initHelix(link.getObj1().getPosition(), link.getObj2().getPosition());
    }

    private void initHelices() {
	this.branchDescriptors = new ArrayList<BranchDescriptor3D>();
	this.helixLengths = new ArrayList<Integer>();
	for (int i = 0; i < links.size(); ++i) {
	    initHelix(i);
	}
    }

    public boolean validate() {
	boolean check1 = branchDescriptors != null && helixLengths != null && links != null && objSet != null
	    && linkIds != null;
	if (!check1) {
	    return false;
	}
	boolean check2 = links.size() == branchDescriptors.size() && links.size() == helixLengths.size();
	if (!check2) {
	    return false;
	}
	boolean check3 = (objSet.size() == linkIds.size());
	if (!check3) {
	    return false;
	}
	return true;
    }

    public double[] generateLowPosition() {
	double[] result = new double[getDimension()];
	Random rand = Randomizer.getInstance();
	for (int i = 0; i < result.length; ++i) {
	    result[i] = 2.0 * Math.PI * rand.nextDouble(); // random angle between 0 and 2 PI
// 	    Vector3D pos = objSet.get(i).getPosition();
// 	    result[3 * i] = pos.getX();
// 	    result[(3 * i) + 1] = pos.getY();
// 	    result[(3 * i) + 2] = pos.getZ();
	}
	return result;
    }

    public double[] generateHighPosition() { return generateLowPosition(); }

    public int getDimension() { return links.size(); }

    public double getValue(double[] angles) {
	double sum = 0.0;
	for (int i = 0; i < objSet.size(); ++i) {
	    sum += computeVertexValue(angles, i);
	}
	return sum;
    }

    /** Returns score for connecting two strands leaving a certain amount of slop inbetween */
    private double computeStrandConnectionValue(Vector3D posFive, Vector3D posThree) {
	double dist = posFive.distance(posThree);
	if (dist > distMax) {
	    return dist - distMax;
	}
	else if (dist < distMin) {
	    return distMin - dist;
	}
	return 0.0; // everything fine, within constraints!
    }

    /** Returns score with respect to n'th vertex. PosFive: positions of all 5' ends, Posthree: positions of all 3' ends */
    private double getVertexValue(Vector3D[] posFive, Vector3D[] posThree, int[] perm) {
	assert posFive.length == posThree.length;
	assert perm.length + 1 == posFive.length;
	double sum = 0.0;
	List<Vector3D> startPositions = new ArrayList<Vector3D>();
	List<Vector3D> endPositions = new ArrayList<Vector3D>();
	for (int i = 1; i < perm.length; ++i) {
	    sum += computeStrandConnectionValue(posFive[perm[i-1]], posThree[perm[i]]);
	    startPositions.add(posFive[perm[i-1]]);
	    endPositions.add(posThree[perm[i]]);
	}
	// connect last helix with two connections:
	Vector3D lastFivePos1 = posFive[perm[perm.length-1]];
	Vector3D firstThreePos1 = posThree[posThree.length-1];
	Vector3D lastFivePos2 = posFive[posFive.length-1];
	Vector3D firstThreePos2 = posThree[perm[0]];
	startPositions.add(lastFivePos1);
	endPositions.add(firstThreePos1);
	sum += computeStrandConnectionValue(lastFivePos1, firstThreePos1);
	startPositions.add(lastFivePos2);
	endPositions.add(firstThreePos2);
	sum += computeStrandConnectionValue( lastFivePos2, firstThreePos2);
	sum += scoreLineCrossings(startPositions, endPositions); // check if bridges not too close to each other
	return sum;
    }

    /** Returns penalty score if two lines in space (representing two RNA bridges) come too close to each other */
    private double scoreLineCrossing(Vector3D p0, Vector3D p1,
				     Vector3D q0, Vector3D q1) {
	Vector3D u = p1.minus(p0);
	Vector3D v = q1.minus(q0);
	assert u.lengthSquare() > 0.0;
	assert v.lengthSquare() > 0.0;
	double dist = GeometryTools.distanceOfLines(p0, u, q0, v);
	double result = 0;
	if (dist < crossingDistanceLimit) {
	    result = crossingDistanceLimit - dist;
	}
	return result;
    }

    /** Returns score with respect to n'th vertex. PosFive: positions of all 5' ends, Posthree: positions of all 3' ends */
    private double scoreLineCrossings(List<Vector3D> startPositions, List<Vector3D> stopPositions) {
	assert startPositions.size() == stopPositions.size();
	double sum = 0.0;
	for (int i = 0; i < startPositions.size(); ++i) {
	    for (int j = i + 1; j < startPositions.size(); ++j) {
		sum += scoreLineCrossing(startPositions.get(i), stopPositions.get(i),
					 startPositions.get(j), stopPositions.get(j));
	    }
	}
	return sum;
    }

    /** Returns number of "collisions" between points */
    private int countCollisions(Vector3D[] pv, double distance) {
	int result = 0;
	for (int i = 0; i < pv.length; ++i) {
	    for (int j = i+1; j < pv.length; ++j) {
		if (pv[i].distance(pv[j]) < distance) {
		    ++result;
		}
	    }
	    
	}
	return result;
    }

    /** Returns score with respect to n'th vertex. PosFive: positions of all 5' ends, Posthree: positions of all 3' ends */
    private int countCollisions(Vector3D[] posFive, Vector3D[] posThree, double distance) {
	int result = countCollisions(posFive, distance) + countCollisions(posThree, distance);
	for (int i = 0; i < posFive.length; ++i) {
	    for (int j = 0; j < posThree.length; ++j) {
		if (posFive[i].distance(posThree[j]) < distance) {
		    ++result;
		}
	    }
	}
	return result;
    }

    /** Returns score with respect to n'th vertex. PosFive: positions of all 5' ends, Posthree: positions of all 3' ends */
    private double computeVertexValue(Vector3D[] posFive, Vector3D[] posThree, int vertexId) {
	assert posFive.length == posThree.length;
	int order = posFive.length;
	if (order < 2) {
	    return 0.0; // nothing to score because less than 2 helices
	}
	IntegerPermutator perm = new PermutationGenerator(order-1); // always start strand circle at first helix, that saves one permutation order
	double bestScore = 1e30;
	do { // loop over all permuations
	    double score = getVertexValue(posFive, posThree, perm.get());
	    if (score < bestScore) {
		bestScore = score;
		bestPerms[vertexId] = IntegerArrayTools.clone(perm.get());
	    }
	}
	while (perm.hasNext() && perm.inc());
	bestScore = collisionPenalty * countCollisions(posFive, posThree, collisionDistance);
	return bestScore;
    }

    /** Returns score with respect to n'th vertex. PosFive: positions of all 5' ends, Posthree: positions of all 3' ends */
    private double computeVertexValue(double[] angles, int vertexId) {
	Vector3D[] posFiveVec = computeFivePrimePositions(angles, vertexId);
	Vector3D[] posThreeVec = computeThreePrimePositions(angles, vertexId);
	return computeVertexValue(posFiveVec, posThreeVec, vertexId);
    }

    /** A branch descriptor describes the orientation of a helix. A helix connects two points,
     * hence each branch descriptor is assigned to only one of the two points. First object in link is associated with branch descriptors */
    private boolean hasBranchDescriptor(int linkId, int vertexId) {
	assert linkId >= 0 && linkId < links.size();
	assert vertexId >= 0 && vertexId < objSet.size();
	Link link = links.get(linkId);
	Object3D obj = objSet.get(vertexId);
	if (link.getObj1() == obj) {
	    return true;
	}
	assert (link.getObj2() == obj);
	return false;
    }

    private Vector3D computeFivePrimePosition(int linkId, double angle, int vertexId) {
	assert branchDescriptors != null && branchDescriptors.size() == links.size();
	BranchDescriptor3D bd = branchDescriptors.get(linkId);
	Vector3D p;
	if (hasBranchDescriptor(linkId, vertexId)) {
	    p = bd.computeHelixPosition(0, BranchDescriptor3D.INCOMING_STRAND);
	}
	else {
	    p = bd.computeHelixPosition(helixLengths.get(linkId), BranchDescriptor3D.OUTGOING_STRAND);
	}
	p = Matrix3DTools.rotate(p, bd.getDirection(), angle, bd.getPosition()); // check!
	
	return p;
    }

    private Vector3D computeThreePrimePosition(int linkId, double angle, int vertexId) {
	assert branchDescriptors != null && branchDescriptors.size() == links.size();
	BranchDescriptor3D bd = branchDescriptors.get(linkId);
	Vector3D p;
	if (hasBranchDescriptor(linkId, vertexId)) {
	    p = bd.computeHelixPosition(0, BranchDescriptor3D.OUTGOING_STRAND);
	}
	else {
	    p = bd.computeHelixPosition(helixLengths.get(linkId), BranchDescriptor3D.INCOMING_STRAND);
	}
	p = Matrix3DTools.rotate(p, bd.getDirection(), angle, bd.getPosition()); // check!

	return p;
    }

    /** Returns 5' positions of helices ending at certain vertex.
     * @param angle Values of all angles of graph in order of links
     * @param vertexId id of n'th vertex
     */
    private Vector3D[] computeFivePrimePositions(double[] angles, int vertexId) {
	// obtain link ids of this vertex:
	List<Integer> linkId = linkIds.get(vertexId);
	Vector3D[] result = new Vector3D[linkId.size()];
	if (verboseLevel > 2) {
	    System.out.print("5' positions of helices at vertex " + (vertexId + 1) + " : ");
	}
	for (int i = 0; i < result.length; ++i) {
	    // supply link id, rotation of link and vertex id
	    result[i] = computeFivePrimePosition(linkId.get(i), angles[linkId.get(i)], vertexId);
	    if (verboseLevel > 2) {
		System.out.print("" + result[i] + " " + objSet.get(vertexId).getPosition().distance(result[i]) + " ; " );
	    }
	}
	if (verboseLevel > 2) {
	    System.out.println();
	}
	return result;
    }

    /** Returns 3' positions of helices ending at certain vertex.
     * @param angle Values of all angles of graph in order of links
     * @param vertexId id of n'th vertex
     */
    private Vector3D[] computeThreePrimePositions(double[] angles, int vertexId) {
	// obtain link ids of this vertex:
	List<Integer> linkId = linkIds.get(vertexId);
	Vector3D[] result = new Vector3D[linkId.size()];
	if (verboseLevel > 2) {
	    System.out.print("3' positions of helices at vertex " + (vertexId + 1) + " : ");
	}
	for (int i = 0; i < result.length; ++i) {
	    // supply link id, rotation of link and vertex id
	    result[i] = computeThreePrimePosition(linkId.get(i), angles[linkId.get(i)], vertexId);
	    if (verboseLevel > 2) {
		System.out.print("" + result[i] + " " + objSet.get(vertexId).getPosition().distance(result[i]) + " ; ");
	    }
	}
	if (verboseLevel > 2) {
	    System.out.println();
	}	
	return result;
    }

    public void setBasepairCharacters(char c1, char c2) {
	this.c1 = c1;
	this.c2 = c2;
    }

    public int getHelixLengthMax() {
	return helixLengthMax;
    }

    /** Careful: if called after "init" method, then the lengths have no effect */
    private void setHelixLengthMax(int len) {
	this.helixLengthMax = len;
    }

}
