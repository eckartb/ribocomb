package rnadesign.rnamodel.predict3d;

public class MotifNotFoundException extends Exception {

    public MotifNotFoundException() {
	super("Unspecified motif exception.");
    }

    public MotifNotFoundException(String msg) {
	super(msg);
    }

}
