package rnadesign.rnamodel.predict3d;

import java.util.*;
import java.util.logging.*;
import sequence.*;
import rnasecondary.*;
import rnasecondary.substructures.*;

public class SubstructureDatabase{
  
  public Map<Class,List<Substructure>> data;
  public Map<Substructure,String> names;
  Random rand;
//  private static Logger log = Logger.getLogger(LOGFILE_DEFAULT);
  
  public SubstructureDatabase( Map<Class,List<Substructure>> data, Map<Substructure,String> names){
    this.data=data;
    this.names=names;
    
    rand = new Random();
  }
    
  public Substructure getMatchingSubstructure(Substructure substructure) throws MotifNotFoundException{
    // System.out.println("type: "+substructure.getType());
    if(!data.containsKey(substructure.getType())){
      throw new MotifNotFoundException("Not supported type:"+substructure.getType());
    }
    List<Substructure> options = data.get(substructure.getType());
    SecondaryStructure sub = substructure.getStructure();
    double best=-1.0;
    List<String> fileNames = new ArrayList<String>();
    List<Integer> indexs = new ArrayList<Integer>();
    for(int i=0; i<options.size(); i++){
      SecondaryStructure motif = options.get(i).getStructure();
      // SecondaryStructureWriter writer = new SecondaryStructureScriptFormatWriter();
      // System.out.println(writer.writeString(sub));
      // System.out.println(writer.writeString(motif));
      SecondaryStructureSimilarity scorer = new SecondaryStructureSimilarity(sub, motif);
      double score = scorer.findSimilarity();
      String curfile = names.get(options.get(i));
      // System.out.println("Score: "+score+" for "+curfile);
      if(score > best){
        fileNames.clear();
        fileNames.add(curfile);
        indexs.clear();
        indexs.add(i);
        best = score;
      } else
      if(score == best){
        fileNames.add(curfile);
        indexs.add(i);
      }
    }
    if(best <=0){
      throw new MotifNotFoundException("Best score:"+best);
    }
    int count = fileNames.size();
    System.out.println(count+" candidate motifs");
    
    int choice = rand.nextInt(count);
    String fileName = fileNames.get(choice);
    Integer index = indexs.get(choice);
    
    System.out.println("Chosen: "+fileName+" with score "+best);
    SecondaryStructureWriter writer = new SecondaryStructureScriptFormatWriter();
    System.out.println(writer.writeString(sub));
    System.out.println(writer.writeString(options.get(index).getStructure()));
    return options.get(index);
  }
  public String getFilename(Substructure sub){
    return names.get(sub);
  }
}