package rnadesign.rnamodel.predict3d;

import sequence.*;
import rnasecondary.*;
import rnasecondary.substructures.*;

/**returns 3rd and 4th numbers of name in nanoscript tree*/
public class SubstructureTreeNames{

    public static String getName(Substructure sub, Residue res){
      Class type = sub.getType();
      Residue[] edges = sub.getEdges();
      return nameDefault(edges, res);
    }
    
    /* Default naming:
        edge[0] is 1.1
        edge[1] is 1.LAST
        edge[2] is 2.1
        edge[4] is 3.1
        ect.   */
    public static String nameDefault(Residue[] edges, Residue res){
      int index=-1;
      for(int i=0; i<edges.length; i++){
        if(edges[i]==res){
          index=i;
        }
      }
      assert index!=-1;
      int fir = (index/2)+1;
      String sec;
      if(index%2==0){
        sec = "1";
      } else{
        sec = "LAST";
      }
      return (fir+"."+sec);
    }
}