package rnadesign.rnamodel.predict3d;

import java.util.*;
import java.util.logging.*;
import tools3d.Vector3D;
import tools3d.TertiaryStructureSimilarity;
import rnadesign.rnamodel.TertiarySubstructure; 

public class TertiarySubstructureDatabase{
  
  public Map<Class,List<TertiarySubstructure>> data;
  public Map<TertiarySubstructure,String> names;
  Random rand;
//  private static Logger log = Logger.getLogger(LOGFILE_DEFAULT);
  
  public TertiarySubstructureDatabase(Map<Class,List<TertiarySubstructure>> data, Map<TertiarySubstructure,String> names){
    this.data=data;
    this.names=names;
    rand = new Random();
  }
    
  public TertiarySubstructure getMatchingSubstructure(Class type, Vector3D [] substructure) throws MotifNotFoundException { 
    //passed coordinates of the carbon atoms of the input structure and the type of substructure
    if(!data.containsKey(type)){
      throw new MotifNotFoundException("Not supported type:"+type);
    }
    List<TertiarySubstructure> options = data.get(type); //data.get(substructure.getType()); 
    Vector3D [] inputStructure = substructure;//.getCarbonPrimes(); 
    if (inputStructure == null) {
      System.out.println ("Null list of carbon atoms");
    } 
    double best=10000000000.0; //rms is 1 meter
    List<String> fileNames = new ArrayList<String>();
    List<Integer> indexs = new ArrayList<Integer>();
    
    for(int i=0; i<options.size(); i++){ 
      Vector3D [] databaseStructure = options.get(i).getCarbonPrimes(); 
      
      if (databaseStructure.length == 0) {
        System.out.println ("Null list of carbon atoms");
      }
      if (databaseStructure.length != inputStructure.length) {
        continue; 
      }
      // System.out.println ("Array lengths equal? (InTSD) " + (inputStructure.length == databaseStructure.length));
      // System.out.println ("Array 1 " + inputStructure.length + " Array 2 " + databaseStructure.length); //TODO: remove lines
      // for (int w = 0; w < databaseStructure.length; w++) {
      //   System.out.println (databaseStructure[w]);
      // }
      TertiaryStructureSimilarity scorer = new TertiaryStructureSimilarity(inputStructure, databaseStructure); //returns RMSD, lower is better
      double score = scorer.findSimilarity();
      String curfile = names.get(options.get(i));
      //System.out.println("Score: "+score+" for "+curfile); //TODO: remove during biowulf testing, before commit
      if(score < best){
        fileNames.clear();
        fileNames.add(curfile);
        indexs.clear();
        indexs.add(i);
        best = score;
      } else if(score == best){
        fileNames.add(curfile);
        indexs.add(i);
      }
    }
    if (best >= 10000000000.0) {
      System.out.println("No motifs found in database.");
    }
    
    int count = fileNames.size();
    System.out.println(count+" candidate motifs");
    int choice = rand.nextInt(count); //will randomly select best motif of multiple canidates
    String fileName = fileNames.get(choice);
    Integer index = indexs.get(choice);
    
    System.out.println("Chosen: "+fileName+" lowest rms "+best);
    return options.get(index);
  }
  
  public String getFilename(TertiarySubstructure sub){
    return names.get(sub);
  }
}