package rnadesign.rnamodel.predict3d;

import java.util.*;
import java.util.logging.*;
import tools3d.Vector3D;
import tools3d.objects3d.*;
import tools3d.symmetry2.*;
import rnadesign.rnamodel.*;
import rnadesign.rnacontrol.*;
import rnasecondary.*;
import rnasecondary.substructures.*;
import sequence.Residue;
import sequence.Sequence;

import static rnadesign.rnacontrol.PackageConstants.*;

public class TraceSecondaryMotifScriptGenerator implements ScriptGenerator {

  private String fileName;
  private List< String[] > fuseNames; //list of objects that will be fuse together
  private List< String > treeNames; //list of objects that will be in tree
  private List< String > removeNames; //list of objects that will be deleted
  private Map<Substructure,String> nameMap; //numeric name for substructure
  private String name; //output name
  private SecondaryStructure structure; //input
  private SubstructureDatabase data; //motif database
  private int treeCount; //number of objects in tree (for naming)
  private int extCount; //number of extend regions (for naming)
  private List< Substructure > subPairs; //list of substructures being used (for fusing)
  
  // private Map<String, Set<String>> blockGroups = new HashMap<String, Set<String>>();
  
  private double repuls = 0.0;
  private double vdw = 1.0;
  private int steps = 500000;
  private double randomizeRadius = 0.0; //default radius randomizeblocks
  private boolean changeOrientation = false; //change the orientation of block in randomizeblocks command
  
  private static Logger log = Logger.getLogger(LOGFILE_DEFAULT);
  
  // List<String> constraints = new ArrayList<String>();

  public TraceSecondaryMotifScriptGenerator(SecondaryStructure structure, String fileName,  String name, SubstructureDatabase data, Properties params) {

    assert structure != null;
    structure = SecondaryStructureTools.cleanCopy(structure);
    this.structure = structure;
    this.fileName = fileName;
    this.name = name;
    this.data = data;
    if(params.getProperty("repuls")!=null){
      this.repuls = Double.parseDouble( params.getProperty("repuls") );
    }
    if(params.getProperty("steps")!=null){
      this.steps = Integer.parseInt( params.getProperty("steps") );
    }
    if(params.getProperty("randomizeRadius")!=null){
      this.randomizeRadius = Double.parseDouble( params.getProperty("randomizeRadius") );
    }
    if(params.getProperty("changeOrientation")!=null){
      this.changeOrientation = Boolean.parseBoolean( params.getProperty("changeOrientation") );
    }
    // log.info("Using " + internalLoops.size() + " internal loop motifs.");
    // exportCount=-1;
    // log.info("Starting TraceSecondaryMotifScriptGenerator with objects:");
  }
  
  /**write a script for 3D structure creation*/
  public String generate() {
    
    SecondaryStructureWriter writer = new SecondaryStructureScriptFormatWriter();
    System.out.println(writer.writeString(structure));
    
    StringBuffer buf = new StringBuffer(); //script string
    
    fuseNames = new ArrayList< String[] >();
    treeNames = new ArrayList< String >();
    removeNames = new ArrayList< String >();
    nameMap = new HashMap<Substructure, String>();
    subPairs = new ArrayList<Substructure>();
    
    //header and defualt script
  	buf.append("# Script generated at " + new java.util.Date() + " by TraceSecondaryMotifScriptGenerator for " + fileName + NEWLINE);
  	buf.append("source ${NANOTILER_HOME}/resources/defaults.script" + NEWLINE);

    SubstructureMapper mapper = new SubstructureMapper(); //divides structure into substructures

    /*import substructures
    the structure is broken down to smaller substructures (loops, stems, ect)
    single stranded regions have extra residues on each end that overlap with stems (used for optimization then removed)
    */
    treeCount = 0;
    // SecondaryStructureWriter writer = new SecondaryStructureScriptFormatWriter();
    System.out.println("structures:");
    List<Substructure> subs = mapper.getSubstructures(structure); //Non-redundant list of largest possible substructures
    for(int i=0; i<subs.size(); i++){
      buf.append( chooseMotif(subs.get(i)) ); //find matching 3D motif for 2D substructure
      // System.out.println(writer.writeString(subs.get(i).getStructure()));
    }
    System.out.println("subs: "+subs.size());

    /*genconstraints
    substructures are linked along backbones.
    optimization constraints are added, then the substructures are fused together
    the placement of the links is determined using overlapping bases
    if both a helix and a single strand contain the same residue, then they should be linked in that spot
    */
    Map<Residue, List<Substructure>> pairs = SubstructureMapper.getPairs(subPairs); //find backbone continuation (fuse locations) between all imported motifs
    System.out.println("pairs: "+pairs.keySet().size());
    for(Residue res : pairs.keySet()){
      List<Substructure> sides = pairs.get(res); //all motifs containing the residue
      log.info(sides.size() + " substructures contain res "+res);
      if(sides.size()==2){ //normal case
        String name0 = getName(sides.get(0),res); //reside index
        String name1 = getName(sides.get(1),res);
        // System.out.println(writer.writeString(sides.get(0).getStructure()));
        // System.out.println(writer.writeString(sides.get(1).getStructure()));
        if(sides.get(0).getType()!=FindHelix.class){ //try to remove overlap from single strand, not helix
          buf.append( addFuse(name0,name1) );
        } else if(sides.get(1).getType()!=FindHelix.class){
          buf.append( addFuse(name1,name0) );
        } else{ //both sides of the overlap were helices. shouldn't happen, even if helices are adjacent (imaginary linker region is used)
          log.severe("Helix overlap");
        }
      }
      if(sides.size()==3){ //speicial case length 1 helix, where single strand overlap comes from both sides of the helix
        Substructure helix = null;
        int helixCount = 0;
        for(int i=0; i<3; i++){ //find which is the helix so it is not removed
          if(sides.get(i).getType()==FindHelix.class){
            helix = sides.get(i);
            helixCount++;
          }
        }
        if(helixCount==1){ //one helix, two single strands
          String name0 = getName(helix,res);
          for(int i=0; i<3; i++){
            if(sides.get(i).getType()!=FindHelix.class){
              String name1=getName(sides.get(i),res);
              buf.append( addFuse(name1,name0) ); //fuse each single strand to the helix, but not to eachother
            }
          }
        } else{ //number of helices besides 1
          log.severe("No helix in 3-way overlap: "+helixCount);
        }
      }
    }
    for(int i=0; i<treeNames.size(); i++){ //atom collision check
      for(int j=i+1; j<treeNames.size(); j++){
        buf.append("gencollisionconstraint "+treeNames.get(i)+" "+treeNames.get(j)+NEWLINE);
      }
    }
    
    /*optimize*/
    String blocks = "";
    for(int i=0; i<treeNames.size(); i++){
      blocks+=treeNames.get(i)+";";
    }
    if(fuseNames.size()>0){
      buf.append("randomizeblocks blocks="+blocks + " r=" + randomizeRadius + " orient=" + changeOrientation + NEWLINE);
      buf.append("optconstraints steps="+steps+" repuls="+repuls+" blocks="+blocks +  NEWLINE);
      // buf.append("optconstraints steps="+steps+" vdw="+vdw+" blocks="+blocks +  NEWLINE);

    }
    buf.append("exportpdb "+name+"_semi.pdb"+NEWLINE);

    /*extend dangling ends*/
  	extCount = 0; //for naming of created regions
  	FindDanglingEnd fde = new FindDanglingEnd(structure); //list of all dangling ends
    for(int i=0; i<fde.getStructures().size(); i++){ //create each
      Substructure sub = fde.getSubstructures().get(i);
      Residue res = fde.getEdges().get(i)[0];
      List<Substructure> sides = pairs.get(res);
      if(sides.size()!=1){ //single strand should only have 1 connection to another substructure
        log.severe(sides.size() + " substructures contain res "+res);
      }
      String name = getName(sides.get(0), res);
      String extendName = "_extend"+extCount;
      extCount++;
      int length = fde.getStructures().get(i).getSequence(0).size()-1; //substructure contains extra base for overlap
      buf.append("extend "+name+" l="+length+" name="+extendName+NEWLINE);
      String[] bond = {name,"root."+extendName};
      fuseNames.add(bond);
      newMotifUpdate(sub);
      buf.append(mutateMotif(sub));
      // treeNames.add("root."+extendName);
    }
    
    /*fusestrands
    an alias is given to all relevant residues
    the alias does not change durring fusing, while the numeric naming does
    */
  	for (int i=0; i<fuseNames.size(); i++){
  		buf.append("alias " + fuseNames.get(i)[0] + " " + i + "_0" + NEWLINE);
  		buf.append("alias " + fuseNames.get(i)[1] + " " + i + "_1" + NEWLINE);
  	}
    for(int i=0; i<removeNames.size(); i++){
      buf.append( "alias "+removeNames.get(i) + " " + i + NEWLINE);
    }
  	for (int i=0; i<fuseNames.size(); i++){
  		buf.append("fusestrands #" + i + "_0 #" + i + "_1" + NEWLINE);
  	}
    for(int i=0; i<removeNames.size(); i++){ //remove overlapping bases
      buf.append( "remove #"+i+ NEWLINE);
    }
    
    // buf.append("mutateall filename=" + fileName + NEWLINE); //fix sequence
    
    /*export*/
    buf.append("exportpdb "+name+".pdb"+NEWLINE);
    
    return buf.toString();
  }
  
  //find mathching 3D motif from database for 2D substructure
  private String chooseMotif(Substructure sub){
    if(sub.getType()==FindHelix.class){ //TODO: use database for helices
      newMotifUpdate(sub);
      String mutate = mutateMotif(sub);
      return "genhelix bp="+sub.getStructure().getSequence(0).size()+NEWLINE+mutate;
    }
    try{
      // System.out.println("type: "+sub.getType());
      Substructure imp = data.getMatchingSubstructure(sub);
      String impName = data.getFilename(imp);
      newMotifUpdate(sub);
      String mutate = mutateMotif(sub);
      return "import "+impName+NEWLINE+mutate;
      
    } catch(MotifNotFoundException e){ //divide motif
      System.out.println("MNFE: "+e);
      String imports = "";
      List<Substructure> children = sub.getChildren();
      if(children.size()==0){ //TODO: handle no children
        System.out.println("NEED SYNTHETIC MOTIF");
      }
      for(int i=0; i<children.size(); i++){
        // System.out.println("child type: "+children.get(i).getType());
        imports+=chooseMotif(children.get(i));
      }
      return imports;
    }
  }
  
  //save name for new import
  private void newMotifUpdate(Substructure sub){
    nameMap.put(sub, "1."+(treeCount+1)); //save for reference
    treeNames.add("1."+(treeCount+1));
    treeCount++;
    subPairs.add(sub); //list of all imported substructures
  }
  
  //fix sequence to match input
  private String mutateMotif(Substructure sub){
    String first = nameMap.get(sub);
    String result = "mutate ";
    
    if(sub.getType()==FindHelix.class){ //reverse opposite strand
      String seq1 = sub.getStructure().getSequence(0).sequenceString();
      String seq2 = sub.getStructure().getSequence(1).sequenceString();
      StringBuffer buffer = new StringBuffer(seq2);
      buffer.reverse();
      String seq2_rev = buffer.toString();
      result+=first+".1:"+seq1+" "+first+".2:"+seq2_rev;
    } else 
      
    if(sub.getType()==FindDanglingEnd.class){ //ignore overlapping residue
      String seq = sub.getStructure().getSequence(0).sequenceString();
      String seq_ = seq.substring(1,seq.length());
      result+=first+":"+seq_;
    } else { //normal case
      
      for(int i=0; i<sub.getStructure().getSequenceCount(); i++){
        Sequence seq = sub.getStructure().getSequence(i);
        result+=first+"."+(i+1)+":"+seq.sequenceString()+" ";
      }
    }
    return result+NEWLINE;
  }
  
  //find numeric id for a residue in a given Substructure
  private String getName(Substructure sub, Residue res){
    String first = nameMap.get(sub);
    String last = SubstructureTreeNames.getName(sub, res);
    return first+"."+last;
  }
  
  //add a constraint and later fuse
  private String addFuse(String fir, String sec){
    StringBuffer buf = new StringBuffer();
    
    buf.append("gensuperimposeconstraint "+fir+" "+sec+ NEWLINE);
    // buf.append("gendistconstraint "+fir+" "+sec+" 0 0"+ NEWLINE);
    String[] pair = {fir, sec};
    fuseNames.add(pair);
    removeNames.add(fir);
    return buf.toString();
  }


}