package rnadesign.rnamodel.predict3d;

import java.util.*;
import java.util.logging.*;
import tools3d.Vector3D;
import tools3d.objects3d.*;
import tools3d.symmetry2.*;
import rnadesign.rnamodel.*;
import rnadesign.rnacontrol.*;
import rnasecondary.*;
import sequence.Residue;
import sequence.Sequence;

import static rnadesign.rnacontrol.PackageConstants.*;

public class TraceSecondaryScriptGenerator implements ScriptGenerator {

    // private Object3D graphRoot;
    // private Object3DSet vertices;
    // private LinkSet links;
    private String fileName;
    private double distMin = 6.0;
    private double distMax = 12.0;
    private int[] helixLengths;
    private List<String> helixNames;
    private List< String[] > fuseNames = new ArrayList< String[] >(); //list of objects that will be fuse together
    private List< String > treeNames = new ArrayList< String >(); //list of objects that will be in tree
    //private int[] origHelixIndices; // for specifying which helices are the original of the symmetry copies
    private double kt = 10.0;
    private String name;
    private int numSteps = 100000;
    private int numStepsHelix = 500000;
    private boolean randomizeMode = false;
    private double vdwWeight = 1.0;
    private String hName = "traced_"; // "helix"; // root.traced.traced_1_root.traced_1_forw
    private int checkLen = 0;
    private double scale = 1.4;
    private SecondaryStructure structure;
    private int extCount;//naming extend regions
    private int exportCount;//naming exported pdb
    private boolean diagnosticmode = false;
    // private List<SymEdgeConstraint> symConstraints;
    private static Logger log = Logger.getLogger(LOGFILE_DEFAULT);
    
    List<String> constraints = new ArrayList<String>();

    public TraceSecondaryScriptGenerator(SecondaryStructure structure, String fileName,  String name, Properties params) {
    
	// Object3DSet vertices
	// this.graphRoot= convertToGraph(structure); //  graphRoot;
	// this.vertices = new SimpleObject3DSet();
	// for (int i = 0; i < graphRoot.size(); ++i) {
	//     vertices.add(graphRoot.getChild(i));
	// }
	// this.links = new SimpleLinkSet();
	assert structure != null;
	this.structure = structure;
	this.fileName = fileName;
	this.name = name;
	extCount=0;
	exportCount=-1;
	System.out.println("Starting TraceSecondaryScriptGenerator with objects:");
	parseParams(params);
    }

    private void parseParams(Properties params) {
	if (params.getProperty("min") != null) {
	    this.distMin = Double.parseDouble(params.getProperty("min"));
	}
	if (params.getProperty("max") != null) {
	    this.distMax = Double.parseDouble(params.getProperty("max"));
	}
    }

    public String generate() {
	StringBuffer buf = new StringBuffer();
	Date date = new java.util.Date();
	buf.append("# Script generated at " + date + " by tracesecondary for " + fileName + NEWLINE);
	buf.append("source ${NANOTILER_HOME}/resources/defaults.script" + NEWLINE);
	buf.append(writeBreak("genhelix"));
	// buf.append("set distMin2 10" + NEWLINE);
	// buf.append("set distMax2 40" + NEWLINE);

	//TODO parse params

	buf.append(generateHelices() ); //make stems
	buf.append(generateSingleStrands() ); //make dist constraints, optimize helices, make bridges 
	
	buf.append(generateExtends() ); //make extends
	
	buf.append(generateFuses() ); //fuse together
	
	buf.append(writeBreak("mutate"));
	
	buf.append("mutateall filename=" + fileName + NEWLINE); //fix sequence
	
	buf.append("exportpdb "+name+".pdb"+NEWLINE);
	
	return buf.toString();
    }
    

	/** creates genhelix command for all stems */
    private String generateHelices() {
	StringBuffer buf = new StringBuffer();
	List<Stem> helices = SecondaryStructureTools.findHelices(structure);
	helixLengths = new int[helices.size()];
	helixNames = new ArrayList<String>();

	for (int i = 0; i < helices.size(); ++i) {
	    Stem helix = helices.get(i);
	    int numBp = helix.size();
	    helixLengths[i] = numBp;
	    helixNames.add("_stem_" + i);

	    buf.append("genhelix bp=" + helix.size() + " name=" + helixNames.get(i) + NEWLINE);
	    treeNames.add( "root." + helixNames.get(i) + "_root." + helixNames.get(i) + "_forw");
	    treeNames.add( "root." + helixNames.get(i) + "_root." + helixNames.get(i) + "_back");
	}


	return buf.toString();
    }
    
    /** creates gendistconstraint, genoptconstraints, and bridgeit commands */ 
	private String generateSingleStrands() {
	
	int min = 3; // min distance between stems
	int max = 4; // max distance between per single-stranded residue
	
	StringBuffer buf = new StringBuffer();
	List<Stem> helices = SecondaryStructureTools.findHelices(structure);
	List<Interaction> singleStrands = SecondaryStructureTools.findSingleStrands(structure);
	
	List<String> startPositions = new ArrayList<String>();
	List<String> stopPositions = new ArrayList<String>();
	List<Integer> lengths = new ArrayList<Integer>();
	
	List<String> startStrandPositions = new ArrayList<String>();
	List<String> stopStrandPositions = new ArrayList<String>();
	
	Stem stem = null;
	
	
		
	for (int i = 0; i < singleStrands.size(); ++i) {
	
		Interaction singleStrand = singleStrands.get(i);
	    
	    Residue startRes = singleStrand.getResidue1();
	    Residue stopRes = singleStrand.getResidue2();
	    assert startRes.getPos() < stopRes.getPos();
	    String startPos = "-1"; // 0 based counting
	    int startStrandPos = -1;
	    int startOrient = -1;
	    
	    for (int j=0; j<helices.size(); j++){
	    
	    	stem = helices.get(j);
	    	startStrandPos = j;
	    	

	    	
	    	if (stem.isThreePrime1(startRes)) {
	    		startPos = Integer.toString(startRes.getPos());
	    		startOrient = 1;
	    		break;
	    	} else if (stem.isThreePrime2(startRes)) {
	    		startPos = Integer.toString(startRes.getPos());
	    		startOrient = 2;
	    		break;
			}
	    	

	    }
	    
	    if (startPos != "-1"){
	    
	   		String stopPos = "-1";
	   		int stopStrandPos = -1;
	   		int stopOrient = -1;
	   		
	    
			for (int j=0; j<helices.size(); j++){
			
				stem = helices.get(j);
				stopStrandPos = j;
			
			
			
	    	if (stem.isFivePrime1(stopRes)) {
	    		stopPos = Integer.toString(stopRes.getPos());
	    		stopOrient = 1;
	    		break;
	    	} else if (stem.isFivePrime2(stopRes)) {
	    		stopPos = Integer.toString(stopRes.getPos());
	    		stopOrient = 2;
	    		break;
	    	} else{
	    	//	System.out.println("failed on stop");
	    	}
				
			}
			
			assert startRes.getParentObject() == stopRes.getParentObject();
			// assert startOrient == stopOrient;
			assert startStrandPos>=0 && stopStrandPos>=0;
			assert startOrient>=0 && stopOrient>=0;
			assert Integer.parseInt(startPos)>=0 && Integer.parseInt(stopPos)>=0;
			assert stem != null;
			//assert stopPos.toInteger() > startPos.toInteger();
			
			
			
			//System.out.println("Start : "+(startStrandPos+1)+ " " + startPos+" Stop : "+(stopStrandPos+1)+ " " +stopPos);
			if(Integer.parseInt(stopPos)>Integer.parseInt(startPos)){
				startPos = "LAST";
				stopPos = "1";
			} else
			if(Integer.parseInt(startPos)>Integer.parseInt(stopPos)){
				stopPos = "LAST";
				startPos = "1";
			} else{
				assert false; //should not be equal
			}
			
			String startID = "1." + (startStrandPos+1) + "." + startOrient + "." + (startPos);
			String stopID = "1." + (stopStrandPos+1) + "." + stopOrient + "." + (stopPos);
			
			startPositions.add(startID);
			stopPositions.add(stopID);
			
			startStrandPositions.add("root."+helixNames.get(startStrandPos)+"_root."+helixNames.get(startStrandPos)+ ( startOrient==1 ? "_forw" : "_back"));
			stopStrandPositions.add("root."+helixNames.get(stopStrandPos)+"_root."+helixNames.get(stopStrandPos)+( stopOrient==1 ? "_forw" : "_back"));
			
			int length = stopRes.getPos() - startRes.getPos();
			assert length > 0;
			lengths.add(length);
			
	    	buf.append("gendistconstraint " +  startID + " " + stopID + " " + min + " " + length*max + NEWLINE);
      //  buf.append("genmotifconstraint " +  startID + " " + stopID + " " + length + NEWLINE);
	    	
	    }

	}
	
	buf.append(generateOptConstraints());
	
	assert startPositions.size()==stopPositions.size();
	assert startPositions.size()==lengths.size();
	
	int bridgecount = 0;
	
	buf.append(writeBreak("bridge"));
	
	for(int j=0; j<startPositions.size(); j++){
		if(lengths.get(j)-1 > 0){
		String fuseName = ("_bridge_" + bridgecount);
		bridgecount++;
		buf.append("bridgeit2 " +  startPositions.get(j) + " " + stopPositions.get(j) + " m="+ (lengths.get(j)-1) + " l=" + (lengths.get(j)-1) + " name=" + fuseName + NEWLINE);
		
		helixNames.add("1."+ (helixNames.size()+1) );
		
		treeNames.add( "root." + fuseName );
		
		String[][] names = { { startStrandPositions.get(j) + ".LAST", "root."+fuseName + ".1"} , { startStrandPositions.get(j) + ".LAST", stopStrandPositions.get(j) + ".1" } };
		fuseNames.add( names[0] );
		fuseNames.add( names[1] );
		} else{
			String[] names = { startStrandPositions.get(j) + ".LAST", stopStrandPositions.get(j) + ".1" };
			fuseNames.add( names );
		}
	}
	
	return buf.toString();
    }
    
    /** creates command to optimize helix placement */
    private String generateOptConstraints() {
    
    List<Stem> helices = SecondaryStructureTools.findHelices(structure);
    
    StringBuffer buf = new StringBuffer();
    
    if(helices.size()>1){
    
    	buf.append(writeBreak("optimize"));
    
    	buf.append("optconstraints " + "steps=500000 kt=100 vdw=1 repuls=100" + " blocks=" ); //TODO make variables
    	for (int j = 0; j < helixNames.size(); ++j) {
			buf.append( "root."+helixNames.get(j)+"_root;");
		}
		buf.append(NEWLINE);
		
    }
	
	
	
	return buf.toString();
    }
    
    /** Write the command to fuse the various stems and loop regions into the original sequences*/
    private String generateFuses() {
    StringBuffer buf = new StringBuffer();
    
    for ( String[] i : fuseNames ){
		//System.out.println ( "To fuse : " + i[0] + " & " + i[1] );
	}
	
	buf.append(writeBreak("fuse"));
	
	for (int i=0; i<fuseNames.size(); i++){
		buf.append("alias " + fuseNames.get(i)[0] + " " + i + "_0" + NEWLINE);
		buf.append("alias " + fuseNames.get(i)[1] + " " + i + "_1" + NEWLINE);
	}
	
	for (int i=0; i<fuseNames.size(); i++){
		buf.append("fusestrands #" + i + "_0 #" + i + "_1" + NEWLINE);
	}

	return buf.toString();
	}
	
	
	/** cycles through end residues of every sequence to find regions that need extending*/
	private String generateExtends() {
	StringBuffer buf = new StringBuffer();
	
	buf.append(writeBreak("extend"));
	
	extCount = 0; //for naming of created regions
	
	for(int i=0; i<structure.getSequenceCount(); i++){
	
		Sequence curSeq = structure.getSequence(i);
		
		buf.append( writeExtends( curSeq.getResidue(0) , SecondaryStructureTools.findExtension(structure, curSeq.getResidue(0),true) ) ); //1st res
		buf.append( writeExtends( curSeq.getResidue(curSeq.size()-1) , SecondaryStructureTools.findExtension(structure, curSeq.getResidue( curSeq.size()-1 ), false) ) ); //last res
	}
	
	/*List<Stem> helices = SecondaryStructureTools.findHelices(structure);
	
	for(int i=0; i<(helices.size()*2); i++){ //check forw and back
		System.out.println( "index:"+i/2 );
		Stem curSeq = helices.get(i/2);
		
		boolean dirForw = treeNames.get(i).contains("forw") ? true : false ;
		
		buf.append( writeExtends( treeNames.get(i) + ".1" , SecondaryStructureTools.findExtension(structure, ( dirForw ? curSeq.getResidue1(0) : curSeq.getResidue2(0) ) ,true) ) );
		buf.append( writeExtends( treeNames.get(i) + ".LAST" , SecondaryStructureTools.findExtension(structure, ( dirForw ? curSeq.getResidue1(curSeq.size()-1) : curSeq.getResidue2(curSeq.size()-1) ), false) ) );
	}
	*/
	return buf.toString();
	}
	
	
	/** Writes extend command for a given extend region*/
	private String writeExtends(Residue res, int length ) { //res specifies the unpaired end of a sequence (extend region)
	StringBuffer buf = new StringBuffer();
	
	if (length>0){
	
	List<Stem> helices = SecondaryStructureTools.findHelices(structure);
	Sequence seq = (Sequence)( res.getParentObject() );
	Residue targetRes;
	
	if ( res.getPos() == 0 ) { //res is unpaired start of extend. Add/subtract length to find first paired residue
		
		targetRes = seq.getResidue( res.getPos() + (length ) );
	} else {
		targetRes = seq.getResidue( res.getPos() - (length ) );
	}
	
	String fuseName = "_extend_"+ extCount;
	String[] names = new String[2];
	names[0] = "root." + fuseName;
	
	for(int i=0; i<helices.size(); i++){ //check forw and back, first and last of every stem for a match

		Stem curStem = helices.get(i);
		
		/* 
		treeNames.get(2*i) returns the forw strand
		treeNames.get(2*i+1) returns the back strand
		
		paired nucleotides in curStem (secondary.Stem) have the same index number
		this is different from the strand3d class where the first residue of the paired sequences are on opposite sides of the stem
		because of this, 
		curStem.getResidue2(0)   (1st residue on 2nd strand of stem)
		is equivalent to 
		treeNames.get(2*i+1) + ".LAST"   (last residue on 2nd strand of strand3d)
		
		this only impacts the back strand, not the forw
		
		*/
		if ( curStem.getResidue1(0) == targetRes ){ //1st res 1st strand
			names[1] = treeNames.get(2*i) + ".1";
			break;
		} else
		if ( curStem.getResidue2(0) == targetRes ){ //1st res 2nd strand
			names[1] = treeNames.get(2*i+1) + ".LAST";
			break;
		} else
		if ( curStem.getResidue1(curStem.size()-1) == targetRes ){ //last res 1st strand
			names[1] = treeNames.get(2*i) + ".LAST";
			break;
		} else
		if ( curStem.getResidue2(curStem.size()-1) == targetRes ){ //last res 2nd strand
			names[1] = treeNames.get(2*i+1) + ".1";
			break;
		}
		
	}
	
	assert names[1] != null;
	
	fuseNames.add( names );
	

		
	buf.append("extend " + names[1] + " l=" + length + " name="+ fuseName + NEWLINE); 
	treeNames.add( "root." + fuseName );
	extCount++;
		
		
	
	
	//String[] names = { startStrandPositions.get(i), "root."+fuseName };
	//fuseNames.add( names );
	}
	return buf.toString();
	
	}
	
	private String writeBreak(String mess ) {
		exportCount++;
		String label = ("# ---" + mess + "--- #" +NEWLINE);
		String export = ("exportpdb " + name + "_step" + exportCount + ".pdb" + NEWLINE);
		if (diagnosticmode){
		return export+label;
		} else {
		return label;
		}
    }
}
    
