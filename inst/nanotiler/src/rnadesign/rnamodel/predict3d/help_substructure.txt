4/18/16
Johann Miller

rnasecondary.substructures.SubstructureMapper
  Divides the secondary structure into substructure blocks
  It uses the various FindSubstructure classes to find these blocks
  
rnadesign.rnamodel.predict3d.SubstructureNames
  translates a residue of a substructure to the corresponding name in the nanoscript tree
  
How to add more substructures to the 3D predictor:
  
  Add a new FindSubstructure implementation for the new type of substructure you are adding

  Add this new implementation to the list of finders in SubstructureMapper

  Check that rnadesign.rnamodel.predict3d.SubstructureNames returns the proper names for your substructure
    If not, add a method specific to your substructure type [ Substructure.getType() == FindYourSubstructure.class ]
    
  Using your new FindSubstructure implementation, find substructure examples in large PDB findSingleStrands
  
  Add the structures you find to the motif database nanotiler/resources/predict3d
  
  Check that rnasecondary.SecondaryStructureSimalarity is able to choose a matching motif for your substructure
  
  
Substructures that should be added:
  kissing loop (children: 4 single strands and 1 helix).
  3-way junction and beyond (children: 3 or more single strands). Build off internal loop finder
  full structure templates?