package rnasecondary;

import java.io.*;
import java.text.ParseException;
import java.util.*;
import java.util.logging.*;
import generaltools.*;
import sequence.*;
import launchtools.*;
import org.testng.annotations.*;

import static rnasecondary.PackageConstants.*;

/** Computes secondary structure by simply setting lowest energy stems first. Relies on matchfold/nanofolder script matchfold.sh */
public class MatchFoldSecondaryStructurePredictor extends ResultWorker {

    private Logger log = Logger.getLogger(LOGFILE_DEFAULT);
    private Level debugLogLevel = Level.FINE;
    private static String scriptName = "matchfold.sh"; // rb.getString("matchfoldscript");

    private UnevenAlignment ali;
    private double gcEnergy = -1.0;
    private double auEnergy = -0.8;
    private Alphabet alphabet = DnaTools.AMBIGUOUS_RNA_ALPHABET;
    private Sequence[] sequences;
    private int stemLengthMin = 3;
    private int stopOffset = 3;

    public MatchFoldSecondaryStructurePredictor(UnevenAlignment ali) { 
	assert ali != null;
	this.ali = ali;
	this.sequences = getSequences(ali);
    }
    
    private Sequence[] getSequences(UnevenAlignment ali) {
	Sequence[] sequences = new Sequence[ali.getSequenceCount()];
	for (int i = 0; i < ali.getSequenceCount(); ++i) {
	    sequences[i] = ali.getSequence(i);
	}
	return sequences;
    }

    /** writes sequences to output stream */
    private void writeSequences(OutputStream os, Sequence[] sequences) throws IOException {
	assert sequences != null;
	assert sequences.length > 0;
	PrintStream ps = new PrintStream(os);
	for (int i = 0; i < sequences.length; ++i) {
	    ps.println(">s" + (i+1));
	    ps.println(sequences[i].sequenceString());
	    // System.out.println("written input sequence: " + sequences[i].sequenceString());
	}
    }

    /** uses FileWriter to write sequences to output stream */
    private void writerSequences(FileWriter os, Sequence[] sequences) throws IOException {
	assert sequences != null;
	assert sequences.length > 0;
	//	PrintStream ps = new PrintStream(os);
	for (int i = 0; i < sequences.length; ++i) {
	    os.write(">s" + (i+1)+"\n");
	    os.write(sequences[i].sequenceString()+"\n");
	    // System.out.println("written input sequence: " + sequences[i].sequenceString());
	}
    }

    /** launches matchfold given sequence, returns the raw line output  */
    private String[] launchFolding(Sequence[] sequences) throws IOException {
	// write secondary structure to temporary file
	// File tmpInputFile = File.createTempFile("nanotiler_matchold",".seq");
	// tmpInputFile.deleteOnExit();
	String inputFileName = "tmpin.seq"; // tmpInputFile.getAbsolutePath();
	// File tmpInputFile = new File(inputFileName); // File.createTempFile("nanotiler_matchold",".seq");
	FileWriter bf = new FileWriter(inputFileName);
	// FileOutputStream fos = new FileOutputStream(tmpInputFile);
	// write secondary structure, but only write sec structure, not sequence:
	log.fine("Writing to file: " + inputFileName);
	// log.log(debugLogLevel,"Writing content: " + sequence);
	// ps.println(sequence); // no special formatting needed!
	writerSequences(bf, sequences);
        bf.close();
	//	assert (tmpInputFile.exists());
	//	fos.close();
	// assert (tmpInputFile.exists());
        FileReader fr = new FileReader(inputFileName);
        // System.out.println("file " + inputFileName  + " exists!");
        fr.close(); // 
	// ;;File tmpOutputFile = File.createTempFile("nanotiler_matchfold",".seq");
	// File tmpOutputFile = File.createTempFile("nanotiler_rnafold", ".sec");
	final String outputFileName = "nanotiler_rnafold_outtmp.sec"; // tmpOutputFile.getAbsolutePath();
//	if (tmpOutputFile.exists()) {
//	    tmpOutputFile.delete();
//	}
	// tmpInputFile.deleteOnExit();
	// generate command string
	// File tempFile = new File(scriptName);
	String[] commandWords = { scriptName, inputFileName, outputFileName}; 
	// generate command
	RunCommand command = new SimpleRunCommand(commandWords);
	log.fine("Issuing command: " + scriptName + " " + inputFileName + " " + outputFileName);
	// log.log(debugLogLevel, "Issuing command: " + scriptName + " " + inputFileName + " " + outputFileName);
	// create queue manager (singleton pattern)
	QueueManager queueManager = SimpleQueueManager.getInstance();
	// generate job
	Job job = queueManager.createJob(command);
	
	// add listener to job !?
	
	// launch command
	queueManager.submit(job);	    
	// log.log(debugLogLevel,"queue manager finished job!");
	// open output file:
	// log.log(debugLogLevel,"Importing optimized sequences from " + outputFileName);
	String[] resultLines = null;
	FileInputStream resultFile = null;
	try {
	    resultFile = new FileInputStream(outputFileName);
	    resultLines = StringTools.readAllLines(resultFile);
	    // sequenceStrings = RnaFoldTools.getSequenceStrings(resultLines);
	}
	catch (IOException ioe) {
	    log.warning("Error when scraping result file from: " 
			+ outputFileName + " : " + ioe.getMessage());
	    assert false;
	    throw ioe;
	}
	finally {
	    if (resultFile != null) {
		resultFile.close();
		// File file = new File(outputFileName);
		// file.delete(); // reactivate
	    }
	    //	    if (tmpInputFile != null) { // reactivate!
	    // 		tmpInputFile.delete();
	    // 	    }
	}
	if (resultLines == null) {
	    assert false;
	    log.warning("matchfold results were null!");
	}
	return resultLines;
    }

    InteractionSet parseMatchfold(String[] lines) throws ParseException, NumberFormatException {
	if(lines.length < 2) {
	    for (int i = 0; i < lines.length; ++i) {
		System.out.println(lines[i]);
	    }
	    throw new ParseException("Expected at least 2 lines in matchfold output!", 0);
	}
	InteractionSet result = new SimpleInteractionSet();
	String totSequence = lines[0];
	int numEntries = Integer.parseInt(lines[1]);
	// int pc = 2;
	for (int ii = 0; ii < numEntries; ++ii) {
	    int i = ii + 2; // offset from header
	    // System.out.println("Parsing line: " + lines[i]);
	    String line = lines[i].trim();
	    String[] words = line.split("\t");
	    if (!((words.length == 2) || (words.length == 3))) {
		throw new ParseException("Expected 2 or 3 words in base pair decription.", i);
	    }
	    int currId = 0;
	    int otherId = 0;
	    try {
		currId = Integer.parseInt(words[0]) - 1;
		otherId = Integer.parseInt(words[1]) - 1;
	    }
	    catch (NumberFormatException nfe) {
		throw new ParseException("Parsing (number format) error in line: " + i + " " + nfe.getMessage(), i);
	    }
	    if (currId == otherId) {
		throw new ParseException("Two identical indices detected.", i);
	    }
	    if ((currId < otherId) && (otherId >= 0)) {
		// find sequence id and offset within that sequence:
		int pc2 = 0;
		int seq1Id = 0;
		int seq1Pos = 0;
		int seq2Id = 0;
		int seq2Pos = 0; 
		boolean found = false;
		for (int j = 0; j < sequences.length; ++j) {
		    for (int k = 0; k < sequences[j].size(); ++k) {
			if (pc2 == currId) {
			    seq1Id = j;
			    seq1Pos = k;
			    found = true;
			    break;
			} else {
			    pc2++;
			}
		    }
		    if (found) {
			break;
		    }
		}
		found = false;
		int pc3 = 0;
		for (int j = 0; j < sequences.length; ++j) {
		    for (int k = 0; k < sequences[j].size(); ++k) {
			if (pc3 == otherId) {
			    seq2Id = j;
			    seq2Pos = k;
			    found = true;
			    break;
			}
			pc3++;
		    }
		    if (found) {
			break;
		    }
		}
		// System.out.println("Index result: " + seq1Id + " " + seq1Pos + " - " + seq2Id + " " + seq2Pos
		// + " from: " + currId + " " + otherId);
		Residue res1 = sequences[seq1Id].getResidue(seq1Pos);
		Residue res2 = sequences[seq2Id].getResidue(seq2Pos);
		assert(res1 != res2);
		RnaInteractionType interactionType = new RnaInteractionType(RnaInteractionType.WATSON_CRICK);
		result.add(new SimpleInteraction(res1, res2, interactionType));
		// System.out.println("Added " + seq1Id + " " + seq1Pos + " - " + seq2Id + " " + seq2Pos);
	    }
	}
	return result;
    }

    protected void runInternal() {
	String[] launchResult = null;
	try {
	    launchResult = launchFolding(sequences);
	    InteractionSet interactionSet = parseMatchfold(launchResult);	    
	    setResult(new SimpleSecondaryStructure(ali, interactionSet));
	    setValid(true);
	}
	catch (IOException ioe) {
	    setException(ioe);
	    setValid(false); // an error occurred, validate() will result in false
	}
	catch (ParseException pe) {
	    setException(pe);
	    setValid(false);
	}
	catch (NumberFormatException nfe) {
	    setException(nfe);
	    setValid(false);
	}
	if (!validate()) {
	    System.out.println("Non-valid MatchFoldScore for Weird matchfold output: ");
	    for (String line : launchResult) {
		System.out.println(line);
	    }
	    if (getException() != null) {
		System.out.println("Exception: " + getException().getMessage());
	    }
	}
	assert getResult() != null;
    }

}
