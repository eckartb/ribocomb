package rnasecondary;

import java.util.List;
import sequence.Alphabet;
import sequence.Residue;
import sequence.Sequence;
import sequence.UnevenAlignment;
import sequence.UnknownSequenceException;

public interface SecondaryStructure extends PackageConvention {

    /** in this mode, do not remove any interactions */
    public static final int NO_PURGE = 0;

    /** in this mode, remove all interactions that point to same base */
    public static final int PURGE_ALL_OVERLAPPING = 1;

    /** in this mode, remove all interactions that point to same base if they are not part of stem */
    public static final int PURGE_NONHELICAL_OVERLAPPING = 2;

    /** returns index of sequence to which residue belongs
     * TODO : very slow implementation ! 
     * TODO : contains BUG !!! */
    public int findSequence(Residue res);

    public int[][][][] generateInteractionMatrices();
   
    public List<Alphabet> getAlphabets();

    public void setAlphabets(List<Alphabet> alphabets);

    public List<Alphabet> generateDefaultAlphabets();

    /** Ensures that weight data structures are well-defined for all residues and sequences. Set default weights to DEFAULT_WEIGHT (typically 1.0) */
    public void generateDefaultWeights();

    public double getEnergy();

    public void setEnergy(double energy);

    public int getSequenceCount();

    public Sequence getSequence(int n);

    public Sequence getSequence(String name) throws UnknownSequenceException;

    public UnevenAlignment getSequences();

    public int getInteractionCount();
    
    /** returns n'th interaction (typically base pair interactions) */
    public Interaction getInteraction(int n);

    /** returns all interactions (typically base pair interactions) */
    public InteractionSet getInteractions();

    /** Returns zero-based indices that first residues of each sequence would have,
     * if all sequences were concatenated to one sequence. */
    public int[] getStarts();

    /** Return weights of n'th sequence */
    public List<Double> getWeights(int n);

    public boolean isActive(int seqId);

    public void setActive(int seqId);

    public void setInActive(int seqId);

    public void setInteractions(InteractionSet interactions);

    public void setSequences(UnevenAlignment sequences);

    /** Return weights of n'th sequence */
    public void setWeights(List<Double> _weights, int n);

    public String toString();

    /** remove overlapping interactions according to mode */
    public void purgeOverlappingInteractions(int purgeMode);

    /** Returns interaction matrix between sequences n and m. Entries of matrix element i,j correspond to RnaInteractionType
     * between residue i of sequence m and residue j of sequence n */
    public int[][] toInteractionMatrix(int m, int n);

    public boolean weightsSanityCheck();

}
