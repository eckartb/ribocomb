package rnasecondary;

import java.io.InputStream;
import java.util.logging.*;
import generaltools.MalformedInputException;
import sequence.Residue;
import sequence.Sequence;

public class SimpleInteraction implements Interaction {

    private InteractionType interactionType;
    private Residue residue1;
    private Residue residue2;
    private static Logger log = Logger.getLogger("NanoTiler_debug");


    /** default constructor is "package" accessible */
    public SimpleInteraction() { }

    public SimpleInteraction(Residue r1, Residue r2, 
			     InteractionType interactionType) {
	assert r1 != null;
	assert r2 != null;
	this.residue1 = r1;
	this.residue2 = r2;
	this.interactionType = interactionType;
    }

    /** TODO : residue1 and 2 are still shallow copies... */
    public Object clone() {
	SimpleInteraction interaction = new SimpleInteraction();
	interaction.interactionType = (InteractionType)(this.interactionType.clone());
	interaction.residue1 = this.residue1;
	interaction.residue2 = this.residue2;
	return interaction;
    }

    /** uses indices to generate "natural" order. Careful: different than zBuffer ordering generally used by Object3D/Positionable3D! */
    public int compareTo(Interaction other) {
	if (!residue1.isSameSequence(other.getResidue1())) {
	    log.finest("Careful, bad comparison in SimpleInteraction.compareTo");
	    return 0;
	}
	int p1 = residue1.getPos();
	int p2 = other.getResidue1().getPos();
	if (p1 < p2) {
	    return -1;
	}
	else if (p2 > p1) {
	    return 1;
	}
	int p1b = residue2.getPos();
	int p2b = other.getResidue2().getPos();
	if (p1b > p2b) { // note reverse ordering! RNA secondary structure that is...
	    return -1;
	}
	else if (p2b < p1b) {
	    return 1;
	}
	return 0; // must be equal
    }

    public String getClassName() { return "Interaction"; }

    public InteractionType getInteractionType() { return interactionType; }

    public Residue getResidue1() { return residue1; }

    public Residue getResidue2() { return residue2; }

    public Sequence getSequence1() { return (Sequence)(residue1.getParentObject()); }

    public Sequence getSequence2() { return (Sequence)(residue2.getParentObject()); }

    public boolean isIntraSequence() {
	return isValid() && getResidue1().isSameSequence(getResidue2());
    }

    /** returns true, if interactions have at least one residue in common */
    public boolean isOverlapping(Interaction other) {
	return ((getResidue1() == other.getResidue1())
	     || (getResidue1() == other.getResidue2())
	     || (getResidue2() == other.getResidue1())
	     || (getResidue2() == other.getResidue2()));
    }

    public boolean isValid() { 
	return (residue1 != null) && (residue2 != null) 
	    && (interactionType != null);
    }
				   
    public void read(InputStream is) throws MalformedInputException {
	PackageConventionTools.readHeader(is, getClassName());
	interactionType.read(is);
	residue1.read(is);
	residue2.read(is);
	PackageConventionTools.readFooter(is, getClassName());
    }

    public void setResidue1(Residue r) { this.residue1 = r; }

    public void setResidue2(Residue r) { this.residue2 = r; }

    public void setInteractionType(InteractionType intType) { 
	interactionType = intType; 
    }

    public String toString() { 
	String result = "(Interaction ";
	if (getInteractionType() != null) {
	    result = result + getInteractionType().toString() + " ";
	}
	else {
	    result = result + "invalidtype ";
	}
	if (residue1 != null) {
	    result = result + residue1.infoString() + " ";
	}
	else {
	    result = result + "invalidresidue1 ";
	}
	if (residue2 != null) {
	    result = result + residue2.infoString() + " ";
	}
	else {
	    result = result + "invalidresidue2 ";
	}
	result = result + " )";
	return result;
    }

}
