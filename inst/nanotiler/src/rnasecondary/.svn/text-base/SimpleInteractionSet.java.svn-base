package rnasecondary;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

import generaltools.MalformedInputException;

import static rnasecondary.PackageConstants.*;

/** stores a set of residue-residue interactions */
public class SimpleInteractionSet implements InteractionSet {

    private List<Interaction> list = new ArrayList<Interaction>();
    
    /** adds a single interaction */
    public void add(Interaction interaction) {
	list.add(interaction);
    }

    /** removes all interactions */
    public void clear() {
	list.clear();
    }

    public Object clone() {
	SimpleInteractionSet newSet = new SimpleInteractionSet();
	newSet.list = new ArrayList<Interaction>();
	for (int i = 0; i < size(); ++i) {
	    newSet.add((Interaction)(((Interaction)(list.get(i))).clone()));
	}
	return newSet;
    }

    /** gets n'th interaction */
    public Interaction get(int n) throws IndexOutOfBoundsException {
	return (Interaction)(list.get(n));
    }

    public String getClassName() { return "InteractionSet"; }

    /** returns number of interactions */
    public int size() { return list.size(); }

    /** sort, such that smallest index of strand one is first */
    public void sort() {
	Collections.sort(list); // sort by natural order so that smallest indices are first
    }

    public void read(InputStream is) throws MalformedInputException {
	PackageConventionTools.readHeader(is, getClassName());
	int numEntries = 0;
	// TODO
	// read number of entries
	for (int i = 0; i < numEntries; ++i) {
	    Interaction interaction = new SimpleInteraction();
	    interaction.read(is);
	    add(interaction);
	}
	PackageConventionTools.readFooter(is, getClassName());
    }

    /** removes interaction from set */
    public void remove(Interaction interaction) {
	list.remove(interaction);
    }

    public String toString() { 
	String result = "(InteractionSet " + size() + " ";
	for (int i = 0; i < size(); ++i) {
	    result = result + get(i) + ENDL;
	}
	result = result + ")";
	return result; 
    }

}
