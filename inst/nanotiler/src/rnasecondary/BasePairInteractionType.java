package rnasecondary;

import java.io.InputStream;

import generaltools.MalformedInputException;

/** this class describes the interaction between different RNA nucleotides */
public class BasePairInteractionType extends RnaInteractionType implements InteractionType, LWEdge {

    // public static final int NO_INTERACTION = -1;
    // public static final int UNKNOWN_SUBTYPE = 0;
    //     public static final int WATSON_CRICK = 1;
    // public static final int WOBBLE = 2;
    // public static final int NON_STANDARD = 3;
    // public static final int HOOGSTEEN = 4;
    // public static final int TERTIARY = 5;
    // public static final int BACKBONE = 4;
    
    // private static final String[] subTypeNames = {"U", "W", "H", "S", "B"}; // only works if ids of WATSON_CRICK is 1, HOOGSTEEN == 2 and SUGAR ==  3
    
    private static final String typeName = "BasePair";

    private int edge1;

    private int edge2;

    private boolean cis = true;

    private int subTypeId = RnaInteractionType.WATSON_CRICK; // TODO this should depend on case. Careful: ambiguous constant in LWEdge interface

    /** constructor: takes integer describing the subtype */
    public BasePairInteractionType(int _edge1, int _edge2, boolean _cis) {
	this.edge1 = _edge1;
	this.edge2 = _edge2;
	this.cis = _cis;
    }

    public Object clone() {
	return new BasePairInteractionType(edge1, edge2, cis);
    }
    
    public boolean equals(Object _other) {
	if (_other instanceof BasePairInteractionType) {
	    BasePairInteractionType other = (BasePairInteractionType)_other;
	    return edge1 == other.edge1 && edge2 == other.edge2 && cis == other.cis;
	}
	return false; // different types
    }

    public String getClassName() { return "BasePairInteractionType"; }

    public String getTypeName() { return typeName; }
    
    public int getEdge1() { return edge1; }

    public int getEdge2() { return edge2; }

    public boolean isCis() { return cis; }

    public String getSubTypeName() {
	return LWNomenclature.computeBasePairClassName(edge1, edge2, cis);
    }   

    public int getSubTypeId() { return subTypeId; }

     public void read(InputStream is) throws MalformedInputException {
	 assert false;
     }

    public void setSubTypeId(int n) { assert false; this.subTypeId = n; }

    public String toString() {
	String result = "(" + getTypeName() + " " + getSubTypeId() 
	    + " " + getSubTypeName() + " )"; 
	return result;
    }

}
