package rnasecondary;

import sequence.*;
import java.util.*;
import java.util.regex.*;
import java.io.*;
import java.text.ParseException;
import java.util.logging.*;
import numerictools.IntervalInt; //
import generaltools.StringTools;
import org.testng.annotations.*;
import java.util.ResourceBundle;

/** parses structure string in bracket notation, adds found "(" and ")" parenthesis to interaction set */
public class CTFileParser implements SecondaryStructureParser {

    final private int SEQUENCE_COLUMN = 1; //places to find values
    final private int INTERACTION_COLUMN = 4;
    final private int WIDTH = 6;
    final private String NUMBER = ".*\\d+.*"; //checks to see if string matches number
    final private String SPACE = "\\s+"; //representation of n number of spaces
    private String structureSequence = "";
    private String sequenceIndexString = ""; //used to get the sequence index of a residue
    private int[] starts = new int[1]; //default 
    //from its position within the total sequence
    
    private boolean fastaMode = false; // if true, parse in pseudo FASTA format (name, sequence, secondary structure in 3 lines)
    public Level debugLogLevel = Level.INFO;
    private Logger log = Logger.getLogger("mcsopt");

    public CTFileParser() {
	     log.finest("starting CTFileParser");
    }
    
    public CTFileParser(boolean fastaMode) {
	     log.finest("starting CTFileParser");
	      this.fastaMode = fastaMode;
    }
    
    
    private int getSequenceIndex (int pos) { //returns which sequence residue is part of
      return Character.getNumericValue(sequenceIndexString.charAt(pos)) - 1;
    }
    
    // private int getSequenceIndex (int id) { //returns index of sub sequence given residue in entire sequence
    //   for (int t = 0; t < starts.length - 1; t++) {
    //     if ((starts [t] <= id) && (id < starts [t + 1])) {
    //       return t;
    //     } 
    //   } 
    //   if (id >= starts [starts.length - 1]) {
    //     return starts.length - 1;
    //   } 
    //   System.out.println ("Error, sequence id not in sequence");
    //   return 0;
    // }

    // private int getInternalId (UnevenAlignment seqs, int sequenceIndex, int id) { //gets internal index given absolute index and sequence number
    //   int internalId = id;
    //   Sequence s; 
    //   for (int r = 0; r < sequenceIndex; r++) {
    //     s = seqs.getSequence (r);
    //     internalId = id - s.size ();
    //   }
    //   return internalId; //gets index of residue in subsequence
    // }
    
    private int getInternalId (int sequenceIndex, int id) { //gets internal index given absolute index and sequence number
      return id - starts[sequenceIndex];
    }
    
    private String[] cleanFile (String [] l) {
      List<String> lines = new ArrayList<String>();
      for (int k = 0; k < l.length; k++) {
        if (!l[k].isEmpty()) {
          String [] tmp;
          tmp = l[k].trim().split(SPACE);
          if (tmp.length == WIDTH) { //line is part of the data table
            lines.add(l[k].trim().toUpperCase());
          } else { //check to see if header or comment
            boolean isHeader = true;
            for (int i = 0; i < tmp.length; i++) {
              if (!tmp[i].matches(NUMBER)) {
                isHeader = false;
                break;
              }
            }
            if (isHeader) {
              //gets information for number of sequences and starting indexes for those sequences
              String[] header = l[k].split(SPACE);
              int strandCount = 1; //default case
              starts[0] = 0; // simplest case of only one sequence, sequence starts at residue 1, index 0
              if (header.length > 1 && header[1].matches(NUMBER)) {
                  strandCount = Integer.parseInt(header[1]);
                  starts = new int[strandCount];
                  for (int m = 0; m < strandCount; m++) {
                     starts[m] = Integer.parseInt(header[m+2])-1; //parses strand starting indexs
                  }
              }
            }
          }
        }
      }
    	return lines.toArray(new String [lines.size()]);
    }
    
    /** Parses secondary structure from given file name. */
    public MutableSecondaryStructure parse(String filename) throws IOException, ParseException {
    	FileInputStream f = new FileInputStream(filename);
    	String [] l = StringTools.readAllLines(f); //all lines in files
    	return parse(this.cleanFile(l));
    }  
    
    /** Generates secondary structure from set of lines */
    public MutableSecondaryStructure parse(String[] l) throws ParseException {
      try {
          String[] lines = this.cleanFile(l);
    	    UnevenAlignment sequences = new SimpleUnevenAlignment();
    	    InteractionSet interactions = new SimpleInteractionSet();
    	    String prefix = "Number ";
          log.log(debugLogLevel, "Parsing input");
          
          // for (int r = 0; r < lines.length; r++) {
          //   System.out.println (lines [r]); //TODO remove
          // }
          
          for (int k = 0; k < lines.length; k++) { //reads all the lines in table, constucts sequence
      			String [] temp;
      		  temp = lines[k].trim().split (SPACE); //splits at all spaces
      			if (temp.length == WIDTH) {
              this.structureSequence = this.structureSequence + temp[SEQUENCE_COLUMN];
            }
      	  }
            
          
          //construct the sequenceIndexString for future use
          int c = 0; //current strand
          for (int j = 0; j < structureSequence.length(); j++) {
            if (c < (starts.length) && (j == starts[c])) {
              c++; 
            }
            sequenceIndexString = sequenceIndexString + c;
          }
        
          //construct sequence objects
          for (int i = 0; i < starts.length; ++i) {
            int lastPos = structureSequence.length();
            if (i + 1 < starts.length) { //unless last sequence, lastPos is equal sequence start index + 1
              lastPos = starts[i+1]; 
            }
            
            String subseq = structureSequence.substring(starts[i], lastPos).toUpperCase();
            Sequence sequence = new SimpleMutableSequence(subseq, prefix + (i+1), DnaTools.AMBIGUOUS_RNA_ALPHABET);
            sequences.addSequence(sequence);
          }
          
          System.out.println (structureSequence);
          System.out.println (sequenceIndexString);
        
          //interactions
          InteractionType interactionType = new RnaInteractionType(RnaInteractionType.WATSON_CRICK);
          for (int k = 0; k < lines.length; k++) { 
            String [] temp1;
            temp1 = lines[k].split (SPACE);
            if (temp1.length == WIDTH) {
              int indexResidue1 = (k); //index of residue
              int indexResidue2 = Integer.parseInt (temp1[INTERACTION_COLUMN]) - 1;
              if (indexResidue1 > indexResidue2) { //prevents redundant pairing
                continue;
              }
              // int sequenceIdRes1 = getSid (starts, indexResidue1); 
              // int sequenceIdRes2 = getSid (starts, indexResidue2); 
              // System.out.println (indexResidue1);
              // System.out.println (indexResidue2);
              int sequenceIdRes1 = getSequenceIndex (indexResidue1); 
              int sequenceIdRes2 = getSequenceIndex (indexResidue2); 
              // System.out.println (sequenceIdRes1);
              // System.out.println (sequenceIdRes2);
              Sequence seq1 = sequences.getSequence(sequenceIdRes1);
              Sequence seq2 = sequences.getSequence(sequenceIdRes2);
              // int id1B = getInternalId (sequences, sequenceIdRes1, indexResidue1); 
              // int id2B = getInternalId (sequences, sequenceIdRes2, indexResidue2);
              int id1B = getInternalId (sequenceIdRes1, indexResidue1); 
              int id2B = getInternalId (sequenceIdRes2, indexResidue2);
              // System.out.println (id1B);
              // System.out.println (id2B);
              interactions.add(new SimpleInteraction(seq1.getResidue(id1B), seq2.getResidue(id2B), interactionType)); //creates both interstrand and
              //intra strand interactions
              System.out.println (new SimpleInteraction(seq1.getResidue(id1B), seq2.getResidue(id2B), interactionType));
            } 
          }
          
          return new SimpleMutableSecondaryStructure(sequences, interactions);
      
      } catch (DuplicateNameException e1) {
      	  System.out.println("DuplicateNameException: " + e1.getMessage());
      } catch (UnknownSymbolException e2) {
    	    System.out.println("UnknownSymbolException: " + e2.getMessage());
    	}
    	return null;
      }
    
    
    
    @Test(groups={"slow"})
    public void testParse() {
    	ResourceBundle rb = ResourceBundle.getBundle("SecondaryStructureDesignTest");
    	String fileNames = rb.getString("testSecondaryStructures");
    	String[] tokens = fileNames.split(",");
    	SecondaryStructureParser parser = new CTFileParser();
    	for (int i = 0; i < tokens.length; ++i) {
    	    MutableSecondaryStructure structure = null;
    	    String fileName = tokens[i];
    	    if (fileName.length() < 2) {
    		continue;
    	    } else {
    		System.out.println("Reading file: " + fileName);
    	    }
    	    try {
    		structure = parser.parse(tokens[i]);
    	    } catch (IOException ioe) {
    		System.out.println(ioe.getMessage());
    		assert false;
    	    } catch (ParseException pe) {
    		System.out.println(pe.getMessage());
    		assert false;
    	    }
    	    assert structure != null;
    	    SecondaryStructureWriter writer = new SecondaryStructureScriptFormatWriter();
    	    System.out.println("Read secondary structure: " + writer.writeString(structure));
	  }
  }
}

