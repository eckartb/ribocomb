package rnasecondary;

import java.io.*;
import java.util.*;
import generaltools.*;
import sequence.*;
import java.util.logging.*;
import launchtools.*;

import org.testng.annotations.*;
import static rnasecondary.PackageConstants.*;

/** Computes secondary structure by launching hxmatch. */
public class HXMatchStructurePredictor extends ResultWorker {
    // private String LOGFILE_DEFAULT = "NanoTiler_debug";

    public static final int CLUSTAL_WIDTH = 60;

    private Logger log = Logger.getLogger(LOGFILE_DEFAULT);
    // private static ResourceBundle rb = ResourceBundle.getBundle(BUNDLE_NAME);
    private String scriptName = "hxmatch.sh";
    private UnevenAlignment ali;
    private double gcEnergy = -1.0;
    private double auEnergy = -0.8;
    private Alphabet alphabet = DnaTools.AMBIGUOUS_RNA_ALPHABET;
    private Sequence[] sequences;
    private int stemLengthMin = 3;
    private int stopOffset = 3;
    private Level debugLogLevel = Level.FINE;

    public HXMatchStructurePredictor(UnevenAlignment ali) { 
	assert ali != null;
	this.ali = ali;
	this.sequences = getSequences(ali);
    }
    
    private Sequence[] getSequences(UnevenAlignment ali) {
	Sequence[] sequences = new Sequence[ali.getSequenceCount()];
	for (int i = 0; i < ali.getSequenceCount(); ++i) {
	    sequences[i] = ali.getSequence(i);
	}
	return sequences;
    }

    private InteractionSet readInteractions(InputStream is) {
	assert(false);
	return null;
    }

    /** writes alignment in CLUSTAL ALN format */
    private void writeCLUSTAL(OutputStream os, String[] sequences, String[] names) {
	assert(sequences.length > 0);
	if (names == null) {
	    names = new String[sequences.length];
	    for (int i = 0; i < names.length; ++i) {
		names[i] = "s" + (i+1);
	    }
	}
	int frontCount = 16; // number of characters of header
	PrintStream ps = new PrintStream(os);
	ps.println("CLUSTAL");
	int colCount = 0;
	int length = sequences[0].length();
	while (colCount < length) {
	    int diff = length - colCount;
	    int sLen = diff;
	    if (sLen > CLUSTAL_WIDTH) {
		sLen = CLUSTAL_WIDTH;
	    }
	    assert(colCount + sLen <= length);
	    for (int i = 0; i < sequences.length; ++i) {
		String s = sequences[i];
		assert(s.length() == length); // all sequences must have same length
		s = s.substring(colCount, colCount + sLen); 
		String name = names[i];
		if (name.length() > frontCount) {
		    name = name.substring(0, frontCount);
		}
		ps.print(name);
		int dChar = frontCount - name.length();
		for (int j = 0; j < dChar; ++j) {
		    ps.print(" ");
		}
		ps.println(s + " " + (colCount + sLen));
	    }
	    for (int j = 0; j < frontCount; ++j) {
		ps.print(" ");
	    }
	    for (int i = 0; i < sLen; ++i) {
		ps.print(".");
	    }
	    ps.println();
	    ps.println();
	    colCount += CLUSTAL_WIDTH;
	}
    }

    static String paste(Sequence[] sequences) {
	StringBuffer buf = new StringBuffer();
	for (int i = 0; i < sequences.length; ++i) {
	    buf.append(sequences[i].toString());
	}
	return buf.toString();
    }

    /** launches RNAcofold or pknotsRG with given sequence, returns the raw line output  */
    String[] launchFolding(Sequence[] sequences) throws IOException {
	// write secondary structure to temporary file
	System.out.println("Launching folding for folling sequences:");
	for (Sequence seq : sequences) {
	    System.out.println(seq.toString());
	}
	File tmpInputFile = File.createTempFile("nanotiler_hxmatch",".clustal");
	// tmpInputFile.deleteOnExit();
	String inputFileName = tmpInputFile.getAbsolutePath();
	FileOutputStream fos = new FileOutputStream(tmpInputFile);
	PrintStream ps = new PrintStream(fos);
	String sequence = paste(sequences);
	// write secondary structure, but only write sec structure, not sequence:
	log.log(debugLogLevel,"Writing to file: " + inputFileName);
	log.log(debugLogLevel,"Writing content: " + sequence);
	StringBuffer seqBuf = new StringBuffer();
	for (int i = 0; i < sequences.length; ++i) {
	    // seqs[i] = sequences[i].toString();
	    seqBuf.append(sequences[i].toString());
	}
	String[] seqs = new String[2];
	seqs[0] = seqBuf.toString();
	seqs[1] = seqBuf.toString();
	String[] names = null;
	writeCLUSTAL(fos, seqs, names); // writes sequences in CLUSTAL format
	fos.close();
	File tmpOutputFile = File.createTempFile("nanotiler_hxmatch", ".sec");
	// tmpOutputFile.deleteOnExit();
	final String outputFileName = tmpOutputFile.getAbsolutePath();
	if (tmpOutputFile.exists()) {
	    tmpOutputFile.delete();
	}

	File tempFile = new File(scriptName);
	String[] commandWords = { scriptName, inputFileName, outputFileName}; 
	// generate command
	RunCommand command = new SimpleRunCommand(commandWords);
	log.log(debugLogLevel, "Issuing command: " + scriptName + " " + inputFileName + " " + outputFileName);
	// create queue manager (singleton pattern)
	QueueManager queueManager = SimpleQueueManager.getInstance();
	// generate job
	Job job = queueManager.createJob(command);
	
	// add listener to job !?
	
	// launch command
	queueManager.submit(job);	    
	log.log(debugLogLevel,"queue manager finished job!");
	// open output file:
	log.log(debugLogLevel,"Importing optimized sequences from " + outputFileName);
	String[] resultLines = null;
	FileInputStream resultFile = null;
	try {
	    resultFile = new FileInputStream(outputFileName);
	    resultLines = StringTools.readAllLines(resultFile);
	    // sequenceStrings = RnaFoldTools.getSequenceStrings(resultLines);
	}
	catch (IOException ioe) {
	    log.warning("Error when scraping result file from: " 
			+ outputFileName);
	    assert false;
	    throw ioe;
	}
	finally {
	    if (resultFile != null) {
		resultFile.close();
		File file = new File(outputFileName);
		file.delete();
	    }
	    if (tmpInputFile != null) {
		tmpInputFile.delete();
	    }
	}
	if (resultLines != null) {
	    log.log(debugLogLevel,"Results for RNAcofold:");
	    for (int i = 0; i < resultLines.length; i++) {
		log.fine(resultLines[i]);
	    }
	}
	else {
	    assert false;
	    log.warning("Rnacofold results were null!");
	}
	System.out.println("Using tmp files: " + tmpInputFile + " " + tmpOutputFile);
	System.out.println("Result from hxmatch:");
	for (int i = 0; i < resultLines.length; ++i) {
	    System.out.println(resultLines[i]);
	}
	return resultLines;
    }

    /** Parses hxmatch result and generates an equivalent secondary structure */
    int[] parseHXMatchLine(String line) throws NumberFormatException {
	String line2 = line.trim();
	String[] words = line2.split(" ");
	int pc = 0;
	int n = 0;
	int m = 0;
	for (int i = 0; i < words.length; ++i) {
	    if (words[i].length() > 0) {
		if (pc == 0) {
		    n = Integer.parseInt(words[i]) - 1; // convert to internal counting
		    pc++;
		} else if (pc == 1) {
		    m = Integer.parseInt(words[i]) - 1;
		    pc++;
		} else {
		    break;
		}
	    }
	}
	int[] result = new int[2];
	result[0] = n;
	result[1] = m;
	return result;
    }

    /** Returns sequence index corresponding to index of concatenated sequence. Index has already been converted to internal counting */
    int[] findSequenceIndex(int pastedIndex) {
	int sum = 0;
	for (int i = 0; i < sequences.length; ++i) {
	    assert(pastedIndex >= sum);
	    if (pastedIndex < (sum + sequences[i].size())) {
		int[] result = new int[2];
		result[0] = i; // index of sequence
		result[1] = pastedIndex - sum; // position in this sequence
		assert(result[1] < sequences[result[0]].size());
		return result;
	    }
	    sum += sequences[i].size();
	}
	assert(false); // should never be here
	return null;
    }

    /** Parses hxmatch result and generates an equivalent secondary structure */
    InteractionSet parseHXMatch(String[] resultLines) throws NumberFormatException  {
	InteractionSet result = new SimpleInteractionSet();
	// System.out.println("parsing hxmatch result...");
	for (int i = 2; i < resultLines.length; ++i) {  // ignore first two lines
	    System.out.println(resultLines[i]);
	    int[] lineResult = parseHXMatchLine(resultLines[i]);
	    assert(lineResult.length == 2);
	    int[] ids1 = findSequenceIndex(lineResult[0]);
	    int[] ids2 = findSequenceIndex(lineResult[1]);
	    System.out.println("Parsing result: " + lineResult[0] + " " + lineResult[1] + " " 
			       + ids1[0] + " " + ids1[1] + " " + ids2[0] + " " + ids2[1]);
	    Residue res1 = sequences[ids1[0]].getResidue(ids1[1]);
	    Residue res2 = sequences[ids2[0]].getResidue(ids2[1]);
	    if(RnaSecondaryTools.isWatsonCrick(res1.getSymbol().getCharacter(),
					       res2.getSymbol().getCharacter())) { // otherwise there is something seriously wrong...
		InteractionType interactionType =  new RnaInteractionType(RnaInteractionType.WATSON_CRICK);
		result.add(new SimpleInteraction(res1, res2, interactionType));
	    } else {
		System.out.println("Warning: ignoring predicted non-Watson-Crick pair: "
				   + res1.getSymbol() + " " + ids1[1]+1 + " " + res2.getSymbol() + " " + ids2[1]);
		StringTools.printStride(System.out, sequences[ids1[0]].sequenceString(), 10);
		System.out.println("");
		StringTools.printStride(System.out, sequences[ids2[0]].sequenceString(), 10);
		System.out.println("");
	    }
	}
	return result;
    }

    protected void runInternal() {
	try {

	    String[] resultLines = launchFolding(sequences); // launch and parse hxmatch

	    InteractionSet interactionSet = parseHXMatch(resultLines); // fetch results
	    setResult(new SimpleSecondaryStructure(ali, interactionSet));
	}
	catch (IOException ioe) {
	    System.out.println("IO exception in HXMatchStructurePredictor: " + ioe.getMessage());
	    setResult(null);
	}
	catch (NumberFormatException nfe) {
	    System.out.println("Number format exception in HXMatchStructurePredictor: " + nfe.getMessage());
	    setResult(null);
	}
    }

}
