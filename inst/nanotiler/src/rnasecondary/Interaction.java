package rnasecondary;

import sequence.Residue;
import sequence.Sequence;

/** describes interaction between two residues. These residues do not 
 * have to be part of the same sequence. The class InteractionType 
 * describes what kind of interaction it is (like RnaInteraction with 
 * its subtypes Watson-Crick, Hoogsteen etc).
 */
public interface Interaction extends PackageConvention, Comparable<Interaction>  {

    public Object clone();

    public InteractionType getInteractionType();
    
    public void setResidue1(Residue r);

    public void setResidue2(Residue r);

    public void setInteractionType(InteractionType intType);

    public Residue getResidue1();
    
    public Residue getResidue2();

    public Sequence getSequence1(); // returns sequence object to which residue1 belongs

    public Sequence getSequence2(); // returns sequence object to which residue2 belongs
    
    /** returns true if both residues belong to same sequence */
    public boolean isIntraSequence();

    /** returns true, if interactions have at least one residue in common */
    public boolean isOverlapping(Interaction other);
    
    /** returns true if residues and interaction type are defined */
    public boolean isValid();
    
    public String toString();
}
