package rnasecondary;

/** stores a set of residue-residue interactions */
public interface InteractionSet extends PackageConvention {

    /** adds a single interaction */
    public void add(Interaction interaction);

    /** removes all interactions */
    public void clear();

    /** deep clone */
    public Object clone();

    /** gets n'th interaction */
    public Interaction get(int n);

    /** removes interaction from set */
    public void remove(Interaction interaction);

    /** returns number of interactions */
    public int size();

    /** sort, such that smallest index of strand one is first */
    public void sort();

}
