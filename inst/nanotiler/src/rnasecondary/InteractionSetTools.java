package rnasecondary;

import sequence.*;

public class InteractionSetTools {

    public static boolean isInteracting(Residue res, InteractionSet set) {
	for (int i = 0; i < set.size(); ++i) {
	    Interaction inter = set.get(i);
	    if ((inter.getResidue1() == res) || (inter.getResidue2() == res)) {
		return true;
	    }
	}
	return false;
    }

}