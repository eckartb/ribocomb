package rnasecondary;

public interface InteractionType extends PackageConvention {

    public Object clone();

    public String getTypeName();

    public String getSubTypeName();

    public int getSubTypeId();

    public void setSubTypeId(int n);

}
