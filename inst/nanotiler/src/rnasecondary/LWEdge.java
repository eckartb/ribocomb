package rnasecondary;

/** Represents the 3 possibilities of Watson-Crick Edge, Hoogsteen edge or sugar edge */
public interface LWEdge {

    public static final int WATSON_CRICK = 1;

    public static final int HOOGSTEEN = 2;

    public static final int SUGAR = 3;

    public static final String WATSON_CRICK_STRING = "W";

    public static final String HOOGSTEENSTRING = "H";
 
    public static final String SUGAR_STRING = "S";

}