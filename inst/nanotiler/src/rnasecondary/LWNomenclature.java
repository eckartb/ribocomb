package rnasecondary;


public class LWNomenclature {

    public static String computeCisTransName(boolean cis) {
	if (cis) {
	    return "c";
	}
	return "t";
    }

    public static String computeEdgeName(int edge) {
	switch (edge) {
	case LWEdge.WATSON_CRICK: return "W";
	case LWEdge.HOOGSTEEN: return "H";
	case LWEdge.SUGAR: return "S";
	    // case LWEdge.BACKBONE: return "B"; // TODO : add in future
	}
	return "U";
    }

    /** Computes names like cWW for cis-Watson-Crick/Watson-Crick interactinos */
    public static String computeBasePairClassName(int edge1, int edge2, boolean cis) {
	return computeCisTransName(cis) + computeEdgeName(edge1) + computeEdgeName(edge2);
    }

    /** Computes names like cWW for cis-Watson-Crick/Watson-Crick interactinos */
    public static String computeBasePairClassName(BasePairInteractionType bp) {
	return computeBasePairClassName(bp.getEdge1(), bp.getEdge2(), bp.isCis());
    }

}