package rnasecondary;

import java.io.*;
import java.text.ParseException;
import java.util.*;
import java.util.logging.*;
import generaltools.*;
import sequence.*;
import launchtools.*;
import org.testng.annotations.*;

import static rnasecondary.PackageConstants.*;

/** Computes secondary structure by simply setting lowest energy stems first. */
public class MatchFoldAllSecondaryStructurePredictor extends ResultWorker {
    public static final double YIELD_MAX = 1.0; // either 100 for percent or 1.0 for fraction of optimal yield
    private Logger log = Logger.getLogger(LOGFILE_DEFAULT);
    private Level debugLogLevel = Level.FINE;
    private static String scriptName = "matchconc.pl"; // rb.getString("matchfoldscript");

    private UnevenAlignment ali;
    private double gcEnergy = -1.0;
    private double auEnergy = -0.8;
    private Alphabet alphabet = DnaTools.AMBIGUOUS_RNA_ALPHABET;
    private Sequence[] sequences;
    private int stemLengthMin = 3;
    private int stopOffset = 3;
    private double yield = 1.0; // stores yield determined by matchconc.pl script 
    // counts how many times "launch" was issued
    private static int launchCount = 0; 
    private static final int LAUNCH_MESSAGE_INTERVAL = 1000;
    public MatchFoldAllSecondaryStructurePredictor(UnevenAlignment ali) { 
	assert ali != null;
	this.ali = ali;
	this.sequences = getSequences(ali);
    }
    
    private Sequence[] getSequences(UnevenAlignment ali) {
	Sequence[] sequences = new Sequence[ali.getSequenceCount()];
	for (int i = 0; i < ali.getSequenceCount(); ++i) {
	    sequences[i] = ali.getSequence(i);
	}
	return sequences;
    }

    /** launches RNAcofold or pknotsRG with given sequence, returns the raw line output  */
    private void writeSequences(OutputStream os, Sequence[] sequences) throws IOException {
	PrintStream ps = new PrintStream(os);
	for (int i = 0; i < sequences.length; ++i) {
	    ps.println(">s" + (i+1));
	    ps.println(sequences[i].sequenceString());
	}
    }

    /** launches RNAcofold or pknotsRG with given sequence, returns the raw line output  */
    private String[] launchFolding(Sequence[] sequences) throws IOException {
	++launchCount;
	// write secondary structure to temporary file
	File tmpInputFile = File.createTempFile("nanotiler_matchold",".seq");
	tmpInputFile.deleteOnExit();
	String inputFileName = tmpInputFile.getAbsolutePath();
	FileOutputStream fos = new FileOutputStream(tmpInputFile);
	// write secondary structure, but only write sec structure, not sequence:
	// log.log(debugLogLevel,"Writing to file: " + inputFileName);
	// log.log(debugLogLevel,"Writing content: " + sequence);
	// ps.println(sequence); // no special formatting needed!
	writeSequences(fos, sequences);
	fos.close();
	File tmpOutputFile = File.createTempFile("nanotiler_rnafold", ".sec");
	final String outputFileName = tmpOutputFile.getAbsolutePath();
	if (tmpOutputFile.exists()) {
	    tmpOutputFile.delete();
	}
	tmpInputFile.deleteOnExit();
	// generate command string
	File tempFile = new File(scriptName);
	String[] commandWords = { scriptName, inputFileName, outputFileName}; 
	// generate command
	RunCommand command = new SimpleRunCommand(commandWords);
	if ((launchCount % LAUNCH_MESSAGE_INTERVAL) == 1) {
	    System.out.println("Issuing command: " + scriptName + " " + inputFileName + " " + outputFileName);
	}
	// log.log(debugLogLevel, "Issuing command: " + scriptName + " " + inputFileName + " " + outputFileName);
	// create queue manager (singleton pattern)
	QueueManager queueManager = SimpleQueueManager.getInstance();
	// generate job
	Job job = queueManager.createJob(command);
	
	// add listener to job !?
	
	// launch command
	queueManager.submit(job);	    
	// log.log(debugLogLevel,"queue manager finished job!");
	// open output file:
	// log.log(debugLogLevel,"Importing optimized sequences from " + outputFileName);
	String[] resultLines = null;
	String[] resultLines2 = null;
	FileInputStream resultFile = null;
	FileInputStream resultFile2 = null;
	try {
	    resultFile = new FileInputStream(outputFileName + ".ct");
	    resultFile2 = new FileInputStream(outputFileName + ".out");
	    resultLines = StringTools.readAllLines(resultFile);
	    resultLines2 = StringTools.readAllLines(resultFile2);
	    this.yield = parseYield(resultLines2);
	    // sequenceStrings = RnaFoldTools.getSequenceStrings(resultLines);
	}
	catch (IOException ioe) {
	    log.warning("IO Error when scraping result file: " + ioe.getMessage());
	    assert false;
	    throw ioe;
	}
	catch (NumberFormatException nfe) {
	    log.warning("Could not scrape yield from matchconc.pl result: " + nfe.getMessage());
	    assert false;
	}
	finally {
	    if (resultFile != null) {
		resultFile.close();
		File file = new File(outputFileName + ".ct");
		file.delete();
	    }
	    if (resultFile2 != null) {
		resultFile2.close();
		File file2 = new File(outputFileName + ".out");
		file2.delete();
	    }
	    if (tmpInputFile != null) {
		tmpInputFile.delete();
	    }
	}
	if (resultLines == null) {
	    assert false;
	    log.warning("matchfold results were null!");
	}
	return resultLines;
    }

    /** Parses yield generated by matchconc.pl script and matchfold / concentration programs */
    double parseYield(String[] lines) throws NumberFormatException {
	assert(lines.length > 0);
	double result = 0.0;
	for (int i = 0; i < lines.length; ++i) {
	    if (lines[i].startsWith("# Start concentration")) {
		String[] words = lines[i].split(" ");
		assert(words.length == 10);
		result = Double.parseDouble(words[9]);
	    }
	}
	return result;
    }

    InteractionSet parseMatchfold(String[] lines) throws ParseException, NumberFormatException {
	if(lines.length < 2) {
	    for (int i = 0; i < lines.length; ++i) {
		System.out.println(lines[i]);
	    }
	    throw new ParseException("Expected at least 2 lines in matchfold output!", 0);
	}
	InteractionSet result = new SimpleInteractionSet();
	String totSequence = lines[0];
	int numEntries = Integer.parseInt(lines[1]);
	// int pc = 2;
	for (int ii = 0; ii < numEntries; ++ii) {
	    int i = ii + 2; // offset from header
	    // System.out.println("Parsing line: " + lines[i]);
	    String line = lines[i].trim();
	    String[] words = line.split("\t");
	    if (!((words.length == 2) || (words.length == 3))) {
		throw new ParseException("Expected 2 or 3 words in base pair decription.", i);
	    }
	    int currId = 0;
	    int otherId = 0;
	    try {
		currId = Integer.parseInt(words[0]) - 1;
		otherId = Integer.parseInt(words[1]) - 1;
	    }
	    catch (NumberFormatException nfe) {
		throw new ParseException("Parsing (number format) error in line: " + i + " " + nfe.getMessage(), i);
	    }
	    if (currId == otherId) {
		throw new ParseException("Two identical indices detected.", i);
	    }
	    if ((currId < otherId) && (otherId >= 0)) {
		// find sequence id and offset within that sequence:
		int pc2 = 0;
		int seq1Id = 0;
		int seq1Pos = 0;
		int seq2Id = 0;
		int seq2Pos = 0; 
		boolean found = false;
		for (int j = 0; j < sequences.length; ++j) {
		    for (int k = 0; k < sequences[j].size(); ++k) {
			if (pc2 == currId) {
			    seq1Id = j;
			    seq1Pos = k;
			    found = true;
			    break;
			} else {
			    pc2++;
			}
		    }
		    if (found) {
			break;
		    }
		}
		found = false;
		int pc3 = 0;
		for (int j = 0; j < sequences.length; ++j) {
		    for (int k = 0; k < sequences[j].size(); ++k) {
			if (pc3 == otherId) {
			    seq2Id = j;
			    seq2Pos = k;
			    found = true;
			    break;
			}
			pc3++;
		    }
		    if (found) {
			break;
		    }
		}
		// System.out.println("Index result: " + seq1Id + " " + seq1Pos + " - " + seq2Id + " " + seq2Pos
		// + " from: " + currId + " " + otherId);
		Residue res1 = sequences[seq1Id].getResidue(seq1Pos);
		Residue res2 = sequences[seq2Id].getResidue(seq2Pos);
		assert(res1 != res2);
		RnaInteractionType interactionType = new RnaInteractionType(RnaInteractionType.WATSON_CRICK);
		result.add(new SimpleInteraction(res1, res2, interactionType));
		// System.out.println("Added " + seq1Id + " " + seq1Pos + " - " + seq2Id + " " + seq2Pos);
	    }
	}
	return result;
    }

    protected void runInternal() {
	String[] launchResult = null;
	try {
	    launchResult = launchFolding(sequences);
	    InteractionSet interactionSet = parseMatchfold(launchResult);	    
	    SimpleSecondaryStructure newResult = new SimpleSecondaryStructure(ali, interactionSet);
	    newResult.setEnergy(YIELD_MAX -this.yield);
	    setResult(newResult);
	    setValid(true);
	}
	catch (IOException ioe) {
	    setException(ioe);
	    setValid(false); // an error occurred, validate() will result in false
	}
	catch (ParseException pe) {
	    setException(pe);
	    setValid(false);
	}
	catch (NumberFormatException nfe) {
	    setException(nfe);
	    setValid(false);
	}
	if (!validate()) {
	    System.out.println("Non-valid MatchFoldScore for Weird matchfold output: ");
	    for (String line : launchResult) {
		System.out.println(line);
	    }
	    if (getException() != null) {
		System.out.println("Excepion: " + getException().getMessage());
	    }
	}
    }

}
