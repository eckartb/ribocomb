package rnasecondary;

import controltools.ModelChanger;
import sequence.Alphabet;
import sequence.DuplicateNameException;
import sequence.Sequence;

/**
 * A MutableSecondaryStructure supports the
 * addition and removal of both single sequences and
 * single interactions.
 * @author Calvin Grunewald
 */
public interface MutableSecondaryStructure extends SecondaryStructure, ModelChanger {

    /**
     * Add a sequence to this secondary structure
     * @param s Sequence to add
     */
    public void addSequence(Sequence s) throws DuplicateNameException;

    /**
     * Add an interaction to this secondary structure
     * @param i Interaction to add
     */
    public void addInteraction(Interaction i);


    /**
     * Remove an interaction from this secondary structure.
     * Note that an interaction <code>interaction</code> will
     * only be removed if <code>interaction.equals(i)</code>.
     * @param i Removes an interaction matching this interaction
     * @return Returns the removed interaction.
     */
    public void removeInteraction(Interaction i);

    /**
     * Add a new strand to the secondary structure
     * @param name Name of the strand
     * @param sequence The sequence
     * @param a The alphabet of the stand
     */
    public void addStrand(String name, String sequence, Alphabet a);


    /**
     * Add a new stem to the secondary structure
     * @param seq1 Sequence containing the beginning of the stem
     * @param seq2 Sequence containing the end of the stem
     * @param start Index of start of stem on seq1
     * @param stop Index of stop of stem on seq2
     * @param length Length of the stem
     */
    /*
      public void addStem(int seq1, int seq2, int start, int stop, int length);
    */

    /**
     * Remove all sequences and interactions from this secondary
     * structure.
     */
    public void clear();


}
