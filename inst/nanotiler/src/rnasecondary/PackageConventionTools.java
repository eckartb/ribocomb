package rnasecondary;

import java.io.InputStream;

import generaltools.MalformedInputException;

/** provides methods that help with reading and writing according
 * to PackageConvention 
 */
public class PackageConventionTools {

    public static void readHeader(InputStream is,
       			  String className) throws MalformedInputException {
	
	// TODO
    }

    public static void readFooter(InputStream is,
			  String className) throws MalformedInputException {

	// TODO
    }
    
    
    public static String headerString(String className) {
	return "(" + className;
    }

    public static String footerString(String className) {
	return ")";
    }

}
