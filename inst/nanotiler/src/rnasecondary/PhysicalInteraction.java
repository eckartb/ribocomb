package rnasecondary;

import generaltools.ConstrainableDouble;

/** describes physical interaction between two residue */
public interface PhysicalInteraction extends ConstrainableDouble, Interaction {


}
