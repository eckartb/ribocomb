package rnasecondary;

import java.io.InputStream;

import generaltools.MalformedInputException;

/** this class describes the interaction between different RNA nucleotides */
public class RnaInteractionType implements InteractionType {

    public static final int NO_INTERACTION = -1;
    public static final int UNKNOWN_SUBTYPE = 0;
    public static final int WATSON_CRICK = 1;
    public static final int WOBBLE = 2;
    public static final int NON_STANDARD = 3;
    public static final int HOOGSTEEN = 4;
    public static final int TERTIARY = 5;
    public static final int BACKBONE = 6;

    private static final String[] subTypeNames = {"Unknown", "WatsonCrick", "Wobble", "NonStandard", "Hoogsteen", "Tertiary", "Backbone" };
    
    private static final String typeName = "RnaInteraction";

    private int subTypeId = UNKNOWN_SUBTYPE;

    public RnaInteractionType() { }

    /** constructor: takes integer describing the subtype */
    public RnaInteractionType(int subTypeId) { this.subTypeId = subTypeId; }

    public Object clone() {
	RnaInteractionType iType = new RnaInteractionType();
	iType.subTypeId = this.subTypeId;
	return iType;
    }
    
    public boolean equals(Object other) {
	if (other instanceof RnaInteractionType) {
	    int otherId = ((RnaInteractionType)other).getSubTypeId();
	    return otherId == this.subTypeId;
	}
	return false; // different types
    }

    public String getClassName() { return "RnaInteractionType"; }

    public String getTypeName() { return typeName; }
    
    public String getSubTypeName() { return subTypeNames[subTypeId]; }

    public int getSubTypeId() { return subTypeId; }

    public void read(InputStream is) throws MalformedInputException {
	// TODO
    }

    public void setSubTypeId(int n) { this.subTypeId = n; }

    public String toString() {
	String result = "(" + getTypeName() + " " + getSubTypeId() 
	    + " " + getSubTypeName() + " )"; 
	return result;
    }

}
