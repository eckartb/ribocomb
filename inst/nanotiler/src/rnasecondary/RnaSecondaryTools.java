package rnasecondary;

import java.io.*;

import org.testng.annotations.*;

public class RnaSecondaryTools {

        /** Watson-Crick pairings: AU, GC, UA, CG (but not GU, UG) */
    public static boolean isWatsonCrick(char c1, char c2) {
	c1 = Character.toUpperCase(c1);
	c2 = Character.toUpperCase(c2);
	if (c1 == c2) {
	    return false;
	}
	if (c1 > c2) {
	    return isWatsonCrick(c2, c1);
	}
	switch (c1) {
	case 'A':
	    return c2 == 'U';
	case 'C':
	    return c2 == 'G';
	}
	return false;
    }

    /** Returns true if c1 and c2 correspond to Watson Crick pairs: (A,U), (U,A), (G,C), (C,G) */
    public static boolean isRnaComplement(char c1, char c2) {
	c1 = Character.toUpperCase(c1);
	c2 = Character.toUpperCase(c2);
	if (c1 == c2) {
	    return false;
	}
	if (c1 > c2) {
	    return isRnaComplement(c2, c1);
	}
	switch (c1) {
	case 'A':
	    return c2 == 'U';
	case 'C':
	    return c2 == 'G';
	case 'G':
	    assert c2 != 'C'; // must be sorted
	    return c2 == 'U';
	}
	return false;
    }

    /** Returns complementary base */
    public static char getRnaComplement(char c1) {
	c1 = Character.toUpperCase(c1);
	switch (c1) {
	case 'A':
	    return 'U';
	case 'C':
	    return 'G';
	case 'G':
	    return 'C'; // must be sorted
	case 'U':
	    return 'A';
	}
	assert false;
	return 'X';
    }

    /** Returns complementary base sequence */
    public static String getRnaComplement(String s) {
	StringBuffer result = new StringBuffer(s);
	int n = s.length();
	for (int i = 0; i < s.length(); ++i) {
	    result.setCharAt(n - i -1, getRnaComplement(s.charAt(i)));
	}
	return result.toString();
    }

    public static int[][] computeDotMatrix(String strand) {
	int length = strand.length();
	int[][] matrix = new int[length][length];
	for (int i = 0; i < length; i++) {
	    for (int j = i; j < length; j++) {
		if (isWatsonCrick(strand.charAt(i), strand.charAt(j))) {
		    matrix[i][j] = 1;
		    matrix[j][i] = 1;
		}
		else {
		    matrix[i][j] = 0;
		    matrix[j][i] = 0;
		}
	    }
	}
	return matrix;
    }

    public static int[][] computeDotMatrix(String strand1, String strand2) {
	String bothStrands = strand1.concat(strand2);
	return computeDotMatrix(bothStrands);
    }

    /** Tests method computeDotMatrix() */
    @Test(groups={"new"})
    public void testComputeDotMatrix() {
	String[] strands = { "gggaaaUCGUCuGGCAUUAACAuUCACACCAAGuGGCUAGUAAUuCUUGA",
			     "gggagaACUUAuGAGUGCGCGAuGCACAGCUUCuUGUUAAUGCCuGAAAC",
			     "gggaucUUGCGuAGCGAUCAGUuUAAGUGUUUCuGACGAUCAAGuGGCGG",
			     "ggcaacUCGCUuACGUGGAAUAuCUACAACAGGuUCGCGCACUCuACUGA",
			     "ggcgcuUGUGCuCCUGUUGUAGuGAUAAUCUAAuCUUGGUGUGAuGAAGC",
			     "ggacauUUAUCuUAUUCCACGUuCGCAACCGCCuAUUACUAGCCuUUAGA" };
	
	for (int x = 0; x < strands.length-1; x++) {
	    for (int y = x+1; y < strands.length; y++) {
		//	System.out.println("Strands " + strands[x] + " & " + strands[y]);
		int[][] matrix = computeDotMatrix(strands[x], strands[y]);
		
		try {
		    File outputFile = new File("dotmatrix" + (x+1) + "-" + (y+1) + ".dat");
		    FileOutputStream fos = new FileOutputStream(outputFile);
		    PrintStream ps = new PrintStream(fos);
		    for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
			    ps.print(matrix[i][j] + "  ");
			}
			ps.println("");
		    }
		    fos.close();
		}
		catch (java.io.IOException ioe) {  }
	    }
	}
	System.out.println("Finished testing computeDotMatrix");
    }
}
