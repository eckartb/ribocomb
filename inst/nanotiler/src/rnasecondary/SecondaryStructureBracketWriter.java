package rnasecondary;

import java.util.logging.Logger;

import sequence.Residue;
import sequence.Sequence;

public class SecondaryStructureBracketWriter implements SecondaryStructureWriter {


    public static final String NEWLINE = System.getProperty("line.separator");
    private static Logger log = Logger.getLogger("NanoTiler_debug"); /** The generated debug log. */

    public static final char NO_BASE_PAIR_CHAR = '.';

    private String writeSequence(SecondaryStructure structure, int seqId) {
	Sequence sequence = structure.getSequence(seqId);
	String result = sequence.sequenceString();
	// get secondary structure representation
	return result;
    }

    /** returns index of sequence to which residue belongs
     * TODO : very slow implementation ! 
     * TODO : contains BUG !!! */
    private int findSequence(Residue res, SecondaryStructure structure) {
	// Sequence seq = res.getSequence();
	for (int i = 0; i < structure.getSequenceCount(); ++i) {
	    Sequence sequence = structure.getSequence(i);
	    for (int j = 0; j < sequence.size(); ++j) {
		if (res == sequence.getResidue(j)) {
		    return i;
		}
	    }
	}
	return -1;
    }

    private void addInteraction(Interaction interaction, 
				Interaction[][] interactionArray,
				SecondaryStructure structure) {
	Residue res1 = interaction.getResidue1();
	Residue res2 = interaction.getResidue2();
	int seqId1 = findSequence(res1, structure);
	int seqId2 = findSequence(res2, structure);
	if ((seqId1 >= 0) && (seqId2 >= 0)) {
	    interactionArray[seqId1][res1.getPos()] = interaction;
	    interactionArray[seqId2][res2.getPos()] = interaction;
	}
	else {
	    log.warning("could not find: " 
			+ res1 + " " + res2);
// 	    log.fine("Respective sequences:");
// 	    log.fine("" + res1.getSequence());
// 	    log.fine("" + res2.getSequence());
	    for (int i = 0; i < structure.getSequenceCount(); ++i) {
		log.fine("" + (i+1) + " : " + structure.getSequence(i));
	    }
	}
    }

    /** returns 'B' when given 'A' etc */
    private char incChar(char c) {
	int n = (int)c;
	++n;
	return (char)n;
    }

    /** returns 'A' when given 'B' etc */
    private char decChar(char c) {
	int n = (int)c;
	--n;
	return (char)n;
    }

    private void generateCharArray(char[][] charArray, 
				   Interaction[][] interactionArray,
				   SecondaryStructure structure) {
	char interactionChar = 'A';
	interactionChar = decChar(interactionChar);
	for (int i = 0; i < interactionArray.length; ++i) {
	    for (int j = 0; j < interactionArray[i].length; ++j) {
		Interaction interaction = interactionArray[i][j];
		if (interaction == null) {
		    charArray[i][j] = NO_BASE_PAIR_CHAR;
		    continue;
		}
		Residue res1 = interaction.getResidue1();
		Residue res2 = interaction.getResidue2();
		int pos1 = res1.getPos();
		int pos2 = res2.getPos();
		if (interaction.isIntraSequence()) {
		    if (pos1 < pos2) {
			charArray[i][pos1] = '(';
			charArray[i][pos2] = ')';
		    }
		    else if (pos1 > pos2) {
			charArray[i][pos1] = ')';
			charArray[i][pos2] = '(';
		    }
		}
		else { // interaction between two different sequences
		    int seqId1 = findSequence(res1, structure);
		    int seqId2 = findSequence(res2, structure);
		    // check if new stem:
		    boolean newStemMode = true;
		    if ((pos1 > 0) && ((pos2+1) < charArray[seqId2].length)) {
			if (charArray[seqId1][pos1-1] == charArray[seqId2][pos2+1]) {
			    newStemMode= false;
			}
		    }
		    if (newStemMode) {
			interactionChar = incChar(interactionChar);
		    }
		    charArray[seqId1][pos1] = interactionChar;
		    charArray[seqId2][pos2] = interactionChar;
		}
	    }
	}
    }

    /** generates string from secondary structure */
    public String writeString(SecondaryStructure structure) {
	Interaction[][] interactionArray = new Interaction[structure.getSequenceCount()][0];
	char[][] charArray = new char[structure.getSequenceCount()][0];
	for (int i = 0; i < structure.getSequenceCount(); ++i) {
	    interactionArray[i] = new Interaction[structure.getSequence(i).size()];
	    charArray[i] = new char[structure.getSequence(i).size()];
	}
	for (int i = 0; i < structure.getInteractionCount(); ++i) {
	    Interaction interaction = structure.getInteraction(i);
	    addInteraction(interaction, interactionArray, structure);
	}
	generateCharArray(charArray, interactionArray, structure);
	String result = "";
	for (int i = 0; i < structure.getSequenceCount(); ++i) {
	    result = result + writeSequence(structure, i) + NEWLINE; // TODO bad style
	    result = result + new String(charArray[i]) + NEWLINE;
	}
	return result;
    }

}
