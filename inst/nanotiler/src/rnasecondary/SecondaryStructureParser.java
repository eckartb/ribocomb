package rnasecondary;

import java.io.IOException;
import java.text.ParseException;

import rnasecondary.MutableSecondaryStructure;

public interface SecondaryStructureParser {

    public MutableSecondaryStructure parse(String filename) throws IOException, ParseException;

    public MutableSecondaryStructure parse(String[] lines) throws ParseException;

}

