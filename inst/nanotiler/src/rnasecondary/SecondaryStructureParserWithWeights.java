package rnasecondary;

import sequence.*;
import java.util.*;
import java.util.regex.*;
import java.io.*;
import java.text.ParseException;
import java.util.logging.*;
import numerictools.IntervalInt;
import generaltools.StringTools;
import org.testng.annotations.*;
import java.util.ResourceBundle;

/** parses structure string in bracket notation, adds found "(" and ")" parenthesis to interaction set */
public class SecondaryStructureParserWithWeights implements SecondaryStructureParser {

    // public int debugLevel = 2;

    private boolean fastaMode = false; // if true, parse in pseudo FASTA format (name, sequence, secondary structure in 3 lines)

    public Level debugLogLevel = Level.FINE;

    private Logger log = Logger.getLogger("mcsopt");

    public SecondaryStructureParserWithWeights() {
	log.finest("starting SecondaryStructureParserWithWeights");
    }

    public SecondaryStructureParserWithWeights(boolean fastaMode) {
	log.finest("starting SecondaryStructureParserWithWeights");
	this.fastaMode = fastaMode;
    }

    private int findInnerInteraction(String s, int n) {
	if (s.charAt(n) != '(') {
	    return -1;
	}
	for (int i = n+1; i < s.length(); ++i) {
	    if (s.charAt(i) == ')') {
		return i;
	    }
	    else if (s.charAt(i) == '(') {
		return -1; // is not inner interaction
	    }
	}
	return -1; // no closing bracket found
    }
    
    private IntervalInt findInnerInteraction(String s) {
	for (int i = 0; i < s.length(); ++i) {
	    int j = findInnerInteraction(s, i);
	    if (j >= 0) {
		return new IntervalInt(i,j);
	    }
	}
	return null;
    }

    /** parses structure string in bracket notation, adds found "(" and ")" parenthesis to interaction set */
    void addInteractions(String structure, Sequence seq, InteractionSet interactions) {
	int numInteractions = StringTools.countChar(structure, '(');
	StringBuffer buf = new StringBuffer(structure);
	int foundInteractions = 0;
	while (foundInteractions < numInteractions) {
	    IntervalInt intervall = findInnerInteraction(buf.toString());
	    if (intervall == null) {
		log.log(debugLogLevel, "Could not find inner interactions in: " + structure);
		break; // could not find more interactions
	    }
	    int j = intervall.getLower();
	    int k = intervall.getUpper();
// 	    if (debugLevel > 1) {
// 		System.out.println("Found interaction");
// 		System.out.println("Structure: " + structure);
// 		System.out.println("Interaction: " + j + "   " + k);
// 	    }
	    // found interaction
	    interactions.add(new SimpleInteraction(seq.getResidue(j), seq.getResidue(k), 
						   new RnaInteractionType(RnaInteractionType.WATSON_CRICK)));
	    buf.setCharAt(j, '.');
	    buf.setCharAt(k, '.'); // delete 
	    foundInteractions++;
	}
	for (int i = 0; i < structure.length(); ++i) {
	    if (structure.charAt(i) == '?') {
		log.log(debugLogLevel, "Setting residue " + (i+1) + " to ignore status!");
		seq.getResidue(i).setProperty("seqstatus", "ignore");
	    }
	}
	assert foundInteractions == numInteractions;
    }

    /** Parses secondary structure from given file name. */
    public MutableSecondaryStructure parse(String filename) throws IOException, ParseException {
	FileInputStream fis = new FileInputStream(filename);
	String[] lines = StringTools.readAllLines(fis);
	return parse(lines);
    }

    /** Generates secondary structure from set of lines */
    public MutableSecondaryStructure parse(String[] lines) throws ParseException {
	try {
	    UnevenAlignment sequences = new SimpleUnevenAlignment();
	    InteractionSet interactions = new SimpleInteractionSet();
	    ArrayList<String> structures = new ArrayList<String>();
	    ArrayList<List<Double > > weights = new ArrayList<List<Double> >();
	    log.log(debugLogLevel, "Parsing input");
	    String name = null;
	    int step = 0;
	    double weight = 1.0;
	    String startPattern = "~STRAND";
	    if (fastaMode) {
		startPattern = ">";
	    }
	    for (int i = 0; i < lines.length; ++i) {
		String line = lines[i];
		log.log(debugLogLevel, "Parsing line " + (i+1) + " : " + line);
		if(step == 0) {
// 		    Pattern p = Pattern.compile("~STRAND *(.*) *(.*)");
// 		    Matcher m = p.matcher(line);
// 		    if(m.matches()) {
			
// 			name = m.group(1);
// 			if (m.groupCount
		    if (line.startsWith(startPattern)) {
			if (!fastaMode) {
			    String[] words = line.split(" ");
			    if ((words.length < 2) || (words.length > 3)) {
				throw new ParseException("Error parsing ~STRAND line " + line, i+1);
			    }
			    name = words[1];
			    if (words.length > 2) {
				try {
				    weight = Double.parseDouble(words[2]);
				}
				catch (NumberFormatException nfe) {
				    throw new ParseException("Error parsing weight in ~STRAND line " + line, i+1);
				}
			    }
			    else {
				weight = 1.0;
			    }
			}
			else { // fastaMode
			    line = line.trim();
			    name = line.substring(1, line.length());
			}
			log.log(debugLogLevel, "Parsed name and weight: " + name + " " + weight);
			++step;
		    }
		    else {
			log.info("Expecting ~STRAND. Ignoring line " + line);
		    }
		}
		else if (step == 1) {
		    Pattern p = null;
		    if (!fastaMode) {
			p = Pattern.compile("SEQUENCE *= *([AaCcGgNnTtUuWwSs]+)");
		    }
		    else {
			p = Pattern.compile("([AaCcGgNnTtUu]+)");
		    }
		    Matcher m = p.matcher(line);
		    if(m.matches()) {
			String seqData = m.group(1);
			List<Integer> lowCharIndices = new ArrayList<Integer>();
			for (int ii = 0; ii < seqData.length(); ++ii) {
			    if (Character.isLowerCase(seqData.charAt(ii))) {
				lowCharIndices.add(ii); // autoboxing!
			    }
			}
			log.log(debugLogLevel, "Using alphabet: " + DnaTools.AMBIGUOUS_RNA_ALPHABET);
			Sequence sequence = new SimpleMutableSequence(seqData.toUpperCase(), name, DnaTools.AMBIGUOUS_RNA_ALPHABET);
			if (weight != 1.0) {
			    sequence.setWeight(weight);
			}
			for (int ii = 0; ii < lowCharIndices.size(); ++ii) {
			    int idx = lowCharIndices.get(ii); // autoboxing !
			    sequence.getResidue(idx).setProperty(SequenceStatus.name,  SequenceStatus.fragment); // set to "fragment" status: keep constant in optimization
			}
			log.log(debugLogLevel, "Adding sequence " + sequence.toString());
			sequences.addSequence(sequence);
			++step;
		    }
		    else {
			log.info("Expecting SEQUENCE. Ignoring line : " + line);
		    }
		}
		else if (step == 2) {
		    Pattern p = null;
		    // allow alphabet as well as . ? - + [ ] ( ) ` ^ _ \
		    String pat = "([A-Za-z\\?\\.\\(\\)\\-\\+\\[\\]\\^\\`\\_\\\\]+)";
		    if (!fastaMode) {
			// p = Pattern.compile("STRUCTURE *= *([A-Za-z\\?\\.\\(\\)\\-]+)");
			p = Pattern.compile("STRUCTURE *= *" + pat);
		    }
		    else {
			p = Pattern.compile(pat);
			// p = Pattern.compile("([A-Za-z\\?\\.\\(\\)\\-]+)");
		    }
		    assert p != null;
		    assert line != null;
		    Matcher m = p.matcher(line);
		    if(m.matches()) {
			structures.add(m.group(1));
			log.log(debugLogLevel, "Adding structure " + m.group(1));
			++step;
		    }
		    else {
			log.info("Expecting STRUCTURE keyword. Ignoring line : " + line);
		    }
		} else if (step == 3) {
		    String pat = "([0-9\\+]+)";		    
		    Pattern p = Pattern.compile("WEIGHTS *= *" + pat);
		    Matcher m = p.matcher(line);
		    if(m.matches()) {
			//	structures.add(m.group(1));
			String weightString = m.group(1);
			log.info("Found weight string " + weightString);
			log.log(debugLogLevel, "Adding structure " + m.group(1));
			List<Double> weightList = new ArrayList<Double>();
			for (int k = 0; k < weightString.length(); ++k) {
			    String c = weightString.substring(k, k+1);
			    assert(c.length() == 1);
			    double resWeight = 1.0;
			    if (!c.equals("+")) {
				try {
				    resWeight = Double.parseDouble(c) / 10.0;
				} catch(NumberFormatException nfe) {
				    throw new ParseException(nfe.getMessage(), i);
				}			 
			    } 
			    weightList.add(resWeight);
			}
			weights.add(weightList);
			step = 0; // reset: ready to read next sequence
		    } else {
			log.info("Expecting WEIGHTS keyword. Ignoring line : " + line);
		    }
		}
	    }

	    // handle secondary structure
	    for(int i = 0; i < structures.size(); ++i) {
		String structure = structures.get(i);
		log.fine("Adding intra-sequence interactions for structure: " + structure);
		addInteractions(structure, sequences.getSequence(i), interactions);
	    }
	    
	    // handle tertiary structure
	    for(int i = 0; i < structures.size(); ++i) {
		for(int j = i + 1; j < structures.size(); ++j) {
		    String s1 = structures.get(i);
		    String s2 = structures.get(j);
		    // int k = 0, l = s2.length() - 1;
		    // while (k < s1.length() && l > -1) {
		    StringBuffer sofarChars = new StringBuffer();
		    for (int k = 0; k < s1.length(); ++k) {
			char c1 = s1.charAt(k);
			// char c2 = s2.charAt(l);
			if (c1 != '(' && c1 != ')' && c1 != '.' && c1 != '-' && (sofarChars.indexOf("" + c1) < 0) ) {
			    addInteractions(s1, s2, sequences.getSequence(i), sequences.getSequence(j), interactions, c1);
			    sofarChars.append("" + c1);
			}
// 			    if(c1 == c2) {
// 				interactions.add(new SimpleInteraction(sequences.getSequence(i).getResidue(k), sequences.getSequence(j).getResidue(l), new RnaInteractionType(RnaInteractionType.WATSON_CRICK)));
// 				++k;
// 				--l;
// 			    }
// 			    else {
// 				--l;
// 			    }
// 			}
// 			else {
// 			    ++k;
			// }
		    }
		}
	    }       
	    MutableSecondaryStructure mutStruct = new SimpleMutableSecondaryStructure(sequences, interactions, weights);
	    assert(mutStruct.weightsSanityCheck());
	    return mutStruct;
	} catch (DuplicateNameException e1) {
	    System.out.println("DuplicateNameException: " + e1.getMessage());
	} catch (UnknownSymbolException e2) {
	    System.out.println("UnknownSymbolException: " + e2.getMessage());
	}
	return null;
    }

    private void addInteractions(String s1, String s2,
				 Sequence seq1, Sequence seq2,
				 InteractionSet interactions, char c) {
	assert s1.length() == seq1.size();
	assert s2.length() == seq2.size();
	log.fine("Starting interaction search: " + c);
	int p1 = 0;
	InteractionType interactionType = new RnaInteractionType(RnaInteractionType.WATSON_CRICK);
	for (int i = 0; i < s1.length(); ++i) {
	    if (s1.charAt(i) == c) {
		p1 = i;
		break;
	    }
	}
	int p2 = s2.length()-1;
	for (int i = s2.length() - 1; i >= 0; --i) {
	    if (s2.charAt(i) == c) {
		p2 = i;
		break;
	    }
	}
	for (int i = 0; i < s1.length(); ++i) {
	    int pp1 = p1 + i;
	    int pp2 = p2 - i;
	    if ((pp1 >= s1.length()) || (pp2 < 0) || (s1.charAt(pp1) != c) || (s2.charAt(pp2) != c)) {
		break;
	    }
	    interactions.add(new SimpleInteraction(seq1.getResidue(pp1), seq2.getResidue(pp2), interactionType));
	}
    }

    private void parseSequences(String[] lines ) {
	
    }

    private void parseInteractions(String[] lines) {


    }

    @Test(groups={"new"})
    public void testParse() {
	ResourceBundle rb = ResourceBundle.getBundle("SecondaryStructureDesignTest");
	String fileNames = rb.getString("testSecondaryStructureParserWithWeights");
	String[] tokens = fileNames.split(",");
	SecondaryStructureParser parser = new SecondaryStructureParserWithWeights();
	for (int i = 0; i < tokens.length; ++i) {
	    MutableSecondaryStructure structure = null;
	    String fileName = tokens[i];
	    if (fileName.length() < 2) {
		continue;
	    }
	    else {
		System.out.println("Reading file: " + fileName);
	    }
	    try {
		structure = parser.parse(tokens[i]);
		boolean foundZero = false;
		boolean foundNonZero = false;
		for (int k = 0; k < structure.getSequenceCount(); ++k) {
		    List<Double> weights = structure.getWeights(k);
		    for (int j =0; j < weights.size(); ++j) {
			System.out.println("Sequence " + (k+1) + " residue " + (j+1) + " weight: " + weights.get(j));
			if (weights.get(j) == 0.0) {
			    foundZero = true;
			} else {
			    foundNonZero = true;
			}
		    }
		}
		if (!foundZero) {
		    log.severe("Did not find any residue that have zero weights.");
		    assert(foundZero);
		}
		if (!foundNonZero) {
		    log.severe("Did not find any residue that have non-zero weights.");
		    assert(foundZero);
		}
	    }
	    catch (IOException ioe) {
		System.out.println(ioe.getMessage());
		assert false;
	    }
	    catch (ParseException pe) {
		System.out.println(pe.getMessage());
		assert false;
	    }
	    assert structure != null;
	    SecondaryStructureWriter writer = new SecondaryStructureScriptFormatWriter();
	    System.out.println("Read secondary structure: " + writer.writeString(structure));

	}
    }
}

