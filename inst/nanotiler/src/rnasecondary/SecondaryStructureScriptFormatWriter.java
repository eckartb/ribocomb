package rnasecondary;

import java.util.*;

import java.util.logging.Logger;
import sequence.*;

import static rnadesign.rnamodel.PackageConstants.*;

public class SecondaryStructureScriptFormatWriter implements SecondaryStructureWriter {

    public static final char NO_BASE_PAIR_CHAR = '.';

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    private boolean writeSequenceMode = true;

    private boolean purgeMultipleInteractions = true; // if true, remove interactions that cannot be drawn as paranthesis

    public SecondaryStructureScriptFormatWriter() { }

    public SecondaryStructureScriptFormatWriter(boolean writeSequenceMode) {
	this.writeSequenceMode = writeSequenceMode;
    }

    public boolean writeWeightsMode = false;

    /** sets character at position pos */
    private static String setChar(String s, char c, int pos) {
	if (pos >= s.length()) {
	    return s;
	}
	String result = s.substring(0, pos) + c + s.substring(pos+1, s.length());
	assert result.length() == s.length();
	return result;
    }

    public boolean isWriteWeightsMode() {
	return writeWeightsMode;
    }

    public void setWriteWeightsMode(boolean mode) {
	writeWeightsMode = mode;
    }

    public String writeWeights(SecondaryStructure structure, int seqId) {
	List<Double> weights = structure.getWeights(seqId);
	StringBuffer buf = new StringBuffer();
	for (int i = 0; i < weights.size(); ++i) {
	    double weight = weights.get(i);
	    String ws = "+";
	    if (weight < 1.0) {
		ws = "" + ((int)(weight*10.0));
		assert(ws.length() == 1); // 0.245 gets converted to "2", 0 gets converted to "0", 0.99 gets converted to "9" etc; 1.0 has the special symbol "+"
	    }
	    buf.append(ws);
	}
	assert (buf.length() == weights.size());
	return buf.toString();
    }

    public String writeSequence(SecondaryStructure structure, int seqId) {
	Sequence sequence = structure.getSequence(seqId);
	String result = sequence.sequenceString();
	log.fine("Writing sequence for optimizer: " + result);
	for (int i = 0; i < result.length(); ++i) {
	    Residue residue = sequence.getResidue(i);
	    Properties prop = residue.getProperties();
	    if (prop != null) {
		String status = prop.getProperty(SequenceStatus.name);
		if ((status != null) && (status.equals(SequenceStatus.adhoc))) {
		    result = setChar(result, Character.toUpperCase(result.charAt(i)), i);
		}
		else {
		    // convert to lower case: this means do not change in sequence optimization
		    char c = Character.toLowerCase(result.charAt(i)); 
		    assert c != 'b';
		    result = setChar(result, c, i);
		}
	    }
	    else { // TODO : consolidate last two cases
		// convert to lower case: this means to not change
		result = setChar(result, Character.toLowerCase(result.charAt(i)), i);
	    }
	}
	return result;
    }

    /** returns full sequence name, checks if part of Object3D hierarchy */
//     public static String sequenceFullName(Sequence seq) {
// 	String result = seq.getName();
// 	if (seq.getParentObject() instanceof Object3D) {
// 	    Object3D parent = (Object3D)(seq.getParentObject());
// 	    result = Object3DTools.getFullName(parent) + "." + result;
// 	}
// 	return result;
//     }

    /** returns full sequence name, checks if part of Object3D hierarchy */
//     public static String sequenceParentName(Sequence seq) {
// 	String result = seq.getName();
// 	if (seq.getParentObject() instanceof Object3D) {
// 	    Object3D parent = (Object3D)(seq.getParentObject());
// 	    result = parent.getName() + "." + result;
// 	}
// 	return result;
//     }

    /** returns index of sequence to which residue belongs
     */
    private int findSequence(Residue res, SecondaryStructure structure) {
 	assert res != null;
 	assert structure != null;
	assert res.getParentObject() != null;
 	// Sequence seq = res.getSequence();
 	// assert seq != null;
 	// String seqName = sequenceParentName(seq);
 	// assert seqName != null;
 	for (int i = 0; i < structure.getSequenceCount(); ++i) {
	    Sequence seq = structure.getSequence(i);
	    Residue other = seq.getResidue(0);
	    if (res.isSameSequence(other)) {
		return i;
	    }
//  	    // String seqNameOther = sequenceParentName(structure.getSequence(i));
//  	    // if complete name is identical:
//  	    log.fine("Comparing: " + seqName + " " + seqNameOther);
//  	    if (seqName.equals(seqNameOther)) {
//  		log.fine("Identical names found! " + i);
//  		return i;
//  	    }
 	}
 	return -1;
    }

    private void addInteraction(Interaction interaction, 
				Interaction[][] interactionArray,
				SecondaryStructure structure) {
	log.fine("Starting addInteraction for " + interaction);
	Residue res1 = interaction.getResidue1();
	Residue res2 = interaction.getResidue2();
	assert res1 != null;
 	assert res2 != null;
	int seqId1 = findSequence(res1, structure);
	int seqId2 = findSequence(res2, structure);
	if (seqId1 < 0) {
	    log.fine("Could not find sequence for residue: " + res1.getAssignedName() + " " + res1.getPos() + " " + res1.getSymbol().getCharacter());
	    return;
	}
	if (seqId2 < 0) {
	    log.fine("Could not find sequence for residue: " + res2.getAssignedName() + " " + res2.getPos() + " " + res2.getSymbol().getCharacter());
	    return;
	}
	assert seqId1 >= 0;
	assert seqId2 >= 0;
	assert seqId1 < structure.getSequenceCount();
	assert seqId2 < structure.getSequenceCount();
	Sequence seq1 = structure.getSequence(seqId1);
	Sequence seq2 = structure.getSequence(seqId2);
	assert seq1.size() == interactionArray[seqId1].length;
	assert seq2.size() == interactionArray[seqId2].length;
	if ((seqId1 >= 0) && (seqId2 >= 0)) {
 	    log.fine("Adding interaction(1): " + (seqId1+1) + ":" 
		     + (res1.getPos()+1) + " " + (seqId2+1) + ":" + (res2.getPos()+1) + " " 
		     + (seq1.size()+1) + " " + (seq2.size() + 1)); //  + " " + interaction);
//  	    log.fine("Adding interaction(2): " + seqId2 + " " 
// 		     + res2.getPos() + " " + seq2.size() + " " + interactionArray[seqId2].length); //  + " " + interaction);
	    assert seqId1 < interactionArray.length;
	    assert res1.getPos() < interactionArray[seqId1].length;
	    assert seqId2 < interactionArray.length;
	    assert res2.getPos() < interactionArray[seqId2].length;
	    interactionArray[seqId1][res1.getPos()] = interaction;
	    interactionArray[seqId2][res2.getPos()] = interaction;
	}
	else {
	    log.fine("could not find: " + res1.getSymbol() + res1.getPos() + " " 
			+ res2.getSymbol() + res2.getPos());
// 	    log.fine("Respective sequences:");
// 	    log.fine("" + res1.getSequence());
// 	    log.fine(sequenceParentName(res1.getSequence()));
// 	    log.fine("" + res2.getSequence());
// 	    log.fine(sequenceParentName(res2.getSequence()));
// 	    for (int i = 0; i < structure.getSequenceCount(); ++i) {
// 		log.finest("" + (i+1) + " : " + structure.getSequence(i));
// 		log.finest(sequenceParentName(structure.getSequence(i)));
// 	    }
	}
    }

    /** returns 'B' when given 'A' etc */
    private char incChar(char c) {
	int n = (int)c;
	++n;
	return (char)n;
    }

    /** returns 'A' when given 'B' etc */
    private char decChar(char c) {
	int n = (int)c;
	--n;
	return (char)n;
    }

    private double[][] generateInteractionMatrix(Interaction[][] interactionArray,
						 int seqId1, 
						 int seqId2,
						 SecondaryStructure structure) {
	
	Sequence sequence1 = structure.getSequence(seqId1);
	Sequence sequence2 = structure.getSequence(seqId2);
	int size1 = sequence1.size();
	int size2 = sequence2.size();
	double[][] matrix = new double[size1][size2];
	for (int i = 0; i < matrix.length; ++i) {
	    for (int j = 0; j < matrix[0].length; ++j) {
		matrix[i][j] = 0;
	    }
	}
	for (int i = 0; i < size1; ++i) {
	    Interaction interaction = interactionArray[seqId1][i];
	    if (interaction == null) {
		continue;
	    }
	    Residue res1 = interaction.getResidue1();
	    Residue res2 = interaction.getResidue2();
	    int sid1 = findSequence(res1, structure);
	    int sid2 = findSequence(res2, structure);
	    if ((sid1 == seqId1) && (sid2 == seqId2)) {
		assert res1.getPos() < matrix.length;
		assert res2.getPos() < matrix[res1.getPos()].length;
		matrix[res1.getPos()][res2.getPos()] = 1.0;
	    }
	    else if ( ((sid1 == seqId2) && (sid2 == seqId1))) {
 		assert res2.getPos() < matrix.length;
		assert res1.getPos() < matrix[res2.getPos()].length;
		matrix[res2.getPos()][res1.getPos()] = 1.0;
	    }

	}
	return matrix;
    }

    /** updates character array for one sequences */
    private String generateCharArray(char[][] charArray, 
				   Interaction[][] interactionArray,
				   SecondaryStructure structure,
				   int seqId1) {
	String wyfcurseq="";
	int seqId2 = seqId1; // really just one sequence, but treat as two
	Sequence sequence1 = structure.getSequence(seqId1);
	Sequence sequence2 = sequence1;
	int numRows = sequence1.size();
	int numCols = sequence2.size();
	assert numRows == numCols;
	double thresh = 0.5;
	int[][] visitedMtx = new int[numRows][numCols];
	double[][] mtx = generateInteractionMatrix(interactionArray, seqId1, seqId2, structure);
	for (int i = 0; i < numRows; ++i) {
	    for (int j = 0; j < numCols; ++j) {
		visitedMtx[i][j] = 0;
	    }
	}
	InteractionType watsonCrick = new RnaInteractionType(RnaInteractionType.WATSON_CRICK);
	for (int i = 0; i < numRows; ++i) {
	    for (int j = 0; j < numCols; ++j) {
		if (visitedMtx[i][j] == 1) {
		    continue; // point already visited
		}
		if (mtx[i][j] >= thresh) {
		    // if stem does currently not exist
		    Stem newStem = new SimpleStem(sequence1, sequence2);
		    int k = 0; 
		    while ( (i+k<numRows) && (j>=k) && (mtx[i+k][j-k] >= thresh)) { 
			if ((visitedMtx[i+k][j-k] > 0)) {
			    break; // already visited
			}
			visitedMtx[i+k][j-k] = 1;
			Residue residue1 = sequence1.getResidue(i+k); // TODO check if order is ok
			Residue residue2 = sequence2.getResidue(j-k);
			Interaction interaction = new SimpleInteraction(residue1, residue2, watsonCrick);
			newStem.add(interaction);
			++k;
		    }
		    // add stem to result:
		    for (int k2 = 0; k2 < newStem.size(); ++k2) {
			int p1 = newStem.getStartPos() + k2;
			int p2 = newStem.getStopPos() - k2;
			log.fine("Stem pos: " + p1 + " " + p2 + " " + charArray.length + " " + charArray[0].length);
			assert p1 < charArray[seqId1].length;
			assert p2 < charArray[seqId1].length;
			if (p1 < p2) {
			    charArray[seqId1][p1] = '(';
			    charArray[seqId1][p2] = ')';
			}
			else {
			    charArray[seqId1][p1] = ')';
			    charArray[seqId1][p2] = '(';
			}
			wyfcurseq = wyfcurseq + "["+p1+","+p2+"]";
		    }
		}
		else {
		    visitedMtx[i][j] = 1;
		}
	    }
	}
	return wyfcurseq;
    }
    
    /** updates character array for pair of sequences */
    private char generateCharArray(char[][] charArray, 
				   Interaction[][] interactionArray,
				   SecondaryStructure structure,
				   int seqId1,
				   int seqId2,
				   char interactionChar) {
	Sequence sequence1 = structure.getSequence(seqId1);
	Sequence sequence2 = structure.getSequence(seqId2);
	int numRows = sequence1.size();
	int numCols = sequence2.size();
	double thresh = 0.5;
	int[][] visitedMtx = new int[numRows][numCols];
	double[][] mtx = generateInteractionMatrix(interactionArray, seqId1, seqId2, structure);
	for (int i = 0; i < numRows; ++i) {
	    for (int j = 0; j < numCols; ++j) {
		visitedMtx[i][j] = 0;
	    }
	}
	InteractionType watsonCrick = new RnaInteractionType(RnaInteractionType.WATSON_CRICK);
	for (int i = 0; i < numRows; ++i) {
	    for (int j = 0; j < numCols; ++j) {
		if (visitedMtx[i][j] == 1) {
		    continue; // point already visited
		}
		if (mtx[i][j] >= thresh) {
		    // if stem does currently not exist
		    Stem newStem = new SimpleStem(sequence1, sequence2);
		    int k = 0; 
		    while ( (i+k<numRows) && (j>=k) && (mtx[i+k][j-k] >= thresh)) { 
			if ((visitedMtx[i+k][j-k] > 0)) {
			    break; // already visited
			}
			visitedMtx[i+k][j-k] = 1;
			Residue residue1 = sequence1.getResidue(i+k); // TODO check if order is ok
			Residue residue2 = sequence2.getResidue(j-k);
			Interaction interaction = new SimpleInteraction(residue1, residue2, watsonCrick);
			newStem.add(interaction);
			++k;
		    }
		    // add stem to result:
		    interactionChar = incChar(interactionChar);
		    for (int k2 = 0; k2 < newStem.size(); ++k2) {
			int p1 = newStem.getStartPos() + k2;
			int p2 = newStem.getStopPos() - k2;
			assert seqId1 < charArray.length;
			assert seqId2 < charArray.length;
			assert charArray[seqId1] != null;
			assert charArray[seqId2] != null;
			assert p1 >= 0;
			assert p1 < charArray[seqId1].length;
			assert p2 >= 0;
			assert p2 < charArray[seqId2].length;
			charArray[seqId1][p1] = interactionChar;
			charArray[seqId2][p2] = interactionChar;
		    }
		}
		else {
		    visitedMtx[i][j] = 1;
		}
	    }
	}
	return interactionChar;
	
// 		Interaction interaction = interactionArray[i][j];
		
// 		if (interaction == null) {
// 		    charArray[i][j] = NO_BASE_PAIR_CHAR;
// 		    continue;
// 		}
// 		else if(seenInteractions[i][j]) {
// 		    continue;
// 		}

// 		Residue res1 = interaction.getResidue1();
// 		Residue res2 = interaction.getResidue2();
// 		int pos1 = res1.getPos();
// 		int pos2 = res2.getPos();
// 		if (interaction.isIntraSequence()) {
// 		    if (pos1 < pos2) {
// 			charArray[i][pos1] = '(';
// 			charArray[i][pos2] = ')';
// 		    }
// 		    else if (pos1 > pos2) {
// 			charArray[i][pos1] = ')';
// 			charArray[i][pos2] = '(';
// 		    }
// 		}
// 		else { // interaction between two different sequences
// 		    int seqId1 = findSequence(res1, structure);
// 		    int seqId2 = findSequence(res2, structure);

// 		    // check if new stem by checking neighbors:
// 		    boolean newStemMode = true;
// 		    for (int k = -1; k <= 1; k+=2) {
// 			if (((pos1-k) >= 0) && ((pos1-k) < charArray[seqId1].length) 
// 			    && ((pos2+k) >= 0) && ((pos2+k) < charArray[seqId2].length)) {
// 			    if ((charArray[seqId1][pos1-k] == charArray[seqId2][pos2+k])
// 				&& (interactionArray[seqId1][pos1-k] != null)) {
// 				newStemMode= false;
// 				break;
// 			    }
// 			}
// 		    }
		    
// 		    if (newStemMode) {
// 			interactionChar = incChar(interactionChar);
// 		    }
		    
// 		    charArray[seqId1][pos1] = interactionChar;
// 		    charArray[seqId2][pos2] = interactionChar;

// 		    seenInteractions[seqId1][pos1] = true;
// 		    seenInteractions[seqId2][pos2] = true;
// 		}
// 	    }
// 	}
    }

    private String[] generateCharArray(char[][] charArray, 
				   Interaction[][] interactionArray,
				   SecondaryStructure structure) {
		
	String[]  wyfpair = new   String[interactionArray.length];	
	char interactionChar = 'A';
	interactionChar = decChar(interactionChar);
	for (int i = 0; i < interactionArray.length; ++i) {
            wyfpair[i]="";
	    wyfpair[i]=generateCharArray(charArray, interactionArray, structure, i);
	    for (int j = i+1; j < interactionArray.length; ++j) {
		interactionChar = generateCharArray(charArray, interactionArray, structure, i, j, interactionChar);
	    }
	}
	return wyfpair;
    }

    /* generates and returns only the secondary structures in char[] form */
    public List<String[]> writeSecondaryStructure(SecondaryStructure structure){
	if ((structure == null) || (structure.getSequenceCount() == 0)) {
	    return null;
	}
	Interaction[][] interactionArray = new Interaction[structure.getSequenceCount()][0];
	char[][] charArray = new char[structure.getSequenceCount()][0];
	for (int i = 0; i < structure.getSequenceCount(); ++i) {	    
	    interactionArray[i] = new Interaction[structure.getSequence(i).size()];
	    charArray[i] = new char[structure.getSequence(i).size()];
	    for (int j = 0; j < charArray[i].length; ++j) {
		charArray[i][j] = '.';
	    }
	}
	log.fine("Generating secondary structure representation using " + structure.getInteractionCount()
		 + " interactions.");
	for (int i = 0; i < structure.getInteractionCount(); ++i) {
	    Interaction interaction = structure.getInteraction(i);
	    int subType = interaction.getInteractionType().getSubTypeId();
	    if ( (subType!= RnaInteractionType.BACKBONE) && (subType != RnaInteractionType.NO_INTERACTION)) {
		addInteraction(interaction, interactionArray, structure);
	    }
	    else {
		log.fine("Ignoring interaction " + interaction.toString());
	    }
	}
	generateCharArray(charArray, interactionArray, structure);


	ArrayList<String[]> secondaryStructures = new ArrayList<String[]>();
	String[] temp;
	for(int i=0;i<charArray.length;i++){
	    temp = new String[charArray[i].length];
	    for(int k=0;k<temp.length;k++){
		temp[k] = String.valueOf(charArray[i][k]);
	    }
	    secondaryStructures.add(temp);
	}

	return secondaryStructures;
    }

    /** generates string from secondary structure */
    public String writeString(SecondaryStructure structure) {
	if ((structure == null) || (structure.getSequenceCount() == 0)) {
	    return "";
	}
	Interaction[][] interactionArray = new Interaction[structure.getSequenceCount()][0];
	char[][] charArray = new char[structure.getSequenceCount()][0];
	for (int i = 0; i < structure.getSequenceCount(); ++i) {	    
	    interactionArray[i] = new Interaction[structure.getSequence(i).size()];
	    charArray[i] = new char[structure.getSequence(i).size()];
	    for (int j = 0; j < charArray[i].length; ++j) {
		charArray[i][j] = '.';
	    }
	}
	for (int i = 0; i < structure.getInteractionCount(); ++i) {
	    Interaction interaction = structure.getInteraction(i);
	    int subType = interaction.getInteractionType().getSubTypeId();
	    if ( (subType!= RnaInteractionType.BACKBONE) && (subType != RnaInteractionType.NO_INTERACTION)) {
		addInteraction(interaction, interactionArray, structure);
	    }
	}
	String [] wyfinfo= new String[interactionArray.length];
	wyfinfo=generateCharArray(charArray, interactionArray, structure);		
	String result = "";
  String strandNameIndex = "ABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890"; //
	String strandName; 
  for (int i = 0; i < structure.getSequenceCount(); ++i) {
	    // result += "~STRAND " + SecondaryStructureScriptFormatWriter.sequenceFullName(structure.getSequence(i)) + "\n";
	    // full name not necessary anymore because ObjectGraphController ensures that all sequences have different names
      
      strandName = structure.getSequence(i).getName() + NEWLINE; //in certain cases, STRAND id is written to sec file as 'NUMBER #' like 
      //~STRAND Number 2, which leads to errors
      String [] tmp = strandName.split ("\\s+"); 
      if (tmp.length > 1) {
        strandName = Character.toString (strandNameIndex.charAt(i)); 
      } //this handles such issues
	    result += "~STRAND " + strandName + NEWLINE;
	    if (writeSequenceMode) {		
		result = result + "SEQUENCE  = " + writeSequence(structure, i) + NEWLINE; // TODO bad style
	    }
	    result = result + "STRUCTURE = " + new String(charArray[i]) + NEWLINE + wyfinfo[i] + NEWLINE ;
	    if (writeWeightsMode && structure.weightsSanityCheck()) {
		result = result + "WEIGHTS   = " + writeWeights(structure, i) + NEWLINE;
	    }
	}
	return result;
    }

}
