package rnasecondary;

import java.util.*;
import sequence.*;

public class SecondaryStructureSimilarity {
  
  private SecondaryStructure s1;
  private SecondaryStructure s2;
  
  public SecondaryStructureSimilarity( SecondaryStructure _s1, SecondaryStructure _s2 ){
    this.s1 = _s1;
    this.s2 = _s2;
    // System.out.println("s1: " + s1.getSequenceCount() );
    // System.out.println("s2: " +  s2.getSequenceCount() );
  }

  public double findSimilarity(){
    double score = 1.0;
    double identical = identicalLengthScore();
    if(identical == 0.0){
      return 0.0;
    }
    
    score+=sequenceScore();
    return score;
  }
  
  public double infScore(){ //TODO implement
    return 0.0;
  }
  
  public double identicalLengthScore(){ //1 if strand lengths are identical, 0 otherwise
    int l1 = s1.getSequenceCount();
    int l2 = s2.getSequenceCount();
    if(l1 != l2){
      return 0.0;
    }
    for(int i=0; i<l1; i++){
      if(s1.getSequence(i).size() != s2.getSequence(i).size()){
        return 0.0;
      }
    }
    return 1.0;
  }
  
  public double sequenceScore(){
    assert identicalLengthScore()==0;
    double score = 0.0;
    for(int i=0; i<s1.getSequenceCount(); i++){
      Sequence seq1 = s1.getSequence(i);
      Sequence seq2 = s2.getSequence(i);
      for(int j=0; j<seq1.size(); j++){
        if(seq1.getResidue(j).getSymbol() .equals( seq2.getResidue(j).getSymbol() ) ){
          score++;
        }
      }
    }
    return score;
  }

}
