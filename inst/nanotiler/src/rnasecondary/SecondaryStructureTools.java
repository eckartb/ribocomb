package rnasecondary;

import java.util.*;
import sequence.*;
import graphtools.PermutationGenerator;

public class SecondaryStructureTools {

    public static boolean isFeasibleSequencePermutation(SecondaryStructure structure1, SecondaryStructure structure2, int[] perm) {
      //System.out.println("Warning: called mockup version of isFeasibleSequencePermutation");
      //if they are same length
      //int n = structure1.getSequences().getTotalResidueCount();
      
      int m = structure1.getSequenceCount();
      for(int i=0; i<m; i++){ //same length strands?
		if( structure1.getSequence(i).size() != structure2.getSequence( perm[i] ).size() ){
			return false;
		}
      }
      
      Map< List<Integer>, List<Integer> > map1 = makeMap(structure1);
      Map< List<Integer>, List<Integer> > preMap2 = makeMap(structure2);
      Map< List<Integer>, List<Integer> > map2 = new HashMap< List<Integer>, List<Integer> >();
      
      for( List<Integer> key : preMap2.keySet()){
      	List<Integer> value = preMap2.get(key);
      	List<Integer> newKey = new ArrayList<Integer>();
      	List<Integer> newValue = new ArrayList<Integer>();
      	
      	newKey.add( perm[key.get(0)] );
      	newKey.add( key.get(1) );
      	newValue.add( perm[value.get(0)] );
      	newValue.add( value.get(1) );
      	
      	map2.put(newKey, newValue);
    //  	System.out.println(perm[0] + " " + perm[1]
      }
      
      
     /* for(List<Integer> key : map1.keySet() ){
      	System.out.println( key.get(0) + "," + key.get(1) + "  :  " + map1.get(key).get(0) + "," + map1.get(key).get(1) );
      } */
      /*
      for( int i: perm){
      	System.out.print(i+" ");
      }
      System.out.println("PreMAP***");
      for(List<Integer> key : preMap2.keySet() ){
      	System.out.println( key.get(0) + "," + key.get(1) + "  :  " + preMap2.get(key).get(0) + "," + preMap2.get(key).get(1) );
      }
      System.out.println();
      System.out.println("MAP2***");
      for(List<Integer> key : map2.keySet() ){
      	System.out.println( key.get(0) + "," + key.get(1) + "  :  " + map2.get(key).get(0) + "," + map2.get(key).get(1) );
      }
      
      System.out.println("here");
      */
      for(List<Integer> key : map1.keySet() ){
      	if(! map1.get(key) .equals( map2.get(key)) ){
      	//System.out.println( key.get(0) + "," + key.get(1) + "  :  " + map1.get(key).get(0) + "," + map1.get(key).get(1) );
      	//System.out.println( key.get(0) + "," + key.get(1) + "  :  " + map2.get(key).get(0) + "," + map2.get(key).get(1) );
      	return false;
      	}
      }
      
      for(List<Integer> key : map2.keySet() ){
      	if(! map1.get(key) .equals( map2.get(key))){
      	//System.out.println( key.get(0) + "," + key.get(1) + "  :  " + map1.get(key).get(0) + "," + map1.get(key).get(1) );
      	//System.out.println( key.get(0) + "," + key.get(1) + "  :  " + map2.get(key).get(0) + "," + map2.get(key).get(1) );
      	return false;
      	}
      }
      
	  //if( ! map1.equals(map2) ){
	  //	System.out.println("fail 2");
	 /// 	return false;
	 // }
      return true; 
    }

    /** returns permutation, such that i'th sequence of first structure is perm[i]'th sequence of second structurn; returns null if no solution */
    public static int[] findSequencePermutation(SecondaryStructure structure1, SecondaryStructure structure2) {
     // loop over permutations;
     // return first feasible permutation
     int n = structure1.getSequences().getTotalResidueCount();
     int m = structure1.getSequenceCount();
     //System.out.println("Sequnce Count: " + n + " " + m);
     
     if (structure2.getSequences().getTotalResidueCount() != n) {
     	assert false;
     	System.out.println("Residue length mismatch: " + n + " " + structure2.getSequences().getTotalResidueCount() );
     	return null;
     }
     if (structure2.getSequenceCount() != m) {
     	assert false;
     	System.out.println("Sequence count mismatch: " + m + " " + structure2.getSequenceCount());
     	return null;
     }
     
     PermutationGenerator perm = new PermutationGenerator(m);
     do {
       int[] a = perm.get();
       if (isFeasibleSequencePermutation(structure1, structure2, a)) {
          return a;
          //System.out.println("true");
       } 
     } while (perm.inc());
     assert false;
     System.out.println("No match found");
     return null; // nothing feasible found
    }
    
    
    public static Map< List<Integer>,List<Integer> > makeMap(SecondaryStructure structure){
	Map< List<Integer>,List<Integer> > pairs = new HashMap< List<Integer>,List<Integer> >(); //(sequence pos, residue pos)
	
	InteractionSet interactions = structure.getInteractions();
	UnevenAlignment sequences = structure.getSequences();
	
	for (int i=0; i<interactions.size(); i++) {
		Interaction inter = interactions.get(i);
		
		Sequence sequence1 = (Sequence)inter.getResidue1().getParentObject();
		Sequence sequence2 = (Sequence)inter.getResidue2().getParentObject();
		
		int seq1Pos = sequences.getIndex( sequence1.getName() );
		int seq2Pos = sequences.getIndex( sequence2.getName() );
		
		int res1Pos = inter.getResidue1().getPos();
		int res2Pos = inter.getResidue2().getPos();
		
		List<Integer> pos1 = new ArrayList<Integer>(2);
		List<Integer> pos2 = new ArrayList<Integer>(2);
		
		pos1.add(seq1Pos);
		pos1.add(res1Pos);
		pos2.add(seq2Pos);
		pos2.add(res2Pos);
		
		pairs.put( pos1,pos2 );
		pairs.put( pos2,pos1 );
	}
	
	return pairs;
    }


    public static List<Stem> findHelices(SecondaryStructure structure) { //returns all lengths of base pairing in structure

	List<Stem> helices = new ArrayList<Stem>();
	
	//helicies.add(new SimpleStem( 0,0,0,new SimpleSequence("sequence","name",new SimpleAlphabet() ), new SimpleSequence() ));
	
	InteractionSet interactions = structure.getInteractions();
	
	HashMap<Residue,Residue> pairs = new HashMap<Residue,Residue>();
	for (int i=0; i<interactions.size(); i++) {
		Interaction inter = interactions.get(i);
		pairs.put( inter.getResidue1(), inter.getResidue2() );
		pairs.put( inter.getResidue2(), inter.getResidue1() );
	}
	
	Stem stem = null;
	for ( int i=0; i<structure.getSequenceCount(); i++ ){
		Sequence seq = structure.getSequence(i);
		boolean inStem = false;
		for ( int j=0; j<seq.size(); j++){
			Residue res = seq.getResidue(j);
			if(pairs.containsKey(res)){
				Residue otherRes = pairs.get(res);
				if(inStem){
					assert stem!=null;

					assert (stem.size()>0); // contiuation of stem
					Residue lastRes = stem.getResidue1( stem.size()-1 ); //last residue on stem
					Residue otherLastRes = stem.getResidue2( stem.size()-1 );
					if ( res.getPos() - lastRes.getPos() != 1 || otherLastRes.getPos() - otherRes.getPos() != 1
					 ||  otherLastRes.getParentObject() != otherRes.getParentObject()  )  { //residues are sequential
						inStem = false;
					}
				}
				if(!inStem){ //CANNOT USE ELSE (condition can change within previous if statement)
					stem = new SimpleStem( (Sequence)res.getParentObject(), (Sequence)otherRes.getParentObject() ); //start new stem
					helices.add(stem);
					inStem = true;
				}
				stem.add( new SimpleInteraction(res,otherRes,null) );  //TODO null for InteractionType (no implementation?)
				pairs.remove(otherRes);
			} else{
				inStem = false;
			}
			pairs.remove(res);
		}
	}
	//System.out.println("Helices found: "+helices.size());
	return helices;
	}
	
	/**returns interaction between the two residues (not part of strand) that are on either end*/
	public static List<Interaction> findSingleStrands(SecondaryStructure structure) {
	
	
	List<Interaction> singleStrands = new ArrayList<Interaction>();
	
	InteractionSet interactions = structure.getInteractions();
	
	HashSet<Residue> paired = new HashSet<Residue>(); //holds all residues that are paired
	for (int i=0; i<interactions.size(); i++) {
		Interaction inter = interactions.get(i);
		paired.add( inter.getResidue1() );
		paired.add( inter.getResidue2() );
	}
	
	HashMap<Residue,Residue> pairs = new HashMap<Residue,Residue>(); //TODO: remove 1 of the redundant data structures
	for (int i=0; i<interactions.size(); i++) {
		Interaction inter = interactions.get(i);
		//System.out.println(inter.getResidue1() +" : "+inter.getResidue2() );
		pairs.put( inter.getResidue1(), inter.getResidue2() );
		pairs.put( inter.getResidue2(), inter.getResidue1() );
	}
	
	
	for ( int i=0; i<structure.getSequenceCount(); i++ ){
		boolean inSingleStrand = false;
		Residue startRes = null;
		Residue stopRes = null;
		Sequence seq = structure.getSequences().getSequence(i);
		
		int firstPaired = -1;
		
		for ( int j=0; j<seq.size(); j++){
			if (paired.contains(seq.getResidue(j) )	){
				firstPaired = j;
				break;
			}
		}
		
		assert firstPaired != -1;

		for ( int j=firstPaired; j<seq.size(); j++){
		
			if ( !paired.contains(seq.getResidue(j)) ){ //if unpaired
				if(inSingleStrand ){
					assert startRes != null; //region should have already begun
					continue;
				} else
				{
					
					
					assert paired.contains(seq.getResidue(j-1));
					startRes = seq.getResidue(j-1); //previous residue
					inSingleStrand = true;
				}
				
			} else{
				if(inSingleStrand){
					assert startRes != null; //region should have already begun
					stopRes = seq.getResidue(j);
					assert stopRes.getPos() > startRes.getPos(); //first residue with pair
					assert startRes.getParentObject() == stopRes.getParentObject();
					singleStrands.add( new SimpleInteraction( startRes, stopRes, null ) );
					startRes = null;
					stopRes = null;
					inSingleStrand = false;
				} else{
					if( j>0 && paired.contains( seq.getResidue(j-1) ) ){
					
						startRes = seq.getResidue(j-1);
						
						Residue otherStartRes = pairs.get(startRes);
						
						stopRes = seq.getResidue(j);
						
						Residue otherStopRes = pairs.get(stopRes);
				
						if ( Math.abs(otherStopRes.getPos() - otherStartRes.getPos()) != 1 || 
						otherStopRes.getParentObject() != otherStartRes.getParentObject()  ) { //new helix has begun
						
							singleStrands.add( new SimpleInteraction( startRes, stopRes, null ) );
							startRes = null;
							stopRes = null;
							
							
						
						}
					}
					
				}
			}
		}
	}
	
	return singleStrands;
	}

	public static int findExtension(SecondaryStructure structure, Residue residue, boolean forward) {
	
	int length=0;
	int iterator = (forward? 1 : -1); //move forwards or backwards on strand
	
	Sequence sequence = (Sequence)residue.getParentObject();
	
	InteractionSet interactions = structure.getInteractions();
	
	HashSet<Residue> paired = new HashSet<Residue>(); //holds all residues that are paired
	for (int i=0; i<interactions.size(); i++) {
		Interaction inter = interactions.get(i);
		paired.add( inter.getResidue1() );
		paired.add( inter.getResidue2() );
	}
	
	int pos = residue.getPos();
	while( ! paired.contains( sequence.getResidue(pos) ) ){
		length++;
		pos += iterator;
		
		if(pos<1 || pos>sequence.size()-1){
			System.out.println("SecondaryStructureTools: reached end of strand without finding interaction");
			break;
		}
	}
	
	return length;
	}
  
  /** returns sec structure with only residue, sequence classes (no 3D)*/
  public static SecondaryStructure cleanCopy(SecondaryStructure structure){
    InteractionSet interactions = structure.getInteractions();
    HashMap<Residue,Residue> pairs = new HashMap<Residue,Residue>();
  	for (int i=0; i<interactions.size(); i++) {
  		Interaction inter = interactions.get(i);
  		pairs.put( inter.getResidue1(), inter.getResidue2() );
  		pairs.put( inter.getResidue2(), inter.getResidue1() );
  	}
    HashMap<Residue,Residue> clones = new HashMap<Residue,Residue>();

    int num = structure.getSequenceCount();
    UnevenAlignment ua = new SimpleUnevenAlignment();
    for(int i=0;i<num;i++){
      Sequence seq = structure.getSequence(i);
      Sequence newSeq = new SimpleSequence("strand_"+i, seq.getAlphabet());
      for(int j=0;j<seq.size();j++){
        Residue res = seq.getResidue(j);
        Residue newRes = new SimpleResidue(res.getSymbol(), newSeq, res.getPos());
        // System.out.println(res.getClass());
        // System.out.println(newRes.getClass());
        clones.put(res,newRes);
        newSeq.addResidue(newRes);
      }
      try{
        ua.addSequence(newSeq);
      } catch (Exception e){
        System.out.println(e);
      }
    }
    InteractionSet newInteractions = new SimpleInteractionSet();
    for(Residue res : pairs.keySet()){
      newInteractions.add(new SimpleInteraction( clones.get(res), clones.get(pairs.get(res)) , new RnaInteractionType(RnaInteractionType.WATSON_CRICK)));
    }
    return new SimpleSecondaryStructure(ua,newInteractions);
  }
  
  /** fixes molecules with multiple unconnected substructures*/
  public static List<Residue> findConnectedResidues (SecondaryStructure structure) {
    List<Residue> connected = new ArrayList<Residue> ();
    //base pairs
    InteractionSet interactions = structure.getInteractions();
    HashMap<Residue,Residue> pairs = new HashMap<Residue,Residue>();
    for (int i=0; i<interactions.size(); i++) {
      Interaction inter = interactions.get(i);
      pairs.put( inter.getResidue1(), inter.getResidue2() );
      pairs.put( inter.getResidue2(), inter.getResidue1() );
    }
    Map<Residue,Boolean> visited = new HashMap<Residue,Boolean>();
    //create edgelist for bfs
    Map <Residue, List <Residue>> edgeList = new HashMap <Residue, List <Residue>> ();
    for (int i = 0; i < structure.getSequenceCount (); i++) {
        Sequence seq = structure.getSequence (i);
        for (int j=0; j<seq.size();j++) {
          Residue res = seq.getResidue(j);
          List<Residue> edges = new ArrayList<Residue> ();

          if (j > 0) {
            edges.add (seq.getResidue (j-1));
          }
          if (j < seq.size () -1 ) {
            edges.add (seq.getResidue (j+1));
          }
          if (pairs.containsKey (res)) {
            edges.add (pairs.get (res));
          }
          visited.put(res,false);
          edgeList.put (res, edges);
        }
    }
    //bfs
    Queue<Residue> q = new LinkedList<Residue> ();
    q.add(structure.getSequence(0).getResidue(0));
    while (!q.isEmpty()) {
      Residue r = (q.peek ());
      if(!visited.get(r)){
        visited.put (r, true);
        connected.add(r);
        List<Residue> edges = edgeList.get(r);
        if (edges != null) {
          for (int i = 0; i < edges.size (); i++) {
            Residue n = edges.get(i);
            if (!visited.get(n)){
              q.add (n);
            }
          }
        }
      }
      q.remove(r);
    }
    return connected;
  }
  
  public static int countResidues(SecondaryStructure structure){
    int resCount = 0;
	for(int i=0; i<structure.getSequenceCount(); i++){
		Sequence seq = structure.getSequence(i);
		resCount+=seq.size();
	}
	return resCount;
  }
  
  
	
}
