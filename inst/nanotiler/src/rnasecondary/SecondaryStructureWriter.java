package rnasecondary;

public interface SecondaryStructureWriter {

    /** generates string from secondary structure */
    public String writeString(SecondaryStructure structure);

}
