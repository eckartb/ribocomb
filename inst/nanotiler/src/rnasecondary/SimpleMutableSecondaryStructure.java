package rnasecondary;

import java.util.ArrayList;
import java.util.List;

import controltools.ModelChangeEvent;
import controltools.ModelChangeListener;
import sequence.*;

/**
 * Provides a default implementation of the
 * MutableSecondaryStucture interface.
 * @author Calvin Grunewald
 */
public class SimpleMutableSecondaryStructure extends SimpleSecondaryStructure implements MutableSecondaryStructure {

    public static final String NEWLINE = System.getProperty("line.separator");
    public static final int STRAND_ADDED_EVENT = 1;
    public static final int STEM_ADDED_EVENT = 2;

    private ArrayList<ModelChangeListener> listeners =
	new ArrayList<ModelChangeListener>();

    public SimpleMutableSecondaryStructure(UnevenAlignment alignment,
				    InteractionSet interactions) {
	super(alignment, interactions);
    }

    public SimpleMutableSecondaryStructure(UnevenAlignment alignment,
				    InteractionSet interactions, List<List<Double> > weights) {
	super(alignment, interactions, weights);
    }

    public SimpleMutableSecondaryStructure() {
	super(new SimpleUnevenAlignment(), new SimpleInteractionSet());
    }
    

    public String getClassName() { return "MutableSecondaryStructure"; }

    
    public void removeInteraction(Interaction i) {
	getInteractions().remove(i);
    }



    public void clear() {
	setSequences(new SimpleUnevenAlignment());
	setInteractions(new SimpleInteractionSet());
    }

    public void addStrand(String name, String seq, Alphabet a) {
	try {
	    addSequence(new SimpleSequence(seq, name, a));
	    fireModelChanged(new ModelChangeEvent(this, STRAND_ADDED_EVENT));
	}
	catch(DuplicateNameException e1) {

	}
	catch(UnknownSymbolException e2) {

	}
    }

    /*
      public void addStem(int seqId1, int seqId2, int start, int stop, int length) {
      System.out.println(NEWLINE + NEWLINE + "Adding stem" + NEWLINE + NEWLINE);
      Sequence seq1 = getSequences().getSequence(seqId1);
      Sequence seq2 = getSequences().getSequence(seqId2);
      for(int i = 0; i < length; ++i) {
      Interaction interaction = new SimpleInteraction(seq1.getResidue(i + start), seq2.getResidue(stop - i), new RnaInteractionType(RnaInteractionType.WATSON_CRICK));
      getInteractions().add(interaction);
      }
      
      fireModelChanged(new ModelChangeEvent(this, STEM_ADDED_EVENT));
      
      }
    */

    public void addModelChangeListener(ModelChangeListener listener) {
	listeners.add(listener);
    }

    public void fireModelChanged(ModelChangeEvent event) {
	for(ModelChangeListener l : listeners) {
	    l.modelChanged(event);
	}
    }

    /*
    public void removeModelChangeListener(ModelChangeListener listener) {
	listeners.remove(listener);
    }
    */
    
}
