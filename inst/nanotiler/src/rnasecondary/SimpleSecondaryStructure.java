package rnasecondary;

import java.io.InputStream;

import generaltools.MalformedInputException;
import sequence.*;
import java.util.*;

public class SimpleSecondaryStructure implements SecondaryStructure {

    public static final double DEFAULT_WEIGHT = 1.0;

    public static final String NEWLINE = System.getProperty("line.separator");

    private UnevenAlignment sequences;

    private InteractionSet interactions;

    private double energy = 0.0;

    private boolean ignoreNotConstantMode = false ;// equivalent to variable with same name in MonteCarloSequenceOptimizer

    private List<Alphabet> alphabets = null;

    private List<List<Double> > weights = new ArrayList<List<Double> >();

    /** stores indices of "inactive" sequences. Used for sequence optimization. Careful: if indices of sequence change, these "active" indices
     * will be outdated and wrong. Adding sequences is ok but not removing them. */
    private Set<Integer> inactiveMap = new HashSet<Integer>(); 

    public SimpleSecondaryStructure(UnevenAlignment alignment,
				    InteractionSet interactions) {
	this.sequences = alignment;
	this.interactions = interactions;
	this.alphabets = generateDefaultAlphabets();
    }

    public SimpleSecondaryStructure(UnevenAlignment alignment,
				    InteractionSet interactions,
				    List<List<Double> > weights) {
	this.sequences = alignment;
	this.interactions = interactions;
	this.weights = weights;
	this.alphabets = generateDefaultAlphabets();
	assert(weightsSanityCheck());
    }
    
    public void setAlphabets(List<Alphabet> alphabets) { 
	assert((alphabets == null) || (alphabets.size() == sequences.getSequenceCount()));
	this.alphabets = alphabets;
    }

    public void addInteraction(Interaction interaction) {
	interactions.add(interaction);
    }
    
    public void addSequence(Sequence sequence) throws DuplicateNameException {
	sequences.addSequence(sequence);
    }

    public List<Alphabet> getAlphabets() { 
	assert((alphabets == null) || (alphabets.size() == getSequenceCount()));
	return alphabets;
    }

    public List<Alphabet> generateDefaultAlphabets() {
	List<Alphabet> list = new ArrayList<Alphabet>();
	for (int i = 0; i < getSequenceCount(); ++i) {
	    list.add(DnaTools.RNA_ALPHABET);
	}
	return list;
    }

    /** Ensures that weight data structures are well-defined for all residues and sequences. Set default weights to DEFAULT_WEIGHT (typically 1.0) */
    public void generateDefaultWeights() {
	weights = new ArrayList<List<Double> >();
	for (int i = 0; i < getSequenceCount(); ++i) {
	    int n = getSequence(i).size();
	    ArrayList<Double> lst= new ArrayList<Double>();
	    for (int j = 0; j < n; ++j) {
		lst.add(DEFAULT_WEIGHT);
	    }
	    assert(lst.size() == n);
	    weights.add(lst);
	}
	assert(weights.size() == getSequenceCount());
	assert(weightsSanityCheck());
    }

    /** returns true, iff weights data structures are well-defined for all residues and sequences */
    public boolean weightsSanityCheck() {
	if (weights.size() != getSequenceCount()) {
	    return false;
	}
	for (int i = 0; i < getSequenceCount(); ++i) {
	    if (weights.get(i) == null) {
		return false;
	    } else if (weights.get(i).size() != getSequence(i).size()) {
		return false;
	    }
	}
	return true;
    }
    
    /** Return weights of n'th sequence */
    public List<Double> getWeights(int n) {
	assert(weightsSanityCheck());
	assert(weights != null);
	assert(n < weights.size());
	return weights.get(n);
    }

    /** Return weights of n'th sequence */
    public void setWeights(List<Double> _weights, int n) {
	assert(weightsSanityCheck());
	assert(weights != null);
	assert(n < weights.size());
	this.weights.set(n, _weights);
	assert(weightsSanityCheck());
    }

    /** returns index of sequence to which residue belongs
     * TODO : very slow implementation ! 
     * TODO : contains BUG !!! */
    public int findSequence(Residue res) {
 	assert res != null;
 	// Sequence seq = res.getSequence();
 	// assert seq != null;
 	// String seqName = sequenceParentName(seq);
 	// assert seqName != null;
 	for (int i = 0; i < getSequenceCount(); ++i) {
	    Sequence seq = getSequence(i);
	    Residue other = seq.getResidue(0);
	    if (res.isSameSequence(other)) { // check if references are same
		return i;
	    }
 	}
 	return -1;
    }

    public int[][][][] generateInteractionMatrices() {
	int[][][][] matrices = new int[getSequenceCount()][getSequenceCount()][0][0];
	for (int i = 0; i < getSequenceCount(); ++i) {
	    for (int j = i; j < getSequenceCount(); ++j) {
		matrices[i][j] = generateInteractionMatrix(i, j);
	    }
	}
	return matrices;
    }

    /** Generates a interaction matrices for a subset of sequences. Specified is an array with zero-based sequence ids. */
    public int[][][][] generateInteractionMatrices(int[] subset) {
        int len = subset.length;
	int[][][][] matrices = new int[len][len][0][0];
	for (int i = 0; i < len; ++i) {
	    for (int j = i; j < len; ++j) {
		matrices[i][j] = generateInteractionMatrix(subset[i], subset[j]);
	    }
	}
	return matrices;
    }


    int[][] generateInteractionMatrix(int id1, int id2) {
	Sequence seq1 = getSequence(id1);
	Sequence seq2 = getSequence(id2);
	int[][] result = new int[seq1.size()][seq2.size()];
	for (int i = 0; i < result.length; ++i) {
	    for (int j = 0; j < result[i].length; ++j) {
		result[i][j] = RnaInteractionType.NO_INTERACTION;
	    }
	}
	for (int i = 0; i < getInteractionCount(); ++i) {
	    Interaction interaction = getInteraction(i);
	    int pos1 = interaction.getResidue1().getPos();
	    int pos2 = interaction.getResidue2().getPos();
	    if ((interaction.getSequence1() == seq1) && (interaction.getSequence2() == seq2)) {
		result[pos1][pos2] = interaction.getInteractionType().getSubTypeId();
		if (id1 == id2) {
		    result[pos2][pos1] = result[pos1][pos2]; // if same sequence: symmetric
		}
	    }
	    else if ((interaction.getSequence2() == seq1) && (interaction.getSequence1() == seq2)) {
		result[pos2][pos1] = interaction.getInteractionType().getSubTypeId();		
		assert id1 != id2;
	    }
	}
	// sets rows and columns to UNKNOWN_INTERACTION, if property seqstatus is "ignore"
	for (int i = 0; i < result.length; ++i) {
	    if (("ignore".equals(seq1.getResidue(i).getProperty("seqstatus")))
		|| (ignoreNotConstantMode && SequenceStatus.fragment.equals(seq1.getResidue(i).getProperty(SequenceStatus.name)))) {
		for (int j = 0; j < result[i].length; ++j) {
		    result[i][j] = RnaInteractionType.UNKNOWN_SUBTYPE;
		}
	    }
	}
	for (int i = 0; i < result[0].length; ++i) {
	    if (("ignore".equals(seq2.getResidue(i).getProperty("seqstatus"))) 
		|| (ignoreNotConstantMode && SequenceStatus.fragment.equals(seq2.getResidue(i).getProperty(SequenceStatus.name)))) {
		for (int j = 0; j < result.length; ++j) {
		    result[j][i] = RnaInteractionType.UNKNOWN_SUBTYPE;
		}
	    }
	}
	return result;
    }


    public String getClassName() { return "SecondaryStructure"; }

    public InteractionSet getInteractions() {
	return interactions;
    }

    public double getEnergy() { return energy; }

    /** Returns zero-based indices that first residues of each sequence would have,
     * if all sequences were concatenated to one sequence. */
    public int[] getStarts() {
	int[] result = new int[getSequenceCount()];
	result[0] = 0;
	for (int i = 1; i < result.length; ++i) {
	    result[i] = result[i-1] + getSequence(i-1).size();
	}
	return result;
    }

    public void setEnergy(double _energy) { this.energy = _energy; }

    public boolean isActive(int seqId) { // returns active flag of sequence
	return (!inactiveMap.contains(seqId));
    }

    public void setInActive(int seqId) {
	inactiveMap.add(seqId);
    }

    public void setActive(int seqId) {
	if (!isActive(seqId)) {
	    inactiveMap.remove(seqId);
	}
    }
    
    public int getSequenceCount() {
	return sequences.getSequenceCount();
    }
    
    public Sequence getSequence(int n) {
	return sequences.getSequence(n);
    }
	
	/** gets sequence by name */
    public Sequence getSequence(String name) throws UnknownSequenceException {
	return sequences.getSequence(name);
    }
  
    //returns entire sequence of structure
    public String getFullSequenceString () {
      String s = "";
      for (int k = 0; k < this.getSequenceCount(); k++) {
        s = s + getSequence(k).sequenceString();
      }
      return s;
    }

    public UnevenAlignment getSequences() {
	return sequences;
    }

    public int getInteractionCount() {
	return interactions.size();
    }

    public Interaction getInteraction(int n) {
	return interactions.get(n);
    }

    public void setSequences(UnevenAlignment sequences) {
	this.sequences = sequences;
    }

    public void setInteractions(InteractionSet interactions) {
	this.interactions = interactions;
    }
    
    public void read(InputStream is) throws MalformedInputException {
	PackageConventionTools.readHeader(is, getClassName());
	sequences.read(is);
	interactions.read(is);
	PackageConventionTools.readFooter(is, getClassName());
    }

    public String toString() {
	StringBuffer buf = new StringBuffer();
	buf.append("(SecondaryStructure ");
	for (int i = 0; i < sequences.getSequenceCount(); ++i) {
	    String s = sequences.getSequence(i).sequenceString();
	    buf.append(s + "\n");
	}
	buf.append(interactions.toString() + NEWLINE + ")");
	return buf.toString();
    }

    /** remove overlapping interactions according to mode */
    private void purgeOverlappingInteractions(InteractionSet overlappingInteractions,
					      List<Integer> overlappingIndices,
					      int purgeMode,
					      boolean[] removeFlags) {
	switch (purgeMode) {
	case NO_PURGE: return;
	case PURGE_ALL_OVERLAPPING:
	    for (int i = 0; i < overlappingIndices.size(); ++i) {
		removeFlags[overlappingIndices.get(i).intValue()] = true;
	    }
	    break;
	case PURGE_NONHELICAL_OVERLAPPING:
	    assert false; // not yet implemented
	}
    }

    /** remove overlapping interactions according to mode */
    public void purgeOverlappingInteractions(int purgeMode) {
	boolean[] removeFlags = new boolean[interactions.size()];
	for (int i = 0; i < interactions.size(); ++i) {
	    if (removeFlags[i]) {
		continue;
	    }
	    InteractionSet overlappingInteractions = new SimpleInteractionSet();
	    List<Integer> overlappingIndices = new ArrayList<Integer>();
	    overlappingInteractions.add(interactions.get(i));
	    overlappingIndices.add(new Integer(i));
	    for (int j = i+1; j < interactions.size(); ++j) {
		if (removeFlags[j]) {
		    continue;
		}
		if (interactions.get(i).isOverlapping(interactions.get(j))) {
		    overlappingInteractions.add(interactions.get(j));
		    overlappingIndices.add(new Integer(j));
		}
	    }
	    if (overlappingInteractions.size() > 1) {
		purgeOverlappingInteractions(overlappingInteractions, 
					     overlappingIndices,
					     purgeMode,
					     removeFlags);
	    }
	}
	// remove according to flags:
	for (int i = interactions.size() - 1; i >= 0; --i) {
	    if (removeFlags[i]) {
		interactions.remove(interactions.get(i));
	    }
	}
    }

    /** Returns interaction matrix between sequences n and m. Entries of matrix element i,j correspond to RnaInteractionType
     * between residue i of sequence m and residue j of sequence n */
    public int[][] toInteractionMatrix(int m, int n) {
	assert m < getSequenceCount();
	assert n < getSequenceCount();
	int len1 = getSequence(m).size();
	int len2 = getSequence(n).size();
	int[][] result = new int[len1][len2];
	for (int i = 0; i < result.length; ++i) {
	    for (int j = 0; j < result[i].length; ++j) {
		result[i][j] = RnaInteractionType.NO_INTERACTION;
	    }
	}
	UnevenAlignment ali = getSequences();
	for (int i = 0; i < getInteractionCount(); ++i) {
	    Interaction inter = getInteraction(i);
	    Sequence s1 = inter.getSequence1();
	    Sequence s2 = inter.getSequence2();
	    int idx1 = ali.getIndex(s1.getName());
	    int idx2 = ali.getIndex(s2.getName());
	    assert idx1 >= 0;
	    assert idx2 >= 0;
	    Residue res1 = inter.getResidue1();
	    Residue res2 = inter.getResidue2();
	    if ((idx1 == m) && (idx2 == n)) {
		result[res1.getPos()][res2.getPos()] = inter.getInteractionType().getSubTypeId();
		if (m == n) {
		    result[res2.getPos()][res1.getPos()] = result[res1.getPos()][res2.getPos()];
		}
	    }
	    else if ((idx1 == n) && (idx2 == m)) {
		result[res2.getPos()][res1.getPos()] = inter.getInteractionType().getSubTypeId();
		if (m == n) {
		    result[res1.getPos()][res2.getPos()] = result[res2.getPos()][res1.getPos()];
		}

	    }
	}
	assert result.length == len1;
	assert result[0].length == len2;
	return result;
    }

}
