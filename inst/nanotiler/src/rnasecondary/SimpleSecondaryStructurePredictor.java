package rnasecondary;

import java.util.*;
import generaltools.*;
import sequence.*;

import org.testng.annotations.*;

/** Computes secondary structure by simply setting lowest energy stems first. */
public class SimpleSecondaryStructurePredictor extends ResultWorker {

    private UnevenAlignment ali;
    private double gcEnergy = -1.0;
    private double auEnergy = -0.8;
    private Alphabet alphabet = DnaTools.AMBIGUOUS_RNA_ALPHABET;
    private Sequence[] sequences;
    private int stemLengthMin = 3;
    private int stopOffset = 3;

    public SimpleSecondaryStructurePredictor(UnevenAlignment ali) { 
	assert ali != null;
	this.ali = ali;
	this.sequences = getSequences(ali);
    }
    
    private Sequence[] getSequences(UnevenAlignment ali) {
	Sequence[] sequences = new Sequence[ali.getSequenceCount()];
	for (int i = 0; i < ali.getSequenceCount(); ++i) {
	    sequences[i] = ali.getSequence(i);
	}
	return sequences;
    }

    private double computeStemEnergy(Stem stem) {
	assert stem.isValid();
	String s1 = stem.getSequence1().sequenceString();
	String s2 = stem.getSequence2().sequenceString();
	int start = stem.getStartPos();
	int stop = stem.getStopPos();
	int len = stem.size();
	double result = 0.0;
	for (int i = 0; i < len; ++i) {
	    int p1 = start + i;
	    int p2 = stop - i;
	    char c1 = s1.charAt(p1);
	    char c2 = s2.charAt(p2);
	    if (RnaSecondaryTools.isWatsonCrick(c1, c2)) {
		if (c2 < c1) { // swap
		    char help = c2;
		    c2 = c1;
		    c1 = help;
		}
		if (c1 == 'A') {
		    result += auEnergy;
		}
		else if (c1 == 'C') {
		    result += gcEnergy;
		}
		else {
		    assert false; // should be AU or GC
		}
	    }
	}
	assert result <= 0.0;
	return result;
    }
    

    private List<Stem> generateStemList(Sequence s1, Sequence s2) {
	assert s1 != s2;
	// generate all stems:
	int maxLen = s1.size();
	if (s2.size() < maxLen) {
	    maxLen = s2.size();
	}
	String s1s = s1.sequenceString();
	String s2s = s2.sequenceString();
	List<Stem> result = new ArrayList<Stem>();
	for (int start = 0; start < (s1.size() - stemLengthMin + 1); ++start) {
	    for (int stop = s2.size()-1; stop >= (stemLengthMin - 1); --stop) {
		for (int len = 1; len <= maxLen; ++len) {
		    if (SimpleStem.isValid(start, stop, len, s1.size(), s2.size()) 
			&& (RnaSecondaryTools.isWatsonCrick(s1s.charAt(start + len - 1),
							    s2s.charAt(stop - len + 1)))) {
			if (len >= stemLengthMin) {
			    Stem stem = new SimpleStem(start, stop, len, s1, s2);
			    assert stem.isValid();
			    assert stem.isComplementary();
			    stem.setEnergy(computeStemEnergy(stem)); // set energy
			    result.add(stem);
			}
		    }
		    else {
			break; // no need to explore stem further for longer lengths
		    }
		}
	    }
	}
	return result;
    }

    private List<Stem> generateStemList(Sequence s1) {
	// generate all stems:
	int maxLen = s1.size();
	List<Stem> result = new ArrayList<Stem>();
	String s1s = s1.sequenceString();
	for (int start = 0; start < s1.size() - stemLengthMin + 1; ++start) {
	    for (int stop = s1.size()-1; stop >= start + stopOffset; --stop) {
		for (int len = 1; len <= maxLen; ++len) {
		    int p1 = start + len - 1;
		    int p2 = stop - len + 1;
		    if (((p1 + stopOffset) < p2) && SimpleStem.isValid(start, stop, len, s1.size(), s1.size())
			&& RnaSecondaryTools.isWatsonCrick(s1s.charAt(p1), 
							   s1s.charAt(p2))) {
			if (len >= stemLengthMin) {
			    Stem stem = new SimpleStem(start, stop, len, s1, s1);
			    assert stem.isValid();
			    assert (stem.isComplementary());
			    stem.setEnergy(computeStemEnergy(stem)); // set energy
			    result.add(stem);
			} // otherwise do nothing, stem might be added next round
		    }
		    else {
			break;
		    }
		}
	    }
	}
	return result;
    }

    /** Generates stem list sorted by energy */
    private List<Stem> generateStemList(Sequence[] sequences) {
	List<Stem> result = new ArrayList<Stem>();
	for (int i = 0; i < sequences.length; ++i) {
	    result.addAll(generateStemList(sequences[i]));
	    for (int j = i + 1; j < sequences.length; ++j) {
		result.addAll(generateStemList(sequences[i], sequences[j]));
	    }
	}
	Collections.sort(result);
	return result;
    }

    private boolean isConflicting(Stem stem, List<Stem> stemList) {
	for (int i = 0; i < stemList.size(); ++i) {
	    if (stemList.get(i).isConflicting(stem)) {
		return true;
	    }
	}
	return false; // no conflicts!
    }

    protected void runInternal() {
	List<Stem> stemList = generateStemList(this.sequences);
	List<Stem> placedStems = new ArrayList<Stem>(); // lowest energy stems are first
	for (int i = 0; i < stemList.size(); ++i) {
	    Stem stem = stemList.get(i);
	    if (!isConflicting(stem, placedStems)) {
		placedStems.add(stem);
	    }
	}
	InteractionSet interactionSet = new SimpleInteractionSet();
	// get all interactions of all placed stems:
	for (int i = 0; i < placedStems.size(); ++i) {
	    for (int j = 0; j < placedStems.get(i).size(); ++j) {
		interactionSet.add(placedStems.get(i).get(j));
	    }
	}
	setResult(new SimpleSecondaryStructure(ali, interactionSet));
    }

}
