package rnasecondary;

import java.io.InputStream;

import numerictools.IntervalInt;
import rnasecondary.RnaInteractionType;
import sequence.Residue;
import sequence.Sequence;

/** an RNA stem is modeled here not as an interaction but as a set of interactions */
public class SimpleStem implements Stem {
    
    private Sequence sequence1;
    private Sequence sequence2;
    private InteractionSet interactions;
    private double energy = 0.0;

    public static final String CLASS_NAME = "STEM";

    /** used for clone method */
    private SimpleStem() { }

    public SimpleStem(int startPos, int stopPos, int length,
		      Sequence sequence1, Sequence sequence2) {
	this.sequence1 = sequence1;
	this.sequence2 = sequence2;
	interactions = new SimpleInteractionSet();
	for (int i = 0; i < length; ++i) {
	    interactions.add(new SimpleInteraction(sequence1.getResidue(startPos + i), 
						   sequence2.getResidue(stopPos - i), 
		   new RnaInteractionType(RnaInteractionType.WATSON_CRICK)));
	}
    }

    /** incomple constructor. Only useful if interactions are added later with the method "add" */
    public SimpleStem(Sequence sequence1, Sequence sequence2) {
	this.sequence1 = sequence1;
	this.sequence2 = sequence2;
	interactions = new SimpleInteractionSet();
    }

    public void add(Interaction interaction) {
	interactions.add(interaction);
	interactions.sort();
    }

    public void clear() { 
	interactions.clear();
	sequence1 = null;
	sequence2 = null;
    }

    /** TODO : clone of sequence is still shallow ! */
    public Object clone() {
	SimpleStem stem = new SimpleStem();
	stem.sequence1 = this.sequence1;
	stem.sequence2 = this.sequence2;
	stem.interactions = (InteractionSet)(interactions.clone());
	stem.energy = this.energy;
	return stem;
    }

    /** Sorts stems by energy */
    public int compareTo(Stem otherStem) {
	// public int compareTo(Object other) {
	// assert other instanceof Stem;
	// Stem otherStem = (Stem)other;
	double diff = this.getEnergy() - otherStem.getEnergy();
	if (diff < 0.0) {
	    return -1;
	}
	else if (diff > 0.0) {
	    return 1;
	}
	return 0;
    }

    /** generates stem with identities of strand 1 and 2 reversed */
    public Stem generateReverseStem() {
	Stem result = new SimpleStem(getStopPos()-size()+1, getStartPos() + size() - 1, size(), sequence2, sequence1);
	result.setEnergy(this.energy);
	return result;
    }

    public double getEnergy() { return energy; }

    public String getClassName() { return CLASS_NAME; }

    /** returns sequence corresponding to getResidue1() */
    public Sequence getSequence1() { return sequence1; }

    /** returns sequence corresponding to getResidue2() */
    public Sequence getSequence2() { return sequence2; }

    /** returns true if other stem occupies some of the same bases.
     * TODO : check if system works fine. Assumes that user has check if both stems have same sequences, otherwise the test is meaningless.
     * Imperative that sequences are NOT cloned, otherwise no conflicts can be detected! */
//     public boolean isConflictingSingleSequence(Stem other) {
// 	assert isSingleSequence();
// 	assert other.isSingleSequence();
// 	// assert getSequence1().isProbablyIdentical(other.getSequence1());
// 	assert getSequence1() == other.getSequence1();
// 	IntervalInt startInterval = new IntervalInt(getStartPos(), getStartPos()+size()-1);
// 	IntervalInt stopInterval = new IntervalInt(getStopPos()-size()+1, getStopPos());
// 	IntervalInt otherStartInterval = new IntervalInt(other.getStartPos(), 
// 							 other.getStartPos()+other.size()-1);
// 	IntervalInt otherStopInterval = new IntervalInt(other.getStopPos()-other.size()+1, other.getStopPos());
// 	if (isSingleSequence() && other.isSingleSequence() && (getSequence1().isProbablyIdentical(other.getSequence1()))) {
// 	    boolean result = (startInterval.isOverlapping(otherStartInterval)
// 			      || startInterval.isOverlapping(otherStopInterval)
// 			      || stopInterval.isOverlapping(otherStartInterval)
// 			      || stopInterval.isOverlapping(otherStopInterval));
// // 	    if (result) {
// // 	    }
// 	    return result;
// 	}
// 	// if (getSequence1().isProbablyIdentical(other.getSequence1())) {
// 	if (startInterval.isOverlapping(otherStartInterval)) {
// 	    return true;
// 	} 
// 	// }
// 	// if (getSequence2().isProbablyIdentical(other.getSequence2())) {
// 	if (stopInterval.isOverlapping(otherStopInterval)) {
// 	    return true;
// 	} 
// 	// }
// 	// if (getSequence1().isProbablyIdentical(other.getSequence2())) {
// 	if (startInterval.isOverlapping(otherStopInterval)) {
// 	    return true;
// 	} 
// 	// }
// 	// if (getSequence2().isProbablyIdentical(other.getSequence1())) {
// 	if (stopInterval.isOverlapping(otherStartInterval)) {
// 	    return true;
// 	} 
// 	// }
// 	return false;
//     }

    /** returns true if other stem occupies some of the same bases.
     * TODO : check if system works fine. Assumes that user has check if both stems have same sequences, otherwise the test is meaningless.
     * Imperative that sequences are NOT cloned, otherwise no conflicts can be detected! */
//     public boolean isConflictingTwoSequences(Stem other) {
// 	// assert false; // not yet read!!!
// 	// assert (getSequence1().isProbablyIdentical(other.getSequence1()));
// 	// assert (getSequence2().isProbablyIdentical(other.getSequence2()));
// 	// assert (!(getSequence1().isProbablyIdentical(getSequence2())));
// 	// assert (!(other.getSequence1().isProbablyIdentical(other.getSequence2())));
// 	IntervalInt startInterval = new IntervalInt(getStartPos(), getStartPos()+size()-1);
// 	IntervalInt stopInterval = new IntervalInt(getStopPos()-size()+1, getStopPos());
// 	IntervalInt otherStartInterval = new IntervalInt(other.getStartPos(), 
// 							 other.getStartPos()+other.size()-1);
// 	IntervalInt otherStopInterval = new IntervalInt(other.getStopPos()-other.size()+1, other.getStopPos());
// 	if (isSingleSequence() && other.isSingleSequence() && (getSequence1().isProbablyIdentical(other.getSequence1()))) {
// 	    boolean result = (startInterval.isOverlapping(otherStartInterval)
// 			      || startInterval.isOverlapping(otherStopInterval)
// 			      || stopInterval.isOverlapping(otherStartInterval)
// 			      || stopInterval.isOverlapping(otherStopInterval));
// // 	    if (result) {
// // 	    }
// 	    return result;
// 	}
// 	if (getSequence1() == other.getSequence1()) {
// 	    if (startInterval.isOverlapping(otherStartInterval)) {
// 		return true;
// 	    } 
// 	}
// 	if (getSequence2() == other.getSequence2()) {
// 	    if (stopInterval.isOverlapping(otherStopInterval)) {
// 		return true;
// 	    } 
// 	}
// 	return false;
//     }

    /** Returns true if all base pairs are Watson-Crick; does not allow GU */
    public boolean isComplementary() {
	assert isValid();
	String s1 = getSequence1().sequenceString();
	String s2 = getSequence2().sequenceString();
	int start = getStartPos();
	int stop = getStopPos();
	int len = size();
	for (int i = 0; i < len; ++i) {
	    int p1 = start + i;
	    int p2 = stop - i;
	    if (!RnaSecondaryTools.isWatsonCrick(s1.charAt(p1), s2.charAt(p2))) {
		return false;
	    }
	}
	return true;
    }

    public boolean isConflicting(Stem other) {
// 	if (isSingleSequence() && other.isSingleSequence()
// 	    && getSequence1().isProbablyIdentical(other.getSequence1())) {
// 	    return isConflictingSingleSequence(other);
// 	}
// 	return isConflictingTwoSequences(other);
	IntervalInt startInterval = new IntervalInt(getStartPos(), getStartPos()+size()-1);
	IntervalInt stopInterval = new IntervalInt(getStopPos()-size()+1, getStopPos());
	IntervalInt otherStartInterval = new IntervalInt(other.getStartPos(), 
							 other.getStartPos()+other.size()-1);
	IntervalInt otherStopInterval = new IntervalInt(other.getStopPos()-other.size()+1, other.getStopPos());
	if (getSequence1() == other.getSequence1()) {
	    if (startInterval.isOverlapping(otherStartInterval)) {
		return true;
	    } 
	}
	if (getSequence1() == other.getSequence2()) {
	    if (startInterval.isOverlapping(otherStopInterval)) {
		return true;
	    } 
	}
	if (getSequence2() == other.getSequence1()) {
	    if (stopInterval.isOverlapping(otherStartInterval)) {
		return true;
	    } 
	}
	if (getSequence2() == other.getSequence2()) {
	    if (stopInterval.isOverlapping(otherStopInterval)) {
		return true;
	    } 
	}
	return false;
    }

    /** returns true if current parameters are logically possible */
    public boolean isValid() {
	if ((getStartPos() >= sequence1.size())
	    || (getStopPos() >= sequence2.size())
	    || (getStartPos()+(size()-1) >= sequence1.size())
	    || (getStopPos()-(size()-1) < 0) ) {
	    return false;
	}
	return true;
    }

    /** returns true if current parameters are logically possible */
    public static boolean isValid(int _start, int _stop, int _length, 
				  int _len1, int _len2) {
	if ((_start >= _len1)
	    || (_stop >= _len2)
	    || (_start+(_length-1) >= _len1)
	    || (_stop-(_length-1) < 0) ) {
	    return false;
	}
	return true;
    }

    /** returns true if interactions are only between one sequence. In this case, getSequence1 and getSequence2
     * point to the same object 
     * TODO : check if implementation is correct!
     */
    public boolean isSingleSequence() {
	return sequence1 == sequence2; 
    }

	/** Rerturn true, iff residue is 5' end of first strand */
	public boolean isFivePrime1(Residue res) {
	    if (size() == 0) {
	    	return false;
	    }
		boolean result = (getResidue1(0) == res);		
		assert (!result) || (size() < 2) || ((res.getPos() + 1) == getResidue1(1).getPos());
		return result;
	}
	
		/** Rerturn true, iff residue is 5' end of second strand */
	public boolean isFivePrime2(Residue res) {
	    if (size() == 0) {
	    	return false;
	    }
		boolean result = (getResidue2(size()-1) == res);		
		assert (!result) || (size() < 2) || ((res.getPos() + 1) ==  getResidue2(size()-2).getPos());
		return result;
	}


	/** Rerturn true, iff residue is 3' end of first strand */
	public boolean isThreePrime1(Residue res) {
	    if (size() == 0) {
	    	return false;
	    }
		boolean result = (getResidue1(size()-1) == res);		
		assert (!result) || (size() < 2) || (res.getPos() == ( getResidue1(size()-2).getPos() +1));
		return result;
	}
	
		/** Rerturn true, iff residue is 3' end of second strand */
	public boolean isThreePrime2(Residue res) {
	    if (size() == 0) {
	    	return false;
	    }
		boolean result = (getResidue2(0) == res);		
		assert (!result) || (size() < 2) || (res.getPos() == (getResidue2(1).getPos()+1));
		return result;
	}

    /** returns length of stem */
    public int size() { return interactions.size(); }

    /** get n'th interaction (0 <= n < getLength()) */
    public Interaction get(int n) { return interactions.get(n); }

    /** start position on sequence one */
    public Residue getResidue1(int n) { return get(n).getResidue1(); }

    /** start position on sequence one */
    public Residue getResidue2(int n) { return get(n).getResidue2(); }

    /** returns 5' nucleotide of n'th interaction */
    public Residue getStartPos(int n) { return get(n).getResidue1(); }

    /** position bonding with start pos (on sequence getSequence2()) */
    public Residue getStopPos(int n) { return get(n).getResidue2(); }

    /** returns 5' nucleotide of n'th interaction with respect to its sequence (not global counting of residue ids) */
    public int getStartPos() { return get(0).getResidue1().getPos(); }

    /** position bonding with start pos (on sequence getSequence2()) */
    public int getStopPos() { return get(0).getResidue2().getPos(); }

    /** TODO : read method not yet implemented! */
    public void read(InputStream is) {
	// TODO 
    }

    /** removes interaction */
    public void remove(Interaction interaction) {
	interactions.remove(interaction);
    }

    public void setEnergy(double energy) { this.energy = energy; }

    /** sorts internal interactions such that smallest index is first */
    public void sort() {
	interactions.sort();
    }

    public String toString() {
	String result = "(Stem " + sequence1.getName() + " " 
	    + sequence2.getName() + " ";
	result = result + (getStartPos()+1) + " " + (getStopPos()+1) + " " + interactions.size() + " )";
	return result;
    }

}
