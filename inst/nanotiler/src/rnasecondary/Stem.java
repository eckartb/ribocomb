package rnasecondary;

import sequence.Residue;
import sequence.Sequence;

/** an RNA stem is modeled here not as an interaction but as a set of interactions */
public interface Stem extends InteractionSet, Comparable<Stem> {

    public Object clone();

    /** generates stem with identities of strand 1 and 2 reversed */
    public Stem generateReverseStem();

    /** Returns energy. */
    public double getEnergy();

    /** returns sequence corresponding to getResidue1() */
    public Sequence getSequence1();

    /** returns sequence corresponding to getResidue2() */
    public Sequence getSequence2();

    /** returns true if other stem occupies some of the same bases */
    // public boolean isConflictingSingleSequence(Stem otherStem);

    /** returns true if other stem occupies some of the same bases */
    // public boolean isConflictingTwoSequences(Stem otherStem);

    /** Returns true if all base pairs are Watson-Crick; does not allow GU */
    public boolean isComplementary();

    /** Returns true if two stems have at least one base in common. */
    public boolean isConflicting(Stem other);

    /** returns true if current parameters are logically possible */
    public boolean isValid();

    /** returns true if interactions are only between one sequence. In this case, getSequence1 and getSequence2
     * point to the same object */
    public boolean isSingleSequence();

    /** Sets energy. */
    public void setEnergy(double energy);

    /** returns length of stem */
    public int size();

    /** get n'th interaction (0 <= n < getLength()) */
    public Interaction get(int n);

    /** start position on sequence one */
    public Residue getResidue1(int n);

    /** start position on sequence one */
    public Residue getResidue2(int n);

    /** position bonding with start pos (on sequence getSequence1()) */
    public Residue getStartPos(int n);

    /** position bonding with start pos (on sequence getSequence2()) */
    public Residue getStopPos(int n);

    /** position bonding with start pos (on sequence getSequence1()) */
    public int getStartPos();

    /** position bonding with start pos (on sequence getSequence2()) */
    public int getStopPos();

	public boolean isFivePrime1(Residue res);
	
		/** Rerturn true, iff residue is 5' end of second strand */
	public boolean isFivePrime2(Residue res);


	/** Rerturn true, iff residue is 3' end of first strand */
	public boolean isThreePrime1(Residue res);
	
		/** Rerturn true, iff residue is 3' end of second strand */
	public boolean isThreePrime2(Residue res);

}
