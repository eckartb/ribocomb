package rnasecondary.substructures;

import java.util.*;
import sequence.*;
import rnasecondary.*;

public class FindBulge extends FindSubstructure{

  public FindBulge(){
  }
  
  public FindBulge(SecondaryStructure structure){
    super(structure);
  }
  
  @Override
  public void run(SecondaryStructure structure){
    FindSubstructure fs = new FindSingleStrand(structure);
    run(structure, fs);
  }
  
  public void run(SecondaryStructure structure, FindSubstructure fs){
    clear();
    
    List<Residue[]> otherEdges = fs.getEdges();
    
    InteractionSet interactions = structure.getInteractions();
    HashMap<Residue,Residue> pairs = new HashMap<Residue,Residue>(); //TODO: remove 1 of the redundant data structures
    for (int i=0; i<interactions.size(); i++) {
      Interaction inter = interactions.get(i);
      pairs.put( inter.getResidue1(), inter.getResidue2() );
      pairs.put( inter.getResidue2(), inter.getResidue1() );
    }
    
    for( int i=0; i<otherEdges.size(); i++){
      Residue[] ends = otherEdges.get(i);
      if(ends.length==2){
        if(pairs.get(ends[0]) != ends[1]){ //specifies bulge
          structures.add(fs.getStructures().get(i));
          residues.add(fs.getResidues().get(i));
          edges.add(fs.getEdges().get(i));
          List<Substructure> chil = new ArrayList<Substructure>();
          chil.add(fs.getSubstructures().get(i));
          children.add(chil);
        }
      }
    }  
  }
}
