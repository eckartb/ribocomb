package rnasecondary.substructures;

/* extend doesn't fuction like other imports, needs to be in database */

import java.util.*;
import sequence.*;
import rnasecondary.*;

public class FindDanglingEnd extends FindSubstructure{
  
  public FindDanglingEnd(){
  }
  
  public FindDanglingEnd(SecondaryStructure structure){
    super(structure);
  }
  
  @Override
  public void run(SecondaryStructure structure){
    clear();
    
    InteractionSet interactions = structure.getInteractions();
    HashSet<Residue> paired = new HashSet<Residue>(); //holds all residues that are paired
    for (int i=0; i<interactions.size(); i++) {
      Interaction inter = interactions.get(i);
      paired.add( inter.getResidue1() );
      paired.add( inter.getResidue2() );
    }
    
    for(int i=0; i<structure.getSequenceCount(); i++){
      Sequence seq = structure.getSequence(i);
      if(!paired.contains(seq.getResidue(0))){
        for(int j=0;j<seq.size();j++){
          if(paired.contains(seq.getResidue(j))){
            add(seq.getResidue(j), seq.getResidue(0));
            break;
          }
        }
      }
      int last = seq.size()-1;
      if(!paired.contains(seq.getResidue(last))){
        for(int j=last;j>-1;j--){
          if(paired.contains(seq.getResidue(j))){
            add(seq.getResidue(j), seq.getResidue(last));
            break;
          }
        }
      }
    }

  }
  
  private void add(Residue startRes, Residue stopRes){
    Residue[] ends = {startRes};
    edges.add(ends);
    int startPos = startRes.getPos();
    int stopPos = stopRes.getPos();
    Sequence seq = (Sequence)startRes.getParentObject();
    Sequence newSeq = new SimpleSequence(seq.getName(), seq.getAlphabet());
    int dir = 1;
    if(startPos>stopPos){ //direction for adding residues
      dir = -1;
    }
    List<Residue> reses = new ArrayList<Residue>();
    for(int i=startPos; (dir==1? i<=stopPos : i>=stopPos); i+=dir){
      Residue res = seq.getResidue(i);
      reses.add(res);
      Residue newRes = (Residue)res.cloneDeep();
      newRes.setParentObject(newSeq);
      newSeq.addResidue(newRes);
    }
    UnevenAlignment ua = new SimpleUnevenAlignment();
    try{
      ua.addSequence(newSeq);
    } catch (DuplicateNameException dne){
      System.out.println("FindDanglingEnd: duplicate names in singleStrands");
    }
    SecondaryStructure newStructure = new SimpleSecondaryStructure(ua, new SimpleInteractionSet());
    structures.add(newStructure);
    residues.add(reses);
    children.add(new ArrayList<Substructure>());
  }

}
