package rnasecondary.substructures;

import java.util.*;
import sequence.*;
import rnasecondary.*;

public class FindHelix extends FindSubstructure{
  
  public FindHelix(){
  }
  
  public FindHelix(SecondaryStructure structure){
    super(structure);
  }
  
  @Override
  public void run(SecondaryStructure structure){
    clear();
    
    List<Stem> helices = new ArrayList<Stem>();
    
    //helicies.add(new SimpleStem( 0,0,0,new SimpleSequence("sequence","name",new SimpleAlphabet() ), new SimpleSequence() ));
    
    InteractionSet interactions = structure.getInteractions();
    
    HashMap<Residue,Residue> pairs = new HashMap<Residue,Residue>();
    for (int i=0; i<interactions.size(); i++) {
      Interaction inter = interactions.get(i);
      pairs.put( inter.getResidue1(), inter.getResidue2() );
      pairs.put( inter.getResidue2(), inter.getResidue1() );
    }
    
    Stem stem = null;
    for ( int i=0; i<structure.getSequenceCount(); i++ ){
      Sequence seq = structure.getSequence(i);
      boolean inStem = false;
      for ( int j=0; j<seq.size(); j++){
        Residue res = seq.getResidue(j);
        if(pairs.containsKey(res)){
          Residue otherRes = pairs.get(res);
          if(inStem){
            assert stem!=null;

            assert (stem.size()>0); // contiuation of stem
            Residue lastRes = stem.getResidue1( stem.size()-1 ); //last residue on stem
            Residue otherLastRes = stem.getResidue2( stem.size()-1 );
            if ( res.getPos() - lastRes.getPos() != 1 || otherLastRes.getPos() - otherRes.getPos() != 1
             ||  otherLastRes.getParentObject() != otherRes.getParentObject()  )  { //residues are sequential
              inStem = false;
            }
          }
          if(!inStem){ //CANNOT USE ELSE (condition can change within previous if statement)
            stem = new SimpleStem( (Sequence)res.getParentObject(), (Sequence)otherRes.getParentObject() ); //start new stem
            helices.add(stem);
            inStem = true;
          }
          stem.add( new SimpleInteraction(res,otherRes,null) );  //TODO null for InteractionType (no implementation?)
          pairs.remove(otherRes);
        } else{
          inStem = false;
        }
        pairs.remove(res);
      }
    }
    
    for(int i=0; i<helices.size(); i++){
      stem = helices.get(i);
      int startPos = stem.getStartPos();
      int stopPos = stem.getStopPos();
      int size = stem.size();
      // System.out.println("helix: "+size);
      Sequence seq1 = stem.getSequence1();
      Sequence seq2 = stem.getSequence2();
      Residue startRes1 = stem.get(0).getResidue1();
      Residue startRes2 = stem.get(size-1).getResidue2();
      Residue stopRes1 = stem.get(size-1).getResidue1();
      Residue stopRes2 = stem.get(0).getResidue2();
      if(size==1){
        stopRes1=null;
        stopRes2=null;
      }
      // Residue startRes1 = seq1.getResidue(startPos);
      // Residue stopRes1 = seq1.getResidue(startPos+size-1);
      // Residue startRes2 = seq2.getResidue(stopPos);
      // Residue stopRes2 = seq2.getResidue(startPos);
      Residue[] ends = {startRes1,stopRes1,startRes2,stopRes2};
      edges.add(ends);
      Sequence newSeq1 = new SimpleSequence("helix1", seq1.getAlphabet());
      Sequence newSeq2 = new SimpleSequence("helix2", seq2.getAlphabet());
      SimpleInteractionSet inters = new SimpleInteractionSet();
      List<Residue> reses = new ArrayList<Residue>();
      
      for(int j=0; j<size; j++){
        Residue res1 = stem.get(j).getResidue1();
        reses.add(res1);
        Residue newRes1 = (Residue)res1.cloneDeep();
        newRes1.setParentObject(newSeq1);
        newSeq1.addResidue(newRes1);
        
        Residue res2 = stem.get(j).getResidue2();
        reses.add(res2);
        Residue newRes2 = (Residue)res2.cloneDeep();
        newRes2.setParentObject(newSeq2);
        newSeq2.addResidue(newRes2);
        
        inters.add(new SimpleInteraction(newRes1, newRes2, new RnaInteractionType(RnaInteractionType.WATSON_CRICK)));

      }
      // for(int j=0; j<size; j++){
      //   inters.add(new SimpleInteraction(stem.get(j).getResidue1(), stem.get(j).getResidue2(), new RnaInteractionType(RnaInteractionType.WATSON_CRICK)));
      // }
      
      UnevenAlignment ua = new SimpleUnevenAlignment();
      try{
        ua.addSequence(newSeq1);
        ua.addSequence(newSeq2);
      } catch (DuplicateNameException dne){
        System.out.println("FindHelix: duplicate names");
      }
      SecondaryStructure newStructure = new SimpleSecondaryStructure(ua, inters);
      structures.add(newStructure);
      residues.add(reses);
      children.add(new ArrayList<Substructure>());
    }
  }
}
