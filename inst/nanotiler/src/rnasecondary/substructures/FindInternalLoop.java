package rnasecondary.substructures;

import java.util.*;
import sequence.*;
import rnasecondary.*;

public class FindInternalLoop extends FindSubstructure{
  
  public FindInternalLoop(){}
  
  public FindInternalLoop(SecondaryStructure structure){
    super(structure);
  }
  
  @Override
  public void run(SecondaryStructure structure){
    FindSubstructure fs = new FindBulge(structure);
    run(structure, fs);
  }

  public void run(SecondaryStructure structure, FindSubstructure fs){
    clear();
    
    InteractionSet interactions = structure.getInteractions();
    HashMap<Residue,Residue> pairs = new HashMap<Residue,Residue>();
    for (int i=0; i<interactions.size(); i++) {
      Interaction inter = interactions.get(i);
      pairs.put( inter.getResidue1(), inter.getResidue2() );
      pairs.put( inter.getResidue2(), inter.getResidue1() );
    }

    List<Residue[]> singleStrands = fs.getEdges();

    //search for complementary single strand (end residues are paired)
    for(int i=0; i<singleStrands.size(); i++){
      Residue res1 = singleStrands.get(i)[0];
      Residue res2 = singleStrands.get(i)[1];
      Residue otherRes1 = pairs.get(res1);
      Residue otherRes2 = pairs.get(res2);
      
      Residue[] int1 = singleStrands.get(i);
      if(Math.abs(int1[1].getPos()-int1[0].getPos())==1){ //check for length 2 bulge (imaginary linker)
        continue;
      }
      Sequence seq1 = (Sequence)int1[0].getParentObject();
      int startPos1 = int1[0].getPos();
      int stopPos1 = int1[1].getPos();
      if(startPos1 > stopPos1){
        System.out.println("FindInternalLoop: interaction 1 positions out of order");
      }
            
      for(int j=i+1; j<singleStrands.size(); j++){ //check all later strands
        Residue res1_ = singleStrands.get(j)[0];
        Residue res2_ = singleStrands.get(j)[1];
        if( (otherRes1==res1_ && otherRes2 == res2_) || (otherRes1==res2_ && otherRes2==res1_) ){ //found internal loop
          
          //put all residues
          List<Residue> loop = new ArrayList<Residue>();          
          System.out.println(residues+" "+loop);
          Residue[] int2 = singleStrands.get(j);
          if(Math.abs(int2[1].getPos()-int2[0].getPos())==1){ //check for length 2 bulge (imaginary linker)
            continue;
          }
          Sequence seq2 = (Sequence)int2[0].getParentObject();
          int startPos2 = int2[0].getPos();
          int stopPos2 = int2[1].getPos();
          if(startPos1 > stopPos1){
            System.out.println("SecondaryStructureTools: interaction 2 positions out of order");
          }
          for(int k=startPos1; k<stopPos1+1; k++){
            loop.add( (Residue)seq1.getResidue(k) );
          }
          for(int k=startPos2; k<stopPos2+1; k++){
          loop.add( (Residue)seq2.getResidue(k) );
          }
          System.out.println(residues+" "+loop);
          residues.add(loop);
          
          //save children
          List<Substructure> chil = new ArrayList<Substructure>();
          chil.add(fs.getSubstructures().get(i));
          chil.add(fs.getSubstructures().get(j));
          children.add(chil);
                  
          //edges        
          edges.add( new Residue[]{res1, res2, res1_, res2_} );
      
          //clone substructure  
          Sequence newSeq1 = new SimpleSequence("internalLoopStrand1", seq1.getAlphabet());
          newSeq1.clear();
          for(int k=startPos1; k<stopPos1+1; k++){
            Residue newRes = (Residue)seq1.getResidue(k).cloneDeep();
            newRes.setParentObject(newSeq1);
            newSeq1.addResidue( newRes );
          }
          Sequence newSeq2 = new SimpleSequence("internalLoopStrand2", seq2.getAlphabet());
          newSeq2.clear();
          for(int k=startPos2; k<stopPos2+1; k++){
            Residue newRes = (Residue)seq2.getResidue(k).cloneDeep();
            newRes.setParentObject(newSeq2);
            newSeq2.addResidue( newRes );
          }
          
          UnevenAlignment ua = new SimpleUnevenAlignment();
          try{
            ua.addSequence(newSeq1);
            ua.addSequence(newSeq2);
          } catch (DuplicateNameException dne){
            System.out.println("FindInternalLoop: duplicate names in internal loops");
          }
          
          InteractionSet interactionSet = new SimpleInteractionSet();
          if(otherRes1==res1_ && otherRes2 == res2_){
            interactionSet.add(new SimpleInteraction( newSeq1.getResidue(0), newSeq2.getResidue(0) , new RnaInteractionType(RnaInteractionType.WATSON_CRICK)));
            interactionSet.add(new SimpleInteraction( newSeq1.getResidue(newSeq1.size()-1), newSeq2.getResidue(newSeq2.size()-1) , new RnaInteractionType(RnaInteractionType.WATSON_CRICK)));

          } else
          if(otherRes1==res2_ && otherRes2==res1_){
            interactionSet.add(new SimpleInteraction( newSeq1.getResidue(0), newSeq2.getResidue(newSeq2.size()-1) , new RnaInteractionType(RnaInteractionType.WATSON_CRICK)));
            interactionSet.add(new SimpleInteraction( newSeq1.getResidue(newSeq1.size()-1), newSeq2.getResidue(0) , new RnaInteractionType(RnaInteractionType.WATSON_CRICK)));
          }
          
          SecondaryStructure newStructure = new SimpleSecondaryStructure(ua, interactionSet);
          structures.add(newStructure);
          
        }
      }    
    }
  }
}
