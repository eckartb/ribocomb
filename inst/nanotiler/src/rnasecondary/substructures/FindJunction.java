package rnasecondary.substructures;

import java.util.*;
import sequence.*;
import rnasecondary.*;

public class FindJunction extends FindSubstructure{

  Map<Substructure, Set<Substructure>> edgeList;

  public FindJunction(){
  }
  
  public FindJunction(SecondaryStructure structure){
    super(structure);
  }
  
  @Override
  public void run(SecondaryStructure structure){
    FindSubstructure fs = new FindBulge(structure);
    run(structure, fs);
  }

  public void run(SecondaryStructure structure, FindSubstructure fs){
    clear();
    
    InteractionSet interactions = structure.getInteractions();
    HashMap<Residue,Residue> pairs = new HashMap<Residue,Residue>(); //basepair map
    for (int i=0; i<interactions.size(); i++) {
      Interaction inter = interactions.get(i);
      pairs.put( inter.getResidue1(), inter.getResidue2() );
      pairs.put( inter.getResidue2(), inter.getResidue1() );
    }
    
    List<Substructure> subs = fs.getSubstructures();
    Map<Residue,List<Substructure>> conns = SubstructureMapper.getPairs(subs); //find overlaps on backbone
    
    edgeList = new HashMap<Substructure, Set<Substructure>>();
    for(Residue res : conns.keySet()){ //map connections between substructures
      Residue resPair = pairs.get(res);
      for(Substructure sub : conns.get(res)){
        if(sub.getStructure().getSequence(0).size()>2){ //not an imaginary bulge
          Substructure subPair = findOppositeBulgeFromGroup(sub, res, resPair, conns);
          if(subPair!=null && subPair.getStructure().getSequence(0).size()>2){
            addToEdgeList(sub, subPair);
            addToEdgeList(subPair, sub);
          }
        }
      }
    }
    
    List<List<Substructure>> juncts = new ArrayList<List<Substructure>>(); //junctions
    List<Substructure> temp = new ArrayList<Substructure>();
    temp.addAll(subs);
    while(temp.size()>0){
      // List<Substructure> group = findGroup(temp.get(0), temp);
      boolean cycle = false;
      List<Substructure> group = new ArrayList<Substructure>();
      Map <Substructure, Boolean> visited = new HashMap<Substructure,Boolean>();
      for(Substructure sub : subs){
        visited.put(sub, false);
      }
      Queue<Substructure> q = new LinkedList<Substructure>();
      q.add(temp.get(0));
      Substructure last = null; //most revent visited
      while(!q.isEmpty()){  //bfs
        Substructure sub = q.peek();
        if(!visited.get(sub)){
          visited.put(sub,true);
          group.add(sub);
          Set<Substructure> edges = edgeList.get(sub);
          if(edges!=null){
            for( Substructure newSub : edges ){
              // if(!visited.get(newSub)){
                q.add( newSub);
              // }
            }
          }
        } else{
          if(sub!=last){ //not directly behind
            cycle=true;
          }
        }
        q.remove(sub);
        last = sub;
      }
      if(cycle){
        juncts.add(group);
        addJunction(group);
      }
      temp.removeAll(group);
    }
    
    System.out.println(juncts.size()+" junctions");
    for(List<Substructure> i : juncts){
      System.out.println(i.size()+"-way");
    }
    for(SecondaryStructure struct : structures){
      SecondaryStructureWriter writer = new SecondaryStructureScriptFormatWriter();
      String out = writer.writeString(struct);
      System.out.println(out);
    }
    
  }
  
  private void addToEdgeList(Substructure sub1, Substructure sub2){
    if(edgeList.containsKey(sub1)){
      Set<Substructure> temp = edgeList.get(sub1);
      temp.add(sub2);
      edgeList.put(sub1,temp);
    } else{
      Set<Substructure> temp = new HashSet<Substructure>();
      temp.add(sub2);
      edgeList.put(sub1,temp);
    }
  }
  
  private static Substructure findOppositeBulgeFromGroup(Substructure bulge, Residue res, Residue resPair, Map<Residue,List<Substructure>> conns){
    
    List<Substructure> pairGroup = conns.get(resPair);
    if(pairGroup!=null){
      if(pairGroup.size()==1){
        return pairGroup.get(0);
      } else
      if(pairGroup.size()==2){ //length 1 helix
        
        boolean first = false; //which side of bulge is Residue on
        if(bulge.getEdges()[0] == res){
          first = true;
        } else
        if(bulge.getEdges()[1] == res){
          first = false;
        } else{
          System.out.println("Error in FindJunction. Res not on bulge!");
        }
        
        boolean[] pairEdgeFirst = new boolean[pairGroup.size()];
        for(int i=0; i<pairGroup.size(); i++){
          Substructure pair = pairGroup.get(i);
          if(pair.getEdges()[0] == resPair){
            pairEdgeFirst[i] = true;
          } else
          if(pair.getEdges()[1] == resPair){
            pairEdgeFirst[i] = false;
          } else{
            System.out.println("Error in FindJunction. Res not on bulge!");
          }
        }
        
        for(int i=0; i<pairGroup.size(); i++){
          if(first ^ pairEdgeFirst[i]){ //bulges are opposite
            return pairGroup.get(i);
          }
        }    
      }    
      System.out.println("Error: "+pairGroup.size()+" bulges in FindJunction!");
    }
    
    return null;
  }
  
  private void addJunction(List<Substructure> bulges){
    
    int nway = bulges.size();
    
    //structure
    UnevenAlignment ua = new SimpleUnevenAlignment();
    InteractionSet interactionSet = new SimpleInteractionSet();
    Residue[][] pairs = new Residue[nway][2];
    for(int i=0; i<nway; i++){
      Sequence seq = bulges.get(i).getStructure().getSequence(0);
      Sequence newSeq = new SimpleSequence("junct"+i, seq.getAlphabet());
      
      Residue[] bulgeEdges = bulges.get(i).getEdges();
      int startPos = bulgeEdges[0].getPos();
      int stopPos = bulgeEdges[1].getPos();
      System.out.println("Positions: "+startPos+" "+stopPos);
      for(int j=0; j<seq.size(); j++){
        Residue newRes = (Residue)seq.getResidue(j).cloneDeep();
        newRes.setParentObject(newSeq);
        newSeq.addResidue( newRes );
        if(j==0){
          pairs[i][0] = newRes;
        } else if(j==seq.size()-1){
          pairs[i][1] = newRes;
        }
      }
      try{
        ua.addSequence(newSeq);
      } catch(DuplicateNameException dne){
        System.out.println("FindJunction: "+dne);
      }
    }
    for(int i=0; i<nway; i++){
      if(i!=nway-1){
        interactionSet.add(new SimpleInteraction( pairs[i][1], pairs[i+1][0], new RnaInteractionType(RnaInteractionType.WATSON_CRICK)));
      } else{
        interactionSet.add(new SimpleInteraction( pairs[i][1], pairs[0][0] , new RnaInteractionType(RnaInteractionType.WATSON_CRICK)));
      }
    }
    SecondaryStructure newStructure = new SimpleSecondaryStructure(ua, interactionSet);
    structures.add(newStructure);
    
    //edges
    Residue[] edgeSet = new Residue[nway*2];
    for(int i=0; i<nway; i++){
      Residue[] bulgeEdges = bulges.get(i).getEdges();
      edgeSet[i*2] = bulgeEdges[0];
      edgeSet[i*2+1] = bulgeEdges[1];
    }
    edges.add(edgeSet);
    
    //residues
    List<Residue> residues_ = new ArrayList<Residue>();
    for(Substructure s : bulges){
      residues_.addAll( s.getResidues() );
    }
    residues.add(residues_);

    //children
    children.add(bulges);
    
  }

  
}
