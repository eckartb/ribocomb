package rnasecondary.substructures;

import java.util.*;
import sequence.*;
import rnasecondary.*;

public class FindSingleStrand extends FindSubstructure{
  
  public FindSingleStrand(){}
  
  public FindSingleStrand(SecondaryStructure structure){
    super (structure);
  }
  
  @Override
  public void run(SecondaryStructure structure){
    clear();
    InteractionSet interactions = structure.getInteractions();
    HashSet<Residue> paired = new HashSet<Residue>(); //holds all residues that are paired
    for (int i=0; i<interactions.size(); i++) {
      Interaction inter = interactions.get(i);
      paired.add( inter.getResidue1() );
      paired.add( inter.getResidue2() );
    }
    HashMap<Residue,Residue> pairs = new HashMap<Residue,Residue>(); //TODO: remove 1 of the redundant data structures
    for (int i=0; i<interactions.size(); i++) {
      Interaction inter = interactions.get(i);
      //System.out.println(inter.getResidue1() +" : "+inter.getResidue2() );
      pairs.put( inter.getResidue1(), inter.getResidue2() );
      pairs.put( inter.getResidue2(), inter.getResidue1() );
    }
    for ( int i=0; i<structure.getSequenceCount(); i++ ){
      boolean inSingleStrand = false;
      Residue startRes = null;
      Residue stopRes = null;
      Sequence seq = structure.getSequences().getSequence(i);
      int firstPaired = -1;
      for ( int j=0; j<seq.size(); j++){
        if (paired.contains(seq.getResidue(j) )	){
          firstPaired = j;
          break;
        }
      }
      if(firstPaired==-1){return;}
      assert firstPaired != -1;
      for ( int j=firstPaired; j<seq.size(); j++){
        if ( !paired.contains(seq.getResidue(j)) ){ //if unpaired
          if(inSingleStrand ){
            assert startRes != null; //region should have already begun
            continue;
          } else
          {
            assert paired.contains(seq.getResidue(j-1));
            startRes = seq.getResidue(j-1); //previous residue
            inSingleStrand = true;
          }			
        } else{
          if(inSingleStrand){
            assert startRes != null; //region should have already begun
            stopRes = seq.getResidue(j);
            assert stopRes.getPos() > startRes.getPos(); //first residue with pair
            assert startRes.getParentObject() == stopRes.getParentObject();
            add(startRes,stopRes);
            startRes = null;
            stopRes = null;
            inSingleStrand = false;
          } else{
            if( j>0 && paired.contains( seq.getResidue(j-1) ) ){
              startRes = seq.getResidue(j-1);
              Residue otherStartRes = pairs.get(startRes);						
              stopRes = seq.getResidue(j);  						
              Residue otherStopRes = pairs.get(stopRes);				
              if ( Math.abs(otherStopRes.getPos() - otherStartRes.getPos()) != 1 || 
              otherStopRes.getParentObject() != otherStartRes.getParentObject()  ) { //new helix has begun
                add(startRes, stopRes); //would return length 0
                startRes = null;
                stopRes = null;
              }
            }
            
          }
        }
      }
    }
  }
  
  private void add(Residue startRes, Residue stopRes){
    Residue[] ends = {startRes, stopRes};
    edges.add(ends);
    int startPos = startRes.getPos();
    int stopPos = stopRes.getPos();
    Sequence seq = (Sequence)startRes.getParentObject();
    Sequence newSeq = new SimpleSequence(seq.getName(), seq.getAlphabet());
    List<Residue> reses = new ArrayList<Residue>();
    for(int i=startPos; i<=stopPos; i++){
      Residue res = seq.getResidue(i);
      reses.add(res);
      Residue newRes = (Residue)res.cloneDeep();
      newRes.setParentObject(newSeq);
      newSeq.addResidue(newRes);
    }
    UnevenAlignment ua = new SimpleUnevenAlignment();
    try{
      ua.addSequence(newSeq);
    } catch (DuplicateNameException dne){
      System.out.println("FindSingleStrand: duplicate names in singleStrands");
    }
    SecondaryStructure newStructure = new SimpleSecondaryStructure(ua, new SimpleInteractionSet());
    structures.add(newStructure);
    residues.add(reses);
    children.add(new ArrayList<Substructure>());
  }
}
