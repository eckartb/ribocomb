package rnasecondary.substructures;

import java.util.List;
import java.util.ArrayList;
import rnasecondary.*;
import sequence.*;

abstract public class FindSubstructure{
  
  public FindSubstructure () {
  }
  
  public FindSubstructure (SecondaryStructure structure) {
    run(structure);
  }
  
  protected List<SecondaryStructure> structures;
  protected List<List<Residue>> residues;
  protected List<Residue[]> edges;
  protected List<List<Substructure>> children;
  
   public abstract void run(SecondaryStructure sec);
   
   public void clear(){
     structures = new ArrayList<SecondaryStructure>();
     residues = new ArrayList<List<Residue>>();
     edges = new ArrayList<Residue[]>();
     children = new ArrayList<List<Substructure>>();
   }
  
  public List<SecondaryStructure> getStructures(){
    return structures;
  }
  public List<List<Residue>> getResidues(){
    return residues;
  }
  public List<Residue[]> getEdges(){
	  return edges;
  }
  public List<List<Substructure>> getChildren(){
    return children;
  }
  
  public List<Substructure> getSubstructures(){
    List<Substructure> substructures = new ArrayList<Substructure>();
    for(int i=0; i<structures.size(); i++){
      substructures.add( new Substructure(this.getClass(), structures.get(i), residues.get(i), edges.get(i), children.get(i)) );
    }
    return substructures;
  }
}