package rnasecondary.substructures;

import java.util.*;
import sequence.*;
import rnasecondary.*;

public class Substructure {
  
  private Class type;
  private SecondaryStructure structure;
  private List<Residue> residues;
  private Residue[] edges;
  private List<Substructure> children;
  
  public Substructure (Class type, SecondaryStructure structure, List<Residue> residues, Residue[] edges, List<Substructure> children){
    this.type = type;
    this.structure = structure;
    this.residues = residues;
    this.edges = edges;
    this.children = children;
  }
  
  public Class getType(){
    return type;
  }
  public SecondaryStructure getStructure(){
    return structure;
  }
  public List<Residue> getResidues(){
    return residues;
  }
  public Residue[] getEdges(){
    return edges;
  }
  public List<Substructure> getChildren(){
    return children;
  }
  
}