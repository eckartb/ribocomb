package rnasecondary.substructures;

import java.util.*;
import sequence.*;
import rnasecondary.*;

/**find which substructures should be paired from SecondaryStructure*/
public class SubstructureMapper{
  
  public Map<Class,Integer> blockValue = new HashMap<Class,Integer>();
  public List<FindSubstructure> finders = new ArrayList<FindSubstructure>();

  
  public SubstructureMapper(){
    /* hierarchy of motifs. Largest and most specific have highest value.
    A higher value block will replace multiple lower values (internal loops replace 2 bulges). */
    blockValue.put(FindHelix.class, 0);
    blockValue.put(FindSingleStrand.class, 0);
    blockValue.put(FindBulge.class, 1);
    blockValue.put(FindHairpin.class, 1);
    blockValue.put(FindInternalLoop.class, 2);
  }
  
  //list of blocks from input
  public List<Substructure> getSubstructures(SecondaryStructure structure){
    List<Substructure> subs = new ArrayList<Substructure>();
    FindHelix fh = new FindHelix();
    FindSingleStrand fss = new FindSingleStrand();
    FindBulge fb = new FindBulge();
    FindHairpin fhp = new FindHairpin();
    FindInternalLoop fil = new FindInternalLoop();
    finders.add(fh);
    finders.add(fss);
    finders.add(fb);
    finders.add(fhp);
    finders.add(fil);
    
    fh.run(structure); //find helices
    fss.run(structure); //find single strands
    fb.run(structure, fss); //find bulges in singles strands
    fhp.run(structure, fss); //find hairpins in single strands
    fil.run(structure, fb); //find internal loops in bulges
    
    for(int i=0; i<finders.size(); i++){ //collect all substructures
      FindSubstructure fs = finders.get(i);
      subs.addAll(fs.getSubstructures());
    }
    
    System.out.println("All substructures: "+subs.size());
    for(int i=0; i<subs.size(); i++){
      System.out.println(subs.get(i).getType());
      
    }
    
    List<Substructure> removeSubs = new ArrayList<Substructure>();
    for(int i=0; i<subs.size(); i++){ //remove substructures contained by another with a higher blockValue
      Substructure a = subs.get(i);
      for(int j=0; j<subs.size(); j++){
        if(i!=j){
          Substructure b = subs.get(j);
          if(containtsSubstructure(a,b)){
            //System.out.println(a.getType()+" containts "+b.getType());
            if(b.getType()!=FindHelix.class){
              removeSubs.add(b);
            }
          }
        }
      }
    }
    for(Substructure s : removeSubs){
      subs.remove(s);
      //System.out.println("removing: "+s.getType());
    }
    
    System.out.println("Non-redundant substructures: "+subs.size());
    for(int i=0; i<subs.size(); i++){
      System.out.println(subs.get(i).getType());
      
    }
    
    return subs;
  }
  
  //blocks sharing same residue
  public static Map<Residue,List<Substructure>> getPairs(List<Substructure> subs){
    Map<Residue,List<Substructure>> pairs = new HashMap<Residue,List<Substructure>>();
    
    for(Substructure s : subs){ //find edges connecting substructures
      //System.out.println(s.getType());
      Residue[] edges = s.getEdges();
      for(Residue r : edges){
        if(r!=null){
          if(pairs.containsKey(r)){
            List<Substructure> temp = pairs.get(r);
            temp.add(s);
            pairs.put(r,temp);
          } else{
            List<Substructure> temp = new ArrayList<Substructure>();
            temp.add(s);
            pairs.put(r,temp);
          }
        }
      }
    }
    return pairs;
  }
  
  public boolean containtsSubstructure(Substructure a, Substructure b){ //is b contained in a
    
    if(blockValue.get(b.getType())>blockValue.get(a.getType())){ //if b is higher order
      return false;
    }
    
    List<Residue> resList1 = a.getResidues();
    List<Residue> resList2 = b.getResidues();
    if(resList2.size()>resList1.size()){
      return false;
    }
    for(int i=0; i<resList2.size(); i++){
      if(!resList1.contains(resList2.get(i))){
        return false;
      }
    }
    
    return true;
  }

  public List<FindSubstructure> getFinders(){
    return finders;
  }
  
}