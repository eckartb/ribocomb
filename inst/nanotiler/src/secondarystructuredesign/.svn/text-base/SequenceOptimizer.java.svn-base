package secondarystructuredesign;

import java.util.Properties;
import generaltools.PropertyCarrier;
import rnasecondary.SecondaryStructure;

/** generates a set of sequences optimized to fullfill secondary structure */
public interface SequenceOptimizer extends PropertyCarrier {

    /** Optimize sequences of given secondary structure. */
    String[] optimize(SecondaryStructure structure);

    /** Evaluate secondary structure, do not optimize. */
    Properties eval(SecondaryStructure structure);

    double getErrorScoreLimit();

    /** Number of steps of first-stage optimazation. */
    public int getIterMax();

    /** Number of steps of second-stage optimization. */
    public int getIter2Max();

    void setErrorScoreLimit(double x);

    /** Number of steps of first-stage optimazation. */
    public void setIterMax(int n);

    /** Number of steps of second-stage optimization. */
    public void setIter2Max(int n);

    /** Sets number of times algorithm is re-started. */
    public void setRerun(int n);

    void setRandomizeFlag(boolean flag);

}
