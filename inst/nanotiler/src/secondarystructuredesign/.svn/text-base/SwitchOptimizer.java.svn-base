package secondarystructuredesign;

import java.util.Properties;
import generaltools.PropertyCarrier;
import rnasecondary.SecondaryStructure;

/** generates a set of sequences optimized to fullfill secondary structure */
public interface SwitchOptimizer extends PropertyCarrier {

    /** Optimize sequences of given secondary structure. */
    public Properties optimize(String[] sequences,
                        SecondaryStructure structure1,
	  	        SecondaryStructure structure2,
		        int[] structure1SeqIds,
		        int[] structure2SeqIds );

    /** Optimize sequences of given secondary structure. */
    public Properties eval(String[] sequences,
		    SecondaryStructure structure1,
		    SecondaryStructure structure2,
		    int[] structure1SeqIds,
		    int[] structure2SeqIds );

    public double getErrorScoreLimit();

    /** Number of steps of first-stage optimazation. */
    public int getIterMax();

    public void setErrorScoreLimit(double x);

    /** Number of steps of first-stage optimazation. */
    public void setIterMax(int n);

    /** Sets number of times algorithm is re-started. */
    public void setRerun(int n);

    public void setRandomizeFlag(boolean flag);

    public void setSwitchScorer(SwitchScorer scorer);

}
