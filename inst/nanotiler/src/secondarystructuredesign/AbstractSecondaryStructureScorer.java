package secondarystructuredesign;
// package SecondaryStructureDesign;

import java.util.logging.*;
import rnasecondary.*;
import numerictools.AccuracyTools;
import sequence.*;
import static rnasecondary.RnaInteractionType.*;

public abstract class AbstractSecondaryStructureScorer implements SecondaryStructureScorer {

    private Alphabet alphabet = DnaTools.AMBIGUOUS_RNA_ALPHABET;

    protected UnevenAlignment generateAlignment(StringBuffer[] bseqs) throws UnknownSymbolException, DuplicateNameException {
	SimpleUnevenAlignment ali = new SimpleUnevenAlignment();
	for (int i = 0; i < bseqs.length; ++i) {
	    Sequence seq = new SimpleSequence(bseqs[i].toString().toUpperCase(), "s" + (i+1), alphabet);
	    ali.addSequence(seq);
	}
	return ali;
    }

    protected double scorePrediction(int[][] prediction,
				   int[][] reference) {
	assert prediction.length == reference.length;
	assert prediction[0].length == reference[0].length;
	int tp = 0;
	int fp = 0;
	int tn = 0;
	int fn = 0;
	for (int i = 0; i < prediction.length; ++i) {
	    for (int j = 0; j < prediction[0].length; ++j) {
		int vp = prediction[i][j];
		int vr = reference[i][j];
		if (vr == NO_INTERACTION) {
		    if (vp == NO_INTERACTION) {
			++tn;
		    }
		    else {
			++fp;
		    }
		}
		else {
		    if (vp == NO_INTERACTION) {
			++fn;
		    }
		    else {
			++tp;
		    }
		}
	    }
	}
	// return scale * (1.0 - AccuracyTools.computeMatthews(tp, fp, tn, fn)); // perfect score: 0.0, otherwise between 0.0 and 2.0
	return fp + fn;
    }

    protected double scorePrediction(SecondaryStructure prediction,
				   SecondaryStructure reference,
				   int[][][][] interactionMatrices) {
	assert(prediction != null);
	assert(reference != null);
	assert(interactionMatrices != null);
	double result = 0.0;
	for (int i = 0; i < prediction.getSequenceCount(); ++i) {
	    for (int j = i; j < prediction.getSequenceCount(); ++j) {
		result += scorePrediction(prediction.toInteractionMatrix(i,j), interactionMatrices[i][j]);
	    }
	}
	return result;
    }

    public void setAlphabet(Alphabet val) { alphabet = val; }

}
