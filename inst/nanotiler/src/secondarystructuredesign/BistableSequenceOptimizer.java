package SecondaryStructureDesign;

import rnasecondary.SecondaryStructure;

/** generates a set of sequences optimized to fullfill secondary structure */
public interface BistableSequenceOptimizer {

    /** Optimizes sequences of two secondary structures consisting of the same sequences */
    String[] optimize(SecondaryStructure structure1, SecondaryStructure structure2);

    /** Gets error score limit. */
    double getErrorScoreLimit();

    /** Sets error score limit. */
    void setErrorScoreLimit(double x);

}
