package SecondaryStructureDesign;

import java.util.Random;
import java.awt.Color;
import generaltools.Randomizer;

public class ColorGradientMixer {
    
    private int red, green, blue;
    
    private int redIncrease, greenIncrease, blueIncrease;
    
    private Random r = Randomizer.getInstance();
    
    private int factor;
    
    
    public ColorGradientMixer(Color color) {
	this(color, 10);
    }
    
    public ColorGradientMixer(int red, int green, int blue) {
	this(red, green, blue, 10);
    }
    
    
    public ColorGradientMixer(int red, int green, int blue, int factor) {
	this(new Color(red, green, blue), factor);
	
	
    }
    
    
    public ColorGradientMixer(Color color, int factor) {
	this.factor = factor;
	red = color.getRed();
	green = color.getGreen();
	blue = color.getBlue();
	initialize(factor);
    }
    
    public Color nextColor() {
	if(red + redIncrease <= 150)
	    redIncrease = r.nextInt(factor) + factor / 2;
	else if(red + redIncrease >= 255)
	    redIncrease = (r.nextInt(factor) + factor / 2) * -1;
	
	if(blue + blueIncrease <= 150)
	    blueIncrease = r.nextInt(factor) + factor / 2;
	else if(blue + blueIncrease >= 255)
	    blueIncrease = (r.nextInt(factor) + factor / 2) * -1;
	
	if(green + greenIncrease <= 150)
	    greenIncrease = r.nextInt(factor) + factor / 2;
	else if(green + greenIncrease >= 255)
	    greenIncrease = (r.nextInt(factor) + factor / 2) * -1;
	
	red += redIncrease;
	blue += blueIncrease;
	green += greenIncrease;
	
	return new Color(red, green, blue);
    }
    
    private void initialize(int factor) {
	redIncrease = r.nextInt(factor * 2) - factor;
	blueIncrease = r.nextInt(factor * 2) - factor;
	greenIncrease = r.nextInt(factor * 2) - factor;
    }
}

