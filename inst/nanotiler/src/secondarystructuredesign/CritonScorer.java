package secondarystructuredesign;
// SecondaryStructureDesign;

import java.io.*;
import java.util.logging.*;
import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.text.ParseException;
import generaltools.StringTools;
import generaltools.ParsingException;
import generaltools.PropertyTools;
import generaltools.TestTools;
import numerictools.*;
import launchtools.*;
import rnasecondary.*;
import sequence.*;
import java.lang.reflect.Array;
import static secondarystructuredesign.PackageConstants.*;

import org.testng.annotations.*;

/** Assigns a score to a secondary structure and a set of potential sequences */
public class CritonScorer implements SecondaryStructureScorer {

    private Logger log = Logger.getLogger(LOGFILE_DEFAULT);

    public static final String CRITON_PROP = "critonlen"; // for setting length of critons

    private int auLim = 2; // not more than this many A/U base pairs in a row

    private int gcLim = 1; // not more than this many G/C base pairs in a row

    private int critonLen = 6;

    private double auViolationWeight = 1.0;

    private double gcViolationWeight = 1.0;

    // These variables should be set to their optimum values
    private double nonUniquePenalty = 1.0;       // Amount added to score if a criton is not unique

    private double complementPenalty = 1.0;      // Amount added to score if a criton has a complement where no folding is desired
                                                 // Set to zero to skip this test
    private double selfComplementPenalty = 1.0;  // Amount added to score if a criton complements itself

    private double similarPenalty = 0.0; // 0.1;         // Amount added to score if two critons are identical or complementary except for bp
                                                 // e.g. gggccc & ggaccc
    private double bulgePenalty = 0.0; // 0.05;           // Amount added to score if two strands are identical or complementary but one strand has one extra bp
                                                 // e.g. gggccc & gggaccc
    private boolean guComplementMode = true; // if FALSE, penalize unwanted GU matches

    private int debugLevel = 0;

    private SecondaryStructureScorer defaultScorer = new MatchFoldSecondaryStructureScorer(); // new DefaultSecondaryStructureScorer();

    private double defaultScorerWeight = 0.0;

    private double branchMigrationWeight = 1.0;

    private double consecutiveWeight = 1.0;

    private double gcMinFrac = 0.76; // ecach helix has to have at least this many GC base pairs (fraction)

    private double gcMinFracWeight = 1.0; // count how many GC base pairs are missing

    private int zipperLength = 1; // length of zipper on both ends of helices

    private double zipperWeight = 1.0;
    
    private int consecutiveLimit = 3; // no more than this many consecutive bases of the same kind 

    private int consecutiveLimitG = 2; // no more than this many consecutive G bases 

    public void setAuViolationWeight(double value) {
	this.auViolationWeight = value;
    }

    public void setGcViolationWeight(double value) {
	this.gcViolationWeight = value;
    }

    public void setBranchMigrationWeight(double value) {
	this.branchMigrationWeight = value;
    }

    public void setBulgePenalty(double p) {
	this.bulgePenalty = p;
    }

    public void setSimilarPenalty(double p) {
	this.similarPenalty = p;
    }

    public void setConsecutiveLimit(int limit) {
	this.consecutiveLimit = limit;
    }

    public void setConsecutiveLimitG(int limit) {
	this.consecutiveLimitG = limit;
    }

    public void setConsecutiveWeight(double value) {
	this.consecutiveWeight = value;
    }

    public void setCritonLen(int newLen) {
	critonLen = newLen;
    }

    public void setDebugLevel(int level) {
	this.debugLevel = level;
    }

    public void setDefaultScorerWeight(double weight) {
	defaultScorerWeight = weight;
    }

    public void setGCMinFracWeight(double value) {
	this.gcMinFracWeight = value;
    }

    public void setGuComplementMode(boolean mode) {
	this.guComplementMode = mode;
    }
    
    public void setZipperWeight(double value) {
	this.zipperWeight = value;
    }

    /** Returns true if the strand is complementary to itself.
	e.g. of the form aggcgccu  */
    private boolean isSelfComplementary(String strand) {
	int n = strand.length();
	int n2 = n/2;
	for (int i = 0; i < n2; i++) {
	    char char1 = strand.charAt(i);
	    char char2 = strand.charAt(n - i - 1);
	    if (!RnaSecondaryTools.isWatsonCrick(char1, char2)) {
		return false;
	    }
	}
	return true;
    }

    /** Returns true if the strands are complementary to each other.
	e.g. if strand1 = agcua and strand2 = ucgau   */
    @Test(groups={"new"})
    public void testIsSelfComplementary() {
	String strand1 = "ACGU";
	String strand2 = "UGCA";
	assert isSelfComplementary(strand1);
	assert isSelfComplementary(strand2);
	assert isSelfComplementary(strand1 + strand1);
	assert isSelfComplementary(strand2 + strand2);
	assert !isSelfComplementary(strand1 + strand2);
	assert !isSelfComplementary(strand2 + strand1);
    }

    /** Returns true if the strands are complementary to each other.
	e.g. if strand1 = agcua and strand2 = ucgau   */
    private boolean strandsComplement(String strand1, String strand2) {
	if (strand1.length() != strand2.length()) {
	    return false;
	}
	int n = strand2.length();
	for (int i = 0; i < strand1.length(); i++) {
	    // changed from testing for Watson-Crick pairing to also consider GU pairing
	    if (guComplementMode) {
		if (!RnaSecondaryTools.isRnaComplement(strand1.charAt(i), strand2.charAt(n - i - 1) ) ) {
		    return false;
		}
	    }
	    else if ( !RnaSecondaryTools.isWatsonCrick(strand1.charAt(i), strand2.charAt(n - i - 1) )) {
		    return false;
	    }
	}
	return true;
    }

    /** Each nucleotide is assigned an index for easy reference. */
    private char getBP(int index) {
	if (index == 0) return 'a';
	else if (index == 1) return 'u';
	else if (index == 2) return 'c';
	else if (index == 3) return 'g';
	else {
	    System.out.println("Invalid bp index: " + index);
	    System.out.println("Index must be between 0 & 3 (inclusive)");
	    assert false;
	    return ' ';
	}
    }

    /** Finds all strands that are 1 bp longer than the criton and have all but one bp identical
     *  e.g. the criton 'gggccc' would return 'gaggccc', 'guggccc', 'ggggccc', 'gcggccc', 'ggagccc',...
     */
    private String[] findAllBulges(String strand) {
	String[] bulges = new String[4*(strand.length() - 1)];
	for (int i = 0; i < strand.length()-1; i++) {
	    for (int n = 0; n < 4; n++) {
		StringBuffer b = new StringBuffer(strand);
		bulges[4*i + n] = b.insert(i+1, getBP(n)).toString();
	    }
	}
	return bulges;
    }

    /** Finds all pairs of critons that have only one unidentical bp
     *  e.g. the bp 'gggccc' would return 'gagccc', 'gugccc', 'gcgccc', 'ggaccc',...
     */
    private String[] findAllSimilar(String strand) {
	String strandLower = strand.toLowerCase();
	String[] similar = new String[3*(strand.length())];
	for (int i = 0; i < strand.length(); i++) {
	    int index = 0;
	    for (int n = 0; n < 4; n++) {
		char c = getBP(n);
		if (strandLower.charAt(i) != c) {
		    StringBuffer b = new StringBuffer(strand);
		    b.setCharAt(i, c);
		    similar[3*i + index] = b.toString();
		    index++;
		}
	    }
	}
	return similar;
    }

    /** Returns true if the strands are complementary to each other.
	e.g. if strand1 = agcua and strand2 = ucgau   */
    @Test(groups={"new"})
    public void testStrandsComplement() {

    }

    /** Separates the strand into critons  */
    private String[] createCritons(String strand, int len) {
	int numCritons = strand.length() - len + 1;
	if (numCritons <= 0) {
	    return new String[0];
	}
	String[] critons = new String[numCritons];
	for (int i = 0; i < critons.length; i++) {
	    critons[i] = strand.substring(i, i + len).toUpperCase();
	}
	assert critons[critons.length-1] != null;
	return critons;
    }

    /** Loosely coupled form of setting parameters. Currently active: critonlen */
    public void setParams(Properties params) {
	if (params.getProperty("critonlen") != null) {
	    setCritonLen(Integer.parseInt(params.getProperty("critonlen")));
	}
    }

    /** Tests that the critons with the given indices is unique (i.e. there are no other critons with the same sequence)  */
    private boolean isUnique(String[][] critons, int xIndex, int yIndex) {
	log.severe("CritonScorer.isUnique is deprecated!");
	assert xIndex < critons.length;
	assert yIndex < critons[0].length;
	String criton = critons[xIndex][yIndex];
	for (int i = 0; i < critons.length; i++) {
	    for (int j = 0; j < critons[i].length; j++) {
		if ( critons[i][j].equalsIgnoreCase(criton) && (i != xIndex || j != yIndex) ) {
		    System.out.print("Not unique!  Identical sequence exists in strand " + (i+1) + " at index " + j + "   ");
		    return false;
		}
	    }
	}
	return true;
    }

    /** Tests if the criton at the given indices has any complementary critons (besides any desired interactions)  */
    private boolean unwantedComplementExists(String[][] critons, int xIndex, int yIndex, int[][][][] interactionMatrices) {
	log.severe("CritonScorer.unwantedComplementExists is deprecated!");
	String criton = critons[xIndex][yIndex];
	for (int i = 0; i < critons.length; i++) {
	    for (int j = 0; j < critons[i].length; j++) {
		if (strandsComplement(criton, critons[i][j])) {
		    int[][] interactionMatrix = null;
		    if (xIndex <= i) interactionMatrix = interactionMatrices[xIndex][i];
		    else interactionMatrix = interactionMatrices[i][xIndex];
		    for (int index = 0; index < critonLen; index++) {
			if (interactionMatrix[yIndex + index][j + index] != RnaInteractionType.WATSON_CRICK) {
			    System.out.print("Unwanted complement exists in strand " + (i + 1) + " at index " + j + "   ");
			    return true;
			}
		    }
		}
	    }
	}
	return false;
    }

    private boolean isGap(char c) {
	return (c == '.') || (c == '-');
    }

    /** Tests if the criton at the given indices is completely base paired without gaps
     * @param pos Position in string
     * @param s Secondary structure string of the form ...AAA...BBBB...AAA.BBBB. or the like */
    private boolean isUniformlyBasePaired(int pos, int len, String s) {
	assert pos + len - 1 < s.length();
	for (int i = pos; i < (pos + len); ++i) {
	    char c = s.charAt(i);
	    if (isGap(c)) {
		return false;
	    }
	    if ((i > pos) && (s.charAt(i-1) != c)) {
		return false;
	    }
	}
	return true;
    }

    @Test(groups={"new"})
    public void testIsUniformlyBasePaired() {
	String s = "..AAAABBBB...AA";
	assert isUniformlyBasePaired(2, 4, s);
	assert isUniformlyBasePaired(6, 4, s);
	assert !isUniformlyBasePaired(3, 4, s);
	assert !isUniformlyBasePaired(1, 4, s);
	assert !isUniformlyBasePaired(0, 2, s);
    }

    /** Makes sure that no base pair occurs too often: returns number of base pairs that occur more often than tooOftenLimit. */
    private int countTooOften(String s, int tooOftenLimit) {
	Set<String> alreadyChecked = new HashSet<String>();
	int result = 0;
	for (int i = 0; i < s.length(); ++i) {
	    char c = s.charAt(i);
	    String cs = "" + c;
	    if (alreadyChecked.contains(cs)) {
		continue;
	    }
	    else {
		alreadyChecked.add(cs);
	    }
	    int n = StringTools.countChar(s, c);
	    if (n > tooOftenLimit) {
		result += n - tooOftenLimit;
	    }
	}
	return result;
    }

    /** Makes sure that no base occurs too often: returns number of bases that occur more often than tooOftenLimit.
     * @param s String to be examinded
     * @param tooOftenLimit maximum number of allowed stretches of nucleotides other than G
     * @param tooOftenLimitG maximum number of allowed stretches of G nucleotides
     */
    private int countTooOftenConsecutive(String s, int tooOftenLimit, int tooOftenLimitG) {
	Set<String> alreadyChecked = new HashSet<String>();
	int result = 0;
	for (int i = 0; i < s.length(); ++i) {
	    char c = s.charAt(i);
	    String cs = "" + c;
	    alreadyChecked.add(cs); // set of unique one char strings
	}
	for (Iterator<String> it = alreadyChecked.iterator(); it.hasNext(); ) {
	    String cs = it.next();
	    assert cs.length() == 1;
	    char c = cs.charAt(0);
	    Pattern pattern = Pattern.compile(cs + "+"); // like "A+" finds stretches of "A"
            Matcher matcher = pattern.matcher(s);
	    while (matcher.find()) {
		String stretch = matcher.group();
		if (Character.toUpperCase(c) != 'G') {
		    if (stretch.length() > tooOftenLimit) {
			result += stretch.length() - tooOftenLimit;
		    }
		}
		else {
		    if (stretch.length() > tooOftenLimitG) {
			result += stretch.length() - tooOftenLimitG;
		    }
		}
	    }
	}
	assert result >= 0;
	return result;
    }

    /** Makes sure that no base pair occurs too often: returns number of base pairs that occur more often than tooOftenLimit. */
    @Test(groups={"new"})
    public void testCountTooOftenConsecutive() {
	String s1 = "ACGUACGUACGU";
	String s2 = "ACGUACGGUACGU";
	String s3 = "ACGUACGGGUACGU";
	String s4 = "ACCCGUACGGGUACGU";

	// no stretch:
	assert countTooOftenConsecutive(s1, 1,1) == 0;
	assert countTooOftenConsecutive(s1, 2,2) == 0;
	
	// check for GG stretch
	assert countTooOftenConsecutive(s2, 1,1) == 1;
	assert countTooOftenConsecutive(s2, 2,2) == 0;
	
	// check for GGG stretch
	assert countTooOftenConsecutive(s3, 1,1) == 2;
	assert countTooOftenConsecutive(s3, 1,2) == 1;
	assert countTooOftenConsecutive(s3, 1,3) == 0;
	
	// two times difference if two stretches:
	assert countTooOftenConsecutive(s4, 1,1) == 4;
// 	assert countTooOftenConsecutive(s4, 2,2) == 2;
// 	assert countTooOftenConsecutive(s4, 3,3) == 0;

    }

    /* Counts the number of base pairs that could participate in branch migration. Maximum two base pairs per helix is counted.
     * Careful: currently only works for secondary structure of the form A, B, C, ... but not yet "(" or ")" symbols.
     * @param bseqs Array of StringBuffers representing the current sequence set
     * @param seqStrings strings of the form: ...AAA... ; ..AAA.BB.. ; ..BB.... or the like
    */
    private int countBranchMigrations(StringBuffer[] bseqs,
				      int[][][][] interactionMatrices) {
	int nSeq = bseqs.length;
	int result = 0;
	for (int i = 0; i < nSeq; ++i) {
	    for (int j = i; j < nSeq; ++j) { // count one sequence order.
		result += countBranchMigrations(bseqs[i], bseqs[j], interactionMatrices[i][j]);
	    }
	}
	return result;
    }
    
    private int countBranchMigrations(StringBuffer sb1, StringBuffer sb2, int[][] mat) {
	assert sb1.length() == mat.length;
	assert sb2.length() == mat[0].length;
	int result = 0;
	for (int m = 0; m < mat.length; ++m) {
	    int nStart = 0; // for different strand
	    if (sb1 == sb2) {
		nStart = m + 1; // in  same strand
	    }
	    for (int n = nStart; n < mat[m].length; ++n) {
		assert (sb1!=sb2) || (mat[m][n] == mat[n][m]); // must be symmetric if same sequences
		if (mat[m][n] != RnaInteractionType.NO_INTERACTION) {
		    if (((m+1) < mat.length) && ((n-1) >= 0) && (mat[m+1][n-1] == RnaInteractionType.NO_INTERACTION)) { // end of helix
			char c1 = sb1.charAt(m+1);
			char c2 = sb2.charAt(n-1);
			if (RnaSecondaryTools.isWatsonCrick(c1, c2)) {
			    // System.out.println("Hey " + m + " " + n);
			    ++result;
			}
		    } // end of helix 
		    if (((m-1) >= 0) && ((n+1) < mat[m-1].length) && (mat[m-1][n+1] == RnaInteractionType.NO_INTERACTION)) { // beginning of helix
			char c1 = sb1.charAt(m-1);
			char c2 = sb2.charAt(n+1);
			if (RnaSecondaryTools.isWatsonCrick(c1, c2)) { // this could lead to migration of helix
			    // System.out.println("Hey2 " + m + " " + n);
			    ++result;
			}
		    } // beginning of helix
		}
	    }
	}
	return result;
    }

    private boolean isGC(char c) {
	c = Character.toUpperCase(c);
	return c == 'G' || c == 'C';
    }

    private boolean isZipperViolation(StringBuffer seq, String sec, int pos) {
	char seqc = seq.charAt(pos);
	char secc = sec.charAt(pos); 
	return (!isGap(secc)) && (!isGC(seqc));
    }

    private int getFirstNongapPos(String sec) {
	for (int i = 0; i < sec.length(); ++i) {
	    if (!isGap(sec.charAt(i))) {
		return i;
	    }
	}
	return -1; // not found
    }

    private int getLastNongapPos(String sec) {
	for (int i = sec.length()-1; i >= 0; --i) {
	    if (!isGap(sec.charAt(i))) {
		return i;
	    }
	}
	return -1; // not found
    }

    /** Penalized if end of sequence base pairs are not G-C or C-G base pairs */
    private int countZipperViolations(StringBuffer seq, String sec) {
	int n = seq.length();
	assert n == sec.length();
	int result = 0;
	int firstPos = getFirstNongapPos(sec);
	int lastPos = getLastNongapPos(sec);
	if (firstPos >= 0) {
	    if (isZipperViolation(seq, sec, firstPos)) {
		result += 1;
	    }
	    if (((firstPos + 1) < n) && isZipperViolation(seq, sec, firstPos+1)) {
		result += 1;
	    }
	}
	if (lastPos >= 0) {
	    if (isZipperViolation(seq, sec, lastPos)) {
		result += 1;
	    }
	    if (((lastPos - 1) >= 0) && isZipperViolation(seq, sec, lastPos-1)) {
		result += 1;
	    }
	}
	return result;
    }

    /** Tests if Penalized if end of sequence base pairs are not G-C or C-G base pairs */
    @Test(groups={"new"})
    public void  testCountZipperViolations() {
	StringBuffer seq1 = new StringBuffer("ACG");
	StringBuffer seq2 = new StringBuffer("CCG");
	StringBuffer seq3 = new StringBuffer("ACU");
	String sec1 = "A-B";
	String sec2 = "---";
	assert countZipperViolations(seq1, sec1) == 1;
	assert countZipperViolations(seq1, sec2) == 0;
	assert countZipperViolations(seq2, sec2) == 0;
	assert countZipperViolations(seq2, sec1) == 0;
	assert countZipperViolations(seq3, sec1) == 2;
	assert countZipperViolations(seq3, sec2) == 0;	
    }

    /** Counts distance ( number of positions ) to closest single strand
     * Is zero, if already single strand */
    private int countSingleStrandDist(String sec, int pos) {
	char currPos = sec.charAt(pos);
	if (isGap(currPos)) {
	    return 0;
	}
	for (int i = 0; i < sec.length(); ++i) {
	    int minPos = pos - i;
	    int maxPos = pos + i;
	    if ((minPos < 0) || (currPos != sec.charAt(minPos))) {
		return i;
	    }
	    if ((maxPos >= sec.length()) || (currPos != sec.charAt(maxPos))) {
		return i;
	    }
	}
	return sec.length();
    }

    /** Counts distance ( number of positions ) to closest single strand
     * Is zero, if already single strand */
    @Test(groups={"new"})
    private void testCountSingleStrandDist() {
	String methodName = "testCountSingleStrandDist";
	System.out.println(TestTools.generateMethodHeader(methodName));
	String[] secs = new String[4];
	secs[0] = "AAAAA";
	secs[1] = "-AAAAA-";
	secs[2] = "-AAAAA---BBBBB--";
	secs[3] = "-AAAAABBBBB--";
	for (int i = 0; i < secs[0].length(); ++i) {
	    System.out.println(countSingleStrandDist(secs[0], i));
	}
	assert(countSingleStrandDist(secs[0], 0) == 1);
	assert(countSingleStrandDist(secs[0], 1) == 2);
	assert(countSingleStrandDist(secs[0], 2) == 3);
	assert(countSingleStrandDist(secs[0], 3) == 2);
	assert(countSingleStrandDist(secs[0], 4) == 1);
// 	for (int k = 0; k < secs.length; ++k) {
// 	    System.out.println("Testing " + secs[k]);
// 	    for (int i = 0; i < secs[k].length(); ++i) {
// 		System.out.println(countSingleStrandDist(secs[k], i));
// 	    }
// 	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Penalized if end of sequence base pairs at each helix are not G-C or C-G base pairs */
    private int countZipperViolations2Pos(StringBuffer seq, String sec, int pos) {
	if (isGC(seq.charAt(pos))) {
	    return 0; // no violation
	}
	int distSingleStrand = countSingleStrandDist(sec, pos);
	int result = 0;
	if ((distSingleStrand > 0) && (distSingleStrand <= zipperLength)) {
	    result = 1;
	}
	return result;
    }

    /** Penalized if end of sequence base pairs at each helix are not G-C or C-G base pairs */
    private int countZipperViolations2(StringBuffer seq, String sec) {
	int n = seq.length();
	assert n == sec.length();
	int result = 0;
	for (int i = 0; i < n; ++i) {
	    result += countZipperViolations2Pos(seq, sec, i);
	}
	return result;
    }

    /** Penalized there are more than auLim A/U base pairs in a row */
    private int countAUViolations(StringBuffer seq, String sec) {
	int n = seq.length();
	assert n == sec.length();
	int result = 0;
	int pc = 0;
	// System.out.println("Testing " + seq + " " + sec + "\n");
	for (int i = 0; i < n; ++i) {
	    boolean reset = false;
	    char c = seq.charAt(i);
	    char cs = sec.charAt(i);
	    if (isGap(cs) || ((i>0)&&(cs != sec.charAt(i-1))) || isGC(c)) {
		reset = true;
	    } else {
		++pc;
	    } 
	    if (reset) {
		if (pc > auLim) {
		    // System.out.println("Using " + pc + " " + auLim + " : " + (pc - auLim));
		    result += pc - auLim;
		}
		pc = 0;
	    }
	}
	if (pc > auLim) {
	    // System.out.println("Using " + pc + " " + auLim + " : " + (pc - auLim));
	    result += pc - auLim;
	}
	return result;
    }

    /** Penalized there are more than gcLim G/C base pairs in a row */
    private int countGCViolations(StringBuffer seq, String sec) {
	int n = seq.length();
	assert n == sec.length();
	int result = 0;
	int pc = 0;
	// System.out.println("Testing " + seq + " " + sec + "\n");
	for (int i = 0; i < n; ++i) {
	    boolean reset = false;
	    char c = seq.charAt(i);
	    char cs = sec.charAt(i);
	    if (isGap(cs) || ((i > 0) && (cs != sec.charAt(i-1))) || (!isGC(c))) {
		reset = true;
	    } else {
		++pc;
	    } 
	    if (reset) {
		if (pc > gcLim) {
		    // System.out.println("Using " + pc + " " + gcLim + " : " + (pc - gcLim));
		    result += pc - gcLim;
		}
		pc = 0;
	    }
	}
	// last GC violation before end of strand:
	if (pc > gcLim) {
	    // System.out.println("Using " + pc + " " + gcLim + " : " + (pc - gcLim));
	    result += pc - gcLim;
	}

	return result;
    }

    /** Counts the number of GC residues that would be needed to reach the G+C content of gcMinFrac in the respective helices. */
    private int countGCFracViolations(StringBuffer seq, String sec) {
	int n = seq.length();
	assert n == sec.length();
	assert n > 0;
	int result = 0;
	// System.out.println("Testing " + seq + " " + sec + "\n");
	int firstPos = -1;
	int lastPos = -1;
	int gcCount = 0;
	for (int i = 0; i < n; ++i) {
	    boolean reset = false;
	    char c = seq.charAt(i);
	    char cs = sec.charAt(i);
	    if ((i > 0) && (!isGap(sec.charAt(i-1))) && ((cs != sec.charAt(i-1)) || (i == (n - 1)))) {
		assert (firstPos >= 0);
		// end of previous helix
		lastPos = i;
		int gcMin = (int)(gcMinFrac * (lastPos - firstPos)); 
		if (gcCount < gcMin) {
		    result += (gcMin - gcCount);
		    //		    System.out.println("Added GC Count Frac violation: " + gcMin + " " + gcCount + " " + i);
		} else {
		    //		    System.out.println("Not Added GC Count Frac violation: " + gcMin + " " + gcCount + " " + i);
		}
		firstPos = -1;
		lastPos = -1;
		gcCount = 0; // reset count
	    }
	    if (!isGap(cs)) {
		if (firstPos < 0) {
		    firstPos = i; 		// beginning of new helix. 
		}
		if (isGC(c)) {
		    ++gcCount;
		}
	    }
	}
	return result;
    }

    /** Tests if Penalized if end of sequence base pairs are not G-C or C-G base pairs */
    @Test(groups={"new"})
    public void  testGCFracViolations() {
	String methodName = "testGCFracViolations";
	System.out.println(TestTools.generateMethodHeader(methodName));
	StringBuffer seq1 = new StringBuffer("GAAAAC");
	StringBuffer seq2 = new StringBuffer("CCGCAC");
	StringBuffer seq3 = new StringBuffer("ACUUAG");
	StringBuffer seq4 = new StringBuffer("ACUUAGAUAUAG");
	String sec1 = "AAAAAA";
	String sec2 = "------";
	String sec4 =                        "AAAAAAAAAAAA";
	System.out.println(countZipperViolations(seq1, sec1));
	System.out.println(countGCFracViolations(seq1, sec2));
	System.out.println(countGCFracViolations(seq2, sec2));
	System.out.println(countGCFracViolations(seq2, sec1));
	System.out.println(countGCFracViolations(seq3, sec1));
	System.out.println(countGCFracViolations(seq3, sec2));
	System.out.println(countGCFracViolations(seq4, sec4));

	assert countGCFracViolations(seq1, sec1) > 0;
	assert countGCFracViolations(seq1, sec2) == 0;
	assert countGCFracViolations(seq2, sec2) == 0;
	assert countGCFracViolations(seq2, sec1) == 0;
	assert countGCFracViolations(seq3, sec1) > 0;
	assert countGCFracViolations(seq3, sec2) == 0;	
	assert countGCFracViolations(seq4, sec4) > 0;	
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Tests if more than auMin number of consecutive A/U base pairs */
    @Test(groups={"new"})
    public void  testAUViolations() {
	String methodName = "testAUViolations";
	System.out.println(TestTools.generateMethodHeader(methodName));
	StringBuffer seq1 = new StringBuffer("GAAAAC");
	StringBuffer seq2 = new StringBuffer("CCGCAC");
	StringBuffer seq3 = new StringBuffer("ACUUAG");
	StringBuffer seq4 = new StringBuffer("ACUUAGAUAUAG");
	String sec1 = "AAAAAA";
	String sec2 = "------";
	String sec4 =                        "AAAAAAAAAAAA";
	System.out.println(countZipperViolations(seq1, sec1));
	System.out.println(countAUViolations(seq1, sec2));
	System.out.println(countAUViolations(seq2, sec2));
	System.out.println(countAUViolations(seq2, sec1));
	System.out.println(countAUViolations(seq3, sec1));
	System.out.println(countAUViolations(seq3, sec2));
	System.out.println(countAUViolations(seq4, sec4));
	assert countAUViolations(seq1, sec1) == 1;
	assert countAUViolations(seq1, sec2) == 0;
	assert countAUViolations(seq2, sec2) == 0;
	assert countAUViolations(seq2, sec1) == 0;
	assert countAUViolations(seq3, sec1) == 0;
	assert countAUViolations(seq3, sec2) == 0;	
	assert countAUViolations(seq4, sec4) == 2;	
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Tests if Penalized if end of sequence base pairs are not G-C or C-G base pairs */
    @Test(groups={"new"})
    public void  testCountZipperViolations2() {
	String methodName = "testCountZipperViolations2";
	System.out.println(TestTools.generateMethodHeader(methodName));
	StringBuffer  seq1 = new StringBuffer("ACGG");
	StringBuffer  seq2 = new StringBuffer("CCGG");
	StringBuffer  seq3 = new StringBuffer("ACUG");
	StringBuffer  seq4 = new StringBuffer("gggaaauuCUAGAGUGUAuuguugccGCGUuuAUACUGGAUGuuUCGC");
	StringBuffer seq4b = new StringBuffer("gggaaauuGCUCUCGUGUuuguugccCCUGuuAAGGCAUCGAuuUUGG");
	String sec4 =                         "QQQQQQ..PPPPPPPPPP..MMMMMMNNNN..EEEEEEEEEE..RRRR";
	String sec1 = "AAAA";
	String sec2 = "----";

	System.out.println("" + countZipperViolations2(seq1, sec1) + " 1");
	System.out.println("" + countZipperViolations2(seq1, sec2) + " 0");
	System.out.println("" + countZipperViolations2(seq2, sec2) + " 0");
	System.out.println("" + countZipperViolations2(seq2, sec1) + " 0");
	System.out.println("" + countZipperViolations2(seq3, sec1) + " 2");
	System.out.println("" + countZipperViolations2(seq3, sec2) + " 0");	
	System.out.println("" + countZipperViolations2(seq4, sec4) + " 11");
	System.out.println("" + countZipperViolations2(seq4, sec4) + " 12");
	assert countZipperViolations2(seq1, sec1) == 1;
	assert countZipperViolations2(seq1, sec2) == 0;
	assert countZipperViolations2(seq2, sec2) == 0;
	assert countZipperViolations2(seq2, sec1) == 0;
	assert countZipperViolations2(seq3, sec1) == 2;
	assert countZipperViolations2(seq3, sec2) == 0;	
	assert countZipperViolations2(seq4, sec4) == 11;
	// assert countZipperViolations2(seq4b, sec4) == 11; FIXIT
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    private int countZipperViolations(StringBuffer[] bseqs, String[] secStrings) {
	assert bseqs.length == secStrings.length;
	int result = 0;
	for (int i = 0; i < bseqs.length; ++i) {
	    result += countZipperViolations(bseqs[i], secStrings[i]);
	}
	return result;
    }

    private int countZipperViolations2(StringBuffer[] bseqs, String[] secStrings) {
	assert bseqs.length == secStrings.length;
	int result = 0;
	for (int i = 0; i < bseqs.length; ++i) {
	    result += countZipperViolations2(bseqs[i], secStrings[i]);
	}
	return result;
    }

    @Test(groups={"new"})
    public void testCountBranchMigrations() {
	String methodName = "testCountBranchMigrations";
	System.out.println(TestTools.generateMethodHeader(methodName));
	int[][][][] mats1 = new int[1][1][6][6];
	int[][][][] mats2 = new int[1][1][6][6];
	int[][][][] mats3 = new int[1][1][6][6];
	IntegerArrayTools.fill(mats1[0][0], RnaInteractionType.NO_INTERACTION);
	IntegerArrayTools.fill(mats2[0][0], RnaInteractionType.NO_INTERACTION);
	IntegerArrayTools.fill(mats3[0][0], RnaInteractionType.NO_INTERACTION);
	mats1[0][0][1][4] = RnaInteractionType.WATSON_CRICK;
	mats1[0][0][4][1] = RnaInteractionType.WATSON_CRICK;
	mats2[0][0][0][4] = RnaInteractionType.WATSON_CRICK;
	mats2[0][0][4][0] = RnaInteractionType.WATSON_CRICK;
	mats3[0][0][0][3] = RnaInteractionType.WATSON_CRICK;
	mats3[0][0][3][0] = RnaInteractionType.WATSON_CRICK;
	StringBuffer sb1 = new StringBuffer("AAAUUU");
	StringBuffer[] sbs = new StringBuffer[1];
	sbs[0] = sb1;
	System.out.println("Found branch migrations: " + countBranchMigrations(sbs, mats1) + " "
			   + countBranchMigrations(sbs, mats2) + " " + countBranchMigrations(sbs, mats3));
	assert countBranchMigrations(sbs, mats1) == 2;
	assert countBranchMigrations(sbs, mats2) == 1; // helix is bordering, must be only one branch migration
	assert countBranchMigrations(sbs, mats3) == 0; // there should be no branch migration
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** returns error score for complete secondary structure and trial sequences */
    public double scoreStructure(StringBuffer[] bseqs,
				 SecondaryStructure structure,
				 int[][][][] interactionMatrices) {
	String scoreString = generateReport(bseqs, structure, interactionMatrices, 0).getProperty("score");
	assert scoreString != null;
	return Double.parseDouble(scoreString);
    }

    /** returns error score for complete secondary structure and trial sequences */
    public Properties generateReport(StringBuffer[] bseqs,
				     SecondaryStructure structure,
				     int[][][][] interactionMatrices) {
	return generateReport(bseqs, structure, interactionMatrices, 10);
    }

    /** returns error score for complete secondary structure and trial sequences */
    @SuppressWarnings(value="unchecked")
    public Properties generateReport(StringBuffer[] bseqs,
				     SecondaryStructure structure,
				     int[][][][] interactionMatrices,
				     int verbosity) {
	Properties resultProperties =new Properties();
	double score = 0.0;
	SecondaryStructureScriptFormatWriter secWriter = new SecondaryStructureScriptFormatWriter();
	List<String[]> secStringArrays = secWriter.writeSecondaryStructure(structure);
	String[] secStrings = new String[secStringArrays.size()]; // convert to ...AAA... ; ..AAA.BB.. ; ..BB.... or the like
	for (int i = 0; i < secStringArrays.size(); ++i) {
	    secStrings[i] = (StringTools.paste(secStringArrays.get(i), ""));
	    if (debugLevel > 0) {
		System.out.println(bseqs[i].toString());
		System.out.println(secStrings[i]);
	    }
	}
	assert secStrings.length == structure.getSequenceCount();
	String[][] critons = new String[bseqs.length][];
	String[][] smallCritons = new String[bseqs.length][]; // ENCOURAGE small helices for each strand
	Set<String> critSet = new HashSet<String>();
	Set<String> complSet = new HashSet<String>();
	Set<String>[] smallComplSets = null; // (Set<String>[]);
	smallComplSets = (Set<String>[])(Array.newInstance(Set.class, bseqs.length)); // new HashSet<String>();
	// (ArrayList<Node>[])
	// Array.newInstance(ArrayList.class,n);
	Set<String> bulgeSet = new HashSet<String>();
	Set<String> bulgeCompSet = new HashSet<String>();
	int critCount = 0;
	int critonLen2 = critonLen - 2; // smaller critons for encourageing self-folding  
	for (int i = 0; i < bseqs.length; i++) {
	    smallComplSets[i] = new HashSet<String>();
	    critons[i] = createCritons(bseqs[i].toString(), critonLen);
	    smallCritons[i] = createCritons(bseqs[i].toString(), critonLen2);
	    String[] bulgeCritons = createCritons(bseqs[i].toString(), critonLen+1);
	    for (int j = 0; j < critons[i].length; ++j) {
		critSet.add(critons[i][j].toLowerCase()); // duplicates are overwritten in set
		complSet.add(RnaSecondaryTools.getRnaComplement(critons[i][j]).toLowerCase());
		if (j < bulgeCritons.length) {
		    bulgeSet.add(bulgeCritons[j].toLowerCase());
		    bulgeCompSet.add(RnaSecondaryTools.getRnaComplement(bulgeCritons[j]).toLowerCase());
		}
	    } 
	    for (int j = 0; j < smallCritons[i].length; ++j) {
		smallComplSets[i].add(RnaSecondaryTools.getRnaComplement(smallCritons[i][j]).toLowerCase());
	    }
	    critCount += critons[i].length;
	}
	int duplicateCount = critCount - critSet.size(); // counts number of critons that are not unique
	assert duplicateCount >= 0;
	double auViolationTerm = 0.0;
	double duplicateTerm = duplicateCount * nonUniquePenalty;
	double complementTerm = 0.0;
	double selfComplementTerm = 0.0;
	double consecutiveTerm = 0.0;
	double branchMigrationTerm = 0.0;
	double bulgesTerm = 0.0;
	double bulgesComplementTerm = 0.0;
	double gcViolationTerm = 0.0;
	double simTerm = 0.0;
	double simComplementTerm = 0.0;
	double zipperTerm = 1.0;
	double smallComplTerm = 0;
	double smallComplWeight = 0.1; // 1.0;
	for (int i = 0; i < critons.length; i++) { // critons are grouped by sequence
	    for (int j = 0; j < critons[i].length; j++) {
		String criton = critons[i][j];
		// if ( nonUniquePenalty > 0 && !isUnique(critons, i, j) ) {
		// double subscore = nonUniquePenalty;
		// score += subscore;
		// }
// 		if (isUniformlyBasePaired(j, critonLen, secStrings[i])) {
// 		    // check that each base occurs only twice:
// 		}
		if ( (complementPenalty != 0.0) && complSet.contains(criton) &&  unwantedComplementExists(critons, i, j, interactionMatrices) ) {
		    complementTerm += complementPenalty;
		}
		if ( (selfComplementPenalty != 0.0) && isSelfComplementary(criton) ) {
		    selfComplementTerm += selfComplementPenalty;
		}
		if (bulgePenalty != 0.0) {
		    String[] bulges = findAllBulges(criton);
		    for (int n = 0; n < bulges.length; n++) {
			if (bulgeSet.contains(bulges[n].toLowerCase())) {
			    bulgesTerm += bulgePenalty;
			}
			if (bulgeCompSet.contains(bulges[n].toLowerCase())) {
			    bulgesComplementTerm += bulgePenalty;
			}
		    }
		}
		if (similarPenalty != 0.0) {
		    String[] similar = findAllSimilar(criton);
		    for (int n = 0; n < similar.length; n++) {
			if (critSet.contains(similar[n].toLowerCase())) {
			    simTerm += similarPenalty;
			}
			if (complSet.contains(similar[n].toLowerCase())) {
			    simComplementTerm += similarPenalty;
			}
		    }
		}
	    }
	    if (smallComplWeight != 0.0) {
		for (int j = 0; j < smallCritons[i].length; ++j) {
		    // check if there is a small complement within the same sequence:
		    if (!smallComplSets[i].contains(smallCritons[i][j])) { // we WANT small level complementarity within the same sequence
			smallComplTerm += smallComplWeight;
		    }
		}
	    }
	}
	if (auViolationWeight != 0.0) {
	    for (int i = 0; i < bseqs.length; ++i) {
		int numberTooOftenCounts = countAUViolations(bseqs[i], secStrings[i]); // counts number of consecutive A/U bases that occur too often
		auViolationTerm += auViolationWeight * numberTooOftenCounts;
	    }
	}
	if (gcViolationWeight != 0.0) {
	    for (int i = 0; i < bseqs.length; ++i) {
		int numberTooOftenCounts = countGCViolations(bseqs[i], secStrings[i]); // counts number of consecutive A/U bases that occur too often
		gcViolationTerm += gcViolationWeight * numberTooOftenCounts;
	    }
	}
	if (consecutiveWeight != 0.0) {
	    for (int i = 0; i < bseqs.length; ++i) {
		int numberTooOftenCounts = countTooOftenConsecutive(bseqs[i].toString(), consecutiveLimit, consecutiveLimitG); // counts number of bases that occur too often
		consecutiveTerm += consecutiveWeight * numberTooOftenCounts;
	    }
	}
	if (branchMigrationWeight != 0.0) {
	    branchMigrationTerm = branchMigrationWeight * countBranchMigrations(bseqs, interactionMatrices);
	}
	if (zipperWeight != 0.0) {
	    zipperTerm = zipperWeight * countZipperViolations2(bseqs, secStrings);
	}
	double defaultScoreTerm = 0.0;
	if (defaultScorerWeight != 0.0) {
	    defaultScoreTerm = defaultScorerWeight * defaultScorer.scoreStructure(bseqs, structure, interactionMatrices);
	}
	double gcMinFracTerm = 0.0;
	if (gcMinFracWeight != 0.0) {
	    for (int i = 0; i < bseqs.length; ++i) {
		int numberTooOftenCounts = countGCFracViolations(bseqs[i], secStrings[i]);
		gcMinFracTerm += gcMinFracWeight * numberTooOftenCounts;
	    }
	}
	score = defaultScoreTerm + duplicateTerm + complementTerm + smallComplTerm + selfComplementTerm + consecutiveTerm + branchMigrationTerm + bulgesTerm 
	           + bulgesComplementTerm + gcViolationTerm + simTerm + simComplementTerm + zipperTerm + auViolationTerm + gcMinFracTerm;
	if (debugLevel > 0) {
	    log.info("Total score, default score, duplicate, complement, smallComplement, selfComplement, consecutive, branch-migration, bulges, bulgesComplement, gcViolationTerm, sim, simComlement, zipper, auViolation, gcMinFracWeight: " 
		     + score + " " + defaultScoreTerm + " " + duplicateTerm + " " + complementTerm + " " + smallComplTerm + " " + selfComplementTerm 
		     + " " + consecutiveTerm + " " + branchMigrationTerm + " " + bulgesTerm + " " + bulgesComplementTerm + " " + gcViolationTerm + " "
		     + simTerm + " " + simComplementTerm + " " + zipperTerm + " " + auViolationTerm + " " + gcMinFracTerm);
	}
	assert score >= 0.0;
	resultProperties.setProperty("score", "" + score);
	if ((verbosity > 1) && (defaultScorer != null) && (defaultScorerWeight != 0.0)) {
	    PropertyTools.mergeProperties(resultProperties,
			  defaultScorer.generateReport(bseqs, structure, interactionMatrices),
		       	  "subscorer.");
	}
	return resultProperties;
    }
}
