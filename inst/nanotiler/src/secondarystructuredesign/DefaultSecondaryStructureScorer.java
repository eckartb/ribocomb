package secondarystructuredesign;

import java.util.Properties;
import rnasecondary.*;
import java.util.Random;
import generaltools.Randomizer;

public class DefaultSecondaryStructureScorer implements SecondaryStructureScorer {

    private double mismatchPenalty = 10.0;
    private double matchPenalty = 5; // 0.1 // 1; // 0.1;
    private double sameResiduePenalty = 0.2;
    private double gcContent = 0.6; // target G+C content
    private Random rand = Randomizer.getInstance();

    private boolean isComplementary(char c1, char c2) {
	return RnaSecondaryTools.isRnaComplement(c1, c2);
    }

    private boolean isWatsonCrick(char c1, char c2) {
	return RnaSecondaryTools.isWatsonCrick(c1, c2);
    }

    private double scoreStructureSequence(StringBuffer bseq,
					  int[][] interactions) {
	assert interactions.length == bseq.length();
	double score = 0.0;
	for (int i = 0; i < interactions.length; ++i) {
	    if ((i >= 2) 
		&& (bseq.charAt(i) == bseq.charAt(i-1))
		&& (bseq.charAt(i) == bseq.charAt(i-2))) {
		score += sameResiduePenalty; // avoid same residues in a row
	    }
	    for (int j = i+1; j < interactions.length; ++j) {
		if (interactions[i][j] == RnaInteractionType.WATSON_CRICK) {
		    if (!isWatsonCrick(bseq.charAt(i), bseq.charAt(j))) {
			score += mismatchPenalty;
		    }
		}
		else if (interactions[i][j] == RnaInteractionType.NO_INTERACTION) { // there should be no interaction with stem length 5 or more
		    if (isWatsonCrick(bseq.charAt(i), bseq.charAt(j))) {
			if ((i > 0)&&((j + 1)< bseq.length())
			    &&(isWatsonCrick(bseq.charAt(i-1), bseq.charAt(j+1)))) {
			    if ((j > 0)&&((i + 1)< bseq.length())
				&&(isWatsonCrick(bseq.charAt(i+1), bseq.charAt(j-1)))) {
				if ((i > 1) && ((j + 2)< bseq.length())
				    && (isWatsonCrick(bseq.charAt(i-2), bseq.charAt(j+2)))) {
				    if ((j > 1) && ((i + 2)< bseq.length())
					&& (isWatsonCrick(bseq.charAt(i+2), bseq.charAt(j-2)))) {
					score += matchPenalty;
				    }
				}
			    }
			}
		    }
		}
	    }
	}
	return score;
    }

    private double scoreStructureSequencePair(StringBuffer bseq1,
					      StringBuffer bseq2,
					      int[][] interactions) {
	assert interactions.length == bseq1.length();
	assert interactions[0].length == bseq2.length();
// 	log.info("Scoring sequences " + NEWLINE + bseq1.toString() + NEWLINE + bseq2.toString());
// 	IntegerArrayTools.writeMatrix(System.out, interactions);
	double score = 0.0;
	for (int i = 0; i < interactions.length; ++i) {
	    for (int j = 0; j < interactions[i].length; ++j) {
		if (interactions[i][j] == RnaInteractionType.WATSON_CRICK) {
		    if (!isWatsonCrick(bseq1.charAt(i), bseq2.charAt(j))) {
			score += mismatchPenalty;
		    }
		}
		else if (interactions[i][j] == RnaInteractionType.NO_INTERACTION) { // there should be no interaction of stem length 5 or more
		    if (isWatsonCrick(bseq1.charAt(i), bseq2.charAt(j))) {
 			if ((i > 0) && ((j + 1) < bseq2.length())&&(isWatsonCrick(bseq1.charAt(i-1), bseq2.charAt(j+1)))) {
			    if ((j > 0) && ((i+1) < bseq1.length())
				&& (isWatsonCrick(bseq1.charAt(i+1), bseq2.charAt(j-1)))) {
				if ((i > 1) && ((j + 2) < bseq2.length())&&(isWatsonCrick(bseq1.charAt(i-2), bseq2.charAt(j+2)))) {
				    if ((j > 1) && ((i + 2) < bseq1.length())&&(isWatsonCrick(bseq1.charAt(i+2), bseq2.charAt(j-2)))) {
					score += matchPenalty;
				    }
				}
			    }
			}
		    }
		}
	    }
	}
	// System.out.println("Result score of sequence pair: " + score);
	return score;
    }

    /** returns error score for complete secondary structure and trial sequences */
    public double scoreStructure(StringBuffer[] bseqs,
				 SecondaryStructure structure,
				 int[][][][] interactionMatrices) {
	String scoreString = generateReport(bseqs, structure, interactionMatrices,0).getProperty("score");
	assert scoreString != null;
	return Double.parseDouble(scoreString);
    }

    /** returns error score for complete secondary structure and trial sequences */
    public Properties generateReport(StringBuffer[] bseqs,
				     SecondaryStructure structure,
				     int[][][][] interactionMatrices) {
	return generateReport(bseqs, structure, interactionMatrices, 10); // very verbose
    }

    /** returns error score for complete secondary structure and trial sequences */
    public Properties generateReport(StringBuffer[] bseqs,
					 SecondaryStructure structure,
					 int[][][][] interactionMatrices,
					 int verbosity) {
	Properties resultProperties = new Properties();
	double score = 0.0;
	for (int i = 0; i < bseqs.length; ++i) {
	    score += scoreStructureSequence(bseqs[i], interactionMatrices[i][i]);
	    for (int j = i+1; j < bseqs.length; ++j) {
		score += scoreStructureSequencePair(bseqs[i], bseqs[j], interactionMatrices[i][j]);
	    }
	}
	resultProperties.setProperty("score", "" + score);
	return resultProperties;
    }

}
