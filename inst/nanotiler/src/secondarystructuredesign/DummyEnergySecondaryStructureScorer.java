package secondarystructuredesign;

import rnasecondary.*;
import java.util.Properties;

/** Assigns a score to a secondary structure and a set of potential sequences */
public class DummyEnergySecondaryStructureScorer implements SecondaryStructureScorer {

    private double energyAU = -0.8;
    private double energyGC = -1.2;
    private boolean zeroMode = false; // if true, always return 0 score. 

    /** Dummy energies of Watson-Crick pairings: AU, GC, UA, CG (but not GU, UG) < 0, all others: 0.0 */
    public double interactionEnergy(char c1, char c2) {
	c1 = Character.toUpperCase(c1);
	c2 = Character.toUpperCase(c2);
	if (c1 == c2) {
	    return 0.0;
	}
	if (c1 > c2) {
	    return interactionEnergy(c2, c1);
	}
	switch (c1) {
	case 'A':
	    if (c2 == 'U') {
		return energyAU;
	    }
	case 'C':
	    if (c2 == 'G') {
		return energyGC;
	    }
	}
	return 0.0;
    }


    private double energyStructureSequence(StringBuffer bseq,
					  int[][] interactions) {
	assert interactions.length == bseq.length();
	double score = 0.0;
	for (int i = 0; i < interactions.length; ++i) {
	    for (int j = i+2; j < interactions.length; ++j) {
		if (interactions[i][j] == RnaInteractionType.WATSON_CRICK) {
		    score += interactionEnergy(bseq.charAt(i), bseq.charAt(j));
		}
	    }
	}
	return score;
    }
    
    private double energyStructureSequencePair(StringBuffer bseq1,
					       StringBuffer bseq2,
					       int[][] interactions) {
	assert interactions.length == bseq1.length();
	assert interactions[0].length == bseq2.length();
// 	log.info("Scoring sequences " + NEWLINE + bseq1.toString() + NEWLINE + bseq2.toString());
// 	IntegerArrayTools.writeMatrix(System.out, interactions);
	double score = 0.0;
	for (int i = 0; i < interactions.length; ++i) {
	    for (int j = 0; j < interactions[i].length; ++j) {
		if (interactions[i][j] != RnaInteractionType.NO_INTERACTION) {
		    score += interactionEnergy(bseq1.charAt(i), bseq2.charAt(j));
		}
	    }
	}
	// System.out.println("Result score of sequence pair: " + score);
	return score;
	
    }

    /** returns error score for complete secondary structure and trial sequences */
    public double scoreStructure(StringBuffer[] bseqs,
				 SecondaryStructure structure,
				 int[][][][] interactionMatrices) {
	String scoreString = generateReport(bseqs, structure, interactionMatrices,0).getProperty("score");
	assert scoreString != null;
	return Double.parseDouble(scoreString);
    }

    /** returns error score for complete secondary structure and trial sequences */
    public Properties generateReport(StringBuffer[] bseqs,
				     SecondaryStructure structure,
				     int[][][][] interactionMatrices) {
	return generateReport(bseqs, structure, interactionMatrices, 10); // very verbose
    }


    /** returns error score for complete secondary structure and trial sequences */
    public Properties generateReport(StringBuffer[] bseqs,
				     SecondaryStructure structure,
				     int[][][][] interactionMatrices,
				     int verbosity) {
	Properties resultProperties = new Properties();
	double score = 0.0;
	if (!zeroMode) {
	    for (int i = 0; i < bseqs.length; ++i) {
		score += energyStructureSequence(bseqs[i], interactionMatrices[i][i]); // same sequence
		for (int j = i+1; j < bseqs.length; ++j) {
		    score += energyStructureSequencePair(bseqs[i], bseqs[j], interactionMatrices[i][j]); // sequence pair
		}
	    }
	}
	resultProperties.setProperty("score", "" + score);
	return resultProperties;
    }

    public void setZeroMode(boolean b) { this.zeroMode = b; }

}
