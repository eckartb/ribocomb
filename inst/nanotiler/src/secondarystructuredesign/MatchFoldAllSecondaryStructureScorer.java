package secondarystructuredesign;

import java.util.Properties;
import java.util.logging.*;
import rnasecondary.*;
import numerictools.AccuracyTools;
import sequence.*;
import static rnasecondary.RnaInteractionType.*;

/** Assigns a score to a secondary structure and a set of potential sequences */
public class MatchFoldAllSecondaryStructureScorer extends AbstractSecondaryStructureScorer {

    public static final double YIELD_LOW_LIM = 1e-100;    
    private double scale = 1.0; // scale factor for result;
    private Level debugLevel = Level.FINE;
    private static Logger log = Logger.getLogger(PackageConstants.LOGFILE_DEFAULT);
    private double yieldWeight = 10.0;

    /** returns error score for complete secondary structure and trial sequences */
    public double scoreStructure(StringBuffer[] bseqs,
				 SecondaryStructure structure,
				 int[][][][] interactionMatrices) {
	String scoreString = generateReport(bseqs, structure, interactionMatrices,0).getProperty("score");
	assert scoreString != null;
	return Double.parseDouble(scoreString);
    }

    /** returns error score for complete secondary structure and trial sequences */
    public Properties generateReport(StringBuffer[] bseqs,
				     SecondaryStructure structure,
				     int[][][][] interactionMatrices) {
	return generateReport(bseqs, structure, interactionMatrices, 10); // very verbose
    }


    /** returns error score for complete secondary structure and trial sequences */
    public Properties generateReport(StringBuffer[] bseqs,
				     SecondaryStructure structure,
				     int[][][][] interactionMatrices,
				     int verbosity) {
	// System.out.println("Starting MatchfoldSecondaryStructureScorer.scoreStructure...");
	Properties resultProperties = new Properties();
	UnevenAlignment ali = null;
	try {
	    ali = generateAlignment(bseqs);
	}
	catch (UnknownSymbolException use) {
	    System.out.println(use.getMessage());
	    assert false;
	}
	catch (DuplicateNameException dne) {
	    System.out.println(dne.getMessage());
	    assert false;
	}
	int totLen = 0;
	for (StringBuffer bseq : bseqs) {
	    totLen += bseq.length();
	}
	// log.log(debugLevel, "Starting SimpleSecondaryStructurePredictor...");
	MatchFoldAllSecondaryStructurePredictor predictor = new MatchFoldAllSecondaryStructurePredictor(ali);
	predictor.run();
	SecondaryStructure prediction = (SecondaryStructure)(predictor.getResult());
	assert prediction != null;
	// log.log(debugLevel, "Finished SimpleSecondaryStructurePredictor. Starting accuracy estimation...");
	double result = scorePrediction(prediction, structure, interactionMatrices);
	double errorFrac = result / (double)(totLen);
	double yieldFrac = MatchFoldAllSecondaryStructurePredictor.YIELD_MAX - prediction.getEnergy(); // 1-yieldfraction : 0 for 100% yield, 1.0 for 0.0% yield : fraction misfolded
	// log.log(debugLevel, "Finished accuracy estimation with result: " + result);
	if (yieldFrac < YIELD_LOW_LIM) {
	    yieldFrac = YIELD_LOW_LIM;
	}
	double finalResult = errorFrac - yieldWeight * Math.log(yieldFrac);
	assert finalResult >= 0.0;
	// System.out.println("Finished MatchfoldSecondaryStructureScorer.scoreStructure: " + result);
	resultProperties.setProperty("score", "" + finalResult);
	return resultProperties;
    }

}
