package secondarystructuredesign;

import java.util.Properties;
import java.util.logging.*;
import rnasecondary.*;
import numerictools.AccuracyTools;
import sequence.*;
import static rnasecondary.RnaInteractionType.*;

/** Assigns a score to a secondary structure and a set of potential sequences */
public class MatchFoldSecondaryStructureScorer extends AbstractSecondaryStructureScorer {
    
    private double scale = 1.0; // scale factor for result;
    private Level debugLevel = Level.FINE;
    private static Logger log = Logger.getLogger(PackageConstants.LOGFILE_DEFAULT);


    /** returns error score for complete secondary structure and trial sequences */
    public double scoreStructure(StringBuffer[] bseqs,
				 SecondaryStructure structure,
				 int[][][][] interactionMatrices) {
	// System.out.println("Starting MatchfoldSecondaryStructureScorer.scoreStructure...");
	UnevenAlignment ali = null;
	try {
	    ali = generateAlignment(bseqs);
	}
	catch (UnknownSymbolException use) {
	    System.out.println(use.getMessage());
	    assert false;
	}
	catch (DuplicateNameException dne) {
	    System.out.println(dne.getMessage());
	    assert false;
	}
	// log.log(debugLevel, "Starting SimpleSecondaryStructurePredictor...");
	MatchFoldSecondaryStructurePredictor predictor = new MatchFoldSecondaryStructurePredictor(ali);
	predictor.run();
	SecondaryStructure prediction = (SecondaryStructure)(predictor.getResult());
	assert prediction != null;
	// log.log(debugLevel, "Finished SimpleSecondaryStructurePredictor. Starting accuracy estimation...");
	double result = scorePrediction(prediction, structure, interactionMatrices);
	// log.log(debugLevel, "Finished accuracy estimation with result: " + result);
	assert result >= 0.0;
	// System.out.println("Finished MatchfoldSecondaryStructureScorer.scoreStructure: " + result);
	return result;
    }

    /** returns error score for complete secondary structure and trial sequences */
    public Properties generateReport(StringBuffer[] bseqs,
				     SecondaryStructure structure,
				     int[][][][] interactionMatrices) {
	// System.out.println("Starting MatchfoldSecondaryStructureScorer.scoreStructure...");
	Properties resultProperties = new Properties();
	UnevenAlignment ali = null;
	try {
	    ali = generateAlignment(bseqs);
	}
	catch (UnknownSymbolException use) {
	    System.out.println(use.getMessage());
	    assert false;
	}
	catch (DuplicateNameException dne) {
	    System.out.println(dne.getMessage());
	    assert false;
	}
	// log.log(debugLevel, "Starting SimpleSecondaryStructurePredictor...");
	MatchFoldSecondaryStructurePredictor predictor = new MatchFoldSecondaryStructurePredictor(ali);
	predictor.run();
	SecondaryStructure prediction = (SecondaryStructure)(predictor.getResult());
	resultProperties.setProperty("prediction",new SecondaryStructureScriptFormatWriter().writeString(prediction));
	assert prediction != null;
	// log.log(debugLevel, "Finished SimpleSecondaryStructurePredictor. Starting accuracy estimation...");
	double result = scorePrediction(prediction, structure, interactionMatrices);
	resultProperties.setProperty("score", "" + result);
	// log.log(debugLevel, "Finished accuracy estimation with result: " + result);
	assert result >= 0.0;
	// System.out.println("Finished MatchfoldSecondaryStructureScorer.scoreStructure: " + result);
	return resultProperties;
    }


}
