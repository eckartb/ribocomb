package secondarystructuredesign;

import generaltools.StringTools;
import java.io.*;
import java.util.*;
import java.util.logging.*;
import rnasecondary.*;
import numerictools.AccuracyTools;
import sequence.*;
import launchtools.*;
import static rnasecondary.RnaInteractionType.*;

/** Assigns a score to a secondary structure and a set of potential sequences */
public class MatchFoldWeightedProbScorer extends AbstractSecondaryStructureScorer {
    
    private double scale = 1.0; // scale factor for result;
    private Level debugLevel = Level.INFO;
    private static Logger log = Logger.getLogger(PackageConstants.LOGFILE_DEFAULT);
    private static String scriptName = "matchfoldweightedprob.pl"; // matchfold.sh"; // rb.getString("matchfoldscript");
    private boolean debugMode = false;

    /** launches RNAcofold or pknotsRG with given sequence, returns the raw line output  */
    private String[] launchFolding(StringBuffer[] sequences,
				   SecondaryStructure structure) throws IOException {
	assert(structure.weightsSanityCheck());
	// write secondary structure to temporary file
	File tmpDir = new File("."); // alternatives: /tmp 
	File tmpInputFile = File.createTempFile("nanotiler_matchfold",".seq", tmpDir);
	if (!debugMode) {
	    tmpInputFile.deleteOnExit();
	}
	String inputFileName = tmpInputFile.getAbsolutePath(); // "tmpin.seq"; // 
	if (debugLevel == Level.FINE) {
	    log.info("temp file for MatchFold results: " + inputFileName);
	}
	// File tmpInputFile = new File(inputFileName); // File.createTempFile("nanotiler_matchfold",".seq");
	FileWriter bf = new FileWriter(inputFileName); // inputFileName);
	// FileOutputStream fos = new FileOutputStream(tmpInputFile);
	// write secondary structure, but only write sec structure, not sequence:
        if (debugMode) {
	    System.out.println("Writing to file: " + inputFileName);
        }
	// log.log(debugLogLevel,"Writing content: " + sequence);
	// ps.println(sequence); // no special formatting needed!
	SecondaryStructureCTFormatWriter writer = new SecondaryStructureCTFormatWriter();
	StringBuffer buf = new StringBuffer();
	for (int i = 0; i < sequences.length; ++i) {
	    buf.append(sequences[i]); // generate concatenated sequences
	}
	bf.write(writer.writeString(buf.toString(), structure)); // write CT format with alternative sequence
        bf.close();
	int nTot = buf.length(); // total number of nucleotides
	//	assert (tmpInputFile.exists());
	//	fos.close();
	// assert (tmpInputFile.exists());
        // FileReader fr = new FileReader(inputFileName);
        // System.out.println("file " + inputFileName  + " exists!");
        // fr.close(); // 
	File tmpOutputFile = File.createTempFile("nanotiler_matchfold","_prob.dat", tmpDir);
	String outputFileName = tmpOutputFile.getAbsolutePath(); // "nanotiler_matchfoldprob_outtmp.sec"; // 
	if (tmpOutputFile.exists()) {
	    tmpOutputFile.delete();
	}
	if (!debugMode) {
	    tmpOutputFile.deleteOnExit();
	}

	File tmpWeightFile = File.createTempFile("nanotiler_matchfold","_weights.dat", tmpDir);
	String weightFileName = tmpWeightFile.getAbsolutePath(); // "nanotiler_matchfoldprob_outtmp.sec"; // 
	if (tmpWeightFile.exists()) {
	    tmpWeightFile.delete();
	}
	if (!debugMode) {
	    tmpWeightFile.deleteOnExit();
	}

	FileWriter bf2 = new FileWriter(weightFileName); // inputFileName);
	bf2.write("" + nTot + " ");
	for (int i = 0; i < structure.getSequenceCount(); ++i) {
	    List<Double> weights = structure.getWeights(i);
	    for (int j = 0; j < weights.size(); ++j) {
		bf2.write(" " + weights.get(j));
	    }
	}
	bf2.write("\n");
        bf2.close();

	// generate command string
	// File tempFile = new File(scriptName);
	String[] commandWords = { scriptName, inputFileName, outputFileName, weightFileName}; 
	// generate command
	RunCommand command = new SimpleRunCommand(commandWords);
	// System.out.println("Issuing command: " + scriptName + " " + inputFileName + " " + outputFileName);
	if (debugLevel.equals(Level.FINE)) {
	    log.info("Issuing command: " + scriptName + " " + inputFileName + " " + outputFileName + " " + weightFileName);
	}
	// create queue manager (singleton pattern)
	QueueManager queueManager = SimpleQueueManager.getInstance();
	// generate job
	Job job = queueManager.createJob(command);
	
	// add listener to job !?
	
	// launch command
	queueManager.submit(job);	    
	// log.log(debugLogLevel,"queue manager finished job!");
	// open output file:
	// log.log(debugLogLevel,"Importing optimized sequences from " + outputFileName);
	String[] resultLines = null;
	FileInputStream resultFile = null;
	try {
	    resultFile = new FileInputStream(outputFileName);
	    resultLines = StringTools.readAllLines(resultFile);
	    // sequenceStrings = RnaFoldTools.getSequenceStrings(resultLines);
	}
	catch (IOException ioe) {
	    log.warning("Error when scraping result file from: " 
			+ outputFileName + " : " + ioe.getMessage());
	    assert false;
	    throw ioe;
	}
	finally {
	    if (resultFile != null) {
		resultFile.close();
		if (!debugMode) {
		    File file = new File(outputFileName);
		    file.delete(); // reactivate
		}
	    }
	    if (!debugMode) {
		if (tmpInputFile != null) { // reactivate!
		    tmpInputFile.delete();
		}
		if (tmpWeightFile != null) { // reactivate!
		    tmpWeightFile.delete();
		}
	    }
	}
	if (resultLines == null) {
	    assert false;
	    log.warning("matchfold results were null!");
	}
	return resultLines;
    }

    /** returns error score for complete secondary structure and trial sequences */
    public double scoreStructure(StringBuffer[] bseqs,
				 SecondaryStructure structure,
				 int[][][][] interactionMatrices) {
	// System.out.println("Starting MatchfoldSecondaryStructureScorer.scoreStructure...");
	double result = 1.0;
	int numResidues = structure.getSequences().getTotalResidueCount();
	assert(numResidues > 0);
	// log.log(debugLevel, "Starting SimpleSecondaryStructurePredictor...");
	try {
	    String[] resultLines = launchFolding(bseqs, structure);
	    if (resultLines.length > 0) {
		String lastLine = resultLines[resultLines.length-1];
		String[] words = lastLine.split(" ");
		if ((words.length >= 2) && (words[0].equals("Score:"))) {
		    result = Double.parseDouble(words[1]);
		    if (debugMode) {
			System.out.println("MatchFold probability matrix score: " + result); //  + " residue-weighted: " + result * numResidues);
		    }
		    // result *= numResidues; // multiply by number of residues
		} else {
		    log.warning("Error parsing matchfold results: " + resultLines.length + " : " + lastLine);
		    for (int i = 0; i < resultLines.length; ++i) {
			System.out.println(resultLines[i]);
		    }
		}
	    }
	} catch (IOException ioe) {
	    log.warning("Error obtaining results from matchfold script: " + ioe.getMessage());
	} catch (NumberFormatException nfe) {
	    log.warning("Error parsing matchfold score: " + nfe.getMessage());
	}
	// log.log(debugLevel, "Finished accuracy estimation with result: " + result);
	assert result >= 0.0;
	// System.out.println("Finished MatchfoldSecondaryStructureScorer.scoreStructure: " + result);
	return result;
    }

    /** returns error score for complete secondary structure and trial sequences */
    public Properties generateReport(StringBuffer[] bseqs,
				     SecondaryStructure structure,
				     int[][][][] interactionMatrices) {
	assert(structure.weightsSanityCheck());
	// System.out.println("Starting MatchfoldSecondaryStructureScorer.scoreStructure...");
	Properties resultProperties = new Properties();
	double result = scoreStructure(bseqs, structure, interactionMatrices);
	resultProperties.setProperty("score", "" + result);
	assert result >= 0.0;
	return resultProperties;
    }


}
