package secondarystructuredesign;

import rnasecondary.*;
import java.util.logging.*;
import sequence.*;
import java.util.*;
import generaltools.PropertyCarrier;
import generaltools.PropertyTools;
import generaltools.Randomizer;
import generaltools.SimplePropertyCarrier;
import numerictools.IntegerArrayTools;
import static secondarystructuredesign.PackageConstants.*;

/** generates a set of sequences optimized to fullfill secondary structure */
public class MonteCarloSequenceOptimizer extends SimplePropertyCarrier implements SequenceOptimizer {
    private Logger log = Logger.getLogger("NanoTiler_debug");
    private Alphabet alphabet = DnaTools.RNA_ALPHABET;
    private double errorScoreLimit = 0.0; // limit for acceptable solution
    private double errorScoreLimit2 = 0.0; // limit for acceptable solution
    private int iterMax = 500;
    private int iter2Max = 200;
    private int rerun = 1;
    private double kt = 0.5;
    private double kt2 = 0.2;
    private double gcContent = 0.6; // target G+C content
    private boolean ignoreNotConstantMode = false; // if true, set all non-constant residue interactions as UNKNOWN_SUBTYPE, effectively ignoring them
    private double mismatchPenalty = 1.0;
    private double matchPenalty = 1.0; // 1; // 0.1;
    private double energyAU = -0.8;
    private double energyGC = -1.2;
    private double sameResiduePenalty = 0.2; // 0.1; // 0.1;
    private double finalScoreWeight = 0.1;
    private Random rand = Randomizer.getInstance();
    private boolean randomizeFlag = true; // true;
    private int[] nucleotideProbabilities;
    private static final int CHAR_HELP_SIZE = 100;
    private char[] nucleotideHelperChars;
    private SecondaryStructureScorer defaultScorer = new CritonScorer(); // new DefaultSecondaryStructureScorer();
    private SecondaryStructureScorer finalScorer = new RnacofoldSecondaryStructureScorer();
    private int stride1 = 1000;
    private int stride2 = 100;
    private boolean doSanityChecks = false; // true;
    private Level debugLogLevel = Level.INFO;
    private SequenceConstraintScorer constraintScorer = null;
    private String algorithm = "twostage";

    public MonteCarloSequenceOptimizer() {
	nucleotideProbabilities = new int[alphabet.size()];
	initProbabilities();
    }

    public int getIterMax() { return iterMax; }


    public int getIter2Max() { return iter2Max; }

    private void initProbabilities() {
	nucleotideProbabilities[0] = (int)(CHAR_HELP_SIZE * (1-gcContent)/2); // A content in percent
	nucleotideProbabilities[1] = (int)(CHAR_HELP_SIZE * (gcContent)/2); // C
	nucleotideProbabilities[2] = (int)(CHAR_HELP_SIZE * (gcContent)/2); // G
	nucleotideProbabilities[3] = (int)(CHAR_HELP_SIZE * (1-gcContent)/2); // U
	int sum = 0;
	for (int i = 0; i < nucleotideProbabilities.length; ++i) {
	    sum += nucleotideProbabilities[i];
	}
	nucleotideHelperChars = new char[sum];
	int pc = 0;
	for (int i = 0; i < nucleotideProbabilities.length; ++i) {
	    int numChar = nucleotideProbabilities[i];
	    for (int j = 0; j < numChar; ++j) {
		if (pc >= nucleotideHelperChars.length) {
		    assert false; // should never happen
		}
		nucleotideHelperChars[pc++] = alphabet.getSymbol(i).getCharacter();
	    } 
	}
	if (pc != CHAR_HELP_SIZE) {
	    log.severe("Bad helper index: " + pc + " " + CHAR_HELP_SIZE);
	}
	StringBuffer buf = new StringBuffer();
	for (int i = 0; i <  nucleotideHelperChars.length; ++i) {
	    buf.append("" + nucleotideHelperChars[i]);
	}
	log.finest("Helper character array: " + buf.toString());
    }

    // internal class describing mutation of n'th sequence at position p with character c 
    private class Mutation {
	private int seqId; // sequence id
	private int pos; // position in sequence
	private char character; // character
	public Mutation(int seqId, int pos, char character) {
	    this.seqId = seqId;
	    this.pos = pos;
	    this.character = character;
	}
	public int getPos() { return pos; }
	public char getCharacter() { return character; }
	public int getSeqId() { return seqId; }
	public void setCharacter(char c) { this.character = c; }
        public String toString() { return "" + (seqId+1) + " " + (pos+1) + " " + character; }
    }

    public Properties eval(SecondaryStructure structure) {
	assert finalScorer != null;
	assert defaultScorer != null;
	Properties properties = new Properties();
	log.info("Starting MonteCarloSequenceOptimizer.eval !");
	this.constraintScorer = new SequenceConstraintScorer(structure);
	int[][][][] interactionMatrices = generateInteractionMatrices(structure);
	StringBuffer[] bseqs = initStringBuffers2(structure, false, interactionMatrices, structure.getAlphabets()); // false stands for no randomization
	// apply default scorer:
	log.info("Starting evaluation with sequences: " + NEWLINE + generateSequencesOutputString(bseqs));
	if (!sanityCheck(bseqs, structure, interactionMatrices)) {
	    log.log(debugLogLevel, "Not all base pairs are yet compatible!");
	}
	if (doSanityChecks) {
	    sanityCheck(bseqs, structure, interactionMatrices);
	}
        try {
	    Properties defaultProperties = defaultScorer.generateReport(bseqs, structure, interactionMatrices);
	    double errorScore = Double.parseDouble(defaultProperties.getProperty("score"));
	    Properties finalProperties = finalScorer.generateReport(bseqs, structure, interactionMatrices);
	    double errorScore2 = Double.parseDouble(finalProperties.getProperty("score"));
	    double errorScoreTot = errorScore + finalScoreWeight * errorScore2;
	    properties.setProperty("score", "" + errorScoreTot);
	    properties.setProperty("score1", "" + errorScore);
	    properties.setProperty("score2", "" + errorScore2);
	    PropertyTools.mergeProperties(properties, defaultProperties, "s1.");
	    PropertyTools.mergeProperties(properties, finalProperties, "s2.");
            if (constraintScorer != null) {
	      Properties constraintScorerProps = constraintScorer.generateReport(bseqs, structure, interactionMatrices);
 	      PropertyTools.mergeProperties(properties, constraintScorerProps, "s3.");
	    }
	     double finalTotalScore = Double.parseDouble(defaultProperties.getProperty("score")) 
		+ finalScoreWeight * Double.parseDouble(finalProperties.getProperty("score"));
	     if (getProperty("s3.score") != null) {
		finalTotalScore += Double.parseDouble(getProperty("s3.score"));
	     }
             properties.setProperty("total_score", "" + finalTotalScore);
	    log.info("Ended evaluation " + errorScore + " " + errorScore2 + " " + errorScoreTot);
	} catch (NumberFormatException nfe) {
	    properties.setProperty("error", "An error occurred during score calculation. " + nfe.getMessage());
            properties.setProperty("total_score", "Internal error: could not add score components.");	    
	}
	log.info("Finished MonteCarloSequenceOptimizer.eval.");
	return properties; 
    }

    public SecondaryStructureScorer getDefaultScorer() { return this.defaultScorer; }

    public double getErrorScoreLimit() { return this.errorScoreLimit; }

    public SecondaryStructureScorer getFinalScorer() { return this.finalScorer; }

    /** Returns ignoreNonConstantMode. See setIgnoreNonConstantMode for more details. */
    public boolean getIgnoreNotConstantMode() { return this.ignoreNotConstantMode; }

    public double getFinalScoreWeight() { return this.finalScoreWeight; }

    public double getGcContent() { return gcContent; }

    /** returns kt value that determines acceptance probability of unfavourable mutations */
    public double getKt() { return kt; }

    public void setDefaultScorer(SecondaryStructureScorer scorer) { this.defaultScorer = scorer; }

    public void setFinalScorer(SecondaryStructureScorer scorer) { this.finalScorer = scorer; }

    public void setFinalScoreWeight(double w) { this.finalScoreWeight = w; }

    public void setGcContent(double x) { this.gcContent = x; initProbabilities(); }

    public void setErrorScoreLimit(double x) { this.errorScoreLimit = x; }

    public void setErrorScoreLimit2(double x) { this.errorScoreLimit2 = x; }
    
    /** if true, set all non-constant residue interactions as UNKNOWN_SUBTYPE, effectively ignoring them. */
    public void setIgnoreNotConstantMode(boolean mode) { this.ignoreNotConstantMode = mode; }

    /** number of steps of first-stage optimazation */
    public void setIterMax(int n) { this.iterMax = n; }

    /** number of steps of second-stage optimization */
    public void setIter2Max(int n) { this.iter2Max = n; }

    /** sets how many times the algorithm starts over */
    public void setRerun(int n) { this.rerun = n; }

    /** sets kt value that determines acceptance probability of unfavourable mutations */
    public void setKt(double kt) {
	assert kt > 0.0;
	this.kt = kt;
    }

    /** sets kt value that determines acceptance probability of unfavourable mutations */
    public void setKt2(double kt2) {
	assert kt2 > 0.0;
	this.kt2 = kt2;
    }

    /** mutates a character (using nucleotide alphabet) if upper case */
    private char mutateCharacter(char c) {
	if (Character.isLowerCase(c)) {
	    return c;
	}
	char result = c;
	do {
	    result = nucleotideHelperChars[rand.nextInt(nucleotideHelperChars.length)];
	}
	while (result == c);
	return result;
    }

    /** randomizes sequence */
    private void randomizeSequence(StringBuffer buf) {
	int origLen = buf.length();
	for (int i = 0; i < buf.length(); ++i) {
	    if (Character.isUpperCase(buf.charAt(i))) {
		buf.setCharAt(i, mutateCharacter(buf.charAt(i)));
// 		if ((i>0) && (buf.charAt(i-1) == buf.charAt(i))) {
// 		    // avoid same nucleotides in a row, try again one more time:
// 		    buf.setCharAt(i, mutateCharacter(buf.charAt(i)));
// 		}
	    }
	}
	assert buf.length() == origLen; // no change in length
    }

    /** returns random watson crick pair (GC or AU) */
    private String generateRandomWatsonCrick() {
	String result = "";
	if (rand.nextDouble() > gcContent) {
	    result = "AU";
	}
	else {
	    result = "CG";
	}
	if (rand.nextDouble() > 0.5) {
	    return "" + result.charAt(1) + result.charAt(0);
	}
	assert result.length() == 2;
	return result;
    }

    /** returns true, if character is in alphabet */
    private boolean isInAlphabet(char c, Alphabet alphabet) {
	return alphabet.contains(c);
    }

    /** returns true, if characters are in alphabet */
    private boolean isInAlphabet(char c1, char c2, Alphabet alphabet1, Alphabet alphabet2) {
	return isInAlphabet(c1, alphabet1) && isInAlphabet(c2, alphabet2);
    }

    /** Returns: 0: AU, 1: CG, 2: GC, 3: UA */
    private String generateWatsonCrick(int id) {
	assert ((id >= 0) && (id < 4));
	switch (id) {
	case 0: return "AU";
	case 1: return "CG";
	case 2: return "GC";
	case 3: return "UA";
	}
	return null;
    }

    /** returns random watson crick pair (GC or AU) */
    private String generateRandomWatsonCrick(Alphabet alphabet1, Alphabet alphabet2) {
	List<Integer> ids = new ArrayList<Integer>();
	for (int i = 0; i < 4; ++i) {
	    String pair = generateWatsonCrick(i);
	    if (isInAlphabet(pair.charAt(0), pair.charAt(1), alphabet1, alphabet2)) {
		ids.add(i);
	    }
	}
	if (ids.size() == 0) {
	    System.out.println("Could not generate watson crick pairs for alphabets: " + alphabet1 + " + " + alphabet2);
	    return null;
	}
	int rid = rand.nextInt(ids.size());
	int id = ids.get(rid);
	String result = generateWatsonCrick(id);
	assert (isInAlphabet(result.charAt(0), result.charAt(1), alphabet1, alphabet2));
	return result;
    }

    private void randomizeSequences2(StringBuffer[] buf,
				     int[][][][] interactionMatrices,
				     List<Alphabet> alphabets) {
	assert ((buf != null) && (buf.length > 0));
	assert ((interactionMatrices != null) && (interactionMatrices.length == buf.length));
	assert((alphabets == null) || (alphabets.size() == buf.length));
	for (int i = 0; i < buf.length; ++i) {
	    randomizeSequence(buf[i]);
	}
	for (int i = 0; i < interactionMatrices.length; ++i) {
	    for (int j = i; j < interactionMatrices[i].length; ++j) {
		if (interactionMatrices[i][j].length != buf[i].length()) {
		    log.warning("" + i + " " + j + " " + interactionMatrices[i][j].length
				+ " " + buf[i].length());
		}
		assert interactionMatrices[i][j].length == buf[i].length();
		for (int k = 0; k < interactionMatrices[i][j].length; ++k) {
		    assert interactionMatrices[i][j][k].length == buf[j].length();
		    for (int m = 0; m < interactionMatrices[i][j][k].length; ++m) {
			if ((interactionMatrices[i][j][k][m] == RnaInteractionType.WATSON_CRICK)
			    && (!RnaSecondaryTools.isWatsonCrick(buf[i].charAt(k), buf[j].charAt(m)) ) ) {
			    if (Character.isUpperCase(buf[i].charAt(k)) && Character.isUpperCase(buf[j].charAt(m) ) ) {
				String randWC = generateRandomWatsonCrick();
				assert randWC != null;
				if (alphabets != null) {
				    randWC = generateRandomWatsonCrick(alphabets.get(i), alphabets.get(j));
				    assert((randWC != null) && (randWC.length() == 2) && isInAlphabet(randWC.charAt(0), alphabets.get(i))
					   && isInAlphabet(randWC.charAt(1), alphabets.get(j)));
				}
				assert randWC != null;
				assert randWC.length() == 2;
				assert buf != null;
				assert buf[i] != null;
				assert buf[j] != null;
				if (randWC == null) {
				    System.out.println("Randwc is null! Internal error!");
				    assert false;
				}
				char c1 = randWC.charAt(0);
				char c2 = randWC.charAt(1);
				StringBuffer buf1 = buf[i];
				StringBuffer buf2 = buf[j];
				buf1.setCharAt(k, c1);
				buf2.setCharAt(m, c2);
// 				if ((k > 0) && (m > 0)) {
// 				    if ((buf[i].charAt(k-1) == buf[i].charAt(k))
// 					|| (buf[j].charAt(m-1) == buf[j].charAt(m))) {
// 					// avoid duplication, try one more time:
// 					randWC = generateRandomWatsonCrick();
// 					buf[i].setCharAt(k, randWC.charAt(0));
// 					buf[j].setCharAt(m, randWC.charAt(1));
// 				    }
//				}
			    } else if (Character.isUpperCase(buf[i].charAt(k)) ) { // only first nt is mutable
				buf[i].setCharAt(k, generateWatsonCrickPartner(buf[j].charAt(m)));
			    } else if (Character.isUpperCase(buf[j].charAt(m) ) ) { // only second nt is mutable
				buf[j].setCharAt(m, generateWatsonCrickPartner(buf[i].charAt(k)));
			    }
			}
			
		    }
		}
	    }
	}
    }

    private void randomizeSequences2Orig(StringBuffer[] buf,
				     int[][][][] interactionMatrices,
				     List<Alphabet> alphabets) {
	assert ((buf != null) && (buf.length > 0));
	assert ((interactionMatrices != null) && (interactionMatrices.length == buf.length));
	assert((alphabets == null) || (alphabets.size() == buf.length));
	for (int i = 0; i < buf.length; ++i) {
	    randomizeSequence(buf[i]);
	}
	for (int i = 0; i < interactionMatrices.length; ++i) {
	    for (int j = i; j < interactionMatrices[i].length; ++j) {
		if (interactionMatrices[i][j].length != buf[i].length()) {
		    log.warning("" + i + " " + j + " " + interactionMatrices[i][j].length
				+ " " + buf[i].length());
		}
		assert interactionMatrices[i][j].length == buf[i].length();
		for (int k = 0; k < interactionMatrices[i][j].length; ++k) {
		    assert interactionMatrices[i][j][k].length == buf[j].length();
		    for (int m = 0; m < interactionMatrices[i][j][k].length; ++m) {
			if (Character.isUpperCase(buf[i].charAt(k)) && Character.isUpperCase(buf[j].charAt(m) ) ) {
			    if (interactionMatrices[i][j][k][m] == RnaInteractionType.WATSON_CRICK) {
				String randWC = generateRandomWatsonCrick();
				assert randWC != null;
				if (alphabets != null) {
				    randWC = generateRandomWatsonCrick(alphabets.get(i), alphabets.get(j));
				    assert((randWC != null) && (randWC.length() == 2) && isInAlphabet(randWC.charAt(0), alphabets.get(i))
					   && isInAlphabet(randWC.charAt(1), alphabets.get(j)));
				}
				assert randWC != null;
				assert randWC.length() == 2;
				assert buf != null;
				assert buf[i] != null;
				assert buf[j] != null;
				if (randWC == null) {
				    System.out.println("Randwc is null! Internal error!");
				    assert false;
				}
				char c1 = randWC.charAt(0);
				char c2 = randWC.charAt(1);
				StringBuffer buf1 = buf[i];
				StringBuffer buf2 = buf[j];
				buf1.setCharAt(k, c1);
				buf2.setCharAt(m, c2);
// 				if ((k > 0) && (m > 0)) {
// 				    if ((buf[i].charAt(k-1) == buf[i].charAt(k))
// 					|| (buf[j].charAt(m-1) == buf[j].charAt(m))) {
// 					// avoid duplication, try one more time:
// 					randWC = generateRandomWatsonCrick();
// 					buf[i].setCharAt(k, randWC.charAt(0));
// 					buf[j].setCharAt(m, randWC.charAt(1));
// 				    }
//				}
			    }
			}
			
		    }
		}
	    }
	}
    }

    private String generateSequencesOutputString(StringBuffer[] bufs) {
	StringBuffer result = new StringBuffer();
	for (int i = 0; i < bufs.length; ++i) {
	    result.append(bufs[i]);
	    result.append(NEWLINE);
	}
	return result.toString();
    }
    
    /** Initializes string buffers. If residue status is unknown, perform no optimization. */
    /*
    private StringBuffer[] initStringBuffers(SecondaryStructure structure, 
					     boolean randomizeFlag) {
	StringBuffer[] bseqs = new StringBuffer[structure.getSequenceCount()];
	for (int i = 0; i < bseqs.length; ++i) {
	    Sequence sequence = structure.getSequence(i);
	    bseqs[i] = new StringBuffer(sequence.sequenceString());
	    for (int j = 0; j < bseqs[i].length(); ++j) {
		Residue res = sequence.getResidue(j);
		if (SequenceStatus.fragment.equals(res.getProperty(SequenceStatus.name))) {
		    bseqs[i].setCharAt(j, Character.toLowerCase(bseqs[i].charAt(j)));
		}
		else if (SequenceStatus.adhoc.equals(res.getProperty(SequenceStatus.name))) {
		    bseqs[i].setCharAt(j, Character.toUpperCase(bseqs[i].charAt(j)));
		}
		else {
		    log.warning("Residue status unknown: " + bseqs[i] + " " + res.getSymbol() + " " 
				+ (i+1) + " " + (j+1));
		    // bseqs[i].setCharAt(j, Character.toUpperCase(bseqs[i].charAt(j)));
		    bseqs[i].setCharAt(j, Character.toLowerCase(bseqs[i].charAt(j))); // do not modify if unknown
		}
	    }
	    if (randomizeFlag) {
		randomizeSequence(bseqs[i]);
	    }
	}
	return bseqs;
    }
    */

    private Mutation generateMutation(StringBuffer[] bseqs, List<Alphabet> alphabets) {
	int seqId = 0;
	int pos = 0;
	char newChar = 'N';
	char oldChar = 'N';
	do {
	    seqId = rand.nextInt(bseqs.length);
	    pos = rand.nextInt(bseqs[seqId].length());
	    oldChar = bseqs[seqId].charAt(pos);
	}
	while (Character.isLowerCase(oldChar)); // do not mutate lower case characters. Careful: infinite loop if all lower char
	do {
	    Alphabet currAlph = alphabet;
	    if (alphabets != null) {
		currAlph = alphabets.get(seqId);
	    }
	    newChar = currAlph.getSymbol(rand.nextInt(currAlph.size())).getCharacter();
	}
	while (newChar == oldChar); // must be different
	assert Character.isUpperCase(bseqs[seqId].charAt(pos));
	return new Mutation(seqId, pos, newChar);
    }

    private char generateWatsonCrickPartner(char c) {
	c = Character.toUpperCase(c);
	switch (c) {
	case 'A': return 'U';
	case 'C': return 'G';
	case 'G': return 'C';
	case 'U': return 'A';
	}
	assert false; // should never be here
	return 'N';
    }

    /** If a given mutation is part of a base pair, find compensatory matching mutation.
     * TO DO: improve slow implementation!
     */
    Mutation findMatchingMutation(Mutation mutation,
				  int[][][][] interactionMatrices,
				  StringBuffer[] bseqs,
				  List<Alphabet> alphabets) {
	assert(alphabets != null);
	int seqId = mutation.getSeqId();
	int pos = mutation.getPos();
	char c = mutation.getCharacter();
	char cPart = generateWatsonCrickPartner(c); 
	for (int i = 0; i <= seqId; ++i) {
	    for (int j = 0; j < interactionMatrices[i][seqId].length; ++j) {
		if (interactionMatrices[i][seqId][j][pos] == RnaInteractionType.WATSON_CRICK) {
		    assert(alphabets != null);
		    // if (alphabets != null) {
		    if ((!alphabets.get(i).contains(cPart)) || Character.isLowerCase(bseqs[i].charAt(j))) {
			// log.info("Found bad matching mutation (1): " + new Mutation(i,j, cPart) + " for sequence: " + bseqs[i]);
			return new Mutation(-i-1, j, cPart); // mutation necessary but not acceptable
		    }
		    // }
                    assert(Character.isUpperCase(bseqs[i].charAt(j)));
		    // log.info("Found matching mutation (1): " + new Mutation(i,j, cPart) + " for sequence: " + bseqs[i]);
		    return new Mutation(i, j, cPart);
		}
	    }
	}
	for (int i = seqId+1; i < interactionMatrices[seqId].length; ++i) {
	    for (int j = 0; j < interactionMatrices[seqId][i][pos].length; ++j) {
		if (interactionMatrices[seqId][i][pos][j] == RnaInteractionType.WATSON_CRICK) {
		    assert(alphabets != null);
		    // if (alphabets != null) {
		    if ((!alphabets.get(i).contains(cPart)) || Character.isLowerCase(bseqs[i].charAt(j))) {
			// return null;
			// log.info("Found bad matching mutation (2): " + new Mutation(i,j, cPart) + " for sequence: " + bseqs[i]);
			return new Mutation(-i-1, j, cPart); // mutation necessary but not acceptable
		    }
			// }
                    assert(Character.isUpperCase(bseqs[i].charAt(j)));
		    // log.info("Found matching mutation (2): " + new Mutation(i,j, cPart) + " for sequence: " + bseqs[i]);
		    return new Mutation(i, j, cPart);
		}
	    }
	}
	return null; // compensatory mutation not necessary because original mutation is not part of a base pair
    }

//     private Mutation generateMatchingMutation(StringBuffer[] bseqs, Mutation prevMutation,
// 					      int[][][][] interactionMatrices, alphabets) {
// 	return findMatchingMutation(prevMutation, interactionMatrices, alphabets);
//     }

    /** Initializes string buffers. If residue status is unknown, perform optimization. */
    private StringBuffer[] initStringBuffers2(SecondaryStructure structure, 
					      boolean randomizeFlag,
					      int[][][][] interactionMatrices,
					      List<Alphabet> alphabets) {
	StringBuffer[] bseqs = new StringBuffer[structure.getSequenceCount()];
	for (int i = 0; i < bseqs.length; ++i) {
	    Sequence sequence = structure.getSequence(i);
	    bseqs[i] = new StringBuffer(sequence.sequenceString());
	    for (int j = 0; j < bseqs[i].length(); ++j) {
		Residue res = sequence.getResidue(j);
		if (SequenceStatus.fragment.equals(res.getProperty(SequenceStatus.name))) {
		    bseqs[i].setCharAt(j, Character.toLowerCase(bseqs[i].charAt(j)));
		}
		else if (SequenceStatus.adhoc.equals(res.getProperty(SequenceStatus.name))) {
		    bseqs[i].setCharAt(j, Character.toUpperCase(bseqs[i].charAt(j)));
		}
		else {
		    log.fine("Residue status unknown: " + bseqs[i] + " " + res.getSymbol() + " " 
				+ (i+1) + " " + (j+1) + " : setting for optimizable (adhoc)");
		    bseqs[i].setCharAt(j, Character.toUpperCase(bseqs[i].charAt(j))); // if residue status unknown, perform optimization
		    // bseqs[i].setCharAt(j, Character.toLowerCase(bseqs[i].charAt(j))); // if residue status unknown, perform NO optimization
		}
	    }
	}
	if (randomizeFlag) {
	    log.info("Randomizing sequences (2)!");
	    randomizeSequences2(bseqs, interactionMatrices, structure.getAlphabets() );
	}
	return bseqs;
    }

    private void applyMutation(Mutation mutation,
			       StringBuffer[] bseqs) {
	if (Character.isUpperCase(bseqs[mutation.getSeqId()].charAt(mutation.getPos()))) {
	    bseqs[mutation.getSeqId()].setCharAt(mutation.getPos(), mutation.getCharacter());
	}
	else {
	    // ignore mutation because residue was set to constant indicated by a lower case residue character
	}
    }

    /** Returns base pair matrix for two given sequence ids id1 and id2 */
    int[][] generateInteractionMatrix(SecondaryStructure structure, int id1, int id2) {
	Sequence seq1 = structure.getSequence(id1);
	Sequence seq2 = structure.getSequence(id2);
	int[][] result = new int[seq1.size()][seq2.size()];
	for (int i = 0; i < result.length; ++i) {
	    for (int j = 0; j < result[i].length; ++j) {
		result[i][j] = RnaInteractionType.NO_INTERACTION;
	    }
	}
	for (int i = 0; i < structure.getInteractionCount(); ++i) {
	    Interaction interaction = structure.getInteraction(i);
	    int pos1 = interaction.getResidue1().getPos();
	    int pos2 = interaction.getResidue2().getPos();
	    if ((interaction.getSequence1() == seq1) && (interaction.getSequence2() == seq2)) {
		result[pos1][pos2] = interaction.getInteractionType().getSubTypeId();
		if (id1 == id2) {
		    result[pos2][pos1] = result[pos1][pos2]; // if same sequence: symmetric
		}
	    }
	    else if ((interaction.getSequence2() == seq1) && (interaction.getSequence1() == seq2)) {
		result[pos2][pos1] = interaction.getInteractionType().getSubTypeId();		
		assert id1 != id2;
	    }
	}
	// sets rows and columns to UNKNOWN_INTERACTION, if property seqstatus is "ignore"
	for (int i = 0; i < result.length; ++i) {
	    if (("ignore".equals(seq1.getResidue(i).getProperty("seqstatus")))
		|| (ignoreNotConstantMode && SequenceStatus.fragment.equals(seq1.getResidue(i).getProperty(SequenceStatus.name)))) {
		for (int j = 0; j < result[i].length; ++j) {
		    result[i][j] = RnaInteractionType.UNKNOWN_SUBTYPE;
		}
	    }
	}
	for (int i = 0; i < result[0].length; ++i) {
	    if (("ignore".equals(seq2.getResidue(i).getProperty("seqstatus"))) 
		|| (ignoreNotConstantMode && SequenceStatus.fragment.equals(seq2.getResidue(i).getProperty(SequenceStatus.name)))) {
		for (int j = 0; j < result.length; ++j) {
		    result[j][i] = RnaInteractionType.UNKNOWN_SUBTYPE;
		}
	    }
	}
	return result;
    }

    /** Generates set of base pair matrices that depend on the sequence ids and the residue ids */
    int[][][][] generateInteractionMatrices(SecondaryStructure structure) {
	int[][][][] matrices = new int[structure.getSequenceCount()][structure.getSequenceCount()][0][0];
	for (int i = 0; i < structure.getSequenceCount(); ++i) {
	    for (int j = i; j < structure.getSequenceCount(); ++j) {
		matrices[i][j] = generateInteractionMatrix(structure, i, j);
	    }
	}
	return matrices;
    }

    /** Return true if all wanted base pairs correspond to GC and AU */
    private boolean sanityCheck(StringBuffer sb1,
				StringBuffer sb2,
				SecondaryStructure structure,
				int[][] interactionMatrices,
				int m, 
				int n ) {
	assert interactionMatrices.length == sb1.length();
	assert interactionMatrices[0].length == sb2.length();
	for (int i = 0; i < interactionMatrices.length; ++i) {
	    for (int j = 0; j < interactionMatrices[0].length; ++j) {
		if (interactionMatrices[i][j] == RnaInteractionType.WATSON_CRICK) {
		    if (!RnaSecondaryTools.isWatsonCrick(sb1.charAt(i), sb2.charAt(j))) {
			return false;
		    }
		}
	    }
	}
	return true;
    }

    /** Return true if all wanted base pairs correspond to GC and AU */
    private boolean sanityCheck(StringBuffer[] bseqs,
				SecondaryStructure structure,
				int[][][][] interactionMatrices) {
	for (int i = 0; i < bseqs.length; ++i) {
	    for (int j = i; j < bseqs.length; ++j) {
		if (!sanityCheck(bseqs[i], bseqs[j], structure, interactionMatrices[i][j], i, j)) {
		    return false;
		}
	    }
	}
	return true;
    }

    /** Return true if all wanted base pairs correspond to GC and AU */
    public boolean sanityCheckSlow(String[] seqs,
				SecondaryStructure structure) {
        System.out.println("sanityCheckSlow");
	int[][][][] interactionMatrices = generateInteractionMatrices(structure);
        StringBuffer[] bseqs = new StringBuffer[seqs.length];
	for (int i = 0; i < bseqs.length; ++i) {
            System.out.println("seqs[i]");
	    bseqs[i] = new StringBuffer(seqs[i]);
	}
        System.out.println("Structure: " + structure.toString());
	return sanityCheck(bseqs, structure, interactionMatrices);
    }

    /** Using only simple scorer */
    private double optimizationTrial(StringBuffer[] bseqs,
				     SecondaryStructure structure,
				     double oldScore,
				     int[][][][] interactionMatrices,
				     boolean finalMode,
				     List<Alphabet> alphabets) {
	log.finest("Starting optimizationTrial: " + oldScore);
	assert(alphabets != null);
	if (doSanityChecks) {
	    assert(sanityCheck(bseqs, structure, interactionMatrices));
	}
	Mutation mutation = null; 
	Mutation mutation2 = null; 
	do {
	    mutation = generateMutation(bseqs, alphabets);
	    mutation2 = findMatchingMutation(mutation, interactionMatrices, bseqs, alphabets);
// 	    if (debugLogLevel > Level.INFO) {
// 		log.info("trying mutation: " + mutation);
// 		if (mutation2 != null) {
// 		    log.info("compensatory mutation: " + mutation2);
// 		} else {
// 		    log.info("no compensatory mutation found or necessary.");
// 		}
// 	    }
	} while (((mutation2 != null) && (mutation2.getSeqId() < 0)) || (!structure.isActive(mutation.getSeqId()))); // alphabet collision
	int count = 0;
	char oldCharacter = bseqs[mutation.getSeqId()].charAt(mutation.getPos());
	char oldCharacter2 = 'X';
	applyMutation(mutation, bseqs); // mutate
	if (mutation2 != null) {
	    oldCharacter2 = bseqs[mutation2.getSeqId()].charAt(mutation2.getPos());
	    applyMutation(mutation2, bseqs); // mutate
	} else {
// 	    if (debugLogLevel > Level.INFO) {
// 		log.info("No matching mutation found");
// 	    }
	}
	boolean undo = false;
	double newScore = oldScore;
	if (doSanityChecks) {
	    if(!sanityCheck(bseqs, structure, interactionMatrices)) {
		log.severe("Sanity check after sequence mutation failed!: " + NEWLINE + generateSequencesOutputString(bseqs));
		
		assert(false);
	    }
	}
	newScore = defaultScorer.scoreStructure(bseqs, structure, interactionMatrices);
	if (constraintScorer != null) {
	    double constraintScore = constraintScorer.scoreStructure(bseqs, structure, interactionMatrices);
	    newScore += constraintScore;
	}
	if (finalMode) { // add second scorer
	    double newScore2 = finalScorer.scoreStructure(bseqs, structure, interactionMatrices);
	    newScore = newScore + finalScoreWeight * newScore2;
	}
	log.fine("new score: " + newScore);
	if (Math.exp(-(newScore - oldScore)/kt) < rand.nextDouble()){ // worse, do not accept step
	    undo = true;
	}
	else {
	    if (newScore < oldScore) {
		log.finest("New score accepted: " + newScore + " " + oldScore);
	    }
	}
	if (undo) {
	    mutation.setCharacter(oldCharacter); // set to former character
	    applyMutation(mutation, bseqs);
	    if (mutation2 != null) {
		mutation2.setCharacter(oldCharacter2); // set to former character
		applyMutation(mutation2, bseqs);
	    }
	    newScore = oldScore;
	}
	if (doSanityChecks) {
	    assert(sanityCheck(bseqs, structure, interactionMatrices));
	}
	log.finest("Finished optimizationTrial!");
	return newScore;
    }

    /** Using only simple scorer */
    private double optimizationSystematicTrial(StringBuffer[] bseqs,
					       SecondaryStructure structure,
					       double oldScore,
					       int[][][][] interactionMatrices,
					       boolean finalMode,
					       List<Alphabet> alphabets,
					       int[] iterators) {
	log.finest("Starting optimizationTrial: " + oldScore);
	assert(alphabets != null);
	if (doSanityChecks) {
	    assert(sanityCheck(bseqs, structure, interactionMatrices));
	}
	assert(iterators.length == 1); // workaround for call-by-reference
	int trialIter = iterators[0];
	Mutation mutation = null; 
	Mutation mutation2 = null; 
	do {
	    mutation = generateMutation(bseqs, alphabets);
	    mutation2 = findMatchingMutation(mutation, interactionMatrices, bseqs, alphabets);
	} while (((mutation2 != null) && (mutation2.getSeqId() < 0)) || (!structure.isActive(mutation.getSeqId()))); // alphabet collision
	int count = 0;
	char oldCharacter = bseqs[mutation.getSeqId()].charAt(mutation.getPos());
	char oldCharacter2 = 'X';
	applyMutation(mutation, bseqs); // mutate
	if (mutation2 != null) {
	    oldCharacter2 = bseqs[mutation2.getSeqId()].charAt(mutation2.getPos());
	    applyMutation(mutation2, bseqs); // mutate
	} else {
	    log.fine("No matching mutation found");
	}
	boolean undo = false;
	double newScore = oldScore;
	if (doSanityChecks) {
	    if(!sanityCheck(bseqs, structure, interactionMatrices)) {
		log.severe("Sanity check after sequence mutation failed!: " + NEWLINE + generateSequencesOutputString(bseqs));
		
		assert(false);
	    }
	}
	newScore = defaultScorer.scoreStructure(bseqs, structure, interactionMatrices);
	if (constraintScorer != null) {
	    double constraintScore = constraintScorer.scoreStructure(bseqs, structure, interactionMatrices);
	    newScore += constraintScore;
	}
	if (finalMode) { // add second scorer
	    double newScore2 = finalScorer.scoreStructure(bseqs, structure, interactionMatrices);
	    newScore = newScore + finalScoreWeight * newScore2;
	}
	log.fine("new score: " + newScore);
	if (Math.exp(-(newScore - oldScore)/kt) < rand.nextDouble()){ // worse, do not accept step
	    undo = true;
	}
	else {
	    if (newScore < oldScore) {
		log.finest("New score accepted: " + newScore + " " + oldScore);
	    }
	}
	if (undo) {
	    mutation.setCharacter(oldCharacter); // set to former character
	    applyMutation(mutation, bseqs);
	    if (mutation2 != null) {
		mutation2.setCharacter(oldCharacter2); // set to former character
		applyMutation(mutation2, bseqs);
	    }
	    newScore = oldScore;
	}
	if (doSanityChecks) {
	    assert(sanityCheck(bseqs, structure, interactionMatrices));
	}
	log.finest("Finished optimizationTrial!");
	return newScore;
    }


    /** Using simple scorer and energy-based scorer. */
    /* private double optimizationTrial2(StringBuffer[] bseqs,
				     SecondaryStructure structure,
				     double oldScore,
				     int[][][][] interactionMatrices) {
	assert false; // outdated
	log.finest("Starting optimizationTrial: " + oldScore);
	Mutation mutation = generateMutation(bseqs);
	Mutation mutation2 = generateMatchingMutation(bseqs, mutation, interactionMatrices);
	char oldCharacter = bseqs[mutation.getSeqId()].charAt(mutation.getPos());
	char oldCharacter2 = bseqs[mutation2.getSeqId()].charAt(mutation2.getPos());
	applyMutation(mutation, bseqs); // mutate
	applyMutation(mutation2, bseqs); // mutate
	double newScore1 = defaultScorer.scoreStructure(bseqs, structure, interactionMatrices);
	double newScore2 = finalScorer.scoreStructure(bseqs, structure, interactionMatrices);
	double newScore = newScore1 + finalScoreWeight * newScore2;
	log.fine("new score: " + newScore + " " + newScore1 + " " + newScore2);
	if (Math.exp(-(newScore - oldScore)/kt2) < rand.nextDouble()){ // worse, do not accept step
	    mutation.setCharacter(oldCharacter); // set to former character
	    mutation2.setCharacter(oldCharacter2); // set to former character
	    applyMutation(mutation, bseqs);
	    applyMutation(mutation2, bseqs);
	    newScore = oldScore;
	}
	else {
	    if (newScore < oldScore) {
		log.finest("New score accepted: " + newScore + " " + oldScore + "(" + newScore1 + " , " + newScore2 + ")");
	    }
	}
	log.finest("Finished optimizationTrial!");
	return newScore;
    }
    */

    /** returns true if sequence has upper case characters */
    boolean hasUpperCaseCharacters(String s) {
	for (int i = 0; i < s.length(); ++i) {
	    if (Character.isUpperCase(s.charAt(i))) {
		return true;
	    }
	}
	return false;
    }
    
    /** returns true if sequence has upper case characters */
    boolean hasUpperCaseCharacters(StringBuffer[] bseqs) {
	for (int i = 0; i < bseqs.length; ++i) {
	    if (hasUpperCaseCharacters(bseqs[i].toString())) {
		return true;
	    }
	}
	return false;
    }

    private StringBuffer[] cloneBuffers(StringBuffer[] bufs) {
	assert bufs != null;
	StringBuffer[] result = new StringBuffer[bufs.length];
	for (int i = 0; i < bufs.length; ++i) {
	    result[i] = new StringBuffer(bufs[i].toString());
	}
	return result;
    }

    public String[] optimize2Stage(SecondaryStructure structure) {
	assert finalScorer != null;
	assert(structure.getAlphabets() != null);
	assert defaultScorer != null;
	log.info("Starting MonteCarloSequenceOptimizer.optimize !");
	int rerunCount = 0;
	int[][][][] interactionMatrices = generateInteractionMatrices(structure);
	// StringBuffer[] bseqsBest = null;
	double errorScoreTotBest = 99999.0;
	double errorScoreBest = 1e10;
	this.constraintScorer = new SequenceConstraintScorer(structure);
	System.out.println("Defined sequence constraints: " + constraintScorer.toString());
	StringBuffer[] bseqsFinal = null;
	RERUN_LOOP: do {
	    StringBuffer[] bseqs = initStringBuffers2(structure, randomizeFlag, interactionMatrices, structure.getAlphabets()); // default: optimization
	    double errorScore = defaultScorer.scoreStructure(bseqs, structure, interactionMatrices) + constraintScorer.scoreStructure(bseqs, structure, interactionMatrices);
	    if (errorScore < errorScoreBest) {
		errorScoreBest = errorScore;
	    }
	    log.info("Starting optimization run " + (rerunCount + 1) + " with score : " 
		     + errorScore + " and sequences: " + NEWLINE + generateSequencesOutputString(bseqs));
	    if (!hasUpperCaseCharacters(bseqs)) {
		log.warning("No residue can be optimized because they are all in lower case characters!");
		break;
	    }
	    int stage1Steps = 0;
	    int stage2Steps = 0;
	    for (int i = 0; (i < iterMax) && (errorScore > 0.0); ++i, ++stage1Steps) {
		// apply default scorer:
		if (!sanityCheck(bseqs, structure, interactionMatrices)) {
		    log.log(debugLogLevel, "Not all base pairs are yet compatible!");
		} else {
		    // log.fine("Sanity checks passed!");
		}
		assert(structure.getAlphabets() != null);
		errorScore = optimizationTrial(bseqs, structure, errorScore, interactionMatrices, false, structure.getAlphabets());
		if (doSanityChecks) {
		    sanityCheck(bseqs, structure, interactionMatrices);
		}
		// log.info("New error Score: " + errorScore + " " + i);
		if (((i % stride1) == 0) || (errorScore < errorScoreBest)) {
		    System.out.println("Round " + (rerunCount+1) + " : Sequences at stage 1 iteration " + (i+1) + " and score: " + errorScore
				       + " so far best: " + errorScoreBest);
		    System.out.println(generateSequencesOutputString(bseqs));
		    PropertyTools.printProperties(System.out,defaultScorer.generateReport(bseqs, structure, interactionMatrices));
		}
		if (errorScore < errorScoreBest) {
		    errorScoreBest = errorScore;
		    log.info("New best sequence optimization (stage 1) score found at run " + (rerunCount+1)
			     + " and iteration " + (i+1) + " " + errorScoreBest);
		    // bseqsBest = cloneBuffers(bseqs);
		}
		if ((errorScore <= errorScoreLimit) || (i >= (iterMax-1))) { // after end of outside loop perform inside loop
		    // discrepancy score with RNAfold prediction:
		    double errorScore2 = finalScorer.scoreStructure(bseqs, structure, interactionMatrices);
		    // inner loop: optimize sequences, such that rules are fullfilled, best RNAfold structure:
		    double errorScoreTot = errorScore + finalScoreWeight * errorScore2;
		    log.info("Starting stage 2 of sequence optimization with error score:" + errorScore + " " + errorScore2 + " " + errorScoreTot);
		    // errorScoreTotBest = errorScoreTot;
		    for (int i2 = 0; i2 < iter2Max; ++i2, ++stage2Steps) {
			// check if basic rules are fullfilled:
			// errorScore = defaultScorer.scoreStructure(bseqs, structure, interactionMatrices);		    
			if (doSanityChecks) {
			    assert sanityCheck(bseqs, structure, interactionMatrices);
			}
			errorScoreTot = optimizationTrial(bseqs, structure, errorScoreTot, interactionMatrices, true, structure.getAlphabets());
			if (doSanityChecks) {
			    assert sanityCheck(bseqs, structure, interactionMatrices);
			}
			if ((errorScoreTot < errorScoreTotBest) || ((i2 % stride2) == 0)) {
			    System.out.println("Round " + (rerunCount+1) + " : Sequences at stage 2 iteration " + (i2+1) + " and score: " + errorScoreTot
					       + " so far best: " + errorScoreTotBest);
			    System.out.println(generateSequencesOutputString(bseqs));
			}
			if (errorScoreTot < errorScoreTotBest) {
			    log.info("Found new best sequences (stage 2) with score: " 
				     + errorScoreTot + " at outer iteration " + (i+1) 
				     + " and inner iteration: " + (i2+1));
			    errorScoreTotBest = errorScoreTot;
			    bseqsFinal = cloneBuffers(bseqs);
			    if (errorScoreTot < errorScoreLimit2) {
				System.out.println("Overall optimization goal of score " + errorScoreLimit2 + " achieved, quitting optimization loop!");
				break RERUN_LOOP; // quit optmization loop!
			    }
			}
		    }
		    break; // quit this loop, start over with completely new set of randomized sequences
		}
	    }
	    log.info("Ended optimization run after round " + (rerunCount + 1) + " steps " + stage1Steps + " " + stage2Steps 
		     + " with strings: " + NEWLINE + generateSequencesOutputString(bseqs));
	}
	while ((++rerunCount < rerun)); // restart if not optimal score was found
	// AFTER_OPT: // label to allow quitting of optimization
	if (bseqsFinal == null) {
	    log.warning("Sorry, no sequences that fullfill all optimization criteria found!");
	    return null;
	}
	String[] result = new String[bseqsFinal.length]; // translate back to strings
	log.info("Best error score of all runs: " + errorScoreBest);
	log.info("Best error score 2 of all runs: " + errorScoreTotBest);
	setProperty("best_score", "" + errorScoreBest);
	setProperty("best_score2", "" + errorScoreTotBest);
        
	log.info("Final strings: " + NEWLINE + generateSequencesOutputString(bseqsFinal));
	for (int i = 0; i < bseqsFinal.length; ++i) {
	    result[i] = bseqsFinal[i].toString();
	}
	Properties defScorerProps = defaultScorer.generateReport(bseqsFinal, structure, interactionMatrices);
	Properties finalScorerProps = finalScorer.generateReport(bseqsFinal, structure, interactionMatrices);
	mergeProperties(defScorerProps, "s1.");
	mergeProperties(finalScorerProps, "s2.");
        if (constraintScorer != null) {
	   Properties constraintScorerProps = constraintScorer.generateReport(bseqsFinal, structure, interactionMatrices);
 	   mergeProperties(constraintScorerProps, "s3.");
	}
	try {
	    double finalTotalScore = Double.parseDouble(defScorerProps.getProperty("score")) 
		+ finalScoreWeight * Double.parseDouble(finalScorerProps.getProperty("score"));
	    if (getProperty("s3.score") != null) {
		finalTotalScore += Double.parseDouble(getProperty("s3.score"));
	    }
            setProperty("total_score", "" + finalTotalScore);
	} catch (NumberFormatException nfe) {
            setProperty("total_score", "Internal error: could not add score components.");	    
	}
	log.info("Finished MonteCarloSequenceOptimizer.optimize !");
	return result; 
    }

    public String[] optimizeSystematic(SecondaryStructure structure) {
	assert finalScorer != null;
	assert(structure.getAlphabets() != null);
	assert defaultScorer != null;
	log.info("Starting MonteCarloSequenceOptimizer.optimize !");
	int rerunCount = 0;
	int[][][][] interactionMatrices = generateInteractionMatrices(structure);
	// StringBuffer[] bseqsBest = null;
	double errorScoreTotBest = 99999.0;
	double errorScoreBest = 1e10;
	this.constraintScorer = new SequenceConstraintScorer(structure);
	System.out.println("Defined sequence constraints: " + constraintScorer.toString());
	StringBuffer[] bseqsFinal = null;
	StringBuffer[] bseqs = initStringBuffers2(structure, randomizeFlag, interactionMatrices, structure.getAlphabets()); // default: optimization
	double errorScore = defaultScorer.scoreStructure(bseqs, structure, interactionMatrices) + constraintScorer.scoreStructure(bseqs, structure, interactionMatrices);
	if (errorScore < errorScoreBest) {
	    errorScoreBest = errorScore;
	}
	log.info("Starting optimization run " + (rerunCount + 1) + " with score : " 
		 + errorScore + " and sequences: " + NEWLINE + generateSequencesOutputString(bseqs));
	if (!hasUpperCaseCharacters(bseqs)) {
	    log.warning("No residue can be optimized because they are all in lower case characters!");
	    return null;
	}
	int stage1Steps = 0;
	int stage2Steps = 0;
	int trialMax = 0;
	for (int i = 0; i < bseqs.length; ++i) {
	    trialMax += bseqs[i].length(); // total number of nucleotides is maximum id of mutation
	}
	for (int i = 0; (i < iterMax) && (errorScore > 0.0); ++i, ++stage1Steps) {
	    int trialIter = 0;
	    int[] trialIters = new int[1];
	    trialIters[0] = trialIter;
	    while (trialIter < trialMax) {
		// apply default scorer:
		if (!sanityCheck(bseqs, structure, interactionMatrices)) {
		    log.log(debugLogLevel, "Not all base pairs are yet compatible!");
		} else {
		    // log.fine("Sanity checks passed!");
		}
		assert(structure.getAlphabets() != null);
		errorScore = optimizationSystematicTrial(bseqs, structure, errorScore, interactionMatrices, false, structure.getAlphabets(),
							 trialIters);
		if (doSanityChecks) {
		    sanityCheck(bseqs, structure, interactionMatrices);
		}
		// log.info("New error Score: " + errorScore + " " + i);
		if (((i % stride1) == 0) || (errorScore < errorScoreBest)) {
		    System.out.println("Round " + (rerunCount+1) + " : Sequences at stage 1 iteration " + (i+1) + " and score: " + errorScore
				       + " so far best: " + errorScoreBest);
		    System.out.println(generateSequencesOutputString(bseqs));
		    PropertyTools.printProperties(System.out,defaultScorer.generateReport(bseqs, structure, interactionMatrices));
		}
		if (errorScore < errorScoreBest) {
		    errorScoreBest = errorScore;
		    log.info("New best sequence optimization (stage 1) score found at run " + (rerunCount+1)
			     + " and iteration " + (i+1) + " " + errorScoreBest);
		    bseqsFinal = cloneBuffers(bseqs);
		}
	    } // while (trialIter < trialMax)
	} // for (int = 0 ...
	log.info("Ended optimization run after steps " + stage1Steps + " " + stage2Steps 
		 + " with strings: " + NEWLINE + generateSequencesOutputString(bseqs));
	// AFTER_OPT: // label to allow quitting of optimization
	if (bseqsFinal == null) {
	    log.warning("Sorry, no sequences that fullfill all optimization criteria found!");
	    return null;
	}
	String[] result = new String[bseqsFinal.length]; // translate back to strings
	log.info("Best error score of all runs: " + errorScoreBest);
	log.info("Best error score 2 of all runs: " + errorScoreTotBest);
	setProperty("best_score", "" + errorScoreBest);
	setProperty("best_score2", "" + errorScoreBest); // TotBest);
	log.info("Final strings: " + NEWLINE + generateSequencesOutputString(bseqsFinal));
	for (int i = 0; i < bseqsFinal.length; ++i) {
	    result[i] = bseqsFinal[i].toString();
	}
	Properties defScorerProps = defaultScorer.generateReport(bseqsFinal, structure, interactionMatrices);
	Properties finalScorerProps = finalScorer.generateReport(bseqsFinal, structure, interactionMatrices);
	mergeProperties(defScorerProps, "s1.");
	mergeProperties(finalScorerProps, "s2.");
        if (constraintScorer != null) {
	   Properties constraintScorerProps = constraintScorer.generateReport(bseqsFinal, structure, interactionMatrices);
 	   mergeProperties(constraintScorerProps, "s3.");
	}
	log.info("Finished MonteCarloSequenceOptimizer.optimize !");
	return result; 
    }

    public String[] optimize(SecondaryStructure structure) {
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Starting sequence optimization with algorithm " + algorithm + " and structure:");
	System.out.println(writer.writeString(structure));
	if (algorithm.equals("twostage")) {
	    return optimize2Stage(structure);
	} else if (algorithm.equals("systematic")) {
	    return optimizeSystematic(structure);
	} else {
	    log.warning("Unknown optimization algorithm! Defined: twostage");
	}
	return null;
    }

    /** If true, randomize sequences upon initialization */
    public void setRandomizeFlag(boolean flag) { this.randomizeFlag = flag; }

}
