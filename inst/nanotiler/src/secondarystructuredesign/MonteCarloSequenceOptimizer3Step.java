package secondarystructuredesign;

import rnasecondary.*;
import java.util.Properties;
import java.util.logging.*;
import sequence.DnaTools;
import sequence.Alphabet;
import sequence.Sequence;
import sequence.SequenceStatus;
import sequence.Residue;
import java.util.Random;
import generaltools.PropertyCarrier;
import generaltools.Randomizer;
import generaltools.SimplePropertyCarrier;
import numerictools.IntegerArrayTools;
import static secondarystructuredesign.PackageConstants.*;

import java.util.Date;


/** generates a set of sequences optimized to fullfill secondary structure */
public class MonteCarloSequenceOptimizer3Step extends SimplePropertyCarrier implements SequenceOptimizer {
    private Logger log = Logger.getLogger("NanoTiler_debug");
    private Alphabet alphabet = DnaTools.RNA_ALPHABET;
    private double errorScoreLimit = 0.0; // limit for acceptable solution
    private int iterMax = 500;
    private int iter2Max = 200;

    // New
    private int iter3Max = 500;

    private int rerun = 1;
    private double kt = 0.5;
    private double kt2 = 0.2;
    private double gcContent = 0.6; // target G+C content
    private boolean ignoreNotConstantMode = false; // if true, set all non-constant residue interactions as UNKNOWN_SUBTYPE, effectively ignoring them
    private double mismatchPenalty = 1.0;
    private double matchPenalty = 1.0; // 1; // 0.1;
    private double energyAU = -0.8;
    private double energyGC = -1.2;
    private double sameResiduePenalty = 0.2; // 0.1; // 0.1;
    
    // New
    private double midScoreWeight = 0.1;
    
    private double finalScoreWeight = 0.1;
    private Random rand = Randomizer.getInstance();
    private boolean randomizeFlag = true; // true;
    private int[] nucleotideProbabilities;
    private static final int CHAR_HELP_SIZE = 100;
    private char[] nucleotideHelperChars;
    private SecondaryStructureScorer defaultScorer = new CritonScorer(); // new DefaultSecondaryStructureScorer();
    
    // New
    private SecondaryStructureScorer midScorer = new RnacofoldSecondaryStructureScorer();
    
    private SecondaryStructureScorer finalScorer = new CritonScorer();
    
    public MonteCarloSequenceOptimizer3Step() {
	nucleotideProbabilities = new int[alphabet.size()];
	initProbabilities();
    }

    /** Evaluate secondary structure, do not optimize. */
    public Properties eval(SecondaryStructure structure) { assert false; return null; }

    private void initProbabilities() {
	nucleotideProbabilities[0] = (int)(CHAR_HELP_SIZE * (1-gcContent)/2); // A content in percent
	nucleotideProbabilities[1] = (int)(CHAR_HELP_SIZE * (gcContent)/2); // C
	nucleotideProbabilities[2] = (int)(CHAR_HELP_SIZE * (gcContent)/2); // G
	nucleotideProbabilities[3] = (int)(CHAR_HELP_SIZE * (1-gcContent)/2); // U
	int sum = 0;
	for (int i = 0; i < nucleotideProbabilities.length; ++i) {
	    sum += nucleotideProbabilities[i];
	}
	nucleotideHelperChars = new char[sum];
	int pc = 0;
	for (int i = 0; i < nucleotideProbabilities.length; ++i) {
	    int numChar = nucleotideProbabilities[i];
	    for (int j = 0; j < numChar; ++j) {
		if (pc >= nucleotideHelperChars.length) {
		    assert false; // should never happen
		}
		nucleotideHelperChars[pc++] = alphabet.getSymbol(i).getCharacter();
	    } 
	}
	if (pc != CHAR_HELP_SIZE) {
	    log.severe("Bad helper index: " + pc + " " + CHAR_HELP_SIZE);
	}
	StringBuffer buf = new StringBuffer();
	for (int i = 0; i <  nucleotideHelperChars.length; ++i) {
	    buf.append("" + nucleotideHelperChars[i]);
	}
	log.finest("Helper character array: " + buf.toString());
    }

    // internal class describing mutation of n'th sequence at position p with character c 
    private class Mutation {
	private int seqId; // sequence id
	private int pos; // position in sequence
	private char character; // character
	Mutation(int seqId, int pos, char character) {
	    this.seqId = seqId;
	    this.pos = pos;
	    this.character = character;
	}
	int getPos() { return pos; }
	char getCharacter() { return character; }
	int getSeqId() { return seqId; }
	void setCharacter(char c) { this.character = c; }
    }

    public SecondaryStructureScorer getDefaultScorer() { return this.defaultScorer; }

    public double getErrorScoreLimit() { return this.errorScoreLimit; }

    public SecondaryStructureScorer getFinalScorer() { return this.finalScorer; }

    /** Returns ignoreNonConstantMode. See setIgnoreNonConstantMode for more details. */
    public boolean getIgnoreNotConstantMode() { return this.ignoreNotConstantMode; }

    public double getGcContent() { return gcContent; }

    /** returns kt value that determines acceptance probability of unfavourable mutations */
    public double getKt() { return kt; }

    public int getIterMax() { return iterMax; }

    public int getIter2Max() { return iter2Max; }

    public void setDefaultScorer(SecondaryStructureScorer scorer) { this.defaultScorer = scorer; }

    // New
    public void setMidScorer(SecondaryStructureScorer scorer) { this.midScorer = scorer; }

    public void setFinalScorer(SecondaryStructureScorer scorer) { this.finalScorer = scorer; }

    public void setMidScoreWeight(double w) { this.midScoreWeight = w; }

    public void setFinalScoreWeight(double w) { this.finalScoreWeight = w; }

    public void setGcContent(double x) { this.gcContent = x; initProbabilities(); }

    public void setErrorScoreLimit(double x) { this.errorScoreLimit = x; }
    
    /** if true, set all non-constant residue interactions as UNKNOWN_SUBTYPE, effectively ignoring them. */
    public void setIgnoreNotConstantMode(boolean mode) { this.ignoreNotConstantMode = mode; }

    /** number of steps of first-stage optimazation */
    public void setIterMax(int n) { this.iterMax = n; }

    /** number of steps of second-stage optimization */
    public void setIter2Max(int n) { this.iter2Max = n; }

    // New
    /** number of steps of third-stage optimization */
    public void setIter3Max(int n) { this.iter3Max = n; }

    /** sets how many times the algorithm starts over */
    public void setRerun(int n) { this.rerun = n; }

    /** sets kt value that determines acceptance probability of unfavourable mutations */
    public void setKt(double kt) {
	assert kt > 0.0;
	this.kt = kt;
    }

    /** mutates a character (using nucleotide alphabet) if upper case */
    private char mutateCharacter(char c) {
	if (Character.isLowerCase(c)) {
	    return c;
	}
	char result = c;
	do {
	    result = nucleotideHelperChars[rand.nextInt(nucleotideHelperChars.length)];
	}
	while (result == c);
	return result;
    }

    /** randomizes sequence */
    private void randomizeSequence(StringBuffer buf) {
	int origLen = buf.length();
	for (int i = 0; i < buf.length(); ++i) {
	    if (Character.isUpperCase(buf.charAt(i))) {
		buf.setCharAt(i, mutateCharacter(buf.charAt(i)));
// 		if ((i>0) && (buf.charAt(i-1) == buf.charAt(i))) {
// 		    // avoid same nucleotides in a row, try again one more time:
// 		    buf.setCharAt(i, mutateCharacter(buf.charAt(i)));
// 		}
	    }
	}
	assert buf.length() == origLen; // no change in length
    }

    /** returns random watson crick pair (GC or AU) */
    private String generateRandomWatsonCrick() {
	String result = "";
	if (rand.nextDouble() > gcContent) {
	    result = "AU";
	}
	else {
	    result = "CG";
	}
	if (rand.nextDouble() > 0.5) {
	    return "" + result.charAt(1) + result.charAt(0);
	}
	assert result.length() == 2;
	return result;
    }

    private void randomizeSequences2(StringBuffer[] buf,
				     int[][][][] interactionMatrices ) {
	for (int i = 0; i < buf.length; ++i) {
	    randomizeSequence(buf[i]);
	}
	for (int i = 0; i < interactionMatrices.length; ++i) {
	    for (int j = i; j < interactionMatrices[i].length; ++j) {
		if (interactionMatrices[i][j].length != buf[i].length()) {
		    log.warning("" + i + " " + j + " " + interactionMatrices[i][j].length
				+ " " + buf[i].length());
		}
		assert interactionMatrices[i][j].length == buf[i].length();
		for (int k = 0; k < interactionMatrices[i][j].length; ++k) {
		    assert interactionMatrices[i][j][k].length == buf[j].length();
		    for (int m = 0; m < interactionMatrices[i][j][k].length; ++m) {
			if (Character.isUpperCase(buf[i].charAt(k)) && Character.isUpperCase(buf[j].charAt(m) ) ) {
			    if (interactionMatrices[i][j][k][m] == RnaInteractionType.WATSON_CRICK) {
				String randWC = generateRandomWatsonCrick();
				buf[i].setCharAt(k, randWC.charAt(0));
				buf[j].setCharAt(m, randWC.charAt(1));
// 				if ((k > 0) && (m > 0)) {
// 				    if ((buf[i].charAt(k-1) == buf[i].charAt(k))
// 					|| (buf[j].charAt(m-1) == buf[j].charAt(m))) {
// 					// avoid duplication, try one more time:
// 					randWC = generateRandomWatsonCrick();
// 					buf[i].setCharAt(k, randWC.charAt(0));
// 					buf[j].setCharAt(m, randWC.charAt(1));
// 				    }
//				}
			    }
			}
			
		    }
		}
	    }
	}
    }

    private String generateSequencesOutputString(StringBuffer[] bufs) {
	StringBuffer result = new StringBuffer();
	for (int i = 0; i < bufs.length; ++i) {
	    result.append(bufs[i]);
	    result.append(NEWLINE);
	}
	return result.toString();
    }
    
    /** Initializes string buffers. If residue status is unknown, perform no optimization. */
    /*
    private StringBuffer[] initStringBuffers(SecondaryStructure structure, 
					     boolean randomizeFlag) {
	StringBuffer[] bseqs = new StringBuffer[structure.getSequenceCount()];
	for (int i = 0; i < bseqs.length; ++i) {
	    Sequence sequence = structure.getSequence(i);
	    bseqs[i] = new StringBuffer(sequence.sequenceString());
	    for (int j = 0; j < bseqs[i].length(); ++j) {
		Residue res = sequence.getResidue(j);
		if (SequenceStatus.fragment.equals(res.getProperty(SequenceStatus.name))) {
		    bseqs[i].setCharAt(j, Character.toLowerCase(bseqs[i].charAt(j)));
		}
		else if (SequenceStatus.adhoc.equals(res.getProperty(SequenceStatus.name))) {
		    bseqs[i].setCharAt(j, Character.toUpperCase(bseqs[i].charAt(j)));
		}
		else {
		    log.warning("Residue status unknown: " + bseqs[i] + " " + res.getSymbol() + " " 
				+ (i+1) + " " + (j+1));
		    // bseqs[i].setCharAt(j, Character.toUpperCase(bseqs[i].charAt(j)));
		    bseqs[i].setCharAt(j, Character.toLowerCase(bseqs[i].charAt(j))); // do not modify if unknown
		}
	    }
	    if (randomizeFlag) {
		randomizeSequence(bseqs[i]);
	    }
	}
	return bseqs;
    }
    */

    private Mutation generateMutation(StringBuffer[] bseqs) {
	int seqId = 0;
	int pos = 0;
	char newChar = 'N';
	char oldChar = 'N';
	do {
	    seqId = rand.nextInt(bseqs.length);
	    pos = rand.nextInt(bseqs[seqId].length());
	    oldChar = bseqs[seqId].charAt(pos);
	}
	while (Character.isLowerCase(oldChar)); // do not mutate lower case characters. Careful: infinite loop if all lower char
	do {
	    newChar = alphabet.getSymbol(rand.nextInt(alphabet.size())).getCharacter();
	}
	while (newChar == oldChar); // must be different
	assert Character.isUpperCase(bseqs[seqId].charAt(pos));
	return new Mutation(seqId, pos, newChar);
    }

    private char generateWatsonCrickPartner(char c) {
	switch (c) {
	case 'A': return 'U';
	case 'C': return 'G';
	case 'G': return 'C';
	case 'U': return 'A';
	}
	assert false; // should never be here
	return 'N';
    }

    Mutation findMatchingMutation(Mutation mutation,
				  int[][][][] interactionMatrices) {
	int seqId = mutation.getSeqId();
	int pos = mutation.getPos();
	char c = mutation.getCharacter();
	for (int i = 0; i < seqId; ++i) {
	    for (int j = 0; j < interactionMatrices[i][seqId].length; ++j) {
		if (interactionMatrices[i][seqId][j][pos] == RnaInteractionType.WATSON_CRICK) {
		    return new Mutation(i, j, generateWatsonCrickPartner(c)); 
		}
	    }
	}
	for (int i = seqId+1; i < interactionMatrices[seqId].length; ++i) {
	    for (int j = 0; j < interactionMatrices[seqId][i][pos].length; ++j) {
		if (interactionMatrices[seqId][i][pos][j] == RnaInteractionType.WATSON_CRICK) {
		    return new Mutation(i, j, generateWatsonCrickPartner(c)); 
		}
	    }
	}
	return null;
    }

    private Mutation generateMatchingMutation(StringBuffer[] bseqs, Mutation prevMutation,
					      int[][][][] interactionMatrices) {
	Mutation mut = findMatchingMutation(prevMutation, interactionMatrices);
	if (mut != null) {
	    return mut;
	}
	int seqId = 0;
	int pos = 0;
	char newChar = 'N';
	char oldChar = 'N';
	do {
	    seqId = rand.nextInt(bseqs.length);
	    pos = rand.nextInt(bseqs[seqId].length());
	    oldChar = bseqs[seqId].charAt(pos);
	}
	while (Character.isLowerCase(oldChar)); // do not mutate lower case characters. Careful: infinite loop if all lower char

	do {
	    newChar = alphabet.getSymbol(rand.nextInt(alphabet.size())).getCharacter();
	}
	while (newChar == oldChar); // must be different
	return new Mutation(seqId, pos, newChar);
    }

    /** Initializes string buffers. If residue status is unknown, perform optimization. */
    private StringBuffer[] initStringBuffers2(SecondaryStructure structure, 
					      boolean randomizeFlag,
					      int[][][][] interactionMatrices ) {
	StringBuffer[] bseqs = new StringBuffer[structure.getSequenceCount()];
	for (int i = 0; i < bseqs.length; ++i) {
	    Sequence sequence = structure.getSequence(i);
	    bseqs[i] = new StringBuffer(sequence.sequenceString());
	    for (int j = 0; j < bseqs[i].length(); ++j) {
		Residue res = sequence.getResidue(j);
		if (SequenceStatus.fragment.equals(res.getProperty(SequenceStatus.name))) {
		    bseqs[i].setCharAt(j, Character.toLowerCase(bseqs[i].charAt(j)));
		}
		else if (SequenceStatus.adhoc.equals(res.getProperty(SequenceStatus.name))) {
		    bseqs[i].setCharAt(j, Character.toUpperCase(bseqs[i].charAt(j)));
		}
		else {
		    log.warning("Residue status unknown: " + bseqs[i] + " " + res.getSymbol() + " " 
				+ (i+1) + " " + (j+1) + " : setting for optimizable (adhoc)");
		    bseqs[i].setCharAt(j, Character.toUpperCase(bseqs[i].charAt(j))); // if residue status unknown, perform optimization
		    // bseqs[i].setCharAt(j, Character.toLowerCase(bseqs[i].charAt(j))); // if residue status unknown, perform NO optimization
		}
	    }
	}
	if (randomizeFlag) {
	    log.info("Randomizing sequences (2)!");
	    randomizeSequences2(bseqs, interactionMatrices );
	}
	return bseqs;
    }

    private void applyMutation(Mutation mutation,
			       StringBuffer[] bseqs) {
	if (Character.isUpperCase(bseqs[mutation.getSeqId()].charAt(mutation.getPos()))) {
	    bseqs[mutation.getSeqId()].setCharAt(mutation.getPos(), mutation.getCharacter());
	}
	else {
	    // ignore mutation because residue was set to constant indicated by a lower case residue character
	}
    }

    int[][] generateInteractionMatrix(SecondaryStructure structure, int id1, int id2) {
	Sequence seq1 = structure.getSequence(id1);
	Sequence seq2 = structure.getSequence(id2);
	int[][] result = new int[seq1.size()][seq2.size()];
	for (int i = 0; i < result.length; ++i) {
	    for (int j = 0; j < result[i].length; ++j) {
		result[i][j] = RnaInteractionType.NO_INTERACTION;
	    }
	}
	for (int i = 0; i < structure.getInteractionCount(); ++i) {
	    Interaction interaction = structure.getInteraction(i);
	    int pos1 = interaction.getResidue1().getPos();
	    int pos2 = interaction.getResidue2().getPos();
	    if ((interaction.getSequence1() == seq1) && (interaction.getSequence2() == seq2)) {
		result[pos1][pos2] = interaction.getInteractionType().getSubTypeId();
		if (id1 == id2) {
		    result[pos2][pos1] = result[pos1][pos2]; // if same sequence: symmetric
		}
	    }
	    else if ((interaction.getSequence2() == seq1) && (interaction.getSequence1() == seq2)) {
		result[pos2][pos1] = interaction.getInteractionType().getSubTypeId();		
		assert id1 != id2;
	    }
	}
	// sets rows and columns to UNKNOWN_INTERACTION, if property seqstatus is "ignore"
	for (int i = 0; i < result.length; ++i) {
	    if (("ignore".equals(seq1.getResidue(i).getProperty("seqstatus")))
		|| (ignoreNotConstantMode && SequenceStatus.fragment.equals(seq1.getResidue(i).getProperty(SequenceStatus.name)))) {
		for (int j = 0; j < result[i].length; ++j) {
		    result[i][j] = RnaInteractionType.UNKNOWN_SUBTYPE;
		}
	    }
	}
	for (int i = 0; i < result[0].length; ++i) {
	    if (("ignore".equals(seq2.getResidue(i).getProperty("seqstatus"))) 
		|| (ignoreNotConstantMode && SequenceStatus.fragment.equals(seq2.getResidue(i).getProperty(SequenceStatus.name)))) {
		for (int j = 0; j < result.length; ++j) {
		    result[j][i] = RnaInteractionType.UNKNOWN_SUBTYPE;
		}
	    }
	}
	return result;
    }


    int[][][][] generateInteractionMatrices(SecondaryStructure structure) {
	int[][][][] matrices = new int[structure.getSequenceCount()][structure.getSequenceCount()][0][0];
	for (int i = 0; i < structure.getSequenceCount(); ++i) {
	    for (int j = i; j < structure.getSequenceCount(); ++j) {
		matrices[i][j] = generateInteractionMatrix(structure, i, j);
	    }
	}
	return matrices;
    }

    /** Using only simple scorer */
    private double optimizationTrial(StringBuffer[] bseqs,
				     SecondaryStructure structure,
				     double oldScore,
				     int[][][][] interactionMatrices) {
	log.finest("Starting optimizationTrial: " + oldScore);
	Mutation mutation = generateMutation(bseqs);
	Mutation mutation2 = generateMatchingMutation(bseqs, mutation, interactionMatrices);
	int count = 0;
// 	while ((mutation2.getPos() == mutation.getPos()) && (mutation2.getSeqId() == mutation.getSeqId())) {
// 	    mutation2 = generateMutation(bseqs);
// 	    if (++count > 100) {
// 		log.warning("Could not find suitable second mutation!");
// 		return oldScore;
// 	    }
// 	}

	char oldCharacter = bseqs[mutation.getSeqId()].charAt(mutation.getPos());
	char oldCharacter2 = bseqs[mutation2.getSeqId()].charAt(mutation2.getPos());
	applyMutation(mutation, bseqs); // mutate
	applyMutation(mutation2, bseqs); // mutate
	double newScore = defaultScorer.scoreStructure(bseqs, structure, interactionMatrices);
	log.fine("new score: " + newScore);
	if (Math.exp(-(newScore - oldScore)/kt) < rand.nextDouble()){ // worse, do not accept step
	    mutation.setCharacter(oldCharacter); // set to former character
	    mutation2.setCharacter(oldCharacter2); // set to former character
	    applyMutation(mutation, bseqs);
	    applyMutation(mutation2, bseqs);
	    newScore = oldScore;
	}
	else {
	    if (newScore < oldScore) {
		log.finest("New score accepted: " + newScore + " " + oldScore);
	    }
	}
	log.finest("Finished optimizationTrial!");
	return newScore;
    }

    /** Using simple scorer and energy-based scorer. */
    private double optimizationTrial2(StringBuffer[] bseqs,
				     SecondaryStructure structure,
				     double oldScore,
				     int[][][][] interactionMatrices) {
	log.finest("Starting optimizationTrial: " + oldScore);
	Mutation mutation = generateMutation(bseqs);
	Mutation mutation2 = generateMatchingMutation(bseqs, mutation, interactionMatrices);
	char oldCharacter = bseqs[mutation.getSeqId()].charAt(mutation.getPos());
	char oldCharacter2 = bseqs[mutation2.getSeqId()].charAt(mutation2.getPos());
	applyMutation(mutation, bseqs); // mutate
	applyMutation(mutation2, bseqs); // mutate
	double newScore1 = defaultScorer.scoreStructure(bseqs, structure, interactionMatrices);
	double newScore2 = midScorer.scoreStructure(bseqs, structure, interactionMatrices);
	double newScore = newScore1 + finalScoreWeight * newScore2;
	log.info("new score: " + newScore + " " + newScore1 + " " + newScore2);
	if (Math.exp(-(newScore - oldScore)/kt2) < rand.nextDouble()){ // worse, do not accept step
	    mutation.setCharacter(oldCharacter); // set to former character
	    mutation2.setCharacter(oldCharacter2); // set to former character
	    applyMutation(mutation, bseqs);
	    applyMutation(mutation2, bseqs);
	    newScore = oldScore;
	}
	else {
	    if (newScore < oldScore) {
		log.finest("New score accepted: " + newScore + " " + oldScore);
	    }
	}
	log.finest("Finished optimizationTrial!");
	return newScore;
    }

    /** Using only simple scorer */
    private double optimizationTrial3(StringBuffer[] bseqs,
                                     SecondaryStructure structure,
                                     double oldScore,
                                     int[][][][] interactionMatrices) {
        log.finest("Starting optimizationTrial: " + oldScore);
        Mutation mutation = generateMutation(bseqs);
        Mutation mutation2 = generateMatchingMutation(bseqs, mutation, interactionMatrices);
        int count = 0;
	//      while ((mutation2.getPos() == mutation.getPos()) && (mutation2.getSeqId() == mutation.getSeqId())) {
	//          mutation2 = generateMutation(bseqs);
	//          if (++count > 100) {
	//              log.warning("Could not find suitable second mutation!");
	//              return oldScore;
	//          }
	//      }
                                                                                                                                                             
        char oldCharacter = bseqs[mutation.getSeqId()].charAt(mutation.getPos());
        char oldCharacter2 = bseqs[mutation2.getSeqId()].charAt(mutation2.getPos());
        applyMutation(mutation, bseqs); // mutate
        applyMutation(mutation2, bseqs); // mutate
        double newScore = finalScorer.scoreStructure(bseqs, structure, interactionMatrices);
        log.fine("new score: " + newScore);
        if (Math.exp(-(newScore - oldScore)/kt) < rand.nextDouble()){ // worse, do not accept step
            mutation.setCharacter(oldCharacter); // set to former character
            mutation2.setCharacter(oldCharacter2); // set to former character
            applyMutation(mutation, bseqs);
            applyMutation(mutation2, bseqs);
            newScore = oldScore;
        }
        else {
            if (newScore < oldScore) {
                log.finest("New score accepted: " + newScore + " " + oldScore);
            }
        }
        log.finest("Finished optimizationTrial!");
        return newScore;
    }

    /** returns true if sequence has upper case characters */
    boolean hasUpperCaseCharacters(String s) {
	for (int i = 0; i < s.length(); ++i) {
	    if (Character.isUpperCase(s.charAt(i))) {
		return true;
	    }
	}
	return false;
    }
    
    /** returns true if sequence has upper case characters */
    boolean hasUpperCaseCharacters(StringBuffer[] bseqs) {
	for (int i = 0; i < bseqs.length; ++i) {
	    if (hasUpperCaseCharacters(bseqs[i].toString())) {
		return true;
	    }
	}
	return false;
    }


    private StringBuffer[] cloneBuffers(StringBuffer[] bufs) {
	assert bufs != null;
	StringBuffer[] result = new StringBuffer[bufs.length];
	for (int i = 0; i < bufs.length; ++i) {
	    result[i] = new StringBuffer(bufs[i].toString());
	}
	return result;
    }

    public String[] optimize(SecondaryStructure structure) {
	assert finalScorer != null;
	log.info("Starting MonteCarloSequenceOptimizer3Step.optimize !");
	int rerunCount = 0;
	int[][][][] interactionMatrices = generateInteractionMatrices(structure);
	// StringBuffer[] bseqsBest = null;
	double errorScoreTotBest = 99999.0;
	double errorScoreBest = 1e10;
	double stage3Best = 99999.0;
	StringBuffer[] bseqsFinal = null;
	do {
	    StringBuffer[] bseqs = initStringBuffers2(structure, randomizeFlag, interactionMatrices); // default: optimization
	    double errorScore = defaultScorer.scoreStructure(bseqs, structure, interactionMatrices);
	    if (errorScore < errorScoreBest) {
		errorScoreBest = errorScore;
	    }
	    log.info("Starting optimization run " + (rerunCount + 1) + " with score : " 
		     + errorScore + " and sequences: " + NEWLINE + generateSequencesOutputString(bseqs));
	    if (!hasUpperCaseCharacters(bseqs)) {
		log.warning("No residue can be optimized because they are all in lower case characters!");
		break;
	    }
	    int stage1Steps = 0;
	    int stage2Steps = 0;
	    int stage3Steps = 0;
	    for (int i = 0; (i < iterMax) && (errorScore > 0.0); ++i, ++stage1Steps) {
		// apply default scorer:
		errorScore = optimizationTrial(bseqs, structure, errorScore, interactionMatrices);
		// log.info("New error Score: " + errorScore + " " + i);
		if (errorScore < errorScoreBest) {
		    errorScoreBest = errorScore;
		    log.info("New best sequence optimization (stage 1) score found at run " + (rerunCount+1)
			     + " and iteration " + (i+1) + " " + errorScoreBest);
		    // bseqsBest = cloneBuffers(bseqs);
		}
		if ((i % 100) == 0) {
		    System.out.println("Round " + (rerunCount+1) + " : Sequences at stage 1 iteration " + (i+1) + " and score: " + errorScore);
		    System.out.println(generateSequencesOutputString(bseqs));
		}
		if (errorScore <= errorScoreLimit) {
		    // discrepancy score with RNAfold prediction:
		    System.out.println("Scoring structure with stage 2 scorer");
		    double errorScore2 = midScorer.scoreStructure(bseqs, structure, interactionMatrices);
		    System.out.println("Done initial scoring!");
		    // inner loop: optimize sequences, such that rules are fullfilled, best RNAfold structure:
		    double errorScoreTot = errorScore + midScoreWeight * errorScore2;
		    log.info("Starting stage 2 of sequence optimization with error score:" + errorScore + " " + errorScore2 + " " + errorScoreTot);
		    // errorScoreTotBest = errorScoreTot;

		    for (int i2 = 0; i2 < iter2Max; ++i2, ++stage2Steps) {
			// check if basic rules are fullfilled:
			// errorScore = defaultScorer.scoreStructure(bseqs, structure, interactionMatrices);		    
			errorScoreTot = optimizationTrial2(bseqs, structure, errorScoreTot, interactionMatrices);
			log.fine("Score of inner loop: " + errorScore
				 + " " + errorScoreTot + " at round " + (rerunCount+1)
				 + " and iteration " + (i2+1));
			if (errorScoreTot < errorScoreTotBest) {
			    log.info("Found new best sequences (stage2) with score: " 
				     + errorScoreTot + " at outer iteration " + (i+1) 
				     + " and inner iteration: " + (i2+1));
			    errorScoreTotBest = errorScoreTot;
			    bseqsFinal = cloneBuffers(bseqs);
			}
			if ((i2 % 10) == 0) {
			    System.out.println("Round " + (rerunCount+1) + " : Sequences at stage 2 iteration " + (i2+1) + " and score: " + errorScoreTot);
			    System.out.println(generateSequencesOutputString(bseqs));
			}
		    }
		    break; // quit this loop, start over with completely new set of randomized sequences
		}
	    }

	    bseqs = cloneBuffers(bseqsFinal);
	    for (int i3 = 0; i3 < iter3Max; ++i3, ++stage3Steps) {
		System.out.println("Entering final scoring stage");

		// check if basic rules are fullfilled:
		errorScore = optimizationTrial3(bseqs, structure, errorScore, interactionMatrices);

                // log.info("New error Score: " + errorScore + " " + i);
                if (errorScore < stage3Best) {
                    stage3Best = errorScore;
		    bseqsFinal = cloneBuffers(bseqs);
                    log.info("New best sequence optimization (stage 1) score found at run " + (rerunCount+1)
                             + " and iteration " + (i3+1) + " " + errorScore);
                    // bseqsBest = cloneBuffers(bseqs);
                }
                if ((i3 % 100) == 0) {
                    System.out.println("Round " + (rerunCount+1) + " : Sequences at stage 3 iteration " + (i3+1) + " and score: " + errorScore);
                    System.out.println(generateSequencesOutputString(bseqs));
                }
	    }
	    log.info("Ended optimization run after steps " + stage1Steps + " " + stage2Steps + " " + stage3Steps
		     + " with strings: " + NEWLINE + generateSequencesOutputString(bseqs));
	}
	while ((++rerunCount < rerun)); // restart if not optimal score was found
	if (bseqsFinal == null) {
	    log.warning("Sorry, no sequences that fullfill all optimization criteria found!");
	    return null;
	}
	String[] result = new String[bseqsFinal.length]; // translate back to strings
	log.info("Best error score of all runs: " + errorScoreBest);
	log.info("Best error score 2 of all runs: " + errorScoreTotBest);
	log.info("Best error score of stage 3: " + stage3Best);
	log.info("Final strings: " + NEWLINE + generateSequencesOutputString(bseqsFinal));
	for (int i = 0; i < bseqsFinal.length; ++i) {
	    result[i] = bseqsFinal[i].toString();
	}
	log.info("Finished MonteCarloSequenceOptimizer.optimize !");

	System.out.println("");
	System.out.println("Final optimized strands from MonteCarloSequenceOptimizer:");
	for (int i = 0; i < result.length; i++) {
	    System.out.println(result);
	}
	System.out.println("");

	return result; 
    }

    /** If true, randomize sequences upon initialization */
    public void setRandomizeFlag(boolean flag) { this.randomizeFlag = flag; }

}
