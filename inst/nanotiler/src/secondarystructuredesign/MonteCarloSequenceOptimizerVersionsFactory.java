package secondarystructuredesign;

import java.util.Properties;
import rnasecondary.*;

/** Generates configurations of sequence optimizer that have been used in previous designs */
public class MonteCarloSequenceOptimizerVersionsFactory implements SequenceOptimizerFactory {

    private Properties rb;

    public static final int VERSION_MARCH_30_2009 = 1;

    public static final int VERSION_OCTOBER_2008 = 2;

    public static final int VERSION_APRIL_16_2009 = 3;

    public static final int VERSION_RULES_ONLY_1 = 4;

    public static final int VERSION_JUNE_16_2009 = 5;

    public static final int VERSION_JUNE_16b_2009 = 6;

    public static final int VERSION_JUNE_17_2009 = 7;

    public static final int VERSION_NO_OPTIMIZATION = 8;

    public static final int VERSION_DECEMBER_2010 = 9;

    public static final int VERSION_JANUARY_2011 = 10;

    public static final int VERSION_DEFAULT = VERSION_OCTOBER_2008; // VERSION_MARCH_30_2009;

    public MonteCarloSequenceOptimizerVersionsFactory(int _versionId) {
	rb = new Properties();
	rb.setProperty("version", "" + _versionId);
    }

    public MonteCarloSequenceOptimizerVersionsFactory(Properties _rb) {
	this.rb = (Properties)(_rb.clone());
    }

    public SequenceOptimizer generate() {
	MonteCarloSequenceOptimizer optimizer = null;
	int versionId = VERSION_DEFAULT;
	if (rb.getProperty("version") != null) {
	    versionId = Integer.parseInt(rb.getProperty("version"));
	}
	switch (versionId) {
	case VERSION_OCTOBER_2008:
	    optimizer = generateVersionOctober2008();
	    break;
	case VERSION_MARCH_30_2009:
	    optimizer = generateVersionMarch302009();
	    break;
	case VERSION_APRIL_16_2009:
	    optimizer = generateVersionApril162009();
	    break;
	case VERSION_RULES_ONLY_1:
	    optimizer = generateRuleOnlyVersionMay202009();
	    break;
	case VERSION_JUNE_16_2009:
	    optimizer = generateVersionJune162009();
	    break;
	case VERSION_JUNE_16b_2009:
	    optimizer = generateVersionJune16b2009();
	    break;
	case VERSION_JUNE_17_2009:
	    optimizer = generateVersionJune172009();
	    break;
	case VERSION_DECEMBER_2010:
	    optimizer = generateVersionDecember2010();
	    break;
	case VERSION_JANUARY_2011:
	    optimizer = generateVersionJanuary2011();
	    break;
	case VERSION_NO_OPTIMIZATION:
	    optimizer = generateNoOptimizationVersion();
	    break;
	default:
	    System.out.println("Internal error: unknown optimizer version");
	    assert(false);
	}
	try {
	    parseAndSetOptimizer(optimizer, rb);
	}
	catch (NumberFormatException nfe) {
	    System.out.println(nfe.getMessage());
	    System.out.println("Error parsing version in parameter file.");
	    System.exit(1); // FIXIT
	}
	return optimizer;
    }

    private void parseAndSetOptimizer(MonteCarloSequenceOptimizer optimizer, Properties rb) throws NumberFormatException {
	if (rb.getProperty("error1") != null) {
	    optimizer.setErrorScoreLimit(Double.parseDouble(rb.getProperty("error1")));
	}
	if (rb.getProperty("iter") != null) {
	    optimizer.setIterMax(Integer.parseInt(rb.getProperty("iter")));
	}
	if (rb.getProperty("iter2") != null) {
	    optimizer.setIter2Max(Integer.parseInt(rb.getProperty("iter2")));
	}
	if (rb.getProperty("rerun") != null) {
	    optimizer.setRerun(Integer.parseInt(rb.getProperty("rerun")));
	}
	boolean optimizeFlag = true;
	String optimizeString = rb.getProperty("optimize");
	if (optimizeString != null) {
	    if (optimizeString.equals("true")) {
		optimizeFlag = true;
	    } else if (optimizeString.equals("false")) {
		optimizeFlag = false;
	    } else {
		throw new NumberFormatException("Expected true or false for boolean option optimize");
	    }
	}
	String randomizeString = rb.getProperty("randomize");
	if (randomizeString != null) {
	    if (randomizeString.equals("true")) {
		optimizer.setRandomizeFlag(true);
	    } else if (randomizeString.equals("false")) {
		optimizer.setRandomizeFlag(false);
	    } else {
		throw new NumberFormatException("Expected true or false for boolean option randomize");
	    }
	} else { // randomimze not specified; for special case of no optimization: set randomize=false
	    if ((!optimizeFlag) || ((optimizer.getIterMax() == 0) && (optimizer.getIter2Max() == 0))) {
		optimizer.setRandomizeFlag(false);
	    }
	}
    }

    private MonteCarloSequenceOptimizer generateVersionMarch302009() {
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setErrorScoreLimit(146.0);
	optimizer.setIterMax(20000);
	optimizer.setIter2Max(1000);
	optimizer.setKt(1.0);
	optimizer.setKt2(0.4);
	optimizer.setRerun(5);
	optimizer.setRandomizeFlag(true); // initial sequence randomization
	// optimizer.setFinalScoreWeight(0.05); 
	optimizer.setFinalScoreWeight(1.0); 
	CritonScorer scorer = new CritonScorer();
	scorer.setConsecutiveLimit(3);
	scorer.setConsecutiveLimitG(2);
	scorer.setCritonLen(6);
	scorer.setAuViolationWeight(10.0);
	scorer.setGcViolationWeight(1.0); // notice: difference compared to testOptimizeCubeCritonLoop24HOpt42UunevenNoWave5
	scorer.setBranchMigrationWeight(10.0);
	scorer.setConsecutiveWeight(10.0);
	scorer.setGCMinFracWeight(0.0); // no minimum value for GC content
	scorer.setGuComplementMode(false);
	scorer.setZipperWeight(10.0);
	optimizer.setDefaultScorer(scorer);
	SecondaryStructureScorer scorer2 = new MatchFoldSecondaryStructureScorer();
	optimizer.setFinalScorer(scorer2);
	return optimizer;
    }

    private MonteCarloSequenceOptimizer generateVersionJune162009() {
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setErrorScoreLimit(146.0);
	optimizer.setIterMax(20000);
	optimizer.setIter2Max(1000);
	optimizer.setKt(1.0);
	optimizer.setKt2(0.4);
	optimizer.setRerun(5);
	optimizer.setRandomizeFlag(true); // initial sequence randomization
	// optimizer.setFinalScoreWeight(0.05); 
	optimizer.setFinalScoreWeight(1.0); 
	CritonScorer scorer = new CritonScorer();
	scorer.setConsecutiveLimit(3);
	scorer.setConsecutiveLimitG(2);
	scorer.setCritonLen(5);
	scorer.setAuViolationWeight(10.0);
	scorer.setGcViolationWeight(1.0); // notice: difference compared to testOptimizeCubeCritonLoop24HOpt42UunevenNoWave5
	scorer.setBranchMigrationWeight(10.0);
	scorer.setConsecutiveWeight(10.0);
	scorer.setGCMinFracWeight(0.0); // no minimum value for GC content
	scorer.setGuComplementMode(false);
	scorer.setZipperWeight(10.0);
	optimizer.setDefaultScorer(scorer);
	SecondaryStructureScorer scorer2 = new MatchFoldSecondaryStructureScorer();
	optimizer.setFinalScorer(scorer2);
	return optimizer;
    }

    /** MODIFIED Version of sequence optimization that was used in June 16th, 2009.
     * Uses RNAcofold and simplefold */
    private MonteCarloSequenceOptimizer generateVersionJune16b2009() {
	System.out.println("Creating MonteCarloSequenceOptimizer version generateVersionJune16b2009");
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setErrorScoreLimit(60.0);
	optimizer.setIterMax(20000);
	optimizer.setIter2Max(10000);
	optimizer.setKt(1.0);
	optimizer.setKt2(0.4);
	optimizer.setRerun(5);
	optimizer.setRandomizeFlag(true); // no initial sequence randomization
	optimizer.setFinalScoreWeight(1.0); // default weight of 0.1

	CritonScorer scorer = new CritonScorer();
	scorer.setConsecutiveLimit(3);
	scorer.setConsecutiveLimitG(2);
	scorer.setCritonLen(5);
	scorer.setAuViolationWeight(10.0);
	scorer.setGcViolationWeight(1.0); // notice: difference compared to testOptimizeCubeCritonLoop24HOpt42UunevenNoWave5
	scorer.setBranchMigrationWeight(10.0);
	scorer.setConsecutiveWeight(10.0);
	scorer.setGCMinFracWeight(0.0); // no minimum value for GC content
	scorer.setGuComplementMode(false);
	scorer.setZipperWeight(10.0);
	optimizer.setDefaultScorer(scorer);

	RnacofoldSecondaryStructureScorer scorer2 = new RnacofoldSecondaryStructureScorer();
	// scorer2.setInterStrandMode(false); // deactivate compute only intra-strand energies
	scorer2.setSelfEnergyCutoff(-10.0); // only penalize strands if very stable energies
	SecondaryStructureScorer scorer3 = new SimpleSecondaryStructurePredictorScorer();
	scorer2.setSubScorer(scorer3);
	scorer2.setSubScorerWeight(5.0);
	optimizer.setFinalScorer(scorer2);
	return optimizer;
    }

    /** MODIFIED Version of sequence optimization that was used in June 17, 2009
     * Uses RNAcofold and simplefold */
    private MonteCarloSequenceOptimizer generateVersionJune172009() {
	System.out.println("Creating MonteCarloSequenceOptimizer version generateVersionJune16b2009");
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setErrorScoreLimit(60.0);
	optimizer.setIterMax(20000);
	optimizer.setIter2Max(10000);
	optimizer.setKt(1.0);
	optimizer.setKt2(0.4);
	optimizer.setRerun(5);
	optimizer.setRandomizeFlag(true); // no initial sequence randomization
	optimizer.setFinalScoreWeight(1.0); // default weight of 0.1

	CritonScorer scorer = new CritonScorer();
	scorer.setConsecutiveLimit(3);
	scorer.setConsecutiveLimitG(2);
	scorer.setCritonLen(5);
	scorer.setAuViolationWeight(10.0);
	scorer.setGcViolationWeight(1.0); // notice: difference compared to testOptimizeCubeCritonLoop24HOpt42UunevenNoWave5
	scorer.setBranchMigrationWeight(10.0);
	scorer.setConsecutiveWeight(10.0);
	scorer.setGCMinFracWeight(0.0); // no minimum value for GC content
	scorer.setGuComplementMode(false);
	scorer.setZipperWeight(10.0);
	optimizer.setDefaultScorer(scorer);

	RnacofoldSecondaryStructureScorer2 scorer2 = new RnacofoldSecondaryStructureScorer2();
	// scorer2.setInterStrandMode(false); // deactivate compute only intra-strand energies
	scorer2.setSelfEnergyCutoff(-5.0); // only penalize strands if very stable energies; previously: -10
	SecondaryStructureScorer scorer3 = new SimpleSecondaryStructurePredictorScorer();
	scorer2.setSubScorer(scorer3);
	scorer2.setSubScorerWeight(5.0);
	optimizer.setFinalScorer(scorer2);
	return optimizer;
    }

    /** Version of sequence optimization that was used for "5'th try" in October 2008.
     * Uses RNAcofold and simplefold */
    private MonteCarloSequenceOptimizer generateVersionOctober2008() {
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setErrorScoreLimit(60.0);
	optimizer.setIterMax(20000);
	optimizer.setIter2Max(40000);
	optimizer.setKt(1.0);
	optimizer.setKt2(0.4);
	optimizer.setRerun(5);
	optimizer.setRandomizeFlag(true); // no initial sequence randomization
	optimizer.setFinalScoreWeight(0.05); // higher than default weight of 0.1
	RnacofoldSecondaryStructureScorer scorer2 = new RnacofoldSecondaryStructureScorer();
	// scorer2.setInterStrandMode(false); // deactivate compute only intra-strand energies
	scorer2.setSelfEnergyCutoff(-10.0); // only penalize strands if very stable energies
	SecondaryStructureScorer scorer3 = new SimpleSecondaryStructurePredictorScorer();
	scorer2.setSubScorer(scorer3);
	scorer2.setSubScorerWeight(5.0);
	optimizer.setFinalScorer(scorer2);
	return optimizer;
    }

    /** Version of sequence optimization that is used in release version of sequence optimization.
     * Uses new version of matchfold */
    private MonteCarloSequenceOptimizer generateVersionJanuary2011() {
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setErrorScoreLimit(60.0);
	optimizer.setIterMax(20000);
	optimizer.setIter2Max(40000);
	optimizer.setKt(1.0);
	optimizer.setKt2(0.4);
	optimizer.setRerun(5);
	optimizer.setRandomizeFlag(true); // no initial sequence randomization
	optimizer.setFinalScoreWeight(1.0); // 0.05); // higher than default weight of 0.1
	SecondaryStructureScorer scorer2 = new MatchFoldProbScorer(); // new RnacofoldSecondaryStructureScorer();
	// scorer2.setInterStrandMode(false); // deactivate compute only intra-strand energies
	// scorer2.setSelfEnergyCutoff(-10.0); // only penalize strands if very stable energies
	// SecondaryStructureScorer scorer3 = new SimpleSecondaryStructurePredictorScorer();
	// scorer2.setSubScorer(scorer3);
	// scorer2.setSubScorerWeight(5.0);
	optimizer.setFinalScorer(scorer2);
	return optimizer;
    }

    private MonteCarloSequenceOptimizer generateVersionApril162009() {
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setErrorScoreLimit(146.0);
	optimizer.setIterMax(20000);
	optimizer.setIter2Max(1000);
	optimizer.setKt(1.0);
	optimizer.setKt2(0.4);
	optimizer.setRerun(5);
	optimizer.setRandomizeFlag(true); // initial sequence randomization
	// optimizer.setFinalScoreWeight(0.05); 
	optimizer.setFinalScoreWeight(1.0); 
	CritonScorer scorer = new CritonScorer();
	scorer.setConsecutiveLimit(3);
	scorer.setConsecutiveLimitG(2);
	scorer.setCritonLen(6);
	scorer.setAuViolationWeight(10.0);
	scorer.setGcViolationWeight(1.0); // notice: difference compared to testOptimizeCubeCritonLoop24HOpt42UunevenNoWave5
	scorer.setBranchMigrationWeight(10.0);
	scorer.setConsecutiveWeight(10.0);
	scorer.setGCMinFracWeight(0.0); // no minimum value for GC content
	scorer.setGuComplementMode(false);
	scorer.setZipperWeight(10.0);
	optimizer.setDefaultScorer(scorer);
	SecondaryStructureScorer scorer2 = new MatchFoldAllSecondaryStructureScorer();
	optimizer.setFinalScorer(scorer2);
	return optimizer;
    }

    private MonteCarloSequenceOptimizer generateRuleOnlyVersionMay202009() {
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setErrorScoreLimit(146.0);
	optimizer.setIterMax(20000);
	optimizer.setIter2Max(1000);
	optimizer.setKt(1.0);
	optimizer.setKt2(0.4);
	optimizer.setRerun(5);
	optimizer.setRandomizeFlag(true); // initial sequence randomization
	// optimizer.setFinalScoreWeight(0.05); 
	optimizer.setFinalScoreWeight(1.0); 
	CritonScorer scorer = new CritonScorer();
	scorer.setConsecutiveLimit(3);
	scorer.setConsecutiveLimitG(2);
	scorer.setCritonLen(6);
	scorer.setAuViolationWeight(10.0);
	scorer.setGcViolationWeight(1.0); // notice: difference compared to testOptimizeCubeCritonLoop24HOpt42UunevenNoWave5
	scorer.setBranchMigrationWeight(10.0);
	scorer.setConsecutiveWeight(10.0);
	scorer.setGCMinFracWeight(0.0); // no minimum value for GC content
	scorer.setGuComplementMode(false);
	scorer.setZipperWeight(10.0);
	optimizer.setDefaultScorer(scorer);
	SecondaryStructureScorer scorer2 = new DummyEnergySecondaryStructureScorer();
	optimizer.setFinalScorer(scorer2);
	return optimizer;
    }


    private MonteCarloSequenceOptimizer generateNoOptimizationVersion() {
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setErrorScoreLimit(100000.0);
	optimizer.setIterMax(1);
	optimizer.setIter2Max(1);
	optimizer.setKt(1.0);
	optimizer.setKt2(0.4);
	optimizer.setRerun(1);
	optimizer.setRandomizeFlag(true); // initial sequence randomization
	// optimizer.setFinalScoreWeight(0.05); 
	optimizer.setFinalScoreWeight(1.0); 
	SecondaryStructureScorer scorer = new DummyEnergySecondaryStructureScorer();
	optimizer.setDefaultScorer(scorer);
	SecondaryStructureScorer scorer2 = new DummyEnergySecondaryStructureScorer();
	optimizer.setFinalScorer(scorer2);
	return optimizer;
    }

    /** Version of sequence optimization that was used for "5'th try" in October 2008.
     * Uses RNAcofold and simplefold */
    private MonteCarloSequenceOptimizer generateVersionDecember2010() {
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setErrorScoreLimit(60.0);
	optimizer.setIterMax(20000);
	optimizer.setIter2Max(10);
	optimizer.setKt(1.0);
	optimizer.setKt2(0.4);
	optimizer.setRerun(1);
	optimizer.setRandomizeFlag(true); // no initial sequence randomization
        double finalScoreWeight = 0.05;
	optimizer.setFinalScoreWeight(finalScoreWeight); // higher than default weight of 0.1
	RnacofoldSecondaryStructureScorer scorer2 = new RnacofoldSecondaryStructureScorer();
	// scorer2.setInterStrandMode(false); // deactivate compute only intra-strand energies
	scorer2.setSelfEnergyCutoff(-10.0); // only penalize strands if very stable energies
	// SecondaryStructureScorer scorer3 = new MatchFoldSecondaryStructureScorer(); 
	((CritonScorer)(optimizer.getDefaultScorer())).setDefaultScorerWeight(5.0 * finalScoreWeight);
	optimizer.setFinalScorer(scorer2);
	return optimizer;
    }
    
}
