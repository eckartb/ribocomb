package secondarystructuredesign;

import java.io.*;
import java.util.*;
import java.util.logging.*;
import java.util.ResourceBundle;
import generaltools.StringTools;
import generaltools.ParsingException;
import generaltools.TestTools;
import numerictools.*;
import launchtools.*;
import rnasecondary.*;

import java.util.*;

import org.testng.annotations.*;

import static secondarystructuredesign.PackageConstants.*;

/** Assigns a score to a secondary structure and a set of potential sequences */
public class NupackStructureScorer implements SecondaryStructureScorer {

    private Logger log = Logger.getLogger(LOGFILE_DEFAULT);
    private static ResourceBundle rb = ResourceBundle.getBundle(BUNDLE_NAME);
    private static String complexesScriptName = rb.getString("nupackComplexes");
    private static String concentrationsScriptName = rb.getString("nupackConcentrations");
    private double energyAU = -0.8;
    private double energyGC = -1.2;

    private double[] initConcentrations;   // initial concentration of each of the strands for the NUPACK concentrations program
    public final double DEFAULT_CONC = Math.pow(10, -6);
    public final double CONC_WEIGHT = Math.pow(10, 6);   // Multiplied by the concentration of each strand so scores are not too low

    public final static int maxComplexSize = 3;
    public final static double cGap = 0.0;           // Desired concentration gap (in microM) between different tiers of complexes


    /** Dummy energies of Watson-Crick pairings: AU, GC, UA, CG (but not GU, UG) < 0, all others: 0.0 */
    public double interactionEnergy(char c1, char c2) {
	c1 = Character.toUpperCase(c1);
	c2 = Character.toUpperCase(c2);
	if (c1 == c2) {
	    return 0.0;
	}
	if (c1 > c2) {
	    return interactionEnergy(c2, c1);
	}
	switch (c1) {
	case 'A':
	    if (c2 == 'U') {
		return energyAU;
	    }
	case 'C':
	    if (c2 == 'G') {
		return energyGC;
	    }
	}
	return 0.0;
    }

    public void setInitConcentrations(double[] concentrations) {
	initConcentrations = new double[concentrations.length];
	for (int i = 0; i < concentrations.length; i++) {
	    initConcentrations[i] = concentrations[i];
	}
    }

    public void setDefaultConcentrations(int numStrands) {
	initConcentrations = new double[numStrands];
	for (int i = 0; i < numStrands; i++) {
	    initConcentrations[i] = DEFAULT_CONC;
	}
    }

    /** launches the NUPACK complexes program with given sequence, returns the raw line output from output.cx and output.ocx-mfe  */
    String[][] launchComplexes(String[] sequences, int maxComplexSize, String outputName) throws IOException {
	// write sequences to temporary file
	File tmpInputFile = File.createTempFile("input",".txt");
	String inputFileName = tmpInputFile.getAbsolutePath();
	FileOutputStream fos = new FileOutputStream(tmpInputFile);
	PrintStream ps = new PrintStream(fos);
	log.fine("Writing to file: " + inputFileName);
	ps.println(outputName);
	ps.println(sequences.length);
	for (int i = 0; i < sequences.length; i++) {
	    ps.println(sequences[i]);
	}
	ps.println(maxComplexSize);
	fos.close();

	// generate command string
	File tempFile = new File(complexesScriptName);
	String[] commandWords = {complexesScriptName, inputFileName, outputName};
	String[] outputFilenames = { outputName + ".cx", outputName + ".ocx-mfe" };
	String[][] rawOutput = runScript(commandWords, outputFilenames);
        if (tmpInputFile != null) {
            tmpInputFile.delete();
        }
        return rawOutput;
    }

    /** launches the NUPACK concentrations program using the results of complexes, returns the raw line output  */
    String[] launchConcentrations(String filePrefix, int numSequences, double[] concentrations) throws IOException {
        assert concentrations.length == numSequences;

	// write concentrations to file
	String inputFileName = filePrefix + ".con";
        File inputFile = new File(inputFileName);
        FileOutputStream fos = new FileOutputStream(inputFile);
        PrintStream ps = new PrintStream(fos);
        log.fine("Writing to file: " + inputFileName);
        for (int i = 0; i < numSequences; i++) {
            ps.println(concentrations[i]);
        }
        fos.close();

	// generate command string
        File tempFile = new File(concentrationsScriptName);
        String[] commandWords = {concentrationsScriptName, filePrefix};
        String[] outputFilenames = { filePrefix + ".eq" };
	String[][] rawOutput = runScript(commandWords, outputFilenames);
	if (inputFile != null) {
	    inputFile.delete();
	}
	return rawOutput[0];
    }


    /** Runs the specified script with the given command words and returns the raw line output */
    private String[][] runScript(String[] commandWords, String[] outputFilenames) throws IOException {
	// generate command
	RunCommand command = new SimpleRunCommand(commandWords);
	
	// create queue manager (singleton pattern)
	QueueManager queueManager = SimpleQueueManager.getInstance();
	// generate job
	Job job = queueManager.createJob(command);
	// launch command
	queueManager.submit(job);	    
	log.fine("queue manager finished job!");
	
	String[][] results = new String[outputFilenames.length][];
	for (int i = 0; i < outputFilenames.length; i++) {
	    // open output file:
	    log.fine("Reading results from " + outputFilenames[i]);
	    String[] resultLines = null;
	    FileInputStream resultFile = null;
	    try {
		 resultFile = new FileInputStream(outputFilenames[i]);
		 resultLines = StringTools.readAllLines(resultFile);
	    }
	    catch (IOException ioe) {
		log.warning("Error when scraping result file from: " + outputFilenames[i]);
		 throw ioe;
	     }
	     finally {
		 if (resultFile != null) {
		     resultFile.close();
		 }
	     }
	     if (resultLines == null) {
		 log.warning("Results were null!");
	     }
	     results[i] = resultLines;
	}
	return results;
    }

    /** Returns true if the complex is in the first tier
	i.e. if the complex is of the form: (1,1,1,0,0,0,...)
    */
    private boolean firstTier(int[] complex) {
	return (complex[0] == 1 && complex[1] == 1 && complex[2] == 1);
    }

    /** Returns true if the complex is in the second tier
	i.e. if the complex is of the form: (1,1,0,0,0,0,...)
    */
    private boolean secondTier(int[] complex) {
	for (int i = 0; i < complex.length; i++) {
	    if (i < 2 && complex[i] != 1) {
		return false;
	    }
	    else if (i >= 2 && complex[i] != 0) {
		return false;
	    }
	}
	return true;
    }

    /** Returns true if the complex is in the third tier
	i.e. if the complex is of the form: (0,...0,1,1,1,0,...)
	                           but not: (1,0,0,....,0,0,1,1)
    */
    private boolean thirdTier(int[] complex) {
	int first1Index = 0;   // The index of the first 1 in the complex
	if (complex[0] != 0) return false;
	for (int i = 1; i < complex.length-2; i++) {
	    if (complex[i] == 1 && complex[i-1] == 0) {
		first1Index = i;
		break;
	    }
	}
	return (complex[first1Index+1] == 1 && complex[first1Index+2] == 1);
    }

    /** Returns a score of 0 if greaterConc > smallerConc + cGap
	Otherwise returns cGap + smallerConc - greaterConc
    */
    private double compareComplexes(double greaterConc, double smallerConc)
    {
	if (greaterConc > (smallerConc + cGap)) {
	    return 0;
	}
	else {
	    return cGap + smallerConc - greaterConc;
	}
    }

    /** 
     *  Check the structures of every complex composed of 2 different strands
     *  Penalize the score if the structure does not match the expected structure
     */
    private double scoreMFEStructures(String[] mfeStructures, int[][][] interactionMatrices)
    {
	double result = 0.0;
	for (int i = 0; i < mfeStructures.length; i++) {
	    String bracket = mfeStructures[i];
	    int[][] predictedInteractions = RnaFoldTools.parseRnacofoldBracketInteractions(bracket);
	    result += RnaFoldTools.computeMatrixDifference(interactionMatrices[i], predictedInteractions) / 2;
	}
	return result / mfeStructures.length;
    }

    @SuppressWarnings(value="unchecked")
    private double scoreComplexConcentrations(String[] unorderedOutput, String[] orderedOutput) {
	double score = 0.0;
	ArrayList[] tiers = new ArrayList[4];   // Complexes are ranked by their importance
	                                        // tiers w/ smaller indexes should have greater concentrations than larger indexes
	for (int i = 0; i < tiers.length; i++) {
	    tiers[i] = new ArrayList();
	}
	for (int i = 0; i < unorderedOutput.length; i++) {
	    int[] complex = NupackTools.findComplex(unorderedOutput, i+1);
	    if (firstTier(complex)) {
		tiers[0].add(complex);
	    }
	    else if (secondTier(complex)) {
		tiers[1].add(complex);
	    }
	    else if (thirdTier(complex)) {
		tiers[2].add(complex);
	    }
	    else {
		tiers[3].add(complex);
	    }
	}
	for (int i = 0; i < tiers.length-1; i++) {
	    Iterator itr1 = tiers[i].iterator();
	    while(itr1.hasNext()) {
		int[] greater = (int[])itr1.next();
		double greaterEn = NupackTools.findConcentration(greater, orderedOutput, unorderedOutput) * CONC_WEIGHT;
		Iterator itr2 = tiers[i+1].iterator();
		while (itr2.hasNext()) {
		    int[] smaller = (int[])itr2.next();
		    double smallerEn = NupackTools.findConcentration(smaller, orderedOutput, unorderedOutput) * CONC_WEIGHT;
		    score += compareComplexes(greaterEn, smallerEn);
		}
	    }
	}
	return score;
    }


    /** returns error score for complete secondary structure and trial sequences */
    public Properties generateReport(StringBuffer[] bseqs,
				     SecondaryStructure structure,
				     int[][][][] interactionMatrices) {
	assert false; // TODO
	return null;
    }

    /** returns error score for complete secondary structure and trial sequences */
    public double scoreStructure(StringBuffer[] bseqs,
				 SecondaryStructure structure,
				 int[][][][] interactionMatrices) {
	double score = 0.0;

	int numStrands = bseqs.length;
	String outputName = "output";
	String[] seqs = new String[numStrands];
	for (int i = 0; i < bseqs.length; i++) {
	    seqs[i] = bseqs[i].toString();
	}
	try {
	    String[][] complexesOutput = launchComplexes(seqs, maxComplexSize, outputName);

	    String[] unorderedRawOutput = complexesOutput[0];
	    if (initConcentrations == null) {
		setDefaultConcentrations(numStrands);
	    }
	    String[] orderedRawOutput = launchConcentrations(outputName, numStrands, initConcentrations);
	    String[] unorderedOutput = NupackTools.findRelevantData(unorderedRawOutput);
	    String[] orderedOutput = NupackTools.findRelevantData(orderedRawOutput);
	    score += scoreComplexConcentrations(unorderedOutput, orderedOutput);

	    System.out.println("Score from complexes = " + score);

	    String[] mfeRawOutput = complexesOutput[1];
	    int numDimers = numStrands * (numStrands-1) / 2;
	    String[] mfeStructures = new String[numDimers];
	    int[][][] matrices = new int[numDimers][][];
	    int[] currComplex = new int[numStrands];
	    for (int i = 0; i < numStrands; i++) {
		currComplex[i] = 0;
	    }
	    // x and y coordinates of the two strands in currComplex
	    int x = 0;
	    int y = 0;

	    for (int i = 0; i < numDimers; i++) {
		// Update to next complex
		currComplex[x] = 0;
		currComplex[y] = 0;
		y++;
		if (y >= numStrands) {
		    x++;
		    y = x+1;
		}
		currComplex[x] = 1;
		currComplex[y] = 1;

		matrices[i] = interactionMatrices[x][y];
		int index = NupackTools.findIndex(currComplex, orderedOutput);
		String struct = NupackTools.parseNupackMFEStructure(mfeRawOutput, index);
		mfeStructures[i] = struct;
	    }
	    double subscore = scoreMFEStructures(mfeStructures, matrices);
	    System.out.println("Score from MFE structure = " + subscore);
	    score += subscore;
	    System.out.println("Total score = " + score);
	}
	catch (java.io.IOException ioe) {
	    log.severe("Error launching NUPACK! Error message: " + ioe.getMessage());
	    return 0.0;
	}

	return score;
    }

    /** Tests the method scoreStructure (the main method of this class)  */
    @Test(groups={"new"})
    public void testScoreStructure() throws IOException, ParsingException {
	String methodName = "testScoreStructure";
	System.out.println(TestTools.generateMethodHeader(methodName));
	// Test sequences
	StringBuffer seq1 = new StringBuffer("ggccggaaaaaaaauu");
	StringBuffer seq2 = new StringBuffer("uuuuaauuuuaaaaaa");
	StringBuffer seq3 = new StringBuffer("uuuuuuaaaauuccuu");
	StringBuffer seq4 = new StringBuffer("aaggaauuuuccggcc");

	int[][][][] interactions = new int[4][4][16][16];
        for (int i = 0; i < interactions.length; i++) {
            for (int j = 0; j < interactions[0].length; j++) {
                interactions[i][j] = initializeblank(16,16);
            }
        }
        for (int i = 0; i < 6; i++) {
            interactions[0][1][10+i][i] = 1;
            interactions[1][0][i][10+i] = 1;
            interactions[1][2][10+i][i] = 1;
            interactions[2][1][i][10+i] = 1;
            interactions[2][3][10+i][i] = 1;
            interactions[3][2][i][10+i] = 1;
            interactions[0][3][i][10+i] = 1;
            interactions[3][0][10+i][i] = 1;
        }

	StringBuffer[] strands = { seq1, seq2, seq3, seq4 };
	double score = scoreStructure(strands, null, interactions);
	System.out.println("Score = " + score);
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Auxiliary method used by testScoreStructure
        Initializes an interaction matrix showing no interactions (all values set to -1)
    */
    private int[][] initializeblank(int width, int height) {
        int[][] inter = new int[width][height];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                inter[i][j] = -1;
            }
        }
        return inter;
    }
}
