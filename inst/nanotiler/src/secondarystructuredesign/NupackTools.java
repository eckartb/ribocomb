package secondarystructuredesign; 

import java.io.*;
import java.util.logging.*;
import java.util.ResourceBundle;
import generaltools.StringTools;
import generaltools.ParsingException;
import generaltools.TestTools;
import numerictools.*;
import launchtools.*;
import rnasecondary.*;
import static secondarystructuredesign.PackageConstants.*;

import java.util.ArrayList;

import org.testng.annotations.*;

public class NupackTools {

    private static Logger log = Logger.getLogger(LOGFILE_DEFAULT);
    private static ResourceBundle rb = ResourceBundle.getBundle(BUNDLE_NAME);

    /** Removes all lines that begin with '%', leaving only the relevant lines containing data behind */
    public static String[] findRelevantData(String[] lines) {
	int index = -1;
	for (int i = 0; i < lines.length; i++) {
	    String currLine = lines[i];
	    if (currLine.charAt(0) != '%') {
		index = i;
		break;
	    }
	}
	assert (index >= 0 && index < lines.length);
	if (index == 0) return lines;
	
	String[] data = new String[lines.length - index];
	for (int i = index; i < lines.length; i++) {
	    data[i - index] = lines[i];
	}
	return data;
    }

    /** Finds the index assigned by nupack to the complex made up of the given # of each strand */
    public static int findIndex(int[] complex, String[] data) {
	for (int i = 0; i < data.length; i++) {
	    String[] line = parseLine(data[i]);
	    boolean matches = true;
	    for (int n = 0; n < complex.length; n++) {
		try {
		    if (Integer.parseInt(line[n+1]) != complex[n]) {
			matches = false;
		    }
		}
		catch (NumberFormatException nfe) {
		    System.out.println("Number format exception in line: " + data[i]);
		    System.out.println(nfe.getMessage());
		    System.out.println("Complex:");
		    for (int j = 0; j < complex.length; ++j) {
			System.out.print(" " + complex[j]);
		    }
		    System.out.println();

		    assert false;
		}
	    }
	    if (matches) return Integer.parseInt(line[0]);
	}
	return -1;
    }

    /** Finds the strands making up the complex for the given index in the data
	Data should be ordered by index
    */
    public static int[] findComplex(String[] data, int index) {
	String[] line = parseLine(data[index-1]);
	int[] complex = new int[line.length - 2];
	for (int i = 1; i < line.length-1; i++) {
	    complex[i-1] = Integer.parseInt(line[i]);
	}
	return complex;
    }

    /** Finds the free energy of the complex made up of the given # of each strand
	orderedData should be ranked from greatest to least concentration
	unorderedData should be ranked in order of index
    */
    public static double findEnergy(int[] complex, String[] orderedData, String[] unorderedData) {
	int index = findIndex(complex, orderedData);
	return findEnergy(index, unorderedData);
    }

    /** Finds the free energy of the complex with the given index 
	Data should be ranked in order of index      */
    private static double findEnergy(int index, String[] data) {
	assert (index > 0 && index <= data.length);
	String[] line = parseLine(data[index-1]);
	String conc = line[line.length-1];
	return parseSciNot(conc);
    }

    /** Finds the equilibrium concentration of the complex made up of the given # of each strand
        orderedData should be ranked from greatest to least concentration
        unorderedData should be ranked in order of index
    */
    public static double findConcentration(int[] complex, String[] orderedData, String[] unorderedData) {
	int index = findIndex(complex, orderedData);
        return findConcentration(index, orderedData);
    }

    /** Finds the equilibrium concentration of the complex with the given index 
	Data should NOT be ranked in order of index     */
    private static double findConcentration(int index, String[] data) {
        assert (index > 0 && index <= data.length);
	for (int i = 0; i < data.length; i++) {
	    String[] line = parseLine(data[i]);
	    if (Integer.parseInt(line[0]) == index) {
		String concentration = line[line.length-1];
		return parseSciNot(concentration);
	    }
	}
	return 0;
    }
    
    /** Splits a line of output from NUPACK into a number of short segments by splitting around tabs  */
    private static String[] parseLine(String line) {
	return line.split("\t");
    }

    /** Returns the actual value of the string s in scientific notation (e.g. in the form 6.023e23)  */
    private static double parseSciNot(String s) {
	String[] line = s.split("e");
	if (line.length == 1) {    // Not in scientific notation!
	    return Double.parseDouble(s);
	}
	else {
	    double prefix = Double.parseDouble(line[0]);
	    if (line[1].startsWith("+")) {
		line[1] = line[1].substring(1,line[1].length());
	    }
	    int exp = Integer.parseInt(line[1]);
	    return prefix * Math.pow(10.0, exp);
	}
    }

    /** Returns the string reprenting the mfe structure of the complex with the given index.
     *  Default complex order is 1  */
    public static String parseNupackMFEStructure(String[] nupackOutput, int index) {
	return parseNupackMFEStructure(nupackOutput, index, 1);
    }

    /** Returns the string representing the mfe structure of the complex with the given index and order  */
    public static String parseNupackMFEStructure(String[] nupackOutput, int index, int order) {
	String line = "% complex" + index + "-order" + order;
	String structure = null;
	for (int i = 0; i < nupackOutput.length; i++) {
	    nupackOutput[i].trim();
	    if (nupackOutput[i].equals(line)) {
		structure = nupackOutput[i+3];
		structure = structure.replace('+','&');
		break;
	    }
	}
	if (structure == null) System.out.println("Correct line not found");
	return structure;
    }
}
