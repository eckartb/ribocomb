package secondarystructuredesign;

import java.io.File;

public class PackageConstants {

    public static final String NANOTILER_HOME_VAR = "NANOTILER_HOME";

    public static final String NANOTILER_HOME = System.getenv(NANOTILER_HOME_VAR);

    public static final String PDB_CHAIN_CHAR = "pdb_chain_char";

    public static final String LOGFILE_DEFAULT = "NanoTiler_debug";
    
    /** default name of properties file */
    public static final String BUNDLE_NAME = "SecondaryStructureDesign";
    
    public static final String NEWLINE = System.getProperty("line.separator");

    public static final String ENDL = NEWLINE;

    public static String SLASH = File.separator; // either "/" or "\"  depending on Unix of Windows

}
