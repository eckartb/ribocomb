package secondarystructuredesign;

import rnasecondary.*;
import sequence.*;
import java.util.*;
import java.util.regex.*;
import java.io.*;
import java.text.ParseException;
import java.util.logging.*;
import numerictools.IntervalInt;
import generaltools.StringTools;
import org.testng.annotations.*;
import java.util.ResourceBundle;
import generaltools.TestTools;
import org.testng.annotations.*;

import static secondarystructuredesign.PackageConstants.*;

/** parses structure string in bracket notation, adds found "(" and ")" or "{}" or "[]" parenthesis to interaction set */
public class PknotsRGParser implements SecondaryStructureParser {

    public int debugLevel = 0;

    private Logger log = Logger.getLogger("mcsopt");

    public PknotsRGParser() {
	log.finest("starting ImprovedSecondaryStructureParser");
    }

    /** If n is opening character (line opening bracket) of innermost interaction,
     * return index of corresponding closing character. Return -1 otherwise.
     */
    private int findInnerInteraction(String s, int n, char openChar, char closeChar) {
	if (s.charAt(n) != openChar) {
	    return -1;
	}
	for (int i = n+1; i < s.length(); ++i) {
	    if (s.charAt(i) == closeChar) {
		return i;
	    }
	    else if (s.charAt(i) == openChar) {
		return -1; // is not inner interaction
	    }
	}
	return -1; // no closing bracket found
    }
    
    private IntervalInt findInnerInteraction(String s, char openChar, char closeChar) {
	for (int i = 0; i < s.length(); ++i) {
	    int j = findInnerInteraction(s, i, openChar, closeChar);
	    if (j >= 0) {
		return new IntervalInt(i,j);
	    }
	}
	return null;
    }

    /** parses structure string in bracket notation, adds found "(" and ")" parenthesis to interaction set */
    int addInteractions(String structure, Sequence seq, InteractionSet interactions, char openChar, char closeChar) {
	int numInteractions = StringTools.countChar(structure, openChar);
	StringBuffer buf = new StringBuffer(structure);
	int foundInteractions = 0;
	while (foundInteractions < numInteractions) {
	    IntervalInt intervall = findInnerInteraction(buf.toString(), openChar, closeChar);
	    if (intervall == null) {
		log.info("Could not find inner interactions in: " + structure);
		break; // could not find more interactions
	    }
	    int j = intervall.getLower();
	    int k = intervall.getUpper();
	    if (debugLevel > 1) {
		System.out.println("Found interaction");
		System.out.println("Structure: " + structure);
		System.out.println("Interaction: " + j + "   " + k);
	    }
	    // found interaction
	    interactions.add(new SimpleInteraction(seq.getResidue(j), seq.getResidue(k), 
						   new RnaInteractionType(RnaInteractionType.WATSON_CRICK)));
	    buf.setCharAt(j, '.');
	    buf.setCharAt(k, '.'); // delete 
	    foundInteractions++;
	}
	for (int i = 0; i < structure.length(); ++i) {
	    if (structure.charAt(i) == '?') {
		log.info("Setting residue " + (i+1) + " to ignore status!");
		seq.getResidue(i).setProperty("seqstatus", "ignore");
	    }
	}
	assert foundInteractions == numInteractions;
	return foundInteractions;
    }

    /** Parses secondary structure from given file name. */
    public MutableSecondaryStructure parse(String filename) throws IOException, ParseException {
	FileInputStream fis = new FileInputStream(filename);
	String[] lines = StringTools.readAllLines(fis);
	return parse(lines);
    }

    /** Generates secondary structure from set of lines */
    public MutableSecondaryStructure parse(String[] lines) throws ParseException {
	try {
	    UnevenAlignment sequences = new SimpleUnevenAlignment();
	    InteractionSet interactions = new SimpleInteractionSet();
	    ArrayList<String> structures = new ArrayList<String>();
	    String name = null;
	    int step = 1;
	    double weight = 1.0;
	    for (int i = 0; i < lines.length; ++i) {
		String line = lines[i];
		if (debugLevel > 1) {
		    log.info("Parsing line " + (i+1) + " : " + line);
		}
		if (step == 1) {
		    if (line.startsWith(">")) {
			if (line.length() < 2) {
			    throw new ParseException("No name specified in line " + line,i+1);
			}
			if (name != null) {
			    throw new ParseException("Name already defined: " + line, i+1);
			}
			name = line.substring(1,line.length());
			continue;
		    }
		    String seqData = line;
		    List<Integer> lowCharIndices = new ArrayList<Integer>();
		    for (int ii = 0; ii < seqData.length(); ++ii) {
			if (Character.isLowerCase(seqData.charAt(ii))) {
			    lowCharIndices.add(ii); // autoboxing!
			}
		    }
		    log.fine("Using alphabet: " + DnaTools.AMBIGUOUS_RNA_ALPHABET);
		    Sequence sequence = new SimpleMutableSequence(seqData.toUpperCase(), name, DnaTools.AMBIGUOUS_RNA_ALPHABET);
		    if (weight != 1.0) {
			sequence.setWeight(weight);
		    }
		    for (int ii = 0; ii < lowCharIndices.size(); ++ii) {
			int idx = lowCharIndices.get(ii); // autoboxing !
			sequence.getResidue(idx).setProperty(SequenceStatus.name,  SequenceStatus.fragment); // set to "fragment" status: keep constant in optimization
		    }
		    if (debugLevel > 1) {
			log.info("Adding sequence " + sequence.toString());
		    }
		    sequences.addSequence(sequence);
		    ++step;
		}
		else { // read structure
		    // allow A-Z,a-z,()[]{}
		    // Pattern p = Pattern.compile("([A-Za-z\\?\\.\\(\\)\\-\\{\\}\\[\\]]+)");
		    // Matcher m = p.matcher(line);
		    String[] words = line.split(" ");
		    if(words.length > 1) {
			structures.add(words[0]);
			if (debugLevel > 1) {
			    log.info("Adding structure " + words[0]);
			}
			// step = 0;
			break; // only one structure can be read
		}
		    else {
			log.info("Expecting structure. Ignoring line : " + line);
		    }
		}
	    }
	    assert structures.size() == 1;
	    // handle secondary structure
	    String structure = structures.get(0);
	    if (debugLevel > 0) {
		log.info("Adding intra-sequence interactions for structure: " + structure);
	    }
	    addInteractions(structure, sequences.getSequence(0), interactions, '(', ')');
	    addInteractions(structure, sequences.getSequence(0), interactions, '{', '}');
	    addInteractions(structure, sequences.getSequence(0), interactions, '[', ']');
	    
	    return new SimpleMutableSecondaryStructure(sequences, interactions);
	} catch (DuplicateNameException e1) {
	    System.out.println("DuplicateNameException: " + e1.getMessage());
	} catch (UnknownSymbolException e2) {
	    System.out.println("UnknownSymbolException: " + e2.getMessage());
	}
	return null;
    }

    private void parseSequences(String[] lines ) {
	
    }

    private void parseInteractions(String[] lines) {


    }

    @Test(groups={"new", "slow"})
    public void testParse() {
	ResourceBundle rb = ResourceBundle.getBundle("SecondaryStructureDesignTest");
	String fileNames = rb.getString("testPknotsRGSecondaryStructures");
	String[] tokens = fileNames.split(",");
	SecondaryStructureParser parser = new PknotsRGParser();
	for (int i = 0; i < tokens.length; ++i) {
	    MutableSecondaryStructure structure = null;
	    String fileName = tokens[i];
	    if (fileName.length() < 2) {
		continue;
	    }
	    else {
		System.out.println("Reading file: " + fileName);
	    }
	    try {
		structure = parser.parse(tokens[i]);
	    }
	    catch (IOException ioe) {
		System.out.println(ioe.getMessage());
		assert false;
	    }
	    catch (ParseException pe) {
		System.out.println(pe.getMessage());
		assert false;
	    }
	    assert structure != null;
	    SecondaryStructureWriter writer = new SecondaryStructureScriptFormatWriter();
	    System.out.println("Read secondary structure: " + writer.writeString(structure));

	}
    }

    @Test(groups={"new", "slow"})
    public void testParseDummyPK() {
	String methodName = "testParseDummyPK";
	System.out.println(TestTools.generateMethodHeader(methodName));
	SecondaryStructureParser parser = new PknotsRGParser();
	MutableSecondaryStructure structure = null;
	String fileName = NANOTILER_HOME + "/test/fixtures/dummypk_pknotsRG.txt";
	System.out.println("Reading file: " + fileName);
	try {
	    structure = parser.parse(fileName);
	}
	catch (IOException ioe) {
	    System.out.println(ioe.getMessage());
	    assert false;
	}
	catch (ParseException pe) {
	    System.out.println(pe.getMessage());
	    assert false;
	}
	assert structure != null;
	SecondaryStructureWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read secondary structure: " + writer.writeString(structure));
	assert structure.getInteractionCount() == 37; // desired number of base pairs
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

}

