package secondarystructuredesign;

import java.io.*;
import java.util.Properties;
import java.util.logging.*;
import java.util.ResourceBundle;
import generaltools.StringTools;
import generaltools.ParsingException;
import graphtools.*;
import numerictools.*;
import launchtools.*;
import rnasecondary.*;
import static secondarystructuredesign.PackageConstants.*;

/** Assigns a score to a secondary structure and a set of potential sequences */
public class RiboswitchSecondaryStructureScorer implements SecondaryStructureScorer {

    private Logger log = Logger.getLogger(LOGFILE_DEFAULT);
    private static ResourceBundle rb = ResourceBundle.getBundle(BUNDLE_NAME);
    private static String binDir = NANOTILER_HOME + SLASH + "bin";
    private static String rnacofoldScriptName = binDir + SLASH + rb.getString("rnacofoldscript");
    private static String rnacofoldPScriptName = binDir + SLASH + rb.getString("rnacofoldpscript");
    private static String rnaFoldScriptName = binDir + SLASH + rb.getString("rnafoldscript");
    private double energyAU = -0.8;
    private double energyGC = -1.2;
    private boolean probabilityMode = true; // Use probability matrices. compatibility with previous results: false;
    private double trueProbWeight = 1.0;
    private double falseProbWeight = 1.0;
    private String linkerSequence = "&";
    private String linkerSequence2 = "UUUUU";
    public static char UNKNOWN_INTERACTION_CHAR = '?'; // can be paired or unpaired

    public String getLinkerSequence() { return this.linkerSequence; }

    public void setLinkerSequence(String s) { this.linkerSequence = s; }

    public void setProbabilityMode(boolean mode) { this.probabilityMode = mode; }

    /** Dummy energies of Watson-Crick pairings: AU, GC, UA, CG (but not GU, UG) < 0, all others: 0.0 */
    public double interactionEnergy(char c1, char c2) {
	c1 = Character.toUpperCase(c1);
	c2 = Character.toUpperCase(c2);
	if (c1 == c2) {
	    return 0.0;
	}
	if (c1 > c2) {
	    return interactionEnergy(c2, c1);
	}
	switch (c1) {
	case 'A':
	    if (c2 == 'U') {
		return energyAU;
	    }
	case 'C':
	    if (c2 == 'G') {
		return energyGC;
	    }
	}
	return 0.0;
    }

    /** launches RNAfold with given sequence, returns the raw RNAfold line output  */
    String[] launchRnacofold(String sequence) throws IOException {
	// write secondary structure to temporary file
	File tmpInputFile = File.createTempFile("nanotiler_rnacofold",".sec");
	tmpInputFile.deleteOnExit();
	String inputFileName = tmpInputFile.getAbsolutePath();
	FileOutputStream fos = new FileOutputStream(tmpInputFile);
	PrintStream ps = new PrintStream(fos);
	// write secondary structure, but only write sec structure, not sequence:
	SecondaryStructureWriter secWriter = new SecondaryStructureScriptFormatWriter();
	log.fine("Writing to file: " + inputFileName);
	log.fine("Writing content: " + sequence);
	ps.println(sequence); // no special formatting needed!
	fos.close();
	File tmpOutputFile = File.createTempFile("nanotiler_rnacofold", ".seq");
	tmpOutputFile.deleteOnExit();
	final String outputFileName = tmpOutputFile.getAbsolutePath();
	if (tmpOutputFile.exists()) {
	    tmpOutputFile.delete();
	}

	// generate command string
	// File tempFile = new File(rnaFoldScriptName);
	// log.info("Path to RNAinverse script: " + tempFile.getAbsolutePath());
	String[] commandWords = {rnacofoldScriptName, inputFileName, outputFileName}; 
	// generate command
	RunCommand command = new SimpleRunCommand(commandWords);
	
	// create queue manager (singleton pattern)
	QueueManager queueManager = SimpleQueueManager.getInstance();
	// generate job
	Job job = queueManager.createJob(command);
	
	// add listener to job !?
	
	// launch command
	queueManager.submit(job);	    
	log.fine("queue manager finished job!");
	// open output file:
	log.fine("Importing optimized sequences from " + outputFileName);
	String[] resultLines = null;
	try {
	    FileInputStream resultFile = new FileInputStream(outputFileName);
	    resultLines = StringTools.readAllLines(resultFile);
	    // sequenceStrings = RnaFoldTools.getSequenceStrings(resultLines);
	}
	catch (IOException ioe) {
	    log.warning("Error when scraping result file from: " 
			+ outputFileName);
	    throw ioe;
	}
	finally {
	    if (tmpInputFile.exists()) {
		tmpInputFile.delete();
	    }
	    if (tmpOutputFile.exists()) {
		tmpOutputFile.delete();
	    }
	}
	return resultLines;
    }

    /** Computes score how well a probability matrix fits a combined interaction matrix. Score zero for perfect agreement.
     * @param selfMode if true, score interactions within one sequence. If false, score interactions between two sequences. */
    private double scoreProbabilityMatrix(double[][] probMatrix, int[][] interactions) {
	assert probMatrix != null && probMatrix[0] != null;
	assert interactions != null && interactions[0] != null;
	assert probMatrix.length == interactions.length;
	assert probMatrix[0].length == interactions[0].length;
	double score = 0.0;
	for (int i = 0; i < interactions.length; ++i) {
	    for (int j = i + 1; j < interactions[i].length; ++j) {		
		if (interactions[i][j] == RnaInteractionType.WATSON_CRICK) {
		    score += trueProbWeight * (1.0-probMatrix[i][j]); // the smaller the score the "better"
		}
		else if (interactions[i][j] == RnaInteractionType.NO_INTERACTION) {
		    score += falseProbWeight * probMatrix[i][j];
		}
	    }
	}
	return score;
    }

    /** launches RNAfold with given sequence, returns the raw RNAfold line output
     * @param orderFlag if true, RNAcofold was launched by concatenating sequences A+B, otherwise B+A
     */
//     private double scoreRnacofold(double[][] probMatrixA, double[][] probMatrixB, double[][] probMatrixAB,
// 				   int[][] interactionsA, int[][] interactionsB, int[][] interactionsAB, boolean orderFlag) throws IOException {
// 	return scoreProbabilityMatrix(probMatrixA, interactionsA, true, true) + scoreProbabilityMatrix(probMatrixB, interactionsB, true, true)
//          	    + scoreProbabilityMatrix(probMatrixAB, interactionsAB, orderFlag, false);
//     }

    /** launches RNAfold with given sequence, returns the raw RNAfold line output  */
    private double[][] launchRnacofoldProb(String sequence) throws IOException {
	assert probabilityMode;
	// write secondary structure to temporary file
	File tmpInputFile = File.createTempFile("nanotiler_rnacofold",".sec");
	tmpInputFile.deleteOnExit();
	String inputFileName = tmpInputFile.getAbsolutePath();
	FileOutputStream fos = new FileOutputStream(tmpInputFile);
	PrintStream ps = new PrintStream(fos);
	// write secondary structure, but only write sec structure, not sequence:
	SecondaryStructureWriter secWriter = new SecondaryStructureScriptFormatWriter();
	log.fine("Writing to file: " + inputFileName);
	log.fine("Writing content: " + sequence);
	ps.println(sequence); // no special formatting needed!
	fos.close();
	File tmpOutputFile = null;
	if (probabilityMode) {
	    tmpOutputFile = File.createTempFile("nanotiler_rnacofold", ".ps");
	}
	else {
	    tmpOutputFile = File.createTempFile("nanotiler_rnacofold", ".seq");
	}
	tmpOutputFile.deleteOnExit();
	final String outputFileName = tmpOutputFile.getAbsolutePath();
	if (tmpOutputFile.exists()) {
	    tmpOutputFile.delete();
	}
	// generate command string
	// File tempFile = new File(rnaFoldScriptName);
	// log.info("Path to RNAinverse script: " + tempFile.getAbsolutePath());
	String[] commandWords = {rnacofoldPScriptName, inputFileName, outputFileName}; 
	// generate command:
	log.fine("Launching command: " + StringTools.paste(commandWords, " "));
	RunCommand command = new SimpleRunCommand(commandWords);	
	// create queue manager (singleton pattern)
	QueueManager queueManager = SimpleQueueManager.getInstance();
	// generate job
	Job job = queueManager.createJob(command);
	// add listener to job !?	
	// launch command
	queueManager.submit(job);	    
	log.fine("queue manager finished job!");
	// open output file:
	log.fine("Importing optimized sequences from " + outputFileName);
	String[] resultLines = null;
	FileInputStream resultFile = null;
	try {
	    resultFile = new FileInputStream(outputFileName);
	    resultLines = StringTools.readAllLines(resultFile);
	    // sequenceStrings = RnaFoldTools.getSequenceStrings(resultLines);
	}
	catch (IOException ioe) {
	    log.warning("Error when scraping result file from: " 
			+ outputFileName);
	    throw ioe;
	}
	finally {
	    if (resultFile != null) {
		resultFile.close();
	    }
	    if (tmpInputFile.exists()) {
		tmpInputFile.delete();
	    }
	    if (tmpOutputFile.exists()) {
		tmpOutputFile.delete();
	    }
	}
	// read probability matrices
	// ...
	double[][] result = null;
	try {
	    result = RnaFoldTools.parseRnafoldProbabilities(resultLines);
	}
	catch (ParsingException pe) {
	    throw new IOException("Parsing exception in parsing RNAcofold result postscript file: " + pe.getMessage());
	}
	finally {
	    if (tmpOutputFile.exists()) {
		assert false; // should already be deleted earlier
		tmpOutputFile.delete();
	    }
	}
	return result;
    }

    /** launches RNAfold with given sequence, returns the raw RNAfold line output  */
    String[] launchRnafold(String sequence) throws IOException {
	// write secondary structure to temporary file
	File tmpInputFile = File.createTempFile("nanotiler_rnafold",".sec");
	tmpInputFile.deleteOnExit();
	String inputFileName = tmpInputFile.getAbsolutePath();
	FileOutputStream fos = new FileOutputStream(tmpInputFile);
	PrintStream ps = new PrintStream(fos);
	// write secondary structure, but only write sec structure, not sequence:
	SecondaryStructureWriter secWriter = new SecondaryStructureScriptFormatWriter();
	log.fine("Writing to file: " + inputFileName);
	log.fine("Writing content: " + sequence);
	ps.println(sequence); // no special formatting needed!
	fos.close();
	File tmpOutputFile = File.createTempFile("nanotiler_rnafold", ".seq");
	tmpOutputFile.deleteOnExit();
	final String outputFileName = tmpOutputFile.getAbsolutePath();
	if (tmpOutputFile.exists()) {
	    tmpOutputFile.delete();
	}

	// generate command string
	File tempFile = new File(rnaFoldScriptName);
	// log.info("Path to RNAinverse script: " + tempFile.getAbsolutePath());
	String[] commandWords = {rnaFoldScriptName, inputFileName, outputFileName}; 
	// generate command
	RunCommand command = new SimpleRunCommand(commandWords);
	
	// create queue manager (singleton pattern)
	QueueManager queueManager = SimpleQueueManager.getInstance();
	// generate job
	Job job = queueManager.createJob(command);
	
	// add listener to job !?
	
	// launch command
	queueManager.submit(job);	    
	log.fine("queue manager finished job!");
	// open output file:
	log.fine("Importing optimized sequences from " + outputFileName);
	String[] resultLines = null;
	FileInputStream resultFile = null;
	try {
	    resultFile = new FileInputStream(outputFileName);
	    resultLines = StringTools.readAllLines(resultFile);
	    // sequenceStrings = RnaFoldTools.getSequenceStrings(resultLines);
	}
	catch (IOException ioe) {
	    log.warning("Error when scraping result file from: " 
			+ outputFileName);
	    throw ioe;
	}
	finally {
	    if (resultFile != null) {
		resultFile.close();
	    }
	    if (tmpOutputFile.exists()) {
		tmpOutputFile.delete();
	    }
	    if (tmpInputFile.exists()) {
		tmpInputFile.delete();
	    }
	}
	return resultLines;
    }

    private double computeMatrixMatthews(int[][] trueInteractions,
					 int[][] predictedInteractions) {
	assert trueInteractions.length == predictedInteractions.length;
	assert trueInteractions[0].length == predictedInteractions[0].length;
	int tp = 0;
	int fp = 0;
	int tn = 0;
	int fn = 0;
	for (int i = 0; i < trueInteractions.length; ++i) {
	    for (int j = i+1; j < trueInteractions[0].length; ++j) {
		int pv = predictedInteractions[i][j];
		int tv = trueInteractions[i][j];
		switch (tv) {
		case RnaInteractionType.WATSON_CRICK:
		    if (pv == RnaInteractionType.WATSON_CRICK) {
			++tp;
		    }
		    else {
			++fn; // missed interaction
		    }
		    break;
		case RnaInteractionType.NO_INTERACTION:
		    if (pv == RnaInteractionType.WATSON_CRICK) {
			++fp;
		    }
		    else {
			++tn; // missed interaction
		    }
		    break;
		case RnaInteractionType.UNKNOWN_SUBTYPE:
		    // ignore interaction
		    break;
		default:
		    assert false; // should never happen
		}
	    }
	}
	return AccuracyTools.computeMatthews(tp, fp, tn, fn);
    }

    private double computeMatrixDifference(int[][] trueInteractions,
					 int[][] predictedInteractions) {
	assert trueInteractions.length == predictedInteractions.length;
	assert trueInteractions[0].length == predictedInteractions[0].length;
	int count = 0;
	for (int i = 0; i < trueInteractions.length; ++i) {
	    for (int j = 0; j < trueInteractions[0].length; ++j) {
		if ((trueInteractions[i][j] != RnaInteractionType.UNKNOWN_SUBTYPE) 
		    && (trueInteractions[i][j] != predictedInteractions[i][j]) ) {
		    ++count;
		}
	    }
	}
	return (double)count;
    }

    private double computeMatrixOverlap(int[][] trueInteractions,
				        int[][] predictedInteractions) {
	assert trueInteractions.length == predictedInteractions.length;
	assert trueInteractions[0].length == predictedInteractions[0].length;
	int count = 0;
	for (int i = 0; i < trueInteractions.length; ++i) {
	    for (int j = 0; j < trueInteractions[0].length; ++j) {
		// only count base pairs, not single stranded regions:
		if ((trueInteractions[i][j] == RnaInteractionType.WATSON_CRICK)  
		    && (trueInteractions[i][j] == predictedInteractions[i][j]) ) {
		    ++count;
		}
	    }
	}
	return (double)count;
    }


    private int findInnerInteraction(String s, int n) {
	if (s.charAt(n) != '(') {
	    return -1;
	}
	for (int i = n+1; i < s.length(); ++i) {
	    if (s.charAt(i) == ')') {
		return i;
	    }
	    else if (s.charAt(i) == '(') {
		return -1; // is not inner interaction
	    }
	}
	return -1; // no closing bracket found
    }
    
    private IntervalInt findInnerInteraction(String s) {
	for (int i = 0; i < s.length(); ++i) {
	    int j = findInnerInteraction(s, i);
	    if (j >= 0) {
		return new IntervalInt(i,j);
	    }
	}
	return null;
    }


    /** parses structure string in bracket notation, adds found "(" and ")" parenthesis to interaction set.
     * It return nxn matrix with filled with RnaInteraction.NO_INTERACTION or RnaInteraction.WATSON_CRICK */
    private int[][] parseBracketInteractions(String structure) {
	assert structure != null;
	int len = structure.length();
	int[][] result = new int[len][len];
	for (int i = 0; i < result.length; ++i) {
	    for (int j = 0; j < result[i].length; ++j) {
		if ((structure.charAt(i) == UNKNOWN_INTERACTION_CHAR)
		    || (structure.charAt(j) == UNKNOWN_INTERACTION_CHAR)) {
		    result[i][j] = RnaInteractionType.UNKNOWN_SUBTYPE; // can be paired or unpaired
		}
		else {
		    result[i][j] = RnaInteractionType.NO_INTERACTION;
		}
	    }
	}
	int numInteractions = StringTools.countChar(structure, '(');
	StringBuffer buf = new StringBuffer(structure);
	int foundInteractions = 0;
	while (foundInteractions < numInteractions) {
	    IntervalInt intervall = findInnerInteraction(buf.toString());
	    if (intervall == null) {
		log.info("Could not find inner interactions in: " + structure);
		break; // could not find more interactions
	    }
	    int j = intervall.getLower();
	    int k = intervall.getUpper();
	    log.fine("Found interaction");
	    log.fine("Structure: " + structure);
	    log.fine("Interaction: " + j + "   " + k);
	    // found interaction
	    result[j][k] = RnaInteractionType.WATSON_CRICK;
	    result[k][j] = RnaInteractionType.WATSON_CRICK; // symmetry
	    buf.setCharAt(j, '.');
	    buf.setCharAt(k, '.'); // delete 
	    foundInteractions++;
	}
	assert foundInteractions == numInteractions;
	return result;
    }

    /** Computes similarity of predicted with reference structure using a single sequence */
    private double scoreStructureSequence(StringBuffer bseq,
					  int[][] interactions) throws IOException, ParsingException {
	assert interactions.length == bseq.length();
	double score = 0.0;
	String[] rnafoldLines = launchRnafold(bseq.toString());
	String bracket = RnaFoldTools.parseRnafoldStructure(rnafoldLines);
	int[][] predictedInteractions = parseBracketInteractions(bracket);
	double result = computeMatrixDifference(interactions, predictedInteractions) / 2; // is symmetric!
	// log.info("Matrix difference for comparing desired and predicted structure: "
	// + result + " " + bracket);
	return result;
    }

    /** Computes similarity of predicted with reference structure using a single sequence.
     * Zero (best) score for completely different structures. Otherwise: score of 1 for each base pair in common. */
    private double scoreStructureSequenceOverlap(StringBuffer bseq,
						 int[][] interactions) throws IOException, ParsingException {
	assert interactions.length == bseq.length();
	double score = 0.0;
	String[] rnafoldLines = launchRnafold(bseq.toString());
	String bracket = RnaFoldTools.parseRnafoldStructure(rnafoldLines);
	int[][] predictedInteractions = parseBracketInteractions(bracket);
	double result = computeMatrixOverlap(interactions, predictedInteractions) / 2; // is symmetric!
	// log.info("Matrix difference for comparing desired and predicted structure: "
	// + result + " " + bracket);
	return result;
    }
    
    /** fuses two sequences by adding linker sequence in middle */
    private String generateFusedSequence(String s1, String s2) {
	return s1 + linkerSequence + s2;
    }

    /** convert nxm matrix (corresponding to two different sequences) to kxk matrix of fused sequence */
    private int[][] generateFusedInteractions(int[][] interactionsA, 
					      int[][] interactionsB,
					      int[][] interactionsAB, boolean orderFlag) {
	assert interactionsA.length ==  interactionsAB.length;
	assert interactionsB.length == interactionsAB[0].length;
	if (!orderFlag) {
	    return generateFusedInteractions(interactionsB, interactionsA, IntegerArrayTools.transpose(interactionsAB), true);
	}
	assert orderFlag;
	int linkLen = 0; // linkerSequence.length(); // point of RNAcofold is that no linker sequence is necessary
	int l1 = interactionsA.length;
	int l2 = interactionsB.length;
	int fusedLen = l1 + linkLen + l2;
	assert fusedLen > 0;
	int[][] result = new int[fusedLen][fusedLen];
	for (int i = 0; i < result.length; ++i) {
	    for (int j = 0; j < result[0].length; ++j) {
		result[i][j] = RnaInteractionType.NO_INTERACTION;
	    }
	}
	for (int i = 0; i < interactionsA.length; ++i) {
	    for (int j = 0; j < interactionsA[0].length; ++j) {
		result[i][j] = interactionsA[i][j];
	    }
	}
	for (int i = 0; i < interactionsB.length; ++i) {
	    for (int j = 0; j < interactionsB[0].length; ++j) {
		result[i+l1 + linkLen][j+l1 + linkLen] = interactionsB[i][j];
	    }
	}
	for (int i = 0; i < interactionsAB.length; ++i) {
	    for (int j = 0; j < interactionsAB[0].length; ++j) {
		int id1 = i;
		int id2 = j + l1 + linkLen;
		assert id1 < result.length;
		assert id2 < result[0].length;
		result[id1][id2] = interactionsAB[i][j];
		result[id2][id1] = interactionsAB[i][j]; // fused matrix is symmetric!
	    }
	}
	return result;
    }

    private double scoreStructureSequencePair(StringBuffer bseq1,
					      StringBuffer bseq2,
					      int[][][][] interactions,
					      int m,
					      int n,
					      boolean orderFlag) throws IOException, ParsingException {
	assert interactions[m][n].length == bseq1.length();
	assert interactions[m][n][0].length == bseq2.length();
// 	log.info("Scoring sequences " + NEWLINE + bseq1.toString() + NEWLINE + bseq2.toString());
// 	IntegerArrayTools.writeMatrix(System.out, interactions);
	double result = 0.0;
	String fusedSequence = "";
	int[][] fusedInteractions = generateFusedInteractions(interactions[m][m], interactions[n][n],
							      interactions[m][n], orderFlag);
	assert fusedInteractions.length == fusedInteractions[0].length; // must be symmetric!
	if (orderFlag) {
	    fusedSequence = generateFusedSequence(bseq1.toString(), bseq2.toString());
	}
	else {
	    fusedSequence = generateFusedSequence(bseq2.toString(), bseq1.toString());
	}
	if (probabilityMode) {
	    double[][] probMatrix = launchRnacofoldProb(fusedSequence);
	    assert probMatrix.length == interactions[m][m].length + interactions[n][n].length;
	    result = scoreProbabilityMatrix(probMatrix, fusedInteractions);
	    // , interactions, m, n, orderFlag);
	    // result = 
	}
	else {
	    String[] rnafoldLines = launchRnacofold(fusedSequence);
	    String bracket = RnaFoldTools.parseRnacofoldStructure(rnafoldLines);
	    int[][] predictedInteractions = parseBracketInteractions(bracket);
	    result = computeMatrixDifference(fusedInteractions, predictedInteractions) / 2;
	    log.info("Matrix difference for comparing fused desired and predicted structure: "
		     + result + " " + bracket);
	}
	return result;
    }

    /** Returns sequence weights. Assumes 1 and 0 values as double */
    int[] extractIntegerWeights(SecondaryStructure structure) {
	int[] result = new int[structure.getSequenceCount()];
	for (int i = 0; i < result.length; ++i) {
	    result[i] = (int)(structure.getSequence(i).getWeight());
	}
	return result;
    }

    /** returns error score for complete secondary structure and trial sequences */
    public double scoreStructure(StringBuffer[] bseqs,
				 SecondaryStructure structure,
				 int[][][][] interactionMatrices,
				 int[] weights,
				 boolean activeState) {
	double score = 0.0;
	try {
	    for (int i = 0; i < bseqs.length; ++i) {
		if (weights[i] == 0) {
		    continue;
		}
		int interactionCount = 0;
		for (int j = i+1; j < bseqs.length; ++j) {
		    if (weights[j] == 0) {
			continue;
		    }
		    ++interactionCount;
		    // symmetrize:
		    double crossTerm1 = scoreStructureSequencePair(bseqs[i], bseqs[j], interactionMatrices, i,j, true);
		    double crossTerm2 = scoreStructureSequencePair(bseqs[i], bseqs[j], interactionMatrices, i,j, false);
		    score += 0.5 * (crossTerm1 + crossTerm2);
		    log.fine("Cross terms " + (i+1) + " " + (j+1) + " : " + crossTerm1 + " " + crossTerm2);
		}
	    }
	    // if ((i == 0) && (interactionCount == 0) && (!probabilityMode)) { // self term only important for first sequence = riboswitch
	    score += scoreStructureSequenceOverlap(bseqs[0], interactionMatrices[0][0]); // Ribozyme without interactions: we WANT it to be different
	}
	catch (java.io.IOException ioe) {
	    log.severe("Error launching RNAfold! Error message: " + ioe.getMessage());
	    return 0.0;
	}
	catch (ParsingException  pe) {
	    log.severe("Error parsing RNAfold results! Error message: " + pe.getMessage());
	    return 0.0;
	}
// 	if (!activeState) {
// 	    score = -score; // reverse scoring: if not active statue, we WANT the predictions to be different from the reference
// 	}
	return score;
    }

    /** Generates sequence concatenated with linkerSequence2 as linker, ignoring first sequence */
    StringBuffer generateCombinedNotFirstSeq(StringBuffer[] bseqs, int[] weights) {
	StringBuffer result = new StringBuffer();
	for (int i = 1; i < weights.length; ++i) {
	    if (weights[i] != 0) {
		if (result.length() > 0) {
		    result.append(linkerSequence2);
		}
		result.append(bseqs[i].toString());
	    }
	}
	return result; 
    }

    int getCombinedInteraction(int[][][][] interactionMatrices, int[] weights, int i, int j) {
	int n0 = 0;
	int x = i;
	int y = 0;
	int offset = 0;
	int n1 = 0;
	int linkLen = linkerSequence2.length();
	for (int k = 1; k < weights.length; ++k) {
	    int currLen = interactionMatrices[0][k][0].length;
	    if (weights[k] != 0) {
		if ((offset + currLen + linkLen) < j) {
		    offset += currLen + linkLen;
		}
		else {
		    n1 = k; // found correct interaction sequence
		    y = j-offset;
		}
	    }
	}
	assert x < interactionMatrices[n0][n1].length;
	assert y < interactionMatrices[n0][n1][x].length;
	return interactionMatrices[n0][n1][x][y];
    }

    int[][] generateCombinedInteractionWithFirstMatrix(StringBuffer[] bseqs, int[][][][] interactionMatrices, int[] weights) {
	int origLen = bseqs[0].length();
	int otherLen = generateCombinedNotFirstSeq(bseqs, weights).length();
	int[][] result = new int[origLen][otherLen];
	for (int i = 0; i < result.length; ++i) {
	    for (int j = 0; j < result.length; ++j) {
		result[i][j] = getCombinedInteraction(interactionMatrices, weights, i, j);
	    }
	}
	return result;
    }

    /** returns error score for complete secondary structure and trial sequences. Alternative implementation that uses concatenated sensors. */
    /*
    public double scoreStructure2(StringBuffer[] bseqs,
				 SecondaryStructure structure,
				 int[][][][] interactionMatrices,
				 int[] weights,
				 boolean activeState) {
	double score = 0.0;
	try {
	    StringBuffer combinedSeq = generateCombinedNotFirstSeq(bseqs, weights);
	    int[][] combinedInteractionMatrix = generateCombinedInteractionWithFirstMatrix(bseqs, interactionMatrices, weights);
	    assert combinedInteractionMatrix.length == bseqs[0].length();
	    assert combinedInteractionMatrix[0].length == combinedSeq.length();
	    if (combinedSeq.length() > 0) {
		double crossTerm1 = scoreStructureSequencePair(bseqs[0], combinedSeq, combinedInteractionMatrix, true);
		double crossTerm2 = scoreStructureSequencePair(bseqs[0], combinedSeq, combinedInteractionMatrix, false);
		score += 0.5 * (crossTerm1 + crossTerm2);
		log.info("Cross terms: " + crossTerm1 + " " + crossTerm2);
	    }
	    else {
		double selfTerm = scoreStructureSequence(bseqs[0], interactionMatrices[0][0]); // same sequence
		score += selfTerm;
		log.info("Self term " + (1) + " : " + selfTerm);
	    }
	} // 
	catch (java.io.IOException ioe) {
	    log.severe("Error launching RNAfold! Error message: " + ioe.getMessage());
	    return 0.0;
	}
	catch (ParsingException  pe) {
	    log.severe("Error parsing RNAfold results! Error message: " + pe.getMessage());
	    return 0.0;
	}
	if (!activeState) {
	    score = -score; // reverse scoring: if not active statue, we WANT the predictions to be different from the reference
	}
	return score;
    }
    */


    /** returns error score for complete secondary structure and trial sequences */
    public Properties generateReport(StringBuffer[] bseqs,
				     SecondaryStructure structure,
				     int[][][][] interactionMatrices) {
	assert false;
	return null;
    }

    /** returns error score for complete secondary structure and trial sequences */
    public double scoreStructure(StringBuffer[] bseqs,
				 SecondaryStructure structure,
				 int[][][][] interactionMatrices) {
	int[] refWeights = extractIntegerWeights(structure);
	IntegerArrayGenerator gen = new IntegerArrayGenerator(refWeights.length, 2); // generate all binary numbers
	assert gen.hasNext();
	int[] currWeights = gen.get();
	double result = 0.0;
	do { 
	    boolean activeState = IntegerArrayTools.equals(currWeights, refWeights);
	    result += scoreStructure(bseqs, structure, interactionMatrices, currWeights, activeState);
	}
	while (gen.hasNext() && ((currWeights = gen.next()) != null));
	return result;
    }

}
