package secondarystructuredesign; 

import java.io.*;
import java.util.logging.*;
import java.util.ResourceBundle;
import generaltools.StringTools;
import generaltools.ParsingException;
import generaltools.TestTools;
import numerictools.*;
import launchtools.*;
import rnasecondary.*;
import static secondarystructuredesign.PackageConstants.*;

import org.testng.annotations.*;

public class RnaFoldTools {

    private static Logger log = Logger.getLogger(LOGFILE_DEFAULT);
    private static ResourceBundle rb = ResourceBundle.getBundle(BUNDLE_NAME);

    /** Reads RNAfold / RNAcofold probability matrices (a postscript file). Converts given square root of probabilities into probabilities. 
     */
    public static double[][] parseRnafoldProbabilities(String[] lines) throws ParsingException {
	// find sequence, deduce size:
	String line;
	String[] words;
	int pc = 0;
	boolean foundSequence = false;
	for (pc = 0; pc < lines.length; ++pc) {
	    line = lines[pc];
	    if ((line.length() > 9) && line.substring(0, 9).equals("/sequence")) {
		foundSequence = true;
		break;
	    }
	}
	if (!foundSequence) {
	    throw new ParsingException("Could not find sequence header!");
	}
	++pc;
	assert pc < lines.length;
	String sequence = lines[pc];
	while (sequence.charAt(sequence.length()-1) == '\\') { // check for trailing '\' character
	    if ((pc + 1) >= lines.length) {
		throw new ParsingException("Error parsing sequence in RNAfold file");
	    }
	    sequence = sequence.substring(0, sequence.length()-1) + lines[++pc]; // delete last character and add new line
	}
	// find "\" character:
	for (int i = 0; i < sequence.length(); ++i) {
	    char c = sequence.charAt(i);
	    if ((c == '\\') || (c == ')')) {
		sequence = sequence.substring(0, i);
		break;
	    }
	}
	// cerr << "Scanned sequence: " << sequence << " with " << sequence.size() << " characters!" << endl;
	int numRes = sequence.length(); // subtract 1 because last character is "\"
	double[][] result = new double[numRes][numRes];
	try {
	while (++pc < lines.length) {
	    line = lines[pc].trim();
	    words = line.split(" ");
	    if ((words.length == 4) && words[3].equals("ubox")) {
		int id1 = Integer.parseInt(words[0])-1;
		int id2 = Integer.parseInt(words[1])-1;
		result[id1][id2] = Double.parseDouble(words[2]);
		result[id1][id2] *= result[id1][id2]; // Postscript file contains square roots of probabilities!
		result[id2][id1] = result[id1][id2];
	    }
	}
	}
	catch(NumberFormatException nfe) {
	    throw new ParsingException("Number format exception in line " + (pc+1) + " : " + nfe.getMessage());
	}
	return result;
    }

    /** Reads RNAfold / RNAcofold probability matrices (a postscript file). Converts given square root of probabilities into probabilities. 
     */
    @Test (groups={"new"})
    public void parseRnafoldProbabilitiesTest() {
	String methodName = "parseRnafoldProbabilitiesTest";
	System.out.println(TestTools.generateMethodHeader(methodName));
	String filename = NANOTILER_HOME + "/test/fixtures/yeslogic_rnafold.ps";
	try {
	    FileInputStream fis = new FileInputStream(filename);
	    String[] lines = StringTools.readAllLines(fis);
	    double[][] matrix = parseRnafoldProbabilities(lines);
	    assert matrix != null;
	    assert matrix.length > 0;
	    assert matrix[0] != null && matrix[0].length == matrix.length;
	    System.out.println("Imported probability matrix: " + matrix.length + " x " + matrix[0].length);
	    DoubleArrayTools.writeMatrix(System.out, matrix);
	    assert matrix.length == 80;
	    // check contact 17 , 43
	    assert Math.abs(matrix[16][42] - 0.695) < 0.05;
	}
	catch(IOException ioe) {
	    System.out.println(ioe.getMessage());
	    assert false;
	}
	catch(ParsingException pe) {
	    System.out.println(pe.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


    /** Parses RNAfold energy. */
    public static double parseRnafoldEnergy(String[] lines) throws ParsingException {
	if ((lines == null) || (lines.length < 2)) {
	    throw new ParsingException("Expected at least two lines in RNAfold output!");
	}
	String lastLine = lines[lines.length-1];
// 	lastLine.replace(0, lastLine.length(), "(", "");
// 	lastLine.replace(0, lastLine.length(), ")", "");

	String[] words = lastLine.split(" ");
	double result = 0;
	if (words.length < 2) {
	    throw new ParsingException("Expected structure and energy in last line of RNAfold input: " + lastLine);
	}
	String lastWord = words[words.length-1];
	if (lastWord.charAt(0) == '(') {
	    lastWord = lastWord.substring(1, lastWord.length()); // remove first character
	}
	assert lastWord.length() > 0;
	if (lastWord.charAt(lastWord.length()-1) == ')') {
	    lastWord = lastWord.substring(0, lastWord.length()-1); // remove last character
	}
	try {
	    result = Double.parseDouble(lastWord); // would be correct without option -a
	}
	catch (NumberFormatException nfe) {
	    throw new ParsingException("Expected energy in last line of RNAfold input: " + words[words.length-1] + " : " 
				       + nfe.getMessage());
	}
	catch (RuntimeException e) {
	    throw new ParsingException("Expected energy in last line of RNAfold input: " 
				       + words[words.length-1] + " : " + e.getMessage());
	}
	return result;
    }

    /** Returns bracked notation for set of raw output lines from RNAfold/RNAcofold */
    public static String parseRnafoldStructure(String[] lines) throws ParsingException {
	if ((lines == null) || (lines.length < 2)) {
	    throw new ParsingException("Expected at least two lines in RNAfold output!");
	}
	int id = lines[0].charAt(0) == '>' ? 2 : 1; // if ">" use third line, otherwise use second line
	if (id >= lines.length) {
	    throw new ParsingException("Not sufficient number of  lines in rnafold structure!");
	}
	String lastLine = lines[id];
	String[] words = lastLine.split(" ");
	if (words.length < 2) {
	    throw new ParsingException("Expected structure and energy in last line of RNAfold input: " + lastLine);
	}
	return words[0];
    }

    /** returns structure with added "&" character between structures */
    public static String parseRnacofoldStructure(String[] lines) throws ParsingException {
	if ((lines == null) || (lines.length < 2)) {
	    throw new ParsingException("Expected at least two lines in RNAcofold output!");
	}
	String line = lines[1];
	if (lines[0].charAt(0) == '>') {
	    if (lines.length < 3) {
		throw new ParsingException("Expected at least three lines in RNAcofold output!");
	    }
	    line = lines[2]; // skip header line
	}
	String[] words = line.split(" ");
	if (words.length < 2) {
	    System.out.println("Severe error while parsing:");
	    for (int i = 0; i < lines.length; ++i) {
		System.out.println(lines[i]);
	    }
	    throw new ParsingException("Expected structure and energy in last line of RNAcofold input: " 
				       + line);
	}
	return words[0];
    }

    /** Parses RNAcofold energy. Assumes that RNAcofold is launched with option -a, processed with  tr "\t" " "  | tr -s " "  */
    public static double parseRnacofoldEnergies(String[] lines, int id) throws ParsingException {
	assert (id >= 0) && (id < 5);
	if ((lines == null) || (lines.length < 2)) {
	    throw new ParsingException("Expected at least two lines in RNAcofold output!");
	}
	
	String lastLine = lines[lines.length-1];
    // 	lastLine.replace(0, lastLine.length(), "(", "");
// 	lastLine.replace(0, lastLine.length(), ")", "");
	String[] words = lastLine.split(" "); // careful: tab had to be translated to space before\t");
	if (words.length == 1) {    // Sometimes energies are separated be "\t" instead of " "
	    words = lastLine.split("\t");
	}

	double result = 0;
	if (words.length < 5) {
	    throw new ParsingException("Energy in last line of RNAcofold input: " + lastLine);
	}
	try {
	    // sample of last two lines:
	    // AB AA BB A B
		// -29.687247 -22.955548 -17.775745 -9.670848 -6.741081";
	    assert id < words.length;
	    result = Double.parseDouble(words[id]);
	}
	catch (RuntimeException e) {
	    throw new ParsingException("Expected energy in last line of RNAcofold input: " + words[words.length-1]);
	}
	return result;
    }

    /** Parses RNAcofold energy AB. Assumes that RNAcofold is launched with option -a */
    public static double parseRnacofoldEnergyAB(String[] lines) throws ParsingException {
	return parseRnacofoldEnergies(lines, 0);
    }
    /** Parses RNAcofold energy AA. Assumes that RNAcofold is launched with option -a */
    public static double parseRnacofoldEnergyAA(String[] lines) throws ParsingException {
	return parseRnacofoldEnergies(lines, 1);
    }
    /** Parses RNAcofold energy BB. Assumes that RNAcofold is launched with option -a */
    public static double parseRnacofoldEnergyBB(String[] lines) throws ParsingException {
	return parseRnacofoldEnergies(lines, 2);
    }
    /** Parses RNAcofold energy A. Assumes that RNAcofold is launched with option -a */
    public static double parseRnacofoldEnergyA(String[] lines) throws ParsingException {
	return parseRnacofoldEnergies(lines, 3);
    }
    /** Parses RNAcofold energy B. Assumes that RNAcofold is launched with option -a */
    public static double parseRnacofoldEnergyB(String[] lines) throws ParsingException {
	return parseRnacofoldEnergies(lines, 4);
    }

    @Test(groups={"new"})
    public void testParseRnacofoldEnergies() {
	String sampleInput = 
	    "GGGAAAAACCGUUUCGAUCUCCUACGACGGAGCUUUACGUUAUCUGACCG&GGGAAAUAUCCUACGGCACCACUAUAAUGACUAUGGAGAUCGAAUGAAUC\n"
	    + "((......))..((((((((((...((((........))))...((.(((&((((....)))).)))))................))))))))))...... (-27.60)\n"
	    + "((......))..((((((((((...((((........))))...{(.(((&((({....}))).)))),................))))))))))...... [-29.69]\n"
	    + "frequency of mfe structure in ensemble 0.0761266 , delta G binding=-13.28\n"
	    + "Free Energies:\n"
	    + "AB AA BB A B\n"
	    + "-29.687247 -22.955548 -17.775745 -9.670848 -6.741081\n";
	String[] lines = sampleInput.split("\n");
	assert lines.length == 7;
	try {
	    assert Math.abs(parseRnacofoldEnergyAB(lines) - (-29.68)) < 0.1;
	    assert Math.abs(parseRnacofoldEnergyAA(lines) - (-22.96)) < 0.1;
	    assert Math.abs(parseRnacofoldEnergyBB(lines) - (-17.78)) < 0.1;
	    assert Math.abs(parseRnacofoldEnergyA(lines) - (-9.67)) < 0.1;
	    assert Math.abs(parseRnacofoldEnergyB(lines) - (-6.74)) < 0.1;
	}
	catch (ParsingException pe) {
	    System.out.println("Error parsing RNAcofold energy output: " + pe.getMessage());
	    assert false;
	}
    }

    /** parses structure String in bracket notation, adds found "(" and ")" parenthesis to interaction set.
     * It return nxn matrix with filled with RnaInteraction.NO_INTERACTION or RnaInteraction.WATSON_CRICK */
    public static int[][] parseBracketInteractions(String structure) {
	assert structure != null;
	int len = structure.length();
	int[][] result = new int[len][len];
	for (int i = 0; i < result.length; ++i) {
	    for (int j = 0; j < result[i].length; ++j) {
		result[i][j] = RnaInteractionType.NO_INTERACTION;
	    }
	}
	int numInteractions = StringTools.countChar(structure, '(');
	StringBuffer buf = new StringBuffer(structure);
	int foundInteractions = 0;
	while (foundInteractions < numInteractions) {
	    IntervalInt intervall = findInnerInteraction(buf.toString());
	    if (intervall == null) {
		log.info("Could not find inner interactions in: " + structure);
		break; // could not find more interactions
	    }
	    int j = intervall.getLower();
	    int k = intervall.getUpper();
	    log.fine("Found interaction");
	    log.fine("Structure: " + structure);
	    log.fine("Interaction: " + j + "   " + k);
	    // found interaction
	    result[j][k] = RnaInteractionType.WATSON_CRICK;
	    result[k][j] = RnaInteractionType.WATSON_CRICK; // symmetry
	    buf.setCharAt(j, '.');
	    buf.setCharAt(k, '.'); // delete 
	    foundInteractions++;
	}
	assert foundInteractions == numInteractions;
	return result;
    }

    /** parses structure String in bracket notation, adds found "(" and ")" parenthesis to interaction set.
     * It return nxn matrix with filled with RnaInteraction.NO_INTERACTION or RnaInteraction.WATSON_CRICK */
    public static int[][] parseRnacofoldBracketInteractions(String structure) {
	assert structure != null;
	// find location of "&" character
	String linker = "&";
	char linkChar= linker.charAt(0);
	int sepId = structure.indexOf(linkChar);
	if (sepId < 0) {
	    System.err.println("Strange RNAcofold bracket interaction (no & character): " + structure);
	    assert false;
	}
	int len1 = sepId;
	// int len = structure.length();
	int len2 = structure.length() - len1 - linker.length();
	int[][] result = new int[len1][len2];
	for (int i = 0; i < result.length; ++i) {
	    for (int j = 0; j < result[i].length; ++j) {
		result[i][j] = RnaInteractionType.NO_INTERACTION;
	    }
	}
	int numInteractions = StringTools.countChar(structure, '(');
	StringBuffer buf = new StringBuffer(structure);
	int foundInteractions = 0;
	while (foundInteractions < numInteractions) {
	    IntervalInt intervall = findInnerInteraction(buf.toString());
	    if (intervall == null) {
		log.info("Could not find inner interactions in: " + structure);
		break; // could not find more interactions
	    }
	    int j = intervall.getLower();
	    int k = intervall.getUpper() - linker.length() - len1; // subtract one because of linker character
	    log.fine("Found interaction");
	    log.fine("Structure: " + structure);
	    log.fine("Interaction: " + j + "   " + k);
	    if ((j < sepId) && (k >= 0)) {
		// found interaction
		result[j][k] = RnaInteractionType.WATSON_CRICK;
	    }
	    // result[k][j] = RnaInteractionType.WATSON_CRICK; // symmetry
	    buf.setCharAt(intervall.getLower(), '.');
	    buf.setCharAt(intervall.getUpper(), '.'); // delete bp
	    foundInteractions++;
	}
	assert foundInteractions == numInteractions;
	return result;
    }

    /** parses structure String in bracket notation, adds found "(" and ")" parenthesis to interaction set.
     * It return nxn matrix with filled with RnaInteraction.NO_INTERACTION or RnaInteraction.WATSON_CRICK */
    public static int[][] parseRnacofoldBracketSingleInteractions(String structure, boolean firstSequence) {
	assert structure != null;
	// find location of "&" character
	String linker = "&";
	char linkChar= linker.charAt(0);
	int sepId = structure.indexOf(linkChar);
	int len1 = sepId;
	// int len = structure.length();
	int len2 = structure.length() - len1 - linker.length();
	int lenSide = len1;
	if (!firstSequence) {
	    lenSide = len2;
	}
	int[][] result = new int[lenSide][lenSide];
	for (int i = 0; i < result.length; ++i) {
	    for (int j = 0; j < result[i].length; ++j) {
		result[i][j] = RnaInteractionType.NO_INTERACTION;
	    }
	}
	int numInteractions = StringTools.countChar(structure, '(');
	StringBuffer buf = new StringBuffer(structure);
	int foundInteractions = 0;
	while (foundInteractions < numInteractions) {
	    IntervalInt intervall = findInnerInteraction(buf.toString());
	    if (intervall == null) {
		log.info("Could not find inner interactions in: " + structure);
		break; // could not find more interactions
	    }
	    int jOrig = intervall.getLower();
	    int kOrig = intervall.getUpper(); // subtract one because of linker character
	    int j = jOrig;
	    int k = kOrig;
	    if (firstSequence) {
		if (k < sepId) {
		    //		    log.fine("Found interaction for first sequence:");
		    // log.fine("Structure: " + structure);
		    // log.fine("Interaction: " + j + "   " + k);
		    // found interaction
		    result[j][k] = RnaInteractionType.WATSON_CRICK;
		}
	    }
	    else {
		j -= sepId - 1;
		k -= sepId - 1;
		if ((j >= 0) && (k >= 0) && (j < result.length) && (k < result[0].length)) {
		    if (k < sepId) {
			//			log.fine("Found interaction for first sequence:");
			// log.fine("Structure: " + structure);
			// log.fine("Interaction: " + j + "   " + k);
			// found interaction
			result[j][k] = RnaInteractionType.WATSON_CRICK;
		    }
		}
	    }
	    // result[k][j] = RnaInteractionType.WATSON_CRICK; // symmetry
	    buf.setCharAt(jOrig, '.');
	    buf.setCharAt(kOrig, '.'); // delete 
	    foundInteractions++;
	}
	assert foundInteractions == numInteractions;
	return result;
    }

    public static double computeMatrixMatthews(int[][] trueInteractions,
					 int[][] predictedInteractions) {
	assert trueInteractions.length == predictedInteractions.length;
	assert trueInteractions[0].length == predictedInteractions[0].length;
	int tp = 0;
	int fp = 0;
	int tn = 0;
	int fn = 0;
	for (int i = 0; i < trueInteractions.length; ++i) {
	    for (int j = i+1; j < trueInteractions[0].length; ++j) {
		int pv = predictedInteractions[i][j];
		int tv = trueInteractions[i][j];
		switch (tv) {
		case RnaInteractionType.WATSON_CRICK:
		    if (pv == RnaInteractionType.WATSON_CRICK) {
			++tp;
		    }
		    else {
			++fn; // missed interaction
		    }
		    break;
		case RnaInteractionType.NO_INTERACTION:
		    if (pv == RnaInteractionType.WATSON_CRICK) {
			++fp;
		    }
		    else {
			++tn; // missed interaction
		    }
		    break;
		default:
		    assert false; // should never happen
		}
	    }
	}
	return AccuracyTools.computeMatthews(tp, fp, tn, fn);
    }

    /** Computes difference between interaction matrices. Careful: ignores UNKNOWN_INTERACTION */
    public static double computeMatrixDifference(int[][] trueInteractions,
						 int[][] predictedInteractions) {
	assert trueInteractions.length == predictedInteractions.length;
	assert trueInteractions[0].length == predictedInteractions[0].length;
	int count = 0;
	for (int i = 0; i < trueInteractions.length; ++i) {
	    for (int j = 0; j < trueInteractions[0].length; ++j) {
		if ((trueInteractions[i][j] != predictedInteractions[i][j])
		    && (trueInteractions[i][j] != RnaInteractionType.UNKNOWN_SUBTYPE)) {
		    ++count;
		}
	    }
	}
	return (double)count;
    }

    /** Computes difference between interaction matrices. Careful: ignores UNKNOWN_INTERACTION */
    public static double countMissedInteractions(int[][] trueInteractions,
						 int[][] predictedInteractions) {
	assert trueInteractions.length == predictedInteractions.length;
	assert trueInteractions[0].length == predictedInteractions[0].length;
	int count = 0;
	for (int i = 0; i < trueInteractions.length; ++i) {
	    for (int j = 0; j < trueInteractions[0].length; ++j) {
		if ((trueInteractions[i][j] != RnaInteractionType.NO_INTERACTION)
		    && (trueInteractions[i][j] != RnaInteractionType.UNKNOWN_SUBTYPE)) {
		    if ((predictedInteractions[i][j] == RnaInteractionType.NO_INTERACTION)
			|| (predictedInteractions[i][j] == RnaInteractionType.UNKNOWN_SUBTYPE) ) {
			++count;
		    }
		}
	    }
	}
	return (double)count;
    }

    private static int findInnerInteraction(String s, int n) {
	if (s.charAt(n) != '(') {
	    return -1;
	}
	for (int i = n+1; i < s.length(); ++i) {
	    if (s.charAt(i) == ')') {
		return i;
	    }
	    else if (s.charAt(i) == '(') {
		return -1; // is not inner interaction
	    }
	}
	return -1; // no closing bracket found
    }
    
    private static IntervalInt findInnerInteraction(String s) {
	for (int i = 0; i < s.length(); ++i) {
	    int j = findInnerInteraction(s, i);
	    if (j >= 0) {
		return new IntervalInt(i,j);
	    }
	}
	return null;
    }


}
