package secondarystructuredesign;

import java.io.*;
import java.util.logging.*;
import java.util.*;
import java.text.ParseException;
import generaltools.StringTools;
import generaltools.ParsingException;
import numerictools.*;
import launchtools.*;
import rnasecondary.*;
import sequence.*;
import static secondarystructuredesign.PackageConstants.*;

/** Assigns a score to a secondary structure and a set of potential sequences */
public class RnacofoldSecondaryStructureScorer implements SecondaryStructureScorer {

    private Logger log = Logger.getLogger(LOGFILE_DEFAULT);
    private static ResourceBundle rb = ResourceBundle.getBundle(BUNDLE_NAME);
    private static String rnafoldScriptName = rb.getString("rnafoldscript");
    private static String rnacofoldScriptName = rb.getString("rnacofoldscript");
    private static String pknotsScriptName = rb.getString("pknotsscript");
    private boolean interStrandMode = true;
    private double energyAU = -0.8;
    private double energyGC = -1.2;
    private String linkerSequence = "&";
    private String pkLinkerSequence = "";
    private boolean energyMode = true; // if true, launch RNAcofold with option "-a" and compute energies of AB, AA, BB, A, B
    private boolean pkMode = false; // true;
    private int debugLevel = 2;
    private Level debugLogLevel = Level.FINE;
    private SecondaryStructureScorer subScorer;
    private double subScorerWeight = 1.0;
    private double selfEnergyCutoff = 0.0;
    private double structureWeight = 1.0;
    private double energyGap = 0.0; // require this minimum interaction energy for intercting strands
    private boolean countMissedMode = true; // only count missed interactions
    private StringBuffer[] lastSeqs = null;
    private double[] singleSequenceScoreCache = null;
    private double[][] sequencePairScoreCache = null;

    public static final int RNAFOLD_BLOCK_SIZE = 2; // this many lines per sequence  in RNAfold output
    public static final int RNACOFOLD_BLOCK_SIZE = 7; // this many lines per sequence pair in RNAcofold output

    // public String getLinkerSequence() { return this.linkerSequence; }

    // public void setLinkerSequence(String s) { this.linkerSequence = s; }

    /** Dummy energies of Watson-Crick pairings: AU, GC, UA, CG (but not GU, UG) < 0, all others: 0.0 */
    public double interactionEnergy(char c1, char c2) {
	c1 = Character.toUpperCase(c1);
	c2 = Character.toUpperCase(c2);
	if (c1 == c2) {
	    return 0.0;
	}
	if (c1 > c2) {
	    return interactionEnergy(c2, c1);
	}
	switch (c1) {
	case 'A':
	    if (c2 == 'U') {
		return energyAU;
	    }
	case 'C':
	    if (c2 == 'G') {
		return energyGC;
	    }
	}
	return 0.0;
    }

    /** launches RNAcofold or pknotsRG with given sequence, returns the raw line output  */
    String[] launchFolding(String sequence) throws IOException {
	// write secondary structure to temporary file
	File tmpInputFile = File.createTempFile("nanotiler_rnafold",".seq");
	String inputFileName = tmpInputFile.getAbsolutePath();
	FileOutputStream fos = new FileOutputStream(tmpInputFile);
	PrintStream ps = new PrintStream(fos);
	// write secondary structure, but only write sec structure, not sequence:
	// SecondaryStructureWriter secWriter = new SecondaryStructureScriptFormatWriter();
	// log.finest(debugLogLevel,"Writing to file: " + inputFileName);
	// log.finest(debugLogLevel,"Writing content: " + sequence);
	ps.println(sequence); // no special formatting needed!
	fos.close();
	File tmpOutputFile = File.createTempFile("nanotiler_rnafold", ".sec");
	final String outputFileName = tmpOutputFile.getAbsolutePath();
	if (tmpOutputFile.exists()) {
	    tmpOutputFile.delete();
	}

	// generate command string
	String scriptName = rnacofoldScriptName;
	if (pkMode) {
	    scriptName = pknotsScriptName;
	}
	else if (sequence.indexOf('&') < 0) { // using RNAfold instead of RNAcofold!
	    scriptName = rnafoldScriptName;
	}
	File tempFile = new File(scriptName);
	String[] commandWords = { scriptName, inputFileName, outputFileName}; 
	// generate command
	RunCommand command = new SimpleRunCommand(commandWords);
	log.log(debugLogLevel, "Issuing command: " + scriptName + " " + inputFileName + " " + outputFileName);
	// create queue manager (singleton pattern)
	QueueManager queueManager = SimpleQueueManager.getInstance();
	// generate job
	Job job = queueManager.createJob(command);
	
	// add listener to job !?
	
	// launch command
	queueManager.submit(job);	    
	// log.log(debugLogLevel,"queue manager finished job!");
	// open output file:
	// log.log(debugLogLevel,"Importing optimized sequences from " + outputFileName);
	String[] resultLines = null;
	FileInputStream resultFile = null;
	try {
	    resultFile = new FileInputStream(outputFileName);
	    resultLines = StringTools.readAllLines(resultFile);
	    // sequenceStrings = RnaFoldTools.getSequenceStrings(resultLines);
	}
	catch (IOException ioe) {
	    log.warning("Error when scraping result file from: " 
			+ outputFileName);
	    assert false;
	    throw ioe;
	}
	finally {
	    if (resultFile != null) {
		resultFile.close();
		File file = new File(outputFileName);
		file.delete();
	    }
	    if (tmpInputFile != null) {
		tmpInputFile.delete();
	    }
	}
	if (resultLines == null) {
	    log.warning("Rnacofold results were null!");
	}
	return resultLines;
    }
    
    public String getLinker() {
	if (pkMode) {
	    return pkLinkerSequence;
	}
	return linkerSequence;
    }

    /** fuses two sequences by adding linker sequence in middle */
    private String generateFusedSequence(String s1, String s2) {
	return s1 + getLinker() + s2;
    }

    /** convert nxm matrix (corresponding to two different sequences) to kxk matrix of fused sequence */
    private int[][] generateFusedInteractions(int[][] interactions) {
	int linkLen = getLinker().length();
	int l1 = interactions.length;
	int l2 = interactions[0].length;
	int fusedLen = l1 + linkLen + l2;
	assert fusedLen > 0;
	int[][] result = new int[fusedLen][fusedLen];
	for (int i = 0; i < result.length; ++i) {
	    for (int j = 0; j < result[0].length; ++j) {
		result[i][j] = RnaInteractionType.NO_INTERACTION;
	    }
	}
	for (int i = 0; i < interactions.length; ++i) {
	    for (int j = 0; j < interactions[0].length; ++j) {
		if (interactions[i][j] == RnaInteractionType.WATSON_CRICK) {
		    int id1 = i;
		    int id2 = j + l1 + linkLen;
		    assert id1 < result.length;
		    assert id2 < result[0].length;
		    
		    result[id1][id2] = RnaInteractionType.WATSON_CRICK;
		    result[id2][id1] = RnaInteractionType.WATSON_CRICK; // fused matrix is symmetric!
		}
	    }
	}
	return result;
    }
    
    /** Returns true if the two strands are interacting */
    private boolean isInteracting(int[][] interactions) {
	for (int i = 0; i < interactions.length; ++i) {
	    for (int j = i+1; j < interactions[i].length; ++j) {
		if (interactions[i][j] != RnaInteractionType.NO_INTERACTION) {
		    return true;
		}
	    }
	}
	return false;
    }

    private double scoreStructureSequencePair(StringBuffer bseq1,
					      StringBuffer bseq2,
					      int[][] interactions,
					      int[][] interactions1,
					      int[][] interactions2,
					      String[] rnafoldLines) throws IOException, ParseException {
	assert interactions.length == bseq1.length();
	assert interactions[0].length == bseq2.length();
// 	log.info("Scoring sequences " + NEWLINE + bseq1.toString() + NEWLINE + bseq2.toString());
// 	IntegerArrayTools.writeMatrix(System.out, interactions);
	// int[][] fusedInteractions = generateFusedInteractions(interactions, orderFlag);
	// assert fusedInteractions.length == fusedInteractions[0].length; // must be symmetric!
	String fusedSequence = "";
	int len1 = bseq1.length();
	int len2 = bseq2.length();
	if (rnafoldLines == null) {
	    fusedSequence = generateFusedSequence(bseq1.toString(), bseq2.toString());
	    rnafoldLines = launchFolding(fusedSequence);
	}
	int[][] predictedInteractions = null;
	int[][] predictedInteractions1 = null;
	int[][] predictedInteractions2 = null;
	if (debugLogLevel.intValue() >= Level.INFO.intValue()) {
	    System.out.println("Output of folding algorithm:");
	    for (int i = 0; i < rnafoldLines.length; ++i) {
		System.out.println(rnafoldLines[i]);
	    }
	}
	if (pkMode) {
	    SecondaryStructureParser parser = new PknotsRGParser();
	    SecondaryStructure structure = parser.parse(rnafoldLines);
	    int[][] allInteractions = structure.toInteractionMatrix(0, 0); // there should be only one sequence
	    assert allInteractions != null && allInteractions.length > 0;
	    assert allInteractions.length == allInteractions[0].length;
	    predictedInteractions = IntegerArrayTools.subMatrix(allInteractions, len1 + getLinker().length(), 0, allInteractions.length, len1);
	    assert predictedInteractions.length == len1;
	    assert predictedInteractions[0].length == len2;
	    predictedInteractions1 = IntegerArrayTools.subMatrix(allInteractions, 0, 0, len1, len1);
	    predictedInteractions2 = IntegerArrayTools.subMatrix(allInteractions, len1 + getLinker().length(), len1 + getLinker().length(),
								 allInteractions.length, allInteractions[0].length);
	    if (debugLevel > 2) {
		System.out.println("All interactions:");
		IntegerArrayTools.writeMatrix(System.out, allInteractions)
;		System.out.println("Cross interactions:");
		IntegerArrayTools.writeMatrix(System.out, predictedInteractions);
;		System.out.println("Interactions internal to first sequence:");
		IntegerArrayTools.writeMatrix(System.out, predictedInteractions1);
;		System.out.println("Interactions internal to second sequence:");
		IntegerArrayTools.writeMatrix(System.out, predictedInteractions2);

	    }

	}
	else {
	    String bracket = RnaFoldTools.parseRnafoldStructure(rnafoldLines);
	    if (debugLogLevel.intValue() >= Level.INFO.intValue()) {
		System.out.println("Parsing RNAcofold output:");
		for (String s : rnafoldLines) {
		    System.out.println(s);
		}
		System.out.println("Parsed bracked notation: " + bracket);
	    }
	    predictedInteractions = RnaFoldTools.parseRnacofoldBracketInteractions(bracket);
	    predictedInteractions1 = RnaFoldTools.parseRnacofoldBracketSingleInteractions(bracket, true);
	    predictedInteractions2 = RnaFoldTools.parseRnacofoldBracketSingleInteractions(bracket, false);
	}
	double result = 0.0;
	if (structureWeight != 0.0) {
	    result += structureWeight * RnaFoldTools.computeMatrixDifference(interactions, predictedInteractions) / 2;
	    result += structureWeight * RnaFoldTools.computeMatrixDifference(interactions1, predictedInteractions1) / 2;
	    result += structureWeight * RnaFoldTools.computeMatrixDifference(interactions2, predictedInteractions2) / 2;
	}
	if (energyMode) {
	    if (!pkMode) {
		double energyAB = RnaFoldTools.parseRnacofoldEnergyAB(rnafoldLines); // energy of AB
		double energyAA = RnaFoldTools.parseRnacofoldEnergyAA(rnafoldLines); // energy of AA
		double energyBB = RnaFoldTools.parseRnacofoldEnergyBB(rnafoldLines); // energy of BB
		double energyA = RnaFoldTools.parseRnacofoldEnergyA(rnafoldLines); // energy of A
		double energyB = RnaFoldTools.parseRnacofoldEnergyB(rnafoldLines); // energy of B
		double eDiff = energyAB - energyA - energyB;
		double eDiffGap = eDiff + energyGap;
		double eDiffAA = energyAB - energyAA;
		double eDiffBB = energyAB - energyBB;
		if (isInteracting(interactions)) { // slow, but probably negligable to compared to RNAcofold launch and parsing
		    if (eDiffGap > 0.0) {
			result += eDiffGap; // Complex has higher energy than seperately folded molecules
			log.fine("Adding energy penalty (1): " + eDiffGap);
		    }
		    if (eDiffAA > 0.0) {
			result += eDiffAA; // penalize if energy AA is lower than AB
			log.fine("Adding energy penalty (AA): " + eDiffAA);
		    }
		    if (eDiffBB > 0.0) {
			result += eDiffBB; // penalize if energy AA is lower than AB
			log.fine("Adding energy penalty (BB): " + eDiffBB);
		    }
		}
		else {
		    if (eDiff < 0.0) {
			result -= eDiff; // penalize that complex has lower energy than separately folded molecules
			log.fine("Adding energy penalty (2): " + (-eDiff));
		    }
		}
		// result += eDiff;	     // BUG! Taken out
	    }
	    else {
		log.fine("Energy mode not active when pseudoknot mode is switched on.");
	    }
	}
	assert result >= 0.0;
	return result;
    }

    /** Scores a single sequence structure */
    private double scoreStructureSingleSequence(StringBuffer bseq1,
						int[][] interactions1,
						String[] rnafoldLines) throws IOException, ParseException {
	assert interactions1.length == bseq1.length();
	assert interactions1[0].length == bseq1.length();
// 	log.info("Scoring sequences " + NEWLINE + bseq1.toString() + NEWLINE + bseq2.toString());
// 	IntegerArrayTools.writeMatrix(System.out, interactions);
	// int[][] fusedInteractions = generateFusedInteractions(interactions, orderFlag);
	// assert fusedInteractions.length == fusedInteractions[0].length; // must be symmetric!
	String fusedSequence = "";
	int len1 = bseq1.length();
	if (rnafoldLines == null) {
	    rnafoldLines = launchFolding(bseq1.toString());
	}
	assert rnafoldLines != null;
	int[][] predictedInteractions1 = null;
	double energy = 0.0;
	if (debugLogLevel.intValue() >= Level.INFO.intValue()) {
	    System.out.println("Output of single sequence folding algorithm:");
	    for (int i = 0; i < rnafoldLines.length; ++i) {
		System.out.println(rnafoldLines[i]);
	    }
	}
	if (pkMode) {
	    SecondaryStructureParser parser = new PknotsRGParser();
	    SecondaryStructure structure = parser.parse(rnafoldLines);
	    assert structure != null;
	    assert structure.getSequenceCount() == 1;
	    predictedInteractions1 = structure.toInteractionMatrix(0, 0); // there should be only one sequence
	    assert predictedInteractions1 != null && predictedInteractions1.length > 0;
	    assert predictedInteractions1.length == predictedInteractions1[0].length;
	    if (debugLevel > 2) {
		System.out.println("Interactions of single-strand evaluation:");
		IntegerArrayTools.writeMatrix(System.out, predictedInteractions1);
	    }

	} else {
	    String bracket = RnaFoldTools.parseRnafoldStructure(rnafoldLines);
	    predictedInteractions1 = RnaFoldTools.parseBracketInteractions(bracket);
	    energy = RnaFoldTools.parseRnafoldEnergy(rnafoldLines);
	}
	double result = 0.0;
	if (countMissedMode) {
	    result = structureWeight * RnaFoldTools.countMissedInteractions(interactions1, predictedInteractions1) / 2;
	} else {
	    result = structureWeight * RnaFoldTools.computeMatrixDifference(interactions1, predictedInteractions1) / 2;
	}
	if (!isInteracting(interactions1)) {
	    if (energy < selfEnergyCutoff) {
		result += selfEnergyCutoff - energy; // folding energy should be zero and not negative idealy if there are no intra-strand base pairs desired.
	    }

	}
	assert result >= 0.0;
	return result;
    }

    private void cacheSeqs(StringBuffer[] bseqs) {
	if ((lastSeqs == null) || (lastSeqs.length != bseqs.length)) {
	    lastSeqs = new StringBuffer[bseqs.length];
	}
	for (int i = 0; i < bseqs.length; ++i) {
	    lastSeqs[i] = new StringBuffer(bseqs[i].toString());
	}
    }
    
    /** array with "true" for sequences that have been modified from last call */
    boolean[] findModified(StringBuffer[] bseqs) {
	boolean[] result = new boolean[bseqs.length];
	for (int i = 0; i < bseqs.length; ++i) {
	    result[i] = true;
	}
	if ((lastSeqs == null) || (result.length != lastSeqs.length)) {
	    return result;
	}
	// System.out.println("findModified sequence comparison:");
	for (int i = 0; i < bseqs.length; ++i) {
	    assert(lastSeqs[i] != null);
	    assert(bseqs[i] != null);
	    result[i] = (lastSeqs[i].toString().compareTo(bseqs[i].toString()) != 0);
	    // System.out.println("" + (i+1) + " " + bseqs[i] + " " + lastSeqs[i] + " " + result[i]);
	}	
	return result;
    }

    private void initScoreCache(StringBuffer[] bseqs) {
	singleSequenceScoreCache = new double[bseqs.length];
	assert(singleSequenceScoreCache[0] == 0.0);
	sequencePairScoreCache = new double[bseqs.length][bseqs.length];
    }

    /** returns error score for complete secondary structure and trial sequences */
    public double scoreStructure(StringBuffer[] bseqs,
				 SecondaryStructure structure,
				 int[][][][] interactionMatrices) {
	String scoreString = generateReport(bseqs, structure, interactionMatrices,0).getProperty("score");
	assert scoreString != null;
	return Double.parseDouble(scoreString);
    }

    /** returns error score for complete secondary structure and trial sequences */
    public Properties generateReport(StringBuffer[] bseqs,
				     SecondaryStructure structure,
				     int[][][][] interactionMatrices,
				     int verbosity) {
	Properties resultProperties = new Properties();
	double score = 0.0;
	boolean[] modified = findModified(bseqs); // array with "yes" for sequences that have been modified from last call
	if ((singleSequenceScoreCache == null) || (singleSequenceScoreCache.length != bseqs.length)) {
	    initScoreCache(bseqs);
	}
	double term = 0.0;
	int termCount = 0;
	int cacheCount = 0;
	List<Integer> singles = new ArrayList<Integer>();
	List<Integer> pairsA = new ArrayList<Integer>();
	List<Integer> pairsB = new ArrayList<Integer>();

	String[] rnacofoldLines = new String[RNACOFOLD_BLOCK_SIZE]; // used for partitioning RNAcofold result output: 7 lines per sequence pair
	String[] rnafoldLines = new String[RNAFOLD_BLOCK_SIZE];
	for (int i = 0; i < bseqs.length; ++i) {
	    if (!structure.isActive(i)) {
		continue; // skip this sequence, it is not "active" in scoring
	    }
	    // score += scoreStructureSequence(bseqs[i], interactionMatrices[i][i]); // same sequence
	    if (interStrandMode) {
		for (int j = i + 1; j < bseqs.length; ++j) {
		    if (!structure.isActive(j)) {
			continue; // skip this sequence, it is not "active" in scoring
		    }
		    if (modified[i] || modified[j]) {
			pairsA.add(i); // autoboxing!
			pairsB.add(j); // autoboxing!
		    } else {
			score += sequencePairScoreCache[i][j];
			++cacheCount;
		    }
		    ++termCount;
		}
	    }
	    if (modified[i]) {
		// term = scoreStructureSingleSequence(bseqs[i], interactionMatrices[i][i]); // single sequence
		// singleSequenceScoreCache[i] = term;
                singles.add(i);
	    } else {
		score += singleSequenceScoreCache[i];
		++cacheCount;
	    }
	    ++termCount;
	}
	assert(pairsA.size() == pairsB.size());
	try {
	    if (pairsA.size() > 0) {
		StringBuffer rnacofoldInput = new StringBuffer();
		for (int k = 0; k < pairsA.size(); ++k) {
		    int i = pairsA.get(k);
		    int j = pairsB.get(k);
		    rnacofoldInput.append(generateFusedSequence(bseqs[i].toString(), bseqs[j].toString()) + NEWLINE);
		}
                String[] combinedResult = launchFolding(rnacofoldInput.toString());
		System.out.println("Combined RNAcofold result:");
		for (String s : combinedResult) {
		    System.out.println(s);
		}
		for (int k = 0; k < pairsA.size(); ++k) {
		    int i = pairsA.get(k);
		    int j = pairsB.get(k);
		    int startLine = k * RNACOFOLD_BLOCK_SIZE;
		    for (int kk = 0; kk < RNACOFOLD_BLOCK_SIZE; ++kk) {
			if ((startLine +kk) >= combinedResult.length) {
			    throw new ParseException("Combined RNAcofold result appears to be incomplete.", startLine+kk);
			}
			rnacofoldLines[kk] = combinedResult[startLine + kk];
		    }
		    term = scoreStructureSequencePair(bseqs[i], bseqs[j], interactionMatrices[i][j],
				      interactionMatrices[i][i], interactionMatrices[j][j], rnacofoldLines); // sequence pair
		    sequencePairScoreCache[i][j] = term;
		    sequencePairScoreCache[j][i] = term;
		    score += term;
		    System.out.println("Term for sequence pair " + i + " " + j + " " + term);
		}
	    }
	    StringBuffer rnafoldInput = new StringBuffer();
	    for (int k = 0; k < singles.size(); ++k) {
		int i = singles.get(k);
		rnafoldInput.append(bseqs[i]);
		rnafoldInput.append(NEWLINE);
	    }
	    System.out.println("RNAFOLD combined result:\n");
	    String[] rnafoldCombined = launchFolding(rnafoldInput.toString());
	    for (String s : rnafoldCombined) {
		System.out.println(s);
	    }
	    for (int k = 0; k < singles.size(); ++k) {
		int i = singles.get(k); // autoboxing
		int startLine = k * RNAFOLD_BLOCK_SIZE;
		for (int kk = 0; kk < RNAFOLD_BLOCK_SIZE; ++kk) {
		    if ((startLine +kk) >= rnafoldCombined.length) {
			throw new ParseException("Combined RNAfold result appears to be incomplete.", startLine+kk);
		    }
		    rnafoldLines[kk] = rnafoldCombined[startLine + kk];
		}
		term = scoreStructureSingleSequence(bseqs[i], interactionMatrices[i][i], rnafoldLines); // single sequence
		singleSequenceScoreCache[i] = term;
		score += term;
	    }
	}
	catch (java.io.IOException ioe) {
	    log.severe("Error launching RNAcofold! Error message: " + ioe.getMessage());
	    resultProperties.setProperty("error", "Error launching RNAcofold! Error message: " + ioe.getMessage());
	    assert false;
	    return resultProperties;
	}
	catch (ParseException  pe) {
	    log.severe("Error parsing RNAcofold results! Error message: " + pe.getMessage());
	    resultProperties.setProperty("error", "Error parsing RNAcofold! Error message: " + pe.getMessage());
	    assert false;
	    return resultProperties;
	}
	assert score >= 0.0;
	if ((subScorer != null) && (subScorerWeight != 0.0)) {
	    score += subScorerWeight * subScorer.scoreStructure(bseqs, structure, interactionMatrices);
	}
	assert(termCount > 0);
	cacheSeqs(bseqs); // store sequences
	// log.info("Fraction of cache usage: " + ((double)cacheCount/(double)termCount) + " " + cacheCount + " " + termCount);
	resultProperties.setProperty("score", "" + score);
	return resultProperties;
    }

    /** returns error score for complete secondary structure and trial sequences */
    public Properties generateReport(StringBuffer[] bseqs,
				     SecondaryStructure structure,
				     int[][][][] interactionMatrices) {
	return generateReport(bseqs, structure, interactionMatrices, 10); // very verbose
    }

    public void setEnergyGap(double value) { this.energyGap = value; }

    public void setEnergyMode(boolean b) { this.energyMode = b; }

    public void setInterStrandMode(boolean mode) { this.interStrandMode = mode; }

    /** If true, use pknotsRG, otherwise RNAcofold */
    public void setPkMode(boolean mode) { this.pkMode = mode; }

    public void setSubScorer(SecondaryStructureScorer scorer) {
	this.subScorer = scorer;
    }

    /** Sets cutof for self energy penalty */
    public void setSelfEnergyCutoff(double cutoff) { this.selfEnergyCutoff = cutoff; }

    /** Sets value of "structureWeight" variable */
    public void setStructureWeight(double weight) { this.structureWeight = weight; }

    /** Sets weight of sub scorer */
    public void setSubScorerWeight(double weight) {
	this.subScorerWeight = weight;
    }


}
