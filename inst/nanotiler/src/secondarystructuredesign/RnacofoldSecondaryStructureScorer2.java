package secondarystructuredesign;

import java.io.*;
import java.util.*;
import java.util.logging.*;
import java.util.ResourceBundle;
import java.text.ParseException;
import generaltools.StringTools;
import generaltools.ParsingException;
import numerictools.*;
import launchtools.*;
import rnasecondary.*;
import sequence.*;
import static secondarystructuredesign.PackageConstants.*;

/** Assigns a score to a secondary structure and a set of potential sequences */
public class RnacofoldSecondaryStructureScorer2 implements SecondaryStructureScorer {

    private Logger log = Logger.getLogger(LOGFILE_DEFAULT);
    private static ResourceBundle rb = ResourceBundle.getBundle(BUNDLE_NAME);
    private static String rnafoldScriptName = rb.getString("rnafoldscript");
    private static String rnacofoldScriptName = rb.getString("rnacofoldscript");
    private static String pknotsScriptName = rb.getString("pknotsscript");
    private boolean interStrandMode = true;
    private double energyAU = -0.8;
    private double energyGC = -1.2;
    private double overlapWeight = 1.0;
    private String linkerSequence = "&";
    private String pkLinkerSequence = "";
    private boolean energyMode = true; // if true, launch RNAcofold with option "-a" and compute energies of AB, AA, BB, A, B
    private boolean pkMode = false; // true;
    private int debugLevel = 2;
    private Level debugLogLevel = Level.FINE;
    private SecondaryStructureScorer subScorer;
    private double subScorerWeight = 1.0;
    private double selfEnergyCutoff = 0.0;
    private double structureWeight = 1.0;
    private double energyGap = 0.0; // require this minimum interaction energy for intercting strands
    private boolean countMissedMode = true; // only count missed interactions
    private StringBuffer[] lastSeqs = null;
    private double[] singleSequenceScoreCache = null;
    private double[][] sequencePairScoreCache = null;
    // public String getLinkerSequence() { return this.linkerSequence; }

    // public void setLinkerSequence(String s) { this.linkerSequence = s; }

    /** Dummy energies of Watson-Crick pairings: AU, GC, UA, CG (but not GU, UG) < 0, all others: 0.0 */
    public double interactionEnergy(char c1, char c2) {
	c1 = Character.toUpperCase(c1);
	c2 = Character.toUpperCase(c2);
	if (c1 == c2) {
	    return 0.0;
	}
	if (c1 > c2) {
	    return interactionEnergy(c2, c1);
	}
	switch (c1) {
	case 'A':
	    if (c2 == 'U') {
		return energyAU;
	    }
	case 'C':
	    if (c2 == 'G') {
		return energyGC;
	    }
	}
	return 0.0;
    }

    /** launches RNAcofold or pknotsRG with given sequence, returns the raw line output  */
    String[] launchFolding(String sequence) throws IOException {
	// write secondary structure to temporary file
	File tmpInputFile = File.createTempFile("nanotiler_rnafold",".seq");
	String inputFileName = tmpInputFile.getAbsolutePath();
	FileOutputStream fos = new FileOutputStream(tmpInputFile);
	PrintStream ps = new PrintStream(fos);
	// write secondary structure, but only write sec structure, not sequence:
	SecondaryStructureWriter secWriter = new SecondaryStructureScriptFormatWriter();
	log.log(debugLogLevel,"Writing to file: " + inputFileName);
	log.log(debugLogLevel,"Writing content: " + sequence);
	ps.println(sequence); // no special formatting needed!
	fos.close();
	File tmpOutputFile = File.createTempFile("nanotiler_rnafold", ".sec");
	final String outputFileName = tmpOutputFile.getAbsolutePath();
	if (tmpOutputFile.exists()) {
	    tmpOutputFile.delete();
	}

	// generate command string
	String scriptName = rnacofoldScriptName;
	if (pkMode) {
	    scriptName = pknotsScriptName;
	}
	else if (sequence.indexOf('&') < 0) { // using RNAfold instead of RNAcofold!
	    scriptName = rnafoldScriptName;
	}
	File tempFile = new File(scriptName);
	String[] commandWords = { scriptName, inputFileName, outputFileName}; 
	// generate command
	RunCommand command = new SimpleRunCommand(commandWords);
	log.log(debugLogLevel, "Issuing command: " + scriptName + " " + inputFileName + " " + outputFileName);
	// create queue manager (singleton pattern)
	QueueManager queueManager = SimpleQueueManager.getInstance();
	// generate job
	Job job = queueManager.createJob(command);
	
	// add listener to job !?
	
	// launch command
	queueManager.submit(job);	    
	log.log(debugLogLevel,"queue manager finished job!");
	// open output file:
	log.log(debugLogLevel,"Importing optimized sequences from " + outputFileName);
	String[] resultLines = null;
	FileInputStream resultFile = null;
	try {
	    resultFile = new FileInputStream(outputFileName);
	    resultLines = StringTools.readAllLines(resultFile);
	    // sequenceStrings = RnaFoldTools.getSequenceStrings(resultLines);
	}
	catch (IOException ioe) {
	    log.warning("Error when scraping result file from: " 
			+ outputFileName);
	    assert false;
	    throw ioe;
	}
	finally {
	    if (resultFile != null) {
		resultFile.close();
		File file = new File(outputFileName);
		file.delete();
	    }
	    if (tmpInputFile != null) {
		tmpInputFile.delete();
	    }
	}
	if (resultLines != null) {
	    log.log(debugLogLevel,"Results for RNAcofold:");
	    for (int i = 0; i < resultLines.length; i++) {
		log.fine(resultLines[i]);
	    }
	}
	else {
	    assert false;
	    log.warning("Rnacofold results were null!");
	}
	return resultLines;
    }
    
    public String getLinker() {
	if (pkMode) {
	    return pkLinkerSequence;
	}
	return linkerSequence;
    }

    /** fuses two sequences by adding linker sequence in middle */
    private String generateFusedSequence(String s1, String s2) {
	return s1 + getLinker() + s2;
    }

    /** convert nxm matrix (corresponding to two different sequences) to kxk matrix of fused sequence */
    private int[][] generateFusedInteractions(int[][] interactions) {
	int linkLen = getLinker().length();
	int l1 = interactions.length;
	int l2 = interactions[0].length;
	int fusedLen = l1 + linkLen + l2;
	assert fusedLen > 0;
	int[][] result = new int[fusedLen][fusedLen];
	for (int i = 0; i < result.length; ++i) {
	    for (int j = 0; j < result[0].length; ++j) {
		result[i][j] = RnaInteractionType.NO_INTERACTION;
	    }
	}
	for (int i = 0; i < interactions.length; ++i) {
	    for (int j = 0; j < interactions[0].length; ++j) {
		if (interactions[i][j] == RnaInteractionType.WATSON_CRICK) {
		    int id1 = i;
		    int id2 = j + l1 + linkLen;
		    assert id1 < result.length;
		    assert id2 < result[0].length;
		    
		    result[id1][id2] = RnaInteractionType.WATSON_CRICK;
		    result[id2][id1] = RnaInteractionType.WATSON_CRICK; // fused matrix is symmetric!
		}
	    }
	}
	return result;
    }
    
    /** Returns true if the two strands are interacting */
    private boolean isInteracting(int[][] interactions) {
	for (int i = 0; i < interactions.length; ++i) {
	    for (int j = i+1; j < interactions[i].length; ++j) {
		if (interactions[i][j] != RnaInteractionType.NO_INTERACTION) {
		    return true;
		}
	    }
	}
	return false;
    }

    /** Returns simply free energy of binding (dG) of RNAcofold */
    private double scoreStructureSequencePair2(StringBuffer bseq1,
					       StringBuffer bseq2,
					       int[][] interactions,
					       int[][] interactions1,
					       int[][] interactions2) throws IOException, ParseException {
	assert interactions.length == bseq1.length();
	assert interactions[0].length == bseq2.length();
// 	log.info("Scoring sequences " + NEWLINE + bseq1.toString() + NEWLINE + bseq2.toString());
// 	IntegerArrayTools.writeMatrix(System.out, interactions);
	// int[][] fusedInteractions = generateFusedInteractions(interactions, orderFlag);
	// assert fusedInteractions.length == fusedInteractions[0].length; // must be symmetric!
	String fusedSequence = "";
	int len1 = bseq1.length();
	int len2 = bseq2.length();
	fusedSequence = generateFusedSequence(bseq1.toString(), bseq2.toString());
	String[] rnafoldLines = launchFolding(fusedSequence);
	if (debugLogLevel.intValue() >= Level.INFO.intValue()) {
	    System.out.println("Output of folding algorithm:");
	    for (int i = 0; i < rnafoldLines.length; ++i) {
		System.out.println(rnafoldLines[i]);
	    }
	}
	double energyAB = RnaFoldTools.parseRnacofoldEnergyAB(rnafoldLines); // energy of AB
	double energyAA = RnaFoldTools.parseRnacofoldEnergyAA(rnafoldLines); // energy of AA
	double energyBB = RnaFoldTools.parseRnacofoldEnergyBB(rnafoldLines); // energy of BB
	double energyA = RnaFoldTools.parseRnacofoldEnergyA(rnafoldLines); // energy of A
	double energyB = RnaFoldTools.parseRnacofoldEnergyB(rnafoldLines); // energy of B
	double eDiff = energyAB - energyA - energyB;
	double eDiffGap = eDiff + energyGap;
	double eDiffAA = energyAB - energyAA;
	double eDiffBB = energyAB - energyBB;
	return eDiff; // returns free energy of binding . Careful: sign is in other direction than usual
    }

    private double scoreStructureSingleSequence(StringBuffer bseq1,
						int[][] interactions1) throws IOException, ParseException {
	assert interactions1.length == bseq1.length();
	assert interactions1[0].length == bseq1.length();
// 	log.info("Scoring sequences " + NEWLINE + bseq1.toString() + NEWLINE + bseq2.toString());
// 	IntegerArrayTools.writeMatrix(System.out, interactions);
	// int[][] fusedInteractions = generateFusedInteractions(interactions, orderFlag);
	// assert fusedInteractions.length == fusedInteractions[0].length; // must be symmetric!
	String fusedSequence = "";
	int len1 = bseq1.length();
	String[] rnafoldLines = launchFolding(bseq1.toString());
	int[][] predictedInteractions1 = null;
	double energy = 0.0;
	if (debugLogLevel.intValue() >= Level.INFO.intValue()) {
	    System.out.println("Output of single sequence folding algorithm:");
	    for (int i = 0; i < rnafoldLines.length; ++i) {
		System.out.println(rnafoldLines[i]);
	    }
	}
	if (pkMode) {
	    SecondaryStructureParser parser = new PknotsRGParser();
	    SecondaryStructure structure = parser.parse(rnafoldLines);
	    assert structure != null;
	    assert structure.getSequenceCount() == 1;
	    predictedInteractions1 = structure.toInteractionMatrix(0, 0); // there should be only one sequence
	    assert predictedInteractions1 != null && predictedInteractions1.length > 0;
	    assert predictedInteractions1.length == predictedInteractions1[0].length;
	    if (debugLevel > 2) {
		System.out.println("Interactions of single-strand evaluation:");
		IntegerArrayTools.writeMatrix(System.out, predictedInteractions1);
	    }

	} else {
	    String bracket = RnaFoldTools.parseRnafoldStructure(rnafoldLines);
	    predictedInteractions1 = RnaFoldTools.parseBracketInteractions(bracket);
	    energy = RnaFoldTools.parseRnafoldEnergy(rnafoldLines);
	}
	double result = 0.0;
	if (countMissedMode) {
	    result = structureWeight * RnaFoldTools.countMissedInteractions(interactions1, predictedInteractions1) / 2;
	} else {
	    result = structureWeight * RnaFoldTools.computeMatrixDifference(interactions1, predictedInteractions1) / 2;
	}
	if (!isInteracting(interactions1)) {
	    if (energy < selfEnergyCutoff) {
		result += selfEnergyCutoff - energy; // folding energy should be zero and not negative idealy if there are no intra-strand base pairs desired.
	    }

	}
	assert result >= 0.0;
	return result;
    }

    private void cacheSeqs(StringBuffer[] bseqs) {
	if ((lastSeqs == null) || (lastSeqs.length != bseqs.length)) {
	    lastSeqs = new StringBuffer[bseqs.length];
	}
	for (int i = 0; i < bseqs.length; ++i) {
	    lastSeqs[i] = new StringBuffer(bseqs[i].toString());
	}
    }
    
    /** array with "true" for sequences that have been modified from last call */
    boolean[] findModified(StringBuffer[] bseqs) {
	boolean[] result = new boolean[bseqs.length];
	for (int i = 0; i < bseqs.length; ++i) {
	    result[i] = true;
	}
	if ((lastSeqs == null) || (result.length != lastSeqs.length)) {
	    return result;
	}
	// System.out.println("findModified sequence comparison:");
	for (int i = 0; i < bseqs.length; ++i) {
	    assert(lastSeqs[i] != null);
	    assert(bseqs[i] != null);
	    result[i] = (lastSeqs[i].toString().compareTo(bseqs[i].toString()) != 0);
	    // System.out.println("" + (i+1) + " " + bseqs[i] + " " + lastSeqs[i] + " " + result[i]);
	}	
	return result;
    }

    private void initScoreCache(StringBuffer[] bseqs) {
	singleSequenceScoreCache = new double[bseqs.length];
	assert(singleSequenceScoreCache[0] == 0.0);
	sequencePairScoreCache = new double[bseqs.length][bseqs.length];
    }


    /** returns error score for complete secondary structure and trial sequences */
    public Properties generateReport(StringBuffer[] bseqs,
				     SecondaryStructure structure,
				     int[][][][] interactionMatrices) {
	assert false; // not implemented TODO
	return null;
    }

    /** returns error score for complete secondary structure and trial sequences */
    public double scoreStructure(StringBuffer[] bseqs,
				 SecondaryStructure structure,
				 int[][][][] interactionMatrices) {
	double score = 0.0;
	if (bseqs.length < 2) {
	    return 0.0;
	}
	boolean[] modified = findModified(bseqs); // array with "yes" for sequences that have been modified from last call
	if ((singleSequenceScoreCache == null) || (singleSequenceScoreCache.length != bseqs.length)) {
	    initScoreCache(bseqs);
	}
	double term = 0.0;
	int termCount = 0;
	int cacheCount = 0;
	List<Double> highScores = new ArrayList<Double>();
	List<Double> lowScores = new ArrayList<Double>();
	try {
	    for (int i = 0; i < bseqs.length; ++i) {
		if (!structure.isActive(i)) {
		    continue; // skip this sequence, it is not "active" in scoring
		}
		// score += scoreStructureSequence(bseqs[i], interactionMatrices[i][i]); // same sequence
		if (interStrandMode) {
		    for (int j = i + 1; j < bseqs.length; ++j) {
			if (!structure.isActive(j)) {
			    continue; // skip this sequence, it is not "active" in scoring
			}
			if (modified[i] || modified[j]) {
			    term = scoreStructureSequencePair2(bseqs[i], bseqs[j], interactionMatrices[i][j],
							       interactionMatrices[i][i], interactionMatrices[j][j]); // sequence pair
			    sequencePairScoreCache[i][j] = term;
			    sequencePairScoreCache[j][i] = term;
			} else {
			    term = sequencePairScoreCache[i][j];
			    ++cacheCount;
			}
			// score += term;
			if (isInteracting(interactionMatrices[i][j])) {
			    // System.out.println("Interacting strands " + (i+1) + " " + (j+1) + " have interaction score: " + term);
			    lowScores.add(term);
			} else {
			    highScores.add(term);
			    // System.out.println("Non-Interacting strands " + (i+1) + " " + (j+1) + " have interaction score: " + term);
			}
			++termCount;
		    }
		}
		if (modified[i]) {
		    term = scoreStructureSingleSequence(bseqs[i], interactionMatrices[i][i]); // single sequence
		    singleSequenceScoreCache[i] = term;
		} else {
		    term = singleSequenceScoreCache[i];
		    ++cacheCount;
		}
		score += term;
		++termCount;
	    }
	}
	catch (java.io.IOException ioe) {
	    log.severe("Error launching RNAcofold! Error message: " + ioe.getMessage());
	    assert false;
	    return 0.0;
	}
	catch (ParseException  pe) {
	    log.severe("Error parsing RNAcofold results! Error message: " + pe.getMessage());
	    assert false;
	    return 0.0;
	}
	Collections.sort(highScores);
	Collections.sort(lowScores);
// 	System.out.println("High scores (non-interacting sequence pairs: " );
// 	for (int i = 0; i < highScores.size(); ++i){
// 	    System.out.println(highScores.get(i));
// 	}
// 	System.out.println("Low scores (interacting sequence pairs: " );
// 	for (int i = 0; i < lowScores.size(); ++i){
// 	    System.out.println(lowScores.get(i));
// 	}
	double overlapTerm = 0.0;
	if ((bseqs.length > 0) && (highScores.size() > 0) && (lowScores.size() > 0)) {
	    overlapTerm = DoubleArrayTools.scoreOverlap(highScores, lowScores);
	}
	score += overlapWeight * overlapTerm;
	// System.out.println("Overlap term: " + overlapTerm);
	assert score >= 0.0;
	if ((subScorer != null) && (subScorerWeight != 0.0)) {
	    score += subScorerWeight * subScorer.scoreStructure(bseqs, structure, interactionMatrices);
	}
	assert(termCount > 0);
	cacheSeqs(bseqs); // store sequences
	// log.info("Fraction of cache usage: " + ((double)cacheCount/(double)termCount) + " " + cacheCount + " " + termCount);
	return score;

    }

    public void setEnergyGap(double value) { this.energyGap = value; }

    public void setEnergyMode(boolean b) { this.energyMode = b; }

    public void setInterStrandMode(boolean mode) { this.interStrandMode = mode; }

    /** If true, use pknotsRG, otherwise RNAcofold */
    public void setPkMode(boolean mode) { this.pkMode = mode; }

    public void setSubScorer(SecondaryStructureScorer scorer) {
	this.subScorer = scorer;
    }

    /** Sets cutof for self energy penalty */
    public void setSelfEnergyCutoff(double cutoff) { this.selfEnergyCutoff = cutoff; }

    /** Sets value of "structureWeight" variable */
    public void setStructureWeight(double weight) { this.structureWeight = weight; }

    /** Sets weight of sub scorer */
    public void setSubScorerWeight(double weight) {
	this.subScorerWeight = weight;
    }


}
