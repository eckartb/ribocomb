package secondarystructuredesign;

import java.io.*;
import java.util.Properties;
import java.util.logging.*;
import java.util.ResourceBundle;
import generaltools.StringTools;
import generaltools.ParsingException;
import numerictools.*;
import launchtools.*;
import rnasecondary.*;
import static secondarystructuredesign.PackageConstants.*;

/** Assigns a score to a secondary structure and a set of potential sequences */
public class RnafoldSecondaryStructureScorer implements SecondaryStructureScorer {

    private Logger log = Logger.getLogger(LOGFILE_DEFAULT);
    private static ResourceBundle rb = ResourceBundle.getBundle(BUNDLE_NAME);
    private static String rnaFoldScriptName = rb.getString("rnafoldscript");
    private double energyAU = -0.8;
    private double energyGC = -1.2;
    private String linkerSequence = "AAAA";

    public String getLinkerSequence() { return this.linkerSequence; }

    public void setLinkerSequence(String s) { this.linkerSequence = s; }

    /** Dummy energies of Watson-Crick pairings: AU, GC, UA, CG (but not GU, UG) < 0, all others: 0.0 */
    public double interactionEnergy(char c1, char c2) {
	c1 = Character.toUpperCase(c1);
	c2 = Character.toUpperCase(c2);
	if (c1 == c2) {
	    return 0.0;
	}
	if (c1 > c2) {
	    return interactionEnergy(c2, c1);
	}
	switch (c1) {
	case 'A':
	    if (c2 == 'U') {
		return energyAU;
	    }
	case 'C':
	    if (c2 == 'G') {
		return energyGC;
	    }
	}
	return 0.0;
    }

    /** launches RNAfold with given sequence, returns the raw RNAfold line output  */
    String[] launchRnafold(String sequence) throws IOException {
	// write secondary structure to temporary file
	File tmpInputFile = File.createTempFile("nanotiler_rnafold",".sec");
	String inputFileName = tmpInputFile.getAbsolutePath();
	FileOutputStream fos = new FileOutputStream(tmpInputFile);
	PrintStream ps = new PrintStream(fos);
	// write secondary structure, but only write sec structure, not sequence:
	SecondaryStructureWriter secWriter = new SecondaryStructureScriptFormatWriter();
	log.fine("Writing to file: " + inputFileName);
	log.fine("Writing content: " + sequence);
	ps.println(sequence); // no special formatting needed!
	fos.close();
	File tmpOutputFile = File.createTempFile("nanotiler_rnafold", ".seq");
	final String outputFileName = tmpOutputFile.getAbsolutePath();
	if (tmpOutputFile.exists()) {
	    tmpOutputFile.delete();
	}

	// generate command string
	File tempFile = new File(rnaFoldScriptName);
	// log.info("Path to RNAinverse script: " + tempFile.getAbsolutePath());
	String[] commandWords = {rnaFoldScriptName, inputFileName, outputFileName}; 
	// generate command
	RunCommand command = new SimpleRunCommand(commandWords);
	
	// create queue manager (singleton pattern)
	QueueManager queueManager = SimpleQueueManager.getInstance();
	// generate job
	Job job = queueManager.createJob(command);
	
	// add listener to job !?
	
	// launch command
	queueManager.submit(job);	    
	log.fine("queue manager finished job!");
	// open output file:
	log.fine("Importing optimized sequences from " + outputFileName);
	String[] resultLines = null;
	try {
	    FileInputStream resultFile = new FileInputStream(outputFileName);
	    resultLines = StringTools.readAllLines(resultFile);
	    // sequenceStrings = RnaFoldTools.getSequenceStrings(resultLines);
	}
	catch (IOException ioe) {
	    log.warning("Error when scraping result file from: " 
			+ outputFileName);
	    throw ioe;
	}

	return resultLines;
    }

    private double computeMatrixMatthews(int[][] trueInteractions,
					 int[][] predictedInteractions) {
	assert trueInteractions.length == predictedInteractions.length;
	assert trueInteractions[0].length == predictedInteractions[0].length;
	int tp = 0;
	int fp = 0;
	int tn = 0;
	int fn = 0;
	for (int i = 0; i < trueInteractions.length; ++i) {
	    for (int j = i+1; j < trueInteractions[0].length; ++j) {
		int pv = predictedInteractions[i][j];
		int tv = trueInteractions[i][j];
		switch (tv) {
		case RnaInteractionType.WATSON_CRICK:
		    if (pv == RnaInteractionType.WATSON_CRICK) {
			++tp;
		    }
		    else {
			++fn; // missed interaction
		    }
		    break;
		case RnaInteractionType.NO_INTERACTION:
		    if (pv == RnaInteractionType.WATSON_CRICK) {
			++fp;
		    }
		    else {
			++tn; // missed interaction
		    }
		    break;
		default:
		    assert false; // should never happen
		}
	    }
	}
	return AccuracyTools.computeMatthews(tp, fp, tn, fn);
    }

    private double computeMatrixDifference(int[][] trueInteractions,
					 int[][] predictedInteractions) {
	assert trueInteractions.length == predictedInteractions.length;
	assert trueInteractions[0].length == predictedInteractions[0].length;
	int count = 0;
	for (int i = 0; i < trueInteractions.length; ++i) {
	    for (int j = 0; j < trueInteractions[0].length; ++j) {
		if (trueInteractions[i][j] != predictedInteractions[i][j]) {
		    ++count;
		}
	    }
	}
	return (double)count;
    }


    private int findInnerInteraction(String s, int n) {
	if (s.charAt(n) != '(') {
	    return -1;
	}
	for (int i = n+1; i < s.length(); ++i) {
	    if (s.charAt(i) == ')') {
		return i;
	    }
	    else if (s.charAt(i) == '(') {
		return -1; // is not inner interaction
	    }
	}
	return -1; // no closing bracket found
    }
    
    private IntervalInt findInnerInteraction(String s) {
	for (int i = 0; i < s.length(); ++i) {
	    int j = findInnerInteraction(s, i);
	    if (j >= 0) {
		return new IntervalInt(i,j);
	    }
	}
	return null;
    }


    /** parses structure string in bracket notation, adds found "(" and ")" parenthesis to interaction set.
     * It return nxn matrix with filled with RnaInteraction.NO_INTERACTION or RnaInteraction.WATSON_CRICK */
    private int[][] parseBracketInteractions(String structure) {
	assert structure != null;
	int len = structure.length();
	int[][] result = new int[len][len];
	for (int i = 0; i < result.length; ++i) {
	    for (int j = 0; j < result[i].length; ++j) {
		result[i][j] = RnaInteractionType.NO_INTERACTION;
	    }
	}
	int numInteractions = StringTools.countChar(structure, '(');
	StringBuffer buf = new StringBuffer(structure);
	int foundInteractions = 0;
	while (foundInteractions < numInteractions) {
	    IntervalInt intervall = findInnerInteraction(buf.toString());
	    if (intervall == null) {
		log.info("Could not find inner interactions in: " + structure);
		break; // could not find more interactions
	    }
	    int j = intervall.getLower();
	    int k = intervall.getUpper();
	    log.fine("Found interaction");
	    log.fine("Structure: " + structure);
	    log.fine("Interaction: " + j + "   " + k);
	    // found interaction
	    result[j][k] = RnaInteractionType.WATSON_CRICK;
	    result[k][j] = RnaInteractionType.WATSON_CRICK; // symmetry
	    buf.setCharAt(j, '.');
	    buf.setCharAt(k, '.'); // delete 
	    foundInteractions++;
	}
	assert foundInteractions == numInteractions;
	return result;
    }

    private double scoreStructureSequence(StringBuffer bseq,
					  int[][] interactions) throws IOException, ParsingException {
	assert interactions.length == bseq.length();
	double score = 0.0;
	String[] rnafoldLines = launchRnafold(bseq.toString());
	String bracket = RnaFoldTools.parseRnafoldStructure(rnafoldLines);
	int[][] predictedInteractions = parseBracketInteractions(bracket);
	double result = computeMatrixDifference(interactions, predictedInteractions) / 2; // is symmetric!
	log.fine("Matrix difference for comparing desired and predicted structure: "
		 + result + " " + bracket);
	return result;
    }
    
    /** fuses two sequences by adding linker sequence in middle */
    private String generateFusedSequence(String s1, String s2) {
	return s1 + linkerSequence + s2;
    }

    /** convert nxm matrix (corresponding to two different sequences) to kxk matrix of fused sequence */
    private int[][] generateFusedInteractions(int[][] interactions, boolean orderFlag) {
	int linkLen = linkerSequence.length();
	int l1 = interactions.length;
	int l2 = interactions[0].length;
	int fusedLen = l1 + linkLen + l2;
	assert fusedLen > 0;
	int[][] result = new int[fusedLen][fusedLen];
	for (int i = 0; i < result.length; ++i) {
	    for (int j = 0; j < result[0].length; ++j) {
		result[i][j] = RnaInteractionType.NO_INTERACTION;
	    }
	}
	for (int i = 0; i < interactions.length; ++i) {
	    for (int j = 0; j < interactions[0].length; ++j) {
		if (interactions[i][j] == RnaInteractionType.WATSON_CRICK) {
		    int id1 = i;
		    int id2 = j + l1 + linkLen;
		    if (! orderFlag) {
			id1 = j;
			id2 = i + l2 + linkLen;
		    }
		    assert id1 < result.length;
		    assert id2 < result[0].length;
		    
		    result[id1][id2] = RnaInteractionType.WATSON_CRICK;
		    result[id2][id1] = RnaInteractionType.WATSON_CRICK; // fused matrix is symmetric!
		}
	    }
	}
	return result;
    }

    private double scoreStructureSequencePair(StringBuffer bseq1,
					      StringBuffer bseq2,
					      int[][] interactions,
					      boolean orderFlag) throws IOException, ParsingException {
	assert interactions.length == bseq1.length();
	assert interactions[0].length == bseq2.length();
// 	log.info("Scoring sequences " + NEWLINE + bseq1.toString() + NEWLINE + bseq2.toString());
// 	IntegerArrayTools.writeMatrix(System.out, interactions);
	int[][] fusedInteractions = generateFusedInteractions(interactions, orderFlag);
	assert fusedInteractions.length == fusedInteractions[0].length; // must be symmetric!
	String fusedSequence = "";
	if (orderFlag) {
	    fusedSequence = generateFusedSequence(bseq1.toString(), bseq2.toString());
	}
	else {
	    fusedSequence = generateFusedSequence(bseq2.toString(), bseq1.toString());
	}
	String[] rnafoldLines = launchRnafold(fusedSequence);
	String bracket = RnaFoldTools.parseRnafoldStructure(rnafoldLines);
	int[][] predictedInteractions = parseBracketInteractions(bracket);
	double result = computeMatrixDifference(fusedInteractions, predictedInteractions) / 2;
	log.fine("Matrix difference for comparing fused desired and predicted structure: "
		 + result + " " + bracket);
	return result;
    }

    /** returns error score for complete secondary structure and trial sequences */
    public double scoreStructure(StringBuffer[] bseqs,
				 SecondaryStructure structure,
				 int[][][][] interactionMatrices) {

	String scoreString = generateReport(bseqs, structure, interactionMatrices, 0).getProperty("score");
	assert scoreString != null;
	return Double.parseDouble(scoreString);
    }

    /** returns error score for complete secondary structure and trial sequences */
    public Properties generateReport(StringBuffer[] bseqs,
				     SecondaryStructure structure,
				     int[][][][] interactionMatrices) {
	return generateReport(bseqs, structure, interactionMatrices, 10);
    }

    /** returns error score for complete secondary structure and trial sequences */
    public Properties generateReport(StringBuffer[] bseqs,
				     SecondaryStructure structure,
				     int[][][][] interactionMatrices, 
				     int verbosity) {
	Properties resultProperties =new Properties();
	double score = 0.0;
	try {
	    for (int i = 0; i < bseqs.length; ++i) {
		score += scoreStructureSequence(bseqs[i], interactionMatrices[i][i]); // same sequence
		for (int j = i+1; j < bseqs.length; ++j) {
		    // symmetrize:
		    score += 0.5 * scoreStructureSequencePair(bseqs[i], bseqs[j], interactionMatrices[i][j], true); // sequence pair
		    score += 0.5 * scoreStructureSequencePair(bseqs[i], bseqs[j], interactionMatrices[i][j], false); // sequence pair
		}
	    }
	}
	catch (java.io.IOException ioe) {
	    log.severe("Error launching RNAfold! Error message: " + ioe.getMessage());
	    return resultProperties;
	}
	catch (ParsingException  pe) {
	    log.severe("Error parsing RNAfold results! Error message: " + pe.getMessage());
	    return resultProperties;
	}
	resultProperties.setProperty("score", "" + score);
	return resultProperties;

    }

}
