package SecondaryStructureDesign;

import java.util.logging.Logger;
import java.util.Properties;
import sequence.*;
import rnasecondary.*;
import generaltools.Namable;

import static rnadesign.rnamodel.PackageConstants.*;

public class SecondaryStructureDebugWriter implements SecondaryStructureWriter {

    public static final char NO_BASE_PAIR_CHAR = '.';

    public static Logger log = Logger.getLogger("NanoTiler_debug");

    private boolean writeSequenceMode = true;

    private boolean purgeMultipleInteractions = true; // if true, remove interactions that cannot be drawn as paranthesis


    public SecondaryStructureDebugWriter() {
	log.info("Started SecondaryStructureDebugWriter");
    }

    /** sets character at position pos */
    private static String setChar(String s, char c, int pos) {
	if (pos >= s.length()) {
	    return s;
	}
	String result = s.substring(0, pos) + c + s.substring(pos+1, s.length());
	assert result.length() == s.length();
	return result;
    }

    private String writeSequence(SecondaryStructure structure, int seqId) {
	Sequence sequence = structure.getSequence(seqId);
	String result = sequence.sequenceString();
	return result;
    }

    /** returns full sequence name, checks if part of Object3D hierarchy */
//     public static String sequenceFullName(Sequence seq) {
// 	String result = seq.getName();
// 	if (seq.getParentObject() instanceof Object3D) {
// 	    Object3D parent = (Object3D)(seq.getParentObject());
// 	    result = Object3DTools.getFullName(parent) + "." + result;
// 	}
// 	return result;
//     }

    /** returns full sequence name, checks if part of Object3D hierarchy */
//     public static String sequenceParentName(Sequence seq) {
// 	String result = seq.getName();
// 	if (seq.getParentObject() instanceof Object3D) {
// 	    Object3D parent = (Object3D)(seq.getParentObject());
// 	    result = parent.getName() + "." + result;
// 	}
// 	return result;
//     }

    /** returns index of sequence to which residue belongs
     * TODO : very slow implementation ! 
     * TODO : contains BUG !!! */
    private int findSequence(Residue res, SecondaryStructure structure) {

 	assert res != null;
 	assert structure != null;
 	// Sequence seq = res.getSequence();
 	// assert seq != null;
 	// String seqName = sequenceParentName(seq);
 	// assert seqName != null;
 	for (int i = 0; i < structure.getSequenceCount(); ++i) {
	    Sequence seq = structure.getSequence(i);
	    Residue other = seq.getResidue(0);
	    if (res.isSameSequence(other)) {
		return i;
	    }
//  	    // String seqNameOther = sequenceParentName(structure.getSequence(i));
//  	    // if complete name is identical:
//  	    log.info("Comparing: " + seqName + " " + seqNameOther);
//  	    if (seqName.equals(seqNameOther)) {
//  		log.info("Identical names found! " + i);
//  		return i;
//  	    }
 	}
 	return -1;
    }

    private void addInteraction(Interaction interaction, 
				Interaction[][] interactionArray,
				SecondaryStructure structure) {
	Residue res1 = interaction.getResidue1();
	Residue res2 = interaction.getResidue2();
	assert res1 != null;
 	assert res2 != null;
	int seqId1 = findSequence(res1, structure);
	int seqId2 = findSequence(res2, structure);
	if ((seqId1 >= 0) && (seqId2 >= 0)) {
// 	    log.info("Adding interaction(1): " + seqId1 + " " 
// 			       + res1.getPos() + " " + interaction);
// 	    log.info("Adding interaction(2): " + seqId2 + " " 
// 			       + res2.getPos() + " " + interaction);
	    interactionArray[seqId1][res1.getPos()] = interaction;
	    interactionArray[seqId2][res2.getPos()] = interaction;
	}
	else {
	    log.warning("could not find: " + res1.getSymbol() + res1.getPos() + " " 
			+ res2.getSymbol() + res2.getPos());
// 	    log.fine("Respective sequences:");
// 	    log.fine("" + res1.getSequence());
// 	    log.fine(sequenceParentName(res1.getSequence()));
// 	    log.fine("" + res2.getSequence());
// 	    log.fine(sequenceParentName(res2.getSequence()));
// 	    for (int i = 0; i < structure.getSequenceCount(); ++i) {
// 		log.finest("" + (i+1) + " : " + structure.getSequence(i));
// 		log.finest(sequenceParentName(structure.getSequence(i)));
// 	    }
	}
    }

    /** returns 'B' when given 'A' etc */
    private char incChar(char c) {
	int n = (int)c;
	++n;
	return (char)n;
    }

    /** returns 'A' when given 'B' etc */
    private char decChar(char c) {
	int n = (int)c;
	--n;
	return (char)n;
    }

    private double[][] generateInteractionMatrix(Interaction[][] interactionArray,
						 int seqId1, 
						 int seqId2,
						 SecondaryStructure structure) {
	
	Sequence sequence1 = structure.getSequence(seqId1);
	Sequence sequence2 = structure.getSequence(seqId2);
	int size1 = sequence1.size();
	int size2 = sequence2.size();
	double[][] matrix = new double[size1][size2];
	for (int i = 0; i < matrix.length; ++i) {
	    for (int j = 0; j < matrix[0].length; ++j) {
		matrix[i][j] = 0;
	    }
	}
	for (int i = 0; i < size1; ++i) {
	    Interaction interaction = interactionArray[seqId1][i];
	    if (interaction == null) {
		continue;
	    }
	    Residue res1 = interaction.getResidue1();
	    Residue res2 = interaction.getResidue2();
	    int sid1 = findSequence(res1, structure);
	    int sid2 = findSequence(res2, structure);
	    if ((sid1 == seqId1) && (sid2 == seqId2)) {
		assert res1.getPos() < matrix.length;
		assert res2.getPos() < matrix[res1.getPos()].length;
		matrix[res1.getPos()][res2.getPos()] = 1.0;
	    }
	    else if ( ((sid1 == seqId2) && (sid2 == seqId1))) {
		assert res2.getPos() < matrix.length;
		assert res1.getPos() < matrix[res2.getPos()].length;
		matrix[res2.getPos()][res1.getPos()] = 1.0;
	    }

	}
	return matrix;
    }


    String writeInteraction(Interaction interaction) {
	StringBuffer result = new StringBuffer();
	Residue residue1 = interaction.getResidue1();
	Residue residue2 = interaction.getResidue2();
	String sname1 = null;
	String sname2 = null;
	if (residue1.getParentObject() instanceof Namable) {
	    sname1 = ((Namable)(residue1.getParentObject())).getName();
	    sname2 = ((Namable)(residue2.getParentObject())).getName();
	}
	result.append(sname1 + interaction.getResidue1().getPos());
	result.append(":");
	result.append(sname2 + interaction.getResidue2().getPos());
	return result.toString();
    }

    /** generates string from secondary structure */
    public String writeString(SecondaryStructure structure) {
	if ((structure == null) || (structure.getSequenceCount() == 0)) {
	    return "";
	}
	StringBuffer buf = new StringBuffer();
	for (int i = 0; i < structure.getSequenceCount(); ++i) {	    
	    buf.append(structure.getSequence(i).sequenceString());
	    buf.append(NEWLINE);
	}
	for (int i = 0; i < structure.getInteractionCount(); ++i) {
	    Interaction interaction = structure.getInteraction(i);
	    if (interaction.getInteractionType().getSubTypeId() != RnaInteractionType.BACKBONE) {
		buf.append(writeInteraction(interaction) + " ");

	    }
	}

	return buf.toString();
    }

}
