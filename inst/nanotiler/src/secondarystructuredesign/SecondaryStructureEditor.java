package SecondaryStructureDesign;

import rnasecondary.SimpleMutableSecondaryStructure;

import javax.swing.*;

public class SecondaryStructureEditor {
    
    public static void main(String[] args) {

	SwingUtilities.invokeLater(new Runnable() {

		public void run() {
		    JFrame frame = new JFrame("Secondary Structure Editor");
		    frame.add(new SecondaryStructureEditorPanel(new SimpleMutableSecondaryStructure(), frame));
		    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		    frame.pack();
		    frame.setVisible(true);

		}

	    });


    }


}
