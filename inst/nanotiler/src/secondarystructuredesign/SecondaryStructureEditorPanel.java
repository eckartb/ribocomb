package SecondaryStructureDesign;

import generaltools.Randomizer;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.text.ParseException;
import javax.swing.*;
import rnasecondary.*;
import tools3d.objects3d.*;
import rnadesign.rnamodel.*;
import sequence.*;
import javax.swing.event.*;
import controltools.*;
import java.io.*;
import java.util.regex.*;
import commandtools.*;


public class SecondaryStructureEditorPanel extends JPanel {

    public static final String NEWLINE = System.getProperty("line.separator");
        
    private boolean sameSequence = false;

    private MutableSecondaryStructure structure;
    
    private Sequence sequence1, sequence2;
    
    private ColorGradientMixer mixer;
    
    private InteractionSet modifiedInteractions = new SimpleInteractionSet();
    private InteractionSet existingInteractions;
    private InteractionSet conflictingInteractions = new SimpleInteractionSet();
    
    private boolean interactionsModified = false;
    
    private final HashMap<Integer, Color> sequenceColorMap =
	new HashMap<Integer, Color>();
    
    // gui components
    private JList sequenceSelector1;
    private JList sequenceSelector2;
    private JScrollPane scrollPane;

    private StemCreationDisplay display = null;

    private JButton closeButton, commitButton, clearButton, clearAllButton,  newSequenceButton, editSequenceButton, importButton, exportButton;

    private JComboBox sequenceEditorSelector = null;

    private JPanel pane;


    private Window w;

    private UnevenAlignment sequences;

    private CommandApplication application;
    
    public SecondaryStructureEditorPanel(MutableSecondaryStructure structure, Window w, CommandApplication application) {
	this.application = application;
	this.structure = structure;
	this.w = w;
	sequences = structure.getSequences();
	existingInteractions = structure.getInteractions();
	createAndShowGUI();
    }


    public SecondaryStructureEditorPanel(MutableSecondaryStructure structure, Window w) {
	this(structure, w, null);

    }

    public MutableSecondaryStructure getSecondaryStructure() {
	return structure;
    }


    private void createAndShowGUI() {
	System.out.println("Creating GUI");

	setLayout(new BorderLayout());

	importButton = new JButton("Import");
	importButton.setToolTipText("Import an existing secondary structure for modification");
	importButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    JFileChooser chooser = new JFileChooser();
		    chooser.setApproveButtonText("Import");
		    chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		    File wd = new File("./");
		    chooser.setCurrentDirectory(wd);
		    int returnVal = chooser.showOpenDialog(SecondaryStructureEditorPanel.this);
		    if(returnVal == JFileChooser.APPROVE_OPTION) {
			File selectedFile = chooser.getSelectedFile();
			if(selectedFile.isFile()) {
			    ImprovedSecondaryStructureParser parser = 
				new ImprovedSecondaryStructureParser();
			    MutableSecondaryStructure ss = null;
			    try {
				ss = parser.parse(selectedFile.getAbsolutePath());
			    }
			    catch (IOException ioe) {
				System.out.println("IOException encountered in " + selectedFile.getAbsolutePath() 
						    + " : " + ioe.getMessage());
			    }
			    catch (ParseException pe) {
				System.out.println("Parsing exception encountered in " + selectedFile.getAbsolutePath() 
						    + " : " + pe.getMessage());
			    }
			    if(ss != null) {
				System.out.println("Successfully parsed input");
				InteractionSet in = ss.getInteractions();
				UnevenAlignment aln = ss.getSequences();
				
				for(int i = 0; i < aln.getSequenceCount(); ++i) {
				    try {
					structure.addSequence(aln.getSequence(i));
				    } catch(DuplicateNameException ex) {
					int j = 0;
					while(j < in.size()) {
					    Interaction interaction = in.get(j);
 					    if(interaction.getSequence1().equals(aln.getSequence(i)) 
					       || interaction.getSequence2().equals(aln.getSequence(i)))
 						in.remove(interaction);
 					    else
 						++j;
					    
					}
					
				    }
				}

				for(int i = sequences.getSequenceCount() - 1; i > sequences.getSequenceCount() - aln.getSequenceCount() - 1; --i)
				    sequenceColorMap.put(i, mixer.nextColor());

				
				for(int i = 0; i < in.size(); ++i)
				    structure.addInteraction(in.get(i));
				
				sequenceSelector1.setModel(new SequenceListModel());
				sequenceSelector2.setModel(new SequenceListModel());
				scrollPane.getViewport().revalidate();
				repaint();
			    }
			    

			}
		    }
		    
		}
	    });

	exportButton = new JButton("Export");
	exportButton.setToolTipText("Export secondary structure");
	exportButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    JFileChooser chooser = new JFileChooser();
		    chooser.setApproveButtonText("Save");
		    chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		    String cwdName = "./";
		    File cwd = new File(cwdName);
		    chooser.setCurrentDirectory(cwd);
		    int returnVal = chooser.showSaveDialog(SecondaryStructureEditorPanel.this); // parent);    	    
		    if(returnVal == JFileChooser.APPROVE_OPTION) {
			String fileName = chooser.getCurrentDirectory() 
			    // won't run on Windows! fix! TODO
			    + "/" + chooser.getSelectedFile().getName();
			
			// open output file
			FileOutputStream fis = null;
			try {
			    fis = new FileOutputStream(fileName);
			    PrintWriter pwtr = new PrintWriter(fis);
			    SecondaryStructureScriptFormatWriter writer =
				new SecondaryStructureScriptFormatWriter();
			    pwtr.println(writer.writeString(structure));
			    pwtr.close();
			}
			catch (IOException exc) {
			    // replace with better error window
			    JOptionPane.showMessageDialog(null, "Could not open file: " + exc.getMessage());
			}
		    }
		}
	    });
	
	sequenceSelector1 = new JList(new SequenceListModel());
	sequenceSelector2 = new JList(new SequenceListModel());
	sequenceSelector1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	sequenceSelector2.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

	Dimension listDimension = new Dimension(150, 200);
	sequenceSelector1.setPreferredSize(listDimension);
	sequenceSelector2.setPreferredSize(listDimension);

	JPanel selectorPanel = new JPanel();
	selectorPanel.setLayout(new BoxLayout(selectorPanel, BoxLayout.Y_AXIS));
	JPanel importExport = new JPanel();
	importExport.setLayout(new BoxLayout(importExport, BoxLayout.X_AXIS));
	importExport.add(importButton);
	importExport.add(Box.createHorizontalStrut(15));
	importExport.add(exportButton);
	importExport.setMaximumSize(new Dimension(importExport.getMaximumSize().width, importExport.getPreferredSize().height));
	
	selectorPanel.add(importExport);	       
	selectorPanel.add(Box.createVerticalStrut(10));
	selectorPanel.add(new JLabel("Sequence 1"));
	selectorPanel.add(new JScrollPane(sequenceSelector1));
	selectorPanel.add(Box.createVerticalStrut(15));
	selectorPanel.add(new JLabel("Sequence 2"));
	selectorPanel.add(new JScrollPane(sequenceSelector2));
	selectorPanel.setPreferredSize(new Dimension(200, 450));
	
	JPanel controlPanel = new JPanel();
	controlPanel.setLayout(new BoxLayout(controlPanel, BoxLayout.X_AXIS));
	
	
	commitButton = new JButton("Commit");
	commitButton.setToolTipText("Commit new interactions");
	commitButton.setEnabled(false);
	commitButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    saveInteractions();
		}
	    });

	clearButton = new JButton("Clear");
	clearButton.setToolTipText("Clear new interactions");
	clearButton.setEnabled(false);
	clearButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    if(discardModifiedInteractions()) {
			updateInteractions();
		
			display.repaint();
		    }

		}
	    });

	clearAllButton = new JButton("Clear All");
	clearAllButton.setToolTipText("Clear all interactions");
	clearAllButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    existingInteractions.clear();
		    modifiedInteractions.clear();
		    conflictingInteractions.clear();

		    display.repaint();

		}
	    });

	closeButton = new JButton("Close");
	closeButton.setToolTipText("Close wizard");
	closeButton.addActionListener(new ActionListener() {

		public void actionPerformed(ActionEvent e) {
		    if(discardModifiedInteractions()) {
			structure.setSequences(sequences);
			structure.setInteractions(existingInteractions);
			System.out.println("Disposing window");
			w.setVisible(false);
			w.dispose();

		    }

		}


	    });

	controlPanel.add(commitButton);
	controlPanel.add(Box.createHorizontalStrut(15));
	controlPanel.add(clearButton);
	controlPanel.add(Box.createHorizontalStrut(15));
	controlPanel.add(clearAllButton);
	controlPanel.add(Box.createHorizontalGlue());
	controlPanel.add(closeButton);

	controlPanel.setMaximumSize(new Dimension(controlPanel.getMaximumSize().width, controlPanel.getPreferredSize().height));

	JPanel sequenceEditorPanel = new JPanel();
	sequenceEditorPanel.setLayout(new BoxLayout(sequenceEditorPanel, BoxLayout.X_AXIS));

	newSequenceButton = new JButton("Add Sequence");
	newSequenceButton.setToolTipText("Create a new sequence");
	newSequenceButton.addActionListener(new ActionListener() {

		public void actionPerformed(ActionEvent e) {
		    if(isInteractionsModified()) {
			int result = JOptionPane.showConfirmDialog(display, "Interactions have been modified. Inorder to edit sequence, changes must be saved. Save changes?");
			if(result != JOptionPane.OK_OPTION)
			    return;
			
			saveInteractions();
		    }

		    boolean valid = false;

		    final JFrame f = null;
		    final JDialog dlg = new JDialog(f, "Create new Sequence", true);
		    final JTextField sizeField = new JTextField(5);
		    final JRadioButton rnaStrand = new JRadioButton("RNA Sequence");
		    final JRadioButton dnaStrand = new JRadioButton("DNA Sequence");
		    final JTextField nameField = new JTextField(10);

		    //dlg.setLayout(new BoxLayout(dlg, BoxLayout.Y_AXIS));

		    JPanel main = new JPanel();
		    main.setLayout(new BoxLayout(main, BoxLayout.Y_AXIS));
		    
		    JLabel nameLabel = new JLabel("Enter Sequence Name");
		    JPanel panel = new JPanel();
		    //panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		    panel.add(nameLabel);
		    panel.add(Box.createHorizontalStrut(1));
		    panel.add(nameField);
		    main.add(panel);

		    JLabel seqSizeLabel = new JLabel("Enter Sequence Size");
		    panel = new JPanel();
		    panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		    panel.add(seqSizeLabel);
		    panel.add(Box.createHorizontalStrut(10));
		    panel.add(sizeField);
		    main.add(panel);

		    final ButtonGroup group = new ButtonGroup();
		    group.add(rnaStrand);
		    group.add(dnaStrand);
		    group.setSelected(rnaStrand.getModel(), true);

		    main.add(Box.createVerticalStrut(15));

		    main.add(rnaStrand);
		    main.add(dnaStrand);

		    main.add(Box.createVerticalStrut(15));


		    final JButton doneButton = new JButton("Done");
		    doneButton.addActionListener(new ActionListener() {
			    public void actionPerformed(ActionEvent e) {
				String size = sizeField.getText();
				String name = nameField.getText();
				boolean rna = group.isSelected(rnaStrand.getModel()) ? true : false;
				int s = 0;
				try {
				    s = Integer.parseInt(size);
				    if(s < 1) throw new NumberFormatException();

				    String sequence = "";
				    for(int i = 0; i < s; ++i)
					sequence += "N";

				    MutableSequence seq = new SimpleMutableSequence(sequence, name, rna ? DnaTools.AMBIGUOUS_RNA_ALPHABET : DnaTools.AMBIGUOUS_DNA_ALPHABET);
				    sequences.addSequence(seq);

				    sequenceColorMap.put(sequences.getSequenceCount() - 1, mixer.nextColor());

				    SequenceEditorWizard wizard = new SequenceEditorWizard(seq);
				    wizard.addActionListener(new ActionListener() {
					    public void actionPerformed(ActionEvent e) {
						if(e.getID() == SequenceEditorWizard.WINDOW_CLOSED) {
						    System.out.println("Window closing");
						    System.out.println("model size: " + sequenceSelector1.getModel().getSize());
						    sequenceSelector1.setModel(new SequenceListModel());
						    sequenceSelector2.setModel(new SequenceListModel());
						    sequenceEditorSelector.setModel(new SequenceSelectorComboBoxModel());
						    
						   
						    scrollPane.getViewport().revalidate();
						    scrollPane.revalidate();
						    repaint();
						}
					    }
					});
				    System.out.println("getting ready to launch the sequence editor wizard");
				    wizard.launchWizard(null, SecondaryStructureEditorPanel.this);
				    
				    
				} catch(NumberFormatException e1) {
				    JOptionPane.showMessageDialog(SecondaryStructureEditorPanel.this, "Invalid sequence size");
				} catch(UnknownSymbolException e2) {
				     JOptionPane.showMessageDialog(SecondaryStructureEditorPanel.this, "Unknown symbol exception");
				} catch(DuplicateNameException e3) {
				    JOptionPane.showMessageDialog(SecondaryStructureEditorPanel.this, "Sequence already exists with that name");
				} catch(Exception e4) {
				    e4.printStackTrace();
				} finally {
				    dlg.setVisible(false);
				    dlg.dispose();
				}
				
			    }
			});

		    main.add(doneButton);

		    dlg.setLayout(new BorderLayout());
		    dlg.add(main);
		    dlg.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		    dlg.pack();
		    dlg.setVisible(true);


		}
	    });

	sequenceEditorPanel.add(newSequenceButton);
	sequenceEditorPanel.add(Box.createHorizontalStrut(15));

	sequenceEditorSelector = new JComboBox(new SequenceSelectorComboBoxModel());
	sequenceEditorSelector.addItemListener(new ItemListener() {
		public void itemStateChanged(ItemEvent e) {
		    if(sequenceEditorSelector.getSelectedIndex() > -1 &&
		       sequences.getSequence(sequenceEditorSelector.getSelectedIndex()) instanceof MutableSequence)
			editSequenceButton.setEnabled(true);
		    else
			editSequenceButton.setEnabled(false);
		}
		
	    });
	
	editSequenceButton = new JButton("Edit Sequence");
	editSequenceButton.setToolTipText("Edit an existing sequence");
	editSequenceButton.setEnabled(false);
	editSequenceButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    if(isInteractionsModified()) {
			int result = JOptionPane.showConfirmDialog(display, "Interactions have been modified. Inorder to edit sequence, changes must be saved. Save changes?");
			if(result != JOptionPane.OK_OPTION)
			    return;

			saveInteractions();
		    }
		    int selectedIndex = sequenceEditorSelector.getSelectedIndex();
		    System.out.println("Selected index is: " + selectedIndex);
		    if(selectedIndex > -1) {
			
			MutableSequence seq = (MutableSequence) sequences.getSequence(sequenceEditorSelector.getSelectedIndex());
			SequenceEditorWizard sequenceEditor = new SequenceEditorWizard(seq, existingInteractions);
			sequenceEditor.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
				    if(e.getID() == SequenceEditorWizard.WINDOW_CLOSED) {
					scrollPane.getViewport().revalidate();
					scrollPane.revalidate();
					repaint();
				    }
				}
			    });
			sequenceEditor.launchWizard(null, SecondaryStructureEditorPanel.this);
		
		    }
		    else
			JOptionPane.showMessageDialog(SecondaryStructureEditorPanel.this,"Must select a sequence to edit");

		}

	    });

	sequenceEditorPanel.add(sequenceEditorSelector);
	sequenceEditorPanel.add(editSequenceButton);
	sequenceEditorPanel.setMaximumSize(new Dimension(sequenceEditorPanel.getMaximumSize().width, sequenceEditorPanel.getPreferredSize().height));

	display = new StemCreationDisplay();

	sequenceSelector1.addListSelectionListener(new ListSelectionListener() {
		public void valueChanged(ListSelectionEvent e) {
		    if(sequenceSelector1.getSelectedValue() != null && 
		       !sequenceSelector1.getSelectedValue().equals(sequence1)) {
			if(discardModifiedInteractions()) 
			    for(int i = 0; i < sequences.getSequenceCount(); ++i) 
				if(sequenceSelector1.getSelectedValue().equals(sequences.getSequence(i).getName())) {
				    setSequence1(sequences.getSequence(i));
				    break;
				}
			    
			else
			    sequenceSelector1.setSelectedValue(sequence1, true);

		    }
		    

		}

	    });

	sequenceSelector2.addListSelectionListener(new ListSelectionListener() {
		public void valueChanged(ListSelectionEvent e) {
		    if(sequenceSelector2.getSelectedValue() != null && 
		       !sequenceSelector2.getSelectedValue().equals(sequence2)) {
			if(discardModifiedInteractions())
			    for(int i = 0; i < sequences.getSequenceCount(); ++i) 
				if(sequenceSelector2.getSelectedValue().equals(sequences.getSequence(i).getName())) {
				    setSequence2(sequences.getSequence(i));
				    break;
				}
			  
			else
			    sequenceSelector2.setSelectedValue(sequence2, true);
		    }
		}

	    });

	scrollPane = new JScrollPane(display);
	scrollPane.getViewport().setBackground(Color.white);
	scrollPane.setPreferredSize(new Dimension(500, 450));

	pane = new JPanel();
	pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));
	pane.add(sequenceEditorPanel);
	pane.add(scrollPane);
	pane.add(controlPanel);


	JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, selectorPanel, pane);


	add(splitPane);

	

    }

    private void saveInteractions() {
	if(isInteractionsModified()) {
	    
	    for(int i = 0; i < conflictingInteractions.size(); ++i) {
		InteractionSet conflicts = getConflictingInteractions(existingInteractions, conflictingInteractions.get(i));
		for(int j = 0; j < conflicts.size(); ++j)
		    existingInteractions.remove(conflicts.get(j));
	    }
	    
	    for(int i = 0; i < modifiedInteractions.size(); ++i)
		existingInteractions.add(modifiedInteractions.get(i));
	    
	    updateInteractions();
	    
	    display.repaint();
	    
	}
	
    }
    
    private class SequenceSelectorComboBoxModel extends AbstractListModel implements ComboBoxModel {
	private Object selectedItem = null;

	public Object getSelectedItem() {
	    return selectedItem;
	}
	
	public void setSelectedItem(Object o) {
	    selectedItem = o;

	}

	public Object getElementAt(int index) {

	    return sequences.getSequence(index).getName();
	}

	public int getSize() {

	    return sequences.getSequenceCount();
	}


    }


    private InteractionSet getValidInteractions(InteractionSet set, InteractionSet conflicts) {
	InteractionSet s = new SimpleInteractionSet();

	for(int i = 0; i < set.size(); ++i)
	    s.add(set.get(i));

	System.out.println(NEWLINE + "Getting Valid Interactions");
	System.out.println("Existing size: " + set.size());
	System.out.println("Conflict size: " + conflicts.size());


	for(int i = 0; i < conflicts.size(); ++i) {
	    InteractionSet c = getConflictingInteractions(s, conflicts.get(i));
	    for(int j = 0; j < c.size(); ++j)
		s.remove(c.get(j));
	}

	System.out.println("Valid size: " + s.size());

	return s;

    }

    public boolean validateInteraction(InteractionSet s, Interaction i) {
	return getConflictingInteractions(s, i).size() == 0;

    }


    public InteractionSet getConflictingInteractions(InteractionSet s, Interaction i) {
	InteractionSet set = new SimpleInteractionSet();
	Residue nr1 = i.getResidue1();
	Residue nr2 = i.getResidue2();

	for(int j = 0; j < s.size(); ++j) {
	    Interaction in = s.get(j);
	    Residue er1 = in.getResidue1();
	    Residue er2 = in.getResidue2();
	
	    if(er1.isSameSequence(nr1) && er1.getPos() == nr1.getPos()) {
		set.add(in);
	    }
	    else if(er1.isSameSequence(nr2) && er1.getPos() == nr2.getPos()) {
		set.add(in);
	    }
	    else if(er2.isSameSequence(nr1) && er2.getPos() == nr1.getPos()) {
		set.add(in);
	    }
	    else if(er2.isSameSequence(nr2) && er2.getPos() == nr2.getPos()) {
		set.add(in);
	    }

	}

	return set;
	
    }
    
    public void removeConflictingInteractions(InteractionSet s, Interaction i) {
	Residue nr1 = i.getResidue1();
	Residue nr2 = i.getResidue2();

	for(int j = 0; j < s.size(); ++j) {
	    Interaction in = s.get(j);
	    Residue er1 = in.getResidue1();
	    Residue er2 = in.getResidue2();
	
	    if(er1.isSameSequence(nr1) && er1.getPos() == nr1.getPos()) {
		s.remove(in);
	    }
	    else if(er1.isSameSequence(nr2) && er1.getPos() == nr2.getPos()) {
		s.remove(in);
	    }
	    else if(er2.isSameSequence(nr1) && er2.getPos() == nr1.getPos()) {
		s.remove(in);
	    }
	    else if(er2.isSameSequence(nr2) && er2.getPos() == nr2.getPos()) {
		s.remove(in);
	    }

	}

    }
    
    public void setSequence1(Sequence sequence1) {
	this.sequence1 = sequence1;
	handleSequenceChange();
    }

    private void handleSequenceChange() {
	display.clearSelection();
	checkSequencesEqual();
	updateInteractions();
	display.updateDimensions();
	pane.revalidate();
	scrollPane.revalidate();
	scrollPane.getViewport().revalidate();
	scrollPane.repaint();

    }
    
    public void setInteractionsModified(boolean b) {
	interactionsModified = b;
	if(b) {
	    commitButton.setEnabled(true);
	    clearButton.setEnabled(true);
	}
	else {
	    commitButton.setEnabled(false);
	    clearButton.setEnabled(false);
	}
    }
    
    public boolean isInteractionsModified() {
	return interactionsModified;
    }
    
    public void setSequence2(Sequence sequence2) {
	this.sequence2 = sequence2;
	handleSequenceChange();
    }
    
    public Sequence getSequence1() {
	return sequence1;
    }
    
    public Sequence getSequence2() {
	return sequence2;
    }

    private boolean checkSequencesEqual() {
	if(sequence1 != null && sequence2 != null) {
	    if(sequence1.equals(sequence2))
		return sameSequence = true;
	    else
		return sameSequence = false;
	}
	
	return false;
    }
    
    
    /** update the interactions between the sequences */
    private void updateInteractions() {
	
	//	existingInteractions.clear();
	modifiedInteractions.clear();
	conflictingInteractions.clear();

	setInteractionsModified(false);
	
	/*if(sequence1 != null && sequence2 != null) {
	    MutableSecondaryStructure structure = graphController.generateSecondaryStructure();
	    InteractionSet set = structure.getInteractions();
	    for(int i = 0; i < set.size(); ++i) {
		Interaction inter = set.get(i);
		if(inter.getResidue1().getSequence().equals(sequence1) ||
		   inter.getResidue1().getSequence().equals(sequence2) ||
		   inter.getResidue2().getSequence().equals(sequence1) ||
		   inter.getResidue2().getSequence().equals(sequence2))
		    existingInteractions.add(inter);
		
	    }
	    }*/
	
    }
    
    private boolean discardModifiedInteractions() {
	if(isInteractionsModified()) {
	    int result = JOptionPane.showConfirmDialog(display, "Interactions have been modified. Discard changes?");
	    if(result == JOptionPane.OK_OPTION)
		return true;
	    
	    return false;
	}
	
	return true;
    }

    private class StemCreationDisplay extends JPanel implements Scrollable {
	
	
	private final Dimension PANEL_DIMENSION =
	    new Dimension(500, 500);
	
	private final Dimension CHARACTER_BOUND =
	    new Dimension(12, 16);

	private final Point SEQ1_POINT = new Point(20, 50);
	private final Point SEQ2_POINT = new Point(20, 300);
	private final Point SEQEQUAL_POINT = new Point(20, 300);

	private final int INTER_SEQUENCE_SPACE = 50;
	private final int BORDER_SPACE = 20;


	





	private Sequence selectedSequence = null;
	private int selectedPosition = -1;


	public void clearSelection() {
	    selectedSequence = null;
	    selectedPosition = -1;
	}

	private Sequence getClickedSequence(Point p) {
	    if(sameSequence) {
		Rectangle sequenceBound = new Rectangle(SEQEQUAL_POINT, new Dimension(CHARACTER_BOUND.width * sequence1.size(), CHARACTER_BOUND.height));
		if(sequenceBound.contains(p))
		    return sequence1;

		return null;

	    }

	    else {
		Rectangle sequence1Bound = new Rectangle(SEQ1_POINT, new Dimension(CHARACTER_BOUND.width * sequence1.size(), CHARACTER_BOUND.height));
		Rectangle sequence2Bound = new Rectangle(SEQ2_POINT, new Dimension(CHARACTER_BOUND.width * sequence2.size(), CHARACTER_BOUND.height));
		System.out.println(sequence1Bound);
		System.out.println(sequence2Bound);
		System.out.println(p);
		if(sequence1Bound.contains(p)) {
		    
		    return sequence1;
		}
		if(sequence2Bound.contains(p)) {
		    
		    return sequence2;
		}
		
		return null;


	    }

	}

	private int getClickedPosition(Point p) {
	    Sequence s = getClickedSequence(p);
	    if(s != null) {
		if(sameSequence)
		    return (p.x - SEQEQUAL_POINT.x) / CHARACTER_BOUND.width;
		else {
		    if(s.equals(sequence1))
			return (p.x - SEQ1_POINT.x) / CHARACTER_BOUND.width;
		    else
			return (p.x - SEQ2_POINT.x) / CHARACTER_BOUND.width;

		}
	
	    }
	
	    return -1;

	}


	public StemCreationDisplay() {
	    Random r = Randomizer.getInstance();

	    System.out.println("Creating stem creation display");

	    mixer = 
		new ColorGradientMixer(new Color(r.nextInt(100) + 155, 
						 r.nextInt(100) + 155,
						 r.nextInt(100) + 155), 40);


	    for(int i = 0; i < sequences.getSequenceCount(); ++i) {
		sequenceColorMap.put(i, mixer.nextColor());
	    }


	    setBackground(Color.white);
	    setPreferredSize(PANEL_DIMENSION);

	    addMouseListener(new MouseAdapter() {
		    
		    public void mouseClicked(MouseEvent e) {
			if(sequence1 != null && sequence2 != null) {
			    
			    if(e.getButton() == MouseEvent.BUTTON1) {
				Point p = e.getPoint();
				Sequence s = getClickedSequence(p);
				int pos = getClickedPosition(p);
				
				if(pos == -1 || s == null)
				    return;
				
				// if nothing is currently selected, make new selection
				else if(selectedSequence == null || selectedPosition == -1) {
				    selectedSequence = s;
				    selectedPosition = pos;
				    System.out.println("Selected sequence " + s.getName() + " at position " + pos);
				}
				
				
				else if(sameSequence && selectedPosition == pos)
				    return;

				else if(!sameSequence && selectedSequence.equals(s)) {
				    selectedPosition = pos;
				    System.out.println("Selected sequence " + selectedSequence.getName() + " at position " + pos);
				}

				
				
				else {
				    System.out.println("Adding interaction: " + selectedSequence.getName() + ", " + selectedPosition + " to " + s.getName() + ", " + pos);
				    Interaction interaction = new SimpleInteraction(selectedSequence.getResidue(selectedPosition), s.getResidue(pos), new RnaInteractionType(RnaInteractionType.WATSON_CRICK));
				    InteractionSet conflictingInteractions = getConflictingInteractions(modifiedInteractions, interaction);
				    System.out.println("Number of conflicting interactions: " + conflictingInteractions.size());
				    if(conflictingInteractions.size() > 0) {
					for(int i = 0; i < conflictingInteractions.size(); ++i)
					    modifiedInteractions.remove(conflictingInteractions.get(i));
				    }
				    
				    conflictingInteractions = getConflictingInteractions(existingInteractions, interaction);
				    
				    if(conflictingInteractions.size() > 0) {
					SecondaryStructureEditorPanel.this.conflictingInteractions.add(interaction);
				    }
				    
				    modifiedInteractions.add(interaction);
				    
				    selectedSequence = null;
				    setInteractionsModified(true);
				    selectedPosition = -1;
				    
				}
				
				
			    }
			    
			}
			
			
			else {
			    selectedSequence = null;
			    selectedPosition = -1;
			}
			
			
			repaint();
			
		    }
		
		    
		});
			     
			     
			     
	}
   
	
	private char getSequenceCharacter(Sequence s, int index) {
	    return s.sequenceString().charAt(index);
	    
	}
	
	private void paintSequence(Graphics g, Point p, Sequence s) {
	    InteractionSet set = null;
	    if(conflictingInteractions.size() > 0) {
		set = getValidInteractions(existingInteractions, conflictingInteractions);
	    }
	    else {

		set = existingInteractions;
	    }

	    // paint existing interaction highlights
	    for(int i = 0; i < set.size(); ++i) {
		Interaction interaction = set.get(i);
		Residue r1 = interaction.getResidue1();
		Residue r2 = interaction.getResidue2();
		Sequence s1 = interaction.getSequence1();
		Sequence s2 = interaction.getSequence2();
		
		if(s1.equals(s)) {
		    Point tp = new Point(p);
		    Color c = null;
		    		    
		    c = getSequenceColor(s2);
		    tp.translate(CHARACTER_BOUND.width * r1.getPos(), 0);
		    
		    Color old = g.getColor();
		    g.setColor(c);
		    g.fillRect(tp.x, tp.y, CHARACTER_BOUND.width, CHARACTER_BOUND.height);
		    g.setColor(old);
		
		}	
		
		if(s2.equals(s)) {
		    Point tp = new Point(p);
		    Color c = null;
			    
		    c = getSequenceColor(s1);
		    tp.translate(CHARACTER_BOUND.width * r2.getPos(), 0);
		    
		    Color old = g.getColor();
		    g.setColor(c);
		    g.fillRect(tp.x, tp.y, CHARACTER_BOUND.width, CHARACTER_BOUND.height);
		    g.setColor(old);
		}
		
	    }

	
    

	    // paint selected base by underlining it
	    if(selectedSequence != null && selectedPosition != -1 && s.equals(selectedSequence)) {
		
		Point tp = new Point(p);
		
		tp.translate(CHARACTER_BOUND.width * selectedPosition, 0);
		
		Color c = g.getColor();
		g.setColor(Color.black);
		g.drawLine(tp.x, tp.y + CHARACTER_BOUND.height + 2, 
			   tp.x + CHARACTER_BOUND.width, 
			   tp.y + CHARACTER_BOUND.height + 2);
		g.setColor(c);
		
	    }

	    // paint bases
	    Point tp = new Point(p);
	    for(int i = 0; i < s.size(); ++i) {
		char[] chars = { getSequenceCharacter(s, i) };
		g.drawChars(chars, 0, 1, tp.x, tp.y + CHARACTER_BOUND.height);
		tp.translate(CHARACTER_BOUND.width, 0);
	    }
	    

	}

	private Color getSequenceColor(Sequence s) {
	    for(int i = 0; i < sequences.getSequenceCount(); ++i) {
		if(sequences.getSequence(i).equals(s))
		    return sequenceColorMap.get(i);

	    }

	    return Color.black;


	}


	private void paintInterSequenceInteraction(Graphics g, Point p1, Sequence sequence1, Point p2, Sequence sequence2, Interaction i) {
	    Residue r1 = i.getResidue1();
	    Residue r2 = i.getResidue2();
	    
	    int seq1Position = 0, seq2Position = 0;

 	    if (r1.isSameSequence(r2)) {
 		seq1Position = r1.getPos();
 		seq2Position = r2.getPos();
		
 	    }
 	    else {
		if(i.getSequence1().equals(sequence1)) {
		    seq1Position = r1.getPos();
		    seq2Position = r2.getPos();
		}
		else {
		    seq1Position = r2.getPos();
		    seq2Position = r1.getPos();
		}
		
 	    }
	    
	    // draw a line
	    Point tp1 = new Point(p1);
	    tp1.translate(CHARACTER_BOUND.width * seq1Position + CHARACTER_BOUND.width / 2, CHARACTER_BOUND.height + 5);
	    Point tp2 = new Point(p2);
	    tp2.translate(CHARACTER_BOUND.width * seq2Position + CHARACTER_BOUND.width / 2, -5);
	    
	    g.drawLine(tp1.x, tp1.y, tp2.x, tp2.y);
	    
	}

	private void paintIntraSequenceInteraction(Graphics g, Point p, Interaction i, boolean above) {
	    
	    Residue r1 = i.getResidue1();
	    Residue r2 = i.getResidue2();
	    
	    int first, last;
	    if(r1.getPos() > r2.getPos()) {
		first = r2.getPos();
		last = r1.getPos();
	    }
	    else {
		first = r1.getPos();
		last = r2.getPos();
		
	    }
	    
	    int width = (last - first) * CHARACTER_BOUND.width;
	    int height = (last - first) * CHARACTER_BOUND.height / 2;
	    
	    Point point = new Point(p);
	    if(!above) {
		 point.translate(CHARACTER_BOUND.width * first + CHARACTER_BOUND.width / 2, (-1 * height / 2) + CHARACTER_BOUND.height + 5 );
		 g.drawArc(point.x, point.y, width, height, 0, -180);

	    }
	    else {
		point.translate(CHARACTER_BOUND.width * first + CHARACTER_BOUND.width / 2, -5 - height / 2);
		g.drawArc(point.x, point.y, width, height, 0, 180);
	    }
		
	}
		
	public void paintComponent(Graphics g) {
	    super.paintComponent(g);
	    Font f = g.getFont();
	    g.setFont(new Font(f.getName(), Font.BOLD, 16));
	    g.setColor(Color.black);
	    if(sequence1 != null && sequence2 != null) {
		// if same sequence, draw arches
		if(sameSequence) {
		    paintSequence(g, SEQEQUAL_POINT, sequence1);
		     
		    // paint user defined interactions
		    for(int i = 0; i < modifiedInteractions.size(); ++i)
			paintIntraSequenceInteraction(g, SEQEQUAL_POINT, modifiedInteractions.get(i), true);

		    // paint existing interactions
		    InteractionSet validInteractions = getValidInteractions(existingInteractions, conflictingInteractions);
		    for(int i = 0; i < validInteractions.size(); ++i) {
			// TODO : verify below code!!!
 			if(validInteractions.get(i).isIntraSequence() && validInteractions.get(i).getResidue1().isSameSequence(sequence1.getResidue(0))) {
			    paintIntraSequenceInteraction(g, SEQEQUAL_POINT, validInteractions.get(i), false);
			}
		    }

		}

		// if different sequences, use lines
		else {
		    paintSequence(g, SEQ1_POINT, sequence1);
		    paintSequence(g, SEQ2_POINT, sequence2);

		    // draw the interactions as lines
		    for(int i = 0; i < modifiedInteractions.size(); ++i) {
			paintInterSequenceInteraction(g, SEQ1_POINT, sequence1, SEQ2_POINT, sequence2 , modifiedInteractions.get(i));
		    }

		    InteractionSet validInteractions = getValidInteractions(existingInteractions, conflictingInteractions);
		    for(int i = 0; i < validInteractions.size(); ++i) {
			Interaction in = validInteractions.get(i);
			if(!in.isIntraSequence()) {
			    
			    Sequence s1 = in.getSequence1().equals(sequence1) ? in.getSequence1() : in.getSequence2();
			    Sequence s2 = in.getSequence2().equals(sequence2) ? in.getSequence2() : in.getSequence1();

			    if((in.getResidue1().isSameSequence(sequence1.getResidue(0)) && in.getResidue2().isSameSequence(sequence2.getResidue(0))) 
			       || (in.getResidue1().isSameSequence(sequence2.getResidue(0)) && in.getResidue2().isSameSequence(sequence1.getResidue(0)))) {
				paintInterSequenceInteraction(g, SEQ1_POINT, s1, SEQ2_POINT, s2,  in);
			    }
			}

		    }

		    // draw any intrasequence interactions
		    for(int i = 0; i < validInteractions.size(); ++i) {
			Interaction in = validInteractions.get(i);
			if(in.isIntraSequence()) {
// 			    if(in.getResidue1().getSequence().equals(sequence1)) {
// 				paintIntraSequenceInteraction(g, SEQ1_POINT, in, true);
// 			    }
// 			    else if(in.getResidue1().getSequence().equals(sequence2)) {
// 				paintIntraSequenceInteraction(g, SEQ2_POINT, in, false);
// 			    }
			    // TODO : verify below code!!!
 			    if(in.getResidue1().isSameSequence(sequence1.getResidue(0))) {
 				paintIntraSequenceInteraction(g, SEQ1_POINT, in, true);
 			    }
 			    else if (in.getResidue1().isSameSequence(sequence2.getResidue(0))) {
 				paintIntraSequenceInteraction(g, SEQ2_POINT, in, false);
 			    }
			}
		    }

		}

	    }
	}



	public void updateDimensions() {
	    if(sequence1 != null && sequence2 != null) {
		int maxLength = 0;
		if(sequence1 != null)
		    maxLength = sequence1.size();
		if(sequence2 != null && sequence2.size() > maxLength)
		    maxLength = sequence2.size();

		int width = maxLength * CHARACTER_BOUND.width + 2 * BORDER_SPACE;
		int height = 0;
		if(sameSequence) {
		    height = 2 * BORDER_SPACE + 2 * maxLength * CHARACTER_BOUND.height / 2 + CHARACTER_BOUND.height;
		    SEQEQUAL_POINT.y = BORDER_SPACE + maxLength * CHARACTER_BOUND.height / 2;
		}
		else {
		    height = 2 * BORDER_SPACE + INTER_SEQUENCE_SPACE + 2 * maxLength * CHARACTER_BOUND.height / 2 +
			2 * CHARACTER_BOUND.height;
		    SEQ1_POINT.y = BORDER_SPACE + maxLength * CHARACTER_BOUND.height / 2;
		    SEQ2_POINT.y = SEQ1_POINT.y + INTER_SEQUENCE_SPACE + CHARACTER_BOUND.height;
		    SEQ1_POINT.x = (width - sequence1.size() * CHARACTER_BOUND.width) / 2 + BORDER_SPACE;
		    SEQ2_POINT.x = (width - sequence2.size() * CHARACTER_BOUND.width) / 2 + BORDER_SPACE;
		}

		setPreferredSize(new Dimension(width, height));
		System.out.println("Setting preferred size to : " + getPreferredSize());
	    }
	    
	    else {
		SEQEQUAL_POINT.x = BORDER_SPACE;
		SEQEQUAL_POINT.y = PANEL_DIMENSION.height - CHARACTER_BOUND.height;
		SEQ1_POINT.x = BORDER_SPACE;
		SEQ1_POINT.y = 50;
		SEQ2_POINT.x = BORDER_SPACE;
		SEQ2_POINT.y = 300;
		setPreferredSize(PANEL_DIMENSION);
	    }

	}


	
	public Dimension getPreferredScrollableViewportSize() {
	    return getPreferredSize();
	}

	public int getScrollableBlockIncrement(Rectangle visibleRect, int orientation,
					       int direction) {
	    
	    if(orientation == SwingConstants.HORIZONTAL) {
		return CHARACTER_BOUND.width;
	    }
	    else
		return CHARACTER_BOUND.height;
	}

	
	public boolean getScrollableTracksViewportHeight() {
		return false;
	}

	
	
	public boolean getScrollableTracksViewportWidth() {
	    return false;    
	}
	
	
	
	public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, 
					      int direction) {    
	    return getScrollableBlockIncrement(visibleRect, orientation, direction);
	    
	}


    }


    // model for the sequence selectors
    private class SequenceListModel extends AbstractListModel {
	public int getSize() {
	    return sequences.getSequenceCount();
	}

	public Object getElementAt(int index) {
	    return sequences.getSequence(index).getName();

	}

    }
    
  
}

