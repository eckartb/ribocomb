package secondarystructuredesign;

import java.util.Properties;
import rnasecondary.*;

/** Assigns a score to a secondary structure and a set of potential sequences */
public interface SecondaryStructureScorer {
    
    /** returns error score for complete secondary structure and trial sequences */
    double scoreStructure(StringBuffer[] bseqs,
			  SecondaryStructure structure,
			  int[][][][] interactionMatrices);

    /** returns error score for complete secondary structure and trial sequences */
    Properties generateReport(StringBuffer[] bseqs,
			      SecondaryStructure structure,
			      int[][][][] interactionMatrices);

}
