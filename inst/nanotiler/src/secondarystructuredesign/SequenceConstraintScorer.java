package secondarystructuredesign;

import java.util.*;

import rnasecondary.*;

public class SequenceConstraintScorer implements SecondaryStructureScorer {

    private double penalty=1000;
    private ArrayList<SequenceConstraint> constraints;

    private class SequenceConstraint {
	int seqId;
	int id;
	char constraint;
	
	SequenceConstraint(int _seqId, int _id, char _constraint) {
	    seqId = _seqId;
	    id = _id;
	    constraint = _constraint;
	}

	/** Returns true if nucleotide base fullfills constraint given as IUPAC code */
	boolean isValid(char base) {
	    return isValid(base, constraint);
	}	

	/** Returns true if nucleotide base fullfills constraint given as IUPAC code */
	boolean isValid(String sequence) {
	    return isValid(sequence.charAt(id), constraint);
	}	

	/** Returns true if nucleotide base fullfills constraint given as IUPAC code */
	boolean isValid(String[] sequences) {
	    return isValid(sequences[seqId].charAt(id), constraint);
	}	

	/** Returns true if nucleotide base fullfills constraint given as IUPAC code */
	boolean isValid(StringBuffer[] sequences) {
	    return isValid(sequences[seqId].charAt(id), constraint);
	}	

	/** Returns true if nucleotide base fullfills constraint given as IUPAC code */
	boolean isValid(char base, char constraint) {
	    switch (constraint) {
	    case 'S': // G or C:
		return (base == 'G') || (base == 'C');
	    case 'W': 
		return (base == 'A') || (base == 'U');
	    default:
		assert false;
	    }
	    return false;
	}
	
	public String toString() {
	    return "" + (seqId + 1) + " " + (id + 1) + " " + constraint;
	}

    }

    public SequenceConstraintScorer(SecondaryStructure structure) {
	constraints = new ArrayList<SequenceConstraint>();
	for (int i = 0; i < structure.getSequenceCount(); ++i) {
	    String s = structure.getSequence(i).sequenceString();
	    for (int j = 0; j < s.length(); ++j) {
		if ((s.charAt(j) == 'W') || (s.charAt(j) == 'S')) {
		    constraints.add(new SequenceConstraint(i, j, s.charAt(j)));
		}
	    }
	}
    }

    /** returns error score for complete secondary structure and trial sequences */
    public double scoreStructure(StringBuffer[] bseqs,
				 SecondaryStructure structure,
				 int[][][][] interactionMatrices) {
	String scoreString = generateReport(bseqs, structure, interactionMatrices,0).getProperty("score");
	assert scoreString != null;
	return Double.parseDouble(scoreString);
    }

    /** returns error score for complete secondary structure and trial sequences */
    public Properties generateReport(StringBuffer[] bseqs,
				     SecondaryStructure structure,
				     int[][][][] interactionMatrices) {
	return generateReport(bseqs, structure, interactionMatrices, 10); // very verbose
    }

    /** returns error score for complete secondary structure and trial sequences */
    public Properties generateReport(StringBuffer[] bseqs,
				     SecondaryStructure structure,
				     int[][][][] interactionMatrices,
				     int verbosity) {
	Properties resultProperties = new Properties();
	double result = 0;
	for (int i = 0; i < size(); ++i) {
	    if (!constraints.get(i).isValid(bseqs)) {
		result += penalty;
	    }
	}
	resultProperties.setProperty("score", "" + result);
	return resultProperties;
    }

    public int size() { return constraints.size(); }

    public String toString() {
	String result = "(SequenceConstraintScorer " + size() + " ";
	for (int i = 0; i < size(); ++i) {
	    result = result + constraints.get(i).toString() + " ";
	}
	result = result + ")";
	return result;
    }

}
