package SecondaryStructureDesign;

import rnadesign.designapp.rnagui.GeneralWizard;
import rnadesign.rnacontrol.Object3DGraphController;
import java.awt.Component;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.awt.event.*;
import sequence.*;
import java.awt.*;
import java.util.logging.*;
import rnasecondary.*;

public class SequenceEditorWizard implements GeneralWizard {

    Logger log = Logger.getLogger("NanoTiler_debug");

    // action id
    public static final int WINDOW_CLOSED = 1;

    private JTable table;
    private JScrollPane scrollPane;

    private JDialog dialog;

    InteractionSet interactions = null;

    private boolean finished = false;

    private ArrayList<ActionListener> listeners = 
	new ArrayList<ActionListener>();

    private Object3DGraphController controller;

    private JButton doneButton,  addBaseButton, insertBaseButton, removeBaseButton;

    private MutableSequence sequence;

    public SequenceEditorWizard(MutableSequence sequence) {
	this.sequence = sequence;
    }

    public SequenceEditorWizard(MutableSequence sequence, InteractionSet set) {
	this.sequence = sequence;
	this.interactions = set;
    }


    public void addActionListener(ActionListener listener) {
	listeners.add(listener);
    }

    public boolean isFinished() {
	return finished;
    }

    private void createAndShowGUI(Component parentFrame) {
	JFrame f = null;
	dialog = new JDialog(f, "Sequence Editor Wizard", true);
	dialog.setLayout(new BorderLayout());

	dialog.addWindowListener(new WindowAdapter() {
		public void windowClosed(WindowEvent e) {
	     
		    fireActionPerformed(new ActionEvent(dialog, WINDOW_CLOSED, ""));

		}
	    });

	JPanel mainPanel = new JPanel();
	mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
	
	table = new JTable(new SequenceTableModel());
	table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
	table.getTableHeader().setReorderingAllowed(false);

	table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
		public void valueChanged(ListSelectionEvent e) {
		    if(e.getValueIsAdjusting()) return;
		    
		    int selectedColumn = table.getSelectedColumn();
		    if(selectedColumn > -1) {
			insertBaseButton.setEnabled(true);
			removeBaseButton.setEnabled(true);

		    }
		    else {
			insertBaseButton.setEnabled(false);
			removeBaseButton.setEnabled(false);
		    }
		}

	    });


	updateTable();

	scrollPane = new JScrollPane(table);
	scrollPane.setPreferredSize(new Dimension(450, 150));
	mainPanel.add(scrollPane);



	doneButton = new JButton("Done");
	doneButton.setToolTipText("Submit changes");
	doneButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    fireActionPerformed(new ActionEvent(dialog, WINDOW_CLOSED, ""));
		    dialog.setVisible(false);
		    dialog.dispose();
		    finished = true;
		}
	    });


	JPanel sequenceMutatorPanel = new JPanel();
	sequenceMutatorPanel.setLayout(new BoxLayout(sequenceMutatorPanel, BoxLayout.X_AXIS));

	insertBaseButton = new JButton("Insert");
	insertBaseButton.setToolTipText("Insert base to the left of selected column");
	insertBaseButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    Object[] symbols = getAlphabetSymbols();
		    int selectedColumn = table.getSelectedColumn();
		    int choice = JOptionPane.showOptionDialog(dialog,
							      "Select base",
							      "Add new base",
							      JOptionPane.YES_NO_OPTION,
							      JOptionPane.QUESTION_MESSAGE,
							      null,
							      symbols,
							      symbols[0]);
		    if(choice != JOptionPane.CLOSED_OPTION) {
			int size = sequence.size();
			sequence.insertResidue(new SimpleResidue((LetterSymbol) symbols[choice], sequence, selectedColumn), selectedColumn);
			table.addColumn(new TableColumn(size));
			updateTable();

		    }
		}

	    });
	insertBaseButton.setEnabled(false);

	addBaseButton = new JButton("Add");
	addBaseButton.setToolTipText("Add base at end of sequence");
	addBaseButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    Object[] symbols = getAlphabetSymbols();
		    int choice = JOptionPane.showOptionDialog(dialog,
							      "Select base",
							      "Add new base",
							      JOptionPane.YES_NO_OPTION,
							      JOptionPane.QUESTION_MESSAGE,
							      null,
							      symbols,
							      symbols[0]);

		    if(choice != JOptionPane.CLOSED_OPTION) {
			log.info("Selected option: " + choice);
			int size = sequence.size();
			sequence.addResidue(new SimpleResidue((LetterSymbol) symbols[choice], sequence, size));
			table.addColumn(new TableColumn(size));
			updateTable();
		    }
							      
		}
	    });
	
	removeBaseButton = new JButton("Remove");
	removeBaseButton.setToolTipText("Remove the selected base");
	removeBaseButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    Residue r = sequence.getResidue(table.getSelectedColumn());
		    handleInteractionRemoval(r);
		    sequence.removeResidue(table.getSelectedColumn());
		    
		    table.removeColumn(table.getColumnModel().getColumn(table.getColumnModel().getColumnCount() - 1));
		    table.clearSelection();
		    removeBaseButton.setEnabled(false);
		    insertBaseButton.setEnabled(false);
		}
	    });
	removeBaseButton.setEnabled(false);

	JPanel temp1 = new JPanel();
	temp1.setLayout(new BoxLayout(temp1, BoxLayout.Y_AXIS));
	temp1.add(sequenceMutatorPanel);
	temp1.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black), "Sequence Mutation Operations"));
	sequenceMutatorPanel.add(insertBaseButton);
	sequenceMutatorPanel.add(Box.createHorizontalStrut(5));
	sequenceMutatorPanel.add(addBaseButton);
	sequenceMutatorPanel.add(Box.createHorizontalStrut(5));
	sequenceMutatorPanel.add(removeBaseButton);

	temp1.add(sequenceMutatorPanel);
	temp1.setAlignmentY(.5f);

	mainPanel.add(temp1);
	mainPanel.add(Box.createVerticalGlue());
		
	JPanel panel2 = new JPanel();
	panel2.setLayout(new BoxLayout(panel2, BoxLayout.Y_AXIS));
	doneButton.setAlignmentY(.5f);
	panel2.add(doneButton);
	panel2.setAlignmentY(.5f);

	mainPanel.add(panel2);
	
	dialog.add(mainPanel);

	dialog.pack();
	dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	dialog.setLocationRelativeTo(parentFrame);
	dialog.setVisible(true);
    }

    private void handleInteractionRemoval(Residue r) {
	if(interactions == null)
	    return;
	for(int i = 0; i < interactions.size(); ++i) {
	    Residue r1 = interactions.get(i).getResidue1();
	    Residue r2 = interactions.get(i).getResidue2();
//  	    log.info("Residue to remove: " + r.getSequence() + "     " + r.getPos());
//  	    log.info("Testing residue 1: " + r1.getSequence() + "     " + r1.getPos());
//  	    log.info("Testing residue 2: " + r2.getSequence() + "     " + r2.getPos());
	    if(   (r1.isSameSequence(r) && r1.getPos() == r.getPos()) 
		  || (r2.isSameSequence(r) && r2.getPos() == r.getPos())) {
		log.info("Removing residue " + r);
		interactions.remove(interactions.get(i));
	    }
	    else {
		log.info("Test did not work!\n");
	    }
	}
    }

    /** generates and inserts new Object3D according to user dialog */
    public void launchWizard(Object3DGraphController controller,
			     Component parentFrame) {
	this.controller = controller;

	final Component c = parentFrame;

	SwingUtilities.invokeLater(new Runnable() {
		public void run() {
		    createAndShowGUI(c);
		}

	    });

    }

    public void setSequence(MutableSequence s) {
	this.sequence = s;
	updateTable();
    }

    private Object[] getAlphabetSymbols() {
	Object[] symbols = new Object[sequence.getAlphabet().size()];
	for(int i = 0; i < sequence.getAlphabet().size(); ++i) {
	    System.out.println(sequence.getAlphabet());
	    symbols[i] = sequence.getAlphabet().getSymbol(i);
	}

	return symbols;
    }

    private void updateTable() {
	table.revalidate();

	for(int i = 0; i < sequence.size(); ++i) {
	    JComboBox box = new JComboBox(getAlphabetSymbols());
	   
	    table.getColumnModel().getColumn(i).setCellEditor(new DefaultCellEditor(box));
	    table.getColumnModel().getColumn(i).setPreferredWidth(45);
	    
	}
	table.revalidate();

    }
    
    private class SequenceTableModel extends AbstractTableModel {

	public int getColumnCount() {
	    return sequence.size();
	}

	public int getRowCount() {
	    return 1;
	}

	public Object getValueAt(int row, int column) {
	    return sequence.getResidue(column).getSymbol();

	}

	public String getColumnName(int column) {

	    return Integer.toString(column);
	}

	public boolean isCellEditable(int row, int column) {
	    return true;
	}

	public void setValueAt(Object o, int row, int column) {
	    sequence.mutateResidue(new SimpleResidue((LetterSymbol) o, sequence, column), column);
	
	}
							       
    }

    public void fireActionPerformed(ActionEvent e) {
	for(ActionListener l : listeners)
	    l.actionPerformed(e);
	
    }




}

