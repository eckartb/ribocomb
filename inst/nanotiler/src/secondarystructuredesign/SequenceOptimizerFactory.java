package secondarystructuredesign;

import rnasecondary.SecondaryStructure;

/** generates a set of sequences optimized to fullfill secondary structure */
public interface SequenceOptimizerFactory {

    public SequenceOptimizer generate();

}
