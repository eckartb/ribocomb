package secondarystructuredesign;

import java.util.Properties;
import java.util.logging.*;
import rnasecondary.*;
import numerictools.AccuracyTools;
import sequence.*;
import static rnasecondary.RnaInteractionType.*;

/** Assigns a score to a secondary structure and a set of potential sequences */
public class SimpleSecondaryStructurePredictorScorer extends AbstractSecondaryStructureScorer {
    
    private double scale = 1.0; // scale factor for result;
    private Level debugLevel = Level.FINE;
    private static Logger log = Logger.getLogger(PackageConstants.LOGFILE_DEFAULT);


    /** returns error score for complete secondary structure and trial sequences */
    public double scoreStructure(StringBuffer[] bseqs,
				 SecondaryStructure structure,
				 int[][][][] interactionMatrices) {
	String scoreString = generateReport(bseqs, structure, interactionMatrices,0).getProperty("score");
	assert scoreString != null;
	return Double.parseDouble(scoreString);
    }

    /** returns error score for complete secondary structure and trial sequences */
    public Properties generateReport(StringBuffer[] bseqs,
				     SecondaryStructure structure,
				     int[][][][] interactionMatrices) {
	return generateReport(bseqs, structure, interactionMatrices, 10); // very verbose
    }

    /** returns error score for complete secondary structure and trial sequences */
    public Properties generateReport(StringBuffer[] bseqs,
				     SecondaryStructure structure,
				     int[][][][] interactionMatrices,
				     int verbosity) {
	Properties resultProperties = new Properties();
	UnevenAlignment ali = null;
	try {
	    ali = generateAlignment(bseqs);
	}
	catch (UnknownSymbolException use) {
	    System.out.println(use.getMessage());
	    assert false;
	}
	catch (DuplicateNameException dne) {
	    System.out.println(dne.getMessage());
	    assert false;
	}
	log.log(debugLevel, "Starting SimpleSecondaryStructurePredictor...");
	SimpleSecondaryStructurePredictor predictor = new SimpleSecondaryStructurePredictor(ali);
	predictor.run();
	SecondaryStructure prediction = (SecondaryStructure)(predictor.getResult());
	log.log(debugLevel, "Finished SimpleSecondaryStructurePredictor. Starting accuracy estimation...");
	double result = scorePrediction(prediction, structure, interactionMatrices);
	log.log(debugLevel, "Finished accuracy estimation with result: " + result);
	assert result >= 0.0;
	resultProperties.setProperty("score", "" + result);
	return resultProperties;
    }

}
