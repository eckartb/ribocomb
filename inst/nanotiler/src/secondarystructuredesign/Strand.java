package secondarystructuredesign;

import java.io.*;
import java.util.*;
import java.util.logging.*;
import java.util.ResourceBundle;
import generaltools.StringTools;
import generaltools.ParsingException;
import numerictools.*;
import launchtools.*;
import rnasecondary.*;

import org.testng.annotations.*;

import static secondarystructuredesign.PackageConstants.*;

/** Assigns a score to a secondary structure and a set of potential sequences */
public class Strand {
    private int index;   // index of strand (used to find correct interaction matrix, etc.)
    private int length;
    private StringBuffer sequence, constraints;
    private boolean inComplex = false;

    public Strand() { }

    public Strand(StringBuffer sequence, int index) {
	this.index = index;
	length = sequence.length();
	this.sequence = sequence;
	constraints = new StringBuffer(length);
	for (int i = 0; i < length; i++) {
	    constraints.append('.');
	}
    }

    public Strand(StringBuffer sequence, StringBuffer constraints, int index) {
	assert sequence.length() == constraints.length();
	this.index = index;
	length = sequence.length();
	this.sequence = sequence;
	this.constraints = constraints;
    }

    public void addToComplex() { inComplex = true; }
    public void removeFromComplex() { inComplex = false; }
    public boolean isInComplex() { return inComplex; }

    
    public void addConstraints(StringBuffer bondingPattern)
    {
	assert bondingPattern.length() == constraints.length();
	for (int i = 0; i < bondingPattern.length(); i++) {
	    if (bondingPattern.charAt(i) == 'x')
		constraints.setCharAt(i, 'x');
	}
    }

    public StringBuffer getSequence() { return sequence; }
    public StringBuffer getConstraints() { return constraints; }

    public int index() { return index; }
}
