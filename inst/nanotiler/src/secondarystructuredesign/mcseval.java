package secondarystructuredesign;

import java.text.ParseException;
import java.util.Properties;
import java.util.logging.*;
import rnasecondary.*;
import java.io.*;
import static secondarystructuredesign.PackageConstants.*;
import generaltools.PropertyTools;
import generaltools.StringTools;

public class mcseval extends mcsopt {

    private static Logger log = Logger.getLogger("mcsopt");
    private static int debugLevel = 1;
    private static int configId = MonteCarloSequenceOptimizerVersionsFactory.VERSION_DEFAULT;

    private static void helpOutput(PrintStream ps) {
	ps.println("Evaluate compatibility of sequence with a secondary structure. Usage: mcseval filename\n");
    }

    public static void main(String[] args) {
	if ((args.length < 1) || (args.length > 2)) {
	    helpOutput(System.out);
	    System.exit(0);
	}
      	String fileName = args[0];
	String altFileName = null;
	if (args.length > 1) {
	    altFileName = args[1];
	}
	SecondaryStructure secStruct = null;
	SecondaryStructure secStruct2 = null;
	log.info("Importing secondary structure from " + fileName);
	try {
	    SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
	    secStruct = parser.parse(fileName);
	    if (altFileName != null) {
		secStruct2 = parser.parse(altFileName);
	    }
	}
	catch (IOException ioe) {
	    log.warning("IO error when scraping result file from: " + fileName);
	    exitError(ioe.getMessage());
	}
	catch (ParseException pe) {
	    log.warning("Parsing error when scraping result file from: " + fileName);
	    exitError(pe.getMessage());
	}
	if (secStruct == null) {
	    exitError("Error parsing secondary structure!");
	}
	else {
	    log.info("Secondary structure correctly read!");
	}
	SecondaryStructureWriter writer = new SecondaryStructureScriptFormatWriter();
	String resultString = writer.writeString(secStruct);
	System.out.println("Initial structure before optimization: " + NEWLINE + resultString);
	prettyPrint(System.out, secStruct);

// 	if (secStruct2 != null) {
// 	    assert false;
//   	    System.out.println("Initial alternative structure before optimization: " + NEWLINE + resultString);
// 	    prettyPrint(System.out, secStruct);
// 	    BistableSequenceOptimizer optimizer = new MonteCarloBistableSequenceOptimizer();
// 	    newSequences = optimizer.optimize(secStruct, secStruct2);
// 	}
// 	else {
	SequenceOptimizerFactory optimizerFactory = new MonteCarloSequenceOptimizerVersionsFactory(configId);
	SequenceOptimizer optimizer = optimizerFactory.generate();
	// evaluation:
	Properties properties = optimizer.eval(secStruct);
	PropertyTools.printProperties(System.out, properties);
	System.out.println("# Good bye!");
    }
}
