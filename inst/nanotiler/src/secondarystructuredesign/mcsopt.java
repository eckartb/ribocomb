package secondarystructuredesign;

import java.text.ParseException;
import java.util.*;
import java.util.logging.*;
import sequence.*;
import rnasecondary.*;
import java.io.*;
import static secondarystructuredesign.PackageConstants.*;
import generaltools.PropertyTools;
import generaltools.StringTools;

public class mcsopt {

    private static Logger log = Logger.getLogger("mcsopt");
    private static int debugLevel = 1;
    private static int configId = MonteCarloSequenceOptimizerVersionsFactory.VERSION_DEFAULT;

    private static void helpOutput(PrintStream ps) {
	ps.println("Usage: mcsopt PARAMETER_FILENAME\n");
    }

    public static void exitError(String message) {
	System.out.println(message);
	System.exit(0);
    }

    private static void outputStrings(PrintStream ps,
		       String[] lines) {
	for (int i = 0; i < lines.length; ++i) {
	    ps.println(lines[i]);
	}
    }

    private static char prettyPrintChar(PrintStream ps, 
				 char c1, char c2) {
	if (RnaSecondaryTools.isRnaComplement(c1, c2)) {
	    return '1';
	}
	return ' ';
    }

    private static char prettyPrintChar(PrintStream ps, 
					String s1,
					String s2,
					int i, int j) {
	char c1 = s1.charAt(i);
	char c2 = s2.charAt(j);
	if ( (i <= 0) || (j <= 0) || ((i+1) >= s1.length())
	     || ((j+1) >= s2.length()))  {
	    return prettyPrintChar(ps, c1, c2);
	}
	else {
	    char c1a = s1.charAt(i-1);
	    char c2a = s2.charAt(j+1);
	    char c1b = s1.charAt(i+1);
	    char c2b = s2.charAt(j-1);
	    if (RnaSecondaryTools.isRnaComplement(c1, c2)
		&& (RnaSecondaryTools.isRnaComplement(c1a, c2a)
		    || RnaSecondaryTools.isRnaComplement(c1b, c2b) ) ) {
		return '1';
	    }
	}
	return ' ';
    }
    
    private static void prettyPrintLine(PrintStream ps, 
			     String seq1, String sec1,
			     String seq2, String sec2,
			     int i) {
	for (int j = 0; j < seq1.length(); ++j) {
	    ps.print("" + prettyPrintChar(ps, seq1, seq2, j, i));
	}
	ps.println(" " + seq2.charAt(i) + " " + sec2.charAt(i));
    }

    private static void prettyPrint(PrintStream ps, 
			     String seq1, String sec1,
			     String seq2, String sec2) {
	ps.println(seq1);
	ps.println(sec1);
	for (int i = 0; i < seq2.length(); ++i) {
	    prettyPrintLine(ps, seq1, sec1, seq2, sec2, i);
	}
    }

    private static void prettyPrint(PrintStream ps, 
				    String seq1, String seq2) {
	prettyPrint(ps, seq1, seq1, seq2, seq2);
    }

    private static void prettyPrint(PrintStream ps, 
				    SecondaryStructure struct,
				    int i, int j) {
	prettyPrint(ps, struct.getSequence(i).sequenceString(),
		    struct.getSequence(j).sequenceString());

    }

    public static void prettyPrint(PrintStream ps, SecondaryStructure struct) {
	for (int i = 0; i < struct.getSequenceCount(); ++i) {
	    for (int j = i; j < struct.getSequenceCount(); ++j) {
		ps.println("Sequence " + (i+1) + " versus " + (j+1));
		prettyPrint(ps, struct, i, j);
	    }
	}
    }

    public static void prettyPrint(PrintStream ps, String[] sequences) {
	assert sequences != null;
	for (int i = 0; i < sequences.length; ++i) {
	    for (int j = i; j < sequences.length; ++j) {
		ps.println("Sequence " + (i+1) + " versus " + (j+1));
		prettyPrint(ps, sequences[i], sequences[j]);
		ps.println();
	    }
	}
    }

    private static List<Alphabet> parseAlphabets(String s) {
	assert(s != null);
	String[] words = s.split(",");
	List<Alphabet> result = new ArrayList<Alphabet>();
	for (int i = 0; i < words.length; ++i) {
	    assert words[i].length() > 0;
	    Alphabet alphabet = new SimpleAlphabet(words[i], SequenceTools.RNA_SEQUENCE);
	    System.out.println("Parsed alphabet: " + words[i] + " with result: " + alphabet);
	    assert alphabet.size() == words[i].length();
	    result.add(alphabet);
	}
	return result;
    }

    private static List<Integer> parseInactive(String s) {
	assert(s != null);
	String[] words = s.split(",");
	List<Integer> result = new ArrayList<Integer>();
	for (int i = 0; i < words.length; ++i) {
	    assert words[i].length() > 0;
	    result.add(new Integer(Integer.parseInt(words[i])-1)); // internal counting starts from 0, external counting starts from 1
	}
	return result;
    }

    public static void fail(int id, Exception e, String message) {
	if (message != null) {
	    System.out.println(message);
	}
	if (e != null) {
	    System.out.println(e.getMessage());
	}
	System.exit(id);
    }

    public static void main(String[] args) {
	if ((args.length < 1) || (args.length > 2)) {
	    helpOutput(System.out);
	    System.exit(0);
	}
	Properties rb = null;
        int verbose = 1;

	String rbFileName = args[0];
	try {
	    FileInputStream fis = new FileInputStream(rbFileName);
	    rb = new Properties();
	    rb.load(fis);
	} catch (IOException ioe) {
	    System.out.println(ioe.getMessage());
	    fail(1, ioe, "Error reading research bundle file: " + rbFileName);
	}
	assert rb != null;
	System.out.println("Defined properties:");
	PropertyTools.printProperties(System.out, rb);
	String nameBase = rb.getProperty("namebase");
	if (nameBase == null) {
	    nameBase = "mcsopt";
	}
      	String fileName = rb.getProperty("ifile");
	if ((fileName == null)  && (args.length > 1)) {
	    fileName = args[1];
	}
	if (fileName == null) {
	    fail(1, new generaltools.ApplicationException(), "No filename specified (either ifile=... in parameter file or mcsopt parameterfile inputfile");
	}
	if (rb.getProperty("verbose") != null) {
	    try {
		verbose = Integer.parseInt(rb.getProperty("verbose"));
	    }
            catch (NumberFormatException nfe) {
		exitError("Error parsing verbose level (should be integer between 0 and 5): " + nfe.getMessage());
	    }
	}
	int stepCount = 0;
// 	String altFileName = null;
// 	if (args.length > 1) {
// 	    altFileName = args[1];
// 	}
	SecondaryStructure secStruct = null;
	// SecondaryStructure secStruct2 = null;
	log.info("Importing secondary structure from " + fileName);
	try {
	    SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
	    secStruct = parser.parse(fileName);
// 	    if (altFileName != null) {
// 		secStruct2 = parser.parse(altFileName);
// 	    }
	}
	catch (IOException ioe) {
	    log.warning("IO error when scraping result file from: " + fileName);
	    exitError(ioe.getMessage());
	}
	catch (ParseException pe) {
	    log.warning("Parsing error when scraping result file from: " + fileName);
	    exitError(pe.getMessage());
	}
	if (secStruct == null) {
	    exitError("Error parsing secondary structure!");
	}
	else {
	    log.info("Secondary structure correctly read!");
	}
	// write inital target structure to CT file format:
      	String initialCtName = nameBase + "_in.ct"; // rb.getProperty("ctinfile");
      	String initialFastaName = nameBase + "_in.fa"; // rb.getProperty("ctinfile");
        assert (initialCtName != null);
	SecondaryStructureCTFormatWriter ctWriter = new SecondaryStructureCTFormatWriter();
	String initialCt = ctWriter.writeString(secStruct);
	try {
	    FileOutputStream fos = new FileOutputStream(initialCtName);
	    PrintStream ps = new PrintStream(fos);
	    ps.println(initialCt);
	    fos.close();
	    // write FASTA file:
	    FileOutputStream fos2 = new FileOutputStream(initialFastaName);
	    PrintStream ps2 = new PrintStream(fos2);
	    ps2.println(SequenceContainerTools.generateFASTA(secStruct.getSequences()));
	    fos2.close();
	} catch (IOException ioe) {
	    exitError("Error writing inital CT format file to " + initialCtName + " : " + ioe.getMessage());
	}

	String alphabetsString = rb.getProperty("alphabets");
	if (alphabetsString != null) {
	    List<Alphabet> alphabets = parseAlphabets(alphabetsString);
	    if (alphabets != null) {
		if (alphabets.size() == secStruct.getSequenceCount()) {
		    secStruct.setAlphabets(alphabets);
		} else {
		    exitError("Number of defined alphabets must match number of sequences to optimize.");
		}
	    }
	}
	String inactiveString = rb.getProperty("inactive");
	if (inactiveString != null) {
	    List<Integer> inactiveIndices = parseInactive(inactiveString);
	    if (inactiveIndices != null) {
		for (Integer iObj : inactiveIndices) {
		    int n = iObj.intValue();
		    if (n >= secStruct.getSequenceCount()) {
			exitError("Index of inactive sequence larger than number of defined sequences: " 
				  + n + " : " + secStruct.getSequenceCount());
		    }
		    secStruct.setInActive(n);
		}
	    }
	}
	SecondaryStructureWriter writer = new SecondaryStructureScriptFormatWriter();
	String resultString = writer.writeString(secStruct);
        if (verbose > 1) {
	    System.out.println("Initial structure before optimization: " + NEWLINE + resultString);
	    prettyPrint(System.out, secStruct);
	}
// 	if (secStruct2 != null) {
// 	    assert false;
//   	    System.out.println("Initial alternative structure before optimization: " + NEWLINE + resultString);
// 	    prettyPrint(System.out, secStruct);
// 	    BistableSequenceOptimizer optimizer = new MonteCarloBistableSequenceOptimizer();
// 	    newSequences = optimizer.optimize(secStruct, secStruct2);
// 	}
// 	else {
	SequenceOptimizerFactory optimizerFactory = new MonteCarloSequenceOptimizerVersionsFactory(rb);
	SequenceOptimizer optimizer = optimizerFactory.generate();
	// performed by VersionsFactory.parseAndSetOptimizer
// 	String randomizeString = rb.getProperty("randomize");
// 	if (randomizeString != null) {
// 	    if (randomizeString.equals("true")) {
// 		optimizer.setRandomizeFlag(true);
// 	    } else if (randomizeString.equals("false")) {
// 		optimizer.setRandomizeFlag(false);
// 	    } else {
// 		exitError("Unknown randomize mode (only true and false is allowed): " + randomizeString);
// 	    }
//	}
	// performed by VersionsFactory.parseAndSetOptimizer
// 	try {
// 	    String iter1String=rb.getProperty("iter");
// 	    if (iter1String!=null) {
// 		optimizer.setIterMax(Integer.parseInt(iter1String));
// 	    }
// 	    String iter2String=rb.getProperty("iter2");
// 	    if (iter2String!=null) {
// 		optimizer.setIter2Max(Integer.parseInt(iter2String));
// 	    }
// 	} catch (NumberFormatException nfe) {
// 	    exitError("Error parsing integer value in iter or iter2: " + nfe.getMessage());
// 	}
	// evaluation:
	Properties properties = optimizer.eval(secStruct);
	PropertyTools.printProperties(System.out, properties);	
	if (((optimizer.getIterMax()==0) && (optimizer.getIter2Max()==0))
	    || "false".equals(rb.getProperty("optimize"))) { // ((args.length >= 2) && args[1].equals("e")) {
	    System.out.println("# Only evaluation and no optimization performed.");
	    System.out.println("# Good bye!");
	    System.exit(0); 
	}
	String[] newSequences = optimizer.optimize(secStruct);
	//	}
	System.out.println("Optimization finished!");
	if (newSequences == null) {
	    System.out.println("Sorry, no sequences that fullfil the error limit were found.");
	    System.exit(0);
	}
	assert newSequences != null;
	// prettyPrint(System.out, newSequences);
	System.out.println("# Optimized sequence(s): ");
	outputStrings(System.out, newSequences);
	Properties allProperties = optimizer.getProperties();
	PropertyTools.printProperties(System.out, allProperties);
        try {
	    double bestRuleScore = Double.parseDouble(optimizer.getProperty("s1.score"));
            if (optimizer.getProperty("s3.score") != null) {
		bestRuleScore += Double.parseDouble(optimizer.getProperty("s3.score")); // constraints are part of rules
	    }
	    double bestStructureScore = Double.parseDouble(optimizer.getProperty("s2.score"));
	    double bestTotalScore = Double.parseDouble(optimizer.getProperty("total_score"));
	    System.out.println("# best rule score: " + bestRuleScore);
	    System.out.println("# best structure score: " + bestStructureScore);
	    System.out.println("# best complete score: " + bestTotalScore);
	} catch (NumberFormatException nfe) {
	    System.out.println("Internal error: could not identify best score components.");
	} catch (java.lang.NullPointerException npe) {
	    System.out.println("Internal error: could not identify best score components!");
	}

     	String finalCtName = nameBase + "_out.ct";
     	String finalSeqName = nameBase + "_out.fa";
	assert (finalCtName != null);
	try {
	    // write CT format file:
            for (int i = 0; i < newSequences.length; ++i) {
		newSequences[i] = newSequences[i].toUpperCase();
	    }
            SimpleUnevenAlignment finalAli = new SimpleUnevenAlignment(newSequences, DnaTools.AMBIGUOUS_RNA_ALPHABET);

	    MatchFoldSecondaryStructurePredictor matchfold = new MatchFoldSecondaryStructurePredictor(finalAli);
	    SecondaryStructure optSeqMatchfoldStruct = (SecondaryStructure)(matchfold.getResult());
	    assert optSeqMatchfoldStruct != null;
	    FileOutputStream fos = new FileOutputStream(finalCtName);
	    PrintStream ps = new PrintStream(fos);
	    ps.println(ctWriter.writeString(optSeqMatchfoldStruct));
	    fos.close();
	    // write FASTA file:
	    FileOutputStream fos2 = new FileOutputStream(finalSeqName);
	    PrintStream ps2 = new PrintStream(fos2);
	    ps2.println(SequenceContainerTools.generateFASTA(finalAli));
	    fos2.close();
	} catch (IOException ioe) {
	    exitError("Error writing optimized sequences to file " + finalCtName + " : " + ioe.getMessage());
	} catch (sequence.DuplicateNameException dne) {
	    exitError("Detected duplicated sequence names in final set of optimized sequences: " + dne.getMessage());
	}  catch (sequence.UnknownSymbolException dne) {
	    exitError("Detected unknown nucleotide symbol in final set of optimized sequences: " + dne.getMessage());
	}       

	System.out.println("# Good bye!");
    }
}
