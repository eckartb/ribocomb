/** 
 * This interface describes the concept of an RNA building block.
 * A building block (like a stem, or a loop) consist of a set of 
 * Sequences, a set of Connectors and internal properties like 
 * stems formed between the sequences
 */
package sequence;

/**
 * @author Eckart Bindewald
 *
 */
public interface SequenceContainer {

    /** removes all sequences */
    public void clear();

    /** returns n'th sequence */
    public Sequence getSequence(int n) throws IndexOutOfBoundsException;

    /** returns number of defined sequences */
    public int getSequenceCount();

    /** adds sequence */
    public void addSequence(Sequence s) throws DuplicateNameException;

}
