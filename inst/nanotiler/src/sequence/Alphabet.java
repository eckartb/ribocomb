package sequence;

public interface Alphabet {

    public void add(LetterSymbol symbol);
    
    public boolean contains(LetterSymbol symbol);

    public boolean contains(char character);

    /** returns type of sequence according to SequenceTools (DNA_SEQUENCE, RNA_SEQUENCE, etc) */
    public int getType();

    /** returns n'th symbol */
    public LetterSymbol getSymbol(int n) throws IndexOutOfBoundsException;

    public void setType(int n);

    /** returns number of symbols */
    public int size();

}
