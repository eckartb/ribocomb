package sequence;

import java.io.InputStream;

/** Most fundamental interface for alignment classes. It defines how to add and retrieve sequences. */
public interface BasicAlignment extends SequenceContainer {

    /** adds sequence */
    public void addSequence(Sequence s) throws DuplicateNameException;

    public Sequence getSequence(String name);

    /** returns index of sequence with this name, -1 if not found. */
    public int getIndex(String name);

    /** simple read method */
    public void read(InputStream is);

    /** Sets weights of all sequences */
    public void setWeights(double[] weights);

    public String toString(); 

    public String toSequenceString();

}
