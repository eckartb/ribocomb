package sequence;

public class DnaTools {

    public static final String AMBIGUOUS_DNA_LETTERS = "ACGTN";

    // public static final String AMBIGUOUS_RNA_LETTERS = "ACGUN";

    public static final String AMBIGUOUS_RNA_LETTERS = "ACGUNSW";

    public static final String DNA_LETTERS = "ACGT";

    public static final String RNA_LETTERS = "ACGU";

    public static final String DNA_RNA_LETTERS = "ACGUT";

    public static final Alphabet AMBIGUOUS_DNA_ALPHABET = new SimpleAlphabet(AMBIGUOUS_DNA_LETTERS,
						  SequenceTools.AMBIGUOUS_DNA_SEQUENCE);

    public static final Alphabet AMBIGUOUS_RNA_ALPHABET = new SimpleAlphabet(AMBIGUOUS_RNA_LETTERS,
									    SequenceTools.AMBIGUOUS_RNA_SEQUENCE);

    public static final Alphabet DNA_ALPHABET = new SimpleAlphabet(DNA_LETTERS, SequenceTools.DNA_SEQUENCE);

    public static final Alphabet RNA_ALPHABET = new SimpleAlphabet(RNA_LETTERS, SequenceTools.RNA_SEQUENCE);

    public static final Alphabet DNA_RNA_ALPHABET = new SimpleAlphabet(DNA_RNA_LETTERS, SequenceTools.DNA_RNA_SEQUENCE);

    /** true if AU or GC pair. assumes upper letter! */
    public static boolean isWatsonCrick(char c1, char c2) {
	if (((int)c2) < ((int)c1)) { // sort
	    char help = c2;
	    c2 = c1;
	    c1 = help;
	}
	return ((c1 == 'A') && ((c2 == 'U') || (c2 == 'T'))) 
	    || ((c1 == 'C') && (c2 == 'G'));
    }

    /** true if AU or GC pair. assumes upper letter! */
    public static boolean isWobble(char c1, char c2) {
	if (((int)c2) < (int)(c1)) { // sort
	    char help = c2;
	    c2 = c1;
	    c1 = help;
	}
	return ((c1 == 'G') && ((c2 == 'T')||(c2 == 'U')));
    }

    /** returns if AU, GC or GU (or AT or GT) */
    public static boolean isComplementary(char c1, char c2) {
	return isWatsonCrick(c1, c2) || isWobble(c1, c2);
    }

    public static boolean checkSequenceCompatible(String s, Alphabet alphabet) {
	boolean result = true;
	try {
	    Sequence seq = new SimpleSequence(s, "dummyname", alphabet);
	}
	catch (UnknownSymbolException use) {
	    result = false;
	}
	return result;
    }

    public static boolean checkSequencesCompatible(String[] sequences, Alphabet alphabet) {
	for (String s : sequences) {
	    s = s.trim();
	    if ((s.length() > 0) && (s.charAt(0) != '>') && (!checkSequenceCompatible(s, alphabet))) { // ignore fasta names
		return false;
	    }
	}
	return true;
    }
	
    public static String[] translateTU(String[] sequences) {
	String[] result = sequences;
	for (int i = 0; i < sequences.length; ++i) {
	    if ((result[i].length() > 0) && (result[i].charAt(0) != '>')) {
		result[i] = sequences[i].replaceAll("T", "U");
	    }
	}
	return result;
    }

    public static String[] translateUT(String[] sequences) {
	String[] result = sequences;
	for (int i = 0; i < sequences.length; ++i) {
	    if ((result[i].length() > 0) && (result[i].charAt(0) != '>')) {
		result[i] = sequences[i].replaceAll("U", "T");
	    }
	}
	return result;
    }

}
