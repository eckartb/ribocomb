package sequence;

import generaltools.ApplicationException;

public class DuplicateNameException extends ApplicationException {

    public DuplicateNameException() {
	super();
    }

    public DuplicateNameException(String s) {
	super(s);
    }

}
