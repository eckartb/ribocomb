package sequence;

import generaltools.Letter;

public interface LetterSymbol extends Letter, Symbol {

    public Object clone();

    public Alphabet getAlphabet();

    public void setAlphabet(Alphabet alpbabet);

}
