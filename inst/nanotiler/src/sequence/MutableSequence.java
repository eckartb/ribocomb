package sequence;

/**
 * A mutable sequence is a sequence that can
 * be changed
 * @author Calvin Grunewald
 */
public interface MutableSequence extends Sequence {
    
    /** Inserts a residue before <code>pos</code> */
    public void insertResidue(Residue residue, int pos);

    /** Removes a residue from <code>pos</code> */
    public void removeResidue(int pos);

    /** Changes the residue at <code>pos</code> to <code>residue</code> */
    public void mutateResidue(Residue residue, int pos);

}
