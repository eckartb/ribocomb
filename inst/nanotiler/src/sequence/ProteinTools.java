package sequence;

public class ProteinTools {

    public static final String AMBIGUOUS_DNA_LETTERS = "ACGTN";

    public static final String AMBIGUOUS_RNA_LETTERS = "ACGUN";

    public static final String DNA_LETTERS = "ACGT";

    public static final String PROTEIN_LETTERS = "ACDEFGHIKLMNPQRSTVWY";

    public static final String RNA_LETTERS = "ACGU";

    public static final String DNA_RNA_LETTERS = "ACGUT";

    public static final Alphabet AMBIGUOUS_DNA_ALPHABET = new SimpleAlphabet(AMBIGUOUS_DNA_LETTERS,
						  SequenceTools.AMBIGUOUS_DNA_SEQUENCE);

    public static final Alphabet AMBIGUOUS_RNA_ALPHABET = new SimpleAlphabet(AMBIGUOUS_RNA_LETTERS,
									    SequenceTools.AMBIGUOUS_RNA_SEQUENCE);

    public static final Alphabet DNA_ALPHABET = new SimpleAlphabet(DNA_LETTERS, SequenceTools.DNA_SEQUENCE);

    public static final Alphabet RNA_ALPHABET = new SimpleAlphabet(RNA_LETTERS, SequenceTools.RNA_SEQUENCE);

    public static final Alphabet DNA_RNA_ALPHABET = new SimpleAlphabet(DNA_RNA_LETTERS, SequenceTools.DNA_RNA_SEQUENCE);

    public static final Alphabet PROTEIN_ALPHABET = new SimpleAlphabet(PROTEIN_LETTERS,
							     SequenceTools.PROTEIN_SEQUENCE);


}
