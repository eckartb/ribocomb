package sequence;

import java.util.Properties;
import java.io.*;
import java.io.InputStream;
import generaltools.PropertyCarrier;

public interface Residue extends PropertyCarrier {

    public Object cloneDeep();

    public LetterSymbol getSymbol();

    /** position in sequence */
    public int getPos();

    /** returns assigned number (like residue number occuring in PDB file */
    public int getAssignedNumber();

    public String getAssignedName();

    /** returns true if part of same sequence */
    public boolean isSameSequence(Residue other);

    /** reference to sequence which residue is part of */
    public Object getParentObject();

    public Properties getProperties();

    public String infoString();

    public void setProperties(Properties p);

    public void read(InputStream is);

    public void setAssignedName(String name);

    /** set assigned number (like residue number occuring in PDB file */
    public void setAssignedNumber(int number);

    public void setPos(int n);

    public void setParentObject(Object obj);

    public void setSymbol(LetterSymbol s);

}
