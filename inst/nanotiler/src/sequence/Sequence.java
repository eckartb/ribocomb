package sequence;

import generaltools.Namable;

public interface Sequence extends Namable {

    /** adds residue at end of sequence */
    public void addResidue(Residue residue);

    /** removes add sequence data. Currently keeps name and alphabet!? TODO! */
    public void clear();

    /** clone method */
    public Object cloneDeep();
    
    /** returns use alphabet */
    public Alphabet getAlphabet();

    /** returns name of sequence */
    public String getName();

    /** returns object that containts sequence (might be a "molecule" or from class rnamodel.Object3D) */
    public Object getParentObject();

    /** returns number of residues */
    public Residue getResidue(int n);

    /** Returns "weight" of sequence. Can be use for sequence-weighting in alignments or for simulating concentrations. */
    public double getWeight();
    
    /** returns number of residues */
    // public Residue getResidueCount(int n);

    /** returns true, if both sequences describe the same entity, even if they were cloned. */
    // public boolean isProbablyEqual(Sequence other);

    /** removes n'th residue */
    // public Residue removeResidue(int n);
    // public void removeChild(int n);

    /** returns sequence representation */
    public String toString();

    /** returns sequence representation */
    public String sequenceString();

    /** sets object that containts sequence (might be a "molecule" or from class rnamodel.Object3D) */
    public void setParent(Object obj);

    /** Sets "weight" of sequence. Can be used for weighted alignment or for simulating concentrations. */
    public void setWeight(double weight);

    /** returns number of residues */
    public int size();

}
