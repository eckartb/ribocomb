package sequence;

public class SequenceContainerTools {

    public static final String NEWLINE = System.getProperty("line.separator");

    public static final String ENDL = NEWLINE;

    /** Converts sequence alignment into String in FASTA format */
    public static String generateFASTA(SequenceContainer alignment) {
	StringBuffer buf = new StringBuffer();
	for (int i = 0; i < alignment.getSequenceCount(); ++i) {
	    Sequence seq = alignment.getSequence(i);
	    buf.append(">" + seq.getName() + ENDL);
	    buf.append(seq.sequenceString() + ENDL);
	}
	return buf.toString();
    }

}
