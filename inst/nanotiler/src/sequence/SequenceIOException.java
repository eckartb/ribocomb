package sequence;

import java.io.IOException;

public class SequenceIOException extends IOException {

    public SequenceIOException() {
	super();
    }

    public SequenceIOException(String msg) {
	super(msg);
    }
}
