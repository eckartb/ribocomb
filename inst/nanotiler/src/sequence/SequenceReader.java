package sequence;

import java.io.DataInputStream;
import java.io.InputStream;
import java.io.IOException;
import java.util.logging.Logger; /** Generates a debug log. */

public class SequenceReader {

    private static Logger log = Logger.getLogger("NanoTiler_debug"); /** The generated debug log. */
   
    /** reads body of SequenceBindingSite definition */
    public void readSequenceSubsetBody(InputStream fis, SequenceSubset subset)
	throws SequenceIOException {

	DataInputStream dis = new DataInputStream(fis);
	// expect sequence:
	String expected = "(sequence";
	String word = readWord(dis);
	if (!word.equals(expected)) {
	    throw new SequenceIOException("SequenceBindingSite: " + expected + " excepted instead of " + word);
	}

	Sequence sequence = readSequenceBody(dis);

	expected = ")"; // end of reading sequence
	word = readWord(dis);
	if (!word.equals(expected)) {
	    throw new SequenceIOException("SequenceBindingSite: " + expected 
					  + " excepted instead of " + word);
	}

	subset.setSequence(sequence);

	word = readWord(dis);
	int seqLen = 0;
	try {
	    seqLen = Integer.parseInt(word);
	}
	catch (NumberFormatException e) {
	    throw new SequenceIOException("SequenceBindingSite: Could not parse length of sequence: " + word);	
	}
	for (int i = 0; i < seqLen; ++i) {
	    word = readWord(dis);
	    int index = 0;
	    try {
		index = Integer.parseInt(word);
	    }
	    catch (NumberFormatException e) {
		throw new SequenceIOException("SequenceBindingSite: Could not parse index of sequence: " + word);	
	    }
	    subset.addIndex(index);
	}	
    }

    /** resds body of SequenceBindingSite definition */
    public Sequence readSequenceBody(InputStream is) throws SequenceIOException {
	DataInputStream dis = new DataInputStream(is);
	// read type: "DNA", "RNA", "DNA_G", "RNA_G", "DNA_AG", "RNA_AG" (A: ambigous, G: gap)
	String word = readWord(dis);
	int sequenceType = 0;
	Alphabet alphabet;
	if (word.equals("DNA")) {
	    sequenceType = SequenceTools.DNA_SEQUENCE;
	    alphabet = DnaTools.DNA_ALPHABET;
	}
	else if (word.equals("RNA")) {
	    sequenceType = SequenceTools.RNA_SEQUENCE;
	    alphabet = DnaTools.RNA_ALPHABET;
	}
	else if (word.equals("DNA_A")) {
	    sequenceType = SequenceTools.AMBIGUOUS_DNA_SEQUENCE;
	    alphabet = DnaTools.AMBIGUOUS_DNA_ALPHABET;
	}
	else if (word.equals("RNA_A")) {
	    sequenceType = SequenceTools.AMBIGUOUS_RNA_SEQUENCE;
	    alphabet = DnaTools.AMBIGUOUS_RNA_ALPHABET;
	}
	else {
	    log.severe("sequence type not yet implemented: " + word);
	    throw new SequenceIOException("sequence type not yet implemented: " + word);
	}
	String name = readWord(dis); // reads name of sequence
	String seq = readWord(dis); // sequence data in one word
	Sequence sequence = null;
	try {
	    sequence = new SimpleSequence(seq, name, alphabet);
	}
	catch (UnknownSymbolException e) {
	    throw new SequenceIOException("unknown sequence characters found in sequence: " 
					  + name);
	}
	return sequence;
    }


	/** reads a single word from data stream */
	public static String readWord(DataInputStream dis) {
		String s = new String("");
		char c = ' ';
		
		// first skip white space
		do {
			try {
			   c = (char)dis.readByte();
			}
			catch (IOException e) {
				break; // end of file reached
			}
		}
		while (Character.isWhitespace(c));
		
		if (!Character.isWhitespace(c)) {
			s = s + c;
		}
		else {
			log.finest("found word: " + s);
			return s;
		}
		
		while (true) {
			try {
			   c = (char)dis.readByte();
			 if (!Character.isWhitespace(c)) {
				 s = s + c;
			 }
			 else {
				 break;
			 }
			}
			catch (IOException e) {
				break; // end of file reached
			}
		}
		log.finest("found word: " + s);
		return s;
	}

}
