package sequence;

/** specifies constant strings that are used to specify the status of a residue as a general property */
public interface SequenceStatus {

    public static final String name = "sequence_status";

    public static final String adhoc = "adhoc";

    public static final String fragment = "fragment";

    public static final String optimized = "optimized";

}
