package sequence;

public interface SequenceSubset {

    public Sequence getSequence();

    public Sequence getSubsetSequence();

    /** adds reference to position pos */
    public void addIndex(int pos);

    /** returns position in sequence of n'th element of subset */
    public int getIndex(int n);
    
    public Residue getResidue(int n);

    public void setSequence(Sequence s);

    /** returns size of subset */
    public int size();
    
    public String toString();

}
