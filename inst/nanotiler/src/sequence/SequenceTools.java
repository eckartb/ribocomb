package sequence;

public class SequenceTools {

    public static final int UNKNOWN_SEQUENCE = 2;

    public static final int AMBIGUOUS_DNA_SEQUENCE = 1;

    public static final int DNA_SEQUENCE = 2;

    public static final int AMBIGUOUS_RNA_SEQUENCE = 3;

    public static final int RNA_SEQUENCE = 4;

    public static final int PROTEIN_SEQUENCE = 5;

    public static final int DNA_RNA_SEQUENCE = 6;

    /** counts number of identical residues. Returns -1 if sequences do not have same size. */
    public static int countIdentical(Sequence s1, Sequence s2) {
	if (s1.size() != s2.size()) {
	    return -1;
	}
	int counter = 0;
	for (int i = 0; i < s1.size(); ++i) {
	    if (s1.getResidue(i).getSymbol().equals(s2.getResidue(i).getSymbol())) {
		++counter;
	    }
	}
	return counter;
    }

}
