package sequence;

import java.util.ArrayList;
import java.util.List;

public class SimpleAlphabet implements Alphabet {

    int type = SequenceTools.UNKNOWN_SEQUENCE;

    private List<LetterSymbol> symbols = new ArrayList<LetterSymbol>();

    public SimpleAlphabet() {
    }

    public SimpleAlphabet(int type) {
	this.type = type;
    }

    public SimpleAlphabet(String letters) {
	for (int i = 0; i < letters.length(); ++i) {
	    try {
		add(new SimpleLetterSymbol(letters.charAt(i), this));
	    }
	    catch (UnknownSymbolException e) {
	    }
	}
    }

    public SimpleAlphabet(String letters, int type) {
	// System.out.println("Initalizing alphabet " + letters + " " + type);
	for (int i = 0; i < letters.length(); ++i) {
	    try {
		add(new SimpleLetterSymbol(letters.charAt(i), this, true)); // allow that letter is not yet part of alphabet
	    }
	    catch (UnknownSymbolException e) {
		System.out.println("Unknown symbol: " + e.getMessage() + " : " + letters.charAt(i));
	    }
	}
	this.type = type;
	// System.out.println("Result of initialization: " + this.toString());
    }

    public void add(LetterSymbol symbol) {
	symbols.add(symbol);
    }
    
    public boolean contains(LetterSymbol symbol) {
	return symbols.contains(symbol);
    }

    public boolean contains(char c) {
	for (int i = 0; i < size(); ++i) {
	    if (getSymbol(i).getCharacter() == c) {
		return true;
	    }
	}
	return false;
    }

    public int getType() { return type; }

    public LetterSymbol getSymbol(int n) throws IndexOutOfBoundsException {
	return (LetterSymbol)(symbols.get(n));
    }
    
    public void setType(int type) { this.type = type; }
    
    public int size() { return symbols.size(); }

    public String toString() {
	StringBuffer result = new StringBuffer();
	for (int i = 0; i < size(); ++i) {
	    result.append("" + getSymbol(i) + " ");
	}
	return result.toString();
    }

}
