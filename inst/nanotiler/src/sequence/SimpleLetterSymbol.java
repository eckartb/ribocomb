package sequence;

import java.util.logging.Logger;

import generaltools.StringTools;

public class SimpleLetterSymbol implements LetterSymbol {

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    private Alphabet alphabet;
    private char character;
    
    public SimpleLetterSymbol(char c, Alphabet alphabet) throws UnknownSymbolException {
	//setAlphabet(alphabet); Doesn't work for some reason, null is passed instead of actual reference
	this.alphabet = alphabet;
	this.character = c;
	if (!alphabet.contains(c)) {
	    throw new UnknownSymbolException("Unknown symbol: " + c);
	}
    }

    public SimpleLetterSymbol(char c, Alphabet alphabet, boolean allow) throws UnknownSymbolException {
	//setAlphabet(alphabet); Doesn't work for some reason, null is passed instead of actual reference
	this.alphabet = alphabet;
	this.character = c;
	if (!allow) {
	    if (!alphabet.contains(c)) {
		throw new UnknownSymbolException("Unknown symbol: " + c);
	    }
	}
    }

    /** returns new copy of Symbol, however only shallow copy of alphabet */
    public Object clone() {
	LetterSymbol newSymbol = null;
	try {
	    newSymbol = new SimpleLetterSymbol(character, alphabet);
	}
	catch (UnknownSymbolException e) {
	    // should never be here!
	    log.warning("Warning: internal error in SimpleLetterSymbol.clone()!");
	}
	return newSymbol;
    }

    public char getCharacter() { return this.character; }

    public Alphabet getAlphabet() { return alphabet; }
    
    public boolean equals(Object other) {
	if (other instanceof LetterSymbol) {
	    LetterSymbol s = (LetterSymbol)other;
	    return getCharacter() == s.getCharacter();
	}
	return false;
    }

    public void setAlphabet(Alphabet alpbabet) { 
	this.alphabet = alphabet; 
    }

    public void setCharacter(char c) { this.character = c; }

    /** output of character */
    public String toString() {
	String s = StringTools.stringFromChar(this.character);
	if (alphabet != null) {
	    int type = alphabet.getType();
	    if (type == SequenceTools.DNA_SEQUENCE) {
		s += " DNA";
	    }
	    else if (type == SequenceTools.AMBIGUOUS_DNA_SEQUENCE) {
		s += " DNA_A";
	    }
	    else if (type == SequenceTools.RNA_SEQUENCE) {
		s += " RNA";
	    }
	    else if (type == SequenceTools.AMBIGUOUS_RNA_SEQUENCE) {
		s += " RNA_A";
	    }
	    else if (type == SequenceTools.PROTEIN_SEQUENCE) {
		s += " PROTEIN";
	    }
	    else if (type == SequenceTools.DNA_RNA_SEQUENCE) {
		s += " DNA_RNA";
	    }
	    else {
		s += " UNKNOWN";
	    }
	}
	else {
	    s += " null";
	}
	return s;
    }

}
