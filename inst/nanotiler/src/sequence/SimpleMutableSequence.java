package sequence;

/**
 * A mutable sequence can have residues inserted, removed, and
 * changed anywhere in the sequence.
 */
public class SimpleMutableSequence extends SimpleSequence implements MutableSequence {

    public SimpleMutableSequence(String s, String name, Alphabet alphabet) throws UnknownSymbolException {
	super(s, name, alphabet);
    }

    public SimpleMutableSequence(Sequence s) throws UnknownSymbolException {
	super(s.sequenceString(), s.getName(), s.getAlphabet());
    }

    public void removeResidue(int pos) {
	Residue res = getSequence().remove(pos);
	for(int i = pos; i < getSequence().size(); ++i) {
	    Residue r = getSequence().get(i);
	    r.setPos(r.getPos() - 1);
	}

    }

    public void insertResidue(Residue residue, int position) {
	getSequence().add(position, residue);
	residue.setParentObject(this);
	for(int i = position + 1; i < getSequence().size(); ++i) {
	    Residue r = getSequence().get(i);
	    r.setPos(r.getPos() + 1);
	    
	}
    }

    public void mutateResidue(Residue residue, int position) {
	getSequence().set(position, residue);

    }

}
