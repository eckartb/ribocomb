package sequence;

import java.util.ArrayList;
import java.util.List;

import generaltools.Namable;

public class SimpleSequence implements Sequence {

    private Alphabet alphabet;
    private Object parent = null;
    private String name;
    private double weight = 1.0;
    private List<Residue> sequence = new ArrayList<Residue>();

    /** default constructor is just used internally for the clone method */
    private SimpleSequence() { }

    public SimpleSequence(String s, String name, Alphabet alphabet) throws UnknownSymbolException {
	this.alphabet = alphabet;
	this.name = name;
	for (int i = 0; i < s.length(); ++i) {
	    Residue residue = new SimpleResidue(new SimpleLetterSymbol(s.charAt(i), alphabet), this, i);
	    addResidue(residue);
	}
    }

    public SimpleSequence(String name, Alphabet alphabet)  {
	this.alphabet = alphabet;
	this.name = name;
    }

    public List<Residue> getSequence() {
	return sequence;
    }

    
    
    /** removes all residue information but not alphabet and name */
    public void clear() { sequence.clear(); }

    /** clone method for deep copy. Only reference to parent and alphabet reference is shallow! TODO : verify */
    public Object cloneDeep() {
	SimpleSequence seq = new SimpleSequence();
	seq.alphabet = this.alphabet; // shallow copy for alphabet!
	seq.parent = this.parent; // only shallow copying of parent!
	seq.name = new String(this.name);
	seq.weight = this.weight;
	for (int i = 0; i < size(); ++i) {
	    seq.addResidue( (Residue)((this.getResidue(i)).cloneDeep()) ); // TODO verify; why not cloneDeep??
	}
	return seq;
    }

    /** orders sequences by name */
//     public int compareTo(Object other) {
// 	if (other instanceof Namable) {
// 	    return getName().compareTo(((Namable)other).getName());
// 	}
// 	return 0;
//     }

    public Alphabet getAlphabet() { return alphabet; }

    public String getName() { return name; }

    public double getWeight() { return weight; }

    /** adds clone of residue (with sequence and position set */
    public void addResidue(Residue residue)  { 
	Residue newResidue = residue; // NOT CLONE ANYMORE!?(Residue)(residue.clone());
	newResidue.setPos(this.size());
	newResidue.setParentObject(this);
	sequence.add(newResidue);
    }

    public Residue getResidue(int n) throws IndexOutOfBoundsException { 
	Residue r = (Residue)(sequence.get(n));
	return (Residue)(sequence.get(n)); 
    }

    public Object getParentObject() { return this.parent; }

    /** returns true, if both sequences describe the same entity, even if they were cloned. */
    public boolean isProbablyIdentical(Sequence other) {
	if ((size() != other.size()) || (!getName().equals(other.getName()))) {
	    return false;
	}
	if (getParentObject() != null) {
	    if (other.getParentObject() == null) {
		return false;
	    }
	    Object o1 = getParentObject();
	    Object o2 = other.getParentObject();
	    if (o1 instanceof Namable) {
		if (! (o2 instanceof Namable)) {
		    return false;
		}
		String parentName = ((Namable)o1).getName();
		String otherParentName = ((Namable)o2).getName();
		if (!parentName.equals(otherParentName)) {
		    return false;
		}
	    }
	}
	else if (other.getParentObject() != null) {
	    return false;
	}
	if (!sequenceString().equals(other.sequenceString())) {
	    return false;
	}
	return true;
    }

    /** removes n'th residue */
    public void removeChild(int n)  { 
	sequence.remove(n); 
    }

    /** TODO SLOW! */
    public String sequenceString() {
	String s = new String();
	for (int i = 0; i < size(); ++i) {
	    char c = getResidue(i).getSymbol().getCharacter();
	    assert c != 'B'; // TODO just for debbugging, take out later
	    s = s + c;
	}
	return s;
    }

    /** sets name */
    public void setName(String s) { this.name = s; }
    
    public void setParent(Object obj) { this.parent = obj; }

    public int size() { return sequence.size(); }
    
    public int getResidueCount() { return sequence.size(); }

    public String getTypeString() {
	int type = alphabet.getType();
	if (type == SequenceTools.DNA_SEQUENCE) {
	    return "DNA";
	}
	else if (type == SequenceTools.AMBIGUOUS_DNA_SEQUENCE) {
	    return "DNA_A";
	}
	else if (type == SequenceTools.RNA_SEQUENCE) {
	    return "RNA";
	}
	else if (type == SequenceTools.AMBIGUOUS_RNA_SEQUENCE) {
	    return "RNA_A";
	}
	return "UNKNOWN";
    }

    /** Sets "weight" of sequence */
    public void setWeight(double weight) { this.weight = weight; }
    
    public String toString() {
	String s = "(Sequence ";
	s = s + getName() + " ";
	s = s + getTypeString() + " ";
	s = s + sequenceString();
	s = s + " )";
	return s;
    }

}
