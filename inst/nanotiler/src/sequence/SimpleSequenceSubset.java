package sequence;

import java.util.ArrayList;
import java.util.List;

public class SimpleSequenceSubset implements SequenceSubset {

    private Sequence sequence;

    private Sequence subsetSequence;

    private List<Integer> indices = new ArrayList<Integer>();

    public SimpleSequenceSubset() {
    }

    public SimpleSequenceSubset(Sequence s, int startPos, int length) {
	this.sequence = s;
	for (int i = 0; i < length; ++i) {
	    addIndex(startPos + length);
	}
    }

    public Sequence getSequence() { return sequence; }

    public Sequence getSubsetSequence() { return subsetSequence; }

    /** adds reference to position pos */
    public void addIndex(int pos) { indices.add(new Integer(pos)); update(); }

    /** returns position in sequence of n'th element of subset */
    public int getIndex(int n) { 
	Integer integer = (Integer)(indices.get(n));
	return integer.intValue();
    }
    
    public Residue getResidue(int n) { return sequence.getResidue(getIndex(n)); }

    public void setSequence(Sequence s) { this.sequence = s; update(); }

    /** returns size of subset */
    public int size() { return indices.size(); }
    
    /** write to string */
    public String toString() {
	String result = "(Subset " + sequence.toString() + " ";
	result = result + size() + " ";
	for (int i = 0; i < size(); ++i) {
	    result = result + (getIndex(i)+1) + " ";
	}
	result = result + " ) ";
	return result;
    }

    private void update() {
	subsetSequence = null; // TODO : not yet implemented
    }

}
