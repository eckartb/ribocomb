package sequence;

import java.io.InputStream;
import java.util.*;

/** similar to BasicAlignment. The "Uneven" indicates that the sequences are gapless, may have different lengths
 * and are not "aligned".
 */
public class SimpleUnevenAlignment implements UnevenAlignment {

    private List<Sequence> list = new ArrayList<Sequence>();

    private Map<String, Integer> map = new HashMap<String, Integer>();

    public SimpleUnevenAlignment() { }

    public SimpleUnevenAlignment(Collection<Sequence> sequences) throws DuplicateNameException {
	Iterator<Sequence> it = sequences.iterator();
	while (it.hasNext()) {
	    addSequence(it.next());
	}
    }

    /** Parses sequence strings or simplified FASTA format (each sequence in one line, no white space) */
    public SimpleUnevenAlignment(String[] lines, Alphabet alphabet) throws DuplicateNameException, UnknownSymbolException {
	boolean fastaMode = false;
	int startLine = 0;
	int stride = 1;
	if (lines[0].charAt(0) == '>') {
	    fastaMode = true;
	    startLine = 1;
	    stride = 2;
	}
	int sCount = 0;
	for (int i = startLine; i < lines.length; i+= stride) {
	    String name = null;
	    if (fastaMode) {
		name = lines[i-1].substring(1, lines[i-1].length()).trim();
	    }
	    else {
		name = "s" + (sCount + 1);
	    }
	    addSequence(new SimpleSequence(lines[i].trim(), name, alphabet));
	    ++sCount;
	}
    }

    public void addSequence(Sequence sequence) throws DuplicateNameException { 
	if (getIndex(sequence.getName()) >= 0) {
	    throw new DuplicateNameException("Sequence name already exists: " + sequence.getName());
	}
	this.list.add(sequence); update(); 
    }
    
    public void clear() {
	list.clear();
    }
    
    public Sequence getSequence(int n) throws IndexOutOfBoundsException {
	return (Sequence)(list.get(n));
    }

    /** returns index of sequence with this name, -1 if not found. */
    public int getIndex(String name) {
	Integer in = (Integer)map.get(name);
	if (in == null) {
	    // throw new UnknownSequenceException();
	    return -1; // not found
	}
	return in.intValue();
    }

    /** retrieves sequence by name */
    public Sequence getSequence(String name) {
	return getSequence(getIndex(name));
    }

    public void read(InputStream is) {
	// TODO
    }
    
    public Sequence get(int n){
		return getSequence(n);
	}

    /** returns number of sequences */
    public int getSequenceCount() {
	return list.size();
    }
    
    public int getSequenceId(String name) {
	   for (int i = 0; i < list.size(); ++i) {
	   	if (list.get(i).getName().equals(name)) {
	   		return i;
	   	}
	   }
	   return -1;		
	}

    public int getTotalResidueCount() {
	int result = 0;
	for (int i = 0; i < list.size(); ++i) {
	    result += getSequence(i).size();
	}
	return result;
    }

    /** Sets weights of all sequences. Interpretation depends on circumstance. Could be: concentrations or "active state" for riboswitch */
    public void setWeights(double[] weights) {
	assert weights.length == getSequenceCount();
	for (int i = 0; i < weights.length; ++i) {
	    getSequence(i).setWeight(weights[i]);
	}
    }
    
    public int size() {
		return getSequenceCount();
	}

    public String toString() {
	String result = "(UnevenAlignment " + getSequenceCount() + " ";
	for (int i = 0; i < getSequenceCount(); ++i) {
	    result = result + getSequence(i) + " ";
	}
	result = result + ")";
	return result;
    }
    
    public String toSequenceString() {
	String result = "";
	for (int i = 0; i < getSequenceCount(); ++i) {
	    result = result + getSequence(i).sequenceString();
	}
	return result;
    }
    
    public void update() {
	// rebuild hash map:
	map.clear();
	for (int i = 0; i < getSequenceCount(); ++i) {
	    map.put(getSequence(i).getName(), new Integer(i));
	}

    }


}
