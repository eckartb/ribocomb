package sequence;

public interface Symbol {

    /** compare symbol */
    public boolean equals(Object other);

    /** convert to string representation */
    public String toString();

}
