package sequence;

/** similar to BasicAlignment. The "Uneven" indicates that the sequences are gapless, may have different lengths
 * and are not "aligned".
 */
public interface UnevenAlignment extends BasicAlignment {

    public int getTotalResidueCount();

}
