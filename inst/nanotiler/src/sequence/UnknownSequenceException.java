package sequence;

import generaltools.ApplicationException;

public class UnknownSequenceException extends ApplicationException {

    public UnknownSequenceException() {
	super();
    }

    public UnknownSequenceException(String s) {
	super(s);
    }

}
