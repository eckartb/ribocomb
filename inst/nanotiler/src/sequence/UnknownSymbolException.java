package sequence;

import generaltools.ApplicationException;

public class UnknownSymbolException extends ApplicationException {

    public UnknownSymbolException() {
	super();
    }

    public UnknownSymbolException(String s) {
	super(s);
    }

}
