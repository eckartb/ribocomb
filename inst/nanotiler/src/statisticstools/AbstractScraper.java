package statisticstools;

import java.util.List;
import java.util.Properties;

import static statisticstools.StatisticsVocabulary.*;

public abstract class AbstractScraper extends SimpleStatisticsResult {

    protected List<String> lines;
    protected String errorMessage;

    public AbstractScraper(List<String> lines) {
	assert lines != null;
	this.lines = lines;
	assert this.lines != null;	
    }

    public Properties generate() {
	Properties properties = new Properties();
	if (validate()) {
	    properties.setProperty(SUCCESS, TRUE_STRING);
	    // properties.setProperty(P_VALUE, "" + pValue);
	}
	else {
	    properties.setProperty(SUCCESS, FALSE_STRING);
	    properties.setProperty(ERROR_MESSAGE, errorMessage);
	}
	return properties;
    }

    /** returns true if lines have been parse and no error was generated */
    public boolean validate() {
	return (errorMessage != null) && (errorMessage.length() == 0);
    }

    /** To be implemented by class */
    protected abstract void parse();

}
