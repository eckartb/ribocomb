package statisticstools;

import generaltools.StringTools;
import java.util.List;
import java.util.Properties;

import static statisticstools.StatisticsVocabulary.*;

public class BindewaldScraper extends AbstractScraper {

    public BindewaldScraper(List<String> lines) {
	super(lines);
	parse();
	setName(StatisticsVocabulary.BINDEWALD);
    }

    public BindewaldScraper(String[] linesOrig) {
	this(StringTools.convertArrayToList(linesOrig));
    }

    protected void parse() {
	assert this.lines != null;
	for (int i = 0; i < lines.size(); ++i) {
	    String[] words = lines.get(i).split(" ");
	    if ((words.length == 6) && (words[3].equals("p-value"))) {
		try {
		    setPValue(Double.parseDouble(words[5]));
		    this.errorMessage = "";
		    break;
		}
		catch (NumberFormatException nfe) {
		    this.errorMessage = nfe.getMessage();
		    assert this.errorMessage != null && this.errorMessage.length() > 0;
		    break;
		}
	    }
	}
	// assert this.errorMessage != null; // either empty string or real error
    }

}
