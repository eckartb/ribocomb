package statisticstools;

import launchtools.SimpleQueueManager;
import java.util.List;

public class BinomialTests {

    private static SimpleQueueManager qm = SimpleQueueManager.getInstance();

    /** Computes p-value of Fisher exact test for a contingency table of two groups a and b. Usage: fisher.sh a1 a2 b1 b2 */
    public static double fisherTest(int a1, int a2, int b1, int b2) {
	List<String> lines = qm.shell("fisher.sh " 
				      + a1 + " " + a2 + " " + b1 + " " 
				      + b2 + " p");
	assert lines.size() == 1;
	String[] words = lines.get(0).split(" ");
	assert words.length == 3;
	return Double.parseDouble(words[2]);
    }  

}
