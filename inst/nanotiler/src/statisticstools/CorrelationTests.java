package statisticstools;

import launchtools.SimpleQueueManager;
import generaltools.ParsingException;
import generaltools.Randomizer;
import generaltools.StringTools;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.io.*;

import static statisticstools.StatisticsVocabulary.*;

public class CorrelationTests {

    public static final StatisticsResult DUMMY_CORRELATION = generateDummyCorrelation();

    public static boolean NDEBUG = true; // if false, use debugging (no deletion of temp files for example)

    /** Directory with binaries */
    private static String BIN_DIR = System.getenv("NANOTILER_HOME");

    private static SimpleQueueManager qm = SimpleQueueManager.getInstance();

    public static StatisticsResult generateDummyCorrelation() {
	StatisticsResult result = new SimpleStatisticsResult();
	result.setProperty(StatisticsVocabulary.ESTIMATE, "0.0");
	result.setProperty(StatisticsVocabulary.ESTIMATE_METHOD, "dummy");
	return result;
    }

    /** parses single line of format "value1 value2" , with value1, value2 representing double */
    private static List<Double> parsePairedDataLine(String line) throws ParsingException {
	assert line != null;
	line = line.trim();
	String[] words = line.split(" ");
	if (words.length != 2) {
	    throw new ParsingException("Two words expected in paired data line: " + line);
	}
	List<Double> result = new ArrayList<Double>();
	try {
	    result.add(new Double(Double.parseDouble(words[0])));
	    result.add(new Double(Double.parseDouble(words[1])));
	}
	catch (NumberFormatException nfe) {
	    throw new ParsingException("Bad number format in paired data line: " + line);
	}
	return result;
    }

    /** reads list of value pairs */
    public static List<List<Double> > readPairedData(InputStream is) throws IOException, ParsingException {
	List<List<Double> > data = new ArrayList<List<Double> >();
	String[] lines = StringTools.readAllLines(is);
	for (int i = 0; i < lines.length; ++i) {
	    String line = lines[i].trim();
	    if ((line != null) && (line.length() > 0) && (line.charAt(0) != '#')) { 
		data.add(parsePairedDataLine(line));
	    }
	}
	return data;
    }

    private static void writeData(OutputStream os, List<List<Double> > data) throws IOException {
	PrintStream ps = new PrintStream(os);
	for (int i = 0; i < data.size(); ++i) {
	    for (int j = 0; j < data.get(i).size(); ++j) {
		ps.print("" + data.get(i).get(j).doubleValue());
		if (j < (data.get(i).size() - 1)) {
		    ps.print(" ");
		}
		else {
		    ps.println("");
		}
	    }
	}
    }

    /** Computes correlation coefficient, p-values etc using external call to R. method can have values "spearman", "pearson" and others TODO */
    public static StatisticsResult computeCorrelation(List<List<Double> > data, String method) {
	if ("dummy".equals(method)) {
	    return DUMMY_CORRELATION;
	}
	String fileName = "";
	File tmpFile;
	try {
	    tmpFile = File.createTempFile("corr", ".dat");
	    if (NDEBUG) {
		tmpFile.deleteOnExit();
	    }
	    fileName = tmpFile.getAbsolutePath();
	    FileOutputStream fos = new FileOutputStream(tmpFile);
	    writeData(fos, data);
	    fos.close();
	}
	catch (IOException ioe) {
	    Properties properties = new Properties();
	    properties.setProperty(SUCCESS,FALSE_STRING);
	    properties.setProperty(ERROR_MESSAGE,"IOException while writing data: " + ioe.getMessage());
	    return new SimpleStatisticsResult(properties);
	}
	String command = BIN_DIR + "/correlation.sh " + fileName;
	if (method != null) {
	    if (method.equals("bindewald")) {
		command = BIN_DIR + "/bindewald.pl " + fileName; // special case for "Bindewald correlation"! See $RSCRIPTS/bindewald.pl for more info
	    }
	    else {
		command = command + " " + method;
	    }
	}
	// System.out.println("Launching command: " + command);
	List<String> lines = qm.shell(command);
	assert lines != null;
	// delete file
	if (NDEBUG) {
	    if (tmpFile != null) {
		tmpFile.delete();
	    }
	}
	// System.out.println("R output for method " + method);
	// for (int i = 0; i < lines.size(); ++i) {
	// System.out.println(lines.get(i));
	// }
	StatisticsResult statResult = interpretCorrelationOutput(lines, method);
	// System.out.println("the p-value is: " + statResult.getPValue());
	return statResult;
    }  

    /** Computes correlation coefficient, p-values etc using external call to R. method can have values "spearman", "pearson" and others TODO */
    public static StatisticsResult computeCorrelation(double[] v1, double[] v2, String method) {
	assert v1.length == v2.length;
	List<List<Double> > list = new ArrayList<List<Double> >();
	for (int i = 0; i < v1.length; ++i) {
	    List<Double> row = new ArrayList<Double>();
	    row.add(new Double(v1[i]));
	    row.add(new Double(v2[i]));
	    list.add(row);
	}
	return computeCorrelation(list, method);
    }

    /** interprets ("scrapes") R output */
    public static StatisticsResult interpretCorrelationOutput(List<String> lines, String method) {
	assert lines != null;
	if (method == null) { // dummy values ! TODO: just for debugging, take out later
	    Random rnd = Randomizer.getInstance();
	    Properties properties = new Properties();
	    properties.setProperty(SUCCESS,TRUE_STRING); // TODO remove mock result
	    properties.setProperty(P_VALUE,"" + 0.1 * rnd.nextDouble());
	    properties.setProperty(CORRELATION_COEFF, "" + 0.1 * rnd.nextDouble());
	    return new SimpleStatisticsResult(properties);
	}
	StatisticsResult scraper = null;
	if (method.equals(SPEARMAN)) {
	    scraper = new RSpearmanScraper(lines);
	}
	else if (method.equals(BINDEWALD)) {
	    scraper = new BindewaldScraper(lines); // same format
	}
	else {
	    System.err.println("Unknown correlation method: " + method);
	    throw new RuntimeException("Unknown correlation method: " + method);
	}
	assert scraper != null;

// 	Properties result = scraper.getProperties();
// 	// there must be a p-value or an error message defined 
// 	assert (result.getProperty(P_VALUE) != null) || (result.getProperty(ERROR_MESSAGE) != null)
// 	assert result != null;
	return scraper;
    }

}
