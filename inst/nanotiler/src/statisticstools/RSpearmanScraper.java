package statisticstools;

import generaltools.StringTools;
import java.util.List;
import java.util.Properties;

import static statisticstools.StatisticsVocabulary.*;

public class RSpearmanScraper extends AbstractScraper {

    public RSpearmanScraper(List<String> lines) {
	super(lines);
	parse();
	setName(StatisticsVocabulary.SPEARMAN);
    }

    public RSpearmanScraper(String[] linesOrig) {
	this(StringTools.convertArrayToList(linesOrig));
    }

    protected void parse() {
	assert this.lines != null;
	int lastLine = 0;
	for (int i = 0; i < lines.size(); ++i) {
	    lastLine = i;
	    String[] words = lines.get(i).split(" ");
	    if ((words.length == 6) && (words[3].equals("p-value"))) {
		try {
		    setPValue(Double.parseDouble(words[5]));
		    this.errorMessage = "";
		}
		catch (NumberFormatException nfe) {
		    this.errorMessage = nfe.getMessage();
		    assert this.errorMessage != null && this.errorMessage.length() > 0;
		}
		break;
	    }
	}
	if ((this.errorMessage == null) || (this.errorMessage.length() == 0)) {
	    for (int i = lastLine + 1; i < lines.size(); ++i) {
		String[] words = lines.get(i).trim().split(" ");
		if ((words.length == 2) && words[0].equals("sample") && words[1].equals("estimates:") && ((i+2) < lines.size())) {
		    setProperty(StatisticsVocabulary.ESTIMATE_METHOD, lines.get(i+1).trim());
		    try {
			setProperty(StatisticsVocabulary.ESTIMATE, "" + Double.parseDouble(lines.get(i+2).trim()));
		    }
		    catch (NumberFormatException nfe) {
			this.errorMessage = "Error parsing correlation coefficient value: " + nfe.getMessage();
			assert this.errorMessage != null && this.errorMessage.length() > 0;
		    }
		    break;
		}
	    }

	}
	assert (!validate()) || (getProperty(StatisticsVocabulary.ESTIMATE) != null); // make sure correlation coefficient is stored
	// assert this.errorMessage != null; // either empty string or real error
    }

}
