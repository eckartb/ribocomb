package statisticstools;

import generaltools.SimplePropertyCarrier;
import java.util.Properties;

import static statisticstools.StatisticsVocabulary.*;

/** Simple implementation of properties object and p-Value */
public class SimpleStatisticsResult extends SimplePropertyCarrier implements StatisticsResult {

    private double pValue = 1.0;

    public static final String DEFAULT_NAME = "unnamed";

    protected String name = DEFAULT_NAME;

    public SimpleStatisticsResult() {
	super();
    }

    public SimpleStatisticsResult(Properties properties) {
	super(properties);
    }

    public SimpleStatisticsResult(double pValue) {
	super();
	setPValue(pValue);
    }

    public SimpleStatisticsResult(double pValue, Properties properties) {
	super(properties);
	setPValue(pValue);
    }

    /** orders statistics results such that smaller p-values are first */
    public int compareTo(Object other) {
	StatisticsResult otherResult = (StatisticsResult)(other);
	double thisP = this.getPValue();
	double otherP = otherResult.getPValue();
	if (thisP < otherP) {
	    return -1;
	}
	else if (thisP > otherP) {
	    return 1;
	}
	return 0;
    }

    public String getName() {
	return name;
    }
    

    /** returns p-value of result */
    public double getPValue() { return pValue; }

    public void setName(String s) { 
	this.name = s;
    }

    public void setPValue(double pValue) {
	this.pValue = pValue;
	properties.setProperty(P_VALUE, "" + pValue);
    }

    public String toString() { return getName() + " p-value: " + getPValue() + " " + properties.toString(); }

    public boolean validate() {
	return super.validate() && (getProperty(ERROR_MESSAGE) == null);
    }

}
