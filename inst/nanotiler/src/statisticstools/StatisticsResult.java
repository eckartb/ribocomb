package statisticstools;

import generaltools.PropertyCarrier;
import generaltools.Validatable;
import generaltools.Namable;

public interface StatisticsResult extends Comparable, Namable, PropertyCarrier, Validatable {

    /** returns p-value of result */
    double getPValue();

}
