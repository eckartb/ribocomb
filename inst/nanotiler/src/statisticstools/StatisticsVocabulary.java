package statisticstools;

public interface StatisticsVocabulary {

    public static final String TRUE_STRING = "true";
    public static final String FALSE_STRING = "false";
    public static final String METHOD = "method";
    public static final String SUCCESS = "success";
    public static final String[] BOOL_OUTCOMES = {TRUE_STRING, FALSE_STRING};
    public static final String[] SUCCESS_OUTCOMES = BOOL_OUTCOMES;
    public static final String BINDEWALD = "bindewald";
    public static final String SPEARMAN = "spearman";
    public static final String PEARSON = "pearson";
    public static final String WILCOXON = "wilcoxon";
    public static final String TTEST = "t-test";
    public static final String PAIRED = "paired";
    public static final String[] PAIRED_OUTCOMES = BOOL_OUTCOMES;
    public static final String P_VALUE = "p-value";
    public static final String CORRELATION_COEFF = "correlation-coeff";
    public static final String ERROR_MESSAGE = "error_message";
    public static final String ESTIMATE_METHOD = "estimate_method"; // either rho or cor or whaetever correlation coefficient method is used in R
    public static final String ESTIMATE = "estimate"; // stands for value of correlation coefficient
    

}
