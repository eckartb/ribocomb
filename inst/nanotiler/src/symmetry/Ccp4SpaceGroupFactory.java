package symmetry; 

import java.io.*;
import java.util.logging.*;
import generaltools.ParsingException;

import org.testng.annotations.*;
import static symmetry.PackageConstants.*;

public class Ccp4SpaceGroupFactory implements SpaceGroupFactory {

    private SpaceGroup[] spaceGroups;
    private Logger log = Logger.getLogger(LOGFILE_DEFAULT);
    public Ccp4SpaceGroupFactory() {
    }

    public Ccp4SpaceGroupFactory(InputStream is ) throws IOException, ParsingException {
	init(is);
    }

    public void init(InputStream is) throws IOException, ParsingException {
	SpaceGroupParser parser = new Ccp4SpaceGroupParser();
	this.spaceGroups = parser.parse(is);
	assert isValid();
    }
    

    /** generate space group by CCP4 number */
    public SpaceGroup generate(int number) throws SymmetryException {
	if (!isValid()) {
	    throw new SymmetryException("No space group definitions read yet!");
	}
	boolean found = false;
	for (int i =0; i < spaceGroups.length; ++i) {
	    System.out.println("Trying number " + spaceGroups[i].getNumber() + " " + number);
	    if (spaceGroups[i].getNumber() == number) {
		found = true;
		return (SpaceGroup)(spaceGroups[i].clone());
	    }
	}
	if (!found) {
	    throw new SymmetryException("Invalid symmetry group number!");
	}
	return null;
    } 

    /** generate space group by name and SpaceGroupConventions convention id */
    public SpaceGroup generate(String name) throws SymmetryException {
	return generate(name, SpaceGroupConvention.DEFAULT_CONVENTION);
    }

    /** generate space group by name and SpaceGroupConventions convention id */
    public SpaceGroup generate(String name, int conventionId) throws SymmetryException {
	if (!isValid()) {
	    throw new SymmetryException("No space group definitions read yet!");
	}
	if ((conventionId < 0) || (conventionId >= SpaceGroupConvention.COUNT)) {
	    throw new SymmetryException("Illegal symmetry group convention id!");
	}
	boolean found = false;
	for (int i =0; i < spaceGroups.length; ++i) {
	    if (spaceGroups[i].getName(conventionId).equals(name)) {
		return (SpaceGroup)(spaceGroups[i].clone());
	    }
	}
	if (!found) {
	    throw new SymmetryException("Invalid symmetry group name!");
	}
	return null;
    }

    /** generate space group by name and by convention name */
    public SpaceGroup generate(String name, String conventionName) throws SymmetryException {
	int conventionId = SpaceGroupConvention.findConventionId(conventionName);
	return generate(name, conventionId);
    }

    public boolean isValid() {
	if (spaceGroups == null) {
	    log.warning("Space group array is null!");
	    return false;
	}
	if (spaceGroups.length == 0) {
	    log.warning("No space groups defined!");
	    return false;
	}
	
	boolean found = false;
	for (int i = 0; i < spaceGroups.length; ++i) {
	    if (spaceGroups[i] == null) {
		continue;
	    }
	    else {
		found = true;
	    }
	    if (!spaceGroups[i].isValid()) {
		log.info("Invalid space group found: " + spaceGroups[i].toString());
		return false;
	    }
	}
	if (!found) {
	    log.warning("All space groups were zero!");
	}
	return found; // return true if non-zero entries found
    }

}
