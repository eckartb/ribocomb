package symmetry;

import generaltools.ParsingException;
import generaltools.StringTools;
import java.io.*;
import java.util.logging.*;
import java.util.ResourceBundle;
import static symmetry.PackageConstants.*;

import org.testng.annotations.*;

public class Ccp4SpaceGroupParser implements  SpaceGroupParser {

    private static final String BEGIN_TOKEN = "begin_spacegroup";
    private static final String END_TOKEN = "end_spacegroup";
    private static Logger log = Logger.getLogger(DEFAULT_LOGGERNAME);

    private String[] parseParenthesized(String line) {
	return line.split("\'");
    }

    private void parseLine(String line, SpaceGroup group) throws ParsingException {
	if ((line == null) || (line.length() == 0) || (line.charAt(0) == '#')) {
	    return;
	}
	String[] tokens = line.split("\\s");
	if ((tokens.length < 2) || (tokens[0].charAt(0) == '#')) {
	    return;
	}
	String key = tokens[0];
	String[] parenthesized = parseParenthesized(line);
	if (key.compareToIgnoreCase("number") == 0) {
	    int number = Integer.parseInt(tokens[tokens.length-1]);
	    assert number > 0;
	    group.setNumber(number);
	    log.finest("Setting number: " + number + " " + group.getNumber());
	}
	else if (key.compareToIgnoreCase("symbol") == 0) {
	    String key2 = tokens[1];
	    if (key2.compareToIgnoreCase("ccp4") == 0) {		
		group.setName(tokens[tokens.length-1], "ccp4");
	    }
	    else if (key2.compareToIgnoreCase("xHM") == 0) {
		if (parenthesized.length != 2) {
		    // System.out.println("Weird xHM case: " + line);
		    // group.setName(parenthesized[parenthesized.length-1], "");
		}
		else {
		    assert parenthesized.length == 2;
		    group.setName(parenthesized[parenthesized.length-1], "xHM");
		}
	    }
	    else if (key2.compareToIgnoreCase("Hall") == 0) {
		assert parenthesized.length == 2;
		group.setName(parenthesized[parenthesized.length-1], "Hall");
	    }
	}
    }

    private int parseSpaceGroup(String[] lines, SpaceGroup group, int currentLine) throws ParsingException {
	// skip to next "begin_spacegroup"
	while (currentLine < lines.length) {
	    if (lines[currentLine].indexOf(BEGIN_TOKEN) == 0) {
		break; // begin found!
	    }
	    ++currentLine;
	}
	while (++currentLine < lines.length) {
	    if (lines[currentLine].indexOf(END_TOKEN) == 0) {
		return ++currentLine; // set current line to one after end_spacegroup
	    }
	    log.finest("Parsing line: " + currentLine + " " + lines[currentLine]);
	    parseLine(lines[currentLine], group);
	}
	assert group.isValid();
	assert group.getNumber() > 0;
	log.finest("Parsed space group: " + group);
	return currentLine;
    }

    private int countSpaceGroups(String[] lines) {
	int count = 0;
	for (int i = 0; i < lines.length; ++i) {
	    if (lines[i].indexOf(BEGIN_TOKEN) == 0) {
		++count;
	    }
	}
	return count;
    }

    public SpaceGroup[] parse(InputStream is) throws IOException, ParsingException {
	String[] lines = StringTools.readAllLines(is);
	for (int i = 0; i < lines.length; ++i) {
	    lines[i] = lines[i].trim(); // remove extra whitespace
	}
	int numGroups = countSpaceGroups(lines);
	System.out.println("Number of space groups: " + numGroups);
	SpaceGroup[] result = new SpaceGroup[numGroups];
	int currentLine = 0; // pointer to current line
	int groupCounter = 0;
	while ((currentLine < lines.length) && (groupCounter < result.length)) {
	    //	    System.out.println("Working on line: " + currentLine + " " + lines[currentLine]);
	    result[groupCounter] = new DefaultSpaceGroup();
	    log.finest("Space group before parsing: " + groupCounter + NEWLINE 
		     + result[groupCounter]);
	    currentLine = parseSpaceGroup(lines, result[groupCounter], currentLine);
	    assert result[groupCounter] != null;
	    if (result[groupCounter].getNumber() <= 0) {
		log.severe("Bad space group: " + result[groupCounter]);
	    }
	    assert result[groupCounter].getNumber() > 0;
	    assert result[groupCounter].isValid();
	    ++groupCounter;
	    // System.out.println("Current line: " + currentLine);
	}
	System.out.println("Read groups: " + groupCounter + " with lines: " + currentLine);
	assert groupCounter == result.length;
	for (int i = 0; i < result.length; ++i) {
	    assert result[i].isValid();
	}
	return  result;
    }

    /** tests reading of CCP4 parameter file */
    @Test(groups={"fast", "new"}) 
    public void testParse() {
	
 	ResourceBundle rb = ResourceBundle.getBundle("symmetry_test");
 	String parameterFile = rb.getString("spacegroupFile");
 	assert parameterFile != null;
 	assert parameterFile.length() > 0;
 	Ccp4SpaceGroupParser parser = new Ccp4SpaceGroupParser();
 	SpaceGroup[] spaceGroups = null;
	try {
 	    FileInputStream fis = new FileInputStream(parameterFile);
 	    spaceGroups = parser.parse(fis);
 	}
 	catch (java.io.IOException ioe) {
 	    System.out.println(ioe.getMessage());
 	    ioe.printStackTrace();
 	    assert false;
 	}
  	catch (ParsingException pe) {
  	    pe.printStackTrace();
  	    System.out.println("Parsing exception: " + pe.getMessage());
  	    assert false;
  	}
 	assert spaceGroups != null;
 	assert spaceGroups.length > 0;
 	for (int i = 0; i < spaceGroups.length; ++i) {
 	    System.out.println("Space group: " + (i+1));
 	    System.out.println(spaceGroups[i].toString());	    
	    assert spaceGroups[i].isValid();
 	}
    }


}
