package symmetry;

import tools3d.Vector3D;
import static symmetry.PackageConstants.*;


/** Defines crystallographic cell dimensions in terms of side lengths a, b, c
 * and angle alpha, beta, gamma.
 * Coding convention: angles are expressed internally in radians NOT in degree! Degrees are only
 * used for the final output to the end-user. */

public interface Cell {

    static final double DEFAULT_LENGTH = 100.0;
    
    static final double DEFAULT_ANGLE = 0.5 * Math.PI;
    
    double getA();
    
    double getB();
    
    double getC();
    
    double getAlpha();
    
    double getBeta();
    
    double getGamma();
    
    void setA(double a);
    
    void setB(double b);
    
    void setC(double c);
    
    void setAlpha(double alpha);
    
    void setBeta(double beta);
    
    void setGamma(double gamma);
    
    String toString();

    Vector3D getX();

    Vector3D getY();
    
    Vector3D getZ();

}
