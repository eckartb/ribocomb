package symmetry;

import tools3d.Vector3D;
import static symmetry.PackageConstants.*;

/** Defines crystallographic cell dimensions in terms of side lengths a, b, c
 * and angle alpha, beta, gamma.
 * Coding convention: angles are expressed internally in radians NOT in degree! Degrees are only
 * used for the final output to the end-user. */

public class DefaultCell implements Cell {

    public static final double DEFAULT_LENGTH = 100.0;
    public static final double DEFAULT_ANGLE = 0.5 * Math.PI;

    private double a, b, c;

    private double alpha, beta, gamma;

    private Vector3D x, y, z;

    public DefaultCell() {
	a = DEFAULT_LENGTH;
	b = DEFAULT_LENGTH;
	c = DEFAULT_LENGTH;
	alpha = DEFAULT_ANGLE;
	beta = DEFAULT_ANGLE;
	gamma = DEFAULT_ANGLE;
	updateXyz();
    }

    public DefaultCell(double a, double b, double c,
		double alpba, double beta, double gamma) {
	this.a = a;
	this.b = b;
	this.c = c;
	this.alpha = alpha;
	this.beta = beta;
	this.gamma = gamma;
	updateXyz();
    }

    public double getA() { return this.a; }

    public double getB() { return this.b; }

    public double getC() { return this.c; }

    public double getAlpha() { return alpha; }

    public double getBeta() { return beta; }

    public double getGamma() { return gamma; }

    
    public void setA(double a) { this.a = a; updateXyz(); }

    public void setB(double b) { this.b = b; updateXyz(); }

    public void setC(double c) { this.c = c; updateXyz(); }

    public void setAlpha(double alpha) { this.alpha = alpha; updateXyz(); }

    public void setBeta(double beta) { this.beta = beta; updateXyz(); }

    public void setGamma(double gamma) { this.gamma = gamma; updateXyz(); }

    public String toString() {
	return "CELL " + a + " " + b + " " + c + " "
	    + RAD2DEG * alpha + " " + RAD2DEG * beta + " " + RAD2DEG * gamma;
    }

    private void updateXyz() {
	Vector3D zero = new Vector3D(0, 0, 0);
	x = new Vector3D(getA(), 0, 0);
	y = new Vector3D(getB() * Math.cos(getGamma()), getB() * Math.sin(getGamma()), 0.0);
	double zxh = Math.cos(getBeta());
	double zyh = Math.cos(getAlpha()) * Math.sin(getGamma());
	double zzh = Math.sqrt(1.0 - (zxh * zxh) - (zyh*zyh));
	z = new Vector3D(zxh, zyh, zzh);
	z.scale(getC());
    }

    public Vector3D getX() { return x; }

    public Vector3D getY() { return y; }

    public Vector3D getZ() { return z; }
}
