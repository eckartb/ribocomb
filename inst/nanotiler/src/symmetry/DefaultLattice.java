package symmetry;

public class DefaultLattice implements Lattice {

    private Cell cell;

    private SpaceGroup spaceGroup;

    public DefaultLattice(Cell cell, SpaceGroup spaceGroup) {
	this.cell = cell;
	this.spaceGroup = spaceGroup;
    }

    public Cell getCell() { return this.cell; }

    public SpaceGroup getSpaceGroup() { return this.spaceGroup; }

    public void setCell(Cell cell) { this.cell = cell; }

    public void setSpaceGroup(SpaceGroup spaceGroup) { this.spaceGroup = spaceGroup; }

}
