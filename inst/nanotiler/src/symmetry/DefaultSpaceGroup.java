package symmetry;

import generaltools.StringTools;
import static symmetry.PackageConstants.*;

/** defines space group. Stores information roughly equivalent to entry of CCP4 
 * space group definition file */
public class DefaultSpaceGroup implements  SpaceGroup {

    private int number;

    private String[] names;

    /** constructor is only package accessible. Use factory class! */
    public DefaultSpaceGroup(int number, String[] names) {
	assert number != 0;
	assert names.length == SpaceGroupConvention.COUNT;
	this.number = number;
	this.names = names;
    }

    /** constructor is only package accessible. Use factory class! */
    public DefaultSpaceGroup() {
	this.number = 0;
	this.names = new String[SpaceGroupConvention.COUNT];
    }

    public Object clone() {
	assert names != null;	
	assert this.names.length == SpaceGroupConvention.COUNT;
	String[] clonedNames = StringTools.cloneLines(names);

	return new DefaultSpaceGroup(number, clonedNames);
    }

    /** returns space group number according to CCP4 convention */
    public int getNumber() { return this.number; }

    public boolean isValid() { return (number > 0) && (names != null) && (names.length > 0); }

    /** returns space group number according to CCP4 convention */
    public void setNumber(int number) { 
	assert number > 0;
	this.number = number;
    }

    /** returns space group name according to default naming convention */
    public String getName() { return getName(SpaceGroupConvention.DEFAULT_CONVENTION); }

    /** returns space group name according to default naming convention */
    public void setName(String name) { setName(name, SpaceGroupConvention.DEFAULT_CONVENTION); }

    /** returns space group name according to specified naming convention */
    public String getName(String convention) {
	int conventionId = SpaceGroupConvention.findConventionId(convention);
	return getName(conventionId);
    }

    /** returns space group name according to specified naming convention */
    public void setName(String name, String convention) {
	int conventionId = SpaceGroupConvention.findConventionId(convention);
	setName(name, conventionId);
    }

    /** returns space group name according to specified naming convention */
    public String getName(int conventionId) {
	assert conventionId >= 0;
	assert conventionId < names.length;
	return names[conventionId];
    }

    /** returns space group name according to specified naming convention */
    public void setName(String name, int conventionId) {
	assert conventionId >= 0;
	assert names != null;
	assert conventionId < names.length;
	names[conventionId] = name.substring(0, name.length());
    }


    public String toString() {
	StringBuffer buf = new StringBuffer();
	buf.append("begin_spacegroup" + NEWLINE);
	buf.append("number  " + getNumber() + NEWLINE);
	assert names != null;
	assert SpaceGroupConvention.COUNT == names.length;
	for (int i = 0; i < SpaceGroupConvention.COUNT; ++i) {
	    if ((names[i] != null) && (names[i].length() > 0)) {
		buf.append(SpaceGroupConvention.NAMES[i] + " " + names[i] + NEWLINE);
	    }
	}
	buf.append("end_spacegroup" + NEWLINE);
	return buf.toString();
    }

}
