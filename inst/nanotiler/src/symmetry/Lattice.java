package symmetry;

public interface Lattice {

    Cell getCell();

    SpaceGroup getSpaceGroup();

    void setCell(Cell cell);

    void setSpaceGroup(SpaceGroup spaceGroup);


}
