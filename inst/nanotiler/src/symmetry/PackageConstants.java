package symmetry;

import java.io.File;

public class PackageConstants {

    public static final String LOGFILE_DEFAULT = "NanoTiler_debug";

    public static final String DEFAULT_LOGGERNAME = "NanoTiler_debug";
    
    public static final String NEWLINE = System.getProperty("line.separator");

    public static final String ENDL = NEWLINE;

    public static String SLASH = File.separator; // either "/" or "\"  depending on Unix of Windows

    public static final double DEG2RAD = Math.PI / 180.0;

    public static final double RAD2DEG = 180.0 / Math.PI;

}
