package symmetry;

public interface SpaceGroup {

    Object clone();
    
    /** returns space group number according to CCP4 convention */
    int getNumber();

    /** returns space group name according to CCP4 convention */
    String getName();
    /** returns space group name according to CCP4 convention */
    String getName(int conventionId);
    /** returns space group name according to CCP4 convention */
    String getName(String conventionName);
    /** returns true if valid definition */
    boolean isValid(); 

    /** sets space group number according to CCP4 convention */
    void setNumber(int number);
    /** sets space group name according to CCP4 convention */
    void setName(String name);
    /** sets space group name according to CCP4 convention */
    void setName(String name, int conventionId);
    /** sets space group name according to CCP4 convention */
    void setName(String name, String conventionName);


}
