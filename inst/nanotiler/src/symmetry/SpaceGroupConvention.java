package symmetry;

public class SpaceGroupConvention {

    public static final int CCP4 = 0;
    public static final int HALL = 1;
    public static final int XHM = 2;
    public static final int OLD = 3;
    public static final int LAUE = 4;
    public static final int PATT = 5;
    public static final int PGRP = 6;
    public static final int DEFAULT_CONVENTION = CCP4;
    public static final int NOT_FOUND_ID = -1;
    public static final String[] NAMES = {"ccp4", "Hall", "xHM", "old", "laue",
					  "patt", "pgrp"};
    /** number of defined conventions */
    public static final int COUNT = NAMES.length;

    /** returns index of convention */
    public static int findConventionId(String conventionName) {
	assert conventionName != null;
	assert conventionName.length() > 0;
	String uName = conventionName.toUpperCase();
	for (int i = 0; i < COUNT; ++i) {
	    if (uName.equals(NAMES[i].toUpperCase())) {
		return i;
	    }
	}
	return NOT_FOUND_ID; // not found
    }


}
