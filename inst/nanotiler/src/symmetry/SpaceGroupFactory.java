package symmetry; 

import java.io.*;
import generaltools.ParsingException;

public interface SpaceGroupFactory {

    boolean isValid();

    /** generate space group by CCP4 number */
    SpaceGroup generate(int number) throws SymmetryException; 

    /** generate space group by name and default SpaceGroupConventions convention id */
    SpaceGroup generate(String name) throws SymmetryException;

    /** generate space group by name and SpaceGroupConventions convention id */
    SpaceGroup generate(String name, int conventionId) throws SymmetryException;

    /** generate space group by name and by convention name */
    SpaceGroup generate(String name, String conventionName) throws SymmetryException;

    void init(InputStream is) throws IOException, ParsingException;
    
}
