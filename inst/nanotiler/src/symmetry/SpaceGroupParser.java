package symmetry;

import java.io.*;
import generaltools.ParsingException;

public interface SpaceGroupParser {

    SpaceGroup[] parse(InputStream is) throws IOException, ParsingException;

}
