package symmetry;

public class SymmetryException extends Exception {

    public SymmetryException(String message) {
	super(message);
    }

}
