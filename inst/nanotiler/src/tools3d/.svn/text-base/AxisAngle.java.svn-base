package tools3d;

import org.testng.annotations.*;

import generaltools.TestTools;

/** Axis angle representation of rotation matrix
 @see http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToAngle/index.htm
*/
public class AxisAngle {

    private double angle;
    private Vector3D axis;

    public AxisAngle() {
	this.angle = 0.0;
	this.axis = new Vector3D(0,0,0);
    }
    
    public AxisAngle(double angle, double x, double y, double z) {
	this.angle = angle;
	this.axis = new Vector3D(x,y,z);
	this.axis.normalize();
    }

    public AxisAngle(double angle, Vector3D axis) {
	this.angle = angle;
	this.axis = new Vector3D(axis);
	this.axis.normalize();
    }
    
    public AxisAngle(AxisAngle other) {
	this(other.getAngle(), other.getAxis());
    }

    public AxisAngle(Matrix3D m) {
	this(toAxisAngle(m));
    }

    public Vector3D getAxis() { return this.axis; }

    public double getAngle() { return angle; }


    /**
       This requires a pure rotation matrix 'm' as input.
    */
    public static AxisAngle toAxisAngle(double[][] m) {
	assert m.length == 3;
	assert m[0].length == 3;
	double angle,x,y,z; // variables for result
	double epsilon = 0.01; // margin to allow for rounding errors
	// optional check that input is pure rotation, 'isRotationMatrix' is defined here:
	// http://www.euclideanspace.com/maths/algebra/matrix/orthogonal/rotation/
	// assert ( isRotationMatrix(m) : "not valid rotation matrix"); // check during debugging only
	if (Math.abs(m[0][1]-m[1][0])< epsilon
	    && Math.abs(m[0][2]-m[2][0])< epsilon
	    && Math.abs(m[1][2]-m[2][1])< epsilon) {
	    // singularity found
	    // first check for identity matrix which must have +1 for all terms in leading diagonal
	    // and zero in other terms
	    if ( (Math.abs(m[0][1]+m[1][0]) < 0.1)
		 && (Math.abs(m[0][2]+m[2][0]) < 0.1)
		 && (Math.abs(m[1][2]+m[2][1]) < 0.1)
		 && ((Math.abs(m[0][0]+m[1][1]+m[2][2])-3) < 0.1)) {
		// this singularity is identity matrix so angle = 0
		// note epsilon is greater in this case since we only have to distinguish between 0 and 180 degrees
		return new AxisAngle(0,1,0,0); // zero angle, arbitrary axis
	    }
	    // otherwise this singularity is angle = 180
	    angle = Math.PI;
	    x = (m[0][0]+1)/2;
	    if (x > 0) { // can only take square root of positive number, always true for orthogonal matrix
		x = Math.sqrt(x);
	    } else {
		x = 0; // in case matrix has become de-orthogonalised
	    }
	    y = (m[1][1]+1)/2;
	    if (y > 0) { // can only take square root of positive number, always true for orthogonal matrix
		y = Math.sqrt(y);
	    } else {
		y = 0; // in case matrix has become de-orthogonalised
	    }
	    z = (m[2][2]+1)/2;
	    if (z > 0) { // can only take square root of positive number, always true for orthogonal matrix
		z = Math.sqrt(z);
	    } else {
		z = 0; // in case matrix has become de-orthogonalised
	    }
	    boolean xZero = (Math.abs(x)<epsilon);
	    boolean yZero = (Math.abs(y)<epsilon);
	    boolean zZero = (Math.abs(z)<epsilon);
	    boolean xyPositive = (m[0][1] > 0);
	    boolean xzPositive = (m[0][2] > 0);
	    boolean yzPositive = (m[1][2] > 0);
	    if (xZero && !yZero && !zZero) { // implements  last 6 rows of above table
		if (!yzPositive) y = -y;
	    } else if (yZero && !zZero) {
		if (!xzPositive) z = -z;
	    } else if (zZero) {
		if (!xyPositive) x = -x;
	    }
	    return new AxisAngle(angle,x,y,z);
	}
	double s = Math.sqrt((m[2][1] - m[1][2])*(m[2][1] - m[1][2])+(m[0][2] - m[2][0])*(m[0][2] - m[2][0])+(m[1][0] - m[0][1])*(m[1][0] - m[0][1])); // used to normalise
	if (Math.abs(s) < 0.001) s=1; // prevent divide by zero, should not happen if matrix is orthogonal and should be
	// caught by singularity test above, but I've left it in just in case
	angle = Math.acos(( m[0][0] + m[1][1] + m[2][2] - 1)/2);
	x = (m[2][1] - m[1][2])/s;
	y = (m[0][2] - m[2][0])/s;
	z = (m[1][0] - m[0][1])/s;
	// } // bug ?
	return new AxisAngle(angle,x,y,z);
    }

    public static AxisAngle toAxisAngle(Matrix3D m) {
	return toAxisAngle(m.toArray());
    }

    /** Axis Angle to Quaternion
    * A rotation around an arbitrary axis in 3D space can be converted to a quaternion as follows

    * If the axis of rotation is        (ax, ay, az)- must be a unit vector
    and the angle is                  theta (radians)
    * w   =   cos(theta/2)
    * x   =   ax * sin(theta/2)
    * y   =   ay * sin(theta/2)
    * z   =   az * sin(theta/2)
    * The axis must first be normalized. If the axis is a zero vector (meaning there is no rotation), the quaternion should be set to the rotation identity quaternion.
    from: http://www.gamedev.net/reference/articles/article1095.asp
    */
    public Vector4D toQuaternion() {
	double theta = getAngle();
	double w   =   Math.cos(theta/2);
	double x   =   axis.getX() * Math.sin(theta/2);
	double y   =   axis.getY() * Math.sin(theta/2);
	double z   =   axis.getZ() * Math.sin(theta/2);
	return new Vector4D(x,y,z,w);
    }

    /** from: http://www.gamedev.net/reference/articles/article1095.asp */
    public static AxisAngle fromQuaternion(Vector4D q) {
	double qx = q.getX();
	double qy = q.getY();
	double qz = q.getZ();
	double scale = Math.sqrt(qx*qx+qy*qy+qz*qz);
	if (scale == 0.0) {
	    return new AxisAngle(0.0, 0.0, 0.0, 1.0); // no real rotation
	}
	double ax= qx / scale;
	double ay= qy / scale;
	double az= qz / scale;
	double angle= 2 * Math.acos(q.getW());
	return new AxisAngle(angle, ax, ay, az);
    }

    /** Computes translation distance plus angle squared */
    public static double distanceAngleSquareNorm(Matrix4D m4, double angleWeight) {
	double distNorm = m4.subvector().length();
	assert distNorm >= 0.0;
	AxisAngle axisAngle = AxisAngle.toAxisAngle(m4.submatrix());
	double angle = axisAngle.getAngle(); // get rotation angle; because from acos, angle is always non-negative, not angle normalization necessary
	assert angle >= 0.0;
	double angleNorm = angle * angle * angleWeight;
	return distNorm + angleNorm;
    }

    /** Computes translation distance plus angle squared */
    public static double distanceQuatNorm(Matrix4D m1, Matrix4D m2, double angleWeight) {
	double distNorm = m2.subvector().distance(m1.subvector());
	assert distNorm >= 0.0;
	AxisAngle aa1 = new AxisAngle(m1.submatrix());
	AxisAngle aa2 = new AxisAngle(m2.submatrix());
	Vector4D q1 = aa1.toQuaternion();
	Vector4D q2 = aa2.toQuaternion();
	double qDist = q1.minus(q2).length(); // quaternion difference
	double angleNorm = angleWeight * qDist; 
	return distNorm + angleNorm;
    }

    /** Computes translation distance plus angle squared */
    public static double distanceAngleNorm(Matrix4D m4, double angleWeight) {
	double distNorm = m4.subvector().length();
	assert distNorm >= 0.0;
	AxisAngle axisAngle = AxisAngle.toAxisAngle(m4.submatrix());
	double angle = axisAngle.getAngle(); // get rotation angle; because from acos, angle is always non-negative, not angle normalization necessary
	assert angle >= 0.0;
	double angleNorm = angle * angleWeight;
	return distNorm + angleNorm;
    }

    /** Converts axis angle representation to 3D matrix. 
     * From: http://www.euclideanspace.com/maths/geometry/rotations/conversions/angleToMatrix/index.htm
     */
    public Matrix3D generateMatrix() {
	double c = Math.cos(angle);
	double s = Math.sin(angle);
	double t = 1 - c;
	Vector3D v = new Vector3D(axis);
	v.normalize();
	double x = v.getX();
	double y = v.getY();
	double z = v.getZ();
	double xx = t*x*x +   c; double xy = t*x*y - z*s; double xz = t*x*z + y*s;
	double yx = t*x*y + z*s; double yy = t*y*y +   c; double yz = t*y*z - x*s;
	double zx = t*x*z - y*s; double zy = t*y*z + x*s; double zz = t*z*z +   c;
	return new Matrix3D(xx, xy, xz,
			    yx, yy, yz,
			    zx, zy, zz);
    }


    

    @Test(groups={"new"})
    public void testGenerateMatrix() {
	String methodName = "testGenerateMatrix";
	System.out.println(TestTools.generateMethodHeader(methodName));
	double angle = 0.25 * Math.PI;
	Vector3D axis = new Vector3D(1.0, 2.0, 3.0);
	AxisAngle axisAngle = new AxisAngle(angle, axis);
	Matrix3D matrix = axisAngle.generateMatrix(); // convert to matrix
	AxisAngle axisAngle2 = new AxisAngle(matrix); // convert again to axis angle
	assert Math.abs(axisAngle.getAngle() - axisAngle2.getAngle()) < 0.1; // should be similar
	assert axisAngle.getAxis().distance(axisAngle2.getAxis()) < 0.1; // should be similar
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

}
