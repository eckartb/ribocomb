/**
 * 
 */
package tools3d;

import java.awt.geom.Point2D;

/**
 * @author Eckart Bindewald
 *
 */
public class LinearProjection implements SpaceToPlaneProjection {

	double a11 = 1.0;
	double a12 = 0.0;
	double a13 = 0.0;
	double a21 = 0.0;
	double a22 = 1.0;
	double a23 = 0.0;
	double b1 = 0.0;
	double b2 = 0.0;
	
	LinearProjection() { }
	
	/* central projection method. Generates 2d point from 3d vector using 3x2 matrix
	 */
	public Point2D project(Vector3D v) {
		double x = a11 * v.getX() + a12 * v.getY() + a13 * v.getZ() + b1;
		double y = a21 * v.getX() + a22 * v.getY() + a23 * v.getZ() + b2;
		return new Point2D.Double(x, y);
	}

	/** convinience method so the user has to know only equivalent screen and space 
	 * dimensions.
	 * Here: look onto xy plane, ignore z
	 */
	public void setXYDimensions(double x3DMin, double x3DMax,
				                double y3DMin, double y3DMax,
				                double x2DMin, double x2DMax,
				                double y2DMin, double y2DMax) {
		a11 = (x2DMax - x2DMin) / (x3DMax - x3DMin); a12 = 0.0; a13 = 0.0;
		a21 = 0.0; a22 = (y2DMax - y2DMin) / (y3DMax - y3DMin); a23 = 0.0;
		b1 = x2DMin - (a11 * x3DMin);
		b2 = y2DMin - (a22 * y3DMin);
	}
	
	/** convinience method so the user has to know only equivalent screen and space 
	 * dimensions.
	 *
	 */
	public void setXZDimensions(double x3DMin, double x3DMax,				 
				                double z3DMin, double z3DMax,
				                double x2DMin, double x2DMax,
				                double y2DMin, double y2DMax) {
		a11 = (x2DMax - x2DMin) / (x3DMax - x3DMin); a12 = 0.0; a13 = 0.0;
		a21 = 0.0; a22 = 0.0; a23 = (y2DMax - y2DMin) / (z3DMax - z3DMin); 
		b1 = x2DMin - (a11 * x3DMin);
		b2 = y2DMin - (a22 * z3DMin);
	}

    public void moveCameraNorth(double v) { b1 += v; }

    public void moveCameraWest(double v) { b2 += v; }

    public void moveCameraEast(double v) { b2 -= v; }

    public void moveCameraSouth(double v) { b1 -= v; }
	

    public void rotateCameraNorth(double angle) { }

    public void rotateCameraWest(double angle) { }

    public void rotateCameraEast(double angle) { }

    public void rotateCameraSouth(double angle) { }

    /** not correct implementation so far */
    public void zoom(double factor) {
	a11 *= factor;
	a12 *= factor;
	a13 *= factor;
	a21 *= factor;
	a22 *= factor;
	a23 *= factor;
    }
}
