package tools3d;

import java.util.Random;
import generaltools.Randomizer;
import java.util.logging.Logger;

public class MCSuperpose implements Superpose {

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    public static Random rnd = Randomizer.getInstance();
    public static final double PI2 = Math.PI * 2.0;
    private int iterMax = 100000;
    private double rmsLim = 0.01;
    private double angleStep = Math.PI;
    private int numTrial = 1;
    private int centerId = -1;

    /** returns rndom number between min and max */
    private double generateRandomDouble(double min, double max) {
	assert max >= min;
	double delta = max - min;
	double x = rnd.nextDouble();
	x *= delta;
	x += min;
	return x;
    }

    /** returns rndom number between min and max */
    private double generateRandomGaussian(double center, double scale) {
	double x = rnd.nextGaussian();
	x *= scale;
	x += center;
	return x;
    }

    private double mutateValue(double x, double step) {
	// return generateRandomDouble(x-step, x+step);
	return generateRandomGaussian(x, step);
    }

    public static double computeRms(Vector3D[] constCoord,
				    Vector3D[] varCoord) {
	double sum = 0.0;
	for (int i = 0; i < constCoord.length; ++i) {
	    sum += (constCoord[i].minus(varCoord[i])).lengthSquare();
	}
	sum /= constCoord.length;
	return Math.sqrt(sum);
    }

    public static double computeRms(Vector3D[] constCoord,
				    Vector3D[] varCoord,
				    double phi, // defines rotation axis
				    double theta, // defines rotation axis
				    double angle) {
	assert constCoord.length == varCoord.length;
	double sum = 0.0;
	Vector3D axis = Vector3DTools.generateCartesianFromPolar(phi, theta, 1.0);
	Matrix3D rotationMatrix = Matrix3D.rotationMatrix(axis, angle);
	SuperpositionResult supResult = new SuperpositionResult();
	supResult.setRotationMatrix(rotationMatrix);
	for (int i = 0; i < constCoord.length; ++i) {
	    Vector3D vTmp = rotationMatrix.multiply(varCoord[i]); // Matrix3DTools.rotate(varCoord[i], axis, angle);
	    assert vTmp.distance(supResult.returnTransformed(varCoord[i])) < 0.1;
	    sum += (constCoord[i].minus(vTmp)).lengthSquare();
	}
	sum /= constCoord.length;
	double rms = Math.sqrt(sum);
	return rms;
    }

    /** returns transformation info that transforms varCoord into constCoord. Ignores shifts */
    SuperpositionResult rotationRun(Vector3D[] constCoord,
				    Vector3D[] varCoord) {
	// log.fine("Starting rotationRun!");
	// start with random orientation:
	double phi = rnd.nextDouble()*PI2;
	double theta = rnd.nextDouble()*Math.PI;
	double angle = rnd.nextDouble()*PI2;
	double angleStepCurr = angleStep;
	double bestRms = computeRms(constCoord, varCoord, phi, theta, angle);
	int iter = 0;
	// log.fine("Initial rms: " + bestRms + " " + phi + " " + theta + " " + angle);
	// log.fine("" + iter + " " + iterMax + " " 
	// + bestRms + " " + rmsLim);
	while ((iter < iterMax) && (bestRms > rmsLim)) {
	    ++iter;
	    // angleStepCurr = angleStep; //  * (0.99*(1.0 - ((double)iter / (double)iterMax))+0.01);
	    double phiNew = mutateValue(phi, angleStepCurr);
	    double thetaNew = mutateValue(theta, 0.5 * angleStepCurr);
	    if (thetaNew < 0.0) {
		thetaNew = 0.0;
	    }
	    if (thetaNew > Math.PI) {
		thetaNew = Math.PI;
	    }
	    double angleNew = mutateValue(angle, angleStepCurr);
	    double rms = computeRms(constCoord, varCoord, phiNew, thetaNew, angleNew);
	    if (rms < bestRms) {
		phi = phiNew;
		theta = thetaNew;
		angle = angleNew;
		bestRms = rms;
		// log.fine("Iteration " + (iter+1) + " : new best rms: " + bestRms + " " + phi
		// 	   + " " + theta + " " + angle);
		angleStepCurr *= 0.9;

		// TODO : debug code, take out later
		// loop invariant
		Vector3D axis = Vector3DTools.generateCartesianFromPolar(phi, theta, 1.0);
		Matrix3D rotationMatrix = Matrix3D.rotationMatrix(axis, angle);
		assert constCoord[1].distance(rotationMatrix.multiply(varCoord[1])) <= (2.0*bestRms);

	    }
	}
	// generate axis:
	Vector3D axis = Vector3DTools.generateCartesianFromPolar(phi, theta, 1.0);
	Matrix3D rotationMatrix = Matrix3D.rotationMatrix(axis, angle);
	SuperpositionResult result = new SuperpositionResult();
	result.setRms(bestRms);
	result.setRotationMatrix(rotationMatrix);
	assert constCoord[1].distance(rotationMatrix.multiply(varCoord[1])) < 2.0 * bestRms;
	assert constCoord[1].distance(result.returnTransformed(varCoord[1])) <= 2.0 * result.getRms();
	// TODO : debug code, take out later !
// 	for (int i = 0; i < constCoord.length; ++i) {
// 	    assert ((rotationMatrix.multiply(varCoord[i])).minus(constCoord[i])).length() <= 2.0 * result.getRms(); 
// 	}

    // log.fine("Finishing rotationRun! RMS: " + bestRms + " iterations: " + iter);
	return result;
    }

    /** sets center id: instead of cener of gravity, structures get superposed at point n */
    public void setCenterId(int n) { this.centerId = n; }

    /** change varCoord such that it closest to constCoord using
     * only tranlation and rotation */
    public SuperpositionResult superpose(Vector3D[] constCoord,
					 Vector3D[] varCoord) {
	assert constCoord.length == varCoord.length;
	Vector3D varCoordOrig = null;
	Vector3D debugPos = new Vector3D(varCoord[1]);
	Vector3D shift1 = new Vector3D();
	Vector3D shift2 = new Vector3D();
	if (centerId >= 0) {
	    assert centerId < constCoord.length;
	    shift1.copy(constCoord[centerId]);
	    shift2.copy(varCoord[centerId]);
	    varCoordOrig = new Vector3D(varCoord[centerId]);
	    // log.fine("Remembering orginal coordinate " + (centerId+1) + " : " + varCoordOrig);
	}
	else {
	    shift1 = Vector3DTools.computeCenterGravity(constCoord);
	    shift2 = Vector3DTools.computeCenterGravity(varCoord);
	}
	// log.fine("using shifts: " + shift1 + " " + shift2);
	for (int i = 0; i < constCoord.length; ++i) {
	    constCoord[i].sub(shift1);
	    varCoord[i].sub(shift2);
	    // log.fine("Shifted: " + (i+1) + " " + constCoord[i] + " " + varCoord[i]);
	}
	SuperpositionResult result = rotationRun(constCoord, varCoord);
	for (int i = 1; i < numTrial; ++i) { //is never used because numTrial always equals 1
	    // log.fine("Superposition trial: " + (i+1));
	    SuperpositionResult newResult = rotationRun(constCoord, varCoord);
	    if (newResult.getRms() < result.getRms()) {
		result = newResult;
	    }
	}
	result.setShift1(shift1);
	result.setShift2(shift2);
	Matrix3D rotMatrix = result.getRotationMatrix();
	for (int i = 0; i < constCoord.length; ++i) {
	    // assert ((rotMatrix.multiply(varCoord[i])).minus(constCoord[i])).length() <= 2.0 * result.getRms(); 
	    constCoord[i].add(shift1);
	    if (i == 1) {
		// log.fine("tmp test: " + varCoord[i]);
	    }
	    varCoord[i] = rotMatrix.multiply(varCoord[i]);
	    if (i == 1) {
		// log.fine("tmp test (rotated): " + varCoord[i]);
	    }
	    varCoord[i].add(shift1); // TODO was : add(shift2) , bug!?
	    if (i == 1) {
		// log.fine("tmp test (shifted): " + varCoord[i]);
	    }
	    // assert (varCoord[i].minus(constCoord[i])).length() <= 2.0 * result.getRms(); 
	}
	// if centerId is active, first vectors must match
	if (varCoordOrig != null) {
	    // log.finest("last check before leaving MCSuperpose: " + (centerId+1) + " orig constant: " + constCoord[centerId] + " original variable: " + varCoordOrig + " transformed orig: " + result.returnTransformed(varCoordOrig) + " distance: " + constCoord[centerId].distance(result.returnTransformed(varCoordOrig)));
	}
	else {
	    assert centerId < 0;
	}
	// log.fine("result of superpose: " + result);
	// assert ( (centerId < 0) || (constCoord[centerId].distance(result.returnTransformed(varCoordOrig)) < 0.1));
	// check if transformation works as expected:
	if (result.returnTransformed(debugPos).distance(varCoord[1]) > 0.01) {
	    log.warning("Error detected in transformation!!! " 
			       + debugPos + " " + result.returnTransformed(debugPos) + " "
			       + varCoord[1]);
	    // log.fine("transformation: " + result);
	    // assert result.returnTransformed(debugPos).distance(varCoord[1]) < 0.01;
	}
	// assert result.returnTransformed(debugPos).distance(varCoord[1]) < 0.01;
	// check if RMS makes sense:
	// assert varCoord[1].distance(constCoord[1]) <= ( 2.0 * result.getRms() );
	return result;
    }


}
