package tools3d;

import org.testng.annotations.*;
import generaltools.TestTools;
import generaltools.Randomizer;
import java.util.Random;
import java.io.*;

/**
 * centrol superposition method, given by Peter Rotkiewitc
 */
public class SuperimposeTest {

    private static Random rnd = Randomizer.getInstance();

    public SuperimposeTest() { }

    private static Vector3D generateRandomPoint() {
	return new Vector3D(rnd.nextDouble(), rnd.nextDouble(), rnd.nextDouble());
    }

    private static Vector3D[] generateRandomPoints(int n) {
	Vector3D[] nPoints = new Vector3D[n];
	for (int i = 0; i < n; ++i) {
	    nPoints[i] = generateRandomPoint();
	}
	return nPoints;
    }

    private static void printPoints(Vector3D[] points, PrintStream ps) {
	for (int i = 0; i < points.length; ++i) {
	    ps.println("" + (i + 1) + " : " + points[i]);
	}
    }


    @Test(groups={"new"})
    public void testSuperimpose() {
	String methodName = "testSuperimpose";
	System.out.println(TestTools.generateMethodHeader(methodName));
	// generate some coordinates:
	int n = 6;
	Vector3D[] points = generateRandomPoints(n);
	Vector3D[] points2 = new Vector3D[n];
	Vector3D trans = new Vector3D(1.0, 0.0, 0.0); // generateRandomPoint();

	for (int i = 0; i < n; ++i) {
	    points2[i] = new Vector3D(points[i]);
	    points2[i].add(trans); // shift
	}
	System.out.println("Original points:");
	printPoints(points, System.out);
	Matrix3D rot = new Matrix3D();
	Vector3D mc1 = new Vector3D();
	Vector3D mc2 = new Vector3D();
	double result = Superimpose.superimpose(points, points2, rot, mc1, mc2);
	System.out.println("translation vector for second set: " + trans);
	System.out.println("Result of superposition: " + result);
	System.out.println("" + rot + " : " + mc1 + " : " + mc2);
	Vector3D[] points3 = Superimpose.applyPoints2(points2, rot, mc1, mc2);
	System.out.println("Superposed points:");
	printPoints(points3, System.out);	
	System.out.println("RMS: " + Superimpose.rms(points, points3));
	assert (Superimpose.rms(points, points3) < 0.1);
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new"})
    public void testSuperimpose2() {
	String methodName = "testSuperimpose2";
	System.out.println(TestTools.generateMethodHeader(methodName));
	// generate some coordinates:
	int n = 6;
	Vector3D[] points = generateRandomPoints(n);
	Vector3D[] points2 = new Vector3D[n];
	Vector3D trans = new Vector3D(1.0, 0.0, 0.0); // generateRandomPoint();
	Vector3D rotDir = Vector3DTools.generateRandomDirection();
	double angle = Math.PI * 2.0 * rnd.nextDouble(); // random angle
	Matrix3D rotMatrix = Matrix3D.rotationMatrix(rotDir, angle);
	for (int i = 0; i < n; ++i) {
	    points2[i] = new Vector3D(points[i]);
	    points2[i] = rotMatrix.multiply(points2[i]); // rotate
	    points2[i].add(trans); // shift
	}
	System.out.println("Original points:");
	printPoints(points, System.out);
	Matrix3D rot = new Matrix3D();
	Vector3D mc1 = new Vector3D();
	Vector3D mc2 = new Vector3D();
	double result = Superimpose.superimpose(points, points2, rot, mc1, mc2);
	System.out.println("translation vector for second set: " + trans);
	System.out.println("rotation matrix for second set: " + rotMatrix);
	System.out.println("Result of superposition: " + result);
	System.out.println("" + rot + " : " + mc1 + " : " + mc2);
	Vector3D[] points3 = Superimpose.applyPoints2(points2, rot, mc1, mc2);
	System.out.println("Superposed points:");
	printPoints(points3, System.out);	
	System.out.println("RMS: " + Superimpose.rms(points, points3));
	assert (Superimpose.rms(points, points3) < 0.1);
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

}
