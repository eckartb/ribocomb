package tools3d;

import org.testng.annotations.Test;

import static tools3d.PackageConstants.NEWLINE;
import java.util.Vector;
import java.util.logging.Logger;

/**
 * Creates a Symmetry to be used by a PointSet2.
 *
 * @author Christine Viets
 */
public class Symmetry {

    public static final int C2_SYMMETRY = 2; /** */
    public static final int C3_SYMMETRY = 3; /** */
    public static final int C4_SYMMETRY = 4; /** */
    public static final int C5_SYMMETRY = 5; /** */
    public static final int C6_SYMMETRY = 6; /** */
    public static final int C7_SYMMETRY = 7; /** */
    public static final int C8_SYMMETRY = 8; /** */

    public static final String C2_SYMMETRY_STRING = "C2"; /** */
    public static final String C3_SYMMETRY_STRING = "C3"; /** */
    public static final String C4_SYMMETRY_STRING = "C4"; /** */
    public static final String C5_SYMMETRY_STRING = "C5"; /** */
    public static final String C6_SYMMETRY_STRING = "C6"; /** */
    public static final String C7_SYMMETRY_STRING = "C7"; /** */
    public static final String C8_SYMMETRY_STRING = "C8"; /** */

    private int type = 0; /** */

    private static Logger log = Logger.getLogger("NanoTiler_debug"); /** */

    private Point endPoint = new Point( "END", 0.0, 0.0, 0.0 ); /** */
    private Point startPoint = new Point( "START", 0.0, 0.0, 0.0 ); /** */

    private String name = ""; /** */

    private Vector<Junction> junctions = new Vector<Junction>(); /** */
    private Vector<SymmetryLink> links = new Vector<SymmetryLink>(); /** */

    /** Creates a new Symmetry. */
    public Symmetry() { }

    /** 
     * Creates a new Symmetry. 
     * 
     * @param name The String name of the Symmetry.
     */
    public Symmetry( String name ) { setName(name); }

    /** 
     * Creates a new Symmetry.
     * 
     * @param name The String name of the Symmetry.
     * @param type The int type of the Symmetry. Understands symmetry types 2 through 8.
     */
    public Symmetry( String name, int type ) {
	setName(name);
	setType(type);
    }

    /** 
     * Creates a new Symmetry.
     * 
     * @param name The String name of the Symmetry.
     * @param type The String type of the Symmetry. Understands symmetry types "C2", "C3", ... , "C8".
     */
    public Symmetry( String name, String type ) {
	setName(name);
	setType(type);
    }

    /**
     * Creates a new Symmetry.
     *
     * @param name The String name of the Symmetry.
     * @param type The int type of the Symmetry. Understands symmetry types 2 through 8.
     * @param startPoint The Point at the beginning of the line to rotate the junction around.
     * @param endPoint The Point at the end of the line to rotate the junction around.
     */
    public Symmetry( String name, int type, Point startPoint, Point endPoint ) {
	setName(name);
	setType(type);
	setStartPoint(startPoint);
	setEndPoint(endPoint);
    }

    /**
     * Creates a new Symmetry.
     *
     * @param name The String name of the Symmetry.
     * @param type The String type of the Symmetry. Understands symmetry types "C2, "C3", ... , "C8".
     * @param startPoint The Point at the beginning of the line to rotate the junction around.
     * @param endPoint The Point at the end of the line to rotate the junction around.
     */
    public Symmetry( String name, String type, Point startPoint, Point endPoint ) {
	setName(name);
	setType(type);
	setStartPoint(startPoint);
	setEndPoint(endPoint);
    }

    /**
     * Adds a junction to the junctions Vector.
     *
     * @param i The index in the Vector to add the junction.
     * @param junction The junction to be added.
     */
    public void addJunction( int i, Junction junction ) { junctions.add( i, junction ); }

    /**
     * Adds a junction to the junctions Vector.
     *
     * @param junction The junction to be added.
     */
    public void addJunction( Junction junction ) { junctions.add(junction); }

    /** */
    @Test public void testAddJunction() {
	Symmetry s = new Symmetry();
	Junction j = new Junction("testJunction");

	s.addJunction(0, j);
	assert(s.getJunction(0).equals(j));

	s = new Symmetry();
	s.addJunction(j);
	assert(s.getJunction(0).equals(j));
    }

    /**
     * Adds a link to the links Vector.
     *
     * @param i The index in the Vector to add the link.
     * @param link The SymmetryLink to be added to the Vector.
     */
    public void addLink( int i, SymmetryLink link ) { links.add( i, link ); }

    /**
     * Adds a link to the links Vector.
     *
     * @param link The SymmetryLink to be added to the Vector.
     */
    public void addLink(SymmetryLink link) { links.add(link); }

    /** */
    @Test public void testAddLink() {
	Symmetry s = new Symmetry();
	SymmetryLink l = new SymmetryLink();

	addLink( 0, l );
    }

    /** */
    public Object cloneDeep() {
	Symmetry newSym = new Symmetry();
	newSym.copyDeepThisCore(this);
	assert getNumJunctions() == newSym.getNumJunctions();
	return newSym;
    }

    /** */
    @Test public void testCloneDeep() {
    }

    /** */
    protected void copyDeepThisCore(Symmetry s) {	
	setName(s.getName());

	if( s.getType() != 0 )
	    setType(s.getType());

	setStartPoint(s.getStartPoint());
	setEndPoint(s.getEndPoint());
	Vector<Junction> j = new Vector<Junction>();
	for( int i = 0; i < s.getNumJunctions(); i++ ) {
	    j.add( (Junction)(s.getJunction(i).cloneDeep()) );
	}
	this.junctions = j;
	Vector<SymmetryLink> l = new Vector<SymmetryLink>();
	for( int i = 0; i < s.getNumLinks(); i++ ) {
	    l.add( (SymmetryLink)((SymmetryLink)(s.getLink(i))).cloneDeep() );
	}
	this.links = l;
    }

    /** */
    public Point getCenterPoint( Junction j ) {
	return j.getCenterPoint();
    }

    /** */
    @Test public void testGetCenterPoint() {
    }

    /** Returns the Point at the end of the line to rotate the junction around. */
    public Point getEndPoint() { return endPoint; }

    /** */
    @Test public void testGetEndPoint() {
    }

    /** Returns the x-coordinate of the end of the line to rotate the junction around. */
    public double getEndX() { return endPoint.getX(); }

    /** */
    @Test public void testGetEndX() {
    }

    /** Returns the y-coordinate of the end of the line to rotate the junction around. */
    public double getEndY() { return endPoint.getY(); }

    /** */
    @Test public void testGetEndY() {
    }

    /** Returns the z-coordinate of the end of the line to rotate the junction around. */
    public double getEndZ() { return endPoint.getZ(); }

    /** */
    @Test public void testGetEndZ() {
    }

    /**
     * Returns the name of the junction.
     *
     * @param i The index of the junction whose name is to be returned.
     */
    public Junction getJunction( int i ) { return (Junction)junctions.get(i); }

    /** */
    public Junction getJunction(String junctionName) {
	for (int i = 0; i < getNumJunctions(); i++) {
	    if (junctions.get(i).getName().equals(junctionName) ) {
		return (Junction)junctions.get(i);
	    }
	}
	return null;
    }

    /** */
    @Test public void testGetJunction() {
    }

    /** Returns the Vector of junctions. */
    public Vector<Junction> getJunctions() { return junctions; }

    /** */
    @Test public void testGetJunctions() {
    }

    /**
     * Returns the SymmetryLink.
     *
     * @param i The index of the SymmetryLink which is to be returned.
     */
    public SymmetryLink getLink( int i ) { return (SymmetryLink)links.get(i); }

    /** */
    @Test public void testGetLink() {
    }

    /** Returns the Vector of SymmetryLinks in the Symmetry. */
    public Vector<SymmetryLink> getLinks() { return links; }

    /** */
    @Test public void testGetLinks() {
    }

    /** Returns the String name of the Symmetry. */
    public String getName() { return name; }

    /** */
    @Test public void testGetName() {
    }

    /** Returns the number of junctions in the junctions Vector. */
    public int getNumJunctions() { return junctions.size(); }

    /** */
    @Test public void testGetNumJunctions() {
    }

    /** */
    public int getNumLinks() { return links.size(); }

    /** */
    @Test public void testGetNumLinks() {
    }

    /** Returns the Point at the beginning of the line to rotate the junction around. */
    public Point getStartPoint() { return startPoint; }

    /** */
    @Test public void testGetStartPoint() {
    }

    /** Returns the x-coordinate of the beginning of the line to rotate the junction around. */
    public double getStartX() {	return startPoint.getX(); }

    /** */
    @Test public void testGetStartX() {
    }

    /** Returns the y-coordinate of the beginning of the line to rotate the junction around. */
    public double getStartY() {	return startPoint.getY(); }

    /** */
    @Test public void testGetStartY() {
    }

    /** Returns the z-coordinate of the beginning of the line to rotate the junction around. */
    public double getStartZ() {	return startPoint.getZ(); }

    /** */
    @Test public void testGetStartZ() {
    }

    /** Returns the int type of the Symmetry. TODO: should type be final ints?? */
    public int getType() { return type; }

    /** */
    @Test public void testGetType() {
    }

    /**
     * Sets the end coordinates of the line to rotate the junction around.
     *
     * @param x The Double x-coordinate of the end of the line to rotate the junction around.
     * @param y The Double y-coordinate of the end of the line to rotate the junction around.
     * @param z The Double z-coordinate of the end of the line to rotate the junction around.
     */
    public void setEndPoint( double x, double y, double z ) {
	setEndX(x);
	setEndY(y);
	setEndZ(z);
    }

    /**
     * Sets the end coordinates of the line to rotate the junction around.
     *
     * @param xyz The CoordinateSet of the end of the line to rotate the junction around.
     */
    public void setEndPoint( Point point ) { endPoint = point; }

    /** */
    @Test public void testSetEndPoint() {
    }

    /**
     * Sets the x-coordinate of the end of the line to rotate the junction around.
     *
     * @param x The Double x-coordinate of the end of the line to rotate the junction around.
     */
    public void setEndX( double x ) { endPoint.setX(x); }

    /** */
    @Test public void testSetEndX() {
    }

    /**
     * Sets the y-coordinate of the end of the line to rotate the junction around.
     *
     * @param y The Double y-coordinate of the end of the line to rotate the junction around.
     */
    public void setEndY( double y ) { endPoint.setY(y); }

    /** */
    @Test public void testSetEndY() {
    }

    /**
     * Sets the z-coordinate of the end of the line to rotate the junction around.
     *
     * @param z The Double z-coordinate of the end of the line to rotate the junction around.
     */
    public void setEndZ( double z ) { endPoint.setZ(z); }

    /** */
    @Test public void testSetEndZ() {
    }

    /**
     * Sets the name of the Symmetry.
     *
     * @param name The String name of the Symmetry.
     */
    public void setName( String name ) { this.name = name; }

    /** */
    @Test public void testSetName() {
    }

    /** 
     * Sets the Coordinates of the beginning of the line to rotate the junction around.
     *
     * @param xyz The CoordinateSEt of the beginning of the line to rotate the junction around.
     */
    public void setStartPoint( Point point ) { startPoint = point; }

    /** */
    @Test public void testSetStartPoint() {
    }

    /**
     * Sets the x-coordinate of the beginning of the line to rotate the junction around.
     *
     * @param x The x-coordinate of the beginning of the line to rotate the junction around.
     */
    public void setStartX( double x ) {	startPoint.setX(x); }

    /** */
    @Test public void testSetStartX() {
    }

    /**
     * Sets the y-coordinate of the beginning of the line to rotate the junction around.
     *
     * @param y The y-coordinate of the beginning of the line to rotate the junction around.
     */
    public void setStartY( double y ) { startPoint.setY(y); }

    /** */
    @Test public void testSetStartY() {
    }

    /**
     * Sets the z-coordinate of the beginning of the line to rotate the junction around.
     *
     * @param z The z-coordinate of the beginning of the line to rotate the junction around.
     */
    public void setStartZ( double z ) {	startPoint.setZ(z); }

    /** */
    @Test public void testSetStartZ() {
    }

    /**
     * Sets the type of the Symmetry.
     *
     * @param type The int type of the Symmetry.
     */
    public void setType(int i) {
	switch (i) {
	case C2_SYMMETRY: {
	    type = C2_SYMMETRY;
	    break; }
	case C3_SYMMETRY: {
	    type = C3_SYMMETRY;
	    break; }
	case C4_SYMMETRY: {
	    type = C4_SYMMETRY;
	    break; }
	case C5_SYMMETRY: {
	    type = C5_SYMMETRY;
	    break; }
	case C6_SYMMETRY: {
	    type = C6_SYMMETRY;
	    break; }
	case C7_SYMMETRY: {
	    type = C7_SYMMETRY;
	    break; }
	case C8_SYMMETRY: {
	    type = C8_SYMMETRY;
	    break; }
	default: {
	    log.warning("Could not read type for int: " + i);
	    break; }
	}
    }

    /**
     * Sets the type of the Symmetry.
     *
     * @param type The String type of the Symmetry.
     */
    public void setType(String type) { 
	if( type.toUpperCase().trim().equals(C2_SYMMETRY_STRING) )
	    this.type = C2_SYMMETRY;
	else if( type.toUpperCase().trim().equals(C3_SYMMETRY_STRING) )
	    this.type = C3_SYMMETRY;
	else if( type.toUpperCase().trim().equals(C4_SYMMETRY_STRING) )
	    this.type = C4_SYMMETRY;
	else if( type.toUpperCase().trim().equals(C5_SYMMETRY_STRING) )
	    this.type = C5_SYMMETRY;
	else if( type.toUpperCase().trim().equals(C6_SYMMETRY_STRING) )
	    this.type = C6_SYMMETRY;
	else if( type.toUpperCase().trim().equals(C7_SYMMETRY_STRING) )
	    this.type = C7_SYMMETRY;
	else if( type.toUpperCase().trim().equals(C8_SYMMETRY_STRING) )
	    this.type = C8_SYMMETRY;
	else
	    log.warning("Error: Couldn't read type: " + type);
    }

    /** */
    @Test public void testSetType() {
    }
    
    /**
     * "Symmetry "name" of type "#";"
     * "Junctions:"
     * junction
     * "   "link
     */
    public String toString() {
	String s = new String( "Symmetry " + name + " of type " + type + ";" + NEWLINE + "Junctions:" + NEWLINE );
	for( int i = 0; i < getNumJunctions(); i++ ) {
	    s += ( getJunction(i) + NEWLINE );
	}
	s += ( "Links:" + NEWLINE );
	for( int i = 0; i < getNumLinks(); i++ ) {
	    s += ( "   " + getLink(i) + NEWLINE );
	}
	return s;
    }

    /** */
    @Test public void testToString() {
    }

}
