/**
 * Describes a vector which originates from a point.
 * Used by PointSet2.
 *
 * @author Christine Viets
 */
package tools3d;

public class Vector2 {

    private Point point = new Point(0.0, 0.0, 0.0);
    private Point vectorPoint = new Point(0.0, 0.0, 0.0);

    public Vector2() { }

    public Vector2( Point point, Point vectorPoint ) {
	setPoint(point);
	setVectorPoint(vectorPoint);
    }

    public Vector2( Point point, CoordinateSet xyz ) {
	setPoint(point);
	setVectorPoint(xyz);
    }

    public Vector2( Point point, double x, double y, double z ) {
	setPoint(point);
	setVectorPoint( new CoordinateSet( x, y, z ) );
    }

    public boolean equals( Vector2 v ) {
	return this.getPoint().similar(v.getPoint()) && this.getVectorPoint().similar(v.getVectorPoint());
    }

    public boolean containsPoint( Point p ) {
	if( p.equals(getPoint()) )
	    return true;
	else if( p.equals(getVectorPoint()) )
	    return true;
	else
	    return false;
    }

    public Object cloneDeep() {
	Vector2 v = new Vector2();
	v.copyDeepThisCore(this);
	return v;
    }

    protected void copyDeepThisCore(Vector2 v) {
	setPoint(v.getPoint());
	setVectorPoint(v.getVectorPoint());
    }

    public String toString() { 
	String s = new String("Vector from " + point + " to " + vectorPoint); 
	return s;
    }

    /** Returns the name of the point from which the vectors originate. */
    public Point getPoint() { return point; }

    /** Returns the Point which indicates the direction of the Vector2 */
    public Point getVectorPoint() { return vectorPoint; }

    /**
     * Sets the point from which the vectors originate.
     *
     * @param point The name of the point from which the vectors originate.
     */
    public void setPoint( Point point ) { this.point = point; }

    /**
     * Sets the Point which indicates the direction of the Vector2.
     *
     * @param vectorPoint The Point which indicates the direction of the Vector2.
     */
    public void setVectorPoint( Point vectorPoint ) { this.vectorPoint = vectorPoint; }

    /**
     * Sets the Point which indicates the direction of the Vector2.
     *
     * @param xyz The CoordinateSet of the Point which indicates the direction of the Vector2.
     */
    public void setVectorPoint( CoordinateSet xyz ) { vectorPoint = new Point(xyz); }

}
