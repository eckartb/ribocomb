package tools3d;

public class AABBBoundingVolume implements BoundingVolume {
	
	private Vector4D maxCorner, minCorner;
	
	private Vector4D[] vertexArray;
	
	// centers of each face of bounding box
	private Vector4D bottomXZ;
	private Vector4D topXZ;
	private Vector4D leftYZ;
	private Vector4D rightYZ;
	private Vector4D frontXY;
	private Vector4D backXY;
	
	private Vector4D[][] faces;
	
	private Vector4D center;
	
	double minX, minY, minZ, maxX, maxY, maxZ;
	
	
	
	public AABBBoundingVolume(Vector4D c1, Vector4D c2) {
		this.maxCorner = c1;
		this.minCorner = c2;
		
		center = new Vector4D((c1.getX() + c2.getX()) / 2.0,
				(c1.getY() + c2.getY()) / 2.0, 
				(c1.getZ() + c2.getZ()) / 2.0, 1.0);
		
		if(c1.getX() > c2.getX()) {
			maxX = c1.getX();
			minX = c2.getX();
		} else {
			maxX = c2.getX();
			minX = c1.getX();
		}
		
		if(c1.getY() > c2.getY()) {
			maxY = c1.getY();
			minY = c2.getY();
		} else {
			maxY = c2.getY();
			minY = c1.getY();
		}
		
		if(c1.getZ() > c2.getZ()) {
			maxZ = c1.getZ();
			minZ = c2.getZ();
		} else {
			maxZ = c2.getZ();
			minZ = c1.getZ();
		}
		
		this.maxCorner = new Vector4D(maxX, maxY, maxZ, 1.0);
		this.minCorner = new Vector4D(minX, minY, minZ, 1.0);
		
		vertexArray = new Vector4D[8];
		vertexArray[0] = new Vector4D(c1);
		vertexArray[1] = new Vector4D(c1.getX(), c1.getY(), c2.getZ());
		vertexArray[2] = new Vector4D(c2.getX(), c1.getY(), c2.getZ());
		vertexArray[3] = new Vector4D(c2.getX(), c1.getY(), c1.getZ());
		vertexArray[4] = new Vector4D(c2);
		vertexArray[5] = new Vector4D(c2.getX(), c2.getY(), c1.getZ());
		vertexArray[6] = new Vector4D(c1.getX(), c2.getY(), c1.getZ());
		vertexArray[7] = new Vector4D(c1.getX(), c2.getY(), c2.getZ());
		
		faces = new Vector4D[6][4];
		faces[0][0] = vertexArray[0];
		faces[0][1] = vertexArray[1];
		faces[0][2] = vertexArray[2];
		faces[0][3] = vertexArray[3];
		faces[1][0] = vertexArray[0];
		faces[1][1] = vertexArray[1];
		faces[1][2] = vertexArray[7];
		faces[1][3] = vertexArray[6];
		faces[2][0] = vertexArray[0];
		faces[2][1] = vertexArray[3];
		faces[2][2] = vertexArray[5];
		faces[2][3] = vertexArray[6];
		faces[3][0] = vertexArray[4];
		faces[3][1] = vertexArray[5];
		faces[3][2] = vertexArray[6];
		faces[3][3] = vertexArray[7];
		faces[4][0] = vertexArray[4];
		faces[4][1] = vertexArray[5];
		faces[4][2] = vertexArray[3];
		faces[4][3] = vertexArray[2];
		faces[5][0] = vertexArray[4];
		faces[5][1] = vertexArray[2];
		faces[5][2] = vertexArray[1];
		faces[5][3] = vertexArray[7];
		
		
		
		
		
		bottomXZ = new Vector4D((c1.getX() + c2.getX()) / 2, minY, (c1.getZ() + c2.getZ()) / 2);
		topXZ = new Vector4D((c1.getX() + c2.getX()) / 2, maxY, (c1.getZ() + c2.getZ()) / 2);
		leftYZ = new Vector4D(minX, (c1.getY() + c2.getY()) / 2, (c1.getZ() + c2.getZ()) / 2);
		rightYZ = new Vector4D(maxX, (c1.getY() + c2.getY()) / 2, (c1.getZ() + c2.getZ()) / 2);
		frontXY = new Vector4D((c1.getX() + c2.getX()) / 2, (c1.getY() + c2.getY()) / 2, maxZ);
		backXY = new Vector4D((c1.getX() + c2.getX()) / 2, (c1.getY() + c2.getY()) / 2, minZ);
	}
	
	public Vector4D[][] getFaces() {
		return faces;
	}
	
	public Vector4D[] getVertices() {
		return vertexArray;
	}
	
	public Vector4D getCorner1() {
		return new Vector4D(maxCorner);
	}
	
	public Vector4D getCorner2() {
		return new Vector4D(minCorner);
	}
	
	public Vector4D getCenter() {
		return center;
	}

	public boolean intersectBV(BoundingVolume v)
			throws IllegalArgumentException {
		
		if(!(v instanceof AABBBoundingVolume)) {
			throw new IllegalArgumentException("Parameter not AABBBoundingVolume");
		}
		
		AABBBoundingVolume bv = (AABBBoundingVolume) v;
		
		if(minX > bv.maxX || bv.minX > maxX) return false;
		if(minY > bv.maxY || bv.minY > maxY) return false;
		if(minZ > bv.maxZ || bv.minZ > maxZ) return false;
		
		return true;
	}

	/**
	 * Algorithm taken from Real Time Rendering, 2nd ed page 577
	 */
	public boolean intersectLine(Vector4D p1, Vector4D p2) {
		Vector4D c = new Vector4D((p1.getX() + p2.getX()) / 2.0,
				(p1.getY() + p2.getY()) / 2.0,
				(p1.getZ() + p2.getZ()) / 2.0, 1.0);
		Vector4D w = new Vector4D(c);
		w = w.minus(p1);
		
		Vector4D t = center.mul(-1);
		
		Vector4D x = new Vector4D(rightYZ);
		x = x.minus(center);
		Vector4D y = new Vector4D(topXZ);
		y = y.minus(center);
		Vector4D z = new Vector4D(frontXY);
		z = z.minus(center);
		
		translateVector(x, t);
		translateVector(y, t);
		translateVector(z, t);
		translateVector(c, t);
		
		double vx = Math.abs(w.getX());
		double vy = Math.abs(w.getY());
		double vz = Math.abs(w.getZ());
		double hx = x.getX();
		double hy = y.getY();
		double hz = z.getZ();
		double cx = c.getX();
		double cy = c.getY();
		double cz = c.getZ();
		
		if(Math.abs(cx) > vx + hx) return false;
		if(Math.abs(cy) > vy + hy) return false;
		if(Math.abs(cz) > vz + hz) return false;
		if(Math.abs(cy * w.getZ() - cz * w.getY()) > hy * vz + hz * vy) return false;
		if(Math.abs(cx * w.getZ() - cz * w.getX()) > hx * vz + hz * vx) return false;
		if(Math.abs(cx * w.getY() - cy * w.getX()) > hx * vy + hy * vz) return false;

		return true;
	}
	
	private Vector4D translateVector(Vector4D v, Vector4D t) {
		v.setX(v.getX() + t.getX());
		v.setY(v.getY() + t.getY());
		v.setZ(v.getZ() + t.getZ());
		
		return v;
	}

	public boolean intersectPlane(Vector4D u, Vector4D v, Vector4D offset) {
		Vector4D n = u.cross(v);
		double d = n.dot(offset) * -1;
		
		Vector4D v1 = null, v2 = null;
		double distance = Double.MAX_VALUE;
		for(int i = 0; i < 4; ++i) {
			Vector4D diagonal = new Vector4D(vertexArray[i]);
			diagonal = diagonal.minus(vertexArray[i + 4]);
			
			Vector4D proj = n.mul(diagonal.dot(n));
			Vector4D dist = new Vector4D(diagonal);
			dist = dist.minus(proj);
			double newDistance = dist.length();
			if(newDistance < distance) {
				distance = newDistance;
				v1 = vertexArray[i];
				v2 = vertexArray[i + 4];
			}
		}
		
		double x1 = n.dot(v1) + d;
		double x2 = n.dot(v2) + d;
		if(x1 * x2 < 0.0)
			return true;
		
		return false;
	}
	
	/**
	 * Woo's algorithm
	 * http://www.acm.org/pubs/tog/GraphicsGems/gems/RayBox.c
	 * Comment: not sure if implementation correct! Needs some test cases!
	 * suspicion because of different behaviour of
	 * viewer/rnadesign/Object3DSelector.findSelected and
	 * viewer/rnadesign/Object3DSelector.findSelected2 Eckart Bindewald Jan 2008
	 */
	public boolean intersectRay(Vector4D line, Vector4D offset) {
		boolean inside = true;
		
		double candidatePlaneX, candidatePlaneY, candidatePlaneZ;
		
		if(offset.getX() < minCorner.getX()) {
			candidatePlaneX = minCorner.getX();
			inside = false;
		} else if(offset.getX() > maxCorner.getX()) {
			candidatePlaneX = maxCorner.getX();
			inside = false;
		} else {
			candidatePlaneX = Double.NaN;
		}
		
		if(offset.getY() < minCorner.getY()) {
			candidatePlaneY = minCorner.getY();
			inside = false;
		} else if(offset.getY() > maxCorner.getY()){
			candidatePlaneY = maxCorner.getY();
			inside = false;
		} else {
			candidatePlaneY = Double.NaN;
		}
		
		if(offset.getZ() < minCorner.getZ()) {
			candidatePlaneZ = minCorner.getZ();
			inside = false;
		} else if(offset.getZ() > maxCorner.getZ()) {
			candidatePlaneZ = maxCorner.getZ();
			inside = false;
		} else {
			candidatePlaneZ = Double.NaN;
		}
		
		if(inside)
			return true;
		
		double tX, tY, tZ;
		
		if(!Double.isNaN(candidatePlaneX) && line.getX() != 0.0) {
			tX = (candidatePlaneX - offset.getX()) / line.getX();
		} else {
			tX = -1.0;
		}
		
		if(!Double.isNaN(candidatePlaneY) && line.getY() != 0.0) {
			tY = (candidatePlaneY - offset.getY()) / line.getY();
		} else {
			tY = -1.0;
		}
		
		if(!Double.isNaN(candidatePlaneZ) && line.getZ() != 0.0) {
			tZ = (candidatePlaneZ - offset.getZ()) / line.getZ();
		} else {
			tZ = -1.0;
		}
		
		double maxT = Math.max(tX, tY);
		maxT = Math.max(maxT, tZ);
		
		if(maxT < 0.0) {
			return false;
		}
		
		Vector4D point = new Vector4D(maxT * line.getX() + offset.getX(),
				maxT * line.getY() + offset.getY(),
				maxT * line.getZ() + offset.getZ(),
				1.0);
		
		if(maxT != tX && (point.getX() < minCorner.getX() || point.getX() > maxCorner.getX()))
			return false;
		if(maxT != tY && (point.getY() < minCorner.getY() || point.getY() > maxCorner.getY()))
			return false;
		if(maxT != tZ && (point.getZ() < minCorner.getZ() || point.getZ() > maxCorner.getZ()))
			return false;
		
		return true;
		
	}

	/**
	 * Woo's algorithm
	 *//*
	public boolean intersectRay(Vector4D line, Vector4D offset) {
		Vector4D xz = line.dot(CollisionDetectionTools.Y_AXIS.mul(-1)) < 0.0 ? bottomXZ : topXZ;
		Vector4D xy = line.dot(CollisionDetectionTools.Z_AXIS.mul(-1)) < 0.0 ? backXY : frontXY;
		Vector4D yz = line.dot(CollisionDetectionTools.X_AXIS.mul(-1)) < 0.0 ? leftYZ : rightYZ;
		Vector4D xzn = xz == bottomXZ ? CollisionDetectionTools.Y_AXIS.mul(-1) : CollisionDetectionTools.Y_AXIS;
		Vector4D xyn = xy == backXY ? CollisionDetectionTools.Z_AXIS.mul(-1) : CollisionDetectionTools.Z_AXIS;
		Vector4D yzn = yz == leftYZ ? CollisionDetectionTools.X_AXIS.mul(-1) : CollisionDetectionTools.X_AXIS;
		
		double tXZ = CollisionDetectionTools.linePlaneIntersectionValue(line, offset, xzn, xzn.dot(xz) * -1);
		double tXY = CollisionDetectionTools.linePlaneIntersectionValue(line, offset, xyn, xyn.dot(xy) * -1);
		double tYZ = CollisionDetectionTools.linePlaneIntersectionValue(line, offset, yzn, yzn.dot(yz) * -1);
		
		
		if(Double.isInfinite(tXZ) || Double.isInfinite(tXY) || Double.isInfinite(tYZ))
			return true;
		
		if(Double.isNaN(tXZ) && Double.isNaN(tXY) && Double.isNaN(tYZ))
			return false;
		
		if(Double.isNaN(tXZ))
			tXZ = -1 * Double.MIN_VALUE;
		if(Double.isNaN(tXY))
			tXY = -1 * Double.MIN_VALUE;
		if(Double.isNaN(tYZ))
			tYZ = -1 * Double.MIN_VALUE;
		
		double max = Math.max(tXZ, tXY);
		max = Math.max(max, tYZ);
		
		
		
		if(max == tXZ) {
			Vector4D point = line.mul(tXZ);
			point.add(offset);
			return pointInBounds(maxCorner.getX(), minCorner.getX(), maxCorner.getZ(), minCorner.getZ(), point.getX(), point.getZ());
			
		} else if(max == tXY) {
			Vector4D point = line.mul(tXY);
			point.add(offset);
			return pointInBounds(maxCorner.getX(), minCorner.getX(), maxCorner.getY(), minCorner.getY(), point.getX(), point.getY());
			
		}
		
		Vector4D point = line.mul(tYZ);
		point.add(offset);
		return pointInBounds(maxCorner.getY(), minCorner.getY(), maxCorner.getZ(), minCorner.getZ(), point.getY(), point.getZ());
		
	}*/
	
	private boolean pointInBounds(double uA1, double uA2, double vA1, double vA2, double pu, double pv) {
		return pu > Math.min(uA1, uA2) && pu < Math.max(uA1, uA2) &&
			pv > Math.min(vA1, vA2) && pv < Math.max(vA1, vA2);
	}


	public boolean containsPoint(Vector4D p) {
		return p.getX() > Math.min(maxCorner.getX(), minCorner.getX()) &&
			p.getX() < Math.max(maxCorner.getX(), minCorner.getX()) &&
			p.getY() > Math.min(maxCorner.getY(), minCorner.getY()) &&
			p.getY() < Math.max(maxCorner.getY(), minCorner.getY()) &&
			p.getZ() > Math.min(maxCorner.getZ(), minCorner.getZ()) &&
			p.getZ() < Math.max(maxCorner.getZ(), minCorner.getZ());
	}
	
	public String toString() {
		return "" + maxCorner + "\t" + minCorner;
	}
	
	

}
