package tools3d;

import java.util.Properties;

/** convinience class: represents point or edge or face in 3d space. */
public abstract class AbstractGeometryElement implements Drawable {

    protected Vector3D position;

    protected double zBufValue = 0.0;

    protected Appearance appearance;

    protected Properties properties; // additional application specific properties like "depth" in tree etc

    protected boolean selected = false;

    /** perform active transformation of all involved vectors */
    public void activeTransform(CoordinateSystem cs) {
	position = cs.activeTransform(position);
    }

    /** perform active transformation of all involved vectors */
    public void passiveTransform(CoordinateSystem cs) {
	position = cs.passiveTransform(position);
    }

    public double distance(Positionable3D other) {
	return (this.getPosition().minus(other.getPosition())).length();
    }

    public Appearance getAppearance() { return appearance; }

    public Vector3D getPosition() {
	return position;
    }

    public Properties getProperties() { return properties; }

    public boolean isSelected() { return selected; }

    /** returns true if position is well defined (not Not-a-number) */
    public boolean isValid() { return getPosition().isValid(); }

    public double getZBufValue() {
	return zBufValue;
    }

    public int compareTo(Positionable3D other) {
	if (other instanceof Positionable3D) {
	    Positionable3D p = (Positionable3D)other;
	    if (zBufValue < p.getZBufValue()) {
		return -1;
	    }
	    else if (zBufValue > p.getZBufValue()) {
		return 1;
	    }
	    return 0;
	}
	return -1; // bad comparison!
    }

    protected void copy(AbstractGeometryElement other) {
	position = other.position;
	zBufValue = other.zBufValue;
	appearance = other.appearance;
	properties = other.properties;
	selected = other.selected;
    }

    public void setAppearance(Appearance appearance) { this.appearance = appearance; }

    protected void setPosition(Vector3D position) {
	this.position = position;
    }

    public void setProperties(Properties properties) { this.properties = properties; }

    public void setSelected(boolean b) { this.selected = b; }

    public void setZBufValue(double v) {
	zBufValue = v;
    }

    public void translate(Vector3D shift) {
	this.position.add(shift);
    }

}
