package tools3d;

/** defines ambiente of environment: light sources, background etc */
public interface Ambiente {

    public void addLightSource(LightSource light);

    public Appearance getBackgroundAppearance();

    public double getDiffuseLightFlux();

    public int getNumberOfLightSources();

    public LightSource getLightSource(int n);

    public void removeLightSource(LightSource light);

    public void setBackgroundAppearance(Appearance appearance);
    
    public void setDiffuseLightFlux(double flux);

}
