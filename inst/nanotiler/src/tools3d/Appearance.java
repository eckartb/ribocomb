package tools3d;

import java.awt.Color;

public class Appearance {

    /** what fraction of light is reflected */
    double reflectionCoefficient = 0.7;

    /** how diffuse is reflected light? (0.0: mirror, 1.0: uniform diffusion)*/
    double diffusionCoefficient = 0.5;

    /** what color has object? */
    Color color = Color.black;

    public Appearance() { }

    public Appearance(Color color) { this.color = color; }

    public Color getColor() { return color; }

    public double getDiffusionCoefficient() {
	return this.diffusionCoefficient;
    }

    public double getReflectionCoefficient() {
	return this.reflectionCoefficient;
    }

    public void setColor(Color color) { this.color = color; }

    public void setDiffusionCoefficient(double d) {
	this.diffusionCoefficient = d;
    }

    public void setReflectionCoefficient(double d) {
	this.reflectionCoefficient = d;
    }

}
