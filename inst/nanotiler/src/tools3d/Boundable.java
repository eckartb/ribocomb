package tools3d;


public interface Boundable {
	public BoundingVolume getBoundingVolume();
	public Dimension3D getDimension();
}
