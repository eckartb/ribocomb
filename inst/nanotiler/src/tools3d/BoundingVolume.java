package tools3d;

public interface BoundingVolume {
	
	/**
	 * Intersection with another bounding volume
	 * @param v Should be an instance of the same class
	 * as this. Otherwise, it will throw an illegal argument
	 * exception
	 * @return
	 * @exception IllegalArgumentException Thrown if instanceof check
	 * fails
	 */
	public boolean intersectBV(BoundingVolume v) throws IllegalArgumentException;
	
	/**
	 * Determines if a ray intersects this bounding volume
	 * @param line
	 * @param offset
	 * @return true if the line intersects, false if otherwise
	 */
	public boolean intersectRay(Vector4D line, Vector4D offset);
	
	/**
	 * Determines if a line segment intersects this bounding volume
	 * @param p1 first endpoint of line segment
	 * @param p2 second endpoint of line segment
	 * @return
	 */
	public boolean intersectLine(Vector4D p1, Vector4D p2);
	
	/**
	 * Determines if a plane intersects this bounding volume
	 * @param u Vector that spans plane, non-colinear with v
	 * @param v Vector that spans plane, non-colinear with u
	 * @param offset offset of plane in 3D space
	 * @return true if the plane intersects, false if otherwise
	 */
	public boolean intersectPlane(Vector4D u, Vector4D v, Vector4D offset);
	
	/**
	 * Determines if this volume contains the point p
	 * @param p
	 * @return
	 */
	public boolean containsPoint(Vector4D p);
	
	/**
	 * Get the vertices making up the bounding volume
	 * @return Returns an array of vertices making up the bounding volume
	 */
	public Vector4D[] getVertices();
	
	/**
	 * Get the faces making up the bounding volume
	 * @return Each face is returned as an array of points
	 * composing the polygon that describes the face
	 */
	public Vector4D[][] getFaces();
	
	
}
