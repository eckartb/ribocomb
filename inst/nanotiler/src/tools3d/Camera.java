package tools3d;

public interface Camera extends SpaceToPlaneProjection {

    public void copy(Camera other);

    /** returns orientation of camera */
    public Matrix3D getOrientation();

    /** returns position of camera */
    public Vector3D getPosition();

    /** returns view direction */
    public Vector3D getViewDirection();

    /** returns zoom factor */
    public double getZoom();

    /** sets pixel coordinates corresponding to positions projected to (0,0) */
    public void setOrigin2D(int x, int y);

    /** sets position of camera */
    public void setPosition(Vector3D position);

    /** sets orientation of camera */
    public void setOrientation(Matrix3D m);

    /** sets zoom factor */
    public void setZoom(double f);

    /** translates the camera relative to the current orientation */
    public void translate(double x, double y);
}
