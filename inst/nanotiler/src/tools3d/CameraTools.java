package tools3d;

import java.util.logging.Logger;

public class CameraTools {

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    private static final Vector3D zFixed = new Vector3D(0.0, 0.0, 1.0);

    /** angles theta, phi correspond to longitude and lattidue on globe. North pole has theta = 0, equator theta = PI/2 */
    public static void updateCameraFromAngles(Camera camera, double theta, double phi, double psi, double dist, Vector3D interest) {
	// compute position of camera:
	double x = Math.cos(phi)*Math.sin(theta) * dist + interest.getX();
	double y = Math.sin(phi)*Math.sin(theta) * dist + interest.getY();
	double z = Math.cos(theta) * dist + interest.getZ();
	Vector3D pos = camera.getPosition();
	Matrix3D orient = camera.getOrientation();
	pos.set(x, y, z);
	Vector3D zVec = interest.minus(pos); // new direction of camera is z-vector is vector from camera position to point of interest
	if (zVec.length() == 0.0) {
	    zVec.copy(zFixed);
	}
	else {
	    zVec.normalize();
	}
	if (zVec.length() > 1.01) {
	    log.warning("Strange zvec found: " + zVec.toString());
	}
	Vector3D xVec = zVec.cross(zFixed);
	if (xVec.length() == 0.0) {
	    xVec.set(1.0, 0.0, 0.0);
	}
	else {
	    xVec.normalize();
	}
	Vector3D yVec = zVec.cross(xVec);
	if (yVec.length() == 0.0) {
	    yVec.set(0.0, 1.0, 0.0);
	}
	else {
	    yVec.normalize();
	}
	orient.copy(xVec, yVec, zVec);
	camera.setPosition(pos);
	camera.setOrientation(orient);
    }

}
