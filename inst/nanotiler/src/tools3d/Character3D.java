package tools3d;

import generaltools.Letter;

/** represents point that contains character information */
public class Character3D extends Point3D implements Letter {

    char character;

    public Character3D(char c) {
	super();
	this.character = c;
    }

    public Character3D() {
	super();
	this.character = 'N';
    }

    public char getCharacter() { return character; }

    public void setCharacter(char c) { this.character = c; }

}
