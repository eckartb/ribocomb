package tools3d;

import generaltools.Letter;

/** represents 3D shape of character */
public class CharacterShape extends SimpleShape3D implements Letter {

    char character;

    public CharacterShape(char c) {
	super();
	this.character = c;
    }

    public CharacterShape() {
	super();
	this.character = 'N';
    }

    public char getCharacter() { return character; }

    public void setCharacter(char c) { character = c; }

    /** creates dummy set of points, edges, faces */
    public Geometry createGeometry(double angleDelta) {
	if (geometry == null) {
	    updateGeometry();
	}
	return geometry;
    }

    // TODO : implement more efficiently!
    protected void updateGeometry() {
	Vector3D pos = getPosition();
	Point3D[] points = new Point3D[1];
	points[0] = new Character3D(getCharacter());
	points[0].setPosition(getPosition());
	Edge3D[] edges = new Edge3D[0];
	Face3D[] faces = new Face3D[0];
	geometry = new StandardGeometry(points, edges, faces, getProperties());
    }
    

}
