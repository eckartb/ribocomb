package tools3d;

public class CollisionDetectionTools {

	public static final Vector4D X_AXIS = new Vector4D(1.0, 0.0, 0.0, 0.0);
	public static final Vector4D Y_AXIS = new Vector4D(0.0, 1.0, 0.0, 0.0);
	public static final Vector4D Z_AXIS = new Vector4D(0.0, 0.0, 1.0, 0.0);
	
	
	/**
	 * Test if a line intersects a plane
	 * @param line Vector parallel to line
	 * @param lineOffset Offset of line in 3d space
	 * @param planeU planeU and planeV vectors span plane
	 * @param planeV planeU and planeV vectors span plane
	 * @param planePoint point on plane 
	 * @return Returns a point of intersection or null if no intersection
	 */
	public static Vector4D linePlaneIntersection(Vector4D line, Vector4D lineOffset,
			Vector4D planeU, Vector4D planeV, Vector4D planePoint) {
		
		Vector4D normal = planeU.cross(planeV);
		normal.normalize();
		
		double d = -1 * normal.dot(planePoint);
		
		
		return linePlaneIntersection(line, lineOffset, normal, d);
	}
	
	/**
	 * Intersection between plane and line
	 * @param v Vector describing line
	 * @param o Line offset
	 * @param n Normal of plane
	 * @param d Plane constant
	 * @return returns point on plane or null if no intersection
	 */
	public static Vector4D linePlaneIntersection(Vector4D v, Vector4D o,
			Vector4D n, double d) {
		
		double bottom = n.dot(v);
		double top = -1 * (d + n.dot(o));
		
		if(Math.abs(bottom) < 0.000000001) {
			if(Math.abs(top) < 0.0000000001) {
				return v;
			}
			else
				return null;
		}
		
		double t = top / bottom;
		
		Vector4D ret = v.mul(t);
		ret.add(o);
		
		return ret;
	}
	
	/**
	 * Same as linePlaneIntersection except it returns the scaling factor
	 * used to calculate the point in space
	 * @param v
	 * @param o
	 * @param n
	 * @param d
	 * @return Returns the scaling factor for v or Double.POSITIVE_INFINITY if
	 * the line is on the plane or Double.NaN if the line doesn't intersect the
	 * plane.
	 */
	public static double linePlaneIntersectionValue(Vector4D v, Vector4D o,
			Vector4D n, double d) {
		

		
		double bottom = n.dot(v);
		double top = -1 * (d + n.dot(o));
		
		if(Math.abs(bottom) < 0.0000000001) {
			if(Math.abs(top) < 0.0000000001) {
				return Double.POSITIVE_INFINITY;
			}
			else
				return Double.NaN;
		}
		
		
		
		double t = top / bottom;
	
		return t;
	}
	
	public static Vector4D intersecton3Planes(Vector4D n1, Vector4D p1,
											  Vector4D n2, Vector4D p2,
											  Vector4D n3, Vector4D p3) {
		
		Matrix3D matrix = new Matrix3D(n1.getX(), n1.getY(), n1.getZ(),
									   n2.getX(), n2.getY(), n2.getZ(),
									   n3.getX(), n3.getY(), n3.getZ());
		
		double determinant = matrix.determinant();
		if(determinant == 0)
			return null;
		
		Vector4D c1 = n2.cross(n3).mul(p1.dot(n1));
		Vector4D c2 = n3.cross(n1).mul(p2.dot(n2));
		Vector4D c3 = n1.cross(n2).mul(p3.dot(n3));
		Vector4D p = c1.plus(c2).plus(c3);
		p = p.mul(1 / determinant);
		
		return p;
		
		
	}
}
