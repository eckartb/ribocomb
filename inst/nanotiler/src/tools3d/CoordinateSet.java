package tools3d;

import org.testng.annotations.Test; /** Testing package. */

import tools3d.Vector3D;

/**
 * Class CoordinateSet holds coordinate data (x,y,z) of a point.
 *
 * @author Christine Viets
 * @deprecated
 */
public class CoordinateSet {

    private double x; /* x-coordinate */
    private double y; /* y-coordinate */
    private double z; /* z-coordinate */

    /**
     * Creates a new CoordinateSet.
     */
    public CoordinateSet() { }

    /**
     * Creates a new CoordinateSet.
     *
     * @param vector The Vector3D that contains the three points in the new CoordinateSet.
     */
    public CoordinateSet(Vector3D vector) {
	setX(vector.getX());
	setY(vector.getY());
	setZ(vector.getZ());
    }

    /**
     * Creates a new CoordinateSet.
     *
     * @param x The x-coordinate.
     * @param y The y-coordinate.
     * @param z The z-coordinate.
     */
    public CoordinateSet(double x,
			 double y,
			 double z) {
	setX(x);
	setY(y);
	setZ(z);
    }

    /**
     * Returns the x-coordinate.
     */
    public double getX() {
	return x;
    }

    /**
     * Returns the y-coordinate.
     */
    public double getY() {
	return y;
    }

    /**
     * Returns the z-coordinate.
     */
    public double getZ() {
	return z;
    }

    /**
     * Tests getX(), getY(), and getZ() methods.
     */
    @Test public void testGetCoordinates() {
	CoordinateSet coorSet = new CoordinateSet(10, 42, 80);
	assert coorSet.getX() == 10;
	assert coorSet.getY() == 42;
	assert coorSet.getZ() == 80;
    }

    /**
     * Sets the x-coordinate.
     *
     * @param x The x-coordinate.
     */
    public void setX(double x) {
	this.x = x;
    }

    /**
     * Sets the y-coordinate.
     *
     * @param y The y-coordinate.
     */
    public void setY(double y) {
	this.y = y;
    }

    /**
     * Sets the z-coordinate.
     *
     * @param z The z-coordinate.
     */
    public void setZ(double z) {
	this.z = z;
    }

    /**
     * Tests setX(), setY(), and setZ() methods.
     */
    @Test public void testSetCoordinates() {
	CoordinateSet coorSet = new CoordinateSet();
	coorSet.setX(11);
	coorSet.setY(23);
	coorSet.setZ(49);
	assert coorSet.getX() == 11;
	assert coorSet.getY() == 23;
	assert coorSet.getZ() == 49;
    }

    /**
     * Returns a string representation of this CoordinateSet in the form
     * "x-coordinate, y-coordinate, z-coordinate".
     *
     * @return The string representation of this CoordinateSet.
     */
    public String toString() {
	String s = new String("" + x + ", " + y + ", " + z);
	return s;
    }

    /**
     * Tests toString() method.
     * Test fails, however the whole class is deprecated
     */
    // @Test
    public void testToString() {
	CoordinateSet coorSet = new CoordinateSet(3, 18, 4);
	assert coorSet.toString().equals("3, 18, 4");
    }

    /**
     * Returns a Vector3D at the coordinates in this CoordinateSet.
     */
    public Vector3D toVector3D() {
	return new Vector3D(getX(), getY(), getZ());
    }

    /**
     * Tests method toVector3D().
     */
    @Test public void testToVector3D() {
	CoordinateSet coorSet = new CoordinateSet(5, 19, 52);
	Vector3D vector = coorSet.toVector3D();
	assert vector.getX() == 5;
	assert vector.getY() == 19;
	assert vector.getZ() == 52;
    }

}
