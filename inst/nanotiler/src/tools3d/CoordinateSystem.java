package tools3d;

public interface CoordinateSystem extends Orientable {

    /** returns true, if x, y and z form a righ-handed coordinates system, all angles are 90 degree, all lengths are one. */
    boolean isCartesian();

    boolean isValid();

    /** "moves" point p like coordinate system was moved */
    Vector3D activeTransform(Vector3D p);

    /** "moves" point p like coordinate system was moved */
    Vector3D activeTransform2(Vector3D p);

    /** "moves" point p like coordinate system was moved */
    Vector3D activeTransform3(Vector3D p);

    /** copies information from other coordinate system to this coordinate system */
    void copy(CoordinateSystem other);

    /** returns 4D matrix describing rotation and translation */
    Matrix4D generateMatrix4D();

    Vector3D getX();

    Vector3D getY();

    Vector3D getZ();

    /** returns if close to unit transform */
    boolean isUnitTransform(double error);

    /** measures coordinates of point p in this coordinate system */
    Vector3D passiveTransform(Vector3D p);

    /** measures coordinates of point p in this coordinate system */
    Vector3D passiveTransform2(Vector3D p);

    /** measures coordinates of point p in this coordinate system using 4D matrix formalism */
    Vector3D passiveTransform3(Vector3D p);

    /** returns array representation */
    double[] toArray();

    /** generates new coordinate system c, such that c.activeTransform(this) is identity transformation */
    CoordinateSystem inverse();

}
