package tools3d;

/** implementation of sphere as Shape3D
 * @TODO NOT YET IMPLEMENTED !
 */
public class Cube extends SimpleShape3D {

    Point3D[] points;
    Vector3D[] offPoints;
    Edge3D[] edges; // = new Edge3D[0]; // TODO
    Face3D[] faces = new Face3D[0];

    /** defines quader with 3 lengths and position */
    public Cube(Vector3D position,  Vector3D dimensions) {
	setPosition(position);
	setDimensions(dimensions);
	double r = dimensions.getX();
	if (dimensions.getY() > r) {
	    r = dimensions.getY();
	}
	if (dimensions.getZ() > r) {
	    r = dimensions.getZ();
	}
	setBoundingRadius(r);
	points =  new Point3D[8];
	for (int i = 0; i < points.length; ++i) {
	    points[i] = new Point3D();
	}
	offPoints =  new Vector3D[8];
	edges = new Edge3D[12];
	initOffPoints();
    }

    /** defines cube with side length and position
     * TODO :slow
     */
    public Cube(Vector3D position, double width) {
	this(position, new Vector3D(width, width, width));
    }

    public String getShapeName() { return "Cube"; }

    /** dimensions have to be set already! */
    private void initOffPoints() {
	Vector3D dim = getDimensions();
	Vector3D dim2 = dim.mul(0.5); // half-lengths
	offPoints[0] = new Vector3D(dim2.getX(), dim2.getY(), dim2.getZ()); 
	offPoints[1] = new Vector3D(dim2.getX(), dim2.getY(), -dim2.getZ()); 
	offPoints[2] = new Vector3D(dim2.getX(), -dim2.getY(), dim2.getZ()); 
	offPoints[3] = new Vector3D(dim2.getX(), -dim2.getY(), -dim2.getZ());
	offPoints[4] = new Vector3D(-dim2.getX(), dim2.getY(), dim2.getZ());
	offPoints[5] = new Vector3D(-dim2.getX(), dim2.getY(), -dim2.getZ());
	offPoints[6] = new Vector3D(-dim2.getX(), -dim2.getY(), dim2.getZ());
	offPoints[7] = new Vector3D(-dim2.getX(), -dim2.getY(), -dim2.getZ());
    }

    /** creates dummy set of points, edges, faces */
    public Geometry createGeometry(double angleDelta) {
	if (geometry == null) {
	    updateGeometry();
	}
	return geometry;
    }

    protected void updateGeometry() {
	Vector3D pos = getPosition();
	for (int i = 0; i < points.length; ++i) {
	    Vector3D rotAxis = getRotationAxis();
	    double rotAngle = getRotationAngle();
	    Vector3D rotPoint = Matrix3DTools.rotate(offPoints[i], rotAxis, rotAngle);
	    points[i].setPosition(pos.plus(rotPoint));
	}
	int pc = 0;
	edges[pc++] = new Edge3D(points[0].getPosition(), points[1].getPosition());
	edges[pc++] = new Edge3D(points[0].getPosition(), points[2].getPosition());
	edges[pc++] = new Edge3D(points[0].getPosition(), points[4].getPosition());
	edges[pc++] = new Edge3D(points[1].getPosition(), points[3].getPosition());
	edges[pc++] = new Edge3D(points[1].getPosition(), points[5].getPosition());
	edges[pc++] = new Edge3D(points[2].getPosition(), points[3].getPosition());
	edges[pc++] = new Edge3D(points[2].getPosition(), points[6].getPosition());
	edges[pc++] = new Edge3D(points[3].getPosition(), points[7].getPosition());
	edges[pc++] = new Edge3D(points[4].getPosition(), points[5].getPosition());
	edges[pc++] = new Edge3D(points[4].getPosition(), points[6].getPosition());
	edges[pc++] = new Edge3D(points[5].getPosition(), points[7].getPosition());
	edges[pc++] = new Edge3D(points[6].getPosition(), points[7].getPosition());

	geometry = new StandardGeometry(points, edges, faces, getProperties());
	geometry.setSelected(isSelected());
    }

}
