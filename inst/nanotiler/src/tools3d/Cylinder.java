package tools3d;

/** implementation of cylinder as Shape3D
 * @TODO NOT YET IMPLEMENTED !
 */
public class Cylinder extends SimpleShape3D {

    Point3D[] pointsLow;
    Point3D[] pointsHigh;
    Point3D[] points;
    Vector3D[] offPointsLow;
    Vector3D[] offPointsHigh;
    Edge3D[] edges = new Edge3D[0]; // TODO
    Face3D[] faces = new Face3D[0];
    double radius;

    /** defines sphere with radius and position */
    public Cylinder(Vector3D position, double height, double radius) {
	setPosition(position);
	Vector3D dimensions = new Vector3D(radius, radius, height);
	setDimensions(dimensions);
	setBoundingRadius(dimensions.max());
	this.radius = radius;
    }

    public String getShapeName() { return "Cylinder"; }

    /** dimensions have to be set already! */
    private void initOffPoints(double height, double radius, int nn) {
	double h2 = 0.5 * height;
	double angleStep = 2.0 * Math.PI / nn;
	if ((pointsLow == null) || (pointsLow.length != nn)) {
	    pointsLow =  new Point3D[nn];
	    pointsHigh =  new Point3D[nn];
	    for (int i = 0; i < nn; ++i) {
		pointsLow[i] = new Point3D();
		pointsHigh[i] = new Point3D();
	    }
	    offPointsLow =  new Vector3D[nn];
	    offPointsHigh =  new Vector3D[nn];
	    for (int i = 0; i < nn; ++i) {
		offPointsLow[i] = new Vector3D();
		offPointsHigh[i] = new Vector3D();
	    }
	}
	for (int i = 0; i < nn; ++i) {
	    double ang = angleStep * i;
	    double x = radius * Math.cos(ang);
	    double y = radius * Math.sin(ang);
	    offPointsLow[i].set(x,y,-h2);
	    offPointsHigh[i].set(x,y,h2);
	}
    }

    /** creates dummy set of points, edges, faces */
    public Geometry createGeometry(double angleDelta) {
	if (!isGeometryUpToDate()) {
	    updateGeometry(angleDelta);
	}
	return geometry;
    }

    protected void updateGeometry(double angleDelta) {
	int nn = GeometryTools.computeCircleDevisions(angleDelta);
	initOffPoints(getDimensions().getZ(), getDimensions().getX(), nn);
	Vector3D pos = getPosition();
	Vector3D rotAxis = getRotationAxis();
	double rotAngle = getRotationAngle();
	for (int i = 0; i < pointsLow.length; ++i) {
	    Vector3D rotPointLow = Matrix3DTools.rotate(offPointsLow[i], rotAxis, rotAngle);
	    Vector3D rotPointHigh = Matrix3DTools.rotate(offPointsHigh[i], rotAxis, rotAngle);
	    pointsLow[i].setPosition(pos.plus(rotPointLow));
	    pointsHigh[i].setPosition(pos.plus(rotPointHigh));
	}
	updatePoints();
	updateEdges();
	geometry = new StandardGeometry(points, edges, faces, getProperties());
	geometry.setSelected(isSelected());
	setGeometryUpToDate(true);
    }

    private void updatePoints() {
	if ((points == null) || (points.length != (pointsLow.length+pointsHigh.length))) {
	    int nt = pointsLow.length + pointsHigh.length;
	    points = new Point3D[nt];
	}
	int pc = 0;
	for (int i = 0; i < pointsLow.length; ++i) {
	    points[pc++] = pointsLow[i];
	}
	for (int i = 0; i < pointsHigh.length; ++i) {
	    points[pc++] = pointsHigh[i];
	}
    }

    private void updateEdges() {
	if ((edges == null) || (edges.length != 3 * pointsHigh.length)) {
	    int nEdges = 3 * pointsHigh.length;
	    edges = new Edge3D[nEdges];
	}
	int pc = 0;
	for (int i = 1; i < pointsLow.length; ++i) {
	    edges[pc++] = new Edge3D(pointsLow[i-1].getPosition(), pointsLow[i].getPosition());
	}
	// connect in circle:
	edges[pc++] = new Edge3D(pointsLow[0].getPosition(), pointsLow[pointsLow.length-1].getPosition());
	for (int i = 1; i < pointsHigh.length; ++i) {
	    edges[pc++] = new Edge3D(pointsHigh[i-1].getPosition(), pointsHigh[i].getPosition());
	}
	// connect in circle:
	edges[pc++] = new Edge3D(pointsHigh[0].getPosition(), pointsHigh[pointsHigh.length-1].getPosition());
	for (int i = 0; i < pointsHigh.length; ++i) {
	    edges[pc++] = new Edge3D(pointsLow[i].getPosition(), pointsHigh[i].getPosition());
	}
    }


}
