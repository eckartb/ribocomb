package tools3d;

import java.util.Properties;

public interface Drawable extends Positionable3D {

    public Appearance getAppearance();

    /** possible container for general properties. It could store things like the "depth" in an object tree etc. */
    public Properties getProperties();

    /** returns true if object is "selected" in GUI */
    public boolean isSelected();

    public void setAppearance(Appearance appearance);

    public void setProperties(Properties p);

    /** sets selected status */
    public void setSelected(boolean b);

}
