package tools3d;

/** represents point in 3d space. most primitive class along with Edge3D, Face3D */
public class Edge3D extends AbstractGeometryElement {

    private Vector3D first;
    private Vector3D second;

    public Edge3D(Vector3D first, Vector3D second) {
	this.first = new Vector3D(first);
	this.second = new Vector3D(second);
	Vector3D pos  = (first.plus(second));
	pos.scale(0.5); // average position
	setPosition(pos);
    }

    public Edge3D(Point3D first, Point3D second) {
	this.first = new Vector3D(first.getPosition());
	this.second = new Vector3D(second.getPosition());
	Vector3D pos  = (first.getPosition().plus(second.getPosition()));
	pos.scale(0.5); // average position
	setPosition(pos);
    }

    private Edge3D(Edge3D other) {
	this(other.getFirst(), other.getSecond());
    }

    /** perform active transformation of all involved vectors */
    public void activeTransform(CoordinateSystem cs) {
	super.activeTransform(cs);
	first = cs.activeTransform(first);
	second = cs.activeTransform(second);
    }

    /** clones object; Careful: not really deep clone implemented. */
    public Object cloneDeep() {
	Edge3D edge = new Edge3D(this);
	edge.superCopy(this);
	return edge;
    }

    /** calls copy method from super-class */
    private void superCopy(Edge3D other) {
	super.copy(other);
    }

    public Vector3D getFirst() {
	return first;
    }

    public Vector3D getSecond() {
	return second;
    }

    /** returns true if at least one vertex is in common */
    public boolean isAdjacent(Edge3D other) {
	if (first.equals(other.getFirst()) || first.equals(other.getSecond())
	    || second.equals(other.getFirst()) || second.equals(other.getSecond())) {
	    return true;
	}
	return false;
    }

    /** returns true if at least one vertex is in common */
    public Vector3D findAdjacent(Edge3D other) {
	if (first.equals(other.getFirst())) {
	    return first;
	}
	if (first.equals(other.getSecond())) {
	    return first;
	}
	if (second.equals(other.getFirst())) {
	    return first;
	}
	if (second.equals(other.getSecond())) {
	    return first;
	}
	    
	return null;
    }

    public void translate(Vector3D shift) {
	position.add(shift);
	this.first.add(shift);
	this.second.add(shift);
    }


}
