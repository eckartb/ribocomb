package tools3d;

/** represents simple face in 3d space. most primitive class along with Edge3D, Point3D */
public class Face3D extends AbstractGeometryElement {

    private Vector3D normal;

    private Vector3D[] points;

    /** defines face with set of points. Compute face normal and mean position */
    public Face3D(Vector3D[] points) {
	assert points != null;
	this.points = points;
	updatePositionAndNormal();
    }

    /** perform active transformation of all involved vectors */
    public void activeTransform(CoordinateSystem cs) {
	super.activeTransform(cs);
	for (int i = 0; i < points.length; ++i) {
	    points[i] = cs.activeTransform(points[i]);
	}
    }

    private void updatePositionAndNormal() {
	Vector3D pos = new Vector3D(0.0, 0.0, 0.0);
	for (int i = 0; i < points.length; ++i) {
	    pos.add(points[i]);
	}
	pos.scale(1.0 / (double)points.length); // average position
	setPosition(pos);
	if (points.length >= 3) {
	    // compute normal of plane:
	    normal = (points[1].minus(points[0])).cross(points[2].minus(points[0]));
	    if (normal.length() > 0) {
		normal.normalize();
	    }
	}
    }


    /** defines face with set of points. Compute face normal and mean position */
    public Face3D(Face3D other) {
	this(other.getPoints());
    }

    public Object cloneDeep() {
	Vector3D[] newPoints = new Vector3D[this.points.length];
	for (int i = 0; i < newPoints.length; ++i) {
	    newPoints[i] = new Vector3D(points[i]);
	}
	return new Face3D(newPoints);
    }

    /** returns n'th point */
    public Vector3D getPoint(int n) {
	return points[n];
    }

    /** returns n'th point */
    public Vector3D[] getPoints() {
	return points;
    }

    /** returns normal vector of plane */
    public Vector3D getNormal() {
	return normal;
    }

    public int size() { return points.length; }

    public void translate(Vector3D v) { 
	for (int i = 0; i < points.length; ++i) {
	    points[i].add(v);
	}
	position.add(v);
    }
}
