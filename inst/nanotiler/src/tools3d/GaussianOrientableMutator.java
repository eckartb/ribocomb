package tools3d;

import java.util.Random;
import generaltools.Randomizer;
import java.util.logging.*;

/** randomly translates and rotates object */
public class GaussianOrientableMutator implements OrientableModifier {

    private static Random rnd = Randomizer.getInstance();
    private static Logger log = Logger.getLogger(PackageConstants.LOGFILE_DEFAULT);

    private double angleStep;
    private Vector3D fixedAxis = null;
    private double translationStep;
    
    public GaussianOrientableMutator(double translationStep, double angleStep) {
	this.translationStep = translationStep;
	this.angleStep = angleStep;
    }

    public boolean isValid() {
	return ! ((angleStep <= 0) || (translationStep < 0.0) || Double.isNaN(angleStep) || Double.isInfinite(angleStep)
		  || Double.isNaN(translationStep) || Double.isInfinite(translationStep));
    }

    /** copies member variables to this object */
    public void copy(GaussianOrientableMutator other) {
	this.angleStep = other.angleStep;
	this.fixedAxis = other.fixedAxis;
	this.translationStep = other.translationStep;
    }

    public void mutate(Orientable obj) {
	rotate(obj);
	translate(obj);
    }
    
    public void rotate(Orientable obj) {
	assert isValid();
	assert obj.isValid();
	assert angleStep > 0.0;
	Vector3D axis = fixedAxis;
	if (fixedAxis == null) {
	    axis = Vector3DTools.generateRandomDirection();
	}
	assert axis.isValid();
	assert axis.length() > 0.0;
	double angle = angleStep * rnd.nextGaussian();
	assert !Double.isNaN(angle);
	assert !Double.isInfinite(angle);
	Vector3D pos1 = obj.getPosition();
	obj.rotate(axis, angle);
	Vector3D pos2 = obj.getPosition();
	assert(pos1.distance(pos2) < 0.01); // object should not be translated
	if (!obj.isValid()) {
	    log.severe("Could not rotate object with axis: " + axis + " and angle " + angle + " " + obj.getPosition());
	}
	assert obj.isValid();
    }
    
    public double getAngleStep() { return this.angleStep; }

    public double getTranslationStep() { return this.translationStep; }

    public void setAngleStep(double d) { this.angleStep = d; assert isValid(); }

    public void setFixedAxis(Vector3D axis) { assert(axis != null); this.fixedAxis = new Vector3D(axis); }

    public void setTranslationStep(double d) { this.translationStep = d; assert isValid(); }

    public void translate(Orientable obj) {
	Vector3D direction = Vector3DTools.generateRandomDirection();
	double translationScale = translationStep * rnd.nextGaussian();
	obj.translate(direction.mul(translationScale));
	assert obj.isValid();
    }

    public void scaleAngleStep(double scale) {
	this.angleStep *= scale;
	assert isValid();
    }

    public void scaleTranslationStep(double scale) {
	this.translationStep *= scale;
	assert isValid();
    }

    public String toString() {
	return "" + translationStep + " " + (PackageConstants.RAD2DEG * angleStep);
    }

}
