package tools3d;

import java.util.Properties;

public interface Geometry {

    public void addToZBuffer(ZBuffer zBuf);

    public Edge3D getEdge(int n);

    public int getNumberEdges();

    public int getNumberPoints();

    public int getNumberFaces();
    
    public Face3D getFace(int n);    

    public Point3D getPoint(int n);    

    /** gets general properties */
    public Properties getProperties();

    public boolean isSelected(); 

    public void merge(Geometry other);
    
    /** sets general properties */
    public void setProperties(Properties properties);

    /** sets selected status */
    public void setSelected(boolean b);

    /** returns total number of elememts */
    public int size();

}
