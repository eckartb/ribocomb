package tools3d;

import java.awt.Color;
import java.util.Properties;

public interface GeometryColorModel {


    public Color computeColor(Ambiente ambiente,
		       Appearance appearance,
			      Properties properties,
		       Vector3D faceNormal,
		       Camera camera);

}
