package tools3d;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import generaltools.TestTools;

import org.testng.*;
import org.testng.annotations.*;

public class GeometryTools {

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    public GeometryTools() { }

    /** how many lines with angle angleDelta approximate circle? */
    public static int computeCircleDevisions(double angleDelta) {
	return (int) (2.0*Math.PI/angleDelta);
    }

    /** computes minimum distance from point to line (defined with offset and direction). Returns negative values if on negative branch */
    public static double distanceToLine(Vector3D point,
					Vector3D offset,
					Vector3D direction)
    {
	assert( (direction.length()>0.0));
	double result = 0.0;
	Vector3D dVec = point.minus(offset);
	double dLength = dVec.length();
	if (dLength == 0.0) {
	    return 0.0; // test point happens to be start point of line
	}
	double cosAlpha = dVec.dot(direction) /(dLength*direction.length());
	double alpha = Math.acos(cosAlpha);
	result = dLength * Math.sin(alpha);
	if (cosAlpha < 0.0)
	    {
		result *= -1.0;
		assert(result <= 0.0);
	    }
	assert((result*cosAlpha) >= 0.0);
	return result;
    }

    /** Computes point on line that corresponds to minimum distance from point to line (defined with offset and direction). */
    public static Vector3D closestPointOnLine(Vector3D point,
					      Vector3D offset,
					      Vector3D directionOrig) {
	assert( (directionOrig.length()>0.0));
	Vector3D direction=new Vector3D(directionOrig);
	direction.normalize(); 
	Vector3D dVec = point.minus(offset);
	double dLength = dVec.length();
	if (dLength == 0.0) {
	    return new Vector3D(point); // test point happens to be start point of line
	}
	double projection = dVec.dot(direction); // already normalized: /direction.length();
	Vector3D closestPoint = offset.plus(direction.mul(projection));
	// double cosAlpha = dVec.dot(direction) /(dLength*direction.length());
	// double alpha = Math.acos(cosAlpha);
	// result = dLength * Math.sin(alpha);
	// if (cosAlpha < 0.0)
	// {
	// result *= -1.0;
	// assert(result <= 0.0);
	// }
	// assert((result*cosAlpha) >= 0.0);
	return closestPoint;
    }

    /** computes point on line that corresponds to minimum distance from point to line (defined with offset and direction). Returns negative values if on negative branch */
    @Test(groups={"new"})
    public static void closestPointOnLineTest() {
	Vector3D p = new Vector3D(-1.0, 0.0, 0.0);
	Vector3D offset = new Vector3D(0.0, 1.0, 0.0);
	Vector3D dir = new Vector3D(1.0, 0.0, 0.0);
	Vector3D result = closestPointOnLine(p, offset, dir); // should be point -1, 1, 0
	assert(result.getX() == -1.0);
	assert(result.getY() == 1.0);
	assert(result.getZ() == 0.0);
    }

    /** computes point on line that corresponds to minimum distance from point to line (defined with offset and direction). Returns negative values if on negative branch */
    @Test(groups={"new"})
    public static void closestPointOnLineTest2() {
	Vector3D p = new Vector3D(-1.0, 1.0, 0.0);
	Vector3D offset = new Vector3D(0.0, 1.0, 0.0);
	Vector3D dir = new Vector3D(1.0, 0.0, 0.0);
	Vector3D result = closestPointOnLine(p, offset, dir); // should be point -1, 1, 0
	assert(result.getX() == -1.0);
	assert(result.getY() == 1.0);
	assert(result.getZ() == 0.0);
    }

    /** computes minimum distance from point to line segment 
     * (line segment going between points offset and offset + directionAndLength)
     */
    public static double distanceToLineSegment(Vector3D point,
					       Vector3D offset,
					       Vector3D directionAndLength) {
	assert( directionAndLength.length() >0.0 );
	double result = distanceToLine(point, offset, directionAndLength);
	if (result < 0) {
	    result = point.distance(offset); // "0" of line segment is closest to point
	} else if (result > directionAndLength.length()) {
	    result = point.distance(offset.plus(directionAndLength)); // "0" of line segment is closest to point
	}
	return result;
    }
    
    /** Computes minimum distance between two skew lines in space: p = p0 + s * u ; q = q0 + t * v
     * @see http://geometryalgorithms.com/Archive/algorithm_0106/algorithm_0106.htm
     */
    public static double distanceOfLines(Vector3D p0, Vector3D u,
					 Vector3D q0, Vector3D v) {
	assert u.lengthSquare() > 0;
	assert v.lengthSquare() > 0;
	double a = u.dot(u);
	assert a > 0.0;
	double b = u.dot(v);
	double c = v.dot(v);
	assert c > 0.0;
	Vector3D w0 = p0.minus(q0);
	double d = u.dot(w0);
	double e = v.dot(w0);
	double denom = a*c - (b*b);
	double s = 0, t = 0;
	if (denom == 0.0) {
	    t = e / c; // special case: set s = 0.0
	}
	else {
	    s = (b*e - (c*d)) / denom;
	    t = (a*e - (b*d)) / denom;
	}
	Vector3D p = p0.plus(u.mul(s));
	Vector3D q = q0.plus(v.mul(t));
	double dist = p.distance(q);
	return dist;
    }

    private static String distanceOfLinesString(Vector3D p0, Vector3D u,
						Vector3D q0, Vector3D v, double dist) {
	return "Distance between lines " + p0 + " " + u + " and " + q0 + " " + v + " : " + dist;
    }

    /** Tests distanceOfLines method using two lines in xy-plane with distance 1.0
     * @see http://geometryalgorithms.com/Archive/algorithm_0106/algorithm_0106.htm     
     */
    @Test(groups={"new"})
    public void distanceOfLinesTest() {
	String methodName = "distanceOfLinesTest";
	System.out.println(TestTools.generateMethodHeader(methodName));
	Vector3D p0 = new Vector3D(0.0, 0.0, 0.0);
	Vector3D p0b = new Vector3D(2.0, 0.0, 0.0);
	Vector3D q0 = new Vector3D(0.0, 0.0, 1.0);
	Vector3D u = new Vector3D(1.0, 0.0, 0.0);
	Vector3D v = new Vector3D(1.0, 1.0, 0.0);
	Vector3D vb = new Vector3D(-1.0, -1.0, 0.0);
	double result1 = distanceOfLines(p0, u, q0, v);
	System.out.println(distanceOfLinesString(p0, u, q0, v, result1));
	double result2 = distanceOfLines(p0b, u, q0, v);
	System.out.println(distanceOfLinesString(p0b, u, q0, vb, result2));
	assert Math.abs(result1 - 1.0) < 0.01; // must be close to one
	assert Math.abs(result2 - 1.0) < 0.01; // must be close to one
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


    public static Geometry triangularize(Vector3D[] v, double angleMin, double angleMax,
					 double lengthMin, double lengthMax) {
	log.fine("Starting triangularization! " + v.length 
			   + " " + angleMin + " " + angleMax + " " + lengthMin + " " + lengthMax);
	List<Edge3D> edgeList = new ArrayList<Edge3D>();
	List<Face3D> faceList = new ArrayList<Face3D>();
	int nn = v.length;
	for (int i = 0; i < nn; ++i) {
	    for (int j = i + 1; j < nn; ++j) {
		double d = v[i].distance(v[j]);
		if ((d <= lengthMax) && (d >= lengthMin)) {
		    edgeList.add(new Edge3D(v[i], v[j]));
		}
	    }
	}
	int ne = edgeList.size();
	log.fine("number of found edges: " + edgeList.size());
	// determine faces by adjacent edges
	for (int i = 0; i < ne; ++i) {
	    Edge3D edgeI = (Edge3D)(edgeList.get(i));
	    for (int j = i + 1; j < ne; ++j) {
		Edge3D edgeJ = (Edge3D)(edgeList.get(j));
		Vector3D pij = edgeI.findAdjacent(edgeJ);
		if (pij != null) {
		    for (int k = j + 1; k < ne; ++k) {
			Edge3D edgeK = (Edge3D)(edgeList.get(k));
			Vector3D pik = edgeI.findAdjacent(edgeK);
			Vector3D pjk = edgeJ.findAdjacent(edgeK);
			if ((pik != null) && (pjk != null)
			    && (pij.distance(pik) > 0) && (pij.distance(pik) > 0) && (pik.distance(pjk) > 0) ) {
			    Vector3D[] threePos = new Vector3D[3];
			    threePos[0] = pij;
			    threePos[1] = pik;
			    threePos[2] = pjk;
			    faceList.add(new Face3D(threePos));
			}
		    }
		}
	    }
	}
	Point3D[] points = new Point3D[v.length];
	Edge3D[] edges = new Edge3D[edgeList.size()];
	Face3D[] faces = new Face3D[faceList.size()];
	for (int i = 0; i < points.length; ++i) {
	    points[i] = new Point3D(v[i]);
	}
	for (int i = 0; i < edges.length; ++i) {
	    edges[i] = (Edge3D)(edgeList.get(i));
	}
	for (int i = 0; i < faces.length; ++i) {
	    faces[i] = (Face3D)(faceList.get(i));
	}
	log.fine("number of found edges: " + edgeList.size() + " faces: " + faceList.size());
	return new StandardGeometry(points, edges, faces);
    }

    /** returns the distance of a point from plane (defined through normal and offset). 
     * Negative distance means that point is "below" plane */
    public static double planeDistance(Vector3D planeNormal, Vector3D planeOffset, Vector3D point) {
	assert(planeNormal.length() > 0.99 && planeNormal.length() < 1.01);
	Vector3D v = point.minus(planeOffset);
	double result = v.dot(planeNormal);
	return result;
    }

}
