package tools3d;

import java.util.logging.Logger; /** Generates a debug log. */
import java.util.Vector; /** The Vectors which contain the data in the Junction. */

import org.testng.annotations.Test; /** Testing package. */

import static tools3d.PackageConstants.*; /** Imports constants. */
import tools3d.objects3d.Link; /** Links in the Junction. */
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DIOException; /** An Exception dealing with input and output of Object3Ds. */
import tools3d.objects3d.SimpleLink; /** Links in the Junction. */

/**
 * The <code>Junction</code> class generates an object which holds information such
 * as <code>Point</code>s, <code>Link</code>s, and <code>Vector2</code>s.
 * A <code>Junction</code> is a group of points, like the corner of a square.
 *
 * @author Christine Viets
 * @deprecated
 */
public class Junction {
    
    private Point centerPoint = new Point("overwrite", 0.0, 0.0, 0.0); //The center point of the junction: default is origin.
    
    private String name = ""; /** The name of this Junction. */
    
    private Vector<Link> links = new Vector<Link>(); /** The Vector which contains all the Links in the Junction. */
    private Vector<Point> points = new Vector<Point>(); /** The Vector which contains all the Points in the Junction. */
    private Vector<Vector2> vectors = new Vector<Vector2>(); /** The Vector which contains all the Vector2s in the Junction. */
    
    private static Logger log = Logger.getLogger("NanoTiler_debug"); /** The generated debug log. */
    
    /** Constructor. Creates a new, blank Junction. */
    public Junction() { }
    
    /**
     * Constructor: Creates a new Junction with the given name.
     *
     * @param name The String representation of the name of the Junction.
     */
    public Junction(String name) {
	setName(name);
    }
    
    /**
     * Adds a Link to the Junction.
     *
     * @param link The Link to be added to the Junction.
     */
    public void addLink(Link link) {
	links.add(link);
    }

    /**
     * Adds a Link to the Junction.
     *
     * @param point1 The first Point in the Link.
     * @param point2 The second Point in the Link.
     */
    public void addLink(Point point1,
			Point point2) {
	Link link = new SimpleLink("name", point1, point2);
	addLink(link);
    }

    /** Tests method addLink. */
    @Test public void testAddLink() {
	Junction testJunction = new Junction();
	Link testLink = new SimpleLink("link",
				       new Point(0, 0, 0),
				       new Point(100, 100, 100) );
	int index = 0;

	assert (testJunction.getNumLinks() == 0);
	/** Test method addLink( Link ). */
	testJunction.addLink(testLink);
	assert (testJunction.getLink(index).equals(testLink) );

	testJunction = new Junction();
	assert (testJunction.getNumLinks() == 0);
	/** Test method addLink( Point, Point ). */
	testJunction.addLink(new Point(0, 0, 0), new Point(100, 100, 100) );
	assert (testJunction.getLink(index).getObj1().getName().equals(testLink.getObj1().getName() ) );
	assert (testJunction.getLink(index).getObj1().getPosition().equals(testLink.getObj1().getPosition() ) );
	assert (testJunction.getLink(index).getObj2().getName().trim().equals(testLink.getObj2().getName().trim() ) );
	assert (testJunction.getLink(index).getObj2().getPosition().equals(testLink.getObj2().getPosition() ) );
    }

    /** 
     * Adds a Point to the Junction.
     *
     * @param point The Point to be added to the Junction.
     */
    public void addPoint(Point point) {
	points.add(point);
	if (point.getCenterPoint() ) {
	    centerPoint = point;
	}
    }

    /** Tests method addPoint. */
    @Test public void testAddPoint() {
	Junction testJunction = new Junction();
	Point testPoint = new Point("testPoint", 10, 10, 10, true);
	int index = 0;

	assert testJunction.getNumPoints() == 0;
	testJunction.addPoint(testPoint);
	assert testJunction.getPoint(index).equals(testPoint);
	assert testJunction.getPoint(index).getCenterPoint();
    }

    /**
     * Adds a Vector2 to the Junction.
     *
     * @param vector The Vector2 to add to the Junction.
     */
    public void addVector(Vector2 vector) {
	vectors.add(vector);
    }

    /**
     * Adds a Vector2 to the Junction.
     *
     * @param point The Point from which the vector originates.
     * @param vectorPoint The Point the Vector2 points to.
     */
    public void addVector(Point point,
			  Point vectorPoint) {
	vectors.add(new Vector2(point, vectorPoint) );
    }

    /** Tests method addVector. */
    @Test public void testAddVector() {
	Junction testJunction = new Junction();
	Vector2 testVector = new Vector2(new Point("origPoint", 10, 10, 10),
					 new Point("vectorPoint", 100, 100, 100) );
	int index = 0;

	/** Test method addVector(Vector2). */
	testJunction.addVector(testVector);
	assert testJunction.getVector(index).equals(testVector);

	testJunction = new Junction();
	assert testJunction.getNumVectors() == 0;
	/** Test method addVector(Point, Point). */
	testJunction.addVector(new Point("origPoint", 10, 10, 10),
			       new Point("vectorPoint", 100, 100, 100) );
	assert testJunction.getVector(index).equals(testVector);
    }

    /**
     * Creates and returns a copy of this Object.
     */
    public Object cloneDeep() {
	Junction newJunction = new Junction();
	newJunction.copyDeepThisCore(this); // Copy this Junction into the new Junction.
	assert newJunction.getNumPoints() == this.getNumPoints();
	return newJunction;
    }

    /** Tests method cloneDeep. */
    @Test public void testCloneDeep() {
	Junction testJunction = new Junction("junction");
	
	Point testPoint1 = new Point("testPoint1", 0, 0, 0);
	testJunction.addPoint(testPoint1);
	Point testPoint2 = new Point("testPoint2", 100, 100, 100);
	testJunction.addPoint(testPoint2);
	
	testJunction.addLink(testPoint1, testPoint2);
	testJunction.addVector(testPoint1, testPoint2);

	Junction newJunction = new Junction();
	newJunction = (Junction)testJunction.cloneDeep(); // Clone the original Junction into the new Junction.
	assert testJunction.getName().equals(newJunction.getName() );
	assert testJunction.getPoint(0).equals(newJunction.getPoint(0) );
	assert testJunction.getPoint(1).equals(newJunction.getPoint(1) );
	assert testJunction.getLink(0).equals(newJunction.getLink(0) );
	assert testJunction.getVector(0).equals(newJunction.getVector(0) );
    }
    
    /**
     * Returns true if one of the points in the Junction is "close" to the point. 
     *
     * @param v The Vector3D point that may be contained in the Junction.
     */
    public boolean containsPoint(Vector3D v) {
	Point point = new Point(v.getX(), v.getY(), v.getZ()); // Create a Point from the Vector3D
	boolean found = false; // If the Point has been found in this Junction.
	int i = -1;
	log.info("" + found + "" + getNumPoints() );
	while ( !found && (i++ < getNumPoints() ) ) { // While the Point has not been found and all the Points haven't been tested.
	    log.fine("testing: " + (Point)points.get(i) );
	    if ( ( (Point)points.get(i) ).isSimilarTo(point) ) // If the Point is close enough to a Point in the Junction.
		found = true; // The Point has been found.
	}
	return found;
    }

    /** Tests method containsPoint. */
    @Test public void testContainsPoint() {
	Junction testJunction = new Junction();
	testJunction.addPoint(new Point("testPoint", 0, 0, 0) );
	assert testJunction.containsPoint(new Vector3D(0, 0, 0) );
    }

    /**
     * Copies all elements of this Junction into a new Junction.
     * Is tested by testCloneDeep
     *
     * @param jToCopy The Junction from which to copy data from.
     */
    protected void copyDeepThisCore(Junction jToCopy) {
	this.setName(jToCopy.getName());
	this.setCenterPoint(jToCopy.getCenterPoint());
	Vector<Point> newPoints = new Vector<Point>();
	for (int i = 0; i < jToCopy.getNumPoints(); i++) {
	    newPoints.add( (Point)(jToCopy.getPoint(i).cloneDeep() ) );
	    assert newPoints.get(i).equals(jToCopy.getPoint(i) );
	}
	this.points = newPoints;
	Vector<Link> newLinks = new Vector<Link>();
	for (int i = 0; i < jToCopy.getNumLinks(); i++) {
	    newLinks.add( (Link)(jToCopy.getLink(i).clone() ) ); // careful: links point to OLD points!
	    assert newLinks.get(i).equals(jToCopy.getLink(i) );
	}
	this.links = newLinks;
	Vector<Vector2> newVectors = new Vector<Vector2>();
	for (int i = 0; i < jToCopy.getNumVectors(); i++) {
	    newVectors.add( (Vector2)(jToCopy.getVector(i).cloneDeep() ) );  
	    assert newVectors.get(i).equals(jToCopy.getVector(i) );
	}
	this.vectors = newVectors;
    }

    /**
     * Returns the center point of the Junction;
     * returns the Point with the most neighbors if the center point has not been set.
     */
    public Point getCenterPoint() {
	if (centerPoint.getName().equals("overwrite") ) { //If the center point has not been set.
	    Point bestGuess = new Point();
	    for (int i = 0; i < getNumPoints(); i++) {
		Point point = getPoint(i);
		if (point.getNumNeighbors() > bestGuess.getNumNeighbors() ) {
		    bestGuess = (Point)point.cloneDeep();
		}
	    }
	    return bestGuess;
	}
	else {
	    return centerPoint;
	}
    }
    
    /** Test method getCenterPoint. */
    @Test public void testGetCenterPoint() {
	Junction testJunction = new Junction("testJunction");
	Point testPoint = new Point("centerPoint", 0, 0, 0, true);
	testJunction.addPoint(testPoint);
	assert testJunction.getCenterPoint().equals(testPoint);
	assert testJunction.getCenterPoint().getCenterPoint(); // Check that the Point is set as a centerPoint.
    }
    
    /**
     * Returns the Link at the given index.
     *
     * @param index The index at which the desired Link occurs.
     */
    public Link getLink(int index) {
	return (Link)links.get(index);
    }
    
    /**
     * Returns the Link with the given name;
     * returns a new object if the Link is not found.
     *
     * @param linkName The String name of the desired Link.
     */
    public Link getLink(String linkName) {
	try {
	    return getLink(getLinkIndex(linkName) );
	}
	catch(Object3DIOException e) {
	    return new SimpleLink();
	}
    }
    
    /** Tests method getLink. */
    @Test public void testGetLink() {
	Junction testJunction = new Junction("testJunction");
	Link testLink = new SimpleLink("testLink",
				       new Point(0, 0, 0),
				       new Point(100, 100, 100) );
	int index = 0;
	
	testJunction.addLink(testLink);
	
	/** Test method getLink(int). */
	assert testJunction.getLink(index).equals(testLink);
	
	/** Test method getLink(String). */
	assert testJunction.getLink("testLink").equals(testLink);
    }
	
    /**
     * Returns the index of the Link with the given name;
     * throws an exception if the Link is not found.
     *
     * @param linkName The String name of the desired Link.
     */
    public int getLinkIndex(String linkName) throws Object3DIOException {
	int index = -1;
	boolean indexFound = false; // If the Link with the given name has been found.
	for (int i = 0; i < getNumLinks(); i++) {
	    if ( ( (Link)links.get(i) ).getName().equals(linkName) ) {
		indexFound = true; // The Link with the given name has been found.
		index = i;
	    }
	}
	if (!indexFound) {
	    throw new Object3DIOException("Link: " + linkName + 
					  " could not be found.");
	}
	else {
	    return index;
	}
    }

    /** Tests method getLinkIndex. */
    @Test public void testGetLinkIndex() {
	Junction testJunction = new Junction("testJunction");
	Link testLink = new SimpleLink("testLink",
				       new Point(0, 0, 0),
				       new Point(100, 100, 100) );
	int index = 0;
	testJunction.addLink(testLink);

	try {
	    assert getLinkIndex("testLink") == index;
	}
	catch( Object3DIOException e ) { } // Test failed.
    }

    /** Returns the String representation of the name of the Junction. */
    public String getName() {
	return name;
    }

    /** Tests method getName. */
    @Test public void testGetName() {
	Junction testJunction = new Junction("testJunction");
	assert testJunction.getName().equals("testJunction");
    }

    /** Returns the number of links in the Junction. */
    public int getNumLinks() {
	return links.size();
    }

    /** Tests method getNumLinks. */
    @Test public void testGetNumLinks() {
	Junction testJunction = new Junction("testJunction");
	Link testLink1 = new SimpleLink("testLink1",
					new Point("point1", 0, 0, 0),
					new Point("point2", 10, 10, 10) );
	Link testLink2 = new SimpleLink("testLink2",
					new Point("point2", 10, 10, 10),
					new Point("point3", 20, 20, 20) );
	int numLinks = 2;

	testJunction.addLink(testLink1);
	testJunction.addLink(testLink2);
	assert testJunction.getNumLinks() == numLinks;
    }

    /** Returns the number of Points in the Junction. */
    public int getNumPoints() {
	return points.size();
    }

    /** Tests method getNumPoints. */
    @Test public void testGetNumPoints() {
	Junction testJunction = new Junction();
	int numPoints = 3;

	testJunction.addPoint(new Point("point1", 0, 0, 0) );
	testJunction.addPoint(new Point("point2", 10, 10, 10) );
	testJunction.addPoint(new Point("point3", 20, 20, 20) );
	assert testJunction.getNumPoints() == numPoints;
    }

    /** Returns the number of vectors in the Junction. */
    public int getNumVectors() {
	return vectors.size();
    }

    /** Tests method getNumVectors. */
    @Test public void testGetNumVectors() {
	Junction testJunction = new Junction();
	int numVectors = 2;

	testJunction.addVector(new Point(0, 0, 0),
			       new Point(10, 10, 10) );
	testJunction.addVector(new Point(10, 10, 10),
			       new Point(20, 20, 20) );
	assert testJunction.getNumVectors() == numVectors;
    }

    /**
     * Returns the Point at the given index.
     *
     * @param index The index at which to find the desired Point.
     */
    public Point getPoint(int index) {
	return (Point)points.get(index);
    }

    /**
     * Returns the Point with the given name;
     * returns a new object if the Point is not found.
     *
     * @param pointName The String name of the desired Point.
     */
    public Point getPoint(String pointName) {
	try {
	    return getPoint(getPointIndex(pointName) );
	}
	catch (Object3DIOException e) {
	    log.severe( "Point with name: " + pointName + 
			" was not found!" );
	    return new Point();
	}
    }

    /** Tests method getPoint. */
    @Test public void testGetPoint() {
	Junction testJunction = new Junction();
	Point testPoint = new Point("testPoint", 0, 0, 0);
	int index = 0;
	testJunction.addPoint(testPoint);

	/** Test method getPoint(int). */
	assert testJunction.getPoint(index).equals(testPoint);
	
	/** Test method getPoint(String). */
	assert testJunction.getPoint("testPoint").equals(testPoint);
    }

    /**
     * Returns the index of the point with the given name;
     * throws an exception if the Point is not found.
     *
     * @param pointName The String name of the desired Point.
     */
    public int getPointIndex(String pointName) throws Object3DIOException {
	int i = -1;
	boolean indexFound = false; // If the index of the Point with the given name has been found.
	while( !indexFound && i < (points.size() - 1) ) { // While the index has not been found and there are still more Points to test,
	    i++;
	    if ( ( (Point)points.get(i) ).getName().equals(pointName) ) {
		indexFound = true;
	    }
	}
	if (!indexFound) {
	    log.severe( "Point with name: " + pointName + 
			" could not be found!" );
	    throw new Object3DIOException("Point: " + pointName + 
					  " could not be found.");
	}
	else {
	    return i;
	}
    }

    /** Tests method getPointIndex. */
    @Test public void testGetPointIndex() {
	Junction testJunction = new Junction();
	int index = 0;
	testJunction.addPoint(new Point("testPoint", 0, 0, 0) );

	try {
	    assert testJunction.getPointIndex("testPoint") == index;
	}
	catch (Object3DIOException e) {
	    assert false;
	    log.severe("Point index was not found.");
	}
    }

    /**
     * Returns the Vector2 at the given index.
     *
     * @param index The index at which the desired Vector2 occurs.
     */
    public Vector2 getVector(int index) {
	return (Vector2)vectors.get(index);
    }

    /**
     * Returns the Vector2 that contains the given Point;
     * throws an Exceptioin if the Vector2 is not found.
     *
     * @param point A Point that occurs in the desired Vector2.
     */
    public Vector2 getVector(Point point) throws Object3DIOException {
	for (int i = 0; i < getNumVectors(); i++) {
	    Vector2 vector = getVector(i);
	    if (vector.containsPoint(point) ) {
		return vector;
	    }
	}
	throw new Object3DIOException("The Vector2 with " + 
				      point + " was not found.");
    }

    /** Tests method getVector. */
    @Test public void testGetVector() {
	int index = 0;
	Junction testJunction = new Junction();
	Vector2 testVector = new Vector2(new Point(0, 0, 0),
					 new Point(100, 100, 100) );
	testJunction.addVector(testVector);
	assert testJunction.getVector(index).equals(testVector);
    }

    public void rotate(Vector3D start, Vector3D axis, double angle) {
	centerPoint.getVector3D().rotate(start, axis, angle);
	for (int i = 0; i < getNumPoints(); i++) {
	    Vector3D vector3d = ((Point)getPoint(i).cloneDeep()).getVector3D();
	    vector3d.rotate(start, axis, angle);
	    Point point = new Point(getPoint(i).getName(), vector3d.getX(), vector3d.getY(), vector3d.getZ(), getPoint(i).getCenterPoint());
	    points.setElementAt(point, i);
	}
	for (int i = 0; i < getNumLinks(); i++) {
	    Link link = (Link)getLink(i).clone();
	    link.getObj1().rotate(start, axis, angle);
	    link.getObj2().rotate(start, axis, angle);
	    links.setElementAt(link, i);
	}
	for (int i = 0; i < getNumVectors(); i++) {
	    Vector2 vector = (Vector2)getVector(i).cloneDeep();
	    Point point = (Point)vector.getPoint().cloneDeep();
	    Point vectorPoint = (Point)vector.getVectorPoint().cloneDeep();
	    point.rotate(start, axis, angle);
	    vectorPoint.rotate(start, axis, angle);
	    Vector2 newVector = new Vector2(point, vectorPoint);
	    vectors.setElementAt(newVector, i);
	}
    }

    /** */
    @Test public void testRotate() {
	Junction testJunction = new Junction("testJunction");
	Point point1 = new Point("point1", 0, 0, 0, true);
	Point point2 = new Point("point2", 1, 1, 1);
	Point vectorPoint = new Point("vectorPoint", 2, 2, 2);
	Link link = new SimpleLink(point1.toObject3D(), point2.toObject3D());
	Vector2 vector = new Vector2(point1, vectorPoint);

	testJunction.addPoint(point1);
	testJunction.addPoint(point2);
	testJunction.addLink(link);
	testJunction.addVector(vector);

	testJunction.rotate(new Vector3D(0, 0, 0), new Vector3D(0, 1, 0), Math.PI);

	assert testJunction.getCenterPoint().equals(point1);
	assert testJunction.getPoint(0).similar(point1);
	assert testJunction.getPoint(1).similar(new Point(-1, 1, -1));
	assert ((Vector3D)(testJunction.getLink(0).getObj1().getPosition())).similar(point1.getVector3D());
	assert ((Vector3D)(testJunction.getLink(0).getObj2().getPosition())).similar(new Point(-1, 1, -1).getVector3D());
	assert testJunction.getVector(0).equals(new Vector2(point1, new Point("vectorPoint", -2, 2, -2)));
    }

    /**
     * Sets the center point of the Junction.
     *
     * @param centerPoint The center point of the Junction.
     */
    public void setCenterPoint(Point centerPoint) {
	if (this.centerPoint.getName().equals("overwrite") ) {
	    this.centerPoint = centerPoint;
	}
	else {
	    if (this.centerPoint.equals(centerPoint) ) {
		return;
	    }
	    else {
		assert(false);
	    }
	}
    }

    /** Tests method setCenterPoint. */
    @Test public void testSetCenterPoint() {
	Junction testJunction = new Junction();
	Point centerPoint = new Point("centPoint", 0, 0, 0, true);

	testJunction.setCenterPoint(centerPoint);
	assert testJunction.getCenterPoint().equals(centerPoint);
    }

    /**
     * Sets the name of the Junction.
     *
     * @param name The String representation of the name of the Junction.
     */
    public void setName(String name) {
	this.name = name;
    }
    
    /** Tests method setName. */
    @Test public void testSetName() {
	Junction testJunction = new Junction();
	
	testJunction.setName("testJunction");
	
	assert testJunction.getName().equals("testJunction");
    }
    
    /**
     * Returns a String representation of the Junction in the format:
     *
     * "Junction "name
     * "Points:"
     * "   "point(0)
     * "   "point(1)
     * ...
     * "Links:"
     * "   "link(0)
     * "   "link(1)
     * ...
     * "Vectors:"
     * "   "vector(0)
     * "   "vector(1)
     * ...
     *
     */
    public String toString() {
	String s = new String("Junction " + getName() + NEWLINE + 
			      "Points:" + NEWLINE);
	for (int i = 0; i < getNumPoints(); i++) {
	    s += "   " + getPoint(i) + NEWLINE;
	}
	s += "Links:" + NEWLINE;
	for (int i = 0; i < getNumLinks(); i++) {
	    s += "   " + getLink(i) + NEWLINE;
	}
	s += "Vectors:" + NEWLINE;
	for(int i = 0; i < getNumVectors(); i++) {
	    s += "   " + getVector(i) + NEWLINE;
	}
	return s;
    }
    
    /** Tests method toString.
     * Test fails, however, the test was taken out because the whole class is deprecated. */
    // @Test
    public void testToString() {
	Junction testJunction = new Junction();
	testJunction.setName("testJunction");
	Point point1 = new Point("point1", 0, 0, 0, true);
	Point point2 = new Point("point2", 100, 100, 100);
	testJunction.addPoint(point1);
	testJunction.addPoint(point2);
	testJunction.addLink(point1,point2);
	Point vectorPoint = new Point("vectorPoint", 50, 50, 50);
	testJunction.addVector(point1,vectorPoint);
	String s = new String("Junction testJunction" + NEWLINE + 
			      "Points:" + NEWLINE + 
			      "   point1: 0.0, 0.0, 0.0 (center point)" + NEWLINE + 
			      "   point2: 100.0, 100.0, 100.0" + NEWLINE + 
			      "Links:" + NEWLINE + 
			      "   (Link name point1 point2)" + NEWLINE + 
			      "Vectors:" + NEWLINE + 
			      "   Vector from point1: 0.0, 0.0, 0.0 (center point) to vectorPoint: 50.0, 50.0, 50.0" + NEWLINE);
	assert testJunction.toString().equals(s);
    }
    
}
