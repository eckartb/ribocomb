package tools3d;

import java.util.Random;
import generaltools.Randomizer;
import java.util.logging.Logger;

public class KabschSuperpose implements Superpose {

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    public static Random rnd = Randomizer.getInstance();
    public static final double PI2 = Math.PI * 2.0;
    private int iterMax = 100000;
    private double rmsLim = 0.01;
    private double angleStep = Math.PI;
    private int numTrial = 1;
    private int centerId = -1;

//     public static double computeRms(Vector3D[] constCoord,
// 				    Vector3D[] varCoord) {
// 	double sum = 0.0;
// 	for (int i = 0; i < constCoord.length; ++i) {
// 	    sum += (constCoord[i].minus(varCoord[i])).lengthSquare();
// 	}
// 	sum /= constCoord.length;
// 	return Math.sqrt(sum);
//     }

//     public static double computeRms(Vector3D[] constCoord,
// 				    Vector3D[] varCoord,
// 				    double phi, // defines rotation axis
// 				    double theta, // defines rotation axis
// 				    double angle) {
// 	assert constCoord.length == varCoord.length;
// 	double sum = 0.0;
// 	Vector3D axis = Vector3DTools.generateCartesianFromPolar(phi, theta, 1.0);
// 	Matrix3D rotationMatrix = Matrix3D.rotationMatrix(axis, angle);
// 	SuperpositionResult supResult = new SuperpositionResult();
// 	supResult.setRotationMatrix(rotationMatrix);
// 	for (int i = 0; i < constCoord.length; ++i) {
// 	    Vector3D vTmp = rotationMatrix.multiply(varCoord[i]); // Matrix3DTools.rotate(varCoord[i], axis, angle);
// 	    assert vTmp.distance(supResult.returnTransformed(varCoord[i])) < 0.1;
// 	    sum += (constCoord[i].minus(vTmp)).lengthSquare();
// 	}
// 	sum /= constCoord.length;
// 	double rms = Math.sqrt(sum);
// 	return rms;
//     }

    /** sets center id: instead of cener of gravity, structures get superposed at point n */
    // public void setCenterId(int n) { this.centerId = n; }

    /** change varCoord such that it closest to constCoord using
     * only tranlation and rotation */
    public SuperpositionResult superpose(Vector3D[] constCoord,
					 Vector3D[] varCoord) {
	assert constCoord.length == varCoord.length;
	Kabsch kabsch = new Kabsch(constCoord, varCoord);
	kabsch.align(); // perform computation
	kabsch.applyTransformation(varCoord);
	SuperpositionResult result = new SuperpositionResult(kabsch);
	return result;
    }

}
