package tools3d;

/** defines section in a 3D lattice */
public class LatticeSection {
    
    private int xmin = 0;
    private int ymin = 0;
    private int zmin = 0;
    private int xmax = 0;
    private int ymax = 0;
    private int zmax = 0;
    
    public LatticeSection(int xmin,
		       int ymin,
		       int zmin,
		       int xmax,
		       int ymax,
		       int zmax) {
	this.xmin = xmin;
	this.ymin = ymin;
	this.zmin = zmin;
	this.xmax = xmax;
	this.ymax = ymax;
	this.zmax = zmax;
    }

    public int getXMin() { return xmin; }

    public int getYMin() { return ymin; }

    public int getZMin() { return zmin; }

    public int getXMax() { return xmax; }

    public int getYMax() { return ymax; }

    public int getZMax() { return zmax; }

    public int getVolume() {
	return (getXMax()-getXMin()) * (getYMax()-getYMin()) * (getZMax()-getZMin());
    }

    public void setXMin(int v) { this.xmin = v; }

    public void setYMin(int v) { this.ymin = v; }

    public void setZMin(int v) { this.zmin = v; }

    public void setXMax(int v) { this.xmax = v; }

    public void setYMax(int v) { this.ymax = v; }

    public void setZMax(int v) { this.zmax = v; }


    public String toString() {
	return "" + xmin + " " + ymin + " " + zmin + " " + xmax + " " + ymax + " " + zmax;
    }

}
