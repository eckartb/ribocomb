package tools3d;


public interface LightSource extends Shape3D {
    
    
    /** what is the total flux of light going from source position in a direction? */
    public double getFlux(Vector3D direction);

    /** what is the flux of light with respect to certain color and bandwith? This can be used to implement an arbitray spectrum. */
    public double getFlux(Vector3D direction, Vector3D color, double bandwidth);    

}
