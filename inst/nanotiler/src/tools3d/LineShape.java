package tools3d;

/** implementation of point (with radius!) as Shape3D
 */
public class LineShape extends SimpleShape3D implements Shape3D {

    private Vector3D position1;

    private Vector3D position2;

    /** defines sphere with radius and position */
    public LineShape(Vector3D pos1, Vector3D pos2) {
	super();
	Vector3D pos = pos1.plus(pos2);
	pos = pos.mul(0.5);
	setPosition(pos);
	this.position1 = pos1;
	this.position2 = pos2;
	Vector3D diff = pos2.minus(pos1);
	double radius = diff.length();
	setBoundingRadius(radius);
	setDimensions(new Vector3D(radius, radius, radius));
    }

    /** creates dummy set of points, edges, faces */
    public Geometry createGeometry(double angleDelta) {
	if (!isGeometryUpToDate()) {
	    updateGeometry(angleDelta);
	}
	return geometry;
    }

    public Vector3D getPosition1() { return position1; }

    public Vector3D getPosition2() { return position2; }

    public String getShapeName() { return "LineShape"; }

    protected void updateGeometry(double angleDelta) {
	Vector3D pos = getPosition();
	double radius = getBoundingRadius();
 	Point3D[] points = new Point3D[0];
	Edge3D[] edges = new Edge3D[1];
	edges[0] = new Edge3D(position1, position2);
 	Face3D[] faces = new Face3D[0];
 	geometry = new StandardGeometry(points, edges, faces);
	geometry.setProperties(getProperties());
	geometry.setSelected(isSelected());
	setGeometryUpToDate(true);
    }

    public String toString() {
	return "(Line " + getPosition1() + " " + getPosition2() + " )";
    }

}
