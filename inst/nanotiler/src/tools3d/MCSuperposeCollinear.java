package tools3d;

import generaltools.Randomizer;
import java.util.Random;
import java.util.logging.Logger;

/** Uses Monte Carlo Optimization to find best superposition of set of Affine Vectors
 * taking collinearity into account/
 * Instead of a set of vectors, the central superpose method uses startpos direction pairs */
public class MCSuperposeCollinear implements SuperposeCollinear {

    public static final double DEG2RAD = Math.PI/180.0;
    public static Random rnd = Randomizer.getInstance();
    public static final double PI2 = Math.PI * 2.0;
    private int iterMax = 100000;
    private double rmsLim = 0.01;
    private double angleStep = Math.PI;
    private int numTrial = 1;
    private int centerId = -1;
    private double shiftStep = 1.0; // for optimization of superposition: initial step size in Angstroem
    private double stepDecayMul = 0.99;
    private double angleWeight = 1.0;
    private double anglePenalty = 10.0;
    private double angleLimit = 40.0 * DEG2RAD; // maximum allowed angle error before penalty is added
    private double distanceLimit = 5.0; // maximum allowed distance error before position penalty is added
    private double distancePenalty = 10.0;
    private static Logger log = Logger.getLogger("NanoTiler_debug");

    /** returns rndom number between min and max */
    private double generateRandomDouble(double min, double max) {
	assert max >= min;
	double delta = max - min;
	double x = rnd.nextDouble();
	x *= delta;
	x += min;
	return x;
    }

    /** returns random number between min and max */
    private double generateRandomGaussian(double center, double scale) {
	double x = rnd.nextGaussian();
	x *= scale;
	x += center;
	return x;
    }

    public double getAngleLimit() { return this.angleLimit; }

    public double getDistanceLimit() { return this.distanceLimit; }

    private double mutateValue(double x, double step) {
	// return generateRandomDouble(x-step, x+step);
	return generateRandomGaussian(x, step);
    }

    /** mutates randomly number each component */
    private Vector3D mutateVector(Vector3D v, double step) {
	// return generateRandomDouble(x-step, x+step);
	return new Vector3D(generateRandomGaussian(v.getX(), step),
			    generateRandomGaussian(v.getY(), step),
			    generateRandomGaussian(v.getZ(), step));
    }
    
//     public static double computeRms(Vector3D[] constCoord,
// 				    Vector3D[] varCoord) {
// 	double sum = 0.0;
// 	for (int i = 0; i < constCoord.length; ++i) {
// 	    sum += (constCoord[i].minus(varCoord[i])).lengthSquare();
// 	}
// 	sum /= constCoord.length;
// 	return Math.sqrt(sum);
//     }

    private double computeError(Vector3D[] constCoord,
			       Vector3D[] constDirs,
			       Vector3D[] varCoord,
			       Vector3D[] varDirs,
			       double phi, // defines rotation axis
			       double theta, // defines rotation axis
			       double angle,
			       Vector3D shift) {
	assert constCoord.length == varCoord.length;
	double sum = 0.0;
	Vector3D axis = Vector3DTools.generateCartesianFromPolar(phi, theta, 1.0);
	Matrix3D rotationMatrix = Matrix3D.rotationMatrix(axis, angle);
	SuperpositionResult supResult = new SuperpositionResult();
	supResult.setRotationMatrix(rotationMatrix);
	for (int i = 0; i < constCoord.length; ++i) {
	    Vector3D vTmp = rotationMatrix.multiply(varCoord[i]).plus(shift); // Matrix3DTools.rotate(varCoord[i], axis, angle);
	    // assert vTmp.distance(supResult.returnTransformed(varCoord[i])) < 0.1;
	    double distTerm = Math.abs(GeometryTools.distanceToLine(vTmp, constCoord[i], constDirs[i])); // distance of vTmp from line in 3D
	    double angleTerm = Math.abs(constDirs[i].angle(rotationMatrix.multiply(varDirs[i])));
	    sum += distTerm + (angleWeight * angleTerm); 
	    if (angleTerm > angleLimit) {
		sum += anglePenalty;
	    }
	    if (distTerm > distanceLimit) {
		sum += distancePenalty;
	    }
	}
	sum /= constCoord.length;
	assert(sum >= 0.0);
	return sum;
    }

    public double getAngleWeight() { return angleWeight; }

    /** returns transformation info that transforms varCoord into constCoord. Ignores shifts */
    private SuperpositionResult monteCarloRun(Vector3D[] constCoord,
				      Vector3D[] constDirs,
				      Vector3D[] varCoord,
				      Vector3D[] varDirs) {
	// log.fine("Starting rotationRun!");
	// start with random orientation:
	double phi = rnd.nextDouble()*PI2;
	double theta = rnd.nextDouble()*Math.PI;
	double angle = rnd.nextDouble()*PI2;
	double angleStepCurr = angleStep;
	double shiftStepCurr = shiftStep;
	Vector3D varShift = new Vector3D(0, 0, 0);
	double bestRms = computeError(constCoord, constDirs, varCoord, varDirs, phi, theta, angle, varShift);
	int iter = 0;
	// log.fine("Initial rms: " + bestRms + " " + phi + " " + theta + " " + angle);
	// log.fine("" + iter + " " + iterMax + " " + bestRms + " " + rmsLim);
	while ((iter < iterMax) && (bestRms > rmsLim)) {
	    ++iter;
	    // log.fine("loop iteration: " + (iter+1));
	    // angleStepCurr = angleStep; //  * (0.99*(1.0 - ((double)iter / (double)iterMax))+0.01);
	    double phiNew = mutateValue(phi, angleStepCurr);
	    double thetaNew = mutateValue(theta, 0.5 * angleStepCurr);
	    Vector3D shiftNew = mutateVector(varShift, shiftStepCurr);
	    if (thetaNew < 0.0) {
		thetaNew = 0.0;
	    }
	    if (thetaNew > Math.PI) {
		thetaNew = Math.PI;
	    }
	    double angleNew = mutateValue(angle, angleStepCurr);
	    double rms = computeError(constCoord, constDirs, varCoord, varDirs, phiNew, thetaNew, angleNew, shiftNew);
	    if (rms < bestRms) {
		phi = phiNew;
		theta = thetaNew;
		angle = angleNew;
		bestRms = rms;
		varShift = shiftNew;
		// log.fine("Iteration " + (iter+1) + " : new best rms: " + bestRms + " " + phi
		// + " " + theta + " " + angle + " shift: " + varShift + " angle-step: " + angleStepCurr + " shift-step: " + shiftStepCurr);
		angleStepCurr *= stepDecayMul;
		shiftStepCurr *= stepDecayMul;

		// TODO : debug code, take out later
		// loop invariant
// 		Vector3D axis = Vector3DTools.generateCartesianFromPolar(phi, theta, 1.0);
// 		Matrix3D rotationMatrix = Matrix3D.rotationMatrix(axis, angle);
		// assert constCoord[1].distance( (rotationMatrix.mul(varCoord[1]) ) <= (2.0*bestRms);

	    }
	}
	// generate axis:
	Vector3D axis = Vector3DTools.generateCartesianFromPolar(phi, theta, 1.0);
	Matrix3D rotationMatrix = Matrix3D.rotationMatrix(axis, angle);
	SuperpositionResult result = new SuperpositionResult();
	result.setRms(bestRms);
	result.setRotationMatrix(rotationMatrix);
	result.setShift1(varShift); // TODO verify that it should not be setShift2

	// log.fine("Finishing monteCarloRun! RMS: " + bestRms + " iterations: " + iter);
	return result;
    }

    /** change varCoord such that it closest to constCoord using
     * only tranlation and rotation
     * Instead of a set of vectors, the central superpose method uses startpos direction pairs
     */
    public SuperpositionResult superpose(Vector3D[] constCoord,
					 Vector3D[] constDirs,
					 Vector3D[] varCoord,
					 Vector3D[] varDirs) {
	assert constCoord.length == varCoord.length;
	Vector3D varCoordOrig = null;

	try{ 
	    Vector3D debugPos = new Vector3D(varCoord[1]); 
	}
	catch( ArrayIndexOutOfBoundsException e ) { //happens if junction of order 1 in graph
	    Vector3D debugPos = new Vector3D(varCoord[0]);
	}

	Vector3D shift1 = Vector3DTools.computeCenterGravity(constCoord);
	Vector3D shift2 = Vector3DTools.computeCenterGravity(varCoord);

	// log.fine("using shifts: " + shift1 + " " + shift2);
	for (int i = 0; i < constCoord.length; ++i) {
	    constCoord[i].sub(shift1);
	    varCoord[i].sub(shift2);
	    // log.fine("Shifted: " + (i+1) + " " + constCoord[i] + " " + varCoord[i]);
	}
	SuperpositionResult result = monteCarloRun(constCoord, constDirs, varCoord, varDirs);
	for (int i = 1; i < numTrial; ++i) { //is never used because numTrial always equals 1
	    // log.fine("Superposition trial: " + (i+1));
	    SuperpositionResult newResult = monteCarloRun(constCoord, constDirs, varCoord, varDirs);
	    if (newResult.getRms() < result.getRms()) {
		result = newResult;
	    }
	}
	result.setShift1(shift1.plus(result.getShift1()));
	result.setShift2(shift2.plus(result.getShift2()));
	for (int i = 0; i < constCoord.length; ++i) {
	    constCoord[i].add(shift1);
	    result.applyTransformation(varCoord[i]);
	}
	return result;
    }

    public void setAngleWeight(double val) { this.angleWeight = val; }

    public void setAngleLimit(double x) { this.angleLimit = x; }

    public void setDistanceLimit(double x) { this.distanceLimit = x; }

}
