/**************************************************************************
 *
 * Class:           Matrix3D
 * 
 * Base class:      -
 *
 * Derived classes: - 
 *
 * Author:          Eckart Bindewald
 *
 * Description:     3x3 Matrix.
 *
 */

package tools3d;

/**
 * 3D Matrix class.
 */
public class Matrix3D implements java.io.Serializable {

    public static final String SPACE = " ";
    public static final String NEWLINE = System.getProperty("line.separator");

    /* row vectors */
    Vector3D xRow;
    Vector3D yRow;
    Vector3D zRow;
    
    public Matrix3D() {    
// 	xRow = new Vector3D();
// 	yRow = new Vector3D();
// 	zRow = new Vector3D();

	setXRow(0, 0, 0);
	setYRow(0, 0, 0);
	setZRow(0, 0, 0);
    }

    public Matrix3D(double diagonal)  {
// 	xRow = new Vector3D();
// 	yRow = new Vector3D();
// 	zRow = new Vector3D();
	setXRow(diagonal, 0, 0);
	setYRow(0, diagonal, 0);
	setZRow(0, 0, diagonal);
    }

    /** uses 3x3 rotation sub matrix of homogeneous coordinate matrix */
    public Matrix3D(Matrix4D m) {
	this(m.submatrix());
    }

    public Matrix3D(double diagX, double diagY, double diagZ) {
// 	xRow = new Vector3D();
// 	yRow = new Vector3D();
// 	zRow = new Vector3D();

	setXRow(diagX, 0, 0);
	setYRow(0, diagY, 0);
	setZRow(0, 0, diagZ);
    }

    public Matrix3D(Vector3D newXRow, Vector3D newYRow, Vector3D newZRow) {
	setXRow(newXRow);
	setYRow(newYRow);
	setZRow(newZRow);
    }

    public Matrix3D(double xx, double xy, double xz,
		    double yx, double yy, double yz,
		    double zx, double zy, double zz) {
// 	xRow = new Vector3D();
// 	yRow = new Vector3D();
// 	zRow = new Vector3D();
	setXRow(xx, xy, xz);
	setYRow(yx, yy, yz);
	setZRow(zx, zy, zz);
    }

    /** Create matrix object from 2D array */
    public Matrix3D(double[][] mtx) {
	this(mtx[0][0], mtx[0][1], mtx[0][2],
	     mtx[1][0], mtx[1][1], mtx[1][2],
	     mtx[2][0], mtx[2][1], mtx[2][2]);
    }

    public Matrix3D(Matrix3D other) {
	this();
	copy(other);
    }



    public boolean equals(Matrix3D other) {
	return (getXRow().equals(other.getXRow())) && (getYRow().equals(other.getYRow())) && (getZRow().equals(other.getZRow()));
    }

    /** Matrix is rotation matrix if and only if its determinant is approximately one.
     * TODO : is check for anti-symmetry needed ? */
    public boolean isRotation() {
	return (Math.abs(determinant() - 1.0) < 0.01);
    }

    public Matrix3D multiply(double number) {
	Matrix3D result = new Matrix3D();
	result.getXRow().copy(getXRow().mul(number));
	result.getYRow().copy(getYRow().mul(number));
	result.getZRow().copy(getZRow().mul(number));
	return result; // return this;
    }
    
    public double determinant() {
    	double[][] m = toArray();
    	return m[0][0] * (m[1][1] * m[2][2] - m[1][2] * m[2][1]) -
    	       m[0][1] * (m[1][0] * m[2][2] - m[2][0] * m[1][2]) + 
    	       m[0][2] * (m[1][0] * m[2][1] - m[2][0] * m[1][1]);
    }

    /** TODO: improve */
    public void scale(double _scale) { 
	xRow.scale(_scale);
	yRow.scale(_scale);
	zRow.scale(_scale);
    }

    /** used */
    public Vector3D multiply(Vector3D vect) {
	Vector3D result = new Vector3D();
	result.setX(getXX() * vect.getX() + 
		    getXY() * vect.getY() + 
		    getXZ() * vect.getZ());
	result.setY(getYX() * vect.getX() + 
		    getYY() * vect.getY() + 
		    getYZ() * vect.getZ());
	result.setZ(getZX() * vect.getX() + 
		    getZY() * vect.getY() +
		    getZZ() * vect.getZ());
	return result;
    }

    /** used */
    public Matrix3D multiply(Matrix3D other) { 
	Matrix3D result = new Matrix3D();
	
	result.setXX(getXX() * other.getXX() + 
		     getXY() * other.getYX() + 
		     getXZ() * other.getZX());
	result.setXY(getXX() * other.getXY() + 
		     getXY() * other.getYY() + 
		     getXZ() * other.getZY());
	result.setXZ(getXX() * other.getXZ() + 
		     getXY() * other.getYZ() + 
		     getXZ() * other.getZZ());
	
	result.setYX(getYX() * other.getXX() +
		     getYY() * other.getYX() +
		     getYZ() * other.getZX());
	result.setYY(getYX() * other.getXY() + 
		     getYY() * other.getYY() + 
		     getYZ() * other.getZY());
	result.setYZ(getYX() * other.getXZ() + 
		     getYY() * other.getYZ() + 
		     getYZ() * other.getZZ());
	
	result.setZX(getZX() * other.getXX() + 
		     getZY() * other.getYX() + 
		     getZZ() * other.getZX());
	result.setZY(getZX() * other.getXY() + 
		     getZY() * other.getYY() + 
		     getZZ() * other.getZY());
	result.setZZ(getZX() * other.getXZ() + 
		     getZY() * other.getYZ() + 
		     getZZ() * other.getZZ());
	
	return result;
    }

    public Matrix3D plus(Matrix3D other) {
	Matrix3D result = new Matrix3D();
	result.setXRow(getXRow().plus(other.getXRow()));
	result.setYRow(getYRow().plus(other.getYRow()));
	result.setZRow(getZRow().plus(other.getZRow()));
	return result;
    }

    public Matrix3D minus(Matrix3D other) {
	Matrix3D result = new Matrix3D();
	result.setXRow(getXRow().minus(other.getXRow()));
	result.setYRow(getYRow().minus(other.getYRow()));
	result.setZRow(getZRow().minus(other.getZRow()));
	return result;
    }
  
    /** Returns norm of matrix, defined here as the sum of its squared elements */
    public double norm() {
	double squareSum = getXRow().lengthSquare()
	    + getYRow().lengthSquare()
	    + getZRow().lengthSquare();
	return Math.sqrt(squareSum);
    }

    public Matrix3D add(Matrix3D other) {
	xRow.add(other.getXRow());
	yRow.add(other.getYRow());
	zRow.add(other.getZRow());
	return this;
    }
    
    public Matrix3D subtract(Matrix3D other) {
	xRow.sub(other.getXRow());
	yRow.sub(other.getYRow());
	zRow.sub(other.getZRow());
	return this;
    }

    /** returns a 3x3 array */
    double[][] toArray() {
	double[][] result = new double[3][3];
	result[0][0] = getXX();
	result[0][1] = getXY();
	result[0][2] = getXZ();
	result[1][0] = getYX();
	result[1][1] = getYY();
	result[1][2] = getYZ();
	result[2][0] = getZX();
	result[2][1] = getZY();
	result[2][2] = getZZ();
	return result;
    }

    /** returns trace of matrix */
    public double trace() {
	return getXX() + getYY() + getZZ();
    }

    /** returns transposed of matrix */
    public Matrix3D transposed() {
	return new Matrix3D(getXX(), getYX(), getZX(),
			    getXY(), getYY(), getZY(),
			    getXZ(), getYZ(), getZZ());
			    
    }
    
    public Vector3D getXRow() { return xRow; }
    public Vector3D getYRow() { return yRow; }
    public Vector3D getZRow() { return zRow; }

    public double getXX() { return getXRow().getX(); }
    public double getXY() { return getXRow().getY(); }
    public double getXZ() { return getXRow().getZ(); }
    public double getYX() { return getYRow().getX(); }
    public double getYY() { return getYRow().getY(); }
    public double getYZ() { return getYRow().getZ(); }
    public double getZX() { return getZRow().getX(); }
    public double getZY() { return getZRow().getY(); }
    public double getZZ() { return getZRow().getZ(); }
    
    public void setXX(double xx) { xRow.setX(xx); }
    public void setXY(double xy) { xRow.setY(xy); }
    public void setXZ(double xz) { xRow.setZ(xz); }
    public void setYX(double yx) { yRow.setX(yx); }
    public void setYY(double yy) { yRow.setY(yy); }
    public void setYZ(double yz) { yRow.setZ(yz); }
    public void setZX(double zx) { zRow.setX(zx); }
    public void setZY(double zy) { zRow.setY(zy); }
    public void setZZ(double zz) { zRow.setZ(zz); }

    public String toString() {
	String result = "(Matrix3D " + NEWLINE
	    + getXX() + SPACE + getXY() + SPACE + getXZ() + NEWLINE 
	    + getYX() + SPACE + getYY() + SPACE + getYZ() + NEWLINE
	    + getZX() + SPACE + getZY() + SPACE + getZZ() + SPACE + ")" + SPACE;
	return result;
    }    

    /** creates rotation matrix: rotation around vector vect, angle alpha */
    public static Matrix3D rotationMatrix(Vector3D vect, double alpha) {
	
	Matrix3D R = new Matrix3D(1.0); // unit matrix
	
	if ((vect.lengthSquare() == 0.0) || (alpha == 0.0)) {
	    return R;
	}
	
	vect.normalize();
	
	Vector3D vectSin = vect.mul(Math.sin(alpha));
	
	double xsin = vectSin.getX();
	double ysin = vectSin.getY();
	double zsin = vectSin.getZ();
	Matrix3D S = new Matrix3D(    0, -zsin,  ysin, 
				   zsin,     0, -xsin,
				  -ysin,  xsin,     0);
	
	double x = vect.getX();
	double y = vect.getY();
	double z = vect.getZ();
	Matrix3D U = new Matrix3D(x * x, x * y, x * z,
				  y * x, y * y, y * z,
				  z * x, z * y, z * z);
	
	R.subtract(U);
	R.scale(Math.cos(alpha));
	R.add(U);
	R.add(S);
	
	return R;
    }

    /* MODIFIER */
    
    /** deep copy */
    public void copy(Matrix3D other) {
	if (getXRow() == null) {
	    setXRow(new Vector3D());
	}
	xRow.copy(other.getXRow());
	if (getYRow() == null) {
	    setYRow(new Vector3D());
	}
	yRow.copy(other.getYRow());
	if (getZRow() == null) {
	    setZRow(new Vector3D());
	}
	zRow.copy(other.getZRow());
    }    

    /** deep copy */
    public void copy(Vector3D newXRow, Vector3D newYRow, Vector3D newZRow) {
	xRow.copy(newXRow);
	yRow.copy(newYRow);
	zRow.copy(newZRow);
    }    

    public void setXRow(Vector3D x) { this.xRow = x; }
    public void setXRow(double x, double y, double z) { setXRow(new Vector3D(x, y, z)); }
    public void setYRow(Vector3D y) { this.yRow = y; }
    public void setYRow(double x, double y, double z) { setYRow(new Vector3D(x, y, z)); }
    public void setZRow(Vector3D z) { this.zRow = z; }
    public void setZRow(double x, double y, double z) { setZRow(new Vector3D(x, y, z)); }

    public void set(Vector3D x, Vector3D y, Vector3D z) { this.xRow = x; this.yRow = y; this.zRow = z; }

    /* return outer product of vectors */
    public static Matrix3D outerProduct(Vector3D a, Vector3D b) {
	double xx = a.getX() * b.getX();
	double xy = a.getX() * b.getY();
	double xz = a.getX() * b.getZ();
	double yx = a.getY() * b.getX();
	double yy = a.getY() * b.getY();
	double yz = a.getY() * b.getZ();
	double zx = a.getZ() * b.getX();
	double zy = a.getZ() * b.getY();
	double zz = a.getZ() * b.getZ();
	return new Matrix3D(xx,xy,xz,yx,yy, yz, zx,zy, zz);
    }

}

/*
inline
istream& 
operator >> (istream& is, Matrix3D m)
{
  double x[9];
  string tag1, tag2;

  is >> tag1 
     >> x[0] >> x[1] >> x[2]
     >> x[3] >> x[4] >> x[5]
     >> x[6] >> x[7] >> x[8]
     >> tag2;

  if ((tag1 != "(Matrix3D") || (tag2 != ")")) 
    {
      PRINT_LOCATION;
      DEBUG_MSG("bad input.");
      is.clear(ios::badbit);
    }
  else
    {
      m = Matrix3D(x[0], x[1], x[2],
		   x[3], x[4], x[5],
		   x[6], x[7], x[8]);
    }

  return is;
}

*/




