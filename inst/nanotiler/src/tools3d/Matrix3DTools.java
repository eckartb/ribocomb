package tools3d;

import java.util.Random;
import generaltools.Randomizer;
import org.testng.annotations.Test;

public class Matrix3DTools {

    public static final double PI2 = 2.0 * Math.PI;
    private static Random rnd = Randomizer.getInstance();
    
    /** Describe rotation as 4d quaternion : http://en.wikipedia.org/wiki/Quaternions_and_spatial_rotation */
    public static Vector4D extractQuaternion(Matrix3D m) {
	int[] perm = new int[3];
	perm[0] = 0;
	perm[1] = 1;
	perm[2] = 2;
	// find largest diagonal element:
	int largest = 0;
	double largestVal = m.getXX();
	if (m.getYY()  > largestVal) {
	    largest = 1;
	    largestVal = m.getYY();
	}
	if (m.getZZ()  > largestVal) {
	    largest = 2;
	    largestVal = m.getZZ();
	}
	if ( largest == 1) {
	    perm[0] = 1; // YY is largest
	    perm[1] = 2;
	    perm[2] = 0;
	}
	else if ( largest == 2) {
	    perm[0] = 2; // ZZ is largest
	    perm[1] = 0;
	    perm[2] = 1;
	}
	double[][] ary = m.toArray();
	double Quu = ary[perm[0]][perm[0]];
	double Quv = ary[perm[0]][perm[1]];
	double Quw = ary[perm[0]][perm[2]];

	double Qvu = ary[perm[1]][perm[0]];
	double Qvv = ary[perm[1]][perm[1]];
	double Qvw = ary[perm[1]][perm[2]];

	double Qwu = ary[perm[2]][perm[0]];
	double Qwv = ary[perm[2]][perm[1]];
	double Qww = ary[perm[2]][perm[2]];

	double rsq = 1 + Quu - Qvv - Qww;
	if (rsq <= 0) {
	    return new Vector4D(1.0, 0.0, 0.0, 0.0); // special case of identity matrix
	}
	double r = Math.sqrt(rsq);
	double q0 = (Qwv - Qvw) / (2*r);
	double qu = r/2.0;
	double qv = (Quv + Qvu) / (2*r);
	double qw = (Qwu + Quw) / (2*r);
	return new Vector4D(q0, qu, qv, qw);
    }

    /* return 3D matrix from Euler angles
       rotation around x-axis: w
       rotation around y-axis: a
       rotation around z-axis: k
       taken from: Ronald Azuma 1991 (collinear.pdf). Check again from other source!!!
    */
    public static Matrix3D computeEulerRotationMatrix(double w, double a, double k)
    {
	Matrix3D m = new Matrix3D(
		   Math.cos(k)*Math.cos(a),-Math.sin(k)*Math.cos(w)+Math.cos(k)*Math.sin(a)*Math.sin(w),Math.sin(k)*Math.sin(w)+Math.cos(w)*Math.cos(k)*Math.sin(a),
		   Math.sin(k)*Math.cos(a),Math.cos(k)*Math.cos(w)+Math.sin(w)*Math.sin(k)*Math.sin(a),-Math.sin(w)*Math.cos(k)+Math.cos(w)*Math.sin(k)*Math.sin(a),
		   -Math.sin(a),Math.sin(w)*Math.cos(a),Math.cos(w)*Math.cos(a) );
	return m;
    }

    /** generates rotation matrix : rotation around random direction with random angle */
    public static Matrix3D generateRandomRotationMatrix() {
	double phi = PI2 * rnd.nextDouble();
	double theta = Math.PI * rnd.nextDouble();
	double angle = PI2 * rnd.nextDouble();
	Vector3D axis = Vector3DTools.generateCartesianFromPolar(phi, theta, 1.0);
	Matrix3D rotationMatrix = Matrix3D.rotationMatrix(axis, angle);
	return rotationMatrix;
    }

    /** project point onto line define by direction dir and offset offs. Dir must be normalized (have length 1)! */
    public static double project(Vector3D point, Vector3D dir, Vector3D offs) {
	// pivot = offs + alpha * dir
	// dir.dot(point-pivot) = 0;
	// dir.dot(point- offs - alpha * dir) = 0;
	// Vector3D v0 = point.minus(offs);
	// dir.dot(v0 - alpha * dir) = 0;
	// dir.dot(v0) - alpha * dir.lengthSquare() = 0;
	return dir.dot(point.minus(offs));
    }

    public static Matrix3D matrixProduct(Vector3D a, Vector3D b) {
	double xx = a.getX() * b.getX();
	double xy = a.getX() * b.getY();
	double xz = a.getX() * b.getZ();
	double yx = a.getY() * b.getX();
	double yy = a.getY() * b.getY();
	double yz = a.getY() * b.getZ();
	double zx = a.getZ() * b.getX();
	double zy = a.getZ() * b.getY();
	double zz = a.getZ() * b.getZ();
	return new Matrix3D(xx,xy,xz,yx,yy, yz, zx,zy, zz);
    }
  
    /** Returns (new) rotated vector. rotate around axis vector  */
    public static Vector3D rotate(Vector3D v, Vector3D axis, double angle) { 
	return (rotationMatrix(axis, angle)).multiply(v); 
    }


    @Test
    public void testRotate() {
	Vector3D xVec = new Vector3D(1,0,0);
	Vector3D yVec = new Vector3D(0,1,0);
	Vector3D zVec = new Vector3D(0,0,1);
	double ang = 0.5 * Math.PI;
	Vector3D result = rotate(xVec, zVec, ang);
	assert(result.similar(yVec));
    }

    /** Returns (new) rotated vector. rotate around axis vector including offset */
    public static Vector3D rotate(Vector3D x, Vector3D axis, double angle,
				  Vector3D axisOffset) { 
	Vector3D result = (rotationMatrix(axis, angle)).multiply(x.minus(axisOffset)); 
	return result.plus(axisOffset);
    }

    /*
    public static Matrix3D inverse(Matrix3D m) {
	Matrix3D result = new Matrix3D(1.0);
	Matrix3D src = new Matrix3D(m);
	// inverse (gauss algorithm)
	double pivot, newPivot;
	pivot = fabs(src.x.getX());
	if ((newPivot = fabs(src.y.getX())) > pivot) { pivot = newPivot; swap(src.y,src.x); swap(result.y,result.x); }
	if ((newPivot = fabs(src.z.getX())) > pivot) { pivot = newPivot; swap(src.z,src.x); swap(result.z,result.x); }
	// now: x.x is biggest element in first row
	if (pivot == 0) return result;
	pivot = 1/src.x.getX();
	result.x *= pivot; src.x *= pivot;
	result.y -= result.x * src.y.getX(); src.y -= src.x * src.y.getX();
	result.z -= result.x * src.z.getX(); src.z -= src.x * src.z.getX();
	// now: first row is (1, 0, 0)
	pivot = fabs(src.y.getY());
	if ((newPivot = fabs(src.z.getY())) > pivot) { pivot = newPivot; swap(src.z,src.y); swap(result.z,result.y); }
	// now: y.y is biggest element in second row
	if (pivot == 0) return result;
	pivot = 1/src.y.getY();
	result.y *= pivot; src.y *= pivot;
	result.z -= result.y * src.z.getY(); src.z -= src.y * src.z.getY();
	// now: second row is (?, 1, 0)
	if (src.z.getZ() == 0) return result;
	pivot = 1/src.z.getZ();
	result.z *= pivot; src.z *= pivot;
	// now: third row is (?, ?, 1)
	result.y -= result.z * src.y.getZ(); src.y -= src.z * src.y.getZ(); 
	result.x -= result.z * src.x.getZ(); src.x -= src.z * src.x.getZ(); 
	// now: third row is (0, 0, 1, 0)
	result.x -= result.y * src.x.getY(); 
	// now: second row is (0, 1, 0, 0)
	return result;
    }
    */

    /** returns rotation matrix around vector n with angle alpha */
    public static Matrix3D rotationMatrix(Vector3D n, double alpha) {
	Matrix3D R = new Matrix3D(1.0); // unit matrix
	Matrix3D S = new Matrix3D();
	Matrix3D U = new Matrix3D();
	if ((n.getX() == 0) && (n.getY() == 0) && (n.getZ() == 0)) {
	    return R;
	}
	n.normalize();
	Vector3D nSin = n.mul(Math.sin(alpha));
	S.setXX(0.0);    S.setXY(-nSin.getZ()); S.setXZ(nSin.getY()); 
	S.setYX(nSin.getZ());  S.setYY(0.0);    S.setYZ(-nSin.getX());
	S.setZX(-nSin.getY()); S.setZY(nSin.getX());  S.setZZ(0.0);      
	
	U.setXX(n.getX()*n.getX()); U.setXY(n.getX()*n.getY()); U.setXZ(n.getX()*n.getZ());
	U.setYX(n.getY()*n.getX()); U.setYY(n.getY()*n.getY()); U.setYZ(n.getY()*n.getZ());
	U.setZX(n.getZ()*n.getX()); U.setZY(n.getZ()*n.getY()); U.setZZ(n.getZ()*n.getZ());
	
	// R -= U;
	R.subtract(U);

	R.scale(Math.cos(alpha)); 
	// R += U;
	R.add(U);
	R.add(S);
	
	return R;
    }

    public static Matrix3D transpose(Matrix3D m) {
	Matrix3D result = new Matrix3D();
	result.setXX(m.getXX());
	result.setXY(m.getYX());
	result.setXZ(m.getZX());
	
	result.setYX(m.getXY());
	result.setYY(m.getYY());
	result.setYZ(m.getZY());
	
	result.setZX(m.getXZ());
	result.setZY(m.getYZ());
	result.setZZ(m.getZZ());
	
	return result;
    }
    
    /** generates J1, J2 and J3 defined by Jj[ik] = epsilon(i,j,k)
     * remember: j has values between 1 and 3 ! */
    public static Matrix3D generateJMatrix(int j) {
	PermutationSymbol3 p = new PermutationSymbol3();
	return new Matrix3D(p.epsilon(1,j,1), p.epsilon(1,j,2), p.epsilon(1,j,3),
			    p.epsilon(2,j,1), p.epsilon(2,j,2), p.epsilon(2,j,3),
			    p.epsilon(3,j,1), p.epsilon(3,j,2), p.epsilon(3,j,3));
			    
    }

    /** returns 4D array with first 3 elements being rotation axis
     * and 4th element being rotation angle
     * @see http://www.mathworks.com/access/helpdesk/help/toolbox/physmod/mech/mech_review7.html
     */
    public static double[] convertRotationMatrixToAxisAngle(Matrix3D R) {
	double trace = R.trace();
	double term1 = Math.sqrt(trace + 1.0);
	double denom = term1 * Math.sqrt(3.0-trace);
	double[] result = new double[4];
	result[3] = 2.0 * Math.acos(0.5 * term1); // angle
	for (int j = 0; j < 3; ++j) {
	    Matrix3D jMatrix = generateJMatrix(j+1); // counting of j-matrices starts at 1
	    Matrix3D jTimesR = jMatrix.multiply(R); // J*R
	    result[j] = jTimesR.trace()/denom; 
	}
	return result;
    }

}
