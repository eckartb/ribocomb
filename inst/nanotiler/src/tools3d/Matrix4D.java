package tools3d;


/*
 * 4x4 Matrix, mostly used to implement rotations and translations 
 * in homogeneous coordinates.
 */
public class Matrix4D implements java.io.Serializable, Comparable<Matrix4D> {

    public static final String SPACE = " ";
    public static final String NEWLINE = System.getProperty("line.separator");
    public static final Matrix4D I4 = new Matrix4D(1.0); // 4D identity matrix
    public static final Matrix4D Z4 = new Matrix4D(0.0); // 4D zero matrix

    /* row vectors */
    Vector4D xRow;
    Vector4D yRow;
    Vector4D zRow;
    Vector4D wRow;
    
    public Matrix4D() {    
	xRow = new Vector4D();
	yRow = new Vector4D();
	zRow = new Vector4D();
	wRow = new Vector4D();
    }

    public Matrix4D(double diagonal)  {
	this();
	setXRow(diagonal, 0.0, 0.0,0.0);
	setYRow(0.0, diagonal, 0.0,0.0);
	setZRow(0.0, 0.0, diagonal,0.0);
	setWRow(0.0, 0.0, 0.0, diagonal);
    }

    public Matrix4D(Vector4D newXRow, Vector4D newYRow, Vector4D newZRow, Vector4D newWRow) {
	this();
	setXRow(newXRow);
	setYRow(newYRow);
	setZRow(newZRow);
	setWRow(newWRow);
    }

    public Matrix4D(double xx, double xy, double xz, double xw,
		    double yx, double yy, double yz, double yw,
		    double zx, double zy, double zz, double zw,
		    double wx, double wy, double wz, double ww) {
	this();
	setXRow(xx, xy, xz, xw);
	setYRow(yx, yy, yz, yw);
	setZRow(zx, zy, zz, zw);
	setWRow(wx, wy, wz, ww);
    }

    public Matrix4D(Matrix4D other) {
	this();
	copy(other);
    }

    public Matrix4D(Matrix3D other) {
	this();
	copy(other);
    }

    /** embeds rotation and translation into 4D matrix */
    public Matrix4D(Matrix3D rotation, Vector3D translation) {
	this();
	copy(rotation);
	setXW(translation.getX());
	setYW(translation.getY());
	setZW(translation.getZ());
	setWW(1.0);
    }

    /** Generates matrix that correspons to coordinate system at origin */
    public static Matrix4D generateUnitMatrix4D() {
	return new Matrix4D(new Matrix3D(1.0), new Vector3D(0.0, 0.0, 0.0));
    }


    /** Compares to 4D matrices using their norm */
    public int compareTo(Matrix4D other) {
	// Matrix4D other = (Matrix4D)(o);
	double s1 = norm();
	double s2 = other.norm();
	if (s1 < s2) {
	    return -1;
	} else if (s1 > s2) {
	    return 1;
	}
	return 0; // same norm
    }

    public boolean equals(Matrix4D other) {
	return (getXRow().equals(other.getXRow())) 
	    && (getYRow().equals(other.getYRow())) 
	    && (getZRow().equals(other.getZRow()))
	    && (getWRow().equals(other.getWRow()));
    }

    /** Generates transformation with opposite translation and rotation.
     * careful: is not real inversion of transformation. Only use in special circumstances. Use method "inverse" in most cases. */
    public Matrix4D homogeneousInverse() {
	Matrix3D rotation = submatrix();
	Vector3D translation = subvector();
	return new Matrix4D(rotation.transposed(), translation.mul(-1.0));
    }

    /**  Returns inverse of matrix */
    public Matrix4D inverse() {
	Matrix4D result = new Matrix4D(1.0);
	Matrix4D src = new Matrix4D(this);
	// inverse (gauss algorithm)
	double pivot, newPivot;
	pivot = Math.abs(src.getXRow().getX());
	if ((newPivot = Math.abs(src.getYRow().getX())) > pivot) { 
	    pivot = newPivot; 
	    src.getYRow().swap(src.getXRow()); 
	    result.getYRow().swap(result.getXRow()); 
	}
	if ((newPivot = Math.abs(src.getZRow().getX())) > pivot) { 
	    pivot = newPivot; 
	    src.getZRow().swap(src.getXRow()); 
	    result.getZRow().swap(result.getXRow()); 
	}
	if ((newPivot = Math.abs(src.getWRow().getX())) > pivot) { 
	    pivot = newPivot; 
	    src.getWRow().swap(src.getXRow()); 
	    result.getWRow().swap(result.getXRow()); 
	}
	// now: x.x is biggest element in first row
	if (pivot == 0.0) return result;
	pivot = 1/src.getXRow().getX();
	result.getXRow().scale(pivot); src.getXRow().scale(pivot);
	result.getYRow().sub(result.getXRow().mul(src.getYRow().getX())); src.getYRow().sub(src.getXRow().mul(src.getYRow().getX()));
	result.getZRow().sub(result.getXRow().mul(src.getZRow().getX())); src.getZRow().sub(src.getXRow().mul(src.getZRow().getX()));
	result.getWRow().sub(result.getXRow().mul(src.getWRow().getX())); src.getWRow().sub(src.getXRow().mul(src.getWRow().getX()));
	// now: first row is (1, 0, 0, 0)
	pivot = Math.abs(src.getYRow().getY());
	if ((newPivot = Math.abs(src.getZRow().getY())) > pivot) { pivot = newPivot; src.getZRow().swap(src.getYRow()); result.getZRow().swap(result.getYRow()); }
	if ((newPivot = Math.abs(src.getWRow().getY())) > pivot) { pivot = newPivot; src.getWRow().swap(src.getYRow()); result.getWRow().swap(result.getYRow()); }
	// now: y.y is biggest element in second row
	if (pivot == 0.0) return result;
	pivot = 1/src.getYRow().getY();
	result.getYRow().scale(pivot); src.getYRow().scale(pivot);
	result.getZRow().sub(result.getYRow().mul(src.getZRow().getY())); src.getZRow().sub(src.getYRow().mul(src.getZRow().getY()));
	result.getWRow().sub(result.getYRow().mul(src.getWRow().getY())); src.getWRow().sub(src.getYRow().mul(src.getWRow().getY()));
	// now: second row is (?, 1, 0, 0)
	pivot = Math.abs(src.getZRow().getZ());
	if ((newPivot = Math.abs(src.getWRow().getZ())) > pivot) { 
	    pivot = newPivot; 
	    src.getWRow().swap(src.getZRow()); 
	    result.getWRow().swap(result.getZRow()); 
	}
	// now: z.z is biggest element in third row
	if (pivot == 0.0) return result;
	pivot = 1/src.getZRow().getZ();
	result.getZRow().scale(pivot); src.getZRow().scale(pivot);
	result.getWRow().sub(result.getZRow().mul(src.getWRow().getZ())); src.getWRow().sub(src.getZRow().mul(src.getWRow().getZ())); 
	// now: third row is (?, ?, 1, 0)
	if (src.getWRow().getW() == 0.0) return result;
	pivot = 1/src.getWRow().getW();
	result.getWRow().scale(pivot); src.getWRow().scale(pivot);
	// now: forth row is (?, ?, ?, 1)
	result.getZRow().sub(result.getWRow().mul(src.getZRow().w)); src.getZRow().sub(src.getWRow().mul(src.getZRow().getW())); 
	result.getYRow().sub(result.getWRow().mul(src.getYRow().w)); src.getYRow().sub(src.getWRow().mul(src.getYRow().getW())); 
	result.getXRow().sub(result.getWRow().mul(src.getXRow().w)); src.getXRow().sub(src.getWRow().mul(src.getXRow().getW())); 
	// now: forth row is (0, 0, 0, 1)
	result.getYRow().sub(result.getZRow().mul(src.getYRow().getZ())); src.getYRow().sub(src.getZRow().mul(src.getYRow().getZ())); 
	result.getXRow().sub(result.getZRow().mul(src.getXRow().getZ())); src.getXRow().sub(src.getZRow().mul(src.getXRow().getZ())); 
	// now: third row is (0, 0, 1, 0)
	result.getXRow().sub(result.getYRow().mul(src.getXRow().getY())); 
	// now: second row is (0, 1, 0, 0)
	return result;
    }

    /** Matrix corresponds to homogenous coordinates if and only if its 3x3 submatrix is rotation matrix
     * and last row contains row except in last element */
    public boolean isHomogenous() {
	Matrix3D m3 = submatrix();
	Vector3D v3 = getWRow().subVector();
	return (m3.isRotation() && (v3.length() < 0.01));
    }

    /** returns new scaled matrix */
    public Matrix4D multiply(double number) {
	Matrix4D result = new Matrix4D();
	result.getXRow().copy(getXRow().mul(number));
	result.getYRow().copy(getYRow().mul(number));
	result.getZRow().copy(getZRow().mul(number));
	result.getWRow().copy(getWRow().mul(number));
	return result; // return this;
    }

    /** Scale vector with number. */
    public void scale(double _scale) { 
	xRow.scale(_scale);
	yRow.scale(_scale);
	zRow.scale(_scale);
	wRow.scale(_scale);
    }

    /** multiplies matrix with 4D vector */
    public Vector4D multiply(Vector4D vect) {
	Vector4D result = new Vector4D();
	result.setX(getXX() * vect.getX() + 
		    getXY() * vect.getY() + 
		    getXZ() * vect.getZ() + 
		    getXW() * vect.getW());
	result.setY(getYX() * vect.getX() + 
		    getYY() * vect.getY() + 
		    getYZ() * vect.getZ() +
		    getYW() * vect.getW());
	result.setZ(getZX() * vect.getX() + 
		    getZY() * vect.getY() +
		    getZZ() * vect.getZ() +
		    getZW() * vect.getW());
	result.setW(getWX() * vect.getX() + 
		    getWY() * vect.getY() +
		    getWZ() * vect.getZ() +
		    getWW() * vect.getW());
	return result;
    }
    
    public double[] getArray() {
    	double[] array = { xRow.getX(), xRow.getY(), xRow.getZ(), xRow.getW(),
    			yRow.getX(), yRow.getY(), yRow.getZ(), yRow.getW(),
    			zRow.getX(), zRow.getY(), zRow.getZ(), zRow.getW(),
    			wRow.getX(), wRow.getY(), wRow.getZ(), wRow.getW()
    	};
    	
    	return array;
    }

    /** multiplies matrix with 4D vector n times */
    public Vector4D multiply(Vector4D vect, int n) {
	Vector4D result = new Vector4D(vect);
	for (int i = 0; i < n; ++i) {
	    result = multiply(result);
	}
	return result;
    }

    /** multiplies matrix with 3D vector embedded in homogeneous coordinate 4D vector */
    public Vector3D multiply(Vector3D vect) {
	return multiply(new Vector4D(vect)).subVector();
    }

    /** multiplies matrix n times with 3D vector embedded in homogeneous coordinate 4D vector */
    public Vector3D multiply(Vector3D vect, int n) {
	return multiply(new Vector4D(vect), n).subVector();
    }

    /** multiplies matrix with 4D matrix */
    public Matrix4D multiply(Matrix4D other) { 
	Matrix4D result = new Matrix4D();
	
	result.setXX(getXX() * other.getXX() + 
		     getXY() * other.getYX() + 
		     getXZ() * other.getZX() + 
		     getXW() * other.getWX());

	result.setXY(getXX() * other.getXY() + 
		     getXY() * other.getYY() + 
		     getXZ() * other.getZY() +
		     getXW() * other.getWY());

	result.setXZ(getXX() * other.getXZ() + 
		     getXY() * other.getYZ() + 
		     getXZ() * other.getZZ() +
		     getXW() * other.getWZ());

	result.setXW(getXX() * other.getXW() + 
		     getXY() * other.getYW() + 
		     getXZ() * other.getZW() +
		     getXW() * other.getWW());
	

	result.setYX(getYX() * other.getXX() +
		     getYY() * other.getYX() +
		     getYZ() * other.getZX() +
		     getYW() * other.getWX());

	result.setYY(getYX() * other.getXY() + 
		     getYY() * other.getYY() + 
		     getYZ() * other.getZY() +
		     getYW() * other.getWY());

	result.setYZ(getYX() * other.getXZ() + 
		     getYY() * other.getYZ() + 
		     getYZ() * other.getZZ() +
		     getYW() * other.getWZ());

	result.setYW(getYX() * other.getXW() + 
		     getYY() * other.getYW() + 
		     getYZ() * other.getZW() +
		     getYW() * other.getWW());
	

	result.setZX(getZX() * other.getXX() + 
		     getZY() * other.getYX() + 
		     getZZ() * other.getZX() +
		     getZW() * other.getWX());
	result.setZY(getZX() * other.getXY() + 
		     getZY() * other.getYY() + 
		     getZZ() * other.getZY() +
		     getZW() * other.getWY());
	result.setZZ(getZX() * other.getXZ() + 
		     getZY() * other.getYZ() + 
		     getZZ() * other.getZZ() +
		     getZW() * other.getWZ());
	result.setZW(getZX() * other.getXW() + 
		     getZY() * other.getYW() + 
		     getZZ() * other.getZW() +
		     getZW() * other.getWW());


	result.setWX(getWX() * other.getXX() + 
		     getWY() * other.getYX() + 
		     getWZ() * other.getZX() +
		     getWW() * other.getWX());
	result.setWY(getWX() * other.getXY() + 
		     getWY() * other.getYY() + 
		     getWZ() * other.getZY() +
		     getWW() * other.getWY());
	result.setWZ(getWX() * other.getXZ() + 
		     getWY() * other.getYZ() + 
		     getWZ() * other.getZZ() +
		     getWW() * other.getWZ());
	result.setWW(getWX() * other.getXW() + 
		     getWY() * other.getYW() + 
		     getWZ() * other.getZW() +
		     getWW() * other.getWW());
	
	return result;
    }

    public Matrix4D plus(Matrix4D other) {
	Matrix4D result = new Matrix4D();
	result.setXRow(getXRow().plus(other.getXRow()));
	result.setYRow(getYRow().plus(other.getYRow()));
	result.setZRow(getZRow().plus(other.getZRow()));
	result.setWRow(getWRow().plus(other.getWRow()));
	return result;
    }

    public Matrix4D minus(Matrix4D other) {
	Matrix4D result = new Matrix4D();
	result.setXRow(getXRow().minus(other.getXRow()));
	result.setYRow(getYRow().minus(other.getYRow()));
	result.setZRow(getZRow().minus(other.getZRow()));
	result.setWRow(getWRow().minus(other.getWRow()));
	return result;
    }
  
    public Matrix4D add(Matrix4D other) {
	xRow.add(other.getXRow());
	yRow.add(other.getYRow());
	zRow.add(other.getZRow());
	wRow.add(other.getWRow());
	return this;
    }

    /** returns n'th power of matrix. */
    public Matrix4D pow(int n) {
	assert n >= 0;
	Matrix4D result = new Matrix4D(1.0);
	for (int i = 0; i < n; ++i) {
	    result = result.multiply(this);
	}
	return result;
    }

    /** returns 3x3 rotation matrix corresponding to rotation in homogenious coordinates */
    public Matrix3D submatrix() {
	return new Matrix3D(getXRow().subVector(), getYRow().subVector(), getZRow().subVector());
    }

    /** returns 3D vector corresponding to translation in homogenious coordinates */
    public Vector3D subvector() {
	return new Vector3D(getXRow().getW(), getYRow().getW(), getZRow().getW());
    }

    public Matrix4D subtract(Matrix4D other) {
	xRow.sub(other.getXRow());
	yRow.sub(other.getYRow());
	zRow.sub(other.getZRow());
	wRow.sub(other.getWRow());
	return this;
    }

    /** returns trace of matrix */
    public double trace() {
	return getXX() + getYY() + getZZ() + getWW();
    }

    /** returns transposed of matrix */
    public Matrix4D transposed() {
	return new Matrix4D(getXX(), getYX(), getZX(), getWX(),
			    getXY(), getYY(), getZY(), getWY(),
			    getXZ(), getYZ(), getZZ(), getWZ(),
			    getXW(), getYW(), getZW(), getWW());
			    
    }
    
    public Vector4D getXRow() { return xRow; }
    public Vector4D getYRow() { return yRow; }
    public Vector4D getZRow() { return zRow; }
    public Vector4D getWRow() { return wRow; }

    public double getXX() { return getXRow().getX(); }
    public double getXY() { return getXRow().getY(); }
    public double getXZ() { return getXRow().getZ(); }
    public double getXW() { return getXRow().getW(); }
    public double getYX() { return getYRow().getX(); }
    public double getYY() { return getYRow().getY(); }
    public double getYZ() { return getYRow().getZ(); }
    public double getYW() { return getYRow().getW(); }
    public double getZX() { return getZRow().getX(); }
    public double getZY() { return getZRow().getY(); }
    public double getZZ() { return getZRow().getZ(); }
    public double getZW() { return getZRow().getW(); }
    public double getWX() { return getWRow().getX(); }
    public double getWY() { return getWRow().getY(); }
    public double getWZ() { return getWRow().getZ(); }
    public double getWW() { return getWRow().getW(); }
    
    public void setXX(double xx) { xRow.setX(xx); }
    public void setXY(double xy) { xRow.setY(xy); }
    public void setXZ(double xz) { xRow.setZ(xz); }
    public void setXW(double xw) { xRow.setW(xw); }
    public void setYX(double yx) { yRow.setX(yx); }
    public void setYY(double yy) { yRow.setY(yy); }
    public void setYZ(double yz) { yRow.setZ(yz); }
    public void setYW(double yw) { yRow.setW(yw); }
    public void setZX(double zx) { zRow.setX(zx); }
    public void setZY(double zy) { zRow.setY(zy); }
    public void setZZ(double zz) { zRow.setZ(zz); }
    public void setZW(double zw) { zRow.setW(zw); }
    public void setWX(double wx) { wRow.setX(wx); }
    public void setWY(double wy) { wRow.setY(wy); }
    public void setWZ(double wz) { wRow.setZ(wz); }
    public void setWW(double ww) { wRow.setW(ww); }

    public String toString() {
	String result = "(Matrix4D " + NEWLINE
	    + getXX() + SPACE + getXY() + SPACE + getXZ() + SPACE + getXW() + NEWLINE 
	    + getYX() + SPACE + getYY() + SPACE + getYZ() + SPACE + getYW() + NEWLINE
	    + getZX() + SPACE + getZY() + SPACE + getZZ() + SPACE + getZW() + NEWLINE
	    + getWX() + SPACE + getWY() + SPACE + getWZ() + SPACE + getWW() + NEWLINE
	    + ")" + SPACE;
	return result;
    }    

    /** returns square root of sum of square of elements */
    public double norm() {
	return Math.sqrt(xRow.lengthSquare() + yRow.lengthSquare() + zRow.lengthSquare()
			 + wRow.lengthSquare());
    }

    /** Copies all matrix elements. */
    public void copy(Matrix4D other) {
	if (getXRow() == null) {
	    setXRow(new Vector4D());
	}
	xRow.copy(other.getXRow());
	if (getYRow() == null) {
	    setYRow(new Vector4D());
	}
	yRow.copy(other.getYRow());
	if (getZRow() == null) {
	    setZRow(new Vector4D());
	}
	zRow.copy(other.getZRow());
	if (getWRow() == null) {
	    setWRow(new Vector4D());
	}
	wRow.copy(other.getWRow());
    }    

    /** Embedding rotation matrix into 4D matrix */
    public void copy(Matrix3D other) {
	xRow.copy(other.getXRow());
	xRow.setW(0.0);
	yRow.copy(other.getYRow());
	yRow.setW(0.0);
	zRow.copy(other.getZRow());
	zRow.setW(0.0);
	wRow.set(0.0, 0.0, 0.0, 1.0);
    }    

    /** deep copy */
    public void copy(Vector4D newXRow, Vector4D newYRow, Vector4D newZRow, Vector3D newWRow) {
	xRow.copy(newXRow);
	yRow.copy(newYRow);
	zRow.copy(newZRow);
	wRow.copy(newWRow);
    }    

    public void setXRow(Vector4D x) { this.xRow = new Vector4D(x); }
    public void setXRow(double x, double y, double z, double w) { xRow.set(x, y, z, w); }

    public void setYRow(Vector4D y) { this.yRow = new Vector4D(y); }
    public void setYRow(double x, double y, double z, double w) { yRow.set(x, y, z, w); }

    public void setZRow(Vector4D z) { this.zRow = new Vector4D(z); }
    public void setZRow(double x, double y, double z, double w) { zRow.set(x, y, z, w); }

    public void setWRow(Vector4D w) { this.wRow = new Vector4D(w); }
    public void setWRow(double x, double y, double z, double w) { wRow.set(x, y, z, w); }

}




