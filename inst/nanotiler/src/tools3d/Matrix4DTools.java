package tools3d;

import generaltools.Randomizer;
import java.util.Properties;
import java.util.Random;
import java.util.logging.*;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Matrix4DTools {

    private static Random rnd = Randomizer.getInstance();
    private static Logger log = Logger.getLogger(PackageConstants.LOGFILE_DEFAULT);

    /** Generates hash string for homogeneous coordinate matrix. Represents rotation as 4D quaternions. */
    public static String computeEpsilonHash(Matrix4D m4, double epsilonRot, double epsilonTrans) {
	// extract rotation matrix; thenquaternion:
	Matrix3D m3 = m4.submatrix();
	Vector3D v3 = m4.subvector();
	Vector4D quat = Matrix3DTools.extractQuaternion(m3); // or use conjugate?
	return Vector3DTools.computeEpsilonHash(quat,epsilonRot) + Vector3DTools.HASH_DELIM + Vector3DTools.computeEpsilonHash(v3, epsilonTrans);
    }
    /** computes approximate root of matrix, assuming homogeneous coordinates */
    public static Matrix4D computeRoot(Matrix4D matrix, double error) {
	return computeRoot(matrix, 2, error);
    }

    private static double computeRootScore(Matrix3D rotMatrix, Vector3D translation, Matrix4D matrix, int power) {
	Matrix4D m = new Matrix4D(rotMatrix, translation);
	Matrix4D finalM = m.pow(power);
	double error = finalM.minus(matrix).norm();
	return error;
    }

    public static double mutate(double x, double step){
	return x + step * rnd.nextGaussian();
    }

    private static void mutateHomogeneousVector(double[] v, double translationStep, double angleStep) {
	v[0] = mutate(v[0], translationStep);
	v[1] = mutate(v[1], translationStep);
	v[2] = mutate(v[2], translationStep);
	v[3] = mutate(v[3], angleStep);
	v[4] = mutate(v[4], angleStep);
	v[5] = mutate(v[5], angleStep);
    }

    private static double computeRootScore(double[] v, Matrix4D matrix, int power) {
	Vector3D translation = new Vector3D(v[0], v[1], v[2]);
	Matrix3D rotMatrix = Matrix3DTools.computeEulerRotationMatrix(v[3], v[4], v[5]);
	return computeRootScore(rotMatrix, translation, matrix, power);
    }

    /** generates 4D matrix from vector consisting of 3 translation and 3 rotation values */
    private static Matrix4D generateMatrixFromVector(double[] v) {
	assert v.length == 6;
	Vector3D translation = new Vector3D(v[0], v[1], v[2]);
	Matrix3D rotMatrix = Matrix3DTools.computeEulerRotationMatrix(v[3], v[4], v[5]);
	return new Matrix4D(rotMatrix, translation);
    }

    private static void arrayCopy(double[] src, double[] dest) {
	assert src.length == dest.length;
	for (int i = 0; i < src.length; ++i) {
	    dest[i] = src[i];
	}
    }

    /** computes matrix**(1/n) of matrix, assuming homogeneous coordinates */
    public static Matrix4D computeRoot(Matrix4D matrix, int power, double errorLimit) {
	assert power >= 0;
	int len = 6;
	double[] v = new double[len]; // three euler angles and three translation coordinates
	double[] vBest = new double[len]; // three euler angles and three translation coordinates
	double[] vSave = new double[len]; // three euler angles and three translation coordinates
	if (power == 0) {
	    return new Matrix4D(1.0); // return unity matrix
	}
	if (power == 1) {
	    return new Matrix4D(matrix);
	}
	double error = computeRootScore(v, matrix, power);
	double translationStep = 1.0;
	double angleStep = 0.1;
	double bestError = error;
	int mulStep = 1000;
	double annealingMul = 0.98;
	int stepCount = 1;
	int stepMax = 1000000;
	while (error > errorLimit)  {
	    ++stepCount;
	    if ((stepCount % mulStep) == 0) {
		angleStep *= annealingMul;
		translationStep *= annealingMul;
	    }
	    if (stepCount > stepMax) {
		log.warning("Could not find matrix root for matrix " + matrix + " and error limit: " + errorLimit);
		return null;
	    }
	    // make safety copy:
	    arrayCopy(v, vSave);
	    // mutate vector:
	    mutateHomogeneousVector(v, translationStep, angleStep);
	    error = computeRootScore(v, matrix, power);
	    if (error < bestError) {
		bestError = error; // accept step
		arrayCopy(v, vBest);
		log.fine("New best transformation matrix found at step " + stepCount 
			 + " with error: " + bestError + "  ");
		// + generateMatrixFromVector(vBest));
	    }
	    else { // do not accept step
		arrayCopy(vSave, v); // revert to previous vector
	    }
	}
	return generateMatrixFromVector(v);
    }

    public static Matrix4D generateRandomHomogeneousMatrix(double translationStep, double angleStep) {
	double[] v = new double[6];
	mutateHomogeneousVector(v, translationStep, angleStep);
	return generateMatrixFromVector(v);
    }

    /** tests computing of root of homogeneous coordinate matrix.
     * Currently not active, because this can be done MUCH better using quaternion formalism.
     */
    // @Test
    public void testComputeRoot() {
	int numTest = 3;
	double errorLimit = 0.02;
	for (int power = 2; power <= 5; ++power) {
	    for (int i = 0; i < numTest; ++i) {
		Matrix4D m = generateRandomHomogeneousMatrix(5.0, Math.PI);
		Matrix4D result = computeRoot(m, power, errorLimit);
		assert result != null;
		log.info("Found root " + power + " solution: " + result.toString() 
			 + " test result for " + m + " is: " + result.pow(power));
		assert result.pow(power).minus(m).norm() <= errorLimit;
	    }
	}
	
    }

    /** returns transformation that takes an orientation (startOrientation) and transforms it to endOrientation */
    private static Matrix4D computeTransformation(Matrix4D startOrient, Matrix4D endOrient) {
	return startOrient.inverse().multiply(endOrient);
    }

    public static double scoreFit(Matrix4D target, Matrix4D query) {
	double transDiff = query.subvector().minus(target.subvector()).length();
	double rotationNorm = (query.submatrix().minus(target.submatrix())).norm();
	return transDiff + rotationNorm;
    }

    public static double scoreFit(Matrix4D target, Matrix4D query, double rotationWeight) {
	double transDiff = query.subvector().minus(target.subvector()).length();
	double rotationNorm = (query.submatrix().minus(target.submatrix())).norm();
	return transDiff + (rotationWeight * rotationNorm);
    }
    
    /** Fitting result of 4D transformation.
     * @param score The score of the fit
     * @param nAlt1 Corresponds to length of preceding helix
     * @param nAlt2 Corresponds to length of trailing-helix
     */
    public static Properties generateMatrix4DFitResult(double score, int nAlt1, int nAlt2) {
	Properties result = new Properties();
	result.setProperty("score", "" + score);
	result.setProperty("num_alt1", "" + nAlt1);
	result.setProperty("num_alt2", "" + nAlt2);
	assert result != null;
	return result;
    }

    /** For a given target and query tranformation, it returns the fitting score; In addition
     * it tries out possible combinations of first multiplying the query with altMatrix1, then with altMatrix2.
     * This correponds to a block closing a loop, allowing for n preceding and trailing transformations (like an RNA or DNA helix) 
     */
    public static Properties scoreFit(Matrix4D target, Matrix4D query, Matrix4D altMatrix1, Matrix4D altMatrix2,
				      int alt1Min, int alt1Max, int alt2Min, int alt2Max) {
	assert alt1Min < alt1Max;
	assert alt2Min < alt2Max;
	Properties result = null;
	double bestScore = 1e30;
	
	for (int i = alt1Min; i < alt1Max; ++i) {
	    Matrix4D m1 = altMatrix1.pow(i);
	    for (int j = alt2Min; j < alt2Max; ++j) {
		Matrix4D m2 = altMatrix2.pow(j);
		Matrix4D m = m1.multiply(query).multiply(m2);
		double score = scoreFit(target, m);
		if (score < bestScore) {
		    result = generateMatrix4DFitResult(score, i, j);
		    bestScore = score;
		}
	    }
	}
	assert result != null;
	return result;
    }

    /** Returns interpolated translation and rotation. TODO: does not work properly!!! Use quaternions! */
    public static Matrix4D computeAverageTransformation(Matrix4D m1, Matrix4D m2) {
	Vector3D v1 = m1.subvector();
	Vector3D v2 = m2.subvector();
	Vector3D avgTrans = Vector3D.average(v1, v2);
	AxisAngle axisAngle1 = new AxisAngle(m1.submatrix());
	AxisAngle axisAngle2 = new AxisAngle(m2.submatrix());
	double avgAngle = 0.5 * (axisAngle1.getAngle() + axisAngle2.getAngle());
	Vector3D avgAxis = Vector3D.average(axisAngle1.getAxis(),axisAngle2.getAxis());
	Matrix3D avgRot = (new AxisAngle(avgAngle, avgAxis)).generateMatrix();
	return new Matrix4D(avgRot, avgTrans);
    }

    /** Returns interpolated translation and rotation. TODO: does not work properly!!! Use quaternions! */
    public static Matrix4D computeAverageTransformationQuat(Matrix4D m1, Matrix4D m2) {
	AxisAngle aa1 = new AxisAngle(m1.submatrix());
	AxisAngle aa2 = new AxisAngle(m2.submatrix());
	Vector4D q1 = aa1.toQuaternion();
	Vector4D q2 = aa2.toQuaternion();
	Vector4D qavg = q1.plus(q2); // !???
	qavg.normalize();
	AxisAngle aaResult = AxisAngle.fromQuaternion(qavg);
	Matrix3D newRot = aaResult.generateMatrix();
	Vector3D t1 = m1.subvector();
	Vector3D t2 = m2.subvector();
	Vector3D tAvg = Vector3D.average(t1, t2);
	return new Matrix4D(newRot, tAvg);
    }

    private double ReciprocalSqrt( double x ) {
// 	long i;
// 	double y, r;
// 	y = x * 0.5;
// 	i = *(long *)( &x );
// 	i = 0x5f3759df - ( i >> 1 );
// 	r = *(double *)( &i );
// 	r = r * ( 1.5f - r * r * y );
// 	return r;
	return 1/Math.sqrt(x);
  }

    /* from: http://www.intel.com/cd/ids/developer/asmo-na/eng/293748.htm 
    void ConvertJointMatsToJointQuats( double[] q, double[] m) {
	assert q.length == 8;
	// const double[][] m = jointMats[i].mat;
	if ( m[0 * 4 + 0] + m[1 * 4 + 1] + m[2 * 4 + 2] > 0.0f ) {
	    double t = + m[0 * 4 + 0] + m[1 * 4 + 1] + m[2 * 4 + 2] + 1.0f;
	    double s = ReciprocalSqrt( t ) * 0.5f;
	    q[3] = s * t;
	    q[2] = ( m[0 * 4 + 1] - m[1 * 4 + 0] ) * s;
	    q[1] = ( m[2 * 4 + 0] - m[0 * 4 + 2] ) * s;
	    q[0] = ( m[1 * 4 + 2] - m[2 * 4 + 1] ) * s;
	} else if ( m[0 * 4 + 0] > m[1 * 4 + 1] && m[0 * 4 + 0] > m[2 * 4 + 2] ) {
	    double t = + m[0 * 4 + 0] - m[1 * 4 + 1] - m[2 * 4 + 2] + 1.0f;
	    double s = ReciprocalSqrt( t ) * 0.5f;
	    q[0] = s * t;
	    q[1] = ( m[0 * 4 + 1] + m[1 * 4 + 0] ) * s;
	    Page 6
		q[2] = ( m[2 * 4 + 0] + m[0 * 4 + 2] ) * s;
	    q[3] = ( m[1 * 4 + 2] - m[2 * 4 + 1] ) * s;
	} else if ( m[1 * 4 + 1] > m[2 * 4 + 2] ) {
	    double t = - m[0 * 4 + 0] + m[1 * 4 + 1] - m[2 * 4 + 2] + 1.0f;
	    double s = ReciprocalSqrt( t ) * 0.5f;
	    q[1] = s * t;
	    q[0] = ( m[0 * 4 + 1] + m[1 * 4 + 0] ) * s;
	    q[3] = ( m[2 * 4 + 0] - m[0 * 4 + 2] ) * s;
	    q[2] = ( m[1 * 4 + 2] + m[2 * 4 + 1] ) * s;
	} else {
	    double t = - m[0 * 4 + 0] - m[1 * 4 + 1] + m[2 * 4 + 2] + 1.0f;
	    double s = ReciprocalSqrt( t ) * 0.5f;
	    q[2] = s * t;
	    q[3] = ( m[0 * 4 + 1] - m[1 * 4 + 0] ) * s;
	    q[0] = ( m[2 * 4 + 0] + m[0 * 4 + 2] ) * s;
	    q[1] = ( m[1 * 4 + 2] + m[2 * 4 + 1] ) * s;
	}
	q[4] = m[0 * 4 + 3];
	q[5] = m[1 * 4 + 3];
	q[6] = m[2 * 4 + 3];
	q[7] = 0.0f;
    }
    */

    /*
    void ConvertJointMatsToJointQuats( JointQuat *jointQuats, const JointMat *jointMats, const int numJoints ) {
	for ( int i = 0; i < numJoints; i++ ) {
	    double s0, s1, s2;
	    int k0, k1, k2, k3;
	    double *q = &jointQuats[i].q;
	    const double *m = jointMats[i].mat;
	    if ( m[0 * 4 + 0] + m[1 * 4 + 1] + m[2 * 4 + 2] > 0.0f ) {
		k0 = 3;
		k1 = 2;
		k2 = 1;
		k3 = 0;
		s0 = 1.0f;
		s1 = 1.0f;
		s2 = 1.0f;
	    } else if ( m[0 * 4 + 0] > m[1 * 4 + 1] && m[0 * 4 + 0] > m[2 * 4 + 2] ) {
		k0 = 0;
		k1 = 1;
		k2 = 2;
		k3 = 3;
		s0 = 1.0f;
		s1 = -1.0f;
		s2 = -1.0f;
	    } else if ( m[1 * 4 + 1] > m[2 * 4 + 2] ) {
		Page 7
		    k0 = 1;
		k1 = 0;
		k2 = 3;
		k3 = 2;
		s0 = -1.0f;
		s1 = 1.0f;
		s2 = -1.0f;
	    } else {
		k0 = 2;
		k1 = 3;
		k2 = 0;
		k3 = 1;
		s0 = -1.0f;
		s1 = -1.0f;
		s2 = 1.0f;
	    }
	    double t = s0 * m[0 * 4 + 0] + s1 * m[1 * 4 + 1] + s2 * m[2 * 4 + 2] + 1.0f;
	    double s = ReciprocalSqrt( t ) * 0.5f;
	    q[k0] = s * t;
	    q[k1] = ( m[0 * 4 + 1] - s2 * m[1 * 4 + 0] ) * s;
	    q[k2] = ( m[2 * 4 + 0] - s1 * m[0 * 4 + 2] ) * s;
	    q[k3] = ( m[1 * 4 + 2] - s0 * m[2 * 4 + 1] ) * s;
	    q[4] = m[0 * 4 + 3];
	    q[5] = m[1 * 4 + 3];
	    q[6] = m[2 * 4 + 3];
	    q[7] = 0.0f;
	}
    }
    */



}
