package tools3d;

import java.util.logging.Logger;

public class MinSuperpose implements Superpose {

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    public boolean debugMode = true;

    public int NUMBER_STEPS_MAX = 1000; // maximum number of optimization steps

    public int numTrials = 1;

    public int centerId = -1;

    private void arrayToVec3D(Vector3D v, double[] ary){
	v.setX(ary[0]);
	v.setY(ary[1]);
	v.setZ(ary[2]);
    }

    void vec3DToArray(Vector3D v, double[] ary)   {
	ary[0] = v.getX();
	ary[1] = v.getY();
	ary[2] = v.getZ();
    }

    private double atan(double x) { return Math.atan(x); }

    public double getNumTrials() { return numTrials; }

    private double sin(double x) { return Math.sin(x); }

    private double cos(double x) { return Math.cos(x); }    
    
    public void setNumTrials(int n) { numTrials = n; }

    /**
     * central superposition method, given by Peter Rotkiewitc
     */
    private double superpose(double[][] coords1, double[][] coords2, 
		     double[][] rot, double[] mc1, double[] mc2)
    {
	assert(coords1.length == coords2.length);
	assert ((rot != null) && (rot.length == 3));
	assert ((mc1 != null) && (mc1.length == 3));
	assert ((mc2 != null) && (mc2.length == 3));

	if (debugMode) {
	    // log.fine("Starting core superpose function");
	}
	int npoints = coords1.length;
	if (npoints < 3) {
	    assert false;
	    return 0.0; // problem, mc1 mc2, rot not specified
	}
	double[][] mat_s = new double[3][3];
	double[][] mat_a = new double[3][3];
	double[][] mat_b = new double[3][3];
	double[][] mat_g = new double[3][3];
	double[][] mat_u = new double[3][3];
	double[][] tmp_mat = new double[3][3];
	// mat_a[3][3], mat_b[3][3], mat_g[3][3];
	// double mat_u[3][3], tmp_mat[3][3];
	double val, d, d2, alpha, beta, gamma, x, y, z;
	double cx1, cy1, cz1, cx2, cy2, cz2, tmpx, tmpy, tmpz;
	int i, j, k, n;

	int stepCounter = 0;
	cx1=cy1=cz1=cx2=cy2=cz2=0.;
	
	for (i=0; i<npoints; i++) {
	    cx1+=coords1[i][0];
	    cy1+=coords1[i][1];
	    cz1+=coords1[i][2];
	    cx2+=coords2[i][0];
	    cy2+=coords2[i][1];
	    cz2+=coords2[i][2];
	}
	
	cx1/=(double)npoints;
	cy1/=(double)npoints;
	cz1/=(double)npoints;
	
	cx2/=(double)npoints;
	cy2/=(double)npoints;
	cz2/=(double)npoints;
	
	// shift such that center of cravity is a zero for each set of vectors
	for (i=0; i<npoints; i++) {
	    coords1[i][0]-=cx1;
	    coords1[i][1]-=cy1;
	    coords1[i][2]-=cz1;
	    coords2[i][0]-=cx2;
	    coords2[i][1]-=cy2;
	    coords2[i][2]-=cz2;
	}
	
	for (i=0; i<3; i++)
	    for (j=0; j<3; j++) {
		if (i==j)
		    mat_s[i][j]=mat_a[i][j]=mat_b[i][j]=mat_g[i][j]=1.0;
		else
		    mat_s[i][j]=mat_a[i][j]=mat_b[i][j]=mat_g[i][j]=0.0;
		mat_u[i][j]=0.;
	    }
	
	for (n=0; n<npoints; n++) {
	    mat_u[0][0]+=coords1[n][0]*coords2[n][0];
	    mat_u[0][1]+=coords1[n][0]*coords2[n][1];
	    mat_u[0][2]+=coords1[n][0]*coords2[n][2];
	    mat_u[1][0]+=coords1[n][1]*coords2[n][0];
	    mat_u[1][1]+=coords1[n][1]*coords2[n][1];
	    mat_u[1][2]+=coords1[n][1]*coords2[n][2];
	    mat_u[2][0]+=coords1[n][2]*coords2[n][0];
	    mat_u[2][1]+=coords1[n][2]*coords2[n][1];
	    mat_u[2][2]+=coords1[n][2]*coords2[n][2];
	}
	
	for (i=0; i<3; i++)
	    for (j=0; j<3; j++)
		tmp_mat[i][j]=0.;
	
	do {
	    d=mat_u[2][1]-mat_u[1][2];
	    d2 = mat_u[1][1]+mat_u[2][2];
	    if ((d==0.0) || d2 == 0.0) {
		alpha=0.0; 
	    }
	    else {
		alpha=atan(d/d2); // /(mat_u[1][1]+mat_u[2][2]));
	    }
	    if (cos(alpha)*(mat_u[1][1]+mat_u[2][2])+sin(alpha)*(mat_u[2][1]-mat_u[1][2])<0.0) alpha+=Math.PI;
	    mat_a[1][1]=mat_a[2][2]=cos(alpha);
	    mat_a[2][1]=sin(alpha);
	    mat_a[1][2]=-mat_a[2][1];
	    for (i=0; i<3; i++)
		for (j=0; j<3; j++)
		    for (k=0; k<3; k++)
			tmp_mat[i][j]+=mat_u[i][k]*mat_a[j][k];
	    for (i=0; i<3; i++)
		for (j=0; j<3; j++) {
		    mat_u[i][j]=tmp_mat[i][j];
		    tmp_mat[i][j]=0.;
		}
	    for (i=0; i<3; i++)
		for (j=0; j<3; j++)
		    for (k=0; k<3; k++)
			tmp_mat[i][j]+=mat_a[i][k]*mat_s[k][j];
	    for (i=0; i<3; i++)
		for (j=0; j<3; j++) {
		    mat_s[i][j]=tmp_mat[i][j];
		    tmp_mat[i][j]=0.;
		}
	    d=mat_u[0][2]-mat_u[2][0];
	    d2 = mat_u[0][0]+mat_u[2][2];
	    if ((d==0.0) || (d2 == 0.0)) {
		beta=0.0; 
	    }
	    else {
		beta=atan(d/d2);
	    }
	    if (cos(beta)*(mat_u[0][0]+mat_u[2][2])+sin(beta)*(mat_u[0][2]-mat_u[2][0])<0.0) beta+=Math.PI;
	    mat_b[0][0]=mat_b[2][2]=cos(beta);
	    mat_b[0][2]=sin(beta);
	    mat_b[2][0]=-mat_b[0][2];
	    for (i=0; i<3; i++)
		for (j=0; j<3; j++)
		    for (k=0; k<3; k++)
			tmp_mat[i][j]+=mat_u[i][k]*mat_b[j][k];
	    for (i=0; i<3; i++)
		for (j=0; j<3; j++) {
		    mat_u[i][j]=tmp_mat[i][j];
		    tmp_mat[i][j]=0.;
		}
	    for (i=0; i<3; i++)
		for (j=0; j<3; j++)
		    for (k=0; k<3; k++)
			tmp_mat[i][j]+=mat_b[i][k]*mat_s[k][j];
	    for (i=0; i<3; i++)
		for (j=0; j<3; j++) {
		    mat_s[i][j]=tmp_mat[i][j];
		    tmp_mat[i][j]=0.;
		}
	    d=mat_u[1][0]-mat_u[0][1];
	    d2 = mat_u[0][0]+mat_u[1][1];
	    if ((d==0.0) || (d2 == 0.0)) {
		gamma=0.0; 
	    }
	    else {
		gamma=atan(d/d2);
	    }
	    if (cos(gamma)*(mat_u[0][0]+mat_u[1][1])+sin(gamma)*(mat_u[1][0]-mat_u[0][1])<0.0) gamma+=Math.PI;
	    mat_g[0][0]=mat_g[1][1]=cos(gamma);
	    mat_g[1][0]=sin(gamma);
	    mat_g[0][1]=-mat_g[1][0];
	    for (i=0; i<3; i++)
		for (j=0; j<3; j++)
		    for (k=0; k<3; k++)
			tmp_mat[i][j]+=mat_u[i][k]*mat_g[j][k];
	    for (i=0; i<3; i++)
		for (j=0; j<3; j++) {
		    mat_u[i][j]=tmp_mat[i][j];
		    tmp_mat[i][j]=0.;
		}
	    for (i=0; i<3; i++)
		for (j=0; j<3; j++)
		    for (k=0; k<3; k++)
			tmp_mat[i][j]+=mat_g[i][k]*mat_s[k][j];
	    for (i=0; i<3; i++)
		for (j=0; j<3; j++) {
		    mat_s[i][j]=tmp_mat[i][j];
		    tmp_mat[i][j]=0.;
		}
	    val=Math.abs(alpha)+Math.abs(beta)+Math.abs(gamma);
	} while ((val>0.0001) && (stepCounter++ < NUMBER_STEPS_MAX)); // added additional break when too many steps (Eckart Bindewald 2004)
	
	val=0.;
	for (i=0; i<npoints; i++) {
	    x=coords2[i][0];
	    y=coords2[i][1];
	    z=coords2[i][2];
	    tmpx=x*mat_s[0][0]+y*mat_s[0][1]+z*mat_s[0][2];
	    tmpy=x*mat_s[1][0]+y*mat_s[1][1]+z*mat_s[1][2];
	    tmpz=x*mat_s[2][0]+y*mat_s[2][1]+z*mat_s[2][2];
	    x=coords1[i][0]-tmpx;
	    y=coords1[i][1]-tmpy;
	    z=coords1[i][2]-tmpz;
	    val+=x*x+y*y+z*z;
	}

	for (i=0; i<npoints; i++) {
	    coords1[i][0]+=cx1;
	    coords1[i][1]+=cy1;
	    coords1[i][2]+=cz1;
	    coords2[i][0]+=cx2;
	    coords2[i][1]+=cy2;
	    coords2[i][2]+=cz2;
	}
	
	if (rot != null) {
	    for (i=0; i<3; i++)
		for (j=0; j<3; j++)
		    rot[i][j]=mat_s[i][j];
	}
	
	if (mc1 != null) {
	    mc1[0]=cx1;
	    mc1[1]=cy1;
	    mc1[2]=cz1;
	}
	
	if (mc2 != null) {
	    mc2[0]=cx2;
	    mc2[1]=cy2;
	    mc2[2]=cz2;
	}
	if (debugMode) {
	    // log.fine("Ending core superpose function");
	}
	return Math.sqrt(val/(double)npoints);
    }
    
    public SuperpositionResult superposeSingle(Vector3D[] v1, Vector3D[] v2) 
	// Matrix3D m, Vector3D& c1, Vector3D& c2)
    {
	assert (v1.length > 2);
 	assert (v1.length == v2.length);
	if (debugMode) {
	    // log.fine("Starting MinSuperpose.superpose!");
	    for (int i = 0; i < v1.length; ++i) {
		// log.fine("v1-" + (i+1) + " : " + v1[i] + " ; v2-" + (i+1) + " : " + v2[i]);
	    }
	}
	Vector3D c1 = new Vector3D();
	Vector3D c2 = new Vector3D();
	int n = v1.length;
	double[][] v1Double = new double[n][3]; //  = * double[n];
	double[][] v2Double = new double[n][3]; //  = new * double[n];  
// 	for (int i = 0; i < n; ++i) {
// 	    v1Double[i] = new double[3];
// 	    v2Double[i] = new double[3];
// 	}
	double[][] rot = new double[3][3];
	double[] row0 = new double[3];
	double[] row1 = new double[3];
	double[] row2 = new double[3];
	rot[0] = row0;
	rot[1] = row1;
	rot[2] = row2;
	double[] mc1 = new double[3];
	double[] mc2 = new double[3];
	for (int i = 0; i < 3; ++i) {
	    mc1[i] = 0.0;
	    mc2[i] = 0.0;
	    for (int j = 0; j < 3; ++j) {
		rot[i][j] = 0.0;
	    }
	}
	for (int i = 0; i < v1.length; ++i) {
	    vec3DToArray(v1[i], v1Double[i]);
	    vec3DToArray(v2[i], v2Double[i]);
	}
	double resultDouble= superpose(v1Double, v2Double, rot, mc1, mc2);
	// copy to array
	for (int i = 0; i < n; ++i) {
	    // assert (v2[i].getX() != v2Double[i][0]); // must change something!!!
	    v2[i].setX(v2Double[i][0]);
	    v2[i].setY(v2Double[i][1]);
	    v2[i].setZ(v2Double[i][2]);
	}
	arrayToVec3D(c1, mc1);
	arrayToVec3D(c2, mc2);
	
	Matrix3D m = new Matrix3D(rot[0][0], rot[0][1], rot[0][2],
				  rot[1][0], rot[1][1], rot[1][2],
				  rot[2][0], rot[2][1], rot[2][2]);
	
	SuperpositionResult superposition = new SuperpositionResult();
	assert (m != null);
	superposition.setRotationMatrix(m);
	assert (v2 != null);
	superposition.setNewCoords(v2);
	assert (c1 != null);
	superposition.setShift1(c1);
	assert (c2 != null);
	superposition.setShift2(c2);
	superposition.setRms(resultDouble);

// 	if (debugMode) {
// 	    for (int i = 0; i < v2.length; ++i) {
// 		Vector3D vTmp = new Vector3D(v2[i]);
// 		applyTransformation(vTmp);
// 	    }
// 	}

	assert superposition.isValid();
	return superposition;
    }

    /** try numTrials iterations of superposition. Vector set v2 is rotated randomly prior to actual superposition. */
    public SuperpositionResult superpose(Vector3D[] v1, Vector3D[] v2Orig, int numTrials) 
	// Matrix3D m, Vector3D& c1, Vector3D& c2)
    {
	assert false; // not supported currently
	assert numTrials > 0;
	assert v1.length > 2;
	assert v1.length == v2Orig.length;
	int dim = v1.length;
	SuperpositionResult bestResult = null;
	double bestRms = 0;
	Matrix3D bestRandomMatrix = null;
	// make deep copy:
	Vector3D[] v2 = new Vector3D[dim];
	Vector3D[] v2Best = new Vector3D[dim];
	for (int i = 0; i < dim; ++i) {
	    v2[i] = new Vector3D(v2Orig[i]);
	    v2Best[i] = new Vector3D(v2Orig[i]);
	}
	for (int i = 0; i < numTrials; ++i) {
	    // reset v2:
	    for (int j = 0; j < dim; ++j) {
		v2[j].copy(v2Orig[j]);
	    }
	    Matrix3D randMtx = SuperpositionTools.randomRotateAroundOrigin(v2);
	    SuperpositionResult supResult = superposeSingle(v1, v2);
	    double rms = supResult.getRms();
	    if ((i==0) || (rms < bestRms)) {
		if ((i > 0) && ((bestRms - rms) > 0.1)) {
		    // log.fine("New best superposition found (rms: " + rms + " so far best rms: " + bestRms + ")");
		}
		bestResult = supResult;
		bestRms = rms;
		bestRandomMatrix = randMtx;
		// reset v2Best:
		for (int j = 0; j < dim; ++j) {
		    v2Best[j].copy(v2[j]);
		}
	    }
	}
	Matrix3D supMatrix = bestResult.getRotationMatrix();
	supMatrix = supMatrix.multiply(bestRandomMatrix); // first multiply with random matrix
	bestResult.setRotationMatrix(supMatrix);
	for (int i = 0; i < dim; ++i) {
	    v2Orig[i].copy(v2Best[i]);
	}
	return bestResult;
    }

    public SuperpositionResult superpose(Vector3D[] v1, Vector3D[] v2) 
	// Matrix3D m, Vector3D& c1, Vector3D& c2)
    {
	SuperpositionResult result = null;

	// log.fine("Starting MinSuperpose.superpose: ");
	for (int i = 0; i < v1.length; ++i) {
	    // log.finest(""+ v1[i] + " vs. " + v2[i]);
	}

	if (numTrials > 1) {
	    result = superpose(v1, v2, numTrials);
	}
	else {
	    result = superposeSingle(v1, v2);
	}
	// log.fine("Finishing MinSuperpose.superpose.");
	// log.fine("Starting MinSuperpose.superpose: ");
	for (int i = 0; i < v1.length; ++i) {
	    Vector3D v = new Vector3D(v2[i]);
	    result.applyTransformation(v);
	    // log.fine(""+ v1[i] + " vs. " + v2[i] + " now: " + v);
	}
	return result;
    }

}
