package tools3d;

/** describes 3d coordinate system in space. 
 * It has a position and orientation.
 */
public interface Orientable extends Positionable3D {

    /** What is object's rotation axis? */
    public Vector3D getRotationAxis();
    
    /** How is object rotated? */
    public double getRotationAngle();
    
    /** Set position in space. Sets absolute position in space */
    public void setPosition(Vector3D point);

    public void setRotationAngle(double angle);

    public void setRotationAxis(Vector3D axis);

    /** Rotate object. */
    public void rotate(Vector3D axis, double angle);
    
    public void rotate(Vector3D center, Vector3D axis, double angle);

    public void rotate(Vector3D center, Matrix3D rotationMatrix);
    
    /** Translate object. */
    public void translate(Vector3D vec);
    
}
