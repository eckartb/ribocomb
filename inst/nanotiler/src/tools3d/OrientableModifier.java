package tools3d;

/** Defines interface for changing position and orientation of an object */
public interface OrientableModifier {

    public double getAngleStep();

    public double getTranslationStep();

    public void mutate(Orientable obj);

    public void rotate(Orientable obj);

    public void translate(Orientable obj);

    public void scaleAngleStep(double scale);

    public void scaleTranslationStep(double scale);

    public void setAngleStep(double d);

    public void setTranslationStep(double d);


}
