package tools3d;

import Jama.*;

public class OrientationTools {

    public static final double MIN_EIGENVALUE = 0.01;

    /* Computes center of gravity of set of positions */
    public static Vector3D computeCenterOfGravity(Vector3D[] mol) {
	Vector3D result = new Vector3D();
	for (Vector3D v : mol) {
	    result.add(v);
	}
	if (mol.length > 0) {
	    result.scale(1.0 / (double)(mol.length));
	}
	return result;
     }

    /** see Goldstein, Classical Mechanics Ch. "The inertia tensor and the
	moment of inertia */
    public static Matrix3D computeInertiaTensor(Vector3D[] v, double[] weights) 
    {
	assert v != null && ((weights == null) || (weights.length == v.length));
	Matrix3D m = new Matrix3D();
	Matrix3D unitMatrix = new Matrix3D(1.0,1.0,1.0);
	for (int i = 0; i < v.length; ++i) {
	    Matrix3D tmpMatrix = ( (unitMatrix.multiply(v[i].dot(v[i]))).minus(Matrix3D.outerProduct(v[i],v[i]) ));
	    if (weights != null) {
		tmpMatrix.scale(weights[i]);
	    }
	    m.add(tmpMatrix);
	}
// 	assert ( isReasonable(translateMatrix3DToVec(m))
// 		  && isSymmetric(translateMatrix3DToVec(m)), exception);
	return m;
    }

    /** see Goldstein, Classical Mechanics Ch. "The inertia tensor and the
	moment of inertia */
    public static EigenvalueDecomposition computeGyrationGeometry(Vector3D[] v,
						    double[] weights) {
	assert (v != null) && (weights == null || (v.length == weights.length));
	Matrix3D inertiaTensor = computeInertiaTensor(v,weights);
	Matrix inertiaMatrix = new Matrix(inertiaTensor.toArray());
	return new EigenvalueDecomposition(inertiaMatrix);
	// 	jacobi(translateMatrix3DToVec(inertiaTensor), eigenValues,
	// 	       eigenVectors);
    }
    
    private static boolean hasPrecedence(double[] v1, double[] v2) {
	int i = 0;
	while ((i < v1.length) && (i < v2.length)) {
	    if (v1[i] > v2[i]) {
		return true;
	    }
	    else if (v1[i] < v2[i]) {
		return false;
	    }
	}
	// all elements so far equal
	return v1.length > v2.length;
    }


    /* transform molecular positions to new coordinates
     * @returns Transforming coordinate system or null if undefined (for less than 3 positions): 
     *   first vector is origin, second and third vector are x and y axis of new coordinate system.
     */
     public static Vector3D[] computeNormalizedOrientation(Vector3D[] mol, double[] weights) {
	 assert mol != null && (weights == null || mol.length == weights.length);
	 if (mol.length < 3) {
	     return null;
	 }
	 Vector3D origin = computeCenterOfGravity(mol);
	 // wrong, already take care of at end of this function
	 // shiftAtoms(mol, origin * (-1.0)); // move to norm center
	 Vector3D x1 = new Vector3D(1.0,0.0,0.0);
	 Vector3D x2 = new Vector3D(0.0,1.0,0.0);
	 assert (mol.length >= 3);
	 EigenvalueDecomposition eigen = computeGyrationGeometry(mol,weights);
	 double[] eigenValues = eigen.getRealEigenvalues();
	 double[][] eigenVectors = eigen.getV().getArray();
	 //  Vec<double> eigenValuesSave = eigenValues;
	 // int[] order = documentedSort(eigenValues);
	 // if eigenvalues too small, set them to minimum value:
	 for (int i = 0; i < 3; ++i) {
	     if (eigenValues[i] < MIN_EIGENVALUE) {
		 eigenValues[i] = MIN_EIGENVALUE;
	     }
	 }
	 int best = 0;
	 int second = 1;
	 int third = 2;
	 // sort eigenvalues
	 /*
	 if ((eigenValues[0] >= eigenValues[1]) && (eigenValues[0] >= eigenValues[2])) {
	     best = 0;
	     second = 2;
	     third = 1;
	 }
	 else if ((eigenValues[1] >= eigenValues[0]) && (eigenValues[1] >= eigenValues[2])) {
	     best = 1;
	     second = 0;
	     third = 2;
	 }
	 else if ((eigenValues[2] >= eigenValues[0]) && (eigenValues[2] >= eigenValues[1])) {
	     best = 2;
	     second = 1;
	     third = 0;
	 }
	 else {
	     assert false; // should never happen
	 }
	 */

	 /*
	 if ((eigenValues[0] >= eigenValues[1]) && (eigenValues[0] >= eigenValues[2])) {
	     best = 0;
	     if (eigenValues[1] >= eigenValues[2]) {
		 second = 1;
		 third = 2;
	     }
	     else {
		 second = 2;
		 third = 1;
	     }
	 }
	 else if ((eigenValues[1] >= eigenValues[0]) && (eigenValues[1] >= eigenValues[2])) {
	     best = 1;
	     if (eigenValues[0] >= eigenValues[2]) {
		 second = 0;
		 third = 2;
	     }
	     else {
		 second = 2;
		 third = 0;
	     }
	 }
	 else if ((eigenValues[2] >= eigenValues[0]) && (eigenValues[2] >= eigenValues[1])) {
	     best = 2;
	     if (eigenValues[0] >= eigenValues[1]) {
		 second = 0;
		 third = 1;
	     }
	     else {
		 second = 1;
		 third = 0;
	     }
	 }
	 else {
	     assert false; // should never happen
	 }
	 */
	 
	 // 	 if (eigenValues[0] > eigenValues[1]) {
	 // 	     if (eigenVa
	 // 	 }
	 // 	 // sort for largest eigenvalues;
	 // 	 int best = order[2];
	 // 	 double bestVal = eigenValues[2];
	 // 	 int second = order[1];
	 // 	 double secondVal = eigenValues[1];
	 // 	 int third = order[0];
	 // 	 double thirdVal = eigenValues[0];
	 
	 // 	 // sort worst two:
	 // 	 if (thirdVal == secondVal) {
	 // 	     // if eigenvalues are equal: choose with "precedence" function
	 // 	     if (hasPrecedence(eigenVectors[third], eigenVectors[second])) {
	 // 		 third = order[1];
	 // 		 thirdVal = eigenValues[1];
	 // 		 second = order[0];
	 // 		 secondVal = eigenValues[0];
	 // 	     }
	 // 	 }
	 
	 // 	 if (bestVal == secondVal) {
	 // 	     // if eigenvalues are equal: choose with "precedence" function
	 // 	     if (hasPrecedence(eigenVectors[second], eigenVectors[best])) {
	 // 		 best = order[1];
	 // 		 bestVal = eigenValues[1];
	 // 		 second = order[2];
	 // 		 secondVal = eigenValues[2];
	 // 	     }
	 // 	 }
	 assert best != second;
	 assert best != third;
	 assert second != third;
	 x1 = new Vector3D(eigenVectors[best][0],
			   eigenVectors[best][1],
			   eigenVectors[best][2]);
	 x2 = new Vector3D(eigenVectors[second][0],
			   eigenVectors[second][1],
			   eigenVectors[second][2]);
	 Vector3D[] result = new Vector3D[3];
	 result[0] = origin;
	 result[1] = x1;
	 result[2] = x2;
	 // CoordinateSystem coord(origin, x1, x2); // new direction and origin
	 assert Math.abs(result[1].dot(result[2])) < 0.01; // must be right angle!
	 return result;
     }
}
