package tools3d;

import java.util.*;
import java.util.regex.*;
import java.io.*;
import java.text.ParseException;
import java.util.logging.*;;
import java.util.ResourceBundle;
import generaltools.StringTools;
import tools3d.Vector3D;

/** parses output files of PARSYS program into coordinates of atoms */
public class PRSFileParser {

    private String [] lines;
    private Vector3D [] carbonAtomPositions;
    private String sequenceString;
    final private int POSITION_COL = 0;
    final private int SEQUENCE_ANSI_VAL_COL = 1;
    final private int SEQUENCE_COL = 2; //columns where values can be found
    final private int STRAND_COL = 3;
    final private int X_COL = 4;
    final private int Y_COL = 5;
    final private int Z_COL = 6;
    final private String NUMBER = ".*\\d+.*"; //number regex
    final private String SPACE = "\\s+"; //n number of spaces regex
    final private String HEADER = "###"; //signifies the header line
    

    public PRSFileParser (String filename)  {
      try {
      	FileInputStream f = new FileInputStream(filename);
        this.lines = StringTools.readAllLines(f);
        parseAtomPositions();
        parseSequence();
      } catch (IOException i) {
        System.out.println ("PRS File not found");
        i.printStackTrace ();
      }
    }

    public PRSFileParser (String [] lines) throws IOException {
      this.lines = lines;
      parseAtomPositions();
      parseSequence();
    }
    
    //gets position of carbon atoms in PRS file
    private void parseAtomPositions() {
      Double [] xPos = new Double [this.lines.length];
      Double [] yPos = new Double [this.lines.length];
      Double [] zPos = new Double [this.lines.length];
      Vector3D [] carbonPos = new Vector3D [this.lines.length];

      for (int k = 0; k < carbonPos.length; k++) { //constucts sequence
	      String [] columns;
        String  lineOfFile = lines [k].trim ();
        lineOfFile = lineOfFile.replaceAll (SPACE, " "); //cleans bad file lines
	      columns = lineOfFile.split (" ");
	      if (columns.length >= 6) {  
        if (columns [X_COL].matches(NUMBER)) {
  		          xPos [k] = Double.parseDouble(columns [X_COL]);
  		  } if (columns [Y_COL].matches(NUMBER)) {
                yPos [k] = Double.parseDouble(columns [Y_COL]);
  		  } if (columns [Z_COL].matches(NUMBER)) {
                zPos [k] = Double.parseDouble(columns [Z_COL]); 
          }
	      }
        carbonPos[k]= new Vector3D(xPos[k], yPos[k], zPos[k]);
	  }

      this.carbonAtomPositions = carbonPos;
    }
    
    private void parseSequence(){
      String s = "";
      for (int k = 0; k < lines.length; k++) { //constucts sequence
        String [] columns;
        String  lineOfFile = lines [k].trim ();
        lineOfFile = lineOfFile.replaceAll (SPACE, " "); //cleans bad file lines
        columns = lineOfFile.split (" ");
        if (columns.length >= 6) {  
          if (columns[SEQUENCE_COL].length () == 1) {
            s = s + columns[SEQUENCE_COL];
          }
        }
      }
      this.sequenceString = s;
    }
    
    public Vector3D [] getAtomPosititions() {
      return this.carbonAtomPositions;
    }
    
    public String getSequenceString () {
      return this.sequenceString;
    }
    
}

