package tools3d;

/** Method epsilon returns 1 if even permutation of 123
 *  (123 or rotations thereof)
 * -1 if uneven permuation of 123 (321 or rotations thereof).
 * Also work with vector 012 instead of 123 !
 * @see http://www.mathworks.com/access/helpdesk/help/toolbox/physmod/mech/mech_review5a.html
 * TODO inefficient implementation. Lookup table would be faster!
 */
public class PermutationSymbol3 {

    public static int epsilon(int i, int j, int k) {
	int[] v = new int[3];
	v[0] = i;
	v[1] = j;
	v[2] = k;
	return epsilon(v);
    }

    /** rotate v by one */
    public static void rotate(int[] v) {
	int help = v[v.length-1];
	for (int i = v.length-2; i >= 0; --i) {
	    v[i+1] = v[i];
	}
	v[0] = help;
    }

    /** true if vector is either 123 or 012 */
    private static boolean isPositive(int[] v) {
	if (v[0] == 0) {
	    return (v[1] == 1) && (v[2] == 2);
	} 
	else if (v[0] == 1) {
	    return (v[1] == 2) && (v[2] == 3);
	}
	return false;
    }

    /** true if vector is either 321 or 210 */
    private static boolean isNegative(int[] v) {
	if (v[0] == 2) {
	    return (v[1] == 1) && (v[2] == 0);
	} 
	else if (v[0] == 3) {
	    return (v[1] == 2) && (v[2] == 1);
	}
	return false;
    }

    private static int epsilon(int[] v) {
	assert v != null;
	int rotCount = 0;
	do {
	    if (isPositive(v)) {
		return +1;
	    }
	    else if (isNegative(v)) {
		return -1;
	    }
	    rotate(v); // TODO inefficient: one useless rotation at end
	    ++rotCount;
	}
	while (rotCount < 3);
	return 0; // neither positive nor negative
    }

}
