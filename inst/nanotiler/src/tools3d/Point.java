/**
 * Creates a point in three-dimensional space.
 *
 * @author Christine Viets
 */
package tools3d;

import java.util.logging.Logger;
import java.util.Vector;

import tools3d.objects3d.Object3D;
import tools3d.objects3d.SimpleObject3D;

public class Point {

    private static Logger log = Logger.getLogger("NanoTiler_debug"); /** The generated debug log. */

    public static final double SIMILAR_CUTOFF = 0.00001;
    public static final double SIMILAR_CUTOFF_OLD = 10.0; // distance cutoff of what points are considered similar

    private boolean centerPoint = false;
    private boolean finalPoint;
    private String name = "POINT";
    private CoordinateSet xyz = new CoordinateSet( 0.0, 0.0, 0.0 );
    private Vector<Point> neighbors = new Vector<Point>();

    /** Creates a new Point. */
    public Point() { }

    /** 
     * Creates a new Point.
     *
     * @param name The String representation of the name of the Point.
     * @param xyz The CoordinateSet of the Point.
     */
    public Point( String name, CoordinateSet xyz ) {
	setName(name);
	setXYZ(xyz);
    }

    public Point( String name, CoordinateSet xyz, boolean centerPoint ) {
	setName(name);
	setXYZ(xyz);
	centerPoint(centerPoint);
    }

    public Point( CoordinateSet xyz ) { setXYZ(xyz); }

    /**
     * Creates a new Point.
     *
     * @param x The x-coordinate of the Point.
     * @param y The y-coordinate of the Point.
     * @param z The z-coordinate of the Point.
     */
    public Point( double x, double y, double z ) {
	setName("aPoint");
	setXYZ( x, y, z );
    }

    /** 
     * Creates a new Point.
     * 
     * @param keep True if the Point will remain after Symmetries are generated, false if Point is simply a placeholder.
     * @param name The String representation of the name of the Point.
     * @param x The x-coordinate of the Point.
     * @param y The y-coordinate of the Point.
     * @param z The z-coordinate of the Point.
     */
    public Point(boolean keep, String name, double x, double y, double z ) {
	setFinal(keep);
	setName(name);
	setXYZ( x, y, z );
    }

    /** 
     * Creates a new Point.
     * 
     * @param name The String representation of the name of the Point.
     * @param x The x-coordinate of the Point.
     * @param y The y-coordinate of the Point.
     * @param z The z-coordinate of the Point.
     */
    public Point( String name, double x, double y, double z ) {
	setName(name);
	setXYZ( x, y, z );
    }

    public Point( String name, double x, double y, double z, boolean centerPoint ) {
	setName(name);
	setXYZ( x, y, z );
	centerPoint(centerPoint);
    }

    /** If this point is the center point in the junction */
    public void centerPoint( boolean b ) {
	centerPoint = b;
    }

    public boolean getCenterPoint() {
	return centerPoint;
    }

    public boolean getFinalPoint() {
	return finalPoint;
    }

    public void addNeighbor( Point p ) {
	boolean neighborAlreadyAdded = false; //The Point to be added as a neighbor is not already a neighbor.
	for( int i = 0; i < getNumNeighbors(); i++ ) { //For each neighbor,
	    if( p.getName().equals(getNeighbor(i).getName()) ) //If the Point is the same as the neighbor,
		neighborAlreadyAdded = true; //The Point has been found as a neighbor.
	} //End for each neighbor.
	if( !neighborAlreadyAdded ) //If the neighbor was not already added,
	    neighbors.add(p); //Add the neighbor.
	p.addNeighborBack(this);
    }

    public void addNeighbor( int index, Point p ) {
	boolean neighborAlreadyAdded = false; //The Point to be added as a neighbor is not already a neighbor.
	for( int i = 0; i < getNumNeighbors(); i++ ) { //For each neighbor,
	    if( p.getName().equals(getNeighbor(i).getName()) ) //If the Point is the same as the neighbor,
		neighborAlreadyAdded = true; //The Point has been found as a neighbor.
	} //End for each neighbor.
	if( !neighborAlreadyAdded ) //If the neighbor was not already added,
	    neighbors.add( index, p ); //Add the neighbor.
    }

    public void addNeighbor( String vector, Point p ) {
	//because the point is a vector2
	log.warning("TODO: Method not yet implemented");
    }

    /**
     * Add a neighbor to the other Point.
     */
    public void addNeighborBack( Point p ) {
	boolean neighborAlreadyAdded = false; //The Point to be added as a neighbor is not already a neighbor.
	for( int i = 0; i < getNumNeighbors(); i++ ) { //For each neighbor,
	    if( p.getName().equals(getNeighbor(i).getName()) ) //If the Point is the same as the neighbor,
		neighborAlreadyAdded = true; //The Point has been found as a neighbor.
	} //End for each neighbor.
	if( !neighborAlreadyAdded ) //If the neighbor was not already added,
	    neighbors.add(p); //Add the neighbor.
    }

    public Point getNeighbor(int i) {
	return (Point)neighbors.get(i);
    }

    public Vector<Point> getNeighbors() {
	return neighbors;
    }

    public int getNumNeighbors() {
	return neighbors.size();
    }

   public Object cloneDeep() {
	Point p = new Point();
	p.copyDeepThisCore(this);
	return p;
    }

    protected void copyDeepThisCore(Point p) {	
	setName(p.getName());
	setXYZ(p.getXYZ());
	centerPoint(p.getCenterPoint());
    }

    /** Does not include centerPoint because user may not have centerPoint flag marked, though the point could be the center point. */
    public boolean equals( Point p ) {
	int result = 0;

	if( this.getName().trim().length() == p.getName().trim().length() )
	    result++; //result is 1
	else
	    return false;

	int sameName = 0;
	for( int i = 0; i < this.getName().trim().length(); i++ ) {
	    if( this.getName().trim().charAt(i) == p.getName().trim().charAt(i) )
		sameName++;
	}
	if( sameName == this.getName().trim().length() )
	    result++; //result is 2
	else
	    return false;

	if( Math.abs( this.getX() - p.getX() ) == 0.0 )
	    result++; //result is 3
	else
	    return false;
	if( Math.abs( this.getY() - p.getY() ) == 0.0 )
	    result++; //result is 4 
	else
	    return false;
	if( Math.abs( this.getZ() - p.getZ() ) == 0.0 )
	    result++; //result is 5
	else
	    return false;

	if( result == 5 )
	    return true;
	else
	    return false;
    }

    /** Checks if a point is within .0001 of another point*/
    public boolean similar(Object other) { 
	if (this.equals(other)) {
	    return true;
	}
	if (! (other instanceof Point)) {
	    return false;
	}
	Point point = (Point)other;
	return similar(this.getX(), point.getX()) && similar(this.getY(), point.getY()) && similar(this.getZ(), point.getZ());
    }

    /** Checks if two double values are close to each other */
    public static boolean similar(double a, double b) {
	return Math.abs(a-b) < SIMILAR_CUTOFF;
    }

    //TODO: decrease error when program is refined
    /** Checks if a point is within 10 angstroms of the created junction */
    public boolean isSimilarTo( Point p ) {
	log.info( "Similarity beteen " + this + " and " + p +
		  Math.sqrt( Math.pow( p.getX() - this.getX(), 2 ) + 
			     Math.pow( p.getY() - this.getY(), 2 ) +
			     Math.pow( p.getZ() - this.getZ(), 2 ) ) );
	
	return ( Math.sqrt( Math.pow( p.getX() - this.getX(), 2 ) + 
			    Math.pow( p.getY() - this.getY(), 2 ) +
			    Math.pow( p.getZ() - this.getZ(), 2 ) ) < SIMILAR_CUTOFF_OLD ); //distance formula
    }

    /** Returns the name of the Point. */
    public String getName() { return name; }

    public Vector3D getVector3D() { return xyz.toVector3D(); }

    /** Returns the CoordinateSet (x, y, z) of the Point. */
    public CoordinateSet getXYZ() { return xyz; }

    /** Returns the x-coordinate of the Point. */
    public double getX() { return xyz.getX(); }

    /** Returns the y-coordinate of the Point. */
    public double getY() { return xyz.getY(); }

    /** Returns the z-coordinate of the Point. */
    public double getZ() { return xyz.getZ(); }

    public void rotate(Vector3D start, Vector3D axis, double angle) {
	Vector3D vector3d = this.getVector3D();
	vector3d.rotate(start, axis, angle);
	setX(vector3d.getX());
	setY(vector3d.getY());
	setZ(vector3d.getZ());
    }

    public void setFinal(boolean keep) {
	this.finalPoint = keep;
    }

    /**
     * Returns a string representation of this Point in the form
     * "point name: x-coordinate, y-coordinate, z-coordinate".
     *
     * @return The String representation of this Point.
     */
    public String toString() {
	String s = "";
	if( getCenterPoint() )
	    s += name + ": " + xyz + " (center point)";
	else
	    s += name + ": " + xyz;
	return s;
    }

    public Object3D toObject3D() {
	Object3D o = new SimpleObject3D();
	o.setPosition( getVector3D() );
	o.setName( getName() );
	return o;
    }

    /** 
     * Sets the name of the Point.
     *
     * @param name The String representation of the name of the Point.
     */
    public void setName( String name ) { this.name = name; }

    /** 
     * Sets the coordinates of the Point.
     *
     * @param x The x-coordinate of the Point.
     * @param y The y-coordinate of the Point.
     * @param z The z-coordinate of the Point.
     */
    public void setXYZ( double x, double y, double z ) { xyz = new CoordinateSet( x, y, z ); }

    /**
     * Sets the coordinates of the Point.
     *
     * @param xyz The CoordinateSet of the Point.
     */
    public void setXYZ( CoordinateSet xyz ) { this.xyz = xyz; }
    
    /** 
     * Sets the x-coordinate of the Point.
     *
     * @param x The x-coordinate of the Point.
     */
    public void setX( double x ) { xyz.setX(x); }
    
    /**
     * Sets the y-coordinate of the Point.
     *
     * @param y The y-coordinate of the Point.
     */
    public void setY( double y ) { xyz.setY(y); }
    
    /**
     * Sets the z-coordinate of the Point.
     *
     * @param z The z-coordinate of the Point.
     */
    public void setZ( double z ) { xyz.setZ(z); }

}
