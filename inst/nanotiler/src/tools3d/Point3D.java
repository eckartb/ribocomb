package tools3d;

/** represents point in 3d space. most primitive class along with Edge3D, Face3D */
public class Point3D extends AbstractGeometryElement {

    private double radius;

    public Point3D() {
	this.position = new Vector3D(0.0, 0.0, 0.0);
	radius = 0.0;
    }

    public Point3D(double x, double y, double z) {
	this.position = new Vector3D(x, y, z);
	radius = 0.0;
    }

    public Point3D(Vector3D position) {
	this.position = new Vector3D(position);
	radius = 0.0;
    }

    public Point3D(Vector3D position, double radius) {
	this.position = new Vector3D(position);
	this.radius = radius;
    }

    /** Careful: not complete */
    private Point3D(Point3D point) {
 	this(point.getPosition(), point.getRadius());
    }
    
    /** clones object; Careful: not really deep copy */
    public Object cloneDeep() {
	return new Point3D(this); 
    }

    public double getRadius() { return radius; }

    public void setRadius(double radius) { this.radius = radius; }

}
