package tools3d;

/** implementation of point (with radius!) as Shape3D
 */
public class PointShape extends SimpleShape3D implements Shape3D {

    /** defines sphere with radius and position */
    public PointShape(Vector3D position, double radius) {
	super();
	setPosition(position);
	setBoundingRadius(radius);
	setDimensions(new Vector3D(radius, radius, radius));
    }

    /** creates dummy set of points, edges, faces */
    public Geometry createGeometry(double angleDelta) {
	if (!isGeometryUpToDate()) {
	    updateGeometry(angleDelta);
	}
	return geometry;
    }

    public String getShapeName() { return "PointShape"; }

    protected void updateGeometry(double angleDelta) {
	Vector3D pos = getPosition();
	double radius = getBoundingRadius();
 	Point3D[] points = new Point3D[1];
	points[0] = new Point3D(pos, radius);
	Edge3D[] edges = new Edge3D[0];
 	Face3D[] faces = new Face3D[0];
 	geometry = new StandardGeometry(points, edges, faces);
	geometry.setProperties(getProperties());
	geometry.setSelected(isSelected());
	setGeometryUpToDate(true);
    }

}
