package tools3d;

import java.io.Serializable;
import tools3d.Vector3D;

public interface Positionable3D extends Comparable<Positionable3D>, Serializable {

    /** perform active transformation of all involved vectors */
    public void activeTransform(CoordinateSystem cs);

    /** perform passive transformation of all involved vectors */
    public void passiveTransform(CoordinateSystem cs);

    public Object cloneDeep();

    /** returns distance to other object */
    public double distance(Positionable3D other);

    /** returns position in 3D space */
    public Vector3D getPosition();

    public double getZBufValue();

    public boolean isValid();

    public void setZBufValue(double v);

    public void translate(Vector3D shift);

}
