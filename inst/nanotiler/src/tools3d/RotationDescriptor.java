package tools3d;

public interface RotationDescriptor extends java.io.Serializable {

    public double getAngle();

    public Vector3D getAxis();

    public Vector3D getCenter();

    public void setAngle(double angle);

    public void setAxis(Vector3D axis);

    public void setCenter(Vector3D center);

    /** object is invalid if rotation axis has zero length */
    public boolean isValid();

}
