package tools3d;

/** describes 3d shape. 
 * It has a position and orientation and lengths in all 3 dimensions 
 * This interface is intended to be extended to derive classes 
 * like Cube, Sphere, Cylinder etc.
 */
public interface Shape3D extends Drawable, Orientable {

    /** activate possible actor */
    public void act();

    public Shape3DActor getActor();

    /** creates corresponding geometry (set of faces, edges, points). AngleDelta is difference of face normals */
    public Geometry createGeometry(double angleDelta);

    /** returns name of shape (like "sphere", "cone", "cylinder") */
    public String getShapeName();
    
    /** How is object scaled in x, y, z direction? */
    public Vector3D getScalingFactor();
    
    /** return radius of bounding sphere centered at getPosition() */
    public double getBoundingRadius();

    /** returns length in x, y, z (corresponding to unrotated form) */
    public Vector3D getDimensions();

    /** sets actor (mostly used to set a painter) */
    public void setActor(Shape3DActor actor);

    /** Scale object. */
    public void scale(Vector3D factor);

    /** set radius of bounding sphere, centered at getPosition */
    public void setBoundingRadius(double radius);

    /** sets length in x, y, z (corresponding to unrotated form) */
    public void setDimensions(Vector3D dimensions);

}
