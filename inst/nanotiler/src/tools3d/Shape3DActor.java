package tools3d;

public interface Shape3DActor {

    public void act(Shape3D shape);

}
