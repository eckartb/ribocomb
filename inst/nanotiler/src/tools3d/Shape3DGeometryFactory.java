package tools3d;

public interface Shape3DGeometryFactory {

    /** creates geometry for given 3D shape */
    public Geometry createGeometry(Shape3D shape, double angleDelta);

}
