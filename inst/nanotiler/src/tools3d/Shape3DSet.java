package tools3d;

public interface Shape3DSet extends Shape3D {

    /** returns n'th shape */
    public Shape3D get(int n);
    
    /** returns number of defined shapes */
    public int size();

    /** adds single shape */
    public void add(Shape3D shape);

    /** merges other shapeset */
    public void merge(Shape3DSet shapeSet);

    /** removes single shape */
    public void remove(Shape3D shape);

}
