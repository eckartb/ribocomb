package tools3d;

import java.awt.geom.Point2D;

/** simple z-projection */
public class SimpleCamera implements Camera {

    private int origin2DX = 0;

    private int origin2DY = 0;

    /** position of camera */
    private Vector3D position;

    /** orienation matrix rows x,y,z represent (x,y) photo-plane and z: direction of camera. rows must be normalized, 
	vectors must be perpendicular */
    private Matrix3D orientation;

    private double zoom;

    public SimpleCamera() {
	position = new Vector3D(0.0, -10.0, 0.0);
	orientation = new Matrix3D(1.0);
	zoom = 1.0;
    }

    public SimpleCamera(Vector3D position, Matrix3D orientation, int origin2DX, int origin2DY) {
	this.origin2DX = origin2DX;
	this.origin2DY = origin2DY;
	this.position = position;
	this.orientation = orientation;
	zoom = 1.0;
    }

    /** deep copy */
    public void copy(Camera other) {
	if (other instanceof SimpleCamera) {
	    SimpleCamera c = (SimpleCamera)other;
	    origin2DX = c.origin2DX;
	    origin2DY = c.origin2DY;
	    position.copy(c.position);
	    orientation.copy(c.orientation);
	    zoom = c.zoom;
	}
    }

    /** returns orientation of camera */
    public Matrix3D getOrientation() { return orientation; }

    /** returns position of camera */
    public Vector3D getPosition() { return position; }

    /** returns view direction */
    public Vector3D getViewDirection() { return orientation.getZRow(); }

    /** returns zoom factor */
    public double getZoom() { return zoom; }

    /** central projection method. No zoom factor or perspective taken into account */
    public Point2D project(Vector3D v) {
	double x = Matrix3DTools.project(v, orientation.getXRow(), position);
	double y = Matrix3DTools.project(v, orientation.getYRow(), position);
	x *= zoom;
	y *= zoom;
	x += origin2DX;
	y += origin2DY;
	return new Point2D.Double(x, y);
    }

    /** sets origin in 2d */
    public void setOrigin2D(int x, int y) {
	this.origin2DX = x;
	this.origin2DY = y;
    }

    /** sets position of camera */
    public void setPosition(Vector3D position) { this.position = position; }

    /** sets orientation of camera */
    public void setOrientation(Matrix3D m) { this.orientation = m; }

    /** sets zoom factor */
    public void setZoom(double f) { zoom = f; }

    /** Translates the camera the given number of units along each basis, relative to the camera's orientation */
    public void translate(double x, double y) {
	//System.out.println("Translating camera by (" + x + ", " + y + ")");
	double dx = orientation.getXX()*x + orientation.getYX()*y;
	double dy = orientation.getXY()*x + orientation.getYY()*y;
	double dz = orientation.getXZ()*x + orientation.getYZ()*y;
	//System.out.println("Oriented basis: (" + dx + ", " + dy + ")");

	position.setX(position.getX() + dx);
	position.setY(position.getY() + dy);
	position.setZ(position.getZ() + dz);
    }
	
    
}
