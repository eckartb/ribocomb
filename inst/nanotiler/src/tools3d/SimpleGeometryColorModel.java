package tools3d;

import java.awt.Color;
import java.util.Properties;

public class SimpleGeometryColorModel implements GeometryColorModel {

    Color defaultColor = Color.ORANGE;

    public Color computeColor(Ambiente ambiente,
			      Appearance appearance,
			      Properties properties,
			      Vector3D faceNormal,
			      Camera camera) {
	if (appearance != null) {
	    return appearance.getColor();
	}
	return defaultColor;
    }

    public void setDefaultColor(Color c) {
	defaultColor = c;
    }

}
