package tools3d;

/** describes 3d coordinate system in space. 
 * It has a position and orientation.
 */
public class SimpleOrientable implements Orientable {

    private Vector3D position;
    private double rotationAngle;
    private Vector3D rotationAxis;
    private double zBufValue;

    public SimpleOrientable() {
	position = new Vector3D(0.0, 0.0, 0.0);
	rotationAxis = new Vector3D(1.0, 0.0, 0.0);
	rotationAngle = 0.0;
	zBufValue = 0.0;
    }    

    public void activeTransform(CoordinateSystem cs) {
	assert false; // TODO not yet implemented
    }

    public void passiveTransform(CoordinateSystem cs) {
	assert false; // TODO not yet implemented
    }
    
    /** deep clone */
    public Object clone() {
	Orientable newObj = new SimpleOrientable();
	newObj.setPosition((Vector3D)(this.position.clone()));
	newObj.setRotationAngle(this.rotationAngle);
	newObj.setRotationAxis((Vector3D)(this.rotationAxis.clone()));
	newObj.setZBufValue(this.zBufValue);
	return newObj;
    }

    /** deep clone */
    public Object cloneDeep() { return clone(); }

    /** deep copy */
    public void copyDeep(Orientable other) {
	setPosition((Vector3D)(other.getPosition().clone()));
	setRotationAngle(other.getRotationAngle());
	setRotationAxis((Vector3D)(other.getRotationAxis().clone()));
	setZBufValue(other.getZBufValue());
    }

    /** return distance between objects */
    public double distance(Positionable3D other) {
	return (getPosition().minus(other.getPosition())).length();
    }

    public Vector3D getPosition() { return this.position; }

    /** What is object's rotation axis? */
    public Vector3D getRotationAxis() { return rotationAxis; }
    
    /** How is object rotated? */
    public double getRotationAngle() { return rotationAngle; }

    public double getZBufValue() { return zBufValue; }

    /** this object is "smaller", if it has a smaller zBufValue */
    public int compareTo(Positionable3D obj) {
	double oVal = ((Positionable3D)obj).getZBufValue();
	if (zBufValue < oVal) {
	    return -1;
	}
	else if (zBufValue > oVal) {
	    return 1;
	}
	return 0;
    }

    /** returns true if position is well defined (not Not-a-number) */
    public boolean isValid() { return getPosition().isValid(); }
    
    /** Set position in space. Sets absolute position in space */
    public void setPosition(Vector3D point) { this.position = point; }

    public void setRotationAngle(double angle) { this.rotationAngle = angle; }

    public void setRotationAxis(Vector3D axis) { this.rotationAxis = axis; }

    /** Rotate object. TODO NOT YET IMPLEMENTED */
    public void rotate(Vector3D axis, double angle) { assert false; }
    
    public void rotate(Vector3D center, Matrix3D matrix) { assert false; }

    /** TODO NOT YET IMPLEMENTED */
    public void rotate(Vector3D center, Vector3D axis, double angle) { assert false; }
    
    public void setZBufValue(double zBufValue) { this.zBufValue = zBufValue; }

    /** Translate object. */
    public void translate(Vector3D vec) { this.position = this.position.plus(vec); }
    
}
