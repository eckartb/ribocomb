package tools3d;

public class SimpleRotationDescriptor implements RotationDescriptor {

    private Vector3D axis;

    private Vector3D center;

    private double angle;

    public SimpleRotationDescriptor() {
	this.axis = new Vector3D(0.0, 0.0, 1.0 );
	this.center = new Vector3D(0.0, 0.0, 0.0);
	this.angle = 0.0;
    }

    public SimpleRotationDescriptor(Vector3D axis,
			      Vector3D center,
			      double angle) {
	this.axis = axis;
	this.center = center;
	this.angle = angle;
    }

    public SimpleRotationDescriptor(RotationDescriptor other) {
	this.axis = new Vector3D(other.getAxis());
	this.center = new Vector3D(other.getCenter());
	this.angle = other.getAngle();
    }

    public boolean equals(Object otherObject) {
	if (!(otherObject instanceof RotationDescriptor)) {
	    return false;
	}
	RotationDescriptor other = (RotationDescriptor)otherObject;
	if (getAngle() != other.getAngle()) {
	    return false;
	}
	if (getAngle() == 0.0) { // if rotation angle is zero, all rotation axis are equivalent
	    return true;
	}
	return getAxis().equals(other.getAxis()) && getCenter().equals(other.getCenter());
    }

    public double getAngle() { return this.angle; }

    public Vector3D getAxis() { return axis; }

    public Vector3D getCenter() { return center; }

    public boolean isValid() { 
	return ((axis != null) && (center != null) && (axis.length() > 0));
    }
				   
    public void setAngle(double angle) { this.angle = angle; }

    public void setAxis(Vector3D axis) { this.axis = axis; }

    public void setCenter(Vector3D center) { this.center = center; }



}
