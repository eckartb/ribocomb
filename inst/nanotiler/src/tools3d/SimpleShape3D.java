package tools3d;

import java.util.Properties;

/** implementation of Shape3D
 */
public class SimpleShape3D implements Shape3D {

    private Appearance appearance;

    private boolean geometryUpToDate;

    private boolean selected = false;

    private int number;
    
    private int typeId;
    
    private double boundingRadius;

    private Properties properties = new Properties();

    private Shape3DActor actor = null;
	
    private String className = "Object3D";
	
    protected Geometry geometry;

    private String name;

    private Vector3D dimensions;
    
    private Vector3D relativePosition;
    
    private Vector3D scaling;
    
    private Vector3D rotationAxis;
    
    private double rotationAngle;
    
    // private Vector3D x,y;
    
    // private Object properties;
	
    double zBufValue;

    public SimpleShape3D() {
	appearance = new Appearance(); // standard appearance (color, reflection & diffusion coefficient)
	geometryUpToDate = false;
	selected = false;
	number = 0;
	typeId = 0;
	name = new String();
	relativePosition = new Vector3D(0.0, 0.0, 0.0);
	scaling = new Vector3D(1.0, 1.0, 1.0);
	rotationAxis = new Vector3D(1.0, 0.0, 0.0);
	rotationAngle = 0.0;
	zBufValue = 0.0;
    }
    
    /** activate possible actor */
    public void act() {
	if (actor != null) {
	    actor.act(this);
	}
    }

    public void activeTransform(CoordinateSystem cs) {
	assert false; // TODO : not yet implemented
    }

    public void passiveTransform(CoordinateSystem cs) {
	assert false; // TODO : not yet implemented
    }
    
    public int compareTo(Positionable3D o) {
	double oVal = ((Positionable3D)o).getZBufValue();
	if (zBufValue < oVal) {
	    return -1;
	}
	else if (zBufValue > oVal) {
	    return 1;
	}
	return 0;
    }

    /** creates dummy set of points, edges, faces */
    public Geometry createGeometry(double angleDelta) {
	if (geometry != null) {
	    return geometry;
	}
	// create only single point:
	Point3D[] points = new Point3D[1];
	points[0] = new Point3D(getPosition());
	Edge3D[] edges = new Edge3D[0];
	Face3D[] faces = new Face3D[0];
	geometry = new StandardGeometry(points, edges, faces);
	return geometry;
    }

    /** return distance between objects */
    public double distance(Positionable3D other) {
	return (getPosition().minus(other.getPosition())).length();
    }

    public Shape3DActor getActor() { return actor; }

    public Appearance getAppearance() { return appearance; }

    public String getClassName() {
	return className;
    }

    /** returns length in x,y,z */
    public Vector3D getDimensions() { return dimensions; }

    /** sets geometry */
    void setGeometry(Geometry geometry) { this.geometry = geometry; geometryUpToDate = true; }

    public Properties getProperties() { return properties; }

    public String getShapeName() { return "SimpleShape3D"; }

    /** returns z-buffer priority */
    public double getZBufValue() {
	return zBufValue;
    }
	
    /** returns property object
     * @TODO propert implementation
     */	
//     public Object getProperty(String key) {
// 	return properties;
//     }
	
// 	public Vector3D getXOrientation() {
// 		return x;
// 	}
	
// 	public Vector3D getYOrientation() {
// 		return y;
// 	}
        
    /** returns true if geometry object is consistent with current coordinates */
    protected boolean isGeometryUpToDate() { return geometryUpToDate; }

    /** selected variable is used to implement interactive editor */
    public boolean isSelected() {
	return selected;
    }

    /** returns true if position is well defined (not Not-a-number) */
    public boolean isValid() { return getPosition().isValid(); }
	
// 	public void setOrientation(Vector3D _x, Vector3D _y) {
// 		x.copy(_x);
// 		y.copy(_y);
// 	}


    public void setSelected(boolean f) {
	selected = f;
    }
    
    /** still implement clone method! */
    /*  virtual pointer clone() const = 0; */


    public Object cloneDeep() {
	assert false; // sorry, not implemented yet
	return null;
    }

	  /* PREDICATES */

	  /** Is other equal to us? */
	 /*  virtual bool equal(const Object3D& other) const; */


	  /** Where is object situated? */
	  public Vector3D getPosition() {
	      return relativePosition;
	  }

	  /** What is object's rotation axis? */
	  public Vector3D getRotationAxis() { return rotationAxis; }
	  
	  /** How is object rotated? */
	  public double getRotationAngle() { return rotationAngle; }
	  
	  /** How is object scaled in x, y, z direction? */
	  public Vector3D getScalingFactor() { return scaling; }

	  /** return radius of bounding sphere centered at getPosition() */
	  public double getBoundingRadius() { 
		  return boundingRadius; 
	  }

	  /* MODIFIERS */

	  /** removes all content from object */
	  public void clear() {
		  number = 0;
		  typeId = 0;
	      boundingRadius = 1.0;
		  name = "";
	      relativePosition.mul(0.0);
		  scaling = new Vector3D(1.0, 1.0, 1.0);
	      rotationAxis = new Vector3D(1.0, 1.0, 1.0);
	      rotationAngle = 0.0;
			
	      // x = new Vector3D(1.0, 0.0, 0.0);
	      // y = new Vector3D(0.0, 1.0, 0.0);
	      properties = null;
	  }
	  
    public void setActor(Shape3DActor actor) { this.actor = actor; }

    /** set appearance of object */
    public void setAppearance(Appearance appearance) { this.appearance = appearance; }

    /** set to true if geometry object is consistent with current coordinates */
    protected void setGeometryUpToDate(boolean b) { geometryUpToDate = b; }

    /** returns length in x,y,z */
    public void setDimensions(Vector3D d) {
	this.dimensions = d;
    }

	  /** Set position in space. Sets absolute position in space and
	      translates children accordingly. */
	  public void setPosition(Vector3D point) {
	      relativePosition = point; geometryUpToDate = false;
	  }

    /** sets arbitrary property object
     * @TODO proper implementation
     */
//     public void setProperty(Object p, String key) {
// 	properties = p;
//     }

    public void setRotationAngle(double angle) { this.rotationAngle = angle; }

    public void setRotationAxis(Vector3D axis) { this.rotationAxis = axis; }

    /** returns properties */
    public void setProperties(Properties properties ) { this.properties = properties; }
	  
    /** Rotate object. NOT OK YET! */
    public void rotate(Vector3D axis, double angle) {
	// TODO not yet implemented!
	geometryUpToDate = false;
    }

    public void rotate(Vector3D center, 
		       Vector3D axis, double angle) {
	// TODO not yet implemented
	geometryUpToDate = false;
    }

    public void rotate(Vector3D center, 
		       Matrix3D rotationMatrix) {
	assert false;
	// TODO not yet implemented
	geometryUpToDate = false;
    }

	  /** Translate object. */
	  public void translate(Vector3D vec) {
		  relativePosition.add(vec);
		  geometryUpToDate = false;
	  }

	  /** Scale object. */
	  public void scale(Vector3D factor) {
		  scaling.copy(factor);
		  geometryUpToDate = false;
	  }

	  /** Accept a Visitor. */
	  // virtual void accept(Visitor& v);

	  /** set radius of bounding sphere, centered at getPosition */
	  public void setBoundingRadius(double radius) {
	      boundingRadius = radius;
	      geometryUpToDate = false;
	  }

    /** sets z-buffer priority */
    public void setZBufValue(double d) {
	zBufValue = d;
    }

    /** copy method (deep copy)
     * @TODO not yet implemented 
     */
    public void copy(SimpleShape3D orig) {
	// not yet implemented
    }

    /** TODO not yet implemented ! */
    public String toStringBody() { return ""; }

    public String toString() {
	String result = new String();
	result = "(" + getClassName() + " "  + toStringBody() + " )";
	return result;
    }

	
}

