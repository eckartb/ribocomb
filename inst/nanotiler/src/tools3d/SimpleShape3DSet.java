package tools3d;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import tools3d.Positionable3D;

public class SimpleShape3DSet implements Shape3DSet {

    // TODO not yet implemented!
    private Shape3D shape = new SimpleShape3D(); 

    private List<Shape3D> shapes = new ArrayList<Shape3D>();

    private double zBufValue = 0;

    /** apply active transformation. TODO : not yet implemented. */
    public void activeTransform(CoordinateSystem cs) {
	assert false;
    }

    /** apply active transformation. TODO : not yet implemented. */
    public void passiveTransform(CoordinateSystem cs) {
	assert false;
    }

    public void clear() {
	// TODO
	shape = null; // new SimpleShape3D();
	shapes.clear();
    }

    public Object cloneDeep() { 
	assert false;  // TODO : not yet implemented
	return null; 
    }

    /** returns geometry coresponding to shape set */
    public Geometry createGeometry(double angleDelta) {
	if (size() == 0) {
	    return new StandardGeometry();
	}
	Shape3D lShape = get(0);
	Geometry geometry = lShape.createGeometry(angleDelta);
	for (int i = 1; i < shapes.size(); ++i) {
	    lShape = get(i);
	    geometry.merge(lShape.createGeometry(angleDelta));
	}
	return geometry;
    }

    public int compareTo(Positionable3D o) {
	assert false;
	return 0;
// 	return this.shape.compareTo((Object)o); //unchecked call
    }

    /** return distance between objects */
    public double distance(Positionable3D other) {
	return (getPosition().minus(other.getPosition())).length();
    }
    
    /** returns n'th shape */
    public Shape3D get(int n) {
	return (Shape3D)(shapes.get(n));
    }

    public Vector3D getPosition() { return shape.getPosition(); }

    public double getZBufValue() { return zBufValue; }

    public boolean isSelected() { return shape.isSelected(); }

    /** returns true if position is well defined (not Not-a-number) */
    public boolean isValid() { return getPosition().isValid(); }
    
    /** returns number of defined shapes */
    public int size() {
	return shapes.size();
    }

    /** adds single shape */
    public void add(Shape3D shape) {
	if (shape != null) {
	    shapes.add(shape);
	}
    }

    /** merges other shapeset */
    public void merge(Shape3DSet shapeSet) {
	if (shapeSet == null) {
	    return; // do nothing
	}
	for (int i = 0; i < shapeSet.size(); ++i) {
	    shapes.add(shapeSet.get(i));
	}
    }

    /** removes single shape
     * @TODO NOT YET IMPLEMENTED */
    public void remove(Shape3D shape) { }

    /** activate possible actor */
    public void act() {
	this.shape.act();
	for (int i = 0; i < shapes.size(); ++i) {
	    ((Shape3D)(shapes.get(i))).act();
	}
    }
    public Shape3DActor getActor() {
	return this.shape.getActor();
    }

    public Appearance getAppearance() {
	return this.shape.getAppearance();
    }

    /** returns name of shape (like "sphere", "cone", "cylinder") */
    public String getShapeName() {
	return this.shape.getShapeName();
    }

    /** returns application specific properties */
    public Properties getProperties() {
	return this.shape.getProperties();
    }

    /** What is object's rotation axis? */
    public Vector3D getRotationAxis() {
	return this.shape.getRotationAxis();
    }
    
    /** How is object rotated? */
    public double getRotationAngle() {
	return this.shape.getRotationAngle();
    }
    
    /** How is object scaled in x, y, z direction? */
    public Vector3D getScalingFactor() {
	return this.shape.getScalingFactor();
    }
    
    /** return radius of bounding sphere centered at getPosition() */
    public double getBoundingRadius() {
	return this.shape.getBoundingRadius();
    }

    /** returns length in x, y, z (corresponding to unrotated form) */
    public Vector3D getDimensions() {
	return this.shape.getDimensions();
    }

    /** sets actor (mostly used to set a painter) */
    public void setActor(Shape3DActor actor) {
	this.shape.setActor(actor);
	for (int i = 0; i < shapes.size(); ++i) {
	    ((Shape3D)(shapes.get(i))).setActor(actor);
	}
    }

    public void setAppearance(Appearance appearance) {
	this.shape.setAppearance(appearance);
	for (int i = 0; i < shapes.size(); ++i) {
	    ((Shape3D)(shapes.get(i))).setAppearance(appearance);
	}

    }

    /** Set position in space. Sets absolute position in space */
    public void setPosition(Vector3D point) {
	this.shape.setPosition(point);
    }

    /** sets application specific properties */
    public void setProperties(Properties p) {
	this.shape.setProperties(p);
    }

    public void setRotationAngle(double angle) { shape.setRotationAngle(angle); }

    public void setRotationAxis(Vector3D axis) { shape.setRotationAxis(axis); }

    public void setSelected(boolean b) { shape.setSelected(b); }

    public void setZBufValue(double val) { this.zBufValue = val; }

    /** Rotate object. */
    public void rotate(Vector3D axis, double angle) {
	this.shape.rotate(axis, angle);
	for (int i = 0; i < shapes.size(); ++i) {
	    ((Shape3D)(shapes.get(i))).rotate(axis, angle);
	}
    }
    
    public void rotate(Vector3D center, Matrix3D matrix) { assert false; }

    public void rotate(Vector3D center, Vector3D axis, double angle) {
	this.shape.rotate(center, axis, angle);
	for (int i = 0; i < shapes.size(); ++i) {
	    ((Shape3D)(shapes.get(i))).rotate(center, axis, angle);
	}
    }
    
    /** Scale object. */
    public void scale(Vector3D factor) {
	this.shape.scale(factor);
	for (int i = 0; i < shapes.size(); ++i) {
	    ((Shape3D)(shapes.get(i))).scale(factor);
	}
    }	

    /** set radius of bounding sphere, centered at getPosition */
    public void setBoundingRadius(double radius) {
	this.shape.setBoundingRadius(radius);
    }

    /** sets length in x, y, z (corresponding to unrotated form) */
    public void setDimensions(Vector3D dimensions) {
	this.shape.setDimensions(dimensions);
    }

    /** Translate object. */
    public void translate(Vector3D vec) {
	for (int i = 0; i < shapes.size(); ++i) {
	    ((Shape3D)(shapes.get(i))).translate(vec);
	}
    }	

}
