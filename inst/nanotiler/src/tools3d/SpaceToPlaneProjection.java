/**
 * 
 */
package tools3d;

import java.awt.geom.Point2D;

/**
 * @author Eckart Bindewald
 *
 */
public interface SpaceToPlaneProjection {
	
    public Point2D project(Vector3D v);

}
