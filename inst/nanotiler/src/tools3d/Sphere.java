package tools3d;

import java.util.Properties;
import java.util.logging.Logger;

/** implementation of sphere as Shape3D
 */
public class Sphere extends SimpleShape3D {

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    /** defines sphere with radius and position */
    public Sphere(Vector3D position, double radius) {
	super();
	setPosition(position);
	setBoundingRadius(radius);
	setDimensions(new Vector3D(radius, radius, radius));
    }

    /** creates dummy set of points, edges, faces */
    public Geometry createGeometry(double angleDelta) {
	if (!isGeometryUpToDate()) {
	    updateGeometry(angleDelta);
	}
	return geometry;
    }

    public String getShapeName() { return "Sphere"; }

    protected void updateGeometry(double angleDelta) {
	Vector3D pos = getPosition();
	int nPoints = (int)(2 * Math.PI / angleDelta);
	double radius = getBoundingRadius();
	log.fine("Generating sphere with " + nPoints + " points!");
	Vector3D[] sphereVectors =  Vector3DTools.generateSpherePoints(nPoints, radius, pos);
	double perArrea = (4 * Math.PI)*radius*radius/nPoints; // arrea per point
	// arrea per point A  = Math.PI * r * r -> r = sqrt(A/Math.PI)
	double lengthTarget =  Math.sqrt(perArrea/Math.PI);
	double lengthMin = 0.5*lengthTarget;
	double lengthMax = 2.5*lengthTarget;
	double angleTarget = 60.0 * Tools3DConstants.DEG2RAD;
	double angleMin = 0.9 * angleTarget;
	double angleMax = 1.1 * angleTarget;
	geometry = GeometryTools.triangularize(sphereVectors, angleMin, angleMax,
					       lengthMin, lengthMax);
	log.fine("triang. result: " + geometry.size());
// 	Point3D[] points = new Point3D[nPoints];
// 	for (int i = 0; i < nPoints; ++i) {
// 	    points[i] = new Point3D(sphereVectors[i]);
// 	}
// 	Edge3D[] edges = new Edge3D[0];
// 	Face3D[] faces = new Face3D[0];
// 	geometry = new StandardGeometry(points, edges, faces);
	geometry.setProperties(getProperties());
	geometry.setSelected(isSelected());
	setGeometryUpToDate(true);
    }

}
