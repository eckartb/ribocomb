package tools3d;

import java.util.Properties;

public class StandardGeometry implements Geometry {

    private Point3D[] points;
    private Edge3D[] edges;
    private Face3D[] faces;
    private Properties properties;
    private boolean selected = false;

    public StandardGeometry() {
	this.points = new Point3D[0];
	this.edges = new Edge3D[0];
	this.faces = new Face3D[0];
    }

    public StandardGeometry(Point3D[] points, Edge3D[] edges, Face3D[] faces) {
	this.points = points;
	this.edges = edges;
	this.faces = faces;
    }

    public StandardGeometry(Point3D[] points, Edge3D[] edges, Face3D[] faces, Properties properties) {
	this.points = points;
	this.edges = edges;
	this.faces = faces;
	setProperties(properties);
    }

    public void addToZBuffer(ZBuffer zBuf) {
	for (int i = 0; i < points.length; ++i) {
	    zBuf.add(points[i]);
	}
	for (int i = 0; i < edges.length; ++i) {
	    zBuf.add(edges[i]);
	}
	for (int i = 0; i < faces.length; ++i) {
	    zBuf.add(faces[i]);
	}
    }

    public Edge3D getEdge(int n) {
	return edges[n];
    }

    public int getNumberEdges() {
	return edges.length;
    }

    public int getNumberPoints() {
	return points.length;
    }

    public int getNumberFaces() {
	return faces.length;
    }
    
    public Face3D getFace(int n) {
	return faces[n];
    }

    public Point3D getPoint(int n) {
	return points[n];
    }

    public Properties getProperties() {
	return properties;
    }

    public boolean isSelected() {
	return selected;
    }

    /** add edges points and faces from other geometry */
    public void merge(Geometry other) {
	// TODO: not yet implemented!
    }

    public static Geometry createSinglePointGeometry(Vector3D position) {
	Point3D[] points = new Point3D[1];
	points[0] = new Point3D(position);
	Edge3D[] edges = new Edge3D[0];
	Face3D[] faces = new Face3D[0];
	return new StandardGeometry(points, edges, faces);
    }

    public void setProperties(Properties properties) {
	this.properties = properties;
	if (properties != null) {
	    for (int i = 0; i < points.length; ++i) {
		points[i].setProperties(properties);
	    }
	    for (int i = 0; i < edges.length; ++i) {
		edges[i].setProperties(properties);
	    }
	    for (int i = 0; i < faces.length; ++i) {
		faces[i].setProperties(properties);
	    }
	}
    }

    public void setSelected(boolean b) {
	this.selected = b;
	for (int i = 0; i < points.length; ++i) {
	    points[i].setSelected(b);
	}
	for (int i = 0; i < edges.length; ++i) {
	    edges[i].setSelected(b);
	}
	for (int i = 0; i < faces.length; ++i) {
	    faces[i].setSelected(b);
	}
    }

    /** returns total number of elememts */
    public int size() {
	return (points.length + edges.length + faces.length);
    }

}
