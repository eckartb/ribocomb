package tools3d;

/**
 * centrol superposition method, given by Peter Rotkiewitc
 */
public class Superimpose {

    /** Returns root mean square of two arrays of 3D vectors */
    public static double rms(Vector3D[] v1, Vector3D[] v2) {
	assert v1 != null && v2 != null && v1.length == v2.length && v1.length > 0;
	double sum = 0; 
	for (int i = 0; i < v1.length; ++i) {
	    sum += (v1[i].minus(v2[i])).lengthSquare();
	}
	sum /= v1.length;
	return Math.sqrt(sum);
    }

    /** Superimpose coordinate vectors. Input: coords1, coords2. Output: rot, mc1, mc2
     */
    public static double superimpose(double[][] coords1, double[][] coords2, double[][] rot, double[] mc1, double[] mc2) {
	assert rot != null && rot.length == 3 && rot[0].length == 3;
	assert mc1 != null && mc1.length == 3;
	assert mc2 != null && mc2.length == 3;
	int npoints = coords1.length;
	assert npoints >= 3;
	if (npoints < 3) {
	    return 0.0; // problem, mc1 mc2, rot not specified
	}
	double[][] mat_s = new double[3][3];
	double[][] mat_a = new double[3][3];
	double[][] mat_b = new double[3][3];
	double[][] mat_g = new double[3][3];
	double[][] mat_u = new double[3][3];
	double[][] tmp_mat = new double[3][3];
	double val, d, d2, alpha, beta, gamma, x, y, z;
	double cx1, cy1, cz1, cx2, cy2, cz2, tmpx, tmpy, tmpz;
	int i, j, k, n;
	int NUMBER_STEPS_MAX = 1000; // maximum number of optimization steps
	int stepCounter = 0;
	cx1=cy1=cz1=cx2=cy2=cz2=0.;
	
	for (i=0; i<npoints; i++) {
	    cx1+=coords1[i][0];
	    cy1+=coords1[i][1];
	    cz1+=coords1[i][2];
	    cx2+=coords2[i][0];
	    cy2+=coords2[i][1];
	    cz2+=coords2[i][2];
	}
	
	cx1/=(double)npoints;
	cy1/=(double)npoints;
	cz1/=(double)npoints;
	
	cx2/=(double)npoints;
	cy2/=(double)npoints;
	cz2/=(double)npoints;
	
	for (i=0; i<npoints; i++) {
	    coords1[i][0]-=cx1;
	    coords1[i][1]-=cy1;
	    coords1[i][2]-=cz1;
	    coords2[i][0]-=cx2;
	    coords2[i][1]-=cy2;
	    coords2[i][2]-=cz2;
	}
	
	for (i=0; i<3; i++)
	    for (j=0; j<3; j++) {
		if (i==j)
		    mat_s[i][j]=mat_a[i][j]=mat_b[i][j]=mat_g[i][j]=1.0;
		else
		    mat_s[i][j]=mat_a[i][j]=mat_b[i][j]=mat_g[i][j]=0.0;
		mat_u[i][j]=0.;
	    }
	
	for (n=0; n<npoints; n++) {
	    mat_u[0][0]+=coords1[n][0]*coords2[n][0];
	    mat_u[0][1]+=coords1[n][0]*coords2[n][1];
	    mat_u[0][2]+=coords1[n][0]*coords2[n][2];
	    mat_u[1][0]+=coords1[n][1]*coords2[n][0];
	    mat_u[1][1]+=coords1[n][1]*coords2[n][1];
	    mat_u[1][2]+=coords1[n][1]*coords2[n][2];
	    mat_u[2][0]+=coords1[n][2]*coords2[n][0];
	    mat_u[2][1]+=coords1[n][2]*coords2[n][1];
	    mat_u[2][2]+=coords1[n][2]*coords2[n][2];
	}
	
	for (i=0; i<3; i++)
	    for (j=0; j<3; j++)
		tmp_mat[i][j]=0.;
	
	do {
	    d=mat_u[2][1]-mat_u[1][2];
	    d2 = mat_u[1][1]+mat_u[2][2];
	    if ((d==0.0) || d2 == 0.0) {
		alpha=0.0; 
	    }
	    else {
		alpha=Math.atan(d/d2); // /(mat_u[1][1]+mat_u[2][2]));
	    }
	    if (Math.cos(alpha)*(mat_u[1][1]+mat_u[2][2])+Math.sin(alpha)*(mat_u[2][1]-mat_u[1][2])<0.0) alpha+=Math.PI;
	    mat_a[1][1]=mat_a[2][2]=Math.cos(alpha);
	    mat_a[2][1]=Math.sin(alpha);
	    mat_a[1][2]=-mat_a[2][1];
	    for (i=0; i<3; i++)
		for (j=0; j<3; j++)
		    for (k=0; k<3; k++)
			tmp_mat[i][j]+=mat_u[i][k]*mat_a[j][k];
	    for (i=0; i<3; i++)
		for (j=0; j<3; j++) {
		    mat_u[i][j]=tmp_mat[i][j];
		    tmp_mat[i][j]=0.;
		}
	    for (i=0; i<3; i++)
		for (j=0; j<3; j++)
		    for (k=0; k<3; k++)
			tmp_mat[i][j]+=mat_a[i][k]*mat_s[k][j];
	    for (i=0; i<3; i++)
		for (j=0; j<3; j++) {
		    mat_s[i][j]=tmp_mat[i][j];
		    tmp_mat[i][j]=0.;
		}
	    d=mat_u[0][2]-mat_u[2][0];
	    d2 = mat_u[0][0]+mat_u[2][2];
	    if ((d==0.0) || (d2 == 0.0)) {
		beta=0.0; 
	    }
	    else {
		beta=Math.atan(d/d2);
	    }
	    if (Math.cos(beta)*(mat_u[0][0]+mat_u[2][2])+Math.sin(beta)*(mat_u[0][2]-mat_u[2][0])<0.0) beta+=Math.PI;
	    mat_b[0][0]=mat_b[2][2]=Math.cos(beta);
	    mat_b[0][2]=Math.sin(beta);
	    mat_b[2][0]=-mat_b[0][2];
	    for (i=0; i<3; i++)
		for (j=0; j<3; j++)
		    for (k=0; k<3; k++)
			tmp_mat[i][j]+=mat_u[i][k]*mat_b[j][k];
	    for (i=0; i<3; i++)
		for (j=0; j<3; j++) {
		    mat_u[i][j]=tmp_mat[i][j];
		    tmp_mat[i][j]=0.;
		}
	    for (i=0; i<3; i++)
		for (j=0; j<3; j++)
		    for (k=0; k<3; k++)
			tmp_mat[i][j]+=mat_b[i][k]*mat_s[k][j];
	    for (i=0; i<3; i++)
		for (j=0; j<3; j++) {
		    mat_s[i][j]=tmp_mat[i][j];
		    tmp_mat[i][j]=0.;
		}
	    d=mat_u[1][0]-mat_u[0][1];
	    d2 = mat_u[0][0]+mat_u[1][1];
	    if ((d==0.0) || (d2 == 0.0)) {
		gamma=0.0; 
	    }
	    else {
		gamma=Math.atan(d/d2);
	    }
	    if (Math.cos(gamma)*(mat_u[0][0]+mat_u[1][1])+Math.sin(gamma)*(mat_u[1][0]-mat_u[0][1])<0.0) gamma+=Math.PI;
	    mat_g[0][0]=mat_g[1][1]=Math.cos(gamma);
	    mat_g[1][0]=Math.sin(gamma);
	    mat_g[0][1]=-mat_g[1][0];
	    for (i=0; i<3; i++)
		for (j=0; j<3; j++)
		    for (k=0; k<3; k++)
			tmp_mat[i][j]+=mat_u[i][k]*mat_g[j][k];
	    for (i=0; i<3; i++)
		for (j=0; j<3; j++) {
		    mat_u[i][j]=tmp_mat[i][j];
		    tmp_mat[i][j]=0.;
		}
	    for (i=0; i<3; i++)
		for (j=0; j<3; j++)
		    for (k=0; k<3; k++)
			tmp_mat[i][j]+=mat_g[i][k]*mat_s[k][j];
	    for (i=0; i<3; i++)
		for (j=0; j<3; j++) {
		    mat_s[i][j]=tmp_mat[i][j];
		    tmp_mat[i][j]=0.;
		}
	    val=Math.abs(alpha)+Math.abs(beta)+Math.abs(gamma);
	} while ((val>0.0001) && (stepCounter++ < NUMBER_STEPS_MAX)); // added additional break when too many steps (Eckart Bindewald 2004)
	
	val=0.;
	for (i=0; i<npoints; i++) {
	    x=coords2[i][0];
	    y=coords2[i][1];
	    z=coords2[i][2];
	    tmpx=x*mat_s[0][0]+y*mat_s[0][1]+z*mat_s[0][2];
	    tmpy=x*mat_s[1][0]+y*mat_s[1][1]+z*mat_s[1][2];
	    tmpz=x*mat_s[2][0]+y*mat_s[2][1]+z*mat_s[2][2];
	    x=coords1[i][0]-tmpx;
	    y=coords1[i][1]-tmpy;
	    z=coords1[i][2]-tmpz;
	    val+=x*x+y*y+z*z;
	}
	
	for (i=0; i<npoints; i++) {
	    coords1[i][0]+=cx1;
	    coords1[i][1]+=cy1;
	    coords1[i][2]+=cz1;
	    coords2[i][0]+=cx2;
	    coords2[i][1]+=cy2;
	    coords2[i][2]+=cz2;
	}
	
	// write back results:
	for (i=0; i<3; i++) {
	    for (j=0; j<3; j++) {
		rot[i][j]=mat_s[i][j];
	    }
	}
	mc1[0]=cx1;
	mc1[1]=cy1;
	mc1[2]=cz1;
	mc2[0]=cx2;
	mc2[1]=cy2;
	mc2[2]=cz2;

	return Math.sqrt(val/(double)npoints);
    }

    /** Superimpose coordinate vectors. Input: coords1, coords2. Output: rot, mc1, mc2
     */
    public static double superimpose(Vector3D[] _coords1, Vector3D[] _coords2, Matrix3D _rot, Vector3D _mc1, Vector3D _mc2) {
	assert _coords1 != null && _coords1 != null && _coords1.length == _coords2.length && _coords1.length >= 3;
	assert _rot != null;
	double[][] coords1 = new double[_coords1.length][3]; 
	double[][] coords2 = new double[_coords2.length][3]; 
	double[][] rot = new double[3][3];
	double[] mc1 = new double[3];
	double[] mc2 = new double[3];
	for (int i = 0; i < coords1.length; ++i) {
	    coords1[i][0] = _coords1[i].getX();
	    coords1[i][1] = _coords1[i].getY();
	    coords1[i][2] = _coords1[i].getZ();
	    coords2[i][0] = _coords2[i].getX();
	    coords2[i][1] = _coords2[i].getY();
	    coords2[i][2] = _coords2[i].getZ();
	}
	double result = superimpose(coords1, coords2, rot, mc1, mc2);
	// write back results:
	_rot.copy(new Matrix3D(rot[0][0], rot[0][1], rot[0][2], rot[1][0], rot[1][1], rot[1][2], rot[2][0], rot[2][1], rot[2][2]));
	_mc1.copy(new Vector3D(mc1[0], mc1[1], mc1[2]));
	_mc2.copy(new Vector3D(mc2[0], mc2[1], mc2[2]));
	return result;
    }

    /** Modify points 2 such that they superpose to points 1.
     * First step: */
    public static Vector3D applyPoint2(Vector3D point, Matrix3D rot, Vector3D mc1, Vector3D mc2) {
	Vector3D dv = mc1.minus(mc2);
	// return rot.multiply(point.plus(dv)); // FIXIT verify
	return (rot.multiply(point.minus(mc2))).plus(mc1); // FIXIT verify
    }

    /** Modify points 2 such that they superpose to points 1 */
    public static Vector3D[] applyPoints2(Vector3D[] points, Matrix3D rot, Vector3D mc1, Vector3D mc2) {
	Vector3D[] result = new Vector3D[points.length];
	for (int i = 0; i < result.length; ++i) {
	    result[i] = applyPoint2(points[i], rot, mc1, mc2);
	}
	return result;
    }


}
