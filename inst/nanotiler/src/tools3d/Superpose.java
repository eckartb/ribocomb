package tools3d;

/** Strategy pattern for superposition */
public interface Superpose {

    /** change varCoord such that it closest to constCoord using
     * only tranlation and rotation */
    SuperpositionResult superpose(Vector3D[] constCoord,
				  Vector3D[] varCoord);

}
