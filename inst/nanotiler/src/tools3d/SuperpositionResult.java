package tools3d;

/** Contains result of superposition algorithm. */
public class SuperpositionResult {

    private Matrix3D rotationMatrix;
    
    private Vector3D[] newCoords;

    private Vector3D shift1;

    private Vector3D shift2;

    private double rms;

    SuperpositionResult() {
	shift1 = new Vector3D();
	shift2 = new Vector3D();
	rotationMatrix = new Matrix3D(1.0);
	rms = 0.0;
    }

    /** Create result object from Kabsch algorithm object, assuming its "align" method has been run */
    SuperpositionResult(Kabsch kabsch) {
	shift1 = new Vector3D(kabsch.getCm1());
	shift2 = new Vector3D(kabsch.getCm2());
	rotationMatrix = new Matrix3D(kabsch.getRotationMatrix());
	rms = kabsch.getRMSD();
    }

    public Matrix3D getRotationMatrix() { return new Matrix3D(rotationMatrix); }

    /** returns true if current transformation is fully defined */
    public boolean isValid() { 
	return ((rotationMatrix != null) //  && (newCoords != null) 
		&& (shift1 != null) && (shift2 != null) ); 
    }

    public void setRotationMatrix(Matrix3D m) { rotationMatrix = new Matrix3D(m); }

    public Vector3D[] getNewCoords() { return newCoords; }

    public void setNewCoords(Vector3D[] coords) { this.newCoords = coords; }

    public Vector3D getShift1() { return new Vector3D(shift1); }

    public Vector3D getShift2() { return new Vector3D(shift2); }

    public void setShift1(Vector3D shift1) { this.shift1 = new Vector3D(shift1); }

    public void setShift2(Vector3D shift2) { this.shift2 = new Vector3D(shift2); }

    public double getRms() { return rms; }

    public void setRms(double rms) { this.rms = rms; }

    public String toString() { return "" + rms + " " + shift1 + " " + shift2 + " " 
				   + rotationMatrix + " transformed coords: "
				   + newCoords; }

    /** applies transformation to 3D object such that second object is superposed to first object.
     * TODO : not yet verified! */
    public void applyTransformation(Orientable obj) {
	assert (isValid());
	Vector3D origPos = obj.getPosition();
	Vector3D zeroVec = new Vector3D(0,0,0);
	// assert false; // not yet implemented
	// Vector3D totShift = shift2.minus(shift1);
	obj.translate(shift2.mul(-1));
	// Vector3D totShift = shift1.minus(shift2);
	// obj.translate(totShift);
// 	double[] axisAngle = Matrix3DTools.convertRotationMatrixToAxisAngle(rotationMatrix);
// 	Vector3D axis = new Vector3D(axisAngle[0], axisAngle[1], axisAngle[2]);
// 	double angle360 = axisAngle[3] * (180.0 / Math.PI);
 	// obj.rotate(zeroVec, axis, axisAngle[3]); // TODO : test!
	obj.rotate(zeroVec, rotationMatrix);
	obj.translate(shift1);
	Vector3D newPos = obj.getPosition();
	assert returnTransformed(origPos).distance(newPos) < 0.1; // check if algorithm ok
    }

    /** applies transformation to 3D object such that second object is superposed to first object.
     * TODO : not yet verified! */
    public void applyTransformation_old(Orientable obj) {
	assert (isValid());
	// assert false; // not yet implemented
	// Vector3D totShift = shift2.minus(shift1);
	Vector3D totShift = shift1.minus(shift2);
	obj.translate(totShift);
	double[] axisAngle = Matrix3DTools.convertRotationMatrixToAxisAngle(rotationMatrix);
	Vector3D axis = new Vector3D(axisAngle[0], axisAngle[1], axisAngle[2]);
	double angle360 = axisAngle[3] * (180.0 / Math.PI);
 	obj.rotate(axis, axisAngle[3]); // TODO : test!
    }

    /** applies transformation to 3D vector such that second object is superposed to first object.
     * TODO : not yet verified! */
    public void applyTransformation_old(Vector3D v) {
	assert (isValid());
	// assert false; // not yet implemented
	// Vector3D totShift = shift2.minus(shift1);
	Vector3D totShift = shift1.minus(shift2);
	v.add(totShift);
	double[] axisAngle = Matrix3DTools.convertRotationMatrixToAxisAngle(rotationMatrix);
	Vector3D axis = new Vector3D(axisAngle[0], axisAngle[1], axisAngle[2]);
	double angle = axisAngle[3];
	// double angle360 = axisAngle[3] * (180.0 / Math.PI);
	//  	obj.rotate(axis, axisAngle[3]); // TODO : test!
	Vector3D newPos = Matrix3DTools.rotate(v, axis, angle);
	v.copy(newPos);
    }

    /** applies transformation to 3D vector such that second object is superposed to first object.
     * TODO : not yet verified! */
    public void applyTransformation(Vector3D v) {
	assert (isValid());
	// assert false; // not yet implemented
	// Vector3D totShift = shift2.minus(shift1);
	v.sub(shift2);
	// Vector3D totShift = shift1.minus(shift2);
	// v.add(totShift);
	
// 	double[] axisAngle = Matrix3DTools.convertRotationMatrixToAxisAngle(rotationMatrix);
// 	Vector3D axis = new Vector3D(axisAngle[0], axisAngle[1], axisAngle[2]);
// 	double angle = axisAngle[3];
	 
	// double angle360 = axisAngle[3] * (180.0 / Math.PI);
	//  	obj.rotate(axis, axisAngle[3]); // TODO : test!

	// Vector3D newPos = Matrix3DTools.rotate(v, axis, angle);
	Vector3D newPos = rotationMatrix.multiply(v);


	v.copy(newPos);
	v.add(shift1);
    }

    public Vector3D returnTransformed(Vector3D v) {
	Vector3D newPos = new Vector3D(v);
	applyTransformation(newPos);
	return newPos;
    }

}
