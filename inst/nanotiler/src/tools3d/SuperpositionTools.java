package tools3d;

import java.util.Random;
import generaltools.Randomizer;

public class SuperpositionTools {

    private static Random rand = Randomizer.getInstance();

    /** generates random vector of length len */
    public static Vector3D generateRandomVector(double len) {
	assert len > 0.0;
	return generateRandomVector(len, rand);
    }

    /** generates random vector of length len */
    public static Vector3D generateRandomVector(double len, Random rand) {
	assert len > 0.0;
	double phi = 2.0*Math.PI * rand.nextDouble(); // angle between 0 and 2PI
	double theta = Math.PI * rand.nextDouble(); // angle between 0 and 2PI
	double r = len;
	double x = r * Math.cos(phi)*Math.sin(theta);
	double y = r * Math.sin(phi)*Math.sin(theta);
	double z = r * Math.cos(theta);
	Vector3D result = new Vector3D(x,y,z);
	assert Math.abs(result.length() - len) < 0.01; // must be approximately length len
	return result;
    }

    public static Matrix3D generateRandomMatrix() {
	Vector3D dir = generateRandomVector(1.0, rand); // random direction
	double angle = 2.0*Math.PI*rand.nextDouble();
	return Matrix3D.rotationMatrix(dir, angle);
    }

    /** multiplies with random matrix, center of gravity might change. */
    public static Matrix3D randomRotate(Vector3D[] v) {
	Matrix3D mtx = generateRandomMatrix();
	for (int i = 0; i < v.length; ++i) {
	    v[i] = mtx.multiply(v[i]);
	}
	return mtx;
    }


    /** multiplies with random matrix, keeps center of gravity constant. */
    public static Matrix3D randomRotateAroundOrigin(Vector3D[] v) {
	Matrix3D mtx = generateRandomMatrix();
	Vector3D center = Vector3DTools.computeCenterGravity(v);
	for (int i = 0; i < v.length; ++i) {
	    v[i].sub(center);
	    v[i] = mtx.multiply(v[i]);
	    v[i].add(center);
	}
	return mtx;
    }

}
