package tools3d;

public class SymmetryLink {
    
//     private String junctionName;
//     private String juncSymName;
//     private String pointName;
//     private String pointSymName;

    private Junction junction;
    private Junction junctionSym;
    private Point point;
    private Point pointSym;
    //    private Symmetry symmetry;

    public SymmetryLink() { }

    public SymmetryLink(Junction junction, Point point, Junction junctionSym, Point pointSym, Symmetry symmetry) {
	setJunction((Junction)junction.cloneDeep());
	setPoint((Point)point.cloneDeep());
	setJunctionSym((Junction)junctionSym.cloneDeep(), (Symmetry)symmetry.cloneDeep());
	setPointSym((Point)pointSym.cloneDeep(), (Symmetry)symmetry.cloneDeep());
	//	setSymmetry(symmetry);
    }

//     public SymmetryLink( String junctionName, String pointName, String juncSymName, String pointSymName ) {
// 	setJunctionName(junctionName);
// 	setPointName(pointName);
// 	setJuncSymName(juncSymName);
// 	setPointSymName(pointSymName);
//     }

    public Object cloneDeep() {
	SymmetryLink sl = new SymmetryLink();
	sl.copyDeepThisCore(this);
	return sl;
    }

    protected void copyDeepThisCore(SymmetryLink sl) {	
	this.junction = (Junction)sl.junction.cloneDeep();
	this.point = (Point)sl.point.cloneDeep();
	this.junctionSym = (Junction)sl.junctionSym.cloneDeep();
	this.pointSym = (Point)sl.pointSym.cloneDeep();
    }

//     protected void copyDeepThisCore(SymmetryLink sl) {	
// 	this.junctionName = sl.junctionName;
// 	this.pointName = sl.pointName;
// 	this.juncSymName = sl.juncSymName;
// 	this.pointSymName = sl.pointSymName;
//     }

    public Junction getJunction() {
	return junction;
    }

    public Junction getJunctionSym() {
	return junctionSym;
    }

    public Point getPoint() {
	return point;
    }

    public Point getPointSym() {
	return pointSym;
    }

    public void setJunction(Junction junction) {
	this.junction = junction;
    }

    public void setJunctionSym(Junction junctionSym, Symmetry symmetry) {
	Vector3D start = symmetry.getStartPoint().getVector3D();
	Vector3D end = symmetry.getEndPoint().getVector3D();
	Vector3D axis = end.minus(start);
	junctionSym.rotate(start, axis, (Math.PI * 2) / (symmetry.getType() ) );
	this.junctionSym = junctionSym;
    }

    public void setPoint(Point point) {
	this.point = point;
    }
    
    public void setPointSym(Point pointSym, Symmetry symmetry) {
	Vector3D start = symmetry.getStartPoint().getVector3D();
	Vector3D end = symmetry.getEndPoint().getVector3D();
	Vector3D axis = end.minus(start);
	pointSym.rotate(start, axis, (Math.PI * 2) / (symmetry.getType() ) );
	this.pointSym = pointSym;
    }

//     public String getJuncSymName() { return juncSymName; }
//     public String getJunctionName() { return this.junctionName; }
//     public String getPointName() { return pointName; }
//     public String getPointSymName() { return pointSymName; }

//     public void setJuncSymName( String juncSymName ) { this.juncSymName = juncSymName; }
//     public void setJunctionName( String junctionName ) { this.junctionName = junctionName; }
//     public void setPointName( String pointName ) { this.pointName = pointName; }
//     public void setPointSymName( String pointSymName ) { this.pointSymName = pointSymName; }

    public String toString() {
	String s = new String("( SymmetryLink: " + point + " in Junction " + junction.getName() + " links to " + point + " in Junction " + junctionSym.getName() + " )" );
	return s;
    }

}
