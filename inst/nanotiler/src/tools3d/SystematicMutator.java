package tools3d;

import java.util.Random;
import generaltools.Randomizer;
import java.util.logging.*;
import graphtools.*;

/** randomly translates and rotates object */
public class SystematicMutator extends IntegerArrayGenerator implements OrientableModifier, IntegerPermutator  {

    private static final int X_ID = 0; // index of x
    private static final int Y_ID = 1; // index of y
    private static final int Z_ID = 2; // index of z
    private static final int ANGLE_ID = 3; // index of angle counter
    private static final int DEFAULT_SIZE = 4;

    private static Logger log = Logger.getLogger(PackageConstants.LOGFILE_DEFAULT);

    private double angleStep;
    private Vector3D fixedAxis;
    private double translationStep;
    private Vector3D minPos;
    private boolean rotationFlag = false;

    public SystematicMutator() { } // FIXIT 

    public SystematicMutator(double translationStep, Vector3D minPos,
			     int[] maxNumbers, // int nx, int ny, int nz, int na,
			     Vector3D axis) {
	super(maxNumbers);
	assert maxNumbers.length == DEFAULT_SIZE;
	this.translationStep = translationStep;
	assert(maxNumbers[ANGLE_ID] > 0);
	assert axis != null;
	this.angleStep = 2.0 * Math.PI / maxNumbers[ANGLE_ID]; // angleStep;
	this.minPos = minPos;
	this.fixedAxis = new Vector3D(axis);
	assert validate();
    }

    public boolean validate() {
	return hasNext() && size() == DEFAULT_SIZE && fixedAxis != null 
	    && fixedAxis.length() > 0.0 && minPos != null;
    }

    /** copies member variables to this object */
    public void copy(SystematicMutator other) {
	super.copy(other);
	this.angleStep = other.angleStep;
	this.fixedAxis = other.fixedAxis;
	this.translationStep = other.translationStep;
	this.minPos = other.minPos;
	this.rotationFlag = other.rotationFlag;
    }
    
    public void rotate(Orientable obj) {
	assert false;
    }
    
    public double getAngleStep() { return this.angleStep; }

    public double getTranslationStep() { return this.translationStep; }

    public void setAngleStep(double d) { this.angleStep = d; assert validate(); }

    public void setFixedAxis(Vector3D axis) { assert(axis != null); this.fixedAxis = new Vector3D(axis); }

    public void setTranslationStep(double d) { this.translationStep = d; assert validate(); }

    public void mutate(Orientable obj) {
	int[] ids = getNumbers();
	if (rotationFlag) {
	    obj.rotate(fixedAxis, ids[ANGLE_ID] * angleStep);
	}
	Vector3D pos = new Vector3D(minPos);
	pos.setX(pos.getX() + ids[X_ID] * translationStep);
	pos.setY(pos.getY() + ids[Y_ID] * translationStep);
	pos.setZ(pos.getZ() + ids[Z_ID] * translationStep);
	obj.setPosition(pos);
    }

    public void reset() {
	super.reset();
	this.rotationFlag = false;
    }

    public void scaleAngleStep(double scale) { angleStep *= scale; }

    public void scaleTranslationStep(double scale) { translationStep *= scale; }

    public void translate(Orientable obj) {
	assert false; // please call mutate
    }

    public String toString() {
	return "" + translationStep + " " + (PackageConstants.RAD2DEG * angleStep) 
	    + " " + minPos.toString() + " " + super.toString();
    }

    /** Increase counters, return true if not starting over but really increasing values */
    public boolean inc() {
	int nai = getNumbers()[ANGLE_ID];
	boolean result = super.inc();
	this.rotationFlag = (getNumbers()[ANGLE_ID] != nai); // if rotation has changed, rotate next mutate operato
	return result;
    }

}
