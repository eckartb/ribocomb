package tools3d;

import java.util.Random;
import generaltools.Randomizer;
import java.util.logging.*;
import graphtools.*;

/** randomly translates and rotates object; Similar to SystematicMutator, however, the rotation axis is also being modified systematically */
public class SystematicMutator2 extends IntegerArrayGenerator implements OrientableModifier, IntegerPermutator  {

    public static final int X_ID = 0; // index of x
    public static final int Y_ID = 1; // index of y
    public static final int Z_ID = 2; // index of z
    public static final int ANGLE_ID = 3; // index of angle counter
    public static final int AXIS_ID = 4; // index of axis counter
    public static final int DEFAULT_SIZE = 5;

    private static Logger log = Logger.getLogger(PackageConstants.LOGFILE_DEFAULT);

    private double angleStep;
    private Vector3D[] fixedAxis;
    private double translationStep;
    private Vector3D minPos;
    // private boolean rotationFlag = false;

    public SystematicMutator2() { }

    public SystematicMutator2(double translationStep, Vector3D minPos,
			     int[] maxNumbers, // int nx, int ny, int nz, int na,
			     Vector3D axis) {
	super(maxNumbers);
	assert maxNumbers.length == DEFAULT_SIZE;
	this.translationStep = translationStep;
	assert(maxNumbers[ANGLE_ID] > 0);
	assert axis != null;
	this.angleStep = 2.0 * Math.PI / maxNumbers[ANGLE_ID]; // angleStep;
	this.minPos = new Vector3D(minPos);
	this.fixedAxis = new Vector3D[1];
	maxNumbers[AXIS_ID] = 1; // there is only one axis
	fixedAxis[0] = new Vector3D(axis); // Vector3DTools.generateSpherePoints(maxNumbers[AXIS_ID], 1.0, Vector3D.ZVEC);
	assert validate();
    }

    public SystematicMutator2(double translationStep, Vector3D minPos,
			      int[] maxNumbers) { // int nx, int ny, int nz, int na,
	super(maxNumbers);
	assert maxNumbers.length == DEFAULT_SIZE;
	this.translationStep = translationStep;
	assert(maxNumbers[ANGLE_ID] > 0);
	this.angleStep = 2.0 * Math.PI / maxNumbers[ANGLE_ID]; // angleStep;
	this.minPos = new Vector3D(minPos);
	this.fixedAxis = Vector3DTools.generateSpherePoints(maxNumbers[AXIS_ID], 1.0, Vector3D.ZVEC);
	assert validate();
    }

    public SystematicMutator2(SystematicMutator2 other) {
	copy(other);
    }

    public Object clone() {
	SystematicMutator2 result = new SystematicMutator2(this);
	return result;
    }

    public boolean validate() {
	return super.validate() && hasNext() && size() == DEFAULT_SIZE && fixedAxis != null 
	    && fixedAxis.length > 0.0 && minPos != null;
    }

    /** copies member variables to this object */
    public void copy(SystematicMutator2 other) {
	assert other != null;
	assert(other.validate());
	super.copy(other);
	this.angleStep = other.angleStep;
	if ((this.fixedAxis == null) || (this.fixedAxis.length != other.fixedAxis.length)) {
	    this.fixedAxis = new Vector3D[other.fixedAxis.length];
	}
	for (int i = 0; i < other.fixedAxis.length; ++i) {
	    if (this.fixedAxis[i] == null) {
		this.fixedAxis[i] = new Vector3D();
	    }
	    this.fixedAxis[i].copy(other.fixedAxis[i]);
	}
	this.translationStep = other.translationStep;
	if (minPos == null) {
	    minPos = new Vector3D(other.minPos);
	} else {
	    this.minPos.copy(other.minPos);
	}
	// this.rotationFlag = other.rotationFlag;
    }
    
    public void rotate(Orientable obj) {
	assert false;
    }
    
    public double getAngleStep() { return this.angleStep; }

    public double getTranslationStep() { return this.translationStep; }

    public void setAngleStep(double d) { this.angleStep = d; assert validate(); }

    // public void setFixedAxis(Vector3D axis) { assert(axis != null); this.fixedAxis = new Vector3D(axis); }

    public void setTranslationStep(double d) { this.translationStep = d; assert validate(); }

    public void mutate(Orientable obj) {
	int[] ids = getNumbers();
	if (ids[ANGLE_ID] != 0) {
	    obj.rotate(fixedAxis[ids[AXIS_ID]], ids[ANGLE_ID] * angleStep); // full rotation, assume that position will be reset
	}
	Vector3D pos = new Vector3D(minPos);
	pos.setX(pos.getX() + ids[X_ID] * translationStep);
	pos.setY(pos.getY() + ids[Y_ID] * translationStep);
	pos.setZ(pos.getZ() + ids[Z_ID] * translationStep);
	obj.setPosition(pos);
    }

    // public void reset() {
    // super.reset();
    // this.rotationFlag = false;
    // }

    public void scaleAngleStep(double scale) { angleStep *= scale; }

    public void scaleTranslationStep(double scale) { translationStep *= scale; }

    public void translate(Orientable obj) {
	assert false; // please call mutate
    }

    public String toString() {
	assert minPos != null;
	return "(SystematicMutator2 " + translationStep + " " + (PackageConstants.RAD2DEG * angleStep) 
	    + " " + minPos.toString() + " " + super.toString() + " )";
    }

    /** Increase counters, return true if not starting over but really increasing values */
    public boolean inc() {
	int nai = getNumbers()[ANGLE_ID];
	boolean result = super.inc();
	// this.rotationFlag = (getNumbers()[ANGLE_ID] != nai); // if rotation has changed, rotate next mutate operato
	return result;
    }

}
