package tools3d;

import java.util.*;
import tools3d.Vector3D;
//import tools3d.Vector3DTools;
import tools3d.KabschSuperpose;
import tools3d.SuperpositionResult;

public class TertiaryStructureSimilarity {
  
  private KabschSuperpose superposer = new KabschSuperpose ();
  private Vector3D [] skeleton1;
  private Vector3D [] skeleton2; //skeleton refers to onlyt he carbon atoms of a structure
  
  public TertiaryStructureSimilarity(Vector3D [] s1, Vector3D [] s2){
    this.skeleton1 = s1;
    this.skeleton2 = s2;
  }

  public double findSimilarity(){
    //return Vector3DTools.computeRms (skeleton1, skeleton2);
    SuperpositionResult result = superposer.superpose(skeleton1, skeleton2); 
    return result.getRms (); //returns RMS of structure
  }

}
