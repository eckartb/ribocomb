package tools3d;

public class Tools3DConstants {

    public static final double DEG2RAD = Math.PI / 180.0;

    public static final double RAD2DEG = 180.0 / Math.PI;

}
