package tools3d;

import java.util.logging.Logger;
import numerictools.DoubleTools;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Vector3D implements java.io.Serializable {

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    public static final double SIMILAR_CUTOFF = 0.00001;
    // public static final double SIMILAR_CUTOFF_OLD = 6;

    /** Cartesian vectors */
    public static final Vector3D ZVEC = new Vector3D(0.0, 0.0, 0.0);
    public static final Vector3D EX = new Vector3D(1.0, 0.0, 0.0);
    public static final Vector3D EY = new Vector3D(0.0, 1.0, 0.0);
    public static final Vector3D EZ = new Vector3D(0.0, 0.0, 1.0);
    
    double x, y, z;
    int index; //TODO: CEV
    
    public Vector3D() { x = 0.0; y = 0.0; z = 0.0; }
    
    public Vector3D(Vector3D other) { copy(other); }
    
    public Vector3D(double _x, double _y, double _z) {
	x = _x; y = _y; z = _z;
    }
    
    public Vector3D(Vector4D v) {
    	x = v.getX();
    	y = v.getY();
    	z = v.getZ();
    }

    public Vector3D(double x,
		    double y,
		    double z,
		    int index) {
	this.x = x;
	this.y = y;
	this.z = z;
	this.index = index;
    }

    public boolean isOrigin() {
	if (getX() == 0 && getY() == 0 && getZ() == 0) {
	    return true;
	}
	return false;
    }

    public boolean isReasonable() {
	return DoubleTools.isReasonable(x) && DoubleTools.isReasonable(y) && DoubleTools.isReasonable(z);
    }
	
    /** returns true if no Not-a-number values in coordinates */
    public boolean isValid() {
	return !(Double.isNaN(x) || Double.isNaN(y) || Double.isNaN(z));
    }

    public boolean equals(Vector3D v) {
	if (getX() == v.getX() && getY() == v.getY() && getZ() == v.getZ()) {
	    return true;
	}
	return false;
    }
    
    /** adds other vector */
    public void add(Vector3D a) {
    	x += a.x; y += a.y; z += a.z;
    }

    /** returns middle position between vectors v1 and v2 */
    public static Vector3D average(Vector3D v1, Vector3D v2) {
	return v1.plus(v2).mul(0.5);
    }
   
    /** overwrite Object.clone() method. "Deep" clone. */
    public Object clone() {
    	 return new Vector3D(x, y, z);
    }

//     public static boolean similarOld(double a, double b) {
// 	return Math.abs(a-b) < SIMILAR_CUTOFF_OLD;
//     }

    public static boolean similar(double a, double b) {
	return Math.abs(a-b) < SIMILAR_CUTOFF;
    }

    /** overwrite standard equals method */
    
    public boolean similar(Object other) {
	if (this == other) {
	    return true;
	}
	if (! (other instanceof Vector3D)) {
	    return false;
	}
	Vector3D v = (Vector3D)other;
	return similar(x,v.x) && similar(y,v.y) && similar(z, v.z);
    }

//     public boolean similarOld(Object other) {
// 	if (this == other) {
// 	    return true;
// 	}
// 	if (!(other instanceof Vector3D)) {
// 	    return false;
// 	}
// 	Vector3D v = (Vector3D)other;
// 	return similarOld(x,v.x) && similarOld(y,v.y) && similarOld(z,v.z);
//     }

    /** returns vector as copied array. TODO : using caching for speedup */
    public double[] getArrayCopy() {
	double[] ary = new double[3];
	ary[0] = x;
	ary[1] = y;
	ary[2] = z;
	return ary;
    }
    
    public double getX() { return x; }
    
    public double getY() { return y; }
    
    public double getZ() { return z; }

    /** returns angle between vector in radians */
    public double angle(Vector3D other) {
	double len = length();
	double len2 = other.length();
	if ((len <= 0.0) || (len2 <= 0.0)) {
	    return 0.0; // technically undefined
	}
	// cosine of angle:
	double cosa = dot(other) / (len *len2);
	if (cosa > 1.0) {
	    cosa = 1.0;
	}
	else if (cosa < -1.0) {
	    cosa = -1.0;
	}
	return Math.acos(cosa);
    }

    /** Computes torsion angle between v1-v2 and v4-v3 . FIXIT: check with official definition! */
    public static double torsionAngle(Vector3D v1, Vector3D v2, Vector3D v3, Vector3D v4) {
	Vector3D va = v1.minus(v2);
	Vector3D vb = v4.minus(v3);
	return va.angle(vb);
    }
    
    /** Generates vector in xy plane corresponding to angle r */
    private static Vector3D genAngleVec(double angle, double r) {
	double x = r * Math.cos(angle);
	double y = r * Math.sin(angle);
	double z = 0.0;
	return new Vector3D(x,y,z);
    }

    /** Returns difference between two angles, measured in radians. Difference between 0 and 2PI is 0 */
    public static double angleDiff(double angle1, double angle2) {
	double r = 1.0;
	Vector3D v1 = genAngleVec(angle1, r);
	Vector3D v2 = genAngleVec(angle2, r);
	//	assert (testAngleDiff());
	return v1.angle(v2);
    }

    /** Returns difference between two angles, measured in radians. Difference between 0 and 2PI is 0 */
    public static double angleNorm(double angle) {
	double r = 1.0;
	Vector3D v1 = genAngleVec(angle, r);
	Vector3D v0 = genAngleVec(0.0, r);
	double result = v1.angle(v0);
	if (v1.getY() < 0) {
	    result = - result;
	}
	return result;
    }

    /** Test method for angleDiff */
    public static void testAngleDiff() {
	double tol = 0.001;
	assert (angleDiff(0.0, 2*Math.PI) < tol);
	assert (Math.abs(angleDiff(0.0, Math.PI) - Math.PI) < tol);
    }

    /** returns angle with current object being in "middle" */
    public double angle(Vector3D other1, Vector3D other2) {
	Vector3D dv1 = other1.minus(this);
	Vector3D dv2 = other2.minus(this);
	return dv1.angle(dv2);
    }



    /** returns distance */
    public double distance(Vector3D other) {
	assert isValid() && other.isValid();
	return (this.minus(other)).length();
    }

    /** returns distance */
    public double distanceSquare(Vector3D other) {
	assert isValid();
	assert other.isValid();
	return (this.minus(other)).lengthSquare();
    }

    /** adds other vector */
    public Vector3D plus(Vector3D a) {
    	return new Vector3D(x + a.x, y + a.y, z + a.z);
    }
   
    
    /** subtracts other vector */
    public void sub(Vector3D a) {
    	x -= a.x; y -= a.y; z -= a.z;
    }

    public void swap(Vector3D a) {
	double h = x;
	x = a.x;
	a.x = h;
	h = y;
	y = a.y;
	a.y = h;
	h = z;
	z = a.z;
	a.z = h;
    }
    
    /** adds other vector */
    public Vector3D minus(Vector3D a) {
    	return new Vector3D(x - a.x, y - a.y, z - a.z);
    }
   
    /** multiplies new object with real number */
    public Vector3D mul(double a) {
	return new Vector3D(x * a, y * a, z * a);
    }
    
    /** multiplies this object with real number */
    public void scale(double a) {
    	x *= a; y *= a; z *= a;	
    }

    /** multiplies this object with three different real numbers */
    public void scale(Vector3D v) {
    	x *= v.getX(); y *= v.getY(); z *= v.getZ();	
    }
    
    /** maximum norm */
    public double max() {
	double result = Math.abs(x);
	if (Math.abs(y) > result) {
	    result = Math.abs(y);
	}
	if (Math.abs(z) > result) {
	    result = Math.abs(z);
	}
	return result;
    }

    public static Vector3D mean(Vector3D a, Vector3D b) {
	Vector3D result = a.plus(b);
	result.scale(0.5);
	return result;
    }

    public void normalize() {
	double len = length();
	if (len > 0.0) {
	    scale(1.0/len);
	}
// 	if (length() > 1.01) {
// print warning message
// 	}

    }

    /** Rotates point described by this vector around center, axis and angle */
    public void rotate(Vector3D center, Vector3D axis, double angle) {
	if (angle == 0.0) { return; }
	double distOrig = distance(center);
	Vector3D position = new Vector3D(this.getX(), this.getY(), this.getZ());
	Vector3D shiftedPos = position.minus(center);
	Vector3D rotatedPos = Matrix3DTools.rotate(shiftedPos, axis, angle);
	rotatedPos.add(center);
	this.set(rotatedPos.getX(), rotatedPos.getY(), rotatedPos.getZ());
	double distNew = distance(center);
	if (Math.abs(distOrig - distNew) > 0.01) { // distance to rotation center should not change
	    log.warning("Strange rotation: " + position + " " + center + " " + axis + " " + Math.toDegrees(angle)
			+ " : " + rotatedPos + " distances: " + distOrig + " " + distNew); 
	}
	assert Math.abs(distOrig - distNew) < 0.01; // distance to rotation center should not change
    }

    @Test public void testRotate() {
	Vector3D testVector = new Vector3D(1, 1, 1);
	testVector.rotate(new Vector3D(0, 0, 0), new Vector3D(0, 1, 0), Math.PI);
	assert testVector.similar(new Vector3D(-1, 1, -1));
    }

    /** returns dot product */
    public double dot(Vector3D a) {
    	return (x*a.x) + (y*a.y) + (z*a.z);
    }

    /** returns cross product */
    public Vector3D cross(Vector3D a) {
	return new Vector3D(y*a.z-z*a.y, z*a.x - x*a.z, x*a.y-y*a.x);
    }
    
    /** returns square root of dot product with self */
    public double length() {
	assert isValid();
    	return Math.sqrt(dot(this));
    }

    /** returns dot product with self */
    public double lengthSquare() {
	assert isValid();
    	double result = dot(this);
	assert !(Double.isNaN(result) || Double.isInfinite(result) || (result < 0));
	return result;
    }
    
    /** copies content of other object to this object */
    public void copy(Vector3D a) { 
	x = a.x; y = a.y; z = a.z; 
    }
        
    public void set(double x, double y, double z) {
	this.x = x;
	this.y = y;
	this.z = z;
    }

    public void setArray(double[] v, int start) {
	assert (start+2) < v.length;
	v[start] = getX();
	v[start+1] = getY();
	v[start+2] = getZ();
    }


    public void setX(double _x) { x = _x; }
    
    public void setY(double _y) { y = _y; }
    
    public void setZ(double _z) { z = _z; }
    
    public String toString() {
	return "(v3 " + getX() + " " + getY() + " " + getZ() + " )";
    }

    /* UNIT TESTING */

     @BeforeClass
       public void setUp() {
        // code that will be invoked when this test is instantiated
     }

    @Test
    public void testPlus() {
	Vector3D v1 = new Vector3D(1,0,0);
	Vector3D v2 = new Vector3D(0,2,0);
	Vector3D v3 = v1.plus(v2);
	// log.fine("Result of " + v1 + " .plus( " + v2 + " ) = "
	// + v3);
	assert(v3.similar(new Vector3D(1, 2, 0)));
    }
    
}
