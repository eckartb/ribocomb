package tools3d;

import generaltools.Randomizer;
import freeware.PrintfFormat; // used in CDK package
import java.util.Arrays;
import java.util.Random;
import Jama.*;
import numerictools.DoubleArrayTools;

/** Mostly static helper methods for Vector3D class */
public class Vector3DTools {

    public static final String HASH_DELIM = "_";

    private static Random rnd = Randomizer.getInstance();

    /** generates vector according to polar coordinates phi, theta, r */
    public static Vector3D generateCartesianFromPolar(double phi, double theta, double r) {
	return new Vector3D(r*Math.cos(phi)*Math.sin(theta),
			    r*Math.sin(phi)*Math.sin(theta),
			    r*Math.cos(theta));
    }
    
    /** generates vector of length 1 with randomized direction */
    public static Vector3D generateRandomDirection() {
	assert rnd != null;
	double phi = 2.0 * Math.PI * rnd.nextDouble();
	double theta = Math.PI * rnd.nextDouble(); 
	Vector3D result = generateCartesianFromPolar(phi, theta, 1.0);
	assert result.isValid();
	return result;
    }

    /** generates vector of that is orthogonal to v, using parts of p */
    public static Vector3D generateOrthogonalDirection(Vector3D vOrig, Vector3D pOrig) {
	assert (vOrig.lengthSquare() > 0.0);
	assert (pOrig.lengthSquare() > 0.0);
	Vector3D v = new Vector3D(vOrig);
	Vector3D p = new Vector3D(pOrig);
	v.normalize();
	p.normalize();
	Vector3D result = p.minus(v.mul(p.dot(v)));
	assert (result.lengthSquare() > 0.0);
	result.normalize();
	assert (Math.abs(result.dot(vOrig)) < 0.01); // check if really orthogonal
	return result;
    }

    /** generates vector of length 1 with randomized direction */
    public static Vector3D generateRandomOrthogonalDirection(Vector3D vOrig) {
	assert (vOrig.lengthSquare() > 0.0);
	Vector3D v = new Vector3D(vOrig);
	v.normalize();
	Vector3D p = null;
	do {
	    p = generateRandomDirection();
	} while ( (p.cross(v)).lengthSquare() == 0.0 ); // try again if parallel
	return generateOrthogonalDirection(v, p);
    }

    /** generate set of n points with radius r around origin vector */
    public static Vector3D[] generateSpherePoints(int n,
				     double r, Vector3D origin) {
	Vector3D[] ray = new Vector3D[n];
	if (n == 0) {
	    return ray;
	}
	double h,theta,phi;
	double oldPhi = 0.0;
	for (int i = 1; i < n-1; ++i) {
	    ray[i] = new Vector3D();
	    h = -1.0 + 2.0 * i / (n-1.0);
	    // ASSERT((h>=-1.0)&&(h<=1.0), exception);
	    theta = Math.acos(h);
	    phi = (oldPhi + 3.6/Math.sqrt(n*(1-h*h)));
	    ray[i].setX(Math.cos(phi) * Math.sin(theta));
	    ray[i].setY(Math.sin(phi)*Math.sin(theta));
	    ray[i].setZ(Math.cos(theta));
	    oldPhi = phi;
	}
	ray[0] = new Vector3D(0.0,0.0,-1.0);
	ray[n-1] = new Vector3D(0.0,0.0,1.0);
	for (int i = 0; i < ray.length; ++i) {
	    ray[i].scale(r);
	    ray[i].add(origin);
	    // ray[i] = (ray[i] * r) + origin;
	}
	return ray;
    }

    /** computes distance root mean square */
    public static double computeDrms(Vector3D[] v1, Vector3D[] v2) {
	double result = 0.0;
	double term = 0.0;
	for (int i = 0; i < v1.length; ++i) {
	    for (int j = i+1; j < v1.length; ++j) {
		term = (v1[i].minus(v1[j])).length()-(v2[i].minus(v2[j])).length();
		result += (term * term);
	    }
	}
	double norm = 0.5 * v1.length * (v1.length-1);
	result /= norm;
	result = Math.sqrt(result);
	return result;
    }

    /** computes root mean square between two vector array, NOT attempting to superpose them */
    public static double computeRms(Vector3D[] v1, Vector3D[] v2) {
	double result = 0.0;
	double term = 0.0;
	for (int i = 0; i < v1.length; ++i) {
	    result += v1[i].distanceSquare(v2[i]);
	}
	result /= v1.length;
	result = Math.sqrt(result);
	return result;
    }

    /** computes center of gravity of vector set */
    public static Vector3D computeCenterGravity(Vector3D[] v) {
	assert v.length > 0;
	Vector3D sum = new Vector3D(0,0,0);
	for (int i = 0; i < v.length; ++i) {
	    sum.add(v[i]);
	}
	sum.scale(1.0/v.length);
	return sum;
    }

    /** computes sorted array of all vector-vector angles */
    public static double[] computeVectorFieldSignature(Vector3D[] directions) {
	int n = directions.length;
	int n2 = (n * (n - 1)) / 2;
	double[] result = new double[n2];
	int pc = 0;
	for (int i = 0; i < n; ++i) {
	    for (int j = 0; j < i; ++j) {
		result[pc++] = directions[i].angle(directions[j]);
	    }
	}
	Arrays.sort(result); // sort in ascending order
	assert(pc == result.length);
	return result;
    }

    /** computes score indicating how different vector fields are. Score zero corresponds to perfect match */
    public static double computeVectorFieldSimilarityScore(Vector3D[] directions1, Vector3D[] directions2) {
	assert (directions1.length > 1);
	assert (directions1.length == directions2.length);
	double[] sig1 = computeVectorFieldSignature(directions1);
	double[] sig2 = computeVectorFieldSignature(directions2);
	double maxDiff = Math.abs(sig1[0]-sig2[0]);
	for (int i = 1; i < sig1.length; ++i) {
	    double diff = Math.abs(sig1[i]-sig2[i]);
	    if (diff > maxDiff) {
		maxDiff = diff;
	    }
	}
	assert (maxDiff >= 0.0);
	return maxDiff;
    }

    public static String prettyString(Vector3D vector, int len, int digits) {
	assert digits >= 0;
	PrintfFormat positionFormat = new PrintfFormat("%" + len + "." + digits + "f");
	return "" + positionFormat.sprintf(vector.getX()) + " "
	    + positionFormat.sprintf(vector.getY()) + " "
	    + positionFormat.sprintf(vector.getZ());
    }

    private static void updateMomentOfInertiaMatrix(Matrix3D m, Vector3D v, double mass, Vector3D rotationCenter) {
	Vector3D d = v.minus(rotationCenter);
	double xx = mass * d.getX() * d.getX();
	double xy = mass * d.getX() * d.getY();
	double xz = mass * d.getX() * d.getZ();
	double yx = mass * d.getY() * d.getX();
	double yy = mass * d.getY() * d.getY();
	double yz = mass * d.getY() * d.getZ();
	double zx = mass * d.getZ() * d.getX();
	double zy = mass * d.getZ() * d.getY();
	double zz = mass * d.getZ() * d.getZ();
	m.setXX(m.getXX()+xx);
	m.setXY(m.getXY()+xy);
	m.setXZ(m.getXZ()+xz);
	m.setYX(m.getYX()+yx);
	m.setYY(m.getYY()+yy);
	m.setYZ(m.getYZ()+yz);
	m.setZX(m.getZX()+zx);
	m.setZY(m.getZY()+zy);
	m.setZZ(m.getZZ()+zz);
    }

    public static Matrix3D getMomentOfInertiaMatrix(Vector3D[] v, double[] masses, Vector3D rotationCenter) {
	assert v.length == masses.length;
	Matrix3D result = new Matrix3D(0.0);
	for (int i = 0; i < v.length; ++i) {
	    updateMomentOfInertiaMatrix(result, v[i], masses[i], rotationCenter);
	}
	return result;
    }

    public static Matrix3D getMomentOfInertiaMatrix(Vector3D[] v, double[] masses) {
	Vector3D rotationCenter = computeCenterGravity(v);
	return getMomentOfInertiaMatrix(v, masses, rotationCenter);
    }

    /** returns radii of gyration of body */
    public static double[] getRadiiOfGyration(Vector3D[] v, double[] masses) {
	assert v.length > 0;
	assert v.length == masses.length;
	Matrix3D inertiaMatrix = getMomentOfInertiaMatrix(v, masses);
	Matrix matrix = new Matrix(inertiaMatrix.toArray()); // generate JAMA matrix
	EigenvalueDecomposition decomp = new EigenvalueDecomposition(matrix);
	double[] eigenValues = decomp.getRealEigenvalues();
	assert eigenValues.length == 3;
	double totalMass = DoubleArrayTools.computeSum(masses);
	assert totalMass > 0.0;
	for (int i = 0; i < eigenValues.length; ++i) {
	    eigenValues[i] = Math.sqrt(eigenValues[i]/totalMass);
	}
	// sort eigenvalues, such that highest eigenvalue is first:
	Arrays.sort(eigenValues);
	double tmp = eigenValues[0]; // swap elements 0 and 2:
	eigenValues[0] = eigenValues[2];
	eigenValues[2] = tmp;
	return eigenValues;
    }

    public static String computeEpsilonHash(double x, double epsilon) {
	return "" + Math.round(x / epsilon);
    }

    public static String computeEpsilonHash(Vector3D v, double epsilon) {
	return computeEpsilonHash(v.getX(), epsilon) + HASH_DELIM
	    + computeEpsilonHash(v.getY(), epsilon) + HASH_DELIM
	    + computeEpsilonHash(v.getZ(), epsilon);
    }

    public static String computeEpsilonHash(Vector4D v, double epsilon) {
	return computeEpsilonHash(v.getX(), epsilon) + HASH_DELIM
	    + computeEpsilonHash(v.getY(), epsilon) + HASH_DELIM
	    + computeEpsilonHash(v.getZ(), epsilon);
    }
    
    
    public static double capsuleIntersectionVolume(Vector3D base1, Vector3D top1, double r1, Vector3D base2, Vector3D top2, double r2){
      final double epsilon = 0.0001;
      final Vector3D epsilonVec = new Vector3D(epsilon, epsilon, epsilon); 
      double rTot = r1 + r2;
      Vector3D axis1 = top1 .minus( base1);
      Vector3D axis2 = top2 .minus( base2);
      Vector3D dir1 = axis1;
      Vector3D dir2 = axis2;
      dir1.normalize();
      dir2.normalize();
      Vector3D p11 = base1 .plus ( dir1.mul(r1) );
      Vector3D p12 = top1 .minus ( dir1.mul(r1) );
      Vector3D p21 = base2 .plus ( dir2.mul(r1) );
      Vector3D p22 = top2 .minus ( dir2.mul(r1) );
      double h1 = axis1.length();
      double h2 = axis2.length();

      double d1 = 2.0 * r1;
      double d2 = 2.0 * r2;

      double v1 = Math.PI * r1 * r1 * h1;
      double v2 = Math.PI * r2 * r2 * h2;
      double vMax = v1;
      if (v2 < v1) {
        vMax = v2;
      }
      Vector3D distResult;
      double dp1 = p11.distance(p12);
      double dp2 = p21.distance(p22);
      if(dp1 > 0.0){
        if(dp2 > 0.0){
          distResult = computeLineSegmentDistance(p22, p12, p21, p22);
        } else{
          distResult = computeLineSegmentDistance(p11, p12, p21, p21 .plus( epsilonVec));
        }
      } else{
        if(dp2> 0.0){
          distResult = computeLineSegmentDistance(p11, p11 .plus( epsilonVec), p21, p22);
        } else{
          distResult = computeLineSegmentDistance(p11, p11 .plus( epsilonVec), p21, p21 .plus( epsilonVec ));
        }
      }
      
      double d = distResult.getZ();
      if(d>rTot){
        return 0.0;
      } 
      double result = vMax - (d*(vMax / rTot));
      
      if(result<0.0){
        result = 0.0;
      } 
      
      System.out.println ("The result is " + result);
      return result;
    } 

   public static double computeSphereVolume(double r) {
      return Math.pow (r, 3) * Math.PI * (4.0/3.0);
    }
       
    /** Computes interection volumn between two spheres.
     * @param base1  Position of first sphere.
      * @param top1 
    * @see 
    */
    public static double computeSphereIntersectionVolume(Vector3D base1, Vector3D base2, double r1, double r2) { //r1, r2 the radius 
      
      assert (r1 > 0.0); // PRECONDITION
      assert (r2 > 0.0);
      
      double result = 0.0;
        
      double baseXDist = Math.abs (base1.getX () - base2.getX ()); //finds the x, y, z distance of the axis
      double baseYDist = Math.abs (base1.getY () - base2.getY ());
      double baseZDist = Math.abs (base1.getZ () - base2.getZ ());
      
       // (x^2 + y^2 + z^2) ^ 0.5
       //absolute distance between the center of two spheres;
      double baseDistance = base1.distance(base2); // Math.pow (Math.pow (baseXDist, 2) + Math.pow (baseYDist, 2) + Math.pow (baseZDist, 2), 0.5);
      
      //V^2=π/12d * (r1+r2−d)^2 * (d2 + 2d(r1+r2) − 3(r1−r2)^2) gives the intersecting volume of two spheres d distance apart
      if (baseDistance == 0.0) {
          
          if (r2 >= r1) { 
            result = computeSphereVolume (r1);
          } else if ( r1 > r2) {
            result = computeSphereVolume (r2);
          } 
        } else if (baseDistance < (r2 + r1)) {
           result = ((Math.PI/(12.0 * baseDistance)) * Math.pow ((r1 + r2 - baseDistance), 2.0) * (Math.pow (baseDistance, 2.0) + 2.0 * baseDistance * (r1 + r2) - 3.0 * Math.pow ((r1 - r2) , 2.0)));
        } // else result=0
        
      // POSTCONDITION
      System.out.println ("Result of sphere intersection volume: " +result);
      assert (result >= 0); 
      assert (result <= computeSphereVolume(r2)); // (Math.pow (r2, 3) * Math.PI * (4.0/3.0))); // result can never be bigger than volumn of second sphere
      assert (result <= computeSphereVolume(r1)); // (Math.pow (r1, 3.0) * Math.PI * (4/3))); 
      return result;

    } 

    /** Computes interection volumn between two spheres.
     * @param base1  Position of first sphere.
      * @param top1 
    * @see 
    */
    
    public static Vector3D randomPointInSphere (double r) { //returns random point within sphere given its radius  
      double theta, phi;
      r = Math.pow (Math.pow (r, 2) * rnd.nextDouble (), 0.5);
      theta = rnd.nextDouble () * 2 * Math.PI; //angle in radians
      phi = rnd.nextDouble () * 2 * Math.PI;
      return generateCartesianFromPolar (phi, theta, r);     
    }
    
    public static double computeSphereIntersectionVolume (Vector3D base1, Vector3D top1, double r1, Vector3D base2, Vector3D top2,  double r2) { //r1, r2 the radius 
      return computeSphereIntersectionVolume(base1, base2, r1, r2) + computeSphereIntersectionVolume(top1, top2, r1, r2);
    }

    public static Vector3D computeLineDistance(Vector3D x0, Vector3D x, Vector3D y0, Vector3D y){
      double A = x.lengthSquare();
      double B = 2.0 * ((x.dot( x0)) - (x.dot(y0)));  
      double C = 2.0 * (x.dot(y));
      double D = 2.0 * ((y.dot(y0) - (y.dot(x0))));
      double E = y.lengthSquare();
      double F = x0.lengthSquare() + y0.lengthSquare();
      double denom = (C*C -4*A*E);
      if(denom == 0.0){
        double dist = distanceToLine(x0, y0, y);
        double lt =0;
        Vector3D v = x0 .minus( y0 );
        double d0 = v.length();
        if(d0 ==  0.0){
          //return 0.0;
          Vector3D zero = new Vector3D (0,0,0);
          //return null; //TODO return 0,0,0 vector instead of null
          return zero;
        }
        double alpha = v.angle(y);
        double ls = Math.cos(alpha) * d0;
        return new Vector3D(lt, ls, dist);
      }
      double s = ((2*A*D) + (B*C)) / denom;
      double t = (C*s - B) / (2.0*A);
      double d = Math.sqrt( (B*C*D+B*B*E+C*C*F+A*(D*D-4.0*E*F))/denom );
      return new Vector3D(t, s, d);
    }
    
    
    public static Vector3D computeLineSegmentDistance(Vector3D a0, Vector3D a1, Vector3D b0, Vector3D b1){
      Vector3D dirA = a1.minus(a0);
      Vector3D dirB = b1.minus(b0);
      double lenA = dirA.length();
      double lenB = dirB.length();
      dirA.normalize();
      dirB.normalize();
      Vector3D distVec = computeLineDistance(a0, dirA, b0, dirB);
      double t = distVec.getX();
      double s = distVec.getY();
      if(t<0.0) {
        t= 0.0;
      }
      else if(t>lenA){
        t= lenA;
      }
      if(s< 0.0){
        s = 0.0;
      }
      else if( s>lenB){
        s = lenB;
      }
      Vector3D pA = a0 .plus (dirA .mul( t ));
      Vector3D pB = b0 .plus (dirB .mul( t ));
      double d = pA.distance( pB );
      return new Vector3D(t,s,d);
    }

    //positive result, if it projects to positive half line, negative otherwise
    public static double distanceToLine(Vector3D point, Vector3D offset, Vector3D direction){
      double result = 0.0;
      Vector3D dVec = point .minus( offset );
      double dLength = dVec.length();
      if (dLength == 0.0) {
        return 0.0;
      }
      double cosAlpha = dVec .dot( direction) / (dLength*direction.length());
      double alpha = Math.acos(cosAlpha);
      result = dLength * Math.sin(alpha);
      if(cosAlpha < 0.0){
        result *= -1.0;
      }
      return result;
    }
}
