package tools3d;

import java.util.logging.Logger;
import numerictools.DoubleTools;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/** 4D vector, mostly used to implement homogeneous coordinates. */
public class Vector4D implements java.io.Serializable {

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    public static final double SIMILAR_CUTOFF = 0.00001;
    // public static final double SIMILAR_CUTOFF_OLD = 6;

    double x, y, z, w;
    // int index; //TODO: CEV
    
    public Vector4D() { x = 0.0; y = 0.0; z = 0.0; w = 1.0; }

    public Vector4D(Vector3D other) { x = other.getX(); y = other.getY(); z = other.getZ(); w = 1.0; }
    
    public Vector4D(Vector4D other) { copy(other); }
    
    public Vector4D(double _x, double _y, double _z) {
	x = _x; y = _y; z = _z; w = 1.0;
    }

    public Vector4D(double _x, double _y, double _z, double _w) {
	x = _x; y = _y; z = _z; w = _w;
    }

    public boolean isReasonable() {
	return DoubleTools.isReasonable(x) && DoubleTools.isReasonable(y) && DoubleTools.isReasonable(z) && DoubleTools.isReasonable(w);
    }
	
    /** returns true if no Not-a-number values in coordinates */
    public boolean isValid() {
	return !(Double.isNaN(x) || Double.isNaN(y) || Double.isNaN(z) || Double.isNaN(w) || Double.isInfinite(x) || Double.isInfinite(y) 
		 || Double.isInfinite(z) || Double.isInfinite(w));
    }

    public boolean equals(Vector4D v) {
	if (getX() == v.getX() && getY() == v.getY() && getZ() == v.getZ() && getW() == v.getW()) {
	    return true;
	}
	return false;
    }
    
    /** adds other vector */
    public void add(Vector4D a) {
    	x += a.x; y += a.y; z += a.z; w += a.w;
    }

    /** returns middle position between vectors v1 and v2 */
    public static Vector4D average(Vector4D v1, Vector4D v2) {
	return v1.plus(v2).mul(0.5);
    }
   
    /** overwrite Object.clone() method. "Deep" clone. */
    public Object clone() {
	return new Vector4D(x, y, z, w);
    }

//     public static boolean similarOld(double a, double b) {
// 	return Math.abs(a-b) < SIMILAR_CUTOFF_OLD;
//     }

    public static boolean similar(double a, double b) {
	return Math.abs(a-b) < SIMILAR_CUTOFF;
    }

    /** overwrite standard equals method */
    
    public boolean similar(Object other) {
	if (this == other) {
	    return true;
	}
	if (! (other instanceof Vector4D)) {
	    return false;
	}
	Vector4D v = (Vector4D)other;
	return similar(x,v.x) && similar(y,v.y) && similar(z, v.z) && similar(w,v.w);
    }

//     public boolean similarOld(Object other) {
// 	if (this == other) {
// 	    return true;
// 	}
// 	if (!(other instanceof Vector4D)) {
// 	    return false;
// 	}
// 	Vector4D v = (Vector4D)other;
// 	return similarOld(x,v.x) && similarOld(y,v.y) && similarOld(z,v.z);
//     }

    /** returns vector as copied array. TODO : using caching for speedup */
    public double[] getArrayCopy() {
	double[] ary = new double[4];
	ary[0] = x;
	ary[1] = y;
	ary[2] = z;
	ary[3] = w;
	return ary;
    }
    
    public double getX() { return x; }
    
    public double getY() { return y; }
    
    public double getZ() { return z; }

    public double getW() { return w; }

    /** returns angle */
    public double angle(Vector4D other) {
	double len = length();
	double len2 = other.length();
	if ((len <= 0.0) || (len2 <= 0.0)) {
	    return 0.0; // technically undefined
	}
	// cosine of angle:
	double cosa = dot(other) / (len *len2);
	if (cosa > 1.0) {
	    cosa = 1.0;
	}
	else if (cosa < -1.0) {
	    cosa = -1.0;
	}
	return Math.acos(cosa);
    }

    /** returns angle with current object being in "middle" */
    public double angle(Vector4D other1, Vector4D other2) {
	Vector4D dv1 = other1.minus(this);
	Vector4D dv2 = other2.minus(this);
	return dv1.angle(dv2);
    }

    /** returns distance */
    public double distance(Vector4D other) {
	return (this.minus(other)).length();
    }

    /** returns distance */
    public double distanceSquare(Vector4D other) {
	assert isValid();
	assert other.isValid();
	return (this.minus(other)).lengthSquare();
    }

    /** adds other vector */
    public Vector4D plus(Vector4D a) {
    	return new Vector4D(x + a.x, y + a.y, z + a.z, w + a.w);
    }
   
    
    /** subtracts other vector */
    public void sub(Vector4D a) {
    	x -= a.x; y -= a.y; z -= a.z; w -= a.w;
    }
    
    public Vector3D subVector() {
	return new Vector3D(x,y,z);
    }

    /** adds other vector */
    public Vector4D minus(Vector4D a) {
    	return new Vector4D(x - a.x, y - a.y, z - a.z, w - a.w);
    }
   
    /** multiplies new object with real number */
    public Vector4D mul(double a) {
	return new Vector4D(x * a, y * a, z * a, w * a);
    }
    
    /** multiplies this object with real number */
    public void scale(double a) {
    	x *= a; y *= a; z *= a;	w *= a;
    }

    /** multiplies this object with three different real numbers */
    public void scale(Vector4D v) {
    	x *= v.getX(); y *= v.getY(); z *= v.getZ(); w *= v.getW();	
    }
    
    public static Vector4D mean(Vector4D a, Vector4D b) {
	Vector4D result = a.plus(b);
	result.scale(0.5);
	return result;
    }

    public void normalize() {
	double len = length();
	if (len > 0.0) {
	    scale(1.0/len);
	}
// 	if (length() > 1.01) {
// print warning message
// 	}
    }

    /*
    public void rotate(Vector4D center, Vector4D axis, double angle) {
	if (angle == 0.0) { return; }
	Vector4D position = new Vector4D(this.getX(), this.getY(), this.getZ());
	Vector4D shiftedPos = position.minus(center);
	Vector4D rotatedPos = Matrix3DTools.rotate(shiftedPos, axis, angle);
	rotatedPos.plus(center);
	this.set(rotatedPos.getX(), rotatedPos.getY(), rotatedPos.getZ());
    }

    @Test public void testRotate() {
	Vector4D testVector = new Vector4D(1, 1, 1);
	testVector.rotate(new Vector4D(0, 0, 0), new Vector4D(0, 1, 0), Math.PI);
	assert testVector.similar(new Vector4D(-1, 1, -1));
    }
    */

    /** returns dot product */
    public double dot(Vector4D a) {
    	return (x*a.x) + (y*a.y) + (z*a.z) + (w*a.w);
    }

    /** returns square root of dot product with self */
    public double length() {
	assert isValid();
    	return Math.sqrt(dot(this));
    }

    /** returns dot product with self */
    public double lengthSquare() {
	assert isValid();
    	double result = dot(this);
	return result;
    }
    
    /** copies content of other object to this object */
    public void copy(Vector4D a) { 
	x = a.x; y = a.y; z = a.z; w = a.w;
    }

    /** Embeds 3D vector in 4D homogenious coordinates. */
    public void copy(Vector3D a) { 
	x = a.x; y = a.y; z = a.z; w = 1.0;
    }
        
    public void set(double x, double y, double z, double w) {
	this.x = x;
	this.y = y;
	this.z = z;
	this.w = w;
    }
    
    public Vector4D cross(Vector4D v) {
    	Vector4D ret = new Vector4D(0.0, 0.0, 0.0, 0.0);
    	ret.setX(y * v.z - z * v.y);
    	ret.setY(z * v.x - x * v.z);
    	ret.setZ(x * v.y - y * v.x);
    	return ret;
    }

    public void setX(double _x) { x = _x; }
    
    public void setY(double _y) { y = _y; }
    
    public void setZ(double _z) { z = _z; }

    public void setW(double _w) { w = _w; }

    public void swap(Vector4D a) {
	double h = x;
	x = a.x;
	a.x = h;

	h = y;
	y = a.y;
	a.y = h;

	h = z;
	z = a.z;
	a.z = h;

	h = w;
	w = a.w;
	a.w = h;
    }

    
    public String toString() {
	return "(v4 " + getX() + " " + getY() + " " + getZ() + " " + getW() + " )";
    }

    /* UNIT TESTING */

     @BeforeClass
       public void setUp() {
        // code that will be invoked when this test is instantiated
     }

    @Test
    public void testPlus() {
	Vector4D v1 = new Vector4D(1,0,0,1);
	Vector4D v2 = new Vector4D(0,2,0,1);
	Vector4D v3 = v1.plus(v2);
	// log.fine("Result of " + v1 + " .plus( " + v2 + " ) = "
	// + v3);
	assert(v3.similar(new Vector4D(1, 2, 0,2)));
    }
    
}
