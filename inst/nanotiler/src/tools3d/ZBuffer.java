package tools3d;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class ZBuffer {

    private List<Positionable3D> list;

    private Iterator iter;

    private boolean sorted = true;

    private Vector3D direction = new Vector3D(0.0, 0.0, 1.0);

    private static final Vector3D zeroPos = new Vector3D(0.0, 0.0, 0.0);

    public ZBuffer() {
    	list = new ArrayList<Positionable3D>();
	iter = list.iterator();
    }

    public void add(Positionable3D p) {
	// generate z-buffer priority and store in object:
	p.setZBufValue(computePriority(p.getPosition()));
	list.add(p);
	sorted = false;
    }

    public Positionable3D get(int i) {
    	return (Positionable3D)(list.get(i));
    }
    
    public boolean isSorted() {
	return sorted;
    }

    /** returns projection on direction vector */
    public double computePriority(Vector3D pos) {
	return Matrix3DTools.project(pos, direction, zeroPos);
    }

    public void sort() {
	Collections.sort(list);
	sorted = true;
    }

    /** resets value */
    public void reset() {
	list.clear();
	iter = list.iterator();
	sorted = true;
    }

    /** sets direction of viewing */
    public void setDirection(Vector3D v) {
	this.direction = v;
    }

    /** number of current poygons */
    public int size() {
	return list.size();
    }

    /** returns n'th element of list. Must be commited first! */
//     public Primitive3D get(int n) {
// 		if (!isSorted()) {
// 		    sort();
// 		}
// 		return (Primitive3D)(list.get(n));
//     }

    public boolean hasNext() {
		return iter.hasNext();
    }

    public Positionable3D next() {
		if (!iter.hasNext()) {
	    	return null;
		}
		return (Positionable3D)iter.next();
    }

}
