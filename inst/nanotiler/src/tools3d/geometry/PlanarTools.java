package tools3d.geometry;

import java.awt.geom.Point2D;

import tools3d.Edge3D;
import tools3d.Face3D;
import tools3d.Geometry;
import tools3d.Point3D;
import tools3d.StandardGeometry;

/**
 * Generates planar structures.
 */
public class PlanarTools {

    /**
     * Generates pentagon, hexagon, heptagon, etc.
     *
     * @param n Number of corners and sides.
     * @param sideLength Length of sides.
     */
    public static Geometry generateNtagon(int n,
					  double sideLength) {
	assert n > 2;
	Point3D[] points = new Point3D[n];
	double l = sideLength;
	double lh = 0.5 * l;
	double angle = 2.0 * Math.PI / n;
	double h = lh / Math.sin(0.5 * angle);
	double r = Math.sqrt((lh * lh) + (h * h));
	for (int i = 0; i < n; ++i) {
	    double currAng = i * angle;
	    double x = r * Math.cos(currAng);
	    double y = r * Math.sin(currAng);
	    double z = 0;
	    points[i] = new Point3D(x,y,z);
	}
	Edge3D[] edges = new Edge3D[n];
	for (int i = 1; i < n; ++i) {
	    edges[i] = new Edge3D(points[i-1], points[i]);
	}
	edges[0] = new Edge3D(points[0], points[n-1]);
	Face3D[] faces = new Face3D[0];
	return new StandardGeometry(points, edges, faces);
    }

    /**
     * Generates pentagon, hexagon, heptagon, etc.
     *
     * @param n Number of corners and sides
     * @param sideLength Length of sides.
     * @param height Height of structure.
     */
    public static Geometry generatePrism(int n,
					 double sideLength,
					 double height) {
	assert n > 2;
	Point3D[] points = new Point3D[2*n];
	double l = sideLength;
	double lh = 0.5 * l;
	double angle = 2.0 * Math.PI / n;
	double h = lh / Math.sin(0.5 * angle);
	double r = Math.sqrt((lh * lh) + (h * h));
	for (int i = 0; i < n; ++i) {
	    double currAng = i * angle;
	    double x = r * Math.cos(currAng);
	    double y = r * Math.sin(currAng);
	    double z0 = 0;
	    double z1 = height;
	    points[i] = new Point3D(x,y,z0);
	    points[i+n] = new Point3D(x,y,z1);
	}
	Edge3D[] edges = new Edge3D[3*n];
	for (int i = 1; i < n; ++i) {
	    edges[i] = new Edge3D(points[i-1], points[i]);
	    edges[i+n] = new Edge3D(points[i-1+n], points[i+n]);
	    edges[i + (2*n)] = new Edge3D(points[i], points[i+n]);
	}
	edges[0] = new Edge3D(points[0], points[n-1]);
	edges[n] = new Edge3D(points[n], points[2*n-1]);
	edges[2 * n] = new Edge3D(points[0], points[n]);
	Face3D[] faces = new Face3D[0];
	for (int i = 0; i < points.length; ++i) {
	    assert points[i] != null;
	}
	for (int i = 0; i < edges.length; ++i) {
	    assert edges[i] != null;
	}
	return new StandardGeometry(points, edges, faces);
    }

}
