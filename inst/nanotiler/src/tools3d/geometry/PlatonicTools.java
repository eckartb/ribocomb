package tools3d.geometry;

import java.awt.geom.Point2D;
import java.util.logging.Logger;

import tools3d.Edge3D;
import tools3d.Face3D;
import tools3d.Geometry;
import tools3d.Point3D;
import tools3d.StandardGeometry;

/**
 * Generates platonic solids for a given lengh of edges.
 */
public class PlatonicTools {

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    public static final double SQRT_2D3 = Math.sqrt(2.0/3.0);
    public static final double SQRT_3 = Math.sqrt(3.0);
    public static final double SQRT_5 = Math.sqrt(5.0);

    public static final int DODECAHEDRON = 3;

    /**
     * generates regular tetrahedron centered around center position.
     * Coordinates taken from:
     * http://mcraefamily.com/MathHelp/GeometrySolidTetrahedron.htm
     * TODO : no faces defined yet.
     *
     * @param sideLength Length of the sides.
     */
    public static Geometry generateTetrahedron(double sideLength) {
	Point3D[] points = new Point3D[4];
	double l = sideLength;
	points[0] = new Point3D(-0.5*l, - l * SQRT_3/6.0, - l * SQRT_2D3/4.0);
	points[1] = new Point3D(0.5*l, - l * SQRT_3/6.0, - l * SQRT_2D3/4.0);
	points[2] = new Point3D(0.0, 2.0 * l*SQRT_3/6.0, - l * SQRT_2D3/4.0);
	points[3] = new Point3D(0.0, 0.0, 3.0 * l * SQRT_2D3/4.0);
	Edge3D[] edges = new Edge3D[6];
	edges[0] = new Edge3D(points[0], points[1]);
	edges[1] = new Edge3D(points[1], points[2]);
	edges[2] = new Edge3D(points[2], points[0]);
	edges[3] = new Edge3D(points[0], points[3]);
	edges[4] = new Edge3D(points[1], points[3]);
	edges[5] = new Edge3D(points[2], points[3]);
	Face3D[] faces = new Face3D[0]; // TODO : no faces defined yet
	return new StandardGeometry(points, edges, faces);
    }

    public static Geometry generateOctahedron(double sideLength) {
	Point3D[] points = new Point3D[6];
	double l = sideLength;
	points[0] = new Point3D(0.5*l, -0.5*l, 0);
	points[1] = new Point3D(-0.5*l, -0.5*l, 0);
	points[2] = new Point3D(-0.5*l, 0.5*l, 0);
	points[3] = new Point3D(0.5*l, 0.5*l, 0);
	points[4] = new Point3D(0, 0, 0.5*l);
	points[5] = new Point3D(0, 0, -0.5*l);
	Edge3D[] edges = new Edge3D[12]; // calculateEdges(points, l, 12);  // new Edge3D[12];
	edges[0] = new Edge3D(points[0], points[1]);
	edges[1] = new Edge3D(points[1], points[2]);
	edges[2] = new Edge3D(points[2], points[3]);
	edges[3] = new Edge3D(points[3], points[0]);
	edges[4] = new Edge3D(points[0], points[5]);
	edges[5] = new Edge3D(points[1], points[5]);
	edges[6] = new Edge3D(points[2], points[5]);
	edges[7] = new Edge3D(points[3], points[5]);
	edges[8] = new Edge3D(points[0], points[4]);
	edges[9] = new Edge3D(points[1], points[4]);
	edges[10] = new Edge3D(points[2], points[4]);
	edges[11] = new Edge3D(points[3], points[4]);
	Face3D[] faces = new Face3D[0];
	return new StandardGeometry(points, edges, faces);
    }

    public static Geometry generateCube(double sideLength) {
 	Point3D[] points = new Point3D[8];
 	double l = sideLength;
 	points[0] = new Point3D(0.5*l, -0.5*l, 0.5*l);
 	points[1] = new Point3D(-0.5*l, -0.5*l, 0.5*l);
	points[2] = new Point3D(-0.5*l, 0.5*l, 0.5*l);
 	points[3] = new Point3D(0.5*l, 0.5*l, 0.5*l);
 	points[4] = new Point3D(-0.5*l, -0.5*l, -0.5*l);
 	points[5] = new Point3D(0.5*l, -0.5*l, -0.5*l);
 	points[6] = new Point3D(0.5*l, 0.5*l, -0.5*l);
 	points[7] = new Point3D(-0.5*l, 0.5*l, -0.5*l);
 	Edge3D[] edges = calculateEdges(points, l, 12);
 	edges[0] = new Edge3D(points[0], points[1]);
  	edges[1] = new Edge3D(points[1], points[2]);
  	edges[2] = new Edge3D(points[2], points[3]);
  	edges[3] = new Edge3D(points[3], points[0]);
  	edges[4] = new Edge3D(points[0], points[5]);
  	edges[5] = new Edge3D(points[1], points[4]);
  	edges[6] = new Edge3D(points[2], points[7]);
  	edges[7] = new Edge3D(points[3], points[6]);
  	edges[8] = new Edge3D(points[4], points[7]);
  	edges[9] = new Edge3D(points[7], points[6]);
  	edges[10] = new Edge3D(points[6], points[5]);
  	edges[11] = new Edge3D(points[5], points[4]);
 	Face3D[] faces = new Face3D[0];
 	return new StandardGeometry(points, edges, faces);
    }

    public static Geometry generateDodecahedron(double sideLength) {
	Point3D[] points = new Point3D[20];
	double l = sideLength;
	double num = 1.401 * l;
  	points[0] = new Point3D(.607*num, 0*num, .795*num);
  	points[1] = new Point3D(.188*num, .577*num, .795*num);
  	points[2] = new Point3D(-.491*num, .357*num, .795*num);
  	points[3] = new Point3D(-.491*num, -.357*num, .795*num);
  	points[4] = new Point3D(.188*num, -.577*num, .795*num);
  	points[5] = new Point3D(.982*num, 0.0*num, .188*num);
  	points[6] = new Point3D(.304*num, .934*num, .188*num);
  	points[7] = new Point3D(-.795*num, .577*num, .188*num);
  	points[8] = new Point3D(-.795*num, -.577*num, .188*num);
  	points[9] = new Point3D(.304*num, -.934*num, .188*num);
  	points[10] = new Point3D(.795*num, .577*num, -.188*num);
  	points[11] = new Point3D(-.304*num, .934*num, -.188*num);
  	points[12] = new Point3D(-.982*num, 0.0, -.188*num);
  	points[13] = new Point3D(-.304*num, -.934*num, -.188*num);
  	points[14] = new Point3D(.795*num, -.577*num, -.188*num);
  	points[15] = new Point3D(.491*num, .357*num, -.795*num);
  	points[16] = new Point3D(-.188*num, .577*num, -.795*num);
  	points[17] = new Point3D(-.607*num, 0.0*num, -.795*num);
 	points[18] = new Point3D(-.188*num, -.577*num, -.795*num);
  	points[19] = new Point3D(.491*num, -.357*num, -.795*num);

	log.finest("GOT HERE, PLATONICTOOLS, LINE 117"); 
 	Edge3D[] edges = calculateEdges(points, sideLength, 30);
 	for (int i = 0; i < edges.length; ++i) {
 	    log.finest("Edge " + i + " is " + edges[i]);
	}
 	log.finest("GOT HERE, PLATONICTOOLS, LINE 119");
	Face3D[] faces = new Face3D[0];
 	return new StandardGeometry(points, edges, faces);
    }
    

    public static Edge3D[] calculateEdges(Point3D[] pt, double length, int edges) {
	 Edge3D[] e3d = new Edge3D[edges];
	 int edgeIndex = 0;
	 //int j = 0;
	 int jIndex = 0;
	 double diff = 0.01 * length;
	 for(int i = 0; i < pt.length; ++i) {
	     for (int j = i + 1; j < pt.length; ++j) {
		 int connections = 0;
		 log.finest("Point " + i + " is " + pt[i]);
		 log.finest("Point " + j + " is " + pt[j]);
		 double pLength = pt[i].distance(pt[j]);
		 log.finest("" + pLength);
		 log.finest("distance between points " + i + " " + j
				    + " " + pLength + " " + (pLength/length)
				    );
		 if((pLength <  (length + diff)) 
		    && (pLength > (length - diff))) {
		     log.finest("Found valid edge between two points");
		     log.finest("Edge Index is: " + edgeIndex);
		     log.finest("First point is: " + pt[i]);
		     log.finest("Second point is: " + pt[j]);
		     e3d[edgeIndex] = new Edge3D(pt[i], pt[j]);
		     ++edgeIndex;
		 }
	     }
	 }
	 if (edgeIndex != edges) {
	     log.warning("Warning: bad number of edges given:"
				+ edges + " instead of " + edgeIndex 
				+ " found."); 				
	 }
	 Edge3D[] e3dFinal = new Edge3D[edgeIndex];
	 for (int i = 0; i < edgeIndex; ++i) {
	     e3dFinal[i] = e3d[i];
	 }
	 return e3dFinal;
     }
    
    public static Geometry generateIcosahedron(double sideLength) {
	Point3D[] points = new Point3D[12];
	double l = sideLength;
	double factor = .95105654 * l;
	points[0] = new Point3D(0, 0, 1*factor);
	points[1] = new Point3D(0.894 * factor, 0, .447 * factor);
	points[2] = new Point3D(0.276 * factor, 0.851 * factor, .447 * factor);
	points[3] = new Point3D(-0.724 * factor, 0.526 * factor, .447 * factor);
	points[4] = new Point3D(-0.724 * factor, -0.526 * factor, .447 * factor);
	points[5] = new Point3D(0.276 * factor, -0.851 * factor, .447 * factor);
	points[6] = new Point3D(0.724 * factor, 0.526 * factor, -.447 * factor);
	points[7] = new Point3D(-0.276 * factor, 0.851 * factor, -.447 * factor);
	points[8] = new Point3D(-0.894 * factor, 0, -.447 * factor);
	points[9] = new Point3D(-0.276 * factor, -0.851 * factor, -.447 * factor);
	points[10] = new Point3D(0.724 * factor, -0.526 * factor, -.447 * factor);
	points[11] = new Point3D(0, 0, -1 * factor);
	Edge3D[] edges = calculateEdges(points, sideLength, 30);
	Face3D[] faces = new Face3D[0];
	return new StandardGeometry(points, edges, faces);
    }

}
