package tools3d.numerics3d;

import java.util.logging.Logger;

import tools3d.Vector3D;

public class Geom3DTools {

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    /** returns distance between two lines in 3D space defined
     * through x0+x*t and y0+y*s  .
     * It returns a 3D vector with the 1. component being 
     * "t", the second component being "s" and the third
     * component being the distance d 
     * Formulas from: Paper by Bard, Himel in class of Dr. Dennis Merino
     * (Mississipi College, USA)
     * www.mc.edu/campus/users/travis/maa/proceedings/spring2001/bard.himel.pdf  
     */
    public static Vector3D computeLineDistance(Vector3D x0,
				      Vector3D x,
				      Vector3D y0, 
				      Vector3D y) {
	double A = x.lengthSquare();
	double B = 2.0 * (x.dot(x0) - x.dot(y0));
	double C = 2.0 * (x.dot(y));
	double D = 2.0 * (y.dot(y0) - y.dot(x0));
	double E = y.lengthSquare();
	double F = x0.lengthSquare() + y0.lengthSquare();
	double denom = (C*C - 4*A*E);
        if (denom == 0.0) {
	    return new Vector3D(0, 0, -1.0); // lines are probably parallel : no unique solution possible
	}
	double s = ((2*A*D) + (B*C)) / denom;
	double t = (C*s - B) / (2.0*A); 
	double d = Math.sqrt( (B*C*D+B*B*E+C*C*F+A*(D*D-4.0*E*F))/denom );
	return new Vector3D(t, s, d);
    }

    /** returns best middle point between two lines
     * Lines are defined through x0+x*t and y0+y*s  .
     * undefined if lines are parallel (exception is thrown)
     */
    public static Vector3D computeLineMedium(Vector3D x0, Vector3D x,
					     Vector3D y0, Vector3D y) throws java.awt.geom.NoninvertibleTransformException {
	Vector3D values = computeLineDistance(x0, x, y0, y);
	if (values.getZ() < 0.0) {
	    throw new java.awt.geom.NoninvertibleTransformException("Trying to obtain distances between parallel lines: " + x0 + " " + x + " " + y0 + " " + y);
	}
	Vector3D p1 = x0.plus(x.mul(values.getX()));
	Vector3D p2 = y0.plus(y.mul(values.getY()));
	return p1.plus(p2).mul(0.5); // average position; middle of shortest connecting line
	// log.warning("Warning: Trying to obtain distances between parallel lines: " + x0 + " " + x + " " + y0 + " " + y);
	// return x0.plus(y0).mul(0.5); // emergency solution!
    }

    /** returns best middle point between two lines */
    public static Vector3D computeLineMedium(Line line1, Line line2) throws java.awt.geom.NoninvertibleTransformException {
	return computeLineMedium(line1.offs, line1.dir, line2.offs, line2.dir);
    }

    /** returns average minimum point that best describes where all lines meet */
    public static Vector3D computeLinesMedium(Line[] lines) throws java.awt.geom.NoninvertibleTransformException {
	Vector3D avg = new Vector3D(0,0,0);
	int counter = 0;
	for (int i = 0; i < lines.length; ++i) {
	    for (int j = (i+1); j < lines.length; ++j) {
		try {
		    Vector3D p = computeLineMedium(lines[i], lines[j]);
		    avg.add(p);
		    ++counter;
		} catch (java.awt.geom.NoninvertibleTransformException nite) {
		    // ok as long as if not ALL lines are parallel
		}
	    }
	}
	if (counter == 0) {
	    throw new java.awt.geom.NoninvertibleTransformException("Only parallel lines found. Could not compute mid point of set of lines.");
	}
	avg.scale(1.0 / (double)counter);
	return avg;
    }

    /** return min distance between point and line , defined trough offset and 
	direction
	positive result, if it projects to positive half line, negative otherwise
	@author Eckart Bindewald
	@date 09/1999
    */
    public static double computeDistanceToLine(Vector3D point, 
				 Vector3D offset, 
				 Vector3D direction)
    {
	// assert((point!=offset)&&(point!=direction)&&(offset!=direction)
	assert (direction.length()>0.0);
	double result = 0.0;
	Vector3D dVec = point.minus(offset);
	double dLength = dVec.length();
	assert(dLength > 0.0);
	double cosAlpha = dVec.dot(direction) /(dLength*direction.length());
	double alpha = Math.acos(cosAlpha);
	result = dLength * Math.sin(alpha);
	if (cosAlpha < 0.0)
	    {
		result *= -1.0;
		assert(result <= 0.0);
	    }
	assert((result*cosAlpha) >= 0.0);
	return result;
    } 


    /** return point that leads to shortest distance between point and line , defined trough offset and 
	direction
	positive result, if it projects to positive half line, negative otherwise
	The resulting number is the distance on the line from the offset that corresponds to the shortest line-point distance
	If negative, the point on line is in oppositite direction from point and direction vector.
	@author Eckart Bindewald
	@date 09/1999
    */
    public static double computeDistanceLinePoint(Vector3D point, 
						  Vector3D offset, 
						  Vector3D direction)
    {
	// assert((point!=offset)&&(point!=direction)&&(offset!=direction)
	assert (direction.length()>0.0);
	Vector3D dVec = point.minus(offset);
	double dLength = dVec.length();
	assert(dLength > 0.0);
	double result = dVec.dot(direction)/direction.length(); // /(dLength*direction.length());
	return result;
    } 


}
