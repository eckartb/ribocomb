package tools3d.numerics3d;

import tools3d.Vector3D;

/** line in 3D space */
public class Line
{
    public Vector3D offs; // offset point

    public Vector3D dir; // direction

    public Line() {
	this.offs = new Vector3D();
	this.dir = new Vector3D(0.0, 0.0, 1.0);
    }

    public Line(Vector3D offs, Vector3D dir) {
	this.offs = new Vector3D(offs);
	this.dir = new Vector3D(dir);
    }

    public Vector3D getOffs() { return offs; }

    public Vector3D getDir() { return dir; }

}
