package tools3d.objects3d;

import tools3d.Shape3DSet;

/** creates a 3D representation as a set of shapes
 * if it creates only shapes for the top object3 or also child nodes
 * is up to the implementing class
 * @see Factory design pattern in Gamma et al: Design patterns
 */
public interface Shape3DSetFactory {

    /** creates a 3D representation as a set of shapes */
    public Shape3DSet createShapeSet(Object3D obj);

    /** creates a 3D representation as a set of shapes */
    public Shape3DSet createShapeSet(Link link);

    /** returns maximum depth for objects for generate shapes for */
    public int getDepthMax();

    /** sets maximum depth for objects for generate shapes for */
    public void setDepthMax(int n);

}
