/** implements concept of two linked objects */
package tools3d.objects3d;

import tools3d.Vector3D;
import generaltools.ConstrainableDouble;

/** interface for link that has a min/max constraint attached to it */
public interface AngleLink extends ConstrainableDouble, Link {

    Object3D getObj3();

    double computeError();

    double computeError(Vector3D pos1, Vector3D pos2, Vector3D pos3);

}
	
