package tools3d.objects3d;

import java.util.Random;
import java.util.logging.Logger;
import generaltools.Randomizer;
import tools3d.Vector3D;

public class BrownianMotionOptimizer implements Object3DOptimizer {

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    private int iterMax = 1000;
    private int verboseLevel = 1;
    private double transStep = 30.0; // initially 10A steps
    private double angleStep = 0.5 * Math.PI;
    private static Random rand = Randomizer.getInstance();

    /** returns random number between min and max */
    private double generateRandomDouble(double min, double max) {
	assert max >= min;
	double delta = max - min;
	double x = rand.nextDouble();
	x *= delta;
	x += min;
	return x;
    }

    private Vector3D generateRandomVector(double min, double max) {
	double x = generateRandomDouble(min, max);
	double y = generateRandomDouble(min, max);
	double z = generateRandomDouble(min, max);
	return new Vector3D(x, y, z);
    }

    public double getAngleStep() { return angleStep; }
    
    public double getTransStep() { return transStep; }

    /** Optimizes position and orientation of 3D object given a potential. */
    public double optimize(Object3D obj, Object3DPotential potential) {
	double bestScore = potential.computeValue(obj);
	if (verboseLevel > 0) {
	    log.fine("Starting BrownianMotionOptimizer.optimize with score: " + bestScore + " and position: " + obj.getPosition());
	}
	for (int i = 0; i < iterMax; ++i) {
	    double modStep = (0.99 * (1.0 - ((double)i / ((double)iterMax)))) + 0.01; // modifier of step width
	    double currTransStep = transStep * modStep;
	    Vector3D trans = generateRandomVector(-currTransStep, currTransStep);
	    Vector3D direction = generateRandomVector(0.0, 1.0);
	    if (direction.length() == 0.0) {
		direction = new Vector3D(1, 0, 0);
	    }
	    direction.normalize();
	    double currAngleStep = angleStep*modStep;
	    double ang = generateRandomDouble(0.0, currAngleStep);
	    obj.translate(trans);
	    obj.rotate(direction, ang);
	    double score = potential.computeValue(obj);
	    if (score >= bestScore) {
		// undo 
		obj.rotate(direction, -ang); // rotate back
		obj.translate(trans.mul(-1.0)); // shift back
	    }
	    else {
		bestScore = score;
		if (verboseLevel > 0) {
		    log.fine("Step " + (i+1) + " New best score found: " + bestScore + " " + obj.getPosition());
		}
	    }
	}
	if (verboseLevel > 0) {
	    log.fine("Ending BrownianMotionOptimizer.optimize with score: " + bestScore + " and position: " + obj.getPosition());
	}
	return bestScore;
    }

    public void setTransStep(double transStep) { this.transStep = transStep; }

    public void setAngleStep(double angleStep) { this.angleStep = angleStep; }

    public void setIterMax(int iter) { this.iterMax = iter; }

    public void setVerboseLevel(int n) { this.verboseLevel = n; }
}
