/** implements concept of two linked objects */
package tools3d.objects3d;

import generaltools.ConstrainableDouble;

/** interface for link that has a min/max constraint attached to it */
public interface ConstraintLink extends ConstrainableDouble, Link {

    int getSymId1();

    int getSymId2();

    void setSymId1(int _symId1);

    void setSymId2(int _symId2);

}
	
