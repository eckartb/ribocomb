package tools3d.objects3d;

import java.util.logging.Logger;

import tools3d.CoordinateSystem;
import tools3d.Matrix3D;
import tools3d.Vector3D;
import tools3d.Matrix4D;
import tools3d.Vector4D;
import tools3d.Vector3DTools;
import tools3d.Orientable;

import static tools3d.PackageConstants.*;

/** Implementation of CoordinateSystem class using SimpleObject3D and 3 child nodes */
public class CoordinateSystem3D extends SimpleObject3D implements Object3D, CoordinateSystem {
    // norm coordinate system at origin
    public static final CoordinateSystem3D CARTESIAN = new CoordinateSystem3D(Vector3D.ZVEC, Vector3D.EX, Vector3D.EY);
    public static final int X_ID = 0;
    public static final int Y_ID = 1;
    public static final int Z_ID = 2;
    public static final String CLASS_NAME = "CoordinateSystem3D";
    public static final String DEFAULT_NAME = "coordinateSystem";
    public static final int ARRAY_DIM = 9; // number of dimensions needed to describe coordinate system
    public static Logger log = Logger.getLogger("NanoTiler_debug");

    /** constructs coordinate system. z-vector : direction of z-vector, computed from x and y
     * @param basePos : coordinate of "zero-point"
     * @param x : direction of x-vector
     * @param y : direction of y-vector
     */
    public CoordinateSystem3D(Vector3D basePos,
			      Vector3D x, 
			      Vector3D y) {
	super(basePos);
	assert (Math.abs(x.dot(y)) < 0.01);
// 	{ // x and y must be at right angle
// 	    log.info("generating non-cartesian coordinate system: " + x + " " +  y);
// 	}
	assert (Math.abs(x.lengthSquare() - 1.0) < 0.01); // check if unit length
	assert (Math.abs(y.lengthSquare() - 1.0) < 0.01); // check if unit length
	Vector3D z = x.cross(y);
	assert (Math.abs(z.lengthSquare() - 1.0) < 0.1); // check if unit length
	z.normalize();
	Object3D xObj = new SimpleObject3D(basePos.plus(x));
	Object3D yObj = new SimpleObject3D(basePos.plus(y));
	Object3D zObj = new SimpleObject3D(basePos.plus(z));
	xObj.setName("x");
	yObj.setName("y");
	zObj.setName("z");
	super.insertChild(xObj);
	super.insertChild(yObj);
	super.insertChild(zObj);
	this.setName(DEFAULT_NAME);	
	assert (isValid());
    }

    /** constructs coordinate system. z-vector : direction of z-vector, computed from x and y
     * @param basePos : coordinate of "zero-point"
     * @param x : direction of x-vector
     * @param y : direction of y-vector
     */
    public CoordinateSystem3D(Vector3D basePos,
			      Vector3D x, 
			      Vector3D y,
			      Vector3D z) {
	super(basePos);
	// assert (Math.abs(x.dot(y)) < 0.001); // x and y must be at right angle
	// assert (Math.abs(x.lengthSquare() - 1.0) < 0.01); // check if unit length
	// assert (Math.abs(y.lengthSquare() - 1.0) < 0.01); // check if unit length
	// Vector3D z = x.cross(y);
	// assert (Math.abs(z.lengthSquare() - 1.0) < 0.01); // check if unit length
	Object3D xObj = new SimpleObject3D(basePos.plus(x));
	Object3D yObj = new SimpleObject3D(basePos.plus(y));
	Object3D zObj = new SimpleObject3D(basePos.plus(z));
	xObj.setName("x");
	yObj.setName("y");
	zObj.setName("z");
	super.insertChild(xObj);
	super.insertChild(yObj);
	super.insertChild(zObj);
	this.setName(DEFAULT_NAME);	
	assert (isValid());
    }

    /** using homogeneous coordinates for initialization */
    public CoordinateSystem3D(Matrix4D m) {
	this(new Vector3D(m.getXW(), m.getYW(), m.getZW()),
	     new Vector3D(m.getXX(), m.getYX(), m.getZX()),
	     new Vector3D(m.getXY(), m.getYY(), m.getZY()));
    }

    /** constructs coordinate system.
     * @param basePos : coordinate of "zero-point"
     * @param x : direction of x-vector
     * @param y : direction of y-vector
     * @param z : direction of z-vector
     */
    public CoordinateSystem3D(CoordinateSystem cs) {
	this(cs.getPosition(), cs.getX(), cs.getY(), cs.getZ());
	assert (isValid());
    }

    /** constructs coordinate system from 9-d array. 3 coordinates each for base position , x-direction and y-direction.
     */
    public CoordinateSystem3D(double[] ary) {
	super(new Vector3D(ary[0], ary[1], ary[2]));
	assert(ary.length == 9); 
	Vector3D basePos = new Vector3D(ary[0], ary[1], ary[2]);
	Vector3D x = new Vector3D(ary[3], ary[4], ary[5]);
	assert(x.length() > 0.0);
	x.normalize();
	Vector3D y = new Vector3D(ary[6], ary[7], ary[8]);
	assert(y.length() > 0.0);
	y.normalize();
	Vector3D z = x.cross(y);
	assert(z.length() > 0.0);
	z.normalize();
	Object3D xObj = new SimpleObject3D(basePos.plus(x));
	Object3D yObj = new SimpleObject3D(basePos.plus(y));
	Object3D zObj = new SimpleObject3D(basePos.plus(z));
	xObj.setName("x");
	yObj.setName("y");
	zObj.setName("z");
	super.insertChild(xObj);
	super.insertChild(yObj);
	super.insertChild(zObj);
	this.setName(DEFAULT_NAME);	
	assert (isValid());
    }


    /** constructs coordinate system at zero point with cartesian directions. Use only for internal object generation.
     * Careful: does not generate child direction vectors. Only for internal use!
     */
    public CoordinateSystem3D() { 
    }
    
    /** constructs coordinate system.
     * @param basePos : coordinate of "zero-point"
      */
    public CoordinateSystem3D(Vector3D pos) {
	this(new Vector3D(pos),
	     new Vector3D(1.0, 0.0, 0.0),
	     new Vector3D(0.0, 1.0, 0.0));
    }

    /** "moves" point p like coordinate system was moved. Returned coordinates are measured in global coordinates. */
    public Vector3D activeTransform(Vector3D p) {
	Vector3D origin = getPosition();
	Matrix3D mt = generateTransposedMatrix();
	return (mt.multiply(p)).plus(origin);   
    }

    /** Different interpretation of coordinate system. First shift to cs with zero origin, rotate, shift back. Returned coordinates are measured in global coordinates. */
    public Vector3D activeTransform2(Vector3D p) {
	Vector3D origin = getPosition();
	Matrix3D mt = generateTransposedMatrix();
	return (mt.multiply(p.minus(origin))).plus(origin);   
    }

    /** used 4D matrix formalism */
    public Vector3D activeTransform3(Vector3D p) {
	Matrix4D m = generateMatrix4D();
	return m.multiply(p);
    }



    /** returns 4D matrix describing rotation and translation */
    public Matrix4D generateMatrix4D() {
	Vector3D origin = getPosition();
	Matrix3D mt = generateTransposedMatrix();
	return new Matrix4D(mt, origin);
    }

//     public void applyActiveTransform(Orientable orient) {
// 	Vector3D newPos = activeTransform(obj.getPosition());
// 	if (obrient instanceof Object3D) {
// 	    Object3D obj = (Object3D)orient;
// 	    obj.setIsolatedPosition(newPos);
// 	    for (int i 0; i < obj.size(); ++i) {
// 		appyActiveTransform(obj.getChild(i));
// 	    }
// 	}
// 	else {
// 	    if (orient instanceof Shape3D) {
// 		log.warning("Shapes currently are not rotated correctly with CoordinateSystem3D!");
// 	    }
// 	    orient.setPosition(newPos);
// 	}
//     }

//     public void applyPassiveTransform(Orientable obj) {

// 	if (obrient instanceof Object3D) {
// 	    Object3D obj = (Object3D)orient;
// 	    obj.setIsolatedPosition(newPos);
// 	    for (int i 0; i < obj.size(); ++i) {
// 		appyActiveTransform(obj.getChild(i));
// 	    }
// 	}
// 	else {
// 	    if (orient instanceof Shape3D) {
// 		log.warning("Shapes currently are not rotated correctly with CoordinateSystem3D!");
// 	    }
// 	    orient.setPosition(newPos);
// 	}
//     }

    /** "deep" clone not including children. Please overwrite this method for subclasses! */
    public Object cloneDeepThis() {
	CoordinateSystem3D newObj = new CoordinateSystem3D();
	newObj.copyDeepThisCore(this);
	return newObj;
    }

    /** copies information from other object. Does not have to be "3D" object. */
    public void copy(CoordinateSystem other) {
	this.setPosition(other.getPosition());
	Object3D xObj = getChild(X_ID);
	Object3D yObj = getChild(Y_ID);
	Object3D zObj = getChild(Z_ID);
	xObj.setRelativePosition(other.getX());
	yObj.setRelativePosition(other.getY());
	zObj.setRelativePosition(other.getZ());
    }

    public boolean equals(Object otherObject) {
	if (!(otherObject instanceof CoordinateSystem)) {
	    return false;
	}
	CoordinateSystem other = (CoordinateSystem)otherObject;
	return getPosition().equals(other.getPosition()) 
	    && getX().equals(other.getX()) && getY().equals(other.getY()) && getZ().equals(other.getZ());
    }

    public String getClassName() { return CLASS_NAME; }

    public Vector3D getX() { return getChild(X_ID).getRelativePosition(); }
    
    public Vector3D getY()  { return getChild(Y_ID).getRelativePosition(); }

    public Vector3D getZ()  { return getChild(Z_ID).getRelativePosition(); }

    private Matrix3D generateMatrix() {
	Vector3D ex = getX();
	Vector3D ey = getY();
	Vector3D ez = getZ();
	Matrix3D m = new Matrix3D(ex.getX(),ex.getY(),ex.getZ(),
				  ey.getX(),ey.getY(),ey.getZ(), 
				  ez.getX(),ez.getY(),ez.getZ());
	return m;
    }

    private Matrix3D generateTransposedMatrix() {
	Vector3D ex = getX();
	Vector3D ey = getY();
	Vector3D ez = getZ();
	Matrix3D mt = new Matrix3D(ex.getX(),ey.getX(),ez.getX(),
				   ex.getY(),ey.getY(),ez.getY(), 
				   ex.getZ(),ey.getZ(),ez.getZ());
	return mt;
    }

    /** generates new coordinate system c, such that this.activeTransform(c) is identity transformation */
    public CoordinateSystem inverse() {
// 	Vector3D x = getX();
// 	Vector3D y = getY();
// 	Vector3D z = getZ();
// 	Vector3D newX = new Vector3D(x.getX(), y.getX(), z.getX());
// 	Vector3D newY = new Vector3D(x.getY(), y.getY(), z.getY());
// 	Vector3D newPos = getPosition().mul(-1);
	Matrix4D m = generateMatrix4D();

	// return new CoordinateSystem3D(newPos, newX, newY);
	// return new CoordinateSystem3D(m.homogeneousInverse());
	return new CoordinateSystem3D(m.inverse());
    }

    /** returns if close to unit transform */
    public boolean isUnitTransform(double error) {
	return (getPosition().length() < 0.01)
	    && (getX().minus(Vector3D.EX).length() < error)
	    && (getY().minus(Vector3D.EY).length() < error)
	    && (getZ().minus(Vector3D.EZ).length() < error);
    }

    /** returns true if coordinate system has all the correct properties of a cartesian coordinate system. */
    public boolean isCartesian() {
	assert isValid();
	Vector3D x = getX();
	Vector3D y = getY();
	Vector3D z = getZ();
	boolean result = isUnitLength(x) && isUnitLength(y) && isUnitLength(z)
	    && isRightAngle(x, y) && isRightAngle(x, z) && isRightAngle(y, z)
	    && isRightHanded(x,y,z);
	return result;
    }

    /** returns true if coordinate system has all the correct properties of a cartesian coordinate system. */
    public boolean isValid() {
	boolean result = true;
	if (size() < 3) {
	    return false;
	}
	return getX().isValid() && getY().isValid() && getZ().isValid();
    }

    /** returns true if vector v has a length of approximately one */
    private static boolean isUnitLength(Vector3D v) {
	return Math.abs(v.length() - 1.0) < 0.001;
    }

    /** returns true if vectors v and w are approximately orthogonal */
    private static boolean isRightAngle(Vector3D v, Vector3D w) {
	return Math.abs(v.dot(w)) < 0.01;
    }

    /** returns true if x cross y is equal to z */
    private static boolean isRightHanded(Vector3D x, Vector3D y, Vector3D z) {
	Vector3D diff = z.minus(x.cross(y));
	return diff.length() < 0.01;
    }

    /** measures coordinates of point p in this coordinate system */
    public Vector3D passiveTransform(Vector3D p) {
	Vector3D origin = getPosition();
	Matrix3D m = generateMatrix();
	return m.multiply(p.minus(origin));  
    } 

    /** measures coordinates of point p in this coordinate system. Compatible with activeTransform2 */
    public Vector3D passiveTransform2(Vector3D p) {
	Vector3D origin = getPosition();
	Matrix3D m = generateMatrix();
	return (m.multiply(p.minus(origin))).plus(origin);  
    } 

    /** used 4D matrix formalism */
    public Vector3D passiveTransform3(Vector3D p) {
	Matrix4D m = generateMatrix4D().inverse();
	return m.multiply(p);
    }

    /** the rotation matrix defined by x and y vectors can contains stretches and shears. Repair somewhat ad hoc. */
    public static void renormalize(double[] ary) {
	// first 3 numbers are position
	Vector3D x = new Vector3D(ary[3], ary[4], ary[5]);
	Vector3D y = new Vector3D(ary[6], ary[7], ary[8]);
	if (x.lengthSquare() == 0.0) {
	    x.copy(Vector3DTools.generateRandomDirection());
	}
	else {
	    x.normalize();
	}
	assert isUnitLength(x);
	if (y.lengthSquare() == 0.0) {
	    y.copy(Vector3DTools.generateRandomOrthogonalDirection(x));
	}
	else {
	    y.copy(Vector3DTools.generateOrthogonalDirection(x, y));
	}
	assert isUnitLength(y);
	assert isRightAngle(x, y);

	x.setArray(ary, 3);
	y.setArray(ary, 6);
    }

    /** Scale object by stretching it and its relative position in three dimensions. Applied also to child nodes. */
    public void scale(Vector3D factor) {
	scaling.scale(factor);
	relativePosition.scale(factor);
	// do NOT scale child nodes (x, y, z)
	// 	for (int i = 0; i < size(); ++i) {
	// 	    getChild(i).scale(factor);
	// 	}
    }

    /** returns 9-dimensional array representation */
    public double[] toArray() {
	double[] result = new double[ARRAY_DIM];
	getPosition().setArray(result, 0);
	getX().setArray(result, 3);
	getY().setArray(result, 6);
	return result;
    }

    /** coordinate system prints all child objects, because they are limited */
    public String toStringBody() {
	String result = new String();
	result = getName() + " ";
	result = result + getPosition(); // getRelativePosition().toString(); // write relative position vector
 	result = result + " " + size();
 	for (int i = 0; i < size(); ++i) {
 	    result = result + " " + getChild(i).getRelativePosition();
 	}
 	result = result + " "; // end of children item
	return result;
    }

    public String toString() {
	String result = new String();
	result = "(" + getClassName() + " "  + toStringBody() + " )";
	return result;
    }


}
