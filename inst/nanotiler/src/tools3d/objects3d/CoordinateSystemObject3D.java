package tools3d.objects3d;

import tools3d.CoordinateSystem;

/** defines Object3D with a local coordinate system */
public interface CoordinateSystemObject3D extends Object3D {

    /** returns local coordinate system */
    CoordinateSystem getCoordinateSystem();

    /** sets coordinate system. For now: cs.getPosition() should be the same as this.getPosition() */
    void setCoordinateSystem(CoordinateSystem cs);

}
