/** implements concept of two linked objects */
package tools3d.objects3d;

import tools3d.Vector3D;
import generaltools.ConstrainableDouble;

/** interface for link that has a min/max constraint attached to it */
public interface CrossProductLink extends ConstrainableDouble, Link {

    Object3D getObj3();

    Object3D getObj4();

    Object3D getObj5();

    Object3D getObj6();

    double computeError();

    /** Spat-product: a= v2-v1; b = v4-v3; c=v6-v5
     * v = (a.normalized cross b.normalized) dot c.normalized
     */
    double computeError(Vector3D v1, Vector3D v2, Vector3D v3, Vector3D v4,
			Vector3D v5, Vector3D v6);

}
	
