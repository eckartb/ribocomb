package tools3d.objects3d;

import java.util.Collection;
import generaltools.CollectionTools;

/** Interface for "check" method that returns yes/no answer for an Object3D */
public class ForbiddenObject3DTester implements Object3DTester {

    private Collection<Object3D> forbidden;

    public ForbiddenObject3DTester(Collection<Object3D> forbidden) {
	assert forbidden != null;
	this.forbidden = forbidden;
    }

    public boolean check(Object3D object) {
	return CollectionTools.containsIdentical(forbidden, object) == null;
    }

}
