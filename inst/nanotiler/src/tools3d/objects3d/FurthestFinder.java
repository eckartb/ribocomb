/**
 * 
 */
package tools3d.objects3d;

import java.util.logging.Logger;

/** Finds object in tree that is furthest away from defined position 
 * @author Eckart Bindewald
 *
 */
public class FurthestFinder implements Object3DAction {

    private static Logger log = Logger.getLogger("NanoTiler_debug");
	
    private Object3D startPosition;

    private Object3D furthestObject;

    private double furthestDistance;
	
    public FurthestFinder(Object3D startObject) {	
	this.startPosition = startObject;
	this.furthestObject = startObject;
	furthestDistance = 0.0;
    }
	
    public void act(Object3D obj) {
	if (obj == null) {
	    return;
	}
	double dist = startPosition.distance(obj);
	if (dist >= furthestDistance) {
	    furthestDistance = dist;
	    furthestObject = obj;
	}
    }

    public Object3D getFurthestObject() { return furthestObject; }
	    
    /** returns object that is furthest away from root object */
    public static Object3D find(Object3D root) {
	FurthestFinder collector = new FurthestFinder(root);
	Object3DActionVisitor visitor = new Object3DActionVisitor(root, collector); 
	visitor.nextToEnd();
	Object3D result = collector.getFurthestObject();
	if (result == null) {
	    log.warning("Warning from FurthestFinder.find: result null!");
	}
	return result;
    }

	
}
