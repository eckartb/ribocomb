package tools3d.objects3d;

import generaltools.AlgorithmFailureException;

/** converts graph into unique representation, invariant 
 * for all automorphisms. Not trivial, only solution for
 * subclasses of graphs (like molecules) known.
 */
public interface GraphCanonizer {

    String generateCanonizedRepresentation(Object3DLinkSetBundle graph) throws AlgorithmFailureException;

}
