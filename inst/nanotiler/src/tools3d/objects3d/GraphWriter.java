package tools3d.objects3d;

import tools3d.Vector3D;

/** Generates text representing simple uncolored undirected unweighted graph. Only direct child nodes are used as vertices, links between them as edges.
 * @author Eckart Bindewald
 * 
 */
public class GraphWriter implements Object3DLinkSetBundleWriter {

    public int getFormatId() { return LISP_FORMAT; }
    
    public static final String UNCOLORED_SYMBOL = "_";

    public String toString(Object3DLinkSetBundle bundle) {
	StringBuffer buf = new StringBuffer();
	Object3D obj = bundle.getObject3D();
	buf.append("(graph3d " + obj.size() + " ");
	for (int i = 0; i < obj.size(); ++i) {
	    Vector3D pos = obj.getChild(i).getPosition();
	    String graphColor = obj.getChild(i).getProperty("graph_color");
	    if ((graphColor == null) || (graphColor.equals(""))) {
		graphColor = UNCOLORED_SYMBOL; // uncolored graph
	    }
	    buf.append("" + (i+1) + " " + graphColor + " " + pos.getX() + " " + pos.getY() + " " + pos.getZ() + "  ");
	}
	LinkSet links = bundle.getLinks();
	LinkSet newLinks = new SimpleLinkSet();
	for (int i = 0; i < obj.size(); ++i) {
	    for (int j = i; j < obj.size(); ++j) {
		Link link = links.find(obj.getChild(i), obj.getChild(j));
		if (link != null) {
		    newLinks.add(link);
		}
	    }
	}
	buf.append(newLinks.size() + " ");
	for (int i = 0; i < newLinks.size(); ++i) {
	    Object3D obj1 = newLinks.get(i).getObj1();
	    Object3D obj2 = newLinks.get(i).getObj2();
	    // external counting: add one
	    buf.append(" " + (obj.getIndexOfChild(obj1)+1) + " " + (obj.getIndexOfChild(obj2)+1) );
	}
	buf.append(" )");
	return buf.toString();
    }

	
}
