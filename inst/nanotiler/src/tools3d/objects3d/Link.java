/** implements concept of two linked objects */
package tools3d.objects3d;

import generaltools.PropertyCarrier;
import java.io.InputStream;

public interface Link extends PropertyCarrier {

    /** Returns angle between two links */
    public double angle(Link other);

    public void clear();
	
    public Object clone();

    public Object clone(Object3D newObj1, Object3D newObj2);

    /** returns general link property */
    // public Object getLinkProperties();
	
    public String getName();
	
    public String getName1();

    public String getName2();

    /** returns first object */
    public Object3D getObj1();
	
    /** returns second object */
    public Object3D getObj2();

    /** if obj is object1 return object2 and vice versa. */
    public Object3D getPartner(Object3D obj);
	
    public String getTypeName();

    /** Returns list of objects that is common between links */
    public Object3DSet findCommon(Link other);
    
    /** returns true, if two objects are linked with this link */
    public boolean isLinked(Object3D o1, Object3D o2);
	
    /** returns true if both objects are non-null */
    public boolean isValid();
    
    /** returns 0 if not found, 1 if found once, 2 if found twice */
    public int linkOrder(Object3D o1);

    public boolean links(String name);

    /** reads link text and converts object names into object references */
    public void read(InputStream is, Object3D tree) throws Object3DIOException;

    public void replaceObjectInLink(Object3D oldObject, Object3D newObject);

    /** sets general property object */
    // public void setLinkProperties(Object o);
	
    /** sets name of individual link (like "StemP4" ) */
    public void setName(String s);
    
    public void setObj1(Object3D o);
	
    public void setObj2(Object3D o);

    /** sets type name of link like "Stem" or "Tertiary */
    public void setTypeName(String s);

    /** swaps objects one and two */
    public void swap();
	
    public String toString();

}
	
