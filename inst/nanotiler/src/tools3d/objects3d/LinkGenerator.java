package rnadesign.rnamodel;

import tools3d.objects3d.LinkSet;

/** Interface for classes computing link sets */
public interface LinkGenerator extends Runnable {

    LinkSet getLinks();

}
