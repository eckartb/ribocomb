package tools3d.objects3d;

import java.io.InputStream;
import java.util.List;

public interface LinkSet {

    public LinkSet cloneDeep();

    public LinkSet cloneDeep(Object3D root);

    /** returns n'th link */
    public void add(Link link);

    public void clear();

    /** returns n'th link */
    public Link get(int n);

    /** return minimum number of links connecting the two objects.
     * zero indicates there is no connection */
    public int getLinkNumber(Object3D obj1,
			     Object3D obj2);

    /** return minimum number of links connecting object
     *  zero indicates there is no connection 
     */
    public int getLinkOrder(Object3D obj1);

    /** Would this link be counted as link 0, 1, ... or n ?
     */
    public int getLinkRank(Object3D obj1, Link link);

    /** add all memebers of other link set to current link set */
    public void merge(LinkSet links);

    /** returns number of links */
    public int size();

    /** returns true if there is a link between objects */
    public boolean contains(Link link);

    /** tries ot find a link between two objects */
    public Link find(Object3D obj1, Object3D obj2);

    /** returns links connecting to object */
    public LinkSet findLinks(Object3D obj);

    /** tries ot find a link between two objects */
    public LinkSet findLinks(Object3D obj1, Object3D obj2);
    
    /** removes a link */
    public void remove(Link link);

    public void removeAndAdd( Object3D objectTree, Object3DSet vertexSet );

    /** remove links that cannot be found in object tree */
    public void removeBadLinks(Object3D tree);

    /** replaces oldobject reference with reference to new object */
    public void replaceObjectInLinks(Object3D oldObject, Object3D newObject);

    public String toString();

    /** reads links, given information about exisiting trees 
     * (used for converting object names into object references)
     */
    public void read(InputStream is, Object3D tree) throws Object3DIOException;

}
