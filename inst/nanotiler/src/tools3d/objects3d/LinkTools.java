package tools3d.objects3d;

public class LinkTools {

    /** counts how many links point towards object. Careful: if one link is a loop (both linked objects are one and the same), 
	then this link is counted as two connections. */
    public static int countLinks(Object3D obj, LinkSet linkSet) {
	int count = 0;
	for (int i = 0; i < linkSet.size(); ++i) {
	    count += linkSet.get(i).linkOrder(obj);
	}
	return count;
    }
    
    /** find neighbors of object, collect in object set  */
    public static Object3DSet findNeighbors(Object3D obj, LinkSet links) {
	Object3DSet result = new SimpleObject3DSet();
	for (int i = 0; i < links.size(); ++i) {
	    Link link = links.get(i);
	    if (link.linkOrder(obj) == 1) {
		//		if (obj.toString().equals(link.getObj1().toString())) {
		if (obj.getName().equals(link.getObj1().getName())) {
		    result.add(link.getObj2());
		}
		//		else if (obj.toString().equals(link.getObj2().toString())) {
		else if (obj.getName().equals(link.getObj2().getName())) {
		    result.add(link.getObj1());
		}
		else {
		    // internal error!
		    assert false;
		}

	    }
	}
	return result;
    }

}
