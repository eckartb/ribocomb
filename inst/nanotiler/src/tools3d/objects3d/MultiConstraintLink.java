/** implements concept of two linked objects */
package tools3d.objects3d;

import generaltools.PropertyCarrier;
import java.io.InputStream;

/** Connects several objects (like helices belonging to one junction) with one link */
public interface MultiConstraintLink extends MultiLink, ConstraintLink {

    int getSymId(int index);

    void setSymId(int index, int symId);

}
	
