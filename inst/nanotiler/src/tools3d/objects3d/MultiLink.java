/** implements concept of two linked objects */
package tools3d.objects3d;

import generaltools.PropertyCarrier;
import java.io.InputStream;

/** Connects several objects (like helices belonging to one junction) with one link */
public interface MultiLink extends Link {

    public void addObj(Object3D obj);

    /** returns n'th object */
    public Object3D getObj(int n);
	
    public int size();

}
	
