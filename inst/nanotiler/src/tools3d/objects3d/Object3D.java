package tools3d.objects3d;

//import tools3d.*;
import tools3d.CoordinateSystem;
import tools3d.Boundable;
import tools3d.Symmetry;
import tools3d.Shape3D;
import tools3d.Shape3DSet;
import tools3d.Vector3D;
import tools3d.Matrix3D;
import tools3d.Positionable3D;
import generaltools.Namable;
import generaltools.PropertyCarrier;
import java.util.Properties;

public interface Object3D extends Comparable<Positionable3D>, Shape3D, Namable, PropertyCarrier, Boundable {

    String name = "default_name"; // descriptor name for the object (used in output filenames)

    // public void rotate( Point start, Point end, double angle );

    /** Apply active transformation to object; version 2 has different interpretation than version 1 */
    public void activeTransform2(CoordinateSystem cs);

    /** Apply active transformation to object; version 3 uses matrix 4D formalism */
    public void activeTransform3(CoordinateSystem cs);

    /** Apply passive transformation to object; version 2 has different interpretation than version 1 */
    public void passiveTransform2(CoordinateSystem cs);

    /** Apply passive transformation to object; version 3 used 4D matrix formalism */
    public void passiveTransform3(CoordinateSystem cs);

    //    public int getNumSymmetries();

    //    public Symmetry getSymmetry(int i);

    //    public void addSymmetry(Symmetry symmetry);

    //    public boolean hasSymmetries();

    /** adds a link to container */
    public void addLink(Link link);

    /** checks if all children are unique */
    public boolean checkChildrenUnique();

    /** "deep" clone including children: every object is newly generated! */
    public Object cloneDeep();

    /** clones current class without copying any data */
    public Object cloneEmpty();

    /** "deep" clone not including children. Please overwrite this method for subclasses! */
    public Object cloneDeepThis();

    /** returns true, if obj is one of its *direct* child objects */ 
    public boolean contains(Object3D obj);
    
    /** returns half the length of one side of a cube surrounding this object
     * and all child objects.
     * @return
     */
    public double getBoundingBox();

    /** returns class name */
    public String getClassName();

    /** Returns absolute name in tree */
    public String getFullName();
	
    /** returns properties data structure */
    public Properties getProperties();

    /** returns certain property of data structure or null if undefined */
    public String getProperty(String name);

    /** Returns potentially defined score. */
    public double getScore();

    /** returns number of children of parent */
    public int getSiblingCount();

    /** returns shape set. careful: has to be updated by user or controller!*/
    public Shape3DSet getShapeSet();
	
    /** what sibling id has this object (parent.getChild(id) == this) */
    public int getSiblingId();
    
    /** returns n'th sibling of parent */
    public Object3D getSibling(int n);

    /** fast and approximate check if two objects describe the same content. Can be true even after cloning. */
    public boolean isProbablyEqual(Object3D o);
	    		
    /** selected variable is used to implement interactive editor */
    public boolean isSelected();

    public boolean isValid();
	
    /** sets current object as selected */
    public void setSelected(boolean f);
    
    /** How many children are there? */
    public int size();
    
    /** What is object's name? Don't confuse with getClassName */
    public String getName();
    
    /** Get number of object. That can be any id the programmer has set it to be. */
    public int getNumber();
    
    /** what is the type code of that object? Id corresponding to a residue type for example*/
    public int getTypeId();
    
    /** How deep is the component nested in composites? */
    public int getDepth();
    
    /** Where is object situated? Already defined in Positionable interface */
    // public Vector3D getPosition();

    /** Where is object situated? Specify id of symmetry transformation. "0" corresonds to regular getPosition() */
    public Vector3D getPosition(int symId);
    
    /** Where is object situated relative to parent? */
    public Vector3D getRelativePosition();
    
    /** Accept a Visitor. */
    public void accept(Object3DVisitor v);

    /** returns set of all links */
    public LinkSet getLinks();
    
    /** Get reference on parent. If parent == 0 return *this. */
    public Object3D getParent(); 
    
    public Object3D getChild(Object3D o);

    /** returns n'th child node */
    public Object3D getChild(int n);

    /** returns n'th child node */
    public Object3D getChild(String name);

    /** returns child node index with certain name; returns -1 if no child object with this name is found */
    public int getIndexOfChild(String name);
    
    /** returns n'th child node with specified class name */
    public int getIndexOfChild(int n, String childClassName);

    /** returns index of child. Returns size if not found */
    public int getIndexOfChild(Object3D child);
    
    /** returns number of children nodes with specified class name */
    public int getChildCount(String childClassName);

    /** returns how many children with same class and smaller index can be found */
    public int getChildClassCounter(Object3D child);

    /** returns total number of objects in tree (this object is root) */
    public int getTotalNumberOfObjects();
    
    /** shorted version of toString: write only short info, not including toString of child nodes */
    public String infoString();

    /** complete output method */
    public String toString();
    
    /* MODIFIERS */
    
    /** removes all content from object */
    public void clear();
    
    /** Generates a child name that does not exist yet */
    public String findSaveName(String name);

    /** Make child children of node.
     * Relative position of child changes according to position of parent.
     * Renames child if name already exists
     * @return Returns name of child node
     * @deprecated use insertChildSafe instead */
    
    public String insertChildSave(Object3D child);

    /** Make child children of node.
     * Relative position of child changes according to position of parent.
     * Renames child if name already exists
     * @return Returns used name of child node
     */
    public String insertChildSafe(Object3D child);

    /** Make child children of node. */
    public void insertChild(Object3D child);

    /** Make child children of node, inserting it so its new position is "position", and the former residue at that position is pushed to position + 1 */
    public void insertChild(Object3D child, int position);

    /** Removes child from node without deleting it. */
    public void removeChild(Object3D child);
    
    /** Removes child n from node without deleting it. */
    public void removeChild(int n);

    /** removes a link from container */
    public void removeLink(Link link);

    /** replaces n'th child object */
    public void replaceChild(int n, Object3D newChild);
    
    /** applies rotation matrix to tree */
    public void rotate(Vector3D center, Matrix3D rotationMatrix);

    /* @param center Vector around which to rotate object.
     * @param axis A Vector defining the plane on which to rotate object.
     * @param anglePerProjection The angle to rotate the object proportional to its projection of the object position onto the rotatino axis..
     */
    public void twist(Vector3D center, Vector3D axis, double anglePerProjection);

    /** Set whether or not this object will remain after Symmetries are generated : only to be used with .points2 files. */
    public void setFinalPoint(boolean finalPoint);

    /** Change name of object (changes id). */
    public void setName(String s);
    
    /** Change number of object. */
    public void setNumber(int n);

    /** sets arbitrary properties */
    public void setProperties(Properties properties);

    /** sets arbitrary property */
    public void setProperty(String key, String value);
    
    /** Sets relative position to parent. */
    public void setRelativePosition(Vector3D v);
    
    /** Sets score to user-defined value. */
    public void setScore(double score);

    /** Sets positions in space without changing position of child nodes */
    public void setIsolatedPosition(Vector3D v);

    /** set pointer to parent object. Use carefully. */
    public void setParent(Object3D p);
    
    /** returns shape set. careful: has to be updated by user or controller!*/
    public void setShapeSet(Shape3DSet shapeSet);
    
    public void copy(Object3D orig);

    public boolean isAncestor(Object3D o);


    public void setFilename(String s);

    public String getFilename();
    

    // private:
    /** Get id of object. */
    // const Identity& getIdentity() const;
		
}


