/** writes scence graph
 * 
 */
package tools3d.objects3d;

import tools3d.objects3d.Object3DLispWriterTools;
import tools3d.objects3d.Object3DWriter;
import tools3d.objects3d.Object3DWriterTools;

/**
 * @author Eckart Bindewald
 *
 */
public abstract class Object3DAbstractLispWriter implements Object3DWriter {

    protected int formatId = LISP_FORMAT;

    protected Object3DWriterTools tools = new Object3DLispWriterTools();

    public int getFormatId() { return formatId; }

    
}
