/** writes scence graph
 * 
 */
package tools3d.objects3d;

import tools3d.objects3d.Object3DWriter;
import tools3d.objects3d.Object3DWriterTools;
import tools3d.objects3d.Object3DXmlWriterTools;

/**
 * @author Eckart Bindewald
 *
 */
public abstract class Object3DAbstractXmlWriter implements Object3DWriter {

    protected int formatId = XML_FORMAT;

    protected Object3DWriterTools tools = new Object3DXmlWriterTools();

    public int getFormatId() { return formatId; }

    
}
