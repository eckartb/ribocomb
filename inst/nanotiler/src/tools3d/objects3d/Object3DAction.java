package tools3d.objects3d;

/** interface for action design pattern. Typical actions might be: remove, translate, change properties */
public interface Object3DAction {

    public void act(Object3D o);

}
