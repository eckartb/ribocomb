package tools3d.objects3d;

import java.util.logging.Logger;

public class Object3DActionVisitor extends Object3DEmptyVisitor {

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    private Object3DAction action;

    private int verboseLevel = 0;
    
    public Object3DActionVisitor(Object3D o, Object3DAction _action) {
		super(o);
		action = _action;
    }

    /** obtain next object and visit it */
    public Object next( ) {
    	Object3D obj = (Object3D)super.next();
	if (obj == null) {
	    log.warning("Warning: null in next command of Object3DActionVisitor!");
	}
    	visit(obj);
	return obj;
    }
    
    /** visits current object */
    public void visit(Object3D o) {
	if (verboseLevel > 1) {
	    log.fine("Object3DActionVisitor: visiting object " + o.getName());
	}
	action.act(o);
    }

    /** issues next command until end is reached */
    public void nextToEnd() {
    	while (hasNext()) {
	    next();
    	}
    }

    public void setVerboseLevel(int n) {
	verboseLevel = n;
    }
    
    /** applies action to whole subtree */
    public static void visitAll(Object3D o, Object3DAction _action) {
	Object3DActionVisitor visitor = new Object3DActionVisitor(o, _action);
	visitor.nextToEnd(); // apply action to whole subtree
    }

}
