/**
 * Collects all links that are linking SequenceBindingSites (= Stems!)
 */
package tools3d.objects3d;

import java.util.HashSet;
import java.util.Set;

public class Object3DCollector implements Object3DAction {
	
    private Set<Object3D> list;
	
    public Object3DCollector() {
	list = new HashSet<Object3D>();
    }
	
    public void act(Object3D obj) {
	list.add(obj);
    }
	
    public boolean contains(Object3D obj) {
	return list.contains(obj);
    }
	
    /** returns number of collected sequences so far */
    public int size() {
	return list.size();
    }

    public static Object3DCollector collectAll(Object3D obj) {
	Object3DCollector action = new Object3DCollector();
	Object3DActionVisitor visitor = new Object3DActionVisitor(obj, action);
	visitor.nextToEnd(); // visit and collect all in set
	return action;
    }

}
