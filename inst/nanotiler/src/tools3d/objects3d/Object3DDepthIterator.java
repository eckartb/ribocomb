package tools3d.objects3d;

import java.util.LinkedList;
import java.util.List;

public class Object3DDepthIterator implements Object3DIterator {

    Object3D tree;

    int depthMax = -1; // no maximum depth

    int depthMin = -1; // no minimum depth
    
    List<Object3D> list = new LinkedList<Object3D>();

    public Object3DDepthIterator(Object3D t) {
	tree = t;
	list.add(t);
    }
	
    public int getDepthMax() { return depthMax; }

    public int getDepthMin() { return depthMin; }
    
	public void reset() {
	    list.clear();
	    list.add(tree);
	}
	
	public boolean hasMoreObjects() {
	    return (list.size() > 0);
	}

    /** gets next object. TODO: implementation not quite correct for depthMin > 0 ! */
	public Object3D getNextObject() {
	    if (list.size() == 0) {
		return null;
	    }
	    Object3D result = null;
	    do {
		result = (Object3D)(list.get(0));
		list.remove(0);
		if ((depthMin > 0) && (result.getDepth() < depthMin)) {
		    for (int i = 0; i < result.size(); ++i) {
			list.add(result.getChild(i));
		    }
		}
		else {
		    break; 
		}
	    }
	    while (list.size() > 0);
	    if (result == null) {
		throw new Object3DBugException("Internal error in Object3DDepthIterator.getNextObject !");
	    }
	    if ((depthMax < 0) || (result.getDepth() < depthMax)) {
		for (int i = 0; i < result.size(); ++i) {
		    list.add(result.getChild(i));
		}
	    }
	    return result;
	}
    
	public void setDepthMax(int n) { depthMax = n; }

	public void setDepthMin(int n) { depthMin = n; }

}
