package tools3d.objects3d;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Object3DEmptyVisitor implements Iterator, Object3DVisitor {

    private List<Object3D> list;
	
    private Object3D tree;
	
    public Object3DEmptyVisitor(Object3D t) {
    	list = new LinkedList<Object3D>();
    	tree = t;
    	list.add(t);
    }

 
    /** empty accept method: override with decendant classes */
    public void visit(Object3D o) {
	// do nothing
    }
    
    /** removes current object. Not yet implemented! */
    public void remove() {
	// TODO
    }
    
    public void reset() {
	list.clear();
	list.add(tree);
    }
    
    public boolean hasNext() {
	return (list.size() > 0);
    }
    
    public Object next() {
	if (list.size() == 0) {
	    return null;
	}
	Object3D result = (Object3D)(list.get(0));
	list.remove(0);    
	for (int i = 0; i < result.size(); ++i) {
	    list.add(result.getChild(i));
	}	  
	return result;
    }
    
}
