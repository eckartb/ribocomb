package tools3d.objects3d;

public class Object3DException extends Exception {

	/**
	 * default UID
	 */
	private static final long serialVersionUID = 1L;
	
	public Object3DException() {
		super();
	}

	public Object3DException(Exception e) {
		super(e.getMessage());
	}
	
	public Object3DException(String message) {
		super(message);
	}
}
