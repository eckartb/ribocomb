/**
 * 
 */
package tools3d.objects3d;

import java.io.InputStream;

import tools3d.objects3d.RotationInfo;

/**
 * @author Eckart Bindewald
 *
 */
public interface Object3DFactory {

    public Object3D readAnyObject3D(InputStream is) throws Object3DIOException;

    public Object3DLinkSetBundle readBundle(InputStream is) throws Object3DIOException;

    // public RotationInfo readPoints2(InputStream is) throws Object3DIOException; //REMOVE?? CEV

    // public LinkSet getLinkSet();

}

