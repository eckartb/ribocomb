/**
 * 
 */
package tools3d.objects3d;

import java.io.InputStream;

import tools3d.objects3d.RotationInfo;

/** Interface for Object3D factories
 * @author Eckart Bindewald
 *
 */
public interface Object3DFactory2 {

    public Object3DLinkSetBundle generate() throws Object3DException;

}

