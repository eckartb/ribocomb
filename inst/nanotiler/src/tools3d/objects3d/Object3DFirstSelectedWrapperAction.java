package tools3d.objects3d;

public class Object3DFirstSelectedWrapperAction implements Object3DAction {
    
    private Object3DAction action;

    public Object3DFirstSelectedWrapperAction(Object3DAction _action) {
	action = _action;
    }

    /** performs action only if "selected" flag of Object3D is on, and off for parent object */
    public void act(Object3D o) {
	if ((o != null) && o.isSelected() && ((o.getParent() == null) || (o.getParent().isSelected() == false))) {
	    action.act(o);
	}
    }

}
