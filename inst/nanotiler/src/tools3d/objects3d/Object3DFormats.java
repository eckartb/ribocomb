package tools3d.objects3d;

public class Object3DFormats {
    
    public static final int LISP_FORMAT = 1;

    public static final int XML_FORMAT = 2;

    /** returns header string. Central for switching formats! */
    public static String getHeader(String name, int formatMode) {
	switch (formatMode) {
	case Object3DFormats.LISP_FORMAT:
	    return "(" + name;
	case Object3DFormats.XML_FORMAT:
	    return "<" + name + ">";
	}
	return "(" + name;
    }

    /** returns footer string of object of class "className". 
     * Central for switching formats! 
     */
    public static String getFooter(String className, int formatMode) {
	switch (formatMode) {
	case Object3DFormats.LISP_FORMAT:
	    return ")";
	case Object3DFormats.XML_FORMAT:
	    return "</" + className + ">";
	}
	return ")";
    }

}
