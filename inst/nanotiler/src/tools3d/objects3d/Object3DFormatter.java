package tools3d.objects3d;

/** basic interface for each read of write component. 
 * Each component knows its own format id */ 
public interface Object3DFormatter {

    public static final int LISP_FORMAT = 1;
    public static final int XML_FORMAT = 2;
    public static final int PDB_FORMAT = 3;

    /** returns format id */
    public int getFormatId();

}
