package tools3d.objects3d;

import java.io.IOException;

public class Object3DIOException extends IOException {

	/**
	 * default UID
	 */
	private static final long serialVersionUID = 1L;
	
	public Object3DIOException() {
		super();
	}

	public Object3DIOException(Exception e) {
		super(e.getMessage());
	}
	
	public Object3DIOException(String message) {
		super(message);
	}
}
