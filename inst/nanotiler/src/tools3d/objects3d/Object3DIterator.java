package tools3d.objects3d;

public interface Object3DIterator {
	
    public int getDepthMax();
	
    public void reset();

    public boolean hasMoreObjects();
	
    public Object3D getNextObject();
    
    public void setDepthMax(int n);
    
}
