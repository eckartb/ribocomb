package tools3d.objects3d;

import tools3d.Positionable3D;

public interface Object3DLinkSetBundle extends Comparable<Object3DLinkSetBundle> {

    public Object cloneDeep();
    
    public Object3D getObject3D();

    public LinkSet getLinks();

    public void setObject3D(Object3D obj);

    public void setLinks(LinkSet linkSet);

}
