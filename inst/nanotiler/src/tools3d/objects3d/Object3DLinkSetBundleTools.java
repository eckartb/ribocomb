package tools3d.objects3d;

import tools3d.Edge3D;
import tools3d.Geometry;
import tools3d.Point3D;
import tools3d.Vector3D;

public class Object3DLinkSetBundleTools {

    /** finds index of point, returns -1 if not found */
    private static int findPointIndex(Vector3D point, Point3D[] points) {
	for (int i = 0; i < points.length; ++i) {
	    if (points[i].getPosition().equals(point)) {
		return i;
	    }
	}
	return -1;
    }

    /** generates Object3DLinkSetBundle from Geometry. All points correspond to a new 
     * instance of template object, all Edges to a link.
     */
    public static Object3DLinkSetBundle generateBundleFromGeometry(Geometry geometry, String rootName,
						     String childNameBase, Object3D template) {
	assert geometry != null;
	assert rootName != null;
	assert childNameBase != null;
	assert template != null;
	Object3D root = (Object3D)(template.cloneEmpty());
	root.setName(rootName);
	Point3D[] points = new Point3D[geometry.getNumberPoints()];
	for (int i = 0; i < geometry.getNumberPoints(); ++i) {
	    Object3D child = (Object3D)(template.cloneEmpty());
	    child.setPosition(geometry.getPoint(i).getPosition());
	    String childName = childNameBase + (i+1);
	    child.setName(childName);
	    root.insertChild(child);
	    points[i] = geometry.getPoint(i);
	}
	LinkSet linkSet = new SimpleLinkSet();
	for (int i = 0; i < geometry.getNumberEdges(); ++i) {
	    Edge3D edge = geometry.getEdge(i);
	    Vector3D p1 = edge.getFirst();
	    Vector3D p2 = edge.getSecond();
	    // find point index
	    int p1Idx = findPointIndex(p1, points); // TODO slow and not clean programming. 
	    int p2Idx = findPointIndex(p2, points); // TODO change Edge3D to Edge (int, int)
	    Link link = new SimpleLink(root.getChild(p1Idx), root.getChild(p2Idx));
	    linkSet.add(link);
	}
	return new SimpleObject3DLinkSetBundle(root, linkSet);
    }

    /** generates deep clone of object 3d. Also generates new links given a set
     * of all possible links, not all necessarily belonging to root
     */
    public static Object3DLinkSetBundle generateClone(Object3D root, LinkSet allLinks) {
	return null; // TODO !!!
    }

    /** Returns objects that are connected objects given by root and links */
    public static Object3DSet generateGraphSet(Object3D root, LinkSet allLinks) {
	Object3DSet objSet = new SimpleObject3DSet(root);
	// check if all objects are connected:
	Object3DSet result = new SimpleObject3DSet();
	for (int i = 0; i < allLinks.size(); ++i) {
	    Link link = allLinks.get(i);
	    Object3D obj1 = link.getObj1();
	    Object3D obj2 = link.getObj2();
	    if (objSet.contains(obj1) && objSet.contains(obj2)) {
		if (!result.contains(obj1)) {
		    result.add(obj1);
		}
		if (!result.contains(obj2)) {
		    result.add(obj2);
		}
	    }
	}
	return result;
    }

    /** Returns links that are connecting objects given by root */
    public static LinkSet generateGraphLinks(Object3D root, LinkSet allLinks) {
	Object3DSet objSet = new SimpleObject3DSet(root);
	// check if all objects are connected:
	LinkSet result = new SimpleLinkSet();
	for (int i = 0; i < allLinks.size(); ++i) {
	    Link link = allLinks.get(i);
	    Object3D obj1 = link.getObj1();
	    Object3D obj2 = link.getObj2();
	    if (objSet.contains(obj1) && objSet.contains(obj2)) {
		result.add(link);
	    }
	}
	return result;
    }

}
