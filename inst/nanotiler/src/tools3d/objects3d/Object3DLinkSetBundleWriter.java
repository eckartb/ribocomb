/** writes scence graph
 * 
 */
package tools3d.objects3d;

import java.io.OutputStream;

/**
 * @author Eckart Bindewald
 *
 */
public interface Object3DLinkSetBundleWriter extends Object3DFormatter {
	
	public String toString(Object3DLinkSetBundle bundle);
	
}
