package tools3d.objects3d;

public class Object3DLispWriterTools implements Object3DWriterTools {

    /** returns format id */
    public int getFormatId() { return LISP_FORMAT; }

    /** writes boolean to string */
    public String writeBoolean(String name, boolean b) {
	String flag = "true";
	if (!b) {
	    flag = "false";
	}
	return encloseBody(name, flag);
    }
	
    /** writes integer array */
    public String writeIntArray(int[] ary) {
	StringBuffer buf = new StringBuffer();
	buf.append("(" + ary.length);
	for (int i = 0; i < ary.length; ++i) {
	    buf.append(" " + ary[i]);
	}
	buf.append(" )");
	return buf.toString();
    }

    /** returns header string. Central for switching formats! */
    public String getHeader(String className) { return "(" + className + " "; }

    /** returns footer : closing bracket in this format */
    public String getFooter(String className) { return " ) "; }

    /** encloses body text by item specified <className> bodyText </className> in XML for example */
    public String encloseBody(String className, String bodyText) {
	return getHeader(className) + bodyText + getFooter(className);
    }

}
