/**
 * 
 */
package tools3d.objects3d;

/** Collects all Rna binding sites of one node in tree
 * @author Eckart Bindewald
 *
 */
public class Object3DNameCollector implements Object3DAction {
	
    private Object3D object3d;
	
    private String search;

    public Object3DNameCollector(String s) {
	search = s;
    }
	
    public void act(Object3D other) {
	if ((search!= null) && (search.equals(other.getName()))) {
	    object3d = other;
	}
    }
	
    /** gets n'th collected binding site */
    public Object3D getObject3d() {
	return object3d;
    }
    
    /** returns number of collected sequences so far */
    public int size() {
	if (object3d == null) {
	    return 0;
	}
	return 1;
    }

    /** returns first object with name, null otherwise if not found */
    public static Object3D find(Object3D root, String search) {
	Object3DNameCollector collector = new Object3DNameCollector(search);
	Object3DActionVisitor visitor = new Object3DActionVisitor(root, collector); 
	visitor.setVerboseLevel(2);
	while (visitor.hasNext()) {
	    if (collector.size() == 1) {
		break;
	    }
	    visitor.next();
	}
	if (collector.size() == 1) {
	    return collector.getObject3d();
	}
	return null;
    }


}
