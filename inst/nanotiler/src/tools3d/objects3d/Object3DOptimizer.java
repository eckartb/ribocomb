package tools3d.objects3d;

public interface Object3DOptimizer {

    /** Optimizes position and orientation of 3D object given a potential. */
    public double optimize(Object3D obj, Object3DPotential potential);

}
