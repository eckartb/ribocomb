package tools3d.objects3d;

public interface Object3DPotential {

    /** assigns a potential value to an object possibly depending on position and orientation */
    public double computeValue(Object3D obj);
    
}
