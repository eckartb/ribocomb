package tools3d.objects3d;

public class Object3DSelectedWrapperAction implements Object3DAction {
    
    private Object3DAction action;

    public Object3DSelectedWrapperAction(Object3DAction _action) {
	action = _action;
    }

    /** performs action only if "selected" flag of Object3D is on */
    public void act(Object3D o) {
	if (o.isSelected()) {
	    action.act(o);
	}
    }

}
