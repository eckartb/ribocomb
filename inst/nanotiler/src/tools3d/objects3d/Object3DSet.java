package tools3d.objects3d;

import tools3d.Vector3D;
import java.util.List;
import java.util.Properties;

/** Ordered and indexed collection of 3D Object3D elements. */
public interface Object3DSet {

    void add(int i, Object3D object3d);

    /** returns n'th object3d */
    void add(Object3D object3d);

    void clear();

    /** returns true, if object is contained in set (using == operator, not equals) */
    boolean contains(Object3D obj);

    /** returns n'th object3d */
    Object3D get(int n) throws IndexOutOfBoundsException;

    /** Returns list of objects */
    List<Object3D> getAsList();

    /** Returns list of objects */
    List<Vector3D> getAsPositions();
 
   /** returns index of object, -1 if not found */
    int indexOf(Object3D obj);

    /** merges other set to current set */
    void merge(Object3DSet other);

    void remove(Object3D object3d);

    void removeExtras(Object3D objectTree);

    /** returns number of objects */
    int size();

    /** sets n'th object */
    void set(int n, Object3D obj);

    /** sets arbitrary text property key - value pair */
    void setProperty(String key, String value);

    /** returns key value pair */
    String getProperty(String key);

    /** returns key value pair */
    Properties getProperties();

    /** returns output string */
    String toString();

}
