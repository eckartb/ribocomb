package tools3d.objects3d;

public class Object3DSetSelected implements Object3DAction {
    
    public static final boolean SET_SELECT = true;

    public static final boolean SET_DESELECT = false;
    
    private boolean choice = false;

    public Object3DSetSelected(boolean _choice) {
	choice = _choice;
    }

    public void act(Object3D o) {
	o.setSelected(choice);
    }

}
