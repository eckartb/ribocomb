package tools3d.objects3d;

import java.util.List;
import tools3d.*;

public class Object3DSetTools {

    public static int indexOf(String name, String[] names) {
	for (int i = 0; i < names.length; ++i) {
	    if (name.equals(names[i])) {
		return i;
	    }
	}
	return -1;
    }

    /** adds any object3d from root (and decendents) to set 
     * if their class name is found in String[] classNames
     * Useful for getting only atoms or only nucleotides etc. */
    public static void addToSet(Object3DSet objectSet, Object3D root,
		    String[] classNames) {
	if (indexOf(root.getClassName(), classNames) >= 0) {
	    objectSet.add(root);
	}
	for (int i = 0; i < root.size(); ++i) {
	    addToSet(objectSet, root.getChild(i), classNames);
	}
    }

    /* transform molecular positions to new coordinates
     * @returns Transforming coordinate system or null if undefined (for less than 3 positions)
     */
     public static CoordinateSystem3D computeNormalizedOrientation(Object3DSet objectSet) {
	 Vector3D[] coord = getCoordinates(objectSet);
	 double[] weights = null;
	 Vector3D[] result = OrientationTools.computeNormalizedOrientation(coord, weights);
	 if (result == null) {
	     return null;
	 }
	 assert result.length == 3 && result[0] != null && result[1] != null && result[2] != null;
	 if ((result[1].lengthSquare() <= 0.0) || (result[2].lengthSquare() <= 0.0)) {
	     return null;
	 }
	 assert Math.abs(result[1].dot(result[2])) < 0.01; // must be right angle
	 result[1].normalize();
	 result[2].normalize();
	 assert Math.abs(result[1].length() - 1.0) < 0.01;
	 assert Math.abs(result[2].length() - 1.0) < 0.01;
	 assert Math.abs(result[1].dot(result[2])) < 0.01; // must be right angle
	 CoordinateSystem3D cs = new CoordinateSystem3D(result[0], result[1], result[2]);
	 // cs.rotate(new Vector3D(1.0, 0.0, 0.0), 0.5 * Math.PI); // rotate 90 degree around x axis! TODO: Why?
	 return cs;
     }

    /** returns minimum distance between object and any object in tree */
    public static double distanceMin(Object3D obj,
				     Object3D tree) {
	Object3DSet atomSet = new SimpleObject3DSet(tree); // get all atoms
	assert atomSet.size() > 0;
	return distanceMin(obj, atomSet);
    }

    /** returns minimum distance between object and any object in tree with specified class name */
    public static double distanceMin(Object3D obj,
				     Object3D tree,
				     String className) {
	Object3DSet atomSet = Object3DTools.collectByClassName(tree, className); // get all atoms
	assert atomSet.size() > 0;
	return distanceMin(obj, atomSet);
    }

    /** returns minimum distance between object and any object in set */
    public static double distanceMin(Object3D objOrig,
				     Object3DSet atomSet) {
	assert atomSet.size() > 0;
	Vector3D pos = objOrig.getPosition();
	double dMin = atomSet.get(0).getPosition().distance(pos);
	for (int i = 1; i < atomSet.size(); ++i) {
	    Object3D obj = atomSet.get(i);
	    double d = obj.getPosition().distance(pos);
	    if (d < dMin) {
		dMin = d;
	    }
	}
	return dMin;
    }

    /** returns id of object that has minimum distance between position and any object in set */
    public static int distanceMinId(Vector3D pos,
				       Object3DSet atomSet) {
	assert atomSet.size() > 0;
	int bestId = 0;
	double dMin = atomSet.get(bestId).getPosition().distance(pos);
	for (int i = 1; i < atomSet.size(); ++i) {
	    Object3D obj = atomSet.get(i);
	    double d = obj.getPosition().distance(pos);
	    if (d < dMin) {
		dMin = d;
		bestId = i;
	    }
	}
	return bestId;
    }

    /** returns center of mass */
    public static Vector3D centerOfMass(Object3DSet objSet) {
	Vector3D sum = new Vector3D(0,0,0);
	for (int i = 0; i < objSet.size(); ++i) {
	    sum.add(objSet.get(i).getPosition());
	}
	sum.scale(1.0 / objSet.size());
	return sum;
    }

    /** returns center of mass */
    public static Vector3D centerOfMass(List<Object3D> objSet) {
	Vector3D sum = new Vector3D(0,0,0);
	for (int i = 0; i < objSet.size(); ++i) {
	    sum.add(objSet.get(i).getPosition());
	}
	sum.scale(1.0 / objSet.size());
	return sum;
    }

    /** generates stair-case like array of all sizes added up */
    public static int[] generateSizeOffsets(Object3DSet set) {
	int[] result = new int[set.size()+1];
	int counter = 0;
	for (int i = 0; i < set.size(); ++i) {
	    result[i] = counter;
	    counter += set.get(i).size();
	}
	result[set.size()] = counter;
	return result;
    }

    /** Returns object 3d set with children specified in names. Throws error if one object was not found */
    public static Object3DSet getChildren(Object3D root, String[] names) throws Object3DException {
	Object3DSet result = new SimpleObject3DSet();
	for (int i = 0; i < names.length; ++i) {
	    Object3D child = root.getChild(names[i]);
	    if (child == null) {
		throw new Object3DException("Could not find child node: " + root.getFullName() + " : " + names[i]);
	    }
	    result.add(child);
	}
	return result;
    }

    /** returns vector of 3d coordinates */
    public static Vector3D[] getCoordinates(Object3DSet objSet) {
	Vector3D[] result = new Vector3D[objSet.size()];
	for (int i = 0; i < objSet.size(); ++i) {
	    result[i] = objSet.get(i).getPosition();
	}
	return result;
    }

    /** returns first found ancestor element if decendant is in subtree in which ancestor is root node */
    public static Object3D findAncestor(Object3DSet ancestors, Object3D decendant) {
	assert decendant != null;
	for (int i = 0; i < ancestors.size(); ++i) {
	    Object3D anc = ancestors.get(i);
	    if ((anc != null) && anc.isAncestor(decendant)) {
		return anc;
	    }
	}
	return null; // nothing found
    }


}
