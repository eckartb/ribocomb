package tools3d.objects3d;

import java.util.Properties;

public interface Object3DSimilarity {

    public static final String DRMS = "drms";

    public static final String SUPERPOSABLE = "superposable";

    Properties similarity(Object3D obj1, Object3D obj2);

}
