package tools3d.objects3d;

/** Interface for "check" method that returns yes/no answer for an Object3D */
public interface Object3DTester {

    boolean check(Object3D object);

}
