package tools3d.objects3d;

import java.io.PrintStream;
import java.util.*;
import java.util.logging.*;
import generaltools.Randomizer;
import tools3d.GeometryTools;
import tools3d.Matrix3D;
import tools3d.SuperpositionResult;
import tools3d.Vector3D;
import tools3d.Vector3DTools;
import tools3d.Matrix3D;
import tools3d.Matrix3DTools;

import static tools3d.PackageConstants.*;

public class Object3DTools {

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    private static Random rnd = Randomizer.getInstance();

    public static boolean debugMode = false; // if true, debug checks are switched on

    /** system independent newline
     * @see http://www.leepoint.net/notes-java/GUI/components/40textarea/40newline.html
     */
    public static final String ENDL = System.getProperty("line.separator");

    public static void addEnding(Object3D tree, String ending, 
				 Set<String> allowedNames,
				 Set<String> forbiddenNames) {
	assert tree != null;
	if (checkPrintable(tree.getClassName(), allowedNames, forbiddenNames)) {
	    tree.setName(tree.getName() + ending);
	}
	for (int i = 0; i < tree.size(); ++i) {
	    addEnding(tree.getChild(i), ending, allowedNames, forbiddenNames);
	}
    }

    /** Applies superposition result that originated from class Superimpose in tools3d */
    public static void applySuperposition(Object3D obj, 
				    Matrix3D rot, Vector3D mc1, Vector3D mc2) {
	obj.translate(mc2.mul(-1));
	obj.rotate(Vector3D.ZVEC, rot);
	obj.translate(mc1);
    }

    /** Validates string of non composite object3d name */
    public static boolean validateName(String name) {
	return name != null && name.length() > 0;
    }

    public static void applyTransformation(Object3D tree, SuperpositionResult superposition) {
	tree.setIsolatedPosition(superposition.returnTransformed(tree.getPosition()));
	for (int i = 0; i < tree.size(); ++i) {
	    applyTransformation(tree.getChild(i), superposition);
	}
    }

    /** Apply symmetry transformation */
    public static void applySymmetry(Object3D tree, int symId) {
	tree.setIsolatedPosition(tree.getPosition(symId));
	for (int i = 0; i < tree.size(); ++i) {
	    applySymmetry(tree.getChild(i), symId);
	}
    }

    /** returns true, if object name is legal */
    public static boolean isAllowedName(String name) {
	if ((name == null) || (name.length() == 0) ) {
	    return false;
	}
	if (name.indexOf('.') >= 0) {
	    return false; // dots are not allowed
	}
	if (name.indexOf(',') >= 0) {
	    return false; 
	}
	if (name.indexOf('$') >= 0) {
	    return false; 
	}
	return true; // so far everything else is allowed
    }

    /* transform molecular positions to new coordinates
     * @returns Transforming coordinate system or null if undefined (for less than 3 positions)
     */
     public static CoordinateSystem3D applyNormalizedOrientation(Object3D root, String className) {
	 Object3DSet objects = collectByClassName(root, className);
	 CoordinateSystem3D cs = Object3DSetTools.computeNormalizedOrientation(objects);
	 if (cs == null) {
	     return null;
	 }
	 root.passiveTransform3(cs);
	 return cs;
     }

    /** returns true if decendant is in subtree in which ancestor is root node */
    public static boolean isAncestor(Object3D ancestor, Object3D decendant) {
	if (ancestor == null) {
	    return false;
	}
	if (decendant == null) {
	    return false;
	}
	if (decendant.getParent() == null) {
	    return false;
	}
	if (decendant.getParent() == ancestor) {
	    return true;
	}
	return isAncestor(ancestor, decendant.getParent());
    }

    /** returns first object in list that is an ancestor of the node */
    public static Object3D findAncestor(Object3D node, List<? extends Object3D> list) {
	for (int i = 0; i < list.size(); ++i) {
	    if (Object3DTools.isAncestor(list.get(i), node)) {
		return list.get(i);
	    }
	}
	return null;
    }

    /** returns first object in list that is an ancestor or same compared to node */
    public static Object3D findAncestorOrEqual(Object3D node, List<? extends Object3D> list) {
	for (int i = 0; i < list.size(); ++i) {
	    if ((node == list.get(i)) || (Object3DTools.isAncestor(list.get(i), node))) {
		return list.get(i);
	    }
	}
	return null;
    }

    /** returns first ancestor with certain class name */
    public static Object3D findAncestorByClassName(Object3D obj, String className) {
	if (obj == null) {
	    return null;
	}
	if (obj.getParent() == null) {
	    return null;
	}
	if (obj.getParent().getClassName().equals(className)) {
	    return obj.getParent();
	}
	return findAncestorByClassName(obj.getParent(), className); // recursive call
    }

    /** returns true, if all branch descriptor occupy a free "corridor" with a certain radius.
     * If supplied radius is smaller or equal zero, true if returned. */
    public static boolean corridorCheck(Vector3D position, Vector3D direction, double corridorRadius,
					double corridorStart, Object3DSet atomSet) {
	for (int i = 0; i < atomSet.size(); ++i) {
	    if (!corridorCheck(position, direction, corridorRadius, corridorStart, atomSet.get(i).getPosition())) {
		return false;
	    }
	}
	return true;
    }

    /** returns true, if all branch descriptor occupy a free "corridor" with a certain radius.
     * If supplied radius is smaller or equal zero, true if returned. */
    public static boolean corridorCheck(Vector3D position, Vector3D direction, double corridorRadius,
					double corridorStart, Vector3D atomPos) {
	Vector3D newDir = new Vector3D(direction);
	newDir.normalize();
	double proj = newDir.dot(atomPos.minus(position));
	if (proj < corridorStart) {
	    return true;
	}
	double dist = Math.abs(GeometryTools.distanceToLine(atomPos, position, direction));
	return (dist > corridorRadius);
    }

    /** returns distance to child furthest away */
    public static double generateBoundingRadius(Object3D obj) {
	double dMax = 0.0;
	for (int i = 0; i < obj.size(); ++i) {
	    double d = obj.getChild(i).getRelativePosition().length();
	    if (d > dMax) {
		dMax = d;
	    }
	}
	return dMax;
    }

    /** generates string with all child names */
    public static String generateChildNames(Object3D root) {
	StringBuffer buf = new StringBuffer();
	for (int i = 0; i < root.size(); ++i) {
	    buf.append(root.getChild(i).getName() + " ");
	}
	return buf.toString();
    }
    
    /** returns strings like 1.4.7 indicating the seventh great-grand child of the fourth grand child of the first child of the root node */
//     public static String generateIndexName(Object3D obj) {
// 	if (obj.getParent() == null) {
// 	    return "1";
// 	}
// 	return generateIndexName(obj.getParent()) + "." + (obj.getSiblingId() + 1);
//     }

    public static String getProperty(Object3D obj, String key) {
	Properties properties = obj.getProperties();
	if (properties == null) {
	    return null;
	}
	return properties.getProperty(key);
    }

    public static void setProperty(Object3D obj, String key, String value) {
	Properties properties = obj.getProperties();
	if (properties == null) {
	    properties = new Properties();
	}
	properties.setProperty(key, value);
	obj.setProperties(properties);
    }

    /** sets property for all objects and child objects with matching className. 
     * Classname is ignored if empty string is provided 
     */
    public static void setRecursiveProperty(Object3D obj, String key, String value,
					    String className) {
	if (obj.getClassName().equals(className)) {
	    setProperty(obj, key, value);
	}
	for (int i = 0; i < obj.size(); ++i) {
	    setRecursiveProperty(obj.getChild(i), key, value, className);
	}
    }

    /** sets property for all objects and child objects with matching className. 
     */
    public static void setRecursiveProperty(Object3D obj, String key, String value) {
	setProperty(obj, key, value);
	for (int i = 0; i < obj.size(); ++i) {
	    setRecursiveProperty(obj.getChild(i), key, value);
	}
    }

    /** returns first leaf node found with depth-first search */ 
    public static Object3D findFirstLeaf(Object3D tree) {
	assert tree != null;
	Object3D result = tree;
	while (result.size() > 0) {
	    result = result.getChild(0);
	}
	return result;
    }

    /** returns "full" object name like "root.strandA.G15" */
    public static String getFullName(Object3D obj) {
	if (obj.getParent() == null) {
	    return obj.getName();
	}
	String name = obj.getName();
	Object3D curr = obj;
	while (curr.getParent() != null) {
	    curr = curr.getParent();
	    name = curr.getName() + "." + name;
	}
	return name;
    }

    /** returns "full" object name like "atom C -> residue G15 -> strand A -> simple import -> root " */
    public static String getFullPrettyString(Object3D obj) {
	assert false; // NOT YET IMPLEMENTED
	String result = "";
	return result;
    }

    /** returns number indicating which child id object has (counting starts at one) */
    public static String getIndexName(Object3D obj) {
	return "" + (obj.getSiblingId() + 1);
    }

    /** returns "full" object name like "root.strandA.G15" */
    public static String getFullIndexName(Object3D obj) {
	String name = getIndexName(obj);
	Object3D curr = obj;
	while (curr.getParent() != null) {
	    curr = curr.getParent();
	    name = getIndexName(curr) + "." + name;
	}
	return name;
    }
    
    /** returns object3d with that name or null if not found */
    public static Object3D find(Object3D root, String name) {
	return Object3DNameCollector.find(root, name);
    }

    /** returns root of object */
    public static Object3D findRoot(Object3D objOrig) {
	Object3D obj = objOrig;
	while (obj.getParent() != null) {
	    obj = obj.getParent();
	}
	return obj;
    }
    
    /** returns object3d with that name or null if not found.
     *  Names can be of the form: root.import.pdb.A.G5.P or alternatively
     * 1.1.3.2.5.1 . The naming by index of by names can be mixed!
     * Tricky workaround: for RNA strands, also the names "0", "1", ..., "9" are allowed.
     * If the user requests an object import.2 , it is first checked if there
     * is an RnaStrand object with name "2", if not the second child node of the import node is returned.*/
    public static Object3D findByFullName(Object3D root, String name) {
	// names separated by dots:
	// String[] tokens = name.split("\."); 
	assert name.length() > 0;
	if (name.charAt(0) == '.') {
	    name = "root" + name; // convert .hello to root.hello
	}
	
	StringTokenizer tokenizer = new StringTokenizer(name, ".");
	Object3D current = root;
	// log.info("Starting findByFullName with " + name);
	String firstToken = tokenizer.nextToken(); // used to skip "root"
	if (firstToken == null) {
	    return null;
	}
	if (! (firstToken.equals(root.getName())  || firstToken.equals("1"))) {
	    return null;
	}
	while (tokenizer.hasMoreTokens()) {
	    String token = tokenizer.nextToken();
	    // log.info("Trying to find " + token + " using current node: " + current.getFullName());
	    if (token.length() == 0) {
		return null;
	    }
	    if (token.charAt(0) == '(') {
		String[] subwords = token.split("\\)");
		if (subwords.length != 2) {
		    return null;
		}
		subwords[0] = subwords[0].substring(1); // skip first character: (helix)2 is parsed to words helix and 2
		try {
		    int id = Integer.parseInt(subwords[1]) - 1; // convert to internal counting
		    String className = subwords[0];
		    // for convinience: translate some words from simplified versions
		    if (className.equals("hxend")) {
			className = "BranchDescriptor3D";
		    }
		    else if (className.equals("helixend")) {
			className = "BranchDescriptor3D";
		    }
		    else if (className.equals("atom")) {
			className = "Atom3D";
		    }
		    else if (className.equals("strand")) {
			className= "RnaStrand";
		    }
		    else if (className.equals("residue")) {
			className = "Nucleotide3D";
		    }
		    else if (className.equals("stem")) {
			className = "RnaStem3D";
		    }
		    int idx = current.getIndexOfChild(id, className);
		    if ((idx < 0) || (idx >= current.size())) {
			return null; // could not find appropriate child id
		    }
		    current = current.getChild(idx);
		}
		catch (NumberFormatException nfe ) {
		    log.warning("Number format exception: " + nfe.getMessage());
		    return null;
		}
	    }
	    else {
 		// log.info("trying to find child node with name : " + token 
		// + " in child: " + current.getFullName());
		Object3D currentTmp = current.getChild(token); // first check if object with this name exists
		if (currentTmp == null) {
		    if (token.equals("LAST")) { // LAST is special case: accesses that child node with the highest id
			if (current.size() == 0) {
			    log.warning("LAST operator not meaningful on object with zero child nodes: " + root.getFullName());
			} else {
			    current = current.getChild(current.size()-1);
			}
		    } else {
			// log.info("child not found under name! Trying number...");
			try {
			    int id = Integer.parseInt(token) - 1; // internal counting starts at zero
			    if ((id < 0) || (id >= current.size())) {
				return null; // could not find appropriate child id
			    }
			    current = current.getChild(id);
			    assert current != null;
			}
			catch (NumberFormatException e) {
			    if (current == null) {
				String childNames = "";
				for (int i = 0; i < root.size(); ++i) {
				    childNames += root.getChild(i).getName() + " ";
				}
				log.warning("Could not find object with name " + name + " in " + root.getFullName() + " : " + childNames);
				return null;
			    }
			}
		    }
		}
		else {
		    current = currentTmp;
		    // log.info("Child node found! New current node: " + current.getFullName());
		}
	    }
	    // log.info("Found : " + current.getFullName());
	}
	// log.info("Result of findByFullName: " + current.getFullName());
	return current;
    }

    /** add to object3dSet objects with that class name 
     * or null if not found 
     */
    private static void addByClassName(Object3D root,
				       String className,
				       Object3DSet objectSet) {
	if (debugMode) {
	    // just for debugging
	    for (int i = 0; i < objectSet.size(); ++i) {
		assert objectSet.get(i).getClassName().equals(className);
	    }
	}
	boolean found = false;
	if (root.getClassName().equals(className)) {
	    found = true;
	    objectSet.add(root);
	}
	// only simple Object3D objects allow same type at different levels in hierarchy: major speed up!
	if ((!found) || (root.getClassName().equals("Object3D"))) {
	    for (int i = 0; i < root.size(); ++i) {
		addByClassName(root.getChild(i), className, objectSet);
	    }
	}

	if (debugMode) {
	    for (int i = 0; i < objectSet.size(); ++i) {
		assert objectSet.get(i).getClassName().equals(className);
	    }
	}
    }

    /** returns index of found word in array or -1 if not found. */
    private static int indexOf(String name,
			   String[] words) {
	for (int i = 0; i < words.length; ++i) {
	    if (name.equals(words[i])) {
		return i;
	    }
	}
	return -1;
    }

    /** add to object3dSet objects with that class name 
     * or null if not found 
     */
    private static void addByClassName(Object3D root,
				       String className,
				       Object3DSet objectSet,
				       String[] breakupNames) {
	if (indexOf(root.getClassName(), breakupNames) >= 0) {
	    return; // forbidden name found. quit search
	}
	boolean found = false;
	if (root.getClassName().equals(className)) {
	    found = true;
	    objectSet.add(root);
	}
	// only simple Object3D objects allow same type at different levels in hierarchy: major speed up!
	if ((!found) || (root.getClassName().equals("Object3D"))) {
	    for (int i = 0; i < root.size(); ++i) {
		addByClassName(root.getChild(i), className, objectSet);
	    }
	}
    }

    /** returns object3dSet with objects with that class name 
     * or null if not found 
     */
    public static Object3DSet collectByClassName(Object3D root, String className) {
	assert className != null;
	Object3DSet result = new SimpleObject3DSet();
	addByClassName(root, className, result);
	if (debugMode) {
	    for (int i = 0; i < result.size(); ++i) {
		assert result.get(i).getClassName().equals(className);
	    }
	}
	return result;
    }

    /** returns object3dSet with objects of Object3DSet with that class name 
     * or null if not found 
     */
    public static Object3DSet collectByClassName(Object3DSet rootSet, String className) {
	assert className != null;
	Object3DSet result = new SimpleObject3DSet();
	for (int i = 0; i < rootSet.size(); ++i) {
	    result.merge(collectByClassName(rootSet.get(i), className));
	}
	return result;
    }

    /** returns object3dSet with objects with that class name corresponding to named subtrees
     */
    public static Object3DSet collectByClassName(Object3D root, String[] names, String className) {
	assert className != null;
	Object3DSet result = new SimpleObject3DSet();
	for (String s : names) {
	    Object3D subTree = findByFullName(root, s);
	    if (subTree != null) {
		Object3DSet subset = collectByClassName(subTree, className);
		result.merge(subset);
	    }
	}
	for (int i = 0; i < result.size(); ++i) {
	    assert result.get(i).getClassName().equals(className);
	}
	return result;
    }

    /** returns closest object of certain class 
     */
    public static Object3D findClosestByClassName(Object3D root, Vector3D position, String className) {
	Object3DSet resultSet = collectByClassName(root, className);
	assert resultSet != null;
	if (resultSet.size() == 0) {
	    return null; // no object of that class found
	}
	return resultSet.get(Object3DSetTools.distanceMinId(position, resultSet));
    }

    /** Finds leaf child node (or self) of object that is closest to position */
    private static Object3D findClosestLeaf(Object3D obj, Vector3D pos, Object3D sofarBest) {
	assert obj != null;
	assert pos != null;
	if (obj.size() == 0) {
	    if (sofarBest == null) {
		assert obj != null && obj.size() == 0;
		return obj;
	    }
	    double bestDist = sofarBest.getPosition().distance(pos);
	    double dist = obj.getPosition().distance(pos);
	    if (dist < bestDist) {
		assert obj != null && obj.size() == 0;
		return obj;
	    }
	    else {
		assert sofarBest != null && sofarBest.size() == 0;
		return sofarBest;
	    }
	}
	else {
	    for (int i = 0; i < obj.size(); ++i) {
		sofarBest = findClosestLeaf(obj.getChild(i), pos, sofarBest);
	    }
	}
	assert sofarBest != null && sofarBest.size() == 0;
	return sofarBest;
    }

    /** Finds leaf child node (or self) of object that is closest to position */
    public static Object3D findClosestLeaf(Object3D obj, Vector3D pos) {
	Object3D result = findClosestLeaf(obj, pos, null);
	assert (result != null) && (result.size() == 0) && (obj == result || isAncestor(obj, result));
	return result;
    }

    /** returns object with specified classname that contains leaf node closest to a specified position */
    public static Object3D findClosestLeafByClassName(Object3D root, Vector3D position, String className) {
	log.info("Started findClosestLeafByClassName!");
	if (root == null) {
	    return null;
	}
	Object3DSet resultSet = collectByClassName(root, className);
	int bestId = 0;
	double bestLeafDist = 1e30;
	for (int i = 0; i < resultSet.size(); ++i) {
	    Object3D closestLeaf = findClosestLeaf(resultSet.get(i), position);
	    double leafDist = closestLeaf.getPosition().distance(position);
	    if (leafDist < bestLeafDist) {
		bestLeafDist = leafDist;
		bestId = i;
	    }
	}
	if (resultSet.size() == 0) {
	    return null;
	}
	log.info("Finished findClosestLeafByClassName: " + position + " :"  + resultSet.get(bestId).getFullName() + " : " + resultSet.get(bestId).getPosition());
	return resultSet.get(bestId);

    }

    /** returns object3dSet with objects with that class name 
     * or null if not found 
     * stop search if nodes with class names found in breakupNames found.
    */
    public static Object3DSet collectByClassName(Object3D root, 
						 String className,
						 String[] breakupNames) {
	    assert root != null;
	    assert className != null;
	    assert className.length() > 0;
	    Object3DSet result = new SimpleObject3DSet();
	    addByClassName(root, className, result, breakupNames);
	    assert result != null;
	    return result;
    }

    /** randomly orients object tree */
    public static void randomizeOrientation(Object3D root) {
	Vector3D axis = Vector3DTools.generateRandomDirection();
	double angle = 2.0 * Math.PI * rnd.nextDouble();
	// log.fine("axis, angle: " + axis + " angle: " + angle);
	root.rotate(root.getPosition(), Matrix3D.rotationMatrix(axis, angle));
    }

    /** randomly orients object tree */
    public static void randomizeOrientation(Vector3D center, Object3D root) {
	double angle = 2.0 * Math.PI * rnd.nextDouble();
	// log.fine("axis, angle: " + axis + " angle: " + angle);
	root.rotate(center, Matrix3D.rotationMatrix(center, angle));
    }

    /** randomly translates object tree */
    public static void randomizeTranslation(Object3D root, double scale) {
	Vector3D direction = Vector3DTools.generateRandomDirection();
	Random rnd = Randomizer.getInstance();
	double gauss = scale * Math.abs(rnd.nextGaussian());
	direction.scale(gauss);
	root.translate(direction); // shift, no rotation
    }

    /** removes object from tree if it is not the root */
    public static void remove(Object3D obj) {
	Object3D parent = obj.getParent();
	if (parent != null) {
	    parent.removeChild(obj);
	}
    }

    /** human readable version of object tree */
    public static void printTree(PrintStream ps, Object3D root) {
	int d = root.getDepth();
	for (int i = 0; i < d; ++i) {
	    ps.print(" ");
	}
	ps.println(root.getClassName() + " " + root.getName() + " " + Vector3DTools.prettyString(root.getPosition(),7,3) + " " + root.size());
	for (int i = 0; i < root.size(); ++i) {
	    printTree(ps, root.getChild(i));
	}
    }

    /** human readable version of object tree */
    public static String generateFullTreeLine(Object3D root) {
	if (root == null) {
	    return "undefined";
	}
	return getFullName(root) + " " + getFullIndexName(root) + " " + root.size() + " " + root.getClassName() + " " 
	    + Vector3DTools.prettyString(root.getPosition(),7,3);
    }

    public static Vector<String> getFullNameTree(Object3D root,
						 Set<String> allowedNames,
						 Set<String> forbiddenNames) {
	Vector<String> result = new Vector<String>();
	if (checkPrintable(root.getClassName(),
			   allowedNames,
			   forbiddenNames)) {
	    result.add(generateFullTreeLine(root));
	}
	// System.out.println(root.getClassName());
	if ((forbiddenNames == null) || (! forbiddenNames.contains(root.getClassName()))) {
	    // only go one lever deeper if it was printable in the first place
	    for (int i = 0; i < root.size(); i++) {
		Vector<String> childResult = getFullNameTree(root.getChild(i),
							     allowedNames,
							     forbiddenNames);
		for (String line : childResult) {
		    result.add(line);
		}
	    }
	}
	return result;
    }


    /** human readable version of object tree */
    public static void printFullNameTree(PrintStream ps, Object3D root,
					 Set<String> allowedNames,
					 Set<String> forbiddenNames) {
	String printTree = "";
	Vector<String> tree = getFullNameTree(root,
				      allowedNames, forbiddenNames);
	for (int i = 0; i < tree.size(); i++) {
	    ps.println(tree.get(i));
	}
	// ps.println(printTree);
	// if (checkPrintable(root.getClassName(), allowedNames, forbiddenNames)) {
	// ps.println(generateFullTreeLine(root));
	// }
	// for (int i = 0; i < root.size(); ++i) {
	// printFullNameTree(ps, root.getChild(i), allowedNames, forbiddenNames);
	// }
    }

    /** returns true if current class name is allowed */
    private static boolean checkPrintable(String className,
					  Set<String> allowedNames,
					  Set<String> forbiddenNames) {
	if (className == null) {
	    return false;
	}
	if ((allowedNames != null) && (allowedNames.size() > 0)) {
	    // must be in here:
	    if (!allowedNames.contains(className)) {
		return false;
	    }
	}
	if ((forbiddenNames != null) && (forbiddenNames.size() > 0)) {
	    // is not allowed to be part of forbiddenNames:
	    if (forbiddenNames.contains(className)) {
		return false;
	    }
	}
	return true;
    }

    /** human readable version of object tree */
    public static void printFullNameTree(StringBuffer buf, Object3D root,
					 Set<String> allowedNames,
					 Set<String> forbiddenNames) {
	
	Vector<String> tree = getFullNameTree(root,
					      allowedNames,
					      forbiddenNames);
	for (int i = 0; i < tree.size(); i++) {
	    buf.append(tree.get(i) + NEWLINE);
	}
	// if (checkPrintable(root.getClassName(), allowedNames, forbiddenNames)) {
	// buf.append(generateFullTreeLine(root));
	// }
	// for (int i = 0; i < root.size(); ++i) {
	// printFullNameTree(buf, root.getChild(i), allowedNames, forbiddenNames);
	// }
    }

    /** human readable version of object tree */
    public static void printTree(StringBuffer buf, Object3D root) {
	int d = root.getDepth();
	for (int i = 0; i < d; ++i) {
	    buf.append(" ");
	}
	buf.append(root.getClassName() + " " + root.getName() + " " + root.getPosition() + " " + root.size() + ENDL);
	for (int i = 0; i < root.size(); ++i) {
	    printTree(buf, root.getChild(i));
	}
    }

    /** removes object from tree if it is not the root and if the class name matches */
    public static void removeByClassName(Object3D obj, String className) {
	if (obj.getClassName().equals(className)) {
	    remove(obj);
	}
	else {
	    for (int i = obj.size()-1; i >= 0; --i) {
		Object3D child = obj.getChild(i);
		if (child.getClassName().equals(className)) {
		    obj.removeChild(i);
		}
		else {
		    removeByClassName(child, className);
		}
	    }
	}
    }

    /** returns object3d with that name or null if not found */
    public static void setSelectAll(Object3D root, boolean choice) {
	Object3DActionVisitor.visitAll(root,
	       new Object3DSetSelected(choice));
    }

    /** stretches relative positions of all object3d's in tree */
    public static void stretch(Object3D obj, double scale) {
	obj.setRelativePosition(obj.getRelativePosition().mul(scale));
	for (int i = 0; i < obj.size(); ++i) {
	    stretch(obj.getChild(i), scale);
	}
    }

    /** position of each node is center of mass */
    public static void balanceTree(Object3D root) {
	if (root.size() == 0) {
	    return; // do nothing if no children nodes
	}
	// TODO : bad workaround :-( . Better: use new interface 
	if (root.getClassName().equals("BranchDescriptor3D")
	    || root.getClassName().equals("StrandJunction3D")
	    || root.getClassName().equals("KissingLoop3D")
	    || root.getClassName().equals("CoordinateSystem3D")) {
	    return;
	}

	for (int i = 0; i < root.size(); ++i) {
	    Object3D child = root.getChild(i);
	    balanceTree(child);
	}
	Vector3D avg = new Vector3D(0,0,0);
	for (int i = 0; i < root.size(); ++i) {
	    Object3D child = root.getChild(i);
	    avg.add(child.getPosition());
	}
	Vector3D testPos = root.getChild(0).getPosition();
	avg.scale(1.0/(double)(root.size()));
	root.setIsolatedPosition(avg); // set to center of mass, do not change position of children 
	Vector3D testPos2 = root.getChild(0).getPosition(); // position of child should not be changed!
	assert (testPos.distance(testPos2) < 0.001); // check of child position has moved
    }

    /** rotates objects individually (using setIsolatedPosition) */
    public static void rotateSet(Object3DSet objectSet, Vector3D axis, Vector3D center, double angle) {
	for (int i = 0; i < objectSet.size(); ++i) {
	    Object3D obj = objectSet.get(i);
	    obj.setIsolatedPosition(Matrix3DTools.rotate(obj.getPosition(), axis, angle, center));
	}
    }

    /** Recursively traverses object tree and fills a HashSet with the references to all objects */
    public static HashSet<Object3D> convertTreeToHash(Object3D root) {
	HashSet<Object3D> result = new HashSet<Object3D>();
	result.add(root);
	for (int i = 0; i < root.size(); ++i) {
	    result.addAll(convertTreeToHash(root.getChild(i)));
	}
	return result;
    }


}
