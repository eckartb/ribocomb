package tools3d.objects3d;

import tools3d.Vector3D;

/** translates current object */
public class Object3DTranslator implements Object3DAction {

    Vector3D v;

    public Object3DTranslator(Vector3D v) { this.v = v; }

    public void act(Object3D o) {
	o.translate(v);
    }

}
