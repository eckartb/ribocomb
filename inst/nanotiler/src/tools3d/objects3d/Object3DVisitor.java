package tools3d.objects3d;

import java.util.Iterator;

/** implements visitor pattern */
public interface Object3DVisitor extends Iterator {

    // central "accept" method: could be used for obtaining or chainging content */
    public void visit(Object3D o);

}
