/** writes scence graph
 * 
 */
package tools3d.objects3d;

import java.io.OutputStream;

/**
 * @author Eckart Bindewald
 *
 */
public interface Object3DWriter extends Object3DFormatter {
	
	public void write(OutputStream os, Object3D tree);

	public void write(OutputStream os, LinkSet links);
	
	public String writeString(Object3D tree);
	
}
