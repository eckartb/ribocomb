package tools3d.objects3d;

public interface Object3DWriterTools extends Object3DFormatter {

    /** writes integer array */
    public String writeIntArray(int[] ary);

    /** returns header string. Central for switching formats! */
    public String getHeader(String className);

    /** returns header string of object of class "className". 
     * Central for switching formats! 
     */
    public String getFooter(String className);

    /** encloses body text by item specified <className> bodyText </className> in XML for example */
    public String encloseBody(String className, String bodyText);

    public String writeBoolean(String name, boolean b);

}
