package tools3d.objects3d;

import java.awt.Color;

import tools3d.CoordinateSystem;
import tools3d.Positionable3D;
import tools3d.Vector3D;

public class Polygon3D implements Primitive3D {

    double zBufValue = 0;

    Vector3D[] points;

    Color color;

    public Polygon3D(Vector3D[] _points, Color _color) {
    	if (_points != null) {
			points = new Vector3D[_points.length];
			for (int i = 0; i < _points.length; ++i) {
				points[i] = (Vector3D)(_points[i].clone());
			}
    	}
    	else {
    		points = new Vector3D[0];
    	}
		if (_color != null) {
			color = new Color(_color.getRed(), _color.getGreen(), _color.getBlue());
		}
    }

    public void clear() {
		points = null;
		color = null;
    }

    public void activeTransform(CoordinateSystem cs) {
	for (int i = 0; i < points.length; ++i) {
	    points[i] = cs.activeTransform(points[i]);
	}
    }

    public void passiveTransform(CoordinateSystem cs) {
	for (int i = 0; i < points.length; ++i) {
	    points[i] = cs.passiveTransform(points[i]);
	}
    }

    /** clones object */
    public Object clone() {
    	Polygon3D result = new Polygon3D(points, color);
    	return result;
    }

    public Object cloneDeep() {
	Vector3D[] newPoints = new Vector3D[this.points.length];
	for (int i = 0; i < newPoints.length; ++i) {
	    newPoints[i] = new Vector3D(points[i]);
	}
	return new Polygon3D(newPoints, color);
    }
    
    /** used for comparing object (< == or >) */
    public int compareTo(Positionable3D o) {
	if (o instanceof Primitive3D) {
	    Primitive3D p = (Primitive3D)o;
	    double val = zBufValue;
	    double oVal = p.getZBufValue();
	    if (val < oVal) {
		return -1;
	    }
	    else if (val == oVal) {
		return 0;
	    }
	    return 1;
	}
	return 1;
    }


    /** return distance between objects */
    public double distance(Positionable3D other) {
	return (getPosition().minus(other.getPosition())).length();
    }

    /** returns n'th point */
    public Vector3D getPoint(int n) {
		return points[n];
    }

    /** return average position */
    public Vector3D getPosition() {
		Vector3D sum = new Vector3D(0.0, 0.0, 0.0);
		if (points.length == 0) {
		    return sum;
		}
		for (int i = 0; i < points.length; ++i) {
		    sum.add(points[i]);
		}
		sum.mul(1.0 / (double)points.length);
		return sum;
    }

    public double getZBufValue() {
	return zBufValue;
    }

    /** returns color */
    public Color getColor() {
		return color;
    }

    /** returns true if position is well defined (not Not-a-number) */
    public boolean isValid() { return getPosition().isValid(); }

    /** sets color */
    public void setColor(Color c) {
		color = c;
    }

    public void setZBufValue(double z) {
	zBufValue = z;
    }

    /** returns number of points */
    public int size() {
	if (points == null) {
	    return 0;
	}
	return points.length;
    }

    public void translate(Vector3D shift) {
	for (int i = 0; i < points.length; ++i) {
	    points[i] = points[i].plus(shift);
	}
    }

}
