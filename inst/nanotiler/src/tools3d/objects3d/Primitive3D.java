package tools3d.objects3d;

import java.awt.Color;

import tools3d.Positionable3D;
import tools3d.Vector3D;

/**
 *  Describes simple polygon or line or point
 */
public interface Primitive3D extends Comparable<Positionable3D>, Positionable3D {

    /** deletes all info */
    public void clear();

    /** clone method inherited from Object */
    public Object clone();
    
    /** returns average of poins */
    public Vector3D getPosition();
    
    /** returns color */
    public Color getColor();

    /** returns n'th point */
    public Vector3D getPoint(int n);

    /** returns value for z-buffer queue */
    public double getZBufValue();

    /** sets color */
    public void setColor(Color c);

    /** returns number of points */
    public int size();

    /** sets value for zbuf hierarch */
    public void setZBufValue(double v);

}
