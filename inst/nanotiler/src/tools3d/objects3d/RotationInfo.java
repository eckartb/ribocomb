package tools3d.objects3d;

import java.util.ArrayList;

/**
 * @author Christine Viets
 */
public class RotationInfo {

    LinkSet linkSet;
    // int[] placeArray;
    Object3D root;
    // ArrayList rotateList;

    public RotationInfo() { }

    public RotationInfo(Object3D root,
			LinkSet linkSet) { // ,
			// int[] placeArray,
			// ArrayList rotateList) {
	setRoot(root);
	setLinkSet(linkSet);
	// setPlaceArray(placeArray);
	// setRotateList(rotateList);
    }

    public LinkSet getLinkSet() { return linkSet; }
    // public int[] getPlaceArray() { return placeArray; }
    public Object3D getRoot() { return root; }
    // public ArrayList getRotateList() { return rotateList; }

    public void setLinkSet(LinkSet newSet) {
	if (root != null) {
	    linkSet = (LinkSet)(newSet.cloneDeep(root));
	}
	else { //Not good??
	    linkSet = (LinkSet)(newSet.cloneDeep());
	}
    }

    // public void setPlaceArray(int[] newArray) {
    // placeArray = (int[])(newArray.clone());
    // }

    public void setRoot(Object3D newRoot) {
	root = (Object3D)(newRoot.cloneDeep());
	if (linkSet != null) {
	    // TODO: check that links are up to date
	}
    }

    // public void setRotateList(ArrayList newList) {
    // rotateList = (ArrayList)(newList.clone());
    // }

}
