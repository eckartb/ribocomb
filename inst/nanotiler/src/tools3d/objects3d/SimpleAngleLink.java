/** implements concept of two linked objects */
package tools3d.objects3d;

import tools3d.Vector3D;
import generaltools.ConstraintDouble;
import generaltools.SimpleConstraintDouble;

public class SimpleAngleLink extends SimpleLink implements AngleLink {

    private ConstraintDouble constraint;
    private Object3D obj3;

    public SimpleAngleLink(Object3D o1, Object3D o2, Object3D _oCenter, ConstraintDouble constraint) {
	super(o1, o2);
	this.obj3 = _oCenter;
	this.constraint = constraint;
    }

    public SimpleAngleLink(Object3D o1, Object3D o2, Object3D _oCenter, double min, double max) {
	super(o1, o2);
	this.obj3 = _oCenter;
	this.constraint = new SimpleConstraintDouble(min, max);	
    }

    /** returns constraint */
    public ConstraintDouble getConstraint() { return constraint; }

    public Object3D getObj3() { return obj3; }

    public double computeError() {
	return computeError(getObj1().getPosition(), getObj2().getPosition(), getObj3().getPosition());
    }

    public double computeError(Vector3D pos1, Vector3D pos2, Vector3D pos3) {
	Vector3D v1 = pos1.minus(pos3);
	Vector3D v2 = pos2.minus(pos3);
	double ang = v1.angle(v2);
	double result = 0.0;
	if (ang < constraint.getMin()) {
	    result = constraint.getMin() - ang;
	} else if (ang > constraint.getMax()) {
	    result = constraint.getMax() - ang;
	}
	return result;
    }

}
	
