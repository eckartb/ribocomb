/** implements concept of two linked objects */
package tools3d.objects3d;

import generaltools.ConstraintDouble;
import generaltools.SimpleConstraintDouble;

public class SimpleConstraintLink extends SimpleLink implements ConstraintLink, SymmetryLinker {

    private ConstraintDouble constraint;

    private int symId1 = 0; // symmetry copy id of object1

    private int symId2 = 0; // symmetry copy id of object2;

    public SimpleConstraintLink(ConstraintDouble constraint) {
	super();
	this.constraint = constraint;
    }
    
    public SimpleConstraintLink(Object3D o1, Object3D o2, ConstraintDouble constraint) {
	super(o1, o2);
	this.constraint = constraint;
    }

    public SimpleConstraintLink(Object3D o1, Object3D o2, double min, double max) {
	super(o1, o2);
	this.constraint = new SimpleConstraintDouble(min, max);
	
    }

    public SimpleConstraintLink(Object3D o1, Object3D o2, double min, double max, int _symId1, int _symId2) {
	super(o1, o2);
	this.constraint = new SimpleConstraintDouble(min, max);	
	this.symId1 = _symId1;
	this.symId2 = _symId2;
    }

    /** returns constraint */
    public ConstraintDouble getConstraint() { return constraint; }

    public int getSymId1() { return symId1; }

    public int getSymId2() { return symId2; }

    public void setSymId1(int _symId1) { this.symId1 = _symId1; }

    public void setSymId2(int _symId2) { this.symId2 = _symId2; }

    public String toString() {
	String result = "(ConstraintLink " + getName() + " " + getObjectName(obj1) + " " + getObjectName(obj2);
	result = result +  " " + getSymId1() + " " + getSymId2() + " " + getConstraint().toString() + " )";
	return result;
    }

}
	
