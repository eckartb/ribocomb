/** implements concept of two linked objects */
package tools3d.objects3d;

import tools3d.Vector3D;

import generaltools.ConstraintDouble;
import generaltools.SimpleConstraintDouble;

public class SimpleCrossProductLink extends SimpleLink implements CrossProductLink {

    private ConstraintDouble constraint;
    private Object3D obj3;
    private Object3D obj4;
    private Object3D obj5;
    private Object3D obj6;

    public SimpleCrossProductLink(Object3D o1, Object3D o2, Object3D o3, Object3D o4, Object3D o5, Object3D o6,  ConstraintDouble constraint) {
	super(o1, o2);
	assert(o1 != null);
	assert(o2 != null);
	assert(o3 != null);
	assert(o4 != null);
	assert(o5 != null);
	assert(o6 != null);
	this.obj3 = o3;
	this.obj4 = o4;
	this.obj5 = o5;
	this.obj6 = o6;
	this.constraint = constraint;
    }

    public SimpleCrossProductLink(Object3D o1, Object3D o2, Object3D o3, Object3D o4, Object3D o5, Object3D o6, double min, double max) {
	super(o1, o2);
	this.obj3 = o3;
	this.obj4 = o4;
	this.obj5 = o5;
	this.obj6 = o6;
	this.constraint = new SimpleConstraintDouble(min, max);	
    }

    /** returns constraint */
    public ConstraintDouble getConstraint() { return constraint; }

    public Object3D getObj3() { return obj3; }

    public Object3D getObj4() { return obj4; }

    public Object3D getObj5() { return obj5; }

    public Object3D getObj6() { return obj6; }

    public double computeError() {
	return computeError(getObj1().getPosition(),
			    getObj2().getPosition(),
			    getObj3().getPosition(),
			    getObj4().getPosition(),
			    getObj5().getPosition(),
			    getObj6().getPosition());
    }
    
    public double computeVolume(Vector3D v1, Vector3D v2, Vector3D v3, Vector3D v4, Vector3D v5, Vector3D v6) {
	Vector3D a = v2.minus(v1);
	a.normalize();
	Vector3D b = v4.minus(v3);
	b.normalize();
	Vector3D c = v6.minus(v5);
	c.normalize();
	double vol = (a.cross(b)).dot(c);
	return vol;
    }

    public double computeError(Vector3D v1, Vector3D v2, Vector3D v3, Vector3D v4, Vector3D v5, Vector3D v6) {
	double ang = Vector3D.torsionAngle(v1, v2, v3, v4);
	assert (ang >= 0.0);
	assert (ang <= Math.PI);
	double vol = computeVolume(v1, v2, v3, v4, v5, v6);
	double result = 0.0;
	if (vol < constraint.getMin()) {
	    result = constraint.getMin() - vol;
	} else if (vol > constraint.getMax()) {
	    result = vol - constraint.getMax();
	}
	assert (result >= 0.0);
	assert (result <= Math.PI);
	return result;
    }

}
	
