package tools3d.objects3d;

import java.io.DataInputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import generaltools.StringTools;
import tools3d.Point;
import tools3d.Vector2;
import tools3d.Vector3D;

/** implements concept of two linked objects */
public class SimpleLink implements Link {

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    // Object linkProperties;
    Object3D obj1;
    Object3D obj2;
    String name = "link";
    String typeName = "Link";
    Properties properties;

    /**
     * Only used with Links in .points2 files.
     * True if the Link will stay after Symmetries are generated.
     */
    boolean finalObject = false;

    public SimpleLink() { }
    
    public SimpleLink(Object3D o1, Object3D o2) {
	obj1 = o1;
	obj2 = o2;
// 	setObj1(o1); // obj1 = o1;
// 	setObj2(o2); // obj2 = o2;
    }
    
    public SimpleLink(String name, Point p1, Point p2) {
	assert(name != null);
	assert (p1 != null);
	assert (p2 != null);
	setName(name);
	obj1 = p1.toObject3D();
	obj2 = p2.toObject3D();
// 	setObj1(p1.toObject3D());
// 	setObj2(p2.toObject3D());
    }
    
    /**
     * Constructor should only be used when generating Links for .points2 files.
     *
     * @param name The String name of the Link.
     * @param p1 The first Point in the Link.
     * @param p2 The second Point in the Link.
     * @param finalObject True if the Link will stay after Symmetries are generated.
     */
    public SimpleLink(String name, Point p1, Point p2, boolean finalObject) {
	assert(name != null);
	assert (p1 != null);
	assert (p2 != null);
	setName(name);
	obj1 = p1.toObject3D();
	obj2 = p2.toObject3D();
// 	setObj1(p1.toObject3D());
// 	setObj2(p2.toObject3D());
	setFinalObject(finalObject);
    }

    public SimpleLink(Vector2 v) {
        assert(v != null);
	setTypeName("Vector");
	setObj1(v.getPoint().toObject3D());
	setObj2(v.getVectorPoint().toObject3D());
    }

    /** Computes angle between two links */
    public double angle(Link other) {
	Object3DSet common = findCommon(other);
	if (common.size() == 2) {
	    return 0.0;
	} 
	else if (common.size() == 1) {
	    Vector3D p0 = common.get(0).getPosition();
	    Vector3D p1 = getPartner(common.get(0)).getPosition();
	    Vector3D p2 = other.getPartner(common.get(0)).getPosition();
	    return p1.minus(p0).angle(p2.minus(p0));
	}
	Vector3D v1 = getObj2().getPosition().minus(getObj1().getPosition());
	Vector3D v2 = other.getObj2().getPosition().minus(other.getObj1().getPosition());
	double a = v1.angle(v2);
	if (a > (0.5 * Math.PI)) {
	    a -= (0.5 * Math.PI);
	}
	return a;
    }

    /** Returns list of objects that is common between links */
    public Object3DSet findCommon(Link other) {
	Object3DSet common = new SimpleObject3DSet();
	if ((getObj1() == other.getObj1()) || (getObj1() == other.getObj2())) {
	    common.add(getObj1());
	}
	if (getObj2() != getObj1()) {
	    if ((getObj2() == other.getObj1()) || (getObj2() == other.getObj2())) {
		common.add(getObj2());
	    }
	    
	}
	return common;
    }

    public void clear() {
	obj1 = null;
	obj2 = null;
	// 	linkProperties = null;
	name = "";
    }

    public Object clone() {
	SimpleLink l = new SimpleLink(getObj1(), getObj2());
	l.setName(new String(getName()));
	l.setTypeName(new String(getTypeName()));
	if (properties != null) {
	    l.properties = (Properties)(properties.clone());
	}
	// l.setLinkProperties((Properties)(getLinkProperties().clone()));

	return l;
    }

    public Object clone(Object3D newObj1, Object3D newObj2) {
	SimpleLink l = (SimpleLink)clone();
	l.setObj1(newObj1);
	l.setObj2(newObj2);
	return l;
    }

//     protected void copyDeepThisCore(SimpleLink l) {
// 	this.obj1 = l.obj1;
// 	this.obj2 = l.obj2;
// 	this.linkProperties = l.linkProperties;
// 	this.name = l.name;
// 	this.typeName = l.typeName;
//     }
    
    /** returns true if same objects are binding partners in both links */
    public boolean equals(Object other) {
	if (other instanceof Link) {
	    Link otherLink = (Link)other;
	    if (((otherLink.getObj1().equals(getObj1())) 
		&& (otherLink.getObj2().equals(getObj2())))
		|| ((otherLink.getObj1().equals(getObj2())) 
		    && (otherLink.getObj2().equals(getObj1()))) ) {
		return true;
	    }
	}
	return false;
    }
	
    /** returns general link property */
//     public Object getLinkProperties() {
// 	return linkProperties;
//     }
    
    public String getName() {
	return name;
    }

    public String getName1() {
	return getObjectName(obj1);
    }

    public String getName2() {
	return getObjectName(obj2);
    }

    /** returns first object */
    public Object3D getObj1() {
	return obj1;
    }
    
    /** returns second object */
    public Object3D getObj2() {
	return obj2;
    }
    
    String getObjectName(Object3D o) {
	if (o == null) {
	    return "__UNDEFINED";
	}
	if (o instanceof Object3D) {
	    return Object3DTools.getFullName((Object3D)(o));
	}
	return  "_";
    }
	
    /** if obj is object1 return object2 and vice versa. */
    public Object3D getPartner(Object3D obj) {
	if (obj == obj1) {
	    return obj2;
	}
	else if (obj == obj2) {
	    return obj1;
	}
	return null;
    }

    public String getProperty(String key) {
	if (properties == null) {
	    return null;
	}
	return properties.getProperty(key);
    }

    public Properties getProperties() {
	return properties;
    }
    
    public String getTypeName() {
	return typeName;
    }
    
    /** returns true, if two objects are linked with this link */
    public boolean isLinked(Object3D o1, Object3D o2) {
	if ((o1 == null) || (o2 == null)) {
	    return false;
	}
	return ((obj1 == o1) && (obj2 == o2)) 
	    || ((obj1 == o2) && (obj2 == o1));
	// 		return ((obj1.equals(o1) && obj2.equals(o2)) 
	// 					|| (obj1.equals(o2) && obj2.equals(o1)));
    }
    
    /** returns true if both objects are non-null */
    public boolean isValid() {
	return ((obj1 != null) && (obj2 != null));
    }
    
    /** returns 0 if not found, 1 if found once, 2 if found twice */
    public int linkOrder(Object3D o1) 
    {
	int result = 0;
	//	if (obj1.toString().equals(o1.toString())) {
	if (obj1.getName().equals(o1.getName())) {
	    ++result;
	}
	//if (obj2.toString().equals(o1.toString())) {
	if (obj2.getName().equals(o1.getName())) {
	    ++result;
	}
	return result;
    }

    public boolean links(String name) {
	if (getName1() == name) {
	    return true;
	}
	else if (getName2() == name) {
	    return true;
	}
	else {
	    return false;
	}
    }

    /** reads link text and converts object names into object references */
    public void read(InputStream is, Object3D tree) throws Object3DIOException {
	StringTools st = new StringTools();
	DataInputStream dis = new DataInputStream(is);
	String expected = "(Link";
	String word = st.readWord(dis);
	if (!word.equals(expected)) {
	    throw new Object3DIOException("SimpleLink.read: " + expected + " excepted instead of " + word);
	}
	String linkName = st.readWord(dis); // Name of link.
	String name1 = st.readWord(dis); // Name of first object.
	String name2 = st.readWord(dis); // Name of second object.
	Object3D o1 = Object3DTools.findByFullName(tree, name1);
	if (o1 == null) {
	    log.severe("Could not find " + name1);
	    throw new Object3DIOException("SimpleLink.read: no object found with name: " + name1);
	}
	else {
	    // log.fine("Found object 1: " + o1.toString());
	}
	Object3D o2 = Object3DTools.findByFullName(tree, name2);
	if (o2 == null) {
	    log.severe("Could not find " + name2);
	    throw new Object3DIOException("SimpleLink.read: no object found with name: " + name2);
	}
	else {
	    // log.fine("Found object 2: " + o2.toString());
	}

	word = st.readWord(dis);
	expected = ")";
	if (!word.equals(expected)) {
	    throw new Object3DIOException("SimpleLink.read: " + expected + " excepted instead of " + word);
	}

	setName(linkName);
	setObj1(o1);
	setObj2(o2);

    }

    public void setFinalObject(boolean finalObject) {
	this.finalObject = finalObject;
    }

    /** replaces oldobject reference with reference to new object */
    public void replaceObjectInLink(Object3D oldObject, Object3D newObject) {
	if (obj1 == oldObject) {
	    obj1 = newObject;
	}
	if (obj2 == oldObject) {
	    obj2 = newObject;
	}
    }

    /** sets general property object */
//     public void setLinkProperties(Object o) {
// 	linkProperties = o;
//     }
    
    /** sets name of individual link (like "StemP4" ) */
    public void setName(String s) {
	name = s;
    }
    
    public void setObj1(Object3D o) {
	obj1 = o;
    }
	
    public void setObj2(Object3D o) {
	obj2 = o;
    }

    public void setProperties(Properties properties) {
	this.properties = properties;
    }

    public void setProperty(String key, String value) {
	if (properties == null) {
	    properties = new Properties();
	}
	properties.setProperty(key, value);
    }

    /** sets type name of link like "Stem" or "Tertiary */
    public void setTypeName(String s) {
	name = s;
    }

    /** swaps objects one and two */
    public void swap() {
	Object3D h = obj1;
	obj1 = obj2;
	obj2 = h;
    }
    
    public String toString() {
	String result = "(Link " + getName() + " " + getObjectName(obj1) + " " + getObjectName(obj2);
	result = result + " )";
	return result;
    }

}
	
