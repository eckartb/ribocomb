package tools3d.objects3d;

import java.io.DataInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.testng.annotations.Test;

import generaltools.StringTools;
import tools3d.Vector3D;

public class SimpleLinkSet implements LinkSet {

    private Logger log = Logger.getLogger("NanoTiler_debug");

    private List<Link> links = new ArrayList<Link>();

    /*
      public List<Object3D> getNeighbors(Object3D o) {
      List<Object3D> neighbors = new ArrayList<Object3D>();
      for (int i = 0; i < links.size(); i++) {
      Link link = links.get(i);
      if (link.getPartner(o) != null) {
      neighbors.add(link.getPartner(o));
      }
      }
      return neighbors;
      }
    */

    public LinkSet cloneDeep() {
	log.warning("Cloning a LinkSet could lead to outdated references. Be careful!!");
	LinkSet result = new SimpleLinkSet();
	for (int i = 0; i < links.size(); i++) {
	    result.add((Link)links.get(i));
	}
	return result;
    }

    /**
     * Creates a LinkSet with new Link references.
     *
     * @param root The Object3D that contains new objects that should be referenced by the Links.
     */
    public LinkSet cloneDeep(Object3D root) {
	LinkSet result = new SimpleLinkSet();
	for (int i = 0; i < size(); i++) {
	    Link link = get(i);
	    Link newLink = new SimpleLink(root.getChild(link.getObj1()),
					  root.getChild(link.getObj2()));
	    result.add(newLink);
	}
	return result;
    }

    
    @Test(groups={"new"})
    public void testCloneDeep() {
	LinkSet test = new SimpleLinkSet();
	Object3D obj1 = new SimpleObject3D(new Vector3D(0,0,0));
	obj1.setName("p1");
	Object3D obj2 = new SimpleObject3D(new Vector3D(1,1,1));
	obj2.setName("p2");
	Object3D obj3 = new SimpleObject3D(new Vector3D(2,2,2));
	obj3.setName("p3");
	Link link1 = new SimpleLink(obj1, obj2);
	Link link2 = new SimpleLink(obj2, obj3);
	Object3D root = new SimpleObject3D();
	root.insertChild(obj1);
	root.insertChild(obj2);
	root.insertChild(obj3);
	test.add(link1);
	test.add(link2);

	LinkSet result = test.cloneDeep(root);

	assert result.size() == test.size();
	for (int i = 0; i < result.size(); i++) {
	    assert result.get(i).getObj1().equals(test.get(i).getObj1());
	    assert result.get(i).getObj2().equals(test.get(i).getObj2());
	}
    }

    /** adds new links */
    public void add(Link link) { links.add(link); }

    /** removes all links */
    public void clear() { links.clear(); }

    /** returns n'th link */
    public Link get(int n) throws IndexOutOfBoundsException { return (Link)(links.get(n)); }

    /** adds all new lines */
    public void merge(LinkSet otherLinks) { 
	for (int i = 0; i < otherLinks.size(); ++i) {
	    links.add(otherLinks.get(i)); 
	}
    }

    /** returns number of links */
    public int size() { return links.size(); }

    /** returns true if link found in set (independent of object order */
    public boolean contains(Link link) {
	for (int i = 0; i < links.size(); ++i) {
	    if (link.equals(get(i))) {
		return true;
	    }
	}
	return false;
    }

    /** returns link corresponding objects 1 and 2, null if those objects are not linked */
    public Link find(Object3D obj1, Object3D obj2) {
	Link testLink = new SimpleLink(obj1, obj2);
	for (int i = 0; i < links.size(); ++i) {
	    if (testLink.equals(get(i))) {
		return get(i);
	    }
	}
	return null;
    }

    /** returns link corresponding objects 1 and 2, null if those objects are not linked */
    public LinkSet findLinks(Object3D obj1, Object3D obj2) {
	LinkSet result = new SimpleLinkSet();
	for (int i = 0; i < size(); ++i) {
	    if (get(i).isLinked(obj1, obj2)) {
		result.add(get(i));
	    }
	}

	return result;
    }

    /** returns links connecting to object 1 */
    public LinkSet findLinks(Object3D obj) {
	LinkSet result = new SimpleLinkSet();
	for (int i = 0; i < size(); ++i) {
	    if (get(i).linkOrder(obj) > 0) {
		result.add(get(i));
	    }
	}
	return result;
    }

    /** returns rank of link that connects to a certain object (zero if first link connecting to that object, one if second and so forth) */
    public int getLinkRank(Object3D obj, Link link) {
	LinkSet links = findLinks(obj);
	for (int i = 0; i < links.size(); ++i) {
	    if (links.get(i) == link) {
		return i;
	    }
	}
	return -1; // not found
    }

    /** return minimum number of links connecting the two objects.
     *  zero indicates there is no connection 
     */
    public int getLinkNumber(Object3D obj1, Object3D obj2) {
	int result = 0;
	for (int i = 0; i < size(); ++i) {
	    if (get(i).isLinked(obj1, obj2)) {
		++result;
	    }
	}
	return result;
    }

    /** return minimum number of links connecting object
     *  zero indicates there is no connection 
     */
    public int getLinkOrder(Object3D obj) {
	int result = 0;
	for (int i = 0; i < size(); ++i) {
	    result += get(i).linkOrder(obj);
	}
	return result;
    }


    public void remove(Link link) { links.remove(link); }

    /** Removes links that occur within a link created by symmetries. Adds the new links created by the symmetries. */
    public void removeAndAdd( Object3D objectTree, Object3DSet vertexSet ) {
	//	log.severe("METHOD NOT YET IMPLEMENTED!");
    }

    /** remove links that cannot be found in object tree */
    public void removeBadLinks(Object3D tree) {
	Object3DCollector collector = Object3DCollector.collectAll(tree); // transform tree to set
	for (int i = size()-1; i >= 0; --i) {
	    Link link = get(i);
	    if (! ( (collector.contains(link.getObj1())) && (collector.contains(link.getObj2())) ) ) {
		links.remove(link);
	    }
	}
    }

    /** returns output string */
    public String toString() {
	String result = "(LinkSet " + size() + " ";
	for (int i = 0; i < size(); ++i) {
	    result = result + get(i).toString() + " ";
	}
	result = result + ")";
	return result;
    }

    /** reads links, given information about exisiting trees 
     * (used for converting object names into object references)
     */
    public void read(InputStream is, Object3D tree) throws Object3DIOException {
	StringTools st = new StringTools();
 	String expected = "(LinkSet";
	DataInputStream dis = new DataInputStream(is);
 	String word = st.readWord(dis);
 	if (!word.equals(expected)) {
 	    throw new Object3DIOException("LinkSet.read: " + expected + " excepted instead of " + word);
 	}
 	word = st.readWord(dis);
 	int numLinks = 0;
 	try {
 	    numLinks = Integer.parseInt(word);
 	}
 	catch (NumberFormatException e) {
 	    throw new Object3DIOException("createObject3DGraph: Could not parse number of children: " + word);	
 	}
 	// read individual links
 	for (int i = 0; i < numLinks; ++i) {
 	    Link link = new SimpleLink();
	    link.read(dis, tree);
	    add(link);
 	}
 	expected = ")"; // end of reading links
 	word = st.readWord(dis);
 	if (!word.equals(expected)) {
 	    throw new Object3DIOException("createObject3D: " + expected 
 					  + " excepted instead of " + word);
 	}
    }

    /** replaces oldobject reference with reference to new object */
    public void replaceObjectInLinks(Object3D oldObject, Object3D newObject) {
	for (int i = 0; i < this.size(); ++i) {
	    get(i).replaceObjectInLink(oldObject, newObject);
	}
    }

}
