package tools3d.objects3d;

import java.io.DataInputStream;
import java.io.InputStream;
import java.util.*;
import java.util.logging.Logger;

import generaltools.StringTools;
import tools3d.Point;
import tools3d.Vector2;
import tools3d.Vector3D;

/** implements concept of two linked objects */
public class SimpleMultiLink implements MultiLink {

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    // Object linkProperties;
    List<Object3D> objects = new ArrayList<Object3D>();
    String name = "multilink";
    String typeName = "MultiLink";
    Properties properties;

    /**
     * Only used with Links in .points2 files.
     * True if the Link will stay after Symmetries are generated.
     */
    boolean finalObject = false;

    public SimpleMultiLink() { }
    
    public SimpleMultiLink(Object3D o1, Object3D o2) {
	addObj(o1);
	addObj(o2);
    }

    public SimpleMultiLink(MultiLink other) {
	setName(other.getName());
	for (int i = 0; i < other.size(); ++i) {
	    addObj(other.getObj(i)); // no deep cloning of objects quite yet
	}
    }
    
    public void addObj(Object3D obj) {
	objects.add(obj);
    }

    /** Computes angle between two links */
    public double angle(Link other) {
	assert(false);
	return 0.0;
    }

    /** Returns list of objects that is common between links */
    public Object3DSet findCommon(Link other) {
	assert false;
	return null;

// 	Object3DSet common = new SimpleObject3DSet();
// 	if ((getObj1() == other.getObj1()) || (getObj1() == other.getObj2())) {
// 	    common.add(getObj1());
// 	}
// 	if (getObj2() != getObj1()) {
// 	    if ((getObj2() == other.getObj1()) || (getObj2() == other.getObj2())) {
// 		common.add(getObj2());
// 	    }
	    
// 	}
    }

    public void clear() {
	objects.clear();
	name = "";
    }

    public Object clone() {
	return new SimpleMultiLink(this);
    }

    public Object clone(Object3D newObj1, Object3D newObj2) {
	assert false; // not yet implemented
	return null;
    }

    /** returns true if same objects are binding partners in both links */
    public boolean equals(Object other) {
	if (other instanceof MultiLink) {
	    MultiLink otherLink = (MultiLink)other;
	    if (size() != otherLink.size()) {
		return false;
	    }
	    for (int i = 0; i < size(); ++i) {
		if (!(otherLink.getObj(i).equals(getObj(i)))) {
		    return false;
		} 
	    }
	}
	return false;
    }
	
    public String getName() {
	return name;
    }

    public String getName1() {
	return getObjectName(getObj(0));
    }

    public String getName2() {
	return getObjectName(getObj(1));
    }

    String getObjectName(Object3D o) {
	if (o == null) {
	    return "__UNDEFINED";
	}
	return o.getFullName();
    }

    /** returns first object */
    public Object3D getObj1() {
	return objects.get(0);
    }
    
    /** returns second object */
    public Object3D getObj2() {
	return objects.get(1);
    }

    /** returns second object */
    public Object3D getObj(int n) {
	return objects.get(n);
    }
    
    /** if obj is object1 return object2 and vice versa. */
    public Object3D getPartner(Object3D obj) {
	assert false;
	return null;
    }

    public String getProperty(String key) {
	if (properties == null) {
	    return null;
	}
	return properties.getProperty(key);
    }

    public Properties getProperties() {
	return properties;
    }
    
    public String getTypeName() {
	return typeName;
    }
    
    /** returns true, if two objects are linked with this link */
    public boolean isLinked(Object3D o1, Object3D o2) {
	if ((o1 == null) || (o2 == null)) {
	    return false;
	}
	return objects.contains(o1) && objects.contains(o2);
    }
    
    /** returns true if both objects are non-null */
    public boolean isValid() {
	return size() > 1;
    }
    
    /** returns 0 if not found, 1 if found once, 2 if found twice */
    public int linkOrder(Object3D o1) 
    {
	int result = 0;
	String fn = o1.getFullName();
	for (int i = 0; i < size(); ++i) {
	    if (getObj(i).getFullName().equals(fn)) {
		++result;
	    }
	}
	return result;
    }

    public boolean links(String name) {
	assert false;
	return false;
    }

    /** reads link text and converts object names into object references */
    public void read(InputStream is, Object3D tree) throws Object3DIOException {
	assert false;
    }

    public void setFinalObject(boolean finalObject) {
	assert false;
    }

    public int size() {
	return objects.size();
    }

    /** replaces oldobject reference with reference to new object */
    public void replaceObjectInLink(Object3D oldObject, Object3D newObject) {
	assert false;
    }

    /** sets general property object */
//     public void setLinkProperties(Object o) {
// 	linkProperties = o;
//     }
    
    /** sets name of individual link (like "StemP4" ) */
    public void setName(String s) {
	name = s;
    }
    
    public void setObj1(Object3D o) {
	if (objects.size() > 0) {
	    objects.set(0,o);
	}
	else {
	    objects.add(o);
	}
    }
	
    public void setObj2(Object3D o) {
	if (objects.size() > 1) {
	    objects.set(1,o);
	}
	else if (objects.size() > 0) {
	    objects.add(o);
	} else {
	    assert false;
	}
    }

    public void setProperties(Properties properties) {
	this.properties = properties;
    }

    public void setProperty(String key, String value) {
	if (properties == null) {
	    properties = new Properties();
	}
	properties.setProperty(key, value);
    }

    /** sets type name of link like "Stem" or "Tertiary */
    public void setTypeName(String s) {
	name = s;
    }

    /** swaps objects one and two */
    public void swap() {
	assert false;
    }
    
    public String toString() {
	String result = "(MultiLink " + getName();
	for (int i = 0; i < size(); ++i) {
	    result = result + " " + getObj(i).getFullName();
	}
	result = result + " )";
	return result;
    }

}
	
