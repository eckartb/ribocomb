package tools3d.objects3d;
import org.testng.annotations.Test; /** Testing package. */

import java.io.PrintWriter;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Properties;
import java.util.Vector;
import java.util.logging.Logger;

import rnadesign.rnamodel.RnaStrand;
import rnadesign.rnamodel.StrandJunction3D;
import tools3d.Appearance;
import tools3d.CoordinateSystem;
import tools3d.Geometry;
import tools3d.Matrix3D;
import tools3d.Matrix3DTools;
import tools3d.Point;
import tools3d.Positionable3D;
import tools3d.Shape3DActor;
import tools3d.Shape3DSet;
import tools3d.StandardGeometry;
import tools3d.Symmetry;
import tools3d.Vector3D;
import tools3d.BoundingVolume;
import tools3d.Vector4D;
import tools3d.AABBBoundingVolume;
import tools3d.Dimension3D;
import tools3d.symmetry2.*;

import static tools3d.PackageConstants.*;

/**
 * Concept of object in 3D space with location and orientation.
 *
 * @author Eckart Bindewald
 */
public class SimpleObject3D implements Object3D {

	private static final String className = "Object3D";
	public static final String CLASS_NAME = "Object3D";
	protected static Logger log = Logger.getLogger("NanoTiler_debug");
	private HashMap<String,Integer> nameIndices; // stores names and indices of child objects
	private Appearance appearance;

	private boolean finalPoint; /** Returns true if this object will remain after Symmetries are generated : only to be used with points2 files. */
	private boolean selected;

	private double boundingBox = -1.0;
	private double boundingRadius;
	private double rotationAngle;
	private double zBufValue;

	private int number;
	private int typeId;

	private LinkSet links = new SimpleLinkSet();

	private List<Object3D> children;

	private Object3D parent;

	private Properties properties;
	
	private BoundingVolume boundingVolume;

	private Shape3DActor actor = null;

	private Shape3DSet shapeSet; // TODO: not yet implemented

	private String name;

	private Vector<Symmetry> symmetriesVector = new Vector<Symmetry>();

	private Vector3D dimensions;
	protected Vector3D relativePosition;
	private Vector3D rotationAxis;
    protected Vector3D scaling;
    private double score = 0.0;

    private String filename = "default_name"; // descriptor name for the object (used in output filenames)


	public SimpleObject3D() {
		appearance = new Appearance(); // standard appearance (color, reflection & diffusion coefficient)
		selected = false;
		number = 0;
		typeId = 0;
		name = "unnamed";
		relativePosition = new Vector3D(0, 0, 0);
		scaling = new Vector3D(1, 1, 1);
		rotationAxis = new Vector3D(1, 0, 0);
		rotationAngle = 0.0;
		parent = null;
		children = new ArrayList<Object3D>();
		// links = new ArrayList();
		zBufValue = 0.0;
		updateNameIndices();
	}    

    /** Constructs an object at the origina with a specified name */
    public SimpleObject3D(String name) {
	this();
	setName(name);
    }

	/**
	 * TODO : copy not yet implemented
	 *
	 * @param obj Object3D to copy to new SimpleObject3D.
	 */
	public SimpleObject3D(Object3D obj) {
	    assert obj.isValid();
	    log.warning("Constructor not yet implemented");
	    copy(obj);
	}

	/**
	 * Constructs a new SimpleObject3D at the given position.
	 *
	 * @param position The position of the new SimpleObject3D.
	 */
	public SimpleObject3D(Vector3D position) {
		this();
		assert position.isValid();
		relativePosition = new Vector3D(position);
	}

	/**
	 * Accept a Visitor.
	 *
	 * @param visitor
	 */
	public void accept(Object3DVisitor visitor) {
		visitor.visit(this);
	}

	/** activate possible actor */
	public void act() {
		if (actor != null) {
			actor.act(this);
		}
	}

	/** Apply active transformation to object. */
	public void activeTransform(CoordinateSystem cs) {
		Vector3D newPos = cs.activeTransform(getPosition());
		setIsolatedPosition(newPos);
		for (int i = 0; i < size(); ++i) {
			getChild(i).activeTransform(cs);
		}
	}


    /** Compare objects by score. Use z-buffer instead! */

    /* public int compareTo(Object other) {
	assert other instanceof Object3D;
	Object3D otherObj = (Object3D)other;
	if (getScore() < otherObj.getScore()) {
	    return -1;
	}
	else if (getScore() > otherObj.getScore()) {
	    return 1;
	}
	return 0; // equal score
    }
    */

    public double getScore() { return score; }

	/** Apply active transformation to object. */
	public void passiveTransform(CoordinateSystem cs) {
		Vector3D newPos = cs.passiveTransform(getPosition());
		setIsolatedPosition(newPos);
		for (int i = 0; i < size(); ++i) {
			getChild(i).passiveTransform(cs);
		}
	}

	/** Apply active transformation to object; version 2 has different interpretation than version 1 */
	public void activeTransform2(CoordinateSystem cs) {
		Vector3D newPos = cs.activeTransform2(getPosition());
		setIsolatedPosition(newPos);
		for (int i = 0; i < size(); ++i) {
			getChild(i).activeTransform2(cs);
		}
	}

	/** Apply active transformation to object; version 2 has different interpretation than version 1 */
	public void activeTransform3(CoordinateSystem cs) {
		Vector3D newPos = cs.activeTransform3(getPosition());
		setIsolatedPosition(newPos);
		for (int i = 0; i < size(); ++i) {
			getChild(i).activeTransform3(cs);
		}
	}

	/** Apply active transformation to object; version 2 has different interpretation than version 1 */
	public void passiveTransform2(CoordinateSystem cs) {
		Vector3D newPos = cs.passiveTransform2(getPosition());
		setIsolatedPosition(newPos);
		for (int i = 0; i < size(); ++i) {
			getChild(i).passiveTransform2(cs);
		}
	}

	/** Apply active transformation to object; version 2 has different interpretation than version 1 */
	public void passiveTransform3(CoordinateSystem cs) {
		Vector3D newPos = cs.passiveTransform3(getPosition());
		setIsolatedPosition(newPos);
		for (int i = 0; i < size(); ++i) {
			getChild(i).passiveTransform3(cs);
		}
	}

	/**
	 * Adds a Link between two objects
	 * Those two objects must be part of the hierarchy
	 * (any descendant object)
	 *
	 * @param link The Link to add to this Object.
	 */
	public void addLink(Link link) {
		links.add(link);
	}

	/**
	 * Adds a link between two objects
	 * Those two objects must be part of the hierarchy
	 * (any descendant object)
	 */
	/*
    public void addLink(Object3D o1, Object3D o2) {
	links.add(new SimpleLink(o1, o2));
    }
	 */

	/**
	 * Adds a Symmetry to this
	 * Only to be used with points2 files!
	 *
	 * @param symmetry The Symmetry to add to this.
	 */
	public void addSymmetry(Symmetry symmetry) {
		symmetriesVector.add(symmetry);
	}

	/**
	 * Checks if all children are unique.
	 */
	public boolean checkChildrenUnique() {
		if ((size() > 0) && (nameIndices == null)) {
			return false;
		}
//		for (int i = 0; i < size(); ++i) {
//		Object3D child1 = getChild(i);
//		for (int j = i+1; j < size(); ++j) {
//		if (child1.isProbablyEqual(getChild(j))) {
//		return false;
//		}
//		}
//		}
		return true;
	}

	/**
	 * Removes all content from object.
	 */
	public void clear() {
		number = 0;
		typeId = 0;
		boundingRadius = 1;
		name = "";
		relativePosition.mul(0.0);
		scaling = new Vector3D(1, 1, 1);
		rotationAxis = new Vector3D(1, 1, 1);
		rotationAngle = 0;
		parent = null;
		for (int i = 0; i < children.size(); ++i) {
			((Object3D)(children.get(i))).clear();	
		}
		children.clear();
		links.clear();
		properties = null;
		updateNameIndices();
		invalidateBoundingCache();

	}

	/**
	 * "deep" clone including children: every object is newly generated!
	 */
	public Object cloneDeep() {
		Object3D newObj = (Object3D)(cloneDeepThis());
		for (int i = 0; i < size(); ++i) {
			newObj.insertChild((Object3D)(getChild(i).cloneDeep()));
		}
		return newObj;
	}

	/**
	 * "deep" clone not including children
	 * Please overwrite this method for subclasses!
	 */
	public Object cloneDeepThis() {
		// make sure this method is overwritten by derived classes!
		if(!getClassName().equals("Object3D")) {
			log.warning("Warning: Object3D class expected: " + getClassName());
		}
		assert (getClassName().equals("Object3D")); 
		SimpleObject3D newObj = new SimpleObject3D();
		newObj.copyDeepThisCore(this);
		return newObj;
	}

	/**
	 * Clones current class without copying any data
	 * TODO: make sure all subclasses overwrite this method!
	 */
	public Object cloneEmpty() {
		return new SimpleObject3D();
	}

	/**
	 * Compares an Object to this.
	 *
	 * @param object The Object to compare.
	 */
    public int compareTo(Positionable3D object) {
		double objectVal = ((Positionable3D)object).getZBufValue();
		if (zBufValue < objectVal) {
			return -1;
		}
		else if (zBufValue > objectVal) {
			return 1;
		}
		return 0;
	}

	public void copy(Object3D orig) {
		// not yet implemented
		log.warning("Warning: copy not yet implemented!");
	}

	/**
	 * "deep" clone copy not including parent and children used for derived classes
	 * TODO : check what to do about parent copying!
	 *
	 * @param obj Object to copy.
	 */
	protected void copyDeepThisCore(SimpleObject3D obj) {	
		this.appearance = obj.appearance; // TODO shallow copy :-(
		this.selected = obj.selected;
		this.number = obj.number;
		this.typeId = obj.typeId;
		this.boundingRadius = obj.boundingRadius;
		this.actor = obj.actor; // TODO
		this.score = obj.score;
		this.shapeSet = obj.shapeSet; // TODO
		this.name = new String(obj.name);
		if (obj.dimensions != null) {
			this.dimensions = (Vector3D)(obj.dimensions.clone());
		}
		this.setPosition(obj.getPosition()); // TODO : not fast
		if (obj.scaling != null) {
			this.scaling = (Vector3D)(obj.scaling.clone());
		}
		if (obj.rotationAxis != null) {
			this.rotationAxis = (Vector3D)(obj.rotationAxis.clone());
		}
		this.rotationAngle = obj.rotationAngle;
		// this.parent = obj.parent; // TODO not clean !? Shallow copy!?
		// ignore copying children
		if (obj.properties != null) {
			this.properties = (Properties)(obj.properties.clone()); // TODO not clean
		}
		else {
			this.properties = null;
		}
		this.zBufValue = obj.zBufValue;
		this.invalidateBoundingCache();
		// nameIndices does not need to be copied!?
	}

	/** returns true, if object is contained in set (using == operator, not equals) */
	public boolean contains(Object3D obj) {
		return children.contains(obj); 
	}

	/**
	 * creates geometry representing object
	 * Better use factor object to create shapes!
	 *
	 * @param angleDelta TODO: Not used.
	 */
	public Geometry createGeometry(double angleDelta) {
		return StandardGeometry.createSinglePointGeometry(getPosition());
	}

	/**
	 * Return distance between this and other object
	 *
	 * @param other The other object
	 */
	public double distance(Positionable3D other) {
	    Vector3D p1 = getPosition();
	    Vector3D p2 = other.getPosition();
	    assert p1.isValid();
	    assert p2.isValid();
	    return (p1.minus(p2)).length();
	}

	/**
	 * Returns the distance between two objects.
	 *
	 * @param vector3d First object: Vector3D.
	 * @param point2 Second object: Point.
	 */
	public double distance(Vector3D vector3d,
			Point point2) {
		Point point1 = new Point(vector3d.getX(),
				vector3d.getY(),
				vector3d.getZ());
		double distance = distance(point1, point2);
		return distance;
	}

	/**
	 * Returns distance between two Points using the distance formula
	 *
	 * @param point1 The first Point.
	 * @param point2 The second Point.
	 */
	public double distance(Point point1,
			Point point2) {
		double x1 = point1.getX();
		double x2 = point2.getX();
		double y1 = point1.getY();
		double y2 = point2.getY();
		double z1 = point1.getZ();
		double z2 = point2.getZ();
		double distance = Math.sqrt(Math.pow((x1-x2), 2) + 
				Math.pow((y1-y2), 2) + 
				Math.pow((z1-z2), 2));
		return distance;
	}

	public Shape3DActor getActor() {
		return actor;
	}

	public Appearance getAppearance() {
		return appearance;
	}

	/**
	 * Return radius of bounding sphere centered at getPosition()
	 */
	public double getBoundingRadius() { 
		return boundingRadius; 
	}

	public double getBoundingBox() {
		if(!isBoundingBoxCacheValid())
			cacheBoundingBox();

		return boundingBox;
	}
	
	
	
	public BoundingVolume getBoundingVolume() {
	    if(boundingVolume == null) {
		generateBoundingVolume();
	    }
	    return boundingVolume;
	}
	
	public Dimension3D getDimension() {
		Vector3D position = getPosition();
		return new Dimension3D(position.getX()-.5, position.getY()-.5, position.getZ()-.5, 1.0, 1.0, 1.0);
	}
	
	private void generateBoundingVolume() {
		if(size() == 0) {
			Dimension3D dim = getDimension();
			boundingVolume = new AABBBoundingVolume(new Vector4D(dim.getX() + dim.getWidth(),
					dim.getY() + dim.getHeight(), dim.getZ() + dim.getDepth(), 1.0),
					new Vector4D(dim.getX(), dim.getY(), dim.getZ(), 1.0));
			return;
		}
		
		double minX = Double.MAX_VALUE, maxX = Double.MAX_VALUE * -1, minY = Double.MAX_VALUE, maxY = Double.MAX_VALUE * -1, minZ = Double.MAX_VALUE, maxZ = Double.MAX_VALUE * -1;
		// double minX = LARGE_DOUBLE, maxX = LARGE_DOUBLE * -1, minY = LARGE_DOUBLE, maxY = LARGE_DOUBLE * -1, 
		// minZ = LARGE_DOUBLE, maxZ = LARGE_DOUBLE * -1;
		Vector4D position = new Vector4D(getPosition());
		for(int i = 0; i < size(); ++i) {
			Vector4D[] vertices = getChild(i).getBoundingVolume().getVertices();
			for(int j = 0; j < vertices.length; ++j) {
			    Vector4D difference = new Vector4D(getChild(i).getPosition()); // TODO : optimize to use getRelativePosition
				difference.add(vertices[j]);
				difference = difference.minus(position);
				minX = Math.min(minX, difference.getX());
				maxX = Math.max(maxX, difference.getX());
				minY = Math.min(minY, difference.getY());
				maxY = Math.max(maxY, difference.getY());
				minZ = Math.min(minZ, difference.getZ());
				maxZ = Math.max(maxZ, difference.getZ());
			}
		}
		
		boundingVolume = new AABBBoundingVolume(new Vector4D(minX, minY, minZ, 1.0), new Vector4D(maxX, maxY, maxZ, 1.0));
	}

	private void cacheBoundingBox() {
		if(size() == 0) {
			boundingBox = 1.0;
			if(getParent() != null)
				((SimpleObject3D)getParent()).cacheBoundingBox();
			return;
		}
		Object3D o = null;
		double distance = -1.0;
		for(int i = 0; i < size(); ++i) {
			Object3D child = getChild(i);
			double d = getPosition().distance(child.getPosition());
			if(d > distance) {
				o = child;
				distance = d;
			}
		}
		
		// System.out.println(o);

		boundingBox = distance + o.getBoundingBox();
		if(getParent() != null)
			((SimpleObject3D)getParent()).cacheBoundingBox();
	}

	private void invalidateBoundingCache() {
	    // System.out.println("Starting SimpleObject3D.invalidateBoundingCache!");
		boundingBox = -1.0;
		boundingVolume = null;
		if(getParent() != null) {
			((SimpleObject3D)getParent()).invalidateBoundingCache();
		}
	}

	private boolean isBoundingBoxCacheValid() {
		return boundingBox >= 0.0;
	}

	/** returns n'th child node */
	public Object3D getChild(int n) {
	    return (Object3D)children.get(n);
	}

	/** SLOW! Better to use another method if possible. CEV */
	public Object3D getChild(Object3D o) {
		for (int i = 0; i < children.size(); i++) {
			Object3D child = getChild(i);
			// if (child instanceof Vector3D &&
			// o instanceof Vector3D) {
			if (child.getPosition().getX() == o.getPosition().getX() &&
					child.getPosition().getY() == o.getPosition().getY() &&
					child.getPosition().getZ() == o.getPosition().getZ()) {
				return child;
			}
			// }
			// else {
			// log.warning("Tried to use getChild method with non-Vector3D");
			// }
		}
		log.warning("Child: " + o + " was not found in Object3D: " + this);
		return null; // change! EB 2007. Weird: return new SimpleObject3D();
	}

	/** returns child node with specified name, null otherwise */
	public Object3D getChild(String name) {
	    if ((name == null) || (name.length() == 0)) {
		log.warning("Insufficent child node name!");
		return null;
	    }
	    int idx = -1;
	    if (name.charAt(0) == '(') {
		String[] subwords = name.split("\\)");
		if (subwords.length != 2) {
		    log.warning("Expected one closing bracked in child object name: " + name);
		    return null;
		}
		subwords[0] = subwords[0].substring(1); // skip first character: (helix)2 is parsed to words helix and 2
		try {
		    int id = Integer.parseInt(subwords[1]) - 1;
		    String className = subwords[0];
		    // for convinience: translate some words from simplified versions
		    if (className.equals("hxend")) {
			className = "BranchDescriptor3D";
		    }
		    else if (className.equals("helixend")) {
			className = "BranchDescriptor3D";
		    }
		    else if (className.equals("atom")) {
			className = "Atom3D";
		    }
		    else if (className.equals("strand")) {
			className= "RnaStrand";
		    }
		    else if (className.equals("residue")) {
			className = "Nucleotide3D";
		    }
		    else if (className.equals("stem")) {
			className = "RnaStem3D";
		    }
		    // log.info("Getting index of child " + id + " with class name: " + className);
		    idx = getIndexOfChild(id, className);
		}
		catch (NumberFormatException nfe ) {
		    log.warning("Number format exception: " + nfe.getMessage());
		}
	    }
	    else {    
		idx = getIndexOfChild(name);
	    }
	    if (idx < 0) {
		String childNames = "";
		for (int i = 0; i < size(); ++i) {
		    childNames += getChild(i).getName() + " ";
		}
		// log.warning("Could not find child node with name: " + name + " in object " + getFullName() + " : " + childNames);
		assert (nameIndices == null) || (nameIndices.get(name) == null); // must not contain child node with this name
		return null;
	    }
	    return (Object3D)children.get(idx);
	}

	/** returns how many children with same class and smaller index can be found */
	public int getChildClassCounter(Object3D child) {
		String childClass = child.getClassName();
		int counter = 0;
		boolean found = false;
		for (int i = 0; i < size(); ++i) {
			if (getChild(i) == child) {
				found = true;
				break; // quit if current child is reached
			}
			if (getChild(i).getClassName().equals(childClass)) {
				++counter;
			}
		}
		assert found; // otherwise object was not child
		return counter;
	}

	/** returns number of children nodes with specified class name. TODO: could be implemented much faster */
	public int getChildCount(String childClassName) {
		int counter = 0;
		for (int i = 0; i < size(); ++i) {
			if (getChild(i).getClassName().equals(childClassName)) {
				++counter;
			}
		} 
		return counter;
	}

	public String getClassName() {
		return className;
	}

	/** How deep is the component nested in composites? */
	public int getDepth() {
		if (parent != null) {
			return parent.getDepth() + 1;
		}
		return 0; // no parent object
	}

	/** returns length in x,y,z */
	public Vector3D getDimensions() { return dimensions; }

	/** If object occupies space: give an "end point" of object. */
	/*  virtual Vector3D getEndPosition() const; */

	/** Returns true if this object will remain after Symmetries are generated : only to be used with points2 files. */
	public boolean getFinalPoint() { return finalPoint; }

	/** returns n'th link */
	/*
      public Link getLink(int n) throws IndexOutOfBoundsException {
      return (Link)(links.get(n));
      }
	 */

	/** returns n'th child node with specified class name. TODO : could be implemented much faster! */
	public int getIndexOfChild(int n,
				   String childClassName) {
		int counter = 0;
		for (int i = 0; i < size(); ++i) {
			if (getChild(i).getClassName().equals(childClassName)) {
				if (counter == n) {
					return i; 
				}
				++counter;
			}
		} 
		return -1; // not found
	}

	/** returns index of child. Returns size if not found. TODO : speed up! */
	public int getIndexOfChild(Object3D child) {
	    for (int i = 0; i < size(); ++i) {
		if (child.equals(children.get(i))) {
		    return i;
		}
	    }
	    assert false; // deprecated
	    return -1; // size();
	}

	/** returns child node index with certain name. Uses fast hash map.*/
	public int getIndexOfChild(String name) {
	    if (nameIndices == null) {
		// return -1;
		updateNameIndices();
	    }
	    if (nameIndices == null) {
		return -1;
	    }
	    Integer integer = nameIndices.get(name);
	    if (integer == null) {
		return -1;
	    }
//		for (int i = 0; i < size(); ++i) {
//		Object3D child = getChild(i);
//		if (child.getName().equals(name)) {
//		return i;
//		}
//		}
	    return integer.intValue(); // not found
	}

	/** returns number of links connecting this object to other object */
	public int getLinkNumber(Object3D obj) {
		return links.getLinkNumber(this, obj);
	}

	/** returns number of links connecting this object to other object */
	/*
      public int getLinkNumber(Object3D obj) {
      return obj.getNumLinks();
      }
	 */

	/** returns set of all links */
	public LinkSet getLinks() {
		return links;
	}

	/** returns number of links */
	/*
      public int getNumLinks() { 
      return links.size(); 
      }
	 */

	/** What is object's name? */
	public String getName() {
		return name;
	}

    public String getFilename() {
	return filename;
    }

	/** What is object's full path name? Example: root.pdb.A.G5.N */
	public String getFullName() {
	    return Object3DTools.getFullName(this);
	}

	/** Get number of object. */
	public int getNumber() {
		return number;
	}

	/*
      public int getNumLinks() {
      return numLinks;
      }
	 */

	/** Returns number of defined symmetries. */
	public int getNumSymmetries() {
		return symmetriesVector.size();
	}

	/** Get reference on parent. If parent == 0 return *this. */
	public Object3D getParent() { 
		return parent; 
	}

	/** Where is object situated? */
	public Vector3D getPosition() {
		if (parent == null) {
			return new Vector3D(getRelativePosition());
		}
		return getRelativePosition().plus(parent.getPosition());
	}

	/** Where is object situated? Specify id of symmetry operation */
	public Vector3D getPosition(int symId) {
	    assert symId >= 0;
	    Vector3D pos = getPosition();
	    if (symId == 0) {
		return pos;
	    }
	    SymCopies symCopies = SymCopySingleton.getInstance();
	    assert symId < symCopies.size();
	    return symCopies.getPosition(pos, symId); // position transformed by symmetry transformation specified with symId
	}

	/** returns properties
	 */	
	public Properties getProperties() {
		return properties;
	}

	/** returns property by key */
	public String getProperty(String key) {
		if (properties == null) {
			return null;
		}
		return properties.getProperty(key);
	}

	/** Where is object situated relative to parent? */
	public Vector3D getRelativePosition() {
		return new Vector3D(relativePosition);
	}

	/** How is object rotated? */
	public double getRotationAngle() {
		return rotationAngle;
	}

	/** What is object's rotation axis? */
	public Vector3D getRotationAxis() {
		return rotationAxis;
	}

	/** How is object scaled in x, y, z direction? */
	public Vector3D getScalingFactor() {
		return scaling;
	}

	public Shape3DSet getShapeSet() {
		return shapeSet;
	}

	/** returns name of shape */
	public String getShapeName() {
		return "Object3D";
	}

	public Object3D getSibling(int n) {
		if ((parent==null) || (n >= getSiblingCount())){
			return null;
		}
		return parent.getChild(n);
	}

	public int getSiblingCount() {
		if (parent == null) {
			return 0;
		}
		return parent.size();
	}

	public int getSiblingId() {
		if (parent == null) {
			return 0;
		}
		for (int i = 0; i < parent.size(); ++i) {
			if (parent.getChild(i) == (this)) {
				return i;
			}
		}
       		assert false;
		return parent.size();
	}

	/** If object occupies space: give an "first point" of object. */
	/*  virtual Vector3D getStartPosition() const; */

	public Symmetry getSymmetry(int i) {
		return (Symmetry)symmetriesVector.get(i);
	}

	/** returns z-buffer priority */
	public double getZBufValue() {
		return zBufValue;
	}

	/** Returns true, if any symmetries are defined. */
	public boolean hasSymmetries() { return symmetriesVector.size() > 0; }

	/** returns true if two 3D objects are linked */
	/*
      public boolean isLinked(Object3D o1, Object3D o2) {
      // not complete: traverse object hierarchy too see if objects part of tree!
      for (int i = 0; i < links.size(); ++i) {
      if (((Link)links.get(i)).isLinked(o1, o2)) {
      return true;
      }

      }
      return false;
      }
	 */

	/** returns total number of objects in tree */
	public int getTotalNumberOfObjects() {
		int sum = 1;
		if (size() == 0) {
			return sum; // zero children but one root object
		}
		for (int i = 0; i < size(); ++i) {
			sum += getChild(i).getTotalNumberOfObjects();  
		}
		return sum;
	}

	/** what is the type code of that object? */
	public int getTypeId() {
		return typeId;
	}

	public String infoString() {
		String result = new String();
		result = "(" + getClassName() + " "  + infoStringBody() + "(children " + size() + " ";
		for (int i = 0; i < size(); ++i) {
		    result += getChild(i).getName() + " ";
		}
		result += " ) )";
		return result;
	}

	public String infoStringBody() {
		String result = new String();
		result = getName() + " ";
		result = result + getPosition(); // getRelativePosition().toString(); // write relative position vector
		// 	result = result + " (children " + size();
		// 	for (int i = 0; i < size(); ++i) {
		// 	    result = result + " " + getChild(i).toString();
		// 	}
		// 	if (size() > 0) {
		// 	    result = result + "..."; // shortcut instead writing children
		// 	}
		// 	result = result + " ) ";
		return result;
	}

	/*
      public void insertChild(PointSet2 child) {
      if (child == null) { return; }
      Vector3D childPos = new Vector3D(child.getPosition());
      child.setRelativePosition(childPos.minus(getPosition()));
      child.setParent(this);
      //	setLinkSet( child.getLinks() );
      //child.addLinks(child);
      children.add(child);
      }
	 */

    /** Generates a child name that does not exist yet */
    public String findSaveName(String name) {
	if (getChild(name) == null) {
	    return name;
	}
	for (int i = 1; i > 0; ++i) {
	    String newName = name + i;
	    if (getChild(newName) == null) {
		return newName;
	    }
	}
	assert false; // should never be here
	return null;
    }

	/** Make child children of node.
	 * Relative position of child changes according to position of parent.
	 * Renames child if name already exists */
	public String insertChildSave(Object3D child) {
	    return insertChildSafe(child); // just for backwards compatibility
	}

	/** Make child children of node.
	 * Relative position of child changes according to position of parent.
	 * Renames child if name already exists */
	public String insertChildSafe(Object3D child) {
	    if (getChild(child.getName()) != null) { // already exists
		child.setName(findSaveName(child.getName())); // find name that does not exist yet
		assert child.getName() != null;
		assert (getChild(child.getName()) == null); // should be new name
		insertChild(child);
	    }
	    else {
		insertChild(child);
	    }
	    return child.getName();
	}

	/** Make child children of node.
	 * Relative position of child changes according to position of parent !*/
	public void insertChild(Object3D child) {
	    if (getChild(child.getName()) != null) {
		log.severe("Child with name " + child.getName() + " already exists for object " + getFullName());
	    }
	    assert (getChild(child.getName()) == null); // should not exist yet
	    insertChild(child, size()); // add at last position
	    //		if (child == null) {
	    //		return; // ignore 
	    //		}
	    //		Vector3D childPos = new Vector3D(child.getPosition());
	    //		// childPos = getRelativePosition().plus(parent.getPosition());
	    //		// hence: getRelativePosition = childPos - parent.getPosition();
	    //		child.setRelativePosition(childPos.minus(getPosition()));
	    //		child.setParent(this);
	    //		children.add(child);
	}

	/** Make child children of node.
	 * Relative position of child changes according to position of parent !*/
	public void insertChild(Object3D child, int position) {
		assert position >= 0;
		assert position <= size();
		if (child == null) {
			return; // ignore 
		}
		Vector3D childPos = new Vector3D(child.getPosition());
		// childPos = getRelativePosition().plus(parent.getPosition());
		// hence: getRelativePosition = childPos - parent.getPosition();
		child.setRelativePosition(childPos.minus(getPosition()));
		child.setParent(this);
		children.add(position, child);
		updateNameIndices();
		invalidateBoundingCache();
	}

	/** fast and approximate check if two objects describe the same content. Can be true even after cloning.
	 * TODO : improve */
	public boolean isProbablyEqual(Object3D obj) {
		if (obj == this) {
			return true; // identical!
		}
		if (!(obj.getClassName().equals(getClassName()) && obj.getName().equals(getName())) ) {
			return false;
		}
		double dist = distance(obj);
		if (dist > 0.001) {
			return false; // allow some numerical inaccuracy
		}
		if (size() != obj.size()) {
			// must have same number of children
			return false;
		}
		for (int i = 0; i < size(); ++i) {
			if (!(getChild(i).isProbablyEqual(obj.getChild(i)))) {
				return false;
			}
		}
		return true;
	}
    
    /** selected variable is used to implement interactive editor */
    public boolean isSelected() {
	return selected;
    }

    /** Consistency check of node */
    public boolean isValid() { 
	boolean check1 = getPosition().isValid();
	if (!check1) {
	    return false;
	}
	// make sure all child nodes have this node as parent node
	for (int i = 0; i < size(); ++i) {
	    if (getChild(i).getParent() != this) {
		return false;
	    }
	}
	return true;
    }

	/** Removes child from node without deleting it. */
	public void removeChild(Object3D child) {
		int origSize = size();
		// not yet implemented
		children.remove(child);
		updateNameIndices();
		assert (size() + 1) == origSize;
		invalidateBoundingCache();
	}

	/** Removes child n from node without deleting it. */
	public void removeChild(int n) {
		assert (n >= 0)  && (n < size());
		int origSize = size();
		children.remove(n);
		updateNameIndices();
		assert (size() + 1) == origSize;
		invalidateBoundingCache();
	}

	/** removes a link from container */
	public void removeLink(Link link) {
		links.remove(link);
	}

	/** replaces n'th child object */
	public void replaceChild(int n, Object3D child) {
		assert n < size();
		assert n >= 0;
		assert child != null;
		Vector3D childPos = new Vector3D(child.getPosition());
		Object3D oldChild = getChild(n);
		assert oldChild != null;
		oldChild.setParent(null); // no parent anymore ...
		child.setRelativePosition(childPos.minus(getPosition()));
		child.setParent(this);
		children.set(n, child);
		updateNameIndices();
		invalidateBoundingCache();
	}

	/** Rotate object around current position. */
	public void rotate(Vector3D axis,
			double angle) {
		rotate(getPosition(), axis, angle);
	}

	/**
	 * Rotates this object and all child nodes around center
	 * using "axis" as axis of rotation,
	 * rotation-axis, and angle.
	 * <P>
	 * Example:<BR>
	 * a_____b<BR>
	 * |&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<BR>
	 * |&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;o&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<BR>
	 * |______|<BR>
	 * c     d<BR>
	 * <P>
	 * To rotate the object 'a' onto point 'b' around 'o',<P>
	 * Vector3D vect1 = a.getPosition().minus(o.getPosition());<BR>
	 * Vector3D vect2 = b.getPosition().minus(o.getPosition());<BR>
	 * Vector3D vect3 = vect1.cross(vect2); // The cross-product of the vectors. Could be vect2.cross(vect1) if rotating the other direction<BR>
	 * a.rotate(o.getPosition(), vect3, vect1.angle(vect2));
	 *
	 * @param center Vector around which to rotate object.
	 * @param axis A Vector defining the plane on which to rotate object.
	 * @param angle The angle to rotate the object.
	 */
	public void rotate(Vector3D center, 
			Vector3D axis,
			double angle) {
		if (angle == 0.0) {
			return;  // do nothing if not needed
		}
		// translate(center.mul(-1));
		Vector3D shiftedPos = getPosition().minus(center);
		// setIsolatedPosition(shiftedPos);
		// Vector3D pos = getPosition();
		Vector3D rotatedPos = Matrix3DTools.rotate(shiftedPos, axis, angle);
		setIsolatedPosition(rotatedPos.plus(center));
		setRotationAxis(axis); // store, it defines internal orientation of body
		setRotationAngle(angle); // store, it defines internal orientation of body
		// rotate all children around new position:
		for (int i = 0; i < size(); ++i) {
			Object3D child = getChild(i);
			// child.rotate(newPos, axis, angle);
			child.rotate(center, axis, angle);
		}
		
		invalidateBoundingCache();
	}

	/**
	 * Twists this object and all child nodes around center
	 * using "axis" as axis of rotation,
	 * rotation-axis, and angle.
	 * Twist is a rotation whose extend is proportional to the value of the project of the object 
	 * coordinate anto the rotation axis.
	 * <P>
	 * Example:<BR>
	 * a_____b<BR>
	 * |&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<BR>
	 * |&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;o&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<BR>
	 * |______|<BR>
	 * c     d<BR>
	 * <P>
	 * To rotate the object 'a' onto point 'b' around 'o',<P>
	 * Vector3D vect1 = a.getPosition().minus(o.getPosition());<BR>
	 * Vector3D vect2 = b.getPosition().minus(o.getPosition());<BR>
	 * Vector3D vect3 = vect1.cross(vect2); // The cross-product of the vectors. Could be vect2.cross(vect1) if rotating the other direction<BR>
	 * a.rotate(o.getPosition(), vect3, vect1.angle(vect2));
	 *
	 * @param center Vector around which to rotate object.
	 * @param axis A Vector defining the plane on which to rotate object.
	 * @param angle The angle to rotate the object.
	 */
	public void twist(Vector3D center, 
			  Vector3D axis,
			  double anglePerProjection) {
		if (anglePerProjection == 0.0) {
			return;  // do nothing if not needed
		}
		// translate(center.mul(-1));
		Vector3D shiftedPos = getPosition().minus(center);
		double projection = shiftedPos.dot(axis) / axis.length();
		double angle = projection * anglePerProjection;
		// setIsolatedPosition(shiftedPos);
		// Vector3D pos = getPosition();
		Vector3D rotatedPos = Matrix3DTools.rotate(shiftedPos, axis, angle);
		setIsolatedPosition(rotatedPos.plus(center));
		setRotationAxis(axis); // store, it defines internal orientation of body
		setRotationAngle(angle); // store, it defines internal orientation of body
		// rotate all children around new position:
		for (int i = 0; i < size(); ++i) {
			Object3D child = getChild(i);
			// child.rotate(newPos, axis, angle);
			child.twist(center, axis, anglePerProjection);
		}
		
		invalidateBoundingCache();
	}

	/** rotates this object and all child nodes around center
	 * rotation-matrix : expects determinant to be 1.0
	 */
	public void rotate(Vector3D center, 
			Matrix3D rotationMatrix) {
		// first shift all such that rotation is around zero:
		// translate(center.mul(-1));
		Vector3D shiftedPos = getPosition().minus(center);
		// setIsolatedPosition(shiftedPos);
		// Vector3D pos = getPosition();
		// Vector3D rotatedPos = rotationMatrix.multiply(shiftedPos); // Matrix3DTools.rotate(shiftedPos, axis, angle);
		Vector3D rotatedPos = rotationMatrix.multiply(shiftedPos); // Matrix3DTools.rotate(shiftedPos, axis, angle);
		setIsolatedPosition(rotatedPos.plus(center));
//		setRotationAxis(axis); // store, it defines internal orientation of body
//		setRotationAngle(angle); // store, it defines internal orientation of body
		// rotate all children around new position:
		for (int i = 0; i < size(); ++i) {
			Object3D child = getChild(i);
			// child.rotate(newPos, axis, angle);
			child.rotate(center, rotationMatrix);
		}
		// undo shifting:
		translate(center);
	}

	/** TODO: Does not test all rotate methods */
	@Test public void testRotate() {
		// Object3D obj = new SimpleObject3D(new Vector3D(1, 1, 0));
		// Vector3D axis = new Vector3D(1, 0, 0);
		// double angle = Math.PI;
		// Object3D rotObj = new SimpleObject3D(new Vector3D(1, -1, 0 ));
		// obj.rotate(axis, angle);
		// assert(obj.getPosition().equals(rotObj.getPosition()));
		Object3D a = new SimpleObject3D(new Vector3D(1,1,0));
		Object3D b = new SimpleObject3D(new Vector3D(-1,1,0));
		Object3D o = new SimpleObject3D(new Vector3D(0,0,0));
		Vector3D vect1 = a.getPosition().minus(o.getPosition());
		Vector3D vect2 = b.getPosition().minus(o.getPosition());
		Vector3D vect3 = vect1.cross(vect2); // The cross-product of the vectors.
		a.rotate(o.getPosition(), vect3, vect1.angle(vect2));
		assert a.getPosition().equals(b.getPosition());
	}

	public void rotateStartEnd( Point start, Point end, double angle ) {
		rotate(start.getVector3D(), end.getVector3D().minus(start.getVector3D()), angle);
	}

	/** Scale object by stretching it and its relative position in three dimensions. Applied also to child nodes. */
	public void scale(Vector3D factor) {
		scaling.scale(factor);
		relativePosition.scale(factor);
		for (int i = 0; i < size(); ++i) {
			getChild(i).scale(factor);
		}
	}

	public void setActor(Shape3DActor actor) { this.actor = actor; }

	/** set appearance of object */
	public void setAppearance(Appearance appearance) { this.appearance = appearance; }

	/** set radius of bounding sphere, centered at getPosition */
	public void setBoundingRadius(double radius) {
		boundingRadius = radius;
	}

	/** returns length in x,y,z */
	public void setDimensions(Vector3D d) {
		this.dimensions = d;
	}

	/** Set whether or not this object will remain after Symmetries are generated : only to be used with .points2 files. */
	public void setFinalPoint(boolean finalPoint) {
		this.finalPoint = finalPoint;
	}


    public void setFilename(String s) { 
	filename = s; 
	// log.info("object filename = " + filename);
    }


	/** Sets positions in space without changing position of child nodes */
	public void setIsolatedPosition(Vector3D point) {
		Vector3D currentPos = getPosition(); // TODO potentially expensive method call
		Vector3D shift = point.minus(currentPos); // difference vector between current position and target position
		Vector3D negShift = shift.mul(-1.0);
		for (int i = 0; i < size(); ++i) {
			Object3D child = getChild(i);
			child.translate(negShift); // translate in opposite direction. Relatively cheap method
		}
		translate(shift);
	}

	public void setLinkSet(LinkSet l) { links = l; }

	/** Change name of object (changes id). */
	public void setName(String n) {
		name = new String(n);
	}

	/** Change number of object. */
	public void setNumber(int n) {
		number = n;
	}

	/** set pointer to parent object. Use carefully. */
	public void setParent(Object3D p) {
		parent = p;
		if (p != null) {
		    ((SimpleObject3D)p).invalidateBoundingCache();
		}
	}

	/** Set position in space. Sets absolute position in space and
	translates children accordingly. If possible prefer method translate, because that method is faster. */
	public void setPosition(Vector3D point) {
	    assert point.isValid();
	    Vector3D currentPos = getPosition(); // TODO potentially expensive method call
	    Vector3D shift = point.minus(currentPos); // difference vector between current position and target position
	    translate(shift);
	}

	/** sets arbitrary property object
	 * @TODO proper implementation
	 */
	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	/** sets property key value pair */
	public void setProperty(String key, String value) {
		if (properties == null) {
			properties = new Properties();
		}
		properties.setProperty(key, value);
	}

    public void setScore(double score) { this.score = score; }

	/** Set position in space. Sets relative position to parent. */
	public void setRelativePosition(Vector3D v) {
		relativePosition.copy(v);
		invalidateBoundingCache();
	}

	/** sets rotation angle defining internal object orientation.
	 * Does not affect child nodes! If you want to rotate a whole
	 * subtree, call the method rotate() 
	 */
	public void setRotationAngle(double a) { this.rotationAngle = a; }

	/** sets rotation axis defining internal object orientation.
	 * Does not affect child nodes! If you want to rotate a whole
	 * subtree, call the method rotate() 
	 */
	public void setRotationAxis(Vector3D v) { this.rotationAxis = v; }

	public void setSelected(boolean f) {
		selected = f;
	}

	public void setShapeSet(Shape3DSet shapeSet) {
		this.shapeSet = shapeSet;
	}

	/** sets z-buffer priority */
	public void setZBufValue(double d) {
		zBufValue = d;
	}

	/** How many children are there? */
	public int size() {
		return children.size();
	}
    
    public String toString() { return infoString(); }

    public String toString2() {
		String result = new String();
		result = "(" + getClassName() + " "  + toStringBody() + " )";
		//	log.info("SimpleObject3D string: " + result);
		return result;
	}

	/** returns body text of SimpleObject3D */
	public static String toStringBody(Object3D obj) {
		return obj.toString();
	}

	public String toStringBody() {
		String result = new String();
		result = getName() + " ";
		result = result + getPosition(); // getRelativePosition().toString(); // write relative position vector
		result = result + " (children " + size();
		for (int i = 0; i < size(); ++i) {
			if (!(this instanceof StrandJunction3D) && 
					!(getChild(i) instanceof RnaStrand)) {
				result += " " + getChild(i).toString();
			}
			else {
				result += " (RnaStrand )";
			}
		}
		result = result + " ) "; // end of children item
		return result;
	}

	/** Translate object. */
	public void translate(Vector3D vec) {
		relativePosition.add(vec);
		invalidateBoundingCache();
	}

	/** writes some of content */
	public void write(PrintWriter pw) {
		pw.print("(" + className + " ");
		if ((name != null)&&(name.length()>0)){
			pw.print(name + " ");
		}
		else {
			pw.print("unnamed ");
		}
		pw.print(")");
	}

	/** Determine if this object is the ancestor of another
	 * object */
	public boolean isAncestor(Object3D o) {
		if(o.getParent() == null)
			return false;

		if(equals(o.getParent()))
			return true;


		return isAncestor(o.getParent());

	}


	/** generates hash table with indices corresponding to names */
	private void updateNameIndices() {
	    if (size() == 0) {
		nameIndices = null;
		return;
	    }
	    else {
		if (nameIndices == null) {
		    nameIndices = new HashMap<String,Integer>();
		}
		nameIndices.clear();
		for (int i = 0; i < size(); ++i) {
		    nameIndices.put(getChild(i).getName(), new Integer(i));
		}
	    }
	    if (nameIndices.size() != size()) {
		nameIndices = null; // hash map cannot be used
		// TODO : fix if this occurs!
		log.warning("Duplicate child name detected for object: " + getName());
		for (int i = 0; i < size(); ++i) {
		    log.warning("" + (i+1) + " " + getChild(i).getName());
		}
		log.warning(new Throwable().getStackTrace().toString());
		assert(false);
	    }
	}

}

/*
  still implement clone method! 
  virtual pointer clone() const = 0; 

     PREDICATES 

     Is other equal to us? 
      virtual bool equal(const Object3D& other) const; 


      iterator begin();

      const_iterator begin() const;

      iterator end();

      const_iterator end() const;


    // THIS METHOD IS PRIVATE NOW. 
     Get id of object. 
    //   const Identity& getIdentity() const;

     MODIFIERS 

 */
