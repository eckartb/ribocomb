package tools3d.objects3d;

import tools3d.Positionable3D;
import org.testng.annotations.Test;

public class SimpleObject3DLinkSetBundle implements Object3DLinkSetBundle {

    LinkSet links;
    Object3D object3D;

    public SimpleObject3DLinkSetBundle() { }

    public SimpleObject3DLinkSetBundle(Object3D object3D, LinkSet linkSet) {
	setObject3D(object3D);
	setLinks(linkSet);
    }
    
    public Object cloneDeep() {
	Object3D newRoot = (Object3D)getObject3D().cloneDeep();
	Object3DSet origSet = new SimpleObject3DSet(this.getObject3D());
	Object3DSet newSet =  new SimpleObject3DSet(newRoot);
	LinkSet newLinks = new SimpleLinkSet();
	for (int i = 0; i < origSet.size(); ++i) {
	    for (int j = i; j < origSet.size(); ++j) {
		LinkSet foundLinks = links.findLinks(origSet.get(i), origSet.get(j));
		for (int k = 0; k < foundLinks.size(); ++k) {
		    // generated cloned link, however the clone contains references to the NEW objects:
		    newLinks.add((Link)foundLinks.get(k).clone(newSet.get(i), newSet.get(j)));
		}
	    }
	}
	return new SimpleObject3DLinkSetBundle(newRoot, newLinks);
    }

    /** Makes bundles sortable by score of its Object3D . */
    public int compareTo(Object3DLinkSetBundle otherBundle) {
	//	assert other instanceof Object3DLinkSetBundle;
	// 	Object3DLinkSetBundle otherBundle = (Object3DLinkSetBundle)other;
	if (getObject3D() == null) {
	    if (otherBundle.getObject3D() == null) {
		return 0;
	    }
	    else {
		return 1; // null is sorted as last element
	    }
	}
	if (otherBundle.getObject3D() == null) {
	    return -1;
	}
	assert getObject3D() != null;
	assert otherBundle.getObject3D() != null;
	return getObject3D().compareTo(otherBundle.getObject3D());
    }

    public Object3D getObject3D() { return object3D; }

    public LinkSet getLinks() { return links; }

    public void setObject3D(Object3D obj) { this.object3D = obj; }

    public void setLinks(LinkSet linkSet) { this.links = linkSet; }

    public String toString() {
	String s = new String("(SimpleObject3DLinkSetBundle ");
	s += getObject3D().toString();
	s += getLinks().toString();
	return s;
    }

    @Test public void testToString() {
	String supposed = "(SimpleObject3DLinkSetBundle ";
	//TODO: christine: not finished!
    }

}
