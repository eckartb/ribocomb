package tools3d.objects3d;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import tools3d.Vector3D;

public class SimpleObject3DSet implements Object3DSet {

    private List<Object3D> objects = new ArrayList<Object3D>();
    private Properties properties = null;

    public SimpleObject3DSet() { }

    public SimpleObject3DSet(Object3D tree) {
	Object3DEmptyVisitor visitor = new Object3DEmptyVisitor(tree);
	while (visitor.hasNext()) {
	    Object3D obj = (Object3D)(visitor.next());
	    add(obj);
	}
    }

    /** Constructor using list of object3d */
    public SimpleObject3DSet(List<? extends Object3D> list) {
	for (Object3D obj : list) {
	    objects.add(obj);
	}
    }

    public void add(int i, Object3D object3d) {objects.add(i, object3d); }

    /** returns n'th object3d */
    public void add(Object3D object3d) { objects.add(object3d); }

    public void clear() { objects.clear(); }

    /** returns true, if object is contained in set (using == operator, not equals )*/
    public boolean contains(Object3D obj) {
	return objects.contains(obj); 
// 	for (int i = 0; i < size(); ++i) {
// 	    if (get(i) == obj) {
// 		return true;
// 	    }
// 	}
// 	return false;
    }

    /** Returns list of objects */
    public List<Object3D> getAsList() { return objects; }

    /** Returns list of positions of objects. TODO : slow, use caching instead */
    public List<Vector3D> getAsPositions() {
	List<Vector3D> positions =new ArrayList<Vector3D>();
	for (Object3D obj : objects) {
	    positions.add(obj.getPosition());
	}
	return positions;
    }

    /** returns key value pair */
    public String getProperty(String key) {
	if (properties == null) {
	    return null;
	}
	return properties.getProperty(key);
    }

    /** returns properties object */
    public Properties getProperties() {
	return properties;
    }

    /** returns index of first occurrent of obj, -1 if not found */
    public int indexOf(Object3D obj) {
	return objects.indexOf(obj);
    }

    /** returns n'th object3d */
    public Object3D get(int n) throws IndexOutOfBoundsException { 
	if (objects.get(n) == null) {
	    return null;
	}
	return (Object3D)(objects.get(n)); 
    }

    /** merges other set to current set */
    public void merge(Object3DSet other) {
	if (other == null) {
	    return;
	}
	for (int i = 0; i < other.size(); ++i) {
	    add(other.get(i));
	}
    }

    /** sets n'th object */
    public void set(int n, Object3D obj) {
	assert(n < size());
	objects.set(n, obj);
    }

    /** sets arbitrary text property key - value pair */
    public void setProperty(String key, String value) {
	if (properties == null) {
	    properties = new Properties();
	}
	properties.setProperty(key, value);
    }

    /** returns number of objects */
    public int size() { return objects.size(); }

    public void remove(Object3D object3d) { objects.remove(object3d); }

    /** Removes the points that occur on a line, those that are not needed. CV */
    public void removeExtras(Object3D objectTree) {
	//	log.severe("METHOD NOT YET IMPLEMENTED");
    }

    /** returns output string */
    public String toString() {
	String result = "(Object3DSet " + size() + " ";
	for (int i = 0; i < size(); ++i) {
	    result = result + get(i).toString() + " ";
	}
	result = result + ")";
	return result;
    }

    /** reads objects, given information about exisiting trees 
     * (used for converting object names into object references)
     */
    /*
    public void read(InputStream is, Object3D tree) throws Object3DIOException {
	// TODO not yet implemented!
	StringTools st = new StringTools();
 	String expected = "(Object3DSet";
	DataInputStream dis = new DataInputStream(is);
 	String word = st.readWord(dis);
 	if (!word.equals(expected)) {
 	    throw new Object3DIOException("Object3DSet.read: " + expected + " excepted instead of " + word);
 	}
 	word = st.readWord(dis);
 	int numObject3Ds = 0;
 	try {
 	    numObject3Ds = Integer.parseInt(word);
 	}
 	catch (NumberFormatException e) {
 	    throw new Object3DIOException("createObject3DGraph: Could not parse number of children: " + word);	
 	}
 	// read individual objects
 	for (int i = 0; i < numObject3Ds; ++i) {
 	    Object3D object3d = new SimpleObject3D();
	    object3d.read(dis, tree);
	    add(object3d);
 	}
 	expected = ")"; // end of reading objects
 	word = st.readWord(dis);
 	if (!word.equals(expected)) {
 	    throw new Object3DIOException("createObject3D: " + expected 
 					  + " excepted instead of " + word);
 	}

    }
    */

}
