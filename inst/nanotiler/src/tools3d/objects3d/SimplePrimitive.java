package tools3d.objects3d;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import tools3d.CoordinateSystem;
import tools3d.Positionable3D;
import tools3d.Vector3D;

/**
 *  Describes simple polygon or line or point
 */
public class SimplePrimitive implements Primitive3D {

    double zBufValue = 0;

    List<Vector3D> list = new ArrayList<Vector3D>();

    Color color = new Color(77, 77, 77); // gray

    Vector3D position = new Vector3D(0.0, 0.0, 0.0);

    public void activeTransform(CoordinateSystem cs) {
	assert false; // TODO : not yet implemented
    }

    public void passiveTransform(CoordinateSystem cs) {
	assert false; // TODO : not yet implemented
    }

    public void add(Vector3D v) {
	list.add(v);
	update();
    }

    /** deletes all info */
    public void clear() {
	list.clear();
	update();
    }

    /** clone method inherited from Object
     * @TODO NOT YET IMPLEMENTED! */
    public Object clone() {
	assert false;
	return this;
    }

    /** clone method inherited from Object
     * @TODO NOT YET IMPLEMENTED! */
    public Object cloneDeep() {
	assert false;
	return this;
    }
    
    /** used for comparing object (< == or >) */
    public int compareTo(Positionable3D p) {
	    double val = zBufValue;
	    double oVal = p.getZBufValue();
	    if (val < oVal) {
		return -1;
	    }
	    else if (val == oVal) {
		return 0;
	    }
	    return 1;
    }

    /** return distance between objects */
    public double distance(Positionable3D other) {
	return (getPosition().minus(other.getPosition())).length();
    }

    /** returns average of poins */
    public Vector3D getPosition() {
	return position;
    }

    public double getZBufValue() {
	return zBufValue;
    }
    
    /** returns color */
    public Color getColor() {
	return color;
    }

    /** returns n'th point */
    public Vector3D getPoint(int n) {
	return (Vector3D)(list.get(n));
    }

    public boolean isValid() { return getPosition().isValid(); }

    /** sets color */
    public void setColor(Color c) {
	color = c;
    }

    public void setZBufValue(double z) {
	zBufValue = z;
    }

    /** returns number of points */
    public int size() {
	return list.size();
    }

    public void translate(Vector3D shift) {
	position.add(shift);
	for (int i = 0; i < list.size(); ++i) {
	    list.get(i).add(shift);
	}
    }

    private void update() {
	position.setX(0.0);
	position.setY(0.0);
	position.setZ(0.0);
	if (size() == 0) {
	    return;
	}
	for (int i = 0; i < size(); ++i) {
	    position.add((Vector3D)(list.get(i)));
	}
	position.scale(1.0/(double)size());
    }

}
