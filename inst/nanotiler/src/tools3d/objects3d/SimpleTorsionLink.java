/** implements concept of two linked objects */
package tools3d.objects3d;

import tools3d.Vector3D;

import generaltools.ConstraintDouble;
import generaltools.SimpleConstraintDouble;

/** Computes torsion angle between v1-v2 and v4-v3 . FIXIT: check with official definition! */
public class SimpleTorsionLink extends SimpleLink implements TorsionLink {

    private ConstraintDouble constraint;
    private Object3D obj3;
    private Object3D obj4;

    public SimpleTorsionLink(Object3D o1, Object3D o2, Object3D o3, Object3D o4, ConstraintDouble constraint) {
	super(o1, o2);
	this.obj3 = o3;
	this.obj4 = o4;
	this.constraint = constraint;
    }

    public SimpleTorsionLink(Object3D o1, Object3D o2, Object3D o3, Object3D o4, double min, double max) {
	super(o1, o2);
	this.obj3 = o3;
	this.obj4 = o4;
	this.constraint = new SimpleConstraintDouble(min, max);	
    }

    /** returns constraint */
    public ConstraintDouble getConstraint() { return constraint; }

    public Object3D getObj3() { return obj3; }

    public Object3D getObj4() { return obj4; }

    public double computeError() {
	return computeError(getObj1().getPosition(),
			    getObj2().getPosition(),
			    getObj3().getPosition(),
			    getObj4().getPosition());
    }
    
    public double computeError(Vector3D v1, Vector3D v2, Vector3D v3, Vector3D v4) {
	double ang = Vector3D.torsionAngle(v1, v2, v3, v4);
	assert (ang >= 0.0);
	assert (ang <= Math.PI);
	double result = 0.0;
	if (ang < constraint.getMin()) {
	    result = constraint.getMin() - ang;
	} else if (ang > constraint.getMax()) {
	    result = ang - constraint.getMax();
	}
	assert (result >= 0.0);
	assert (result <= Math.PI);
	return result;
    }

}
	
