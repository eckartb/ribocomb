package tools3d.objects3d;

/** Interface for entity that links two different symmetries. Ids are usually according to SymmetryController */
public interface SymmetryLinker {

    int getSymId1();

    int getSymId2();

    void setSymId1(int id);

    void setSymId2(int id);

}
