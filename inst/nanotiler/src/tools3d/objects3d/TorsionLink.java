/** implements concept of two linked objects */
package tools3d.objects3d;

import tools3d.Vector3D;
import generaltools.ConstrainableDouble;

/** interface for link that has a min/max constraint attached to it */
public interface TorsionLink extends ConstrainableDouble, Link {

    Object3D getObj3();

    Object3D getObj4();

    double computeError();

    double computeError(Vector3D v1, Vector3D v2, Vector3D v3, Vector3D v4);

}
	
