package tools3d.objects3d.modeling;

import java.util.ArrayList;
import java.util.List;

import controltools.ModelChangeEvent;
import controltools.ModelChangeListener;

/** interface for optimization algorithms */
public abstract class AbstractMinimizer implements Minimizer {

    private List<ModelChangeListener> changeListeners = new ArrayList<ModelChangeListener>();

    protected boolean running = false;

    protected Trajectory trajectory;

    public void addModelChangeListener(ModelChangeListener listener) {
	changeListeners.add(listener);
    }

    // Typilcally specify in constructor: Object3D objects, LinkSet links, MinimizationParameters);

    public void fireModelChanged(ModelChangeEvent event) {
	for (int i = 0; i < changeListeners.size(); ++i) {
	    ModelChangeListener listener = (ModelChangeListener)(changeListeners.get(i));
	    listener.modelChanged(event);
	}
    }

    /** returns trajectory */
    public Trajectory getTrajectory() { return trajectory; }

    /*
    public void removeModelChangeListener(ModelChangeListener listener) {
	changeListeners.remove(listener);
    }
    */

}
