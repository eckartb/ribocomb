package tools3d.objects3d.modeling;

import tools3d.objects3d.LinkSet;
import tools3d.objects3d.Object3DSet;

/** Detects collisions between objects */
public class CollisionForceField implements ForceField {

    private double collisionDistance = 0.5;

    private double penalty = 100.0;

    public double energy(Object3DSet objects, LinkSet links) {
	double result = 0.0;
	double dist = 0.0;
	for (int i = 0; i < objects.size(); ++i) {
	    for (int j = i+1; j < objects.size(); ++j) {
		dist = objects.get(i).distance(objects.get(j));
		if (dist < collisionDistance) {
		    result = 100.0;
		    continue;
		    //    result += penalty; //CHRISTINE - is it necessary to keep testing after one collision is detected? Much faster if quit loop.
		}
	    }
	}
	return result;
    }

    public double getCollisionDistance() { return this.collisionDistance; }

    public double getPenalty() { return penalty; }

    public void setCollisionDistance(double c) { 
	this.collisionDistance = c; 
    }

    public void setPenalty(double p) { this.penalty = p; }

}
