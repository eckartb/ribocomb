package tools3d.objects3d.modeling;

public interface CompositeForceField extends ForceField {

    public void addForceField(ForceField ff, double weight);

    /** returns n'th force field */
    public ForceField getForceField(int n);

    /** returns weight of n'th force field */
    public double getWeight(int n);
    
    /** returns number of forcefields */
    public int size();

}
