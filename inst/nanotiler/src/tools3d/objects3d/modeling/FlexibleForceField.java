package tools3d.objects3d.modeling;

public interface FlexibleForceField extends ForceField, ForceFieldPairElement, ForceFieldLinkPairElement {

    /** adds force field that computes pair of elements. 
     * Carelful: add force field to be applicable most likely first
     * (for efficiency reasons)
     */
    public void addForceFieldPairElement(ForceFieldPairElement ff);
    
    /** adds force field that computes pair of elements. 
     * Carelful: add force field to be applicable most likely first
     * (for efficiency reasons)
     */
    public void addForceFieldLinkPairElement(ForceFieldLinkPairElement ff);
    
    /** returns n'th force field element */
    public ForceFieldPairElement getForceFieldPairElement(int n);

    /** returns n'th force field element */
    public ForceFieldLinkPairElement getForceFieldLinkPairElement(int n);

    /** returns number of pair force fields */
    public int forceFieldPairElementCount();

    /** returns number of pair force fields */
    public int forceFieldLinkPairElementCount();

}
