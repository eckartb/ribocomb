package tools3d.objects3d.modeling;

import tools3d.objects3d.LinkSet;
import tools3d.objects3d.Object3DSet;

/** important basic interface for forcefield */
public interface ForceField {

    double energy(Object3DSet objects, LinkSet links);

}
