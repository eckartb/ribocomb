package tools3d.objects3d.modeling;

/** interface for factory pattern of forcefield */
public interface ForceFieldFactory {

    public ForceField generateForceField();

    /** returns id of current force field (corresponding to getForceFieldNames) */
    public int getForceFieldId();

    /** returns all defined force field names */
    public String[] getForceFieldNames();

    /** sets id of current force field (corresponding to getForceFieldNames) */
    public void setForceFieldId(int n);

}
