package tools3d.objects3d.modeling;

import tools3d.objects3d.Link;

public interface ForceFieldLinkPairElement {

    /** return energy of pair of links */
    double pairEnergy(Link link1, Link link2);

    /** returns true if it can compute pairEnergy of link pair */
    boolean handles(Link link1, Link link2);

}
