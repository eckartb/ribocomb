package tools3d.objects3d.modeling;

import tools3d.objects3d.LinkSet;
import tools3d.objects3d.Object3D;

public interface ForceFieldPairElement {

    /** return energy of pair of objects. Does NOT traverse tree. */
    double pairEnergy(Object3D obj1, Object3D obj2,LinkSet links);

    /** returns true if it can compute pairEnergy of object pair */
    boolean handles(Object3D obj1, Object3D obj2, LinkSet links);

}
