package tools3d.objects3d.modeling;

import tools3d.objects3d.LinkSet;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DSet;

/** returns zero energy if all constraints are fullfilled, higher zero otherwise */
public class GaussianRepulsionForceField implements ForceField {

    public GaussianRepulsionForceField() {
    }

    /** returns energy: only a function of violated constraints */
    public double energy(Object3DSet objects, LinkSet links) {
	double result = 0.0;
	for (int i = 0; i < objects.size(); ++i) {
	    for (int j = 0; j > i; ++j) {
		result += interactionEnergy(objects.get(i), objects.get(j));
	    }
	}
	return result;
    }

    /** interaction energy depends on distance, radii and depth
     *  TODO remove depth dependence
     */
    double interactionEnergy(Object3D o1, Object3D o2) {
	double result = 0.0;
	if (o1.getDepth() == o2.getDepth()) {
	    double dist = o1.distance(o2);
	    double r = o1.getBoundingRadius() + o2.getBoundingRadius();
	    double term = (dist/r);
	    result = Math.exp(-term*term);
	}
	return result;
    }

}
