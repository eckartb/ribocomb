package tools3d.objects3d.modeling;

import tools3d.objects3d.Object3D;

/** interface for parameters of optimization algorithms */
public interface MinimizationParameters {

    public ForceField getForceField();

    /** how often is a ModelChangeEvent fired? "5" means for example every 5 time steps */
    public int getModelChangeIntervall();

    public Object3D getPrototype();

    /** gets maximum number of "time" steps */
    public int getTimeStepMax();

    /** if false, do not keep trajectory information */
    public boolean isCollectTrajectoryMode();

    public boolean isValid();

    /** if false, do not keep trajectory information */
    public void setCollectTrajectoryMode(boolean mode);

    /** how often is a ModelChangeEvent fired? "5" means for example every 5 time steps */
    public void setModelChangeIntervall(int n);

    public void setForceField(ForceField ff);

    public void setPrototype(Object3D prototype);

    /** sets maximum number of "time" steps */
    public void setTimeStepMax(int n);

}
