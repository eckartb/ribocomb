package tools3d.objects3d.modeling;

import controltools.ModelChanger;

/** interface for optimization algorithms */
public interface Minimizer extends ModelChanger, Runnable {

    // Typilcally specify in constructor: Object3D objects, LinkSet links, MinimizationParameters);

    /** returns trajectory */
    public Trajectory getTrajectory();

    /** start central minimization routine */
    public void run(); 


}
