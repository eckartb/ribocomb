package tools3d.objects3d.modeling;

import java.util.Random;
import java.util.logging.Logger;
import generaltools.Randomizer;
import controltools.ModelChangeEvent;
import tools3d.Orientable;
import tools3d.Vector3D;
import tools3d.objects3d.LinkSet;
import tools3d.objects3d.Object3DSet;

/** interface for optimization algorithms */
public class MonteCarloMinimizer extends AbstractMinimizer implements Minimizer {

    Object3DSet objects;

    LinkSet links;

    MinimizationParameters parameters;

    CoordinateSnapShot origSnapShot;

    Trajectory trajectory;

    Random random = Randomizer.getInstance();

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    MonteCarloParameters mcParameters;

    /** specialized parameters */
    public class MonteCarloParameters {

	double randomWidth = 1.0;

    }

    // Typilcally specify in constructor: Object3D objects, LinkSet links, MinimizationParameters);
    public MonteCarloMinimizer(Object3DSet objects, LinkSet links, MinimizationParameters parameters,
			       MonteCarloParameters mcParameters) {
	this.objects = objects;
	this.links = links;
	this.parameters = parameters;
	// this.origSnapShot = new SimpleCoordinateSnapShot(objects, parameters.getPrototype());
	this.origSnapShot = generateSnapShot();
	this.mcParameters = mcParameters;
    }

    // Typilcally specify in constructor: Object3D objects, LinkSet links, MinimizationParameters);
    public MonteCarloMinimizer(Object3DSet objects, LinkSet links, MinimizationParameters parameters) {
	this.objects = objects;
	this.links = links;
	this.parameters = parameters;
	this.origSnapShot = generateSnapShot();
	log.finest("Snapshot of size " + origSnapShot.size() + " generated");
	this.mcParameters = new MonteCarloParameters();
    }

     /** start central minimization routine */
    public void run() {
	ForceField ff = parameters.getForceField();
	for (int i = 0; i < parameters.getTimeStepMax(); ++i) {
	    double oldEnergy = ff.energy(objects, links);
	    CoordinateSnapShot oldSnapShot = generateSnapShot();
	    mutate(objects);
	    double newEnergy = ff.energy(objects, links);
	    log.finest("minimizing step " + (i + 1) + " old " + oldEnergy + " new " + newEnergy);
	    if (newEnergy > oldEnergy) {
		applySnapShot(oldSnapShot); // revert to old coordinates
	    }
	    else if (newEnergy < oldEnergy) {
		// trajectory.addSnapShot(new SimpleCoordinateSnapShot(objects));
	    }
	}
	fireModelChanged(new ModelChangeEvent(this)); // so far notife at every step
    }

    public void applySnapShot(CoordinateSnapShot snapShot) {
	snapShot.applySnapShot(objects);
    }

    public CoordinateSnapShot generateSnapShot() {
	return new SimpleCoordinateSnapShot(objects, parameters.getPrototype());
    }

    public void mutate(Object3DSet objects) {
	CoordinateSnapShot snapShot = generateSnapShot();
	int nCoord = random.nextInt(snapShot.size());
	Orientable orient = snapShot.getCoordinate(nCoord);
	mutate(orient);
	applySnapShot(snapShot);
    }

    public void mutate(Orientable orient) {
	Vector3D pos = orient.getPosition();
	mutate(pos);
    }

    public void mutate(Vector3D pos) {
	pos.setX(pos.getX() + (2.0*mcParameters.randomWidth*(random.nextDouble()-0.5)));
	pos.setY(pos.getY() + (2.0*mcParameters.randomWidth*(random.nextDouble()-0.5)));
	pos.setZ(pos.getZ() + (2.0*mcParameters.randomWidth*(random.nextDouble()-0.5)));
    }
}
