package tools3d.objects3d.modeling;

import java.util.ArrayList;
import java.util.List;

import tools3d.objects3d.LinkSet;
import tools3d.objects3d.Object3DSet;

public class SimpleCompositeForceField implements CompositeForceField {

    private List<ForceField> forceFields = new ArrayList<ForceField>();

    private List<Double> weights = new ArrayList<Double>();

    public void addForceField(ForceField ff, double weight) { 
	forceFields.add(ff); 
	weights.add(new Double(weight)); 
    }

    public double energy(Object3DSet objects, LinkSet links) {
	double result = 0.0;
	for (int i = 0; i < size(); ++i) {
	    result += getForceField(i).energy(objects, links);
	}
	return result;
    }

    /** returns n'th force field */
    public ForceField getForceField(int n) { return (ForceField)(forceFields.get(n)); }

    /** returns weight of n'th force field */
    public double getWeight(int n) { 
	Double d = (Double)(weights.get(n));
	return d.doubleValue();
    }
    
    /** returns number of forcefields */
    public int size() { return forceFields.size(); }

}
