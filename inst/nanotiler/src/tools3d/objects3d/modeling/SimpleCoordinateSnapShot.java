package tools3d.objects3d.modeling;

import java.util.ArrayList;
import java.util.List;

import tools3d.Orientable;
import tools3d.SimpleOrientable;
import tools3d.Vector3D;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DEmptyVisitor;
import tools3d.objects3d.Object3DSet;

/** stores only positions and orientations of object tree.
 * Variant of mementum pattern. TODO: implement true mementum pattern, would be safer.  
 * This version requires constant objects identities in set.*/
public class SimpleCoordinateSnapShot implements CoordinateSnapShot {

    private List<SimpleOrientable> coordinates = new ArrayList<SimpleOrientable>();
    private Object3D prototype = null;

    public SimpleCoordinateSnapShot(Object3D objects) {
	Object3DEmptyVisitor visitor = new Object3DEmptyVisitor(objects);
	while (visitor.hasNext()) {
	    SimpleOrientable orientable = new SimpleOrientable();
	    orientable.copyDeep((Object3D)(visitor.next()));
	    coordinates.add(orientable);
	}
    }

    public SimpleCoordinateSnapShot(Object3DSet objects) {
	for (int i = 0; i < objects.size(); ++i) {
	    SimpleOrientable orientable = new SimpleOrientable();
	    orientable.copyDeep(objects.get(i));
	    coordinates.add(orientable);
	}
    }

    public SimpleCoordinateSnapShot(Vector3D[] positions) {
	for (int i = 0; i < positions.length; ++i) {
	    SimpleOrientable orientable = new SimpleOrientable();
	    orientable.setPosition(positions[i]);
	    coordinates.add(orientable);
	}
    }

    public SimpleCoordinateSnapShot(Object3DSet objects, Object3D prototype) {
	this.prototype = prototype;
	for (int i = 0; i < objects.size(); ++i) {
	    Object3D obj = objects.get(i);
	    if (obj.getClassName().equals(prototype.getClassName())) {
		SimpleOrientable orientable = new SimpleOrientable();
		orientable.copyDeep(obj);
		coordinates.add(orientable);
	    }
	}
    }

    public SimpleCoordinateSnapShot(Object3D objects, Object3D prototype) {
	Object3DEmptyVisitor visitor = new Object3DEmptyVisitor(objects);
	this.prototype = prototype;
	while (visitor.hasNext()) {
	    Object3D obj  = (Object3D)(visitor.next());
	    if (obj.getClassName().equals(prototype.getClassName())) {
		SimpleOrientable orientable = new SimpleOrientable();
		orientable.copyDeep(obj);
		coordinates.add(orientable);
	    }
	}
    }

    /** sets coordinates of object tree to stored coordinates */
    public void applySnapShot(Object3D objects) {
	Object3DEmptyVisitor visitor = new Object3DEmptyVisitor(objects);
	int pc = 0;
	while (visitor.hasNext()) {
	    Object3D obj = (Object3D)(visitor.next());
	    if ((prototype == null) || (obj.getClassName().equals(prototype.getClassName()))) {
		SimpleOrientable orientable = (SimpleOrientable)(coordinates.get(pc++));
		applySnapShot(obj, orientable);
	    }
	}
    }

    /** sets coordinates of object tree to stored coordinates */
    public void applySnapShot(Object3DSet objects) {
	int pc = 0;
	for (int i = 0; i < coordinates.size(); ++i, ++pc) {
	    Object3D obj = objects.get(pc);
	    if (prototype != null) {
		while ((pc < objects.size()) && (! obj.getClassName().equals(prototype.getClassName()))) {
		    ++pc;
		    obj = objects.get(pc);
		}
	    }
	    if ((prototype == null) || (obj.getClassName().equals(prototype.getClassName()))) {
		SimpleOrientable orientable = (SimpleOrientable)(coordinates.get(i));
		applySnapShot(obj, orientable);
	    }
	}
    }

    /** sets coordinates of object3d according to coordinate system */
    public void applySnapShot(Object3D obj, Orientable orient) {
	obj.setPosition(orient.getPosition());
	obj.setRotationAngle(orient.getRotationAngle());
	obj.setZBufValue(orient.getZBufValue());
	obj.setRotationAxis(orient.getRotationAxis());
    }

    public Orientable getCoordinate(int n) {
	return (Orientable)(coordinates.get(n));
    }

    public int size() {	return coordinates.size();  }

}
