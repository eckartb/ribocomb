package tools3d.objects3d.modeling;

import generaltools.DoubleFunctor;
import tools3d.objects3d.LinkSet;
import tools3d.objects3d.Object3D;

/** implements simple distance depends force field element
 * that computes an interactio energy of pairs of objects.
 * Optionally one can specifiy minimum and maximum link numbers
 * (the number of links needed to connected the two objects)
 * with zero indicating "no link" (equivalent to an infinite link number)
 * or smaller zero values indicating "don't care".
 */
public class SimpleDistanceForceFieldPairElement implements ForceFieldPairElement {

    /** class that implements function from R-> R */
    private DoubleFunctor functor;
    private int linkNumberMax;
    private int linkNumberMin;
    private Object3D prototype;

    public SimpleDistanceForceFieldPairElement(Object3D prototype,
					       DoubleFunctor functor,
					       int linkNumberMin,
					       int linkNumberMax) {
	this.prototype = prototype;
	this.functor = functor;
	this.linkNumberMin = linkNumberMin;
	this.linkNumberMax = linkNumberMax;
    }

    /** return energy of pair of objects. Does NOT traverse tree.
    */
    public double pairEnergy(Object3D obj1, Object3D obj2, LinkSet links) {
	if (handles(obj1, obj2, links)) {
	    double dist = obj1.distance(obj2);
	    return functor.doubleFunc(dist);
	}
	return 0.0;
    }

    private boolean handlesLinkNumberMax(int linkNumber) {
	// maximum number of links zero: infinite number of links 
	// (== no connection), (<0 :we dont care)
	if (linkNumberMax <= 0) {
	    return true; // return true no matter what
	}
	return (linkNumber <= linkNumberMax);
    }

    private boolean handlesLinkNumberMin(int linkNumber) {
	// maximum number of links zero: infinite number of links 
	// (== no connection), (<0 :we dont care)
	if (linkNumberMax < 0) {
	    return true; // return true no matter what
	}
	else if (linkNumberMin == 0) { // no connection required
	    return (linkNumber == 0);
	}
	return (linkNumber >= linkNumberMin);
    }


    /*
     * link number n: there are n-links between obj1 and obj2.
     * n zero: there is no link between objects obj1 and obj2 
     * n negative : don't care about this link number.
     */
    private boolean handlesLinkNumber(int linkNumber) {
	return (handlesLinkNumberMax(linkNumber)
		&& handlesLinkNumberMin(linkNumber));
    }

    /** returns true if it can compute pairEnergy of object pair */
    public boolean handles(Object3D obj1, Object3D obj2, LinkSet links) {
	if ((prototype.getClassName().equals(obj1.getClassName()))
	    && (prototype.getClassName().equals(obj2.getClassName()))) {
	    int linkNumber = links.getLinkNumber(obj1, obj2);
	    return handlesLinkNumber(linkNumber);
	}
	return false;
    }

}
