package tools3d.objects3d.modeling;

import java.util.ArrayList;
import java.util.List;

import tools3d.objects3d.Link;
import tools3d.objects3d.LinkSet;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DSet;

public class SimpleFlexibleForceField implements FlexibleForceField {

    private List<ForceFieldPairElement> pairForceFields = new ArrayList<ForceFieldPairElement>();
    private List<ForceFieldLinkPairElement> linkPairForceFields = new ArrayList<ForceFieldLinkPairElement>();

    /** adds force field that computes pair of elements. 
     * Carelful: add force field to be applicable most likely first
     * (for efficiency reasons)
     */
    public void addForceFieldPairElement(ForceFieldPairElement ff) {
	pairForceFields.add(ff);
    }

    public int forceFieldPairElementCount() { 
	return pairForceFields.size();
    }

    public ForceFieldPairElement getForceFieldPairElement(int n) {
	return (ForceFieldPairElement)(pairForceFields.get(n));
    }

    /** adds force field that computes pair of elements. 
     * Carelful: add force field to be applicable most likely first
     * (for efficiency reasons)
     */
    public void addForceFieldLinkPairElement(ForceFieldLinkPairElement ff) {
	linkPairForceFields.add(ff);
    }

    public int forceFieldLinkPairElementCount() { 
	return linkPairForceFields.size();
    }

    public ForceFieldLinkPairElement getForceFieldLinkPairElement(int n) {
	return (ForceFieldLinkPairElement)(linkPairForceFields.get(n));
    }

    /** central energy computation */
    public double energy(Object3DSet objects, LinkSet links) {
	double result = 0.0;
	for (int i = 1; i < objects.size(); ++i) {
	    for (int j = 0; j < i; ++j) {
		result += pairEnergy(objects.get(i), objects.get(j),links);
	    }
	}
	for (int i = 1; i < links.size(); ++i) {
	    for (int j = 0; j < i; ++j) {
		result += pairEnergy(links.get(i), links.get(j));
	    }
	}
	return result;
    }

    /** return energy of pair of objects. Does NOT traverse tree. */
    public double pairEnergy(Object3D obj1, Object3D obj2,LinkSet links) {
	for (int i = 0; i < forceFieldPairElementCount(); ++i) {
	    ForceFieldPairElement ff = getForceFieldPairElement(i);
	    if (ff.handles(obj1, obj2, links)) {
		return ff.pairEnergy(obj1, obj2, links);
	    }
	}
	return 0.0;
    }

    /** return energy of pair of objects. Does NOT traverse tree. */
    public double pairEnergy(Link l1, Link l2) {
	for (int i = 0; i < forceFieldLinkPairElementCount(); ++i) {
	    ForceFieldLinkPairElement ff = getForceFieldLinkPairElement(i);
	    if (ff.handles(l1, l2)) {
		return ff.pairEnergy(l1, l2);
	    }
	}
	return 0.0;
    }

    /** returns true if it can compute pairEnergy of object pair */
    public boolean handles(Object3D obj1, Object3D obj2, LinkSet links) {
	for (int i = 0; i < forceFieldPairElementCount(); ++i) {
	    ForceFieldPairElement ff = getForceFieldPairElement(i);
	    if (ff.handles(obj1, obj2, links)) {
		return true;
	    }
	}
	return false;	
    }

    /** returns true if it can compute pairEnergy of object pair */
    public boolean handles(Link l1, Link l2) {
	for (int i = 0; i < forceFieldLinkPairElementCount(); ++i) {
	    ForceFieldLinkPairElement ff = getForceFieldLinkPairElement(i);
	    if (ff.handles(l1, l2)) {
		return true;
	    }
	}
	return false;	
    }

}
