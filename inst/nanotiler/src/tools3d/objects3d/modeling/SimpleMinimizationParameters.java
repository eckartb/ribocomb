package tools3d.objects3d.modeling;

import tools3d.objects3d.Object3D;

/** interface for parameters of optimization algorithms */
public class SimpleMinimizationParameters implements MinimizationParameters {

    private ForceField forceField;

    private int modelChangeIntervall;

    private Object3D prototype;

    private int timeStepMax;

    private boolean collectTrajectoryMode;
    

    public ForceField getForceField() { return forceField; }

    /** how often is a ModelChangeEvent fired? "5" means for example every 5 time steps */
    public int getModelChangeIntervall() { return modelChangeIntervall; }

    public Object3D getPrototype() { return prototype; }

    /** gets maximum number of "time" steps */
    public int getTimeStepMax() { return timeStepMax; }

    /** if false, do not keep trajectory information */
    public boolean isCollectTrajectoryMode() { return collectTrajectoryMode; }

    public boolean isValid() {
	return forceField != null;
    }

    /** if false, do not keep trajectory information */
    public void setCollectTrajectoryMode(boolean mode) { this.collectTrajectoryMode = mode; }

    /** how often is a ModelChangeEvent fired? "5" means for example every 5 time steps */
    public void setModelChangeIntervall(int n) { this.modelChangeIntervall = n; }

    public void setForceField(ForceField ff) { this.forceField = ff; }

    public void setPrototype(Object3D prototype) { this.prototype = prototype; }

    /** sets maximum number of "time" steps */
    public void setTimeStepMax(int n) { this.timeStepMax = n; }

}
