package tools3d.objects3d.modeling;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import tools3d.objects3d.Object3D;

/** implements concept of a container to molecular trajectory data */
public class SimpleTrajectory implements Trajectory {

    private List<CoordinateSnapShot> snapShots = new ArrayList<CoordinateSnapShot>();
    private Object3D root;
    private double timeIntervall;

    public SimpleTrajectory(Object3D root, double timeIntervall) {
	this.timeIntervall = timeIntervall;
	this.root = root;
    }

    public void addSnapShot(CoordinateSnapShot snapShot) {
	snapShots.add(snapShot);
    }

    /** returns number of defined shapshots */
    public int getDefinedTimeStepCount() {
	return snapShots.size();
    }

    public int getMaxTimeSteps() {
	return 0; // TODO :not yet implemented
    }

    /** returns coordinates as they were at the n'th time step */
    public Object3D getObject3D(int timeStep) {
	applySnapShot(timeStep);
	return root;
    }

    public Properties getProperties(int timeStep) {
	return null; // not yet implemented!
    }
    
    public double getTime(int timeStep) { return timeIntervall * timeStep; }

    public double getTimeIntervall() { return timeIntervall; }

    public void setProperties(Properties p, int timeStep) {
	// TODO not yet implemented!
    }

    /** sets coordinates of root according to snapshot */
    private void applySnapShot(int timeStep) {
	CoordinateSnapShot snapShot = (CoordinateSnapShot)(snapShots.get(timeStep));
	snapShot.applySnapShot(root);
    }

}
