package tools3d.objects3d.modeling;

import java.util.Properties;

import tools3d.objects3d.Object3D;

public interface Trajectory {

    public void addSnapShot(CoordinateSnapShot snapShot);

    public int getDefinedTimeStepCount();

    public int getMaxTimeSteps();
    
    public Object3D getObject3D(int timeStep);

    public Properties getProperties(int timeStep);
    
    public double getTime(int timeStep);

    public void setProperties(Properties p, int timeStep);
}
