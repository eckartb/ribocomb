package tools3d.splines;

import tools3d.Vector3D;

import no.geosoft.cc.geometry.spline.SplineFactory;

/** wrapper interface for spline libary using double[] arrays */
public class SimpleSplineFactory implements Spline3DFactory {

    private double[] translateToArray(Vector3D[] points) {
	double[] result = new double[3*points.length];
	int pc = 0;
	for (int i = 0; i < points.length; ++i) {
	    result[pc++] = points[i].getX();
	    result[pc++] = points[i].getY();
	    result[pc++] = points[i].getZ();
	}
	return result;
    }

    private Vector3D[] translateToVectors(double[] data) {
	Vector3D[] result = new Vector3D[data.length/3];
	int pc = 0;
	for (int i = 0; i < result.length; ++i) {
		result[i] = new Vector3D(data[pc++], data[pc++], data[pc++]);
	}
	return result;
    }

    /** Create a Bezier spline based on the given control points.  */
    public Vector3D[] createBezier(Vector3D[] controlPoints, int nParts) {
	return translateToVectors(SplineFactory.createBezier(translateToArray(controlPoints), nParts));
    }

    /** Create a Catmull-Rom spline based on the given control points.   */
    public Vector3D[] createCatmullRom(Vector3D[] controlPoints, int nParts) {
	return translateToVectors(SplineFactory.createCatmullRom(translateToArray(controlPoints), nParts));
    }
	
    /** Create a cubic spline based on the given control points.  */
    public Vector3D[] createCubic(Vector3D[] controlPoints, int nParts) {
	return translateToVectors(SplineFactory.createCubic(translateToArray(controlPoints), nParts));
    }

}
