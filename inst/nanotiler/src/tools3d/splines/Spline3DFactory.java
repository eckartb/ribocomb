package tools3d.splines;

import tools3d.Vector3D;

/** wrapper interface for spline libary using double[] arrays
 * uses code from : no.geosoft.cc.geometry.spline 
 * http://geosoft.no/software/spline/docs/index.html
 */
public interface Spline3DFactory {

    /** Create a Bezier spline based on the given control points.  */
    Vector3D[] createBezier(Vector3D[] controlPoints, int nParts);

    /** Create a Catmull-Rom spline based on the given control points.   */
    Vector3D[] createCatmullRom(Vector3D[] controlPoints, int nParts);
	
    /** Create a cubic spline based on the given control points.  */
    Vector3D[] createCubic(Vector3D[] controlPoints, int nParts);

}
