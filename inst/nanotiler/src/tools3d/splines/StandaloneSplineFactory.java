package tools3d.splines;

import tools3d.Vector3D;

public class StandaloneSplineFactory implements Spline3DFactory {

	public Vector3D[] createBezier(Vector3D[] controlPoints, int nParts) {
		double[] data = vectorToArray(controlPoints);
		double[] points = createBezier(data, nParts);
		return arrayToVector(points);
	}
	
	public Vector3D[] createTangentBezier(Vector3D[] controlPoints, int nParts) {
		double[] data = vectorToArray(controlPoints);
		double[] points = createTangentBezier(data, nParts);
		return arrayToVector(points);
	}

	public Vector3D[] createCatmullRom(Vector3D[] controlPoints, int nParts) {
		// TODO Auto-generated method stub
		return null;
	}

	
	/**
	 * Parts between each control point
	 */
	public Vector3D[] createCubic(Vector3D[] controlPoints, int nParts) {
		double[] data = vectorToArray(controlPoints);
		double[] points = createCubic(data, nParts);
		return arrayToVector(points);
	}
	
	
	private static double[] createCubic(double[] data, int parts) {
	    assert data.length >= 3;
	    if (data.length == 3) { // special case: if only one point given, return that point
		double[] result = new double[3];
		result[0] = data[0];
		result[1] = data[1];
		result[2] = data[2];
		return result;
	    }
		double[] tangents = new double[data.length];
		tangents[0] = .5 * (data[3] - data[0]);
		tangents[1] = .5 * (data[4] - data[1]);
		tangents[2] = .5 * (data[5] - data[2]);
		for(int i = 1; i < data.length / 3 - 1; ++i) {
			tangents[i * 3] = .5 * (data[(i + 1) * 3] - data[(i - 1) * 3]);
			tangents[i * 3 + 1] = .5 * (data[(i + 1) * 3 + 1] - data[(i - 1) * 3 + 1]);
			tangents[i * 3 + 2] = .5 * (data[(i + 1) * 3 + 2] - data[(i - 1) * 3 + 2]);
		}
		
		tangents[data.length - 3] = .5 * (data[data.length - 3] - data[data.length - 6]);
		tangents[data.length - 2] = .5 * (data[data.length - 2] - data[data.length - 5]);
		tangents[data.length - 1] = .5 * (data[data.length - 1] - data[data.length - 4]);
		
		
		double[] points = new double[data.length + (parts - 1) * (data.length - 3)];
		int pc = 0;
		for(int i = 0; i < data.length / 3 - 1; ++i) {
			double t = 1.0 / parts;
			for(int j = 0; j < parts; ++j) {
				double[] p = cubicHermiteInterpolation(data, i * 3, tangents, i * 3, j * t);
				points[pc++] = p[0];
				points[pc++] = p[1];
				points[pc++] = p[2];
			}
		}
		
		points[pc++] = data[data.length - 3];
		points[pc++] = data[data.length - 2];
		points[pc++] = data[data.length - 1];
		
		return points;
	}
	
	private static double[] cubicHermiteInterpolation(double[] points, int offset, double[] tangents, int tOffset, double t) {
		double[] point = new double[3];
		for(int i = 0; i < 3; ++i) {
			point[i] = (2 * Math.pow(t, 3) - 3 * Math.pow(t, 2) + 1) * points[offset + i] +
				(Math.pow(t, 3) - 2 * Math.pow(t, 2) + t) * tangents[tOffset + i] +
				(Math.pow(t, 3) - Math.pow(t, 2)) * tangents[tOffset + 3 + i] + 
				(-2 * Math.pow(t, 3) + 3 * Math.pow(t, 2)) * points[offset + 3 + i];
		}
		
		return point;
	}
	
	
	private static double[] vectorToArray(Vector3D[] controlPoints) {
		double[] array = new double[controlPoints.length * 3];
		int j = 0;
		for(int i = 0; i < controlPoints.length; ++i) {
			array[j++] = controlPoints[i].getX();
			array[j++] = controlPoints[i].getY();
			array[j++] = controlPoints[i].getZ();
		}
		
		return array;
	}
	
	private static Vector3D[] arrayToVector(double[] data) {
		Vector3D[] array = new Vector3D[data.length / 3];
		int j = 0;
		for(int i = 0; i < array.length; ++i) {
			array[i] = new Vector3D(data[j++], data[j++], data[j++]);
		}
		
		return array;
	}
	
	private static double[] createBezier(double[] data, int parts) {
		double tValue = 1.0 / (parts);
		double[] array = new double[parts * 3 + 3];
		for(int i = 0; i < parts + 1; ++i) {
			System.out.println("T: " + tValue * (i + 1));
			double[] point = findBezierPoint(data.clone(), tValue * i, data.length / 3);
			array[i * 3] = point[0];
			array[i * 3 + 1] = point[1];
			array[i * 3 + 2] = point[2];
		}
		
		return array;
	}
	
	private static double[] createTangentBezier(double[] data, int parts) {
		double tValue = 1.0 / (parts + 1);
		double[] array = new double[parts * 3];
		for(int i = 0; i < parts; ++i) {
			double[] point = findTangentBezierPoint(data.clone(), tValue * (i + 1), data.length / 3);
			array[i * 3] = point[0];
			array[i * 3 + 1] = point[1];
			array[i * 3 + 2] = point[2];
		}
		
		return array;
	}
	
	private static double[] findTangentBezierPoint(double[] data, double t, int size) {
		if(size == 0)
			return data;
		
		for(int i = 0; i < size - 1; ++i) {
			data[i * 3] = - data[i * 3] + data[(i + 1) * 3];
			data[i * 3 + 1] = - data[i * 3 + 1] + data[(i + 1) * 3 + 1];
			data[i * 3 + 2] = - data[i * 3 + 2] + data[(i + 1) * 3 + 2];
		}
		
		return findBezierPoint(data, t, size - 1);
	}
	
	public void createBezier(Vector3D[] controlPoints, Vector3D[] vertices, Vector3D[] tangent, int size) {
		double[] data = vectorToArray(controlPoints);
		double tValue = 1.0 / (size + 1);
		for(int i = 0; i < size; ++i) {
			double[] v = data.clone();
			double[] t = data.clone();
			interpolatePoint(v, t, tValue * (i + 1), data.length / 3);
			vertices[i] = new Vector3D(v[0], v[1], v[2]);
			tangent[i] = new Vector3D(t[0], t[1], t[2]);
		}
	}
	
	/**
	 * Interpolates the position and tangent vector from
	 * a set of vertices
	 * @param vertices
	 * @param tangents
	 * @param t
	 * @param size
	 */
	private static void interpolatePoint(double[] data, double[] tangent, double t, int size) {
		if(size == 0)
			return;
		
		for(int i = 0; i < size - 1; ++i) {
			data[i * 3] = (1 - t) * data[i * 3] + t * data[(i + 1) * 3];
			data[i * 3 + 1] = (1 - t) * data[i * 3 + 1] + t * data[(i + 1) * 3 + 1];
			data[i * 3 + 2] = (1 - t) * data[i * 3 + 2] + t * data[(i + 1) * 3 + 2];
			tangent[i * 3] = - tangent[i * 3] + tangent[(i + 1) * 3];
			tangent[i * 3 + 1] = - tangent[i * 3 + 1] + tangent[(i + 1) * 3 + 1];
			tangent[i * 3 + 2] = - tangent[i * 3 + 2] + tangent[(i + 1) * 3 + 2];
		}
		
		interpolatePoint(data, tangent, t, size - 1);
		
		
	}
	
	private static double[] findBezierPoint(double[] data, double t, int size) {
		if(size == 0)
			return data;
		
		for(int i = 0; i < size - 1; ++i) {
			data[i * 3] = (1 - t) * data[i * 3] + t * data[(i + 1) * 3];
			data[i * 3 + 1] = (1 - t) * data[i * 3 + 1] + t * data[(i + 1) * 3 + 1];
			data[i * 3 + 2] = (1 - t) * data[i * 3 + 2] + t * data[(i + 1) * 3 + 2];
		}
		
		return findBezierPoint(data, t, size - 1);
		
	}

}
