package tools3d.symmetry2;

import java.util.*;
import tools3d.*;

public class SymCopies extends ArrayList<CoordinateSystem> {

    /** Constructor is not public; use SymCopySingleton instead to obtain instance */
    SymCopies() { }

    /** Returns position of n'th "mirror" copy */
    public Vector3D getPosition(Vector3D pos, int id) {
	assert ((id >= 0) && (id < size()));
	return get(id).activeTransform(pos);
    }

    public boolean validate() { return size() > 0; }

}
