package tools3d.symmetry2;

/** Singleton pattern for SymCopies "keeper of symmetry copies" */
public class SymCopySingleton {

    private static SymCopies symCopies = new SymCopies();

    private SymCopySingleton() { }    

    public static SymCopies getInstance() { return symCopies; }


}
