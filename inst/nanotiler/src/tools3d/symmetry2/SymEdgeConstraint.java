package tools3d.symmetry2;

public class SymEdgeConstraint {

    public int currId1, currId2, origId1, origId2, symId;

    public SymEdgeConstraint(int _currId1, int _currId2, int _origId1, int _origId2, int _symId) {
	this.currId1 = _currId1;
	this.currId2 = _currId2;
	this.origId1 = _origId1;
	this.origId2 = _origId2;
	this.symId = _symId;
    }

    public String toString() { return "(SymEdgeConstraint " + currId1 + " " + currId2 + " " + origId1 + " " + origId2 + " " + symId + " )"; }

}
