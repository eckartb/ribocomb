package viewer.commands;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;


import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.KeyStroke;

import rnadesign.rnacontrol.Object3DGraphController;
import tools3d.BoundingVolume;
import tools3d.CollisionDetectionTools;
import tools3d.Vector3D;
import tools3d.Vector4D;
import tools3d.objects3d.Object3DSetTools;
import tools3d.objects3d.SimpleObject3DSet;
import viewer.display.Display;
import viewer.display.OrthogonalDisplay;
import viewer.display.PerspectiveDisplay;
import viewer.util.Tools;

/**
 * Points the view towards the object in 3D space
 * @author Calvin
 *
 */
public class FindObject implements DisplayCommand {
	private Object3DGraphController controller;
	
	/**
	 * Construct the command with access to the object
	 * @param controller
	 */
	public FindObject(Object3DGraphController controller) {
		this.controller = controller;
	}

	public JComponent component(final Display d) {
		JButton button = new JButton("Find");
		//button.setMaximumSize(new Dimension(25, 25));
		//button.setPreferredSize(new Dimension(25, 25));
		button.setToolTipText("Find");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				execute(d);
			}
		});
		
		return button;
		
	}

	public String description() {
		return "Centers camera on the entire object";
	}

	public void execute(Display d) {
		
		
		
		
		if(d instanceof PerspectiveDisplay) {
			centerPerspectiveDisplay((PerspectiveDisplay) d);
		}
		else {
			centerOrthogonalDisplay((OrthogonalDisplay) d);
		}
	}

	/**
	 * Center a persepective display on the object
	 * @param d Display to center
	 */
	public void centerPerspectiveDisplay(PerspectiveDisplay d) {
		
		Vector4D direction = d.getView().getViewDirection();
		Vector4D normal = d.getView().getViewDirectionNormal();
		final Vector4D location = d.getView().getViewLocation();
		
		Vector4D cross = direction.cross(normal);
		cross.normalize();
		
		double maxDU, maxDV, minDU, minDV;
		maxDU = maxDV = Double.MAX_VALUE * -1;
		minDU = minDV = Double.MAX_VALUE;
		
		
		// project each point onto the plane spanned by the normal and cross of the view
		BoundingVolume volume = controller.getGraph().getGraph().getBoundingVolume();
		double distance = Double.MAX_VALUE;
		Vector4D closest = null;
		double closestDValue = -1 * location.dot(direction);
		for(Vector4D vertex : volume.getVertices()) {
			double dU = Tools.project(vertex, cross);
			double dV = Tools.project(vertex, normal);
			
			if(dU > maxDU) {
				maxDU = dU;
			}
			
			if(dV > maxDV) {
				maxDV = dV;
			}
			
			if(dU < minDU) {
				minDU = dU;
			}
			
			if(dV < minDV) {
				minDV = dV;
			}
			
			double dist = direction.dot(vertex) + closestDValue;
			if(dist < distance) {
				closest = vertex;
				distance = dist;
			}
		}
		
		double dValue = -1 * direction.dot(closest);
		
		
		Vector4D minPoint = cross.mul(minDU).plus(normal.mul(minDV));
		Vector4D maxPoint = cross.mul(maxDU).plus(normal.mul(maxDV));
		
	
		
		double angle = Math.PI * (d.getAngle() / 2.0) / 180;
		
		// get the view frutrum planes
		final Vector4D tn = Tools.rotate(normal, cross, angle);
		final Vector4D bn = Tools.rotate(normal.mul(-1.), cross, -angle);
		final Vector4D ln = Tools.rotate(cross.mul(-1.), normal, angle);
		final Vector4D rn = Tools.rotate(cross, normal, -angle);
	
		Vector4D ltr = CollisionDetectionTools.intersecton3Planes(ln, minPoint, rn, maxPoint, tn, maxPoint);
		Vector4D trb = CollisionDetectionTools.intersecton3Planes(tn, maxPoint, rn, maxPoint, bn, minPoint);
		Vector4D rbl = CollisionDetectionTools.intersecton3Planes(rn, maxPoint, bn, minPoint, ln, minPoint);
		Vector4D blt = CollisionDetectionTools.intersecton3Planes(bn, minPoint, ln, minPoint, tn, maxPoint);
		
		
		double ltrProj = direction.dot(ltr) + dValue;
		double trbProj = direction.dot(trb) + dValue;
		
		double min = Math.min(ltrProj, trbProj);
		
		double x = (ltr.getX() + trb.getX() + rbl.getX() + blt.getX()) / 4.0;
		double y = (ltr.getY() + trb.getY() + rbl.getY() + blt.getY()) / 4.0;
		
		if(min == ltrProj) {
			d.getView().setViewLocation(x, y, (ltr.getZ() + rbl.getZ()) / 2.0);
		} else {
			d.getView().setViewLocation(x, y, (trb.getZ() + blt.getZ()) / 2.0);
		}
		
		
	}
	
	/**
	 * Center an orthogonal display on an object
	 * @param d Display to center
	 */
	public void centerOrthogonalDisplay(OrthogonalDisplay d) {
		Vector3D center = Object3DSetTools.centerOfMass(new SimpleObject3DSet(controller.getGraph().getGraph()));
		d.getView().lookAt(center.getX(), center.getY(), center.getZ());
		
		double minX, minY, minZ;
		double maxX, maxY, maxZ;
		minX = minY = minZ = Double.MAX_VALUE;
		maxX = maxY = maxZ = -1.0 * Double.MIN_VALUE;
		
		BoundingVolume volume = controller.getGraph().getGraph().getBoundingVolume();
		for(Vector4D v : volume.getVertices()) {
			if(v.getX() < minX)
				minX = v.getX();
			if(v.getX() > maxX) 
				maxX = v.getX();
			if(v.getY() < minY)
				minY = v.getY();
			if(v.getY() > maxY)
				maxY = v.getY();
			if(v.getZ() < minZ)
				minZ = v.getZ();
			if(v.getZ() > maxZ)
				maxZ = v.getZ();
		}
		
		double x = (maxX - minX) / 2.;
		double y = (maxY - minY) / 2.;
		double z = (maxZ - minZ) / 2.;
		
		double max = Math.max(x, Math.max(y, z));
		
		if(Math.abs(max) <= 0.000000001)
			return;
		
		
		double screen = Math.max(d.getSize().width, d.getSize().height);
		
		double zoom = screen / (2 * max);
		
		d.getView().setZoom(zoom);
		
		
		
		
	}

	public KeyStroke keyStroke() {
		return KeyStroke.getKeyStroke(KeyEvent.VK_F, KeyEvent.CTRL_MASK, true);
	}
}
