package viewer.commands;

import java.awt.Dimension;
import java.awt.event.*;

import javax.swing.*;

import rnadesign.rnacontrol.Object3DController;

import tools3d.objects3d.Object3DSetTools;
import tools3d.objects3d.SimpleObject3DSet;
import tools3d.Vector3D;

import viewer.display.Display;
import viewer.view.AimablePerspectiveView;

/**
 * This command centers the view on the center of mass
 * of the object loaded into the display. For perpective
 * displays, the view will rotate around the object
 * when this is toggled on.
 * @author Calvin
 *
 */
public class CenterOnMolecularMass extends ToggableDisplayCommand {
	
	private Object3DController controller;
	
	/**
	 * Construct the command
	 * @param controller Controller that allows access to the 3d object
	 */
	public CenterOnMolecularMass(Object3DController controller) {
		this.controller = controller;
	}

	public JComponent component(final Display d) {
		JButton button = new JButton("Center");
		//button.setMaximumSize(new Dimension(25, 25));
		//button.setPreferredSize(new Dimension(25, 25));
		button.setToolTipText("Center");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				execute(d);
			}
		});
		
		return button;
	}

	public String description() {
		return "Centers view on the center of the loaded object";
	}

	public void executeOn(Display d) {
		Vector3D com = Object3DSetTools.centerOfMass(new SimpleObject3DSet(controller.getGraph()));
		d.getView().lookAt(com.getX(), com.getY(), com.getZ());
		d.repaint();

	}
	
	public void executeOff(Display d) {
		if(d.getView() instanceof AimablePerspectiveView) {
			((AimablePerspectiveView)d.getView()).point(null);
		} else {
			executeOn(d);
		}
	}

	public KeyStroke keyStroke() {
		return KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK, true);
	}

}
