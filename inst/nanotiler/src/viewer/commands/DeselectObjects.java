package viewer.commands;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.KeyStroke;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;


import viewer.display.Display;
import viewer.rnadesign.GraphControllerDisplayManager;

/**
 * Deselects all currently selected objects
 * @author Calvin
 *
 */
public class DeselectObjects implements DisplayCommand {
	
	private GraphControllerDisplayManager manager;
	
	/**
	 * Construct command with access to the selection manager
	 * @param manager Display manager that provides access
	 * to the selection manager.
	 */
	public DeselectObjects(GraphControllerDisplayManager manager) {
		this.manager = manager;
	}

	public JComponent component(final Display d) {
		JButton button = new JButton("Deselect");
		//button.setMaximumSize(new Dimension(25, 25));
		//button.setPreferredSize(new Dimension(25, 25));
		button.setToolTipText("Deselect");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				execute(d);
			}
		});
		
		return button;
	}

	public String description() {
		return "Deselect all selected objects";
	}

	public void execute(Display d) {
		manager.getSelector().deselectAll();
		//manager.display();
	}

	public KeyStroke keyStroke() {
		return KeyStroke.getKeyStroke(KeyEvent.VK_D, KeyEvent.CTRL_MASK, true);
	}

}
