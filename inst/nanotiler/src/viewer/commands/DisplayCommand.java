package viewer.commands;

import javax.swing.*;

import viewer.display.Display;

/**
 * Encapsulates the command, keystroke, and appearence
 * of a command to use with a display
 * @author Calvin
 *
 */
public interface DisplayCommand {
	
	/**
	 * Put all execution code here. The display the 
	 * command is triggered on is passed in for use
	 * of the command
	 * @param d Display the command was triggered on
	 * (ie, the display where the hot keys were pressed)
	 */
	public void execute(Display d);
	
	/**
	 * Key stroke that triggers this command
	 * @return Returns keystroke that triggers the command
	 */
	public KeyStroke keyStroke();
	
	/**
	 * GUI element that triggers the command.
	 * @param d Display this particular component will
	 * be attached to if the command operates on a specific
	 * display. If the command doesn't operate on a specific
	 * display, just ignore the parameter.
	 * @return Returns a component that can be added to a GUI.
	 */
	public JComponent component(Display d);
	
	/**
	 * Get a textual description of the command.
	 * @return Returns string describing command
	 */
	public String description();
	
}
