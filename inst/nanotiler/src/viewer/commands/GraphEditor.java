package viewer.commands;

import javax.media.opengl.GL;

import java.awt.event.*;
import java.awt.*;

import java.text.DecimalFormat;

import javax.swing.JComponent;
import javax.swing.KeyStroke;
import javax.swing.WindowConstants;

import viewer.deprecated.DefaultRNAModelRenderer;
import viewer.display.Display;
import viewer.event.*;
import viewer.rnadesign.GraphControllerDisplayManager;
import viewer.graphics.*;
import viewer.rnadesign.LinkRenderer;
import viewer.util.Tools;

import rnadesign.rnacontrol.Object3DGraphController;

import tools3d.objects3d.*;
import tools3d.*;

import javax.swing.*;

/**
 * Allow the user to dynamically edit a graph
 * @author Calvin
 *
 */
public class GraphEditor extends ToggableDisplayCommand implements RenderableModel,
	DisplayViewChangedListener, DisplayLayoutListener {
	
	private Object3DGraphController controller;
	private GraphControllerDisplayManager manager;
	private LinkSet links;
	
	private Object3D root;
	
	public enum Mode {
		NodeAdd, LinkAdd, LinkRemove, NodeRemove, NodeManipulate;
	}
	
	private int nodeCounter = 0;
	private int linkCounter = 0;
	
	private Listener listener = new Listener();
	
	private Object3D selected1, selected2;
	
	private ModePanel panel;
	
	private Mode currentMode = Mode.NodeAdd;
	
	/**
	 * Construct the graph editor command
	 * @param controller Controller to update with the new graph
	 * @param manager Display to update as the graph is created
	 */
	public GraphEditor(Object3DGraphController controller, GraphControllerDisplayManager manager) {
		this.controller = controller;
		this.manager = manager;
		
		root = new SimpleObject3D();
		links = new SimpleLinkSet();
		
	}

	@Override
	public void executeOff(Display d) {
		manager.setObjectSelectionEnabled(true);
		if(root.size() > 0) {
			controller.getGraph().addGraph(root);
			controller.getLinks().addLinks(links);
		}
		
		root = new SimpleObject3D();
		panel.dispose();
		
		manager.getDisplay().removeRenderable(this);
		
		nodeCounter = linkCounter = 0;
		
		manager.removeDisplayLayoutListener(this);
		manager.getDisplay().removeDisplayViewChangedListener(this);
		manager.setViewTranslationEnabled(true);
		
		detachListener();

	}

	@Override
	public void executeOn(Display d) {
		manager.setObjectSelectionEnabled(false);
		
		panel = new ModePanel();
		panel.setVisible(true);
		
		manager.getDisplay().addRenderable(this);
		
		manager.addDisplayLayoutListener(this);
		manager.getDisplay().addDisplayViewChangedListener(this);
		
		links = new SimpleLinkSet();
		
		attachListener();
		

	}
	
	public void executeOn(Display d, Object3D root, LinkSet links) {
		manager.setObjectSelectionEnabled(false);
		
		panel = new ModePanel();
		panel.setVisible(true);
		
		manager.getDisplay().addRenderable(this);
		
		manager.addDisplayLayoutListener(this);
		manager.getDisplay().addDisplayViewChangedListener(this);
	}
	
	private void attachListener() {
		for(Display d : manager.getDisplay().getDisplays()) {
			d.addMouseListener(listener);
			d.addMouseMotionListener(listener);
		}
	}
	
	private void detachListener() {
		for(Display d : manager.getDisplay().getDisplays()) {
			d.removeMouseListener(listener);
			d.removeMouseMotionListener(listener);
		}
	}
	
	public void execute(Display d) {
		if(isOn() && root.size() > 0) {
			int result = JOptionPane.showConfirmDialog(manager, "Graph has been manipulated. Commit graph?");
			if(result == JOptionPane.CANCEL_OPTION)
				return;
			else if(result == JOptionPane.NO_OPTION) {
				links = new SimpleLinkSet();
				root = new SimpleObject3D();
			}
		}
			
		super.execute(d);
	}

	public JComponent component(final Display d) {
		JButton button = new JButton("Graph Editor");
		//button.setMaximumSize(new Dimension(25, 25));
		//button.setPreferredSize(new Dimension(25, 25));
		button.setToolTipText("Graph Editor");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				execute(d);
			}
		});
		
		return button;
	}

	public String description() {
		return "Toggles graph editor mode";
	}

	public KeyStroke keyStroke() {
		return KeyStroke.getKeyStroke(KeyEvent.VK_E, KeyEvent.CTRL_MASK, true);
	}
	
	private class Listener extends MouseAdapter implements MouseMotionListener {
		Vector4D p = null;
		
		public void mousePressed(MouseEvent e) {
			if(currentMode == Mode.NodeManipulate)
				p = ((Display)e.getSource()).getCoordinates(e.getPoint());
		}
		
		public void mouseReleased(MouseEvent e) {
			p = null;
		}
		
		public void mouseMoved(MouseEvent e) {
			
		}
		
		public void mouseDragged(MouseEvent e) {
			if(currentMode != Mode.NodeManipulate || selected1 == null)
				return;
			
			Vector4D to = ((Display)e.getSource()).getCoordinates(e.getPoint());
			Vector4D diff = to.minus(p);
			
			Vector3D offset = new Vector3D(diff);
			selected1.setPosition(selected1.getPosition().plus(offset));
			
			p = to;
			
			manager.getDisplay().repaint();
		}
		
		
		public void mouseClicked(MouseEvent e) {
			Display d = (Display) e.getSource();
			
			Vector4D origin = new Vector4D(0.0, 0.0, 0.0, 1.0);
			Vector4D ray = new Vector4D(0.0, 0.0, 0.0, 0.0);
			Tools.screenTo3DSpace(e.getPoint(), d, ray, origin);
			Object3D found = manager.getSelector().findSelected(ray, origin, root);
			if(found != null && found != root) {
				if(selected1 == null) {
					selected1 = found;
					selected1.setSelected(true);
				}
				else if(selected1 == found) {
					selected1.setSelected(false);
					selected1 = null;
				}
				else if(selected2 == null && currentMode != Mode.NodeManipulate)
					selected2 = found;
				else if(currentMode != Mode.NodeManipulate) {
					selected1.setSelected(false);
					selected2.setSelected(false);
					selected1 = found;
					selected2 = null;
				}
				
				
			}
			
			if(currentMode == Mode.NodeAdd && (found == null || found == root)) {
				
				final Vector3D position = new Vector3D(d.getCoordinates(e.getPoint()));
				if(e.getButton() != MouseEvent.BUTTON1) {
					final JDialog dialog = new JDialog((Frame) null, "Coordinate Manipulator", true);
					JPanel panel = new JPanel();
					panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
					
					JPanel temp = new JPanel();
					temp.add(new JLabel("X"));
					final JTextField xfield = new JTextField(5);
					DecimalFormat fmt = new DecimalFormat("###.000");
					xfield.setText(fmt.format(position.getX()));
					temp.add(xfield);
					panel.add(temp);
					temp = new JPanel();
					temp.add(new JLabel("Y"));
					final JTextField yfield = new JTextField(5);
					yfield.setText(fmt.format(position.getY()));
					temp.add(yfield);
					panel.add(temp);
					temp = new JPanel();
					temp.add(new JLabel("Z"));
					final JTextField zfield = new JTextField(5);
					zfield.setText(fmt.format(position.getZ()));
					temp.add(zfield);
					panel.add(temp);
					temp = new JPanel();
					JButton submit = new JButton("Submit");
					submit.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							try {
								position.setX(Double.parseDouble(xfield.getText()));
								position.setY(Double.parseDouble(yfield.getText()));
								position.setZ(Double.parseDouble(zfield.getText()));
								dialog.dispose();
							} catch(NumberFormatException ex) {
								JOptionPane.showMessageDialog(dialog, "Please enter only numbers");
							}
						}
					});
					temp.add(submit);
					panel.add(temp);
					dialog.setLocationRelativeTo(manager);
					dialog.add(panel);
					dialog.pack();
					dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
					dialog.setVisible(true);
					
				}
				
				Object3D o = new SimpleObject3D(position);
				o.setName("n" + nodeCounter++);
				
				root.insertChild(o);
				
			}
			
			else if(currentMode == Mode.NodeRemove && found != null && found != root) {
				root.removeChild(found);
				links.removeBadLinks(root);
				selected1 = null;
			}
			
			else if(selected1 != null && selected2 != null) {
				Link l = links.find(selected1, selected2);
				if(currentMode == Mode.LinkAdd && l == null) {
					Link link = new SimpleLink(selected1, selected2);
					link.setName("l" + linkCounter++);
					links.add(link);
					selected1.setSelected(false);
					selected2.setSelected(false);
					selected1 = selected2 = null;
				}
				else if(currentMode == Mode.LinkRemove && l != null) {
					links.remove(l);
					selected1.setSelected(false);
					selected2.setSelected(false);
					selected1 = selected2 = null;
				}
			}
			
			manager.display();
		}
		
		
	}
	
	private void clearSelection() {
		if(selected1 != null) {
			selected1.setSelected(false);
			selected1 = null;
		}
		
		if(selected2 != null) {
			selected2.setSelected(false);
			selected2 = null;
		}
	}
	
	private class ModePanel extends JDialog {
		private ButtonGroup group;
		private JRadioButton nodeAdd, nodeRemove, linkAdd, linkRemove, nodeManipulate;
		
		public ModePanel() {
			super((Frame) null, "Mode Select", false);
			setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			
			JPanel panel = new JPanel();
			panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
			group = new ButtonGroup();
			nodeAdd = new JRadioButton("Add Graph Node");
			nodeAdd.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					currentMode = Mode.NodeAdd;
					manager.setViewTranslationEnabled(true);
					clearSelection();
				}
			});
			group.add(nodeAdd);
			group.setSelected(nodeAdd.getModel(), true);
			panel.add(nodeAdd);
			
			nodeManipulate = new JRadioButton("Manipulate Graph Node");
			nodeManipulate.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					currentMode = Mode.NodeManipulate;
					manager.setViewTranslationEnabled(false);
					clearSelection();
				}
			});
			group.add(nodeManipulate);
			panel.add(nodeManipulate);
			
			nodeRemove = new JRadioButton("Remove Graph Node");
			nodeRemove.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					currentMode = Mode.NodeRemove;
					manager.setViewTranslationEnabled(true);
					clearSelection();
				}
			});
			group.add(nodeRemove);
			panel.add(nodeRemove);
			linkAdd = new JRadioButton("Add Link");
			linkAdd.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					currentMode = Mode.LinkAdd;
					manager.setViewTranslationEnabled(true);
					clearSelection();
				}
			});
			group.add(linkAdd);
			panel.add(linkAdd);
			linkRemove = new JRadioButton("Remove Link");
			linkRemove.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					currentMode = Mode.LinkRemove;
					manager.setViewTranslationEnabled(true);
					clearSelection();
				}
			});
			group.add(linkRemove);
			panel.add(linkRemove);

			JLabel label = new JLabel("Select the graph editor button again to close this window");
			panel.add(label);
			
			addWindowListener(new WindowAdapter() {

				

				@Override
				public void windowClosing(WindowEvent e) {
					recreate();
				}
				
				private void recreate() {
					GraphEditor.this.panel = new ModePanel();
					GraphEditor.this.panel.setVisible(true);
					
					GraphEditor.this.panel.setSelected(currentMode);
					
				}
				
			});
			
			add(panel);
			pack();
			
			
		}
		
		public void setSelected(Mode mode) {
			switch(mode) {
			case NodeAdd:
				group.setSelected(nodeAdd.getModel(), true);
				return;
			case NodeRemove:
				group.setSelected(nodeRemove.getModel(), true);
				return;
			case LinkAdd:
				group.setSelected(linkAdd.getModel(), true);
				return;
			case LinkRemove:
				group.setSelected(linkRemove.getModel(), true);
				return;
			case NodeManipulate:
				group.setSelected(nodeManipulate.getModel(), true);
				return;
			}
		}
	}
	
	public void render(GL gl) {
		Material material = new Material(Color.white);
		Material.renderMaterial(gl, material);
		
		DefaultRNAModelRenderer renderer = new DefaultRNAModelRenderer();
		
		for(int i = 0; i < root.size(); ++i) {
			Object3D o = root.getChild(i);
			Material.renderMaterial(gl, material);
			renderer.renderSphere(o, gl);
			if(o.isSelected())
				renderer.renderSelected(o, gl);
		}
		
		LinkRenderer linkRenderer = manager.getLinkRenderer();
		
		for(int i = 0; i < links.size(); ++i) {
			linkRenderer.renderLink(links.get(i), gl);
		}
		
	}

	public void displayLayoutChanged(DisplayEvent e) {
		attachListener();
		manager.getDisplay().addDisplayViewChangedListener(this);
	}

	public void displayChanged(DisplayEvent e) {
		if(e.getId() == DisplayEvent.VIEW_ADDED) {
			Display d = e.getDisplay();
			d.addMouseListener(listener);
		}
		
	}
	
	

}
