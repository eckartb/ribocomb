package viewer.commands;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.KeyStroke;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.InputEvent;

import viewer.display.*;

/**
 * This command resets the view camera to its original (starting)
 * position in 3D space
 * @author Calvin
 *
 */
public class ResetCamera implements DisplayCommand {

	public JComponent component(final Display d) {
		JButton button = new JButton("Reset");
		//button.setMaximumSize(new Dimension(25, 25));
		//button.setPreferredSize(new Dimension(25, 25));
		button.setToolTipText("Reset");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				execute(d);
			}
		});
		
		return button;
	}

	public String description() {
		return "Resets the view";
	}

	public void execute(Display d) {
		d.getView().reset();
		d.display();
		d.display(); // second call to make orthogonal displays refresh properly
	}

	public KeyStroke keyStroke() {
		return KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_MASK, true);
	}

}
