package viewer.commands;

import viewer.display.Display;

/**
 * Allows commands to be toggled on and off
 * @author Calvin
 *
 */
public abstract class ToggableDisplayCommand implements DisplayCommand {

	private boolean off = true;
	
	/**
	 * This method will be called when the command
	 * is toggled on.
	 * @param d Display the command was invoked on.
	 */
	public abstract void executeOn(Display d);
	
	/**
	 * This method will be called when the command
	 * is toggled off.
	 * @param d Display the command was invoked on.
	 */
	public abstract void executeOff(Display d);
	
	public void execute(Display d) {
		if(off) {
			executeOn(d);
			off = false;
		}
		else {
			executeOff(d);
			off = true;
		}
	}
	
	/**
	 * Get the command mode.
	 * @return Returns true if the command is on, ie, the last
	 * method called was executeOn or returns false if the command
	 * is off, ie, the last method called was executeOff
	 */
	public boolean isOn() {
		return !off;
	}
}
