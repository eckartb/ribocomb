package viewer.commands;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.KeyEvent;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.nio.DoubleBuffer;
import java.nio.IntBuffer;

import javax.media.opengl.GL;
import javax.media.opengl.GLContext;
import javax.media.opengl.glu.GLU;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.KeyStroke;

import tools3d.Vector4D;
import tools3d.Vector3D;
import tools3d.objects3d.Object3D;
import viewer.graphics.Gizmo;
import viewer.graphics.Material;
import viewer.rnadesign.GraphControllerDisplayManager;
import viewer.util.Tools;
import viewer.view.OrthogonalView;
import viewer.view.PerspectiveView;
import viewer.view.XYView;
import viewer.view.XZView;
import viewer.graphics.ObjectTranslationManipulator;
import viewer.display.*;
import viewer.event.*;
import viewer.graph.RenderableGraph;

public class TranslateObjects extends ToggableDisplayCommand implements SelectionListener, 
		DisplayViewChangedListener, DisplayLayoutListener {
	
	private GraphControllerDisplayManager manager;
	
	
	private Vector4D position;
	
	private static final Material X_MATERIAL = new Material(Color.green);
	private static final Material Y_MATERIAL = new Material(Color.red);
	private static final Material Z_MATERIAL = new Material(Color.blue);
	private static final Material S_MATERIAL = new Material(Color.yellow);
	
	public TranslateObjects(GraphControllerDisplayManager manager) {
		this.manager = manager;
		
	}

	@Override
	public void executeOff(Display d) {
		manager.setViewTranslationEnabled(true);
		manager.getSelector().setMaxNumberSimultaneousSelections(Integer.MAX_VALUE);
		manager.getSelector().removeSelectionListener(this);
		manager.removeDisplayLayoutListener(this);
		manager.getDisplay().removeDisplayViewChangedListener(this);
		for(Display dd : manager.getDisplay().getDisplays()) {
			dd.setGizmo(null);
			dd.setGizmoEnabled(false);
		}
		
		position = null;
		manager.repaint();
	}

	@Override
	public void executeOn(Display d) {
		manager.setViewTranslationEnabled(false);
		manager.getSelector().setMaxNumberSimultaneousSelections(1);
		manager.getSelector().addSelectionListener(this);
		manager.addDisplayLayoutListener(this);
		manager.getDisplay().addDisplayViewChangedListener(this);
		manager.getSelector().deselectAll();
		manager.repaint();
		
	}
	
	
	
	public void displayLayoutChanged(DisplayEvent e) {
		if(position == null)
			return;
		
		manager.setViewTranslationEnabled(false);
		
		for(Display dd : manager.getDisplay().getDisplays()) {
			attachGizmo(dd);
			dd.setGizmoEnabled(true);
		}
		
	}

	public void displayChanged(DisplayEvent e) {
		if(e.getId() == DisplayEvent.VIEW_ADDED) {
			attachGizmo(e.getDisplay());
			e.getDisplay().setMouseTranslationEnabled(false);
		}
		
		
	}
	
	private void attachGizmo(Display d) {
		if(position == null)
			return;
		
		if(d.getView() instanceof PerspectiveView)
			d.setGizmo(new PerspectiveGizmo(position));
		else
			d.setGizmo(new OrthogonalGizmo(position));
		
		d.setGizmoEnabled(true);
	}
	

	public void objectSelected(SelectionEvent e) {
		
		position = new Vector4D(e.getSelectedObject().getPosition());
		position.setW(1.0);
		
		for(Display dd : manager.getDisplay().getDisplays()) {
			attachGizmo(dd);
			dd.setGizmoEnabled(true);
		}
		
	}
	
	public void objectDeselected(SelectionEvent e) {

		position = null;
		
		for(Display dd : manager.getDisplay().getDisplays()) {
			dd.setGizmo(null);
			dd.setGizmoEnabled(false);
			dd.repaint();
		}
		
	}


	public JComponent component(final Display d) {
		JButton button = new JButton("Translate");
		//button.setMaximumSize(new Dimension(25, 25));
		//button.setPreferredSize(new Dimension(25, 25));
		button.setToolTipText("Translate");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				execute(d);
			}
		});
		
		return button;
	}

	public String description() {
		return "Translate Object Mode";
	}

	public KeyStroke keyStroke() {
		return KeyStroke.getKeyStroke(KeyEvent.VK_T, KeyEvent.CTRL_MASK, true);
	}
	
	private class OrthogonalGizmo extends ObjectTranslationManipulator 
		implements Gizmo, MouseListener, MouseMotionListener {
		
		
		private Vector4D p;
		
		private Vector4D uTranslation; // mouse moved right
		private Vector4D vTranslation; // mouse moved up
		
		private boolean intersectsU = false;
		private boolean intersectsV = false;
		
		private int allowedAxisIntersectionMask = 0;
		private int uMask = 0;
		private int vMask = 0;
		
		private Display d;
		
		public OrthogonalGizmo(Vector4D position) {
			super(position);
			
			setXAxisMaterial(X_MATERIAL);
			setYAxisMaterial(Y_MATERIAL);
			setZAxisMaterial(Z_MATERIAL);
		}

		public MouseListener getMouseListener() {
			return this;
		}

		public MouseMotionListener getMouseMotionListener() {
			return this;
		}
		
		
		public void mouseDragged(MouseEvent e) {
			Vector4D np = new Vector4D();
			Tools.screenTo3DSpace(e.getPoint(), d, null, np);
			Vector4D diff = np.minus(p);
			
			double u = diff.dot(uTranslation);
			double v = diff.dot(vTranslation);
			
			Vector3D uT = new Vector3D(uTranslation);
			uT = uT.mul(u);
			Vector3D vT = new Vector3D(vTranslation);
			vT = vT.mul(v);
			
			if(intersectsU) {
				manager.getSelector().getSelected()[0].translate(uT);
				setPosition(getPosition().plus(uTranslation.mul(u)));
			}
			
			else if(intersectsV) {
				manager.getSelector().getSelected()[0].translate(vT);
				setPosition(getPosition().plus(vTranslation.mul(v)));
			}
			
			else {
				manager.getSelector().getSelected()[0].translate(uT.plus(vT));
				setPosition(getPosition().plus(uTranslation.mul(u)));
				setPosition(getPosition().plus(vTranslation.mul(v)));
			}
			
			manager.repaint();
			
			p = np;
			
		}

		public void mouseMoved(MouseEvent e) {
			Vector4D ray = new Vector4D();
			Vector4D origin = new Vector4D();
			Tools.screenTo3DSpace(e.getPoint(), d, ray, origin);
			
			
			int mask = getIntersectionAxis(ray, origin) & allowedAxisIntersectionMask;
			if(mask == 0 || mask == allowedAxisIntersectionMask) {
				setXAxisMaterial(X_MATERIAL);
				setYAxisMaterial(Y_MATERIAL);
				setZAxisMaterial(Z_MATERIAL);
				d.repaint();
				return;
			}
			
			if((mask & X_AXIS) == X_AXIS)
				setXAxisMaterial(S_MATERIAL);
			else
				setXAxisMaterial(X_MATERIAL);
			
			if((mask & Y_AXIS) == Y_AXIS)
				setYAxisMaterial(S_MATERIAL);
			else
				setYAxisMaterial(Y_MATERIAL);
			
			if((mask & Z_AXIS) == Z_AXIS)
				setZAxisMaterial(S_MATERIAL);
			else
				setZAxisMaterial(Z_MATERIAL);
			
			d.repaint();
		}

		public void mouseClicked(MouseEvent e) { }
		public void mouseEntered(MouseEvent e) { }
		public void mouseExited(MouseEvent e) {	}
		
		public void mousePressed(MouseEvent e) {
			p = new Vector4D();
			
			Vector4D ray = new Vector4D();
			Tools.screenTo3DSpace(e.getPoint(), d, ray, p);
			
			int mask = getIntersectionAxis(ray, p) & allowedAxisIntersectionMask;
			if((mask & uMask) == uMask)
				intersectsU = true;
			else if((mask & vMask) == vMask)
				intersectsV = true;
		}

		public void mouseReleased(MouseEvent e) {
			p = null;
			intersectsU = false;
			intersectsV = false;
			
		}

		public void setDisplay(Display d) {
			this.d = d;
			
			OrthogonalView view = d.getView();
			
			setXzPlaneEnabled(false);
			setXyPlaneEnabled(false);
			setYzPlaneEnabled(false);
			
			if(view instanceof XYView) {
				setZAxisEnabled(false);
				uTranslation = new Vector4D(view.getViewDirection().getZ() < 0.0 ? 1.0 : -1.0, 0.0, 0.0, 0.0);
				vTranslation = new Vector4D(0.0, view.getViewDirectionNormal().getY(), 0.0);
				uMask = X_AXIS;
				vMask = Y_AXIS;
			}
			
			else if(view instanceof XZView) {
				setYAxisEnabled(false);
				uTranslation = new Vector4D(view.getViewDirection().getY() < 0.0 ? 1.0 : -1.0, 0.0, 0.0, 0.0);
				vTranslation = new Vector4D(0.0, 0.0, view.getViewDirectionNormal().getZ(), 0.0);
				uMask = X_AXIS;
				vMask = Z_AXIS;
			}
			
			else {
				setXAxisEnabled(false);
				uTranslation = new Vector4D(0.0, 0.0, view.getViewDirection().getY() < 0.0 ? -1.0 : 1.0, 0.0);
				vTranslation = new Vector4D(0.0, view.getViewDirectionNormal().getY(), 0.0);
				uMask = Z_AXIS;
				vMask = Y_AXIS;
			}
			
			allowedAxisIntersectionMask = uMask | vMask;
			
		}
		
	}
	
	private class PerspectiveGizmo extends ObjectTranslationManipulator 
		implements Gizmo, MouseListener, MouseMotionListener {
		
		private Vector4D p;
		
		private Display d;
		
		private boolean onAxis = false;
		private boolean onPlane = false;
		
		private Vector4D u, v;
		
		public PerspectiveGizmo(Vector4D position) {
			super(position);
			
			setXAxisMaterial(X_MATERIAL);
			setYAxisMaterial(Y_MATERIAL);
			setZAxisMaterial(Z_MATERIAL);
			setXyPlaneMaterial(X_MATERIAL);
			setXzPlaneMaterial(Z_MATERIAL);
			setYzPlaneMaterial(Y_MATERIAL);
		}

		public MouseListener getMouseListener() {
			return this;
		}

		public MouseMotionListener getMouseMotionListener() {
			return this;
		}

		public void setDisplay(Display d) {
			this.d = d;
		}

		/* Not used */
		public void mouseClicked(MouseEvent e) { }
		public void mouseEntered(MouseEvent e) { }
		public void mouseExited(MouseEvent e) {	}

		public void mousePressed(MouseEvent e) {
			Vector4D ray = new Vector4D();
			p = new Vector4D();
			Tools.screenTo3DSpace(e.getPoint(), d, ray, p);
			
			int axisMask = this.getIntersectionAxis(ray, p);
			int planeMask = this.getIntersectionPlane(ray, p);
			
			System.out.println("Plane Mask: " + planeMask);
			
			if(axisMask == X_AXIS) {
				onAxis = true;
				u = Tools.X_AXIS;
			}
			else if(axisMask == Y_AXIS) {
				onAxis = true;
				u = Tools.Y_AXIS;
			}
			else if(axisMask == Z_AXIS) {
				onAxis = true;
				u = Tools.Z_AXIS;
			}
			
			if(onAxis) return;
			
			if(planeMask == XY_PLANE) {
				onPlane = true;
				u = Tools.X_AXIS;
				v = Tools.Y_AXIS;
			}
			else if(planeMask == XZ_PLANE) {
				onPlane = true;
				u = Tools.X_AXIS;
				v = Tools.Z_AXIS;
			}
			else if(planeMask == YZ_PLANE) {
				onPlane = true;
				u = Tools.Y_AXIS;
				v = Tools.Z_AXIS;
			}
			
		}

		public void mouseReleased(MouseEvent e) {
			p = null;
			onAxis = onPlane = false;
			u = v = null;
		}

		public void mouseDragged(MouseEvent e) {
			Vector4D np = new Vector4D();
			Tools.screenTo3DSpace(e.getPoint(), d, null, np);
			double x = (np.getX() - p.getX()) * 2.0;
			double y = (- np.getY() + p.getY()) * 2.0;
			
			if(onAxis) {
				Vector3D translation = new Vector3D(u);
				translation = translation.mul(x);
				manager.getSelector().getSelected()[0].translate(translation);
				RenderableGraph graph = manager.getRenderer().getGraph(manager.getSelector().getSelected()[0]);
				graph.update();
				setPosition(getPosition().plus(u.mul(x)));
			}
			
			else if(onPlane) {
				Vector4D t = u.mul(x).plus(v.mul(y));
				Vector3D translation = new Vector3D(t);
				manager.getSelector().getSelected()[0].translate(translation);
				RenderableGraph graph = manager.getRenderer().getGraph(manager.getSelector().getSelected()[0]);
				graph.update();
				setPosition(getPosition().plus(t));
				
			}
			p = np;
			manager.repaint();
			
		}

		public void mouseMoved(MouseEvent e) {
			Vector4D ray = new Vector4D();
			Vector4D origin = new Vector4D();
			Tools.screenTo3DSpace(e.getPoint(), d, ray, origin);
			
			if(!this.getBoundingVolume().intersectRay(ray, origin)) {
				setXAxisMaterial(X_MATERIAL);
				setYAxisMaterial(Y_MATERIAL);
				setZAxisMaterial(Z_MATERIAL);
				setXyPlaneMaterial(X_MATERIAL);
				setXzPlaneMaterial(Z_MATERIAL);
				setYzPlaneMaterial(Y_MATERIAL);
				manager.repaint();
				return;
			}
			
			int axisMask = this.getIntersectionAxis(ray, origin);
			int planeMask = this.getIntersectionPlane(ray, origin);
			
			System.out.println("Axis Mask: " + axisMask);
			System.out.println("Plane Mask: " + planeMask);
			
			boolean onAxis = false;
			
			if(axisMask == X_AXIS) {
				setXAxisMaterial(S_MATERIAL);
				setYAxisMaterial(Y_MATERIAL);
				setZAxisMaterial(Z_MATERIAL);
				onAxis = true;
			}
			
			else if(axisMask == Y_AXIS) {
				setXAxisMaterial(X_MATERIAL);
				setYAxisMaterial(S_MATERIAL);
				setZAxisMaterial(Z_MATERIAL);
				onAxis = true;
				
			}
			
			else if(axisMask == Z_AXIS) {
				setXAxisMaterial(X_MATERIAL);
				setYAxisMaterial(Y_MATERIAL);
				setZAxisMaterial(S_MATERIAL);
				onAxis = true;
			}
			
			else {
				setXAxisMaterial(X_MATERIAL);
				setYAxisMaterial(Y_MATERIAL);
				setZAxisMaterial(Z_MATERIAL);
			}
			
			if(onAxis) {
				setXyPlaneMaterial(X_MATERIAL);
				setXzPlaneMaterial(Z_MATERIAL);
				setYzPlaneMaterial(Y_MATERIAL);
				manager.repaint();
				return;
			}
			
			if(planeMask == XY_PLANE) {
				setXyPlaneMaterial(S_MATERIAL);
				setXzPlaneMaterial(Z_MATERIAL);
				setYzPlaneMaterial(Y_MATERIAL);
			}
			
			else if(planeMask == XZ_PLANE) {
				setXyPlaneMaterial(X_MATERIAL);
				setXzPlaneMaterial(S_MATERIAL);
				setYzPlaneMaterial(Y_MATERIAL);
			}
			
			else if(planeMask == YZ_PLANE) {
				setXyPlaneMaterial(X_MATERIAL);
				setXzPlaneMaterial(Z_MATERIAL);
				setYzPlaneMaterial(S_MATERIAL);
			}
			else {
				setXyPlaneMaterial(X_MATERIAL);
				setXzPlaneMaterial(Z_MATERIAL);
				setYzPlaneMaterial(Y_MATERIAL);
			}
			
			manager.repaint();
			
		}
		
		
		
	}

}
