package viewer.components;

import javax.swing.*;


import java.awt.*;
import java.awt.event.*;

import viewer.util.KeyBindings;
import viewer.view.OrthogonalView;
import viewer.view.PerspectiveView;
import viewer.view.ViewType;

import javax.swing.ImageIcon;

/**
 * The view manipulator is a set of controls to manipulate a view.
 * @author grunewac
 *
 */
public class ViewManipulator extends JPanel {
	
	private ViewType viewType;
	private OrthogonalView view;
	
	private JButton translate;
	private JButton zoom;
	private JButton rotate;
	private JCheckBox absolute;
	
        private static final String NANOTILER_HOME = System.getenv("NANOTILER_HOME");
	private static final ImageIcon ROTATE_ICON = new ImageIcon(NANOTILER_HOME+"/resources/rotate.gif");
	private static final ImageIcon ZOOM_ICON = new ImageIcon(NANOTILER_HOME+"/resources/zoom.gif");
	private static final ImageIcon TRANSLATE_ICON = new ImageIcon(NANOTILER_HOME+"/resources/translate.gif");
	
	private ZoomListener zoomListener;
	private RotateListener rotateListener;
	private TranslateListener translateListener;
	
	private double rotationSensitivity = 0.1;
	
	private KeyBindings bindings = KeyBindings.getKeyBindings();
	
	
	private final static Dimension BUTTON_DIM = new Dimension(18, 18);
	
	public ViewType getViewType() {
		return viewType;
	}
	
	public void setViewType(ViewType viewType) {
		this.viewType = viewType;
	}
	
	
	public boolean isAbsolute() {
		return absolute.isSelected();
	}
	
	public void setAbsolute(boolean b) {
		absolute.setSelected(b);
	}
	
	public ViewManipulator(PerspectiveView view) {
		this.view = view;
		viewType = ViewType.Perspective;
		
		createGUI();
		handleViewChange();
	}
	
	public ViewManipulator(OrthogonalView view) {
		this.view = view;
		viewType = ViewType.Orthogonal;
		
		createGUI();
		handleViewChange();
	}
	
	public void setView(OrthogonalView view) {
		this.view = view;
		viewType = ViewType.Orthogonal;
		
		handleViewChange();
	}
	
	public void setView(PerspectiveView view) {
		this.view = view;
		viewType = ViewType.Perspective;
		
		handleViewChange();
	}
	
	private void handleViewChange() {
		if(viewType == ViewType.Orthogonal) {
			absolute.setSelected(true);
			absolute.setEnabled(false);
			rotate.setEnabled(false);
		}
		
		else {
			absolute.setEnabled(true);
			rotate.setEnabled(true);
			absolute.setSelected(false);
		}
	}
	
	private void createGUI() {
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		
		// create buttons
		translate = new JButton(TRANSLATE_ICON);
		zoom = new JButton(ZOOM_ICON);
		rotate = new JButton(ROTATE_ICON);
		absolute = new JCheckBox("Absolute");
		
		translate.setToolTipText("Translate");
		zoom.setToolTipText("Zoom");
		rotate.setToolTipText("Rotate");
		
		translate.setPreferredSize(BUTTON_DIM);
		translate.setMinimumSize(BUTTON_DIM);
		translate.setMaximumSize(BUTTON_DIM);
		zoom.setPreferredSize(BUTTON_DIM);
		zoom.setMinimumSize(BUTTON_DIM);
		zoom.setMaximumSize(BUTTON_DIM);
		rotate.setPreferredSize(BUTTON_DIM);
		rotate.setMinimumSize(BUTTON_DIM);
		rotate.setMaximumSize(BUTTON_DIM);
		
		
		rotateListener = new RotateListener();
		zoomListener = new ZoomListener();
		translateListener = new TranslateListener();
		
		translate.addActionListener(translateListener);
		zoom.addActionListener(zoomListener);
		rotate.addActionListener(rotateListener);
		
		translate.addMouseMotionListener(translateListener);
		zoom.addMouseMotionListener(zoomListener);
		rotate.addMouseMotionListener(rotateListener);
		
		translate.addMouseListener(translateListener);
		zoom.addMouseListener(zoomListener);
		rotate.addMouseListener(rotateListener);
		
		
		add(translate);
		add(Box.createHorizontalStrut(3));
		add(rotate);
		add(Box.createHorizontalStrut(3));
		add(zoom);
		add(Box.createHorizontalStrut(3));
		add(absolute);
	}
	
	public boolean isTransformationAbsolute() {
		return absolute.isSelected();
	}
	
	public void setTransformationAbsolute(boolean b) {
		absolute.setSelected(b);
	}
	
	public class RotateListener extends MouseAdapter implements ActionListener, MouseMotionListener {
		
		private Point p = null;
		
		public void actionPerformed(ActionEvent e) {
			
			
		}
		
		public void mouseDragged(MouseEvent e) {

			
			if(p == null) {
				p = e.getPoint();
				return;
			}
			
			PerspectiveView v = (PerspectiveView) view;
			
			int onmask = bindings.getViewManipulationBindings();
			if(((e.getModifiersEx() & onmask) == bindings.getRotateXBinding() && e.getSource() != rotate) || e.getSource() == rotate) {
				double up = e.getY() - p.getY();

				
				if(isAbsolute()) {
					v.rotateWorldViewX(up * rotationSensitivity);
				}
				else {
					v.rotateLocalViewX(up * rotationSensitivity);
				}
					
			}
			
			if(((e.getModifiersEx() & onmask) == bindings.getRotateYBinding() && e.getSource() != rotate) || e.getSource() == rotate) {			
				double left = e.getX() - p.getX();
				
				if(isAbsolute()) {
					v.rotateWorldViewY(-left * rotationSensitivity);
				}
				else {
					v.rotateLocalViewY(-left * rotationSensitivity);
				}
					
			}
			
			if(((e.getModifiersEx() & onmask) == bindings.getRotateZBinding() && e.getSource() != rotate) || (e.getSource() == rotate && e.getButton() == MouseEvent.BUTTON2)) {
				double left = e.getY() - p.getY();
				
				if(isAbsolute()) {
					v.rotateWorldViewZ(left * rotationSensitivity);
				}
				else
					v.rotateLocalViewZ(left * rotationSensitivity);
			}
			
			p = e.getPoint();
			
			
		}
		
		public void mouseMoved(MouseEvent e) {
			
		}
		
		public void mouseReleased(MouseEvent e) {
			p = null;
		}
	}
	
	public class TranslateListener extends MouseAdapter implements ActionListener, MouseMotionListener {
		
		private Point p = null;
		
		public void actionPerformed(ActionEvent e) {
			
		}
		
		
		public void mouseDragged(MouseEvent e) {
			if(p == null) {
				p = e.getPoint();
				return;
			}
			
			int onmask = bindings.getViewManipulationBindings();
			
			if((e.getModifiersEx() & onmask) == bindings.getTranslationBinding() || (e.getSource() == translate && (e.getModifiersEx() & InputEvent.BUTTON1_DOWN_MASK) == InputEvent.BUTTON1_DOWN_MASK)) {
				double left = e.getX() - p.getX();
				double up = e.getY() - p.getY();
				p = e.getPoint();
			
			
				if(isAbsolute()) {
					view.translateWorldView(left, -up);
				}
				else {
					((PerspectiveView)view).translateLocalView(left, -up);
				}
			}
		}
		
		public void mouseMoved(MouseEvent e) {
			
		}
		
		public void mouseReleased(MouseEvent e) {
			p = null;
		}
	}
	
	public class ZoomListener extends MouseAdapter implements ActionListener, MouseMotionListener {
		
		private Point p = null;
		
		public void actionPerformed(ActionEvent e) {
			
		}

		
		public void mouseDragged(MouseEvent e) {
			if(p == null) {
				p = e.getPoint();
				return;
			}
			
			int onmask = bindings.getViewManipulationBindings();
			
			if((e.getModifiersEx() & onmask) == bindings.getZoomBinding() || e.getSource() == zoom) {
				double moved = e.getY() - p.getY();
				p = e.getPoint();

				if(isAbsolute()) {
					view.zoomWorldView(moved);
				}
				else {
					((PerspectiveView)view).zoomLocalView(moved);
				}
			}
		}
		
		public void mouseMoved(MouseEvent e) {
			
			
		}
		
		public void mouseReleased(MouseEvent e) {
			p = null;
		}
		
	}



	public RotateListener getRotateListener() {
		return rotateListener;
	}

	public TranslateListener getTranslateListener() {
		return translateListener;
	}

	public ZoomListener getZoomListener() {
		return zoomListener;
	}
	
	
	
	
}
