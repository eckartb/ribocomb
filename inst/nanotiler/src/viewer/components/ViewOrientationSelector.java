package viewer.components;

import viewer.display.*;
import viewer.view.ViewOrientation;

import javax.swing.*;

/**
 * A popmenu that contains the different types of views.
 * @author calvin
 *
 */
public class ViewOrientationSelector extends JComboBox {
	
	public ViewOrientationSelector(ViewOrientation orientation) {
		this.setModel(new Model());
		this.setSelectedItem(orientation);
	}
	
	private class Model extends AbstractListModel implements ComboBoxModel {
		
		private ViewOrientation orientation;

		public Object getSelectedItem() {
			return orientation;
		}

		public void setSelectedItem(Object o) {
			orientation = (ViewOrientation) o;
			
		}

		public Object getElementAt(int index) {
			return ViewOrientation.values()[index];
		}

		public int getSize() {
			return ViewOrientation.values().length;
		}
		
	}
}
