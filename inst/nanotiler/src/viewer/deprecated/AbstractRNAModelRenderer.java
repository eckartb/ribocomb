package viewer.deprecated;

import java.awt.Color;

import javax.media.opengl.GL;

import rnadesign.rnamodel.Atom3D;
import rnadesign.rnamodel.BranchDescriptor3D;
import rnadesign.rnamodel.KissingLoop3D;
import rnadesign.rnamodel.Nucleotide3D;
import rnadesign.rnamodel.RnaStem3D;
import rnadesign.rnamodel.RnaStrand;
import rnadesign.rnamodel.StrandJunction3D;
import tools3d.objects3d.Link;
import tools3d.objects3d.Object3D;
import tools3d.BoundingVolume;
import tools3d.Vector4D;
import tools3d.Vector3D;
import viewer.graphics.Material;
import viewer.graphics.RenderableModel;
import rnadesign.rnacontrol.*;

/**
 * Classes wanting to render an RNA molecule in certain ways
 * should extend this class. This class provides empty implementations
 * for all the different rendering modes and doesn't do anything
 * unless it is overwritten.
 * 
 * Objects in the scene graph are rendered in hierarchical order.
 * That is, an RnaStrand object is rendered before the objects
 * that compose that strand are rendered (the atoms and the nucleotides).
 * This ordering can be used to draw strands and the atoms that compose them
 * but no other atoms (by calling the render atom routine from within 
 * the render strand routine)
 * @author Calvin
 *
 */
public abstract class AbstractRNAModelRenderer implements RNAModelRenderer,
		RenderableModel {
	
	private Object3DGraphController controller;
	
	private boolean wireframeMode = false;
	
	public void setWireframeMode(boolean wireframeMode) {
		this.wireframeMode = wireframeMode;
	}
	
	public boolean isWireframeMode() {
		return wireframeMode;
	}
	
	public AbstractRNAModelRenderer(Object3DGraphController controller) {
		this.controller = controller;
	}
	
	public AbstractRNAModelRenderer() {
		this(null);
	}
	
	public void setController(Object3DGraphController controller) {
		this.controller = controller;
	}
	
	public Object3DGraphController getController() {
		return controller;
	}

	public boolean getRenderAtom() {
		return false;
	}

	public boolean getRenderBranchDescriptor() {
		return false;
	}

	public boolean getRenderKissingLoop() {
		return false;
	}

	public boolean getRenderLinks() {
		return false;
	}

	public boolean getRenderNucleotide() {
		return false;
	}

	public boolean getRenderRNAStem() {
		return false;
	}

	public boolean getRenderStrand() {
		return false;
	}

	public boolean getRenderStrandJunction() {
		return false;
	}

	public boolean getRenderOther() {
		return false;
	}

	public void renderOther(Object3D o, GL gl) {
		
	}

	public void renderAtom(Atom3D atom, GL gl) {

	}

	public void renderBranchDescriptor(BranchDescriptor3D bd, GL gl) {

	}

	public void renderKissingLoop(KissingLoop3D kl, GL gl) {

	}

	public void renderLink(Link link, GL gl) {

	}

	public void renderNucleotide(Nucleotide3D n, GL gl) {

	}

	public void renderRNAStem(RnaStem3D stem, GL gl) {

	}

	public void renderRnaStrand(RnaStrand strand, GL gl) {

	}

	public void renderStrandJunction(StrandJunction3D sj, GL gl) {


	}
	
	public void renderSelected(Object3D o, GL gl) {
		Material material = new Material();
		Color c = Color.yellow;
		material.setEmissive(c.getRed() / 255.0, c.getGreen() / 255.0, c.getBlue() / 255.0, 1.0);
		gl.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, material.getAmbient().buffer());
		gl.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, material.getDiffuse().buffer());
		gl.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, material.getSpecular().buffer());
		gl.glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, material.getEmissive().buffer());
		gl.glMaterialf(GL.GL_FRONT, GL.GL_SHININESS, (float) material.getHighlight());
		
		BoundingVolume v = o.getBoundingVolume();
		Vector4D[][] faces = v.getFaces();
		gl.glPushMatrix();
		Vector3D p = o.getPosition();
		gl.glTranslated(p.getX(), p.getY(), p.getZ());
		gl.glBegin(GL.GL_LINES);
		for(int i = 0; i < faces.length; ++i) {
			for(int j = 0; j < faces[i].length; ++j) {
					Vector4D vec = faces[i][j];
					gl.glVertex3d(vec.getX(), vec.getY(), vec.getZ());
					vec = faces[i][(j + 1) % faces[i].length];
					gl.glVertex3d(vec.getX(), vec.getY(), vec.getZ());
				
				
			}
		}
		
		gl.glEnd();
		gl.glPopMatrix();
		
	}

	public void render(GL gl) {
		
		render(gl, controller.getGraph().getGraph());
		
		if(getRenderLinks()) {
			LinkController links = controller.getLinks();
			for(int i = 0; i < links.size(); ++i) {
				Link link = links.get(i);
				renderLink(link, gl);
			}
		}

	}
	
	protected void render(GL gl, Object3D o) {
		
		
		
		if(getRenderNucleotide() && o instanceof Nucleotide3D) {
			renderNucleotide((Nucleotide3D) o, gl);
		}
		else if(getRenderAtom() && o instanceof Atom3D) {
			renderAtom((Atom3D) o, gl);
		}
		else if(getRenderStrand() && o instanceof RnaStrand) {
			renderRnaStrand((RnaStrand) o, gl);
		}
		else if(getRenderRNAStem() && o instanceof RnaStem3D) {
			renderRNAStem((RnaStem3D) o, gl);
		}
		else if(getRenderBranchDescriptor() && o instanceof BranchDescriptor3D) {
			renderBranchDescriptor((BranchDescriptor3D) o, gl);
		}
		else if(getRenderStrandJunction() && o instanceof StrandJunction3D) {
			renderStrandJunction((StrandJunction3D) o, gl);
		}
		else if(getRenderKissingLoop() && o instanceof KissingLoop3D) {
			renderKissingLoop((KissingLoop3D) o, gl);
		}
		else if(getRenderOther()) {
			renderOther(o, gl);
		}
		
		if(o.isSelected())
			renderSelected(o, gl);
		
		if(o.size() == 0)
			return;
		
		
		
		for(int i = 0; i < o.size(); ++i) {
			render(gl, o.getChild(i));
		}
	}
	
	public Object clone() {
		try {
			return super.clone();
		} catch(CloneNotSupportedException e) {
			
		}
		
		return null;
	}
}
