package viewer.deprecated;

import rnadesign.rnacontrol.Object3DGraphController;
import viewer.graphics.*;
import tools3d.objects3d.*;

import javax.media.opengl.*;
import java.awt.Color;

public class ColorableRnaModelRenderer extends AbstractRNAModelRenderer implements Colorable {
	
	private ColorModel model;

	public ColorableRnaModelRenderer(Object3DGraphController controller, ColorModel model) {
		super(controller);
		
		this.model = model;
	}
	
	public ColorableRnaModelRenderer(Object3DGraphController controller) {
		this(controller, new DefaultColorModel());
	}
	
	public ColorableRnaModelRenderer(ColorModel model) {
		this(null, model);
	}
	
	public ColorableRnaModelRenderer() {
		this(null, null);
	}
	
	public ColorModel getColorModel() {
		return model;
	}
	
	public void setColorModel(ColorModel model) {
		this.model = model;
	}
	
	public void render(GL gl) {
		super.render(gl);
	}
	

	
	protected void render(GL gl, Object3D o) {
		
		Material material = model.getMaterial(o);
		gl.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, material.getAmbient().buffer());
		gl.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, material.getDiffuse().buffer());
		gl.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, material.getSpecular().buffer());
		gl.glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, material.getEmissive().buffer());
		gl.glMaterialf(GL.GL_FRONT, GL.GL_SHININESS, (float) material.getHighlight());
		
		super.render(gl, o);
	}

	@Override
	public void renderLink(Link link, GL gl) {
		Material material = model.getMaterial(link);
		gl.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, material.getAmbient().buffer());
		gl.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, material.getDiffuse().buffer());
		gl.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, material.getSpecular().buffer());
		gl.glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, material.getEmissive().buffer());
		gl.glMaterialf(GL.GL_FRONT, GL.GL_SHININESS, (float) material.getHighlight());
		
		super.renderLink(link, gl);
	}
	
	public Object clone() {
		return super.clone();
	}
	
	
	
	
	

}
