package viewer.deprecated;

import java.awt.Color;
import tools3d.*;
import viewer.util.Tools;
import static viewer.graphics.OldMesh.Quad;
import static viewer.graphics.OldMesh.Line;
import viewer.graphics.OldMesh;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Calvin
 *@deprecated
 */
public class Cylinder implements OldMesh {
	
	private Vector4D e1, e2;
	private Vector4D norm;
	
	private Color wireframeColor = Color.blue;
	private Color solidColor = Color.white;
	private boolean renderWireframe = false;
	private boolean renderSolid = true;
	
	private int crossSections = 1;
	private int thetaIncrements = 12;
	
	public Cylinder(double radius, Vector4D e1, Vector4D e2) {
		this.e1 = e1;
		this.e2 = e2;
		Vector4D v = e1.minus(e2);
		Vector4D u = Tools.orthogonal(v);
		
		u.setX(v.getX() + 1);
		double value = Tools.project(u, v);
		Vector4D uv = v.mul(value);
		norm = u.minus(uv);
		norm.normalize();
		norm = norm.mul(radius);
	}
	
	public Cylinder(double radius, Vector4D e1, Vector4D e2, int crossSections, int thetaIncrements) {
		this(radius, e1, e2);
		this.crossSections = crossSections;
		this.thetaIncrements = thetaIncrements;
	}

	public Line[] getLines() {
		ArrayList<Line> lines = new ArrayList<Line>();
		compute(null, lines);
		Line[] l = new Line[lines.size()];
		lines.toArray(l);
		return l;
	}

	public Quad[] getQuads() {
		ArrayList<Quad> quads = new ArrayList<Quad>();
		compute(quads, null);
		Quad[] q = new Quad[quads.size()];
		quads.toArray(q);
		return q;
	}
	
	private void compute(List<Quad> quads, List<Line> lines) {
		Vector4D v = e1.minus(e2);
		double length = v.length();
		double lengthIncrement = length / crossSections;
		v.normalize();
		
		// geometry for the first disk
		double thetaInc = Math.PI * 2 / thetaIncrements;
		Vector4D norm = new Vector4D(this.norm);
		for(int i = 0; i < thetaIncrements; ++i) {
			
			Quad q = new Quad();
			q.v1 = new Vector4D(e2);
			Vector4D n = v.mul(-1);
			n.normalize();
			q.v1n = n;
			q.v2n = n;
			q.v3n = n;
			q.v4n = n;
			q.v2 = new Vector4D(e2);
			q.v2.add(norm);
			norm = Tools.rotate(norm, v, -thetaInc);
			q.v3 = new Vector4D(e2);
			q.v3.add(norm);
			q.v4 = new Vector4D(e2);
			if(quads != null)
				quads.add(q);
			
			
			
			if(lines != null) {
				Line l1 = new Line();
				Line l2 = new Line();
				Line l3 = new Line();
				Line l4 = new Line();
				l1.v1 = q.v1;
				l1.v2 = q.v2;
				l2.v1 = q.v2;
				l2.v2 = q.v3;
				l3.v1 = q.v3;
				l3.v2 = q.v4;
				l4.v1 = q.v4;
				l4.v2 = q.v1;
				lines.add(l1);
				lines.add(l2);
				lines.add(l3);
				lines.add(l4);
			}
		}
		
		// mid section geometry
		
		Vector4D lengthVector = v.mul(lengthIncrement);
		for (int i = 0; i < thetaIncrements; i++) {
			Vector4D normLocation = new Vector4D(e2);
			for (int j = 0; j < crossSections; ++j) {
				
				Quad q = new Quad();
				Vector4D v1 = new Vector4D(normLocation);
				v1.add(norm);
				q.v1 = v1;
				q.v1n = new Vector4D(norm);
				q.v1n.normalize();
				normLocation.add(lengthVector);
				Vector4D v2 = new Vector4D(normLocation);
				v2.add(norm);
				q.v2 = v2;
				q.v2n = new Vector4D(norm);
				q.v2n.normalize();

				norm = Tools.rotate(norm, v, -thetaInc);

				Vector4D v3 = new Vector4D(normLocation);
				v3.add(norm);
				q.v3 = v3;
				q.v3n = new Vector4D(norm);
				q.v3n.normalize();

				normLocation = normLocation.minus(lengthVector);

				Vector4D v4 = new Vector4D(normLocation);
				v4.add(norm);
				q.v4 = v4;
				q.v4n = new Vector4D(norm);
				q.v4n.normalize();

				

				if (quads != null)
					quads.add(q);

				if (lines != null) {
					Line l1 = new Line();
					Line l2 = new Line();
					Line l3 = new Line();
					Line l4 = new Line();
					l1.v1 = q.v1;
					l1.v2 = q.v2;
					l2.v1 = q.v2;
					l2.v2 = q.v3;
					l3.v1 = q.v3;
					l3.v2 = q.v4;
					l4.v1 = q.v4;
					l4.v2 = q.v1;
					lines.add(l1);
					lines.add(l2);
					lines.add(l3);
					lines.add(l4);
				}

				normLocation.add(lengthVector);
			}
		}
		// add geometry for the final disk
		for(int i = 0; i < thetaIncrements; ++i) {
			Quad q = new Quad();
			q.v1 = new Vector4D(e1);
			q.v1n = v;
			q.v2n = v;
			q.v3n = v;
			q.v4n = v;
			q.v2 = new Vector4D(e1);
			q.v2.add(norm);
			norm = Tools.rotate(norm, v, thetaInc);
			q.v3 = new Vector4D(e1);
			q.v3.add(norm);
			q.v4 = new Vector4D(e1);
			
			if(quads != null)
				quads.add(q);
			
			if(lines != null) {
				Line l1 = new Line();
				Line l2 = new Line();
				Line l3 = new Line();
				Line l4 = new Line();
				l1.v1 = q.v1;
				l1.v2 = q.v2;
				l2.v1 = q.v2;
				l2.v2 = q.v3;
				l3.v1 = q.v3;
				l3.v2 = q.v4;
				l4.v1 = q.v4;
				l4.v2 = q.v1;
				lines.add(l1);
				lines.add(l2);
				lines.add(l3);
				lines.add(l4);
			}
		}
	}

	public Color getSolidColor() {
		return solidColor;
	}

	public Color getWireframeColor() {
		return wireframeColor;
	}

	public void refine() {
		thetaIncrements *= 2;
		crossSections *= 2;
	}

	public boolean renderSolid() {
		return renderSolid;
	}

	public boolean renderWireframe() {
		return renderWireframe;
	}

}
