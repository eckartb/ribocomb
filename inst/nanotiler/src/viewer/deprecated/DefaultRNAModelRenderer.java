package viewer.deprecated;


import javax.media.opengl.GL;

import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnamodel.Atom3D;
import rnadesign.rnamodel.BranchDescriptor3D;
import rnadesign.rnamodel.KissingLoop3D;
import rnadesign.rnamodel.Nucleotide3D;
import rnadesign.rnamodel.RnaStem3D;
import rnadesign.rnamodel.RnaStrand;
import rnadesign.rnamodel.StrandJunction3D;
import tools3d.Vector3D;
import tools3d.Vector4D;
import tools3d.objects3d.Link;
import tools3d.objects3d.Object3D;
import viewer.graphics.ColorModel;
import viewer.deprecated.Cylinder;
import viewer.graphics.Material;
import viewer.graphics.MeshRenderer;
import viewer.graphics.PrimitiveFactory;
import viewer.graphics.Mesh;
import viewer.graphics.WireframeMeshRenderer;
import viewer.graphics.SolidMeshRenderer;

import java.awt.Color;

public class DefaultRNAModelRenderer extends ColorableRnaModelRenderer {

	private boolean renderAtom = true;
	private boolean renderNucleotides = true;
	private boolean renderBranchDescriptor = true;
	private boolean renderKissingLoop = true;
	private boolean renderLinks = true;
	private boolean renderOther = true;
	private boolean renderRNAStem = true;
	private boolean renderStrand = true;
	private boolean renderStrandJunction = true;
	
	private MeshRenderer renderer = null;
	
	public DefaultRNAModelRenderer(Object3DGraphController controller, ColorModel model) {
		super(controller, model);
		
		setWireframeMode(false);
	}
	
	public DefaultRNAModelRenderer(Object3DGraphController controller) {
		super(controller);
		
		setWireframeMode(false);
	}
	
	public DefaultRNAModelRenderer() {
		setWireframeMode(false);
	}
	
	public void setWireframeMode(boolean wireframeMode) {
		super.setWireframeMode(wireframeMode);
		Mesh sphere = PrimitiveFactory.generateSphere(0.5, 4, 4);
		if(wireframeMode) {
			renderer = new WireframeMeshRenderer(sphere);
		}
		else {
			renderer = new SolidMeshRenderer(sphere);
		}
	}
	
	


	public boolean getRenderAtom() {
		return renderAtom;
	}




	public void setRenderAtom(boolean renderAtom) {
		this.renderAtom = renderAtom;
	}




	public boolean getRenderBranchDescriptor() {
		return renderBranchDescriptor;
	}




	public void setRenderBranchDescriptor(boolean renderBranchDescriptor) {
		this.renderBranchDescriptor = renderBranchDescriptor;
	}




	public boolean getRenderKissingLoop() {
		return renderKissingLoop;
	}




	public void setRenderKissingLoop(boolean renderKissingLoop) {
		this.renderKissingLoop = renderKissingLoop;
	}




	public boolean getRenderLinks() {
		return renderLinks;
	}




	public void setRenderLinks(boolean renderLinks) {
		this.renderLinks = renderLinks;
	}




	public boolean getRenderNucleotide() {
		return renderNucleotides;
	}




	public void setRenderNucleotide(boolean renderNucleotides) {
		this.renderNucleotides = renderNucleotides;
	}




	public boolean getRenderOther() {
		return renderOther;
	}




	public void setRenderOther(boolean renderOther) {
		this.renderOther = renderOther;
	}




	public boolean getRenderRNAStem() {
		return renderRNAStem;
	}




	public void setRenderRNAStem(boolean renderRNAStem) {
		this.renderRNAStem = renderRNAStem;
	}




	public boolean getRenderStrand() {
		return renderStrand;
	}




	public void setRenderStrand(boolean renderStrand) {
		this.renderStrand = renderStrand;
	}




	public boolean getRenderStrandJunction() {
		return renderStrandJunction;
	}




	public void setRenderStrandJunction(boolean renderStrandJunction) {
		this.renderStrandJunction = renderStrandJunction;
	}
	
	public void renderSphere(Object3D o, GL gl) {
		Vector3D p = o.getPosition();
		gl.glPushMatrix();
		gl.glTranslated(p.getX(), p.getY(), p.getZ());
		
		renderer.render(gl);
		
		/*Material m = new Material(Color.yellow);
		Material.renderMaterial(gl, m);
		
		tools3d.BoundingVolume volume = o.getBoundingVolume();
		gl.glBegin(GL.GL_LINES);
		Vector4D[] vertices = volume.getVertices();
		for(int i = 0; i < vertices.length; ++i) {
			for(int j = i + 1; j < vertices.length + 1; ++j) {
				gl.glVertex3d(vertices[i].getX(), vertices[i].getY(), vertices[i].getZ());
				gl.glVertex3d(vertices[j % vertices.length].getX(), vertices[j % vertices.length].getY(), vertices[j % vertices.length].getZ());
			}
		}
		gl.glEnd();*/
		
		gl.glPopMatrix();
	}
	

	@Override
	public void renderAtom(Atom3D atom, GL gl) {
		renderSphere(atom, gl);
	}

	@Override
	public void renderBranchDescriptor(BranchDescriptor3D bd, GL gl) {
		renderSphere(bd, gl);
	}

	@Override
	public void renderKissingLoop(KissingLoop3D kl, GL gl) {
		renderSphere(kl, gl);
	}

	@Override
	public void renderNucleotide(Nucleotide3D n, GL gl) {
		renderSphere(n, gl);
	}

	@Override
	public void renderOther(Object3D o, GL gl) {
		renderSphere(o, gl);
	}

	@Override
	public void renderRNAStem(RnaStem3D stem, GL gl) {
		renderSphere(stem, gl);
	}

	@Override
	public void renderRnaStrand(RnaStrand strand, GL gl) {
		renderSphere(strand, gl);
	}

	@Override
	public void renderStrandJunction(StrandJunction3D sj, GL gl) {
		renderSphere(sj, gl);
	}
	
	public void renderLink(Link link, GL gl) {
		super.renderLink(link, gl);
		
		Vector4D p1 = new Vector4D(link.getObj1().getPosition());
		Vector4D p2 = new Vector4D(link.getObj2().getPosition());
		p1.setW(0.0);
		p2.setW(0.0);

		Cylinder c = new Cylinder(.2, p2, p1);
		//MeshRenderer renderer = new MeshRenderer();
		//renderer.addMesh(c);
		//renderer.render(gl);
	}
	
	
	
	

}
