package viewer.deprecated;

import javax.media.opengl.GL;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;

import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnamodel.Atom3D;
import rnadesign.rnamodel.BranchDescriptor3D;
import rnadesign.rnamodel.KissingLoop3D;
import rnadesign.rnamodel.Nucleotide3D;
import rnadesign.rnamodel.RnaStem3D;
import rnadesign.rnamodel.RnaStrand;
import rnadesign.rnamodel.StrandJunction3D;
import rnadesign.rnamodel.Residue3D;
import rnadesign.rnamodel.NucleotideTools;
import rnadesign.rnamodel.AtomTools;
import rnadesign.rnamodel.NucleotideTools;
import rnadesign.rnamodel.RnaModelException;
import tools3d.objects3d.Link;
import tools3d.objects3d.Object3D;
import tools3d.splines.StandaloneSplineFactory;
import tools3d.Vector3D;
import tools3d.Vector4D;
import viewer.graphics.ColorModel;
import viewer.graphics.Colorable;
import viewer.graphics.DefaultColorModel;
import viewer.graphics.CurveRenderer;
import viewer.graphics.CylindricalCurveRenderer;
import viewer.graphics.Material;
import viewer.graphics.RenderableModel;
import viewer.graphics.PrimitiveFactory;
import viewer.graphics.Mesh;
import viewer.graphics.MeshOperations;
import viewer.graphics.SolidMeshRenderer;
import viewer.graphics.WireframeMeshRenderer;
import viewer.graphics.MeshRenderer;
import viewer.util.Tools;

import tools3d.objects3d.LinkSet;
import tools3d.objects3d.SimpleLinkSet;
import tools3d.objects3d.SimpleLink;
import tools3d.objects3d.Link;

import controltools.*;

public class NucleotideBackboneRenderer implements RNAModelRenderer, Colorable,
	ModelChangeListener {

	private StandaloneSplineFactory factory = new StandaloneSplineFactory();
	
	private Object3DGraphController controller;
	
	private boolean wireframeMode = false;
	
	private static final int REFINEMENT = 3;
	
	private Map<Object3D, RenderableModel> renderers =
		new HashMap<Object3D, RenderableModel>();
	
	private ColorModel colorModel = new DefaultColorModel();
	
	public NucleotideBackboneRenderer(Object3DGraphController controller) {
		this.controller = controller;
		// controller.getGraph().addModelChangeListener(this);
		controller.addModelChangeListener(this);
		
		assembleMeshes();
	}
	
	public NucleotideBackboneRenderer(Object3DGraphController controller, ColorModel colorModel) {
		this(controller);
		this.colorModel = colorModel;
	}
	
	
	public Object3DGraphController getController() {
		return controller;
	}
	
	

	public void modelChanged(ModelChangeEvent e) {
		assembleMeshes();
	}
	
	private void assembleMeshes() {
		renderers.clear();
		
		assembleMeshes(controller.getGraph().getGraph());
	}
	
	private void assembleMeshes(Object3D o) {
		if(o instanceof RnaStrand) {
			assembleRnaStrandMesh((RnaStrand) o);
			return;
		}
		
		if(o.size() > 0)
			for(int i = 0; i < o.size(); ++i)
				assembleMeshes(o.getChild(i));
	}
	
	private void assembleRnaStrandMesh(RnaStrand strand) {
		List<Vector3D> backbone = new ArrayList<Vector3D>();
		List<Vector3D> bases = new ArrayList<Vector3D>();
		List<Boolean> purines = new ArrayList<Boolean>();
		for(int i = 0; i < strand.getResidueCount(); ++i) {
			Residue3D residue = strand.getResidue3D(i);
			if(residue instanceof Nucleotide3D) {
				Nucleotide3D n = (Nucleotide3D) residue;
				if(n.getSymbol().getCharacter() == 'A' || n.getSymbol().getCharacter() == 'G')
					purines.add(true);
				else
					purines.add(false);
				
				try {
					bases.add(assembleNucleotideMesh(n, backbone).getPosition());
				} catch(RnaModelException e) {
					System.out.println(e.getMessage());
				}
			}
		}
		
		// check to make sure at least the backbone can be drawn
		if(backbone.size() < 2) {
			JOptionPane.showMessageDialog(null, "Cartoon renderer cannot render molecule. Please switch to a different renderer.");
			return;
		}
		
		
		Vector3D[] points = new Vector3D[backbone.size()];
		backbone.toArray(points);
		
		Vector3D[] b = new Vector3D[bases.size()];
		bases.toArray(b);
		
		Vector3D[] curve = factory.createCubic(points, REFINEMENT);
		
		
		renderers.put(strand, new CylindricalCurveRenderer(curve, 0.5, 6, wireframeMode));
		
		
	}
	
	private Atom3D assembleNucleotideMesh(Nucleotide3D n, List<Vector3D> backbone)
		throws RnaModelException {
		Set<Atom3D> atoms = new HashSet<Atom3D>();
		if(n.getIndexOfChild("O5*") < 0)
			throw new RnaModelException("Atom O5* not a member of nucleotide");
		backbone.add(n.getChild("O5*").getPosition());
		for(int i = 0; i < n.getAtomCount(); ++i) {
			Atom3D atom = n.getAtom(i);
			
			if(NucleotideTools.isBaseAtom(atom))
				atoms.add(atom);
		}
		
		/*LinkSet covalentLinks = NucleotideTools.getCovalentLinks(n);
		
		MeshRenderer renderer = wireframeMode ? new WireframeMeshRenderer() : new SolidMeshRenderer();
		
		for(int i = 0; i < covalentLinks.size(); ++i) {
			Link l = covalentLinks.get(i);
			Atom3D o1 = (Atom3D) l.getObj1();
			Atom3D o2 = (Atom3D) l.getObj2();
			Vector4D axis = new Vector4D(o2.getPosition().minus(o1.getPosition()));
			axis.setW(0.0);
			axis.normalize();
			Mesh cylinder = PrimitiveFactory.generateCylinder(axis, 0.25, o2.getPosition().distance(o1.getPosition()), 1, 6);
			Vector4D midpoint = new Vector4D((o1.getPosition().getX() + o2.getPosition().getX()) / 2.0,
					(o1.getPosition().getY() + o2.getPosition().getY()) / 2.0,
					(o1.getPosition().getZ() + o2.getPosition().getZ()) / 2.0, 1.0);
			cylinder = MeshOperations.translateMesh(cylinder, midpoint);
			renderer.addMesh(cylinder);
			
		}
		
		Object3D o1 = n.getChild("O5*");
		Object3D o2 = null;
		if(n.getSymbol().getCharacter() == 'A' || n.getSymbol().getCharacter() == 'G') {
			if(n.getIndexOfChild("N9") < 0)
				throw new RnaModelException("Atom N9 not in nucleotide");
			
			o2 = n.getChild("N9");
		}
		else {
			if(n.getIndexOfChild("N1") < 0)
				throw new RnaModelException("Atom N1 not in nucleotide");
			
			o2 = n.getChild("N1");
		}
		Vector4D axis = new Vector4D(o2.getPosition().minus(o1.getPosition()));
		axis.setW(0.0);
		axis.normalize();
		Mesh cylinder = PrimitiveFactory.generateCylinder(axis, 0.25, o2.getPosition().distance(o1.getPosition()), 1, 6);
		Vector4D midpoint = new Vector4D((o1.getPosition().getX() + o2.getPosition().getX()) / 2.0,
				(o1.getPosition().getY() + o2.getPosition().getY()) / 2.0,
				(o1.getPosition().getZ() + o2.getPosition().getZ()) / 2.0, 1.0);
		cylinder = MeshOperations.translateMesh(cylinder, midpoint);
		renderer.addMesh(cylinder);
		
		renderers.put(n, renderer);
		*/
		
		
		return (Atom3D) n.getChild("C1*");
	}
	

	public boolean getRenderAtom() { return false; }
	public boolean getRenderBranchDescriptor() { return false; }
	public boolean getRenderKissingLoop() { return false; }
	public boolean getRenderLinks() { return true; }
	public boolean getRenderNucleotide() { return true; }
	public boolean getRenderOther() { return false; }
	public boolean getRenderRNAStem() { return false; }
	public boolean getRenderStrand() { return true; }
	public boolean getRenderStrandJunction() { return false; }

	/* Not used */
	public void renderAtom(Atom3D atom, GL gl) { }
	public void renderBranchDescriptor(BranchDescriptor3D bd, GL gl) { }
	public void renderKissingLoop(KissingLoop3D kl, GL gl) { }
	public void renderOther(Object3D o, GL gl) { }
	public void renderRNAStem(RnaStem3D stem, GL gl) { }
	public void renderStrandJunction(StrandJunction3D sj, GL gl) { }

	public void renderLink(Link link, GL gl) {
		// TODO Auto-generated method stub

	}

	public void renderNucleotide(Nucleotide3D n, GL gl) {
		// TODO Auto-generated method stub

	}


	public void renderRnaStrand(RnaStrand strand, GL gl) {
		
	}
	
	public void renderNucleotide(Nucleotide3D n, GL gl, List<Vector3D> backbone) {
		
	}

	public void renderSelected(Object3D o, GL gl) {
		// TODO Auto-generated method stub

	}

	

	public void setController(Object3DGraphController controller) {
		this.controller = controller;
	}

	public void setWireframeMode(boolean wireframeMode) {
		this.wireframeMode = wireframeMode;
		assembleMeshes();

	}

	public void render(GL gl) {
		for(Map.Entry<Object3D, RenderableModel> e : renderers.entrySet()) {
			if(colorModel != null) {
				Material material = colorModel.getMaterial(e.getKey());
				Material.renderMaterial(gl, material);
			}
			
			e.getValue().render(gl);
		}

	}

	public ColorModel getColorModel() {
		return colorModel;
	}

	public void setColorModel(ColorModel model) {
		this.colorModel = model;
	}
	
	public Object clone() {
		try {
			return super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		
		return null;
	}

}
