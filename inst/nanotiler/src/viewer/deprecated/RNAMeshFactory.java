package viewer.deprecated;

import viewer.display.*;
import viewer.graphics.*;
import viewer.util.Tools;
import tools3d.*;

import java.util.*;

public class RNAMeshFactory {
	
	private static final double PENT_RADIUS = 1.0 / (2. * Math.sin(Math.PI / 5));

	public static Mesh generatePurine() {
		Mesh hex = hexagon();
		Mesh pent = pentagon();
		
		pent = MeshOperations.translateMesh(pent, new Vector4D(1. + PENT_RADIUS, 0.0, 0.0, 0.0));
	
		return MeshOperations.synthesize(hex, pent);
	}
	
	private static Vector3D[] controlPoints(List<Vector4D> array) {
		Vector3D[] a = new Vector3D[array.size()];
		for(int i = 0; i < a.length; ++i) {
			a[i] = new Vector3D(array.get(i));
		}
		
		return a;
	}
	
	
	public static Mesh generatePyrimidine() {
		return hexagon();
	}
	
	private static Mesh hexagon() {
		ArrayList<Vector4D> vertices = new ArrayList<Vector4D>(14);
		ArrayList<Vector4D> normals = new ArrayList<Vector4D>(14);
		ArrayList<int[]> triangles = new ArrayList<int[]>();
		
		double angle = Math.PI * 2 / 6;
		Vector4D normal = new Vector4D(0.0, 0.0, 1.0, 0.0);
		ArrayList<Vector4D> polygon = new ArrayList<Vector4D>();
		Vector4D normalPolygon = new Vector4D(0.0, .25, 0.0, 0.0);
		for(int i = 0; i < 6; ++i) {
			vertices.add(normal);
			normals.add(new Vector4D(0.0, 1.0, 0.0, 0.0));
			normal = Tools.rotate(normal, Tools.Y_AXIS, angle);
			polygon.add(normalPolygon);
			normalPolygon = Tools.rotate(normalPolygon, Tools.Z_AXIS, -angle);
		}
		
		Vector3D[] controlPoints = controlPoints(vertices);
		Vector4D[] aPolygon = new Vector4D[6];
		polygon.toArray(aPolygon);
		
		Mesh mesh1 = MeshOperations.extendRing(aPolygon, Tools.Z_AXIS, controlPoints);
		
		for(int i = 0; i < 6; ++i) {
			vertices.add(new Vector4D(vertices.get(i)));
			normals.add(new Vector4D(0.0, -1.0, 0.0, 0.0));
			int[] triangle1 = {
					i, (i + 1) % 6, 12
			};
			
			int[] triangle2 = {
					6 + ((i + 1) % 6), 6 + i, 13
			};
			
			triangles.add(triangle1);
			triangles.add(triangle2);
		}
		
		vertices.add(new Vector4D(0.0, 0.0, 0.0, 0.0));
		vertices.add(new Vector4D(0.0, 0.0, 0.0, 0.0));
		normals.add(new Vector4D(0.0, 1.0, 0.0, 0.0));
		normals.add(new Vector4D(0.0, -1.0, 0.0, 0.0));
		
		Mesh mesh2 = new DefaultMesh(vertices, normals, triangles, 3);
		return MeshOperations.synthesize(mesh1, mesh2);
	}
	
	private static Mesh pentagon() {
		ArrayList<Vector4D> vertices = new ArrayList<Vector4D>(12);
		ArrayList<Vector4D> normals = new ArrayList<Vector4D>(12);
		ArrayList<int[]> triangles = new ArrayList<int[]>();
		
		double radius = PENT_RADIUS;
		Vector4D normal = new Vector4D(1.0, 0.0, 0.0, 0.0);
		normal = normal.mul(radius);
		double angle = Math.PI * 2 / 5;
		
		ArrayList<Vector4D> polygon = new ArrayList<Vector4D>();
		Vector4D normalPolygon = new Vector4D(0.0, .25, 0.0, 0.0);
		for(int i = 0; i < 5; ++i) {
			vertices.add(normal);
			normals.add(new Vector4D(0.0, 1.0, 0.0, 0.0));
			normal = Tools.rotate(normal, Tools.Y_AXIS, angle);
			polygon.add(normalPolygon);
			normalPolygon = Tools.rotate(normalPolygon, Tools.Z_AXIS, -angle);
		}
		
		Vector3D[] controlPoints = controlPoints(vertices);
		Vector4D[] aPolygon = new Vector4D[5];
		polygon.toArray(aPolygon);
		
		Mesh mesh1 = MeshOperations.extendRing(aPolygon, Tools.Z_AXIS, controlPoints);
		
		for(int i = 0; i < 5; ++i) {
			vertices.add(new Vector4D(vertices.get(i)));
			normals.add(new Vector4D(0.0, -1.0, 0.0, 0.0));
			int[] triangle1 = {
					i, (i + 1) % 5, 10
			};
			
			int[] triangle2 = {
					5 + ((i + 1) % 5), 5 + i, 11
			};
			
			triangles.add(triangle1);
			triangles.add(triangle2);
		}
		
		vertices.add(new Vector4D(0.0, 0.0, 0.0, 0.0));
		vertices.add(new Vector4D(0.0, 0.0, 0.0, 0.0));
		normals.add(new Vector4D(0.0, 1.0, 0.0, 0.0));
		normals.add(new Vector4D(0.0, -1.0, 0.0, 0.0));
		
		Mesh mesh2 = new DefaultMesh(vertices, normals, triangles, 3);
		
		return MeshOperations.synthesize(mesh1, mesh2);
	}
	
	
}
