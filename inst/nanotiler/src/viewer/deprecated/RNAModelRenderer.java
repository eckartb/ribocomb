package viewer.deprecated;

import rnadesign.rnamodel.*;
import tools3d.objects3d.*;

import javax.media.opengl.GL;
import viewer.graphics.CloneableRenderableModel;

import rnadesign.rnacontrol.Object3DGraphController;

public interface RNAModelRenderer extends CloneableRenderableModel {
	
	/**
	 * Set the graph controller that this RNA renderer will render
	 * @param controller
	 */
	public void setController(Object3DGraphController controller);
	
	/**
	 * Get the graph controller this renderer renders
	 * @return
	 */
	public Object3DGraphController getController();
	
	/**
	 * Determine if the molecules should be rendered using
	 * solids or wireframes
	 * @param wireframeMode
	 */
	public void setWireframeMode(boolean wireframeMode);
	
	/**
	 * Determines if the renderer should render
	 * anything that doesn't have its own
	 * explicit render method.
	 * @return
	 */
	public boolean getRenderOther();
	
	
	/**
	 * Determines if this renderer will render a
	 * branch descriptor
	 * @return
	 */
	public boolean getRenderBranchDescriptor();
	
	/**
	 * Determines if this renderer will render
	 * an atom
	 * @return
	 */
	public boolean getRenderAtom();
	
	
	/**
	 * Determines if this renderer will render a 
	 * nucleotide
	 * @return
	 */
	public boolean getRenderNucleotide();
	

	/**
	 * Determines if this renderer will render
	 * links between objects
	 * @return
	 */
	public boolean getRenderLinks();
	
	/**
	 * Determines if this renderer will render
	 * a kissing loop
	 * @return
	 */
	public boolean getRenderKissingLoop();
	
	/**
	 * Determines if this renderer will render an
	 * RNA strand.
	 * @return
	 */
	public boolean getRenderStrand();
	
	/**
	 * Determines if this renderer will render an
	 * RNA stem
	 * @return
	 */
	public boolean getRenderRNAStem();
	
	
	/**
	 * Determines if this renderer will render a strand
	 * junction
	 * @return
	 */
	public boolean getRenderStrandJunction();
	
	/**
	 * Render a link
	 * @param link TODO
	 * @param gl
	 */
	public void renderLink(Link link, GL gl);
	
	/**
	 * Render a stem
	 * @param stem
	 * @param gl
	 */
	public void renderRNAStem(RnaStem3D stem, GL gl);
	
	/**
	 * Render an rna strand
	 * @param strand
	 * @param gl
	 */
	public void renderRnaStrand(RnaStrand strand, GL gl);
	
	/**
	 * Render a strand junction
	 * @param sj
	 * @param gl
	 */
	public void renderStrandJunction(StrandJunction3D sj, GL gl);
	
	/**
	 * Render a kissing loop
	 * @param kl
	 * @param gl
	 */
	public void renderKissingLoop(KissingLoop3D kl, GL gl);
	
	/**
	 * Render a nucleotide
	 * @param n
	 * @param gl
	 */
	public void renderNucleotide(Nucleotide3D n, GL gl);
	
	/**
	 * Render an atom
	 * @param atom
	 * @param gl
	 */
	public void renderAtom(Atom3D atom, GL gl);
	
	/**
	 * Render a branch descriptor
	 * @param bd
	 * @param gl
	 */
	public void renderBranchDescriptor(BranchDescriptor3D bd, GL gl);
	
	/**
	 * Render an unknown object
	 * @param o
	 * @param gl
	 */
	public void renderOther(Object3D o, GL gl);
	
	public void renderSelected(Object3D o, GL gl);
	
	
}
