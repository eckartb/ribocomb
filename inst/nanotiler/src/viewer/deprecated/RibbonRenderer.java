package viewer.deprecated;

import javax.media.opengl.GL;
import javax.swing.JOptionPane;


import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnamodel.Atom3D;
import rnadesign.rnamodel.BranchDescriptor3D;
import rnadesign.rnamodel.KissingLoop3D;
import rnadesign.rnamodel.Nucleotide3D;
import rnadesign.rnamodel.NucleotideTools;
import rnadesign.rnamodel.RnaStem3D;
import rnadesign.rnamodel.RnaStrand;
import rnadesign.rnamodel.StrandJunction3D;
import rnadesign.rnamodel.Residue3D;
import tools3d.objects3d.Link;
import tools3d.objects3d.Object3D;
import tools3d.Vector4D;
import tools3d.Vector3D;
import viewer.graphics.ColorModel;
import tools3d.splines.StandaloneSplineFactory;
import viewer.graphics.Colorable;
import viewer.graphics.Mesh;
import viewer.graphics.MeshRenderer;
import viewer.graphics.SolidMeshRenderer;
import viewer.graphics.WireframeMeshRenderer;
import viewer.graphics.MeshOperations;
import viewer.graphics.Material;

import viewer.rnadesign.StrandColorModel;
import viewer.util.Pair;

import java.util.ArrayList;
import java.util.List;

import controltools.*;

public class RibbonRenderer implements RNAModelRenderer, Colorable, ModelChangeListener {
	
	private Vector4D[] shape = {
			new Vector4D(1.5, 0.0, 0.0001, 1.0),
			new Vector4D(-1.5, 0.0, 0.0001, 1.0),
			new Vector4D(-1.5, 0.0, -0.0001, 1.0),
			new Vector4D(1.5, 0.0, -0.0001, 1.0)
	};
	
	private static final int REFINEMENT = 3;
	
	private List<Pair<Mesh, Material>> meshes = new ArrayList<Pair<Mesh, Material>>();
	
	private Vector4D shapeNormal = new Vector4D(0.0, 1.0, 0.0, 0.0);
	
	private ColorModel model = new StrandColorModel();
	
	private Object3DGraphController controller;
	
	private boolean wireframeMode = false;

	public Object3DGraphController getController() {
		return controller;
	}
	
	public RibbonRenderer(Object3DGraphController controller) {
		this(controller, null);
	}
	
	public RibbonRenderer() {
		this(null, null);
	}
	
	public RibbonRenderer(Object3DGraphController controller, ColorModel model) {
		this.controller = controller;
		this.model = model;
		
		// controller.getGraph().addModelChangeListener(this);
		controller.addModelChangeListener(this);
	}
	
	public void modelChanged(ModelChangeEvent e) {
		assembleMesh();
	}

	public boolean getRenderAtom() { return false; }
	public boolean getRenderBranchDescriptor() { return false; }
	public boolean getRenderKissingLoop() { return false; }
	public boolean getRenderLinks() { return false; }
	public boolean getRenderNucleotide() { return false; }
	public boolean getRenderOther() { return false; }
	public boolean getRenderRNAStem() { return false; }
	public boolean getRenderStrand() { return true; }
	public boolean getRenderStrandJunction() { return false; }

	public void renderAtom(Atom3D atom, GL gl) { }
	public void renderBranchDescriptor(BranchDescriptor3D bd, GL gl) { }
	public void renderKissingLoop(KissingLoop3D kl, GL gl) { }
	public void renderLink(Link link, GL gl) { }
	public void renderNucleotide(Nucleotide3D n, GL gl) { }
	public void renderOther(Object3D o, GL gl) { }
	public void renderRNAStem(RnaStem3D stem, GL gl) { }
	public void renderRnaStrand(RnaStrand strand, GL gl) { }
	public void renderSelected(Object3D o, GL gl) { }
	public void renderStrandJunction(StrandJunction3D sj, GL gl) { }
	
	private void assembleMesh() {
		meshes.clear();
		assembleMeshes(controller.getGraph().getGraph());
	}
	
	private void assembleMeshes(Object3D o) {
		if(o instanceof RnaStrand)
			assembleStrandMesh((RnaStrand) o);
		
		if(o.size() == 0)
			return;
		
		for(int i = 0; i < o.size() ; ++i)
			assembleMeshes(o.getChild(i));
	}
	
	private void assembleStrandMesh(RnaStrand s) {
		List<Vector3D> backbone = new ArrayList<Vector3D>();
		for(int i = 0; i < s.getResidueCount(); ++i) {
			Residue3D r = s.getResidue3D(i);
			if(r instanceof Nucleotide3D) {
				Nucleotide3D n = (Nucleotide3D) r;
				for(int j = 0; j < n.getAtomCount(); ++j) {
					Atom3D atom = n.getAtom(j);
					if(NucleotideTools.isPhosphate(atom) &&
							NucleotideTools.isBackboneAtom(atom)) {
						backbone.add(atom.getPosition());
						break;
					}
				}
			}
		}
		
//		 check to make sure at least the backbone can be drawn
		if(backbone.size() < 2) {
			JOptionPane.showMessageDialog(null, "Ribbon renderer cannot render molecule. Please switch to a different renderer.");
			return;
		}
		
		Vector3D[] points = new Vector3D[backbone.size()];
		backbone.toArray(points);
		
		StandaloneSplineFactory factory =  new StandaloneSplineFactory();
		Vector3D[] curve = factory.createCubic(points, REFINEMENT);
		
		Mesh m = MeshOperations.extend(shape, shapeNormal, curve);
		meshes.add(Pair.makePair(m, model != null ? model.getMaterial(s) : null));
	}

	

	public void setController(Object3DGraphController controller) {
		this.controller = controller;
		assembleMeshes(controller.getGraph().getGraph());
	}

	public void setWireframeMode(boolean wireframeMode) {
		this.wireframeMode = wireframeMode;

	}

	public void render(GL gl) {
		render(gl, controller.getGraph().getGraph());

	}
	
	private void render(GL gl, Object3D o) {
		if(meshes.size() == 0)
			assembleMesh();
		
		for(Pair<Mesh, Material> m : meshes) {
			if(m.getSecond() != null)
				Material.renderMaterial(gl, m.getSecond());
			MeshRenderer r = wireframeMode ? new WireframeMeshRenderer(m.getFirst()) : new SolidMeshRenderer(m.getFirst());
			r.render(gl);
		}
	}

	public ColorModel getColorModel() {
		return model;
	}

	public void setColorModel(ColorModel model) {
		this.model = model;
		assembleMesh();

	}
	
	public Object clone() {
		try {
			return super.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

}
