package viewer.deprecated;

import javax.media.opengl.GL;
import javax.swing.*;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.LinkController;
import rnadesign.rnamodel.Atom3D;
import rnadesign.rnamodel.BranchDescriptor3D;
import rnadesign.rnamodel.KissingLoop3D;
import rnadesign.rnamodel.Nucleotide3D;
import rnadesign.rnamodel.RnaStem3D;
import rnadesign.rnamodel.RnaStrand;
import rnadesign.rnamodel.StrandJunction3D;
import tools3d.objects3d.Link;
import tools3d.objects3d.Object3D;
import tools3d.Vector3D;
import tools3d.Vector4D;
import tools3d.Matrix4D;
import viewer.graphics.ColorModel;
import viewer.graphics.Colorable;
import viewer.graphics.Material;
import viewer.graphics.Mesh;
import viewer.graphics.SolidMeshRenderer;
import viewer.graphics.WireframeMeshRenderer;
import viewer.graphics.PrimitiveFactory;
import viewer.graphics.MeshRenderer;
import viewer.graphics.Configurable;
import viewer.util.Tools;

public class StickRenderer implements RNAModelRenderer, Colorable, Configurable {
	
	private Object3DGraphController controller;
	private ColorModel colorModel;
	private boolean wireframeMode = false;
	private static final Mesh CYLINDER = PrimitiveFactory.generateCylinder(new Vector4D(1.0, 0.0, 0.0, 0.0), 0.5, 2.0, 1, 8);
	
	public static enum RenderMode {
		Line, Cylinder, Capsule
	}
	
	private RenderMode renderMode;
	
	public StickRenderer(Object3DGraphController controller) {
		this.controller = controller;
		renderMode = RenderMode.Cylinder;
	}
	
	public StickRenderer() {
		this(null);
	}
	
	public RenderMode getRenderMode() {
		return renderMode;
	}
	
	public void setRenderMode(RenderMode renderMode) {
		this.renderMode = renderMode;
	}

	public Object3DGraphController getController() {
		return controller;
	}

	public boolean getRenderAtom() {
		return false;
	}

	public boolean getRenderBranchDescriptor() {
		return false;
	}

	public boolean getRenderKissingLoop() {
		return false;
	}

	public boolean getRenderLinks() {
		return true;
	}

	public boolean getRenderNucleotide() {
		return false;
	}

	public boolean getRenderOther() {
		return false;
	}

	public boolean getRenderRNAStem() {
		return false;
	}

	public boolean getRenderStrand() {
		return false;
	}

	public boolean getRenderStrandJunction() {
		return false;
	}

	public void renderAtom(Atom3D atom, GL gl) {

	}

	public void renderBranchDescriptor(BranchDescriptor3D bd, GL gl) {

	}

	public void renderKissingLoop(KissingLoop3D kl, GL gl) {

	}
	
	

	public void launchConfigurationGUI() {
		Object result = JOptionPane.showInputDialog((JDialog) null, "Select Rendering Mode", "Rendering Modes", JOptionPane.PLAIN_MESSAGE, null, RenderMode.values(), renderMode);
		if(result != null) {
			renderMode = (RenderMode) result;
		}
	}

	public void renderLink(Link link, GL gl) {
		Material material = colorModel.getMaterial(link);
		
		gl.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, material.getAmbient().buffer());
		gl.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, material.getDiffuse().buffer());
		gl.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, material.getSpecular().buffer());
		gl.glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, material.getEmissive().buffer());
		gl.glMaterialf(GL.GL_FRONT, GL.GL_SHININESS, (float) material.getHighlight());
		
		Object3D o1 = link.getObj1();
		Object3D o2 = link.getObj2();
		
		switch(renderMode) {
		case Cylinder:
			Vector4D axis = new Vector4D(o1.getPosition().minus(o2.getPosition()));
			double length = axis.length() / 2.0;
			Vector4D midpoint = new Vector4D(o1.getPosition().plus(o2.getPosition()));
			midpoint = midpoint.mul(0.5);
			midpoint.setW(1.0);
			axis.setW(0.0);
			axis.normalize();
			MeshRenderer renderer;
			if(wireframeMode) {
				renderer = new WireframeMeshRenderer(CYLINDER);
			}
			else {
				renderer = new SolidMeshRenderer(CYLINDER);
			}
			gl.glEnable(GL.GL_NORMALIZE);
			gl.glPushMatrix();
			gl.glTranslated(midpoint.getX(), midpoint.getY(), midpoint.getZ());
			Vector4D rotationAxis = axis.cross(Tools.X_AXIS);
			double angle = Math.acos(axis.dot(Tools.X_AXIS));
			rotationAxis.normalize();
			Matrix4D rotation = Tools.getRotationMatrix(rotationAxis, angle);
			double[] matrix = rotation.getArray();
			gl.glMultMatrixd(matrix, 0);
			gl.glScaled(length, 1.0, 1.0);
			renderer.render(gl);
			gl.glPopMatrix();
			gl.glDisable(GL.GL_NORMALIZE);
			
			break;
		case Capsule:
		case Line:
			gl.glBegin(GL.GL_LINES);
			gl.glVertex3d(o1.getPosition().getX(), o1.getPosition().getY(), o1.getPosition().getZ());
			gl.glVertex3d(o2.getPosition().getX(), o2.getPosition().getY(), o2.getPosition().getZ());
			gl.glEnd();
			break;
		}
		
	}

	public void renderNucleotide(Nucleotide3D n, GL gl) {

	}

	public void renderOther(Object3D o, GL gl) {

	}

	public void renderRNAStem(RnaStem3D stem, GL gl) {

	}

	public void renderRnaStrand(RnaStrand strand, GL gl) {

	}

	public void renderSelected(Object3D o, GL gl) {
		// TODO Auto-generated method stub

	}

	public void renderStrandJunction(StrandJunction3D sj, GL gl) {
		// TODO Auto-generated method stub

	}

	public void setController(Object3DGraphController controller) {
		// TODO Auto-generated method stub

	}

	public void setWireframeMode(boolean wireframeMode) {
		this.wireframeMode = wireframeMode;

	}

	public void render(GL gl) {
		gl.glLineWidth(2.0f);
		LinkController linkController = controller.getLinks();
		for(int i = 0; i < linkController.size(); ++i) {
			renderLink(linkController.get(i), gl);
		}
	}

	public ColorModel getColorModel() {
		return colorModel;
	}

	public void setColorModel(ColorModel model) {
		this.colorModel = model;

	}
	
	public Object clone() {

		try {
			return super.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

}
