package viewer.display;

import java.awt.Color;

import javax.swing.JPanel;
import java.awt.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import viewer.event.DisplayEvent;
import viewer.event.DisplayViewChangedListener;
import viewer.graphics.RenderableModel;
import viewer.graphics.CloneableRenderableModel;
import viewer.graphics.RenderableAggregator;

/**
 * A multi display contains and controls multiple displays. It enables
 * a model to be viewed from multiple orientations.
 * @author Calvin
 *
 */
public class MultiDisplay extends JPanel implements RenderableAggregator {
	
	private RenderableModel model;
	
	private LinkedList<Display> displays =
		new LinkedList<Display>();
	
	private ArrayList<DisplayViewChangedListener> listeners =
		new ArrayList<DisplayViewChangedListener>();
	
	/**
	 * Adds a display view changed listener. Listener is notified
	 * whenever a view inside the multidisplay changes.
	 * @param l Listener to add
	 */
	public void addDisplayViewChangedListener(DisplayViewChangedListener l) {
		listeners.add(l);
	}
	
	/**
	 * Removes a display view changed listener.
	 * @param l Listenr to remove
	 * @return Returns true if the listener is removed and false if otherwise.
	 */
	public boolean removeDisplayViewChangedListener(DisplayViewChangedListener l) {
		return listeners.remove(l);
	}
	
	/**
	 * Notify all attached listeners that a display
	 * view has changed.
	 * @param id Id to pass to listeners
	 * @param d Display that changed
	 */
	public void notifyDisplayViewChanged(int id, Display d) {
		for(DisplayViewChangedListener l : listeners)
			l.displayChanged(new DisplayEvent(this, id, d));
	}
	
	/**
	 * Register a display with the multi display.
	 * This method does not actually add the display
	 * to the multi display panel.
	 * @param d Display to add.
	 */
	public void registerDisplay(Display d) {
		if(d.getOwner() != null)
			d.getOwner().unregisterDisplay(d);
		
		d.setOwner(this);
		displays.add(d);
		
		
		notifyDisplayViewChanged(DisplayEvent.VIEW_ADDED, d);
	}
	
	/**
	 * Unregisters a display from the multi display
	 * @param d Display to unregister
	 * @return Returns true if the display was successfully
	 * unregistered or false if otherwise.
	 */
	public boolean unregisterDisplay(Display d) {
		d.setOwner(null);
		notifyDisplayViewChanged(DisplayEvent.VIEW_REMOVED, d);
		
		return displays.remove(d);
	}

	/**
	 * Sets the grid color of all the displays in the multi display
	 * @param c Color to set the grid to
	 */
	public void setGridColor(Color c) {
		for(Display d : displays)
			d.setGridColor(c);
	}


	/**
	 * Sets whether or not the grid is visible in all
	 * the displays
	 * @param gridVisible True if the grid is visible and 
	 * false if otherwise
	 */
	public void setGridVisible(boolean gridVisible) {
		for(Display d : displays)
			d.setGridVisible(gridVisible);

	}

	/**
	 * Sets the model for each display contained within
	 * the multi display. Note that this model is <b>shared</b>
	 * amongst all the displays so a change to the model
	 * will be reflected in each display.
	 * @param model Model that each display renders
	 */
	public void setModel(RenderableModel model) {
		this.model = model;
		for(Display d : displays)
			d.setModel(model);
	}
	
	/**
	 * Sets the model for each display contained within
	 * the multidisplay. Each display gets a clone of the model
	 * and therefore allows each display to manipulate the model.
	 * @param model Model that each display renders
	 */
	public void setModelClones(CloneableRenderableModel model) {
		this.model = model;
		for(Display d : displays) {
			d.setModel((RenderableModel) model.clone());
		}
	}
	
	/**
	 * Get the model each display is displaying. Note
	 * that if the displays have cloned models, this
	 * model will not reflect the model in each display.
	 * It is actually the original model passed to the
	 * multi display
	 * @return Model of the multidisplay
	 */
	public RenderableModel getModel() {
		return model;
	}
	
	
	public void remove(Component c) {
		if(c instanceof PerspectiveDisplay || c instanceof OrthogonalDisplay)
			displays.remove(c);
		
		super.remove(c);
	}
	
	public void removeAll() {
		super.removeAll();
		displays.clear();
	}
	
	/**
	 * Get the displays this multidisplay contains and manages
	 * @return Returns List of displays
	 */
	public List<Display> getDisplays() {
		return new LinkedList<Display>(displays);
	}
	
	public List<RenderableModel> getRenderables() {
		if(getDisplays().size() > 0)
			return getDisplays().get(0).getRenderables();		return null;
	}
	
	public void addRenderable(Collection<RenderableModel> models) {
		for(Display d : getDisplays())
			d.addRenderable(models);
	}
	
	public void addRenderable(RenderableModel model) {
		for(Display d : getDisplays())
			d.addRenderable(model);
	}
	
	public boolean removeRenderable(RenderableModel model) {
		boolean allReturnedTrue = true;
		for(Display d : getDisplays())
			if(!d.removeRenderable(model))
				allReturnedTrue = false;
		
		return allReturnedTrue;
	}
	
	public void removeAllRenderables() {
		for(Display d : getDisplays())
			d.removeAllRenderables();
	}



}
