package viewer.display;

import viewer.graphics.*;
import java.awt.Color;
import java.awt.Dimension;

import javax.media.opengl.GL;
import javax.media.opengl.GLContext;
import javax.media.opengl.GLJPanel;
import java.awt.event.*;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Collection;

import javax.media.opengl.*;

import tools3d.Vector4D;
import java.awt.Point;

import viewer.components.ViewManipulator.RotateListener;
import viewer.components.ViewManipulator.TranslateListener;
import viewer.components.ViewManipulator.ZoomListener;
import viewer.graphics.RenderableModel;
import viewer.view.OrthogonalView;

/**
 * A display is used to render models using open gl. A display
 * supports a number of features, such as having a grid. This
 * class doesn't directly do any of the open gl rendering. It should
 * be subclassed for specific rendering commands. For example, this
 * class provides a method to turn on and off lighting but it doesn't actually
 * do anything. The specific open gl commands to accomplish this should
 * be implemented in a sub class.
 * 
 * <b>Important:</b> Each display needs to be backed by a view.
 * 
 * @see viewer.display.OrthogonalDisplay
 * @see viewer.display.PerspectiveDisplay
 * 
 * @author Calvin
 *
 */
public abstract class Display extends GLJPanel implements Gizmoable,
	RenderableAggregator {
	
	private MultiDisplay owner = null;
	
	private Color xAxisColor = Color.green;
	private Color yAxisColor = Color.red;
	private Color zAxisColor = Color.blue;
	
	private boolean lighting = true;
	private boolean mouseTranslationEnabled = true;
	
	private Gizmo gizmo;
	private boolean gizmoEnabled = false;
	
	private boolean gridVisible = true;
	private Color gridColor = new Color(102,153,255, 127);
	private double gridSize = 10.0;
	private double viewDistance = 2000;
	
	private boolean renderCoordinateAxis = true;
	
	private RenderableModel model = null;
	
	private ArrayList<RenderableModel> renderables =
		new ArrayList<RenderableModel>();
	
	/**
	 * Construct a default display
	 *
	 */
	public Display() {
		addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent e) {
				requestFocusInWindow();
			}
		});
	}
	
	public void setGizmo(Gizmo gizmo) {
		if(this.gizmo != null) {
			removeMouseListener(this.gizmo.getMouseListener());
			removeMouseMotionListener(this.gizmo.getMouseMotionListener());
		}
		this.gizmo = gizmo;
		if(this.gizmo == null)
			gizmoEnabled = false;
		else {
			this.gizmo.setDisplay(this);
			addMouseListener(gizmo.getMouseListener());
			addMouseMotionListener(gizmo.getMouseMotionListener());
		}
	}
	
	public Gizmo getGizmo() {
		return gizmo;
	}
	
	public boolean isGizmoEnabled() {
		return gizmoEnabled;
	}
	
	public void setGizmoEnabled(boolean gizmoEnabled) {
		if(gizmo != null)
			this.gizmoEnabled = gizmoEnabled;
	}
	
	/**
	 * Determines if the view backing this display can
	 * be manipulated using the mouse
	 * @return Returns true if the view can be manipulated
	 * or false if the view cannot be manipulated.
	 */
	public boolean isMouseTranslationEnabled() {
		return mouseTranslationEnabled;
	}
	
	/**
	 * Turns on and off mouse manipulation
	 * @param mouseTranslationEnabled pass in true if mouse translation should
	 * be enabled or false if it should be disabled
	 */
	public void setMouseTranslationEnabled(boolean mouseTranslationEnabled) {
		this.mouseTranslationEnabled = mouseTranslationEnabled;
	}
	
	/**
	 * Set the multidisplay that owns this display.
	 * @param owner MultiDisplay that owns this display or null
	 * if nothing owns it. 
	 */
	public void setOwner(MultiDisplay owner) {
		this.owner = owner;
	}
	
	/**
	 * Get the multidisplay that owns this display.
	 * @return Returns the multidisplay or null if no
	 * multidisplay owns this display
	 */
	public MultiDisplay getOwner() {
		return owner;
	}
	
	/**
	 * Enable lighting in the display
	 * @param lighting True if lighting should be
	 * enabled or false if lighting should be disabled.
	 */
	public void setLighting(boolean lighting) {
		this.lighting = lighting;
		GLContext context = getContext();
		if(context != null) {
			int result = context.makeCurrent();
			if(result == GLContext.CONTEXT_CURRENT) {
				GL gl = context.getGL();
				if(lighting)
					gl.glEnable(GL.GL_LIGHTING);
				else
					gl.glDisable(GL.GL_LIGHTING);
			}
		}
	}
	
	public boolean getLighting() {
		return lighting;
	}
	
	/**
	 * Sets the main model of the display. A display can
	 * have many different rendererers, but only
	 * one model renderer. The actual use of
	 * this is up to client code.
	 * @param model
	 */
	public void setModel(RenderableModel model) {
		if(this.model != null)
			renderables.remove(this.model);
		
		this.model = model;
		if(model != null)
			renderables.add(model);
	}
	
	/**
	 * Sets the main model of the display. Optionally
	 * adds it to the list of renderers. This should
	 * be used if a client wants to change the model
	 * to a different renderable already added. Use
	 * this to avoid model being added twice
	 * @param model
	 * @param add Add model to renderable list
	 */
	public void setModel(RenderableModel model, boolean add) {
		if(add)
			setModel(model);
		else
			this.model = model;
	}
	
	/**
	 * A display can only have one model. The actual meaning
	 * of the model is left to client code. This method
	 * returns the model the display is currently rendering.
	 * @return
	 */
	public RenderableModel getModel() {
		return model;
	}
	
	/**
	 * Does the display render coordinate axis
	 * @return Returns true if the display renders coordinate
	 * axis or false if it does not.
	 */
	public boolean isRenderCoordinateAxis() {
		return renderCoordinateAxis;
	}

	/**
	 * Render the coordinate axis
	 * @param renderCoordinateAxis Pass in true if the axis should
	 * be renderered or false if the axis should not be rendererd.
	 */
	public void setRenderCoordinateAxis(boolean renderCoordinateAxis) {
		this.renderCoordinateAxis = renderCoordinateAxis;
	}

	/**
	 * Get the zoom listener this display uses
	 * to zoom the view. Dependent on the view
	 * @return Returns the zoom listener.
	 */
	public abstract ZoomListener getZoomListener();
	
	/**
	 * Get the rotation listener this display uses
	 * to rotate the view. Dependent on the view.
	 * @return Returns the rotation listener.
	 */
	public abstract RotateListener getRotateListener();
	
	/**
	 * Get the translation listener this display uses to
	 * translate the view. Dependent on the view.
	 * @return Returns the translation listener.
	 */
	public abstract TranslateListener getTranslateListener();
	
	/**
	 * Sets the zoom listener this display uses to zoom
	 * the view
	 * @param listener
	 */
	public abstract void setZoomListener(ZoomListener listener);
	
	/**
	 * Sets the rotate listener this display uses to rotate the view
	 * @param listener
	 */
	public abstract void setRotateListener(RotateListener listener);
	
	/**
	 * Sets the translate listener this display uses to translate the view
	 * @param listener
	 */
	public abstract void setTranslateListener(TranslateListener listener);
	
	/**
	 * Get the color of the grid
	 * @return Returns the color of the grid
	 */	
	public Color getGridColor() {
		return gridColor;
	}
	
	/**
	 * Set the color of the grid
	 * @param gridColor The color of the grid
	 */
	public void setGridColor(Color gridColor) {
		this.gridColor = gridColor;
	}
	
	/**
	 * Get the size of each square grid cell.
	 * @return Returns the size
	 */
	public double getGridSize() {
		return gridSize;
	}

	/**
	 * Get the view backing this display
	 * @return Retuns the view
	 */
	public abstract OrthogonalView getView();
	
	/**
	 * Does this display render the grid
	 * @return Returns true if the grid is rendererd
	 * and false if otherwise
	 */
	public boolean isGridVisible() {
		return gridVisible;
	}
	
	/**
	 * Turn on and off grid rendering
	 * @param gridVisible Pass in true if the grid should be rendered and false
	 * if the grid should not be rendered
	 */
	public void setGridVisible(boolean gridVisible) {
		this.gridVisible = gridVisible;
	}
	
	/**
	 * Get the view distance. Exact meaning of this
	 * is up to the sub class.
	 * @return View distance
	 */
	public double getViewDistance() {
		return viewDistance;
	}
	
	/**
	 * Set the size (length and width) of each grid cell
	 * @param gridSize size
	 */
	public void setGridSize(double gridSize) {
		this.gridSize = gridSize;
	}
	
	/**
	 * Set the view distance of the display. Exact meaning of this
	 * is up to the sub class.
	 * @param viewDistance
	 */
	public void setViewDistance(double viewDistance) {
		this.viewDistance = viewDistance;
	}
	
	public List<RenderableModel> getRenderables() {
		return renderables;
	}
	
	public void addRenderable(Collection<RenderableModel> models) {
		renderables.addAll(models);
	}
	
	public void addRenderable(RenderableModel model) {
		renderables.add(model);
		display();
	}
	
	public boolean removeRenderable(RenderableModel model) {
		return renderables.remove(model);
	}
	
	public void removeAllRenderables() {
		renderables.clear();
	}
	
	/**
	 * Convert the point in 2D screen space into a point in 3D space
	 * @param p Point in 2D space to convert to a point in 3D space
	 * @return Returns the point in 3D space.
	 */
	public abstract Vector4D getCoordinates(Point p);

	/**
	 * Get the color of the X axis
	 * @return Returns the color of the X-Axis
	 */
	public Color getXAxisColor() {
		return xAxisColor;
	}

	/**
	 * Set the color of the X axis
	 * @param axisColor color
	 */
	public void setXAxisColor(Color axisColor) {
		xAxisColor = axisColor;
	}

	/**
	 * Get the color of the Y axis
	 * @return Returns the color of the Y axis
	 */
	public Color getYAxisColor() {
		return yAxisColor;
	}

	/**
	 * Set the color of the Y axis
	 * @param axisColor Color
	 */
	public void setYAxisColor(Color axisColor) {
		yAxisColor = axisColor;
	}

	/**
	 * Get the color of the Z axis
	 * @return Returns the color of the Z axis
	 */
	public Color getZAxisColor() {
		return zAxisColor;
	}

	/**
	 * Sets the color the Z axis
	 * @param axisColor color
	 */
	public void setZAxisColor(Color axisColor) {
		zAxisColor = axisColor;
	}
	
	/**
	 * Sets the color the of the background.
	 * @param bg Background color
	 */
	public void setGLBackground(Color bg) {
		super.setBackground(bg);
		GLContext context = getContext();
		if(context != null) {
			int result = context.makeCurrent();
			if(result == GLContext.CONTEXT_CURRENT) {
				GL gl = context.getGL();
				float[] fogColor = { bg.getRed() / 255.0f, bg.getGreen() / 255.0f, bg.getBlue() / 255.0f, 1.0f};
				
				gl.glFogfv(GL.GL_FOG_COLOR, FloatBuffer.wrap(fogColor));
				gl.glClearColor(fogColor[0], fogColor[1] , fogColor[2], 0.0f);
			}
		}
	}
	
	/**
	 * Get the color of the background
	 * @return Returns the color of the background
	 */
	public Color getGLBackground() {
		return getBackground();
	}
	
	
	
	
}
