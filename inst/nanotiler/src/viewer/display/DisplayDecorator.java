package viewer.display;

import javax.swing.JPanel;

import viewer.view.ViewOrientation;

/**
 * Decorates a display and returns a wrapped panel
 * @author Calvin
 *
 */
public interface DisplayDecorator {
	
	/**
	 * Decorates a display. Actual decoration is left up
	 * to implementing classes
	 * @param display Display to decorate
	 * @param orientation Orientation of the display
	 * @return Returns the decorated display as a panel. Note that
	 * while the display could be returned, it is very likely that
	 * a new JPanel instance will be returned that serves as a container
	 * for the display. therefore, all casts should be checked.
	 */
	 public JPanel decorateDisplay(final Display display, ViewOrientation orientation);
}
