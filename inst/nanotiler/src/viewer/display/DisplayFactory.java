package viewer.display;

import java.awt.event.*;

import javax.swing.*;

import viewer.view.LeftHandedViewFactory;
import viewer.view.ViewFactory;
import viewer.view.ViewOrientation;

import java.awt.*;
import java.util.List;
import java.util.ArrayList;

/**
 * Factory that creates displays.
 * @author Calvin
 *
 */
public class DisplayFactory {
	private static ViewFactory viewFactory = new LeftHandedViewFactory();
	private static boolean decorated = true;
	private static boolean resizable = true;
	private static boolean preserveDimensions = false;
	
	
	
	private static DisplayDecorator decorator = new DefaultDecorator();
	
	/**
	 * Default size of the created displays
	 */
	public static final Dimension PREFAB_DIMENSION = new Dimension(800, 600);
	
	
	
	/**
	 * Gets the decorator used to decorate this display
	 * @return Returns the decorator
	 */
	public static DisplayDecorator getDecorator() {
		return decorator;
	}
	
	/**
	 * Sets the decorator to use to decorate displays
	 * @param decorator Decorator to use to decorate displays
	 */
	public static void setDecorator(DisplayDecorator decorator) {
		DisplayFactory.decorator = decorator;
	}
	
	/**
	 * Prefabricated Multi Displays. These enumeration values
	 * can be used to create canned multi displays using certain defaults
	 * and layout schemes.
	 * @author Calvin
	 *
	 */
	public static enum PrefabricatedDisplays {
		FourQuadrants(4), VerticalSplit(2), HorizontalSplit(2), TopAndTwoBottoms(3),
		Single(1), BottomAndTwoTops(3), LeftAndTwoRights(3), RightAndTwoLefts(3);
		
		private int viewCount;
		
		/**
		 * Get the number of views a display has.
		 * @return
		 */
		public int getViewCount() {
			return viewCount;
		}
		
		private PrefabricatedDisplays(int viewCount) {
			this.viewCount = viewCount;
		}
		
		private static final int[][] FOUR_QUADRANTS = { {1, 2}, {3, 4} };
		private static final int[][] VERTICAL_SPLIT = { {1, 2} };
		private static final int[][] HORIZONTAL_SPLIT = { {1}, {2} };
		private static final int[][] TOP_AND_TWO_BOTTOMS = { {1, 1}, {2, 3} };
		private static final int[][] SINGLE = { {1} };
		private static final int[][] BOTTOM_AND_TWO_TOPS = { {1, 2}, {3, 3} };
		private static final int[][] LEFT_AND_TWO_RIGHTS = { {1, 2}, {1, 3} };
		private static final int[][] RIGHT_AND_TWO_LEFTS = { {1, 2}, {3, 2} };
		
		private static final Dimension DEFAULT_SIZE = new Dimension(800, 600);
		private static final ViewOrientation[] DEFAULT_ORIENTATIONS = {
			ViewOrientation.PerspectiveTop,
			ViewOrientation.Top,
			ViewOrientation.Left,
			ViewOrientation.Back
		};
		
		/**
		 * Get the default MultiDisplay
		 * @return Returns a MultiDisplay created using Factory defaults
		 */
		public MultiDisplay getDefaultDisplay() {
			return getDisplay(DEFAULT_ORIENTATIONS);
		}
		
		/**
		 * Get a multi display using the orientations in each of the view windows
		 * @param orientations
		 * @return Returns the multi display with the provided orientations
		 */
		public MultiDisplay getDisplay(ViewOrientation[] orientations) {
			return getDisplay(orientations, DEFAULT_SIZE);
		}
		
		/**
		 * Get a multi display that uses specific orientations and is of a specific size.
		 * @param orientations Orientations to use to create the various views of the multi display
		 * @param size Size of the multi display
		 * @return Returns a multi display
		 */
		public MultiDisplay getDisplay(ViewOrientation[] orientations, Dimension size) {
			switch(this) {
			case FourQuadrants:
				return createDisplay(new DisplayGridLayout(FOUR_QUADRANTS, orientations, size));
			case VerticalSplit:
				return createDisplay(new DisplayGridLayout(VERTICAL_SPLIT, orientations, size));
			case HorizontalSplit:
				return createDisplay(new DisplayGridLayout(HORIZONTAL_SPLIT, orientations, size));
			case TopAndTwoBottoms:
				return createDisplay(new DisplayGridLayout(TOP_AND_TWO_BOTTOMS, orientations, size));
			case Single:
				return createDisplay(new DisplayGridLayout(SINGLE, orientations, size));
			case BottomAndTwoTops:
				return createDisplay(new DisplayGridLayout(BOTTOM_AND_TWO_TOPS, orientations, size));
			case LeftAndTwoRights:
				return createDisplay(new DisplayGridLayout(LEFT_AND_TWO_RIGHTS, orientations, size));
			case RightAndTwoLefts:
				return createDisplay(new DisplayGridLayout(RIGHT_AND_TWO_LEFTS, orientations, size));
			}
			
			return null;
		}
		
	}
	
	/**
	 * Sets the view factory the display factory uses to create views based
	 * upon view orientations
	 * @param viewFactory Factory to use to create views.
	 */
	public static void setViewFactory(ViewFactory viewFactory) {
		DisplayFactory.viewFactory = viewFactory;
	}
	
	/**
	 * Get the factory used to create views for the displays
	 * @return Factory
	 */
	public static ViewFactory getViewFactory() {
		return viewFactory;
	}
	
	/**
	 * Should the factory decorate displays
	 * @return Return true if the factory decorates displays
	 * or false if the factory does not decorate displays
	 */
	public static boolean isDecorated() {
		return decorated;
	}

	/**
	 * Set if the factory should decorate displays
	 * @param decorated pass in true if the factory should
	 * decorate displays or false if the factory
	 * should not decorate displays.
	 */
	public static void setDecorated(boolean decorated) {
		DisplayFactory.decorated = decorated;
	}
	
	/**
	 * Can each individual display be resized
	 * @return Returns true if each individual view in the
	 * created multi display can be resized and false if otherwise
	 */
	public static boolean isResizable() {
		return resizable;
	}
	
	/**
	 * Sets if the factory can create multi displays
	 * whose displays can be resized.
	 * @param resizable True if the factory should
	 * create Multi Displays whose displays can
	 * be resized or false if the displays should
	 * not be able to be resized.
	 */
	public void setResizable(boolean resizable) {
		DisplayFactory.resizable = resizable;
	}
	
	/**
	 * Not used anywhere in the factory
	 * @param preserveDimensions
	 */
	public void setPreserveDimensions(boolean preserveDimensions) {
		DisplayFactory.preserveDimensions = preserveDimensions;
	}
	
	/**
	 * Not used anywhere in the factory
	 * @return
	 */
	public boolean getPreserveDimensions() {
		return preserveDimensions;
	}
	
	/**
	 * Create a display with the specified orientation
	 * @param orientation Orientation to create display with
	 * @return Returns the created display with the specific orientaiton
	 */
	public static Display createDisplay(ViewOrientation orientation) {
		return createDisplay(orientation, false);
	}
	
	/**
	 * Create a display with the specified orientation
	 * @param orientation Orientation to create display with
	 * @param manipulatable not used
	 * @return Returns the created display
	 */
	public static Display createDisplay(ViewOrientation orientation, boolean manipulatable) {
		Display display = null;
		switch(orientation) {
		case Left:
			display = new OrthogonalDisplay(viewFactory.createLeftView());
			break;
		case Right:
			display = new OrthogonalDisplay(viewFactory.createRightView());
			break;
		case Top:
			display = new OrthogonalDisplay(viewFactory.createTopView());
			break;
		case Bottom: 
			display = new OrthogonalDisplay(viewFactory.createBottomView());
			break;
		case Front:
			display = new OrthogonalDisplay(viewFactory.createFrontView());
			break;
		case Back:
			display = new OrthogonalDisplay(viewFactory.createBackView());
			break;
		case PerspectiveTop:
			display = new PerspectiveDisplay(viewFactory.createPerspectiveView(LeftHandedViewFactory.TOP_PERSPECTIVE_VIEW));
			break;
		case PerspectiveHorizon:
			display = new PerspectiveDisplay(viewFactory.createPerspectiveView(LeftHandedViewFactory.HORIZON_PERSPECTIVE_VIEW));
			break;
		}
		
		if(!manipulatable)
			return display;
		
		return display;
	}
	
	
	private static JPanel decorateDisplay(Display display, ViewOrientation orientation) {
		return decorator.decorateDisplay(display, orientation);
	}
	
	/**
	 * Synthesize a MultiDisplay using a request
	 * @param request Request providing instructions as to how to create and layout
	 * the multi display
	 * @return Returns the synthesized multi display
	 */
	public static MultiDisplay createDisplay(DisplayLayoutRequest request) {
		MultiDisplay panel = new MultiDisplay();
		panel.setPreferredSize(request.getDisplaySize());
		int numberViews = request.getNumberViews();
		
		// ensure that none of the view rectangles overlap
		for(int i = 0; i < numberViews; ++i) {
			for(int j = i + 1; j < numberViews; ++j) {
				Rectangle v1 = request.getViewBounds(i);
				Rectangle v2 = request.getViewBounds(j);
				if(v1.intersects(v2)) {
					throw new InvalidRequest("View bounds intersect with one another");
				}
			}
		}
		
		
		List<JPanel> panels = new ArrayList<JPanel>();
		// create actual panels for each 
		for(int i = 0; i < numberViews; ++i) {
			JPanel displayPanel = new JPanel(new BorderLayout());
			Display display = createDisplay(request.getViewOrientation(i), true);
			panel.registerDisplay(display);
			
			panel.registerDisplay(display);
			
			if(decorated) {
				displayPanel = decorateDisplay(display, request.getViewOrientation(i));
					
			}
			
			else {
				displayPanel.add(display);
			}
			
			displayPanel.setBounds(request.getViewBounds(i));
			displayPanel.setPreferredSize(request.getViewBounds(i).getSize());
			panels.add(displayPanel);
			
			
			
			
		}
		
		// if resizable, compute panel adjacencies
		// stretches panel to fill empty space and
		// may readjust adjacent panel sizes to match
		if(resizable) {
			assemble(panels);
		}
		
		for(JPanel p : panels)
			panel.add(p);
		
		// this is the ideal...we don't want a custom layout manager
		if(panels.size() == 1) {
			panel.setLayout(new BorderLayout());
			panel.add(panels.get(0));
			return panel;
		}
		
		
		
		// arrange remainder of panels using a custom layout
		// if the ideal isn't met
		final Dimension requestDimension = request.getDisplaySize();
		panel.setLayout(new LayoutManager() {

			public void addLayoutComponent(String s, Component c) {
				// TODO Auto-generated method stub
				
			}

			public void layoutContainer(Container container) {
				Dimension size = container.getSize();
				double scaleWidth = size.width / (double) requestDimension.width;
				double scaleHeight = size.height / (double) requestDimension.height;
				for(Component c : container.getComponents()) {
					// scale all the components' bounds
					Point location = c.getLocation();
					Dimension d = c.getPreferredSize();
					c.setLocation((int) (scaleWidth * location.x), (int) (scaleHeight * location.y));
					c.setSize((int) (scaleWidth * d.width), (int) (scaleHeight * d.height));
				}
			}

			public Dimension minimumLayoutSize(Container c) {
				return preferredLayoutSize(c);
			}

			public Dimension preferredLayoutSize(Container c) {
				int maxX = 0, maxY = 0;
				for(Component component : c.getComponents()) {
					Dimension d = component.getPreferredSize();
					if(component.getX() + (int) d.getWidth() > maxX)
						maxX = component.getX() + (int) d.getWidth();
					if(component.getY() + (int) d.getHeight() > maxY)
						maxY = component.getY() + (int) d.getHeight();
				}
				
				return new Dimension(maxX, maxY);
			}

			public void removeLayoutComponent(Component c) {
				
			}
			
		});
		
		return panel;
	}
	
	// assemble into resizable units until either there
	// is only one panel left or no panels share a common
	// side
	private static void assemble(List<JPanel> panels) {
		if(panels.size() == 1)
			return;
		
		JPanel panel1 = null;
		JPanel panel2 = null;
		boolean breakOut = false;
		
		//System.out.println("Starting assemble");
		
		for(int i = 0; i < panels.size(); ++i) {
			for(int j = i + 1; j < panels.size(); ++j) {
				Rectangle r1 = panels.get(i).getBounds();
				Rectangle r2 = panels.get(j).getBounds();
				
				//System.out.println("Testing Bounds:\n\t" + r1 + "\n\t" + r2);
				
				// rectangles share the same dimensions
				if(r1.x == r2.x && r1.width == r2.width &&
					(r1.y - r2.y == r2.height || r2.y - r1.y == r1.height)) {
					//System.out.println("Panels share width");
					if(r1.y < r2.y) {
						panel1 = panels.get(i);
						panel2 = panels.get(j);
					}
					else {
						panel1 = panels.get(j);
						panel2 = panels.get(i);
					}
					panels.remove(panel1);
					panels.remove(panel2);
					
					final JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, panel1, panel2);
					splitPane.setOneTouchExpandable(true);
					final JPanel panel = new JPanel(new BorderLayout());
					panel.add(splitPane);
					splitPane.addHierarchyListener(new HierarchyListener() {

						public void hierarchyChanged(HierarchyEvent e) {
				
							if(e.getChangeFlags() == HierarchyEvent.SHOWING_CHANGED) {
								splitPane.setDividerLocation(.5);
								splitPane.removeHierarchyListener(this);
							}
						}
						
					});
					
					Dimension newSize = new Dimension(r1.width, r1.height + r2.height);
					panel.setSize(newSize);
					panel.setPreferredSize(newSize);
					panel.setLocation(r1.x, r1.y > r2.y ? r2.y : r1.y);
					
					//System.out.println("Combined panel bounds: " + panel.getBounds());
					
					panels.add(panel);
					breakOut = true;
					break;
				}
				
				else if(r1.y == r2.y && r1.height == r2.height && 
					(r1.x - r2.x == r2.width || r2.x - r1.x == r1.width)) {
					//System.out.println("Panels share height");
					if(r1.x < r2.x) {
						panel1 = panels.get(i);
						panel2 = panels.get(j);
					}
					else {
						panel1 = panels.get(j);
						panel2 = panels.get(i);
					}
					
					panel1 = panels.get(i);
					panel2 = panels.get(j);
					panels.remove(panel1);
					panels.remove(panel2);
					
					//System.out.println("Removed: ");
					//System.out.println("" + panel1 + "\n" + panel2);
					
				
					
					final JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, panel1, panel2);
					final JPanel panel = new JPanel(new BorderLayout());
					panel.add(splitPane);
					splitPane.setOneTouchExpandable(true);
					splitPane.addHierarchyListener(new HierarchyListener() {
					
						public void hierarchyChanged(HierarchyEvent e) {
							if(e.getChangeFlags() == HierarchyEvent.SHOWING_CHANGED) {
								splitPane.setDividerLocation(.5);
								splitPane.removeHierarchyListener(this);
							}
						}
						
					});
					
					Dimension newSize = new Dimension(r1.width + r2.width, r1.height);
					panel.setSize(newSize);
					panel.setPreferredSize(newSize);
					panel.setLocation(r1.x > r2.x ? r2.x : r1.x, r1.y);
					
					//System.out.println("Combined panel bounds: " + panel.getBounds());
					
					panels.add(panel);
					breakOut = true;
					break;
				}
				
		
			}
			
			
			if(breakOut)
				break;
		}
	
		
		if(breakOut)
			assemble(panels);
	}
	
	/**
	 * If a request to create a MutliDisplay is invalid, either
	 * because bounds overlap or other information doesn't work,
	 * this exception is thrown.
	 * @author Calvin
	 *
	 */
	public static class InvalidRequest extends RuntimeException {
		public InvalidRequest(String s) {
			super(s);
		}
	}
}
