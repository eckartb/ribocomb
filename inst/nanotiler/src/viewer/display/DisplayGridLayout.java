package viewer.display;

import java.util.*;
import java.awt.Dimension;
import java.awt.Rectangle;

import viewer.view.ViewOrientation;

/**
 * A display grid layout lays out displays in a grid pattern
 * based upon a passed in array.
 * @author Calvin
 *
 */
public class DisplayGridLayout implements DisplayLayoutRequest {
	
	private Dimension displaySize;
	private ArrayList<Rectangle> viewBounds = 
		new ArrayList<Rectangle>();
	private ViewOrientation[] orientations;
	
	/**
	 * Construct a display grid layout request
	 * @param grid Grid to construct layout from. An example of a grid that has
	 * four windows, each with the same size is shown below:
	 * <br />
	 * { { 1, 2} , { 3, 4} }
	 * <br />
	 * Each int[] in the grid parameter denotes a row of displays. Each number
	 * denotes a seperate display. For displays that span multiple rows and multiple
	 * colums, the same number should be used. For example:
	 * <br />
	 * { {1, 2}, {1, 2} }
	 * <br />
	 * This grid will construct two displays; one on the left and one on the right
	 * @param orientations The orientations array should have as many elements
	 * as numbers specified in the grid parameter. Each orientation in the orientation
	 * array gets mapped to a specific grid location in the grid parameter.
	 * @param displaySize Size of the display that should be constructed
	 */
	public DisplayGridLayout(int[][] grid, ViewOrientation[] orientations, Dimension displaySize) {
		this.orientations = orientations;
		this.displaySize = displaySize;
		
		createViewsFromGrid(grid);
		
		if(viewBounds.size() > orientations.length)
			throw new IllegalArgumentException();
	}

	public String getDescription() {
		return "Grid Layout";
	}

	public Dimension getDisplaySize() {
		return displaySize;
	}

	public int getNumberViews() {
		return viewBounds.size();
	}

	public Rectangle getViewBounds(int view) {
		return viewBounds.get(view);
	}

	public ViewOrientation getViewOrientation(int view) {
		return orientations[view];
	}

	/**
	 * Magic happens here. Converts the grid into
	 * the actual request by computing bounds and sizes from
	 * the grid.
	 * @param grid
	 */
	private void createViewsFromGrid(int[][] grid) {
		boolean[][] seen = new boolean[grid.length][];
		for(int i = 0; i < grid.length; ++i)
			seen[i] = new boolean[grid[i].length];
		
		int x1 = 0, y1 = 0;
		double xDivision = displaySize.width / grid[0].length;
		double yDivision = displaySize.height / grid.length;
		
		// iterate through the seen array
		for(int i = 0; i < seen.length; ++i) {
			y1 = (int) (yDivision * i);
			for(int j = 0; j < seen[i].length; ++j) {
				x1 = (int) (xDivision * j);
				//System.out.println("Seen: " + seen[i][j]);
				if(seen[i][j])
					continue;
				
				
				// found a square that hasn't been examined yet
				int view = grid[i][j];
				
				//System.out.println("View: " + view);
				
				// expand down and to the right to see how
				//   far it stretches
				
				// do down first
				int k = i + 1;
				while(k < grid.length && grid[k][j] == view)
					++k;
				
				
				// now do right
				int l = j + 1;
				while(l < grid[i].length && grid[i][l] == view)
					++l;
				
				
				// k and l represent one past the square bound
				
				// compute bounds
				int height = ((int) (k * yDivision)) - y1;
				int width = ((int) (l * xDivision)) - x1;
				
				
				viewBounds.add(new Rectangle(x1, y1, width, height));
				
				// udpate seen array
				for(int m = i; m < k; ++m)
					for(int n = j; n < l; ++n) {
						//System.out.println("Setting " + m + "," + n + " to true");
						seen[m][n] = true;
					}
			}
		}
		
	}
	

}
