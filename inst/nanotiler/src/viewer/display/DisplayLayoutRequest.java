package viewer.display;

import java.awt.*;

import viewer.view.ViewOrientation;

/**
 * A display can be built by querying the request for information
 * @author calvin
 *
 */
public interface DisplayLayoutRequest {
	
	/**
	 * Get description of the request
	 * @return
	 */
	public String getDescription();
	
	/**
	 * Get the number of views this display should create
	 * @return
	 */
	public int getNumberViews();
	
	/**
	 * Get the orientation of the view
	 * @param view
	 * @return
	 */
	public ViewOrientation getViewOrientation(int view);
	
	/**
	 * Get the bounds of the view
	 * @param view
	 * @return
	 */
	public Rectangle getViewBounds(int view);
	
	/**
	 * Get the dimension of the display
	 * @return
	 */
	public Dimension getDisplaySize();
}
