package viewer.display;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.TitledBorder;

import viewer.graph.rna.RnaRendererGraphController;
import viewer.graphics.*;
import viewer.rnadesign.*;
import rnadesign.rnamodel.RnaTools;

/**
 * A GUI interface for adjusting display options such
 * as background color and the grid.
 * @author Calvin
 *
 */
public class DisplayOptions extends JDialog {
	
	private Display display;
	private static final String LOD_COARSE = "Coarse";
	private static final String LOD_MEDIUM = "Medium";
	private static final String LOD_FINE = "Fine";

	/**
	 * Construct display options
	 * @param display Display that the options manipulate
	 * @throws HeadlessException
	 */
	public DisplayOptions(Display display) throws HeadlessException {
		super((Frame) null, "Display Options", true);
		
		this.display = display;
	}
	
	/**
	 * Create and show the GUI
	 *
	 */
	public void createAndShowGUI() {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		JTabbedPane pane = new JTabbedPane();
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		final JColorChooser backgroundChooser = new JColorChooser(display.getBackground());
		backgroundChooser.getSelectionModel().addChangeListener(new ChangeListener() {

			public void stateChanged(ChangeEvent e) {
				display.setGLBackground(backgroundChooser.getColor());
				display.display();
			}
			
		});
		
		backgroundChooser.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black), "Background Color", TitledBorder.LEFT, TitledBorder.TOP));
		
		panel.add(backgroundChooser);
		
		pane.addTab("Background", panel);
		
		
		panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		

		final JColorChooser gridChooser = new JColorChooser(display.getGridColor());
		gridChooser.getSelectionModel().addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				display.setGridColor(gridChooser.getColor());
				display.display();
			}
		});
		
		JPanel flow = new JPanel();
		JLabel label = new JLabel("Grid spacing");
		flow.add(label);
		
		final JTextField sizeField = new JTextField(5);
		sizeField.setEditable(false);
		sizeField.setText("" + display.getGridSize());
		final JSlider gridSize = new JSlider();
		gridSize.setMinimum(1);
		gridSize.setMaximum(100);
		gridSize.setSnapToTicks(true);
		gridSize.setPaintLabels(false);
		gridSize.setPaintTicks(true);
		gridSize.setMajorTickSpacing(10);
		gridSize.setMinorTickSpacing(5);
		gridSize.setValue((int) (display.getGridSize()));
		gridSize.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if(gridSize.getValueIsAdjusting())
					return;
				double size = gridSize.getValue();
				display.setGridSize(size);
				sizeField.setText("" + size);
				display.display();
			}
		});
		
		flow.add(gridSize);
		flow.add(sizeField);
		//flow.setAlignmentX(SwingConstants.LEFT);
		
		gridChooser.setEnabled(display.isGridVisible());
		gridChooser.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black), "Background Color", TitledBorder.LEFT, TitledBorder.TOP));
		gridChooser.setAlignmentX(JComponent.LEFT_ALIGNMENT);
		
		final JCheckBox gridEnabled = new JCheckBox("Grid Visible");
		gridEnabled.setSelected(display.isGridVisible());
		gridEnabled.setAlignmentX(JComponent.LEFT_ALIGNMENT);
		gridEnabled.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				display.setGridVisible(gridEnabled.isSelected());
				gridChooser.setEnabled(gridEnabled.isSelected());
				gridSize.setEnabled(gridEnabled.isSelected());
				sizeField.setEnabled(gridEnabled.isSelected());
				display.display();
			}
		});
		
		panel.add(gridEnabled);
		panel.add(Box.createVerticalStrut(15));
		
		flow.setAlignmentX(JComponent.LEFT_ALIGNMENT);
		
		panel.add(flow);
		
		panel.add(Box.createVerticalStrut(15));
		
		panel.add(gridChooser);
		
		pane.addTab("Grid", panel);
		
		panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		flow = new JPanel();
		flow.setLayout(new BoxLayout(flow, BoxLayout.X_AXIS));
		label = new JLabel("Graphical Detail");
		flow.add(label);
		flow.add(Box.createHorizontalStrut(15));
		final Object[] objects = { LOD_COARSE, LOD_MEDIUM, LOD_FINE };
		final JComboBox lodBox = new JComboBox(objects);
		lodBox.setEditable(false);
		lodBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Object selected = lodBox.getSelectedItem();
				RnaRendererGraphController lod = (RnaRendererGraphController) display.getModel();
				if(selected == LOD_COARSE)
					lod.setRefinementLevel(LODCapable.LOD_COARSE);
				else if(selected == LOD_MEDIUM)
					lod.setRefinementLevel(LODCapable.LOD_MEDIUM);
				else
					lod.setRefinementLevel(LODCapable.LOD_FINE);
				
				
				lod.updateMeshes();
				display.display();
			}
		});
		
		if(display.getModel() instanceof LODCapable) {
			RnaRendererGraphController lod = (RnaRendererGraphController) display.getModel();
			int refinement = lod.getRefinementLevel();
			if(refinement == LODCapable.LOD_COARSE)
				lodBox.setSelectedIndex(0);
			else if(refinement == LODCapable.LOD_MEDIUM)
				lodBox.setSelectedIndex(1);
			else
				lodBox.setSelectedIndex(2);
		} else {
			lodBox.setEnabled(false);
		}
		
		flow.add(lodBox);
		flow.setAlignmentX(JComponent.LEFT_ALIGNMENT);
		panel.add(flow);
		
		final JCheckBox lighting = new JCheckBox("Lighting Enabled");
		lighting.setSelected(display.getLighting());
		lighting.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				display.setLighting(lighting.isSelected());
				display.display();
			}
		});
		
		panel.add(Box.createVerticalStrut(15));
		panel.add(lighting);

		final JCheckBox depthCue = new JCheckBox("Depth Cue Enabled (fog effect)");
		depthCue.setSelected(((PerspectiveDisplay)display).getFog());
		depthCue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			    ((PerspectiveDisplay)display).setFog(depthCue.isSelected());
			    display.display();
			}
		});

		panel.add(depthCue);
		
		panel.add(Box.createVerticalGlue());
		
		JPanel temp = new JPanel();
		temp.setLayout(new BoxLayout(temp, BoxLayout.Y_AXIS));
		/*temp.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black), "Object Selection Options", TitledBorder.LEFT, TitledBorder.TOP));
		 final String[] rnaObjectTypes = RnaTools.getRnaClassNames();
		final JComboBox objectSelectionBox = new JComboBox(rnaObjectTypes);
		for(int k=0;k<rnaObjectTypes.length;k++){
		    if(rnaObjectTypes[k].equals(RnaTools.getRnaDisplaySelectionType()))
			objectSelectionBox.setSelectedIndex(k);
		}
		objectSelectionBox.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
			    String selection = (String)objectSelectionBox.getSelectedItem();
			    RnaTools.setRnaDisplaySelectionType(selection);
			}
		});
	        temp.add(objectSelectionBox);
		temp.add(Box.createVerticalGlue());*/

		panel.add(temp);

		temp = new JPanel();
		temp.setLayout(new BoxLayout(temp, BoxLayout.Y_AXIS));
		temp.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black), "Selective Rendering", TitledBorder.LEFT, TitledBorder.TOP));
		final JCheckBox renderStrands = new JCheckBox("Render Strands");
		final JCheckBox renderNucleotides = new JCheckBox("Render Nucleotides");
		final JCheckBox renderAtoms = new JCheckBox("Render Atoms");
		final JCheckBox renderLinks = new JCheckBox("Render Links");
		final JCheckBox renderBranchDescriptors = new JCheckBox("Render helix end descriptors");
		renderStrands.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				RnaRendererGraphController rna = (RnaRendererGraphController) display.getModel();
				rna.setRenderStrands(renderStrands.isSelected());
				display.display();
			}
		});
		renderNucleotides.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				RnaRendererGraphController rna = (RnaRendererGraphController) display.getModel();
				rna.setRenderNucleotides(renderNucleotides.isSelected());
				display.display();
			}
		});
		renderBranchDescriptors.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				RnaRendererGraphController rna = (RnaRendererGraphController) display.getModel();
				rna.setRenderBranchDescriptors(renderBranchDescriptors.isSelected());
				display.display();
			}
		});
		renderAtoms.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				RnaRendererGraphController rna = (RnaRendererGraphController) display.getModel();
				rna.setRenderAtoms(renderAtoms.isSelected());
				display.display();
			}
		});
		renderLinks.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				RnaRendererGraphController rna = (RnaRendererGraphController) display.getModel();
				rna.setRenderLinks(renderLinks.isSelected());
				display.display();
			}
		});
		
		if(display.getModel() instanceof RnaRendererGraphController) {
			RnaRendererGraphController rna = (RnaRendererGraphController) display.getModel();
			renderLinks.setSelected(rna.isRenderLinks());
			renderAtoms.setSelected(rna.isRenderAtoms());
			renderNucleotides.setSelected(rna.isRenderNucleotides());
			renderStrands.setSelected(rna.isRenderStrands());
			renderBranchDescriptors.setSelected(rna.isRenderBranchDescriptors());
			
		} else {
			renderLinks.setEnabled(false);
			renderAtoms.setEnabled(false);
			renderStrands.setEnabled(false);
			renderNucleotides.setEnabled(false);
			renderBranchDescriptors.setEnabled(false);
		}
		
		temp.add(renderStrands);
		temp.add(Box.createVerticalStrut(10));
		temp.add(renderNucleotides);
		temp.add(Box.createVerticalStrut(10));
		temp.add(renderAtoms);
		temp.add(Box.createVerticalStrut(10));
		temp.add(renderBranchDescriptors);
		temp.add(Box.createVerticalStrut(10));
		temp.add(renderLinks);

		panel.add(temp);
		
		pane.addTab("Rendering Options", panel);
		
		add(pane, BorderLayout.CENTER);
		JPanel bottom = new JPanel();
		JButton close = new JButton("Close");
		close.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
			}
		});
		bottom.add(close);
		add(bottom, BorderLayout.SOUTH);
		pack();
		setVisible(true);
	}

}
