package viewer.display;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import java.util.ArrayList;
import java.util.List;

import viewer.commands.DisplayCommand;
import viewer.event.*;
import viewer.view.ViewOrientation;

/**
 * Multi display manager manages the multi display and all of its displays. It
 * allows for the total reconfiguration of a multi display. For example, changing
 * from one view to four different views.
 * @author Calvin
 *
 */
public class MultiDisplayManager extends JPanel implements DisplayViewChangedListener {
	
	private ArrayList<DisplayLayoutListener> listeners =
		new ArrayList<DisplayLayoutListener>();
	
	
	private ArrayList<DisplayCommand> commands = 
		new ArrayList<DisplayCommand>();
	
	private MultiDisplay display;
	
	/**
	 * Size of configuration dialog
	 */
	public final static Dimension CONFIG_SIZE = new Dimension(400, 500);
	
	public MultiDisplayManager() {
		this(DisplayFactory.PrefabricatedDisplays.FourQuadrants.getDefaultDisplay());
		
	}
	
	/**
	 * A manager needs a multidisplay to be constructed
	 * @param display Display to construct manager around
	 */
	public MultiDisplayManager(MultiDisplay display) {
		this.display = display;
		
		setLayout(new BorderLayout());
		add(display, BorderLayout.CENTER);
	}
	
	
	
	public void displayChanged(DisplayEvent e) {
		if(e.getId() == DisplayEvent.VIEW_ADDED) {
			for(final DisplayCommand c : commands) {
				final Display d = e.getDisplay();
				AbstractAction action = new AbstractAction() {
					public void actionPerformed(ActionEvent e) {
						c.execute(d);
					}
				};
				
				
				d.getActionMap().put(commands.indexOf(c), action);
				d.getInputMap().put(c.keyStroke(), commands.indexOf(c));
			}
		}
		
	}
	

	/**
	 * Add a command to each display.
	 * @param command Command to add.
	 */
	public void addCommand(DisplayCommand command) {
		commands.add(command);
		for(final Display d : display.getDisplays()) {
			final DisplayCommand c = command;
			AbstractAction action = new AbstractAction() {
				public void actionPerformed(ActionEvent e) {
					c.execute(d);
				}
			};
			
			
			d.getActionMap().put(commands.indexOf(c), action);
			d.getInputMap().put(c.keyStroke(), commands.indexOf(c));
		}
	}
	
	/**
	 * Get commands added to displays
	 * @return Returns list of commands
	 */
	public List<DisplayCommand> getCommands() {
		return commands;
	}
	
	/**
	 * Adds commands to displays
	 *
	 */
	protected void refreshCommands() {
		for(final DisplayCommand c : commands) {
			for(final Display d : display.getDisplays()) {
				AbstractAction action = new AbstractAction() {
					public void actionPerformed(ActionEvent e) {
						c.execute(d);
					}
				};
				
				d.getActionMap().put(commands.indexOf(c), action);
				d.getInputMap().put(c.keyStroke(), commands.indexOf(c));
			}
		}
	}
	
	/**
	 * Get the multi display backing the manager
	 * @return Returns the multi display
	 */
	public MultiDisplay getDisplay() {
		return display;
	}
	
	/**
	 * Add a display layout listener. Listener is notified if the layout of displays,
	 * ie, the display manager, is changed.
	 * @param listener Listener to add.
	 */
	public void addDisplayLayoutListener(DisplayLayoutListener listener) {
		listeners.add(listener);
	}
	
	/**
	 * Removes a display layout listener.
	 * @param listener Listener to remove
	 * @return Returns true if the listener was removed or false if otherwise
	 */
	public boolean removeDisplayLayoutListener(DisplayLayoutListener listener) {
		return listeners.remove(listener);
	}
	
	/**
	 * Notify all attached listeners that the display layout
	 * has changed
	 *
	 */
	public void notifyDisplayLayoutChanged() {
		for(DisplayLayoutListener l : listeners)
			l.displayLayoutChanged(new DisplayEvent(this));
	}
	
	/**
	 * Set the multidisplay backing this display.
	 * @param display Multi display to use
	 */
	public void setDisplay(MultiDisplay display) {
		if(display == null)
			throw new IllegalArgumentException("Display cannot be null");
		
		display.setPreferredSize(this.display.getPreferredSize());
		this.remove(this.display);
		
		
		this.display = display;
		add(display, BorderLayout.CENTER);
		
		notifyDisplayLayoutChanged();
		
	}
	
	/**
	 * A dialog that enables the configuration of the
	 * view layouts of the multi display.
	 * @author Calvin
	 *
	 */
	public class Configuration extends JDialog {
		private JButton ok;
		private JButton close;
		
		private JComboBox comboBox;
		private OrientationPanel orientationPanel;
		
		private JTabbedPane pane;
		
		/**
		 * Provide access to tabbed pane so
		 * subclasses can add their own specific
		 * config options
		 * @return Tabbed Pane
		 */
		public JTabbedPane getPane() {
			return pane;
		}
		
		public JButton getOKButton() {
			return ok;
		}
		
		public JButton getCloseButton() {
			return close;
		}
		
		public Configuration(String title) {
			super((Frame)null, title, true);
			
			pane = new JTabbedPane();
			
			final JPanel panel = new JPanel();
			panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
			JLabel label = new JLabel("Select Display Layout");
			label.setAlignmentX(JComponent.CENTER_ALIGNMENT);
			comboBox = new JComboBox(DisplayFactory.PrefabricatedDisplays.values());
			panel.add(label);
			panel.add(Box.createVerticalStrut(15));
			
			JPanel temp = new JPanel();
			//temp.setLayout(new BoxLayout(temp, BoxLayout.X_AXIS));
			temp.add(comboBox);
			final JPanel preview = new JPanel() {
				{
					setPreferredSize(new Dimension(59, 59));
					setSize(new Dimension(59, 59));
					setBackground(new Color(150, 150, 150));
				}
				
				public void paintComponent(Graphics g) {
					super.paintComponent(g);
					g.setColor(Color.white);
					char[] numbers = { '1', '2', '3', '4' };
					int height = 10;
					DisplayFactory.PrefabricatedDisplays d = (DisplayFactory.PrefabricatedDisplays) comboBox.getSelectedItem();
					switch(d) {
					case FourQuadrants:
						g.fillRect(3, 3, 25, 25);
						g.fillRect(31, 31, 25, 25);
						g.fillRect(3, 31, 25, 25);
						g.fillRect(31, 3, 25, 25);
						g.setColor(Color.black);
						g.drawChars(numbers, 2, 1, 3 , 31 + height);
						g.drawChars(numbers, 3, 1, 31, 31 + height);
						g.drawChars(numbers, 0, 1, 3, 3 + height);
						g.drawChars(numbers, 1, 1, 31, 3 + height);
						break;
					case VerticalSplit:
						g.fillRect(3, 3, 25, 53);
						g.fillRect(31, 3, 25, 53);
						g.setColor(Color.black);
						g.drawChars(numbers, 0, 1, 3, 3 + height);
						g.drawChars(numbers, 1, 1, 31, 3 + height);
						break;
					case HorizontalSplit:
						g.fillRect(3, 3, 53, 25);
						g.fillRect(3, 31, 53, 25);
						g.setColor(Color.black);
						g.drawChars(numbers, 0, 1, 3, 3 + height);
						g.drawChars(numbers, 1, 1, 3, 31 + height);
						break;
					case TopAndTwoBottoms:
						g.fillRect(3, 3, 53, 25);
						g.fillRect(31, 31, 25, 25);
						g.fillRect(3, 31, 25, 25);
						g.setColor(Color.black);
						g.drawChars(numbers, 0, 1, 3, 3 + height);
						g.drawChars(numbers, 1, 1, 3, 31 + height);
						g.drawChars(numbers, 2, 1, 31, 31 + height);
						break;
					case Single:
						g.fillRect(3, 3, 53, 53);
						g.setColor(Color.black);
						g.drawChars(numbers, 0, 1, 3, 3 + height);
						break;
					case BottomAndTwoTops:
						g.fillRect(3, 31, 53, 25);
						g.fillRect(3, 3, 25, 25);
						g.fillRect(31, 3, 25, 25);
						g.setColor(Color.black);
						g.drawChars(numbers, 0, 1, 3, 3 + height);
						g.drawChars(numbers, 1, 1, 31, 3 + height);
						g.drawChars(numbers, 2, 1, 3, 31 + height);
						break;
					case LeftAndTwoRights:
						g.fillRect(3, 3, 25, 53);
						g.fillRect(31, 31, 25, 25);
						g.fillRect(31, 3, 25, 25);
						g.setColor(Color.black);
						g.drawChars(numbers, 0, 1, 3, 3 + height);
						g.drawChars(numbers, 1, 1, 31, 3 + height);
						g.drawChars(numbers, 2, 1, 31, 31 + height);
						break;
					case RightAndTwoLefts:
						g.fillRect(31, 3, 25, 53);
						g.fillRect(3, 3, 25, 25);
						g.fillRect(3, 31, 25, 25);
						g.setColor(Color.black);
						g.drawChars(numbers, 0, 1, 3, 3 + height);
						g.drawChars(numbers, 1, 1, 31, 3 + height);
						g.drawChars(numbers, 2, 1, 3, 31 + height);
						break;
					}
				}
			};
			
			orientationPanel = new OrientationPanel();
			
			
			
			comboBox.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					panel.remove(orientationPanel);
					orientationPanel = new OrientationPanel();
					panel.add(orientationPanel);
					panel.revalidate();
					panel.repaint();
					orientationPanel.repaint();
					
				}
			});
			
			temp.add(Box.createHorizontalGlue());
			temp.add(preview);
			panel.add(temp);
			panel.add(Box.createVerticalStrut(15));
			panel.add(orientationPanel);
			panel.add(Box.createVerticalGlue());
			
			
			pane.addTab("Display Layout", panel);
			getContentPane().setLayout(new BorderLayout());
			add(pane, BorderLayout.CENTER);
			
			ok = new JButton("OK");
			close = new JButton("Close");
			
			ok.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					MultiDisplayManager.this.remove(display);
					DisplayFactory.PrefabricatedDisplays displayType =
						(DisplayFactory.PrefabricatedDisplays) comboBox.getSelectedItem();
					
					MultiDisplay d = displayType.getDisplay(orientationPanel.getSelectedOrientations());;
					d.setModel(display.getModel());
					d.setPreferredSize(display.getPreferredSize());
					
					display = d;
					
					refreshCommands();
					
					MultiDisplayManager.this.add(d, BorderLayout.CENTER);
					MultiDisplayManager.this.revalidate();
					
					notifyDisplayLayoutChanged();
					
					setVisible(false);
					dispose();
					
				}
			});
			
			close.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					setVisible(false);
					dispose();
				}
			});
			
			JPanel bottom = new JPanel();
			bottom.add(ok);
			bottom.add(close);
			bottom.setAlignmentX(JComponent.CENTER_ALIGNMENT);
			
			add(bottom, BorderLayout.SOUTH);
			
			setPreferredSize(CONFIG_SIZE);
			setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			pack();
		}
		
		private class OrientationPanel extends JPanel {
			private ArrayList<JComboBox> boxes =
				new ArrayList<JComboBox>();
			
			public OrientationPanel() {
				setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
				DisplayFactory.PrefabricatedDisplays d = 
					(DisplayFactory.PrefabricatedDisplays) comboBox.getSelectedItem();
				for(int i = 0; i < d.getViewCount(); ++i) {
					JPanel panel = new JPanel();
					panel.add(new JLabel("View " + (i + 1)));
					JComboBox box = new JComboBox(ViewOrientation.values());
					boxes.add(box);
					panel.add(box);
					add(panel);
				}
			}
			
			public ViewOrientation[] getSelectedOrientations() {
				ViewOrientation[] ret = new ViewOrientation[boxes.size()];
				for(int i = 0; i < ret.length; ++i)
					ret[i] = (ViewOrientation) boxes.get(i).getSelectedItem();
				
				return ret;
			}
		}
	}
	
	/**
	 * Update the displays. Causes each display
	 * to render its contents by calling the Display.display()
	 * method for each display.
	 *
	 */
	public void display() {
		for(Display d : getDisplay().getDisplays()) {
			System.out.println("Calling display for: " + d);
			d.display();
		}
	}
}
