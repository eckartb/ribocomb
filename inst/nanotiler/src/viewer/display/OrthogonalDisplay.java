package viewer.display;

import javax.media.opengl.*;
import javax.media.opengl.glu.*;
import java.awt.*;
import java.awt.Point;
import java.nio.FloatBuffer;

import tools3d.*;
import viewer.graphics.*;
import viewer.util.Tools;
import viewer.view.OrthogonalView;
import viewer.components.ViewManipulator;
import viewer.components.ViewManipulator.RotateListener;
import viewer.components.ViewManipulator.TranslateListener;
import viewer.components.ViewManipulator.ZoomListener;
import viewer.event.*;

/**
 * An orthogonal display.
 * @author Calvin
 *
 */
public class OrthogonalDisplay extends Display {

	private OrthogonalView view;
	
	private Mesh grid;
	
	private ZoomListener zoomListener;
	private TranslateListener translateListener;
	
	/**
	 * Create the display using an orthogonal view
	 * @param view View to create display with
	 */
	public OrthogonalDisplay(OrthogonalView view) {
		this.view = view;
		final GLEventListener listener = new OrthogonalDisplayRenderer();
		addGLEventListener(listener);
		setBackground(Color.black);
		
		view.addViewListener(new ViewListener() {
			public void viewChanged(ViewEvent e) {
				display();
			}
		});
		
		
		grid = PrimitiveFactory.generateSheet(view.getViewDirection().mul(-1), 500, 500, (int) (500 / getGridSize()), (int) (500 / getGridSize()));
		
		ViewManipulator manipulator = new ViewManipulator(getView());
		
		zoomListener = manipulator.new ZoomListener();
		translateListener = manipulator.new TranslateListener();
		
		
		addMouseListener(zoomListener);
		addMouseMotionListener(zoomListener);
		
		addMouseListener(translateListener);
		addMouseMotionListener(translateListener);
	}
	
	/**
	 * Creates an OrthogonalDisplay using the view and sets the model
	 * @param view View to create display with
	 * @param model Model that the display should render
	 */
	public OrthogonalDisplay(OrthogonalView view, RenderableModel model) {
		this(view);

		setModel(model);
		
	}
	
	public void setMouseTranslationEnabled(boolean mouseTranslationEnabled) {
		boolean oldMode = isMouseTranslationEnabled();
		
		
		super.setMouseTranslationEnabled(mouseTranslationEnabled);
		
		if(mouseTranslationEnabled && !oldMode) {
			addMouseListener(zoomListener);
			addMouseMotionListener(zoomListener);
			
			addMouseListener(translateListener);
			addMouseMotionListener(translateListener);
		}
		else if(!mouseTranslationEnabled && oldMode) {
			removeMouseListener(zoomListener);
			removeMouseMotionListener(zoomListener);
			
			removeMouseListener(translateListener);
			removeMouseMotionListener(translateListener);
		}
	}
	
	public ZoomListener getZoomListener() {
		return zoomListener;
	}
	
	public RotateListener getRotateListener() {
		return null;
	}
	
	public TranslateListener getTranslateListener() {
		return translateListener;
	}
	
	public void setZoomListener(ZoomListener listener) {
		if(zoomListener != null) {
			removeMouseListener(zoomListener);
			removeMouseMotionListener(zoomListener);
			zoomListener = listener;
			if(zoomListener != null) {
				addMouseListener(zoomListener);
				addMouseMotionListener(zoomListener);
			}
		}
	}
	

	public void setRotateListener(RotateListener listener) {
		
	}
	
	public void setTranslateListener(TranslateListener listener) {
		if(translateListener != null) {
			removeMouseListener(translateListener);
			removeMouseMotionListener(translateListener);
			translateListener = listener;
			if(translateListener != null) {
				addMouseListener(translateListener);
				addMouseMotionListener(translateListener);
			}
		}
	}
	
	
	

	
	public OrthogonalView getView() {
		return view;
	}
	
	
	



	/**
	 * Does all the rendering for the orthogonal display
	 * @author Calvin
	 *
	 */
	public class OrthogonalDisplayRenderer implements GLEventListener {

		public void display(GLAutoDrawable d) {
			GL gl = d.getGL();
			
			gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
			
			gl.glMatrixMode(GL.GL_PROJECTION);
			gl.glLoadIdentity();
			Dimension screenSize = OrthogonalDisplay.this.getSize();
			double zoom = view.getZoom();
			
			gl.glOrtho(-1 * screenSize.width / 2 / zoom, screenSize.width / 2 / zoom, 
					screenSize.height / 2 / zoom * -1, screenSize.height / zoom / 2, 
					-getViewDistance(), getViewDistance());
			
			gl.glMatrixMode(GL.GL_MODELVIEW);
			gl.glLoadIdentity();
			
			GLU glu = new GLU();
			Vector4D eye = view.getViewLocation();
			Vector4D lookAt = view.getViewDirection();
			Vector4D up = view.getViewDirectionNormal();
			glu.gluLookAt(eye.getX(), eye.getY(), eye.getZ(), 
					eye.getX() + lookAt.getX(), eye.getY() + lookAt.getY(), 
					eye.getZ() + lookAt.getZ(), up.getX(), 
					up.getY(), up.getZ());
			
			
		
			for(RenderableModel m : getRenderables())
				m.render(gl);
			
			if(isGizmoEnabled())
				getGizmo().render(gl);
			
			if(isRenderCoordinateAxis()) {
				gl.glBegin(GL.GL_LINES);
				Vector4D direction = view.getViewDirection();
				if(direction.getX() == 0.0) {
					Material material = new Material(getXAxisColor());
					Material.renderMaterial(gl, material);
					gl.glVertex3d(-250.0, 0.0, 0.0);
					gl.glVertex3d(250.0, 0.0, 0.0);
				}
				if(direction.getY() == 0.0) {
					Material material = new Material(getYAxisColor());
					Material.renderMaterial(gl, material);
					gl.glVertex3d(0.0, -250.0, 0.0);
					gl.glVertex3d(0.0, 250.0, 0.0);
				}
				if(direction.getZ() == 0.0) {
					Material material = new Material(getZAxisColor());
					Material.renderMaterial(gl, material);
					gl.glVertex3d(0.0, 0.0, -250.0);
					gl.glVertex3d(0.0, 0.0, 250.0);
				}
				
				
				gl.glEnd();
			}
			
			
			if(isGridVisible()) {
				gl.glLineWidth(0.5f);
				MeshRenderer renderer = new WireframeMeshRenderer(grid);
				Material material = new Material(getGridColor());
				Material.renderMaterial(gl, material);
				renderer.render(gl);
			}
			
			
			
			
		}

		public void displayChanged(GLAutoDrawable d, boolean arg1, boolean arg2) {
			// TODO Auto-generated method stub
			
		}

		public void init(GLAutoDrawable d) {
			GL gl = d.getGL();
			
			if(getLighting()) {
				float[] ambient = { 0.2f, 0.2f, 0.2f, 1.0f };
				float[] specular = { 1.0f, 1.0f, 1.0f, 1.0f };
				float[] position = { 0.0f, 0.0f, 1.0f, 0.0f };
				float[] ambientModel = { 0.1f, 0.1f, 0.1f, 0.1f };

				gl.glLightfv(GL.GL_LIGHT0, GL.GL_AMBIENT, ambient, 0);
				gl.glLightfv(GL.GL_LIGHT0, GL.GL_SPECULAR, specular, 0);
				gl.glLightfv(GL.GL_LIGHT0, GL.GL_POSITION, position, 0);
				gl.glLightModelfv(GL.GL_LIGHT_MODEL_AMBIENT, ambientModel, 0);
				gl.glEnable(GL.GL_LIGHT0);
				gl.glEnable(GL.GL_LIGHTING);

			}
			
			Color bg = getGLBackground();
			
			gl.glEnableClientState(GL.GL_VERTEX_ARRAY);
			gl.glEnableClientState(GL.GL_NORMAL_ARRAY);
			
			gl.glEnable(GL.GL_DEPTH_TEST);
			
			
			gl.glClearColor(bg.getRed() / 255.0f, bg.getGreen() / 255.0f , bg.getBlue() / 255.0f, 0.0f);
			gl.glEnable(GL.GL_BLEND);
			gl.glBlendEquation(GL.GL_FUNC_ADD);
			
		}

		public void reshape(GLAutoDrawable d, int x, int y, int width, int height) {
			GL gl = d.getGL();
			
			double zoom = view.getZoom();
			
			gl.glMatrixMode(GL.GL_PROJECTION);
			gl.glLoadIdentity();
			Dimension screenSize = OrthogonalDisplay.this.getSize();
			
			gl.glOrtho(-1 * screenSize.width / 2 / zoom, screenSize.width / 2 / zoom, 
					screenSize.height / 2 / zoom * -1, screenSize.height / zoom / 2, 
					-getViewDistance(), getViewDistance());
			gl.glViewport(0, 0, screenSize.width, screenSize.height);
			
		}
		
	}

	public void setGridSize(double gridSize) {
		super.setGridSize(gridSize);
		grid = PrimitiveFactory.generateSheet(view.getViewDirection().mul(-1), 500, 500, (int) (500 / gridSize), (int) (500 / gridSize));
	}
	
	/**
	 * Sets the view of the display
	 * @param view View to use
	 */
	public void setView(OrthogonalView view) {
		this.view = view;
		grid = PrimitiveFactory.generateSheet(view.getViewDirection().mul(-1), 500, 500, (int) (500 / getGridSize()), (int) (500 / getGridSize()));
		display();
	}

    /** converts 2D point into 3D coordinates. TODO : contains bug !
     */
	@Override
	public Vector4D getCoordinates(Point p) {
		Vector4D ray = new Vector4D(0.0, 0.0, 0.0, 0.0);
		Vector4D origin = new Vector4D(0.0, 0.0, 0.0, 1.0);
		Tools.screenTo3DSpace(p, this, ray, origin);
		Vector4D direction = getView().getViewDirection();
		
		Vector4D position = new Vector4D(0.0, 0.0, 0.0, 1.0);
		
		if(direction.getX() == 0.0)
			position.setX(origin.getX());
		if(direction.getY() == 0.0)
			position.setY(origin.getY());
		if(direction.getZ() == 0.0)
			position.setZ(origin.getZ());
		
		return position;
	}
	
	
	
	
	
}
