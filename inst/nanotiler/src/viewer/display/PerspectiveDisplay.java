package viewer.display;

import javax.media.opengl.*;
import javax.media.opengl.glu.*;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.nio.FloatBuffer;

import viewer.graphics.*;
import viewer.util.Tools;
import viewer.view.PerspectiveView;
import viewer.components.ViewManipulator;
import viewer.components.ViewManipulator.RotateListener;
import viewer.components.ViewManipulator.TranslateListener;
import viewer.components.ViewManipulator.ZoomListener;
import viewer.event.*;
import tools3d.*;

/**
 * A perspective display
 * @author Calvin
 *
 */
public class PerspectiveDisplay extends  Display {
	
	private double viewSize = 4000.0;

        private double viewBegin = 5;
	
	private Mesh grid;
	
	private boolean fog = false;
	
	private ZoomListener zoomListener;
	private RotateListener rotateListener;
	private TranslateListener translateListener;
	
	private double angle;
	
	private PerspectiveView view;
	
	/**
	 * Construct a PerspectiveDisplay from the passed in view.
	 * @param view View to construct display from.
	 */
	public PerspectiveDisplay(PerspectiveView view) {
		this.view = view;
		setBackground(Color.black);
		addGLEventListener(new PerspectiveDisplayRenderer());
		
		view.addViewListener(new ViewListener() {
			public void viewChanged(ViewEvent e) {
				display();
			}
		});
		
		
		grid = PrimitiveFactory.generateSheet(Tools.Y_AXIS, 500, 500, (int) (500 / getGridSize()), (int) (500 / getGridSize()));

		ViewManipulator manipulator = new ViewManipulator(getView());
		
		zoomListener = manipulator.new ZoomListener();
		rotateListener = manipulator.new RotateListener();
		translateListener = manipulator.new TranslateListener();
		
		
		addMouseListener(zoomListener);
		addMouseMotionListener(zoomListener);
		
		addMouseListener(rotateListener);
		addMouseMotionListener(rotateListener);
		
		addMouseListener(translateListener);
		addMouseMotionListener(translateListener);
		
	}
	
	/**
	 * Construct PerspectiveDisplay from view and set the model
	 * @param view View to create display from.
	 * @param model Model to render
	 */
	public PerspectiveDisplay(PerspectiveView view, RenderableModel model) {
		this(view);
		
		setModel(model);
	}
	
	public void setMouseTranslationEnabled(boolean mouseTranslationEnabled) {
		boolean oldMode = isMouseTranslationEnabled();
		
		super.setMouseTranslationEnabled(mouseTranslationEnabled);
		
		if(mouseTranslationEnabled && !oldMode) {
			addMouseListener(zoomListener);
			addMouseMotionListener(zoomListener);
			
			addMouseListener(rotateListener);
			addMouseMotionListener(rotateListener);
			
			addMouseListener(translateListener);
			addMouseMotionListener(translateListener);
		}
		else if(!mouseTranslationEnabled && oldMode) {
			removeMouseListener(zoomListener);
			removeMouseMotionListener(zoomListener);
			
			removeMouseListener(rotateListener);
			removeMouseMotionListener(rotateListener);
			
			removeMouseListener(translateListener);
			removeMouseMotionListener(translateListener);
		}
	}
	
	
	public ZoomListener getZoomListener() {
		return zoomListener;
	}
	
	public RotateListener getRotateListener() {
		return rotateListener;
	}
	
	public TranslateListener getTranslateListener() {
		return translateListener;
	}
	
	public void setZoomListener(ZoomListener listener) {
		if(zoomListener != null) {
			removeMouseListener(zoomListener);
			removeMouseMotionListener(zoomListener);
			zoomListener = listener;
			if(zoomListener != null) {
				addMouseListener(zoomListener);
				addMouseMotionListener(zoomListener);
			}
		}
	}
	

	public void setRotateListener(RotateListener listener) {
		if(rotateListener != null) {
			removeMouseListener(rotateListener);
			removeMouseMotionListener(rotateListener);
			rotateListener = listener;
			if(rotateListener != null) {
				addMouseListener(rotateListener);
				addMouseMotionListener(rotateListener);
			}
		}
	}
	
	public void setTranslateListener(TranslateListener listener) {
		if(translateListener != null) {
			removeMouseListener(translateListener);
			removeMouseMotionListener(translateListener);
			translateListener = listener;
			if(translateListener != null) {
				addMouseListener(translateListener);
				addMouseMotionListener(translateListener);
			}
		}
	}
	
	/**
	 * Determine if Fog is turned on or off.
	 * @return Returns true if fog is on or false
	 * if fog is off
	 */
	public boolean getFog() {
		return fog;
	}
	
	/**
	 * Turn fog on and off
	 * @param fog True if fog should be on or
	 * false if fog should be off
	 */
	public void setFog(boolean fog) {
		this.fog = fog;
		GLContext context = getContext();
		int result = context.makeCurrent();
		if(result == GLContext.CONTEXT_CURRENT) {
			GL gl = context.getGL();
			if(fog)
				gl.glEnable(GL.GL_FOG);
			else
				gl.glDisable(GL.GL_FOG);
		}
		
		repaint();
		
	}
	
	/**
	 * Renders the perspective view
	 * @author Calvin
	 *
	 */
	public class PerspectiveDisplayRenderer implements GLEventListener {

		public void display(GLAutoDrawable d) {
			GL gl = d.getGL();
			
			gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
			
			gl.glMatrixMode(GL.GL_MODELVIEW);
			gl.glLoadIdentity();
			
			GLU glu = new GLU();
			Vector4D eye = view.getViewLocation();
			Vector4D lookAt = view.getViewDirection();
			Vector4D up = view.getViewDirectionNormal();
		
			
			glu.gluLookAt(eye.getX(), eye.getY(), eye.getZ(), 
					eye.getX() + lookAt.getX(), eye.getY() + lookAt.getY(), 
					eye.getZ() + lookAt.getZ(), up.getX(), 
					up.getY(), up.getZ());
			
			for(RenderableModel m : getRenderables()) {
				m.render(gl);
			}
			
			
			if(isGizmoEnabled())
				getGizmo().render(gl);
			
			if(isRenderCoordinateAxis()) {
				gl.glLineWidth(1.0f);
				gl.glBegin(GL.GL_LINES);
				Material material = new Material(getXAxisColor());
				Material.renderMaterial(gl, material);
				gl.glVertex3d(0.0, 0.0, 0.0);
				gl.glVertex3d(1.0, 0.0, 0.0);
				material = new Material(getYAxisColor());
				Material.renderMaterial(gl, material);
				gl.glVertex3d(0.0, 0.0, 0.0);
				gl.glVertex3d(0.0, 1.0, 0.0);
				material = new Material(getZAxisColor());
				Material.renderMaterial(gl, material);
				gl.glVertex3d(0.0, 0.0, 0.0);
				gl.glVertex3d(0.0, 0.0, 1.0);
				gl.glEnd();
			}
			
			if(isGridVisible()) {
				gl.glLineWidth(0.5f);
				MeshRenderer renderer = new WireframeMeshRenderer(grid);
				Material material = new Material(getGridColor());
				Material.renderMaterial(gl, material);
				renderer.render(gl);
			}
			

		}

		public void displayChanged(GLAutoDrawable d, boolean arg1, boolean arg2) {
			// TODO Auto-generated method stub
			
		}

		public void init(GLAutoDrawable d) {
			GL gl = d.getGL();
			
			if(getLighting()) {
				float[] ambient = { 0.2f, 0.2f, 0.2f, 1.0f };
				float[] specular = { 1.0f, 1.0f, 1.0f, 1.0f };
				float[] position = { 0.0f, 0.0f, 1.0f, 0.0f };
				float[] ambientModel = { 0.1f, 0.1f, 0.1f, 0.1f };

				gl.glLightfv(GL.GL_LIGHT0, GL.GL_AMBIENT, ambient, 0);
				gl.glLightfv(GL.GL_LIGHT0, GL.GL_SPECULAR, specular, 0);
				gl.glLightfv(GL.GL_LIGHT0, GL.GL_POSITION, position, 0);
				gl.glLightModelfv(GL.GL_LIGHT_MODEL_AMBIENT, ambientModel, 0);
				gl.glEnable(GL.GL_LIGHT0);
				gl.glEnable(GL.GL_LIGHTING);
			}
			
			gl.glEnable(GL.GL_DEPTH_TEST);
			
			Color bg = getGLBackground();
			
			if(getFog()) {
				gl.glEnable(GL.GL_FOG);
				float[] fogColor = { bg.getRed() / 255.0f, bg.getGreen() / 255.0f, bg.getBlue() / 255.0f, 1.0f};
				
				gl.glFogfv(GL.GL_FOG_COLOR, FloatBuffer.wrap(fogColor));
				gl.glFogf(GL.GL_FOG_DENSITY, 0.002f);
				gl.glFogi(GL.GL_FOG_MODE, GL.GL_EXP2);
			}
			

			gl.glEnableClientState(GL.GL_VERTEX_ARRAY);
			gl.glEnableClientState(GL.GL_NORMAL_ARRAY);
			
			
			
			
			gl.glClearColor(bg.getRed() / 255.0f, bg.getGreen() / 255.0f , bg.getBlue() / 255.0f, 0.0f);
			gl.glShadeModel(GL.GL_SMOOTH);
			gl.glEnable(GL.GL_BLEND);
			gl.glEnable(GL.GL_CULL_FACE);
			gl.glCullFace(GL.GL_BACK);
			gl.glBlendEquation(GL.GL_FUNC_ADD);
		}

		public void reshape(GLAutoDrawable d, int x, int y, int width, int height) {
			GL gl = d.getGL();
			
			gl.glMatrixMode(GL.GL_PROJECTION);
			gl.glLoadIdentity();
			
			angle = Math.atan((viewSize / 2) / getViewDistance()) * 2;
			angle *= 180 / Math.PI;
			
			GLU glu = new GLU();
			
		
			glu.gluPerspective(angle, 1, viewBegin, getViewDistance());
			if(width > height) {
				gl.glViewport(0, -((width - height) / 2), width, width);
			}
			else {
				gl.glViewport(-((height - width) / 2), 0, height, height);
			}
			
			
		}
		
	}


	public void setGridSize(double gridSize) {
		super.setGridSize(gridSize);
		grid = PrimitiveFactory.generateSheet(Tools.Y_AXIS, 500, 500, (int) (500 / gridSize), (int) (500 / gridSize));
	}
	
	
	public PerspectiveView getView() {
		return view;
	}
	
	/**
	 * Set the view the display uses.
	 * @param view View
	 */
	public void setView(PerspectiveView view) {
		this.view = view;
		display();
	}
	
	/**
	 * Get the field of view angle in degrees
	 * @return
	 */
	public double getAngle() {
		return angle;
	}

    /** converts 2D point into 3D coordinates. TODO : contains bug !
     */
	@Override
	public Vector4D getCoordinates(Point p) {
		Vector4D ray = new Vector4D(0.0, 0.0, 0.0, 0.0);
		Vector4D origin = new Vector4D(0.0, 0.0, 0.0, 1.0);
		Tools.screenTo3DSpace(p, this, ray, origin);
		
		Vector4D offset = Tools.translate(getView().getViewLocation(), ray, 100.0);
		Vector4D normal = getView().getViewDirection().mul(-1);
		Vector4D collision = CollisionDetectionTools.linePlaneIntersection(ray, origin, normal, -1 * offset.dot(normal));
		return collision;
	}
	

}
