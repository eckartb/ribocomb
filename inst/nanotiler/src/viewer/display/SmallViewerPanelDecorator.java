package viewer.display;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;

import javax.swing.*;

import viewer.components.ViewManipulator;
import viewer.components.ViewOrientationSelector;
import viewer.view.PerspectiveView;
import viewer.view.ViewOrientation;
import viewer.graph.rna.RnaRendererGraphController;
import viewer.graphics.LODCapable;

/**
 * The default decorator attaches a view orientation selector
 * to a display along with a view manipulator to manipulate the view
 * without the use of window listeners
 * @author Calvin
 *
 */
public class SmallViewerPanelDecorator implements DisplayDecorator {
	
	private ViewManipulator manipulator;
     	private static final String LOD_COARSE = "Coarse";
	private static final String LOD_MEDIUM = "Medium";
	private static final String LOD_FINE = "Fine";
	
	/**
	 * Gets the view manipulator used by this decorator
	 * @return Returns the view manipulator
	 */
	public ViewManipulator getManipulator() {
		return manipulator;
	}

	public JPanel decorateDisplay(final Display display, ViewOrientation orientation) {
		final JPanel panel = new JPanel(new BorderLayout());
		final Display graphicDisplay = display;
		final ViewOrientation finalOrientation = orientation;
		final JPanel top = new JPanel();
		top.setLayout(new BoxLayout(top, BoxLayout.X_AXIS));
		
		manipulator = null;
		if(orientation.isPerspective())
			manipulator = new ViewManipulator((PerspectiveView)display.getView());
		else
			manipulator = new ViewManipulator(display.getView());
		
		display.setTranslateListener(manipulator.new TranslateListener());
		display.setZoomListener(manipulator.new ZoomListener());
		display.setRotateListener(manipulator.new RotateListener());
		

		final JButton options = new JButton("Options");
		
		final JComboBox comboBox = new ViewOrientationSelector(orientation);
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ViewOrientation orientation = (ViewOrientation) comboBox.getSelectedItem();
				Display c = (Display) ((BorderLayout)panel.getLayout()).getLayoutComponent(BorderLayout.CENTER);
				panel.remove(c);
				final Display display = DisplayFactory.createDisplay(orientation, true);
				if(c.getModel() != null){
				    display.setModel(c.getModel(), false);
				    display.addRenderable(c.getRenderables());
				}
				else{
				    display.setModel(null,false);
				}
				
				if(c.getOwner() != null) {
					c.getOwner().registerDisplay(display);
					c.getOwner().unregisterDisplay(c);
					
				}
				panel.add(display, BorderLayout.CENTER);
				top.remove(2);
				ViewManipulator manipulator = null;
				if(orientation.isPerspective())
					manipulator = new ViewManipulator((PerspectiveView)display.getView());
				else
					manipulator = new ViewManipulator(display.getView());
				top.add(manipulator, 2);
				
				options.removeActionListener(options.getActionListeners()[0]);
				options.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						DisplayOptions dialog = new DisplayOptions(display);
						dialog.createAndShowGUI();
					}
				});
				
				panel.revalidate();
				
				
			}
		});

		final Object[] objects = { LOD_COARSE, LOD_MEDIUM, LOD_FINE };
		final JComboBox lodBox = new JComboBox(objects);
		//lodBox.setEditable(false);
		lodBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			    Object selected = lodBox.getSelectedItem();
			    RnaRendererGraphController lod = (RnaRendererGraphController) display.getModel();
			    if(selected == LOD_COARSE)
				lod.setRefinementLevel(LODCapable.LOD_COARSE);
			    else if(selected == LOD_MEDIUM)
				lod.setRefinementLevel(LODCapable.LOD_MEDIUM);
			    else
				lod.setRefinementLevel(LODCapable.LOD_FINE);
				
			    
			    lod.updateMeshes();
			    display.display();
			    
			}	
		});
		if(display.getModel() instanceof LODCapable) {
		    RnaRendererGraphController lod = (RnaRendererGraphController) display.getModel();
		    int refinement = lod.getRefinementLevel();
		    if(refinement == LODCapable.LOD_COARSE)
			lodBox.setSelectedIndex(0);
		    else if(refinement == LODCapable.LOD_MEDIUM)
			lodBox.setSelectedIndex(1);
		    else
			lodBox.setSelectedIndex(2);
		} 

		final JButton shrinkScreen = new JButton("View Size");
		shrinkScreen.setToolTipText("Switch the screen size to either full screen or half screen");
		shrinkScreen.addActionListener(new ActionListener() {
			boolean fullScreen = true;
			public void actionPerformed(ActionEvent e){
			    DisplayFactory.setDecorator(new DefaultDecorator());
			    //DisplayFactory.getDecorator().decorateDisplay(graphicDisplay,finalOrientation);
			}
		});

		top.add(comboBox, 0);
		top.add(Box.createHorizontalGlue(), 1);
		top.add(new JLabel("Graphical Detail "),2);
		top.add(lodBox,3);
		top.add(manipulator, 4);
		top.add(shrinkScreen,5);
		
		
		options.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DisplayOptions dialog = new DisplayOptions(display);
				dialog.createAndShowGUI();
			}
		});
		
		top.add(options, 6);
		
		Dimension dim = new Dimension(250,0);
		JPanel tempPanel = new JPanel(new BorderLayout());
		tempPanel.add(Box.createRigidArea(dim),BorderLayout.WEST);
		tempPanel.add(graphicDisplay, BorderLayout.CENTER);
		tempPanel.add(Box.createRigidArea(dim),BorderLayout.EAST);

		panel.add(top, BorderLayout.NORTH);
		panel.add(tempPanel, BorderLayout.CENTER);
		return panel;
	}

}
