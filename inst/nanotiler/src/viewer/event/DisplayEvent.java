package viewer.event;
import viewer.display.*;

/**
 * Event when a display is changed
 * @author Calvin
 *
 */
public class DisplayEvent {
	
	/**
	 * View is added
	 */
	public static final int VIEW_ADDED = 1;
	
	/**
	 * View is removed
	 */
	public static final int VIEW_REMOVED = 2;
	
	/**
	 * Layout changed
	 */
	public static final int LAYOUT_CHANGED = 3;

	private Object source;
	private int id;
	private Display display;
	
	/**
	 * Constuct event with only a source
	 * @param source
	 */
	public DisplayEvent(Object source) {
		this.source = source;
	}
	
	/**
	 * Construct fully detailed event.
	 * @param source Source of the event
	 * @param id ID of the event
	 * @param display Display triggering the event.
	 */
	public DisplayEvent(Object source, int id, Display display) {
		this(source);
		this.id = id;
		this.display = display;
	}
	
	/**
	 * Get the source of the event
	 * @return Returns the source
	 */
	public Object getSource() {
		return source;
	}
	
	/**
	 * Get the ID of the event.
	 * @return Returns the id of the event
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Get the display triggering or involved in
	 * the event.
	 * @return
	 */
	public Display getDisplay() {
		return display;
	}
}
