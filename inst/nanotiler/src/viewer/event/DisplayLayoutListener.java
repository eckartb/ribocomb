package viewer.event;

/**
 * Listener that is notified when
 * a change occurs to the layout of the
 * displays.
 * @author Calvin
 *
 */
public interface DisplayLayoutListener {
	/**
	 * Called when the display layout is changed
	 * @param e Event specific information
	 */
	public void displayLayoutChanged(DisplayEvent e);
}
