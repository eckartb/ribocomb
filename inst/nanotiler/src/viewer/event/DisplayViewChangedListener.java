package viewer.event;

/**
 * Listener interface for when a display is changed.
 * @author Calvin
 *
 */
public interface DisplayViewChangedListener {
	
	/**
	 * Called when a display is changed.
	 * @param e Event specific information
	 */
	public void displayChanged(DisplayEvent e);
}
