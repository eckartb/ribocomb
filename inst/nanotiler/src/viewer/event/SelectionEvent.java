package viewer.event;

import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DTools;
import rnadesign.rnamodel.RnaTools;
import commandtools.CommandApplication;
import commandtools.CommandException;

/**
 * Contains information pertanent to when
 * an object is selected through a display.
 * @author Calvin
 *
 */
public class SelectionEvent {

	private Object3D selectedObject;
	private Object source;
	private boolean selectionAdjusting = false;
	
	public SelectionEvent(Object3D selectedObject, Object source) {
		super();
		this.selectedObject = selectedObject;
		this.source = source;
		System.out.println("3D object selected: " + selectedObject.getFullName());
		System.out.println("Closest objects for given class names: " 
				   + RnaTools.reportClosestRnaObjects(Object3DTools.findRoot(this.selectedObject),
								   this.selectedObject.getPosition()));
	}

	
	public SelectionEvent(Object3D selectedObject, Object source, boolean selectionAdjusting) {
		this(selectedObject, source);
		this.selectionAdjusting = selectionAdjusting;
	}
	
	/**
	 * Gets the selected object.
	 * @return Returns the object selected or deslected
	 */
	public Object3D getSelectedObject() {
		return selectedObject;
	}
	
	/**
	 * Sets the selected object.
	 * @param selectedObject Object that was selected
	 */
	public void setSelectedObject(Object3D selectedObject) {
		this.selectedObject = selectedObject;
	}

        public void setSelectedObject(CommandApplication application,Object3D selectedObject){
	    try{
		application.runScriptLine("select "+Object3DTools.getFullName(selectedObject));
	    }
	    catch(CommandException e){
		System.out.println(e + ": " + e.getMessage());
	    }
	    setSelectedObject(selectedObject);
	}

	/**
	 * Get the source of the selection event.
	 * @return Source of the selection event.
	 */
	public Object getSource() {
		return source;
	}
	
	/**
	 * Set the source of the selection event.
	 * @param source Source of the selection event.
	 */
	public void setSource(Object source) {
		this.source = source;
	}
	
	/**
	 * Is the selection currently changing. If it is,
	 * a nother selection event will follow. Client
	 * code should make use of this fact.
	 * @return Returns true if more selection events are
	 * on the way or false if this is the last one
	 * for an unknown period of time.
	 */
	public boolean isSelectionAdjusting() {
		return selectionAdjusting;
	}
	
	
}
