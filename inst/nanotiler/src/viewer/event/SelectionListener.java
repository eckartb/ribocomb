package viewer.event;

/**
 * Listener interface to be notified of object3D selection
 * events.
 * @author Calvin
 *
 */
public interface SelectionListener {
	/**
	 * Called when an object is selected.
	 * @param e Event specific information
	 */
	public void objectSelected(SelectionEvent e);
	
	/**
	 * Called when an object is deselected
	 * @param e Event specific information
	 */
	public void objectDeselected(SelectionEvent e);
}
