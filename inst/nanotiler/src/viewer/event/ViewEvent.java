package viewer.event;

import tools3d.*;

/**
 * Event that contains information about view orientation changes,
 * such as translation, rotation, or zoom.
 * @author Calvin
 *
 */
public class ViewEvent {

	/**
	 * View changed through the concept of local movement
	 * (View is using its own coordinate system)
	 */
	public static final int LOCAL_MOVEMENT = 0;
	
	/**
	 * View changed through the concept of world movement
	 * (View is using the world coordinate system)
	 */
	public static final int WORLD_MOVEMENT = 1;
	
	/**
	 * View changed through the concept of point movement
	 * (View is pointing at a point in space and is
	 * rotating, zooming, translating around this point)
	 */
	public static final int POINT_MOVEMENT = 2;
	
	/**
	 * View rotated
	 */
	public static final int ROTATION = 0;
	
	/**
	 * View translated
	 */
	public static final int TRANSLATION = 1;
	
	/**
	 * View zoomed
	 */
	public static final int ZOOM = 2;
	
	private int movementRelation;
	private int movementType;
	
	private Vector4D oldPosition;
	private Vector4D newPosition;
	
	private double oldZoom;
	private double newZoom;
	
	private Vector4D oldRotation;
	private Vector4D newRotation;
	private Vector4D oldNormal;
	private Vector4D newNormal;
	
	public ViewEvent() {
		
	}
	
	/**
	 * Construct view event
	 * @param movementType Type of movement. Should be either
	 * <ul><li>ViewEvent.Rotation</li><li>ViewEvent.TRANSLATION</li>
	 * <li>ViewEvent.ZOOM</li></ul>
	 * @param movementRelation Type of movement relation. Should be either
	 * <ul><li>ViewEvent.LOCAL_MOVEMENT</li><li>ViewEvent.POINT_MOVEMENT</li>
	 * <li>ViewEvent.WORLD_MOVEMENT</li></ul>
	 */
	public ViewEvent(int movementType, int movementRelation) {
		this.movementRelation = movementRelation;
		this.movementType = movementType;
	}
	
	/**
	 * Constructs event using movement type and two vectors. If
	 * type is ROTATION, vectors denote the old and new rotations. If
	 * type is TRANSLATION, vectors denote the old and new positions.
	 * @param movementType Type of movement
	 * @param movementRelation Type of movement relation
	 * @param v1 Old vector
	 * @param v2 New Vector
	 */
	public ViewEvent(int movementType, int movementRelation,
			Vector4D v1, Vector4D v2) {
		this(movementType, movementRelation);
		if(movementType == TRANSLATION) {
			this.oldPosition = v1;
			this.newPosition = v2;
		}
		else {
			this.oldRotation = v1;
			this.newRotation = v2;
		}
	}
	
	/**
	 * Constructs event using movement type and directions and normals.
	 * @param movementType Type of movement
	 * @param movementRelation Type of relation
	 * @param oldDirection Either old point in space or old direction
	 * @param newDirection Either new point in space or new direction
	 * @param oldNormal Old normal
	 * @param newNormal New normal
	 */
	public ViewEvent(int movementType, int movementRelation,
			Vector4D oldDirection, Vector4D newDirection,
			Vector4D oldNormal, Vector4D newNormal) {
		this(movementType, movementRelation, oldDirection, newDirection);
		this.oldNormal = oldNormal;
		this.newNormal = newNormal;
	}
	
	
	/**
	 * Construct event for change in zoom of a view
	 * @param movementType Type of movement
	 * @param movementRelation Type of relation
	 * @param oldZoom Old zoom
	 * @param newZoom New zoom
	 */
	public ViewEvent(int movementType, int movementRelation,
			double oldZoom, double newZoom) {
		this(movementType, movementRelation);
		this.oldZoom = oldZoom;
		this.newZoom = newZoom;
	}
	
	/**
	 * Gets the old normal
	 * @return Returns the old normal
	 */
	public Vector4D getOldNormal() {
		return oldNormal;
	}
	
	/**
	 * Gets the new normal
	 * @return
	 */
	public Vector4D getNewNormal() {
		return newNormal;
	}
	
	/**
	 * Gets the new position
	 * @return
	 */
	public Vector4D getNewPosition() {
		return newPosition;
	}
	/**
	 * Gets the new rotation
	 * @return
	 */
	public Vector4D getNewRotation() {
		return newRotation;
	}
	
	/**
	 * Gets the new zoom
	 * @return
	 */
	public double getNewZoom() {
		return newZoom;
	}
	
	/**
	 * Gets the old position
	 * @return
	 */
	public Vector4D getOldPosition() {
		return oldPosition;
	}
	
	/**
	 * Gets the old rotation
	 * @return
	 */
	public Vector4D getOldRotation() {
		return oldRotation;
	}
	
	/**
	 * Gets the old zoom
	 * @return
	 */
	public double getOldZoom() {
		return oldZoom;
	}
	
	/**
	 * Get the movement relation type.
	 * @return Returns one of the following
	 * <ul><li>ViewEvent.LOCAL_MOVEMENT</li><li>ViewEvent.POINT_MOVEMENT</li>
	 * <li>ViewEvent.WORLD_MOVEMENT</li></ul>
	 */
	public int getMovementRelation() {
		return movementRelation;
	}
	
	/**
	 * Gets the type of movement
	 * @return Returns one of the following:
	 * <ul><li>ViewEvent.Rotation</li><li>ViewEvent.TRANSLATION</li>
	 * <li>ViewEvent.ZOOM</li></ul>
	 */
	public int getMovementType() {
		return movementType;
	}
	
	
}
