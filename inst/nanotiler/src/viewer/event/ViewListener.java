package viewer.event;

/**
 * Listener interface for changes to a view
 * @author Calvin
 *
 */
public interface ViewListener {
	
	/**
	 * Notified when a view is changed. The
	 * event data structure hold the information
	 * corresponding to the change
	 * @param e Event specific information
	 */
	public void viewChanged(ViewEvent e);
}
