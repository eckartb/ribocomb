package viewer.event;

import java.util.*;
import tools3d.*;

/**
 * The View Listener Manager manages view listeners
 * and provides a way to notify them for specific events.
 * Instead of a view managing listeners itself, it holds
 * an instance of this class which does all that work.
 * @author Calvin
 *
 */
public class ViewListenerManager {
	
	private ArrayList<ViewListener> listeners =
		new ArrayList<ViewListener>();
	
	/**
	 * Adds a view listener
	 * @param listener Listener to add
	 */
	public void addViewListener(ViewListener listener) {
		listeners.add(listener);
	}
	
	
	/**
	 * Removes a view listener
	 * @param listener Listener to remove
	 * @return Returns true if the listener was removed or false
	 * if it couldn't be removed.
	 */
	public boolean removeViewListener(ViewListener listener) {
		return listeners.remove(listener);
	}
	
	/**
	 * Removes all listeners
	 *
	 */
	public void removeAllListeners() {
		listeners.clear();
	}
	
	/**
	 * Notifies listeners of view translation
	 * @param local Local movement or world movement?
	 * @param oldPosition Old position of view
	 * @param newPosition New position of view
	 */
	public void notifyViewTranslation(boolean local, Vector4D oldPosition, Vector4D newPosition) {
		for(ViewListener l : listeners)
			l.viewChanged(new ViewEvent(ViewEvent.TRANSLATION, local ? ViewEvent.LOCAL_MOVEMENT : ViewEvent.WORLD_MOVEMENT,
					oldPosition, newPosition));
	}
	
	/**
	 * Notifies listeners of view zoom
	 * @param local Local movement or world movement?
	 * @param oldZoom Old zoom level
	 * @param newZoom New zoom level
	 */
	public void notifyViewZoom(boolean local, double oldZoom, double newZoom) {
		for(ViewListener l : listeners)
			l.viewChanged(new ViewEvent(ViewEvent.ZOOM, local ? ViewEvent.LOCAL_MOVEMENT : ViewEvent.WORLD_MOVEMENT,
					oldZoom, newZoom));
	}
	
	/**
	 * Notifies listeners of view rotation
	 * @param local Local rotation or world rotation?
	 * @param oldDirection Old direction of view
	 * @param oldNormal Old direction of normal
	 * @param newDirection New Direction of view
	 * @param newNormal New direction of normal
	 */
	public void notifyViewRotation(boolean local, Vector4D oldDirection, Vector4D oldNormal,
			Vector4D newDirection, Vector4D newNormal) {
		for(ViewListener l : listeners)
			l.viewChanged(new ViewEvent(ViewEvent.ROTATION, local ? ViewEvent.LOCAL_MOVEMENT : ViewEvent.WORLD_MOVEMENT,
					
					oldDirection, newDirection, oldNormal, newNormal));
	}
	
	/**
	 * Notify listeners of general view change. No
	 * information passed in event.
	 *
	 */
	public void notifyViewChanged() {
		for(ViewListener l : listeners) {
			l.viewChanged(new ViewEvent());
		}
	}
	
	
	
}
