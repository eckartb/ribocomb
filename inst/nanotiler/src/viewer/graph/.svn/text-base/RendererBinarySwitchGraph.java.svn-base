package viewer.graph;

import javax.media.opengl.GL;

/**
 * The RendererBinarySwitchGraph can turn on and off parts of the
 * renderable scene graph based on predicates. If the predicate
 * is true, the RendererBinarySwitchGraph will proceed to render
 * its children. If the predicate is false, the graph will not
 * render any of its children.
 * @author Calvin
 *
 */
public class RendererBinarySwitchGraph extends RenderableGraphImp {

	/**
	 * Predicate interface.
	 * @author Calvin
	 *
	 */
	public static interface Predicate {
		/**
		 * Is the predicate true?
		 * @return Returns true if the predicate is true or
		 * false if the predicate is not true.
		 */
		public boolean isTrue();
	}
	
	private Predicate predicate;
	
	/**
	 * Construct around a predicate
	 * @param predicate Predicate
	 */
	public RendererBinarySwitchGraph(Predicate predicate) {
		this.predicate = predicate;
	}
	
	public String getClassName() {
		return "RendererBinarySwitchGraph";
	}
	
	/**
	 * Get the predicate
	 * @return Returns the predicate
	 */
	public Predicate getPredicate() {
		return predicate;
	}
	
	/**
	 * Is the predicate true?
	 * @return Returns true if the predicate is true
	 * or false if the predicate is false.
	 */
	public boolean evaluatePredicate() {
		return predicate.isTrue();
	}
	
	public void render(GL gl) {
		if(predicate.isTrue())
			super.render(gl);
	}
}
