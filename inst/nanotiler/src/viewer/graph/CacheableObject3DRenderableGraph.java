package viewer.graph;

import javax.media.opengl.GL;

import viewer.graphics.AdvancedMesh;
import viewer.graphics.ColorModel;
import viewer.graphics.MeshRenderer;
import viewer.graphics.AdvancedSolidMeshRenderer;
import viewer.graphics.AdvancedWireframeMeshRenderer;
import viewer.graphics.MeshCacheable;
import tools3d.objects3d.Object3D;

/**
 * Many 3D objects have complex meshes that are computationally intensive
 * to generate. Rather than continually generate the meshes on the fly,
 * the CacheableObject3DRenerableGraph can be used to store the mesh
 * until it needs to be updated. This drastically improves the rendering
 * speed.
 * @author Calvin
 *
 */
public abstract class CacheableObject3DRenderableGraph extends Object3DRenderableGraph implements MeshCacheable {

	/**
	 * Construct from object
	 * @param object Object that is rendered
	 */
	public CacheableObject3DRenderableGraph(Object3D object) {
		super(object);
	}
	
	public String getClassName() {
		return "CacheableObject3DRenderableGraph";
	}
	
	private AdvancedMesh mesh;
	
	public void render(GL gl) {
		AdvancedMesh m;
		if(isMeshCached())
			m = getCachedMesh();
		else {
			m = generateMesh();
		}
		
		MeshRenderer renderer;
		if(isWireframeMode())
			renderer = new AdvancedWireframeMeshRenderer(m);
		else
			renderer = new AdvancedSolidMeshRenderer(m);
		
		renderer.render(gl);
		
		super.render(gl);
	}
	
	

	@Override
	public void setColorModel(ColorModel model) {
		super.setColorModel(model);
		updateMesh();
	}

	public AdvancedMesh getCachedMesh() {
		return mesh;
	}

	public boolean isMeshCached() {
		return mesh != null;
	}

	public void setMeshCached(boolean meshCached) {
		if(meshCached)
			mesh = generateMesh();
		else
			mesh = null;
	}

	public void updateMesh() {
		if(isMeshCached()) {
			mesh = generateMesh();
		}
	}
	
	public void update() {
		updateMesh();
	}
	
	public void setRefinementLevel(int refinementLevel) {
		super.setRefinementLevel(refinementLevel);
		
		if(isMeshCached()) {
			updateMesh();
		}
	}
	
	/**
	 * Generates the mesh that will be cached
	 * @return Returns the generated mesh
	 */
	public abstract AdvancedMesh generateMesh();
	
	public Object clone() {
		CacheableObject3DRenderableGraph graph = (CacheableObject3DRenderableGraph) super.clone();
		//graph.mesh = (AdvancedMesh) mesh.clone();
		return graph;
	}

}