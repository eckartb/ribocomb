package viewer.graph;

import viewer.graphics.LODCapable;

/**
 * The GeometryRenderableGraph is responsible for rendering
 * geometry. Accordingly, geometry can be rendered in either
 * a solid mode or wireframe mode. Additionally, it can
 * be rendered differently based upon levels of detail.
 * @author Calvin
 *
 */
public class GeometryRenderableGraph extends RenderableGraphImp implements LODCapable {
	
	private boolean wireframeMode = false;
	private int refinementLevel = LOD_MEDIUM;

	/**
	 * Is the geometry being rendered in wireframe mode?
	 * @return Returns true if the node is in wireframe
	 * mode or false if the node is not in wireframe mode.
	 */
	public boolean isWireframeMode() {
		return wireframeMode;
	}
	
	/**
	 * Set the wireframe mode
	 * @param wireframeMode True for wireframe mode, false for solid mode.
	 */
	public void setWireframeMode(boolean wireframeMode) {
		this.wireframeMode = wireframeMode;
	}

	public int getRefinementLevel() {
		return refinementLevel;
	}

	public void setRefinementLevel(int refinementLevel) {
		this.refinementLevel = refinementLevel;
	}
	
	public String getClassName() {
		return "GeometryRenderableGraph";
	}
}
