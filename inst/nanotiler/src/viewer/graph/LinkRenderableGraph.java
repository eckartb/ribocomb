package viewer.graph;

import tools3d.*;
import tools3d.objects3d.*;
import rnadesign.rnamodel.Atom3D;
import viewer.graphics.*;
import viewer.util.*;
import rnadesign.rnamodel.Atom3D;
import javax.media.opengl.GL;
import java.awt.Color;


/**
 * The LinkRenderableGraph is responsible for rendering links.
 * @author Calvin
 *
 */
public class LinkRenderableGraph extends GeometryRenderableGraph implements Colorable {

	private static final Mesh CYLINDER = PrimitiveFactory.generateCylinder(new Vector4D(1.0, 0.0, 0.0, 0.0), 0.5, 2.0, 1, 8);

	private RenderMode renderMode = RenderMode.Line;
	
	private Link link;
	
	private ColorModel model;

	private Color linkColor = Color.white;

	public static enum RenderMode {
		Line, Cylinder, Capsule
	}
	
	/**
	 * Construct a new node
	 * @param link Link to render
	 */
	public LinkRenderableGraph(Link link) {
		this.link = link;
	}


    public void render(GL gl) {
	renderLink(link, gl);
    }

    public RenderMode getRenderMode() {
	return renderMode;
    }
    

    public Color getLinkColor() {
	return linkColor;
    }

    private Color getCovalentBondLinkColor(Link link) {
	assert link.getObj1() instanceof Atom3D;
	assert link.getObj2() instanceof Atom3D;
	Atom3D atom1 = (Atom3D)link.getObj1();
	Atom3D atom2 = (Atom3D)link.getObj2();
	Material m1 = getColorModel().getAtomMaterial(atom1);
	Material m2 = getColorModel().getAtomMaterial(atom2);
	ColorVector ambient1 = m1.getAmbient();
	ColorVector ambient2 = m2.getAmbient();
	return getAverageColor(ambient1.getAWTColor(),ambient2.getAWTColor());
       
	// TODO : quick fix, better: average between material
	//return m1.getAmbient().getAWTColor();
    }

    private Color getAverageColor(Color color1, Color color2){
	double  blue = (color1.getBlue() + color2.getBlue()) / 2.0;
	double  red = (color1.getRed() + color2.getRed()) / 2.0;
	double  green = (color1.getGreen() + color2.getGreen()) / 2.0;
	double  alpha = (color1.getAlpha() + color2.getAlpha()) / 2.0;

	// System.out.println("Blue: "+blue+" Red: "+red+" Green: "+green+" Alpha: "+alpha);

	return new Color((int) (red), (int) ( green), (int) ( blue), (int) (alpha));
    }

    private boolean isCovalentBondLink(Link link) {
	return (link.getObj1() instanceof Atom3D) && (link.getObj2() instanceof Atom3D);
    }

    public Color getLinkColor(Link link) {
	if (isCovalentBondLink(link)) {
	    return getCovalentBondLinkColor(link);
	}
	// other cases: base pair interaction, graph point links, constraints etc
	return linkColor;
    }
	
	public void renderLink(Link l, GL gl) {
		Object3D o1 = l.getObj1();
		Object3D o2 = l.getObj2();
		
		Material material = new Material(getLinkColor(l), true);
		Material.renderMaterial(gl, material);
		
		switch(getRenderMode()) {
		case Cylinder:
			Vector4D axis = new Vector4D(o1.getPosition().minus(o2.getPosition()));
			double length = axis.length() / 2.0;
			Vector4D midpoint = new Vector4D(o1.getPosition().plus(o2.getPosition()));
			midpoint = midpoint.mul(0.5);
			midpoint.setW(1.0);
			axis.setW(0.0);
			axis.normalize();
			MeshRenderer renderer;
			if(isWireframeMode()) {
				renderer = new WireframeMeshRenderer(CYLINDER);
			}
			else {
				renderer = new SolidMeshRenderer(CYLINDER);
			}
			gl.glEnable(GL.GL_NORMALIZE);
			gl.glPushMatrix();
			gl.glTranslated(midpoint.getX(), midpoint.getY(), midpoint.getZ());
			Vector4D rotationAxis = axis.cross(Tools.X_AXIS);
			double angle = Math.acos(axis.dot(Tools.X_AXIS));
			rotationAxis.normalize();
			Matrix4D rotation = Tools.getRotationMatrix(rotationAxis, angle);
			double[] matrix = rotation.getArray();
			gl.glMultMatrixd(matrix, 0);
			gl.glScaled(length, 1.0, 1.0);
			renderer.render(gl);
			gl.glPopMatrix();
			gl.glDisable(GL.GL_NORMALIZE);
			
			break;
		case Capsule:
		    break;
		case Line:
		    // System.out.println("Drawling line for " + o1.getName() + " " + o2.getName());
			gl.glBegin(GL.GL_LINES);
			gl.glVertex3d(o1.getPosition().getX(), o1.getPosition().getY(), o1.getPosition().getZ());
			gl.glVertex3d(o2.getPosition().getX(), o2.getPosition().getY(), o2.getPosition().getZ());
			gl.glEnd();
			
			break;
		default:
		    assert false;
		}
	}


	/**
	 * Get the link this node renders
	 * @return Link being rendered
	 */
	public Link getLink() {
		return link;
	}

	public String getClassName() {
		return "LinkRenderableGraph";
	}

	public ColorModel getColorModel() {
		return model;
	}

	public void setColorModel(ColorModel model) {
		this.model = model;
		
	}
	
	

}
