package viewer.graph;

import tools3d.objects3d.Object3D;
import tools3d.BoundingVolume;
import viewer.graphics.ColorModel;
import viewer.graphics.Colorable;
import viewer.graphics.DefaultColorModel;

/**
 * The Object3DRenderableGraph is responsible for rendering
 * an Object3D.
 * @author Calvin
 *
 */
public class Object3DRenderableGraph extends GeometryRenderableGraph implements Colorable {
	
	private Object3D object;
	private ColorModel model = new DefaultColorModel();

	/**
	 * Construct an Object3DRenderableGraph
	 * @param object Object to render
	 */
	public Object3DRenderableGraph(Object3D object) {
		this.object = object;
	}
	
	/**
	 * Gets the object this node renders
	 * @return Returns the object
	 */
	public Object3D getObject() {
		return object;
	}
	
	/**
	 * Get the bounding volume of the object being rendered
	 * @return Returns the bounding volume.
	 */
	public BoundingVolume getBoundingVolume() {
		return object.getBoundingVolume();
	}
	
	public String getClassName() {
		return "Object3DRenderableGraph";
	}

	public ColorModel getColorModel() {
		return model;
	}

	public void setColorModel(ColorModel model) {
		this.model = model;
	}
	
	
	
	
	
	
	
	
}
