package viewer.graph;

import java.util.List;

import viewer.graphics.CloneableRenderableModel;

/**
 * A node of the renderable scene graph. Meant to be
 * subclassed to perform specific functions.
 * @author Calvin
 *
 */
public interface RenderableGraph extends CloneableRenderableModel {

	/**
	 * Number of children of this node
	 * @return Returns the number of children
	 */
	public int size();
	
	/**
	 * Get the child at the specified index
	 * @param index Index of child node
	 * @return Returns the child node at the specified index
	 */
	public RenderableGraph getChild(int index);
	
	/**
	 * Get a list of all the children of this
	 * node
	 * @return Returns a list of all the children
	 */
	public List<RenderableGraph> getChildren();
	
	/**
	 * Adds a child to this node
	 * @param graph Child node to add to this node
	 */
	public void addChild(RenderableGraph graph);
	
	/**
	 * Inserts a child at the specified index
	 * @param graph Child to insert
	 * @param index Index to insert child at
	 */
	public void insertChild(RenderableGraph graph, int index);
	
	/**
	 * Replaces the child at the specified index with a new child
	 * @param graph Child to replace old child with
	 * @param index Index of old child to replace
	 * @return Returns the replaced child
	 */
	public RenderableGraph replaceChild(RenderableGraph graph, int index);
	
	/**
	 * Replace this node with another one. Copies the children to the new node
	 * and sets the parent of the new node.
	 * @param graph Node to replace this node with
	 * @return Returns this node.
	 */
	public RenderableGraph replace(RenderableGraph graph);
	
	/**
	 * Remove a child from this node.
	 * @param graph Child to remove
	 * @return Returns true if the child is removed and
	 * false if otherwise
	 */
	public boolean removeChild(RenderableGraph graph);
	
	/**
	 * Remove a child from this node.
	 * @param index Index of the child to remove
	 * @return Returns the removed child.
	 */
	public RenderableGraph removeChild(int index);
	
	/**
	 * Gets the index of the child
	 * @param graph Child to get index of
	 * @return Returns the index of the child node.
	 */
	public int indexOf(RenderableGraph graph);
	
	/**
	 * Get the parent of this node
	 * @return Returns the parent node or null
	 * if the node has no parent.
	 */
	public RenderableGraph getParent();
	
	/**
	 * Sets the parent of this node. Should
	 * be used carefull as the tree structure can
	 * easily be broken.
	 * @param graph
	 */
	public void setParent(RenderableGraph graph);
	
	/**
	 * Is the node marked?
	 * @return Returns true if the node is marked and false
	 * if otherwise.
	 */
	public boolean isMarked();
	
	/**
	 * Mark the node
	 * @param mark True to mark the node or false to unmark the node.
	 */
	public void setMark(boolean mark);
	
	/**
	 * Get the name of the node
	 * @return Returns the name of the node.
	 */
	public String getName();
	
	/**
	 * Get the classname of the node
	 * @return Returns the class name of the node.
	 */
	public String getClassName();
	
	/**
	 * Get the depth of the node in the scene graph
	 * @return Returns the depth.
	 */
	public int getDepth();
	
	/**
	 * Get the index of this node.
	 * @return Returns the index of this node in its parents
	 * list of children or 0 if it doesn't have a parent.
	 */
	public int getIndex();
	
	/**
	 * Update the node.
	 *
	 */
	public void update();
}
