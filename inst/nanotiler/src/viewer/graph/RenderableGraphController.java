package viewer.graph;

import javax.media.opengl.GL;

import viewer.graphics.ColorModel;
import viewer.graphics.Colorable;
import viewer.graphics.LODCapable;
import viewer.graphics.MeshCacheable;
import viewer.graphics.RenderableModel;

/**
 * Controls and manages a renderable graph. Allows various
 * parameters in specific graph nodes to be set from the top level.
 * @author Calvin
 *
 */
public class RenderableGraphController implements LODCapable, Colorable,
		RenderableModel {
	
        private int refinementLevel = LODCapable.LOD_COARSE; //   LODCapable.LOD_MEDIUM;
	private RenderableGraph root;
	private boolean meshCached;
	private ColorModel colorModel;
	private boolean wireframeMode = false;
	
	/**
	 * Get the root of the scene graph
	 * @return Returns the root node
	 */
	public RenderableGraph getRoot() {
		return root;
	}
	
	/**
	 * Set the root node of the scene graph
	 * @param root Root node of the scene graph
	 */
	public void setRoot(RenderableGraph root) {
		this.root = root;
	}

	public int getRefinementLevel() {
		return refinementLevel;
	}

	public void setRefinementLevel(int refinementLevel) {
		this.refinementLevel = refinementLevel;
		setRefinementLevel(root);
	}

	private void setRefinementLevel(RenderableGraph graph) {
		
		if(graph instanceof LODCapable) {
			((LODCapable)graph).setRefinementLevel(refinementLevel);
		}
		
		for(int i = 0; i < graph.size(); ++i) 
			setRefinementLevel(graph.getChild(i));
	}
	
	public ColorModel getColorModel() {
		return colorModel;
	}

	public void setColorModel(ColorModel model) {

		this.colorModel = model;
		setColorModel(root);
	}
	
	private void setColorModel(RenderableGraph graph) {
		if(graph instanceof Colorable) {
			((Colorable)graph).setColorModel(colorModel);
		}
		
		for(int i = 0; i < graph.size(); ++i)
			setColorModel(graph.getChild(i));
	}

	/**
	 * Are meshes in the scene graph cached?
	 * @return Returns true if the meshes are cached or
	 * false if otherwise.
	 */
	public boolean isMeshCached() {
		return meshCached;
	}

	/**
	 * Set whether or not meshes should be cached.
	 * @param meshCached True if meshes should be cached
	 * and false if otherwise
	 */
	public void setMeshCached(boolean meshCached) {
		this.meshCached = meshCached;
		
		setMeshCached(root);
	}
	
	private void setMeshCached(RenderableGraph root) {
		if(root instanceof MeshCacheable)
			((MeshCacheable)root).setMeshCached(meshCached);
		
		for(int i = 0; i < root.size(); ++i)
			setMeshCached(root.getChild(i));
	}

	public void render(GL gl) {
		root.render(gl);
	}
	
	/**
	 * Update all the meshes in
	 * the scenegraph
	 *
	 */
	public void updateMeshes() {
		udpdateMeshes(root);
	}
	
	private void udpdateMeshes(RenderableGraph root) {
		if(root instanceof MeshCacheable)
			((MeshCacheable)root).updateMesh();
		
		for(int i = 0; i < root.size(); ++i)
			udpdateMeshes(root.getChild(i));
	}
	
	/**
	 * Set the wireframe mode for all geometry nodes in the scene graph.
	 * @param wireframeMode True for wireframe mode or false
	 * for solid mode.
	 */
	public void setWireframeMode(boolean wireframeMode) {
		this.wireframeMode = wireframeMode;
		setWireframeMode(root);
	}
	
	/**
	 * Are the geometry nodes of the scenegraph in wireframe mode?
	 * @return Returns true if the nodes are in wireframe mode or 
	 * false if they are in solid mode.
	 */
	public boolean isWireframeMode() {
		return wireframeMode;
	}
	
	private void setWireframeMode(RenderableGraph graph) {
		if(graph instanceof GeometryRenderableGraph) {
			((GeometryRenderableGraph)graph).setWireframeMode(wireframeMode);
		}
		
		for(int i = 0; i < graph.size(); ++i)
			setWireframeMode(graph.getChild(i));
	}

}
