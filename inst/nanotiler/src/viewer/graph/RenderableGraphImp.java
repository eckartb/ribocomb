package viewer.graph;

import java.util.List;
import java.util.ArrayList;

import javax.media.opengl.GL;

/**
 * Default implementation of the RenderableGraph
 * interface. All classes wanting to implement the
 * RenderableGraph interface should extend this class.
 * @author Calvin
 *
 */
public class RenderableGraphImp implements RenderableGraph {
	
	private ArrayList<RenderableGraph> children =
		new ArrayList<RenderableGraph>();
	
	private RenderableGraph parent = null;
	
	private boolean marked = false;

	public boolean isMarked() {
		return marked;
	}

	public void setMark(boolean mark) {
		this.marked = mark;
	}

	public void addChild(RenderableGraph graph) {
		children.add(graph);
		graph.setParent(this);

	}

	public void insertChild(RenderableGraph graph, int index) {
		children.add(index, graph);
		graph.setParent(this);

	}
	
	public RenderableGraph getChild(int index) {
		return children.get(index);
	}
	

       @SuppressWarnings(value="unchecked")
	public List<RenderableGraph> getChildren() {
		return (List<RenderableGraph>) children.clone();
	}

	public String getClassName() {
		return "RenderableGraphImp";
	}

	public int getDepth() {
		if(getParent() == null)
			return 0;
		
		return 1 + getParent().getDepth();
	}

	public String getName() {
		if(parent == null)
			return getClassName() + "0";
		
		return parent.getName() + "." + getClassName() + getIndex();
	}

	public RenderableGraph getParent() {
		return parent;
	}
	
	public void setParent(RenderableGraph parent) {
		this.parent = parent;
	}

	public int indexOf(RenderableGraph graph) {
		return children.indexOf(graph);
	}

	public boolean removeChild(RenderableGraph graph) {
		return children.remove(graph);
	}

	public RenderableGraph removeChild(int index) {
		return children.remove(index);
	}

	public int size() {
		return children.size();
	}
	
	public int getIndex() {
		if(getParent() == null) {
			return 0;
		}
		
		return getParent().indexOf(this);
	}

	/**
	 * Recursively renders the graph and its children
	 */
	public void render(GL gl) {
		for(RenderableGraph g : children) {
			g.render(gl);
		}
	}
	
	
	
	public RenderableGraph replaceChild(RenderableGraph graph, int index) {
		graph.setParent(this);
		return children.set(index, graph);
	}
	
	public RenderableGraph replace(RenderableGraph graph) {
		
		getParent().addChild(graph);
		getParent().removeChild(this);
		for(int i = 0; i < size(); ++i) {
			graph.addChild(getChild(i));
		}
		
		return this;
	}
	
	public Object clone() {
		RenderableGraph graph = null;
		try {
			graph = (RenderableGraph) super.clone();
		} catch (CloneNotSupportedException e) {
			return null;
		}
		
		for(int i = 0; i < size(); ++i) {
			graph.replaceChild((RenderableGraph) getChild(i).clone(), i);
		}
		
		return graph;
	}
	
	public void update() {
		
	}
	
	

}
