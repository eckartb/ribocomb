package viewer.graph;

import javax.media.opengl.GL;

/**
 * A RendererSwitchGraph is like a RendererBinarySwitchGraph except
 * it allows selective rendering of its children as oppossed to the idea of
 * rendering all the children or none of them as is the case with the
 * RendererBinarySwitchGraph. This class won't allow more children to
 * be inserted than is permitted by the logic interface.
 * @author Calvin
 *
 */
public class RendererSwitchGraph extends RenderableGraphImp {

	/**
	 * Interface the RendererSwitchGraph uses to determine
	 * which children to render.
	 * @author Calvin
	 *
	 */
	public static interface Logic {
		/**
		 * Returns a bit set of children to render
		 * @return
		 */
		public long doLogic();
		
		/**
		 * Returns the max number of fields in the bitset
		 * returned by <code>doLogic()</code>
		 * @return
		 */
		public int maxFields();
	}
	
	private Logic logic;
	
	/**
	 * Construct a RendererSwitchGraph using the supplied logic
	 * @param logic Logic
	 */
	public RendererSwitchGraph(Logic logic) {
		this.logic = logic;
	
	}
	
	/**
	 * Get the logic
	 * @return Returnst the logic
	 */
	public Logic getLogic() {
		return logic;
	}
	
	public void render(GL gl) {
		long bitset = logic.doLogic();
		for(int i = 0; i < size() && i < logic.maxFields(); ++i) {
			if((bitset & (1 << i)) == (1 << i)) {
				getChild(i).render(gl);
			}
		}
	}

	@Override
	public void addChild(RenderableGraph graph) {
		if(size() + 1 < logic.maxFields())
			super.addChild(graph);
	}

	@Override
	public void insertChild(RenderableGraph graph, int index) {
		if(size() + 1 < logic.maxFields())
			super.insertChild(graph, index);
	}
	
	
	
}
