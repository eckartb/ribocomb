package viewer.graph;

import tools3d.Matrix4D;
import javax.media.opengl.GL;

/**
 * The TransformGraph allows renderable graphs
 * to be translated before being rendered.
 * @author Calvin
 *
 */
public class TransformGraph extends RenderableGraphImp {
	
	private Matrix4D transform;
	
	/**
	 * Construct a new node using the transformatin matrix
	 * @param transform Matrix
	 */
	public TransformGraph(Matrix4D transform) {
		this.transform = transform;
	}
	
	/**
	 * Get the transformation matrix
	 * @return Returns the matrix
	 */
	public Matrix4D getTransform() {
		return transform;
	}
	
	public void render(GL gl) {
		gl.glPushMatrix();
		gl.glMultMatrixd(transform.getArray(), 0);
		super.render(gl);
		gl.glPopMatrix();
	}
	
	public String getClassName() {
		return "TransformGraph";
	}
	
	public Object clone() {
		TransformGraph graph = (TransformGraph) super.clone();
		graph.transform = new Matrix4D();
		graph.transform.copy(transform);
		return graph;
	}
}
