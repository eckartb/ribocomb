package viewer.graph.rna;

import tools3d.objects3d.Object3D;
import viewer.graph.CacheableObject3DRenderableGraph;
import viewer.graphics.AdvancedMesh;
import viewer.graphics.DefaultAdvancedMesh;
import rnadesign.rnamodel.Atom3D;

/**
 * This is the abstract class for anything that
 * can render an atom. It serves mainly as a marker
 * but also allows access to the underlying Atom.
 * @author Calvin
 *
 */
public abstract class AtomRendererGraph extends CacheableObject3DRenderableGraph {
	public AtomRendererGraph(Atom3D object) {
		super(object);
	}
	
	/**
	 * Get the atom.
	 * @return Returns the atom this graph renders
	 */
	public Atom3D getAtom() {
		return (Atom3D) getObject();
	}
	
	


}
