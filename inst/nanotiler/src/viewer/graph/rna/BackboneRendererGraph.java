package viewer.graph.rna;

import viewer.graph.CacheableObject3DRenderableGraph;
import viewer.graphics.Mesh;
import viewer.graphics.MeshRenderer;
import viewer.graphics.WireframeMeshRenderer;
import viewer.graphics.SolidMeshRenderer;
import viewer.graphics.MeshCacheable;
import rnadesign.rnamodel.RnaStrand;
import javax.media.opengl.GL;

/**
 * Abstract class that renders a backbone. This class
 * serves mainly as a marker but it also allows access
 * to the underlying backbone model object, the RnaStrand
 * @author Calvin
 *
 */
public abstract class BackboneRendererGraph extends CacheableObject3DRenderableGraph  {

	
	public BackboneRendererGraph(RnaStrand strand) {
		super(strand);
	}
	
	public BackboneRendererGraph(RnaStrand strand, boolean cache) {
		super(strand);
		setMeshCached(cache);
	}
	
	/**
	 * Get the strand
	 * @return Returns the strand
	 */
	public RnaStrand getStrand() {
		return (RnaStrand) getObject();
	}
	
	
}
