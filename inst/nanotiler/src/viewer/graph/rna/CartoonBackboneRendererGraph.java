package viewer.graph.rna;

import rnadesign.rnamodel.*;
import viewer.graphics.*;

import java.util.ArrayList;
import tools3d.*;
import tools3d.splines.StandaloneSplineFactory;

/**
 * Renders the backbone of an rna molecule as a cartoon. A cartoon
 * is defined as a spline interpolated from the specific atoms
 * that compose the backbone. Additionall, sticks extending from
 * the backbone are rendered. These sticks represent the attachment
 * of the nucleotide to the backbone. They are derived from an
 * atom within the nucleotide. If the backbone or nucleotide is improperly
 * formed, that is, the atom nomenclature doesn't adhere to the PDB format,
 * certain elements of the model will not be generated.
 * @author Calvin
 *
 */
public class CartoonBackboneRendererGraph extends BackboneRendererGraph {
	
	public CartoonBackboneRendererGraph(RnaStrand strand, boolean cache) {
		super(strand, cache);
	}

	public CartoonBackboneRendererGraph(RnaStrand strand) {
		super(strand);
	}

	@Override
	public AdvancedMesh generateMesh() {
		RnaStrand strand = getStrand();
		ArrayList<Vector3D> backbone = new ArrayList<Vector3D>(strand.getResidueCount());
		ArrayList<Vector3D> bases = new ArrayList<Vector3D>(strand.getResidueCount());
		for(int i = 0; i < strand.getResidueCount(); ++i) {
			if(strand.getResidue3D(i) instanceof Nucleotide3D &&
					strand.getResidue3D(i).getIndexOfChild("O5*") >= 0) {

				Nucleotide3D n = (Nucleotide3D) strand.getResidue3D(i);
				backbone.add(n.getChild("O5*").getPosition());
				if((n.getSymbol().getCharacter() == 'G' || n.getSymbol().getCharacter() == 'A') &&
						n.getIndexOfChild("N9") >= 0)
					bases.add(n.getChild("N9").getPosition());
				else if(n.getIndexOfChild("N1") >= 0)
					bases.add(n.getChild("N1").getPosition());
			}
			else
				backbone.add(strand.getResidue3D(i).getPosition());
		}
		
		Vector3D[] points = new Vector3D[backbone.size()];
		backbone.toArray(points);
		
		Vector3D[] b = new Vector3D[bases.size()];
		bases.toArray(b);
		
		StandaloneSplineFactory factory = new StandaloneSplineFactory();
		Vector3D[] curve = factory.createCubic(points, getRefinement());
		
		DefaultAdvancedMesh mesh = new DefaultAdvancedMesh();
		
		if(getRefinementLevel() == LODCapable.LOD_COARSE) {
			Material material = new Material(getColorModel().getColor(strand), true);
			material.getEmissive().setAlpha(1.0);
			mesh.add(new Vector4D(curve[0]), new Vector4D(0.0, 0.0, -1.0, 0.0), material);
			for(int i = 0; i < curve.length - 1; ++i) {
				mesh.add(new Vector4D(curve[i + 1]), new Vector4D(0.0, 0.0, -1.0, 0.0), material);
				int[] line = {i, i + 1};
				mesh.add(line);
			}
			if(b.length == points.length) {
				for(int i = 0; i < b.length; ++i) {
					mesh.add(new Vector4D(b[i]), new Vector4D(0.0, 0.0, -1.0, 0.0), material);
					int[] line = {i * getRefinement(), i + curve.length };
					mesh.add(line);
				}
			}
		}
		
		else {
			Vector4D[] polygon = {
					new Vector4D(0.433, 0.25, 0.0, 0.0),
					new Vector4D(0.0, 0.5, 0.0, 0.0),
					new Vector4D(-0.433, 0.25, 0.0, 0.0),
					new Vector4D(-0.433, -0.25, 0.0, 0.0),
					new Vector4D(0.0, -0.5, 0.0, 0.0),
					new Vector4D(0.433, -0.25, 0.0, 0.0),
			};
			
			Vector4D normal = new Vector4D(0.0, 0.0, -1.0, 0.0);
			Material material = getColorModel().getMaterial(strand);
			if (curve.length >= 6) {
			    mesh.add(MeshOperations.extend(polygon, normal, curve), material);
			}
			
			if(b.length == points.length) {
				for(int i = 0; i < b.length; ++i) {
					Vector4D axis = new Vector4D(b[i].minus(points[i]));
					axis.setW(0.0);
					double length = axis.length();
					axis.normalize();
					
					Vector4D midpoint = new Vector4D(b[i].plus(points[i]).mul(0.5));
					midpoint.setW(1.0);
					
					Mesh cylinder = PrimitiveFactory.generateCylinder(axis, 0.25, length, 1, 6);
					MeshOperations.translateMesh(cylinder, midpoint);
					mesh.add(cylinder, material);
				}
			}
			
			
		}
		
		if(getStrand().isSelected() && getRefinementLevel() != LODCapable.LOD_COARSE) {
			Material material = new Material();
			material.setEmissive(1.0, 1.0, 0.0, 0.0);
			DefaultAdvancedMesh combo = new DefaultAdvancedMesh();
			combo.add(mesh);
			combo.add(AdvancedMeshTools.extractCage(mesh, material));
			return combo;
		}
		
		return mesh;
	}

	private int getRefinement() {
		if(getRefinementLevel() == LODCapable.LOD_COARSE || getRefinementLevel() == LODCapable.LOD_MEDIUM)
			return 3;
		
		else
			return 5;
	}
}
