package viewer.graph.rna;


import viewer.graph.CacheableObject3DRenderableGraph;

import rnadesign.rnamodel.Nucleotide3D;

/**
 * An abstract class that serves as a marker
 * for classes that can render nucleotides. Also
 * provide access to the underlying Nucleotide model.
 * @author Calvin
 *
 */
public abstract class NucleotideRendererGraph extends CacheableObject3DRenderableGraph  {
	
	public NucleotideRendererGraph(Nucleotide3D n) {
		super(n);
	}
	
	/**
	 * Get the nucloetide.
	 * @return Returns the nucleotide.
	 */
	public Nucleotide3D getNucleotide() {
		return (Nucleotide3D) getObject();
	}
	
	

}
