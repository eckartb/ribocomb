package viewer.graph.rna;

import viewer.graph.*;
import viewer.rnadesign.AvailableRenderers;
import rnadesign.rnamodel.*;
import rnadesign.rnacontrol.*;
import tools3d.objects3d.*;
import tools3d.*;

/**
 * Generates renderable scene graphs from Object3D scene graphs.
 * @author Calvin
 *
 */
public class RenderableGraphFactory {

	/**
	 * Generates a renderable scenegraph from the RNA associated portions
	 * of the Object3D scene graph. Uses Links, Atoms, Nucleotides, and Strands
	 * to generate the renderable scene graph. Different rendering
	 * modes are available via the enumeration constants.
	 * @param controller The Object3D scene graph. This method traverses this scene
	 * graph and generates a renderable scene graph to compliment it.
	 * @param renderer The enum constant to tell the method which
	 * type of renderable scene graph to generate
	 * @return Returns the root node of the renderable scene graph.
	 */
	public static RnaRendererGraphController generate(Object3DGraphController controller, AvailableRenderers renderer) {
		RenderableGraphImp graph = new RenderableGraphImp();
		RnaRendererGraphController rnaController = new RnaRendererGraphController();
		
		RendererBinarySwitchGraph strands = new RendererBinarySwitchGraph(rnaController.new StrandPredicate());
		RendererBinarySwitchGraph nucleotides = new RendererBinarySwitchGraph(rnaController.new NucleotidePredicate());
		RendererBinarySwitchGraph atoms = new RendererBinarySwitchGraph(rnaController.new AtomPredicate());
		// add here 
		RendererBinarySwitchGraph links = new RendererBinarySwitchGraph(rnaController.new LinkPredicate());
		RendererBinarySwitchGraph branchDescriptors = new RendererBinarySwitchGraph(rnaController.new BranchDescriptorPredicate());
		graph.addChild(strands);
		graph.addChild(nucleotides);
		graph.addChild(atoms);
		System.out.println("Adding link renderer!");
		graph.addChild(links);
		System.out.println("Adding branch descriptor renderer!");
		graph.addChild(branchDescriptors);

		generate(controller.getGraph().getGraph(), renderer, strands, nucleotides, atoms);
		generate(controller.getLinks(), renderer, links);

		System.out.println("Strand renderer children: " + strands.size());
		System.out.println("Nucleotide renderer children: " + nucleotides.size());
		System.out.println("Atom renderer children: " + atoms.size());
		System.out.println("Branch descriptor renderer children: " + branchDescriptors.size());
		System.out.println("Link renderer children: " + links.size() + " generated from " + controller.getLinks().size() + " links.");

		rnaController.setRoot(graph);
		rnaController.setRenderer(renderer);
		rnaController.setMeshCached(true);
		
		//configureController(rnaController, renderer);
		
		return rnaController;
	}
	
    private static void generate(LinkSet linkSet, AvailableRenderers renderer, RenderableGraph links) {
	for (int i = 0; i < linkSet.size();++i) {
	    Link linkInstance = linkSet.get(i);
	    links.addChild(new LinkRenderableGraph(linkInstance));
	    
	}
    }

	private static void generate(Object3D object, AvailableRenderers renderer, RenderableGraph strands, 
			RenderableGraph nucleotides, RenderableGraph atoms) {
	    //if (!(stickMode || wireFrameMode)) {
		if(object instanceof RnaStrand) {
			RenderableGraph graph = generate((RnaStrand)object, renderer);
			if(graph != null)
				strands.addChild(graph);
		}
		
		else if(object instanceof Nucleotide3D) {
			RenderableGraph graph = generate((Nucleotide3D)object, renderer);
			
			if(graph != null)
				nucleotides.addChild(graph);
		}
		
		else if(object instanceof Atom3D) {
			RenderableGraph graph = generate((Atom3D)object, renderer);
			if(graph != null)
				atoms.addChild(graph);
		}
		//}
	    if(object instanceof BranchDescriptor3D) {
		RenderableGraph graph = generate((BranchDescriptor3D)object, renderer);
		if(graph != null)
		    atoms.addChild(graph);
	    }
	    
	    for(int i = 0; i < object.size(); ++i) {
		generate(object.getChild(i), renderer, strands, nucleotides, atoms);
	    }
	}
	
	/**
	 * Generate a renderable graph node for a nucleotide
	 * @param n Nucleotide to generate renderable node for
	 * @param renderer Enum constant specifying the type
	 * of rendering to be done
	 * @return Returns a renderable graph node.
	 */
	public static RenderableGraph generate(Nucleotide3D n, AvailableRenderers renderer) {
		switch(renderer) {
		case Cartoon:
		    // return new StickNucleotideRendererGraph(n, false, false, true);
		    return new StickNucleotideRendererGraph(n, true, true, true);
		case Stick:
		case BallAndStick:
			return new StickNucleotideRendererGraph(n);
		default:
			return null;
		}
		
	}
	
	
	/**
	 * Generate a renderable graph node for a strand
	 * @param s Strand to generate renderable node for
	 * @param renderer Enum constant specifying the type of
	 * rendering to be done.
	 * @return Returns a renderable graph node.
	 */
	public static RenderableGraph generate(RnaStrand s, AvailableRenderers renderer) {
		switch(renderer) {
		case Cartoon:
			return new CartoonBackboneRendererGraph(s);
		case Ribbon:
			return new RibbonBackboneRenderer(s);
		default:
			return null;
		}
		
	}

	/**
	 * Generate a renderable graph node for a strand
	 * @param s Strand to generate renderable node for
	 * @param renderer Enum constant specifying the type of
	 * rendering to be done.
	 * @return Returns a renderable graph node.
	 */
	public static RenderableGraph generate(BranchDescriptor3D bd, AvailableRenderers renderer) {
		switch(renderer) {
		case Cartoon:
		    return new ArrowBranchDescriptorRendererGraph(bd);
		case Ribbon:
		    return new ArrowBranchDescriptorRendererGraph(bd);
		default:
		    return null;
		}
		
	}
	
	/**
	 * Generate a renderable graph node for an atom
	 * @param a Atom to generate renderable node for
	 * @param renderer Enum constant specifying the type of
	 * rendering to be done.
	 * @return Returns a renderable graph node.
	 */
	public static RenderableGraph generate(Atom3D a, AvailableRenderers renderer) {
		switch(renderer) {
		case Ball:
		case BallAndStick:
			RenderableGraph g = new TranslatedSphericalAtomRenderer(a);
			return g;
		default:
			return null;
		}
	}
	
	private static TransformGraph computeTranslationNode(Vector4D from, Vector4D to) {
		Vector4D diff = to.minus(from);
		Matrix4D transform = new Matrix4D(1.0, 0.0, 0.0, 0.0,
						  0.0, 1.0, 0.0, 0.0,
						  0.0, 0.0, 1.0, 0.0,
						  diff.getX(), diff.getY(), diff.getZ(), 1.0);
		return new TransformGraph(transform);
	}
	
	private static void configureController(RnaRendererGraphController controller, AvailableRenderers renderer) {
		switch(renderer) {
		case Cartoon:
		    // controller.setRenderAtoms(false);
			controller.setRenderAtoms(true);
			break;
		case Ribbon:
			controller.setRenderAtoms(false);
			controller.setRenderNucleotides(false);
			break;
		case Stick:
			controller.setRenderAtoms(false);
			break;
		case BallAndStick:
			break;
		case Ball:
			controller.setRenderNucleotides(false);
			controller.setRenderStrands(false);
			break;
		default:
		}
	}
	
}
