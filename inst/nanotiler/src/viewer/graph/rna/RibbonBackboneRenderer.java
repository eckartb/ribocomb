package viewer.graph.rna;

import rnadesign.rnamodel.*;
import viewer.graphics.*;
import viewer.util.Tools;
import tools3d.*;
import tools3d.splines.StandaloneSplineFactory;

import java.util.*;

/**
 * Renders the backbone of the rna molecule as a ribbon. The normal of
 * the ribbon points in the orientation of the nucleotide extending
 * from the backbone.
 * @author Calvin
 *
 */
public class RibbonBackboneRenderer extends BackboneRendererGraph {

	
	
	public RibbonBackboneRenderer(RnaStrand strand, boolean cache) {
		super(strand, cache);
	}

	public RibbonBackboneRenderer(RnaStrand strand) {
		super(strand);
	}

	@Override
	public AdvancedMesh generateMesh() {
		RnaStrand strand = getStrand();
		ArrayList<Vector3D> backbone = new ArrayList<Vector3D>(strand.getResidueCount());
		ArrayList<Vector3D> orientations = new ArrayList<Vector3D>(strand.getResidueCount());
		for(int i = 0; i < strand.getResidueCount(); ++i) {
			Residue3D residue = strand.getResidue3D(i);
			if(residue.getIndexOfChild("O5*") >= 0) {
				backbone.add(residue.getChild("O5*").getPosition());
				
				if((residue.getSymbol().getCharacter() == 'G' || residue.getSymbol().getCharacter() == 'A') &&
						residue.getIndexOfChild("N9") >= 0) {
					Vector3D v = residue.getChild("N9").getPosition().minus(residue.getChild("O5*").getPosition());
					v.normalize();
					orientations.add(v);
				}
				else if(residue.getIndexOfChild("N1") >= 0) {
					Vector3D v = residue.getChild("N1").getPosition().minus(residue.getChild("O5*").getPosition());
					v.normalize();
					orientations.add(v);
				}
			}
			
			else {
				backbone.add(residue.getPosition());
			}
		}
		
		Vector3D[] points = new Vector3D[backbone.size()];
		backbone.toArray(points);
		
		Vector3D[] o = new Vector3D[orientations.size()];
		orientations.toArray(o);
		
		StandaloneSplineFactory factory = new StandaloneSplineFactory();
		Vector3D[] curve = factory.createCubic(points, getRefinement());
		
		Vector4D[] polygon = {
			new Vector4D(1.0, 0.1, 0.0, 0.0),
			new Vector4D(0.0, 0.1, 0.0, 0.0),
			new Vector4D(-1.0, 0.1, 0.0, 0.0),
			new Vector4D(-1.0, -0.1, 0.0, 0.0),
			new Vector4D(0.0, -0.1, 0.0, 0.0),
			new Vector4D(1.0, -0.1, 0.0, 0.0)
		};
		
		Vector4D polygonNormal = new Vector4D(0.0, 0.0, -1.0, 0.0);
		Mesh mesh = null;
		if(o.length > 0) {
			Vector3D[] io = new Vector3D[curve.length];
			io[0] = o[0];
			for(int i = 0; i < o.length - 1; ++i) {
				io[(i + 1) * getRefinement()] = o[i + 1];
				Vector4D i1 = new Vector4D(o[i]);
				i1.setW(0.0);
				Vector4D i2 = new Vector4D(o[i + 1]);
				i2.setW(0.0);
				Vector4D axis = new Vector4D(i1.cross(i2));
				axis.setW(0.0);
				axis.normalize();
				double angle = Math.acos(i1.dot(i2));
				angle /= getRefinement() - 1;
				for(int j = 0; j < getRefinement() - 1; ++j) {
					
					Vector4D ri = Tools.rotate(i1, axis, angle);
					io[i * getRefinement() + j + 1] = new Vector3D(ri);
					i1 = ri;
					i1.normalize();
				}
			}
			
			Vector4D polygonOrientation = new Vector4D(0.0, 1.0, 0.0, 0.0);
			mesh = MeshOperations.extend(polygon, polygonNormal, polygonOrientation, curve, io);
			
			
		}
		
		else {
			mesh = MeshOperations.extend(polygon, polygonNormal, curve);
		}
		
		DefaultAdvancedMesh m = new DefaultAdvancedMesh();
		m.add(mesh, getColorModel().getMaterial(strand));
		
		if(getStrand().isSelected() && getRefinementLevel() != LODCapable.LOD_COARSE) {
			Material material = new Material();
			material.setEmissive(1.0, 1.0, 0.0, 0.0);
			DefaultAdvancedMesh combo = new DefaultAdvancedMesh();
			combo.add(m);
			combo.add(AdvancedMeshTools.extractCage(m, material));
			return combo;
		}
		
		return m;
	}
	
	private int getRefinement() {
		switch(getRefinementLevel()) {
		case LODCapable.LOD_FINE:
			return 8;
		case LODCapable.LOD_MEDIUM:
			return 6;
		}
		
		return 4;
	}

}
