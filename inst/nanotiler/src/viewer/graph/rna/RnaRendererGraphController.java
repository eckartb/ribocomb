package viewer.graph.rna;

import viewer.graph.*;
import viewer.graphics.CloneableRenderableModel;
import viewer.rnadesign.AvailableRenderers;
import tools3d.objects3d.Object3D;

/**
 * Controls the rna specific functions of a renderable scene graph.
 * @author Calvin
 *
 */
public class RnaRendererGraphController extends RenderableGraphController implements CloneableRenderableModel {
	
	private boolean renderLinks = true;
	private boolean renderNucleotides = true;
	private boolean renderAtoms = true;
	private boolean renderStrands = true;
	private boolean renderBranchDescriptors = true;
	
	private AvailableRenderers renderer;

	public RnaRendererGraphController() {
		
	}

	/**
	 * Does the graph render atoms
	 * @return Returns true if atoms are
	 * rendered and false if otherwise
	 */
	public boolean isRenderAtoms() {
		return renderAtoms;
	}

	/**
	 * Does the graph render branch descriptors
	 * @return Returns true if branch descriptors are
	 * rendered and false if otherwise
	 */
    public boolean isRenderBranchDescriptors() { 
	return renderBranchDescriptors;
    }

    /**
     * Sets if graph renders branch descriptors
     */
    public void setRenderBranchDescriptors(boolean b) { 
	this.renderBranchDescriptors = b;
    }
    
	/**
	 * Set atom rendering
	 * @param renderAtoms True if atoms should
	 * be rendered and false if they should
	 * not be rendered
	 */
	public void setRenderAtoms(boolean renderAtoms) {
		this.renderAtoms = renderAtoms;
	}

	/**
	 * Are links rendered?
	 * @return Returns true if links are rendered
	 * and false if they are not rendered
	 */
	public boolean isRenderLinks() {
		return renderLinks;
	}

	/**
	 * Set link rendering
	 * @param renderLinks True if links should
	 * be rendered or false if they should not
	 * be rendered
	 */
	public void setRenderLinks(boolean renderLinks) {
		this.renderLinks = renderLinks;
	}

	/**
	 * Are nucleotides rendered?
	 * @return Returns true if nucleotides are rendered
	 * or false if they are not rendered
	 */
	public boolean isRenderNucleotides() {
		return renderNucleotides;
	}

	/**
	 * Set nucleotide rendering
	 * @param renderNucleotides True if nucleotides
	 * should be rendered or false if they should
	 * not be rendered.
	 */
	public void setRenderNucleotides(boolean renderNucleotides) {
		this.renderNucleotides = renderNucleotides;
	}

	public boolean isRenderStrands() {
		return renderStrands;
	}

	public void setRenderStrands(boolean renderStrands) {
		this.renderStrands = renderStrands;
	}
	
	/**
	 * Get the graph node that corresponds to this 3D object
	 * @param object Object the graph node corresponds to
	 * @return Returns the graph node encapsulating this object
	 */
	public Object3DRenderableGraph getGraph(Object3D object) {
		return getGraph(object, getRoot());
	}
	
	private Object3DRenderableGraph getGraph(Object3D object, RenderableGraph graph) {
		if(graph instanceof Object3DRenderableGraph &&
				((Object3DRenderableGraph)graph).getObject().equals(object)) {
			return (Object3DRenderableGraph) graph;
		}
		
		for(int i = 0; i < graph.size(); ++i) {
			Object3DRenderableGraph g = getGraph(object, graph.getChild(i));
			if(g != null)
				return g;
		}
		
		return null;
	}
	
	/**
	 * Predicate to turn on and off strand rendering in the scene graph
	 * @author Calvin
	 *
	 */
	class StrandPredicate implements RendererBinarySwitchGraph.Predicate {

		public boolean isTrue() {
			return isRenderStrands();
		}

	}
	
	/**
	 * Predicate to turn on and off nucleotide rendering in the scene graph
	 * @author Calvin
	 *
	 */
	class NucleotidePredicate implements RendererBinarySwitchGraph.Predicate {

		public boolean isTrue() {
			return isRenderNucleotides();
		}

	}
	
	/**
	 * Predicate to turn on and off atom rendering in the scene graph
	 * @author Calvin
	 *
	 */
	class AtomPredicate implements RendererBinarySwitchGraph.Predicate {

		public boolean isTrue() {
			return isRenderAtoms();
		}

	}

	/**
	 * Predicate to turn on and off atom rendering in the scene graph
	 * @author Calvin
	 *
	 */
	class BranchDescriptorPredicate implements RendererBinarySwitchGraph.Predicate {

		public boolean isTrue() {
		    return isRenderBranchDescriptors();
		}

	}
	
	/**
	 * Predicate to turn on and off link rendering in scene graph
	 * @author Calvin
	 *
	 */
	class LinkPredicate implements RendererBinarySwitchGraph.Predicate {

		public boolean isTrue() {
			return isRenderLinks();
		}

	}
	
	public Object clone() {
		try {
			RnaRendererGraphController controller = (RnaRendererGraphController) super.clone();
			controller.setRoot((RenderableGraph) getRoot().clone());
			controller.loadState(this); // added by EB
			return controller;
		} catch (CloneNotSupportedException e) {
			return null;
		}
	}

	/**
	 * Get the rendering mode for this graph
	 * @return
	 */
	public AvailableRenderers getRenderer() {
		return renderer;
	}

	/**
	 * Set the rendering mode for this graph. Note that
	 * this doesn't actually affect the scene graph.
	 * The scene graph will need to be recreated
	 * in order for this to take affect.
	 * @param renderer
	 */
	public void setRenderer(AvailableRenderers renderer) {
		this.renderer = renderer;
	}
	
	/**
	 * Copiese the state of one controller to this one.
	 * @param rc Controller to copy state from
	 */
	public void loadState(RnaRendererGraphController rc) {
		setWireframeMode(rc.isWireframeMode());
		setRefinementLevel(rc.getRefinementLevel());
		setMeshCached(rc.isMeshCached());
		setColorModel(rc.getColorModel());
		setRenderAtoms(rc.isRenderAtoms());
		setRenderNucleotides(rc.isRenderNucleotides());
		setRenderStrands(rc.isRenderStrands());
		setRenderLinks(rc.isRenderLinks());
		setRenderBranchDescriptors(rc.isRenderBranchDescriptors());
	}
	
	/**
	 * Update all nodes in the renderable scene graph.
	 *
	 */
	public void update() {
		update(getRoot());
	}
	
	private void update(RenderableGraph graph) {
		graph.update();
		
		for(int i = 0; i < graph.size(); ++i) {
			update(graph.getChild(i));
		}
	}
	
	
	
	
	
	
	
	
	
}
