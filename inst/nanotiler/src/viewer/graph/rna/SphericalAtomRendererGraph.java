package viewer.graph.rna;

import viewer.graphics.AdvancedMesh;
import viewer.graphics.DefaultAdvancedMesh;
import viewer.graphics.PrimitiveFactory;
import viewer.graphics.LODCapable;
import rnadesign.rnamodel.Atom3D;
import tools3d.Vector4D;
import viewer.graphics.ColorModel;
import viewer.graphics.Material;
import viewer.graphics.AdvancedMeshTools;

/**
 * Renders an atom as a sphere centered at the origin
 * @author Calvin
 *
 */
public class SphericalAtomRendererGraph extends AtomRendererGraph {
	
	public SphericalAtomRendererGraph(Atom3D atom) {
		super(atom);
		
		COARSE_MESH = new DefaultAdvancedMesh();
		COARSE_MESH.add(new Vector4D(0.0, 0.0, 0.0, 1.0), new Vector4D(0.0, 0.0, -1.0, 0.0), new Material(getColorModel().getColor(getAtom())));
		int[] point = { 0 };
		COARSE_MESH.add(point);
		COARSE_MESH.setPointSize(5.0);
		
		MEDIUM_MESH = new DefaultAdvancedMesh();
		MEDIUM_MESH.add(PrimitiveFactory.generateSphere(0.5, 4, 4), getColorModel().getMaterial(getAtom()));
		
		FINE_MESH = new DefaultAdvancedMesh();
		FINE_MESH.add(PrimitiveFactory.generateSphere(0.5, 6, 6), getColorModel().getMaterial(getAtom()));
	}
	
	private final DefaultAdvancedMesh COARSE_MESH;
	private final DefaultAdvancedMesh MEDIUM_MESH;
	private final DefaultAdvancedMesh FINE_MESH;

	public void setColorModel(ColorModel model) {
		Material material = new Material(model.getColor(getAtom()), true);
		COARSE_MESH.setVertexMaterial(0, material);
		material = model.getMaterial(getAtom());
		for(int i = 0; i < MEDIUM_MESH.getVertexCount(); ++i)
			MEDIUM_MESH.setVertexMaterial(i, material);
		for(int i = 0; i < FINE_MESH.getVertexCount(); ++i)
			FINE_MESH.setVertexMaterial(i, material);
		
		super.setColorModel(model);
		
	}

	@Override
	public AdvancedMesh generateMesh() {
		AdvancedMesh mesh = null;
		switch(this.getRefinementLevel()) {
		case LODCapable.LOD_COARSE:
			mesh = (AdvancedMesh) COARSE_MESH.clone();
			break;
		case LODCapable.LOD_MEDIUM:
			mesh = (AdvancedMesh) MEDIUM_MESH.clone();
			break;
		case LODCapable.LOD_FINE:
			mesh = (AdvancedMesh) FINE_MESH.clone();
			break;
		default:
		    mesh = (AdvancedMesh) MEDIUM_MESH.clone();
		}
		
		if(getAtom().isSelected()) {
			Material material = new Material();
			material.setEmissive(1.0, 1.0, 0.0, 0.0);
			DefaultAdvancedMesh combo = new DefaultAdvancedMesh();
			combo.add(mesh);
			combo.add(AdvancedMeshTools.extractCage(mesh, material));
			return combo;
		}
		
		return mesh;
		
	
	}
	
	public String getClassName() {
		return "SphericalAtomRendererGraph";
	}

}
