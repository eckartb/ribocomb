package viewer.graph.rna;

import rnadesign.rnamodel.*;
import viewer.graphics.*;
import tools3d.objects3d.*;

import java.util.*;
import tools3d.*;

/**
 * Renders nucleotides as sticks by rendering the covalent bonds
 * between each atom composing the nucleotide.
 * @author Calvin
 *
 */
public class StickNucleotideRendererGraph extends
		NucleotideRendererGraph {
    public boolean atomLoopMode = false; // deprecated, use instead link renderer
	private boolean renderSugar = true;
	private boolean renderPhosphate = true;
	private boolean renderBase = true;

	public StickNucleotideRendererGraph(Nucleotide3D n) {
		super(n);
	}
	
	public StickNucleotideRendererGraph(Nucleotide3D n, boolean renderSugar,
			boolean renderPhosphate, boolean renderBase) {
		this(n);
		this.renderSugar = renderSugar;
		this.renderPhosphate = renderPhosphate;
		this.renderBase = renderBase;
	}
	
	public StickNucleotideRendererGraph(Nucleotide3D n, boolean renderSugar,
			boolean renderPhosphate, boolean renderBase, boolean cache) {
		this(n, renderSugar, renderPhosphate, renderBase);
		setMeshCached(cache);
	}
	
	public StickNucleotideRendererGraph(Nucleotide3D n, boolean cache) {
		this(n);
		setMeshCached(cache);
	}

	@Override
	public AdvancedMesh generateMesh() {

		List<Atom3D> atoms = atomCollector();
		DefaultAdvancedMesh mesh = new DefaultAdvancedMesh();
		if (atomLoopMode) {
		    for(int i = 0; i < atoms.size(); ++i) {
			for(int j = i + 1; j < atoms.size(); ++j) {
				Atom3D a1 = atoms.get(i);
				Atom3D a2 = atoms.get(j);
				    //if(NucleotideTools.isCovalentlyBonded(getNucleotide(), a1.getName(), a2.getName())) {
				    if(NucleotideTools.isCovalentlyBonded(getNucleotide(),a1,a2)){


		    //System.out.println("Rendering link between:" + a1.getName() + " " +a2.getName());
		    Vector4D p1 = new Vector4D(a1.getPosition());
		    Vector4D p2 = new Vector4D(a2.getPosition());
						
						
		    if(getRefinementLevel() == LODCapable.LOD_COARSE) {
			Material material = new Material(getColorModel().getColor(getNucleotide()), true);
			DefaultAdvancedMesh am = new DefaultAdvancedMesh();
			am.setLineWidth(3.0);
			am.add(p1, new Vector4D(0.0, 0.0, -1.0, 0.0), material);
			am.add(p2, new Vector4D(0.0, 0.0, -1.0, 0.0), material);

			int[] line = {0,1};
			am.add(line);
			mesh.add(am);
		    } else {
			Vector4D axis = p2.minus(p1);
			double length = axis.length();
			axis.setW(0.0);
			axis.normalize();
			Mesh cylinder = PrimitiveFactory.generateCylinder(axis, 0.25, length, 1, getStickSubdivisions());
			Vector4D midpoint = new Vector4D((p1.getX() + p2.getX()) / 2.0, (p1.getY() + p2.getY()) / 2.0,
							 (p1.getZ() + p2.getZ()) / 2.0, 1.0);
			cylinder = MeshOperations.translateMesh(cylinder, midpoint);
			mesh.add(cylinder, getColorModel().getMaterial(getNucleotide()));
			
		    }
		}
	       	}
		}
		}
		if(getNucleotide().isSelected() && getRefinementLevel() != LODCapable.LOD_COARSE) {
			Material material = new Material();
			material.setEmissive(1.0, 1.0, 0.0, 0.0);
			DefaultAdvancedMesh combo = new DefaultAdvancedMesh();
			combo.add(mesh);
			combo.add(AdvancedMeshTools.extractCage(mesh, material));
			return combo;
		}
		
		return mesh;
		
	}

        public void setAtomLoopMode(boolean atomLoopMode){
	    this.atomLoopMode = atomLoopMode;
	}

	public boolean isRenderBase() {
		return renderBase;
	}

	public void setRenderBase(boolean renderBase) {
		this.renderBase = renderBase;
		if(isMeshCached())
			updateMesh();
	}

	public boolean isRenderPhosphate() {
		return renderPhosphate;
	}

	public void setRenderPhosphate(boolean renderPhosphate) {
		this.renderPhosphate = renderPhosphate;
		if(isMeshCached())
			updateMesh();
	}

	public boolean isRenderSugar() {
		return renderSugar;
	}

	public void setRenderSugar(boolean renderSugar) {
		this.renderSugar = renderSugar;
		if(isMeshCached())
			updateMesh();

	}
	
	private List<Atom3D> atomCollector() {
		List<Atom3D> atoms = new ArrayList<Atom3D>(getNucleotide().getAtomCount());
		for(int i = 0; i < getNucleotide().getAtomCount(); ++i) {
			Atom3D atom = getNucleotide().getAtom(i);	
			//System.out.println("running atomCollector with: "+atom.getName());
			if(renderSugar && NucleotideTools.isSugarAtom(atom)){
			    //System.out.println("Sugar Atom "+atom.getName()+" added.");
			    atoms.add(atom);
			}
			
			else if(renderPhosphate && NucleotideTools.isPhosphate(atom)){
			    //System.out.println("Phosphate Atom "+atom.getName()+" added.");
			    atoms.add(atom);
			}
			
			else if(renderBase && NucleotideTools.isBaseAtom(atom)){
			    //System.out.println("Base Atom "+atom.getName()+" added.");
			    atoms.add(atom);
			}
			else if(NucleotideTools.isBackboneAtom(atom)){
			    //System.out.println("Backbone Atom "+atom.getName()+" added.");
			    atoms.add(atom);
			}
		}
		
		return atoms;
	}

	
	protected int getStickSubdivisions() {
		if(this.getRefinementLevel() == LODCapable.LOD_MEDIUM)
			return 4;
		if(this.getRefinementLevel() == LODCapable.LOD_FINE)
			return 6;
		
		return 4;
	}
	
	


}
