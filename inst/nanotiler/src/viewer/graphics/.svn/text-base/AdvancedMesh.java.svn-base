package viewer.graphics;

/**
 * Supports vertex coloring and polygon
 * sorting based on sides
 * @author Calvin
 *
 */
public interface AdvancedMesh extends Mesh, Cloneable {
	/**
	 * Get the material of the vertex
	 * @param vertex Index of vertex to get the material color of
	 * @return Returns the material
	 */
	public Material getVertexMaterial(int vertex);
	
	/**
	 * Get an array of the points
	 * @return Returns an array of arrays of indices for the
	 * vertices of points
	 */
	public int[][] getPoints();
	
	/**
	 * Get an array of the lines
	 * @return Returns an array of arrays of indices for the
	 * vertices of lines.
	 */
	public int[][] getLines();
	
	/**
	 * Get an array of the triangles
	 * @return Returns an array of arrays of indices for the
	 * vertices of triangles.
	 */
	public int[][] getTriangles();
	
	/**
	 * Get an array of the quads
	 * @return Returns an array of arrays of indices for the
	 * vertices of quads.
	 */
	public int[][] getQuads();
	
	/**
	 * Get an array of polygons (>= 5 sides)
	 * @return Returns an array of arrays of indices for the
	 * vertices of polygons.
	 */
	public int[][] getPolygons();
	
	/**
	 * Get the number of polygons
	 * @return Returns the number of polygons
	 */
	public int getPolygonCount();
	
	/**
	 * Get the number of points
	 * @return Returns the number of points
	 */
	public int getPointCount();
	
	/**
	 * Get the number of lines
	 * @return Returns the number of lines
	 */
	public int getLineCount();
	
	/**
	 * Get the number of triangles
	 * @return Returns the number of triangles
	 */
	public int getTriangleCount();
	
	/**
	 * Get the number of quads
	 * @return Returns the number of quads
	 */
	public int getQuadCount();
	
	/**
	 * Clone the mesh
	 * @return Returns the cloned mesh
	 */
	public Object clone();
	
	/**
	 * Get the width of the lines 
	 * @return Returns the width of the lines
	 */
	public double getLineWidth();
	
	/**
	 * Get the size of the points
	 * @return Returns the size of the points
	 */
	public double getPointSize();
}
