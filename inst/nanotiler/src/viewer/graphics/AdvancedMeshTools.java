package viewer.graphics;

/**
 * Tools for manipulating and synthesizing advanced meshes
 * @author Calvin
 *
 */
public class AdvancedMeshTools {

	/**
	 * Extracts the wireframe cage of the passed in mesh. Uses colors
	 * stored in the mesh itself
	 * @param mesh Mesh
	 * @return Returns a new mesh that is a cage of the passed in mesh
	 */
	public static AdvancedMesh extractCage(AdvancedMesh mesh) {
		return extractCage(mesh, null);
	}
	
	/**
	 * Extracts the wireframe cage of the passed in mesh. Uses the passed
	 * material to color the wireframe cage mesh. 
	 * @param mesh Mesh to generate cage from
	 * @param material Color of the caged mesh
	 * @return Returns a new mesh that is a cage of the passed in mesh
	 */
	public static AdvancedMesh extractCage(AdvancedMesh mesh, Material material) {
		DefaultAdvancedMesh m = new DefaultAdvancedMesh();
		
		int[][] elements = mesh.getGeometryElements();
		int lineCounter = 0;
		for(int i = 0; i < elements.length; ++i) {
			if(elements[i].length < 3)
				continue;
			
			for(int j = 0; j < elements[i].length; ++j) {
				m.add(mesh.getVertex(elements[i][j]), mesh.getNormal(elements[i][j]), material == null ? mesh.getVertexMaterial(elements[i][j]) : material);
				m.add(mesh.getVertex(elements[i][(j + 1) % elements[i].length]), mesh.getNormal(elements[i][(j + 1) % elements[i].length]), material == null ? mesh.getVertexMaterial(elements[i][(j + 1) % elements[i].length]) : material);
				int[] line = {
						2 * lineCounter,
						2 * lineCounter + 1
				};
				m.add(line);
				lineCounter++;
			}
		}
		
		return m;
	}
}
