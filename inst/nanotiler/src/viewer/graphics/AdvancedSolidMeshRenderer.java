package viewer.graphics;

import javax.media.opengl.GL;
import tools3d.Vector4D;

/**
 * Renders and advanced mesh. Fills in all the polygons as oppossed to
 * only rendering the lines that compose them.
 * @author Calvin
 *
 */
public class AdvancedSolidMeshRenderer extends SolidMeshRenderer {

	public AdvancedSolidMeshRenderer(Mesh m) {
		super(m);
	}
	
	public AdvancedSolidMeshRenderer() {
		
	}
	
	@Override
	public void renderMesh(GL gl, Mesh mesh) {
		if(mesh instanceof AdvancedMesh) {
			AdvancedMesh m = (AdvancedMesh) mesh;
			// render points
			gl.glPointSize((float)m.getPointSize());
			gl.glBegin(GL.GL_POINTS);
			for(int[] point : m.getPoints()) {
				Material material = m.getVertexMaterial(point[0]);
				if(material != null) {
					Material.renderMaterial(gl, material);
					ColorVector vector = material.getDiffuse();
					gl.glColor3d(vector.getRed(), vector.getGreen(), vector.getBlue());
				}
				Vector4D normal = m.getNormal(point[0]);
				Vector4D vertex = m.getVertex(point[0]);
				gl.glNormal3d(normal.getX(), normal.getY(), normal.getZ());
				gl.glVertex3d(vertex.getX(), vertex.getY(), vertex.getZ());
			}
			
			gl.glEnd();
			
			// render lines
			gl.glLineWidth((float)m.getLineWidth());
			gl.glBegin(GL.GL_LINES);
			for(int[] line : m.getLines()) {
				Material material = m.getVertexMaterial(line[0]);
				if(material != null) {
					ColorVector vector = material.getDiffuse();
					gl.glColor3d(vector.getRed(), vector.getGreen(), vector.getBlue());
					Material.renderMaterial(gl, material);
				}
				Vector4D normal = m.getNormal(line[0]);
				Vector4D vertex = m.getVertex(line[0]);
				gl.glNormal3d(normal.getX(), normal.getY(), normal.getZ());
				gl.glVertex3d(vertex.getX(), vertex.getY(), vertex.getZ());
				material = m.getVertexMaterial(line[1]);
				if(material != null) {
					Material.renderMaterial(gl, material);
					ColorVector vector = material.getDiffuse();
					gl.glColor3d(vector.getRed(), vector.getGreen(), vector.getBlue());
				}
				normal = m.getNormal(line[1]);
				vertex = m.getVertex(line[1]);
				gl.glNormal3d(normal.getX(), normal.getY(), normal.getZ());
				gl.glVertex3d(vertex.getX(), vertex.getY(), vertex.getZ());
			}
			
			gl.glEnd();
			// render triangles
			gl.glBegin(GL.GL_TRIANGLES);
			for(int[] a : m.getTriangles()) {
				for(int i = 0; i < 3; ++i) {
					Material material = m.getVertexMaterial(a[i]);
					if(material != null) {
						Material.renderMaterial(gl, material);
						ColorVector vector = material.getDiffuse();
						gl.glColor3d(vector.getRed(), vector.getGreen(), vector.getBlue());
					}
					Vector4D normal = m.getNormal(a[i]);
					Vector4D vertex = m.getVertex(a[i]);
					gl.glNormal3d(normal.getX(), normal.getY(), normal.getZ());
					gl.glVertex3d(vertex.getX(), vertex.getY(), vertex.getZ());
				}
			}
			
			
			gl.glEnd();
			
			// render quads
			gl.glBegin(GL.GL_QUADS);
			for(int[] a : m.getQuads()) {
				for(int i = 0; i < 4; ++i) {
					Material material = m.getVertexMaterial(a[i]);
					if(material != null) {
						Material.renderMaterial(gl, material);
						ColorVector vector = material.getDiffuse();
						gl.glColor3d(vector.getRed(), vector.getGreen(), vector.getBlue());
					}
					Vector4D normal = m.getNormal(a[i]);
					Vector4D vertex = m.getVertex(a[i]);
					gl.glNormal3d(normal.getX(), normal.getY(), normal.getZ());
					gl.glVertex3d(vertex.getX(), vertex.getY(), vertex.getZ());
				}
			}
			
			gl.glEnd();
			
			// render polygons
			for(int[] a : m.getGeometryElements()) {
				gl.glBegin(GL.GL_POLYGON);
				for(int i = 0; i < a.length; ++i) {
					Material material = m.getVertexMaterial(a[i]);
					if(material != null) {
						Material.renderMaterial(gl, material);
						ColorVector vector = material.getDiffuse();
						gl.glColor3d(vector.getRed(), vector.getGreen(), vector.getBlue());
					}
					Vector4D normal = m.getNormal(a[i]);
					Vector4D vertex = m.getVertex(a[i]);
					gl.glNormal3d(normal.getX(), normal.getY(), normal.getZ());
					gl.glVertex3d(vertex.getX(), vertex.getY(), vertex.getZ());
				}
				
				gl.glEnd();
			}
			
		}
		
		else
			super.renderMesh(gl, mesh);
	}

}
