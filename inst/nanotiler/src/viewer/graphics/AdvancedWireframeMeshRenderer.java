package viewer.graphics;

import javax.media.opengl.GL;
import tools3d.Vector4D;

/**
 * Renders an advanced mesh. Only renders the edges of polygons.
 * @author Calvin
 *
 */
public class AdvancedWireframeMeshRenderer extends WireframeMeshRenderer {

	public AdvancedWireframeMeshRenderer(AdvancedMesh mesh) {
		super(mesh);
	}
	
	public AdvancedWireframeMeshRenderer() {
		
	}
	
	public void renderMesh(GL gl, Mesh m) {
		if(m instanceof AdvancedMesh) {
			AdvancedMesh mesh = (AdvancedMesh) m;
			int[][] elements = m.getGeometryElements();
			gl.glPointSize((float)mesh.getPointSize());
			gl.glBegin(GL.GL_POINTS);
			for(int i = 0; i < mesh.getPointCount(); ++i) {
				int[] point = elements[i];
				Material material = mesh.getVertexMaterial(point[0]);
				Material.renderMaterial(gl, material);
				ColorVector vector = material.getDiffuse();
				gl.glColor3d(vector.getRed(), vector.getGreen(), vector.getBlue());
				Vector4D normal = mesh.getNormal(point[0]);
				gl.glNormal3d(normal.getX(), normal.getY(), normal.getZ());
				Vector4D vertex = mesh.getVertex(point[0]);
				gl.glVertex3d(vertex.getX(), vertex.getY(), vertex.getZ());
				
			}
			
			gl.glEnd();
			gl.glLineWidth((float)mesh.getLineWidth());
			gl.glBegin(GL.GL_LINES);
			for(int i = mesh.getPointCount(); i < m.getGeometryElementCount(); ++i) {
				int[] poly = elements[i];
				for(int j = 0; j < poly.length; ++j) {
					Material material = mesh.getVertexMaterial(poly[j]);
					Material.renderMaterial(gl, material);
					ColorVector vector = material.getDiffuse();
					gl.glColor3d(vector.getRed(), vector.getGreen(), vector.getBlue());
					Vector4D normal = mesh.getNormal(poly[j]);
					gl.glNormal3d(normal.getX(), normal.getY(), normal.getZ());
					Vector4D vertex = mesh.getVertex(poly[j]);
					gl.glVertex3d(vertex.getX(), vertex.getY(), vertex.getZ());
					
					material = mesh.getVertexMaterial(poly[(j + 1) % poly.length]);
					Material.renderMaterial(gl, material);
					vector = material.getDiffuse();
					gl.glColor3d(vector.getRed(), vector.getGreen(), vector.getBlue());
					normal = mesh.getNormal(poly[(j + 1) % poly.length]);
					gl.glNormal3d(normal.getX(), normal.getY(), normal.getZ());
					vertex = mesh.getVertex(poly[(j + 1) % poly.length]);
					gl.glVertex3d(vertex.getX(), vertex.getY(), vertex.getZ());
				}
			}
			
			gl.glEnd();
		}
		
		else
			super.renderMesh(gl, m);
	}
	
}
