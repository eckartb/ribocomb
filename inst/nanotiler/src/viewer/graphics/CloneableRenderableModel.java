package viewer.graphics;

/**
 * Allows a renderable model to be cloned
 * @author Calvin
 *
 */
public interface CloneableRenderableModel extends RenderableModel, Cloneable {
	/**
	 * Clone the model
	 * @return Returns a clone of the model
	 */
	public Object clone();
}
