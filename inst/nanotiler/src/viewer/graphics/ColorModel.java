package viewer.graphics;

import tools3d.objects3d.*;
import tools3d.objects3d.Link;
import rnadesign.rnamodel.Atom3D;
import java.awt.Color;

/**
 * A color model allows maps a color to an object
 * @author Calvin
 *
 */
public interface ColorModel {
	/**
	 * Get a material for an object
	 * @param o Object to get the material for
	 * @return Returns a material
	 */
	public Material getMaterial(Object3D o);
	
	/**
	 * Get a color for an object. Ideally, the color
	 * should be representative of the returned material.
	 * @param o Object to get color for.
	 * @return Returns the color for the object
	 */
	public Color getColor(Object3D o);
	
	/**
	 * Get a material for a link
	 * @param link Link to get material for.
	 * @return Returns the material for the link.
	 */
	public Material getMaterial(Link link);
	
	/**
	 * Resets the color model. Some color models
	 * may not need to be reset. In that case,
	 * this method can be left blank.
	 *
	 */
	public void reset();

        /**
	 * Get a material for an atom
	 * @param link Link to get material for.
	 * @return Returns the material for the atom.
	 *   
	 */
        public Material getAtomMaterial(Atom3D atom); 
}
