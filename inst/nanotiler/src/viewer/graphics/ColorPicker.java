package viewer.graphics;

import java.awt.Color;
import java.util.Random;

public class ColorPicker {
	
	private double red, green, blue;
	
	private double redFactor, blueFactor, greenFactor;

	public ColorPicker(Color color) {
		this(color.getRed() / 255.0, color.getGreen() / 255.0, color.getBlue() / 255.0);
	}
	
	public ColorPicker(int red, int green, int blue) {
		this(red / 255.0, green / 255.0, blue / 255.0);
	}
	
	public ColorPicker(double red, double green, double blue) {
		this.red = red;
		this.green = green;
		this.blue = blue;
		
		Random r = new Random();
		redFactor = (r.nextInt(20) - 10) / 255.0;
		blueFactor = (r.nextInt(20) - 10) / 255.0;
		greenFactor = (r.nextInt(20) - 10) / 255.0;
	}
	
	public Color getColor() {
		return new Color((int) (red * 255), (int) (green * 255), (int) (blue * 255));
	}
	
	public ColorVector getColorVector() {
		return new ColorVector(red, green, blue, 1.0);
	}
	
	public double getRed() {
		return red;
	}
	
	public double getBlue() {
		return blue;
	}
	
	public double getGreen() {
		return green;
	}
	
	public ColorVector getColorVector(int i) {
		double r = (red + i * redFactor) % 2.0;
		double g = (green + i * greenFactor) % 2.0;
		double b = (blue + i * blueFactor) % 2.0;
		
		r = clampColor(r);
		g = clampColor(g);
		b = clampColor(b);
		
		return new ColorVector(r, g, b, 1.0);
		
		
	}
	
	private double clampColor(double d) {
		if(d < 0.0) {
			if(d >= -1.0) 
				return Math.abs(d);
			
			
			return 2.0 - Math.abs(d);
			
		}
		
		if(d <= 1.0)
				return d;

		return 2.0 - d;
		
	}
	
	public Color getColor(int i) {
		ColorVector vector = getColorVector(i);
		return new Color((int) (vector.getRed() * 255), 
				(int) (vector.getGreen() * 255), 
				(int) (vector.getBlue() * 255));
	}
}
