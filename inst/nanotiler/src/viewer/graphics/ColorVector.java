package viewer.graphics;

import java.nio.FloatBuffer;
import java.awt.Color;

/**
 * A color vector is essentially a vector whose
 * components are color values. A color vector has four components:
 * red, green, blue, and alpha. Each of these has a possible value
 * from 0.0 to 1.0.
 * @author Calvin
 *
 */
public class ColorVector implements Cloneable {
	
	private double red;
	private double blue;
	private double green;
	private double alpha;
	
	public ColorVector(double red, double green, double blue, double alpha) {
		this.red = red;
		this.green = green;
		this.blue = blue;
		this.alpha = alpha;
	}
	
	public ColorVector(ColorVector v) {
		red = v.red;
		green = v.green;
		blue = v.blue;
		alpha = v.alpha;
	}
	
	/**
	 * Get the AWT Color (java.awt.Color) for
	 * this color vector.
	 * @return Returns the color
	 */
	public Color getAWTColor() {
		return new Color((int) (255 * red), (int) (255 * green), (int) (255 * blue), (int) (255 * alpha));
	}

	/**
	 * Get the blue component
	 * @return Returns blue component
	 */
	public double getBlue() {
		return blue;
	}

	/**
	 * Set the blue component
	 * @param blue Value ranging from 0.0 to 1.0
	 */
	public void setBlue(double blue) {
		this.blue = blue;
	}

	/**
	 * Get the green component
	 * @return Returns the green component
	 */
	public double getGreen() {
		return green;
	}

	/**
	 * Set the green component
	 * @param green Value between 0.0 and 1.0
	 */
	public void setGreen(double green) {
		this.green = green;
	}

	/**
	 * Get the red component
	 * @return Returns the red component
	 */
	public double getRed() {
		return red;
	}

	/**
	 * Set the red component
	 * @param red Value ranging from 0.0 to 1.0
	 */
	public void setRed(double red) {
		this.red = red;
	}

	/**
	 * Get the alpha component
	 * @return Returns the alpha component
	 */
	public double getAlpha() {
		return alpha;
	}

	/**
	 * Set the alpha component
	 * @param alpha Value ranging from 0.0 to 1.0
	 */
	public void setAlpha(double alpha) {
		this.alpha = alpha;
	}
	
	/**
	 * Returns the color vector as a buffer of floats. Meant
	 * for use with open_gl rendering routines.
	 * @return Returns a buffer of floats in this order:
	 * {red, green, blue, alpha}
	 */
	public FloatBuffer buffer() {
		float[] array = { (float) red, (float) green, (float) blue, (float) alpha };
		return FloatBuffer.wrap(array);
	}
	
	public Object clone() {
		try {
			return super.clone();
		} catch (CloneNotSupportedException e) {
			return null;
		}
	}
}
