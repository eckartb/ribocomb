package viewer.graphics;

/**
 * Colorable objects have access to a color model which they
 * can use to get the color for objects.
 * @author Calvin
 *
 */
public interface Colorable {
	
	/**
	 * Set the color model
	 * @param model Model
	 */
	public void setColorModel(ColorModel model);
	
	/**
	 * Get the color model.
	 * @return Returns the color model
	 */
	public ColorModel getColorModel();
}
