package viewer.graphics;

import tools3d.Vector3D;

/**
 * Renders a curve
 * @author Calvin
 *
 */
public interface CurveRenderer extends RenderableModel {
	/**
	 * Get the curve this renderer is rendering
	 * @return Returns an array of points composing the curve
	 */
	public Vector3D[] getCurve();
}
