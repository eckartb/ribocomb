package viewer.graphics;

import javax.media.opengl.GL;

import java.util.ArrayList;

import tools3d.Vector3D;
import tools3d.Vector4D;

import viewer.util.Tools;

/**
 * Renders a cylindrical curve
 * @author Calvin
 *
 */
public class CylindricalCurveRenderer implements CurveRenderer {
	
	
	private Vector3D[] curve;

	
	private boolean wireframeMode;
	
	private Mesh mesh;
	
	private double radius;
	private int sides;
	
	private static final Vector4D POLYGON_NORMAL = new Vector4D(0.0, 1.0, 0.0, 0.0);
	
	public CylindricalCurveRenderer(Vector3D[] curve) {
		this(curve, false);
	}
	
	public CylindricalCurveRenderer(Vector3D[] curve, boolean wireframeMode) {
		this(curve, 0.5, 8, false);
	}
	
	public CylindricalCurveRenderer(Vector3D[] curve, double radius, int sides, boolean wireframeMode) {
		this.curve = curve;
		this.wireframeMode = wireframeMode;
		this.radius = radius;
		this.sides = sides;
		
		Vector4D[] polygon = new Vector4D[sides];
		Vector4D normal = Tools.orthogonal(POLYGON_NORMAL);
		normal.normalize();
		normal = normal.mul(radius);
		double angle = -Math.PI * 2.0 / sides;
		for(int i = 0; i < sides; ++i) {
			polygon[i] = normal;
			normal = Tools.rotate(normal, POLYGON_NORMAL, angle);
			normal.normalize();
			normal = normal.mul(radius);
		}
		
		mesh = MeshOperations.extend(polygon, POLYGON_NORMAL, curve);
		
	}

	public Vector3D[] getCurve() {
		return curve;
	}

	public void render(GL gl) {
		MeshRenderer renderer;
		if(wireframeMode) {
			renderer = new WireframeMeshRenderer(mesh);
		} else {
			renderer = new SolidMeshRenderer(mesh);
		}
		
		renderer.render(gl);

	}

	public boolean isWireframeMode() {
		return wireframeMode;
	}

	public void setWireframeMode(boolean wireframeMode) {
		this.wireframeMode = wireframeMode;
	}
	
	/*private void assembleMesh() {
		final ArrayList<Vector4D> vertices = new ArrayList<Vector4D>();
		final ArrayList<Vector4D> normals = new ArrayList<Vector4D>();
		final ArrayList<int[]> polygons = new ArrayList<int[]>();
		
		double inc = Math.PI * 2 / rotations;
		
		Vector4D position = new Vector4D(curve[0]);
		Vector4D axis = new Vector4D(curve[1]).minus(new Vector4D(curve[0]));
		axis.normalize();
		Vector4D normal = ViewerTools.orthogonal(axis);
		normal.normalize();
		normal = normal.mul(radius);
		
		
		for(int i = 0; i < rotations; ++i) {
			vertices.add(normal.plus(position));
			Vector4D n = new Vector4D(normal);
			n.normalize();
			normals.add(n);
			normal = ViewerTools.rotate(normal, axis, inc);
		}
		
		for(int i = 1; i < curve.length - 1; ++i) {
			position = new Vector4D(curve[i]);
			axis = new Vector4D(curve[i + 1].minus(curve[i - 1]));
			axis.normalize();
			normal = ViewerTools.orthogonal(axis);
			normal.normalize();
			normal = normal.mul(radius);
			for(int j = 0; j < rotations; ++j) {
				vertices.add(normal.plus(position));
				Vector4D n = new Vector4D(normal);
				n.normalize();
				normals.add(n);
				normal = ViewerTools.rotate(normal, axis, inc);
			}
		}
		
		position = new Vector4D(curve[curve.length - 1]);
		axis = new Vector4D(curve[curve.length - 1].minus(curve[curve.length - 2]));
		axis.normalize();
		normal = ViewerTools.orthogonal(axis);
		normal.normalize();
		normal = normal.mul(radius);
		
		for(int i = 0; i < rotations; ++i) {
			vertices.add(normal.plus(position));
			Vector4D n = new Vector4D(normal);
			n.normalize();
			normals.add(n);
			normal = ViewerTools.rotate(normal, axis, inc);
		}
		
		for(int i = 0; i < curve.length - 1; ++i) {
			for(int j = 0; j < rotations; ++j) {
				int[] t1 = {
						(i * rotations) + j,
						(i * rotations) + ((j + 1) % rotations),
						((i + 1) * rotations) + ((j + 1) % rotations),
				};
				
				int[] t2 = {
					t1[0],
					t1[2],
					((i + 1) * rotations) + j,
				};
				
				polygons.add(t1);
				polygons.add(t2);
			}
		}
		
		vertices.add(new Vector4D(curve[0]));
		normal = new Vector4D(curve[0].minus(curve[1]));
		normal.normalize();
		normals.add(normal);
		
		vertices.add(new Vector4D(curve[curve.length -1]));
		normal = new Vector4D(curve[curve.length - 1].minus(curve[curve.length - 2]));
		normal.normalize();
		normals.add(normal);
		
		for(int i = 0; i < rotations; ++i) {
			int[] t1 = {
					i, i + 1, vertices.size() - 2,
			};
			
			int offset = (curve.length - 1) * rotations;
			
			int[] t2 = {
				offset + i, offset + i + 1, vertices.size() - 1,
			};
			
			polygons.add(t1);
			polygons.add(t2);
		}
		
		
		mesh = new Mesh() {

			public Vector4D getNormal(int index) {
				return normals.get(index);
			}

			public Vector4D[] getNormals() {
				Vector4D[] array = new Vector4D[normals.size()];
				normals.toArray(array);
				return array;
			}

			public int[] getPolygon(int index) {
				return polygons.get(index);
			}

			public int getPolygonCount() {
				return polygons.size();
			}

			public int[][] getPolygons() {
				int[][] array = new int[polygons.size()][];
				polygons.toArray(array);
				return array;
			}

			public int getPolygonSideCount() {
				return 3;
			}

			public Vector4D getVertex(int index) {
				return vertices.get(index);
			}

			public int getVertexCount() {
				return vertices.size();
			}

			public Vector4D[] getVertices() {
				Vector4D[] array = new Vector4D[vertices.size()];
				vertices.toArray(array);
				return array;
			}
			
		};
		
	}*/
	
	
	
	

}
