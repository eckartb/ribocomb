package viewer.graphics;

import tools3d.Vector4D;
import java.util.ArrayList;

/**
 * Default implementation of an advanced mesh
 * @author Calvin
 *
 */
public class DefaultAdvancedMesh implements AdvancedMesh {
	
	private ArrayList<Vector4D> vertices = new ArrayList<Vector4D>();
	private ArrayList<Vector4D> normals = new ArrayList<Vector4D>();
	private ArrayList<Material> materials = new ArrayList<Material>();
	private ArrayList<int[]> points = new ArrayList<int[]>();
	private ArrayList<int[]> lines = new ArrayList<int[]>();
	private ArrayList<int[]> triangles = new ArrayList<int[]>();
	private ArrayList<int[]> quads = new ArrayList<int[]>();
	private ArrayList<int[]> polygons = new ArrayList<int[]>();
	
	private double pointSize = 1.0;
	private double lineWidth = 1.0;

	/**
	 * Get polygons with the specified side. Note that
	 * if sides > 4, polygons with sides greater than 4
	 * will be returned for any value of sides
	 * @param sides Number of sides of the polygon
	 * @return Returns an array of arrays of vertex indices
	 */
	public int[][] getPolygons(int sides) {
		ArrayList<int[]> list = null;
		switch(sides) {
		case 1:
			list = points;
			break;
		case 2:
			list = lines;
			break;
		case 3:
			list = triangles;
			break;
		case 4:
			list = quads;
			break;
		default:
			list = polygons;
		}
		
		int[][] a = new int[list.size()][];
		list.toArray(a);
		return a;
	}

	public Material getVertexMaterial(int vertex) {
		return materials.get(vertex);
	}
	
	/**
	 * Set the material for a vertex
	 * @param vertex Index of the vertex to set the material
	 * for
	 * @param material Material to color vertex with
	 */
	public void setVertexMaterial(int vertex, Material material) {
		materials.set(vertex, material);
	}

	public Vector4D getNormal(int index) {
		return normals.get(index);
	}

	public Vector4D[] getNormals() {
		Vector4D[] array = new Vector4D[normals.size()];
		normals.toArray(array);
		return array;
	}

	public int[] getGeometryElement(int index) {
		return getGeometryElements()[index];
	}
	
	/**
	 * Get a line
	 * @param index Index of line
	 * @return Returns an array of vertex indices composing
	 * the line
	 */
	public int[] getLine(int index) {
		return lines.get(index);
	}
	
	/**
	 * Get a point
	 * @param index Index of point
	 * @return Returns an array of vertex indices composing
	 * the point.
	 */
	public int[] getPoint(int index) {
		return points.get(index);
	}
	
	/**
	 * Get a triangle
	 * @param index Index of the triangle
	 * @return Returns an array of vertex indices composing
	 * the triangle
	 */
	public int[] getTriangle(int index) {
		return triangles.get(index);
	}
	
	/**
	 * Get a quad
	 * @param index Index of the quad
	 * @return Returns an array of vertex indices composing
	 * the quad
	 */
	public int[] getQuad(int index) {
		return quads.get(index);
	}

	public int getPolygonCount() {
		return polygons.size();
	}
	
	public int getPointCount() {
		return points.size();
	}
	
	public int getLineCount() {
		return lines.size();
	}
	
	public int getTriangleCount() {
		return triangles.size();
	}
	
	public int getQuadCount() {
		return quads.size();
	}

	
	public int getPolygonSideCount() {
		throw new UnsupportedOperationException();
	}

	public int[][] getPolygons() {
		int[][] array = new int[polygons.size()][];
		polygons.toArray(array);
		return array;
	}
	
	

	public int[][] getLines() {
		int[][] array = new int[lines.size()][];
		lines.toArray(array);
		return array;
	}

	public int[][] getPoints() {
		int[][] array = new int[points.size()][];
		points.toArray(array);
		return array;
	}

	public int[][] getQuads() {
		int[][] array = new int[quads.size()][];
		quads.toArray(array);
		return array;
	}

	public int[][] getTriangles() {
		int[][] array = new int[triangles.size()][];
		triangles.toArray(array);
		return array;
	}

	public Vector4D getVertex(int index) {
		return vertices.get(index);
	}

	public int getVertexCount() {
		return vertices.size();
	}

	public Vector4D[] getVertices() {
		Vector4D[] array = new Vector4D[vertices.size()];
		vertices.toArray(array);
		return array;
	}
	
	/**
	 * Add a geometry component to the mesh
	 * @param vertex Vertex to add
	 * @param normal Normal of vertex to add
	 * @param material Material to color vertex with
	 * @return Returns index of added geometry element
	 */
	public int add(Vector4D vertex, Vector4D normal, Material material) {
		int ret = vertices.size();
		vertices.add(vertex);
		normals.add(normal);
		materials.add(material);
		return ret;
	}
	
	/**
	 * Adds the geometry contained in the mesh
	 * to this advanced mesh.
	 * @param mesh Mesh to add geometry from
	 * @param material Material to color all vertices
	 * taken from mesh
	 */
	public void add(Mesh mesh, Material material) {
		int offset = getVertexCount();
		int[][] elements = mesh.getGeometryElements().clone();
		for(int i = 0; i < mesh.getVertexCount(); ++i) {
			add((Vector4D) mesh.getVertex(i).clone(), (Vector4D) mesh.getNormal(i).clone(), material);
		}
		for(int i = 0; i < elements.length; ++i) {
			for(int j = 0; j < elements[i].length; ++j) {
				elements[i][j] += offset;
			}
			
			add(elements[i]);
		}
	}
	
	/**
	 * Add the geometry from an advanced mesh to this mesh
	 * @param mesh Mesh to add geometry from
	 */
	public void add(AdvancedMesh mesh) {
		int offset = getVertexCount();
		int[][] elements = mesh.getGeometryElements().clone();
		for(int i = 0; i < mesh.getVertexCount(); ++i)
			add((Vector4D) mesh.getVertex(i).clone(), (Vector4D) mesh.getNormal(i).clone(), (Material) mesh.getVertexMaterial(i).clone());
		for(int i = 0; i < elements.length; ++i) {
			for(int j = 0; j < elements[i].length; ++j) {
				elements[i][j] += offset;
			}
			
			add(elements[i]);
		}
	}
	
	/**
	 * Add a polygon consisting of
	 * indices to geometry elements.
	 * @param polygon Array of indices
	 */
	public void add(int[] polygon) {
		switch(polygon.length) {
		case 1:
			points.add(polygon);
			break;
		case 2:
			lines.add(polygon);
			break;
		case 3:
			triangles.add(polygon);
			break;
		case 4:
			quads.add(polygon);
			break;
		default:
			polygons.add(polygon);
				
		}
	}
	
	public int getGeometryElementCount() {
		return lines.size() + points.size() + triangles.size() + quads.size() + polygons.size();
	}
	
	public int[][] getGeometryElements() {
		ArrayList<int[]> all = new ArrayList<int[]>(getGeometryElementCount());
		all.addAll(points);
		all.addAll(lines);
		all.addAll(triangles);
		all.addAll(quads);
		all.addAll(polygons);
		
		int[][] array = new int[all.size()][];
		all.toArray(array);
		return array;
	}
	
	public Object clone() {
		DefaultAdvancedMesh mesh = new DefaultAdvancedMesh();
		mesh.add(this);
		return mesh;
	}

	public double getLineWidth() {
		return lineWidth;
	}

	/**
	 * Set the line width that should
	 * be used to render lines
	 * @param lineWidth Width in pixels of line
	 */
	public void setLineWidth(double lineWidth) {
		this.lineWidth = lineWidth;
	}

	public double getPointSize() {
		return pointSize;
	}

	/**
	 * Set the point size that should
	 * be used to render points
	 * @param pointSize Size of points.
	 */
	public void setPointSize(double pointSize) {
		this.pointSize = pointSize;
	}
	
	/**
	 * Convenience method to add a point to the mesh
	 * @param vertex Vertex of the point to add
	 * @param normal Normal of the point to add
	 * @param material Material of the point to add
	 */
	public void addPoint(Vector4D vertex, Vector4D normal, Material material) {
		int index = vertices.size();
		vertices.add(vertex);
		normals.add(normal);
		int[] a = { index };
		points.add(a);
		materials.add(material);
	}
	
	
	

}
