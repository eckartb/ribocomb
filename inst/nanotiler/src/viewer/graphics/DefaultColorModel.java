package viewer.graphics;

import java.awt.Color;
import tools3d.objects3d.Link;
import tools3d.objects3d.Object3D;
import rnadesign.rnamodel.Atom3D;

/**
 * This color model provides a white material.
 * @author Calvin
 *
 */
public class DefaultColorModel implements ColorModel {

	public Color getColor(Object3D o) {
		return Color.white;
	}

	public Material getMaterial(Object3D o) {
		return new Material(1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.8, 1.0, 1.0, 1.0, 0.2, 0.0, 0.0, 0.0, 0.0, 10.0);
	}

	public Material getMaterial(Link link) {
		return getMaterial(link.getObj1());
	}

    public Material getAtomMaterial(Atom3D atom){
	return getMaterial((Object3D)atom);
    }

	public void reset() {
		// TODO Auto-generated method stub

	}

}
