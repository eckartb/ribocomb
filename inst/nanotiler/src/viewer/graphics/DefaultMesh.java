package viewer.graphics;

import java.util.List;

import tools3d.Vector4D;

/**
 * Default implementation of mesh interface
 * @author Calvin
 *
 */
public class DefaultMesh implements Mesh {

	private List<Vector4D> vertices;
	private List<Vector4D> normals;
	private List<int[]> polygons;
	private int sideCount;
	
	
	
	/**
	 * Constructs mesh from lists of vertices, normals, polygons, and a side
	 * count the specifies the number of sides of each of the polygons.
	 * @param vertices
	 * @param normals
	 * @param polygons
	 * @param sideCount An integer >= 3 that specifies the number of sides of
	 * EVERY polygon in the polygons list
	 */
	public DefaultMesh(List<Vector4D> vertices, List<Vector4D> normals, List<int[]> polygons, int sideCount) {
		super();
		this.vertices = vertices;
		this.normals = normals;
		this.polygons = polygons;
		this.sideCount = sideCount;
	}

	public Vector4D getNormal(int index) {
		return normals.get(index);
	}

	public Vector4D[] getNormals() {
		Vector4D[] array = new Vector4D[normals.size()];
		normals.toArray(array);
		return array;
	}

	public int[] getGeometryElement(int index) {
		return polygons.get(index);
	}

	public int getGeometryElementCount() {
		return polygons.size();
	}

	public int[][] getGeometryElements() {
		int[][] array = new int[polygons.size()][];
		polygons.toArray(array);
		return array;
	}

	public int getPolygonSideCount() {
		return sideCount;
	}

	public Vector4D getVertex(int index) {
		return vertices.get(index);
	}

	public int getVertexCount() {
		return vertices.size();
	}

	public Vector4D[] getVertices() {
		Vector4D[] array = new Vector4D[vertices.size()];
		vertices.toArray(array);
		return array;
	}
	
}
