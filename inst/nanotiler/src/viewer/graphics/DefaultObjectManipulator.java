package viewer.graphics;

import javax.media.opengl.GL;

import tools3d.Vector4D;
import tools3d.AABBBoundingVolume;
import tools3d.CollisionDetectionTools;
import tools3d.BoundingVolume;

/**
 * Manipulator that allows objects to be manipulated
 * in 3D space by interacting with the manipulator
 * @author Calvin
 *
 */
public class DefaultObjectManipulator extends ObjectManipulatorPlanes {
	
	private AABBBoundingVolume xAxisVolume;
	private AABBBoundingVolume yAxisVolume;
	private AABBBoundingVolume zAxisVolume;
	
	private Vector4D xyPlaneDiagonal;
	private Vector4D xzPlaneDiagonal;
	private Vector4D yzPlaneDiagonal;
	
	public static final double BV_WIDTH = 0.25;
	public static final double BV_LENGTH = 2.0;
	public static final double DIAGONAL_LENGTH = 2.0;
	
	private AABBBoundingVolume boundingVolume;
	
	
	public DefaultObjectManipulator(Vector4D position) {
		setPosition(position);
	}
	
	public BoundingVolume getBoundingVolume() {
		return boundingVolume;
	}
	
	public void setPosition(Vector4D position) {
		super.setPosition(position);
		
		double x = position.getX();
		double y = position.getY();
		double z = position.getZ();
		
		Vector4D c1 = new Vector4D(x, y - BV_WIDTH, z - BV_WIDTH, 1.0);
		Vector4D c2 = new Vector4D(x + BV_LENGTH, y + BV_WIDTH, z + BV_WIDTH, 1.0);
		
		xAxisVolume = new AABBBoundingVolume(c1, c2);
		
		c1 = new Vector4D(x - BV_WIDTH, y, z - BV_WIDTH, 1.0);
		c2 = new Vector4D(x + BV_WIDTH, y + BV_LENGTH, z + BV_WIDTH);
		
		yAxisVolume = new AABBBoundingVolume(c1, c2);
		
		c1 = new Vector4D(x - BV_WIDTH, y - BV_WIDTH , z, 1.0);
		c2 = new Vector4D(x + BV_WIDTH, y + BV_WIDTH, z + BV_LENGTH, 1.0);
		
		zAxisVolume = new AABBBoundingVolume(c1, c2);
		
		double n = BV_LENGTH / Math.sqrt(2);
		
		xyPlaneDiagonal = new Vector4D(n, n, 0.0, 0.0);
		xyPlaneDiagonal.setX(xyPlaneDiagonal.getX() + x);
		xyPlaneDiagonal.setY(xyPlaneDiagonal.getY() + y);
		xyPlaneDiagonal.setZ(z);
		xyPlaneDiagonal.setW(1.0);
		
		xzPlaneDiagonal = new Vector4D(n, 0.0, n, 0.0);
		xzPlaneDiagonal.setX(xzPlaneDiagonal.getX() + x);
		xzPlaneDiagonal.setY(y);
		xzPlaneDiagonal.setZ(xzPlaneDiagonal.getZ() + z);
		xzPlaneDiagonal.setW(1.0);
		
		yzPlaneDiagonal = new Vector4D(0.0, n, n, 0.0);
		yzPlaneDiagonal.setX(x);
		yzPlaneDiagonal.setY(yzPlaneDiagonal.getY() + y);
		yzPlaneDiagonal.setZ(yzPlaneDiagonal.getZ() + z);
		yzPlaneDiagonal.setW(1.0);
		
		boundingVolume = new AABBBoundingVolume(new Vector4D(x - BV_WIDTH, y - BV_WIDTH, z - BV_WIDTH, 1.0), 
				new Vector4D(x + BV_LENGTH, y + BV_LENGTH, z + BV_LENGTH, 1.0));

	}

	@Override
	public int getIntersectionPlane(Vector4D ray, Vector4D origin) {
		int mask = 0;
		Vector4D pointOnPlane = xyPlaneDiagonal.minus(getPosition());
		double d = -1 * pointOnPlane.dot(CollisionDetectionTools.Z_AXIS);
		Vector4D intersection = CollisionDetectionTools.linePlaneIntersection(ray, origin, CollisionDetectionTools.Z_AXIS, d);
		
		if(intersection != null && intersection.getX() > getPosition().getX()
				&& intersection.getX() < xyPlaneDiagonal.getX() && 
				intersection.getY() > getPosition().getY() &&
				intersection.getY() < xyPlaneDiagonal.getY())
			mask = mask | XY_PLANE;
		
		pointOnPlane = xzPlaneDiagonal.minus(getPosition());
		d = -1 * pointOnPlane.dot(CollisionDetectionTools.Y_AXIS);
		intersection = CollisionDetectionTools.linePlaneIntersection(ray, origin, CollisionDetectionTools.Y_AXIS, d);
		
		if(intersection != null && intersection.getX() > getPosition().getX() &&
				intersection.getX() < xzPlaneDiagonal.getX() &&
				intersection.getZ() > getPosition().getZ() &&
				intersection.getZ() < xzPlaneDiagonal.getZ())
			mask = mask | XZ_PLANE;
		
		pointOnPlane = yzPlaneDiagonal.minus(getPosition());
		d = -1 * pointOnPlane.dot(CollisionDetectionTools.X_AXIS);
		intersection = CollisionDetectionTools.linePlaneIntersection(ray, origin, CollisionDetectionTools.X_AXIS, d);
		
		if(intersection != null && intersection.getY() > getPosition().getY() &&
				intersection.getY() < yzPlaneDiagonal.getY() &&
				intersection.getZ() > getPosition().getZ() &&
				intersection.getZ() < yzPlaneDiagonal.getZ())
			mask = mask | YZ_PLANE;
		
		return mask;
	}

	@Override
	public int getIntersectionAxis(Vector4D ray, Vector4D origin) {
		int mask = 0;
		if(xAxisVolume.intersectRay(ray, origin))
			mask = mask | X_AXIS;
		if(yAxisVolume.intersectRay(ray, origin))
			mask = mask | Y_AXIS;
		if(zAxisVolume.intersectRay(ray, origin))
			mask = mask | Z_AXIS;
		
		return mask;
	}

	public void render(GL gl) {
		// TODO Auto-generated method stub

	}

}
