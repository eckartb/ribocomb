package viewer.graphics;

import javax.media.opengl.*;


/**
 * Renders the model and allows the model to change with time
 * @author Calvin
 *
 */
public interface DynamicRenderableModel extends RenderableModel {
	
	/**
	 * Render the model
	 * @param gl Instance of GL to do rendering
	 * @param time Time to calculate things like model position
	 * and orientation. Use of this parameter is entirely up
	 * to the implementor.
	 */
	public void render(GL gl, long time);
}
