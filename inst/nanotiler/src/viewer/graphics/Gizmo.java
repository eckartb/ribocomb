package viewer.graphics;

import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.media.opengl.GL;


import viewer.display.Display;
import tools3d.Vector4D;

/**
 * A gizmo is an object in 3d space that can be used to
 * manipulate other objects in 3d space
 * @author Calvin
 *
 */
public interface Gizmo extends RenderableModel {
	
	/**
	 * Get the mouse listener that that
	 * translates user commands into interactions
	 * with the gizmo
	 * @return Returns the mouse listener
	 */
	public MouseListener getMouseListener();
	
	/**
	 * Gets the mouse motion listener that
	 * translates user mouse drags and movements
	 * into interactions with the gizmo.
	 * @return
	 */
	public MouseMotionListener getMouseMotionListener();
	
	/**
	 * Render the gizmo
	 */
	public void render(GL gl);
	
	/**
	 * Get the position of the gizmo in 3D space
	 * @return Returns the position of the gizmo
	 * as a vector
	 */
	public Vector4D getPosition();
	
	/**
	 * Set the display for this gizmo
	 * @param d Display
	 */
	public void setDisplay(Display d);
	
}
