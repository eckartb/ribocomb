package viewer.graphics;


/**
 * Any display that uses Gizmos should implement Gizmoable
 * @author Calvin
 *
 */
public interface Gizmoable {
	
	/**
	 * Set the gizmo of the display
	 * @param gizmo Gizmo to use with this display
	 */
	public void setGizmo(Gizmo gizmo);
	
	/**
	 * Get the gizmo used with this display
	 * @return Returns the gizmo used with this display
	 */
	public Gizmo getGizmo();
	
	/**
	 * Is the gizmo enabled?
	 * @return Returns true if the gizmo is enabled or
	 * false if the gizmo is not enabled.
	 */
	public boolean isGizmoEnabled();
	
	/**
	 * Enable or disable the gizmo
	 * @param b True to enable the gizmo or false
	 * to disable the gizmo.
	 */
	public void setGizmoEnabled(boolean b);
}
