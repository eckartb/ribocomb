package viewer.graphics;

import javax.media.opengl.*;
import java.awt.*;

import tools3d.*;

public class Grid implements RenderableModel {
	
	private Dimension size = new Dimension(100, 100);
	private Dimension spacing = new Dimension(20, 20);
	private Color color = new Color(255, 255, 255, 127);
	private Material material = new Material(1.0, 1.0, 1.0, 1.0,
			1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
	
	private Vector4D u;
	private Vector4D v;
	private Vector4D position;
	
	public Grid(Vector4D u, Vector4D v, Vector4D position, Dimension size) {
		super();
		this.u = u;
		this.v = v;
		this.position = position;
		this.size = size;
	}



	public Color getColor() {
		return color;
	}



	public void setColor(Color color) {
		this.color = color;
	}



	



	public Vector4D getU() {
		return u;
	}



	public void setU(Vector4D u) {
		this.u = u;
	}



	public Vector4D getV() {
		return v;
	}



	public void setV(Vector4D v) {
		this.v = v;
	}



	public Dimension getSize() {
		return size;
	}



	public void setSize(Dimension size) {
		this.size = size;
	}



	public Dimension getSpacing() {
		return spacing;
	}



	public void setSpacing(Dimension spacing) {
		this.spacing = spacing;
	}
	
	



	public Vector4D getPosition() {
		return position;
	}
	
	public Material getMaterial() {
		return material;
	}
	
	public void setMaterial(Material material) {
		this.material = material;
	}



	public void setPosition(Vector4D position) {
		this.position = position;
	}



	public void render(GL gl) {
		
		gl.glColor4d(color.getRed() / 255,
				color.getGreen() / 255,
				color.getBlue() / 255,
				color.getAlpha() / 255);
		
		gl.glMaterialfv(GL.GL_FRONT_AND_BACK, GL.GL_AMBIENT, material.getAmbient().buffer());
		gl.glMaterialfv(GL.GL_FRONT_AND_BACK, GL.GL_DIFFUSE, material.getDiffuse().buffer());
		gl.glMaterialfv(GL.GL_FRONT_AND_BACK, GL.GL_SPECULAR, material.getSpecular().buffer());
		gl.glMaterialfv(GL.GL_FRONT_AND_BACK, GL.GL_EMISSION, material.getEmissive().buffer());
		gl.glMaterialf(GL.GL_FRONT_AND_BACK, GL.GL_SHININESS, (float) material.getHighlight());
		
		
		gl.glLineWidth(1.0f);
		
		Vector4D normal = u.cross(v);
		normal.normalize();
		
		gl.glBegin(GL.GL_LINES);
		double width = size.width / 2;
		double height = size.height / 2;
		Vector4D ep1 = new Vector4D(u.getX() * width + position.getX() - height * v.getX(),
				u.getY() * width + position.getY() - height * v.getY(),
				u.getZ() * width + position.getZ() - height * v.getZ(), 0.0);
		Vector4D ep2 = new Vector4D(u.getX() * width * -1 + position.getX() - height * v.getX(),
				u.getY() * width * -1 + position.getY() - height * v.getY(),
				u.getZ() * width * -1 + position.getZ() - height * v.getZ(), 0.0);
		double gridHeight = 0.0;
		Vector4D increment = new Vector4D(v.getX() * spacing.height,
				v.getY() * spacing.height,
				v.getZ() * spacing.height, 0.0);
		
		for(; gridHeight < size.height; gridHeight += spacing.height) {
			gl.glNormal3d(normal.getX(), normal.getY(), normal.getZ());
			gl.glVertex3d(ep1.getX(), ep1.getY(), ep1.getZ());
			gl.glNormal3d(normal.getX(), normal.getY(), normal.getZ());
			gl.glVertex3d(ep2.getX(), ep2.getY(), ep2.getZ());
			
			ep1.add(increment);
			ep2.add(increment);
		}
		
		ep1 = new Vector4D(v.getX() * height + position.getX() - width * u.getX(),
				v.getY() * height + position.getY() - width * u.getY(),
				v.getZ() * height + position.getZ() - width * u.getZ(), 0.0);
		ep2 = new Vector4D(v.getX() * height * -1 + position.getX() - width * u.getX(),
				v.getY() * height * -1 + position.getY() - width * u.getY(),
				v.getZ() * height * -1 + position.getZ() - width * u.getZ(), 0.0);
		gridHeight = 0.0;
		increment = new Vector4D(u.getX() * spacing.width,
				u.getY() * spacing.width,
				u.getZ() * spacing.width, 0.0);
		
		for(; gridHeight < size.width; gridHeight += spacing.width) {
			//gl.glNormal3d(normal.getX(), normal.getY(), normal.getZ());
			gl.glVertex3d(ep1.getX(), ep1.getY(), ep1.getZ());
			//gl.glNormal3d(normal.getX(), normal.getY(), normal.getZ());
			gl.glVertex3d(ep2.getX(), ep2.getY(), ep2.getZ());
			
			ep1.add(increment);
			ep2.add(increment);
		}
		
		gl.glEnd();
	}
}
