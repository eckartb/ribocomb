package viewer.graphics;

/**
 * Any class implementing this interface
 * has a concept of Levels of Detail.
 * @author Calvin
 *
 */
public interface LODCapable {
	
	public static final int LOD_COARSE = 1;
	public static final int LOD_FINE = 3;
	public static final int LOD_MEDIUM = 2;
	
	/**
	 * Get the level of detail for this class
	 * @return Returns an integer describing the level
	 * of detail
	 */
	public int getRefinementLevel();
	
	/**
	 * Set the level of detail for this class
	 * @param refinementLevel Integer describing level of detail
	 */
	public void setRefinementLevel(int refinementLevel);
	
}
