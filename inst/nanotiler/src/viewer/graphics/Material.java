package viewer.graphics;

import tools3d.Vector3D;

import java.awt.Color;
import javax.media.opengl.*;

/**
 * A material allows objects rendered using open gl
 * to have shading.
 * @author Calvin
 *
 */
public class Material implements Cloneable {
	
	private ColorVector ambient;
	private ColorVector diffuse;
	private ColorVector emissive;
	private ColorVector specular;
	
	private double highlight;

	public Material(double ambientR, double ambientG, double ambientB, double ambientI,
			double diffuseR, double diffuseG, double diffuseB, double diffuseI,
			double specularR, double specularG, double specularB, double specularI, 
			double emissiveR, double emissiveG, double emissiveB, double emissiveI,
			double highlight) {
		ambient = new ColorVector(ambientR, ambientG, ambientB, ambientI);
		diffuse = new ColorVector(diffuseR, diffuseG, diffuseB, diffuseI);
		emissive = new ColorVector(emissiveR, emissiveG, emissiveB, emissiveI);
		specular = new ColorVector(specularR, specularG, specularB, specularI);
		this.highlight = highlight;
	}
	
	/**
	 * Construct a material from a color
	 * @param color
	 */
	public Material(Color color) {
		this(color, false);
	}
	
	/**
	 * Construct a material from a color
	 * @param color Color to construct material from
	 * @param matte Should the material be matte, that is,
	 * have no shininess or specular qualities.
	 */
	public Material(Color color, boolean matte) {
		double red = color.getRed() / 255.0;
		double blue = color.getBlue() / 255.0;
		double green = color.getGreen() / 255.0;
		
		setAmbient(red, green, blue, 1.0);
		setDiffuse(red * 0.8, green * 0.8, blue * 0.8, 1.0);
		setSpecular(red * 0.2, green * 0.2, blue * 0.2, 1.0);
		if(!matte)
			setEmissive(0.0, 0.0, 0.0, 0.0);
		else
			setEmissive(red, green, blue, 0.0);
		setHighlight(10.0);
	}
	
	/**
	 * Construct default material
	 *
	 */
	public Material() {
		this(0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0);
	}
	
	/**
	 * Get the ambient component of the material
	 * @return
	 */
	public ColorVector getAmbient() {
		return new ColorVector(ambient);
	}
	
	/**
	 * Get the diffuse component of the material
	 * @return
	 */
	public ColorVector getDiffuse() {
		return new ColorVector(diffuse);
	}
	
	/**
	 * Get the emissive component of the material
	 * @return
	 */
	public ColorVector getEmissive() {
		return new ColorVector(emissive);
	}
	
	/**
	 * Get the specular component of the material
	 * @return
	 */
	public ColorVector getSpecular() {
		return new ColorVector(specular);
	}
	
	/**
	 * Get the highlight of the material
	 * @return
	 */
	public double getHighlight() {
		return highlight;
	}
	
	/**
	 * Set the ambient component of the material
	 * @param r
	 * @param g
	 * @param b
	 * @param i
	 */
	public void setAmbient(double r, double g, double b, double i) {
		ambient = new ColorVector(r, g, b, i);
	}
	
	/**
	 * Set the diffuse component of the material
	 * @param r
	 * @param g
	 * @param b
	 * @param i
	 */
	public void setDiffuse(double r, double g, double b, double i) {
		diffuse = new ColorVector(r, g, b, i);
	}
	
	/**
	 * Set the emissive component of the material
	 * @param r
	 * @param g
	 * @param b
	 * @param i
	 */
	public void setEmissive(double r, double g, double b, double i) {
		emissive = new ColorVector(r, g, b, i);
	}
	
	/**
	 * Set the specular component of the material
	 * @param r
	 * @param g
	 * @param b
	 * @param i
	 */
	public void setSpecular(double r, double g, double b, double i) {
		specular = new ColorVector(r, g, b, i);
	}
	
	
	/**
	 * Set the ambient component of the material
	 * @param ambient
	 */
	public void setAmbient(ColorVector ambient) {
		this.ambient = new ColorVector(ambient);
	}

	/**
	 * Set the diffuse component of the material
	 * @param diffuse
	 */
	public void setDiffuse(ColorVector diffuse) {
		this.diffuse = new ColorVector(diffuse);
	}

	/**
	 * Set the emissive component of the material
	 * @param emissive
	 */
	public void setEmissive(ColorVector emissive) {
		this.emissive = new ColorVector(emissive);
	}

	/**
	 * Set the specular component of the material
	 * @param specular
	 */
	public void setSpecular(ColorVector specular) {
		this.specular = new ColorVector(specular);
	}

	/**
	 * Set the highlight of the material
	 * @param highlight
	 */
	public void setHighlight(double highlight) {
		this.highlight = highlight;
	}
	
	/**
	 * Render the material using open gl.
	 * @param gl
	 * @param material
	 */
	public static void renderMaterial(GL gl, Material material) {
		gl.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, material.getAmbient().buffer());
		gl.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, material.getDiffuse().buffer());
		gl.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, material.getSpecular().buffer());
		gl.glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, material.getEmissive().buffer());
		gl.glMaterialf(GL.GL_FRONT, GL.GL_SHININESS, (float) material.getHighlight());
	}
	
	public Object clone() {
		Material material = null;
		try {
			material = (Material) super.clone();
		} catch (CloneNotSupportedException e) {
			return null;
		}
		
		material.ambient = (ColorVector) ambient.clone();
		material.diffuse = (ColorVector) diffuse.clone();
		material.emissive = (ColorVector) emissive.clone();
		material.specular = (ColorVector) specular.clone();
		
		return material;
	}
	
	
	
}
