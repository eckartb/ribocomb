package viewer.graphics;

import tools3d.Vector4D;

/**
 * A three dimensional mesh
 * @author Calvin
 *
 */
public interface Mesh {
	/**
	 * Get all the vertices of the mesh.
	 * 
	 * A geometry element is an array of indices into the 
	 * vertices and normals array. Using these indices,
	 * geometric primitives can be constructed by getting
	 * the actual vertices and normals from the vertices
	 * and normal arrays (by colling getVertices and getNormals)
	 * @return Returns an array of vertices
	 */
	public Vector4D[] getVertices();
	
	/**
	 * Get all the normals of the mesh
	 * @return Returns an array of normals
	 */
	public Vector4D[] getNormals();
	
	/**
	 * Get all the polygons composing
	 * the mesh.
	 * @return Returns an array of array of polygons
	 */
	public int[][] getGeometryElements();
	
	/**
	 * Get the side count for the polygons. Note
	 * that some implementations of this class
	 * may choose not to support this method
	 * and may throw an unsupported operation exception.
	 * Please see the documentation for extending
	 * classes.
	 * @return Returns the number of sides that each geometry
	 * element "should" have.
	 * @throws UnsupportedOperationException If the side count
	 * is irrelevant to the function of the class
	 */
	public int getPolygonSideCount();
	
	/**
	 * Get the geometry element at the specified index.
	 * @param index Index of geometry element.
	 * @return Returns the geometry element
	 */
	public int[] getGeometryElement(int index);
	
	/**
	 * Get the vertex at the specified index
	 * @param index Index to get the vertix at
	 * @return Returns the vertex
	 */
	public Vector4D getVertex(int index);
	
	/**
	 * Get the normal at the specified index
	 * @param index Index to get the normal at
	 * @return Returns the normal
	 */
	public Vector4D getNormal(int index);
	
	/**
	 * Get the number of geometry elements
	 * @return Returns the number of geometry elements
	 */
	public int getGeometryElementCount();
	
	/**
	 * Get the number of vertices and normals.
	 * @return Returns the number of vertices and normals
	 */
	public int getVertexCount();
}
