package viewer.graphics;

/**
 * Any class that can store a mesh so the mesh
 * doesn't have to be recalculated on the fly for 
 * each rendering pass should implement this class.
 * @author Calvin
 *
 */
public interface MeshCacheable {
	
	/**
	 * Get the cached mesh.
	 * @return Returns the cached mesh.
	 */
	public AdvancedMesh getCachedMesh();
	
	/**
	 * Is a mesh currently cached?
	 * @return Returns true if a mesh is cached
	 * or false if a mesh is not cached
	 */
	public boolean isMeshCached();
	
	/**
	 * Turn on and off mesh caching
	 * @param meshCached True if meshes should
	 * be cached or false if meshes should not
	 * be cached.
	 */
	public void setMeshCached(boolean meshCached);
	
	/**
	 * Update the cached mesh
	 *
	 */
	public void updateMesh();
}
