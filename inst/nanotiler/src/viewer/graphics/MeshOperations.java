package viewer.graphics;

import tools3d.Vector4D;
import tools3d.Vector3D;
import tools3d.splines.StandaloneSplineFactory;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

import viewer.util.Tools;

/**
 * Perform various operations on meshes.
 * @author Calvin
 *
 */
public class MeshOperations {
	
	/**
	 * Same as extend only connects first and last points
	 * @param polygon
	 * @param normal
	 * @param controlPoints
	 * @return
	 */
	public static Mesh extendRing(Vector4D[] polygon, Vector4D normal, Vector3D[] controlPoints) {
//		Vector3D[] vtx = splineFactory.createBezier(controlPoints, segments);
		Vector4D[] points = convert(controlPoints);
		Vector4D[] tangents = computeRingTangents(points);
		
		normal.normalize();
		
		Vector4D polygonCenter = centerOfPolygon(polygon);
		
		Vector4D[] polygonNormals = normalsOfPolygon(polygon);
		
		final ArrayList<Vector4D> vertices = new ArrayList<Vector4D>();
		final ArrayList<Vector4D> normals = new ArrayList<Vector4D>();
		final ArrayList<int[]> triangles = new ArrayList<int[]>();
		
		for(int i = 0; i < polygon.length; ++i)
			polygon[i].sub(polygonCenter);
		
		
		for(int i = 0; i < points.length; ++i) {
			tangents[i].normalize();
			double angle = Math.acos(tangents[i].dot(normal));
			Vector4D axis = normal.cross(tangents[i]);
			axis.normalize();
			for(int j = 0; j < polygon.length; ++j) {
				polygon[j] = Tools.rotate(polygon[j], axis, angle);
				polygonNormals[j] = Tools.rotate(polygonNormals[j], axis, angle);
				vertices.add(polygon[j].plus(points[i]));
				normals.add(polygonNormals[j]);
				
				if(i > 0) {
					int[] t1 = {
							(i - 1) * polygon.length + ((j + 1) % polygon.length),
							(i - 1) * polygon.length + j,
							i * polygon.length + j,		
					};
					
					int[] t2 = {
							i * polygon.length + j,
							i * polygon.length + ((j + 1) % polygon.length),
							(i - 1) * polygon.length + ((j + 1) % polygon.length),
							
					};
					
					triangles.add(t1);
					triangles.add(t2);
				}
				
			}
			
			normal = tangents[i];
			
		}

		for(int j = 0; j < polygon.length; ++j) {
			
			int[] t1 = {
					(points.length - 1) * polygon.length + ((j + 1) % polygon.length),
					(points.length - 1) * polygon.length + j,
					j,					
			};
			
			int[] t2 = {
					j,
					((j + 1) % polygon.length),
					(points.length - 1) * polygon.length + ((j + 1) % polygon.length),
					
			};
			
			
			
			triangles.add(t1);
			triangles.add(t2);
		
		
	}
		
		
		return new DefaultMesh(vertices, normals, triangles, 3);
	}
	
	/**
	 * Generates mesh by extending polygon along the Bezier curve
	 * generated using the control points and segments parameters
	 * @param polygon
	 * @param normal
	 * @param controlPoints
	 * @param segments
	 * @return
	 */
	public static Mesh extend(Vector4D[] polygon, Vector4D normal, Vector3D[] controlPoints) {

		//Vector3D[] vtx = splineFactory.createBezier(controlPoints, segments);
		Vector4D[] points = convert(controlPoints);
		Vector4D[] tangents = computeTangents(points);
		
		normal.normalize();
		
		Vector4D polygonCenter = centerOfPolygon(polygon);
		
		Vector4D[] polygonNormals = normalsOfPolygon(polygon);
		
		final ArrayList<Vector4D> vertices = new ArrayList<Vector4D>();
		final ArrayList<Vector4D> normals = new ArrayList<Vector4D>();
		final ArrayList<int[]> triangles = new ArrayList<int[]>();
		
		for(int i = 0; i < polygon.length; ++i)
			polygon[i].sub(polygonCenter);
		
		
		for(int i = 0; i < points.length; ++i) {
			tangents[i].normalize();
			double angle = Math.acos(tangents[i].dot(normal));
			Vector4D axis = normal.cross(tangents[i]);
			axis.normalize();
			for(int j = 0; j < polygon.length; ++j) {
				polygon[j] = Tools.rotate(polygon[j], axis, angle);
				polygonNormals[j] = Tools.rotate(polygonNormals[j], axis, angle);
				vertices.add(polygon[j].plus(points[i]));
				normals.add(polygonNormals[j]);
				
				if(i > 0) {
					int[] t1 = {
							(i - 1) * polygon.length + ((j + 1) % polygon.length),
							(i - 1) * polygon.length + j,
							i * polygon.length + j,
							
							
							
					};
					
					int[] t2 = {
							i * polygon.length + j,
							i * polygon.length + ((j + 1) % polygon.length),
							(i - 1) * polygon.length + ((j + 1) % polygon.length),
							
					};
					
					
					
					triangles.add(t1);
					triangles.add(t2);
				}
				
			}

			normal = tangents[i];
			
		}
		
		
		return new DefaultMesh(vertices, normals, triangles, 3);
		
	}
	
	/**
	 * Generates mesh by extending polygon along the Bezier curve
	 * generated using the control points and segments parameters
	 * @param polygon
	 * @param normal
	 * @param controlPoints
	 * @param segments
	 * @return
	 */
	public static Mesh extend(Vector4D[] polygon, Vector4D normal, Vector4D orientation, Vector3D[] controlPoints, Vector3D[] orientations) {

		//Vector3D[] vtx = splineFactory.createBezier(controlPoints, segments);
		Vector4D[] points = convert(controlPoints);
		Vector4D[] ups = convert(orientations);
		Vector4D[] tangents = computeTangents(points);
		
		normal.normalize();
		
		Vector4D polygonCenter = centerOfPolygon(polygon);
		
		Vector4D[] polygonNormals = normalsOfPolygon(polygon);
		
		final ArrayList<Vector4D> vertices = new ArrayList<Vector4D>();
		final ArrayList<Vector4D> normals = new ArrayList<Vector4D>();
		final ArrayList<int[]> triangles = new ArrayList<int[]>();
		
		for(int i = 0; i < polygon.length; ++i) {
			polygon[i].sub(polygonCenter);
		}
		
		for(int i = 0; i < points.length; ++i) {
			ups[i].setW(0.0);
			ups[i].normalize();
			double dot = orientation.dot(ups[i]);
			double angle2 = Math.acos(dot <= 1.0 ? dot : 1.0);
			Vector4D orientationAxis = orientation.cross(ups[i]);
			orientationAxis.setW(0.0);
			orientationAxis.normalize();
			for(int j = 0; j < polygon.length; ++j) {
				polygon[j] = Tools.rotate(polygon[j], orientationAxis, angle2);
				polygonNormals[j] = Tools.rotate(polygonNormals[j], orientationAxis, angle2);
				vertices.add(polygon[j].plus(points[i]));
				normals.add(polygonNormals[j]);
				
				if(i > 0) {
					int[] t1 = {
							(i - 1) * polygon.length + ((j + 1) % polygon.length),
							(i - 1) * polygon.length + j,
							i * polygon.length + j,
							
							
							
					};
					
					int[] t2 = {
							i * polygon.length + j,
							i * polygon.length + ((j + 1) % polygon.length),
							(i - 1) * polygon.length + ((j + 1) % polygon.length),
							
					};
					
					
					
					triangles.add(t1);
					triangles.add(t2);
				}
				
			}

			normal = tangents[i];
			orientation = ups[i];
		}
		
		
		return new DefaultMesh(vertices, normals, triangles, 3);
		
	}
	
	private static Vector4D[] computeTangents(Vector4D[] points) {
		Vector4D[] array = new Vector4D[points.length];
		array[0] = new Vector4D(points[1].minus(points[0]));
		array[0].normalize();
		
		for(int i = 1; i < points.length - 1; ++i) {
			Vector4D v = new Vector4D(points[i + 1].minus(points[i - 1]));
			v.normalize();
			array[i] = v;
		}
		
		array[points.length - 1] = new Vector4D(points[points.length - 1].minus(points[points.length - 2]));
		array[points.length - 1].normalize();
		return array;
		
		
	}
	
	private static Vector4D[] computeRingTangents(Vector4D[] points) {
		Vector4D[] array = new Vector4D[points.length];
		array[0] = new Vector4D(points[1].minus(points[points.length - 1]));
		array[0].normalize();
		
		for(int i = 1; i < points.length - 1; ++i) {
			Vector4D v = new Vector4D(points[i + 1].minus(points[i - 1]));
			v.normalize();
			array[i] = v;
		}
		
		array[points.length - 1] = new Vector4D(points[0].minus(points[points.length - 2]));
		array[points.length - 1].normalize();
		return array;
		
	}
	
	private static Vector4D[] convert(Vector3D[] array) {
		Vector4D[] a = new Vector4D[array.length];
		for(int i = 0; i < a.length; ++i) {
			a[i] = new Vector4D(array[i]);
			a[i].setW(0.0);
		}
		
		return a;
	}
	
	private static Vector4D[] normalsOfPolygon(Vector4D[] polygon) {
		Vector4D center = centerOfPolygon(polygon);
		Vector4D[] array = new Vector4D[polygon.length];
		for(int i = 0; i < array.length; ++i) {
			Vector4D normal = polygon[i].minus(center);
			normal.normalize();
			array[i] = normal;
		}
		
		return array;
	}
	
	public static Vector4D centerOfPolygon(Vector4D[] polygon) {
		double x = 0.0;
		double y = 0.0;
		double z = 0.0;
		double w = 0.0;
		for(int i = 0; i < polygon.length; ++i) {
			x += polygon[i].getX();
			y += polygon[i].getY();
			z += polygon[i].getZ();
			w += polygon[i].getW();
		}
		
		return new Vector4D(x / polygon.length, y / polygon.length, z / polygon.length, w / polygon.length);
		
		
	}
	
	
	public static Mesh rotateMesh(final Mesh mesh, Vector4D axis, double angle) {
		final Vector4D[] vertices = mesh.getVertices().clone();
		final Vector4D[] normals = mesh.getNormals().clone();
		
		for(int i = 0; i < vertices.length; ++i) {
			vertices[i] = Tools.rotate(vertices[i], axis, angle);
			normals[i] = Tools.rotate(normals[i], axis, angle);
		}
		
		return new DefaultMesh(Arrays.asList(vertices), Arrays.asList(normals), Arrays.asList(mesh.getGeometryElements().clone()), mesh.getPolygonSideCount());
	}
	
	public static Mesh translateMesh(final Mesh mesh, Vector4D offset) {
		offset.setW(0.0);
		final List<Vector4D> vertices = Arrays.asList(mesh.getVertices());
		for(int i = 0; i < vertices.size(); ++i) {
			vertices.get(i).add(offset);
		}
		
		return mesh;
	}
	
	/**
	 * Synthesize two meshes into one
	 * @param m1
	 * @param m2
	 * @return
	 */
	public static Mesh synthesize(Mesh m1, Mesh m2) {
		boolean resized = false;
		if(m1.getPolygonSideCount() != m2.getPolygonSideCount()) {
			m1 = triple(m1);
			m2 = triple(m2);
			resized = true;
		}
		
		List<Vector4D> vertices = new ArrayList<Vector4D>(Arrays.asList(m1.getVertices()));
		List<Vector4D> normals = new ArrayList<Vector4D>(Arrays.asList(m1.getNormals()));
		List<int[]> polygons = new ArrayList<int[]>(Arrays.asList(m1.getGeometryElements()));
		
		int offset = vertices.size();
		
		vertices.addAll(Arrays.asList(m2.getVertices()));
		normals.addAll(Arrays.asList(m2.getNormals()));
		
		for(int i = 0; i < m2.getGeometryElementCount(); ++i) {
			for(int j = 0; j < m2.getGeometryElement(i).length; ++j) {
				m2.getGeometryElement(i)[j] += offset;
			}
			
			polygons.add(m2.getGeometryElement(i));
		}
		
		return new DefaultMesh(vertices, normals, polygons, resized ? 3 : m1.getPolygonSideCount());
		
	}
	
	/**
	 * Break each polygon in a mesh into triangles. Every polygon is split into
	 * a number of triangles equal to its number of sides.
	 * @param mesh
	 * @return
	 */
	public static Mesh triple(Mesh mesh) {
		List<Vector4D> vertices = new ArrayList<Vector4D>(Arrays.asList(mesh.getVertices()));
		List<Vector4D> normals = new ArrayList<Vector4D>(Arrays.asList(mesh.getNormals()));
		List<int[]> polygons = new ArrayList<int[]>();
		
		for(int[] polygon : mesh.getGeometryElements()) {
			List<Vector4D> polygonVertices = new ArrayList<Vector4D>();
			Vector4D normal = new Vector4D(0.0, 0.0, 0.0, 0.0);
			for(int p : polygon) {
				polygonVertices.add(vertices.get(p));
				normal.add(normals.get(p));
			}
			Vector4D[] v = new Vector4D[polygonVertices.size()];
			polygonVertices.toArray(v);
			
			Vector4D center = centerOfPolygon(v);
			
			vertices.add(center);
			normal.mul(1.0 / polygon.length);
			normal.normalize();
			normals.add(normal);
			
			
			for(int i = 0; i < polygon.length; ++i) {
				int[] triangle = {
					polygon[i], polygon[(i + 1) % polygon.length], vertices.size() - 1	
				};
				
				polygons.add(triangle);
			}
			
		}
		
		return new DefaultMesh(vertices, normals, polygons, 3);
	}
	
}
