package viewer.graphics;

/**
 * Renders a mesh
 * @author Calvin
 *
 */
public interface MeshRenderer extends RenderableModel {
	
	/**
	 * Get a mesh
	 * @return Returns the mesh
	 */
	public Mesh getMesh();
	
	/**
	 * Add a mesh to be rendered
	 * @param m Mesh to add
	 */
	public void addMesh(Mesh m);
	
	/**
	 * Remove a mesh to be rendered
	 * @param m Mesh to remove
	 * @return Returns true if the mesh was
	 * removed or false if the mesh wasn't removed
	 */
	public boolean removeMesh(Mesh m);
}
