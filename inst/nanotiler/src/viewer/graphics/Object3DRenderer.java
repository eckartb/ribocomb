package viewer.graphics;

import javax.media.opengl.GL;
import tools3d.objects3d.Object3D;

/**
 * Render an object3D
 * @author Calvin
 *
 */
public interface Object3DRenderer {
	public void render(Object3D object, GL gl);
}
