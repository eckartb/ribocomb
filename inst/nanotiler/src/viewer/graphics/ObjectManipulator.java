package viewer.graphics;

import tools3d.Vector4D;
import tools3d.BoundingVolume;

/**
 * A graphical representation
 * of a construct that can manipulate
 * objects
 * @author Calvin
 *
 */
public abstract class ObjectManipulator implements RenderableModel {
	
	public static final int X_AXIS = 0x1;
	public static final int Y_AXIS = 0x1 << 1;
	public static final int Z_AXIS = 0x1 << 2;
	
	private boolean xAxisEnabled = true;
	private boolean yAxisEnabled = true;
	private boolean zAxisEnabled = true;
	
	private Vector4D position = null;
	
	private Material xAxisMaterial = null;
	private Material yAxisMaterial = null;
	private Material zAxisMaterial = null;
	
	/**
	 * Gets the axis the ray intersects
	 * @param ray Normalized ray vector
	 * @param origin Origin of the ray
	 * @return Returns an integer mask denoting the axis or 0 if
	 * the ray doesn't intersect with any axis
	 */
	public abstract int getIntersectionAxis(Vector4D ray, Vector4D origin);

	public abstract BoundingVolume getBoundingVolume();
	
	public Vector4D getPosition() {
		return position;
	}

	public void setPosition(Vector4D position) {
		this.position = position;
	}

	public boolean isXAxisEnabled() {
		return xAxisEnabled;
	}

	public void setXAxisEnabled(boolean axisEnabled) {
		xAxisEnabled = axisEnabled;
	}

	public Material getXAxisMaterial() {
		return xAxisMaterial;
	}

	public void setXAxisMaterial(Material axisMaterial) {
		xAxisMaterial = axisMaterial;
	}

	public boolean isYAxisEnabled() {
		return yAxisEnabled;
	}

	public void setYAxisEnabled(boolean axisEnabled) {
		yAxisEnabled = axisEnabled;
	}

	public Material getYAxisMaterial() {
		return yAxisMaterial;
	}

	public void setYAxisMaterial(Material axisMaterial) {
		yAxisMaterial = axisMaterial;
	}

	public boolean isZAxisEnabled() {
		return zAxisEnabled;
	}

	public void setZAxisEnabled(boolean axisEnabled) {
		zAxisEnabled = axisEnabled;
	}

	public Material getZAxisMaterial() {
		return zAxisMaterial;
	}

	public void setZAxisMaterial(Material axisMaterial) {
		zAxisMaterial = axisMaterial;
	}
	
	
	
	
	
}
