package viewer.graphics;

import tools3d.Vector4D;

/**
 * An extenstion to the object manipulator
 * that allows object manipulation on planes
 * in addition to the coordinate axis.
 * @author Calvin
 *
 */
public abstract class ObjectManipulatorPlanes extends ObjectManipulator {
	
	public static final int XY_PLANE = 0x1;
	public static final int XZ_PLANE = 0x1 << 1;
	public static final int YZ_PLANE = 0x1 << 2;
	
	private boolean xyPlaneEnabled = true;
	private boolean xzPlaneEnabled = true;
	private boolean yzPlaneEnabled = true;
	
	private Material xyPlaneMaterial = null;
	private Material xzPlaneMaterial = null;
	private Material yzPlaneMaterial = null;
	
	public abstract int getIntersectionPlane(Vector4D ray, Vector4D origin);

	public boolean isXyPlaneEnabled() {
		return xyPlaneEnabled;
	}

	public void setXyPlaneEnabled(boolean xyPlaneEnabled) {
		this.xyPlaneEnabled = xyPlaneEnabled;
	}

	public Material getXyPlaneMaterial() {
		return xyPlaneMaterial;
	}

	public void setXyPlaneMaterial(Material xyPlaneMaterial) {
		this.xyPlaneMaterial = xyPlaneMaterial;
	}

	public boolean isXzPlaneEnabled() {
		return xzPlaneEnabled;
	}

	public void setXzPlaneEnabled(boolean xzPlaneEnabled) {
		this.xzPlaneEnabled = xzPlaneEnabled;
	}

	public Material getXzPlaneMaterial() {
		return xzPlaneMaterial;
	}

	public void setXzPlaneMaterial(Material xzPlaneMaterial) {
		this.xzPlaneMaterial = xzPlaneMaterial;
	}

	public boolean isYzPlaneEnabled() {
		return yzPlaneEnabled;
	}

	public void setYzPlaneEnabled(boolean yzPlaneEnabled) {
		this.yzPlaneEnabled = yzPlaneEnabled;
	}

	public Material getYzPlaneMaterial() {
		return yzPlaneMaterial;
	}

	public void setYzPlaneMaterial(Material yzPlaneMaterial) {
		this.yzPlaneMaterial = yzPlaneMaterial;
	}
	
	
	
}
