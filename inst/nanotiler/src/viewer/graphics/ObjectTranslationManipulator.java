package viewer.graphics;

import javax.media.opengl.GL;

import tools3d.Vector4D;

/**
 * Manipulator to tanslate an object.
 * @author Calvin
 *
 */
public class ObjectTranslationManipulator extends DefaultObjectManipulator {
	
	private static final Mesh CYLINDER = 
		PrimitiveFactory.generateCylinder(new Vector4D(1.0, 0.0, 0.0, 0.0), 
				ObjectTranslationManipulator.BV_WIDTH / 2, 
				ObjectTranslationManipulator.BV_LENGTH, 
				1, 
				8);
	
	private static final Mesh CONE = 
		PrimitiveFactory.generateCone(new Vector4D(1.0, 0.0, 0.0, 0.0), 
				ObjectTranslationManipulator.BV_WIDTH, 
				ObjectTranslationManipulator.BV_LENGTH / 4, 
				8);
	
	private static final Mesh PLANE = 
		PrimitiveFactory.generateSheet(new Vector4D(0.0, 1.0, 0.0, 0.0), 
				ObjectTranslationManipulator.DIAGONAL_LENGTH / Math.sqrt(2), 
				ObjectTranslationManipulator.DIAGONAL_LENGTH / Math.sqrt(2), 
				1, 
				1);

	
	private static final MeshRenderer cylinderRenderer = new SolidMeshRenderer(CYLINDER);
	private static final MeshRenderer coneRenderer = new SolidMeshRenderer(CONE);
	private static final MeshRenderer planeRenderer = new SolidMeshRenderer(PLANE);
	
	
	public ObjectTranslationManipulator(Vector4D position) {
		super(position);
		// TODO Auto-generated constructor stub
	}



	@Override
	public void render(GL gl) {
		double x = getPosition().getX();
		double y = getPosition().getY();
		double z = getPosition().getZ();
		
		if(isXAxisEnabled()) {
			Material material = getXAxisMaterial();
			Material.renderMaterial(gl, material);
			gl.glPushMatrix();
			gl.glTranslated(x + BV_LENGTH / 2.0, y, z);
			cylinderRenderer.render(gl);
			gl.glTranslated(BV_LENGTH / 2.0, 0.0, 0.0);
			coneRenderer.render(gl);
			gl.glPopMatrix();
		}
		
		if(isYAxisEnabled()) {
			Material material = getYAxisMaterial();
			Material.renderMaterial(gl, material);
			gl.glPushMatrix();
			gl.glTranslated(x, y + BV_LENGTH / 2.0, z);
			gl.glRotated(90.0, 0.0, 0.0, 1.0);
			cylinderRenderer.render(gl);
			gl.glPopMatrix();
			gl.glPushMatrix();
			gl.glTranslated(x, y + BV_LENGTH, z);
			gl.glRotated(90.0, 0.0, 0.0, 1.0);
			coneRenderer.render(gl);
			gl.glPopMatrix();
		}
		
		if(isZAxisEnabled()) {
			Material material = getZAxisMaterial();
			Material.renderMaterial(gl, material);
			gl.glPushMatrix();
			gl.glTranslated(x, y, z + BV_LENGTH / 2.0);
			gl.glRotated(90.0, 0.0, 1.0, 0.0);
			cylinderRenderer.render(gl);
			gl.glPopMatrix();
			gl.glPushMatrix();
			gl.glTranslated(x, y, z + BV_LENGTH);
			gl.glRotated(-90.0, 0.0, 1.0, 0.0);
			coneRenderer.render(gl);
			gl.glPopMatrix();
		}
		
		double hSL = DIAGONAL_LENGTH / Math.sqrt(2) / 2.0;
		
		if(isXyPlaneEnabled()) {
			Material material = getXyPlaneMaterial();
			Material.renderMaterial(gl, material);
			gl.glPushMatrix();
			gl.glTranslated(x + hSL, y + hSL, z);
			gl.glRotated(90.0, 1.0, 0.0, 0.0);
			planeRenderer.render(gl);
			gl.glRotated(180.0, 1.0, 0.0, 0.0);
			planeRenderer.render(gl);
			gl.glPopMatrix();
		}
		
		if(isXzPlaneEnabled()) {
			Material material = getXzPlaneMaterial();
			Material.renderMaterial(gl, material);
			gl.glPushMatrix();
			gl.glTranslated(x + hSL, y, z + hSL);
			planeRenderer.render(gl);
			gl.glRotated(180.0, 0.0, 0.0, 1.0);
			planeRenderer.render(gl);
			gl.glPopMatrix();
		}
		
		if(isYzPlaneEnabled()) {
			Material material = getYzPlaneMaterial();
			Material.renderMaterial(gl, material);
			gl.glPushMatrix();
			gl.glTranslated(x, y + hSL, z + hSL);
			gl.glRotated(90.0, 0.0, 0.0, 1.0);
			planeRenderer.render(gl);
			gl.glRotated(180.0, 0.0, 0.0, 1.0);
			planeRenderer.render(gl);
			gl.glPopMatrix();
			
		}
	}
	
	
	
}
