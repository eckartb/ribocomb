package viewer.graphics;

import java.awt.Color;

import tools3d.Vector4D;

/**
 * @deprecated
 * @author Calvin
 *
 */
public interface OldMesh {
	
	public static class Quad {
		public Vector4D v1, v2, v3, v4, v1n, v2n, v3n, v4n;
	}
	
	public static class Triangle {
		public Vector4D v1, v2, v3, v1n, v2n, v3n;
	}
	
	public static class Line {
		public Vector4D v1, v2;
	}

	public void refine();
	
	public boolean renderWireframe();
	public boolean renderSolid();
	
	public Color getSolidColor();
	public Color getWireframeColor();
	
	public Quad[] getQuads();
	public Line[] getLines();
}
