package viewer.graphics;

import java.util.*;
import tools3d.*;
import viewer.util.Tools;

/**
 * Generates meshes of primitives
 * @author Calvin
 *
 */
public class PrimitiveFactory {
	
	public static final Mesh UNIT_SPHERE;
	public static final Mesh UNIT_BOX;
	public static final Mesh UNIT_CYLINDER;
	
	
	static {
		UNIT_SPHERE = generateSphere(1.0, 4, 4);
		UNIT_BOX = null;
		UNIT_CYLINDER = generateCylinder(new Vector4D(1.0, 0.0, 0.0, 0.0), 
				1.0, 2.0, 1, 8);
	}
	
	/**
	 * Generates a sphere mesh
	 * @param radius Radius of sphere
	 * @param subdivisionsU Theta subdivisions (spherical coordinates)
	 * @param subdivisionsV Phi subdivisions (spherical coordinates)
	 * @return Returns mesh of a sphere centered at the origin
	 */
	public static Mesh generateSphere(double radius, int subdivisionsU, int subdivisionsV) {
		final ArrayList<Vector4D> vertices = new ArrayList<Vector4D>();
		final ArrayList<Vector4D> normals = new ArrayList<Vector4D>();
		final ArrayList<int[]> triangles = new ArrayList<int[]>();
		
		Vector4D v = new Vector4D(0.0, -radius, 0.0, 0.0);
		Vector4D vN = new Vector4D(0.0, 0.0, -1.0, 0.0);
		final Vector4D y = new Vector4D(0.0, -1.0, 0.0, 0.0);
		
		double thetaIncrement = Math.PI * 2 / subdivisionsU;
		double phiIncrement = Math.PI / subdivisionsV;
		
		int thetaIncrementCount = 0;
		int phiIncrementCount = 1;
		
		vertices.add(new Vector4D(v));
		Vector4D n = new Vector4D(v);
		n.normalize();
		normals.add(n);
		
		while(phiIncrementCount < subdivisionsV) {
			//rotate up by phi
			Vector4D vX = v.cross(vN);
			vX.normalize();
			
			v = Tools.rotate(v, vX, phiIncrement);
			vN = Tools.rotate(vN, vX, phiIncrement);
			
			// rotate by half theta 
			v = Tools.rotate(v, y, thetaIncrement / 2.0);
			vN = Tools.rotate(vN,  y, thetaIncrement / 2.0);
			
			while(thetaIncrementCount < subdivisionsU) {
				
				vertices.add(new Vector4D(v));
				n = new Vector4D(v);
				n.normalize();
				normals.add(n);
				
				// rotate by theta
				v = Tools.rotate(v, y, thetaIncrement);
				vN = Tools.rotate(vN,  y, thetaIncrement);
				
				
				++thetaIncrementCount;
			}

			
			
			++phiIncrementCount;
			thetaIncrementCount = 0;
		}
		
		v = new Vector4D(0.0, radius, 0.0, 0.0);
		vertices.add(new Vector4D(v));
		v.normalize();
		normals.add(v);
		
		
		// triangles for the bottom
		for(int i = 0; i < subdivisionsU - 1; ++i) {
			int[] triangle = { 0, i + 1, i + 2 };
			triangles.add(triangle);
		}
		
		int[] extraBottom = {0, subdivisionsU, 1 };
		triangles.add(extraBottom);

		for(int i = 0; i < subdivisionsV - 2; ++i) {
			for(int j = 0; j < subdivisionsU; ++j) {
				triangles.add(findClosestPoints(vertices, 1 + j + i * subdivisionsU, (i + 1) * subdivisionsU + 1, subdivisionsU, false));
			}
		}
		
		for(int i = 1; i < subdivisionsV - 1; ++i) {
			for(int j = 0; j < subdivisionsU; ++j) {
				triangles.add(findClosestPoints(vertices, 1 + j + i * subdivisionsU, (i - 1) * subdivisionsU + 1, subdivisionsU, true));
			}
		}
		
		//triangles for the top
		for(int i = vertices.size() - subdivisionsU - 1; i < vertices.size() - 1; ++i) {
			int[] triangle = { vertices.size() - 1, i + 1, i  };
			triangles.add(triangle);
		}
		
		int[] extraTop = { vertices.size() - 1, vertices.size() - subdivisionsU - 1,  vertices.size() - 2 };
		triangles.add(extraTop);
		
		return new Mesh() {

			public Vector4D getNormal(int index) {
				return normals.get(index);
			}

			public Vector4D[] getNormals() {
				Vector4D[] array = new Vector4D[normals.size()];
				normals.toArray(array);
				return array;
			}

			public int[] getGeometryElement(int index) {
				return triangles.get(index);
			}

			public int getGeometryElementCount() {
				return triangles.size();
			}

			public int[][] getGeometryElements() {
				int[][] array = new int[triangles.size()][];
				triangles.toArray(array);
				return array;
			}

			public Vector4D getVertex(int index) {
				return vertices.get(index);
			}

			public int getVertexCount() {
				return vertices.size();
			}

			public Vector4D[] getVertices() {
				Vector4D[] array = new Vector4D[vertices.size()];
				vertices.toArray(array);
				return array;
			}
			
			public int getPolygonSideCount() {
				return 3;
			}
			
		};
	}
	
	private static int[] findClosestPoints(List<Vector4D> vertices, int point, int offset, int length, boolean rotate) {
		int index1 = -1;
		int index2 = -1;
		double smallestDistance = Double.MAX_VALUE;
		Vector4D v = vertices.get(point);
		for(int i = 0; i < length; ++i) {
			int i1 = offset + i;
			int i2 = offset + ((i + 1) % length);
			
			Vector4D p1 = vertices.get(i1);
			Vector4D p2 = vertices.get(i2);
			double distance = v.distance(p1) + v.distance(p2);
			if(distance < smallestDistance) {
				index1 = i1;
				index2 = i2;
				smallestDistance = distance;
			}
			
		}
		int ret[] = new int[3];
		ret[0] = point;
		if(rotate) {
			ret[1] = index2;
			ret[2] = index1;
		} else {
			ret[1] = index1;
			ret[2] = index2;
		}
		
		return ret;
	}
	
	/**
	 * Generates a cylinder mesh
	 * @param axis Normalized vector for cylinder axis
	 * @param radius
	 * @param length
	 * @param subdivisionsU Cross sections
	 * @param subdivisionsV Rotations
	 * @return
	 */
	public static Mesh generateCylinder(Vector4D axis, double radius, double length, int subdivisionsU, int subdivisionsV) {
		final ArrayList<Vector4D> vertices = new ArrayList<Vector4D>();
		final ArrayList<Vector4D> normals = new ArrayList<Vector4D>();
		final ArrayList<int[]> polygons = new ArrayList<int[]>();
		
		Vector4D normal = new Vector4D(axis);
		double x = Math.abs(normal.getX());
		double y = Math.abs(normal.getY());
		double z = Math.abs(normal.getZ());
		if(x < y && x < z) {
			normal.setX(0.0);
			double yy = normal.getY();
			normal.setY(-1 * normal.getZ());
			normal.setZ(yy);
		}
		
		else if(y < x && y < z) {
			normal.setY(0.0);
			double xx = normal.getX();
			normal.setX(-1 * normal.getZ());
			normal.setZ(xx);
		}
		
		else {
			normal.setZ(0.0);
			double xx = normal.getX();
			normal.setX(-1 * normal.getY());
			normal.setY(xx);
		}
		
		normal.normalize();
		normal = normal.mul(radius);
		normal.setW(0.0);
		
		// generate the middle vertices
		Vector4D increment = axis.mul(length / subdivisionsU);
		double uCounter = 0;
		Vector4D start = axis.mul(length / 2.0);
		double thetaIncrement = Math.PI * 2 / subdivisionsV;
		while(uCounter < subdivisionsU + 1) {
			double vCounter = 0;
			while(vCounter < subdivisionsV) {
				
				vertices.add(start.plus(normal));
				Vector4D n = new Vector4D(normal);
				n.normalize();
				normals.add(n);
				
				normal = Tools.rotate(normal, axis, thetaIncrement);
				
				++vCounter;
			}
			start.sub(increment);
			++uCounter;
		}
		
		// generate middle quads
		for(int i = 0; i < subdivisionsU; ++i) {
			for(int j = 0; j < subdivisionsV; ++j) {
				int[] tri1 = {
						i * subdivisionsV + j,
						(i + 1) * subdivisionsV + j,
						(i + 1) * subdivisionsV + ((j + 1) % subdivisionsV),
				};
				
				int[] tri2 = {
						
						i * subdivisionsV + j,
						(i + 1) * subdivisionsV + ((j + 1) % subdivisionsV),
						i * subdivisionsV + ((j + 1) % subdivisionsV),
						
				};
				polygons.add(tri1);
				polygons.add(tri2);
			}
		}
		
		// generate end vertices
		Vector4D startEnd = axis.mul(length / 2);
		vertices.add(startEnd);
		normals.add(new Vector4D(axis));
		
		Vector4D endEnd = startEnd.mul(-1);
		vertices.add(endEnd);
		normals.add(new Vector4D(axis.mul(-1)));
		
		for(int i = 0; i < subdivisionsV; ++i) {
			int[] quad1 = { vertices.size() - 2, vertices.size() - 2,
				i, (i + 1) % subdivisionsV	
			};
			
			int[] tri1a = {
					vertices.size() - 2, vertices.size() - 2,
					i,
			};
			
			int[] tri1b = {
					vertices.size() - 2,
					i, (i + 1) % subdivisionsV
			};
			
			int[] quad2 = { vertices.size() - 1, vertices.size() - 1,
					subdivisionsU * (subdivisionsV) + ((i + 1) % subdivisionsV),
					subdivisionsU * (subdivisionsV) + i
					
			};
			
			int[] tri2a = {
					vertices.size() - 1, vertices.size() - 1,
					subdivisionsU * (subdivisionsV) + ((i + 1) % subdivisionsV),
			};
			
			int[] tri2b = {
					vertices.size() - 1,
					subdivisionsU * (subdivisionsV) + ((i + 1) % subdivisionsV),
					subdivisionsU * (subdivisionsV) + i
			};
			
			polygons.add(tri1a);
			polygons.add(tri1b);
			polygons.add(tri2a);
			polygons.add(tri2b);
		}
		
		
		return new Mesh() {

			public Vector4D getNormal(int index) {
				return normals.get(index);
			}

			public Vector4D[] getNormals() {
				Vector4D[] array = new Vector4D[normals.size()];
				normals.toArray(array);
				return array;
			}

			public int[] getGeometryElement(int index) {
				return polygons.get(index);
			}

			public int getGeometryElementCount() {
				return polygons.size();
			}

			public int[][] getGeometryElements() {
				int[][] array = new int[polygons.size()][];
				polygons.toArray(array);
				return array;
			}

			public int getPolygonSideCount() {
				return 3;
			}

			public Vector4D getVertex(int index) {
				return vertices.get(index);
			}

			public int getVertexCount() {
				return vertices.size();
			}

			public Vector4D[] getVertices() {
				Vector4D[] array = new Vector4D[vertices.size()];
				vertices.toArray(array);
				return array;
			}
			
		};
	}
	
	/**
	 * Generates a casule mesh
	 * @param radius Radius of the capsule
	 * @param length Length of capsule cylinder part (actual
	 * length of capsule is length + 2 * radius)
	 * @param subdivisionsU Cross sections
	 * @param subdivisionsV Rotations
	 * @return
	 */
	public static Mesh generateCapsule(double radius, double length, int subdivisionsU, int subdivisionsV) {
		return null;
	}
	
	public static Mesh generateSheet(final Vector4D normal, double uLength, double vLength, int subdivisionsU, int subdivisionsV) {
		final ArrayList<Vector4D> vertices = new ArrayList<Vector4D>();
		final ArrayList<int[]> polygons = new ArrayList<int[]>();
		
		normal.normalize();
		
		Vector4D u = Tools.orthogonal(normal);
		Vector4D v = normal.cross(u);
		
		u.normalize();
		v.normalize();
		
		
		double uIncrement = uLength / subdivisionsU;
		double vIncrement = vLength / subdivisionsV;
		
		
		Vector4D start = u.mul(uLength / 2 * -1).plus(v.mul(vLength / 2 * -1));
		
		
		int uCounter = 0;
		while(uCounter < subdivisionsU + 1) {
			int vCounter = 0;
			Vector4D uu = u.mul(uCounter * uIncrement);
			while(vCounter < subdivisionsV + 1) {
				Vector4D vv = v.mul(vCounter * vIncrement);
				Vector4D vertex = start.plus(vv.plus(uu));
				vertices.add(vertex);
				++vCounter;
			}
			
			++uCounter;
		}
		
		uCounter = 0;
		while(uCounter < subdivisionsU) {
			int vCounter = 0;
			while(vCounter < subdivisionsV) {
				int subV = subdivisionsV + 1;
				int[] array = {
					(uCounter * subV) + vCounter,
					((uCounter + 1) * subV) + vCounter,
					((uCounter + 1) * subV) + vCounter + 1,
					(uCounter * subV) + vCounter + 1
					
					
				};
				polygons.add(array);
				
				++vCounter;
			}
			
			++uCounter;
		}
		
		
		
		return new Mesh() {

			public Vector4D getNormal(int index) {
				return normal;
			}

			public Vector4D[] getNormals() {
				Vector4D[] normals = { normal };
				return normals;
			}

			public int[] getGeometryElement(int index) {
				return polygons.get(index);
			}

			public int getGeometryElementCount() {
				return polygons.size();
			}

			public int[][] getGeometryElements() {
				int[][] array = new int[polygons.size()][];
				polygons.toArray(array);
				return array;
			}

			public int getPolygonSideCount() {
				return 4;
			}

			public Vector4D getVertex(int index) {
				return vertices.get(index);
			}

			public int getVertexCount() {
				return vertices.size();
			}

			public Vector4D[] getVertices() {
				Vector4D[] array = new Vector4D[vertices.size()];
				vertices.toArray(array);
				return array;
			}
			
		};
		
	}
	
	public static Mesh generateCone(Vector4D axis, double baseRadius, double height, int subdivisionsU) {
		final ArrayList<Vector4D> vertices = new ArrayList<Vector4D>();
		final ArrayList<Vector4D> normals = new ArrayList<Vector4D>();
		final ArrayList<int[]> triangles = new ArrayList<int[]>();
		
		Vector4D normal = Tools.orthogonal(axis);
		normal.normalize();
		
		axis.normalize();
		
		double incs = Math.PI * 2 / subdivisionsU;
		int counter = 0;
		while(counter < subdivisionsU) {
			Vector4D vertex = normal.mul(baseRadius);
			vertices.add(vertex);
			normals.add(normal);
			
			normal = Tools.rotate(normal, axis, incs);
			
			++counter;
		}
		
		vertices.add(new Vector4D(0.0, 0.0, 0.0, 0.0));
		normals.add(axis.mul(-1));
		
		vertices.add(axis.mul(height));
		normals.add(axis);
		
		int topIndex = normals.size() - 1;
		int bottomIndex = normals.size() - 2;
		
		for(int i = 0; i < subdivisionsU + 1; ++i) {
			int[] triangle1 = {
				(i + 1) % subdivisionsU, topIndex, i
			};
			
			int[] triangle2 = {
				i, bottomIndex, (i + 1) % subdivisionsU
			};
			
			triangles.add(triangle1);
			triangles.add(triangle2);
		}
		
		
		return new Mesh() {

			public Vector4D getNormal(int index) {
				return normals.get(index);
			}

			public Vector4D[] getNormals() {
				Vector4D[] array = new Vector4D[normals.size()];
				normals.toArray(array);
				return array;
			}

			public int[] getGeometryElement(int index) {
				return triangles.get(index);
			}

			public int getGeometryElementCount() {
				return triangles.size();
			}

			public int[][] getGeometryElements() {
				int[][] array = new int[triangles.size()][];
				triangles.toArray(array);
				return array;
			}

			public int getPolygonSideCount() {
				return 3;
			}

			public Vector4D getVertex(int index) {
				return vertices.get(index);
			}

			public int getVertexCount() {
				return vertices.size();
			}

			public Vector4D[] getVertices() {
				Vector4D[] array = new Vector4D[vertices.size()];
				vertices.toArray(array);
				return array;
			}
			
		};
	}
	
	public static Mesh generateBox(double xLength, double yLength, double zLength) {
		return null;
	}
	
	public static Mesh generateTorus(double innerRadius, double outerRadius, int subdivisionsU, int subdivisionsV) {
		return null;
	}
	
	/**
	 * Generates unit polygons (radius of 1) on the XZ plane
	 * @param sides
	 * @return
	 */
	public static Mesh generatePolygon(int sides) {
		final ArrayList<Vector4D> vertices = 
			new ArrayList<Vector4D>();
		
		final ArrayList<int[]> triangles = 
			new ArrayList<int[]>();
			
		Vector4D v = new Vector4D(1.0, 0.0, 0.0, 0.0);
		double angle = Math.PI * 2.0 / sides;
		for(int i = 0; i < sides; ++i) {
			vertices.add(v);
			v = Tools.rotate(v, Tools.Y_AXIS, angle);
			int[] triangle = {
				i, (i + 1) % sides, sides	
			};
			triangles.add(triangle);
		}
		
		vertices.add(new Vector4D(0.0, 0.0, 0.0, 0.0));
		
		ArrayList<Vector4D> normals = new ArrayList<Vector4D>();
		for(int i = 0; i < vertices.size(); ++i) {
			normals.add(new Vector4D(0.0, 1.0, 0.0, 0.0));
		}
		
		return new DefaultMesh(vertices, normals, triangles, 3);
		
	}
	
	
	
	

}
