package viewer.graphics;

import java.util.Collection;
import java.util.List;

/**
 * Collects renderables.
 * @author Calvin
 *
 */
public interface RenderableAggregator {
	/**
	 * Get list of renderables
	 * @return
	 */
	public List<RenderableModel> getRenderables();
	public void addRenderable(Collection<RenderableModel> models);
	public void addRenderable(RenderableModel model);
	public boolean removeRenderable(RenderableModel model);
	public void removeAllRenderables();
}
