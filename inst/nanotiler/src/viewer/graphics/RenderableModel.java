package viewer.graphics;

import javax.media.opengl.*;

/**
 * Any object that can be rendered using opengl should
 * impliment this method
 * @author Calvin
 *
 */
public interface RenderableModel {
	
	public void render(GL gl);
	
}
