package viewer.graphics;

public interface Solid {
	
	public double getScale();
	
	public void setCrossSectionsX(int x);
	public void setCrossSectionsY(int y);
}
