package viewer.graphics;

import javax.media.opengl.GL;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import tools3d.Vector4D;
import java.util.ArrayList;

/**
 * Renders a solid mesh
 * @author Calvin
 *
 */
public class SolidMeshRenderer implements MeshRenderer {
	
	private Mesh mesh;
	
	private ArrayList<Mesh> meshes = new ArrayList<Mesh>();
	

	
	public void addMesh(Mesh m) {
		meshes.add(m);
	}
	
	public boolean removeMesh(Mesh m) {
		return meshes.remove(m);
	}
	
	public SolidMeshRenderer() {
		
	}
	
	public SolidMeshRenderer(Mesh mesh) {
		meshes.add(mesh);
		
		
	}

	public void render(GL gl) {
		
		for (Mesh mesh : meshes) {
			renderMesh(gl, mesh);
		}
	}
	
	protected void renderMesh(GL gl, Mesh mesh) {
		if (mesh.getPolygonSideCount() == 3) {
			gl.glBegin(GL.GL_TRIANGLES);
		} else if (mesh.getPolygonSideCount() == 4) {
			gl.glBegin(GL.GL_QUADS);
		} else {
			gl.glBegin(GL.GL_POLYGON);
		}
		for (int i = 0; i < mesh.getGeometryElementCount(); ++i) {
			int[] triangle = mesh.getGeometryElement(i);
			for (int j = 0; j < triangle.length; ++j) {
				Vector4D p = mesh.getNormal(triangle[j]);
				gl.glNormal3d(p.getX(), p.getY(), p.getZ());
				p = mesh.getVertex(triangle[j]);
				gl.glVertex3d(p.getX(), p.getY(), p.getZ());

			}
		}
		gl.glEnd();
	}
	
	public Mesh getMesh() {
		return mesh;
	}

}
