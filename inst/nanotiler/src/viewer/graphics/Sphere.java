package viewer.graphics;

import java.awt.*;
import java.util.List;
import java.util.ArrayList;

import tools3d.*;
import viewer.display.*;
import viewer.util.Tools;

public class Sphere implements OldMesh {
	
	private double scale;
	private int crossSectionsX;
	private int crossSectionsY;
	
	private Color solidColor = Color.red;
	private Color wireframeColor = Color.blue;
	
	private Line[] lines;
	private Quad[] quads;
	
	private boolean renderWireFrame = false;
	private boolean renderSolid = true;
	
	public Sphere(double scale, int crossSectionsX, int crossSectionsY) {
		this.scale = scale;
		this.crossSectionsX = crossSectionsX;
		this.crossSectionsY = crossSectionsY;
	}
	
	public void refine() {
		crossSectionsX *= 2;
		crossSectionsY *= 2;
		quads = null;
		lines = null;
		
	}
	
	public void setSolidColor(Color solidColor) {
		this.solidColor = solidColor;
	}
	
	public void setWireframeColor(Color wireframeColor) {
		this.wireframeColor = wireframeColor;
	}
	
	public boolean renderWireFrame() {
		return renderWireFrame;
	}
	
	public void renderWireFrame(boolean renderWireFrame) {
		this.renderWireFrame = renderWireFrame;
	}
	
	public boolean renderSolid() {
		return renderSolid;
	}
	
	public void renderSolid(boolean renderSolid) {
		this.renderSolid = renderSolid;
	}
	
	public Line[] getLines() {
		if(lines != null) 
			return lines;
		
		List<Line> lines = new ArrayList<Line>();
		compute(null, lines);
		Line[] l = new Line[lines.size()];
		lines.toArray(l);
		this.lines = l;
		return l;
	}

	public Quad[] getQuads() {
		if(quads != null)
			return quads;
		
		List<Quad> quads = new ArrayList<Quad>();
		compute(quads, null);
		Quad[] q = new Quad[quads.size()];
		quads.toArray(q);
		this.quads = q;
		return q;
	}

	public Color getSolidColor() {
		return solidColor;
	}

	public Color getWireframeColor() {
		return wireframeColor;
	}

	public boolean renderWireframe() {
		return renderWireFrame;
	}
	
	private void compute(List<OldMesh.Quad> quads, List<OldMesh.Line> lines) {
		Vector4D v = new Vector4D(0.0, -scale, 0.0, 0.0);
		Vector4D vN = new Vector4D(0.0, 0.0, -1.0, 0.0);
		final Vector4D y = new Vector4D(0.0, -1.0, 0.0, 0.0);
		
		double thetaIncrement = Math.PI * 2 / crossSectionsX;
		double phiIncrement = Math.PI / crossSectionsY;
		
		
		
		int thetaIncrementCount = 0;
		int phiIncrementCount = 0;
		
		while(phiIncrementCount < crossSectionsY) {
			while(thetaIncrementCount < crossSectionsX) {
				// draw first vertex at v
				Vector4D v1 = v;
				Vector4D v1N = new Vector4D(v1);
				v1N.normalize();
				
				
				// rotate up by phi
				Vector4D vX = v.cross(vN);
				vX.normalize();
				
				
				
				v = Tools.rotate(v, vX, phiIncrement);
				vN = Tools.rotate(vN, vX, phiIncrement);
				Vector4D v2 = v;
				Vector4D v2N = new Vector4D(v2);
				v2N.normalize();
				
				
				// rotate by theta
				v = Tools.rotate(v, y, thetaIncrement);
				vN = Tools.rotate(vN,  y, thetaIncrement);
				Vector4D v3 = v;
				Vector4D v3N = new Vector4D(v3);
				v3N.normalize();
				
				// rotate down by phi
				vX = v.cross(vN);
				vX.normalize();
				
				v = Tools.rotate(v, vX, -phiIncrement);
				vN = Tools.rotate(vN, vX, -phiIncrement);
				Vector4D v4 = v;
				Vector4D v4N = new Vector4D(v4);
				v4N.normalize();
				
				if(quads != null) {
					Quad q = new Quad();
					q.v1 = v1;
					q.v2 = v2;
					q.v3 = v3;
					q.v4 = v4;
					q.v1n = v1N;
					q.v2n = v2N;
					q.v3n = v3N;
					q.v4n = v4N;

					quads.add(q);

				}
				
				
				if(lines != null) {
					Line l = new Line();
					l.v1 = v1;
					l.v2 = v2;
					lines.add(l);

					l = new Line();
					l.v1 = v2;
					l.v2 = v3;
					lines.add(l);

					l = new Line();
					l.v1 = v3;
					l.v2 = v4;
					lines.add(l);

					l = new Line();
					l.v1 = v4;
					l.v2 = v1;
					lines.add(l);
				}
				
				
				++thetaIncrementCount;
			}
			

			
			// rotate up by phi
			Vector4D vX = v.cross(vN);
			vX.normalize();
			v = Tools.rotate(v, vX, phiIncrement);
			vN = Tools.rotate(vN, vX, phiIncrement);
			
			
			++phiIncrementCount;
			thetaIncrementCount = 0;
		}
	}

	
}
