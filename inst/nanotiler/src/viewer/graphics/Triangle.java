package viewer.graphics;

import tools3d.*;
import java.awt.Color;

public class Triangle {
	
	
	
	private Vector4D vertex1, normal1;
	private Vector4D vertex2, normal2;
	private Vector4D vertex3, normal3;
	
	private Material material;
	
	private Color color;

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public Vector4D getNormal1() {
		return normal1;
	}

	public void setNormal1(Vector4D normal1) {
		this.normal1 = normal1;
	}

	public Vector4D getNormal2() {
		return normal2;
	}

	public void setNormal2(Vector4D normal2) {
		this.normal2 = normal2;
	}

	public Vector4D getNormal3() {
		return normal3;
	}

	public void setNormal3(Vector4D normal3) {
		this.normal3 = normal3;
	}

	public Vector4D getVertex1() {
		return vertex1;
	}

	public void setVertex1(Vector4D vertex1) {
		this.vertex1 = vertex1;
	}

	public Vector4D getVertex2() {
		return vertex2;
	}

	public void setVertex2(Vector4D vertex2) {
		this.vertex2 = vertex2;
	}

	public Vector4D getVertex3() {
		return vertex3;
	}

	public void setVertex3(Vector4D vertex3) {
		this.vertex3 = vertex3;
	}
	
	
}
