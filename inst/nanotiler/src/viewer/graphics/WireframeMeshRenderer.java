package viewer.graphics;

import javax.media.opengl.GL;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.ArrayList;

import tools3d.Vector4D;

/**
 * Renders a wireframe mesh
 * @author Calvin
 */
public class WireframeMeshRenderer implements MeshRenderer {

	private Mesh mesh;
	
	private LinkedList<Vector4D> vertexArray;
	private LinkedList<Vector4D> normalArray;
	private LinkedList<Material> materialArray;
	
	private ArrayList<Mesh> meshes = new ArrayList<Mesh>();
	
	public void addMesh(Mesh m) {
		meshes.add(m);
	}
	
	public boolean removeMesh(Mesh m) {
		return meshes.remove(m);
	}
	
	public WireframeMeshRenderer(Mesh mesh) {
		meshes.add(mesh);
	}
	
	public WireframeMeshRenderer() {
		
	}
	
	public void render(GL gl) {
		for (Mesh mesh : meshes) {
			renderMesh(gl, mesh);
			
		}		

	}
	
	protected void renderMesh(GL gl, Mesh mesh) {
		vertexArray = new LinkedList<Vector4D>();
		normalArray = new LinkedList<Vector4D>();
	
		
		int[][] elements = mesh.getGeometryElements();
		for(int i = 0; i < elements.length; ++i) {
			int[] triangle = elements[i];
			for(int j = 0; j < triangle.length; ++j) {

				vertexArray.add(mesh.getVertex(triangle[j]));
				vertexArray.add(mesh.getVertex(triangle[(j + 1) % triangle.length]));
				normalArray.add(mesh.getNormal(triangle[j]));
				normalArray.add(mesh.getNormal(triangle[(j + 1) % triangle.length]));
			}
		}
		
		
		gl.glBegin(GL.GL_LINES);
		Iterator<Vector4D> vertexIterator = vertexArray.iterator();
		Iterator<Vector4D> normalIterator = normalArray.iterator();
		while (vertexIterator.hasNext()) {
			Vector4D v = vertexIterator.next();
			Vector4D n = normalIterator.next();

			gl.glNormal3d(n.getX(), n.getY(), n.getZ());
			gl.glVertex3d(v.getX(), v.getY(), v.getZ());
		}
		gl.glEnd();
	}
	
	public Mesh getMesh() {
		return mesh;
	}

}
