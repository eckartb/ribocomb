package viewer.rnadesign;

import rnadesign.rnamodel.Atom3D;
import rnadesign.rnamodel.BranchDescriptor3D;
import rnadesign.rnamodel.KissingLoop3D;
import rnadesign.rnamodel.Nucleotide3D;
import rnadesign.rnamodel.RnaStem3D;
import rnadesign.rnamodel.RnaStrand;
import rnadesign.rnamodel.StrandJunction3D;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Link;
import viewer.graphics.Material;
import viewer.graphics.ColorPicker;
import viewer.graphics.ColorVector;

import java.awt.Color;
import java.util.Map;
import java.util.HashMap;

import viewer.util.Pair;

/**
 * For each type of object, the color model will
 * return a slightly different color for each subsequent
 * call
 * @author Calvin
 *
 */
public class RainbowColorModel extends RNAColorModel {
        /*rainbow goes from color COLOR_PAIRS[0][0] to [0][1] to [1][1] to ... ends at color [length][1]*/ 
        private final static Color[][] COLOR_PAIRS = {
		{ Color.red, Color.orange },
		{ Color.orange, Color.yellow },
		{ Color.yellow, Color.green },
		{ Color.green, Color.blue } 
	};
	
	private int pairIndexer = 0;
	
	private Map<Object3D, Material> nMap = 
		new HashMap<Object3D, Material>();
	
	private Map<Object3D, Pair<Color, Color>> sMap =
		new HashMap<Object3D, Pair<Color, Color>>();

	
	public Material getDefaultMaterial() {
		Material m = super.getDefaultMaterial();
		m.getAmbient().setAlpha(1.0);
		return m;
	}
	
	public Material getMaterial(Link link) {
		return getDefaultMaterial();
	}

	@Override
	public Material getAtomMaterial(Atom3D atom) {
		Nucleotide3D n = getNucleotide(atom);
		if(n == null)
			return getDefaultMaterial();
		
		return getNucleotideMaterial(n);
		
	}

	@Override
	public Material getBranchDescriptorMaterial(BranchDescriptor3D b) {
		return getDefaultMaterial();
	}

	@Override
	public Material getKissingLoopMaterial(KissingLoop3D k) {
		return getDefaultMaterial();
	}

	@Override
	public Material getNucleotideMaterial(Nucleotide3D n) {
	    System.out.println("Rainbow Color Model getNucleotideMaterial() called");
		if(nMap.containsKey(n))
			return nMap.get(n);
		
		RnaStrand s;
		if(n.getParent() instanceof RnaStrand)
			s = (RnaStrand) n.getParent();
		else
			return getDefaultMaterial();


		
		Color color1, color2;

		if(sMap.containsKey(s)) {
			Pair<Color, Color> p = sMap.get(s);
			color1 = p.getFirst();
			color2 = p.getSecond();
		} 

		int size = s.size();
		int index = s.getIndexOfChild(n);
		double ratio = index / (double) size;
		
		for(int section=1;section<=COLOR_PAIRS.length;section++){
		    double sectionStart = (section-1)/(double)COLOR_PAIRS.length;
		    double sectionEnd = section/(double)COLOR_PAIRS.length;

		    if(ratio>=sectionStart && ratio<=sectionEnd){
			color1 = COLOR_PAIRS[section-1][0];
			color2 = COLOR_PAIRS[section-1][1];

		        int rRange = color2.getRed() - color1.getRed();
			int bRange = color2.getBlue() - color1.getBlue();
			int gRange = color2.getGreen() - color1.getGreen();
			double sectionRatio = (ratio-sectionStart)/(sectionEnd-sectionStart);

			Color c = new Color(color1.getRed() + (int)(sectionRatio*rRange),
					    color1.getGreen() + (int)(sectionRatio * gRange),
					    color1.getBlue() + (int)(sectionRatio * bRange));
			Material m = new Material(c);
			nMap.put(n,m);
			return m;
		    }
		}

		//should not get to this point
		return getDefaultMaterial();

	}

	@Override
	public Material getOtherMaterial(Object3D o) {
		return getDefaultMaterial();
	}

	@Override
	public Material getRnaStemMaterial(RnaStem3D s) {
		return getDefaultMaterial();
	}

	@Override
	public Material getRnaStrandMaterial(RnaStrand s) {
		return getDefaultMaterial();
	}

	@Override
	public Material getStrandJunctionMaterial(StrandJunction3D j) {
		return getDefaultMaterial();
	}
	
	
	private Nucleotide3D getNucleotide(Object3D o) {
		if(o == null)
			return null;
		
		if(o instanceof Nucleotide3D) {
			return (Nucleotide3D) o;
		}
		
		return getNucleotide(o.getParent());
	}
	
	public void reset() {
		nMap.clear();
	}

}
