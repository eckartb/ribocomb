package viewer.rnadesign;

import rnadesign.rnamodel.Atom3D;
import rnadesign.rnamodel.BranchDescriptor3D;
import rnadesign.rnamodel.KissingLoop3D;
import rnadesign.rnamodel.Nucleotide3D;
import rnadesign.rnamodel.RnaStem3D;
import rnadesign.rnamodel.RnaStrand;
import rnadesign.rnamodel.StrandJunction3D;
import rnadesign.rnamodel.AtomTools;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Link;
import viewer.graphics.Material;
import viewer.graphics.ColorPicker;
import viewer.graphics.ColorVector;
import viewer.graphics.Configurable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Atoms of a specified type are assigned a specific color
 * while other objects coloring are slightly different randomized
 * colors
 */
public class AtomColorModel extends RNAColorModel implements Configurable{

        private JFrame frame;


        //atom colors
        private ColorVector carbonColor;
        private ColorVector hydrogenColor;
        private ColorVector nitrogenColor;
        private ColorVector sulfurColor;;
        private ColorVector oxygenColor;
        private ColorVector phosphorusColor;
        private ColorVector defaultColor;
	
	public AtomColorModel(Color color) {

		//default atom colors
	        carbonColor =     new ColorVector(102.0/255.0,102.0/255.0,102.0/255.0,1);//dark gray toward black
		hydrogenColor =   new ColorVector(1,1,1,1);//white 
		nitrogenColor =   new ColorVector(0,0,1,1);//blue
		sulfurColor  =    new ColorVector(1,1,0,1);//yellow
		oxygenColor =     new ColorVector(1,0,0,1);//red
		phosphorusColor = new ColorVector(255.0/255.0,102.0/255.0,204.0/255.0,1);//pink
		defaultColor =    new ColorVector(1,1,1,1);//white
	}
	
	public Material getMaterial(Object3D o1, Object3D o2) {
		return getDefaultMaterial();
	}
 
	
	public Material getMaterial(Link l){
	    return getDefaultMaterial();
	}

	@Override
	public Material getAtomMaterial(Atom3D atom) {
		Material material = getDefaultMaterial();
		ColorVector color;
		char atomType = AtomTools.getElementChar(atom);
		switch(atomType){
		    case 'H':
			color = hydrogenColor;
			break;
		    case 'C':
			color = carbonColor;
			break;
		    case 'N':
			color = nitrogenColor;
			break;
		    case 'O':
			color = oxygenColor;
			break;
		    case 'S':
			color = sulfurColor;
			break;
		    case 'P':
			color = phosphorusColor;
			break;
		    default:
			color = defaultColor;
			break;
		}
		color.setAlpha(0.6);
		material.setDiffuse(color);
		color.setAlpha(0.4);
		material.setSpecular(color);
		return material;
	}

	@Override
	public Material getBranchDescriptorMaterial(BranchDescriptor3D b) {
		return getDefaultMaterial();
	}

	@Override
	public Material getKissingLoopMaterial(KissingLoop3D k) {
		return getDefaultMaterial();
	}

	@Override
	public Material getNucleotideMaterial(Nucleotide3D n) {
		return getDefaultMaterial();
	}

	@Override
	public Material getOtherMaterial(Object3D o) {
		return getDefaultMaterial();
	}

	@Override
	public Material getRnaStemMaterial(RnaStem3D s) {
		return getDefaultMaterial();
	}

	@Override
	public Material getRnaStrandMaterial(RnaStrand s) {
		return getDefaultMaterial();
	}

	@Override
	public Material getStrandJunctionMaterial(StrandJunction3D j) {
		return getDefaultMaterial();
	}
	

        public void launchConfigurationGUI(){
	    frame = new JFrame("Choose the preferred atom colors");
	    frame.setLayout(new BorderLayout());

	    JTabbedPane atomTabs = new JTabbedPane();

	    JPanel hydrogen = new JPanel();
	    Color init = new Color((float)hydrogenColor.getRed(),(float)hydrogenColor.getGreen(),(float)hydrogenColor.getBlue());
	    final JColorChooser hydrogenChooser = new JColorChooser(init);
	    hydrogen.add(hydrogenChooser);
	    atomTabs.add("Hydrogen",hydrogen);

	    JPanel carbon = new JPanel();
	    init = new Color((float)carbonColor.getRed(),(float)carbonColor.getGreen(),(float)carbonColor.getBlue());
	    final JColorChooser carbonChooser = new JColorChooser(init);
	    carbon.add(carbonChooser);
	    atomTabs.add("Carbon",carbon);

	    JPanel nitrogen = new JPanel();
	    init = new Color((float)nitrogenColor.getRed(),(float)nitrogenColor.getGreen(),(float)nitrogenColor.getBlue());
	    final JColorChooser nitrogenChooser = new JColorChooser(init);
	    nitrogen.add(nitrogenChooser);
	    atomTabs.add("Nitrogen",nitrogen);

	    JPanel oxygen = new JPanel();
	    init = new Color((float)oxygenColor.getRed(),(float)oxygenColor.getGreen(),(float)oxygenColor.getBlue());
	    final JColorChooser oxygenChooser = new JColorChooser(init);
	    oxygen.add(oxygenChooser);
	    atomTabs.add("Oxygen",oxygen);

	    JPanel sulfur = new JPanel();
	    init = new Color((float)sulfurColor.getRed(),(float)sulfurColor.getGreen(),(float)sulfurColor.getBlue());
	    final JColorChooser sulfurChooser = new JColorChooser(init);
	    sulfur.add(sulfurChooser);
	    atomTabs.add("Sulfur",sulfur);
	    
            JPanel phosphorus = new JPanel();
	    init = new Color((float)phosphorusColor.getRed(),(float)phosphorusColor.getGreen(),(float)phosphorusColor.getBlue());
	    final JColorChooser phosphorusChooser = new JColorChooser(init);
	    phosphorus.add(phosphorusChooser);
	    atomTabs.add("Phosphorus",phosphorus);

	    frame.add(atomTabs,BorderLayout.NORTH);

	    JPanel buttonPanel = new JPanel();
	    JButton cancelButton = new JButton("Cancel");
	    cancelButton.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			frame.setVisible(false);
			frame = null;
		    }
	    });
	    buttonPanel.add(cancelButton);
	    JButton configureButton = new JButton("Apply Changes");
	    configureButton.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			Color t;
			t = hydrogenChooser.getColor();
			hydrogenColor = new ColorVector(t.getRed()/255.0,t.getGreen()/255.0,t.getBlue()/255.0,1);
			t = carbonChooser.getColor();
			carbonColor = new ColorVector(t.getRed()/255.0,t.getGreen()/255.0,t.getBlue()/255.0,1);
			t = nitrogenChooser.getColor();
			nitrogenColor = new ColorVector(t.getRed()/255.0,t.getGreen()/255.0,t.getBlue()/255.0,1);
			t = oxygenChooser.getColor();
			oxygenColor = new ColorVector(t.getRed()/255.0,t.getGreen()/255.0,t.getBlue()/255.0,1);
			t = sulfurChooser.getColor();
			sulfurColor = new ColorVector(t.getRed()/255.0,t.getGreen()/255.0,t.getBlue()/255.0,1);
			t = phosphorusChooser.getColor();
			phosphorusColor = new ColorVector(t.getRed()/255.0,t.getGreen()/255.0,t.getBlue()/255.0,1);

			frame.setVisible(false);
			frame = null;
		    }
	    });
	    buttonPanel.add(configureButton);

	    frame.add(buttonPanel,BorderLayout.SOUTH);

	    frame.setVisible(true);
	    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	    frame.pack();
	}

}
