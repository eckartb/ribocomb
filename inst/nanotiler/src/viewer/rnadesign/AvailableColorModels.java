package viewer.rnadesign;

import viewer.graphics.ColorModel;
import viewer.graphics.DefaultColorModel;

import java.awt.Color;

/**
 * Enumeration containing the available color models
 * @author Calvin
 *
 */
public enum AvailableColorModels {
	
	StrandColorModel(new StrandColorModel(), "Strand"), Rainbow(new RainbowColorModel(), "Rainbow"), Atom(new AtomColorModel(Color.gray),"Atom"),
    White(new DefaultColorModel(), "White");
	
	private ColorModel model;
	private String name;
	
	private AvailableColorModels(ColorModel model, String name) {
		this.model = model;
		this.name = name;
	}
	
	/**
	 * Get the color model
	 * @return
	 */
	public ColorModel getModel() {
		return model;
	}
	
	public String toString() {
		return name;
	}
	
}
