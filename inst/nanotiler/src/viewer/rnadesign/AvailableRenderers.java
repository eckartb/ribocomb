package viewer.rnadesign;


/**
 * Factory enumeration for creating rna renderers
 * @author Calvin
 *
 */
public enum AvailableRenderers {
	Cartoon("Cartoon"), Ribbon("Ribbon"), Ball("Ball"), BallAndStick("Ball And Stick"), Stick("Stick");
	
	private String name;
	
	private AvailableRenderers(String name) {
		this.name = name;
	}
	
	public String toString() {
		return name;
	}
	
}
