package viewer.rnadesign;

import java.awt.event.*;
import java.awt.*;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.*;

import tools3d.Vector4D;

import javax.media.opengl.*;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DTools;

import viewer.graph.RenderableGraph;
import viewer.graph.rna.RenderableGraphFactory;
import viewer.graph.rna.RnaRendererGraphController;
import viewer.graphics.*;
import viewer.view.ViewOrientation;
import viewer.deprecated.RNAModelRenderer;
import viewer.display.*;
import viewer.display.MultiDisplayManager.Configuration;
import viewer.event.*;
import viewer.commands.*;
import rnadesign.rnacontrol.*;
import rnadesign.rnamodel.RnaTools;
import commandtools.*;
import controltools.*;
import java.text.DecimalFormat;
import java.awt.Graphics;
import java.awt.Dimension;

/**
 * Manager for displays used in Nanotiler
 * @author Calvin
 *
 */
public class GraphControllerDisplayManager extends MultiDisplayManager 
	implements DisplayLayoutListener, DisplayDecorator, DisplayViewChangedListener,
	SelectionListener, ModelChangeListener {
	
	private JTextField xLocation, yLocation, zLocation;
        private JTextField chosenField;
        private JComboBox objectSelectionTypeBox;
        private JComboBox colorBox;
        private JButton linkButton;
        public String objectSelectionType;
        private final int COMBOBOX_WIDTH= 89;
	private final int COMBOBOX_HEIGHT = 10;
        private final String[] rnaClassNames = RnaTools.getRnaClassNames();

	
	
	/**
	 * Get a configuration dialog for this manager
	 * @author Calvin
	 *
	 */
	public class GraphConfiguration extends Configuration {
		
		private JColorChooser colorChooser;

		public GraphConfiguration(String title) {
			super(title);
			/*JTabbedPane pane = getPane();
			JPanel panel = new JPanel();
			JPanel backgroundColor = new JPanel();
			backgroundColor.setLayout(new BoxLayout(backgroundColor, BoxLayout.Y_AXIS));
			colorChooser = new JColorChooser(Color.black);
			colorChooser.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black), "Background Color", TitledBorder.LEFT, TitledBorder.TOP));	
			panel.add(colorChooser);
			
			JButton ok = getOKButton();
			ok.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					Color color = colorChooser.getSelectionModel().getSelectedColor();
					for(Display d : getDisplay().getDisplays()) {
						d.setBackground(color);
						d.display();
					}
				}
			});
			pane.addTab("Display Options", panel);*/
			
			
		}	
		
		
	}

	private Object3DGraphController controller;
	private CommandApplication application;
	
	private LinkRenderer linkRenderer;
	
	//private RNAModelRenderer renderer;
	private RnaRendererGraphController renderer;
	
	private Object3DSelector selector;
	
	private boolean objectSelectionEnabled = true;
	private boolean viewTranslationEnabled = true;
	
	public GraphControllerDisplayManager(Object3DGraphController controller) {
		this(controller, null);
	}
	
	public GraphControllerDisplayManager(Object3DGraphController controller,
			CommandApplication application) {
		
		addCommand(new ResetCamera());
		addCommand(new CenterOnMolecularMass(controller.getGraph()));
		addCommand(new DeselectObjects(this));
		addCommand(new TranslateObjects(this));
		addCommand(new FindObject(controller));
		addCommand(new GraphEditor(controller, this));
		
		this.controller = controller;
		this.application = application;
		
		DisplayFactory.setDecorator(this);
		setDisplay(DisplayFactory.PrefabricatedDisplays.Single.getDefaultDisplay());
		
		
		ColorModel colorModel = ((AvailableColorModels) colorBox.getSelectedItem()).getModel();
		// AvailableColorModels.values()[0].getModel();

		this.linkRenderer = new LinkRenderer(controller, colorModel);
		linkRenderer.setRenderLinks(true);
		linkRenderer.addAllowedLink("Link");
		linkRenderer.addAllowedLink("SimpleLink");
		linkRenderer.addAllowedLink("SimpleHelixConstraintLink");
		getDisplay().addRenderable(linkRenderer);
		
		selector = new Object3DSelector(controller, getDisplay());
		selector.addSelectionListener(this);
		
		// controller.getGraph().addModelChangeListener(new ModelChangeListener() {
		// with this change, also listens to changes with respect to links, elastic network controller etc:
		controller.addModelChangeListener(this);
		
		getDisplay().addDisplayViewChangedListener(this);
		addDisplayLayoutListener(this);
		attachSelectionListeners();
		
		
		/*renderer = AvailableRenderers.values()[0].getRenderer(controller);
		if(renderer instanceof Colorable) {
			System.out.println("Setting color model of renderer");
			((Colorable)renderer).setColorModel(AvailableColorModels.values()[0].getModel());
		}
		
		getDisplay().setModelClones(renderer);
		
		*/
		
		RnaRendererGraphController rc = RenderableGraphFactory.generate(controller, AvailableRenderers.values()[0]);
		rc.setColorModel(AvailableColorModels.values()[0].getModel());
		rc.setMeshCached(true);
		//rc.setRefinementLevel(LODCapable.LOD_MEDIUM);
	        rc.setRefinementLevel(LODCapable.LOD_COARSE);
		renderer = rc;
		getDisplay().setModelClones(renderer);
		
		refreshCommands();

		objectSelectionTypeBox = new JComboBox(RnaTools.getRnaClassNames());
		objectSelectionTypeBox.setEditable(false);
		objectSelectionTypeBox.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
			    objectSelectionType = (String)objectSelectionTypeBox.getSelectedItem();
			    objectSelectionTypeBox.setSelectedItem(objectSelectionType);
			    System.out.println("Selection Type: "+objectSelectionType);
			}
		});
		objectSelectionType = (String)objectSelectionTypeBox.getSelectedItem();

		
		xLocation = new JTextField(5);
		yLocation = new JTextField(5);
		zLocation = new JTextField(5);
		xLocation.setEditable(false);
		yLocation.setEditable(false);
		zLocation.setEditable(false);

		chosenField = new JTextField(40);
		chosenField.setEditable(false);

		linkButton = new JButton("Find Links");
		linkButton.addActionListener(new LinkListener());
		JPanel bottomPanel = new JPanel();
		JPanel bottom = new JPanel();
		bottomPanel.setLayout(new BoxLayout(bottomPanel, BoxLayout.X_AXIS));
		bottomPanel.add(Box.createHorizontalGlue());
		bottom.add(new JLabel("Selection Type: "));
		bottom.add(objectSelectionTypeBox);
		bottom.add(Box.createHorizontalStrut(20));
		bottom.add(new JLabel("Selected: "));
		bottom.add(chosenField);
		bottom.add(Box.createHorizontalStrut(30));
		bottom.add(new JLabel("X"));
		bottom.add(xLocation);
		bottom.add(new JLabel("Y"));
		bottom.add(yLocation);
		bottom.add(new JLabel("Z"));
		bottom.add(zLocation);
		bottom.add(linkButton);
		bottomPanel.add(bottom);		
		bottomPanel.add(Box.createHorizontalGlue());
		
		addAncestorListener(new AncestorListener() {

			public void ancestorAdded(AncestorEvent event) {
				if(event.getAncestor().isShowing())
					attachLocationListeners();
				
			}

			public void ancestorMoved(AncestorEvent event) {
				// TODO Auto-generated method stub
				
			}

			public void ancestorRemoved(AncestorEvent event) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		
		add(bottomPanel, BorderLayout.SOUTH);
	}

        /* Listener for the link button which runs through the entire structure and 
	   checks for missing links.
	*/ 
        private final class LinkListener implements ActionListener{
	    public void actionPerformed(ActionEvent e){
		controller.addMissingLinks();
	    }
	}


	
	/**
	 * Get the selection manager used
	 * to manage object selections
	 * and deslections
	 * @return
	 */
	public Object3DSelector getSelector() {
		return selector;
	}

	/**
	 * Is object selection enabled
	 * @return Returns true if object selection
	 * is enabled or false if object selection
	 * isn't enabled.
	 */
	public boolean isObjectSelectionEnabled() {
		return objectSelectionEnabled;
	}

    /** Central method for notifying graphics of a change. TODO : check! */
    public void modelChanged(ModelChangeEvent e) {
	System.out.println("GraphControllerDisplayManager.Called modelChanged!");
	updateModel();
    }

	/**
	 * Turns on and off object selection
	 * @param objectSelectionEnabled
	 */
	public void setObjectSelectionEnabled(boolean objectSelectionEnabled) {
		if(this.objectSelectionEnabled != objectSelectionEnabled) {
			this.objectSelectionEnabled = objectSelectionEnabled;
			if(objectSelectionEnabled)
				attachSelectionListeners();
			else
				detachSelectionListeners();
		}
		
		
		
	}

	/**
	 * Can the views be manipulated by using mouse
	 * gestures in display windows
	 * @return Returns true if the views can be manipulated
	 * or false if they cannot be manipulated
	 */
	public boolean isViewTranslationEnabled() {
		return viewTranslationEnabled;
	}

	/**
	 * Turn on and off view manipulation via the mouse
	 * @param viewTranslationEnabled
	 */
	public void setViewTranslationEnabled(boolean viewTranslationEnabled) {
		this.viewTranslationEnabled = viewTranslationEnabled;
		for(Display d : getDisplay().getDisplays()) {
			d.setMouseTranslationEnabled(viewTranslationEnabled);
		}
	}

	private void attachSelectionListeners() {
		for(Display d : getDisplay().getDisplays()) {
			d.addMouseListener(selector);
		}
	}
	
	private void detachSelectionListeners() {
		for(Display d : getDisplay().getDisplays()) {
			d.removeMouseListener(selector);
		}
	}
	

	public void displayLayoutChanged(DisplayEvent e) {
		attachSelectionListeners();
		getDisplay().setModelClones(renderer);
		getDisplay().addDisplayViewChangedListener(this);
		selector.setDisplay(getDisplay());
		attachLocationListeners();
		
	}
	
	private void attachLocationListeners() {
		for(Display d : getDisplay().getDisplays()) {
			d.addMouseMotionListener(new LocationListener(d));
			d.addMouseListener(new LODTranslationListener(d));
		}
	}
	
	

	public void displayChanged(DisplayEvent e) {
		super.displayChanged(e);
		
		if(e.getId() == DisplayEvent.VIEW_ADDED) {
			e.getDisplay().addMouseListener(selector);
			e.getDisplay().addMouseMotionListener(new LocationListener(e.getDisplay()));
			e.getDisplay().addMouseListener(new LODTranslationListener(e.getDisplay()));
		}
		
	}
	

	public JPanel decorateDisplay(final Display display, ViewOrientation orientation) {
	    DefaultDecorator decorator = new DefaultDecorator();
	    /*DisplayDecorator decorator = DisplayFactory.getDecorator();
	    if(decorator == null){
		decorator = new DefaultDecorator();
		}*/
		final JPanel panel = decorator.decorateDisplay(display, orientation);
		
		JPanel bottomPanel = new JPanel();
		bottomPanel.setLayout(new BoxLayout(bottomPanel, BoxLayout.X_AXIS));
		JLabel label = new JLabel("Mode");
 		bottomPanel.add(label);
		
		Object[] objects = { "Solid", "Wireframe" };
		final JComboBox modeBox = new JComboBox(objects);
		modeBox.setPreferredSize(new Dimension(COMBOBOX_WIDTH,COMBOBOX_HEIGHT));
		modeBox.setEditable(false);
		modeBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Display display = (Display) ((BorderLayout)panel.getLayout()).getLayoutComponent(BorderLayout.CENTER);
				//RNAModelRenderer renderer = (RNAModelRenderer) display.getModel();
				RnaRendererGraphController renderer = (RnaRendererGraphController) display.getModel();
				if(modeBox.getSelectedItem().equals("Solid")) {
					renderer.setWireframeMode(false);
				} else {
					renderer.setWireframeMode(true);
				}
				
				display.repaint();
			}
		});
		
		bottomPanel.add(Box.createHorizontalStrut(5));
		bottomPanel.add(modeBox);
		
		final JComboBox rendererBox = new JComboBox(AvailableRenderers.values());
		rendererBox.setPreferredSize(new Dimension(COMBOBOX_WIDTH,COMBOBOX_HEIGHT));
		this.colorBox = new JComboBox(AvailableColorModels.values());
		this.colorBox.setPreferredSize(new Dimension(COMBOBOX_WIDTH,COMBOBOX_HEIGHT));

		final JButton rendererModelConfigureButton = new JButton("...");
		rendererModelConfigureButton.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
		    	Display display = (Display) ((BorderLayout)panel.getLayout()).getLayoutComponent(BorderLayout.CENTER);
				RNAModelRenderer renderer = (RNAModelRenderer) display.getModel();
				if(renderer instanceof Configurable)
				    ((Configurable)renderer).launchConfigurationGUI();
				
				display.display();
		    }
	        });
		rendererModelConfigureButton.setEnabled(false);		
		rendererBox.setEditable(false);
		rendererBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/*AvailableRenderers r = (AvailableRenderers) rendererBox.getSelectedItem();
				RNAModelRenderer renderer = r.getRenderer(controller);
				if(renderer instanceof Colorable) {
					colorBox.setEnabled(true);
					Colorable c = (Colorable) renderer;
					AvailableColorModels cm = (AvailableColorModels) colorBox.getSelectedItem();
					ColorModel model = cm.getModel();
					c.setColorModel(model);
					
				}
				else {
					colorBox.setEnabled(false);
				}
				if(renderer instanceof Configurable)
				    rendererModelConfigureButton.setEnabled(true);
				else
				    rendererModelConfigureButton.setEnabled(false);
				*/
				Display display = (Display) ((BorderLayout)panel.getLayout()).getLayoutComponent(BorderLayout.CENTER);
				RnaRendererGraphController oldRc = (RnaRendererGraphController) display.getModel();
				RnaRendererGraphController rc = RenderableGraphFactory.generate(controller, (AvailableRenderers) rendererBox.getSelectedItem());
				rc.loadState(oldRc);
				display.setModel(rc);
				
				display.display();
			}
		});

		final JButton colorModelConfigureButton = new JButton("...");
		colorModelConfigureButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
			    Display display = (Display) ((BorderLayout)panel.getLayout()).getLayoutComponent(BorderLayout.CENTER);
			    RnaRendererGraphController c = (RnaRendererGraphController) display.getModel();
			    AvailableColorModels cm = (AvailableColorModels)colorBox.getSelectedItem();

			    if(cm.getModel() instanceof Configurable)
				((Configurable)cm.getModel()).launchConfigurationGUI();
			    c.setColorModel(cm.getModel());
			    c.updateMeshes();
			    display.repaint();
		        }
		});
		colorModelConfigureButton.setEnabled(false);
		
		label = new JLabel("Renderer");
		bottomPanel.add(label);
		bottomPanel.add(Box.createHorizontalStrut(5));
		bottomPanel.add(rendererBox);
		bottomPanel.add(rendererModelConfigureButton);
		
		colorBox.setEditable(false);
		colorBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Display display = (Display) ((BorderLayout)panel.getLayout()).getLayoutComponent(BorderLayout.CENTER);
				Colorable c = (Colorable) display.getModel();
				AvailableColorModels cm = (AvailableColorModels) colorBox.getSelectedItem();
				c.setColorModel(cm.getModel());

				if(cm.getModel() instanceof Configurable)
				    colorModelConfigureButton.setEnabled(true);
				else
				    colorModelConfigureButton.setEnabled(false);
				
				display.repaint();
			}
		});
		
		
		
		label = new JLabel("Color Model");
		bottomPanel.add(label);
		bottomPanel.add(Box.createHorizontalStrut(5));
		bottomPanel.add(colorBox);
		bottomPanel.add(colorModelConfigureButton);
		bottomPanel.add(Box.createHorizontalGlue());

		// outdated ! Use view options instead
// 		final JCheckBox linkCheckBox = new JCheckBox("Links");
// 		linkCheckBox.setSelected(true);
// 		linkCheckBox.addActionListener(new ActionListener() {
// 			public void actionPerformed(ActionEvent e) {
// 				linkRenderer.setRenderLinks(linkCheckBox.isSelected());
// 				repaint();
// 			}
// 		});
// 		bottomPanel.add(linkCheckBox);
		bottomPanel.add(Box.createHorizontalGlue());
		
		for(DisplayCommand c : getCommands()) {
			bottomPanel.add(c.component(display));
		}
		
		
		
		panel.add(bottomPanel, BorderLayout.SOUTH);
		
		
		return panel;
	}
	
	/**
	 * Update the model that is being rendered
	 *
	 */
	public void updateModel() {
	    //System.out.println("Called GraphControllerDisplayManager.updateModel!");
		for(Display d : getDisplay().getDisplays()) {
			RnaRendererGraphController oldRc = (RnaRendererGraphController) d.getModel();
			RnaRendererGraphController rc = RenderableGraphFactory.generate(controller, oldRc.getRenderer());
			rc.loadState(oldRc);
			d.setModel(rc);
			d.display();
		}
		Object3D selected = controller.getGraph().getSelectionRoot();
		if(selected != null){
		    chosenField.setText(Object3DTools.getFullName(selected));
		}
		//System.out.println("Finished Updatemodel");
	}
	
	/**
	 * Get the link renderer
	 * @return
	 */
	public LinkRenderer getLinkRenderer() {
		return linkRenderer;
	}
	
	private class LocationListener extends MouseMotionAdapter {
		private Display d;
		
		public LocationListener(Display d) {
			this.d = d;
		}
		
		public void mouseMoved(MouseEvent e) {
			Vector4D point = d.getCoordinates(e.getPoint());
			if(point == null)
				return;
			DecimalFormat fmt = new DecimalFormat("###.000");
			xLocation.setText(fmt.format(point.getX()));
			yLocation.setText(fmt.format(point.getY()));
			zLocation.setText(fmt.format(point.getZ()));
		}
	}
	
	private class LODTranslationListener extends MouseAdapter {
		
		private Display d;
		private int lod;
		
		public LODTranslationListener(Display d) {
			this.d = d;
			
		}
		
		public void mousePressed(MouseEvent e) {
			RnaRendererGraphController rc = (RnaRendererGraphController) d.getModel();
			if(isViewTranslationEnabled() && rc.getRefinementLevel() != LODCapable.LOD_COARSE) {
				lod = rc.getRefinementLevel();
				rc.setRefinementLevel(LODCapable.LOD_COARSE);
				rc.updateMeshes();
			} else {
				lod = -1;
			}
		}
		
		public void mouseReleased(MouseEvent e) {
			if(isViewTranslationEnabled() && lod != -1) {
				RnaRendererGraphController rc = (RnaRendererGraphController) d.getModel();
				rc.setRefinementLevel(lod);
				rc.updateMeshes();
				d.display();
			}
		}
	}

	public void objectDeselected(SelectionEvent e) {
		handleSelection(e);
		
		
	}

	public void objectSelected(SelectionEvent e) {
		handleSelection(e);
	}
	
	private void handleSelection(SelectionEvent e) {
	    //System.out.println("Calling handleSelection() method");
		Object3D object = e.getSelectedObject();

		object = getObjectOfSelectionType(object);

		if(object!=null){
		    e.setSelectedObject(application,object);
		}
		else{
		    e.setSelectedObject(null);
		}

		object = e.getSelectedObject();

		if(object!= null){
		    //System.out.println("Called chosenField.setText(Object3DTools.getFullName(object))");
		    chosenField.setText(Object3DTools.getFullName(object));
		}
		else{
		    chosenField.setText("Object of type "+objectSelectionType+" not found");
		}
		RenderableGraph graph = renderer.getGraph(object);
		if(graph == null)
			return;

		graph.update(); // TODO check!!
		
		if(!e.isSelectionAdjusting()) {
			display();
		}
		//System.out.println("finished handleSelection!");
	}
 
        private Object3D getObjectOfSelectionType(Object3D object){
	    return Object3DTools.findClosestByClassName(Object3DTools.findRoot(object),object.getPosition(),objectSelectionType);

	}


	/**
	 * Get the renderer that is passed to all the displays.
	 * Note that this renderer is not the same renderer as the
	 * displays have; the displays have a clone of it.
	 * @return
	 */
	public RnaRendererGraphController getRenderer() {
		return renderer;
	}
	
	
	
}
