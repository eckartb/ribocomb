package viewer.rnadesign;

import java.awt.Color;
import javax.media.opengl.GL;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.LinkController;
import rnadesign.rnamodel.Atom3D;
import rnadesign.rnamodel.Nucleotide3D;
import viewer.graphics.ColorModel;
import viewer.graphics.ColorVector;
import viewer.graphics.Mesh;
import viewer.graphics.MeshRenderer;
import viewer.graphics.PrimitiveFactory;
import viewer.graphics.RenderableModel;
import viewer.graphics.SolidMeshRenderer;
import viewer.graphics.WireframeMeshRenderer;
import viewer.graphics.Material;
import viewer.util.Tools;
import tools3d.Matrix4D;
import tools3d.Vector4D;
import tools3d.objects3d.Link;
import tools3d.objects3d.Object3D;

import java.util.Set;

/**
 * Can render links from the LinkController as
 * lines or cylinders. Capsules still needs to be
 * implimented.
 * @author Calvin
 *
 */
public class LinkRenderer extends LinkVisualizationManager implements RenderableModel {
	
    private Object3DGraphController controller;
    private static final Mesh CYLINDER = PrimitiveFactory.generateCylinder(new Vector4D(1.0, 0.0, 0.0, 0.0), 0.5, 2.0, 1, 8);
    private ColorModel colorModel;
    private Color basePairLinkColor = Color.GRAY;
    private Color covalentBondLinkColor = Color.WHITE;

    public LinkRenderer(Object3DGraphController controller, ColorModel colorModel) {
	this.controller = controller;
	this.colorModel = colorModel;
	assert colorModel != null;
    }

    public ColorModel getColorModel() { return colorModel; }

    public void setColorModel(ColorModel colorModel) { this.colorModel = colorModel; }
    
    public void render(GL gl) {
	if(!isRenderLinks())
	    return;
	
	Set<String> allowed = getAllowedLinks();
	
	LinkController linkController = controller.getLinks();
	for(int i = 0; i < linkController.size(); ++i) {
	    Link l = linkController.get(i);
	    if(!allowed.contains(l.getTypeName()))
		return;
	    
	    renderLink(l, gl);
	    
	}
	
    }
    
    public void renderLink(Link l, GL gl) {

	Object3D o1 = l.getObj1();
	Object3D o2 = l.getObj2();
	Color color = this.getLinkColor(l);
// 	if ((o1 instanceof Atom3D) && (o2 instanceof Atom3D)) {
// 	    System.out.println("Rendering link between atoms " + o1.getFullName() + " " + o2.getFullName());
// 	}
	// if ((o1 instanceof Nucleotide3D) && (o2 instanceof Nucleotide3D)) {
	    // System.out.println("Rendering link between nucleotides " + o1.getFullName() + " " + o2.getFullName() + " with color "
	    // + color.getRed() + " " + color.getGreen() + " " + color.getBlue());
	// }
	Material material = new Material(color);
	Material.renderMaterial(gl, material);
	
	switch(getRenderMode()) {
	case Cylinder:
	    Vector4D axis = new Vector4D(o1.getPosition().minus(o2.getPosition()));
	    double length = axis.length() / 2.0;
	    Vector4D midpoint = new Vector4D(o1.getPosition().plus(o2.getPosition()));
	    midpoint = midpoint.mul(0.5);
	    midpoint.setW(1.0);
	    axis.setW(0.0);
	    axis.normalize();
	    MeshRenderer renderer;
	    if(isWireframeMode()) {
		renderer = new WireframeMeshRenderer(CYLINDER);
	    }
	    else {
		renderer = new SolidMeshRenderer(CYLINDER);
	    }
	    gl.glEnable(GL.GL_NORMALIZE);
	    gl.glPushMatrix();
	    gl.glTranslated(midpoint.getX(), midpoint.getY(), midpoint.getZ());
	    Vector4D rotationAxis = axis.cross(Tools.X_AXIS);
	    double angle = Math.acos(axis.dot(Tools.X_AXIS));
	    rotationAxis.normalize();
	    Matrix4D rotation = Tools.getRotationMatrix(rotationAxis, angle);
	    double[] matrix = rotation.getArray();
	    gl.glMultMatrixd(matrix, 0);
	    gl.glScaled(length, 1.0, 1.0);
	    renderer.render(gl);
	    gl.glPopMatrix();
	    gl.glDisable(GL.GL_NORMALIZE);
	    
	    break;
	case Capsule:
	case Line:
	    gl.glBegin(GL.GL_LINES);
	    gl.glVertex3d(o1.getPosition().getX(), o1.getPosition().getY(), o1.getPosition().getZ());
	    gl.glVertex3d(o2.getPosition().getX(), o2.getPosition().getY(), o2.getPosition().getZ());
	    gl.glEnd();
	    break;
	}
    }
    
    private Color getCovalentBondLinkColor(Link link) {
	return this.covalentBondLinkColor;
// 	assert link.getObj1() instanceof Atom3D;
// 	assert link.getObj2() instanceof Atom3D;
// 	Atom3D atom1 = (Atom3D)link.getObj1();
// 	Atom3D atom2 = (Atom3D)link.getObj2();
// 	Material m1 = getColorModel().getAtomMaterial(atom1);
// 	Material m2 = getColorModel().getAtomMaterial(atom2);
// 	ColorVector ambient1 = m1.getAmbient();
// 	ColorVector ambient2 = m2.getAmbient();
// 	return getAverageColor(ambient1.getAWTColor(),ambient2.getAWTColor());
       
	// TODO : quick fix, better: average between material
	// return m1.getAmbient().getAWTColor();
    }

    private Color getBasePairLinkColor() {
	return this.basePairLinkColor;
    }

    private Color getBasePairLinkColor(Link link) {
	// TODO
	return basePairLinkColor;
    }

    private void setBasePairLinkColor(Color c) {
	this.basePairLinkColor = c;
    }

    /** Returns average color TODO */
    private Color getAverageColor(Color color1, Color color2){
	return color1; 
	/* TODO !!!
	double  blue = (color1.getBlue() + color2.getBlue()) / 2.0;
	double  red = (color1.getRed() + color2.getRed()) / 2.0;
	double  green = (color1.getGreen() + color2.getGreen()) / 2.0;
	double  alpha = (color1.getAlpha() + color2.getAlpha()) / 2.0;

	System.out.println("Blue: "+blue+" Red: "+red+" Green: "+green+" Alpha: "+alpha);

	return new Color((int) (red), (int) ( green), (int) ( blue), (int) (alpha));
	*/
    }
    
    private boolean isCovalentBondLink(Link link) {
	return (link.getObj1() instanceof Atom3D) && (link.getObj2() instanceof Atom3D);
    }


    private boolean isBasePairLink(Link link) {
	return (link.getObj1() instanceof Nucleotide3D) && (link.getObj2() instanceof Nucleotide3D);
    }

    // @OVERRIDE
    public Color getLinkColor(Link link) {
	if (isCovalentBondLink(link)) {
	    // System.out.println("Covalent bond link: " + link + " " +  this.getCovalentBondLinkColor(link));
	    return getCovalentBondLinkColor(link);
	}
	else if (isBasePairLink(link)) {
	    // System.out.println("Base pair link: " + link + " " +  this.getBasePairLinkColor(link));
	    return this.getBasePairLinkColor(link);
	}
	else {
	    // System.out.println("Rendering nusual link: " + link);
	}
	// other cases: base pair interaction, graph point links, constraints etc
	return super.getLinkColor();
    }
	
    public Object3DGraphController getController() {
	return controller;
    }
    
    public void setController(Object3DGraphController controller) {
	this.controller = controller;
    }
    
}
