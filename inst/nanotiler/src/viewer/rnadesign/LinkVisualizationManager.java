package viewer.rnadesign;

import java.util.HashSet;
import java.util.Set;
import java.awt.Color;

// import viewer.graphics.ColorModel;

/**
 * Manages the rendering of links
 * @author Calvin
 *
 */
public class LinkVisualizationManager {
	
	private boolean renderLinks = false;
	
	private RenderMode renderMode = RenderMode.Line;
	
	private Color linkColor = Color.white;

    // private ColorModel colorModel;

	
	private Set<String> allowedLinks = new HashSet<String>();
	
	private boolean wireframeMode = false;
	
	public static enum RenderMode {
		Line, Cylinder, Capsule
	}
	
	public boolean isRenderLinks() {
		return renderLinks;
	}
	public void setRenderLinks(boolean renderLinks) {
		this.renderLinks = renderLinks;
	}
	
	public void setRenderMode(RenderMode renderMode) {
		this.renderMode = renderMode;
	}
	
	public RenderMode getRenderMode() {
		return renderMode;
	}
	
	public void addAllowedLink(String s) {
		allowedLinks.add(s);
	}
	
	public boolean removeAllowedLink(String s) {
		return allowedLinks.remove(s);
	}

    @SuppressWarnings(value="unchecked")	
	public Set<String> getAllowedLinks() {
		return (Set<String>) ((HashSet<String>)allowedLinks).clone();
	}
	
	public void setLinkColor(Color linkColor) {
		this.linkColor = linkColor;
	}
	
	public Color getLinkColor() {
		return linkColor;
	}
	
	public boolean isWireframeMode() {
		return wireframeMode;
	}
	
	public void setWireframeMode(boolean wireframeMode) {
		this.wireframeMode = wireframeMode;
	}
	
	
	
	
}
