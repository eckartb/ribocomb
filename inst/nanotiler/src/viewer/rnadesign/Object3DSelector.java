package viewer.rnadesign;

import java.awt.event.*;
import java.nio.*;
import javax.media.opengl.*;
import javax.media.opengl.glu.*;
import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;
import java.util.ArrayList;



import rnadesign.rnacontrol.*;
import tools3d.objects3d.*;
import tools3d.*;

import viewer.display.*;
import viewer.event.*;

/**
 * Allows objects to be selected through
 * interaction with the display
 * @author Calvin
 *
 */
public class Object3DSelector extends MouseAdapter {
	
	private Object3DGraphController controller;
	private MultiDisplay display;
	
	private Set<Object3D> selected = new HashSet<Object3D>();
	private Set<String> disallowedSelections = new HashSet<String>();
	
	private ArrayList<SelectionListener> listeners = 
		new ArrayList<SelectionListener>();
	
	private int maxNumberSimultaneousSelections = Integer.MAX_VALUE;
	
	public Object3DSelector(Object3DGraphController controller) {
		this.controller = controller;
	}
	
	public Object3DSelector(Object3DGraphController controller, MultiDisplay display) {
		this.controller = controller;
		this.display = display;
	}
	
	/**
	 * Maximum number of objects that can be selected
	 * at the same time
	 * @return
	 */
	public int getMaxNumberSimultaneousSelections() {
		return maxNumberSimultaneousSelections;
	}
	
	/**
	 * Adds a selection listener. Listeners
	 * notified when selections are made or deselections
	 * occur
	 * @param listener
	 */
	public void addSelectionListener(SelectionListener listener) {
		listeners.add(listener);
	}
	
	/**
	 * Remove a selection listener
	 * @param listener
	 * @return
	 */
	public boolean removeSelectionListener(SelectionListener listener) {
		return listeners.remove(listener);
	}
	
	protected void notifyObjectSelected(Object3D selected) {
		for(SelectionListener l : listeners)
			l.objectSelected(new SelectionEvent(selected, this));
	}
	
	protected void notifyObjectDeselected(Object3D deselected) {
		for(SelectionListener l : listeners)
			l.objectDeselected(new SelectionEvent(deselected, this));
	}
	
	protected void notifyObjectDeselected(Object3D deselected, boolean adjusting) {
		for(SelectionListener l : listeners) {
			l.objectDeselected(new SelectionEvent(deselected, this, adjusting));
		}
	}
	
	protected void notifyObjectSelected(Object3D selected, boolean adjusting) {
		for(SelectionListener l : listeners) {
			l.objectSelected(new SelectionEvent(selected, this, adjusting));
		}
	}
	
	/**
	 * Has the effect of deselecting all current selections
	 * @param maxNumberSimultaneousSelections
	 */
	public void setMaxNumberSimultaneousSelections(int maxNumberSimultaneousSelections) {
		if(maxNumberSimultaneousSelections < 0)
			throw new IllegalArgumentException("Argument must be greater than -1");
		this.maxNumberSimultaneousSelections = maxNumberSimultaneousSelections;
		if(selected.size() > maxNumberSimultaneousSelections)
			deselectAll();
	}
	
	/**
	 * Dont allow classes with this name to be selected.
	 * By default, no class name is disallowed
	 * @param className
	 */
	public void disallowClassName(String className) {
		disallowedSelections.add(className);
	}
	
	/**
	 * Allow class names with this name to be selected.
	 * By default, all class names are allowed
	 * @param className
	 */
	public void allowClassName(String className) {
		disallowedSelections.remove(className);
	}

	/**
	 * Get an array of the selected objects
	 * @return
	 */
	public Object3D[] getSelected() {
		Object3D[] array = new Object3D[selected.size()];
		selected.toArray(array);
		return array;
	}
	
	
	/**
	 * Deselect all the objects
	 *
	 */
	public void deselectAll() {
		int count = 0;
		for(Object3D o : selected) {
			o.setSelected(false);
			notifyObjectDeselected(o, ++count == selected.size() ? false : true);
		}
		
		selected.clear();
	}
	
	/**
	 * Select an object programatically
	 * @param object
	 */
	public void select(Object3D object) {
		if(!selected.contains(object))
			selected.add(object);
		
		object.setSelected(true);
		notifyObjectSelected(object);
	}
	
	/**
	 * Deslect an object progamatically
	 * @param object
	 */
	public void deselect(Object3D object) {
		selected.remove(object);
		object.setSelected(false);
		notifyObjectDeselected(object);
	}
	
	/**
	 * Determine if an object is selected
	 * @param object
	 * @return
	 */
	public boolean isSelected(Object3D object) {
		return selected.contains(object);
	}
	
	/**
	 * Are any objects selected
	 * @return
	 */
	public boolean isSelected() {
		return selected.size() != 0;
	}
	/*
	public Object3D findSelectedSet(Vector4D ray, Vector4D offset, Object3D root) {
		BoundingVolume volume = root.getBoundingVolume();
		Vector4D o = new Vector4D(offset);
		o.setW(0.0);
		Vector4D p = new Vector4D(root.getPosition());
	}
	*/

	/**
	 * Find an object that is selected
	 * Not sure if method intersectRay is correct or bounding volume up to date.
	 * Currently prefer findSelected2 method, although this method should be faster
	 * @param ray Ray extending through three dimensional space
	 * @param offset Offset of ray
	 * @param root Root of the tree to search for a likely selection candidate
	 * @return Returns an object if it is selected (intersected by ray) or
	 * null if no object is selected.
	 */
	public Object3D findSelected(Vector4D ray, Vector4D offset, Object3D root) {
	    System.out.println("Starting findSelected for node: " + root.getFullName());
	    if(disallowedSelections.contains(root.getClassName())) {
		System.out.println("No selection possible (0)!");
		return null;
	    }
	    if (ray.length() == 0) {
		System.out.println("Internal error: view ray has zero length! No selection of objects is possible");
		return null;
	    }
		BoundingVolume volume = root.getBoundingVolume();
		System.out.println("Using bounding volume: " + volume + " and ray: " + ray + " and offset: " + offset);
		Vector4D o = new Vector4D(offset);
		o.setW(0.0);
		Vector4D p = new Vector4D(root.getPosition());
		//o.set(o.getX() - p.getX(), o.getY() - p.getY(), o.getZ() - p.getZ(), 1.0);
		if((root.getParent() != null && root.getParent().isSelected())) {
		    // || !volume.intersectRay(ray, offset)) {
		    System.out.println("No selection possible (1a)!");
		    return null;
		}
		// check BOTH directions of viewing ray:
 		if((!volume.intersectRay(ray, offset)) && (!volume.intersectRay(ray.mul(-1.0), offset))) {
 		    System.out.println("No selection possible (1b)!");
 		    return null;
 		}
		if(root.size() == 0) {
		    System.out.println("Selected leaf node!");
		    return root;
		}
		
		double distance = Double.MAX_VALUE;
		int maxDepth = Integer.MIN_VALUE;
		Object3D found = null;
		for (int i = 0; i < root.size(); ++i) {
			Object3D pos = findSelected(ray, offset, root.getChild(i));
			if(pos == null) {
			    System.out.println("No selection possible (2)!");
				continue;
			}
// 			Vector4D position = new Vector4D(pos.getPosition());
// 			position = position.minus(offset);
// 			position.setW(0.0);
// 			double d = position.dot(ray);

			double d = Math.abs(GeometryTools.distanceToLine(pos.getPosition(), new Vector3D(offset), new Vector3D(ray))); // bugfix!!! :-) EB
			int depth = pos.getDepth();

			if(depth > maxDepth || (depth == maxDepth && d < distance)) {
				distance = d;
				found = pos;
				maxDepth = depth;
			}
			
			
		}
		
		if(found == null) {
		    System.out.println("No selection possible (3)!");
		    return root;
		}
		System.out.println("Found selection object: " + found.getFullName());
		return found;
		
	}

	/**
	 * Find an object that is selected
	 * @param ray Ray extending through three dimensional space
	 * @param offset Offset of ray
	 * @param root Root of the tree to search for a likely selection candidate
	 * @return Returns an object if it is selected (intersected by ray) or
	 * null if no object is selected.
	 */
	public Object3D findSelected2(Vector4D ray, Vector4D offset, Object3D root) {
	    // System.out.println("Starting findSelected for node: " + root.getFullName());
	    if(disallowedSelections.contains(root.getClassName())) {
		System.out.println("No selection possible because of object class!");
		return null;
	    }
	    Object3DSet objectSet = new SimpleObject3DSet(root);
// 		BoundingVolume volume = root.getBoundingVolume();
// 		System.out.println("Using bounding volume: " + volume + " and ray: " + ray + " and offset: " + offset);
// 		Vector4D o = new Vector4D(offset);
// 		o.setW(0.0);
// 		Vector4D p = new Vector4D(root.getPosition());
// 		//o.set(o.getX() - p.getX(), o.getY() - p.getY(), o.getZ() - p.getZ(), 1.0);
// 		if((root.getParent() != null && root.getParent().isSelected())) {
// 		    // || !volume.intersectRay(ray, offset)) {
// 		    System.out.println("No selection possible (1a)!");
// 		    return null;
// 		}
//  		if(!volume.intersectRay(ray, offset)) {
//  		    System.out.println("No selection possible (1b)!");
//  		    return null;
//  		}
// 		if(root.size() == 0) {
// 		    System.out.println("Selected leaf node!");
// 		    return root;
// 		}
		
		double distance = Double.MAX_VALUE;
		int maxDepth = Integer.MIN_VALUE;
		Object3D found = null;
		for (int i = 0; i < objectSet.size(); ++i) {
		    Object3D pos = objectSet.get(i); // findSelected(ray, offset, root.getChild(i));
		    if(pos == null) {
			System.out.println("No selection possible, object is null!");
			continue;
		    }
// 		    Vector4D position = new Vector4D(pos.getPosition());
// 		    position = position.minus(offset);
// 		    position.setW(0.0);
		    Vector3D objectPos = pos.getPosition();
		    // double d = position.dot(ray); // buggy? Yes
		    double d = Math.abs(GeometryTools.distanceToLine(objectPos, new Vector3D(offset), new Vector3D(ray))); // bugfix!!! :-) EB
		    int depth = pos.getDepth();
		    
		    if(depth > maxDepth || (depth == maxDepth && d < distance)) {
			distance = d;
			found = pos;
			maxDepth = depth;
		    }
		}
		
		if(found == null) {
		    System.out.println("No selection found!");
		    return root;
		}
		System.out.println("Found initial selection object: " + found.getFullName());
		return found;
		
	}

	/**
	 * Set the display
	 * @param display
	 */
	public void setDisplay(MultiDisplay display) {
		this.display = display;
	}
	
	public void mouseClicked(MouseEvent e) {
		
		Display display = (Display) e.getSource();
		GLContext context = display.getContext();
		int result = context.makeCurrent();
		if(result == GLContext.CONTEXT_NOT_CURRENT) {
			System.out.println("Could not make context current");
			return;
		}
		
		GL gl = context.getGL();
		GLU glu = new GLU();
		IntBuffer viewport = IntBuffer.allocate(4);
		DoubleBuffer projection = DoubleBuffer.allocate(16);
		DoubleBuffer modelview = DoubleBuffer.allocate(16);
		
		gl.glGetIntegerv(GL.GL_VIEWPORT, viewport);
		gl.glGetDoublev(GL.GL_PROJECTION_MATRIX, projection);
		gl.glGetDoublev(GL.GL_MODELVIEW_MATRIX, modelview);
		
		int y = display.getHeight() - e.getY() - 1;
		
		DoubleBuffer near = DoubleBuffer.allocate(4);
		DoubleBuffer far = DoubleBuffer.allocate(4);
		
		boolean check1 = glu.gluUnProject(e.getX(), y, 0.0, modelview, projection, viewport, near);
		boolean check2 = glu.gluUnProject(e.getX(), y, 1.0, modelview, projection, viewport, far);
		assert check1;
		assert check2;
		final Vector4D n = new Vector4D(near.get(0), near.get(1), near.get(2), 0.0);
		final Vector4D f = new Vector4D(far.get(0), far.get(1), far.get(2), 0.0);
				
		final Vector4D v = f.minus(n);
		
		v.normalize();

		// Object3D found = findSelected(v, n, controller.getGraph().getGraph());
		Object3D found = findSelected2(v, n, controller.getGraph().getGraph()); // just for testing
		
		if(found != null && !selected.contains(found)) {
			Iterator<Object3D> iter = selected.iterator();
			boolean removed = false;
			while(iter.hasNext()) {
				Object3D o = iter.next();
				if(found.isAncestor(o)) {	
					removed = true;
					o.setSelected(false);
					iter.remove();
				}
			}
			
			if(removed || selected.size() < maxNumberSimultaneousSelections) {
				selected.add(found);
				found.setSelected(true);
				notifyObjectSelected(found);
			}
			
			
		} else if(found != null) {
			selected.remove(found);
			found.setSelected(false);
			notifyObjectDeselected(found);
		}
		

	}
	
}
