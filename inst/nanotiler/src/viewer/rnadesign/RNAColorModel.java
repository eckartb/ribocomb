package viewer.rnadesign;

import tools3d.objects3d.Object3D;
import viewer.graphics.ColorModel;
import viewer.graphics.Material;
import viewer.graphics.ColorVector;
import rnadesign.rnamodel.*;

import java.awt.Color;

/**
 * Assigns a color to each type of model that composes
 * an RNA molecule
 * @author Calvin
 *
 */
public abstract class RNAColorModel implements ColorModel {
	

	public Material getMaterial(Object3D o) {
		if(o instanceof BranchDescriptor3D)
		    return getBranchDescriptorMaterial((BranchDescriptor3D) o);
		if(o instanceof Nucleotide3D)
			return getNucleotideMaterial((Nucleotide3D) o);
		if(o instanceof RnaStrand)
			return getRnaStrandMaterial((RnaStrand) o);
		if(o instanceof RnaStem3D)
			return getRnaStemMaterial((RnaStem3D) o);
		if(o instanceof BranchDescriptor3D)
			return getBranchDescriptorMaterial((BranchDescriptor3D) o);
		if(o instanceof StrandJunction3D)
			return getStrandJunctionMaterial((StrandJunction3D) o);
		if(o instanceof KissingLoop3D) 
			return getKissingLoopMaterial((KissingLoop3D) o);
		if(o instanceof Atom3D)
			return getAtomMaterial((Atom3D) o);
		
		return getOtherMaterial(o);
		
	}
	
	public Color getColor(Object3D o) {
		Material m = getMaterial(o);
		ColorVector v = m.getDiffuse();
		return new Color((int) (v.getRed() * 255), (int) (v.getGreen() * 255), (int) (v.getBlue() * 255));
	}
	
	public void reset() {
		
	}
	
	public Material getDefaultMaterial() {
		return new Material(0.0, 0.0, 0.0, 0.0,
				0.8, 0.8, 0.8, 1.0,
				0.2, 0.2, 0.2, 1.0,
				0.0, 0.0, 0.0, 1.0,
				32.0);
	}
	
	public abstract Material getAtomMaterial(Atom3D atom);
	
	public abstract Material getBranchDescriptorMaterial(BranchDescriptor3D b);

	public abstract Material getNucleotideMaterial(Nucleotide3D n);
	
	public abstract Material getRnaStrandMaterial(RnaStrand s);

	public abstract Material getRnaStemMaterial(RnaStem3D s);
	
	public abstract Material getKissingLoopMaterial(KissingLoop3D k);
	
	public abstract Material getStrandJunctionMaterial(StrandJunction3D j);
	
	public abstract Material getOtherMaterial(Object3D o);
	
	
}
