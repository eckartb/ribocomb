package viewer.rnadesign;

import rnadesign.rnamodel.Atom3D;
import rnadesign.rnamodel.BranchDescriptor3D;
import rnadesign.rnamodel.KissingLoop3D;
import rnadesign.rnamodel.Nucleotide3D;
import rnadesign.rnamodel.RnaStem3D;
import rnadesign.rnamodel.RnaStrand;
import rnadesign.rnamodel.StrandJunction3D;
import tools3d.objects3d.Link;
import tools3d.objects3d.Object3D;
import viewer.graphics.Material;

import java.util.Map;
import java.util.HashMap;
import java.util.Random;
import java.awt.Color;

/**
 * Assigns different colors to different strands
 * @author Calvin
 *
 */
public class StrandColorModel extends RNAColorModel {
	
        /*rainbow goes from color COLOR_PAIRS[0][0] to [0][1] to [1][1] to ... ends at color [length][1]
          rainbow applies to the strand colors*/ 
        private final static Color[][] COLOR_PAIRS = {
		{ Color.red, Color.orange },
		{ Color.orange, Color.yellow },
		{ Color.yellow, Color.green },
		{ Color.green, Color.cyan } 
	};

	private Map<Object3D, Material> map = 
		new HashMap<Object3D, Material>();

	@Override
	public Material getAtomMaterial(Atom3D atom) {
		return materialFromParent(atom);
	}

	@Override
	public Material getBranchDescriptorMaterial(BranchDescriptor3D b) {
		return getDefaultMaterial();
	}

	@Override
	public Material getKissingLoopMaterial(KissingLoop3D k) {
		return getDefaultMaterial();
	}

	@Override
	public Material getNucleotideMaterial(Nucleotide3D n) {
		return materialFromParent(n);
	}

	@Override
	public Material getOtherMaterial(Object3D o) {
		return getDefaultMaterial();
	}

	@Override
	public Material getRnaStemMaterial(RnaStem3D s) {
		return getDefaultMaterial();
	}
        //first strand is red  last is blue  in between red->orange->yellow->green->blue.
	@Override
	public Material getRnaStrandMaterial(RnaStrand s) {
		if(map.containsKey(s))
			return map.get(s);
		

		Color color1, color2;

		int size = s.getSiblingCount();
		int index = s.getParent().getIndexOfChild(s);
		if(index>0)index++;
		double ratio = index / (double) size;
		System.out.println("Size: "+size+" index: "+index+" ratio: "+ratio);
		
		for(int section=1;section<=COLOR_PAIRS.length;section++){
		    double sectionStart = (section-1)/(double)COLOR_PAIRS.length;
		    double sectionEnd = section/(double)COLOR_PAIRS.length;

		    if(ratio>=sectionStart && ratio<=sectionEnd){
			color1 = COLOR_PAIRS[section-1][0];
			color2 = COLOR_PAIRS[section-1][1];

		        int rRange = color2.getRed() - color1.getRed();
			int bRange = color2.getBlue() - color1.getBlue();
			int gRange = color2.getGreen() - color1.getGreen();
			double sectionRatio = (ratio-sectionStart)/(sectionEnd-sectionStart);

			Color c = new Color(color1.getRed() + (int)(sectionRatio*rRange),
					    color1.getGreen() + (int)(sectionRatio * gRange),
					    color1.getBlue() + (int)(sectionRatio * bRange));
			Material m = new Material(c);
			map.put(s,m);
			return m;
		    }
		}
		//should not get to this point
		return getDefaultMaterial();
	}

	@Override
	public Material getStrandJunctionMaterial(StrandJunction3D j) {
		return getDefaultMaterial();
	}

	public Material getMaterial(Link link) {
		return getDefaultMaterial();
	}
	
	private Material materialFromParent(Object3D o) {
		if(o == null)
			return getDefaultMaterial();
		
		if(o instanceof RnaStrand) {
			if(map.containsKey(o))
				return map.get(o);
			
			Random r = new Random();
			Color c = new Color(r.nextInt(200) + 55, r.nextInt(200) + 55, r.nextInt(200) + 55);
			Material m = new Material(c);
			
			map.put(o, m);
			return m;
			
		}
		
		return materialFromParent(o.getParent());
		
		
	}
	
	public void reset() {
		map.clear();
	}

}
