package viewer.util;

import java.awt.*;
import java.awt.event.*;

/**
 * Class that encapsulates all key
 * bindings for the viewer
 * @author Calvin
 *
 */
public class KeyBindings {
	
	private int zoomBinding = InputEvent.BUTTON1_DOWN_MASK | InputEvent.SHIFT_DOWN_MASK;
	private int rotateXBinding = InputEvent.BUTTON1_DOWN_MASK;
	private int rotateYBinding = InputEvent.BUTTON1_DOWN_MASK;
	private int rotateZBinding = InputEvent.BUTTON3_DOWN_MASK;
	private int translationBinding = InputEvent.BUTTON1_DOWN_MASK | InputEvent.CTRL_DOWN_MASK;
	
	private KeyBindings() {
		
	}
	
	public int getViewManipulationBindings() {
		return zoomBinding | rotateXBinding | rotateYBinding | rotateZBinding | translationBinding;
	}
	
	private static KeyBindings keyBindings = new KeyBindings();
	
	public static KeyBindings getKeyBindings() {
		return keyBindings;
	}

	public int getRotateXBinding() {
		return rotateXBinding;
	}

	public void setRotateXBinding(int rotateXBinding) {
		this.rotateXBinding = rotateXBinding;
	}
	
	public int getRotateYBinding() {
		return rotateYBinding;
	}
	
	public void setRotateYBinding(int rotateYBinding) {
		this.rotateYBinding = rotateYBinding;
	}

	public int getRotateZBinding() {
		return rotateZBinding;
	}

	public void setRotateZBinding(int rotateZBinding) {
		this.rotateZBinding = rotateZBinding;
	}

	public int getTranslationBinding() {
		return translationBinding;
	}

	public void setTranslationBinding(int translationBinding) {
		this.translationBinding = translationBinding;
	}

	public int getZoomBinding() {
		return zoomBinding;
	}

	public void setZoomBinding(int zoomBinding) {
		this.zoomBinding = zoomBinding;
	}

	public static void setKeyBindings(KeyBindings keyBindings) {
		KeyBindings.keyBindings = keyBindings;
	}
	
	
	
	
}
