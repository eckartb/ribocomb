package viewer.util;

public class Pair<U, V> {
	private U first;
	private V second;
	
	public Pair() {
		this(null, null);
	}
	
	public Pair(U first, V second) {
		this.first = first;
		this.second = second;
	}

	public U getFirst() {
		return first;
	}

	public void setFirst(U first) {
		this.first = first;
	}

	public V getSecond() {
		return second;
	}

	public void setSecond(V second) {
		this.second = second;
	}
	
	public boolean equals(Object o) {
		if(o instanceof Pair) {
			Pair p = (Pair) o;
			return p.getFirst().equals(getFirst()) && p.getSecond().equals(getSecond());
		}
		
		return false;
	}
	
	public int hashCode() {
		return getFirst().hashCode() + getSecond().hashCode();
	}
	
	public static <U, V> Pair<U, V> makePair(U first, V second) {
		return new Pair<U, V>(first, second);
	}

}
