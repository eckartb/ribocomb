package viewer.util;

import java.nio.DoubleBuffer;
import java.nio.IntBuffer;

import tools3d.*;
import viewer.display.Display;

import javax.media.opengl.GL;
import javax.media.opengl.GLContext;
import javax.media.opengl.GLException;
import javax.media.opengl.glu.GLU;

public class Tools {
	
	public static final Vector4D X_AXIS = new Vector4D(1.0, 0.0, 0.0, 0.0);
	public static final Vector4D Y_AXIS = new Vector4D(0.0, 1.0, 0.0, 0.0);
	public static final Vector4D Z_AXIS = new Vector4D(0.0, 0.0, 1.0, 0.0);
	
	/**
	 * Translate a vector v on the axis axis by the amount given
	 * @param v
	 * @param axis normalized vector
	 * @param amount
	 * @return
	 */
	public static Vector4D translate(Vector4D v, Vector4D axis, double amount) {
		Vector4D temp = new Vector4D(v);
		Vector4D translation = axis.mul(amount);
		
		return temp.plus(translation);
	}
	
	/**
	 * Rotate a vector around an axis
	 * @param v Vector to rotate
	 * @param axis Axis to rotate around, must be normalized
	 * @param angle Angle of rotation in radians
	 * @return Returns the rotated vector or null if axis isn't normalized or
	 * angle isn't in radian form
	 */
	public static Vector4D rotate(Vector4D v, Vector4D axis, double angle) {
		Matrix4D matrix = getRotationMatrix(axis, angle);
		return matrix.multiply(v);
	}
	
	public static Matrix4D getRotationMatrix(Vector4D axis, double angle) {
		double cos = Math.cos(angle);
		double sin = Math.sin(angle);
		double x = axis.getX();
		double y = axis.getY();
		double z = axis.getZ();
		return new Matrix4D(cos + (1 - cos) * x * x, (1 - cos) * x * y - z * sin, (1 - cos) * x * z + y * sin, 0.0,
				(1 - cos) * x * y + z * sin, cos + (1 - cos) * y * y, (1 - cos) * y * z - x * sin, 0.0,
				(1 - cos) * x * z - y * sin, (1 - cos) * y * z + x * sin, cos + (1 - cos) * z * z, 0.0,
				0.0, 0.0, 0.0, 1.0);
	}
	
	
	/**
	 * Projects vector u onto vector v
	 * @param u
	 * @param v
	 * @return
	 */
	public static double project(Vector4D u, Vector4D v) {
		return u.dot(v) / v.dot(v);
	}
	
	/**
	 * Generate a linearly independent vector
	 * @param v
	 * @return
	 */
	public static Vector4D orthogonal(Vector4D v) {
		Vector4D normal = new Vector4D(v);
		double x = Math.abs(normal.getX());
		double y = Math.abs(normal.getY());
		double z = Math.abs(normal.getZ());
		if(x <= y && x <= z) {
			normal.setX(0.0);
			double yy = normal.getY();
			normal.setY(-1 * normal.getZ());
			normal.setZ(yy);
		}
		
		else if(y <= x && y <= z) {
			normal.setY(0.0);
			double xx = normal.getX();
			normal.setX(-1 * normal.getZ());
			normal.setZ(xx);
		}
		
		else {
			normal.setZ(0.0);
			double xx = normal.getX();
			normal.setX(-1 * normal.getY());
			normal.setY(xx);
		}
		
		return normal;
		
	}
	
	public static double angle(Vector4D u, Vector4D v) {
		double angle = (u.dot(v)) / (u.length() * v.length());
		angle = angle * Math.PI / 180;
		return Math.acos(angle);
	}
	
	public static Vector4D[] lineSphereIntersection(Vector4D line, Vector4D offset, double radius, Vector4D sphereCenter) {
		double a = line.getX() * line.getX() + line.getY() + line.getY() + line.getZ() + line.getZ();
		double b = 2 * (line.getX() * (offset.getX() - sphereCenter.getX()) + line.getY() * (offset.getY() - sphereCenter.getY()) + line.getZ() * (offset.getZ() - sphereCenter.getZ()));
		double c = offset.getX() * offset.getX() + offset.getY() + offset.getY() + offset.getZ() * offset.getZ() + 
			sphereCenter.getX() * sphereCenter.getX() + sphereCenter.getY() * sphereCenter.getY() + sphereCenter.getZ() * sphereCenter.getZ() -
			2 * (offset.getX() * sphereCenter.getX() + offset.getY() * sphereCenter.getY() + offset.getZ() * sphereCenter.getZ()) - 
			radius * radius;
		double quadraticPart = b * b - 4 * a * c;
		if(quadraticPart < 0.0) {
			Vector4D[] array = new Vector4D[0];
			return array;
		}
		else if(quadraticPart < 0.000000000000000001) {
			Vector4D[] array = new Vector4D[1];
			double d = -b / (2 * a);
			array[0] = new Vector4D(line);
			array[0] = array[0].mul(d);
			array[0].add(offset);
			return array;
		}
			
		return null;
	}
	
	/**
	 * 
	 * @param p Point clicked
	 * @param d Display clicked on
	 * @param ray Filled out with ray information 
	 * @param origin Filled out with origin of ray information
	 */
	public static void screenTo3DSpace(java.awt.Point p, Display d, Vector4D ray, Vector4D origin) {
		GL gl = null;
		GLU glu = null;
		
		try {
			GLContext context = d.getContext();
			context.makeCurrent();
			gl = context.getGL();
			glu = new GLU();
		} catch (NullPointerException e) {
			return;
		}
		
		IntBuffer viewport = IntBuffer.allocate(4);
		DoubleBuffer projection = DoubleBuffer.allocate(16);
		DoubleBuffer modelview = DoubleBuffer.allocate(16);
		
		gl.glGetIntegerv(GL.GL_VIEWPORT, viewport);
		gl.glGetDoublev(GL.GL_PROJECTION_MATRIX, projection);
		gl.glGetDoublev(GL.GL_MODELVIEW_MATRIX, modelview);
		
		int y = d.getHeight() - (int) p.getY() - 1;
		
		DoubleBuffer near = DoubleBuffer.allocate(3);
		DoubleBuffer far = DoubleBuffer.allocate(3);
		
		glu.gluUnProject(p.getX(), y, 0.0, modelview, projection, viewport, near);
		glu.gluUnProject(p.getX(), y, 1.0, modelview, projection, viewport, far);
		
		
		final Vector4D o = new Vector4D(near.get(0), near.get(1), near.get(2), 0.0);
		final Vector4D r = new Vector4D(far.get(0), far.get(1), far.get(2), 0.0);
		r.sub(o);
		r.normalize();
		if(ray != null)
			ray.set(r.getX(), r.getY(), r.getZ(), r.getW());
		if(origin != null)
			origin.set(o.getX(), o.getY(), o.getZ(), o.getW());
	}
}
