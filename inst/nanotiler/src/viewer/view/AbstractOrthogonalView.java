package viewer.view;

import tools3d.*;
import viewer.event.*;

/**
 * An orthogonal view whose orientation isn't yet defined. Sub-classes should
 * define specific orientations.
 * @author Calvin
 *
 */
public abstract class AbstractOrthogonalView implements OrthogonalView {
	
	protected Vector4D viewDirection;
	protected Vector4D viewLocation;
	protected Vector4D viewDirectionNormal;
	private Vector4D viewDirectionCopy;
	private Vector4D viewLocationCopy;
	private Vector4D viewDirectionNormalCopy;
	
	protected double zoom = 1.0;
	private ViewListenerManager manager = new ViewListenerManager();
	private int zoomFactor = 1;
	
	public AbstractOrthogonalView(Vector4D viewDirection, Vector4D viewLocation,
			Vector4D viewDirectionNormal) {
		this.viewDirection = viewDirection;
		this.viewLocation = viewLocation;
		this.viewDirectionNormal = viewDirectionNormal;
		this.viewDirectionCopy = new Vector4D(viewDirection);
		this.viewLocationCopy = new Vector4D(viewLocation);
		this.viewDirectionNormalCopy = new Vector4D(viewDirectionNormal);
	}
	
	public void reset() {
		viewDirection = new Vector4D(viewDirectionCopy);
		viewLocation = new Vector4D(viewLocationCopy);
		viewDirectionNormal = new Vector4D(viewDirectionNormalCopy);
		zoom = 1.0;
	}
	
	protected ViewListenerManager getManager() {
		return manager;
	}
	
	public void addViewListener(ViewListener l) {
		manager.addViewListener(l);
	}
	
	public void removeViewListener(ViewListener l) {
		manager.removeViewListener(l);
	}
	
	public Vector4D getViewDirection() {
		return viewDirection;
	}
	
	public Vector4D getViewLocation() {
		return viewLocation;
	}
	
	public Vector4D getViewDirectionNormal() {
		return viewDirectionNormal;
	}
	
	public double getZoom() {
		return zoom;
	}

	public int getZoomFactor() {
		return zoomFactor;
	}

	public void setZoomFactor(int zoomFactor) {
		this.zoomFactor = zoomFactor;
	}
	
	public void zoomWorldView(double z) {
		z *= -1;
		double oldZoom = zoom;
		if(z > 0.0) {
			zoom *= 1.1;
			manager.notifyViewZoom(false, oldZoom, zoom);
		}
		else {
			zoom *= 0.9;
			manager.notifyViewZoom(false, oldZoom, zoom);
		}
		
	}

	public void setViewDirection(double x, double y, double z) {
		viewDirection = new Vector4D(x, y, z, 0.0);
		manager.notifyViewChanged();
		
	}

	public void setViewLocation(double x, double y, double z) {
		viewLocation = new Vector4D(x, y, z, 1.0);
		manager.notifyViewChanged();
		
	}

	public void setViewNormal(double x, double y, double z) {
		viewDirectionNormal = new Vector4D(x, y, z, 0.0);
		manager.notifyViewChanged();
	}
	
	public void setZoom(double zoom) {
		this.zoom = zoom;
		manager.notifyViewChanged();
	}
	
	
	
	
}
