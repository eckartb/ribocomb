package viewer.view;

import tools3d.*;
import viewer.util.Tools;

/**
 * A perspective view that can be aimed and rotated around a point in 3D space
 * @author Calvin
 *
 */
public class AimablePerspectiveView extends DefaultPerspectiveView {
	
	private Vector4D origin;
	private Vector4D axis = new Vector4D(1.0, 0.0, 0.0, 0.0);
	
	
	/**
	 * Construcs a normal perspective view that doesn't point at anything
	 *
	 */
	public AimablePerspectiveView() {
		super();
	}
	
	
	/**
	 * Constructs aimable perspective view with specified orientation
	 * @param viewDirection Direction view looks in
	 * @param viewLocation Location in 3D space of view
	 * @param viewNormal 'Up' direction of view
	 */
	public AimablePerspectiveView(Vector4D viewDirection, Vector4D viewLocation, Vector4D viewNormal) {
		super(viewDirection, viewLocation, viewNormal);
	}

	/**
	 * Constructs aimable perspective view looking at this point
	 * @param origin Point the view looks at and rotates around
	 */
	public AimablePerspectiveView(Vector4D origin) {
		super();
		this.origin = origin;
		pointAtOrigin();
	}
	
	/**
	 * Get the point the view is currently looking at.
	 * @return Returns the point or null if the view isn't looking
	 * at a specific point.
	 */
	public Vector4D getOrigin() {
		return origin;
	}
	
	/**
	 * Determines if the view is currently pointing at a point
	 * in space
	 * @return Returns true if the view is looking at a point and
	 * false if otherwise
	 */
	public boolean isPointing() {
		return origin != null;
	}
	
	/**
	 * Recalculate direction and normal
	 * vector to look at the specified point
	 * @param origin
	 */
	public void point(Vector4D origin) {
		this.origin = origin;
		
		if(!isPointing())
			return;
		
		pointAtOrigin();
		getManager().notifyViewChanged();
	}
	
	public void lookAt(double x, double y, double z) {
		point(new Vector4D(x, y, z, 1.0));
	}

	@Override
	public void rotateLocalViewX(double angle) {
		if(!isPointing()) {
			super.rotateLocalViewX(angle);
			return;
		}
		
		rotatePointViewX(angle);
	}

	@Override
	public void rotateLocalViewY(double angle) {
		if(!isPointing()) {
			super.rotateLocalViewY(angle);
			return;
		}
		
		rotatePointViewY(angle);
	}

	@Override
	public void rotateLocalViewZ(double angle) {
		if(!isPointing()) {
			super.rotateLocalViewZ(angle);
			return;
		}
		
		rotatePointViewZ(angle);
	}

	@Override
	public void translateLocalView(double x, double y) {
		if(!isPointing()) {
			super.translateLocalView(x, y);
			return;
		}
		
		translatePointView(x, y);
	}

	@Override
	public void zoomLocalView(double zoom) {
		if(!isPointing()) {
			super.zoomLocalView(zoom);
			return;
		}
		
		zoomPointView(zoom);
	}

	@Override
	public void rotateWorldViewX(double angle) {
		if(!isPointing()) {
			super.rotateWorldViewX(angle);
			return;
		}
		
		rotatePointViewX(angle);
	}

	@Override
	public void rotateWorldViewY(double angle) {
		if(!isPointing()) {
			super.rotateWorldViewY(angle);
			return;
		}
		
		rotatePointViewY(angle);
		
	}

	@Override
	public void rotateWorldViewZ(double angle) {
		if(!isPointing()) {
			super.rotateWorldViewZ(angle);
			return;
		}
		
		rotatePointViewZ(angle);
	}

	@Override
	public void translateWorldView(double x, double y) {
		if(!isPointing()) {
			super.translateWorldView(x, y);
			return;
		}
		
		translatePointView(x, y);
	}

	@Override
	public void zoomWorldView(double z) {
		if(!isPointing()) {
			super.zoomWorldView(z);
			return;
		}
		
		zoomPointView(z);
	}
	
	/**
	 * Zooms the view towards the point
	 * @param z Amount to zoom view by
	 */
	public void zoomPointView(double z) {
		super.zoomLocalView(z);
	}
	
	/**
	 * Rotates the view around the x-axis extending through the point
	 * @param angle Amount to rotate point given in degrees
	 */
	public void rotatePointViewX(double angle) {
		angle = angle * Math.PI / 180;
		
		Vector4D viewLocation = getViewLocation();
		viewLocation = viewLocation.minus(origin);
		viewLocation = Tools.rotate(viewLocation, axis, angle);
		setViewDirection(Tools.rotate(getViewDirection(), axis, angle));
		setViewNormal(Tools.rotate(getViewDirectionNormal(), axis, angle));
		viewLocation.add(origin);
		setViewLocation(viewLocation);
		getManager().notifyViewChanged();
	}
	
	/**
	 * Rotates the view around the y-axis extending through the point
	 * @param angle Amount to rotate around point given in degrees
	 */
	public void rotatePointViewY(double angle) {
		angle = angle * Math.PI / 180;
		Vector4D viewLocation = getViewLocation();
		viewLocation = viewLocation.minus(origin);
		Matrix4D matrix = new Matrix4D(1.0);
		double cos = Math.cos(angle);
		double sin = Math.sin(angle);
		matrix.setXX(cos);
		matrix.setXZ(sin);
		matrix.setZX(-sin);
		matrix.setZZ(cos);
		viewLocation = matrix.multiply(viewLocation);
		axis = matrix.multiply(axis);
		viewLocation.add(origin);
		setViewLocation(viewLocation);
		setViewDirection(matrix.multiply(getViewDirection()));
		setViewNormal(matrix.multiply(getViewDirectionNormal()));
		getManager().notifyViewChanged();
	}
	
	/**
	 * Rotate view around the z-axis extending through the point
	 * @param angle Amount to rotate around the point given in degrees
	 */
	public void rotatePointViewZ(double angle) {
		super.rotateLocalViewZ(angle);
	}
	
	/**
	 * Translates the view about the point. The percieved affect of translation based
	 * on a point in space is odd.
	 * @param x Amount to translate
	 * @param y Amount to translate
	 */
	public void translatePointView(double x, double y) {
		Vector4D viewLocation = getViewLocation();
		
		
		viewLocation = Tools.translate(viewLocation, Tools.Z_AXIS, y);
		viewLocation = Tools.translate(viewLocation, Tools.X_AXIS, x);
		setViewLocation(viewLocation);

		pointAtOrigin();
		getManager().notifyViewChanged();
	}
	
	/**
	 * Points the view at the origin and performs all necessary
	 * recalculations for normal vector
	 *
	 */
	private void pointAtOrigin() {
		Vector4D oldDirection = getViewDirection();
		Vector4D newDirection = origin.minus(getViewLocation());
		newDirection.normalize();
	
		Vector4D x = Tools.X_AXIS.mul(newDirection.getX());
		Vector4D y = Tools.Y_AXIS.mul(newDirection.getY());
		Vector4D z = Tools.Z_AXIS.mul(newDirection.getZ());
		
		double xAngle = Tools.angle(x, oldDirection);
		double yAngle = Tools.angle(y, oldDirection);
		double zAngle = Tools.angle(z, oldDirection);
		
		double xSin = Math.sin(xAngle);
		double xCos = Math.cos(xAngle);
		double ySin = Math.sin(yAngle);
		double yCos = Math.cos(yAngle);
		double zSin = Math.sin(zAngle);
		double zCos = Math.cos(zAngle);
		
		Matrix4D xMatrix = new Matrix4D(1.0, 0.0, 0.0, 0.0,
										0.0, xCos, -xSin, 0.0,
										0.0, xSin, xCos, 0.0,
										0.0, 0.0, 0.0, 1.0);
		Matrix4D yMatrix = new Matrix4D(yCos, 0.0, ySin, 0.0,
										0.0, 1.0, 0.0, 0.0,
										-ySin, 0.0, yCos, 0.0,
										0.0, 0.0, 0.0, 1.0);
		Matrix4D zMatrix = new Matrix4D(zCos, -zSin, 0.0, 0.0,
										zSin, zCos, 0.0, 0.0,
										0.0, 0.0, 1.0, 0.0,
										0.0, 0.0, 0.0, 1.0);
		
		Vector4D viewNormal = getViewDirectionNormal();
		Vector4D xNorm = new Vector4D(viewNormal.getX(), 0.0, 0.0, 0.0);
		Vector4D yNorm = new Vector4D(0.0, viewNormal.getY(), 0.0, 0.0);
		Vector4D zNorm = new Vector4D(0.0, 0.0, viewNormal.getZ(), 0.0);
		Vector4D xAxis = new Vector4D(axis.getX(), 0.0, 0.0, 0.0);
		Vector4D yAxis = new Vector4D(0.0, axis.getY(), 0.0, 0.0);
		Vector4D zAxis = new Vector4D(0.0, 0.0, axis.getZ(), 0.0);
		
		xNorm = xMatrix.multiply(xNorm);
		yNorm = yMatrix.multiply(yNorm);
		zNorm = zMatrix.multiply(zNorm);
		xAxis = xMatrix.multiply(xAxis);
		yAxis = yMatrix.multiply(yAxis);
		zAxis = zMatrix.multiply(zAxis);
		
		
		setViewNormal(new Vector4D(xNorm.getX(), yNorm.getY(), zNorm.getZ(), 0.0));
		axis = new Vector4D(xAxis.getX(), yAxis.getY(), zAxis.getZ(), 0.0);
		setViewDirection(newDirection);
	}

	public void reset() {
		super.reset();
		axis = new Vector4D(1.0, 0.0, 0.0, 0.0);
	}
	
	
	
	
	
	
}
