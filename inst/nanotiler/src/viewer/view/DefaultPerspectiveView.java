package viewer.view;


import tools3d.Vector4D;
import tools3d.Matrix4D;
import viewer.event.*;
import viewer.util.Tools;

/**
 * Default implementation of perspective view
 * @author Calvin
 *
 */
public class DefaultPerspectiveView implements PerspectiveView {
	
	private Vector4D viewDirection = new Vector4D(0.0, 0.0, -1.0, 0.0);
	private Vector4D viewLocation = new Vector4D(0.0, 0.0, 0.0, 0.0);
	private Vector4D viewNormal = new Vector4D(0.0, 1.0, 0.0, 0.0);
	
	private Vector4D viewDirectionSaved;
	private Vector4D viewLocationSaved;
	private Vector4D viewNormalSaved;
	
	private ViewListenerManager manager = new ViewListenerManager();
	
	/**
	 * Constructs a perspective view with a default orientation of looking down
	 * the negative Z axis and located at the origin with a normal point
	 * up the positive Y axis.
	 *
	 */
	public DefaultPerspectiveView() {
		this(new Vector4D(0.0, 0.0, -1.0, 0.0), new Vector4D(0.0, 0.0, 0.0, 0.0), new Vector4D(0.0, 1.0, 0.0, 0.0));
	}
	
	/**
	 * Allows the user to specify the orientation for the perspective view
	 * @param viewDirection Direction the view points
	 * @param viewLocation Location of the view in 3D space
	 * @param viewNormal 'Up' direction of the view
	 */
	public DefaultPerspectiveView(Vector4D viewDirection, Vector4D viewLocation, Vector4D viewNormal) {
		this.viewDirection = viewDirection;
		this.viewLocation = viewLocation;
		this.viewNormal = viewNormal;
		
		viewDirectionSaved = (Vector4D) viewDirection.clone();
		viewLocationSaved = (Vector4D) viewLocation.clone();
		viewNormalSaved = (Vector4D) viewNormal.clone();
	}

	public void reset() {
		viewDirection = (Vector4D) viewDirectionSaved.clone();
		viewLocation = (Vector4D) viewLocationSaved.clone();
		viewNormal = (Vector4D) viewNormalSaved.clone();
	}
	
	public Vector4D getViewDirectionNormal() {
		return viewNormal;
	}
	
	public ViewListenerManager getManager() {
		return manager;
	}
	
	public void lookAt(double x, double y, double z) {
		/*Vector4D point = new Vector4D(x, y, z, 1.0);
		if(point.equals(viewLocation))
			return;
		
		
		viewDirection = viewLocation.minus(point);
		viewDirection.normalize();
		
		*/
	}

	public void rotateLocalViewX(double angle) {
		angle = angle * Math.PI / 180;
		
	

		Vector4D axis = viewNormal.cross(viewDirection);
		axis.normalize();

		Vector4D oldDirection = viewDirection;
		viewDirection = Tools.rotate(viewDirection, axis, angle);
		Vector4D oldNormal = viewNormal;
		viewNormal = Tools.rotate(viewNormal, axis, angle);
		manager.notifyViewRotation(true, oldDirection, oldNormal, viewDirection, viewNormal);


	}
	
	public void addViewListener(ViewListener l) {
		manager.addViewListener(l);
	}
	
	public void removeViewListener(ViewListener l) {
		manager.removeViewListener(l);
	}

	public void rotateLocalViewY(double angle) {
		angle = angle * Math.PI / 180;
		
		
		Vector4D oldDirection = viewDirection;
		viewDirection = Tools.rotate(viewDirection, viewNormal, angle);

		manager.notifyViewRotation(true, oldDirection, viewNormal, viewDirection, viewNormal);
	}

	public void rotateLocalViewZ(double angle) {
		angle = angle * Math.PI / 180;
	
		
		Vector4D oldNormal = viewNormal;
		viewNormal = Tools.rotate(viewNormal, viewDirection, angle);

		manager.notifyViewRotation(true, viewDirection, oldNormal, viewDirection, viewNormal);
	}

	public void rotateWorldViewX(double angle) {
		angle = (-angle) * Math.PI / 180;
		Matrix4D rotation = new Matrix4D(1);
		double cos = Math.cos(angle);
		double sin = Math.sin(angle);
		rotation.setYY(cos);
		rotation.setYZ(-sin);
		rotation.setZY(sin);
		rotation.setZZ(cos);
		Vector4D oldDirection = viewDirection;
		viewDirection = rotation.multiply(viewDirection);
		manager.notifyViewRotation(false, oldDirection, viewNormal, viewDirection, viewNormal);
	}

	public void rotateWorldViewY(double angle) {
		angle = (-angle) * Math.PI / 180;
		Matrix4D rotation = new Matrix4D(1);
		double cos = Math.cos(angle);
		double sin = Math.sin(angle);
		rotation.setXX(cos);
		rotation.setXZ(sin);
		rotation.setZX(-sin);
		rotation.setZZ(cos);
		Vector4D oldDirection = viewDirection;
		viewDirection = rotation.multiply(viewDirection);
		manager.notifyViewRotation(false, oldDirection, viewNormal, viewDirection, viewNormal);

	}

	public void rotateWorldViewZ(double angle) {
		angle = (-angle) * Math.PI / 180;
		Matrix4D rotation = new Matrix4D(1);
		double cos = Math.cos(angle);
		double sin = Math.sin(angle);
		rotation.setXX(cos);
		rotation.setXY(-sin);
		rotation.setYX(sin);
		rotation.setYY(cos);
		Vector4D oldDirection = viewDirection;
		viewDirection = rotation.multiply(viewDirection);
		manager.notifyViewRotation(false, oldDirection, viewNormal, viewDirection, viewNormal);

	}

	public void translateLocalView(double x, double y) {
		Vector4D axis = viewDirection.cross(viewNormal);

		Vector4D oldLocation = viewLocation;
		viewLocation = Tools.translate(viewLocation, viewNormal, y);
		viewLocation = Tools.translate(viewLocation, axis, x);
		manager.notifyViewTranslation(true, oldLocation, viewLocation);
		
		
	}

	public void zoomLocalView(double zoom) {
		Vector4D oldLocation = viewLocation;
		viewLocation = Tools.translate(viewLocation, viewDirection, -zoom);
		manager.notifyViewTranslation(true, oldLocation, viewLocation);
	}

	public void flipView() {
		viewDirection = viewDirection.mul(-1);

	}

	public Vector4D getViewDirection() {
		return viewDirection;
	}

	public Vector4D getViewLocation() {
		return viewLocation;
	}

	public void translateWorldView(double x, double y) {
		Vector4D translation = new Vector4D(x, y, 0.0, 0.0);
		Vector4D oldLocation = new Vector4D(viewLocation);
		viewLocation.add(translation);
		manager.notifyViewTranslation(false, oldLocation, viewLocation);

	}

	public void zoomWorldView(double z) {
		Vector4D oldLocation = new Vector4D(viewLocation);
		Vector4D translation = new Vector4D(0.0, 0.0, z, 0.0);
		viewLocation.add(translation);
		manager.notifyViewTranslation(false, oldLocation, viewLocation);
	}
	
	public double getZoom() {
		return 1.0;
	}

	/**
	 * Programatically set the view direction. Does not query
	 * the manager to notify listeners.
	 * @param viewDirection
	 */
	public void setViewDirection(Vector4D viewDirection) {
		this.viewDirection = viewDirection;
	}

	/**
	 * Programatically set the view location. Does not query the manager
	 * to notify listeners.
	 * @param viewLocation
	 */
	public void setViewLocation(Vector4D viewLocation) {
		this.viewLocation = viewLocation;
	}

	/**
	 * Programatically set the view normal. Does not querty the
	 * manager to notify listeners.
	 * @param viewNormal
	 */
	public void setViewNormal(Vector4D viewNormal) {
		this.viewNormal = viewNormal;
	}
	
	public void setViewDirection(double x, double y, double z) {
		viewDirection = new Vector4D(x, y, z, 0.0);
		manager.notifyViewChanged();
		
	}

	public void setViewLocation(double x, double y, double z) {
		viewLocation = new Vector4D(x, y, z, 1.0);
		manager.notifyViewChanged();
		
	}

	public void setViewNormal(double x, double y, double z) {
		viewNormal = new Vector4D(x, y, z, 0.0);
		manager.notifyViewChanged();
	}
	
	/**
	 * Does nothing. The concept of zooming in 3D space doesn't
	 * exist. A zoom is merely translation along a specific axis.
	 */
	public void setZoom(double zoom) {
		
	}

	
	
	
	
	

}
