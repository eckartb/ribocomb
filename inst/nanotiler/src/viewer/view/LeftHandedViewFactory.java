package viewer.view;

import tools3d.Vector4D;

/**
 * In a left handed coordinate system, the camera defaults
 * to pointing down the -z axis. We will define this view
 * as 'front'. Therefore, the front of the object is facing the +z
 * axis. The left of object is facing the +x axis and therefore,
 * the left view is looking down the -x axis. The top of the object
 * faces the +y axis; therefore, the top view is looking down
 * the -y axis.
 * 
 * 
 * @author calvin
 *
 */
public class LeftHandedViewFactory implements ViewFactory {
	
	public static final Object TOP_PERSPECTIVE_VIEW = new String("Top");
	public static final Object HORIZON_PERSPECTIVE_VIEW = new String("Horizon");
	
	public OrthogonalView createLeftView() {
		return new YZView();
	}
	
	public OrthogonalView createRightView() {
		OrthogonalView right = new YZView();
		right.flipView();
		return right;
	}
	
	public OrthogonalView createTopView() {
		return new XZView();
	}
	
	public OrthogonalView createBottomView() {
		OrthogonalView bottom = new XZView();
		bottom.flipView();
		return bottom;
	}
	
	public OrthogonalView createFrontView() {
		return new XYView();
	}
	
	public OrthogonalView createBackView() {
		OrthogonalView back = new XYView();
		back.flipView();
		return back;
	}
	
	public PerspectiveView createPerspectiveView(Object option) {
		if(option.equals(TOP_PERSPECTIVE_VIEW))
			return createTopPerspectiveView();
		
		return createHorizonPerspectiveView();
	}
	
	/**
	 * Create a perspective view looking down from the top, like a top orthogonal view
	 * @return Returns the perspective view
	 */
	public PerspectiveView createTopPerspectiveView() {
		return new AimablePerspectiveView(new Vector4D(0.0, -1.0, 0.0, 0.0), new Vector4D(0.0, 50.0, 0.0, 1.0), new Vector4D(0.0, 0.0, -1.0, 0.0));
	}
	
	/**
	 * Create a perspective view looking forward, down the -z axis like a front orthogonal view
	 * @return Returns the perspective view
	 */
	public PerspectiveView createHorizonPerspectiveView() {
		return new AimablePerspectiveView(new Vector4D(0.0, 0.0, -1.0, 0.0), new Vector4D(0.0, 0.0, 50.0, 1.0), new Vector4D(0.0, 1.0, 0.0, 0.0));
	}
	
	                                               
}
