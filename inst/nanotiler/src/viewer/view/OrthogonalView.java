package viewer.view;

import tools3d.*;

import viewer.event.*;

/**
 * Any display that provides an orthogonal view should
 * implement this interface
 * @author grunewac
 *
 */
public interface OrthogonalView {
	
	public void translateWorldView(double x, double y);
	public void zoomWorldView(double z);
	public Vector4D getViewDirection();
	public void setViewDirection(double x, double y, double z);
	public void setViewLocation(double x, double y, double z);
	public void setViewNormal(double x, double y, double z);
	public Vector4D getViewLocation();
	public void flipView();
	public Vector4D getViewDirectionNormal();
	public void addViewListener(ViewListener l);
	public double getZoom();
	public void reset();
	public void lookAt(double x, double y, double z);
	public void setZoom(double zoom);
}
