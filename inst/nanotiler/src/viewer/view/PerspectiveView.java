package viewer.view;



public interface PerspectiveView extends OrthogonalView {
	public void rotateWorldViewX(double angle);
	public void rotateWorldViewY(double angle);
	public void rotateWorldViewZ(double angle);
	public void rotateLocalViewX(double angle);
	public void rotateLocalViewZ(double angle);
	public void rotateLocalViewY(double angle);
	public void translateLocalView(double x, double y);
	public void zoomLocalView(double z);
	
}
