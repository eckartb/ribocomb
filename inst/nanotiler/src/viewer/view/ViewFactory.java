package viewer.view;

/**
 * The orientation of views have different meanings
 * depending on the coordinate system. Hide coordinate system
 * details by implementing this interface for specific coordinate systems
 * @author calvin
 *
 */
public interface ViewFactory {
	public OrthogonalView createLeftView();
	
	public OrthogonalView createRightView();
	
	public OrthogonalView createTopView();
	
	public OrthogonalView createBottomView();
	
	public OrthogonalView createFrontView();
	
	public OrthogonalView createBackView();
	
	public PerspectiveView createPerspectiveView(Object option);
}