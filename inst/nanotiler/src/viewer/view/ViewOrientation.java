package viewer.view;

public enum ViewOrientation {
	Left("Left"), Right("Right"), Top("Top"), Bottom("Bottom"), 
	Front("Front"), Back("Back"), PerspectiveTop("Top Perspective"), PerspectiveHorizon("Horizon Persepective");
	
	private String name;
	
	private ViewOrientation(String name) {
		this.name = name;
	}
	
	public String toString() {
		return name;
	}
	
	public boolean isPerspective() {
		return this == PerspectiveTop || this == PerspectiveHorizon;
	}
}
