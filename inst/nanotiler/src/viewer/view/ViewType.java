package viewer.view;

public enum ViewType {
	Orthogonal, Perspective;
}
