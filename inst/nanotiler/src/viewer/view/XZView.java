package viewer.view;

import tools3d.*;

public class XZView extends AbstractOrthogonalView {
	
	private int scalar = 1;

	public XZView() {
		super(new Vector4D(0.0, -1.0, 0.0, 0.0), new Vector4D(0.0, 1.0, 0.0, 1.0),
				new Vector4D(0.0, 0.0, -1.0, 0.0));
	}
	
	public void lookAt(double x, double y, double z) {
		Vector4D oldLocation = new Vector4D(viewLocation);
		viewLocation.setX(x);
		viewLocation.setZ(z);
		getManager().notifyViewTranslation(false, oldLocation, viewLocation);
	}
	
	public void flipView() {
		viewDirection.setY(viewDirection.getY() * -1);
		viewLocation.setY(viewLocation.getY() * -1);
		scalar *= -1;

	}

	public void translateWorldView(double x, double y) {

		Vector4D translation = new Vector4D(-1 * scalar * x, 0.0, -1 * scalar * y, 0.0);
		Vector4D oldLocation = new Vector4D(viewLocation);

		viewLocation.add(translation);

		getManager().notifyViewTranslation(false, oldLocation, viewLocation);

	}



}
