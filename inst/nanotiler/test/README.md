# NanoTiler Test Suite

The NanoTiler test suite is based on the TestNG test framework. It can be activated by the commands `runtestng_newest.sh` or `runtestng_dev1.sh` or `runtestng_dev2.sh`. It is based on several XML files that specify which packages and keywords to include in the test.

## Test-suite keywords:

* newest
* current
* new
* slow
* tight
* outdated

