package nanotilertests.graphtools;

import generaltools.TestTools;
import org.testng.*;
import org.testng.annotations.*;
import graphtools.*;

public class BipartiteGraphPermutatorTest {


    @Test(groups={"new"})
    public void testBipartiteGraphPermutator() {
	String methodName = "testBipartiteGraphPermutator";
	System.out.println(TestTools.generateMethodHeader(methodName));
	IntegerPermutator gen = new BipartiteGraphPermutator(5,2); // toLen, minvalue, maxvalue, how many different
	int count = 0;
	System.out.println("Results of testBipartiteGraphPermutator");
	do {
	    ++count;
	    int[] x = gen.get();
	    System.out.print("" + count + " : ");
 	    GraphTools.printVec(System.out, x);
	    
// 	    assert gen.validate();
	    System.out.println("" + gen.validate());
	    //	    assert constraint.validate(x); // no more than two different connections
	}
	while (gen.hasNext() && gen.inc());
	System.out.println(TestTools.generateMethodFooter(methodName));
	assert count == 15;
    }

    /* 
    @Test(groups={"new"})
    public void testMappedMaxDiffIntegerPermutatorRunToEnd() {
	String methodName = "testMappedMaxDiffIntegerPermutator";
	System.out.println(TestTools.generateMethodHeader(methodName));
	MappedMaxDiffIntegerPermutator gen = new MappedMaxDiffIntegerPermutator(6,3,6,2); // toLen, minvalue, maxvalue, how many different
	IntegerPermutatorTools.testIntegerPermutatorRunToEnd(gen);
	System.out.println(TestTools.generateMethodFooter(methodName));
    }
    */



}
