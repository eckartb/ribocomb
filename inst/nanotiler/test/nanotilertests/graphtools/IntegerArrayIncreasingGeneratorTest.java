package nanotilertests.graphtools;

import java.io.*;
import java.util.*;
import generaltools.TestTools;
import org.testng.*;
import org.testng.annotations.*;
import graphtools.*;

public class IntegerArrayIncreasingGeneratorTest {

    public IntegerArrayIncreasingGeneratorTest() { }

    private void printVec(PrintStream ps, int[] x) {
        for (int n : x) {
            ps.print("" + n + " ");
        }
    }


  @Test(groups={"new"})
  public void testIntegerArrayIncreasingGenerator2Verbose() {
    String methodName = "testIntegerArrayIncreasingGenerator2";
    System.out.println(TestTools.generateMethodHeader(methodName));
    int len = 4;
    int base = 6;
    System.out.println("Length: " + len + " base : " + base);
    IntegerArrayIncreasingGenerator gen = new IntegerArrayIncreasingGenerator(len,base);
    int count = 0;
    System.out.println("Results of integer Array generator");
    do {
	int[] x = gen.get();
	assert x != null;
	printVec(System.out,x);
	System.out.println();
	++count;
    }
    while (gen.hasNext() && (gen.next() != null));
    System.out.println("Found " + count + " solutions. Last id: " + gen);
    assert count == 15;
    System.out.println(TestTools.generateMethodFooter(methodName));
}

  @Test(groups={"new"})
  public void testIntegerArrayIncreasingGenerator2() {
      String methodName = "testIntegerArrayIncreasingGenerator2Quiet";
      // System.out.println(TestTools.generateMethodHeader(methodName));
      int len = 4;
      int base = 6;
      // System.out.println("Length: " + len + " base : " + base);
      IntegerArrayIncreasingGenerator gen = new IntegerArrayIncreasingGenerator(len,base);
      int count = 0;
      // System.out.println("Results of integer Array generator");
      do {
	  int[] x = gen.get();
	  assert x != null;
	  // printVec(System.out,x);
	  // System.out.println();
	  ++count;
      }
      while (gen.hasNext() && (gen.next() != null));
      // System.out.println("Found " + count + " solutions. Last id: " + gen);
      assert count == 15;
      // System.out.println(TestTools.generateMethodFooter(methodName));
  }
    
}
