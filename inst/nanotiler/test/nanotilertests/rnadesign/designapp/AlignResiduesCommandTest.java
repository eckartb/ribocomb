package nanotilertests.rnadesign.designapp;

import commandtools.*;
import generaltools.TestTools;
import org.testng.annotations.*;
import rnadesign.designapp.*;

public class AlignResiduesCommandTest {

    public AlignResiduesCommandTest() { }
    
    @Test(groups={"new"}) 
    public void testAlignResidues() {
	String methodName = "testAlignResidues";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/cube6_C4.pdb name=cube");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/2OIU.rnaview.pdb_j3_P-C7_P-C17_P-A41.filt2.pdb name=motif");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("align B:1-5 A_1:1-5 root.cube root.motif");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	} catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

}
