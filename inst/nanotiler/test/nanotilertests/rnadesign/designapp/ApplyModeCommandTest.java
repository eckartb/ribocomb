package nanotilertests.rnadesign.designapp;

import generaltools.TestTools;
import org.testng.annotations.*; // for testing
import commandtools.CommandException;
import rnadesign.designapp.*;

/** Tests class ApplyModeCommand, corresponding to command "applymode" */
public class ApplyModeCommandTest {

    @Test (groups={"new", "incomplete"})
    /** Reads triangular RNA structure and applies normal mode analysis. */
    public void testApplyModeCommand1(){
	String methodName = "testApplyModeCommand1";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1Y69.pdb_j2_0-A1004_0-U1170_chain_ring1122l00_align.pdb findstems=false");
	    assert app.getGraphController().getGraph().getGraph().size() > 0; // there must be objects defined
	    app.runScriptLine("elastic class=Nucleotide3D root=root.import");
	    app.runScriptLine("applymode"); // apply first mode!
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandException e) {
	    System.out.println(e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test (groups={"new", "incomplete"})
    /** Reads square as points file and applies normal mode analysis. */
    public void testApplyModeCommandToSquare(){
	String methodName = "testApplyModeCommandToSquare";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/square.points");
	    assert app.getGraphController().getGraph().getGraph().size() > 0; // there must be objects defined
	    app.runScriptLine("tree");
	    app.runScriptLine("elastic class=Object3D root=root.import");
	    for (int mode = 1; mode <= 4; ++mode) {
		String fileName = "square_m" + mode;
		app.runScriptLine("applymode mode=" + mode + " file=" + fileName); // apply first mode!
	    }
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandException e) {
	    System.out.println(e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


}
