package nanotilertests.rnadesign.designapp;

import java.util.Properties;
import generaltools.TestTools;
import commandtools.CommandException;
import org.testng.annotations.*;
import rnadesign.designapp.*;

public class BridgeAllCommandTest {

    /** Imoports one cube corner and attempts to fuse its strands */
    @Test(groups={"new","slow"})
    public void testBridgeAllCubeCorner1() {
	String methodName = "testBridgeAllCubeCorner1";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/testTraceRnaGraphCommandSmallCube_traced.save0.pdb");
	    int numSeqs = app.getGraphController().getSequences().getSequenceCount();
	    app.runScriptLine("exportpdb " + methodName + "_unbridged.pdb");
	    app.runScriptLine("bridgeall root.import rms=3 min=1");
	    int numSeqs2 = app.getGraphController().getSequences().getSequenceCount();
	    app.runScriptLine("exportpdb " + methodName + "_bridged.pdb");
	    if (numSeqs2 <= numSeqs) {
		// at least one bridge must be generated in order for the test to succeed
		System.out.println("No bridges could be generated!");
		assert false;
	    }
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Imoports one cube corner and attempts to fuse its strands */
    @Test(groups={"new","slow"})
    public void testBridgeAllCubeCorner2() {
	String methodName = "testBridgeAllCubeCorner2";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    //	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/testGenJunctionCommandCube.pdb");
	    int numSeqs = app.getGraphController().getSequences().getSequenceCount();
	    app.runScriptLine("exportpdb " + methodName + "_unbridged.pdb");
	    app.runScriptLine("bridgeall root.import rms=2 max=2");
	    int numSeqs2 = app.getGraphController().getSequences().getSequenceCount();
	    app.runScriptLine("exportpdb " + methodName + "_bridged.pdb");
	    if (numSeqs2 <= numSeqs) {
		// at least one bridge must be generated in order for the test to succeed
		System.out.println("No bridges could be generated!");
		assert false;
	    }
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


}
