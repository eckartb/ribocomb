package nanotilertests.rnadesign.designapp;

import java.util.Properties;
import generaltools.TestTools;
import commandtools.CommandException;
import org.testng.annotations.*;
import rnadesign.designapp.*;
import rnadesign.rnamodel.*;
import sequence.*;
import tools3d.objects3d.Object3D;

public class BridgeItCommandTest {

    /** Imports one cube corner and attempts to fuse its strands */
    @Test(groups={"new","slow"})
    public void bridgeCubeCorner1() {
	String methodName = "bridgeCubeCorner1";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/fuseAllCube_unfused_corner_QR.pdb");
	    assert app.getGraphController().getSequences().getSequenceCount() == 2; // 6 strands before fusing
	    app.runScriptLine("bridgeit root.import.A.G27 root.import.B.C1 rms=2 min=1");
	    app.runScriptLine("exportpdb " + methodName + "_bridged.pdb");
	    assert app.getGraphController().getSequences().getSequenceCount() == 2; // no bridge, because strands are already hydrogen bonded!
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Imports one cube corner and attempts to fuse its strands */
    @Test(groups={"new","slow"})
    public void bridgeCubeCorner2() {
	String methodName = "bridgeCubeCorner2";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/fuseAllCube_unfused_corner_AC.pdb");
	    assert app.getGraphController().getSequences().getSequenceCount() == 2;
	    app.runScriptLine("bridgeit root.import.A.27 root.import.B.1 rms=2 min=1");
	    app.runScriptLine("exportpdb " + methodName + "_bridged.pdb");
	    assert app.getGraphController().getSequences().getSequenceCount() == 3; // there must be a new bridge
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Bridges two single stranded nucleotides */
    @Test(groups={"new", "slow"})
    public void testBridgeIt1() {
	String methodName = "testBridgeIt1";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1OOA_missing4n.pdb");
	    app.runScriptLine("tree allowed=Nucleotide3D");
	    String name1 = "root.import.A.C1";
	    String name2 = "root.import.B.G21";
	    Properties resultProperties = app.runScriptLine("bridgeit " + name1 + " " + name2);
	    System.out.println("Result of " + methodName + " : " 
			       + resultProperties);
	    assert name1.equals(resultProperties.getProperty("name1"));
	    assert name2.equals(resultProperties.getProperty("name2"));
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    // app.runScriptLine("bridgeit root.pdb.B.C1 root.pdb.A.G21");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Same as test 1, but using solution 2 */
    @Test(groups={"slow","new"})
    public void testBridgeIt2() {
	String methodName = "testBridgeIt2";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1OOA_missing4n.pdb");
	    app.runScriptLine("tree allowed=Nucleotide3D");
	    String name1 = "root.import.A.C1";
	    String name2 = "root.import.B.G21";
	    Properties resultProperties = app.runScriptLine("bridgeit " + name1 + " " + name2 + " n=2");
	    System.out.println("Result of " + methodName + " : " 
			       + resultProperties);
	    assert name1.equals(resultProperties.getProperty("name1"));
	    assert name2.equals(resultProperties.getProperty("name2"));
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    // app.runScriptLine("bridgeit root.pdb.B.C1 root.pdb.A.G21");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Same as test 1 and 2, but different order of nucleotides! */
    @Test(groups={"new","slow"})
    public void testBridgeIt3() {
	String methodName = "testBridgeIt3";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1OOA_missing4n.pdb");
	    app.runScriptLine("tree allowed=Nucleotide3D");
	    String name2 = "root.import.A.C1";
	    String name1 = "root.import.B.G21"; // other way around!!
	    Properties resultProperties = app.runScriptLine("bridgeit " + name1 + " " + name2 + " n=1");
	    System.out.println("Result of " + methodName + " : " 
			       + resultProperties);
	    assert name1.equals(resultProperties.getProperty("name1"));
	    assert name2.equals(resultProperties.getProperty("name2"));
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    // app.runScriptLine("bridgeit root.pdb.B.C1 root.pdb.A.G21");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Same as test 3, but adding two strands! */
    @Test(groups={"new","slow"})
    public void testBridgeIt4() {
	String methodName = "testBridgeIt4";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1OOA_missing4n.pdb");
	    app.runScriptLine("tree allowed=Nucleotide3D");
	    String name2 = "root.import.A.C1";
	    String name1 = "root.import.B.G21"; // other way around!!
	    String name4 = "root.import.B.C1";
	    String name3 = "root.import.A.G21"; // other way around!!
	    Properties resultProperties = app.runScriptLine("bridgeit " + name1 + " " + name2 + " n=1");
	    System.out.println("Result of " + methodName + " : " + resultProperties);
	    resultProperties = app.runScriptLine("bridgeit " + name3 + " " + name4 + " n=1");
// 	    assert name1.equals(resultProperties.getProperty("name1"));
// 	    assert name2.equals(resultProperties.getProperty("name2"));
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    // app.runScriptLine("bridgeit root.pdb.B.C1 root.pdb.A.G21");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Same as test 3, but adding two strands! */
    @Test(groups={"new", "slow"})
    public void testBridgeItTwoSingleStrands3n() {
	String methodName = "testBridgeItTwoSingleStrands3n";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1OOA_missing3n.pdb");
	    app.runScriptLine("tree allowed=Nucleotide3D");
	    String name2 = "root.import.A.1";
	    String name1 = "root.import.B.23"; // other way around!!
	    String name4 = "root.import.B.1";
	    String name3 = "root.import.A.23"; // other way around!!
	    Properties resultProperties = app.runScriptLine("bridgeit " + name1 + " " + name2 + " n=1");
	    System.out.println("Result of " + methodName + " : " 
			       + resultProperties);
	    resultProperties = app.runScriptLine("bridgeit " + name3 + " " + name4 + " n=1");
// 	    assert name1.equals(resultProperties.getProperty("name1"));
// 	    assert name2.equals(resultProperties.getProperty("name2"));
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    // app.runScriptLine("bridgeit root.pdb.B.C1 root.pdb.A.G21");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Same as test 3, but adding two strands! */
    @Test(groups={"new", "slow"})
    public void testBridgeItTwoSingleStrandsToCube() {
	String methodName = "testBridgeItTwoSingleStrandsToCube";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/testCubeFragmentPlacement.pdb findstems=false");
	    app.runScriptLine("tree strand");
	    String name1 = "root.import.B.44";
	    String name2 = "root.import.H.1"; // other way around!!
	    String name3 = "root.import.F.44";
	    String name4 = "root.import.G.1"; // other way around!!
	    Properties resultProperties = app.runScriptLine("bridgeit " + name1 + " " + name2 + " rms=3.0 n=5 l=6");
	    System.out.println("Result of " + methodName + " : " 
			       + resultProperties);
	    resultProperties = app.runScriptLine("bridgeit " + name3 + " " + name4 + " rms=3.0 n=5 l=6");
// 	    assert name1.equals(resultProperties.getProperty("name1"));
// 	    assert name2.equals(resultProperties.getProperty("name2"));
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    // app.runScriptLine("bridgeit root.pdb.B.C1 root.pdb.A.G21");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Same as test 3, but adding two strands simultaneously! */
    @Test(groups={"new", "slow"})
    public void testBridgeItTwoSingleStrands3nCombined() {
	String methodName = "testBridgeItTwoSingleStrands3nCombined";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1OOA_missing3n.pdb");
	    app.runScriptLine("tree allowed=Nucleotide3D");
	    String name2 = "root.import.A.1";
	    String name1 = "root.import.B.23"; // other way around!!
	    String name4 = "root.import.B.1";
	    String name3 = "root.import.A.23"; // other way around!!
	    Properties resultProperties = app.runScriptLine("bridgeit " + name1 + " " + name2 + " " + name3 + " " + name4 + " n=50 combined=true rms=2.0 prefix=" + methodName);
	    System.out.println("Result of " + methodName + " : " + resultProperties);
// 	    assert name1.equals(resultProperties.getProperty("name1"));
// 	    assert name2.equals(resultProperties.getProperty("name2"));
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    // app.runScriptLine("bridgeit root.pdb.B.C1 root.pdb.A.G21");
	    assert resultProperties.getProperty("name3") != null; // make sure combined mode was used
	    assert resultProperties.getProperty("name4") != null;
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** First test of finding one briding kissing loop motif between two helices */
    @Test(groups={"new", "slow"})
    public void testHelixBridgeIt1OOA() {
	String methodName = "testHelixBridgeIt1OOA";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1OOA.pdb");
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("tree");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/db/rnajunction/orig/k2_rep/subset.names ${NANOTILER_HOME}/db/rnajunction/orig/k2_rep 2");
	    app.runScriptLine("status");
	    String name2 = "root.import.stems.stemA_1.A_29_A_1";
	    String name1 = "root.import.stems.stemB_1.B_29_B_1";
	    Properties resultProperties = app.runScriptLine("bridgeit " + name1 + " " + name2 + " n=1 rms=30");
	    System.out.println("Result of " + methodName + " : " + resultProperties);
	    assert name1.equals(resultProperties.getProperty("name1"));
	    assert name2.equals(resultProperties.getProperty("name2"));
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    // app.runScriptLine("bridgeit root.pdb.B.C1 root.pdb.A.G21");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** First test of finding one briding kissing loop motif between two helices */
    @Test(groups={"new", "slow"})
    public void testHelixBridgeIt1OOA_missing() {
	String methodName = "testHelixBridgeIt1OOA_missing";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1OOA_missing3n.pdb");
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("tree");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/db/rnajunction/filt/k2_rep/rnaviewdir/subset.names ${NANOTILER_HOME}/db/rnajunction/filt/k2_rep/rnaviewdir 0");
	    app.runScriptLine("status");
	    String name2 = "root.import.stems.stemA_1.A_23_A_1";
	    String name1 = "root.import.stems.stemB_1.B_23_B_1";
	    Properties resultProperties = app.runScriptLine("bridgeit " + name1 + " " + name2 + " n=1 rms=100 angle=50");
	    System.out.println("Result of " + methodName + " : " + resultProperties);
	    assert name1.equals(resultProperties.getProperty("name1"));
	    assert name2.equals(resultProperties.getProperty("name2"));
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    // app.runScriptLine("bridgeit root.pdb.B.C1 root.pdb.A.G21");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** First test of finding two briding kissing loop motifs between two helices */
    @Test(groups={"new", "slow"})
    public void testHelixBridgeIt1OOA_missing_alg2() {
	String methodName = "testHelixBridgeIt1OOA_missing_alg2";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1OOA_missing3n.pdb");
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("tree");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/db/rnajunction/filt/k2_rep/rnaviewdir/subset.names ${NANOTILER_HOME}/db/rnajunction/filt/k2_rep/rnaviewdir 0");
	    app.runScriptLine("status");
	    String name2 = "root.import.stems.stemA_1.A_23_A_1";
	    String name1 = "root.import.stems.stemB_1.B_23_B_1";
	    Properties resultProperties = app.runScriptLine("bridgeit " + name1 + " " + name2 + " n=10 rms=35 angle=10 a=2 prefix=" 
							    + methodName + "_bridge_");
	    System.out.println("Result of " + methodName + " : " + resultProperties);
	    assert name1.equals(resultProperties.getProperty("name1"));
	    assert name2.equals(resultProperties.getProperty("name2"));
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    // app.runScriptLine("bridgeit root.pdb.B.C1 root.pdb.A.G21");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** First test of finding two briding internal loop motifs (algorithm 2) between two helices */
    @Test(groups={"new", "slow"})
    public void testHelixBridgeIt1OOA_missing_alg2_j2all() {
	String methodName = "testHelixBridgeIt1OOA_missing_alg2_j2all";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1OOA_missing3n.pdb");
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("tree");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/db/rnajunction/filt/j2_rep/rnaviewdir/subset.names ${NANOTILER_HOME}/db/rnajunction/filt/j2_rep/rnaviewdir 0");
	    app.runScriptLine("status");
	    String name2 = "root.import.stems.stemA_1.A_23_A_1";
	    String name1 = "root.import.stems.stemB_1.B_23_B_1";
	    Properties resultProperties = app.runScriptLine("bridgeit " + name1 + " " + name2 + " n=100 rms=15 angle=10 a=2 prefix=" 
							    + methodName + "_bridge_");
	    System.out.println("Result of " + methodName + " : " + resultProperties);
	    assert name1.equals(resultProperties.getProperty("name1"));
	    assert name2.equals(resultProperties.getProperty("name2"));
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    // app.runScriptLine("bridgeit root.pdb.B.C1 root.pdb.A.G21");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Test of finding a briding motif between two helices, production run trying more than 1700 2-way junctions! */
    @Test(groups={"new", "slow"})
    public void testHelixBridgeIt1OOA_3n_allj2() {
	String methodName = "testHelixBridgeIt1OOA_3n_allj2";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1OOA_missing3n.pdb");
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("tree");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/db/rnajunction/filt/j2_rep/rnaviewdir/subset.names ${NANOTILER_HOME}/db/rnajunction/filt/j2_rep/rnaviewdir 0");
	    app.runScriptLine("status");
	    String name2 = "root.import.stems.stemA_1.A_23_A_1";
	    String name1 = "root.import.stems.stemB_1.B_23_B_1";
	    Properties resultProperties = app.runScriptLine("bridgeit " + name1 + " " + name2 + " n=1 rms=100 angle=50");
	    System.out.println("Result of " + methodName + " : " + resultProperties);
	    assert name1.equals(resultProperties.getProperty("name1"));
	    assert name2.equals(resultProperties.getProperty("name2"));
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    // app.runScriptLine("bridgeit root.pdb.B.C1 root.pdb.A.G21");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Test of finding a briding motif between two helices, production run trying more than 1700 2-way junctions! */
    @Test(groups={"new", "slow"})
    public void testHelixBridgeIt1OOA_3n_top20j3() {
	String methodName = "testHelixBridgeIt1OOA_3n_top20j3";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1OOA_missing3n.pdb");
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("tree");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/db/rnajunction/filt/j3_rep/rnaviewdir/subset_top20.names ${NANOTILER_HOME}/db/rnajunction/filt/j3_rep/rnaviewdir 0");
	    app.runScriptLine("status");
	    String name2 = "root.import.stems.stemA_1.A_23_A_1";
	    String name1 = "root.import.stems.stemB_1.B_23_B_1";
	    Properties resultProperties = app.runScriptLine("bridgeit " + name1 + " " + name2 + " n=1 rms=100 angle=50");
	    System.out.println("Result of " + methodName + " : " + resultProperties);
	    assert name1.equals(resultProperties.getProperty("name1"));
	    assert name2.equals(resultProperties.getProperty("name2"));
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    // app.runScriptLine("bridgeit root.pdb.B.C1 root.pdb.A.G21");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Test of finding a briding motif between two helices, production run trying all representative 3-way junctions! */
    @Test(groups={"new", "slow"})
    public void testHelixBridgeIt1OOA_3n_allj3() {
	String methodName = "testHelixBridgeIt1OOA_3n_allj3";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1OOA_missing3n.pdb");
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("tree");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/db/rnajunction/filt/j3_rep/rnaviewdir/subset.names ${NANOTILER_HOME}/db/rnajunction/filt/j3_rep/rnaviewdir 0");
	    app.runScriptLine("status");
	    String name2 = "root.import.stems.stemA_1.A_23_A_1";
	    String name1 = "root.import.stems.stemB_1.B_23_B_1";
	    Properties resultProperties = app.runScriptLine("bridgeit " + name1 + " " + name2 + " n=1 rms=30 angle=10");
	    System.out.println("Result of " + methodName + " : " + resultProperties);
	    assert name1.equals(resultProperties.getProperty("name1"));
	    assert name2.equals(resultProperties.getProperty("name2"));
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    // app.runScriptLine("bridgeit root.pdb.B.C1 root.pdb.A.G21");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Second test of finding a briding motif between two helices using a simple system */
    @Test(groups={"new","slow"})
    public void testHelixBridgeIt2() {
	String methodName = "testHelixBridgeIt2";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/2GYA.pdb_j2_0-G539_0-G553_chain_ring.1FFK.pdb_k2_0-U55_0-C83_1221_L75.pdb_fixed_ABKL.pdb");
	    app.runScriptLine("tree");
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/helix_bridge.names ${NANOTILER_HOME}/test/fixtures 2"); // only one entry, but that one should fit
	    app.runScriptLine("status");
// 	    String name1 = "root.import.stems.stemA_B_1.B_7_A_1";
// 	    String name2 = "root.import.stems.stemK_L_2.K_7_L_1";
	    String name1 = "root.import.import_cov_jnc.j1.A_1_B_7";
	    String name2 = "root.import.import_cov_jnc.j2.D_1_C_7";
	    Properties resultProperties = app.runScriptLine("bridgeit " + name1 + " " + name2 + " rms=230");
	    System.out.println("Result of " + methodName + " : " + resultProperties);
	    assert name1.equals(resultProperties.getProperty("name1"));
	    assert name2.equals(resultProperties.getProperty("name2"));
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    // app.runScriptLine("bridgeit root.pdb.B.C1 root.pdb.A.G21");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


    /** Like testHelixBridgeIt2, but using different branch descriptors. */
    @Test(groups={"new", "slow"})
    public void testHelixBridgeIt3() {
	String methodName = "testHelixBridgeIt3";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/2GYA.pdb_j2_0-G539_0-G553_chain_ring.1FFK.pdb_k2_0-U55_0-C83_1221_L75.pdb_fixed.pdb");
	    app.runScriptLine("tree");
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    // app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/helix_bridge.names ${NANOTILER_HOME}/test/fixtures 2");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/db/rnajunction/orig/k2_rep/subset.names ${NANOTILER_HOME}/db/rnajunction/orig/k2_rep 2");
	    app.runScriptLine("status");
	    // String name2 = "root.import.stems.stemA_1.A_29_A_1";
	    String name2 = "root.import.stems.stemS_T_1.S_7_T_1";
	    // String name1 = "root.import.stems.stemB_1.B_29_B_1";
	    String name1 = "root.import.stems.stemY_Z_1.Z_5_Y_1";
	    Properties resultProperties = app.runScriptLine("bridgeit " + name1 + " " + name2 + " rms=30");
	    System.out.println("Result of " + methodName + " : " + resultProperties);
	    assert name1.equals(resultProperties.getProperty("name1"));
	    assert name2.equals(resultProperties.getProperty("name2"));
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    // app.runScriptLine("bridgeit root.pdb.B.C1 root.pdb.A.G21");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new","slow"})
    public void testHelixBridgeIt4() {
	String methodName = "testHelixBridgeIt4";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1A9L.pdb_j2_A-A6_A-G29_chain_ring.2D19.pdb_k2_A-G4_B-A5_1221_L57_ABKL.pdb");
	    app.runScriptLine("tree");
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/helix_bridge_test4.names ${NANOTILER_HOME}/test/fixtures 2"); // only one entry but that one should fit
	    app.runScriptLine("status");
// 	    String name1 = "root.import.stems.stemA_B_1.B_7_A_1";
// 	    String name2 = "root.import.stems.stemK_L_2.K_7_L_1";
	    String name1 = "root.import.import_cov_jnc.j1.B_1_A_9";
	    String name2 = "root.import.import_cov_jnc.j2.D_1_C_9";
	    Properties resultProperties = app.runScriptLine("bridgeit " + name1 + " " + name2 + " rms=230");
	    System.out.println("Result of " + methodName + " : " + resultProperties);
	    assert name1.equals(resultProperties.getProperty("name1"));
	    assert name2.equals(resultProperties.getProperty("name2"));
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    // app.runScriptLine("bridgeit root.pdb.B.C1 root.pdb.A.G21");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Bridges two single stranded nucleotides */
    @Test(groups={"slow", "tight"})
    public void testBridgeTwoHelices() {
	String methodName = "testBridgeTwoHelices";
	int length = 3; // length of bridge
	double distanceTolerance = 4.0;
	double rms = 5.0; // RMS limit for superposition
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/twoUnfusedHelices.pdb");
	    app.runScriptLine("tree allowed=Nucleotide3D");
	    String name1 = "root.import.A.LAST";
	    String name2 = "root.import.C.1";
	    app.runScriptLine("remove root.import.F");
	    app.runScriptLine("remove root.import.E");
	    Properties resultProperties = app.runScriptLine("bridgeit " + name1 + " " + name2 + " l=" + length + " m=" + length + " rms=" + rms);
	    System.out.println("Result of " + methodName + " : " 
			       + resultProperties);
	    app.runScriptLine("tree allowed=Nucleotide3D");
	    // assert name1.equals(resultProperties.getProperty("name1"));
	    // assert name2.equals(resultProperties.getProperty("name2"));
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    // app.runScriptLine("bridgeit root.pdb.B.C1 root.pdb.A.G21");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	    SequenceContainer seqs = app.getGraphController().getSequences();
	    for (int i = 0; i < seqs.size(); ++i) {
		System.out.println("Sequence " + (i+1) + " : " + seqs.get(i).getName() + " " + seqs.get(i).size());
	    }
	    assert(app.getGraphController().getSequences().size() == 5); // two helices plus one bridge strand
	    app.runScriptLine("tree strand");
	    Object3D bridgeStrand = app.getGraphController().getGraph().findByFullName(".A_1");
	    System.out.println("Name of bridge strand: " + bridgeStrand.getFullName());
	    System.out.println("There are " + app.getGraphController().getSequences().size() + " sequences. The bridging sequence has the length " + 
			       + bridgeStrand.size() + ". The desired length is " + length); // must have correct length

	    // assert(app.getGraphController().getSequences().getSequence(4).size() == length); // must have correct length
	    Object3D o3 = bridgeStrand.getChild(bridgeStrand.size()-1).getChild("O3*");
	    assert (o3 != null);
	    RnaStrand strandC = (RnaStrand)(seqs.getSequence("C"));
	    Object3D p = strandC.getResidue3D(0).getChild("P");
	    assert p != null;
	    assert o3 != null;
	    double dist = p.distance(o3);
	    System.out.println("The distance between O3* and P atoms is: " + dist + " The allowed tolerance is a maximum distance of " + distanceTolerance + " Angstroem.");
	    assert(bridgeStrand.size() == length);
	    assert(dist < distanceTolerance);
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


}
