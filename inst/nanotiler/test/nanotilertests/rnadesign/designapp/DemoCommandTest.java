package nanotilertests.rnadesign.designapp;

import commandtools.*;
import generaltools.TestTools;
import org.testng.*;
import org.testng.annotations.*;
import rnadesign.designapp.*;

public class DemoCommandTest {

    @Test(groups={"new"})
    public void DemoCommandTest() {
	String methodName = "testDemoCommand1";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("demo optbasepairs.script");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	} catch(CommandException e) {
	    System.out.println("Command exception in demo command test: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

}
