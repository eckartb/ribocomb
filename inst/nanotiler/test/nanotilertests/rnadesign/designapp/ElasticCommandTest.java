package nanotilertests.rnadesign.designapp;

import generaltools.TestTools;
import org.testng.annotations.*; // for testing
import commandtools.CommandException;
import rnadesign.designapp.*;

public class ElasticCommandTest {


    @Test (groups={"new", "incomplete"})
    /** Tests elastic command. Not completely implemented! TODO */
    public void testElasticCommand1(){
	String methodName = "testElasticCommand1";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/2GYA-nooverlap-nov20.pdb findstems=false");
	    app.runScriptLine("tree residues");
	    assert app.getGraphController().getGraph().getGraph().size() > 0; // there must be objects defined
	    app.runScriptLine("elastic class=Nucleotide3D root=root.import");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandException e) {
	    System.out.println(e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


}
