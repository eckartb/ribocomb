package nanotilertests.rnadesign.designapp;

import java.util.Properties;
import generaltools.TestTools;
import commandtools.CommandException;
import org.testng.annotations.*;
import rnadesign.designapp.*;

public class ExtendCommandTest {

    @Test(groups={"new", "slow"})
    public void testExtendAtEnd() {
	String methodName = "testExtendAtEnd";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("source ${NANOTILER_HOME}/resources/defaults.script");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/optimizedcube_step12_fused2_rnaview.pdb findstems=false");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("status");
 	    String name1 = "root.import.A.G44";
	    Properties resultProperties = app.runScriptLine("extend " + name1 + " rms=0.5 l=10");
	    System.out.println("Result of " + methodName + " : " + resultProperties);
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    assert name1.equals(resultProperties.getProperty("name1"));
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new", "slow"})
    public void testExtendAtStart() {
	String methodName = "testExtendAtStart";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("source ${NANOTILER_HOME}/resources/defaults.script");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/optimizedcube_step12_fused2_rnaview.pdb findstems=false");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("status");
 	    String name1 = "root.import.A.1";
 	    String name2 = "root.import.B.1";
 	    String name3 = "root.import.C.1";
 	    String name4 = "root.import.D.1";
 	    String name5 = "root.import.E.1";
 	    String name6 = "root.import.F.1";
	    // add fragment of length 5 to all strands
	    Properties resultProperties = app.runScriptLine("extend " + name1 + " rms=0.5 l=6 n=3");
	    resultProperties = app.runScriptLine("extend " + name2 + " rms=0.5 l=6 n=3");
	    resultProperties = app.runScriptLine("extend " + name3 + " rms=0.5 l=6 n=3");
	    resultProperties = app.runScriptLine("extend " + name4 + " rms=0.5 l=6 n=3");
	    resultProperties = app.runScriptLine("extend " + name5 + " rms=0.5 l=6 n=3");
	    resultProperties = app.runScriptLine("extend " + name6 + " rms=0.5 l=6 n=3");
	    app.runScriptLine("tree strand");
	    System.out.println("Result of " + methodName + " : " + resultProperties);
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("echo Mutating leader sequences...");
	    //  Example: mutate A:C5,G15,A2 B:U1,G2,A15
	    app.runScriptLine("mutate A_1:A6,A5,A4,G3,G2,G1");
	    app.runScriptLine("mutate A_2:A6,A5,A4,G3,G2,G1");
	    app.runScriptLine("mutate A_3:A6,A5,A4,G3,G2,G1");
	    app.runScriptLine("mutate A_4:A6,A5,A4,G3,G2,G1");
	    app.runScriptLine("mutate A_5:A6,A5,A4,G3,G2,G1");
	    app.runScriptLine("mutate A_6:A6,A5,A4,G3,G2,G1");
	    app.runScriptLine("exportpdb " + methodName + "_mutated.pdb");
	    app.runScriptLine("echo Fusing strands...");
	    app.runScriptLine("fusestrands root.A_1 root.import.A");
	    app.runScriptLine("fusestrands root.A_2 root.import.B");
	    app.runScriptLine("fusestrands root.A_3 root.import.C");
	    app.runScriptLine("fusestrands root.A_4 root.import.D");
	    app.runScriptLine("fusestrands root.A_5 root.import.E");
	    app.runScriptLine("fusestrands root.A_6 root.import.F");
	    app.runScriptLine("echo Fused strands:");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("exportpdb " + methodName + "_fused.pdb");
	    
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Test similar to testExtendAtStart, but modifying strand connectivity such that
     *  strand ends point towards the outside.
     */
    @Test(groups={"new", "production", "slow"})
    public void testExtendAtStart2() {
	String methodName = "testExtendAtStart2";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("source ${NANOTILER_HOME}/resources/defaults.script");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/optimizedcube_step12_fused2_rnaview.pdb findstems=false");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("status");
 	    String name1 = "root.import.A.1";
 	    String name2 = "root.import.B.1";
 	    String name3 = "root.import.C.1";
 	    String name4 = "root.import.D.1";
 	    String name5 = "root.import.E.1";
 	    String name6 = "root.import.F.1";
	    // add fragment of length 5 to all strands
	    app.runScriptLine("echo Working on strand A:");
	    app.runScriptLine("splitstrand  root.import.A 43");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("fusestrands root.import.A_split root.import.A");
	    Properties resultProperties = app.runScriptLine("extend root.import.A_split.1 rms=0.5 l=6 n=3");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("fusestrands root.A root.import.A_split");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("exportpdb " + methodName + "_mutated_A.pdb");

	    app.runScriptLine("echo Working on strand B:");
	    app.runScriptLine("splitstrand  root.import.B 43");
	    app.runScriptLine("fusestrands root.import.B_split root.import.B");
	    resultProperties = app.runScriptLine("extend root.import.B_split.1 rms=0.5 l=6 n=3");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("fusestrands root.A_1 root.import.B_split");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("exportpdb " + methodName + "_mutated_AB.pdb");

	    app.runScriptLine("echo Working on strand C:");
	    app.runScriptLine("splitstrand  root.import.C 43");
	    app.runScriptLine("fusestrands root.import.C_split root.import.C");
	    resultProperties = app.runScriptLine("extend root.import.C_split.1 rms=0.5 l=6 n=3");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("fusestrands root.A_2 root.import.C_split");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("exportpdb " + methodName + "_mutated_ABC.pdb");

	    app.runScriptLine("echo Working on strand D:");
	    app.runScriptLine("splitstrand  root.import.D 43");
	    app.runScriptLine("fusestrands root.import.D_split root.import.D");
	    resultProperties = app.runScriptLine("extend root.import.D_split.1 rms=0.5 l=6 n=3");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("fusestrands root.A_3 root.import.D_split");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("exportpdb " + methodName + "_mutated_ABCD.pdb");

	    app.runScriptLine("echo Working on strand E:");
	    app.runScriptLine("splitstrand  root.import.E 43");
	    app.runScriptLine("fusestrands root.import.E_split root.import.E");
	    resultProperties = app.runScriptLine("extend root.import.E_split.1 rms=0.5 l=6 n=3");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("fusestrands root.A_4 root.import.E_split");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("exportpdb " + methodName + "_mutated_ABCDE.pdb");

	    app.runScriptLine("echo Working on strand F:");
	    app.runScriptLine("splitstrand  root.import.F 43");
	    app.runScriptLine("fusestrands root.import.F_split root.import.F");
	    resultProperties = app.runScriptLine("extend root.import.F_split.1 rms=0.5 l=6 n=3");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("fusestrands root.A_5 root.import.F_split");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("exportpdb " + methodName + "_mutated_ABCDEF.pdb");

	    // app.runScriptLine("fusestrands root.import.B_split root.import.B");
	    // resultProperties = app.runScriptLine("extend " + name3 + " rms=0.5 l=6 n=3");
	    // resultProperties = app.runScriptLine("extend " + name4 + " rms=0.5 l=6 n=3");
	    // resultProperties = app.runScriptLine("extend " + name5 + " rms=0.5 l=6 n=3");
	    // resultProperties = app.runScriptLine("extend " + name6 + " rms=0.5 l=6 n=3");
	    app.runScriptLine("tree strand");
	    System.out.println("Result of " + methodName + " : " + resultProperties);
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("echo Mutating leader sequences...");
	    //  Example: mutate A:C5,G15,A2 B:U1,G2,A15
	    //	    app.runScriptLine("mutate A_1:A6,A5,A4,G3,G2,G1");
// 	    app.runScriptLine("mutate A_2:A6,A5,A4,G3,G2,G1");
// 	    app.runScriptLine("mutate A_3:A6,A5,A4,G3,G2,G1");
// 	    app.runScriptLine("mutate A_4:A6,A5,A4,G3,G2,G1");
// 	    app.runScriptLine("mutate A_5:A6,A5,A4,G3,G2,G1");
// 	    app.runScriptLine("mutate A_6:A6,A5,A4,G3,G2,G1");
	    app.runScriptLine("exportpdb " + methodName + "_mutated.pdb");
	    assert false; // quit here
	    app.runScriptLine("echo Fusing strands...");
	    app.runScriptLine("fusestrands root.A_1 root.import.A");
	    app.runScriptLine("fusestrands root.A_2 root.import.B");
	    app.runScriptLine("fusestrands root.A_3 root.import.C");
	    app.runScriptLine("fusestrands root.A_4 root.import.D");
	    app.runScriptLine("fusestrands root.A_5 root.import.E");
	    app.runScriptLine("fusestrands root.A_6 root.import.F");
	    app.runScriptLine("echo Fused strands:");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("exportpdb " + methodName + "_fused.pdb");
	    
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


    /** Test similar to testExtendAtStart, but modifying strand connectivity such that
     *  strand ends point towards the outside.
     */
    @Test(groups={"new", "production", "slow"})
    public void testExtendAtStart3() {
	String methodName = "testExtendAtStart3";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("source ${NANOTILER_HOME}/resources/defaults.script");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/optimizedcube_step12_fused2_rnaview.pdb findstems=false");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("status");
 	    String name1 = "root.import.A.1";
 	    String name2 = "root.import.B.1";
 	    String name3 = "root.import.C.1";
 	    String name4 = "root.import.D.1";
 	    String name5 = "root.import.E.1";
 	    String name6 = "root.import.F.1";
	    // add fragment of length 5 to all strands
	    app.runScriptLine("echo Working on strand A:");
	    app.runScriptLine("splitstrand  root.import.A 44");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("fusestrands root.import.A_split root.import.A");
	    Properties resultProperties = app.runScriptLine("extend root.import.A_split.1 rms=0.5 l=6 n=3");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("fusestrands root.A root.import.A_split");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("exportpdb " + methodName + "_mutated_A.pdb");

	    app.runScriptLine("echo Working on strand B:");
	    app.runScriptLine("splitstrand  root.import.B 44");
	    app.runScriptLine("fusestrands root.import.B_split root.import.B");
	    resultProperties = app.runScriptLine("extend root.import.B_split.1 rms=0.5 l=6 n=3");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("fusestrands root.A_1 root.import.B_split");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("exportpdb " + methodName + "_mutated_AB.pdb");

	    app.runScriptLine("echo Working on strand C:");
	    app.runScriptLine("splitstrand  root.import.C 44");
	    app.runScriptLine("fusestrands root.import.C_split root.import.C");
	    resultProperties = app.runScriptLine("extend root.import.C_split.1 rms=0.5 l=6 n=3");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("fusestrands root.A_2 root.import.C_split");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("exportpdb " + methodName + "_mutated_ABC.pdb");

	    app.runScriptLine("echo Working on strand D:");
	    app.runScriptLine("splitstrand  root.import.D 44");
	    app.runScriptLine("fusestrands root.import.D_split root.import.D");
	    resultProperties = app.runScriptLine("extend root.import.D_split.1 rms=0.5 l=6 n=3");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("fusestrands root.A_3 root.import.D_split");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("exportpdb " + methodName + "_mutated_ABCD.pdb");

	    app.runScriptLine("echo Working on strand E:");
	    app.runScriptLine("splitstrand  root.import.E 44");
	    app.runScriptLine("fusestrands root.import.E_split root.import.E");
	    resultProperties = app.runScriptLine("extend root.import.E_split.1 rms=0.5 l=6 n=3");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("fusestrands root.A_4 root.import.E_split");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("exportpdb " + methodName + "_mutated_ABCDE.pdb");

	    app.runScriptLine("echo Working on strand F:");
	    app.runScriptLine("splitstrand  root.import.F 44");
	    app.runScriptLine("fusestrands root.import.F_split root.import.F");
	    resultProperties = app.runScriptLine("extend root.import.F_split.1 rms=0.5 l=6 n=3");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("fusestrands root.A_5 root.import.F_split");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("exportpdb " + methodName + "_mutated_ABCDEF.pdb");

	    // app.runScriptLine("fusestrands root.import.B_split root.import.B");
	    // resultProperties = app.runScriptLine("extend " + name3 + " rms=0.5 l=6 n=3");
	    // resultProperties = app.runScriptLine("extend " + name4 + " rms=0.5 l=6 n=3");
	    // resultProperties = app.runScriptLine("extend " + name5 + " rms=0.5 l=6 n=3");
	    // resultProperties = app.runScriptLine("extend " + name6 + " rms=0.5 l=6 n=3");
	    app.runScriptLine("tree strand");
	    System.out.println("Result of " + methodName + " : " + resultProperties);
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("echo Mutating leader sequences...");
	    //  Example: mutate A:C5,G15,A2 B:U1,G2,A15
	    //	    app.runScriptLine("mutate A_1:A6,A5,A4,G3,G2,G1");
// 	    app.runScriptLine("mutate A_2:A6,A5,A4,G3,G2,G1");
// 	    app.runScriptLine("mutate A_3:A6,A5,A4,G3,G2,G1");
// 	    app.runScriptLine("mutate A_4:A6,A5,A4,G3,G2,G1");
// 	    app.runScriptLine("mutate A_5:A6,A5,A4,G3,G2,G1");
// 	    app.runScriptLine("mutate A_6:A6,A5,A4,G3,G2,G1");
	    app.runScriptLine("exportpdb " + methodName + "_mutated.pdb");
	    assert false; // quit here
	    app.runScriptLine("echo Fusing strands...");
	    app.runScriptLine("fusestrands root.A_1 root.import.A");
	    app.runScriptLine("fusestrands root.A_2 root.import.B");
	    app.runScriptLine("fusestrands root.A_3 root.import.C");
	    app.runScriptLine("fusestrands root.A_4 root.import.D");
	    app.runScriptLine("fusestrands root.A_5 root.import.E");
	    app.runScriptLine("fusestrands root.A_6 root.import.F");
	    app.runScriptLine("echo Fused strands:");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("exportpdb " + methodName + "_fused.pdb");
	    
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Test similar to testExtendAtStart, but modifying strand connectivity such that
     *  strand ends point towards the inside, should cut helices in 5+5 bp
     * not a good test case, result is 4+6 helix
     */
    @Test(groups={"new", "production", "slow", "outdated"})
    public void testExtendAtStart4() {
	String methodName = "testExtendAtStart4";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("source ${NANOTILER_HOME}/resources/defaults.script");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/optimizedcube_step12_fused2_rnaview.pdb findstems=false");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("status");
 	    String name1 = "root.import.A.1";
 	    String name2 = "root.import.B.1";
 	    String name3 = "root.import.C.1";
 	    String name4 = "root.import.D.1";
 	    String name5 = "root.import.E.1";
 	    String name6 = "root.import.F.1";
	    // add fragment of length 5 to all strands
	    app.runScriptLine("echo Working on strand A:");
	    app.runScriptLine("splitstrand  root.import.A 2");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("fusestrands root.import.A_split root.import.A");

	    app.runScriptLine("echo Working on strand B:");
	    app.runScriptLine("splitstrand  root.import.B 2");
	    app.runScriptLine("fusestrands root.import.B_split root.import.B");
	    app.runScriptLine("tree strand");

	    app.runScriptLine("echo Working on strand C:");
	    app.runScriptLine("splitstrand  root.import.C 2");
	    app.runScriptLine("fusestrands root.import.C_split root.import.C");
	    app.runScriptLine("tree strand");

	    app.runScriptLine("echo Working on strand D:");
	    app.runScriptLine("splitstrand  root.import.D 2");
	    app.runScriptLine("fusestrands root.import.D_split root.import.D");
	    app.runScriptLine("tree strand");

	    app.runScriptLine("echo Working on strand E:");
	    app.runScriptLine("splitstrand  root.import.E 2");
	    app.runScriptLine("fusestrands root.import.E_split root.import.E");
	    app.runScriptLine("tree strand");

	    app.runScriptLine("echo Working on strand F:");
	    app.runScriptLine("splitstrand  root.import.F 2");
	    app.runScriptLine("fusestrands root.import.F_split root.import.F");
	    app.runScriptLine("tree strand");

	    app.runScriptLine("exportpdb " + methodName + "_movedsplit.pdb");

	    Properties resultProperties 
                             = app.runScriptLine("extend root.import.A_split.1 rms=0.5 l=6 n=3");
	    app.runScriptLine("exportpdb " + methodName + "_extended_A.pdb");
	    resultProperties = app.runScriptLine("extend root.import.B_split.1 rms=0.5 l=6 n=3");
	    app.runScriptLine("exportpdb " + methodName + "_extended_B.pdb");
	    resultProperties = app.runScriptLine("extend root.import.C_split.1 rms=0.5 l=6 n=3");
	    app.runScriptLine("exportpdb " + methodName + "_extended_C.pdb");
	    resultProperties = app.runScriptLine("extend root.import.D_split.1 rms=0.5 l=6 n=3");
	    app.runScriptLine("exportpdb " + methodName + "_extended_D.pdb");
	    resultProperties = app.runScriptLine("extend root.import.E_split.1 rms=0.5 l=6 n=3");
	    app.runScriptLine("exportpdb " + methodName + "_extended_E.pdb");
	    resultProperties = app.runScriptLine("extend root.import.F_split.1 rms=0.5 l=6 n=3");

	    // app.runScriptLine("fusestrands root.import.B_split root.import.B");
	    // resultProperties = app.runScriptLine("extend " + name3 + " rms=0.5 l=6 n=3");
	    // resultProperties = app.runScriptLine("extend " + name4 + " rms=0.5 l=6 n=3");
	    // resultProperties = app.runScriptLine("extend " + name5 + " rms=0.5 l=6 n=3");
	    // resultProperties = app.runScriptLine("extend " + name6 + " rms=0.5 l=6 n=3");
	    app.runScriptLine("tree strand");
	    System.out.println("Result of " + methodName + " : " + resultProperties);
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("echo Mutating leader sequences...");
	    //  Example: mutate A:C5,G15,A2 B:U1,G2,A15
	    //	    app.runScriptLine("mutate A_1:A6,A5,A4,G3,G2,G1");
// 	    app.runScriptLine("mutate A_2:A6,A5,A4,G3,G2,G1");
// 	    app.runScriptLine("mutate A_3:A6,A5,A4,G3,G2,G1");
// 	    app.runScriptLine("mutate A_4:A6,A5,A4,G3,G2,G1");
// 	    app.runScriptLine("mutate A_5:A6,A5,A4,G3,G2,G1");
// 	    app.runScriptLine("mutate A_6:A6,A5,A4,G3,G2,G1");
	    app.runScriptLine("exportpdb " + methodName + "_mutated.pdb");
	    assert false; // quit here
	    app.runScriptLine("echo Fusing strands...");
	    app.runScriptLine("fusestrands root.A_1 root.import.A");
	    app.runScriptLine("fusestrands root.A_2 root.import.B");
	    app.runScriptLine("fusestrands root.A_3 root.import.C");
	    app.runScriptLine("fusestrands root.A_4 root.import.D");
	    app.runScriptLine("fusestrands root.A_5 root.import.E");
	    app.runScriptLine("fusestrands root.A_6 root.import.F");
	    app.runScriptLine("echo Fused strands:");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("exportpdb " + methodName + "_fused.pdb");
	    
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new", "slow"})
    public void testFuseAndExtendAtStart() {
	String methodName = "testFuseAndExtendAtStart";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("source ${NANOTILER_HOME}/resources/defaults.script");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/testCubeFragmentPlacementLoop_bridged.pdb findstems=false");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("status");
 	    String name1 = "root.import.A.1";
 	    String name2 = "root.import.B.1";
 	    String name3 = "root.import.C.1";
 	    String name4 = "root.import.D.1";
 	    String name5 = "root.import.E.1";
 	    String name6 = "root.import.F.1";
	    app.runScriptLine("fusestrands root.import.H root.import.I");
	    app.runScriptLine("exportpdb " + methodName + "_fused.pdb");
	    app.runScriptLine("fusestrands root.import.G root.import.J");
	    app.runScriptLine("exportpdb " + methodName + "_fused.pdb");
	    // add fragment of length 5 to all strands
	    Properties resultProperties =
                               app.runScriptLine("extend " + name1 + " rms=0.5 l=6 n=3");
	    resultProperties = app.runScriptLine("extend " + name2 + " rms=0.5 l=6 n=3");
	    resultProperties = app.runScriptLine("extend " + name3 + " rms=0.5 l=6 n=3");
	    resultProperties = app.runScriptLine("extend " + name4 + " rms=0.5 l=6 n=3");
	    resultProperties = app.runScriptLine("extend " + name5 + " rms=0.5 l=6 n=3");
	    resultProperties = app.runScriptLine("extend " + name6 + " rms=0.5 l=6 n=3");
	    app.runScriptLine("tree strand");
	    System.out.println("Result of " + methodName + " : " + resultProperties);
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("echo Mutating leader sequences...");
	    //  Example: mutate A:C5,G15,A2 B:U1,G2,A15
	    app.runScriptLine("mutate A_1:A6,A5,A4,G3,G2,G1");
	    app.runScriptLine("mutate A_2:A6,A5,A4,G3,G2,G1");
	    app.runScriptLine("mutate A_3:A6,A5,A4,G3,G2,G1");
	    app.runScriptLine("mutate A_4:A6,A5,A4,G3,G2,G1");
	    app.runScriptLine("mutate A_5:A6,A5,A4,G3,G2,G1");
	    app.runScriptLine("mutate A_6:A6,A5,A4,G3,G2,G1");
	    app.runScriptLine("exportpdb " + methodName + "_mutated.pdb");
	    app.runScriptLine("echo Fusing strands...");
	    app.runScriptLine("fusestrands root.A_1 root.import.A");
	    app.runScriptLine("fusestrands root.A_2 root.import.B");
	    app.runScriptLine("fusestrands root.A_3 root.import.C");
	    app.runScriptLine("fusestrands root.A_4 root.import.D");
	    app.runScriptLine("fusestrands root.A_5 root.import.E");
	    app.runScriptLine("fusestrands root.A_6 root.import.F");
	    app.runScriptLine("echo Fused strands:");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("exportpdb " + methodName + "_fused.pdb");
	    
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Finalize MG motif in cube design. Same as testFuseAndExtendAtStart, but using recut MG motif */
    @Test(groups={"new", "slow"})
    public void testFuseAndExtendAtStart3() {
	String methodName = "testFuseAndExtendAtStart3";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("source ${NANOTILER_HOME}/resources/defaults.script");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/testCubeFragmentPlacementLoop3_bridged.pdb findstems=false");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("status");
 	    String name1 = "root.import.A.1";
 	    String name2 = "root.import.B.1";
 	    String name3 = "root.import.C.1";
 	    String name4 = "root.import.D.1";
 	    String name5 = "root.import.E.1";
 	    String name6 = "root.import.F.1";
	    app.runScriptLine("fusestrands root.import.H root.import.I");
	    app.runScriptLine("exportpdb " + methodName + "_fused.pdb");
	    app.runScriptLine("fusestrands root.import.G root.import.J");
	    app.runScriptLine("exportpdb " + methodName + "_fused.pdb");
	    // add fragment of length 5 to all strands
	    Properties resultProperties =
                               app.runScriptLine("extend " + name1 + " rms=0.5 l=6 n=3");
	    resultProperties = app.runScriptLine("extend " + name2 + " rms=0.5 l=6 n=3");
	    resultProperties = app.runScriptLine("extend " + name3 + " rms=0.5 l=6 n=3");
	    resultProperties = app.runScriptLine("extend " + name4 + " rms=0.5 l=6 n=3");
	    resultProperties = app.runScriptLine("extend " + name5 + " rms=0.5 l=6 n=3");
	    resultProperties = app.runScriptLine("extend " + name6 + " rms=0.5 l=6 n=3");
	    app.runScriptLine("tree strand");
	    System.out.println("Result of " + methodName + " : " + resultProperties);
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("echo Mutating leader sequences...");
	    //  Example: mutate A:C5,G15,A2 B:U1,G2,A15
	    app.runScriptLine("mutate A_1:A6,A5,A4,G3,G2,G1");
	    app.runScriptLine("mutate A_2:A6,A5,A4,G3,G2,G1");
	    app.runScriptLine("mutate A_3:A6,A5,A4,G3,G2,G1");
	    app.runScriptLine("mutate A_4:A6,A5,A4,G3,G2,G1");
	    app.runScriptLine("mutate A_5:A6,A5,A4,G3,G2,G1");
	    app.runScriptLine("mutate A_6:A6,A5,A4,G3,G2,G1");
	    app.runScriptLine("exportpdb " + methodName + "_mutated.pdb");
	    app.runScriptLine("echo Fusing strands...");
	    app.runScriptLine("fusestrands root.A_1 root.import.A");
	    app.runScriptLine("fusestrands root.A_2 root.import.B");
	    app.runScriptLine("fusestrands root.A_3 root.import.C");
	    app.runScriptLine("fusestrands root.A_4 root.import.D");
	    app.runScriptLine("fusestrands root.A_5 root.import.E");
	    app.runScriptLine("fusestrands root.A_6 root.import.F");
	    app.runScriptLine("echo Fused strands:");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("exportpdb " + methodName + "_fused.pdb");
	    
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Test similar to testExtendAtStart, but modifying strand connectivity such that
     *  strand ends point towards the outside.
     */
    @Test(groups={"new", "production", "slow"})
    public void testExtendAtStartLargeCube() {
	String methodName = "testExtendAtStartLargeCube";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("source ${NANOTILER_HOME}/resources/defaults.script");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/optimizedcube15_step12_fused2.pdb findstems=false");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("status");
	    Properties resultProperties = app.runScriptLine("extend root.import.A.1 rms=0.5 l=6 n=3");
	    resultProperties = app.runScriptLine("extend root.import.B.1 rms=0.5 l=6 n=3");
	    resultProperties = app.runScriptLine("extend root.import.C.1 rms=0.5 l=6 n=3");
	    resultProperties = app.runScriptLine("extend root.import.D.1 rms=0.5 l=6 n=3");
	    resultProperties = app.runScriptLine("extend root.import.E.1 rms=0.5 l=6 n=3");
	    resultProperties = app.runScriptLine("extend root.import.F.1 rms=0.5 l=6 n=3");
	    app.runScriptLine("exportpdb " + methodName + "_extended.pdb");

	    // app.runScriptLine("fusestrands root.import.B_split root.import.B");
	    // resultProperties = app.runScriptLine("extend " + name3 + " rms=0.5 l=6 n=3");
	    // resultProperties = app.runScriptLine("extend " + name4 + " rms=0.5 l=6 n=3");
	    // resultProperties = app.runScriptLine("extend " + name5 + " rms=0.5 l=6 n=3");
	    // resultProperties = app.runScriptLine("extend " + name6 + " rms=0.5 l=6 n=3");
	    app.runScriptLine("tree strand");
	    System.out.println("Result of " + methodName + " : " + resultProperties);
	    // app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("echo Mutating leader sequences...");
	    //  Example: mutate A:C5,G15,A2 B:U1,G2,A15
	    app.runScriptLine("mutate A_1:A6,A5,A4,G3,G2,G1");
	    app.runScriptLine("mutate A_2:A6,A5,A4,G3,G2,G1");
 	    app.runScriptLine("mutate A_3:A6,A5,A4,G3,G2,G1");
 	    app.runScriptLine("mutate A_4:A6,A5,A4,G3,G2,G1");
 	    app.runScriptLine("mutate A_5:A6,A5,A4,G3,G2,G1");
 	    app.runScriptLine("mutate A_6:A6,A5,A4,G3,G2,G1");
	    app.runScriptLine("exportpdb " + methodName + "_extended_mutated.pdb");
	    app.runScriptLine("echo Fusing strands...");
	    app.runScriptLine("fusestrands root.A_1 root.import.A");
	    app.runScriptLine("fusestrands root.A_2 root.import.B");
	    app.runScriptLine("fusestrands root.A_3 root.import.C");
	    app.runScriptLine("fusestrands root.A_4 root.import.D");
	    app.runScriptLine("fusestrands root.A_5 root.import.E");
	    app.runScriptLine("fusestrands root.A_6 root.import.F");

	    app.runScriptLine("mutate A_1:U15,U37,U59,U81");
	    app.runScriptLine("mutate A_2:U15,U37,U59,U81");
 	    app.runScriptLine("mutate A_3:U15,U37,U59,U81");
 	    app.runScriptLine("mutate A_4:U15,U37,U59,U81");
 	    app.runScriptLine("mutate A_5:U15,U37,U59,U81");
 	    app.runScriptLine("mutate A_6:U15,U37,U59,U81");

	    app.runScriptLine("echo Fused strands:");
	    app.runScriptLine("secondary");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

}
