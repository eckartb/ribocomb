package nanotilertests.rnadesign.designapp;

import commandtools.CommandException;
import commandtools.CommandExecutionException;
import generaltools.TestTools;
import org.testng.annotations.*; // for testing
import rnadesign.designapp.*;

/** Test class for import command */
public class FitCommandTest {

    public FitCommandTest() { }

    @Test (groups={"slow"})
    public void testFitCommand(){
	String methodName = "testFitCommand";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1S1I.pdb_j2_3-U2732_3-A2746_chain_ring1122l88_g__g3_b6_1_edit2_realgap_rnaview.pdb");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/1S1I.pdb_j2_3-U2732_3-A2746_chain_ring1122l88_g__g3_b6_1_edit2_realgap_rnaview.names ${NANOTILER_HOME}/test/fixtures");
	    app.runScriptLine("status");
	    app.runScriptLine("echo Starting to fit kissing loop into gap!");
	    app.runScriptLine("fit k root.import_cov_jnc.j4.K_3_L_4 root.import_cov_jnc.j5.O_3_P_4 place=true rms=40");
	    app.runScriptLine("exportpdb fit_test_tmp.pdb junction=true");
	    app.runScriptLine("clear all");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1Y69.pdb_j2_0-A1004_0-U1170_chain_ring1122l00_rnaview.pdb");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/1Y69.pdb_j2_0-A1004_0-U1170_chain_ring1122l00_rnaview.names ${NANOTILER_HOME}/test/fixtures");
	    app.runScriptLine("tree helixends");
	    app.runScriptLine("fit j root.import_cov_jnc.j6.L_3_L_11 root.import_cov_jnc.j3.E_3_E_12 h1min=0 h1max=0 h2min=0 h2max=0");
	    app.runScriptLine("exportpdb fit_test2_tmp.pdb junction=true");
	    app.runScriptLine("scorefit root.import_cov_jnc.j2.D_3_D_11 root.import_cov_jnc.j6.K_3_K_12 root.import_cov_jnc.j4.H_3_G_6 root.import_cov_jnc.j4.G_3_H_5");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	} catch(CommandException e) {
	    System.out.println("Command exception in import command test: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }



}
