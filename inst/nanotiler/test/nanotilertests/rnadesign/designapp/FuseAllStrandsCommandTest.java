package nanotilertests.rnadesign.designapp;

import generaltools.TestTools;
import commandtools.*;
import org.testng.annotations.*;
import rnadesign.designapp.*;

public class FuseAllStrandsCommandTest {

    /** Imoports one cube corner and attempts to fuse its strands */
    @Test(groups={"new"})
    public void twoUnfusedHelices() {
	String methodName = "twoUnfusedHelices";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/twoUnfusedHelices.pdb");
	    assert app.getGraphController().getSequences().getSequenceCount() == 6; // 6 strands before fusing
	    app.runScriptLine("fuseallstrands cutoff=5");
	    app.runScriptLine("exportpdb " + methodName + "_fused.pdb");
	    assert app.getGraphController().getSequences().getSequenceCount() == 2; // 3 strands after fusing
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Imoports one cube corner and attempts to fuse its strands */
    @Test(groups={"new", "slow"})
    public void fuseAllCubeCorner() {
	String methodName = "fuseAllCubeCorner";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/fuseAllCube_unfused_corner.pdb");
	    assert app.getGraphController().getSequences().getSequenceCount() == 6; // 6 strands before fusing
	    app.runScriptLine("fuseallstrands rms=4 root=root min=1 max=8 cutoff=20");
	    app.runScriptLine("exportpdb " + methodName + "_fused.pdb");
	    // assert app.getGraphController().getSequences().getSequenceCount() == 9; // 3 strands after fusing
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Generates one cube corner and attempts to fuse its strands */
    @Test(groups={"new", "slow"})
    public void fuseAllCube() {
	String methodName = "fuseAllCube";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("genshape cube");
	    app.runScriptLine("optrnagraph root.cube offset=18 iter=1000000");
	    app.runScriptLine("tracernagraph root=root.cube error=30 offset=12 max=20 steps=1000000");
	    app.runScriptLine("exportpdb " + methodName + "_unfused.pdb");
	    app.runScriptLine("fuseallstrands rms=4 root=root min=1");
	    app.runScriptLine("exportpdb " + methodName + "_fused.pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Generates one cube corner and attempts to fuse its strands */
    @Test(groups={"new", "slow"})
    public void fuseAllCubeModified() {
	String methodName = "fuseAllCubeModified";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("genshape cube");
	    app.runScriptLine("optrnagraph root.cube offset=18 iter=1000000");
	    app.runScriptLine("tracernagraph root=root.cube error=30 offset=12 max=20 steps=1000000 mod=true");
	    app.runScriptLine("exportpdb " + methodName + "_unfused.pdb");
	    app.runScriptLine("fuseallstrands rms=4 root=root min=1");
	    app.runScriptLine("exportpdb " + methodName + "_fused.pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new", "slow"})
    public void fuseAllTetrahedron() {
	String methodName = "fuseAllTetrahedron";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("genshape tetrahedron");
	    app.runScriptLine("optrnagraph root.tetrahedron offset=18 iter=1000000");
	    app.runScriptLine("tracernagraph root=root.tetrahedron error=20 offset=18 max=10 steps=1000000");
	    app.runScriptLine("exportpdb " + methodName + "_unfused.pdb");
	    app.runScriptLine("fuseallstrands");
	    app.runScriptLine("exportpdb " + methodName + "_fused.pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new", "slow"})
    public void fuseAllSquare() {
	String methodName = "fuseAllSquare";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("genshape ring4");
	    app.runScriptLine("tree");
	    app.runScriptLine("optrnagraph root.ring4 offset=12 iter=1000000 error=0");
	    app.runScriptLine("tracernagraph root=root.ring4 error=20 offset=12 max=20 steps=1000000 kt=2");
	    app.runScriptLine("exportpdb " + methodName + "_unfused.pdb");
	    app.runScriptLine("fuseallstrands min=1 max=10 rms=3.5 cutoff=20");
	    app.runScriptLine("exportpdb " + methodName + "_fused.pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

	@Test(groups={"new"})
	public void fuseAllBridgedSquare(){
		String methodName = "fuseAllBridgedSquare";
		System.out.println(TestTools.generateMethodHeader(methodName));
		NanoTilerScripter app = new NanoTilerScripter();
		try {
	   		app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    	app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/unfused_bridged_square.pdb");
	    	app.runScriptLine("fuseallstrands");
	    	app.runScriptLine("exportpdb " + methodName + "_fused.pdb");
	    	app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
		}
		catch (CommandException ce) {
	  		System.out.println(ce.getMessage());
	    	assert false;
		}
		System.out.println(TestTools.generateMethodFooter(methodName));
	}


}
