package nanotilertests.rnadesign.designapp;

import commandtools.*;
import org.testng.annotations.*;
import rnadesign.designapp.*;

public class FuseStrandsCommandTest {

    @Test(groups={"new"})
    public void testFuseStrandsCommandTest() {
	String methodName = "testFuseStrandsCommandTest";
	String s = "2J00.rnaview.pdb_j3_A-G1064_A-C1107_A-G1187_grow_ring_rnaview.pdb";
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/" + s + " findstems=false");
	    app.runScriptLine("tree residue");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("exportpdb " + methodName + "_initial.pdb");
	    app.runScriptLine("fusestrands root.import.D root.import.O");
	    app.runScriptLine("echo fusestrands root.import.N root.import.K");
	    app.runScriptLine("fusestrands root.import.N root.import.K");
	    app.runScriptLine("echo fusestrands root.import.G root.import.F");
	    app.runScriptLine("fusestrands root.import.G root.import.F");
	    app.runScriptLine("echo fusestrands root.import.A root.import.H");
	    app.runScriptLine("fusestrands root.import.A root.import.H");
	    app.runScriptLine("echo fusestrands root.import.M root.import.C");
	    app.runScriptLine("fusestrands root.import.M root.import.C");
	    app.runScriptLine("echo fusestrands root.import.L root.import.I");
	    app.runScriptLine("fusestrands root.import.L root.import.I");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("clear all");
	    app.runScriptLine("import " + methodName + ".pdb findstems=false");
	    app.runScriptLine("echo fusestrands root.import.A root.import.C");
	    app.runScriptLine("fusestrands root.import.A root.import.C");
	    app.runScriptLine("echo fusestrands root.import.B root.import.H");
	    app.runScriptLine("fusestrands root.import.B root.import.H");
	    app.runScriptLine("echo fusestrands root.import.E root.import.G");
	    app.runScriptLine("fusestrands root.import.E root.import.G");
	    app.runScriptLine("exportpdb " + methodName + "_final.pdb");
	    app.runScriptLine("clear all");
	    app.runScriptLine("import " + methodName + "_final.pdb findstems=false");
	    app.runScriptLine("fusestrands root.import.F root.import.C");
	    app.runScriptLine("fusestrands root.import.F root.import.E");
	    app.runScriptLine("exportpdb " + methodName + "_final2.pdb");
	}
	catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	}
    }

}
