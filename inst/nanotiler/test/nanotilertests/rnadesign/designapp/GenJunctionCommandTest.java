package nanotilertests.rnadesign.designapp;

import java.util.Properties;
import commandtools.*;
import generaltools.*;
import tools3d.objects3d.*;
import org.testng.annotations.*;
import rnadesign.designapp.*;

public class GenJunctionCommandTest {


    @Test(groups={"new"})
    public void testGenJunctionCommandTriangle() {
	String methodName = "testGenJunctionCommandTriangle";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/triangle.points");
	    app.runScriptLine("tree");
	    app.runScriptLine("echo Starting genjunction command...");
	    app.runScriptLine("genjunction root.import.p0");
	    app.runScriptLine("tree");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    Object3DSet junctions = app.getGraphController().getGraph().collectJunctions();
	    // assert junctions.size() > 0;
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandException e) {
	    System.out.println(e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new"})
    public void testGenJunctionCommandTriangleStar() {
	String methodName = "testGenJunctionCommandTriangleStar";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/trianglestar.points");
	    app.runScriptLine("tree");
	    app.runScriptLine("echo Starting genjunction command...");
	    app.runScriptLine("genjunction root.import.p0");
	    app.runScriptLine("tree");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    Object3DSet junctions = app.getGraphController().getGraph().collectJunctions();
	    // assert junctions.size() > 0;
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandException e) {
	    System.out.println(e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new", "slow"})
    public void testGenJunctionCommandCube() {
	String methodName = "testGenJunctionCommandCube";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("genshape cube");
	    app.runScriptLine("tree");
	    app.runScriptLine("echo Starting genjunction command...");
	    app.runScriptLine("genjunction root.cube.p1 offset=10");
	    app.runScriptLine("tree");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("bridgeall root");
	    app.runScriptLine("exportpdb " + methodName + "_bridged.pdb");
// 	    Object3DSet junctions = app.getGraphController().getGraph().collectJunctions();
// 	    assert junctions.size() > 0;
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandException e) {
	    System.out.println(e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Similar to testGenJunctionCommandCube, but using a different constraint */
    @Test(groups={"new", "slow"})
    public void testGenJunctionCommandCubeNoGap() {
	String methodName = "testGenJunctionCommandCubeNoGap";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("genshape cube");
	    app.runScriptLine("tree");
	    app.runScriptLine("echo Starting genjunction command...");
	    app.runScriptLine("genjunction root.cube.p1 offset=10 min=1.5 max=4.0");
	    app.runScriptLine("tree");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    // app.runScriptLine("bridgeall root");
	    // app.runScriptLine("exportpdb " + methodName + "_bridged.pdb");
	    // Object3DSet junctions = app.getGraphController().getGraph().collectJunctions();
	    // assert junctions.size() > 0;
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandException e) {
	    System.out.println(e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Similar to testGenJunctionCommandCube, but using a different constraint */
    @Test(groups={"new", "slow"})
    public void testGenJunctionCommandCubeGapLoop() {
	String methodName = "testGenJunctionCommandCubeGapLoop";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
            app.runScriptLine("source ../test/test_scripts/testGenJunctionCommandCubeGapLoop.script");
	}
	catch(CommandException e) {
	    System.out.println(e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Similar to testGenJunctionCommandCube, but using a different constraint that corresponds to zero loop length */
    @Test(groups={"new", "slow"})
    public void testGenJunctionCommandCubeNoGapLoop() {
	String methodName = "testGenJunctionCommandCubeNoGapLoop";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    //            app.runScriptLine("source ../test/test_scripts/testGenJunctionCommandCubeGapLoop.script");
	    for (int i = 8; i >= 5; --i) {
		app.runScriptLine("echo Working on junction offset " + i);
                app.runScriptLine("clear all");
		app.runScriptLine("genshape cube");
		Properties resultProperties = app.runScriptLine("genjunction root.cube.p1 offset=" + i + " min=1.5 max=4.0");
		if ((resultProperties.getProperty("best_score") != null) 
		    && (Double.parseDouble(resultProperties.getProperty("best_score")) == 0.0)) {
		    app.runScriptLine("exportpdb " + methodName + "_" + i + ".pdb");
		}
	    }
	}
	catch(CommandException e) {
	    System.out.println(e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Similar to testGenJunctionCommandCube, but using a different constraint */
    @Test(groups={"new", "slow"})
    public void testGenJunctionCommandCubeGapLoop3() {
	String methodName = "testGenJunctionCommandCubeGapLoop3";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    //            app.runScriptLine("source ../test/test_scripts/testGenJunctionCommandCubeGapLoop.script");
	    for (int i = 10; i <= 16; ++i) {
		app.runScriptLine("echo Working on junction offset " + i);
                app.runScriptLine("clear all");
		app.runScriptLine("genshape cube");
		Properties resultProperties = app.runScriptLine("genjunction root.cube.p1 offset=" + i 
								+ " min=3.0 max=9.0");
		if ((resultProperties.getProperty("best_score") != null) 
		    && (Double.parseDouble(resultProperties.getProperty("best_score")) == 0.0)) {
		    app.runScriptLine("exportpdb " + methodName + "_" + i + ".pdb");
		}
	    }
	}
	catch(CommandException e) {
	    System.out.println(e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Similar to testGenJunctionCommandCube, but using a different constraint. Loop over loop length sizes. */
    @Test(groups={"new", "slow"})
    public void testGenJunctionCommandCubeGapLoop4() {
	String methodName = "testGenJunctionCommandCubeGapLoop4";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
        double maxStretch0 = 2.0;
	double maxStretch = 7.0; // maximum stretch per residue (O3* - P distance that can be bridged)
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    //            app.runScriptLine("source ../test/test_scripts/testGenJunctionCommandCubeGapLoop.script");
            for (int bridgeLen = 3; bridgeLen >= 1; bridgeLen--) {
		double maxDist = (maxStretch * bridgeLen) + maxStretch0;
		for (int i = 25; i >= 5; --i) {
		    app.runScriptLine("echo Working on junction offset " + i);
		    app.runScriptLine("clear all");
		    app.runScriptLine("genshape cube");
		    Properties resultProperties = app.runScriptLine("genjunction root.cube.p1 offset=" + i 
								    + " min=3.0 max=" + maxDist);
		    if ((resultProperties.getProperty("best_score") != null) 
			&& (Double.parseDouble(resultProperties.getProperty("best_score")) == 0.0)) {
			app.runScriptLine("exportpdb " + methodName + "_b" + bridgeLen + "_d" + i + ".pdb");
			break; // solution found; quit inner loop
		    }
		}
            }
	}
	catch(CommandException e) {
	    System.out.println(e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new"})
    public void testGenJunctionCommandTetrahedron() {
	String methodName = "testGenJunctionCommandTetrahedron";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("genshape tetrahedron");
	    app.runScriptLine("tree");
	    app.runScriptLine("echo Starting genjunction command...");
	    app.runScriptLine("genjunction root.tetrahedron.p1 offset=22 min=16 max=19");
	    app.runScriptLine("tree");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("bridgeall root");
	    app.runScriptLine("exportpdb " + methodName + "_bridged.pdb");
	    Object3DSet junctions = app.getGraphController().getGraph().collectJunctions();
	    // assert junctions.size() > 0;
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandException e) {
	    System.out.println(e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new"})
    public void testGenJunctionCommandDodecahedron() {
	String methodName = "testGenJunctionCommandDodecahedron";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("genshape dodecahedron");
	    app.runScriptLine("tree");
	    app.runScriptLine("echo Starting genjunction command...");
	    app.runScriptLine("genjunction root.dodecahedron.p1 offset=10 max=3 min=1");
	    app.runScriptLine("tree");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("bridgeall root");
	    app.runScriptLine("exportpdb " + methodName + "_bridged.pdb");
// 	    Object3DSet junctions = app.getGraphController().getGraph().collectJunctions();
// 	    assert junctions.size() > 0;
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandException e) {
	    System.out.println(e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new", "slow"})
    public void testGenJunctionCommandOctahedron() {
	String methodName = "testGenJunctionCommandOctahedron";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("genshape octahedron");
	    app.runScriptLine("tree");
	    app.runScriptLine("echo Starting genjunction command...");
	    app.runScriptLine("genjunction root.octahedron.p1 offset=26 min=15 max=20 n=36");
	    app.runScriptLine("tree");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("bridgeall root");
	    app.runScriptLine("exportpdb " + methodName + "_bridged.pdb");
// 	    Object3DSet junctions = app.getGraphController().getGraph().collectJunctions();
// 	    assert junctions.size() > 0;
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandException e) {
	    System.out.println(e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


}
