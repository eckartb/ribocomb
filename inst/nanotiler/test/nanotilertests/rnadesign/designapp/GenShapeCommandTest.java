package nanotilertests.rnadesign.designapp;

import commandtools.CommandException;
import commandtools.CommandExecutionException;
import tools3d.objects3d.*;
import org.testng.annotations.*; // for testing
import rnadesign.rnacontrol.*;
import rnadesign.rnamodel.*;
import generaltools.TestTools;
import rnadesign.designapp.*;

/** Test class for import command */
public class GenShapeCommandTest {

    public GenShapeCommandTest() { }

    @Test (groups={"new"})
    public void testGenerateCube(){
	String methodName = "testGenerateCube";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("genshape cube length=200");
	    app.runScriptLine("tree");
	    Object3D graph = app.getGraphController().getGraph().findByFullName("root.cube");
	    assert graph != null;
	    assert graph.size() == 8; // we generated a cube, should have 8 sides
	    LinkSet links = app.getGraphController().getLinks();
	    assert links.size() == 12; // a cube should have 12 edges
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch (CommandException e) {
	    System.out.println("Command exception in genshape command test: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test (groups={"new"})
    public void testGenerateTetrahedron(){
	String methodName = "testGenerateTetrahedron";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("genshape tetrahedron length=50 name=tet");
	    app.runScriptLine("tree");
	    Object3D graph = app.getGraphController().getGraph().findByFullName("root.tet");
	    assert graph != null;
	    assert graph.size() == 4; // we generated a cube, should have 8 sides
	    LinkSet links = app.getGraphController().getLinks();
	    assert links.size() == 6; // a cube should have 12 edges
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch (CommandException e) {
	    System.out.println("Command exception in genshape command test: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test (groups={"new"})
    public void testGenerateRing(){
	String methodName = "testGenerateRing";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("genshape ring5 length=50");
	    app.runScriptLine("tree");
	    Object3D graph = app.getGraphController().getGraph().findByFullName("root.ring5");
	    assert graph != null;
	    assert graph.size() == 5; // we generated a cube, should have 8 sides
	    LinkSet links = app.getGraphController().getLinks();
	    assert links.size() == 5; // a cube should have 12 edges
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch (CommandException e) {
	    System.out.println("Command exception in genshape command test: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test (groups={"new"})
    public void testGeneratePrism(){
	String methodName = "testGeneratePrism";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("genshape prism7 length=1000");
	    app.runScriptLine("tree");
	    Object3D graph = app.getGraphController().getGraph().findByFullName("root.prism7");
	    assert graph != null;
	    assert graph.size() == 14; // we generated a heptagonal prism, should have 14 vertices
	    LinkSet links = app.getGraphController().getLinks();
	    assert links.size() == 21; // a prism should have 3N edges
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch (CommandException e) {
	    System.out.println("Command exception in genshape command test: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test (groups={"new"})
    public void testGenerateJunk(){
	String methodName = "testGenerateJunk";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("genshape junk length=50 name=tet");
	    app.runScriptLine("tree");
	    assert false; // should never be here, exception should have been thrown because of unknown keyword "junk"
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch (CommandException e) {
	    System.out.println("EXPECTED Command exception in genshape command test: " + e.getMessage());
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


}
