package nanotilertests.rnadesign.designapp;

import java.util.Properties;
import commandtools.*;
import generaltools.TestTools;
import org.testng.annotations.*;
import rnadesign.designapp.*;

public class GenerateDistanceConstraintCommandTest {

    /** Moves Malachite green binding aptamer to center of cube. Same as testCubeFragmentPlacement. */
    @Test(groups={"new", "production"})
    public void testCubeFragmentPlacement() {
	String methodName = "testCubeFragmentPlacement";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/optimizedcube_step12_fused2_rnaview.pdb findstems=false"); // import cube
	    app.runScriptLine("tree strand");
	    app.runScriptLine("characterize");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1Q8N_A_edit2_rnaview.pdb findstems=false"); // import fragment
	    app.runScriptLine("echo Randomizing orientation of fragment!");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("randomize root.import1");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("gendistconstraint root.import.B.44.O3* root.import1.2.1.P 7 7");
	    app.runScriptLine("gendistconstraint root.import.F.44.O3* root.import1.1.1.P 5 5");
	    // app.runScriptLine("links");
	    app.runScriptLine("optconstraints blocks=root.import;root.import1 steps=50000");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("tree strand"); // output of strand names
	    app.runScriptLine("remove root.import.B.44"); // just for debugging show which nucleotide was important
	    app.runScriptLine("remove root.import.F.44");
	    app.runScriptLine("exportpdb " + methodName + "_debug.pdb");
	}
	catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Moves Malachite green binding aptamer to center of cube. Same as testCubeFragmentPlacement,
     * only using 1Q8N_A_edit3_rnaview.pdb instead of 1Q8N_A_edit2_rnaview.pdb; 
     * also now connecting C-E and (lated in version 5) F-B */
    @Test(groups={"new", "production"})
    public void testCubeFragmentPlacement4() {
	String methodName = "testCubeFragmentPlacement4";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/optimizedcube_step12_fused2_rnaview.pdb findstems=false"); // import cube
	    app.runScriptLine("tree strand");
	    app.runScriptLine("characterize");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1Q8N_A_edit3_rnaview.pdb findstems=false"); // import fragment
	    app.runScriptLine("echo Randomizing orientation of fragment!");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("randomize root.import1");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("gendistconstraint root.import.C.44.O3* root.import1.2.1.P 13 13");
	    app.runScriptLine("gendistconstraint root.import.E.44.O3* root.import1.1.1.P 13 13");
	    // app.runScriptLine("links");
	    app.runScriptLine("optconstraints blocks=root.import;root.import1 steps=100000");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("tree strand"); // output of strand names
	    app.runScriptLine("remove root.import.B.44"); // just for debugging show which nucleotide was important
	    app.runScriptLine("remove root.import.F.44");
	    app.runScriptLine("exportpdb " + methodName + "_debug.pdb");
	}
	catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Moves Malachite green binding aptamer to center of cube. Same as testCubeFragmentPlacement,
     * only using 1Q8N_A_edit3_rnaview.pdb instead of 1Q8N_A_edit2_rnaview.pdb; 
     * also now connecting F-B */
    @Test(groups={"new", "production"})
    public void testCubeFragmentPlacement5() {
	String methodName = "testCubeFragmentPlacement5";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/optimizedcube_step12_fused2_rnaview.pdb findstems=false"); // import cube
	    app.runScriptLine("tree strand");
	    app.runScriptLine("characterize");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1Q8N_A_edit3_rnaview.pdb findstems=false"); // import fragment
	    app.runScriptLine("echo Randomizing orientation of fragment!");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("randomize root.import1");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("gendistconstraint root.import.B.44.O3* root.import1.2.1.P 13 13");
	    app.runScriptLine("gendistconstraint root.import.F.44.O3* root.import1.1.1.P 13 13");
	    // app.runScriptLine("links");
	    app.runScriptLine("optconstraints blocks=root.import;root.import1 steps=100000");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("tree strand"); // output of strand names
	    app.runScriptLine("remove root.import.B.44"); // just for debugging show which nucleotide was important
	    app.runScriptLine("remove root.import.F.44");
	    app.runScriptLine("exportpdb " + methodName + "_debug.pdb");
	}
	catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new", "production"})
    public void testCubeFragmentPlacementLoop() {
	String methodName = "testCubeFragmentPlacementLoop";
	int maxLoop = 100;
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    for (int i = 0; i < maxLoop; ++i) {
		app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/optimizedcube_step12_fused2_rnaview.pdb findstems=false"); // import cube
		app.runScriptLine("tree strand");
		app.runScriptLine("characterize");
		app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1Q8N_A_edit2_rnaview.pdb findstems=false"); // import fragment
		app.runScriptLine("echo Randomizing orientation of fragment!");
		app.runScriptLine("tree strand");
		app.runScriptLine("randomize root.import1"); // randomize orientation
		app.runScriptLine("tree strand");
		app.runScriptLine("gendistconstraint root.import.B.44.O3* root.import1.2.1.P 7 7");
		app.runScriptLine("gendistconstraint root.import.F.44.O3* root.import1.1.1.P 6 6");
		// app.runScriptLine("links");
		app.runScriptLine("optconstraints blocks=root.import;root.import1 steps=50000");
		app.runScriptLine("exportpdb " + methodName + "_" + (i+1) + ".pdb");
		app.runScriptLine("tree strand"); // output of strand names
		app.runScriptLine("bridgeit root.import.B.44 root.import1.2.1 rms=3.0 n=5 l=6");
		app.runScriptLine("bridgeit root.import.F.44 root.import1.1.1 rms=3.0 n=5 l=6");
		app.runScriptLine("exportpdb " + methodName + "_" + (i+1) + "_bridged.pdb");
		app.runScriptLine("clear all");
	    }
	}
	catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new", "production"})
    public void testCubeFragmentPlacementLoop3() {
	String methodName = "testCubeFragmentPlacementLoop3";
	int maxLoop = 100;
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    for (int i = 0; i < maxLoop; ++i) {
		app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/optimizedcube_step12_fused2_rnaview.pdb findstems=false"); // import cube
		app.runScriptLine("tree strand");
		app.runScriptLine("characterize");
		app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1Q8N_A_edit3_rnaview.pdb findstems=false"); // import fragment
		app.runScriptLine("echo Randomizing orientation of fragment!");
		app.runScriptLine("tree strand");
		app.runScriptLine("randomize root.import1"); // randomize orientation
		app.runScriptLine("tree strand");
		app.runScriptLine("gendistconstraint root.import.B.44.O3* root.import1.2.1.P 13 13");
		app.runScriptLine("gendistconstraint root.import.F.44.O3* root.import1.1.1.P 13 13");
		// app.runScriptLine("links");
		app.runScriptLine("optconstraints blocks=root.import;root.import1 steps=50000");
		app.runScriptLine("exportpdb " + methodName + "_" + (i+1) + ".pdb");
		app.runScriptLine("tree strand"); // output of strand names
		app.runScriptLine("bridgeit root.import.B.44 root.import1.2.1 rms=2.0 n=5 l=6");
		app.runScriptLine("bridgeit root.import.F.44 root.import1.1.1 rms=2.0 n=5 l=6");
		app.runScriptLine("exportpdb " + methodName + "_" + (i+1) + "_bridged.pdb");
		app.runScriptLine("clear all");
	    }
	}
	catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new", "production"})
    public void testCubeFragmentPlacementLoop4() {
	String methodName = "testCubeFragmentPlacementLoop4";
	int maxLoop = 100;
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    for (int i = 0; i < maxLoop; ++i) {
		app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/optimizedcube_step12_fused2_rnaview.pdb findstems=false"); // import cube
		app.runScriptLine("tree strand");
		app.runScriptLine("characterize");
		app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1Q8N_A_edit3_rnaview.pdb findstems=false"); // import fragment
		app.runScriptLine("echo Randomizing orientation of fragment!");
		app.runScriptLine("tree strand");
		app.runScriptLine("randomize root.import1"); // randomize orientation
		app.runScriptLine("tree strand");
		app.runScriptLine("gendistconstraint root.import.C.44.O3* root.import1.2.1.P 13 13");
		app.runScriptLine("gendistconstraint root.import.E.44.O3* root.import1.1.1.P 13 13");
		// app.runScriptLine("links");
		app.runScriptLine("optconstraints blocks=root.import;root.import1 steps=50000");
		app.runScriptLine("exportpdb " + methodName + "_" + (i+1) + ".pdb");
		app.runScriptLine("tree strand"); // output of strand names
		app.runScriptLine("bridgeit root.import.B.44 root.import1.2.1 rms=2.0 n=5 l=6");
		app.runScriptLine("bridgeit root.import.F.44 root.import1.1.1 rms=2.0 n=5 l=6");
		app.runScriptLine("exportpdb " + methodName + "_" + (i+1) + "_bridged.pdb");
		app.runScriptLine("clear all");
	    }
	}
	catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Places MG fragment between ends of strands F and B. Similar to testCubeFragmentPlacementLoop5,
     * however connection is performed other way around
     */
    @Test(groups={"new", "production"})
    public void testCubeFragmentPlacementLoop4b() {
	String methodName = "testCubeFragmentPlacementLoop4b";
	int maxLoop = 100;
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    for (int i = 0; i < maxLoop; ++i) {
		app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/optimizedcube_step12_fused2_rnaview.pdb findstems=false"); // import cube
		app.runScriptLine("tree strand");
		app.runScriptLine("characterize");
		app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1Q8N_A_edit3_rnaview.pdb findstems=false"); // import fragment
		app.runScriptLine("echo Randomizing orientation of fragment!");
		app.runScriptLine("tree strand");
		app.runScriptLine("randomize root.import1"); // randomize orientation
		app.runScriptLine("tree strand");
		app.runScriptLine("gendistconstraint root.import.C.44.O3* root.import1.2.1.P 13 13");
		app.runScriptLine("gendistconstraint root.import.E.44.O3* root.import1.1.1.P 13 13");
		// app.runScriptLine("links");
		app.runScriptLine("optconstraints blocks=root.import;root.import1 steps=50000");
		app.runScriptLine("exportpdb " + methodName + "_" + (i+1) + ".pdb");
		app.runScriptLine("tree strand"); // output of strand names
		app.runScriptLine("bridgeit root.import.B.44 root.import1.2.1 rms=2.0 n=5 l=6");
		app.runScriptLine("bridgeit root.import.F.44 root.import1.1.1 rms=2.0 n=5 l=6");
		app.runScriptLine("exportpdb " + methodName + "_" + (i+1) + "_bridged.pdb");
		app.runScriptLine("clear all");
	    }
	}
	catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new", "production"})
    public void testCubeFragmentPlacementLoop5() {
	String methodName = "testCubeFragmentPlacementLoop5";
	int maxLoop = 100;
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    for (int i = 0; i < maxLoop; ++i) {
		app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/optimizedcube_step12_fused2_rnaview.pdb findstems=false"); // import cube
		app.runScriptLine("tree strand");
		app.runScriptLine("characterize");
		app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1Q8N_A_edit3_rnaview.pdb findstems=false"); // import fragment
		app.runScriptLine("echo Randomizing orientation of fragment!");
		app.runScriptLine("tree strand");
		app.runScriptLine("randomize root.import1"); // randomize orientation
		app.runScriptLine("tree strand");
		app.runScriptLine("gendistconstraint root.import.F.44.O3* root.import1.2.1.P 13 13");
		app.runScriptLine("gendistconstraint root.import.B.44.O3* root.import1.1.1.P 13 13");
		// app.runScriptLine("links");
		app.runScriptLine("optconstraints blocks=root.import;root.import1 steps=50000");
		app.runScriptLine("exportpdb " + methodName + "_" + (i+1) + ".pdb");
		app.runScriptLine("tree strand"); // output of strand names
		app.runScriptLine("bridgeit root.import.F.44 root.import1.2.1 rms=2.0 n=5 l=6");
		app.runScriptLine("bridgeit root.import.B.44 root.import1.1.1 rms=2.0 n=5 l=6");
		app.runScriptLine("exportpdb " + methodName + "_" + (i+1) + "_bridged.pdb");
		app.runScriptLine("clear all");
	    }
	}
	catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new", "production"})
    public void testTriangleSymmetryPlacement() {
	String methodName = "testTriangleSymmetryPlacement";
	int maxLoop = 100;
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/helix10_1.pdb name=import");
	    app.runScriptLine("tree");
	    app.runScriptLine("synth cs cs1 root 0 0 0"); // generate coordinate system for symmetry copy 120 rotated
	    app.runScriptLine("select root.cs1");
	    app.runScriptLine("rotate 120 0 0 1");
	    app.runScriptLine("synth cs cs2 root 0 0 0"); // generate coordinate system for symmetry copy 240 rotated
	    app.runScriptLine("select root.cs2");
	    app.runScriptLine("rotate 240 0 0 1"); // generate coordinate system for symmetry copy 240 rotated
	    app.runScriptLine("symadd root.cs1"); // add to used symmetry
	    app.runScriptLine("symadd root.cs2");
	    app.runScriptLine("syminfo");
	    app.runScriptLine("echo Generating first distance constraint:");
	    app.runScriptLine("gendistconstraint root.import.1.G10.C3* root.import.1.1.O5* 8 8 sym1=0 sym2=1");
	    app.runScriptLine("gendistconstraint root.import.2.1.O5* root.import.2.10.C3* 8 8 sym1=0 sym2=1");

	    app.runScriptLine("gendistconstraint root.import.1.G10.C3* root.import.1.1.O5*  8 8 sym1=1 sym2=2");
	    app.runScriptLine("gendistconstraint root.import.2.1.O5* root.import.2.10.C3* 8 8 sym1=1 sym2=2");

	    app.runScriptLine("gendistconstraint root.import.1.G10.C3* root.import.1.1.O5*  8 8 sym1=2 sym2=0");
	    app.runScriptLine("gendistconstraint root.import.2.1.O5* root.import.2.10.C3* 8 8 sym1=2 sym2=0");

	    app.runScriptLine("optconstraints blocks=root.import steps=100000 repuls=5");
	    app.runScriptLine("clone root.import root import2");
	    app.runScriptLine("clone root.import root import3");
	    app.runScriptLine("symapply root.import2 1"); //  apply symmetry transformations
	    app.runScriptLine("symapply root.import3 2");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    Properties resultProperties = app.runScriptLine("dist root.import.2.1.O5* root.import2.2.10.C3*"); // should have small distance
	    System.out.println(resultProperties);
	    assert(Double.parseDouble(resultProperties.getProperty("dist")) < 10.0);
	}
	catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

}
