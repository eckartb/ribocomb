package nanotilertests.rnadesign.designapp;

import java.util.Properties;
import tools3d.objects3d.Object3DSet;
import generaltools.TestTools;
import commandtools.CommandException;
import org.testng.annotations.*;
import rnadesign.designapp.*;

public class GenerateHelixCommandTest {

    public GenerateHelixCommandTest() { }

    @Test(groups={"new"})
    public void testGenerateIsolatedHelix() {
	String methodName = "testGenerateIsolatedHelix";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("genhelix bp=20");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new"})
    public void testGenerate1BPHelix() {
	String methodName = "testGenerate1BPHelix";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("genhelix bp=1");
	    Object3DSet residues = app.getGraphController().getGraph().collectResidues();
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    assert residues.size() == 2;
	    for (int i = 0; i < residues.size(); ++i) {
		if (residues.get(i).getIndexOfChild("P") < 0) {
		    assert false; // check if phosphor atom is present
		}
	    }
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new", "slow"})
    public void testGenerateUnmodifiedIsolatedAUHelix() {
	String methodName = "testGenerateUnmodifiedIsolatedAUHelix";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("loadbasepairs ${NANOTILER_HOME}/resources/2J00_A_rnaview.pdb"); // whole ribosome structure
	    app.runScriptLine("genhelix bp=14 bases=AU");
	    app.runScriptLine("tree");
	    app.runScriptLine("mutate helix_forw:G2,C13 helix_back:C13,G2");
	    // root.helix_root.helix_forw
	    app.runScriptLine("remove root.helix_root.helix_forw.A1"); // remove 4 ending bases
	    app.runScriptLine("remove root.helix_root.helix_forw.A14"); // remove 4 ending bases
	    app.runScriptLine("remove root.helix_root.helix_back.U1"); // remove 4 ending bases
	    app.runScriptLine("remove root.helix_root.helix_back.U14"); // remove 4 ending bases
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new", "slow"})
    public void testGenerateUnmodifiedIsolatedGCHelix() {
	String methodName = "testGenerateUnmodifiedIsolatedGCHelix";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("loadbasepairs ${NANOTILER_HOME}/resources/2J00_A_rnaview.pdb"); // whole ribosome structure
	    app.runScriptLine("genhelix bp=14 bases=GC");
	    app.runScriptLine("tree");
	    app.runScriptLine("mutate helix_forw:G2,C13 helix_back:C13,G2");
	    // root.helix_root.helix_forw
	    app.runScriptLine("remove root.helix_root.helix_forw.G1"); // remove 4 ending bases
	    app.runScriptLine("remove root.helix_root.helix_forw.G14"); // remove 4 ending bases
	    app.runScriptLine("remove root.helix_root.helix_back.C1"); // remove 4 ending bases
	    app.runScriptLine("remove root.helix_root.helix_back.C14"); // remove 4 ending bases
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new", "slow"})
    public void testGenerateModifiedIsolatedHelix() {
	String methodName = "testGenerateModifiedIsolatedHelix";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB_modA_again.pdb");
	    app.runScriptLine("loadbasepairs ${NANOTILER_HOME}/resources/2J00_A_rnaview.pdb"); // whole ribosome structure
	    app.runScriptLine("genhelix bp=14 bases=AU");
	    app.runScriptLine("tree");
	    app.runScriptLine("mutate helix_forw:G2,C13 helix_back:C13,G2");
	    // root.helix_root.helix_forw
	    app.runScriptLine("remove root.helix_root.helix_forw.A1"); // remove 4 ending bases
	    app.runScriptLine("remove root.helix_root.helix_forw.A14"); // remove 4 ending bases
	    app.runScriptLine("remove root.helix_root.helix_back.U1"); // remove 4 ending bases
	    app.runScriptLine("remove root.helix_root.helix_back.U14"); // remove 4 ending bases
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new","slow"})
    public void testGenerateAndMutateIsolatedHelix() {
	String methodName = "testGenerateAndMutateIsolatedHelix";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB_modA_again.pdb");
	    app.runScriptLine("# Load 1 reference A-U base pair containing a modified A:");
	    app.runScriptLine("loadbasepairs ${NANOTILER_HOME}/resources/NA1_U_edit2.pdb"); // whole ribosome structure
	    app.runScriptLine("# Generate helix:");
	    app.runScriptLine("genhelix bp=12 bases=GC");
	    app.runScriptLine("tree");
	    app.runScriptLine("links");
	    app.runScriptLine("# Mutate G-C pairs to A-U except at strand ends:");
	    app.runScriptLine("mutate helix_forw:A2,A3,A4,A5,A6,A7,A8,A9,A10,A11 helix_back:U11,U10,U9,U8,U7,U6,U5,U4,U3,U2");
	    // root.helix_root.helix_forw
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Compare synthetic helix with 52bp helix generated by Accellrys Discovery Studio */
    @Test(groups={"new"})
    public void testGenerateIsolatedHelix2GC() {
	double rmsLimit = 0.3; // pretty good agreement with reference helix
	String methodName = "testGenerateIsolatedHelix2GC";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("import ${NANOTILER_HOME}/resources/helices2/GC52.pdb");
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("genhelix bp=40");
	    app.runScriptLine("tree strand");
	    Properties prop = app.runScriptLine("align A:2-41;B:12-51 helix_forw:1-40;helix_back:1-40 root.import root.helix_root atoms=P,C1*,C4*,O3*,N1,C2,N3");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    if (Double.parseDouble(prop.getProperty("score")) > rmsLimit) { // must be alignment close to this RMS value!
		System.out.println("Insufficient RMS of helix compared to reference helix!");
	    }
	    assert (Double.parseDouble(prop.getProperty("score")) <= rmsLimit); // must be alignment close to this RMS value!
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Compare synthetic helix with 52bp helix generated by Accellrys Discovery Studio */
    @Test(groups={"new"})
    public void testGenerateIsolatedHelix2AU() {
	double rmsLimit = 0.5; // not as accurate as GC helix
	String methodName = "testGenerateIsolatedHelix2AU";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("import ${NANOTILER_HOME}/resources/helices2/AU52.pdb");
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("genhelix bp=40 bases=AU");
	    app.runScriptLine("tree strand");
	    Properties prop = app.runScriptLine("align A:2-41;B:12-51 helix_forw:1-40;helix_back:1-40 root.import root.helix_root atoms=P,C1*,C4*,O3*,N1,C2,N3");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    if (Double.parseDouble(prop.getProperty("score")) > rmsLimit) { // must be alignment close to this RMS value!
		System.out.println("Insufficient RMS of helix compared to reference helix!");
	    }
	    assert (Double.parseDouble(prop.getProperty("score")) <= rmsLimit); // must be alignment close to this RMS value!
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


    /** Extend an exhisting helix with a generated helix. Check manually for steric clashes. */
    @Test(groups={"new"})
    public void testGenerateAttachedHelix() {
	String methodName = "testGenerateAttachedHelix";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("import ${NANOTILER_HOME}/resources/helices2/AU52.pdb findstems=true");
            app.runScriptLine("tree hxend");
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("genhelix bp=15 bd1=root.import.stems.stemA_B_1.B_52_A_1");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


}
