package nanotilertests.rnadesign.designapp;

import org.testng.*;
import org.testng.annotations.*;
import generaltools.TestTools;
import commandtools.*;
import rnadesign.designapp.*;

public class GenerateJunctionConstraintCommandTest {

    @Test(groups={"new"})
    public void testGenerateJunctionConstraintCommandTwoHelices() {
	NanoTilerScripter app = new NanoTilerScripter();
	String methodName = "testGenerateJunctionConstraintCommandTwoHelices";
	System.out.println(TestTools.generateMethodHeader(methodName));
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("set distMin 2.5");
	    app.runScriptLine("set distMax 3");
	    app.runScriptLine("synth simple square root 0 0 0");
	    app.runScriptLine("genhelix bp=12 root=root.square name=helix0");
	    app.runScriptLine("genhelix bp=12 root=root.square name=helix1");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("echo Links before genjunctionconstraint " + methodName);
	    app.runScriptLine("links");
	    app.runScriptLine("genjunctionconstraint ends=root.square.helix0_root.helix0_forw.1,root.square.helix0_root.helix0_back.12,root.square.helix1_root.helix1_forw.1,root.square.helix1_root.helix1_back.12 min=${distMin} max=${distMax}");
	    app.runScriptLine("echo Links after genjunctionconstraint " + methodName);
	    app.runScriptLine("links");
	    app.runScriptLine("echo Still running " + methodName);
	    app.runScriptLine("optconstraints vdw=1 blocks=root.square.helix0_root;root.square.helix1_root");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + ".script");
	} catch (CommandException ce) {
	    System.out.println("Exception thrown in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new"})
    public void testGenerateJunctionConstraintCommandSquare() {
	NanoTilerScripter app = new NanoTilerScripter();
	String methodName = "testGenerateJunctionConstraintCommandSquare";
	System.out.println(TestTools.generateMethodHeader(methodName));
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("set distMin 3.0");
	    app.runScriptLine("set distMax 9.0");
	    app.runScriptLine("set bp 10");
	    app.runScriptLine("synth simple square root 0 0 0");
	    app.runScriptLine("genhelix bp=${bp} root=root.square name=helix0");
	    app.runScriptLine("genhelix bp=${bp} root=root.square name=helix1");
	    app.runScriptLine("genhelix bp=${bp} root=root.square name=helix2");
	    app.runScriptLine("genhelix bp=${bp} root=root.square name=helix3");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("echo Links before genjunctionconstraint " + methodName);
	    app.runScriptLine("links");
	    app.runScriptLine("genjunctionconstraint ends=root.square.helix0_root.helix0_forw.1,root.square.helix0_root.helix0_back.${bp},root.square.helix1_root.helix1_back.1,root.square.helix1_root.helix1_forw.${bp} min=${distMin} max=${distMax}");
	    app.runScriptLine("genjunctionconstraint ends=root.square.helix1_root.helix1_forw.1,root.square.helix1_root.helix1_back.${bp},root.square.helix2_root.helix2_back.1,root.square.helix2_root.helix2_forw.${bp} min=${distMin} max=${distMax}");
	    app.runScriptLine("genjunctionconstraint ends=root.square.helix2_root.helix2_forw.1,root.square.helix2_root.helix2_back.${bp},root.square.helix3_root.helix3_back.1,root.square.helix3_root.helix3_forw.${bp} min=${distMin} max=${distMax}");
	    app.runScriptLine("genjunctionconstraint ends=root.square.helix3_root.helix3_forw.1,root.square.helix3_root.helix3_back.${bp},root.square.helix0_root.helix0_back.1,root.square.helix0_root.helix0_forw.${bp} min=${distMin} max=${distMax}");
	    app.runScriptLine("echo Links after genjunctionconstraint " + methodName);
	    app.runScriptLine("links");
	    app.runScriptLine("echo Still running " + methodName);
	    app.runScriptLine("optconstraints vdw=1 blocks=root.square.helix0_root;root.square.helix1_root;root.square.helix2_root;root.square.helix3_root");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + ".script");
	} catch (CommandException ce) {
	    System.out.println("Exception thrown in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new"})
    public void testGenerateJunctionConstraintCommandSquareGrid() {
	NanoTilerScripter app = new NanoTilerScripter();
	String methodName = "testGenerateJunctionConstraintCommandSquareGrid";
	System.out.println(TestTools.generateMethodHeader(methodName));
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("set distMin 3.0");
	    app.runScriptLine("set distMax 20.0");
	    for (int bp = 6; bp <= 20; bp+=2) {
		// app.runScriptLine("foreach bp ( 8 10 12 14 16 )");
		for (int len = -20; len >= -70; len-=10) {
		    // app.runScriptLine("foreach len ( -20 -30 -40 -50 )");
		    System.out.println("Working on " + bp + " base pairs and length offset of " + len);
		    app.runScriptLine("clear all");
		    app.runScriptLine("set dx " + len);
		    app.runScriptLine("set dy " + len);
		    app.runScriptLine("synth simple square root 0 0 0");
		    app.runScriptLine("synth cs tx root 0 0 0");
		    app.runScriptLine("synth cs ty root 0 0 0");
		    app.runScriptLine("select root.tx");
		    app.runScriptLine("shift ${dx} 0 0");
		    app.runScriptLine("symadd root.tx");
		    app.runScriptLine("select root.ty");
		    app.runScriptLine("shift 0 ${dy} 0");
		    app.runScriptLine("symadd root.ty");
		    app.runScriptLine("syminfo");
		    app.runScriptLine("genhelix bp="+bp + " root=root.square name=helix0");
		    app.runScriptLine("genhelix bp="+bp + " root=root.square name=helix1");
		    app.runScriptLine("randomize .square.helix0_root");
		    app.runScriptLine("randomize .square.helix1_root");
		    app.runScriptLine("exportpdb " + methodName + "_initial.pdb");
		    app.runScriptLine("tree strand");
		    // app.runScriptLine("echo Links before genjunctionconstraint " + methodName);
		    // app.runScriptLine("links");
		    // describe 4 junctions counter clock wise
		    app.runScriptLine("genjunctionconstraint ends=.square.helix0_root.helix0_forw.1,.square.helix0_root.helix0_back.LAST,.square.helix1_root.helix1_forw.1,.square.helix1_root.helix1_back.LAST,.square.helix0_root.helix0_back.1,.square.helix0_root.helix0_forw.LAST,.square.helix1_root.helix1_back.1,.square.helix1_root.helix1_forw.LAST min=${distMin} max=${distMax} sym=0,0,1,2");
		    // app.runScriptLine("echo Links after genjunctionconstraint " + methodName);
		    // app.runScriptLine("links");
		    app.runScriptLine("echo Still running " + methodName);
		    app.runScriptLine("optconstraints vdw=1 repuls=0 kt=20 steps=100000 blocks=root.square.helix0_root;root.square.helix1_root");
		    app.runScriptLine("exportpdb " + methodName + ".pdb");
		    app.runScriptLine("clone root.square.helix0_root root helix0_1_1");
		    app.runScriptLine("clone root.square.helix0_root root helix0_1_2");
		    app.runScriptLine("clone root.square.helix0_root root helix0_1_3");
		    app.runScriptLine("clone root.square.helix0_root root helix0_2_1"); // other sym operation
		    app.runScriptLine("clone root.square.helix1_root root helix1_2_1");
		    app.runScriptLine("clone root.square.helix1_root root helix1_1_1"); // other sym operation
		    app.runScriptLine("clone root.square.helix1_root root helix1_2_2");
		    app.runScriptLine("clone root.square.helix1_root root helix1_2_3");
		    app.runScriptLine("symapply root.helix0_1_1 1");
		    app.runScriptLine("symapply root.helix0_2_1 2");
		    app.runScriptLine("symapply root.helix0_1_2 1");
		    app.runScriptLine("symapply root.helix0_1_2 1");
		    app.runScriptLine("symapply root.helix0_1_3 1");
		    app.runScriptLine("symapply root.helix0_1_3 1");
		    app.runScriptLine("symapply root.helix0_1_3 1");
		    app.runScriptLine("symapply root.helix1_2_1 2");
		    app.runScriptLine("symapply root.helix1_1_1 1");
		    app.runScriptLine("symapply root.helix1_2_2 2");
		    app.runScriptLine("symapply root.helix1_2_2 2");
		    app.runScriptLine("symapply root.helix1_2_3 2");
		    app.runScriptLine("symapply root.helix1_2_3 2");
		    app.runScriptLine("symapply root.helix1_2_3 2");
		    app.runScriptLine("tree strand");
		    app.runScriptLine("exportpdb " + methodName + "_" + bp + "_" + len + "_final.pdb");
		    // app.runScriptLine("end");
		    // app.runScriptLine("end");
		}
	    }
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + ".script");
	} catch (CommandException ce) {
	    System.out.println("Exception thrown in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new"})
    public void testGenerateJunctionConstraintCommandJ4() {
	NanoTilerScripter app = new NanoTilerScripter();
	String methodName = "testGenerateJunctionConstraintCommandSquareGrid";
	System.out.println(TestTools.generateMethodHeader(methodName));
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("set distMin 3.0");
	    app.runScriptLine("set distMax 15.0");
	    app.runScriptLine("set bp 10");
	    app.runScriptLine("genhelix bp=${bp} root=root.square name=helix0");
	    app.runScriptLine("genhelix bp=${bp} root=root.square name=helix1");
	    app.runScriptLine("genhelix bp=${bp} root=root.square name=helix2");
	    app.runScriptLine("genhelix bp=${bp} root=root.square name=helix3");
	    app.runScriptLine("randomize .square.helix0_root");
	    app.runScriptLine("randomize .square.helix1_root");
	    app.runScriptLine("randomize .square.helix2_root");
	    app.runScriptLine("randomize .square.helix3_root");

	    app.runScriptLine("exportpdb " + methodName + "_initial.pdb");
	    app.runScriptLine("tree strand");
	    // app.runScriptLine("echo Links before genjunctionconstraint " + methodName);
	    // app.runScriptLine("links");
	    // describe 4 junctions counter clock wise
	    app.runScriptLine("genjunctionconstraint ends=.square.helix0_root.1.1,.square.helix0_root.2.LAST,.square.helix1_root.1.1,root.square.helix1_root.2.LAST,root.square.helix2_root.1.1,root.square.helix2_root.2.LAST,root.square.helix3_root.1.1,root.square.helix3_root.2.LAST min=${distMin} max=${distMax} ");
	    // app.runScriptLine("echo Links after genjunctionconstraint " + methodName);
	    // app.runScriptLine("links");
	    app.runScriptLine("echo Still running " + methodName);
	    app.runScriptLine("optconstraints vdw=0 repuls=10 kt=100 steps=1000000 blocks=root.square.helix0_root;root.square.helix1_root;root.square.helix2_root;root.square.helix3_root");
	    app.runScriptLine("exportpdb " + methodName + "_final.pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + ".script");
	} catch (CommandException ce) {
	    System.out.println("Exception thrown in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }
    
}
