package nanotilertests.rnadesign.designapp;

import org.testng.*;
import org.testng.annotations.*;
import generaltools.TestTools;
import commandtools.*;
import java.util.Properties;
import rnadesign.designapp.*;

public class GenerateJunctionDBConstraintCommandTest {

    @Test(groups={"new"})
    public void testGenerateJunctionDBConstraintCommandTwoHelices() {
	NanoTilerScripter app = new NanoTilerScripter();
	String methodName = "testGenerateJunctionConstraintCommandTwoHelices";
	System.out.println(TestTools.generateMethodHeader(methodName));
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    // app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/dimer2.names ${NANOTILER_HOME}/test/fixtures");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/j2.names ${NANOTILER_HOME}/test/fixtures");
	    app.runScriptLine("status");
	    app.runScriptLine("synth simple square root 0 0 0");
	    app.runScriptLine("genhelix bp=12 root=root.square name=helix0");
	    app.runScriptLine("genhelix bp=12 root=root.square name=helix1");
	    app.runScriptLine("tree allowed=BranchDescriptor3D");
	    app.runScriptLine("echo Links before genjunctionconstraint " + methodName);
	    app.runScriptLine("links");
	    app.runScriptLine("genjunctiondbconstraint jl1 root.square.helix0_root.helix0_forw_1_helix0_back_12 root.square.helix1_root.helix1_forw_1_helix1_back_12");
	    app.runScriptLine("echo Links after genjunctionconstraint " + methodName);
	    app.runScriptLine("links");
	    app.runScriptLine("echo Still running " + methodName);
	    app.runScriptLine("optconstraints vdw=1 blocks=root.square.helix0_root;root.square.helix1_root steps=100000 error=0.1");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + ".script");
	} catch (CommandException ce) {
	    System.out.println("Exception thrown in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new"})
    public void testGenerateJunctionDBConstraintCommandThreeHelices() {
	NanoTilerScripter app = new NanoTilerScripter();
	String methodName = "testGenerateJunctionConstraintCommandThreeHelices";
	System.out.println(TestTools.generateMethodHeader(methodName));
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    // app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/dimer2.names ${NANOTILER_HOME}/test/fixtures");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/j2.names ${NANOTILER_HOME}/test/fixtures");
	    app.runScriptLine("status");
	    app.runScriptLine("synth simple square root 0 0 0");
	    app.runScriptLine("set BP 4");
	    app.runScriptLine("genhelix bp=${BP} root=root.square name=helix0");
	    app.runScriptLine("genhelix bp=${BP} root=root.square name=helix1");
	    app.runScriptLine("genhelix bp=${BP} root=root.square name=helix2");
	    app.runScriptLine("tree allowed=BranchDescriptor3D");
	    app.runScriptLine("echo Links before genjunctionconstraint " + methodName);
	    app.runScriptLine("links");
	    app.runScriptLine("genjunctiondbconstraint jl1 root.square.helix0_root.helix0_forw_1_helix0_back_${BP} root.square.helix1_root.helix1_back_1_helix1_forw_${BP}");
	    app.runScriptLine("genjunctiondbconstraint jl2 root.square.helix1_root.helix1_forw_1_helix1_back_${BP} root.square.helix2_root.helix2_back_1_helix2_forw_${BP}");
	    app.runScriptLine("echo Links after genjunctionconstraint " + methodName);
	    app.runScriptLine("links");
	    app.runScriptLine("echo Still running " + methodName);
	    app.runScriptLine("optconstraints vdw=1 blocks=root.square.helix0_root;root.square.helix1_root;root.square.helix2_root steps=100000 error=0.1");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + ".script");
	} catch (CommandException ce) {
	    System.out.println("Exception thrown in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new"})
    public void testGenerateJunctionDBConstraintCommandFourHelices() {
	NanoTilerScripter app = new NanoTilerScripter();
	String methodName = "testGenerateJunctionConstraintCommandThreeHelices";
	System.out.println(TestTools.generateMethodHeader(methodName));
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    // app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/dimer2.names ${NANOTILER_HOME}/test/fixtures");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/j2.names ${NANOTILER_HOME}/test/fixtures");
	    app.runScriptLine("status");
	    app.runScriptLine("synth simple square root 0 0 0");
	    app.runScriptLine("set BP 4");
	    app.runScriptLine("genhelix bp=${BP} root=root.square name=helix0");
	    app.runScriptLine("genhelix bp=${BP} root=root.square name=helix1");
	    app.runScriptLine("genhelix bp=${BP} root=root.square name=helix2");
	    app.runScriptLine("genhelix bp=${BP} root=root.square name=helix3");
	    app.runScriptLine("tree allowed=BranchDescriptor3D");
	    app.runScriptLine("echo Links before genjunctionconstraint " + methodName);
	    app.runScriptLine("links");
	    app.runScriptLine("genjunctiondbconstraint jl1 root.square.helix0_root.helix0_forw_1_helix0_back_${BP} root.square.helix1_root.helix1_back_1_helix1_forw_${BP}");
	    app.runScriptLine("genjunctiondbconstraint jl2 root.square.helix1_root.helix1_forw_1_helix1_back_${BP} root.square.helix2_root.helix2_back_1_helix2_forw_${BP}");
	    app.runScriptLine("genjunctiondbconstraint jl3 root.square.helix2_root.helix2_forw_1_helix2_back_${BP} root.square.helix3_root.helix3_back_1_helix3_forw_${BP}");
	    app.runScriptLine("echo Links after genjunctionconstraint " + methodName);
	    app.runScriptLine("links");
	    app.runScriptLine("echo Still running " + methodName);
	    app.runScriptLine("optconstraints vdw=1 blocks=root.square.helix0_root;root.square.helix1_root;root.square.helix2_root;root.square.helix3_root steps=1000000 error=0.1");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + ".script");
	} catch (CommandException ce) {
	    System.out.println("Exception thrown in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Place two helices, such that a kissing loop from database fits. */
    @Test(groups={"new"})
    public void testGenerateJunctionDBConstraintCommandTwoHelices2() {
	NanoTilerScripter app = new NanoTilerScripter();
	String methodName = "testGenerateJunctionConstraintCommandTwoHelices2";
	System.out.println(TestTools.generateMethodHeader(methodName));
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/kl_1FFK.names ${NANOTILER_HOME}/test/fixtures");
	    app.runScriptLine("status");
	    app.runScriptLine("synth simple square root 0 0 0");
	    app.runScriptLine("genhelix bp=12 root=root.square name=helix0");
	    app.runScriptLine("genhelix bp=12 root=root.square name=helix1");
	    app.runScriptLine("tree allowed=BranchDescriptor3D");
	    app.runScriptLine("echo Links before genjunctionconstraint " + methodName);
	    app.runScriptLine("links");
	    app.runScriptLine("genjunctiondbconstraint jl1 root.square.helix0_root.helix0_forw_1_helix0_back_12 root.square.helix1_root.helix1_forw_1_helix1_back_12");
	    app.runScriptLine("echo Links after genjunctionconstraint " + methodName);
	    app.runScriptLine("links");
	    app.runScriptLine("echo Still running " + methodName);
	    app.runScriptLine("optconstraints vdw=1 blocks=root.square.helix0_root;root.square.helix1_root steps=10000");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + ".script");
	} catch (CommandException ce) {
	    System.out.println("Exception thrown in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new"})
    public void testGenerateJunctionDBConstraintCommandTwoHelicesCircle() {
	NanoTilerScripter app = new NanoTilerScripter();
	String methodName = "testGenerateJunctionConstraintCommandTwoHelicesCircle";
	System.out.println(TestTools.generateMethodHeader(methodName));
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    // app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/dimer2.names ${NANOTILER_HOME}/test/fixtures");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/dimer2.names ${NANOTILER_HOME}/test/fixtures");
	    app.runScriptLine("status");
	    app.runScriptLine("synth simple square root 0 0 0");
	    app.runScriptLine("genhelix bp=12 root=root.square name=helix0");
	    app.runScriptLine("genhelix bp=12 root=root.square name=helix1");
	    app.runScriptLine("tree allowed=BranchDescriptor3D");
	    app.runScriptLine("echo Links before genjunctionconstraint " + methodName);
	    app.runScriptLine("links");
	    /* 
	       root.square.helix0_root.helix0_forw_1_helix0_back_12 1.1.1.4 1 BranchDescriptor3D   0.000   0.000   0.000
	       root.square.helix0_root.helix0_back_1_helix0_forw_12 1.1.1.5 1 BranchDescriptor3D   0.000   0.000  29.020
	       root.square.helix1_root.helix1_forw_1_helix1_back_12 1.1.2.4 1 BranchDescriptor3D   0.000   0.000   0.000
	       root.square.helix1_root.helix1_back_1_helix1_forw_12 1.1.2.5 1 BranchDescriptor3D   0.000   0.000  29.020
	    */
	    app.runScriptLine("genjunctiondbconstraint jl1 root.square.helix0_root.helix0_forw_1_helix0_back_12 root.square.helix1_root.helix1_forw_1_helix1_back_12");
	    app.runScriptLine("genjunctiondbconstraint jl2 root.square.helix0_root.helix0_back_1_helix0_forw_12 root.square.helix1_root.helix1_back_1_helix1_forw_12");
	    app.runScriptLine("echo Links after genjunctionconstraint " + methodName);
	    app.runScriptLine("links");
	    app.runScriptLine("echo Still running " + methodName);
	    app.runScriptLine("optconstraints vdw=1 blocks=root.square.helix0_root;root.square.helix1_root steps=100000 error=0.1");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + ".script");
	} catch (CommandException ce) {
	    System.out.println("Exception thrown in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"newest_later"})
    public void testGenerateJunctionDBConstraintCommandSquare() {
	NanoTilerScripter app = new NanoTilerScripter();
	String methodName = "testGenerateJunctionConstraintCommandTriangle";
	System.out.println(TestTools.generateMethodHeader(methodName));
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    // app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/dimer2.names ${NANOTILER_HOME}/test/fixtures");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/j2_1Q82/j2_1Q82.names ${NANOTILER_HOME}/test/fixtures/j2_1Q82");
	    app.runScriptLine("status");
	    // app.runScriptLine("synth simple square root 0 0 0");
	    app.runScriptLine("genshape ring4 length=40");
            app.runScriptLine("set BP 4");
	    Properties traceProperties = app.runScriptLine("tracernagraph root=root.ring4 bpmax=${BP}");
	    /* app.runScriptLine("genhelix bp=${BP} root=root.square name=helix0");
	    app.runScriptLine("genhelix bp=${BP} root=root.square name=helix1");
	    app.runScriptLine("genhelix bp=${BP} root=root.square name=helix2");
	    app.runScriptLine("genhelix bp=${BP} root=root.square name=helix3"); */
	    app.runScriptLine("tree allowed=BranchDescriptor3D");
	    app.runScriptLine("echo Links before genjunctionconstraint " + methodName);
	    app.runScriptLine("links");
	    /* 
	       root.square.helix0_root.helix0_forw_1_helix0_back_${BP} 1.1.1.4 1 BranchDescriptor3D   0.000   0.000   0.000
	       root.square.helix0_root.helix0_back_1_helix0_forw_${BP} 1.1.1.5 1 BranchDescriptor3D   0.000   0.000  29.020
	       root.square.helix1_root.helix1_forw_1_helix1_back_${BP} 1.1.2.4 1 BranchDescriptor3D   0.000   0.000   0.000
	       root.square.helix1_root.helix1_back_1_helix1_forw_${BP} 1.1.2.5 1 BranchDescriptor3D   0.000   0.000  29.020
	       root.square.helix2_root.helix2_forw_1_helix2_back_${BP} 1.1.2.4 1 BranchDescriptor3D   0.000   0.000   0.000
	       root.square.helix2_root.helix2_back_1_helix2_forw_${BP} 1.1.2.5 1 BranchDescriptor3D   0.000   0.000  29.020
	    */
	    /*
root.traced.traced_1_root.traced_1_forw_1_traced_1_back_6 1.2.1.4 1 BranchDescriptor3D  27.641  -7.000   0.000
root.traced.traced_1_root.traced_1_back_1_traced_1_forw_6 1.2.1.5 1 BranchDescriptor3D  19.000 -15.641  -0.000
root.traced.traced_2_root.traced_2_forw_1_traced_2_back_6 1.2.2.4 1 BranchDescriptor3D  27.641   7.000   0.000
root.traced.traced_2_root.traced_2_back_1_traced_2_forw_6 1.2.2.5 1 BranchDescriptor3D  19.000  15.641  -0.000
root.traced.traced_3_root.traced_3_forw_1_traced_3_back_6 1.2.3.4 1 BranchDescriptor3D  -7.000  27.641   0.000
root.traced.traced_3_root.traced_3_back_1_traced_3_forw_6 1.2.3.5 1 BranchDescriptor3D -15.641  19.000   0.000
root.traced.traced_4_root.traced_4_forw_1_traced_4_back_6 1.2.4.4 1 BranchDescriptor3D -27.641  -7.000   0.000
root.traced.traced_4_root.traced_4_back_1_traced_4_forw_6 1.2.4.5 1 BranchDescriptor3D -19.000 -15.641   0.000
	    */
	    app.runScriptLine("genjunctiondbconstraint jl1 root.traced.traced_1_root.traced_1_forw_1_traced_1_back_${BP} root.traced.traced_2_root.traced_2_back_1_traced_2_forw_${BP}");
	    app.runScriptLine("genjunctiondbconstraint jl2 root.traced.traced_2_root.traced_2_forw_1_traced_2_back_${BP} root.traced.traced_3_root.traced_3_back_1_traced_3_forw_${BP}");
	    app.runScriptLine("genjunctiondbconstraint jl3 root.traced.traced_3_root.traced_3_forw_1_traced_3_back_${BP} root.traced.traced_4_root.traced_4_back_1_traced_4_forw_${BP}");
	    app.runScriptLine("genjunctiondbconstraint jl4 root.traced.traced_4_root.traced_4_forw_1_traced_4_back_${BP} root.traced.traced_1_root.traced_1_back_1_traced_1_forw_${BP}");

	    /* app.runScriptLine("genjunctiondbconstraint jl1 root.square.helix0_root.helix0_forw_1_helix0_back_${BP} root.square.helix1_root.helix1_back_1_helix1_forw_${BP}");
	    app.runScriptLine("genjunctiondbconstraint jl2 root.square.helix1_root.helix1_forw_1_helix1_back_${BP} root.square.helix2_root.helix2_back_1_helix2_forw_${BP}");
	    app.runScriptLine("genjunctiondbconstraint jl3 root.square.helix2_root.helix2_forw_1_helix2_back_${BP} root.square.helix3_root.helix3_back_1_helix3_forw_${BP}");
	    app.runScriptLine("genjunctiondbconstraint jl4 root.square.helix3_root.helix3_forw_1_helix3_back_${BP} root.square.helix0_root.helix0_back_1_helix0_forw_${BP}");
	    */
	    app.runScriptLine("echo Links after genjunctionconstraint " + methodName);
	    app.runScriptLine("links");
	    app.runScriptLine("echo Still running " + methodName);
	    app.runScriptLine("optconstraints vdw=1 blocks=root.traced.traced_1_root;root.traced.traced_2_root;root.traced.traced_3_root;root.traced.traced_4_root steps=1000 error=0.1");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + ".script");
	} catch (CommandException ce) {
	    System.out.println("Exception thrown in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


    @Test(groups={"new"})
    public void testGenerateJunctionConstraintCommandSquare() {
	NanoTilerScripter app = new NanoTilerScripter();
	String methodName = "testGenerateJunctionConstraintCommandSquare";
	System.out.println(TestTools.generateMethodHeader(methodName));
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("set distMin 3.0");
	    app.runScriptLine("set distMax 9.0");
	    app.runScriptLine("set bp 10");
	    app.runScriptLine("synth simple square root 0 0 0");
	    app.runScriptLine("genhelix bp=${bp} root=root.square name=helix0");
	    app.runScriptLine("genhelix bp=${bp} root=root.square name=helix1");
	    app.runScriptLine("genhelix bp=${bp} root=root.square name=helix2");
	    app.runScriptLine("genhelix bp=${bp} root=root.square name=helix3");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("echo Links before genjunctionconstraint " + methodName);
	    app.runScriptLine("links");
	    app.runScriptLine("genjunctionconstraint ends=root.square.helix0_root.helix0_forw.1,root.square.helix0_root.helix0_back.${bp},root.square.helix1_root.helix1_back.1,root.square.helix1_root.helix1_forw.${bp} min=${distMin} max=${distMax}");
	    app.runScriptLine("genjunctionconstraint ends=root.square.helix1_root.helix1_forw.1,root.square.helix1_root.helix1_back.${bp},root.square.helix2_root.helix2_back.1,root.square.helix2_root.helix2_forw.${bp} min=${distMin} max=${distMax}");
	    app.runScriptLine("genjunctionconstraint ends=root.square.helix2_root.helix2_forw.1,root.square.helix2_root.helix2_back.${bp},root.square.helix3_root.helix3_back.1,root.square.helix3_root.helix3_forw.${bp} min=${distMin} max=${distMax}");
	    app.runScriptLine("genjunctionconstraint ends=root.square.helix3_root.helix3_forw.1,root.square.helix3_root.helix3_back.${bp},root.square.helix0_root.helix0_back.1,root.square.helix0_root.helix0_forw.${bp} min=${distMin} max=${distMax}");
	    app.runScriptLine("echo Links after genjunctionconstraint " + methodName);
	    app.runScriptLine("links");
	    app.runScriptLine("echo Still running " + methodName);
	    app.runScriptLine("optconstraints vdw=1 blocks=root.square.helix0_root;root.square.helix1_root;root.square.helix2_root;root.square.helix3_root");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + ".script");
	} catch (CommandException ce) {
	    System.out.println("Exception thrown in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }
    
}
