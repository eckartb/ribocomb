package nanotilertests.rnadesign.designapp;

import tools3d.objects3d.*;
import generaltools.TestTools;
import java.io.PrintStream;
import java.math.BigInteger;
import java.util.List;
import java.util.ArrayList;
import java.util.Properties;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import rnadesign.rnamodel.DBElementDescriptor;
import rnadesign.rnamodel.DBElementConnectionDescriptor;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandException;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import tools3d.Vector3D;
import rnadesign.designapp.*;

import org.testng.annotations.*; // for testing

public class GrowGraphCommandTest {

    public GrowGraphCommandTest() { }

    /** tests building a triangular structure out of one building block */
    @Test (groups={"new"})
    public void testGrowGraphCommand1(){
	String methodName = "testGrowGraphCommand1";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/triangle_il3.names ${NANOTILER_HOME}/test/fixtures 2");
	    app.runScriptLine("status");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/triangle.points scale=0.17 name=newgraph");
	    app.runScriptLine("tree");
	    app.runScriptLine("status");
	    Properties growProperties1 = app.runScriptLine("growgraph root=root.newgraph gen=2 block-max=1 conn-max=1 var=10 build=true ring-limit=25 name="
							   + methodName);
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of grow graph command: " + growProperties1);
 	    String exportname = methodName + ".pdb";
 	    app.runScriptLine("exportpdb " + exportname + " junction=true");
 	    app.runScriptLine("tree strands");
 	    Object3DSet junctions = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), "StrandJunction3D");
 	    System.out.println("Placed junctions: " + junctions.size());
 	    for (int i = 0; i < junctions.size(); ++i) {
 		System.out.println(junctions.get(i).getFullName() + " " + junctions.get(i).getProperties());
 	    }
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testGrowCommandAsGraph: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testGrowCommandAsGraph: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a triangular structure out of one building block */
    @Test (groups={"new"})
    public void testGrowGraphCommand1Square(){
	String methodName = "testGrowGraphCommand1Square";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/triangle_il3.names ${NANOTILER_HOME}/test/fixtures 2");
	    app.runScriptLine("status");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/square.points scale=0.17 name=newgraph");
	    app.runScriptLine("tree");
	    app.runScriptLine("status");
	    Properties growProperties1 = app.runScriptLine("growgraph root=root.newgraph gen=2 block-max=1 conn-max=1 var=10 build=true ring-limit=25 name="
							   + methodName);
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of grow graph command: " + growProperties1);
 	    String exportname = methodName + ".pdb";
 	    app.runScriptLine("exportpdb " + exportname + " junction=true");
 	    app.runScriptLine("tree strands");
 	    Object3DSet junctions = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), "StrandJunction3D");
 	    System.out.println("Placed junctions: " + junctions.size());
 	    for (int i = 0; i < junctions.size(); ++i) {
 		System.out.println(junctions.get(i).getFullName() + " " + junctions.get(i).getProperties());
 	    }
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testGrowCommandAsGraph: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testGrowCommandAsGraph: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test (groups={"new"})
    public void testGrowGraphCommandFourWaySquare(){
	String methodName = "testGrowGraphCommand1Square";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    // app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/triangle_il3.names ${NANOTILER_HOME}/test/fixtures 2");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/fourway.names ${NANOTILER_HOME}/test/fixtures 2");
	    app.runScriptLine("status");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/square.points scale=0.5 name=newgraph");
	    app.runScriptLine("tree");
	    app.runScriptLine("status");
	    Properties growProperties1 = app.runScriptLine("growgraph root=root.newgraph gen=2 block-max=1 conn-max=1 var=12 build=true ring-limit=35 name="
							   + methodName);
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of grow graph command: " + growProperties1);
 	    String exportname = methodName + ".pdb";
 	    app.runScriptLine("exportpdb " + exportname + " junction=true");
 	    app.runScriptLine("tree strands");
 	    Object3DSet junctions = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), 
								     "StrandJunction3D");
 	    System.out.println("Placed junctions: " + junctions.size());
 	    for (int i = 0; i < junctions.size(); ++i) {
 		System.out.println(junctions.get(i).getFullName() + " " + junctions.get(i).getProperties());
 	    }
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testGrowCommandAsGraph: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testGrowCommandAsGraph: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a triangular structure out of one building block allowing two different types of helix lengths */
    @Test (groups={"new"})
    public void testGrowGraphCommand2(){
	String methodName = "testGrowGraphCommand2";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/triangle_il3.names ${NANOTILER_HOME}/test/fixtures 2");
	    app.runScriptLine("status");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/triangle.points scale=0.17 name=newgraph");
	    app.runScriptLine("tree");
	    app.runScriptLine("status");
	    Properties growProperties1 = app.runScriptLine("growgraph root=root.newgraph gen=2 block-max=1 conn-max=2 var=5 build=true ring-limit=25 name="
							   + methodName);
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of grow graph command: " + growProperties1);
 	    String exportname = methodName + ".pdb";
 	    app.runScriptLine("exportpdb " + exportname + " junction=true");
 	    app.runScriptLine("tree strands");
 	    Object3DSet junctions = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), "StrandJunction3D");
 	    System.out.println("Placed junctions: " + junctions.size());
 	    for (int i = 0; i < junctions.size(); ++i) {
 		System.out.println(junctions.get(i).getFullName() + " " + junctions.get(i).getProperties());
 	    }
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testGrowCommandAsGraph: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testGrowCommandAsGraph: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a tetrahedron structure out of one building block */
    @Test (groups={"new", "slow"})
    public void testGrowGraphCommandTetrahedron(){
	String methodName = "testGrowGraphCommandTetrahedron";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/triangle.names ${NANOTILER_HOME}/test/fixtures 2");
	    app.runScriptLine("status");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/tetrahedron.points scale=0.17");
	    app.runScriptLine("tree");
	    Properties growProperties1 = app.runScriptLine("growgraph root=root.import gen=3 block-max=1 conn-max=2 var=6 name=" + methodName );
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of grow graph command: " + growProperties1);
	    String exportname = methodName + ".pdb";
	    app.runScriptLine("exportpdb " + exportname + " junction=true");
	    app.runScriptLine("tree strands");
	    Object3DSet junctions = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), "StrandJunction3D");
	    System.out.println("Placed junctions: " + junctions.size());
	    for (int i = 0; i < junctions.size(); ++i) {
		System.out.println(junctions.get(i).getFullName() + " " + junctions.get(i).getProperties());
	    }
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a tetrahedron structure out of one building block, NOT using any graph, should build anything */
    @Test (groups={"new", "slow"})
    public void testGrowGraphCommandTetrahedron2(){
	String methodName = "testGrowGraphCommandTetrahedron2";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/triangle.names ${NANOTILER_HOME}/test/fixtures 2");
	    app.runScriptLine("status");
	    // 	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/tetrahedron.points scale=0.17");
	    app.runScriptLine("tree");
	    Properties growProperties1 = app.runScriptLine("growgraph gen=3 block-max=1 conn-max=2 var=6 name=" + methodName );
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of grow graph command: " + growProperties1);
	    String exportname = methodName + ".pdb";
	    app.runScriptLine("exportpdb " + exportname + " junction=true");
	    app.runScriptLine("tree strands");
	    Object3DSet junctions = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), "StrandJunction3D");
	    System.out.println("Placed junctions: " + junctions.size());
	    for (int i = 0; i < junctions.size(); ++i) {
		System.out.println(junctions.get(i).getFullName() + " " + junctions.get(i).getProperties());
	    }
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


    /** tests building a tetrahedron structure out of one building block, NOT using any graph, should build anything */
    @Test (groups={"new", "slow"})
    public void testGrowGraphCommandStar(){
	String methodName = "testGrowGraphCommandStar";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/triangle.names ${NANOTILER_HOME}/test/fixtures 2");
	    app.runScriptLine("status");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/trianglestar.points scale=0.17 name=star");
	    app.runScriptLine("signature root.star");
	    app.runScriptLine("tree");
	    Properties growProperties1 = app.runScriptLine("growgraph root=root.star gen=3 block-max=1 conn-max=2 var=6 name=" + methodName );
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of grow graph command: " + growProperties1);
	    String exportname = methodName + ".pdb";
	    app.runScriptLine("exportpdb " + exportname + " junction=true");
	    app.runScriptLine("tree strands");
	    Object3DSet junctions = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), "StrandJunction3D");
	    System.out.println("Placed junctions: " + junctions.size());
	    for (int i = 0; i < junctions.size(); ++i) {
		System.out.println(junctions.get(i).getFullName() + " " + junctions.get(i).getProperties());
	    }
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a triangular structure. Not structure should be written, because 
     * the rings generated with 3-way junctions are stars, not simple rings */
    @Test (groups={"new", "slow"})
    public void testGrowGraphCommandJ3Triangle(){
	String methodName = "testGrowGraphCommandJ3Triangle";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/triangle.names ${NANOTILER_HOME}/test/fixtures 2");
	    app.runScriptLine("status");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/triangle.points scale=0.17 name=triangle");
	    app.runScriptLine("signature root.triangle");
	    app.runScriptLine("tree");
	    Properties growProperties1 = app.runScriptLine("growgraph root=root.triangle gen=3 block-max=1 conn-max=2 var=6 name=" + methodName );
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of grow graph command: " + growProperties1);
	    String exportname = methodName + ".pdb";
	    app.runScriptLine("exportpdb " + exportname + " junction=true");
	    app.runScriptLine("tree strands");
	    Object3DSet junctions = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), "StrandJunction3D");
	    System.out.println("Placed junctions: " + junctions.size());
	    for (int i = 0; i < junctions.size(); ++i) {
		System.out.println(junctions.get(i).getFullName() + " " + junctions.get(i).getProperties());
	    }
	}
	catch(CommandExecutionException e) {
	    System.out.println("Expected error in " + methodName + " : " + e.getMessage());
	    System.out.println(TestTools.generateMethodFooter(methodName));
	    return;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

}
