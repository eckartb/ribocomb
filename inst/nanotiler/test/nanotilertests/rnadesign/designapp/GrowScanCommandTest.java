package nanotilertests.rnadesign.designapp;

import tools3d.objects3d.*;
import generaltools.TestTools;
import java.io.File;
import java.io.PrintStream;
import java.util.List;
import java.util.ArrayList;
import java.util.Properties;
import graphtools.*;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import rnadesign.rnamodel.DBElementDescriptor;
import rnadesign.rnamodel.DBElementConnectionDescriptor;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandException;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import tools3d.Vector3D;
import rnadesign.designapp.*;
import org.testng.annotations.*; // for testing

public class GrowScanCommandTest {

     public GrowScanCommandTest() { }

    Object3DSet getJunctions(NanoTilerScripter app) {
	return Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), "StrandJunction3D");
    }

    Object3DSet getKissingLoops(NanoTilerScripter app) {
	return Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), "KissingLoop3D");
    }

    Object3DSet getJunctionsAndKissingLoops(NanoTilerScripter app) {
	Object3DSet result = getJunctions(app);
	result.merge(getKissingLoops(app));
	return result;
    }

    /** Prints all StrandJunction3D objects placed in controller */
    private void printJunctions(PrintStream ps, NanoTilerScripter app) {
	Object3DSet junctions = getJunctions(app);
	ps.println("Placed junctions: " + junctions.size());
	for (int i = 0; i < junctions.size(); ++i) {
	    ps.println(junctions.get(i).getFullName() + " " + junctions.get(i).getProperties());
	}
    }

    /** tests building a triangular structure out of one building block (using 3 non-equivalent copies, graph mode switched off ) */
    @Test (groups={"new"})
    public void testGrowScanCommand3WAll(){
	String methodName = "testGrowScanCommand3WAll";
	System.out.println(TestTools.generateMethodHeader(methodName));
	IntegerArrayIncreasingGenerator gen = new IntegerArrayIncreasingGenerator();
	gen.testIntegerArrayIncreasingGenerator2();
	System.out.println("Finished first part of test of IntegerArrayIncreasingGenerator");
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    app.runScriptLine("echo Loading junctions...");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/triangle.names ${NANOTILER_HOME}/test/fixtures 2");
	    app.runScriptLine("echo Status after oading junctions:");
	    app.runScriptLine("status");
	    // Properties growProperties1 = app.runScriptLine("growscan blocks=j,3,1;j,3,1;j,3,1 connect=1,2,(hxend)1,(hxend)2,6;2,3,(hxend)1,(hxend)2,6;3,1,(hxend)1,(hxend)2,6 gen=3 helices=true steric=true ring-export-limit=20 graph=false" + " ring-export=" + methodName + "_1");
	    // Properties growProperties1 = app.runScriptLine("growscan gen=3 helices=true steric=true ring-export-limit=20 bp=7 graph=false" + " ring-export=" + methodName + "_1");
	    Properties growProperties1 = app.runScriptLine("growscan");
	    // Properties growProperties1 = app.runScriptLine("growscan blocks=j,3,1;j,3,1;j,3,1 gen=3 helices=true steric=true ring-export-limit=20 graph=false" + " ring-export=" + methodName + "_1");
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of growscan command: " + growProperties1);
	    String exportname = methodName + ".pdb";
	    app.runScriptLine("exportpdb " + exportname + " junction=true");
	    // assert "1".equals(growProperties1.getProperty("ring_count"));
	    app.runScriptLine("tree strands");
	    printJunctions(System.out, app);
	    // assert getJunctions(app).size() == 3;
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testGrowScanCommandAsGraph: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testGrowScanCommandAsGraph: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }	    

    /** tests building a triangular structure out of one building block (using 3 non-equivalent copies, graph mode switched off ) */
    @Test (groups={"new", "slow"})
    public void testGrowScanCommand4Blocks(){
	String methodName = "testGrowScanCommand4Blocks";
	System.out.println(TestTools.generateMethodHeader(methodName));
	IntegerArrayIncreasingGenerator gen = new IntegerArrayIncreasingGenerator();
	gen.testIntegerArrayIncreasingGenerator2();
	System.out.println("Finished first part of test of IntegerArrayIncreasingGenerator");
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    app.runScriptLine("echo Loading junctions...");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/fourblocks.names ${NANOTILER_HOME}/test/fixtures 2");
	    app.runScriptLine("echo Status after oading junctions:");
	    app.runScriptLine("status");
	    // Properties growProperties1 = app.runScriptLine("growscan blocks=j,3,1;j,3,1;j,3,1 connect=1,2,(hxend)1,(hxend)2,6;2,3,(hxend)1,(hxend)2,6;3,1,(hxend)1,(hxend)2,6 gen=3 helices=true steric=true ring-export-limit=20 graph=false" + " ring-export=" + methodName + "_1");
	    // Properties growProperties1 = app.runScriptLine("growscan gen=3 helices=true steric=true ring-export-limit=20 bp=7 graph=false" + " ring-export=" + methodName + "_1");
	    Properties growProperties1 = app.runScriptLine("growscan");
	    // Properties growProperties1 = app.runScriptLine("growscan blocks=j,3,1;j,3,1;j,3,1 gen=3 helices=true steric=true ring-export-limit=20 graph=false" + " ring-export=" + methodName + "_1");
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of growscan command: " + growProperties1);
	    String exportname = methodName + ".pdb";
	    app.runScriptLine("exportpdb " + exportname + " junction=true");
	    // assert "1".equals(growProperties1.getProperty("ring_count"));
	    app.runScriptLine("tree strands");
	    printJunctions(System.out, app);
	    // assert getJunctions(app).size() == 3;
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testGrowScanCommandAsGraph: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testGrowScanCommandAsGraph: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }	    

    /** tests building a triangular structure out of one building block (using 3 non-equivalent copies, graph mode switched off ) */
    @Test (groups={"new"})
    public void testGrowScanCommand3Blocks(){
	String methodName = "testGrowScanCommand3Blocks";
	System.out.println(TestTools.generateMethodHeader(methodName));
	IntegerArrayIncreasingGenerator gen = new IntegerArrayIncreasingGenerator();
	gen.testIntegerArrayIncreasingGenerator2();
	System.out.println("Finished first part of test of IntegerArrayIncreasingGenerator");
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    app.runScriptLine("echo Loading junctions...");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/threeblocks.names ${NANOTILER_HOME}/test/fixtures 2");
	    app.runScriptLine("echo Status after oading junctions:");
	    app.runScriptLine("status");
	    // Properties growProperties1 = app.runScriptLine("growscan blocks=j,3,1;j,3,1;j,3,1 connect=1,2,(hxend)1,(hxend)2,6;2,3,(hxend)1,(hxend)2,6;3,1,(hxend)1,(hxend)2,6 gen=3 helices=true steric=true ring-export-limit=20 graph=false" + " ring-export=" + methodName + "_1");
	    // Properties growProperties1 = app.runScriptLine("growscan gen=3 helices=true steric=true ring-export-limit=20 bp=7 graph=false" + " ring-export=" + methodName + "_1");
	    Properties growProperties1 = app.runScriptLine("growscan");
	    // Properties growProperties1 = app.runScriptLine("growscan blocks=j,3,1;j,3,1;j,3,1 gen=3 helices=true steric=true ring-export-limit=20 graph=false" + " ring-export=" + methodName + "_1");
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of growscan command: " + growProperties1);
	    String exportname = methodName + ".pdb";
	    app.runScriptLine("exportpdb " + exportname + " junction=true");
	    // assert "1".equals(growProperties1.getProperty("ring_count"));
	    app.runScriptLine("tree strands");
	    printJunctions(System.out, app);
	    // assert getJunctions(app).size() == 3;
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testGrowScanCommandAsGraph: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testGrowScanCommandAsGraph: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }	    

    /** tests building a triangular structure out of one building block (using 3 non-equivalent copies, graph mode switched off ) */
    @Test (groups={"newest_later", "core"})
    public void testGrowScanCommand3WSimple(){
	String methodName = "testGrowScanCommand3WSimple";
	System.out.println(TestTools.generateMethodHeader(methodName));
	IntegerArrayIncreasingGenerator gen = new IntegerArrayIncreasingGenerator();
	gen.testIntegerArrayIncreasingGenerator2();
	System.out.println("Finished first part of test of IntegerArrayIncreasingGenerator");
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    app.runScriptLine("echo Loading junctions...");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/triangle.names ${NANOTILER_HOME}/test/fixtures 2");
	    app.runScriptLine("echo Status after oading junctions:");
	    app.runScriptLine("status");
	    // Properties growProperties1 = app.runScriptLine("growscan blocks=j,3,1;j,3,1;j,3,1 connect=1,2,(hxend)1,(hxend)2,6;2,3,(hxend)1,(hxend)2,6;3,1,(hxend)1,(hxend)2,6 gen=3 helices=true steric=true ring-export-limit=20 graph=false" + " ring-export=" + methodName + "_1");
	    // Properties growProperties1 = app.runScriptLine("growscan gen=3 helices=true steric=true ring-export-limit=20 bp=7 graph=false" + " ring-export=" + methodName + "_1");
	    Properties growProperties1 = app.runScriptLine("growscan");
	    // Properties growProperties1 = app.runScriptLine("growscan blocks=j,3,1;j,3,1;j,3,1 gen=3 helices=true steric=true ring-export-limit=20 graph=false" + " ring-export=" + methodName + "_1");
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of growscan command: " + growProperties1);
	    String exportname = methodName + ".pdb";
	    app.runScriptLine("exportpdb " + exportname + " junction=true");
	    // assert "1".equals(growProperties1.getProperty("ring_count"));
	    app.runScriptLine("tree strands");
	    printJunctions(System.out, app);
	    assert(getJunctions(app).size() > 0);
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testGrowScanCommandAsGraph: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testGrowScanCommandAsGraph: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }	    

    /** tests building a triangular structure out of one building block (using 3 non-equivalent copies, graph mode switched off ) */
    @Test (groups={"newest_later", "core"})
    public void testGrowScanCommandKLJ3(){
	String methodName = "testGrowScanCommandKLJ3";
	System.out.println(TestTools.generateMethodHeader(methodName));
	IntegerArrayIncreasingGenerator gen = new IntegerArrayIncreasingGenerator();
	gen.testIntegerArrayIncreasingGenerator2();
	System.out.println("Finished first part of test of IntegerArrayIncreasingGenerator");
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    app.runScriptLine("echo Loading junctions...");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/klj3.names ${NANOTILER_HOME}/test/fixtures 2");
	    app.runScriptLine("echo Status after loading junctions:");
	    app.runScriptLine("status");
	    // Properties growProperties1 = app.runScriptLine("growscan blocks=j,3,1;j,3,1;j,3,1 connect=1,2,(hxend)1,(hxend)2,6;2,3,(hxend)1,(hxend)2,6;3,1,(hxend)1,(hxend)2,6 gen=3 helices=true steric=true ring-export-limit=20 graph=false" + " ring-export=" + methodName + "_1");
	    // Properties growProperties1 = app.runScriptLine("growscan gen=3 helices=true steric=true ring-export-limit=20 bp=7 graph=false" + " ring-export=" + methodName + "_1");
	    Properties growProperties1 = app.runScriptLine("growscan");
	    // Properties growProperties1 = app.runScriptLine("growscan blocks=j,3,1;j,3,1;j,3,1 gen=3 helices=true steric=true ring-export-limit=20 graph=false" + " ring-export=" + methodName + "_1");
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of growscan command: " + growProperties1);
	    String exportname = methodName + ".pdb";
	    app.runScriptLine("exportpdb " + exportname + " junction=true");
	    // assert "1".equals(growProperties1.getProperty("ring_count"));
	    app.runScriptLine("tree strands");
	    printJunctions(System.out, app);
	    assert(getJunctions(app).size() > 0);
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testGrowScanCommandAsGraph: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testGrowScanCommandAsGraph: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }	    



}
