package nanotilertests.rnadesign.designapp;

import org.testng.*;
import org.testng.annotations.*;
import generaltools.TestTools;
import commandtools.*;
import rnadesign.designapp.*;

public class HelixConstraintCommandTest {

    @Test(groups={"new"})
    public void testHelixConstraintCommandTestSquare() {
	NanoTilerScripter app = new NanoTilerScripter();
	String methodName = "testHelixConstraintCommandTestSquare";
	System.out.println(TestTools.generateMethodHeader(methodName));
	try {
	    app.runScriptLine("set base " + methodName);
	    app.runScriptLine("set bp 2");
	    app.runScriptLine("set dist 86");
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("echo Test ${base} . Working with ${bp} base pairs at distance ${dist}...");
	    app.runScriptLine("synth cs c1 root 0 0 0");
	    app.runScriptLine("select root.c1");
	    app.runScriptLine("shift ${dist}  0 0");
	    app.runScriptLine("symadd root.c1");
	    app.runScriptLine("syminfo");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/j4_min.pdb findstems=true");
	    app.runScriptLine("secondary");
	    app.runScriptLine("genhelixconstraint B:1 C:22 bp=${bp} sym1=0 sym2=1");
	    app.runScriptLine("opthelices blocks=root.import rms=100 helices=true");
	    app.runScriptLine("clone root.import root clone_1");
	    app.runScriptLine("clone root.import root clone_2");
	    app.runScriptLine("symapply root.clone_1 1");
	    app.runScriptLine("symapply root.clone_2 1");
	    app.runScriptLine("symapply root.clone_2 1");
	    app.runScriptLine("exportpdb ${base}_${bp}_${dist}.pdb");

	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + ".script");
	} catch (CommandException ce) {
	    System.out.println("Exception thrown in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Moves Malachite green binding aptamer to center of cube. Same as testCubeFragmentPlacement. */
    @Test(groups={"new", "production"})
    public void testHelixExtensionBasic() {
	String methodName = "testHelixExtensionBasic";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("loadnucleotides /home/sharanr2/spvs_projects/nanotiler/develop/nanotiler/resources/nucleotidesDB.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/squarefused.pdb findstems=true"); // import cube
	    app.runScriptLine("tree strand");
	    app.runScriptLine("tree hxend");
      	app.runScriptLine("genhelix bp=10");
	    app.runScriptLine("genhelixconstraint root.import.import_cov_jnc.j1.A_1_B_21 root.helix_root.helix_back_1_helix_forw_10 bp=0");
	    app.runScriptLine("opthelices blocks=root.import,root.helix_root");
	    app.runScriptLine("exportpdb " + methodName + "_debug.pdb");
	}
	catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


    /** Moves Malachite green binding aptamer to center of cube. Same as testCubeFragmentPlacement. */
    @Test(groups={"new", "production"})
    public void testHelixExtensionConvenient() {
	String methodName = "testHelixExtensionConvenient";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("loadnucleotides /home/sharanr2/spvs_projects/nanotiler/develop/nanotiler/resources/nucleotidesDB.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/squarefused.pdb findstems=true"); // import cube
	    app.runScriptLine("tree strand");
	    app.runScriptLine("tree hxend");
	    app.runScriptLine("genhelix bp=10");
	    app.runScriptLine("genhelixconstraint A:1 root.helix_root.helix_back_1_helix_forw_10 bp=0"); 
	    app.runScriptLine("opthelices blocks=root.import,root.helix_root");
	    // genhelixconstraint root.import.import_cov_jnc.j1.A_1_B_21 root.helix_root.helix_back_1_helix_forw_10 bp=0 
	    app.runScriptLine("exportpdb " + methodName + "_debug.pdb");
	}
	catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }
    
}
