package nanotilertests.rnadesign.designapp;

import commandtools.CommandException;
import commandtools.CommandExecutionException;
import tools3d.objects3d.*;
import org.testng.annotations.*; // for testing
import rnadesign.rnacontrol.*;
import rnadesign.rnamodel.*;
import generaltools.TestTools;
import rnadesign.designapp.*;

/** Test class for import command */
public class ImportCommandTest {

    public ImportCommandTest() { }

    /** Reads imperfect, generates helix constraints corresponding to perfectly symmetric ring */
    // @Test (groups={"strange"}) // note: below structure is questionable, not clear if even legal PDB file
    public void testImportCommand3(){
	String methodName = "testImportCommand1";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("controller import-stem-length-min=3");
	    assert app.getGraphController().getImportStemLengthMin() == 3;
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1NKW.pdb_j2_0-A1043_0-C1132_chain_ring_1221_L5_rnaview.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/2GYA-nooverlap-nov20_edit_filt.pdb");
	    app.runScriptLine("links"); // show all found links like base pairs
	    app.runScriptLine("echo Found junctions:");
	    app.runScriptLine("tree allowed=StrandJunction3D");
	    // there must be at least one junction!
	    int numJunctions = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), 
						    "StrandJunction3D").size();
	    int seqNum1 = app.getGraphController().getSequences().getSequenceCount();
	    System.out.println("Number of sequences: " + seqNum1);
	    System.out.println("Number of junctions: " + numJunctions);
	    app.runScriptLine("tree strands");
	    app.runScriptLine("exportpdb " + methodName + ".pdb junction=false");
	    assert numJunctions > 0;
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	} catch(CommandException e) {
	    System.out.println("Command exception in import command test: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test (groups={"new"})
    public void testImportLargePdb1(){
	String methodName = "testImportLargePdb1";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1VOQ.pdb_j3_A-C826_A-C857_A-G869_chain_ring_kl2_2231_L22_dendrimer_ring_kl1_122131_L822.pdb format=pdb");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	} catch(CommandException e) {
	    System.out.println("Command exception in import command test: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test (groups={"new"})
    public void testImportLargePdb2(){
	String methodName = "testImportLargePdb2";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1VOQ.pdb_j3_A-C826_A-C857_A-G869_chain_ring_kl2_2231_L22_dendrimer_ring_kl1_122131_L822.pdb format=rnaview");
	    // should lead to exception because more than 36 strands cannot be handled by RNAview!
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    System.out.println("Error in " + methodName + " reading of more than 36 strands should lead to an exception with importing in RNAview mode, however no exception was thrown");
	    assert false;
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	} catch(CommandException e) {
	    System.out.println("EXPECTED Command exception in import command test: " + e.getMessage());
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test (groups={"new"})
    public void testImportLargePdb3(){
	String methodName = "testImportLargePdb3";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1S1Icleanfusedreidede25i25i225.pdb");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	} catch(CommandException e) {
	    System.out.println("Command exception in import command test: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Tests import of graph and placement of junctions. Demo for double-y shape! */
    @Test (groups={"new"})
    public void testImportAndPlace() {
	String methodName = "testImportAndPlace";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("source ${NANOTILER_HOME}/test/scripts/triangle_180_forloop_demo.script");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch (CommandException e) {
	    System.out.println("Command exception in import command test: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Tests import of graph with duplicate strand names. Checks for bug with single Phosphate as first residue of strand */
    @Test (groups={"new"})
    public void testImportDuplicateStrandNames() {
	String methodName = "testImportDuplicateStrandNames";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    // test for bug with single phosphote in first nucleotide in case of duplicate strand names:
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1NKW.pdb_j2_0-A1043_0-C1132_chain_ring_1221_L5.pdb");
	    app.runScriptLine("links"); // check if any base pairs were generated by launching RNAview
	    SequenceController sequences = app.getGraphController().getSequences();
	    for (int i = 0; i < sequences.getSequenceCount(); ++i) {
		NucleotideStrand strand = (NucleotideStrand)(sequences.getSequence(i));
		System.out.println("Checking imported strand " + strand.getFullName());
		for (int j = 0; j < strand.size(); ++j) {
		    if (strand.getChild(j).size() < 3) {
			System.out.println("Weird nucleotide encountered: " + strand.getChild(j).getFullName() + " " + strand.getChild(j));
			assert false;
		    }
		}
	    }
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + ".script");
	}
	catch (CommandException e) {
	    System.out.println("Command exception in import command test: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Difficult test case: test if RNAview is launched in case there is no RNAview info, tests if pdbfilter2 is launched in case of duplicate strand names */
    @Test (groups={"new"})
    public void testImportWithoutRnaview() {
	String methodName = "testImportWithoutRnaview";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    // Reads 3-ring with RNAview info:
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1NKW.pdb_j2_0-A1043_0-C1132_chain_ring_1221_L5_rnaview.pdb");
	    // app.runScriptLine("links"); // check if any base pairs were generated by launching RNAview
	    Object3D root1 = app.getGraphController().getGraph().getGraph();
	    Object3DSet junctions = Object3DTools.collectByClassName(root1, "StrandJunction3D"); // collect all junctions
	    app.runScriptLine("tree forbidden=Atom3D,Nucleotide3D");
	    assert junctions.size() == 3; // in this case, there have to be three junctions
	    app.runScriptLine("clear all"); // remove all content

	    // same structure, now without RNAview info!
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1NKW.pdb_j2_0-A1043_0-C1132_chain_ring_1221_L5.pdb");
	    // app.runScriptLine("links"); // check if any base pairs were generated by launching RNAview
	    Object3D root2 = app.getGraphController().getGraph().getGraph();
	    junctions = Object3DTools.collectByClassName(root2, "StrandJunction3D"); // collect all junctions
	    app.runScriptLine("tree forbidden=Atom3D,Nucleotide3D");
	    assert junctions.size() == 3; // in this case, there have to be three junctions

	    assert root1.getTotalNumberOfObjects() > 0;
	    assert root2.getTotalNumberOfObjects() > 0;
	    // assert root1.getTotalNumberOfObjects() == root2.getTotalNumberOfObjects(); // must be same result with or without RNAview !
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch (CommandException e) {
	    System.out.println("Command exception in import command test: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test (groups={"new"})
    public void testImportPointsFile(){
	String methodName = "testImportPointsFile";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    // Reads 3-ring with RNAview info:
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/triangle.points");
	    app.runScriptLine("tree");
	    Object3D graph = app.getGraphController().getGraph().findByFullName("root.import");
	    assert graph != null;
	    assert graph.size() == 3; // we loaded a triangle, should have 3 sides
	    LinkSet links = app.getGraphController().getLinks();
	    assert links.size() == 3;
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch (CommandException e) {
	    System.out.println("Command exception in import command test: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test (groups={"new"})
    public void testImportPointsFileAndPlace(){
	String methodName = "testImportPointsFileAndPlace";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    // Reads 3-ring with RNAview info:
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/triangle_il.names ${NANOTILER_HOME}/test/fixtures");
	    app.runScriptLine("status");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/triangle.points addstems=true stemrms=15.0 junctionrms=13.2 junctionangle=18.0 stemangle=30.0 rerun=5 scale=0.25");
	    app.runScriptLine("tree forbidden=Atom3D allowed=RnaStrand");
	    app.runScriptLine("exportpdb testImportPointsFileAndPlace.pdb");
	    Object3D graph = app.getGraphController().getGraph().findByFullName("root.import");
	    assert graph != null;
	    assert graph.size() == 3; // we loaded a triangle, should have 3 sides
	    LinkSet links = app.getGraphController().getLinks();
	    assert links.size() == 3;
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch (CommandException e) {
	    System.out.println("Command exception in import command test: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Reads a single base pair containing a chemically modified A */
    @Test (groups={"current_later"})
    public void testImportNonStandardBasePair(){
	String methodName = "testImportNonStandardBasePair";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    // app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("controller import-stem-length-min=3");
	    assert app.getGraphController().getImportStemLengthMin() == 3;
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1NKW.pdb_j2_0-A1043_0-C1132_chain_ring_1221_L5_rnaview.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/NA1_U_edit2.pdb");
	    app.runScriptLine("links"); // show all found links like base pairs
	    app.runScriptLine("tree");
	    app.runScriptLine("secondary");
	    // there must be at least one junction!
	    int numStrands = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), 
						    "RnaStrand").size();
	    int numNucs = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), 
						    "Nucleotide3D").size();
	    int seqNum1 = app.getGraphController().getSequences().getSequenceCount();
	    System.out.println("Number of sequences: " + seqNum1);
	    System.out.println("Number of nucleotides: " + numNucs);
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    assert seqNum1 == 2;
	    assert numNucs == 2;
	    assert numStrands == 2;
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	} catch(CommandException e) {
	    System.out.println("Command exception in import command test: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Reads a single base pair containing a chemically modified A */
    @Test (groups={"current_later"})
    public void testImportTHWBasePair(){
	String methodName = "testImportTHWBasePair";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    // app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("controller import-stem-length-min=3");
	    assert app.getGraphController().getImportStemLengthMin() == 3;
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1NKW.pdb_j2_0-A1043_0-C1132_chain_ring_1221_L5_rnaview.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/4NLF_tWH.pdb");
	    app.runScriptLine("links"); // show all found links like base pairs
	    app.runScriptLine("tree");
	    app.runScriptLine("secondary");
	    // there must be at least one junction!
	    int numStrands = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), 
						    "RnaStrand").size();
	    int numNucs = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), 
						    "Nucleotide3D").size();
	    int seqNum1 = app.getGraphController().getSequences().getSequenceCount();
	    System.out.println("Number of sequences: " + seqNum1);
	    System.out.println("Number of nucleotides: " + numNucs);
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	    assert seqNum1 == 1;
	    assert numNucs == 2;
	    assert numStrands == 1;
	} catch(CommandException e) {
	    System.out.println("Command exception in import command test: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Reads a single base pair containing a chemically modified A */
    @Test (groups={"y2015", "tight", "August"})
    public void testImport4WayJunction(){
	String methodName = "testImport4WayJunction";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    // app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("controller import-stem-length-min=3");
	    assert app.getGraphController().getImportStemLengthMin() == 3;
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1NKW.pdb_j2_0-A1043_0-C1132_chain_ring_1221_L5_rnaview.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/j4_min.pdb");
	    app.runScriptLine("links"); // show all found links like base pairs
	    app.runScriptLine("tree");
	    app.runScriptLine("secondary");
	    // there must be at least one junction!
	    int numStrands = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), 
						    "RnaStrand").size();
	    int numNucs = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), 
						    "Nucleotide3D").size();
	    int seqNum1 = app.getGraphController().getSequences().getSequenceCount();
	    LinkSet links = app.getGraphController().getLinks();
	    System.out.println("Number of sequences: " + seqNum1);
	    System.out.println("Number of nucleotides: " + numNucs);
	    System.out.println("Number of links: " + links.size());
	    System.out.println("Interactions:");
	    System.out.println(BasePairTools.toString(links));
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	    assert seqNum1 == 4;
	    // assert numNucs == 2;
	    assert numStrands == 4;
	    assert links.size() == 2821; // this inclused covalent bonds, but also base pair interactions
	} catch(CommandException e) {
	    System.out.println("Command exception in import command test: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test (groups={"new"})
    public void testImportCubeFileAndPlace(){
	String methodName = "testImportCubeFileAndPlace";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    // Reads 3-ring with RNAview info:
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/cubecorner.names ${NANOTILER_HOME}/test/fixtures");
	    app.runScriptLine("status");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/cube.points addstems=true stemrms=15.0 junctionrms=13.2 junctionangle=18.0 stemangle=30.0 rerun=1 scale=1.1");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("exportpdb testImportCubeFileAndPlace.pdb");
	    Object3D graph = app.getGraphController().getGraph().findByFullName("root.import");
	    assert graph != null;
	    assert graph.size() == 8; // we loaded a cube, should have 8 points
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch (CommandException e) {
	    System.out.println("Command exception in import command test: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }



    /** Reads imperfect, generates helix constraints corresponding to perfectly symmetric ring */
    @Test (groups={"new"}) // note: below structure is questionable, not clear if even legal PDB file
    public void testImportCommandCube(){
	String methodName = "testImportCommandCube";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("controller import-stem-length-min=3");
	    assert app.getGraphController().getImportStemLengthMin() == 3;
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1NKW.pdb_j2_0-A1043_0-C1132_chain_ring_1221_L5_rnaview.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/optimizedcube_step12_fused2_rnaview.pdb findstems=false");
	    app.runScriptLine("links"); // show all found links like base pairs
	    //	    app.runScriptLine("echo Found junctions:");
	    // app.runScriptLine("tree allowed=StrandJunction3D");
	    // there must be at least one junction!
	    int numJunctions = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), 
						    "StrandJunction3D").size();
	    int seqNum = app.getGraphController().getSequences().getSequenceCount();
	    System.out.println("Number of sequences: " + seqNum);
	    int bpNum = app.getGraphController().getLinks().getHydrogenBondInteractions().size();
	    System.out.println("Number of base pairs: " + bpNum);
	    assert seqNum == 6;
	    assert bpNum == 120;
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	} catch(CommandException e) {
	    System.out.println("Command exception in import command test: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

	@Test (groups={"new"})
	public void testImportCommandSquare(){
		String methodName = "testImportCommandSquare";
		System.out.println(TestTools.generateMethodHeader(methodName));
 		NanoTilerScripter app = new NanoTilerScripter();
		try{
	    	app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
			app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/squarefused.pdb findstems=false");
	   	 	app.runScriptLine("tree strand");
			System.out.println("Helix Ends: ");
	    	app.runScriptLine("tree hxend");
		}catch(CommandException e) {
	    	System.out.println("Command exception in import command test: " + e.getMessage());
	    	assert false;
		}
		System.out.println(TestTools.generateMethodFooter(methodName));
	}

	@Test (groups={"new"})
	public void testImportCommandSquareStemsTrue(){
		String methodName = "testImportCommandSquareStemsTrue";
		System.out.println(TestTools.generateMethodHeader(methodName));
 		NanoTilerScripter app = new NanoTilerScripter();
		try{
	    	app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
			app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/squarefused.pdb findstems=true");
	   	 	app.runScriptLine("tree strand");
			System.out.println("Helix Ends: ");
	    	app.runScriptLine("tree hxend");
		}catch(CommandException e) {
	    	System.out.println("Command exception in import command test: " + e.getMessage());
	    	assert false;
		}
		System.out.println(TestTools.generateMethodFooter(methodName));
	}
}
