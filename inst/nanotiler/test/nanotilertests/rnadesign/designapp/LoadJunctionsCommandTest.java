package nanotilertests.rnadesign.designapp;

import commandtools.CommandException;
import commandtools.CommandExecutionException;
import tools3d.objects3d.*;
import org.testng.annotations.*; // for testing
import rnadesign.rnacontrol.*;
import rnadesign.rnamodel.*;
import generaltools.TestTools;
import rnadesign.designapp.*;

/** Test class for import command */
public class LoadJunctionsCommandTest {

    public LoadJunctionsCommandTest() { }

    /** Reads kissing loop structure */
    @Test (groups={"new"})
    public void testLoadJunctionsCommand1(){
	String methodName = "testLoadJunctionsCommand1";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/kissingloops.names ${NANOTILER_HOME}/test/fixtures 2");
	    app.runScriptLine("status");
	    // make sure no 4-way junction is defined:
	    assert app.getGraphController().getJunctionController().getJunctionDB().size(4) == 0;
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	} catch(CommandException e) {
	    System.out.println("Command exception in loadjunctions command test: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Reads right-angle motif structure - it is neither a kissing loop nor a junction */
    @Test (groups={"newest_later"})
    public void testLoadJunctionsCommandGeneralMotifs(){
	String methodName = "testLoadJunctionsCommandGeneralMotifs";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/ramotif.names ${NANOTILER_HOME}/test/fixtures 2 mode=g");
	    app.runScriptLine("status");
	    // make sure no 4-way junction is defined:
	    assert app.getGraphController().getJunctionController().getMotifDB().size(3) == 1;
	    app.runScriptLine("grow blocks=m,3,1 connect=1,1,1,2,5 gen=3");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	} catch(CommandException e) {
	    System.out.println("Command exception in loadjunctions command test: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Reads double-y-shape structure - it is neither a kissing loop nor a junction */
    @Test (groups={"current_later"})
    public void testLoadJunctionsCommandGeneralMotifsDoubleY(){
	String methodName = "testLoadJunctionsCommandGeneralMotifsDoubleY";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("loadjunctions yshape_double_180_stretch1.325_1.325_r75d.pdb ${NANOTILER_HOME}/test/fixtures 2 mode=g");
	    app.runScriptLine("status");
	    // make sure no 4-way junction is defined:
	    assert app.getGraphController().getJunctionController().getMotifDB().size(3) == 1;
	    app.runScriptLine("grow blocks=m,4,1 connect=1,1,1,3,5 gen=3");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	} catch(CommandException e) {
	    System.out.println("Command exception in loadjunctions command test: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


    /** Reads double-y-shape structure - it is neither a kissing loop nor a junction */
    @Test (groups={"new", "y2015"})
    public void testLoadJunctionsCommandGeneralMotifsTriangle(){
	String methodName = "testLoadJunctionsCommandGeneralMotifsTriangle";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("loadjunctions 2J00_j3_A-G1064_A-C1107_A-G1187_triangle_rnaview.pdb ${NANOTILER_HOME}/test/fixtures 2 mode=g");
	    app.runScriptLine("status");
	    // make sure no 4-way junction is defined:
	    assert app.getGraphController().getJunctionController().getMotifDB().size(3) == 1;
	    app.runScriptLine("grow blocks=m,3,1 connect=1,1,1,3,5 gen=3");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	} catch(CommandException e) {
	    System.out.println("Command exception in loadjunctions command test: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


}
