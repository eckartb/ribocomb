package nanotilertests.rnadesign.designapp;

import rnadesign.rnamodel.Atom3D;
import tools3d.objects3d.SimpleLink;
import commandtools.*;
import generaltools.*;
import org.testng.*;
import rnadesign.designapp.*;
import org.testng.annotations.*;

/** Tests "missinglink" command */
public class MissingLinkCommandTest {

    /** reads a database of junctions (containing one three-way junction) and places that junction */
    @Test (groups={"new"})
    public void testMisingLinkCommand(){
	final String methodName = "testMissinglinkCommand";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    String namesfileName = "${NANOTILER_HOME}/test/fixtures/triangle.names";
	    app.runScriptLine("loadjunctions " + namesfileName + " ${NANOTILER_HOME}/test/fixtures");
	    app.runScriptLine("status");
	    app.runScriptLine("place j root j1 3 1 0 0 0 v_1");
	    // root.j1.A_1-9v_1.C2.P
	    String atomName1 = "root.j1.A_1-9v_1.C2.P";
	    String atomName2 = "root.j1.A_1-9v_1.C2.O1P";

	    app.runScriptLine("links");
	    assert app.getGraphController().getLinks().size() == 0;
	    app.runScriptLine("tree name=root.j1.1.2 allowed=Atom3D,Nucleotide3D");
	    app.runScriptLine("tree name=root.j1.A_1-9v_1.C2 allowed=Atom3D,Nucleotide3D");
	    app.runScriptLine("history file=missinglink.script");
	    //	    assert app.getGraphController().getGraph().findByFullName(atomName1).getFullName().equals(atomName1);
	    //	    assert app.getGraphController().getGraph().findByFullName(atomName2).getFullName().equals(atomName2);
	    //	    Atom3D atom1 = (Atom3D)app.getGraphController().getGraph().findByFullName(atomName1);
	    // Atom3D atom2 = (Atom3D)app.getGraphController().getGraph().findByFullName(atomName2);
	    // assert !app.getGraphController().getLinks().contains(new SimpleLink(atom1,atom2));
	    app.runScriptLine("missinglink");
	    app.runScriptLine("links");
	    assert app.getGraphController().getLinks().size() > 0;
	    //assert app.getGraphController().getLinks().size() == 711;
	    // app.runScriptLine("rmlink 1");
	    app.runScriptLine("missinglink");
	    // assert app.getGraphController().getLinks().contains(new SimpleLink(atom1,atom2));
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }
}
