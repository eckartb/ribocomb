package nanotilertests.rnadesign.designapp;

import java.util.Properties;
import generaltools.TestTools;
import commandtools.CommandException;
import rnadesign.designapp.*;

import org.testng.annotations.*;

public class MutateCommandTest {

    @Test(groups={"new", "slow"})
    public void testMutateLeader() {
	String methodName = "testMutateLeader";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("source ${NANOTILER_HOME}/resources/defaults.script");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/testExtendAtStart.pdb findstems=false");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("status");
 	    String name1 = "root.import.A.1";
 	    String name2 = "root.import.B.1";
 	    String name3 = "root.import.C.1";
 	    String name4 = "root.import.D.1";
 	    String name5 = "root.import.E.1";
 	    String name6 = "root.import.F.1";
	    app.runScriptLine("echo Mutating leader sequences...");
	    //  Example: mutate A:C5,G15,A2 B:U1,G2,A15

	    app.runScriptLine("mutate G:A6,A5,A4,G3,G2,G1");
	    app.runScriptLine("mutate H:A6,A5,A4,G3,G2,G1");
	    app.runScriptLine("mutate I:A6,A5,A4,G3,G2,G1");
	    app.runScriptLine("mutate J:A6,A5,A4,G3,G2,G1");
	    app.runScriptLine("mutate K:A6,A5,A4,G3,G2,G1");
	    app.runScriptLine("mutate L:A6,A5,A4,G3,G2,G1");

	    app.runScriptLine("tree strand");
	    app.runScriptLine("exportpdb " + methodName + "_mutated.pdb");
	    app.runScriptLine("echo Fusing strands...");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("secondary");
	    app.runScriptLine("fusestrands root.import.G root.import.A");
	    app.runScriptLine("fusestrands root.import.H root.import.B");
	    app.runScriptLine("fusestrands root.import.I root.import.C");
	    app.runScriptLine("fusestrands root.import.J root.import.D");
	    app.runScriptLine("fusestrands root.import.K root.import.E");
	    app.runScriptLine("fusestrands root.import.L root.import.F");
	    app.runScriptLine("secondary");
	    app.runScriptLine("echo Fused strands:");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("exportpdb " + methodName + "_fused.pdb");
	    
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Using list of start sequencecs according:
     * Here is the list of the starting sequences for the cube strands, which
     * will give us similar yields of transcription reactions (Milligan et al, NAR, 1987):
     * GGGAAA
     * GGGAGA
     * GGGAUC
     * GGCAAC
     * GGCGCU
     * GGACAU
     * GGCUCG
     */
    @Test(groups={"new", "slow"})
    public void testMutateUnevenLeader() {
	String methodName = "testMutateUnevenLeader";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("source ${NANOTILER_HOME}/resources/defaults.script");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/testExtendAtStart.pdb findstems=false");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("status");
 	    String name1 = "root.import.A.1";
 	    String name2 = "root.import.B.1";
 	    String name3 = "root.import.C.1";
 	    String name4 = "root.import.D.1";
 	    String name5 = "root.import.E.1";
 	    String name6 = "root.import.F.1";
	    app.runScriptLine("echo Mutating leader sequences...");
	    //  Example: mutate A:C5,G15,A2 B:U1,G2,A15

	    app.runScriptLine("mutate G:A6,A5,A4,G3,G2,G1");
	    app.runScriptLine("mutate H:A6,G5,A4,G3,G2,G1");
	    app.runScriptLine("mutate I:C6,U5,A4,G3,G2,G1");
	    app.runScriptLine("mutate J:C6,A5,A4,C3,G2,G1");
	    app.runScriptLine("mutate K:U6,C5,G4,C3,G2,G1");
	    app.runScriptLine("mutate L:U6,A5,C4,A3,G2,G1");

	    app.runScriptLine("tree strand");
	    app.runScriptLine("exportpdb " + methodName + "_mutated.pdb");
	    app.runScriptLine("echo Fusing strands...");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("secondary");
	    app.runScriptLine("fusestrands root.import.G root.import.A");
	    app.runScriptLine("fusestrands root.import.H root.import.B");
	    app.runScriptLine("fusestrands root.import.I root.import.C");
	    app.runScriptLine("fusestrands root.import.J root.import.D");
	    app.runScriptLine("fusestrands root.import.K root.import.E");
	    app.runScriptLine("fusestrands root.import.L root.import.F");
	    app.runScriptLine("secondary");
	    app.runScriptLine("echo Fused strands:");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("exportpdb " + methodName + "_fused.pdb");
	    
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new"})
    public void testMutateCorners() {
	String methodName = "testMutateCorners";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("source ${NANOTILER_HOME}/resources/defaults.script");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/testMutateLeader_fused.pdb findstems=false");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("status");
	    app.runScriptLine("secondary");
	    app.runScriptLine("echo Mutating corner loops to U...");
	    //  Example: mutate A:C5,G15,A2 B:U1,G2,A15
	    app.runScriptLine("mutate A:U12,U23,U34,U45");
	    app.runScriptLine("mutate B:U12,U23,U34,U45");
	    app.runScriptLine("mutate C:U12,U23,U34,U45");
	    app.runScriptLine("mutate D:U12,U23,U34,U45");
	    app.runScriptLine("mutate E:U12,U23,U34,U45");
	    app.runScriptLine("mutate F:U12,U23,U34,U45");
	    app.runScriptLine("exportpdb " + methodName + "_mutated.pdb");
	    app.runScriptLine("secondary");
	    
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new", "slow"}, dependsOnMethods = { "testMutateUnevenLeader" })
    public void testMutateUnevenCorners() {
	String methodName = "testMutateUnevenCorners";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("source ${NANOTILER_HOME}/resources/defaults.script");
	    app.runScriptLine("import ${NANOTILER_HOME}/src/testMutateUnevenLeader_fused.pdb findstems=false");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("status");
	    app.runScriptLine("secondary");
	    app.runScriptLine("echo Mutating corner loops to U...");
	    //  Example: mutate A:C5,G15,A2 B:U1,G2,A15
	    app.runScriptLine("mutate A:U12,U23,U34,U45");
	    app.runScriptLine("mutate B:U12,U23,U34,U45");
	    app.runScriptLine("mutate C:U12,U23,U34,U45");
	    app.runScriptLine("mutate D:U12,U23,U34,U45");
	    app.runScriptLine("mutate E:U12,U23,U34,U45");
	    app.runScriptLine("mutate F:U12,U23,U34,U45");
	    app.runScriptLine("exportpdb " + methodName + "_mutated.pdb");
	    app.runScriptLine("secondary");
	    
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }
    
    /** Mutates leader sequences to gggaaa. Uses 3D model with new extensions that point outwards.
     * dependsOn Uses result of ExtendCommandTest.testExtendAtStart2
     */
    @Test(groups={"new", "slow"})
    public void testMutateLeader2() {
	String methodName = "testMutateLeader2";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("source ${NANOTILER_HOME}/resources/defaults.script");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/testExtendAtStart2_mutated_ABCDEF.pdb findstems=false");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("status");
 	    String name1 = "root.import.A.1";
 	    String name2 = "root.import.B.1";
 	    String name3 = "root.import.C.1";
 	    String name4 = "root.import.D.1";
 	    String name5 = "root.import.E.1";
 	    String name6 = "root.import.F.1";
	    app.runScriptLine("secondary");
	    app.runScriptLine("echo Mutating leader sequences to GGGAAA ...");
	    app.runScriptLine("mutate A:G8,G7,A6,A5,A4,G3,G2,G1 C:C43,C42");
	    app.runScriptLine("mutate B:C8,G7,A6,A5,A4,G3,G2,G1 C:C32,G31");
	    app.runScriptLine("mutate C:G8,C7,A6,A5,A4,G3,G2,G1 F:G32,C31");
	    app.runScriptLine("mutate D:C8,C7,A6,A5,A4,G3,G2,G1 C:G21,G20");
	    app.runScriptLine("mutate E:G8,C7,A6,A5,A4,G3,G2,G1 B:G32,C31");
	    app.runScriptLine("mutate F:C8,G7,A6,A5,A4,G3,G2,G1 E:C32,G31");
	    app.runScriptLine("echo Mutating corners to \"U\"...:");
	    app.runScriptLine("mutate A:U14,U25,U36,U47");
	    app.runScriptLine("mutate B:U14,U25,U36,U47");
	    app.runScriptLine("mutate C:U14,U25,U36,U47");
	    app.runScriptLine("mutate D:U14,U25,U36,U47");
	    app.runScriptLine("mutate E:U14,U25,U36,U47");
	    app.runScriptLine("mutate F:U14,U25,U36,U47");
	    app.runScriptLine("echo Mutating sequence ends to be GC rich...");
	    app.runScriptLine("mutate A:C49,C50 C:G44,G45");
	    app.runScriptLine("mutate B:G49,C50 C:G33,C34");
	    app.runScriptLine("mutate C:C49,G50 F:C33,G34");
	    app.runScriptLine("mutate D:G49,G50 C:C22,C23");
	    app.runScriptLine("mutate E:G49,G50 B:C33,C34");
	    app.runScriptLine("mutate F:C49,C50 E:G33,G34");
	    app.runScriptLine("secondary");
	    app.runScriptLine("exportpdb " + methodName + "_mutated.pdb");

	    // fusing not necessary anymore:

	    // 	    app.runScriptLine("echo Fusing strands...");
	    // 	    app.runScriptLine("tree strand");
	    // 	    app.runScriptLine("secondary");
	    // 	    app.runScriptLine("fusestrands root.import.G root.import.A");
	    // 	    app.runScriptLine("fusestrands root.import.H root.import.B");
	    // 	    app.runScriptLine("fusestrands root.import.I root.import.C");
	    // 	    app.runScriptLine("fusestrands root.import.J root.import.D");
	    // 	    app.runScriptLine("fusestrands root.import.K root.import.E");
	    // 	    app.runScriptLine("fusestrands root.import.L root.import.F");
	    // 	    app.runScriptLine("secondary");
	    // 	    app.runScriptLine("echo Fused strands:");
	    // 	    app.runScriptLine("tree strand");
	    // 	    app.runScriptLine("exportpdb " + methodName + "_fused.pdb");
	    
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


    /** Mutates leader sequences to gggaaa. Uses 3D model with new extensions that point outwards.
     * dependsOn Uses result of ExtendCommandTest.testExtendAtStart3
     */
    @Test(groups={"new", "production", "slow"})
    public void testMutateAllSmallCube() {
	String methodName = "testMutateAllSmallCube";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("source ${NANOTILER_HOME}/resources/defaults.script");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/testExtendAtStart3_mutated.pdb findstems=false");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("status");
	    app.runScriptLine("secondary");
	    app.runScriptLine("echo Mutating leader sequences to GGGAAA as well as all other residues...");
	    app.runScriptLine("mutate    A:GGGAAACGACACUCUAAUCGUGGUACAAUCUCAUUGCACUGUCCUUAGGC "
         			      + "B:GGGAAAGUAUAAUCUGGAUAGUAUCACGCAGGUCUCCACGAUUAGUUUGG "
			              + "C:GGGAAACCGAGCUCCUAGCCCUCUUUAUACCCAAUGUGUCGGCCUUGCGU "
			              + "D:GGGAAAGCUAGGUCGUCUGCAUGUGAUAUAGCUGUUACUAUCCAGUGAGG "
			              + "E:GGGAAAUGCGUGUCAGCUAUAUCUGGAGCGCCCGUAUGAGAUUGUUGACC "
			              + "F:GGGAAACGCUCCUCAUGCAGACGUGCUCGGACGCUAGGACAGUGCUCGGG");
	    app.runScriptLine("secondary");
	    app.runScriptLine("exportpdb " + methodName + "_mutated.pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Mutates leader sequences to gggaaa. Uses 3D model with new extensions that point outwards.
     * dependsOn Uses result of MonteCarloSequenceOptimizerTest.testOptimizeCubeMG_BF
     * gggagauCCUCCUCUCUuuGGCACAUGCGuuUAUGAGAUGCuuCUGGC
     * ggcgcuuGACGCUGUAGuuccuguGAGCAuuAGAGAGGAGGuuAGUCAGGcagguaacgaaugg
     * ggcaacucccuaGCCGGuugcgccUGACUuucucccGCCAGuuGGUUA
     * gggaucuUGGACUAUGGuuUUAGUAGCUGuuCUACAGCGUCuuCCGGC
     * ggacauuCAGCUACUAAuugccuuACACGuuCGCAUGUGCCuuUGCUC
     * gggaaauCCAUAGUCCAuuuugccUAACCuuGCAUCUCAUAuuCGUGUCGcccgacug
     * After mutations, the secondary structure was:
     * ~STRAND A
     * SEQUENCE  = gggagaccuccucucuuggcacaugcguuaugagaugcucuggc
     * STRUCTURE = BBBBB.AAAAAAAAAA.DDDDDDDDDD.EEEEEEEEEE.CCCCC
     * ~STRAND B
     * SEQUENCE  = ggcgcugacgcuguaguccugugagcauagagaggagguagucaggcagguaacgaaugg
     * STRUCTURE = FFFFF.HHHHHHHHHH.IIIIIJJJJJ.AAAAAAAAAA.GGGGG..KKK....L....MM
     * ~STRAND C
     * SEQUENCE  = ggcaaccccuagccggugcgccugacuucucccgccagugguua
     * STRUCTURE = PPPPP.NNNNNOOOOO.FFFFFGGGGG.BBBBBCCCCC.QQQQQ
     * ~STRAND D
     * SEQUENCE  = gggaucuggacuaugguuuaguagcugucuacagcgucuccggc
     * STRUCTURE = NNNNN.SSSSSSSSSS.RRRRRRRRRR.HHHHHHHHHH.OOOOO
     * ~STRAND E
     * SEQUENCE  = ggacaucagcuacuaaugccuuacacgucgcaugugccuugcuc
     * STRUCTURE = IIIII.RRRRRRRRRR.TTTTTUUUUU.DDDDDDDDDD.JJJJJ
     * ~STRAND F
     * SEQUENCE  = gggaaaccauaguccauuugccuaaccugcaucucauaucgugucgcccgacug
     * STRUCTURE = TTTTT.SSSSSSSSSS.PPPPPQQQQQ.EEEEEEEEEE.UUUUU..MM.L.KKK
     */
    @Test(groups={"new", "production", "slow"})
    public void testMutateAllSmallMG_BF_Cube() {
	String methodName = "testMutateAllSmallMG_BG_Cube";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("source ${NANOTILER_HOME}/resources/defaults.script");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/testCubeFragmentPlacementLoop5_27_bridged_fused_rnaview.pdb findstems=false");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("status");
	    app.runScriptLine("secondary");
	    app.runScriptLine("echo Mutating leader sequences as well as all other residues. Mutated real sequences have one more U.");
	    app.runScriptLine("mutate    "
			      + "A:gggagaCCUCCUCUCUuGGCACAUGCGuUAUGAGAUGCuCUGGC ".toUpperCase()
			      //   gggagaccuccucucuuggcacaugcguuaugagaugcucuggc
			      + "B:ggcgcuGACGCUGUAGuccuguGAGCAuAGAGAGGAGGuAGUCAGGcagguaacgaaugg ".toUpperCase()
			      //   ggcgcugacgcuguaguccugugagcauagagaggagguagucaggcagguaacgaaugg
			      + "C:ggcaaccccuaGCCGGugcgccUGACUucucccGCCAGuGGUUA ".toUpperCase()
			      //   ggcaaccccuagccggugcgccugacuucucccgccagugguua
			      + "D:gggaucUGGACUAUGGuUUAGUAGCUGuCUACAGCGUCuCCGGC ".toUpperCase()
			      //   gggaucuggacuaugguuuaguagcugucuacagcgucuccggc
			      + "E:ggacauCAGCUACUAAugccuuACACGuCGCAUGUGCCuUGCUC ".toUpperCase()
			      //   ggacaucagcuacuaaugccuuacacgucgcaugugccuugcuc
			      + "F:gggaaaCCAUAGUCCAuuugccUAACCuGCAUCUCAUAuCGUGUCGcccgacug".toUpperCase());
	                      //   gggaaaccauaguccauuugccuaaccugcaucucauaucgugucgcccgacug
	    app.runScriptLine("secondary");
	    app.runScriptLine("exportpdb " + methodName + "_mutated.pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Mutates leader sequences to gggaaa. Uses 3D model with new extensions that point outwards.
     * Using the following sequences (loops contain only one U instead of two in the sequences) 
     >> A
     > gggaaaGUGUCACAuuGCGGUGUGCUCGCGUCAGACUuuCCGAGAUUACCGGACUGAAAUuuCGACCAUCUGAUUGCUAGGACuuGUCGCUGCAAAGG
     > .....CCCCCCCC..EEEEEEEEEEEEEEEEEEEEE..BBBBBBBBBBBBBBBBBBBBB..AAAAAAAAAAAAAAAAAAAAA..DDDDDDDDDDDDD
     >> B
     > gggaaaGGACGAUGuuGUCCUAGCAAUCAGAUGGUCGuuGCCUAUCAGGUUGUUCUGGCCuuCACAAUGAAUUACGUCUCCUCuuCAUGCCAUGAGUC
     > .....IIIIIIII..AAAAAAAAAAAAAAAAAAAAA..FFFFFFFFGGGGGGGGGGGGG..HHHHHHHHHHHHHHHHHHHHH..JJJJJJJJJJJJJ
     >> C
     > gggaaaUGAUAGGCuuAUUUCAGUCCGGUAAUCUCGGuuAUGUAGGCCGUUCCUUGAGUAuuCUCGCAAGAGAUACCGAUUUCuuGGCCAGAACAACC
     > .....FFFFFFFF..BBBBBBBBBBBBBBBBBBBBB..LLLLLLLLMMMMMMMMMMMMM..KKKKKKKKKKKKKKKKKKKKK..GGGGGGGGGGGGG
     >> D
     > gggaaaCCUGUGAUuuGAGGAGACGUAAUUCAUUGUGuuGAAAUCGGUAUCUCUUGCGAGuuUAGAUGAUCUACCGAAAGAGCuuAGUGCGUCGGUCG
     > .....NNNNNNNN..HHHHHHHHHHHHHHHHHHHHH..KKKKKKKKKKKKKKKKKKKKK..PPPPPPPPPPPPPPPPPPPPP..OOOOOOOOOOOOO
     >> E
     > gggaaaCGUGUAUAuuUGUGACACCCUUUGCAGCGACuuCAUCGUCCGACUCAUGGCAUGuuAUCACAGGCGACCGACGCACUuuACGCAUGGAACCG
     > .....QQQQQQQQ..CCCCCCCCDDDDDDDDDDDDD..IIIIIIIIJJJJJJJJJJJJJ..NNNNNNNNOOOOOOOOOOOOO..RRRRRRRRRRRRR
     >> F
     > gggaaaGCCUACAUuuAGUCUGACGCGAGCACACCGCuuUAUACACGCGGUUCCAUGCGUuuGCUCUUUCGGUAGAUCAUCUAuuUACUCAAGGAACG
     > .....LLLLLLLL..EEEEEEEEEEEEEEEEEEEEE..QQQQQQQQRRRRRRRRRRRRR..PPPPPPPPPPPPPPPPPPPPP..MMMMMMMMMMMMM
     >
     */
    @Test(groups={"new", "production", "slow"})
    public void testMutateAllLargeCube() {
	String methodName = "testMutateAllLargeCube";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("source ${NANOTILER_HOME}/resources/defaults.script");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/testExtendAtStartLargeCube_rnaview.pdb findstems=false");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("status");
	    app.runScriptLine("secondary");
	    app.runScriptLine("echo Mutating leader sequences to GGGAAA as well as all other residues...");
	    app.runScriptLine("mutate    A:GGGAAAGUGUCACAUGCGGUGUGCUCGCGUCAGACUUCCGAGAUUACCGGACUGAAAUUCGACCAUCUGAUUGCUAGGACUGUCGCUGCAAAGG "
         			      + "B:GGGAAAGGACGAUGUGUCCUAGCAAUCAGAUGGUCGUGCCUAUCAGGUUGUUCUGGCCUCACAAUGAAUUACGUCUCCUCUCAUGCCAUGAGUC "
			              + "C:GGGAAAUGAUAGGCUAUUUCAGUCCGGUAAUCUCGGUAUGUAGGCCGUUCCUUGAGUAUCUCGCAAGAGAUACCGAUUUCUGGCCAGAACAACC "
			              + "D:GGGAAACCUGUGAUUGAGGAGACGUAAUUCAUUGUGUGAAAUCGGUAUCUCUUGCGAGUUAGAUGAUCUACCGAAAGAGCUAGUGCGUCGGUCG "
			              + "E:GGGAAACGUGUAUAUUGUGACACCCUUUGCAGCGACUCAUCGUCCGACUCAUGGCAUGUAUCACAGGCGACCGACGCACUUACGCAUGGAACCG "
			              + "F:GGGAAAGCCUACAUUAGUCUGACGCGAGCACACCGCUUAUACACGCGGUUCCAUGCGUUGCUCUUUCGGUAGAUCAUCUAUUACUCAAGGAACG");
	    app.runScriptLine("secondary");
	    app.runScriptLine("exportpdb " + methodName + "_mutated.pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Mutates leader sequences to gggaaa. Uses 3D model with new extensions that point outwards.
     * Derived from the following sequences (loops contain only one U instead of two in the sequences) 
     >A ......DDDDDD..AAAAAAAAAA..BBBBBBBBBB..CCCCCCCCCC..EEEE
     gggaaaGCUACGuuAUCACAUGGAuuGCCAUAGACCuuCUCAUACGACuuCAGC
     >B ......FFFFFF..JJJJJJJJJJ..IIIIIIIIII..HHHHHHHHHH..GGGG
     gggaaaGCGAGGuuUACUCCGGCAuuAGAGGAUAGCuuGAUGGUGCGGuuAGGC
     >C ......AAAAAAAAAA..KKKKKKKKKK
     gggaaaUCCAUGUGAUuuGCAAGGAACC
     >D ......FFFFFFGGGG..LLLLLLLLLL
     gggaaaCCUCGCGCCUuuCCUACCGAAG
     >E ......BBBBBBBBBB..LLLLLLLLLL
     gggaaaGGUCUAUGGCuuCUUCGGUAGG
     >F ......HHHHHHHHHH..MMMMMMMMMM
     gggaaaCCGCACCAUCuuGGCCGUGCUC
     >G ......CCCCCCCCCC..MMMMMMMMMM
     gggaaaGUCGUAUGAGuuGAGCACGGCC
     >H ......IIIIIIIIII..NNNNNNNNNN
     gggaaaGCUAUCCUCUuuACCAAACUGC
     >I ......DDDDDDEEEE..NNNNNNNNNN
     gggaaaCGUAGCGCUGuuGCAGUUUGGU
     >J ......JJJJJJJJJJ..KKKKKKKKKK
     gggaaaUGCCGGAGUAuuGGUUCCUUGC
     */
    @Test(groups={"new", "production", "slow"})
    public void testMutateAllCubeS10() {
	String methodName = "testMutateAllCubeS10";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("source ${NANOTILER_HOME}/resources/defaults.script");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/cube_s10_rnaview.pdb findstems=false");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("status");
	    app.runScriptLine("secondary");
	    app.runScriptLine("echo Mutating leader sequences to GGGAAA as well as all other residues...");
	    app.runScriptLine(
		         "mutate A:GGGAAAGCUACGUAUCACAUGGAUGCCAUAGACCUCUCAUACGACUCAGC "
			      + "B:GGGAAAGCGAGGUUACUCCGGCAUAGAGGAUAGCUGAUGGUGCGGUAGGC "
			      + "C:GGGAAAUUCCAUGUGAUUGCAAGGAACC "
			      + "D:GGGAAAUCCUCGCGCCUUCCUACCGAAG "
			      + "E:GGGAAAUGGUCUAUGGCUCUUCGGUAGG "
			      + "F:GGGAAAUCCGCACCAUCUGGCCGUGCUC "
			      + "G:GGGAAAUGUCGUAUGAGUGAGCACGGCC "
			      + "H:GGGAAAUGCUAUCCUCUUACCAAACUGC "
			      + "I:GGGAAAUCGUAGCGCUGUGCAGUUUGGU "
			      + "J:GGGAAAUUGCCGGAGUAUGGUUCCUUGC");
	    app.runScriptLine("secondary");
	    app.runScriptLine("exportpdb " + methodName + "_mutated.pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

}
