package nanotilertests.rnadesign.designapp;

import commandtools.CommandException;
import commandtools.CommandExecutionException;
import generaltools.Optimizer;
import generaltools.PropertyTools;
import generaltools.TestTools;
import java.util.Properties;
import rnadesign.designapp.*;

import org.testng.annotations.*; // for testing

/** Test class for import command */
public class OptimizeBasepairsCommandTest {

    public OptimizeBasepairsCommandTest() { }

    /** reads one side of triangle; clones triangle three times; set base pair constraints; optimized structure wrt constraints. Finds 0.3A solution! */
    @Test (groups={"new", "slow"})
    public void testOptimizeBasepairs(){
	String methodName = "testOptimizeBasepairs";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	Properties properties = null;
	try{
	    //	    app.runScriptLine("loadbasepairs ${NANOTILER_HOME}/resources/157D_rnaview_edit.pdb");
	    app.runScriptLine("loadbasepairs ${NANOTILER_HOME}/resources/augc2bp_edit_rnaview.pdb"); // 2J00_A_rnaview.pdb");
	    app.runScriptLine("status");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/sequence-with-no-overlap-in-cofold.pdb");
	    app.runScriptLine("clone root.import root import_b");
	    app.runScriptLine("clone root.import root import_c");
	    app.runScriptLine("select root.import_b");
	    app.runScriptLine("shift 40 0 0");
	    app.runScriptLine("select root.import_c");
	    app.runScriptLine("shift 0 40 0");
	    app.runScriptLine("tree residues");
	    app.runScriptLine("genbpconstraint root.import.A.11 root.import_b.A.47 3");
	    // app.runScriptLine("genbpconstraint root.import.A.12 root.import_b.A.46 3");
	    app.runScriptLine("genbpconstraint root.import_b.A.11 root.import_c.A.47 3");
	    // app.runScriptLine("genbpconstraint root.import_b.A.12 root.import_c.A.46 3");
	    app.runScriptLine("genbpconstraint root.import_c.A.11 root.import.A.47 3");
	    // app.runScriptLine("genbpconstraint root.import_c.A.12 root.import.A.46 3");
	    properties = app.runScriptLine("optconstraints blocks=root.import;root.import_b;root.import_c steps=500000");
	    assert properties != null;
	    app.runScriptLine("exportpdb testOptimizeBasepairsCommand.pdb");
	    String propString = properties.getProperty(Optimizer.FINAL_SCORE);
	    if (propString == null) {
		System.out.println("Could not find property string: " + "OPTIMIZER.FINAL_SCORE");
		PropertyTools.printProperties(System.out, properties);
		assert false;
	    }
	    double score = Double.parseDouble(properties.getProperty(Optimizer.FINAL_SCORE));
	    assert score <= 5; // should find very good solution!
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandException e) {
	    System.out.println("Command exception in import command test: " + e.getMessage());
	    assert false;
	}
	catch (NumberFormatException e) {
	    System.out.println("Improperly defined property " + Optimizer.FINAL_SCORE + " in result properties of optimization: " + properties);
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }



    /** reads one side of triangle; clones triangle three times; set base pair constraints; optimized structure wrt constraints. Finds 0.3A solution! */
    @Test (groups={"new", "slow"})
    public void testOptimizeBasepairs2(){
	String methodName = "testOptimizeBasepairs2";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	Properties properties = null;
	try{
	    // app.runScriptLine("loadbasepairs ${NANOTILER_HOME}/resources/157D_rnaview_edit.pdb");
	    app.runScriptLine("loadbasepairs ${NANOTILER_HOME}/resources/augc2bp_edit_rnaview.pdb"); // 2J00_A_rnaview.pdb");
	    app.runScriptLine("status");
	    // app.runScriptLine("loadbasepairs ${NANOTILER_HOME}/resources/2J00_A_rnaview.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/sequence-with-no-overlap-in-cofold.pdb");
	    app.runScriptLine("clone root.import root import_b");
	    app.runScriptLine("clone root.import root import_c");
	    app.runScriptLine("select root.import_b");
	    app.runScriptLine("shift 40 0 0");
	    app.runScriptLine("select root.import_c");
	    app.runScriptLine("shift 0 40 0");
	    app.runScriptLine("tree residues");
	    app.runScriptLine("genbpconstraint root.import.A.11 root.import_b.A.47 2");
	    // app.runScriptLine("genbpconstraint root.import.A.12 root.import_b.A.46 3");
	    app.runScriptLine("genbpconstraint root.import_b.A.11 root.import_c.A.47 2");
	    // app.runScriptLine("genbpconstraint root.import_b.A.12 root.import_c.A.46 3");
	    app.runScriptLine("genbpconstraint root.import_c.A.11 root.import.A.47 2");
	    // app.runScriptLine("genbpconstraint root.import_c.A.12 root.import.A.46 3");
	    properties = app.runScriptLine("optconstraints blocks=root.import;root.import_b;root.import_c steps=5000000");
	    app.runScriptLine("exportpdb testOptimizeBasepairsCommand.pdb");
	    assert properties != null;
	    app.runScriptLine("tree strand");
	    double score = Double.parseDouble(properties.getProperty(Optimizer.FINAL_SCORE));
	    assert score <= 15; // should find very good solution!
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + ": " 
			       + e.getMessage());
	    assert false;
	}
	catch (NumberFormatException e) {
	    System.out.println("Improperly defined property " + Optimizer.FINAL_SCORE + " in result properties of optimization: " + properties);
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


    /** reads one side of triangle; clones triangle three times; set base pair constraints; optimized structure wrt constraints. Finds 0.3A solution! */
    @Test (groups={"new", "slow"})
    public void testOptimizeBasepairs2Short(){
	String methodName = "testOptimizeBasepairs2Short";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	Properties properties = null;
	try{
	    // app.runScriptLine("loadbasepairs ${NANOTILER_HOME}/resources/157D_rnaview_edit.pdb");
	    app.runScriptLine("loadbasepairs ${NANOTILER_HOME}/resources/augc2bp_edit_rnaview.pdb"); // 2J00_A_rnaview.pdb");
	    app.runScriptLine("status");
	    // app.runScriptLine("loadbasepairs ${NANOTILER_HOME}/resources/2J00_A_rnaview.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/sequence-with-no-overlap-in-cofold.pdb");
	    app.runScriptLine("clone root.import root import_b");
	    app.runScriptLine("clone root.import root import_c");
	    app.runScriptLine("select root.import_b");
	    app.runScriptLine("shift 100 0 0");
	    app.runScriptLine("select root.import_c");
	    app.runScriptLine("shift 0 100 0");
	    app.runScriptLine("tree residues");
	    app.runScriptLine("genbpconstraint root.import.A.11 root.import_b.A.47 2");
	    // app.runScriptLine("genbpconstraint root.import.A.12 root.import_b.A.46 3");
	    app.runScriptLine("genbpconstraint root.import_b.A.11 root.import_c.A.47 2");
	    // app.runScriptLine("genbpconstraint root.import_b.A.12 root.import_c.A.46 3");
	    app.runScriptLine("genbpconstraint root.import_c.A.11 root.import.A.47 2");
	    // app.runScriptLine("genbpconstraint root.import_c.A.12 root.import.A.46 3");
	    properties = app.runScriptLine("optconstraints blocks=root.import;root.import_b;root.import_c steps=50000");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    assert properties != null;
	    app.runScriptLine("tree strand");
	    double score = Double.parseDouble(properties.getProperty(Optimizer.FINAL_SCORE));
	    assert score <= 15; // should find very good solution!
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + ": " 
			       + e.getMessage());
	    assert false;
	}
	catch (NumberFormatException e) {
	    System.out.println("Improperly defined property " + Optimizer.FINAL_SCORE + " in result properties of optimization: " + properties);
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }



}
