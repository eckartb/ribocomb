package nanotilertests.rnadesign.designapp;

import generaltools.TestTools;
import generaltools.Optimizer;
import java.io.PrintStream;
import java.util.List;
import java.util.ArrayList;
import java.util.Properties;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import rnadesign.rnamodel.DBElementDescriptor;
import rnadesign.rnamodel.DBElementConnectionDescriptor;
import rnadesign.rnamodel.HelixOptimizerTools;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandException;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import tools3d.Vector3D;
import rnadesign.designapp.*;
import org.testng.annotations.*; // for testing

public class OptimizeHelicesCommandTest {

    /** tests building a triangular structure out of one building block */
    @Test (groups={"newest_later", "slow"})
    public void testOptimizeHelicesCommandSimple(){
	final String methodName = "testOptimizeHelicesCommandSimple";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    String jncFileName = "${NANOTILER_HOME}/test/fixtures/2J00.rnaview.pdb_j3_A-G1064_A-C1107_A-G1187_rnaview.pdb";
	    String namesfileName = "${NANOTILER_HOME}/test/fixtures/triangle.names";
	    app.runScriptLine("loadjunctions " + namesfileName + " ${NANOTILER_HOME}/test/fixtures");
	    app.runScriptLine("status");
	    app.runScriptLine("place j root j1 3 1 0 0 0 _1");
	    app.runScriptLine("place j root j2 3 1 100 0 0 _2");
	    // 	    app.runScriptLine("import " + jncFileName + " name=j1");
	    // 	    app.runScriptLine("clone " + jncFileNa + " name=j1");
	    // 	    app.runScriptLine("clone " + jncFileName + " name=j1");
	    // 	    // app.runScriptLine("import " + jncFileName + " name=j2");
	    // 	    // app.runScriptLine("import " + jncFileName + " name=j3");
	    app.runScriptLine("tree forbidden=Atom3D");
	    System.out.println("Number of generated child nodes of root after placing 2 junctions: " +app.getGraphController().getGraph().getGraph().size()); // must be multiple of 3 children
	    assert (app.getGraphController().getGraph().getGraph().size() == 2); // must be 3 children
	    System.out.println("Setting first constraint...");
	    String hx1 = "(hxend)1";
	    String hx2 = "(hxend)2";
	    int hxlen = 6;
	    String otherOptions = " bp=" + hxlen;
	    app.runScriptLine("genhelixconstraint root.j1." + hx1 + " root.j2." + hx2 + otherOptions);
	    app.runScriptLine("links");
	    // app.runScriptLine("genhelixconstraint root.j2.(hxend)1 root.j3.(hxend)2 bpmin=6 bpmax=6");
	    // app.runScriptLine("genhelixconstraint root.j2_cov_jnc.j1.A_1_3_B_1_8 root.j3_cov_jnc.j1.B_2_3_C_2_9 bpmin=6 bpmax=6");
	    // System.out.println("Setting third constraint...");
	    // app.runScriptLine("genhelixconstraint root.j3." + hx1 + " root.j1." + hx2 + otherOptions);
	    // app.runScriptLine("genhelixconstraint root.j3.(hxend)1 root.j1.(hxend)2 bpmin=6 bpmax=6");
	    // app.runScriptLine("genhelixconstraint root.j3_cov_jnc.j1.A_2_3_B_2_8 root.j1_cov_jnc.j1.B_3_C_9 1.3.1.6 bpmin=6 bpmax=6");
	    app.runScriptLine("opthelices helices=false steps=100000 kt=10 firstfixed=false parent=root.j1,root.j2"); // not necessary: parent=root");
	    String outFileName = "testOptimizeHelicesCommand_tmp.pdb";
	    app.runScriptLine("exportpdb " + outFileName + " junction=true");
	    // app.runScriptLine("tree forbidden=Atom3D");
	    // assert Double.parseDouble(optProperties.getProperty(Optimizer.FINAL_SCORE)) < 20.0; // must be pretty good ring-closure
	    // app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a triangular structure out of one building block */
    @Test (groups={"newest_later", "slow"})
    public void testOptimizeHelicesCommand(){
	final String methodName = "testOptimizeHelicesCommand";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    String jncFileName = "${NANOTILER_HOME}/test/fixtures/2J00.rnaview.pdb_j3_A-G1064_A-C1107_A-G1187_rnaview.pdb";
	    String namesfileName = "${NANOTILER_HOME}/test/fixtures/triangle.names";
	    app.runScriptLine("loadjunctions " + namesfileName + " ${NANOTILER_HOME}/test/fixtures");
	    app.runScriptLine("status");
	    app.runScriptLine("place j root j1 3 1 0 0 0 _1");
	    app.runScriptLine("place j root j2 3 1 100 0 0 _2");
	    app.runScriptLine("place j root j3 3 1 0 100 0 _3");
	    // 	    app.runScriptLine("import " + jncFileName + " name=j1");
	    // 	    app.runScriptLine("clone " + jncFileNa + " name=j1");
	    // 	    app.runScriptLine("clone " + jncFileName + " name=j1");
	    // 	    // app.runScriptLine("import " + jncFileName + " name=j2");
	    // 	    // app.runScriptLine("import " + jncFileName + " name=j3");
	    app.runScriptLine("tree forbidden=Atom3D");
	    System.out.println("Number of generated child nodes of root after importing 3 junctions: " +app.getGraphController().getGraph().getGraph().size()); // must be multiple of 3 children
	    assert (app.getGraphController().getGraph().getGraph().size() == 3); // must be 3 children
	    System.out.println("Setting first constraint...");
	    String hx1 = "(hxend)1";
	    String hx2 = "(hxend)2";
	    int hxlen = 6;
	    String otherOptions = " bp=" + hxlen;
	    app.runScriptLine("genhelixconstraint root.j1." + hx1 + " root.j2." + hx2 + otherOptions);
	    System.out.println("Setting second constraint...");
	    app.runScriptLine("genhelixconstraint root.j2." + hx1 + " root.j3." + hx2 + otherOptions);
	    app.runScriptLine("links");
	    // app.runScriptLine("genhelixconstraint root.j2.(hxend)1 root.j3.(hxend)2 bpmin=6 bpmax=6");
	    // app.runScriptLine("genhelixconstraint root.j2_cov_jnc.j1.A_1_3_B_1_8 root.j3_cov_jnc.j1.B_2_3_C_2_9 bpmin=6 bpmax=6");
	    // System.out.println("Setting third constraint...");
	    // app.runScriptLine("genhelixconstraint root.j3." + hx1 + " root.j1." + hx2 + otherOptions);
	    // app.runScriptLine("genhelixconstraint root.j3.(hxend)1 root.j1.(hxend)2 bpmin=6 bpmax=6");
	    // app.runScriptLine("genhelixconstraint root.j3_cov_jnc.j1.A_2_3_B_2_8 root.j1_cov_jnc.j1.B_3_C_9 1.3.1.6 bpmin=6 bpmax=6");
	    System.out.println("Optimizing helices before closing circle (two-stages: later a ring closing constraint is set and the structure is again optimized");
	    // app.runScriptLine("opthelices helices=false steps=100000 kt=10 movable=root.j1,root.j2,root.j3 firstfixed=false"); // not necessary: parent=root");
	    app.runScriptLine("opthelices helices=false steps=10 kt=10 parent=root.j1,root.j2,root.j3 firstfixed=false"); // not necessary: parent=root");
	    String outFileName = "testOptimizeHelicesCommand_tmp.pdb";
	    app.runScriptLine("exportpdb " + outFileName + " junction=true");
	    String outFileName2 = "testOptimizeHelicesCommand.pdb";
	    System.out.println("Setting third constraint...");
	    app.runScriptLine("genhelixconstraint root.j3." + hx1 + " root.j1." + hx2 + otherOptions);
	    System.out.println("Optimizing helices");
	    Properties optProperties = app.runScriptLine("opthelices helices=true steps=1000000 kt=50 error=8 parent=root.j1,root.j2,root.j3 helixint=1000");
	    app.runScriptLine("exportpdb " + outFileName2 + " junction=true helices=true");
	    // app.runScriptLine("tree forbidden=Atom3D");
	    assert Double.parseDouble(optProperties.getProperty(Optimizer.FINAL_SCORE)) < 30.0; // must be pretty good ring-closure
	    // app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a triangular structure out of one building block */
    @Test (groups={"newest_later", "slow"})
    public void testOptimizeHelicesCommandWithLengths(){
	final String methodName = "testOptimizeHelicesCommandWithLengths";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    String jncFileName = "${NANOTILER_HOME}/test/fixtures/2J00.rnaview.pdb_j3_A-G1064_A-C1107_A-G1187_rnaview.pdb";
	    String namesfileName = "${NANOTILER_HOME}/test/fixtures/triangle.names";
	    app.runScriptLine("loadjunctions " + namesfileName + " ${NANOTILER_HOME}/test/fixtures");
	    app.runScriptLine("status");
	    app.runScriptLine("place j root j1 3 1 0 0 0 _1");
	    app.runScriptLine("place j root j2 3 1 100 0 0 _2");
	    app.runScriptLine("place j root j3 3 1 0 100 0 _3");
	    // 	    app.runScriptLine("import " + jncFileName + " name=j1");
	    // 	    app.runScriptLine("clone " + jncFileNa + " name=j1");
	    // 	    app.runScriptLine("clone " + jncFileName + " name=j1");
	    // 	    // app.runScriptLine("import " + jncFileName + " name=j2");
	    // 	    // app.runScriptLine("import " + jncFileName + " name=j3");
	    app.runScriptLine("tree forbidden=Atom3D");
	    System.out.println("Number of generated child nodes of root after importing 3 junctions: " +app.getGraphController().getGraph().getGraph().size()); // must be multiple of 3 children
	    assert (app.getGraphController().getGraph().getGraph().size() == 3); // must be 3 children
	    System.out.println("Setting first constraint...");
	    String hx1 = "(hxend)1";
	    String hx2 = "(hxend)2";
	    int hxlen = 5;
	    String otherOptions = " bp=" + hxlen;
	    app.runScriptLine("genhelixconstraint root.j1." + hx1 + " root.j2." + hx2 + otherOptions);
	    System.out.println("Setting second constraint...");
	    app.runScriptLine("genhelixconstraint root.j2." + hx1 + " root.j3." + hx2 + otherOptions);
	    app.runScriptLine("links");
	    // app.runScriptLine("genhelixconstraint root.j2.(hxend)1 root.j3.(hxend)2 bpmin=6 bpmax=6");
	    // app.runScriptLine("genhelixconstraint root.j2_cov_jnc.j1.A_1_3_B_1_8 root.j3_cov_jnc.j1.B_2_3_C_2_9 bpmin=6 bpmax=6");
	    // System.out.println("Setting third constraint...");
	    // app.runScriptLine("genhelixconstraint root.j3." + hx1 + " root.j1." + hx2 + otherOptions);
	    // app.runScriptLine("genhelixconstraint root.j3.(hxend)1 root.j1.(hxend)2 bpmin=6 bpmax=6");
	    // app.runScriptLine("genhelixconstraint root.j3_cov_jnc.j1.A_2_3_B_2_8 root.j1_cov_jnc.j1.B_3_C_9 1.3.1.6 bpmin=6 bpmax=6");
	    System.out.println("Optimizing helices before closing circle (two-stages: later a ring closing constraint is set and the structure is again optimized");
	    // app.runScriptLine("opthelices helices=false steps=100000 kt=10 movable=root.j1,root.j2,root.j3 firstfixed=false"); // not necessary: parent=root");
	    app.runScriptLine("opthelices helices=false steps=10 kt=10 parent=root.j1,root.j2,root.j3 firstfixed=false"); // not necessary: parent=root");
	    String outFileName = "testOptimizeHelicesCommand_tmp.pdb";
	    app.runScriptLine("exportpdb " + outFileName + " junction=true");
	    String outFileName2 = "testOptimizeHelicesCommand.pdb";
	    System.out.println("Setting third constraint...");
	    app.runScriptLine("genhelixconstraint root.j3." + hx1 + " root.j1." + hx2 + otherOptions);
	    System.out.println("Optimizing helices");
	    Properties optProperties = app.runScriptLine("opthelices helices=true steps=10000000 kt=100 error=8 parent=root.j1,root.j2,root.j3 bpinterval=50000");
	    app.runScriptLine("exportpdb " + outFileName2 + " junction=true helices=true");
	    // app.runScriptLine("tree forbidden=Atom3D");
	    assert Double.parseDouble(optProperties.getProperty(Optimizer.FINAL_SCORE)) < 40.0; // must be pretty good ring-closure
	    // app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


    /** tests building a triangular structure out of one building block; fuse generated helices with junctions */
    @Test (groups={"new", "slow"})
    public void testOptimizeHelicesCommandAppend(){
	final String methodName = "testOptimizeHelicesCommandAppend";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    String jncFileName = "${NANOTILER_HOME}/test/fixtures/2J00.rnaview.pdb_j3_A-G1064_A-C1107_A-G1187_rnaview.pdb";
	    String namesfileName = "${NANOTILER_HOME}/test/fixtures/triangle.names";
	    app.runScriptLine("loadjunctions " + namesfileName + " ${NANOTILER_HOME}/test/fixtures");
	    app.runScriptLine("status");
	    app.runScriptLine("place j root j1 3 1 0 0 0 _1");
	    app.runScriptLine("place j root j2 3 1 100 0 0 _2");
	    app.runScriptLine("place j root j3 3 1 0 100 0 _3");
	    // 	    app.runScriptLine("import " + jncFileName + " name=j1");
	    // 	    app.runScriptLine("clone " + jncFileNa + " name=j1");
	    // 	    app.runScriptLine("clone " + jncFileName + " name=j1");
	    // 	    // app.runScriptLine("import " + jncFileName + " name=j2");
	    // 	    // app.runScriptLine("import " + jncFileName + " name=j3");
	    app.runScriptLine("tree forbidden=Atom3D");
	    System.out.println("Number of generated child nodes of root after importing 3 junctions: " +app.getGraphController().getGraph().getGraph().size()); // must be multiple of 3 children
	    assert (app.getGraphController().getGraph().getGraph().size() == 3); // must be 3 children
	    System.out.println("Setting first constraint...");
	    String hx1 = "(hxend)1";
	    String hx2 = "(hxend)2";
	    int hxlen = 6;
	    String otherOptions = " bp=" + hxlen;
	    app.runScriptLine("genhelixconstraint root.j1." + hx1 + " root.j2." + hx2 + otherOptions);
	    System.out.println("Setting second constraint...");
	    app.runScriptLine("genhelixconstraint root.j2." + hx1 + " root.j3." + hx2 + otherOptions);
	    // app.runScriptLine("genhelixconstraint root.j2.(hxend)1 root.j3.(hxend)2 bpmin=6 bpmax=6");
	    // app.runScriptLine("genhelixconstraint root.j2_cov_jnc.j1.A_1_3_B_1_8 root.j3_cov_jnc.j1.B_2_3_C_2_9 bpmin=6 bpmax=6");
	    // System.out.println("Setting third constraint...");
	    // app.runScriptLine("genhelixconstraint root.j3." + hx1 + " root.j1." + hx2 + otherOptions);
	    // app.runScriptLine("genhelixconstraint root.j3.(hxend)1 root.j1.(hxend)2 bpmin=6 bpmax=6");
	    // app.runScriptLine("genhelixconstraint root.j3_cov_jnc.j1.A_2_3_B_2_8 root.j1_cov_jnc.j1.B_3_C_9 1.3.1.6 bpmin=6 bpmax=6");
	    System.out.println("Optimizing helices before closing circle (two-stages: later a ring closing constraint is set and the structure is again optimized");
	    app.runScriptLine("opthelices helices=false steps=1000 kt=1");
	    String outFileName = methodName + "_tmp.pdb";
	    app.runScriptLine("exportpdb " + outFileName + " junction=true");
	    String outFileName2 = methodName + ".pdb";
	    System.out.println("Setting third constraint...");
	    app.runScriptLine("genhelixconstraint root.j3." + hx1 + " root.j1." + hx2 + otherOptions);
	    System.out.println("Optimizing helices");
	    Properties optProperties = app.runScriptLine("opthelices helices=true steps=100000 kt=1 error=20 fuse=append");
	    app.runScriptLine("exportpdb " + outFileName2 + " junction=true helices=true");
	    // app.runScriptLine("tree forbidden=Atom3D");
	    assert Double.parseDouble(optProperties.getProperty(Optimizer.FINAL_SCORE)) < 25.0; // must be pretty good ring-closure
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a triangular structure out of one building block; fuse generated helices with junctions */
    @Test (groups={"new", "slow"})
    public void testOptimizeHelicesCommandPrepend(){
	final String methodName = "testOptimizeHelicesCommandPrepend";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    String jncFileName = "${NANOTILER_HOME}/test/fixtures/2J00.rnaview.pdb_j3_A-G1064_A-C1107_A-G1187_rnaview.pdb";
	    String namesfileName = "${NANOTILER_HOME}/test/fixtures/triangle.names";
	    app.runScriptLine("loadjunctions " + namesfileName + " ${NANOTILER_HOME}/test/fixtures");
	    app.runScriptLine("status");
	    app.runScriptLine("place j root j1 3 1 0 0 0 _1");
	    app.runScriptLine("place j root j2 3 1 100 0 0 _2");
	    app.runScriptLine("place j root j3 3 1 0 100 0 _3");
	    // 	    app.runScriptLine("import " + jncFileName + " name=j1");
	    // 	    app.runScriptLine("clone " + jncFileNa + " name=j1");
	    // 	    app.runScriptLine("clone " + jncFileName + " name=j1");
	    // 	    // app.runScriptLine("import " + jncFileName + " name=j2");
	    // 	    // app.runScriptLine("import " + jncFileName + " name=j3");
	    app.runScriptLine("tree forbidden=Atom3D");
	    System.out.println("Number of generated child nodes of root after importing 3 junctions: " +app.getGraphController().getGraph().getGraph().size()); // must be multiple of 3 children
	    assert (app.getGraphController().getGraph().getGraph().size() == 3); // must be 3 children
	    System.out.println("Setting first constraint...");
	    String hx1 = "(hxend)1";
	    String hx2 = "(hxend)2";
	    int hxlen = 6;
	    String otherOptions = " bp=" + hxlen;
	    app.runScriptLine("genhelixconstraint root.j1." + hx1 + " root.j2." + hx2 + otherOptions);
	    System.out.println("Setting second constraint...");
	    app.runScriptLine("genhelixconstraint root.j2." + hx1 + " root.j3." + hx2 + otherOptions);
	    // app.runScriptLine("genhelixconstraint root.j2.(hxend)1 root.j3.(hxend)2 bpmin=6 bpmax=6");
	    // app.runScriptLine("genhelixconstraint root.j2_cov_jnc.j1.A_1_3_B_1_8 root.j3_cov_jnc.j1.B_2_3_C_2_9 bpmin=6 bpmax=6");
	    // System.out.println("Setting third constraint...");
	    // app.runScriptLine("genhelixconstraint root.j3." + hx1 + " root.j1." + hx2 + otherOptions);
	    // app.runScriptLine("genhelixconstraint root.j3.(hxend)1 root.j1.(hxend)2 bpmin=6 bpmax=6");
	    // app.runScriptLine("genhelixconstraint root.j3_cov_jnc.j1.A_2_3_B_2_8 root.j1_cov_jnc.j1.B_3_C_9 1.3.1.6 bpmin=6 bpmax=6");
	    System.out.println("Optimizing helices before closing circle (two-stages: later a ring closing constraint is set and the structure is again optimized");
	    app.runScriptLine("opthelices helices=false steps=1000 kt=1");
	    String outFileName = methodName + "_tmp.pdb";
	    app.runScriptLine("exportpdb " + outFileName + " junction=true");
	    String outFileName2 = methodName + ".pdb";
	    System.out.println("Setting third constraint...");
	    app.runScriptLine("genhelixconstraint root.j3." + hx1 + " root.j1." + hx2 + otherOptions);
	    System.out.println("Optimizing helices");
	    Properties optProperties = app.runScriptLine("opthelices helices=true steps=100000 kt=1 error=20 fuse=prepend");
	    app.runScriptLine("exportpdb " + outFileName2 + " junction=true helices=true");
	    // app.runScriptLine("tree forbidden=Atom3D");
	    assert Double.parseDouble(optProperties.getProperty(Optimizer.FINAL_SCORE)) < 25.0; // must be pretty good ring-closure
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a triangular structure out of one building block */
    @Test (groups={"new", "slow", "outdated"})
    public void testOptimizeHelicesShort(){
	final String methodName = "testOptimizeHelicesShort";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    String jncFileName = "${NANOTILER_HOME}/test/fixtures/2J00.rnaview.pdb_j3_A-G1064_A-C1107_A-G1187_rnaview.pdb";
	    String namesfileName = "${NANOTILER_HOME}/test/fixtures/triangle.names";
	    app.runScriptLine("loadjunctions " + namesfileName + " ${NANOTILER_HOME}/test/fixtures");
	    app.runScriptLine("status");
	    app.runScriptLine("place j root j1 3 1 0 0 0 _1");
	    app.runScriptLine("place j root j2 3 1 100 0 0 _2");
	    app.runScriptLine("place j root j3 3 1 0 100 0 _3");
	    // 	    app.runScriptLine("import " + jncFileName + " name=j1");
	    // 	    app.runScriptLine("clone " + jncFileNa + " name=j1");
	    // 	    app.runScriptLine("clone " + jncFileName + " name=j1");
	    // 	    // app.runScriptLine("import " + jncFileName + " name=j2");
	    // 	    // app.runScriptLine("import " + jncFileName + " name=j3");
	    app.runScriptLine("tree forbidden=Atom3D");
	    System.out.println("Number of generated child nodes of root after importing 3 junctions: " +app.getGraphController().getGraph().getGraph().size()); // must be multiple of 3 children
	    assert (app.getGraphController().getGraph().getGraph().size() == 3); // must be 3 children
	    System.out.println("Setting first constraint...");
	    String hx1 = "(hxend)1";
	    String hx2 = "(hxend)2";
	    int hxlen = 6;
	    String otherOptions = " bp=" + hxlen;
	    app.runScriptLine("genhelixconstraint root.j1." + hx1 + " root.j2." + hx2 + otherOptions);
	    System.out.println("Setting second constraint...");
	    app.runScriptLine("genhelixconstraint root.j2." + hx1 + " root.j3." + hx2 + otherOptions);
	    // app.runScriptLine("genhelixconstraint root.j2.(hxend)1 root.j3.(hxend)2 bpmin=6 bpmax=6");
	    // app.runScriptLine("genhelixconstraint root.j2_cov_jnc.j1.A_1_3_B_1_8 root.j3_cov_jnc.j1.B_2_3_C_2_9 bpmin=6 bpmax=6");
	    // System.out.println("Setting third constraint...");
	    // app.runScriptLine("genhelixconstraint root.j3." + hx1 + " root.j1." + hx2 + otherOptions);
	    // app.runScriptLine("genhelixconstraint root.j3.(hxend)1 root.j1.(hxend)2 bpmin=6 bpmax=6");
	    // app.runScriptLine("genhelixconstraint root.j3_cov_jnc.j1.A_2_3_B_2_8 root.j1_cov_jnc.j1.B_3_C_9 1.3.1.6 bpmin=6 bpmax=6");
	    System.out.println("Optimizing helices before closing circle (two-stages: later a ring closing constraint is set and the structure is again optimized");
	    // app.runScriptLine("opthelices helices=false steps=100000 kt=10");
	    // String outFileName = methodName + "_tmp.pdb";
	    // app.runScriptLine("exportpdb " + outFileName + " junction=true");
	    String outFileName2 = methodName + ".pdb";
	    System.out.println("Setting third constraint...");
	    app.runScriptLine("genhelixconstraint root.j3." + hx1 + " root.j1." + hx2 + otherOptions);
	    System.out.println("Optimizing helices");
	    Properties optProperties = app.runScriptLine("opthelices helices=true steps=10000 kt=50 error=8");
	    app.runScriptLine("exportpdb " + outFileName2 + " junction=true helices=true");
	    // app.runScriptLine("tree forbidden=Atom3D");
	    assert Double.parseDouble(optProperties.getProperty(Optimizer.FINAL_SCORE)) < 20.0; // must be pretty good ring-closure
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a triangular structure out of one building block */
    @Test (groups={"new", "slow"})
    public void testOptimizeHelicesCommand2(){
	final String methodName = "testOptimizeHelicesCommand2";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    String jncFileName = "${NANOTILER_HOME}/test/fixtures/testOptimizeHelicesCommand.pdb";
	    app.runScriptLine("import " + jncFileName + " name=pdb1");
	    app.runScriptLine("import " + jncFileName + " name=pdb2");
	    app.runScriptLine("status");
	    String fullName1 = "root.pdb1.pdb1_cov_jnc.j1.(hxend)3";
	    String fullName1b = "root.pdb1.pdb1_cov_jnc.j1.B_1_C_11";
	    String fullName2 = "root.pdb2.pdb2_cov_jnc.j1.(hxend)1";
	    String fullName2b = "root.pdb2.pdb2_cov_jnc.j1.A_1_1_B_1_10"; 
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("echo Info about first helix:");
	    app.runScriptLine("tree forbidden=Atom3D name=" + fullName1b);
	    app.runScriptLine("echo Info about second helix:");
	    app.runScriptLine("tree forbidden=Atom3D name=" + fullName2b);
	    System.out.println("Number of generated child nodes of root after importing 3 junctions: " +app.getGraphController().getGraph().getGraph().size()); // must be multiple of 3 children
	    // assert (app.getGraphController().getGraph().getGraph().size() == 3); // must be 3 children
	    System.out.println("Setting first constraint...");
	    String hx1 = "(hxend)1";
	    String hx2 = "(hxend)2";
	    int hxlen = 6;
	    String otherOptions = " bp=" + hxlen;
	    app.runScriptLine("genhelixconstraint " + fullName1b + " " + fullName2b + " " + otherOptions);
	    // app.runScriptLine("genhelixconstraint root.j2.(hxend)1 root.j3.(hxend)2 bpmin=6 bpmax=6");
	    // app.runScriptLine("genhelixconstraint root.j2_cov_jnc.j1.A_1_3_B_1_8 root.j3_cov_jnc.j1.B_2_3_C_2_9 bpmin=6 bpmax=6");
	    // System.out.println("Setting third constraint...");
	    // app.runScriptLine("genhelixconstraint root.j3." + hx1 + " root.j1." + hx2 + otherOptions);
	    // app.runScriptLine("genhelixconstraint root.j3.(hxend)1 root.j1.(hxend)2 bpmin=6 bpmax=6");
	    // app.runScriptLine("genhelixconstraint root.j3_cov_jnc.j1.A_2_3_B_2_8 root.j1_cov_jnc.j1.B_3_C_9 1.3.1.6 bpmin=6 bpmax=6");
	    String outFileName2 = "testOptimizeHelicesCommand2.pdb";
	    System.out.println("Optimizing helices");
	    Properties optProperties = app.runScriptLine("opthelices helices=true steps=1000000 kt=50 error=8 parent=root.pdb1,root.pdb2");
	    app.runScriptLine("exportpdb " + outFileName2 + " junction=true helices=true");
	    // app.runScriptLine("tree forbidden=Atom3D");
	    assert Double.parseDouble(optProperties.getProperty(Optimizer.FINAL_SCORE)) < 10.0; // must be pretty good ring-closure
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a triangular structure out of one building block */
    @Test (groups={"new", "slow"})
    public void testOptimizeHelicesCommand3(){
	final String methodName = "testOptimizeHelicesCommand3";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    String ringFileName = "${NANOTILER_HOME}/test/fixtures/testOptimizeHelicesCommand.pdb";
	    String group1FileName = "${NANOTILER_HOME}/test/fixtures/1LDZ_model1.pdb";
	    String group2FileName = "${NANOTILER_HOME}/test/fixtures/1LDZ_model1.pdb";
	    //	    app.runScriptLine("import " + ringFileName + " name=ring");
	    app.runScriptLine("import " + group1FileName + " name=group1 findstems=true");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("import " + group2FileName + " name=group2 findstems=true");
	    app.runScriptLine("status");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("genhelixconstraint root.group1.stems.stemA_2.(hxend)1 root.group2.stems.stemA_1_2.(hxend)1 bp=0");
	    app.runScriptLine("links");
	    app.runScriptLine("opthelices parent=root.group1,root.group2");
	    app.runScriptLine("exportpdb " + methodName + ".pdb junction=false");
// 	    String fullName1 = "root.pdb1.pdb1_cov_jnc.j1.(hxend)3";
// 	    String fullName1b = "root.pdb1.pdb1_cov_jnc.j1.B_1_C_11";
// 	    String fullName2 = "root.pdb2.pdb2_cov_jnc.j1.(hxend)1";
// 	    String fullName2b = "root.pdb2.pdb2_cov_jnc.j1.A_1_1_B_1_10"; 
// 	    app.runScriptLine("tree forbidden=Atom3D");
// 	    app.runScriptLine("echo Info about first helix:");
// 	    app.runScriptLine("tree forbidden=Atom3D name=" + fullName1b);
// 	    app.runScriptLine("echo Info about second helix:");
// 	    app.runScriptLine("tree forbidden=Atom3D name=" + fullName2b);
// 	    System.out.println("Number of generated child nodes of root after importing 3 junctions: " +app.getGraphController().getGraph().getGraph().size()); // must be multiple of 3 children
// 	    // assert (app.getGraphController().getGraph().getGraph().size() == 3); // must be 3 children
// 	    System.out.println("Setting first constraint...");
// 	    String hx1 = "(hxend)1";
// 	    String hx2 = "(hxend)2";
// 	    int hxlen = 6;
// 	    String otherOptions = " bpmin=" + hxlen + " bpmax=" + hxlen;
// 	    app.runScriptLine("genhelixconstraint " + fullName1b + " " + fullName2b + " " + otherOptions);
// 	    // app.runScriptLine("genhelixconstraint root.j2.(hxend)1 root.j3.(hxend)2 bpmin=6 bpmax=6");
// 	    // app.runScriptLine("genhelixconstraint root.j2_cov_jnc.j1.A_1_3_B_1_8 root.j3_cov_jnc.j1.B_2_3_C_2_9 bpmin=6 bpmax=6");
// 	    // System.out.println("Setting third constraint...");
// 	    // app.runScriptLine("genhelixconstraint root.j3." + hx1 + " root.j1." + hx2 + otherOptions);
// 	    // app.runScriptLine("genhelixconstraint root.j3.(hxend)1 root.j1.(hxend)2 bpmin=6 bpmax=6");
// 	    // app.runScriptLine("genhelixconstraint root.j3_cov_jnc.j1.A_2_3_B_2_8 root.j1_cov_jnc.j1.B_3_C_9 1.3.1.6 bpmin=6 bpmax=6");
// 	    String outFileName2 = "testOptimizeHelicesCommand2.pdb";
// 	    System.out.println("Optimizing helices");
// 	    Properties optProperties = app.runScriptLine("opthelices helices=true steps=1000000 kt=50 error=8 parent=root.pdb1,root.pdb2");
// 	    app.runScriptLine("exportpdb " + outFileName2 + " junction=true helices=true");
// 	    // app.runScriptLine("tree forbidden=Atom3D");
// 	    assert Double.parseDouble(optProperties.getProperty(Optimizer.FINAL_SCORE)) < 10.0; // must be pretty good ring-closure
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a triangular structure out of one building block */
    @Test (groups={"new", "slow"})
    public void testOptimizeHexagonalHelicesBp8(){
	final String methodName = "testOptimizeHexganalHelicesBp8";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    String klFileName = "${NANOTILER_HOME}/test/fixtures/2BJ2.rnaview.pdb_k2_A-G7_B-G6.pdb";
	    app.runScriptLine("import " + klFileName + " name=kl findstems=true");
	    // app.runScriptLine("select root.kl");
	    // app.runScriptLine("unshift 66.27619041916168 -2.76757245508982 0.7947149700598815"); // move to center of gravity
	    // app.runScriptLine("randomize root.kl");
	    // app.runScriptLine("shift 100 0 0");
	    app.runScriptLine("synth cs cs60 root 0 0 0");
	    app.runScriptLine("select root.cs60");
	    app.runScriptLine("rotate 60 0 0 1");
	    app.runScriptLine("symadd root.cs60");
	    app.runScriptLine("synth cs cs120 root 0 0 0");
	    app.runScriptLine("select root.cs120");
	    app.runScriptLine("rotate 120 0 0 1");
	    app.runScriptLine("symadd root.cs120");
	    app.runScriptLine("synth cs cs180 root 0 0 0");
	    app.runScriptLine("select root.cs180");
	    app.runScriptLine("rotate 180 0 0 1");
	    app.runScriptLine("symadd root.cs180");
	    app.runScriptLine("synth cs cs240 root 0 0 0");
	    app.runScriptLine("select root.cs240");
	    app.runScriptLine("rotate 240 0 0 1");
	    app.runScriptLine("symadd root.cs240");
	    app.runScriptLine("synth cs cs300 root 0 0 0");
	    app.runScriptLine("select root.cs300");
	    app.runScriptLine("rotate 300 0 0 1");
	    app.runScriptLine("symadd root.cs300");
	    app.runScriptLine("syminfo");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("set bd1 root.kl.stems.stemA_1.A_13_A_1");
	    app.runScriptLine("set bd2 root.kl.stems.stemB_1.B_13_B_1");
	    app.runScriptLine("genhelixconstraint ${bd1} ${bd2} bp=8 sym1=0 sym2=1");
	    Properties optProperties = app.runScriptLine("opthelices helices=true steps=1400000 rms=50 kt=100 error=0.1 parent=root.kl firstfixed=false");
	    app.runScriptLine("exportpdb " + methodName + "_single.pdb junction=false");
	    app.runScriptLine("echo Attempting to generate a connector helix...");
	    app.runScriptLine("symapply ${bd1} 1");
	    app.runScriptLine("genhelix bp1=${bd1} bp2=${bd2} bp=9 rms=15");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("exportpdb " + methodName + "_helix.pdb junction=false");
	    app.runScriptLine("remove root.kl.stems");
	    app.runScriptLine("remove root.kl.kl_cov_kl");
	    app.runScriptLine("clone root.kl root kl60");
	    app.runScriptLine("clone root.helix_root root hr60");
	    app.runScriptLine("clone root.kl root kl120");
	    app.runScriptLine("clone root.helix_root root hr120");
	    app.runScriptLine("clone root.kl root kl180");
	    app.runScriptLine("clone root.helix_root root hr180");
	    app.runScriptLine("clone root.kl root kl240");
	    app.runScriptLine("clone root.helix_root root hr240");
	    app.runScriptLine("clone root.kl root kl300");
	    app.runScriptLine("clone root.helix_root root hr300");
	    app.runScriptLine("symapply root.kl60 1");
	    app.runScriptLine("symapply root.kl120 2");
	    app.runScriptLine("symapply root.kl180 3");
	    app.runScriptLine("symapply root.kl240 4");
	    app.runScriptLine("symapply root.kl300 5");
	    app.runScriptLine("symapply root.hr60 1");
	    app.runScriptLine("symapply root.hr120 2");
	    app.runScriptLine("symapply root.hr180 3");
	    app.runScriptLine("symapply root.hr240 4");
	    app.runScriptLine("symapply root.hr300 5");
	    app.runScriptLine("exportpdb " + methodName + ".pdb junction=false");

	    app.runScriptLine("exportpdb " + methodName + "_final.pdb junction=false");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
 	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a triangular structure out of one building block */
    @Test (groups={"new", "slow"})
    public void testOptimizeHexagonalHelicesBp9(){
	final String methodName = "testOptimizeHexagonalHelicesBp9";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    String klFileName = "${NANOTILER_HOME}/test/fixtures/2BJ2.rnaview.pdb_k2_A-G7_B-G6.pdb";
	    app.runScriptLine("import " + klFileName + " name=kl findstems=true");
	    // app.runScriptLine("select root.kl");
	    // app.runScriptLine("unshift 66.27619041916168 -2.76757245508982 0.7947149700598815"); // move to center of gravity
	    // app.runScriptLine("randomize root.kl");
	    // app.runScriptLine("shift 100 0 0");
	    app.runScriptLine("synth cs cs60 root 0 0 0");
	    app.runScriptLine("select root.cs60");
	    app.runScriptLine("rotate 60 0 0 1");
	    app.runScriptLine("symadd root.cs60");
	    app.runScriptLine("synth cs cs120 root 0 0 0");
	    app.runScriptLine("select root.cs120");
	    app.runScriptLine("rotate 120 0 0 1");
	    app.runScriptLine("symadd root.cs120");
	    app.runScriptLine("synth cs cs180 root 0 0 0");
	    app.runScriptLine("select root.cs180");
	    app.runScriptLine("rotate 180 0 0 1");
	    app.runScriptLine("symadd root.cs180");
	    app.runScriptLine("synth cs cs240 root 0 0 0");
	    app.runScriptLine("select root.cs240");
	    app.runScriptLine("rotate 240 0 0 1");
	    app.runScriptLine("symadd root.cs240");
	    app.runScriptLine("synth cs cs300 root 0 0 0");
	    app.runScriptLine("select root.cs300");
	    app.runScriptLine("rotate 300 0 0 1");
	    app.runScriptLine("symadd root.cs300");
	    app.runScriptLine("syminfo");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("set bd1 root.kl.stems.stemA_1.A_13_A_1");
	    app.runScriptLine("set bd2 root.kl.stems.stemB_1.B_13_B_1");
	    app.runScriptLine("genhelixconstraint ${bd1} ${bd2} bp=9 sym1=0 sym2=1");
	    Properties optProperties = app.runScriptLine("opthelices helices=true steps=1400000 rms=50 kt=100 error=0.1 parent=root.kl firstfixed=false");
	    app.runScriptLine("exportpdb " + methodName + "_single.pdb junction=false");
	    app.runScriptLine("echo Attempting to generate a connector helix...");
	    app.runScriptLine("symapply ${bd1} 1");
	    app.runScriptLine("genhelix bp1=${bd1} bp2=${bd2} bp=9 rms=15");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("exportpdb " + methodName + "_helix.pdb junction=false");
	    app.runScriptLine("remove root.kl.stems");
	    app.runScriptLine("remove root.kl.kl_cov_kl");
	    app.runScriptLine("clone root.kl root kl60");
	    app.runScriptLine("clone root.helix_root root hr60");
	    app.runScriptLine("clone root.kl root kl120");
	    app.runScriptLine("clone root.helix_root root hr120");
	    app.runScriptLine("clone root.kl root kl180");
	    app.runScriptLine("clone root.helix_root root hr180");
	    app.runScriptLine("clone root.kl root kl240");
	    app.runScriptLine("clone root.helix_root root hr240");
	    app.runScriptLine("clone root.kl root kl300");
	    app.runScriptLine("clone root.helix_root root hr300");
	    app.runScriptLine("symapply root.kl60 1");
	    app.runScriptLine("symapply root.kl120 2");
	    app.runScriptLine("symapply root.kl180 3");
	    app.runScriptLine("symapply root.kl240 4");
	    app.runScriptLine("symapply root.kl300 5");
	    app.runScriptLine("symapply root.hr60 1");
	    app.runScriptLine("symapply root.hr120 2");
	    app.runScriptLine("symapply root.hr180 3");
	    app.runScriptLine("symapply root.hr240 4");
	    app.runScriptLine("symapply root.hr300 5");
	    app.runScriptLine("exportpdb " + methodName + ".pdb junction=false");

	    app.runScriptLine("exportpdb " + methodName + "_final.pdb junction=false");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
 	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


    /** tests building a triangular structure out of one building block */
    @Test (groups={"new", "slow"})
    public void testOptimizeHexagonalHelicesBp10(){
	final String methodName = "testOptimizeHexagonalHelicesBp10";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    String klFileName = "${NANOTILER_HOME}/test/fixtures/2BJ2.rnaview.pdb_k2_A-G7_B-G6.pdb";
	    app.runScriptLine("import " + klFileName + " name=kl findstems=true");
	    // app.runScriptLine("select root.kl");
	    // app.runScriptLine("unshift 66.27619041916168 -2.76757245508982 0.7947149700598815"); // move to center of gravity
	    // app.runScriptLine("randomize root.kl");
	    // app.runScriptLine("shift 100 0 0");
	    app.runScriptLine("synth cs cs60 root 0 0 0");
	    app.runScriptLine("select root.cs60");
	    app.runScriptLine("rotate 60 0 0 1");
	    app.runScriptLine("symadd root.cs60");
	    app.runScriptLine("synth cs cs120 root 0 0 0");
	    app.runScriptLine("select root.cs120");
	    app.runScriptLine("rotate 120 0 0 1");
	    app.runScriptLine("symadd root.cs120");
	    app.runScriptLine("synth cs cs180 root 0 0 0");
	    app.runScriptLine("select root.cs180");
	    app.runScriptLine("rotate 180 0 0 1");
	    app.runScriptLine("symadd root.cs180");
	    app.runScriptLine("synth cs cs240 root 0 0 0");
	    app.runScriptLine("select root.cs240");
	    app.runScriptLine("rotate 240 0 0 1");
	    app.runScriptLine("symadd root.cs240");
	    app.runScriptLine("synth cs cs300 root 0 0 0");
	    app.runScriptLine("select root.cs300");
	    app.runScriptLine("rotate 300 0 0 1");
	    app.runScriptLine("symadd root.cs300");
	    app.runScriptLine("syminfo");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("set bd1 root.kl.stems.stemA_1.A_13_A_1");
	    app.runScriptLine("set bd2 root.kl.stems.stemB_1.B_13_B_1");
	    app.runScriptLine("genhelixconstraint ${bd1} ${bd2} bp=10 sym1=0 sym2=1");
	    Properties optProperties = app.runScriptLine("opthelices helices=true steps=1400000 rms=50 kt=100 error=0.1 parent=root.kl firstfixed=false");
	    app.runScriptLine("exportpdb " + methodName + "_single.pdb junction=false");
	    app.runScriptLine("echo Attempting to generate a connector helix...");
	    app.runScriptLine("symapply ${bd1} 1");
	    app.runScriptLine("genhelix bp1=${bd1} bp2=${bd2} bp=9 rms=5");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("exportpdb " + methodName + "_helix.pdb junction=false");
	    app.runScriptLine("remove root.kl.stems");
	    app.runScriptLine("remove root.kl.kl_cov_kl");
	    app.runScriptLine("clone root.kl root kl60");
	    app.runScriptLine("clone root.helix_root root hr60");
	    app.runScriptLine("clone root.kl root kl120");
	    app.runScriptLine("clone root.helix_root root hr120");
	    app.runScriptLine("clone root.kl root kl180");
	    app.runScriptLine("clone root.helix_root root hr180");
	    app.runScriptLine("clone root.kl root kl240");
	    app.runScriptLine("clone root.helix_root root hr240");
	    app.runScriptLine("clone root.kl root kl300");
	    app.runScriptLine("clone root.helix_root root hr300");
	    app.runScriptLine("symapply root.kl60 1");
	    app.runScriptLine("symapply root.kl120 2");
	    app.runScriptLine("symapply root.kl180 3");
	    app.runScriptLine("symapply root.kl240 4");
	    app.runScriptLine("symapply root.kl300 5");
	    app.runScriptLine("symapply root.hr60 1");
	    app.runScriptLine("symapply root.hr120 2");
	    app.runScriptLine("symapply root.hr180 3");
	    app.runScriptLine("symapply root.hr240 4");
	    app.runScriptLine("symapply root.hr300 5");
	    app.runScriptLine("exportpdb " + methodName + ".pdb junction=false");

	    app.runScriptLine("exportpdb " + methodName + "_final.pdb junction=false");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
 	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a triangular structure out of one building block */
    @Test (groups={"new", "slow"})
    public void testOptimizeHexagonalHelicesBp11(){
	final String methodName = "testOptimizeHexagonalHelicesBp11";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    String klFileName = "${NANOTILER_HOME}/test/fixtures/2BJ2.rnaview.pdb_k2_A-G7_B-G6.pdb";
	    app.runScriptLine("import " + klFileName + " name=kl findstems=true");
	    // app.runScriptLine("select root.kl");
	    // app.runScriptLine("unshift 66.27619041916168 -2.76757245508982 0.7947149700598815"); // move to center of gravity
	    // app.runScriptLine("randomize root.kl");
	    // app.runScriptLine("shift 100 0 0");
	    app.runScriptLine("synth cs cs60 root 0 0 0");
	    app.runScriptLine("select root.cs60");
	    app.runScriptLine("rotate 60 0 0 1");
	    app.runScriptLine("symadd root.cs60");
	    app.runScriptLine("synth cs cs120 root 0 0 0");
	    app.runScriptLine("select root.cs120");
	    app.runScriptLine("rotate 120 0 0 1");
	    app.runScriptLine("symadd root.cs120");
	    app.runScriptLine("synth cs cs180 root 0 0 0");
	    app.runScriptLine("select root.cs180");
	    app.runScriptLine("rotate 180 0 0 1");
	    app.runScriptLine("symadd root.cs180");
	    app.runScriptLine("synth cs cs240 root 0 0 0");
	    app.runScriptLine("select root.cs240");
	    app.runScriptLine("rotate 240 0 0 1");
	    app.runScriptLine("symadd root.cs240");
	    app.runScriptLine("synth cs cs300 root 0 0 0");
	    app.runScriptLine("select root.cs300");
	    app.runScriptLine("rotate 300 0 0 1");
	    app.runScriptLine("symadd root.cs300");
	    app.runScriptLine("syminfo");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("set bd1 root.kl.stems.stemA_1.A_13_A_1");
	    app.runScriptLine("set bd2 root.kl.stems.stemB_1.B_13_B_1");
	    app.runScriptLine("genhelixconstraint ${bd1} ${bd2} bp=11 sym1=0 sym2=1");
	    Properties optProperties = app.runScriptLine("opthelices helices=true steps=1400000 rms=50 kt=100 error=0.1 parent=root.kl firstfixed=false");
	    app.runScriptLine("exportpdb " + methodName + "_single.pdb junction=false");
	    app.runScriptLine("echo Attempting to generate a connector helix...");
	    app.runScriptLine("symapply ${bd1} 1");
	    app.runScriptLine("genhelix bp1=${bd1} bp2=${bd2} bp=9 rms=5");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("exportpdb " + methodName + "_helix.pdb junction=false");
	    app.runScriptLine("remove root.kl.stems");
	    app.runScriptLine("remove root.kl.kl_cov_kl");
	    app.runScriptLine("clone root.kl root kl60");
	    app.runScriptLine("clone root.helix_root root hr60");
	    app.runScriptLine("clone root.kl root kl120");
	    app.runScriptLine("clone root.helix_root root hr120");
	    app.runScriptLine("clone root.kl root kl180");
	    app.runScriptLine("clone root.helix_root root hr180");
	    app.runScriptLine("clone root.kl root kl240");
	    app.runScriptLine("clone root.helix_root root hr240");
	    app.runScriptLine("clone root.kl root kl300");
	    app.runScriptLine("clone root.helix_root root hr300");
	    app.runScriptLine("symapply root.kl60 1");
	    app.runScriptLine("symapply root.kl120 2");
	    app.runScriptLine("symapply root.kl180 3");
	    app.runScriptLine("symapply root.kl240 4");
	    app.runScriptLine("symapply root.kl300 5");
	    app.runScriptLine("symapply root.hr60 1");
	    app.runScriptLine("symapply root.hr120 2");
	    app.runScriptLine("symapply root.hr180 3");
	    app.runScriptLine("symapply root.hr240 4");
	    app.runScriptLine("symapply root.hr300 5");
	    app.runScriptLine("exportpdb " + methodName + ".pdb junction=false");
	    app.runScriptLine("exportpdb " + methodName + "_final.pdb junction=false");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
 	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	app = null;
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a triangular structure out of one building block */
    @Test (groups={"new", "slow"})
    public void testOptimizeHexagonalHelicesBp18(){
	final String methodName = "testOptimizeHexagonalHelicesBp18";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    String klFileName = "${NANOTILER_HOME}/test/fixtures/2BJ2.rnaview.pdb_k2_A-G7_B-G6.pdb";
	    app.runScriptLine("import " + klFileName + " name=kl findstems=true");
	    // app.runScriptLine("select root.kl");
	    // app.runScriptLine("unshift 66.27619041916168 -2.76757245508982 0.7947149700598815"); // move to center of gravity
	    // app.runScriptLine("randomize root.kl");
	    // app.runScriptLine("shift 100 0 0");
	    app.runScriptLine("synth cs cs60 root 0 0 0");
	    app.runScriptLine("select root.cs60");
	    app.runScriptLine("rotate 60 0 0 1");
	    app.runScriptLine("symadd root.cs60");
	    app.runScriptLine("synth cs cs120 root 0 0 0");
	    app.runScriptLine("select root.cs120");
	    app.runScriptLine("rotate 120 0 0 1");
	    app.runScriptLine("symadd root.cs120");
	    app.runScriptLine("synth cs cs180 root 0 0 0");
	    app.runScriptLine("select root.cs180");
	    app.runScriptLine("rotate 180 0 0 1");
	    app.runScriptLine("symadd root.cs180");
	    app.runScriptLine("synth cs cs240 root 0 0 0");
	    app.runScriptLine("select root.cs240");
	    app.runScriptLine("rotate 240 0 0 1");
	    app.runScriptLine("symadd root.cs240");
	    app.runScriptLine("synth cs cs300 root 0 0 0");
	    app.runScriptLine("select root.cs300");
	    app.runScriptLine("rotate 300 0 0 1");
	    app.runScriptLine("symadd root.cs300");
	    app.runScriptLine("syminfo");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("set bd1 root.kl.stems.stemA_1.A_13_A_1");
	    app.runScriptLine("set bd2 root.kl.stems.stemB_1.B_13_B_1");
	    app.runScriptLine("set bp 18");
	    app.runScriptLine("genhelixconstraint ${bd1} ${bd2} bp=${bp} sym1=0 sym2=1");
	    Properties optProperties = app.runScriptLine("opthelices helices=true steps=1400000 rms=50 kt=100 error=0.1 parent=root.kl firstfixed=false");
	    app.runScriptLine("exportpdb " + methodName + "_single.pdb junction=false");
	    app.runScriptLine("echo Attempting to generate a connector helix...");
	    app.runScriptLine("symapply ${bd1} 1");
	    app.runScriptLine("genhelix bp1=${bd1} bp2=${bd2} bp=${bp} rms=5");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("exportpdb " + methodName + "_helix.pdb junction=false");
	    app.runScriptLine("remove root.kl.stems");
	    app.runScriptLine("remove root.kl.kl_cov_kl");
	    app.runScriptLine("clone root.kl root kl60");
	    app.runScriptLine("clone root.helix_root root hr60");
	    app.runScriptLine("clone root.kl root kl120");
	    app.runScriptLine("clone root.helix_root root hr120");
	    app.runScriptLine("clone root.kl root kl180");
	    app.runScriptLine("clone root.helix_root root hr180");
	    app.runScriptLine("clone root.kl root kl240");
	    app.runScriptLine("clone root.helix_root root hr240");
	    app.runScriptLine("clone root.kl root kl300");
	    app.runScriptLine("clone root.helix_root root hr300");
	    app.runScriptLine("symapply root.kl60 1");
	    app.runScriptLine("symapply root.kl120 2");
	    app.runScriptLine("symapply root.kl180 3");
	    app.runScriptLine("symapply root.kl240 4");
	    app.runScriptLine("symapply root.kl300 5");
	    app.runScriptLine("symapply root.hr60 1");
	    app.runScriptLine("symapply root.hr120 2");
	    app.runScriptLine("symapply root.hr180 3");
	    app.runScriptLine("symapply root.hr240 4");
	    app.runScriptLine("symapply root.hr300 5");
	    app.runScriptLine("exportpdb " + methodName + ".pdb junction=false");
	    app.runScriptLine("exportpdb " + methodName + "_final.pdb junction=false");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
 	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	app = null;
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a hexagonal structure out of one building block */
    @Test (groups={"new", "slow"})
    public void testOptimizeHexagonalHelicesBp19(){
	final String methodName = "testOptimizeHexagonalHelicesBp19";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
          app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
          app.runScriptLine("status");
          String klFileName = "${NANOTILER_HOME}/test/fixtures/2BJ2.rnaview.pdb_k2_A-G7_B-G6.pdb";
          app.runScriptLine("import " + klFileName + " name=kl findstems=true");
          // app.runScriptLine("select root.kl");
          // app.runScriptLine("unshift 66.27619041916168 -2.76757245508982 0.7947149700598815"); // move to center of gravity
          // app.runScriptLine("randomize root.kl");
          // app.runScriptLine("shift 100 0 0");
          app.runScriptLine("synth cs cs60 root 0 0 0");
          app.runScriptLine("select root.cs60");
          app.runScriptLine("rotate 60 0 0 1");
          app.runScriptLine("symadd root.cs60");
          app.runScriptLine("synth cs cs120 root 0 0 0");
          app.runScriptLine("select root.cs120");
          app.runScriptLine("rotate 120 0 0 1");
          app.runScriptLine("symadd root.cs120");
          app.runScriptLine("synth cs cs180 root 0 0 0");
          app.runScriptLine("select root.cs180");
          app.runScriptLine("rotate 180 0 0 1");
          app.runScriptLine("symadd root.cs180");
          app.runScriptLine("synth cs cs240 root 0 0 0");
          app.runScriptLine("select root.cs240");
          app.runScriptLine("rotate 240 0 0 1");
          app.runScriptLine("symadd root.cs240");
          app.runScriptLine("synth cs cs300 root 0 0 0");
          app.runScriptLine("select root.cs300");
          app.runScriptLine("rotate 300 0 0 1");
          app.runScriptLine("symadd root.cs300");
          app.runScriptLine("syminfo");
          app.runScriptLine("tree forbidden=Atom3D");
          app.runScriptLine("set bd1 root.kl.stems.stemA_1.A_13_A_1");
          app.runScriptLine("set bd2 root.kl.stems.stemB_1.B_13_B_1");
	  app.runScriptLine("set bp 19");
          app.runScriptLine("genhelixconstraint ${bd1} ${bd2} bp=${bp} sym1=0 sym2=1");
          Properties optProperties = app.runScriptLine("opthelices helices=true steps=1400000 rms=50 kt=100 error=0.1 parent=root.kl firstfixed=false");
          app.runScriptLine("exportpdb " + methodName + "_single.pdb junction=false");
          app.runScriptLine("echo Attempting to generate a connector helix...");
          app.runScriptLine("symapply ${bd1} 1");
          app.runScriptLine("genhelix bp1=${bd1} bp2=${bd2} bp=${bp} rms=5");
          app.runScriptLine("tree forbidden=Atom3D");
          app.runScriptLine("exportpdb " + methodName + "_helix.pdb junction=false");
          app.runScriptLine("remove root.kl.stems");
          app.runScriptLine("remove root.kl.kl_cov_kl");
	  app.runScriptLine("clone root.kl root kl60");
	  app.runScriptLine("clone root.helix_root root hr60");
	  app.runScriptLine("clone root.kl root kl120");
	  app.runScriptLine("clone root.helix_root root hr120");
	  app.runScriptLine("clone root.kl root kl180");
	  app.runScriptLine("clone root.helix_root root hr180");
	  app.runScriptLine("clone root.kl root kl240");
	  app.runScriptLine("clone root.helix_root root hr240");
	  app.runScriptLine("clone root.kl root kl300");
	  app.runScriptLine("clone root.helix_root root hr300");
	  app.runScriptLine("symapply root.kl60 1");
	  app.runScriptLine("symapply root.kl120 2");
	  app.runScriptLine("symapply root.kl180 3");
	  app.runScriptLine("symapply root.kl240 4");
	  app.runScriptLine("symapply root.kl300 5");
	  app.runScriptLine("symapply root.hr60 1");
	  app.runScriptLine("symapply root.hr120 2");
	  app.runScriptLine("symapply root.hr180 3");
	  app.runScriptLine("symapply root.hr240 4");
	  app.runScriptLine("symapply root.hr300 5");
	  app.runScriptLine("exportpdb " + methodName + ".pdb junction=false");
	  app.runScriptLine("exportpdb " + methodName + "_final.pdb junction=false");
	  app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
 	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	app = null;
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a hexagonal structure out of one building block */
    @Test (groups={"new", "slow"})
    public void testOptimizeHexagonalHelicesBp20(){
	final String methodName = "testOptimizeHexagonalHelicesBp20";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    String klFileName = "${NANOTILER_HOME}/test/fixtures/2BJ2.rnaview.pdb_k2_A-G7_B-G6.pdb";
	    app.runScriptLine("import " + klFileName + " name=kl findstems=true");
	    // app.runScriptLine("select root.kl");
	    // app.runScriptLine("unshift 66.27619041916168 -2.76757245508982 0.7947149700598815"); // move to center of gravity
	    // app.runScriptLine("randomize root.kl");
	    // app.runScriptLine("shift 100 0 0");
	    app.runScriptLine("synth cs cs60 root 0 0 0");
	    app.runScriptLine("select root.cs60");
	    app.runScriptLine("rotate 60 0 0 1");
	    app.runScriptLine("symadd root.cs60");
	    app.runScriptLine("synth cs cs120 root 0 0 0");
	    app.runScriptLine("select root.cs120");
	    app.runScriptLine("rotate 120 0 0 1");
	    app.runScriptLine("symadd root.cs120");
	    app.runScriptLine("synth cs cs180 root 0 0 0");
	    app.runScriptLine("select root.cs180");
	    app.runScriptLine("rotate 180 0 0 1");
	    app.runScriptLine("symadd root.cs180");
	    app.runScriptLine("synth cs cs240 root 0 0 0");
	    app.runScriptLine("select root.cs240");
	    app.runScriptLine("rotate 240 0 0 1");
	    app.runScriptLine("symadd root.cs240");
	    app.runScriptLine("synth cs cs300 root 0 0 0");
	    app.runScriptLine("select root.cs300");
	    app.runScriptLine("rotate 300 0 0 1");
	    app.runScriptLine("symadd root.cs300");
	    app.runScriptLine("syminfo");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("set bd1 root.kl.stems.stemA_1.A_13_A_1");
	    app.runScriptLine("set bd2 root.kl.stems.stemB_1.B_13_B_1");
	    app.runScriptLine("set bp 20");
	    app.runScriptLine("genhelixconstraint ${bd1} ${bd2} bp=${bp} sym1=0 sym2=1");
	    Properties optProperties = app.runScriptLine("opthelices helices=true steps=1400000 rms=50 kt=100 error=0.1 parent=root.kl firstfixed=false");
	    app.runScriptLine("exportpdb " + methodName + "_single.pdb junction=false");
	    app.runScriptLine("echo Attempting to generate a connector helix...");
	    app.runScriptLine("symapply ${bd1} 1");
	    app.runScriptLine("genhelix bp1=${bd1} bp2=${bd2} bp=${bp} rms=5");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("exportpdb " + methodName + "_helix.pdb junction=false");
	    app.runScriptLine("remove root.kl.stems");
	    app.runScriptLine("remove root.kl.kl_cov_kl");
	    app.runScriptLine("clone root.kl root kl60");
	    app.runScriptLine("clone root.helix_root root hr60");
	    app.runScriptLine("clone root.kl root kl120");
	    app.runScriptLine("clone root.helix_root root hr120");
	    app.runScriptLine("clone root.kl root kl180");
	    app.runScriptLine("clone root.helix_root root hr180");
	    app.runScriptLine("clone root.kl root kl240");
	    app.runScriptLine("clone root.helix_root root hr240");
	    app.runScriptLine("clone root.kl root kl300");
	    app.runScriptLine("clone root.helix_root root hr300");
	    app.runScriptLine("symapply root.kl60 1");
	    app.runScriptLine("symapply root.kl120 2");
	    app.runScriptLine("symapply root.kl180 3");
	    app.runScriptLine("symapply root.kl240 4");
	    app.runScriptLine("symapply root.kl300 5");
	    app.runScriptLine("symapply root.hr60 1");
	    app.runScriptLine("symapply root.hr120 2");
	    app.runScriptLine("symapply root.hr180 3");
	    app.runScriptLine("symapply root.hr240 4");
	    app.runScriptLine("symapply root.hr300 5");
	    app.runScriptLine("exportpdb " + methodName + ".pdb junction=false");
	    app.runScriptLine("exportpdb " + methodName + "_final.pdb junction=false");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
 	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	app = null;
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a hexagonal structure out of one building block */
    @Test (groups={"new", "slow"})
    public void testOptimizeHexagonalHelicesBp21(){
	final String methodName = "testOptimizeHexagonalHelicesBp21";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    String klFileName = "${NANOTILER_HOME}/test/fixtures/2BJ2.rnaview.pdb_k2_A-G7_B-G6.pdb";
	    app.runScriptLine("import " + klFileName + " name=kl findstems=true");
	    // app.runScriptLine("select root.kl");
	    // app.runScriptLine("unshift 66.27619041916168 -2.76757245508982 0.7947149700598815"); // move to center of gravity
	    // app.runScriptLine("randomize root.kl");
	    // app.runScriptLine("shift 100 0 0");
	    app.runScriptLine("synth cs cs60 root 0 0 0");
	    app.runScriptLine("select root.cs60");
	    app.runScriptLine("rotate 60 0 0 1");
	    app.runScriptLine("symadd root.cs60");
	    app.runScriptLine("synth cs cs120 root 0 0 0");
	    app.runScriptLine("select root.cs120");
	    app.runScriptLine("rotate 120 0 0 1");
	    app.runScriptLine("symadd root.cs120");
	    app.runScriptLine("synth cs cs180 root 0 0 0");
	    app.runScriptLine("select root.cs180");
	    app.runScriptLine("rotate 180 0 0 1");
	    app.runScriptLine("symadd root.cs180");
	    app.runScriptLine("synth cs cs240 root 0 0 0");
	    app.runScriptLine("select root.cs240");
	    app.runScriptLine("rotate 240 0 0 1");
	    app.runScriptLine("symadd root.cs240");
	    app.runScriptLine("synth cs cs300 root 0 0 0");
	    app.runScriptLine("select root.cs300");
	    app.runScriptLine("rotate 300 0 0 1");
	    app.runScriptLine("symadd root.cs300");
	    app.runScriptLine("syminfo");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("set bd1 root.kl.stems.stemA_1.A_13_A_1");
	    app.runScriptLine("set bd2 root.kl.stems.stemB_1.B_13_B_1");
	    app.runScriptLine("set bp 21");
	    app.runScriptLine("genhelixconstraint ${bd1} ${bd2} bp=${bp} sym1=0 sym2=1");
	    Properties optProperties = app.runScriptLine("opthelices helices=true steps=1400000 rms=50 kt=100 error=0.1 parent=root.kl firstfixed=false");
	    app.runScriptLine("exportpdb " + methodName + "_single.pdb junction=false");
	    app.runScriptLine("echo Attempting to generate a connector helix...");
	    app.runScriptLine("symapply ${bd1} 1");
	    app.runScriptLine("genhelix bp1=${bd1} bp2=${bd2} bp=${bp} rms=5");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("exportpdb " + methodName + "_helix.pdb junction=false");
	    app.runScriptLine("remove root.kl.stems");
	    app.runScriptLine("remove root.kl.kl_cov_kl");
	    app.runScriptLine("clone root.kl root kl60");
	    app.runScriptLine("clone root.helix_root root hr60");
	    app.runScriptLine("clone root.kl root kl120");
	    app.runScriptLine("clone root.helix_root root hr120");
	    app.runScriptLine("clone root.kl root kl180");
	    app.runScriptLine("clone root.helix_root root hr180");
	    app.runScriptLine("clone root.kl root kl240");
	    app.runScriptLine("clone root.helix_root root hr240");
	    app.runScriptLine("clone root.kl root kl300");
	    app.runScriptLine("clone root.helix_root root hr300");
	    app.runScriptLine("symapply root.kl60 1");
	    app.runScriptLine("symapply root.kl120 2");
	    app.runScriptLine("symapply root.kl180 3");
	    app.runScriptLine("symapply root.kl240 4");
	    app.runScriptLine("symapply root.kl300 5");
	    app.runScriptLine("symapply root.hr60 1");
	    app.runScriptLine("symapply root.hr120 2");
	    app.runScriptLine("symapply root.hr180 3");
	    app.runScriptLine("symapply root.hr240 4");
	    app.runScriptLine("symapply root.hr300 5");
	    app.runScriptLine("exportpdb " + methodName + ".pdb junction=false");
	    app.runScriptLine("exportpdb " + methodName + "_final.pdb junction=false");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
 	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	app = null;
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a hexagonal structure out of one building block */
    @Test (groups={"new", "slow"})
    public void testOptimizeHexagonalHelicesBp22(){
	final String methodName = "testOptimizeHexagonalHelicesBp22";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    String klFileName = "${NANOTILER_HOME}/test/fixtures/2BJ2.rnaview.pdb_k2_A-G7_B-G6.pdb";
	    app.runScriptLine("import " + klFileName + " name=kl findstems=true");
	    // app.runScriptLine("select root.kl");
	    // app.runScriptLine("unshift 66.27619041916168 -2.76757245508982 0.7947149700598815"); // move to center of gravity
	    // app.runScriptLine("randomize root.kl");
	    // app.runScriptLine("shift 100 0 0");
	    app.runScriptLine("synth cs cs60 root 0 0 0");
	    app.runScriptLine("select root.cs60");
	    app.runScriptLine("rotate 60 0 0 1");
	    app.runScriptLine("symadd root.cs60");
	    app.runScriptLine("synth cs cs120 root 0 0 0");
	    app.runScriptLine("select root.cs120");
	    app.runScriptLine("rotate 120 0 0 1");
	    app.runScriptLine("symadd root.cs120");
	    app.runScriptLine("synth cs cs180 root 0 0 0");
	    app.runScriptLine("select root.cs180");
	    app.runScriptLine("rotate 180 0 0 1");
	    app.runScriptLine("symadd root.cs180");
	    app.runScriptLine("synth cs cs240 root 0 0 0");
	    app.runScriptLine("select root.cs240");
	    app.runScriptLine("rotate 240 0 0 1");
	    app.runScriptLine("symadd root.cs240");
	    app.runScriptLine("synth cs cs300 root 0 0 0");
	    app.runScriptLine("select root.cs300");
	    app.runScriptLine("rotate 300 0 0 1");
	    app.runScriptLine("symadd root.cs300");
	    app.runScriptLine("syminfo");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("set bd1 root.kl.stems.stemA_1.A_13_A_1");
	    app.runScriptLine("set bd2 root.kl.stems.stemB_1.B_13_B_1");
            app.runScriptLine("set bp 22");
	    app.runScriptLine("genhelixconstraint ${bd1} ${bd2} bp=${bp} sym1=0 sym2=1");
	    Properties optProperties = app.runScriptLine("opthelices helices=true steps=1400000 rms=50 kt=100 error=0.1 parent=root.kl firstfixed=false");
	    app.runScriptLine("exportpdb " + methodName + "_single.pdb junction=false");
	    app.runScriptLine("echo Attempting to generate a connector helix...");
	    app.runScriptLine("symapply ${bd1} 1");
	    app.runScriptLine("genhelix bp1=${bd1} bp2=${bd2} bp=${bp} rms=5");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("exportpdb " + methodName + "_helix.pdb junction=false");
	    app.runScriptLine("remove root.kl.stems");
	    app.runScriptLine("remove root.kl.kl_cov_kl");
	    app.runScriptLine("clone root.kl root kl60");
	    app.runScriptLine("clone root.helix_root root hr60");
	    app.runScriptLine("clone root.kl root kl120");
	    app.runScriptLine("clone root.helix_root root hr120");
	    app.runScriptLine("clone root.kl root kl180");
	    app.runScriptLine("clone root.helix_root root hr180");
	    app.runScriptLine("clone root.kl root kl240");
	    app.runScriptLine("clone root.helix_root root hr240");
	    app.runScriptLine("clone root.kl root kl300");
	    app.runScriptLine("clone root.helix_root root hr300");
	    app.runScriptLine("symapply root.kl60 1");
	    app.runScriptLine("symapply root.kl120 2");
	    app.runScriptLine("symapply root.kl180 3");
	    app.runScriptLine("symapply root.kl240 4");
	    app.runScriptLine("symapply root.kl300 5");
	    app.runScriptLine("symapply root.hr60 1");
	    app.runScriptLine("symapply root.hr120 2");
	    app.runScriptLine("symapply root.hr180 3");
	    app.runScriptLine("symapply root.hr240 4");
	    app.runScriptLine("symapply root.hr300 5");
	    app.runScriptLine("exportpdb " + methodName + ".pdb junction=false");
	    app.runScriptLine("exportpdb " + methodName + "_final.pdb junction=false");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
 	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	app = null;
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a hexagonal structure out of one building block */
    @Test (groups={"new", "slow"})
    public void testOptimizeHexagonalHelicesBp23(){
	final String methodName = "testOptimizeHexagonalHelicesBp23";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    String klFileName = "${NANOTILER_HOME}/test/fixtures/2BJ2.rnaview.pdb_k2_A-G7_B-G6.pdb";
	    app.runScriptLine("import " + klFileName + " name=kl findstems=true");
	    // app.runScriptLine("select root.kl");
	    // app.runScriptLine("unshift 66.27619041916168 -2.76757245508982 0.7947149700598815"); // move to center of gravity
	    // app.runScriptLine("randomize root.kl");
	    // app.runScriptLine("shift 100 0 0");
	    app.runScriptLine("synth cs cs60 root 0 0 0");
	    app.runScriptLine("select root.cs60");
	    app.runScriptLine("rotate 60 0 0 1");
	    app.runScriptLine("symadd root.cs60");
	    app.runScriptLine("synth cs cs120 root 0 0 0");
	    app.runScriptLine("select root.cs120");
	    app.runScriptLine("rotate 120 0 0 1");
	    app.runScriptLine("symadd root.cs120");
	    app.runScriptLine("synth cs cs180 root 0 0 0");
	    app.runScriptLine("select root.cs180");
	    app.runScriptLine("rotate 180 0 0 1");
	    app.runScriptLine("symadd root.cs180");
	    app.runScriptLine("synth cs cs240 root 0 0 0");
	    app.runScriptLine("select root.cs240");
	    app.runScriptLine("rotate 240 0 0 1");
	    app.runScriptLine("symadd root.cs240");
	    app.runScriptLine("synth cs cs300 root 0 0 0");
	    app.runScriptLine("select root.cs300");
	    app.runScriptLine("rotate 300 0 0 1");
	    app.runScriptLine("symadd root.cs300");
	    app.runScriptLine("syminfo");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("set bd1 root.kl.stems.stemA_1.A_13_A_1");
	    app.runScriptLine("set bd2 root.kl.stems.stemB_1.B_13_B_1");
            app.runScriptLine("set bp 23");
	    app.runScriptLine("genhelixconstraint ${bd1} ${bd2} bp=${bp} sym1=0 sym2=1");
	    Properties optProperties = app.runScriptLine("opthelices helices=true steps=1400000 rms=50 kt=100 error=0.1 parent=root.kl firstfixed=false");
	    app.runScriptLine("exportpdb " + methodName + "_single.pdb junction=false");
	    app.runScriptLine("echo Attempting to generate a connector helix...");
	    app.runScriptLine("symapply ${bd1} 1");
	    app.runScriptLine("genhelix bp1=${bd1} bp2=${bd2} bp=${bp} rms=5");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("exportpdb " + methodName + "_helix.pdb junction=false");
	    app.runScriptLine("remove root.kl.stems");
	    app.runScriptLine("remove root.kl.kl_cov_kl");
	    app.runScriptLine("clone root.kl root kl60");
	    app.runScriptLine("clone root.helix_root root hr60");
	    app.runScriptLine("clone root.kl root kl120");
	    app.runScriptLine("clone root.helix_root root hr120");
	    app.runScriptLine("clone root.kl root kl180");
	    app.runScriptLine("clone root.helix_root root hr180");
	    app.runScriptLine("clone root.kl root kl240");
	    app.runScriptLine("clone root.helix_root root hr240");
	    app.runScriptLine("clone root.kl root kl300");
	    app.runScriptLine("clone root.helix_root root hr300");
	    app.runScriptLine("symapply root.kl60 1");
	    app.runScriptLine("symapply root.kl120 2");
	    app.runScriptLine("symapply root.kl180 3");
	    app.runScriptLine("symapply root.kl240 4");
	    app.runScriptLine("symapply root.kl300 5");
	    app.runScriptLine("symapply root.hr60 1");
	    app.runScriptLine("symapply root.hr120 2");
	    app.runScriptLine("symapply root.hr180 3");
	    app.runScriptLine("symapply root.hr240 4");
	    app.runScriptLine("symapply root.hr300 5");
	    app.runScriptLine("exportpdb " + methodName + ".pdb junction=false");
	    app.runScriptLine("exportpdb " + methodName + "_final.pdb junction=false");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
 	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	app = null;
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a hexagonal structure out of one building block */
    @Test (groups={"new", "slow"})
    public void testOptimizeHexagonalHelicesBp24(){
	final String methodName = "testOptimizeHexagonalHelicesBp24";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    String klFileName = "${NANOTILER_HOME}/test/fixtures/2BJ2.rnaview.pdb_k2_A-G7_B-G6.pdb";
	    app.runScriptLine("import " + klFileName + " name=kl findstems=true");
	    // app.runScriptLine("select root.kl");
	    // app.runScriptLine("unshift 66.27619041916168 -2.76757245508982 0.7947149700598815"); // move to center of gravity
	    // app.runScriptLine("randomize root.kl");
	    // app.runScriptLine("shift 100 0 0");
	    app.runScriptLine("synth cs cs60 root 0 0 0");
	    app.runScriptLine("select root.cs60");
	    app.runScriptLine("rotate 60 0 0 1");
	    app.runScriptLine("symadd root.cs60");
	    app.runScriptLine("synth cs cs120 root 0 0 0");
	    app.runScriptLine("select root.cs120");
	    app.runScriptLine("rotate 120 0 0 1");
	    app.runScriptLine("symadd root.cs120");
	    app.runScriptLine("synth cs cs180 root 0 0 0");
	    app.runScriptLine("select root.cs180");
	    app.runScriptLine("rotate 180 0 0 1");
	    app.runScriptLine("symadd root.cs180");
	    app.runScriptLine("synth cs cs240 root 0 0 0");
	    app.runScriptLine("select root.cs240");
	    app.runScriptLine("rotate 240 0 0 1");
	    app.runScriptLine("symadd root.cs240");
	    app.runScriptLine("synth cs cs300 root 0 0 0");
	    app.runScriptLine("select root.cs300");
	    app.runScriptLine("rotate 300 0 0 1");
	    app.runScriptLine("symadd root.cs300");
	    app.runScriptLine("syminfo");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("set bd1 root.kl.stems.stemA_1.A_13_A_1");
	    app.runScriptLine("set bd2 root.kl.stems.stemB_1.B_13_B_1");
            app.runScriptLine("set bp 24");
	    app.runScriptLine("genhelixconstraint ${bd1} ${bd2} bp=${bp} sym1=0 sym2=1");
	    Properties optProperties = app.runScriptLine("opthelices helices=true steps=1400000 rms=50 kt=100 error=0.1 parent=root.kl firstfixed=false");
	    app.runScriptLine("exportpdb " + methodName + "_single.pdb junction=false");
	    app.runScriptLine("echo Attempting to generate a connector helix...");
	    app.runScriptLine("symapply ${bd1} 1");
	    app.runScriptLine("genhelix bp1=${bd1} bp2=${bd2} bp=${bp} rms=5");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("exportpdb " + methodName + "_helix.pdb junction=false");
	    app.runScriptLine("remove root.kl.stems");
	    app.runScriptLine("remove root.kl.kl_cov_kl");
	    app.runScriptLine("clone root.kl root kl60");
	    app.runScriptLine("clone root.helix_root root hr60");
	    app.runScriptLine("clone root.kl root kl120");
	    app.runScriptLine("clone root.helix_root root hr120");
	    app.runScriptLine("clone root.kl root kl180");
	    app.runScriptLine("clone root.helix_root root hr180");
	    app.runScriptLine("clone root.kl root kl240");
	    app.runScriptLine("clone root.helix_root root hr240");
	    app.runScriptLine("clone root.kl root kl300");
	    app.runScriptLine("clone root.helix_root root hr300");
	    app.runScriptLine("symapply root.kl60 1");
	    app.runScriptLine("symapply root.kl120 2");
	    app.runScriptLine("symapply root.kl180 3");
	    app.runScriptLine("symapply root.kl240 4");
	    app.runScriptLine("symapply root.kl300 5");
	    app.runScriptLine("symapply root.hr60 1");
	    app.runScriptLine("symapply root.hr120 2");
	    app.runScriptLine("symapply root.hr180 3");
	    app.runScriptLine("symapply root.hr240 4");
	    app.runScriptLine("symapply root.hr300 5");
	    app.runScriptLine("exportpdb " + methodName + ".pdb junction=false");
	    app.runScriptLine("exportpdb " + methodName + "_final.pdb junction=false");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
 	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	app = null;
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a hexagonal structure out of a kissing loop (formaing a dumb-bell shape structure for each side) */
    public void testOptimizeHexagonalHelicesBpN(int nbp, double error, double helixRms) 
	throws CommandException, CommandExecutionException {
	final String methodName = "testOptimizeHexagonalHelicesBpN" + nbp;
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	app.runScriptLine("status");
	String klFileName = "${NANOTILER_HOME}/test/fixtures/2BJ2.rnaview.pdb_k2_A-G7_B-G6.pdb";
	app.runScriptLine("import " + klFileName + " name=kl findstems=true");
	// app.runScriptLine("select root.kl");
	// app.runScriptLine("unshift 66.27619041916168 -2.76757245508982 0.7947149700598815"); // move to center of gravity
	// app.runScriptLine("randomize root.kl");
	// app.runScriptLine("shift 100 0 0");
	app.runScriptLine("synth cs cs60 root 0 0 0");
	app.runScriptLine("select root.cs60");
	app.runScriptLine("rotate 60 0 0 1");
	app.runScriptLine("symadd root.cs60");
	app.runScriptLine("synth cs cs120 root 0 0 0");
	app.runScriptLine("select root.cs120");
	app.runScriptLine("rotate 120 0 0 1");
	app.runScriptLine("symadd root.cs120");
	app.runScriptLine("synth cs cs180 root 0 0 0");
	app.runScriptLine("select root.cs180");
	app.runScriptLine("rotate 180 0 0 1");
	app.runScriptLine("symadd root.cs180");
	app.runScriptLine("synth cs cs240 root 0 0 0");
	app.runScriptLine("select root.cs240");
	app.runScriptLine("rotate 240 0 0 1");
	app.runScriptLine("symadd root.cs240");
	app.runScriptLine("synth cs cs300 root 0 0 0");
	app.runScriptLine("select root.cs300");
	app.runScriptLine("rotate 300 0 0 1");
	app.runScriptLine("symadd root.cs300");
	app.runScriptLine("syminfo");
	app.runScriptLine("tree forbidden=Atom3D");
	app.runScriptLine("set bd1 root.kl.stems.stemA_1.A_13_A_1");
	app.runScriptLine("set bd2 root.kl.stems.stemB_1.B_13_B_1");
	app.runScriptLine("genhelixconstraint ${bd1} ${bd2} bp=" + nbp + " sym1=0 sym2=1");
	Properties optProperties = app.runScriptLine("opthelices helices=true steps=1400000 rms=50 kt=100 error="
						     + error + " parent=root.kl firstfixed=false");
	app.runScriptLine("exportpdb " + methodName + "_single.pdb junction=false");
	app.runScriptLine("echo Attempting to generate a connector helix...");
	app.runScriptLine("symapply ${bd1} 1");
	app.runScriptLine("genhelix bp1=${bd1} bp2=${bd2} bp=" + nbp + " rms=" + helixRms);
	app.runScriptLine("tree forbidden=Atom3D");
	app.runScriptLine("exportpdb " + methodName + "_helix.pdb junction=false");
	app.runScriptLine("remove root.kl.stems");
	app.runScriptLine("remove root.kl.kl_cov_kl");
	app.runScriptLine("clone root.kl root kl60");
	app.runScriptLine("clone root.helix_root root hr60");
	app.runScriptLine("clone root.kl root kl120");
	app.runScriptLine("clone root.helix_root root hr120");
	app.runScriptLine("clone root.kl root kl180");
	app.runScriptLine("clone root.helix_root root hr180");
	app.runScriptLine("clone root.kl root kl240");
	app.runScriptLine("clone root.helix_root root hr240");
	app.runScriptLine("clone root.kl root kl300");
	app.runScriptLine("clone root.helix_root root hr300");
	app.runScriptLine("symapply root.kl60 1");
	app.runScriptLine("symapply root.kl120 2");
	app.runScriptLine("symapply root.kl180 3");
	app.runScriptLine("symapply root.kl240 4");
	app.runScriptLine("symapply root.kl300 5");
	app.runScriptLine("symapply root.hr60 1");
	app.runScriptLine("symapply root.hr120 2");
	app.runScriptLine("symapply root.hr180 3");
	app.runScriptLine("symapply root.hr240 4");
	app.runScriptLine("symapply root.hr300 5");
	app.runScriptLine("exportpdb " + methodName + ".pdb junction=false");
	app.runScriptLine("exportpdb " + methodName + "_final.pdb junction=false");
	app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + ".script");
	app.runScriptLine("clear all");           
	app = null;
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test (groups={"new", "slow"})
    public void testOptimizeHexagonalHelicesBpRange() {
	String methodName = "testOptimizeHexagonalHelicesBpRange";
	double helixRms = 15.0;
	for (int i = 11; i <= 11; ++i) {
	    try {
		System.out.println("Starting hexagonal ring structure optimization with bp=" + i + " (+ 6bp from motifs)");
		testOptimizeHexagonalHelicesBpN(i, 150, helixRms);
	    }
	    catch(CommandExecutionException e) {
		System.out.println("Error in " + methodName + " for " + i + " base pairs : " + e.getMessage());
	    } catch (CommandException ce) {
		System.out.println("Error for bp=" + i + " : " + ce.getMessage());
	    }
	}
    }

    /** tests building a pentagonal ring of out kissing loops and helices. Each "dump-bell" shaped sequence will have 8 + 3 + 3 = 14 bp */
    @Test (groups={"new", "slow"})
    public void testOptimizePentagonalHelicesBp8(){
	final String methodName = "testOptimizePentagonalHelicesBp8";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    String klFileName = "${NANOTILER_HOME}/test/fixtures/2BJ2.rnaview.pdb_k2_A-G7_B-G6.pdb";
	    app.runScriptLine("import " + klFileName + " name=kl findstems=true");
	    // app.runScriptLine("select root.kl");
	    // app.runScriptLine("unshift 66.27619041916168 -2.76757245508982 0.7947149700598815"); // move to center of gravity
	    // app.runScriptLine("randomize root.kl");
	    // app.runScriptLine("shift 100 0 0");
	    app.runScriptLine("synth cs cs72 root 0 0 0");
	    app.runScriptLine("select root.cs72");
	    app.runScriptLine("rotate 72 0 0 1");
	    app.runScriptLine("symadd root.cs72");
	    app.runScriptLine("synth cs cs144 root 0 0 0");
	    app.runScriptLine("select root.cs144");
	    app.runScriptLine("rotate 144 0 0 1");
	    app.runScriptLine("symadd root.cs144");
	    app.runScriptLine("synth cs cs216 root 0 0 0");
	    app.runScriptLine("select root.cs216");
	    app.runScriptLine("rotate 216 0 0 1");
	    app.runScriptLine("symadd root.cs216");
	    app.runScriptLine("synth cs cs288 root 0 0 0");
	    app.runScriptLine("select root.cs288");
	    app.runScriptLine("rotate 288 0 0 1");
	    app.runScriptLine("symadd root.cs288");
	    app.runScriptLine("syminfo");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("set bd1 root.kl.stems.stemA_1.A_13_A_1");
	    app.runScriptLine("set bd2 root.kl.stems.stemB_1.B_13_B_1");
	    app.runScriptLine("genhelixconstraint ${bd1} ${bd2} bp=8 sym1=0 sym2=1");
	    Properties optProperties = app.runScriptLine("opthelices helices=true steps=1400000 rms=50 kt=100 error=8 parent=root.kl firstfixed=false");
	    app.runScriptLine("exportpdb " + methodName + "_single.pdb junction=false");
	    app.runScriptLine("echo Attempting to generate a connector helix...");
	    app.runScriptLine("symapply ${bd1} 1");
	    app.runScriptLine("genhelix bp1=${bd1} bp2=${bd2} bp=9 rms=5");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("exportpdb " + methodName + "_helix.pdb junction=false");
	    app.runScriptLine("remove root.kl.stems");
	    app.runScriptLine("remove root.kl.kl_cov_kl");
	    app.runScriptLine("clone root.kl root kl60");
	    app.runScriptLine("clone root.helix_root root hr60");
	    app.runScriptLine("clone root.kl root kl120");
	    app.runScriptLine("clone root.helix_root root hr120");
	    app.runScriptLine("clone root.kl root kl180");
	    app.runScriptLine("clone root.helix_root root hr180");
	    app.runScriptLine("clone root.kl root kl240");
	    app.runScriptLine("clone root.helix_root root hr240");
	    app.runScriptLine("symapply root.kl60 1");
	    app.runScriptLine("symapply root.kl120 2");
	    app.runScriptLine("symapply root.kl180 3");
	    app.runScriptLine("symapply root.kl240 4");
	    app.runScriptLine("symapply root.hr60 1");
	    app.runScriptLine("symapply root.hr120 2");
	    app.runScriptLine("symapply root.hr180 3");
	    app.runScriptLine("symapply root.hr240 4");
	    app.runScriptLine("exportpdb " + methodName + ".pdb junction=false");

	    app.runScriptLine("exportpdb " + methodName + "_final.pdb junction=false");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
 	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a pentagonal ring of out kissing loops and helices. Each "dump-bell" shaped sequence will have 9 + 3 + 3 = 15 bp */
    @Test (groups={"new", "slow"})
    public void testOptimizePentagonalHelicesBp9(){
	final String methodName = "testOptimizePentagonalHelicesBp9";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    String klFileName = "${NANOTILER_HOME}/test/fixtures/2BJ2.rnaview.pdb_k2_A-G7_B-G6.pdb";
	    app.runScriptLine("import " + klFileName + " name=kl findstems=true");
	    // app.runScriptLine("select root.kl");
	    // app.runScriptLine("unshift 66.27619041916168 -2.76757245508982 0.7947149700598815"); // move to center of gravity
	    // app.runScriptLine("randomize root.kl");
	    // app.runScriptLine("shift 100 0 0");
	    app.runScriptLine("synth cs cs72 root 0 0 0");
	    app.runScriptLine("select root.cs72");
	    app.runScriptLine("rotate 72 0 0 1");
	    app.runScriptLine("symadd root.cs72");
	    app.runScriptLine("synth cs cs144 root 0 0 0");
	    app.runScriptLine("select root.cs144");
	    app.runScriptLine("rotate 144 0 0 1");
	    app.runScriptLine("symadd root.cs144");
	    app.runScriptLine("synth cs cs216 root 0 0 0");
	    app.runScriptLine("select root.cs216");
	    app.runScriptLine("rotate 216 0 0 1");
	    app.runScriptLine("symadd root.cs216");
	    app.runScriptLine("synth cs cs288 root 0 0 0");
	    app.runScriptLine("select root.cs288");
	    app.runScriptLine("rotate 288 0 0 1");
	    app.runScriptLine("symadd root.cs288");
	    app.runScriptLine("syminfo");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("set bd1 root.kl.stems.stemA_1.A_13_A_1");
	    app.runScriptLine("set bd2 root.kl.stems.stemB_1.B_13_B_1");
	    app.runScriptLine("genhelixconstraint ${bd1} ${bd2} bp=9 sym1=0 sym2=1");
	    Properties optProperties = app.runScriptLine("opthelices helices=true steps=1400000 rms=50 kt=100 error=8 parent=root.kl firstfixed=false");
	    app.runScriptLine("exportpdb " + methodName + "_single.pdb junction=false");
	    app.runScriptLine("echo Attempting to generate a connector helix...");
	    app.runScriptLine("symapply ${bd1} 1");
	    app.runScriptLine("genhelix bp1=${bd1} bp2=${bd2} bp=9 rms=5");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("exportpdb " + methodName + "_helix.pdb junction=false");
	    app.runScriptLine("remove root.kl.stems");
	    app.runScriptLine("remove root.kl.kl_cov_kl");
	    app.runScriptLine("clone root.kl root kl60");
	    app.runScriptLine("clone root.helix_root root hr60");
	    app.runScriptLine("clone root.kl root kl120");
	    app.runScriptLine("clone root.helix_root root hr120");
	    app.runScriptLine("clone root.kl root kl180");
	    app.runScriptLine("clone root.helix_root root hr180");
	    app.runScriptLine("clone root.kl root kl240");
	    app.runScriptLine("clone root.helix_root root hr240");
	    app.runScriptLine("symapply root.kl60 1");
	    app.runScriptLine("symapply root.kl120 2");
	    app.runScriptLine("symapply root.kl180 3");
	    app.runScriptLine("symapply root.kl240 4");
	    app.runScriptLine("symapply root.hr60 1");
	    app.runScriptLine("symapply root.hr120 2");
	    app.runScriptLine("symapply root.hr180 3");
	    app.runScriptLine("symapply root.hr240 4");
	    app.runScriptLine("exportpdb " + methodName + ".pdb junction=false");

	    app.runScriptLine("exportpdb " + methodName + "_final.pdb junction=false");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
 	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a pentagonal ring of out kissing loops and helices. Each "dump-bell" shaped sequence will have 10 + 3 + 3 = 16 bp */
    @Test (groups={"new", "slow"})
    public void testOptimizePentagonalHelicesBp10(){
	final String methodName = "testOptimizePentagonalHelicesBp10";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    String klFileName = "${NANOTILER_HOME}/test/fixtures/2BJ2.rnaview.pdb_k2_A-G7_B-G6.pdb";
	    app.runScriptLine("import " + klFileName + " name=kl findstems=true");
	    // app.runScriptLine("select root.kl");
	    // app.runScriptLine("unshift 66.27619041916168 -2.76757245508982 0.7947149700598815"); // move to center of gravity
	    // app.runScriptLine("randomize root.kl");
	    // app.runScriptLine("shift 100 0 0");
	    app.runScriptLine("synth cs cs72 root 0 0 0");
	    app.runScriptLine("select root.cs72");
	    app.runScriptLine("rotate 72 0 0 1");
	    app.runScriptLine("symadd root.cs72");
	    app.runScriptLine("synth cs cs144 root 0 0 0");
	    app.runScriptLine("select root.cs144");
	    app.runScriptLine("rotate 144 0 0 1");
	    app.runScriptLine("symadd root.cs144");
	    app.runScriptLine("synth cs cs216 root 0 0 0");
	    app.runScriptLine("select root.cs216");
	    app.runScriptLine("rotate 216 0 0 1");
	    app.runScriptLine("symadd root.cs216");
	    app.runScriptLine("synth cs cs288 root 0 0 0");
	    app.runScriptLine("select root.cs288");
	    app.runScriptLine("rotate 288 0 0 1");
	    app.runScriptLine("symadd root.cs288");
	    app.runScriptLine("syminfo");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("set bd1 root.kl.stems.stemA_1.A_13_A_1");
	    app.runScriptLine("set bd2 root.kl.stems.stemB_1.B_13_B_1");
	    app.runScriptLine("genhelixconstraint ${bd1} ${bd2} bp=10 sym1=0 sym2=1");
	    Properties optProperties = app.runScriptLine("opthelices helices=true steps=1400000 rms=50 kt=100 error=8 parent=root.kl firstfixed=false");
	    app.runScriptLine("exportpdb " + methodName + "_single.pdb junction=false");
	    app.runScriptLine("echo Attempting to generate a connector helix...");
	    app.runScriptLine("symapply ${bd1} 1");
	    app.runScriptLine("genhelix bp1=${bd1} bp2=${bd2} bp=9 rms=5");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("exportpdb " + methodName + "_helix.pdb junction=false");
	    app.runScriptLine("remove root.kl.stems");
	    app.runScriptLine("remove root.kl.kl_cov_kl");
	    app.runScriptLine("clone root.kl root kl60");
	    app.runScriptLine("clone root.helix_root root hr60");
	    app.runScriptLine("clone root.kl root kl120");
	    app.runScriptLine("clone root.helix_root root hr120");
	    app.runScriptLine("clone root.kl root kl180");
	    app.runScriptLine("clone root.helix_root root hr180");
	    app.runScriptLine("clone root.kl root kl240");
	    app.runScriptLine("clone root.helix_root root hr240");
	    app.runScriptLine("symapply root.kl60 1");
	    app.runScriptLine("symapply root.kl120 2");
	    app.runScriptLine("symapply root.kl180 3");
	    app.runScriptLine("symapply root.kl240 4");
	    app.runScriptLine("symapply root.hr60 1");
	    app.runScriptLine("symapply root.hr120 2");
	    app.runScriptLine("symapply root.hr180 3");
	    app.runScriptLine("symapply root.hr240 4");
	    app.runScriptLine("exportpdb " + methodName + ".pdb junction=false");

	    app.runScriptLine("exportpdb " + methodName + "_final.pdb junction=false");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
 	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a triangular structure out of one building block */
    @Test (groups={"new", "slow"})
    public void testOptimizePentagonalHelicesBp11(){
	final String methodName = "testOptimizePentagonalHelicesBp11";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    String klFileName = "${NANOTILER_HOME}/test/fixtures/2BJ2.rnaview.pdb_k2_A-G7_B-G6.pdb";
	    app.runScriptLine("import " + klFileName + " name=kl findstems=true");
	    // app.runScriptLine("select root.kl");
	    // app.runScriptLine("unshift 66.27619041916168 -2.76757245508982 0.7947149700598815"); // move to center of gravity
	    // app.runScriptLine("randomize root.kl");
	    // app.runScriptLine("shift 100 0 0");
	    app.runScriptLine("synth cs cs72 root 0 0 0");
	    app.runScriptLine("select root.cs72");
	    app.runScriptLine("rotate 72 0 0 1");
	    app.runScriptLine("symadd root.cs72");
	    app.runScriptLine("synth cs cs144 root 0 0 0");
	    app.runScriptLine("select root.cs144");
	    app.runScriptLine("rotate 144 0 0 1");
	    app.runScriptLine("symadd root.cs144");
	    app.runScriptLine("synth cs cs216 root 0 0 0");
	    app.runScriptLine("select root.cs216");
	    app.runScriptLine("rotate 216 0 0 1");
	    app.runScriptLine("symadd root.cs216");
	    app.runScriptLine("synth cs cs288 root 0 0 0");
	    app.runScriptLine("select root.cs288");
	    app.runScriptLine("rotate 288 0 0 1");
	    app.runScriptLine("symadd root.cs288");
	    app.runScriptLine("syminfo");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("set bd1 root.kl.stems.stemA_1.A_13_A_1");
	    app.runScriptLine("set bd2 root.kl.stems.stemB_1.B_13_B_1");
	    app.runScriptLine("genhelixconstraint ${bd1} ${bd2} bp=11 sym1=0 sym2=1");
	    Properties optProperties = app.runScriptLine("opthelices helices=true steps=1400000 rms=50 kt=100 error=8 parent=root.kl firstfixed=false");
	    app.runScriptLine("exportpdb " + methodName + "_single.pdb junction=false");
	    app.runScriptLine("echo Attempting to generate a connector helix...");
	    app.runScriptLine("symapply ${bd1} 1");
	    app.runScriptLine("genhelix bp1=${bd1} bp2=${bd2} bp=9 rms=5");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("exportpdb " + methodName + "_helix.pdb junction=false");
	    app.runScriptLine("remove root.kl.stems");
	    app.runScriptLine("remove root.kl.kl_cov_kl");
	    app.runScriptLine("clone root.kl root kl60");
	    app.runScriptLine("clone root.helix_root root hr60");
	    app.runScriptLine("clone root.kl root kl120");
	    app.runScriptLine("clone root.helix_root root hr120");
	    app.runScriptLine("clone root.kl root kl180");
	    app.runScriptLine("clone root.helix_root root hr180");
	    app.runScriptLine("clone root.kl root kl240");
	    app.runScriptLine("clone root.helix_root root hr240");
	    app.runScriptLine("symapply root.kl60 1");
	    app.runScriptLine("symapply root.kl120 2");
	    app.runScriptLine("symapply root.kl180 3");
	    app.runScriptLine("symapply root.kl240 4");
	    app.runScriptLine("symapply root.hr60 1");
	    app.runScriptLine("symapply root.hr120 2");
	    app.runScriptLine("symapply root.hr180 3");
	    app.runScriptLine("symapply root.hr240 4");
	    app.runScriptLine("exportpdb " + methodName + ".pdb junction=false");

	    app.runScriptLine("exportpdb " + methodName + "_final.pdb junction=false");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
 	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a pentagonal ring of out kissing loops and helices. Each "dump-bell" shaped sequence will have 10 + 3 + 3 = 16 bp */
    @Test (groups={"new", "slow"})
    public void testOptimizePentagonalHelicesBp18(){
	final String methodName = "testOptimizePentagonalHelicesBp18";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    String klFileName = "${NANOTILER_HOME}/test/fixtures/2BJ2.rnaview.pdb_k2_A-G7_B-G6.pdb";
	    app.runScriptLine("import " + klFileName + " name=kl findstems=true");
	    // app.runScriptLine("select root.kl");
	    // app.runScriptLine("unshift 66.27619041916168 -2.76757245508982 0.7947149700598815"); // move to center of gravity
	    // app.runScriptLine("randomize root.kl");
	    // app.runScriptLine("shift 100 0 0");
	    app.runScriptLine("synth cs cs72 root 0 0 0");
	    app.runScriptLine("select root.cs72");
	    app.runScriptLine("rotate 72 0 0 1");
	    app.runScriptLine("symadd root.cs72");
	    app.runScriptLine("synth cs cs144 root 0 0 0");
	    app.runScriptLine("select root.cs144");
	    app.runScriptLine("rotate 144 0 0 1");
	    app.runScriptLine("symadd root.cs144");
	    app.runScriptLine("synth cs cs216 root 0 0 0");
	    app.runScriptLine("select root.cs216");
	    app.runScriptLine("rotate 216 0 0 1");
	    app.runScriptLine("symadd root.cs216");
	    app.runScriptLine("synth cs cs288 root 0 0 0");
	    app.runScriptLine("select root.cs288");
	    app.runScriptLine("rotate 288 0 0 1");
	    app.runScriptLine("symadd root.cs288");
	    app.runScriptLine("syminfo");
	    app.runScriptLine("tree allowed=BranchDescriptor3D");
	    app.runScriptLine("set bd1 root.kl.stems.stemA_1.A_13_A_1");
	    app.runScriptLine("set bd2 root.kl.stems.stemB_1.B_13_B_1");
	    app.runScriptLine("set bp 18");
	    app.runScriptLine("genhelixconstraint ${bd1} ${bd2} bp=${bp} sym1=0 sym2=1");
	    Properties optProperties = app.runScriptLine("opthelices helices=true steps=1400000 rms=50 kt=100 error=250 parent=root.kl firstfixed=false");
	    app.runScriptLine("exportpdb " + methodName + "_single.pdb junction=false");
	    app.runScriptLine("echo Attempting to generate a connector helix...");
	    app.runScriptLine("symapply ${bd1} 1");
	    app.runScriptLine("history file=" + methodName + ".tmp1.hist");
	    app.runScriptLine("genhelix bp1=${bd1} bp2=${bd2} bp=${bp} rms=15");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("exportpdb " + methodName + "_helix.pdb junction=false");
	    app.runScriptLine("remove root.kl.stems");
	    app.runScriptLine("remove root.kl.kl_cov_kl");
	    app.runScriptLine("clone root.kl root kl60");
	    app.runScriptLine("clone root.helix_root root hr60");
	    app.runScriptLine("clone root.kl root kl120");
	    app.runScriptLine("clone root.helix_root root hr120");
	    app.runScriptLine("clone root.kl root kl180");
	    app.runScriptLine("clone root.helix_root root hr180");
	    app.runScriptLine("clone root.kl root kl240");
	    app.runScriptLine("clone root.helix_root root hr240");
	    app.runScriptLine("symapply root.kl60 1");
	    app.runScriptLine("symapply root.kl120 2");
	    app.runScriptLine("symapply root.kl180 3");
	    app.runScriptLine("symapply root.kl240 4");
	    app.runScriptLine("symapply root.hr60 1");
	    app.runScriptLine("symapply root.hr120 2");
	    app.runScriptLine("symapply root.hr180 3");
	    app.runScriptLine("symapply root.hr240 4");
	    app.runScriptLine("exportpdb " + methodName + ".pdb junction=false");

	    app.runScriptLine("exportpdb " + methodName + "_final.pdb junction=false");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
 	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a pentagonal ring of out kissing loops and helices. Each "dump-bell" shaped sequence will have 10 + 3 + 3 = 16 bp */
    @Test (groups={"new", "slow"})
    public void testOptimizePentagonalHelicesBp19(){
	final String methodName = "testOptimizePentagonalHelicesBp19";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    String klFileName = "${NANOTILER_HOME}/test/fixtures/2BJ2.rnaview.pdb_k2_A-G7_B-G6.pdb";
	    app.runScriptLine("import " + klFileName + " name=kl findstems=true");
	    // app.runScriptLine("select root.kl");
	    // app.runScriptLine("unshift 66.27619041916168 -2.76757245508982 0.7947149700598815"); // move to center of gravity
	    // app.runScriptLine("randomize root.kl");
	    // app.runScriptLine("shift 100 0 0");
	    app.runScriptLine("synth cs cs72 root 0 0 0");
	    app.runScriptLine("select root.cs72");
	    app.runScriptLine("rotate 72 0 0 1");
	    app.runScriptLine("symadd root.cs72");
	    app.runScriptLine("synth cs cs144 root 0 0 0");
	    app.runScriptLine("select root.cs144");
	    app.runScriptLine("rotate 144 0 0 1");
	    app.runScriptLine("symadd root.cs144");
	    app.runScriptLine("synth cs cs216 root 0 0 0");
	    app.runScriptLine("select root.cs216");
	    app.runScriptLine("rotate 216 0 0 1");
	    app.runScriptLine("symadd root.cs216");
	    app.runScriptLine("synth cs cs288 root 0 0 0");
	    app.runScriptLine("select root.cs288");
	    app.runScriptLine("rotate 288 0 0 1");
	    app.runScriptLine("symadd root.cs288");
	    app.runScriptLine("syminfo");
	    app.runScriptLine("tree allowed=BranchDescriptor3D");
	    app.runScriptLine("set bd1 root.kl.stems.stemA_1.A_13_A_1");
	    app.runScriptLine("set bd2 root.kl.stems.stemB_1.B_13_B_1");
	    app.runScriptLine("set bp 19");
	    app.runScriptLine("genhelixconstraint ${bd1} ${bd2} bp=${bp} sym1=0 sym2=1");
	    Properties optProperties = app.runScriptLine("opthelices helices=true steps=1400000 rms=50 kt=100 error=250 parent=root.kl firstfixed=false");
	    app.runScriptLine("exportpdb " + methodName + "_single.pdb junction=false");
	    app.runScriptLine("echo Attempting to generate a connector helix...");
	    app.runScriptLine("symapply ${bd1} 1");
	    app.runScriptLine("history file=" + methodName + ".tmp1.hist");
	    app.runScriptLine("genhelix bp1=${bd1} bp2=${bd2} bp=${bp} rms=15");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("exportpdb " + methodName + "_helix.pdb junction=false");
	    app.runScriptLine("remove root.kl.stems");
	    app.runScriptLine("remove root.kl.kl_cov_kl");
	    app.runScriptLine("clone root.kl root kl60");
	    app.runScriptLine("clone root.helix_root root hr60");
	    app.runScriptLine("clone root.kl root kl120");
	    app.runScriptLine("clone root.helix_root root hr120");
	    app.runScriptLine("clone root.kl root kl180");
	    app.runScriptLine("clone root.helix_root root hr180");
	    app.runScriptLine("clone root.kl root kl240");
	    app.runScriptLine("clone root.helix_root root hr240");
	    app.runScriptLine("symapply root.kl60 1");
	    app.runScriptLine("symapply root.kl120 2");
	    app.runScriptLine("symapply root.kl180 3");
	    app.runScriptLine("symapply root.kl240 4");
	    app.runScriptLine("symapply root.hr60 1");
	    app.runScriptLine("symapply root.hr120 2");
	    app.runScriptLine("symapply root.hr180 3");
	    app.runScriptLine("symapply root.hr240 4");
	    app.runScriptLine("exportpdb " + methodName + ".pdb junction=false");

	    app.runScriptLine("exportpdb " + methodName + "_final.pdb junction=false");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
 	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a pentagonal ring of out kissing loops and helices. Each "dump-bell" shaped sequence will have 10 + 3 + 3 = 16 bp */
    @Test (groups={"new", "slow"})
    public void testOptimizePentagonalHelicesBp20(){
	final String methodName = "testOptimizePentagonalHelicesBp20";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    String klFileName = "${NANOTILER_HOME}/test/fixtures/2BJ2.rnaview.pdb_k2_A-G7_B-G6.pdb";
	    app.runScriptLine("import " + klFileName + " name=kl findstems=true");
	    // app.runScriptLine("select root.kl");
	    // app.runScriptLine("unshift 66.27619041916168 -2.76757245508982 0.7947149700598815"); // move to center of gravity
	    // app.runScriptLine("randomize root.kl");
	    // app.runScriptLine("shift 100 0 0");
	    app.runScriptLine("synth cs cs72 root 0 0 0");
	    app.runScriptLine("select root.cs72");
	    app.runScriptLine("rotate 72 0 0 1");
	    app.runScriptLine("symadd root.cs72");
	    app.runScriptLine("synth cs cs144 root 0 0 0");
	    app.runScriptLine("select root.cs144");
	    app.runScriptLine("rotate 144 0 0 1");
	    app.runScriptLine("symadd root.cs144");
	    app.runScriptLine("synth cs cs216 root 0 0 0");
	    app.runScriptLine("select root.cs216");
	    app.runScriptLine("rotate 216 0 0 1");
	    app.runScriptLine("symadd root.cs216");
	    app.runScriptLine("synth cs cs288 root 0 0 0");
	    app.runScriptLine("select root.cs288");
	    app.runScriptLine("rotate 288 0 0 1");
	    app.runScriptLine("symadd root.cs288");
	    app.runScriptLine("syminfo");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("set bd1 root.kl.stems.stemA_1.A_13_A_1");
	    app.runScriptLine("set bd2 root.kl.stems.stemB_1.B_13_B_1");
	    app.runScriptLine("set bp 20");
	    app.runScriptLine("genhelixconstraint ${bd1} ${bd2} bp=${bp} sym1=0 sym2=1");
	    Properties optProperties = app.runScriptLine("opthelices helices=true steps=1400000 rms=50 kt=100 error=250 parent=root.kl firstfixed=false");
	    app.runScriptLine("exportpdb " + methodName + "_single.pdb junction=false");
	    app.runScriptLine("echo Attempting to generate a connector helix...");
	    app.runScriptLine("symapply ${bd1} 1");
	    app.runScriptLine("genhelix bp1=${bd1} bp2=${bd2} bp=${bp} rms=15");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("exportpdb " + methodName + "_helix.pdb junction=false");
	    app.runScriptLine("remove root.kl.stems");
	    app.runScriptLine("remove root.kl.kl_cov_kl");
	    app.runScriptLine("clone root.kl root kl60");
	    app.runScriptLine("clone root.helix_root root hr60");
	    app.runScriptLine("clone root.kl root kl120");
	    app.runScriptLine("clone root.helix_root root hr120");
	    app.runScriptLine("clone root.kl root kl180");
	    app.runScriptLine("clone root.helix_root root hr180");
	    app.runScriptLine("clone root.kl root kl240");
	    app.runScriptLine("clone root.helix_root root hr240");
	    app.runScriptLine("symapply root.kl60 1");
	    app.runScriptLine("symapply root.kl120 2");
	    app.runScriptLine("symapply root.kl180 3");
	    app.runScriptLine("symapply root.kl240 4");
	    app.runScriptLine("symapply root.hr60 1");
	    app.runScriptLine("symapply root.hr120 2");
	    app.runScriptLine("symapply root.hr180 3");
	    app.runScriptLine("symapply root.hr240 4");
	    app.runScriptLine("exportpdb " + methodName + ".pdb junction=false");

	    app.runScriptLine("exportpdb " + methodName + "_final.pdb junction=false");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
 	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a pentagonal ring of out kissing loops and helices. Each "dump-bell" shaped sequence will have 10 + 3 + 3 = 16 bp */
    @Test (groups={"new", "slow"})
    public void testOptimizePentagonalHelicesBp21(){
	final String methodName = "testOptimizePentagonalHelicesBp21";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    String klFileName = "${NANOTILER_HOME}/test/fixtures/2BJ2.rnaview.pdb_k2_A-G7_B-G6.pdb";
	    app.runScriptLine("import " + klFileName + " name=kl findstems=true");
	    // app.runScriptLine("select root.kl");
	    // app.runScriptLine("unshift 66.27619041916168 -2.76757245508982 0.7947149700598815"); // move to center of gravity
	    // app.runScriptLine("randomize root.kl");
	    // app.runScriptLine("shift 100 0 0");
	    app.runScriptLine("synth cs cs72 root 0 0 0");
	    app.runScriptLine("select root.cs72");
	    app.runScriptLine("rotate 72 0 0 1");
	    app.runScriptLine("symadd root.cs72");
	    app.runScriptLine("synth cs cs144 root 0 0 0");
	    app.runScriptLine("select root.cs144");
	    app.runScriptLine("rotate 144 0 0 1");
	    app.runScriptLine("symadd root.cs144");
	    app.runScriptLine("synth cs cs216 root 0 0 0");
	    app.runScriptLine("select root.cs216");
	    app.runScriptLine("rotate 216 0 0 1");
	    app.runScriptLine("symadd root.cs216");
	    app.runScriptLine("synth cs cs288 root 0 0 0");
	    app.runScriptLine("select root.cs288");
	    app.runScriptLine("rotate 288 0 0 1");
	    app.runScriptLine("symadd root.cs288");
	    app.runScriptLine("syminfo");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("set bd1 root.kl.stems.stemA_1.A_13_A_1");
	    app.runScriptLine("set bd2 root.kl.stems.stemB_1.B_13_B_1");
	    app.runScriptLine("set bp 21");
	    app.runScriptLine("genhelixconstraint ${bd1} ${bd2} bp=${bp} sym1=0 sym2=1");
	    Properties optProperties = app.runScriptLine("opthelices helices=true steps=1400000 rms=50 kt=100 error=250 parent=root.kl firstfixed=false");
	    app.runScriptLine("exportpdb " + methodName + "_single.pdb junction=false");
	    app.runScriptLine("echo Attempting to generate a connector helix...");
	    app.runScriptLine("symapply ${bd1} 1");
	    app.runScriptLine("genhelix bp1=${bd1} bp2=${bd2} bp=${bp} rms=15");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("exportpdb " + methodName + "_helix.pdb junction=false");
	    app.runScriptLine("remove root.kl.stems");
	    app.runScriptLine("remove root.kl.kl_cov_kl");
	    app.runScriptLine("clone root.kl root kl60");
	    app.runScriptLine("clone root.helix_root root hr60");
	    app.runScriptLine("clone root.kl root kl120");
	    app.runScriptLine("clone root.helix_root root hr120");
	    app.runScriptLine("clone root.kl root kl180");
	    app.runScriptLine("clone root.helix_root root hr180");
	    app.runScriptLine("clone root.kl root kl240");
	    app.runScriptLine("clone root.helix_root root hr240");
	    app.runScriptLine("symapply root.kl60 1");
	    app.runScriptLine("symapply root.kl120 2");
	    app.runScriptLine("symapply root.kl180 3");
	    app.runScriptLine("symapply root.kl240 4");
	    app.runScriptLine("symapply root.hr60 1");
	    app.runScriptLine("symapply root.hr120 2");
	    app.runScriptLine("symapply root.hr180 3");
	    app.runScriptLine("symapply root.hr240 4");
	    app.runScriptLine("exportpdb " + methodName + ".pdb junction=false");

	    app.runScriptLine("exportpdb " + methodName + "_final.pdb junction=false");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
 	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


    /** tests building a pentagonal ring of out kissing loops and helices. Each "dump-bell" shaped sequence will have 10 + 3 + 3 = 16 bp */
    @Test (groups={"new", "slow"})
    public void testOptimizePentagonalHelicesBp22(){
	final String methodName = "testOptimizePentagonalHelicesBp22";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    String klFileName = "${NANOTILER_HOME}/test/fixtures/2BJ2.rnaview.pdb_k2_A-G7_B-G6.pdb";
	    app.runScriptLine("import " + klFileName + " name=kl findstems=true");
	    // app.runScriptLine("select root.kl");
	    // app.runScriptLine("unshift 66.27619041916168 -2.76757245508982 0.7947149700598815"); // move to center of gravity
	    // app.runScriptLine("randomize root.kl");
	    // app.runScriptLine("shift 100 0 0");
	    app.runScriptLine("synth cs cs72 root 0 0 0");
	    app.runScriptLine("select root.cs72");
	    app.runScriptLine("rotate 72 0 0 1");
	    app.runScriptLine("symadd root.cs72");
	    app.runScriptLine("synth cs cs144 root 0 0 0");
	    app.runScriptLine("select root.cs144");
	    app.runScriptLine("rotate 144 0 0 1");
	    app.runScriptLine("symadd root.cs144");
	    app.runScriptLine("synth cs cs216 root 0 0 0");
	    app.runScriptLine("select root.cs216");
	    app.runScriptLine("rotate 216 0 0 1");
	    app.runScriptLine("symadd root.cs216");
	    app.runScriptLine("synth cs cs288 root 0 0 0");
	    app.runScriptLine("select root.cs288");
	    app.runScriptLine("rotate 288 0 0 1");
	    app.runScriptLine("symadd root.cs288");
	    app.runScriptLine("syminfo");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("set bd1 root.kl.stems.stemA_1.A_13_A_1");
	    app.runScriptLine("set bd2 root.kl.stems.stemB_1.B_13_B_1");
	    app.runScriptLine("set bp 22");
	    app.runScriptLine("genhelixconstraint ${bd1} ${bd2} bp=${bp} sym1=0 sym2=1");
	    Properties optProperties = app.runScriptLine("opthelices helices=true steps=1400000 rms=50 kt=100 error=250 parent=root.kl firstfixed=false");
	    app.runScriptLine("exportpdb " + methodName + "_single.pdb junction=false");
	    app.runScriptLine("echo Attempting to generate a connector helix...");
	    app.runScriptLine("symapply ${bd1} 1");
	    app.runScriptLine("genhelix bp1=${bd1} bp2=${bd2} bp=${bp} rms=15");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("exportpdb " + methodName + "_helix.pdb junction=false");
	    app.runScriptLine("remove root.kl.stems");
	    app.runScriptLine("remove root.kl.kl_cov_kl");
	    app.runScriptLine("clone root.kl root kl60");
	    app.runScriptLine("clone root.helix_root root hr60");
	    app.runScriptLine("clone root.kl root kl120");
	    app.runScriptLine("clone root.helix_root root hr120");
	    app.runScriptLine("clone root.kl root kl180");
	    app.runScriptLine("clone root.helix_root root hr180");
	    app.runScriptLine("clone root.kl root kl240");
	    app.runScriptLine("clone root.helix_root root hr240");
	    app.runScriptLine("symapply root.kl60 1");
	    app.runScriptLine("symapply root.kl120 2");
	    app.runScriptLine("symapply root.kl180 3");
	    app.runScriptLine("symapply root.kl240 4");
	    app.runScriptLine("symapply root.hr60 1");
	    app.runScriptLine("symapply root.hr120 2");
	    app.runScriptLine("symapply root.hr180 3");
	    app.runScriptLine("symapply root.hr240 4");
	    app.runScriptLine("exportpdb " + methodName + ".pdb junction=false");

	    app.runScriptLine("exportpdb " + methodName + "_final.pdb junction=false");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
 	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a pentagonal ring of out kissing loops and helices. Each "dump-bell" shaped sequence will have 10 + 3 + 3 = 16 bp */
    @Test (groups={"new", "slow"})
    public void testOptimizePentagonalHelicesBp23(){
	final String methodName = "testOptimizePentagonalHelicesBp23";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    String klFileName = "${NANOTILER_HOME}/test/fixtures/2BJ2.rnaview.pdb_k2_A-G7_B-G6.pdb";
	    app.runScriptLine("import " + klFileName + " name=kl findstems=true");
	    // app.runScriptLine("select root.kl");
	    // app.runScriptLine("unshift 66.27619041916168 -2.76757245508982 0.7947149700598815"); // move to center of gravity
	    // app.runScriptLine("randomize root.kl");
	    // app.runScriptLine("shift 100 0 0");
	    app.runScriptLine("synth cs cs72 root 0 0 0");
	    app.runScriptLine("select root.cs72");
	    app.runScriptLine("rotate 72 0 0 1");
	    app.runScriptLine("symadd root.cs72");
	    app.runScriptLine("synth cs cs144 root 0 0 0");
	    app.runScriptLine("select root.cs144");
	    app.runScriptLine("rotate 144 0 0 1");
	    app.runScriptLine("symadd root.cs144");
	    app.runScriptLine("synth cs cs216 root 0 0 0");
	    app.runScriptLine("select root.cs216");
	    app.runScriptLine("rotate 216 0 0 1");
	    app.runScriptLine("symadd root.cs216");
	    app.runScriptLine("synth cs cs288 root 0 0 0");
	    app.runScriptLine("select root.cs288");
	    app.runScriptLine("rotate 288 0 0 1");
	    app.runScriptLine("symadd root.cs288");
	    app.runScriptLine("syminfo");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("set bd1 root.kl.stems.stemA_1.A_13_A_1");
	    app.runScriptLine("set bd2 root.kl.stems.stemB_1.B_13_B_1");
	    app.runScriptLine("set bp 23");
	    app.runScriptLine("genhelixconstraint ${bd1} ${bd2} bp=${bp} sym1=0 sym2=1");
	    Properties optProperties = app.runScriptLine("opthelices helices=true steps=1400000 rms=50 kt=100 error=250 parent=root.kl firstfixed=false");
	    app.runScriptLine("exportpdb " + methodName + "_single.pdb junction=false");
	    app.runScriptLine("echo Attempting to generate a connector helix...");
	    app.runScriptLine("symapply ${bd1} 1");
	    app.runScriptLine("genhelix bp1=${bd1} bp2=${bd2} bp=${bp} rms=15");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("exportpdb " + methodName + "_helix.pdb junction=false");
	    app.runScriptLine("remove root.kl.stems");
	    app.runScriptLine("remove root.kl.kl_cov_kl");
	    app.runScriptLine("clone root.kl root kl60");
	    app.runScriptLine("clone root.helix_root root hr60");
	    app.runScriptLine("clone root.kl root kl120");
	    app.runScriptLine("clone root.helix_root root hr120");
	    app.runScriptLine("clone root.kl root kl180");
	    app.runScriptLine("clone root.helix_root root hr180");
	    app.runScriptLine("clone root.kl root kl240");
	    app.runScriptLine("clone root.helix_root root hr240");
	    app.runScriptLine("symapply root.kl60 1");
	    app.runScriptLine("symapply root.kl120 2");
	    app.runScriptLine("symapply root.kl180 3");
	    app.runScriptLine("symapply root.kl240 4");
	    app.runScriptLine("symapply root.hr60 1");
	    app.runScriptLine("symapply root.hr120 2");
	    app.runScriptLine("symapply root.hr180 3");
	    app.runScriptLine("symapply root.hr240 4");
	    app.runScriptLine("exportpdb " + methodName + ".pdb junction=false");

	    app.runScriptLine("exportpdb " + methodName + "_final.pdb junction=false");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
 	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }



}
