package nanotilertests.rnadesign.designapp;

import tools3d.objects3d.*;
import generaltools.TestTools;
import java.io.PrintStream;
import java.math.BigInteger;
import java.util.List;
import java.util.ArrayList;
import java.util.Properties;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import rnadesign.rnamodel.DBElementDescriptor;
import rnadesign.rnamodel.DBElementConnectionDescriptor;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandException;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import tools3d.Vector3D;
import rnadesign.designapp.*;

import org.testng.annotations.*; // for testing

public class OptimizeRnaGraphCommandTest {

    public OptimizeRnaGraphCommandTest() { }

    /** tests building a triangular structure out of one building block */
    @Test (groups={"new", "incomplete"})
    public void testOptimizeRnaGraphCommand1(){
	String methodName = "testOptimizeRnaGraphCommand1";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/triangle.points scale=0.17 name=newgraph");
	    app.runScriptLine("tree");
	    Properties optProperties = app.runScriptLine("optrnagraph root=root.newgraph");
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of optimize RNA graph command: " + optProperties);
	    app.runScriptLine("tree");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testOptimizeRnaGraphCommand: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testOptimizeRnaGraphCommand: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a triangular structure out of one building block */
    @Test (groups={"new", "incomplete"})
    public void testOptimizeRnaGraphCommand2(){
	String methodName = "testOptimizeRnaGraphCommand2";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/triangle.points scale=2.5 name=newgraph");
	    app.runScriptLine("tree");
	    Properties optProperties = app.runScriptLine("optrnagraph root=root.newgraph");
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of optimize RNA graph command: " + optProperties);
	    app.runScriptLine("tree");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testOptimizeRnaGraphCommand2: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testOptimizeRnaGraphCommand2: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Test using small cube structure */
    @Test (groups={"new"})
    public void testOptimizeRnaGraphSmallCube(){
	String methodName = "testOptimizeRnaGraphSmallCube";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("genshape cube length=50 name=opt");
	    app.runScriptLine("tree");
	    app.runScriptLine("echo Optimizing graph edges...");
	    app.runScriptLine("optrnagraph root=root.opt steps=3000000 verbose=1 error=2.0 offset=12");
	    app.runScriptLine("tree");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testTraceRnaGraphCommand2: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

}
