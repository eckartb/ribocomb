package nanotilertests.rnadesign.designapp;

import java.util.*;
import commandtools.CommandException;
import generaltools.TestTools;
import org.testng.annotations.*;
import rnadesign.designapp.*;

/** Test class for optsequences command / OptimizeSequencesCommand class. */
public class OptimizeSequencesCommandTest {

    @Test(groups={"new"})
    public void testOptimizeSequences1() {
	String methodName = "testOptimizeSequences1";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("genhelix");
	    app.runScriptLine("tree");
	    app.runScriptLine("resproperty all adhoc");
	    app.runScriptLine("secondary");
	    app.runScriptLine("optsequences iter=30 iter2=40 error=10");
	    app.runScriptLine("tree");
	}
	catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new", "slow"})
    public void testOptimizeSequencePathway() {
	String methodName = "testOptimizeSequencePathway";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("genhelix");
	    app.runScriptLine("tree");
	    app.runScriptLine("resproperty all adhoc");
	    app.runScriptLine("secondary");
	    Properties properties = app.runScriptLine("optsequences iter=30 iter2=40 error=10");
	    app.runScriptLine("optsequences iter=3000 iter2=40 error=2000");
	    app.runScriptLine("tree");
	    assert ( properties.getProperty("score") != null ) && (Double.parseDouble(properties.getProperty("score")) < 5.0);
	}
	catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new"})
    public void testOptimizeBistableSequences1() {
	String methodName = "testOptimizeBistableSequences1";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("genhelix");
	    app.runScriptLine("tree");
	    app.runScriptLine("resproperty all adhoc");
	    app.runScriptLine("secondary");
	    app.runScriptLine("optsequences iter=30 iter2=40 switch=true"); // alt=${NANOTILER_HOME}/test/fixtures/bistabletest.sec");
	    app.runScriptLine("tree");
	}
	catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new", "outdated"})
    public void testOptimizeBistableSequences2() {
	String methodName = "testOptimizeBistableSequences2";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("genhelix");
	    app.runScriptLine("tree");
	    app.runScriptLine("resproperty all adhoc");
	    app.runScriptLine("secondary");
	    app.runScriptLine("optsequences iter=30 iter2=40 alt=${NANOTILER_HOME}/test/fixtures/bistabletest2.sec");
	    app.runScriptLine("tree");
	}
	catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


}
