package nanotilertests.rnadesign.designapp;

import commandtools.CommandException;
import commandtools.CommandExecutionException;
import generaltools.TestTools;
import rnadesign.designapp.*;
import org.testng.annotations.*; // for testing

/** Test class for orient command */
public class OrientCommandTest {

    public OrientCommandTest() { }

    /** Reads imperfect, generates helix constraints corresponding to perfectly symmetric ring */
    public void testOrientCommand(String fileName, String outputFileNameBase){
	String methodName = "testOrientCommand";
	System.out.println("Testing automatic principal axis orientation for input output files: " + fileName + " " + outputFileNameBase);
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    // hard test: RNAview does not recognize one three-way junction:
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/2J00.rnaview.pdb_j3_A-G1064_A-C1107_A-G1187_grow_ring_rnaview.pdb");
	    // again, junctions are not properly recognized
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1S1Icleanfusedreidede25i25i225_renumbered.pdb");
	    // make sure stems of length 2 can be read:
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/2I2T.pdb_j2_B-C274_B-G361_chain_ring_1221_L7_rnaview.pdb");
	    app.runScriptLine("import " + fileName + " findstems=false addjunctions=false format=pdb"); // do not use rnaview format, structures too large
	    // app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("echo Performing normalization of orientation for file: " + fileName);
	    app.runScriptLine("orient name=root.import");
	    // app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("exportpdb " + outputFileNameBase + "_xy.pdb");
	    app.runScriptLine("select root");
	    app.runScriptLine("echo Rotating 90 degree around x axis!");
	    app.runScriptLine("rotate 90 1 0 0"); 
	    app.runScriptLine("exportpdb " + outputFileNameBase + "_xz.pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	} catch(CommandException e) {
	    System.out.println("Command exception in orient command test: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Reads imperfect, generates helix constraints corresponding to perfectly symmetric ring */
    @Test (groups={"new", "slow"})
    public void testOrientCommand(){
	String methodName = "testOrientCommand";
	System.out.println(TestTools.generateMethodHeader(methodName));
	String[] fileNames = {
	    "${NANOTILER_HOME}/test/fixtures/1NKW.pdb_j2_0-A1043_0-C1132_chain_ring_1221_L5.pdb",
	    "${NANOTILER_HOME}/test/fixtures/1S1Icleanfusedreidede25i25i225_renumbered.pdb",
	    "${NANOTILER_HOME}/test/fixtures/2J01.pdb_j2_A-U1798_A-U1818_chain_ring_1221_L7.pdb",
	    "${NANOTILER_HOME}/test/fixtures/1CQ5.pdb_j2_A-U6_A-A36_chain_ring.1Y0Q.pdb_k2_A-C146_A-A225_1122_L1010.pdb"
	};
	for (int i = 0; i < fileNames.length; ++i) {
	    String outputFileNameBase = "orientCommandTest_" + (i+1);
	    testOrientCommand(fileNames[i], outputFileNameBase);
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

}
