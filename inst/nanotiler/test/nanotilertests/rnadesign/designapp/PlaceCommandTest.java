package nanotilertests.rnadesign.designapp;

import commandtools.*;
import generaltools.*;
import org.testng.*;
import org.testng.annotations.*;
import rnadesign.designapp.*;

/** Tests "place" command */
public class PlaceCommandTest {

    /** reads a database of junctions (containing one three-way junction) and places that junction */
    @Test (groups={"new"})
    public void testPlaceCommand(){
	final String methodName = "testPlaceCommand";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    //	    String jncFileName = "${NANOTILER_HOME}/test/fixtures/2J00.rnaview.pdb_j3_A-G1064_A-C1107_A-G1187_rnaview.pdb";
	    String namesfileName = "${NANOTILER_HOME}/test/fixtures/triangle.names";
	    app.runScriptLine("loadjunctions " + namesfileName + " ${NANOTILER_HOME}/test/fixtures");
	    app.runScriptLine("status");
	    app.runScriptLine("place j root j1 3 1 0 0 0 _1");
	    // app.runScriptLine("tree forbidden=Atom3D");
	    assert app.getGraphController().getGraph().findByFullName("root.j1") != null;
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }
}
