package nanotilertests.rnadesign.designapp;

import tools3d.objects3d.*;
import generaltools.TestTools;
import java.io.File;
import java.io.PrintStream;
import java.util.List;
import java.util.ArrayList;
import java.util.Properties;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import rnadesign.rnamodel.DBElementDescriptor;
import rnadesign.rnamodel.DBElementConnectionDescriptor;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandException;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import tools3d.Vector3D;
import rnadesign.designapp.*;
import org.testng.annotations.*; // for testing

public class RGrowCommandTest {

     public RGrowCommandTest() { }

    /** tests building a triangular structure out of one building block */
    @Test (groups={"newest_later"})
    public void testRGrowCommand1(){
	String methodName = "testRGrowCommand1";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/triangle.names ${NANOTILER_HOME}/test/fixtures 2");
	    app.runScriptLine("status");
	    Properties growProperties = app.runScriptLine("rgrow blocks=j,3,1 connect=1,1,(hxend)1,(hxend)2,6 gen=3 helices=true steric=true ring-export-limit=5"
							  + " ring-export=" + methodName + "_1");
	    // assert "1".equals(growProperties.getProperty("ringless_collisions"));
	    System.out.println("Result of rgrow command(1): " + growProperties);
	    app.runScriptLine("tree strands");
	    app.runScriptLine("exportpdb " + methodName + "_exp_1.pdb junction=true");
	    assert "0".equals(growProperties.getProperty("ring_count"));
	    app.runScriptLine("status");
	    app.runScriptLine("clear all");
	    Properties growProperties2 = app.runScriptLine("rgrow blocks=j,3,1 connect=1,1,(hxend)1,(hxend)2,6 gen=3 helices=true steric=true ring-export-limit=20"
							   + " ring-export=" + methodName + "_2");
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of rgrow command(2): " + growProperties2);
	    assert "1".equals(growProperties2.getProperty("ring_count"));
	    app.runScriptLine("clear all");
	    app.runScriptLine("echo rgrow command using 3 explicit building blocks...");
	    Properties growProperties3 = app.runScriptLine("rgrow blocks=j,3,1;j,3,1;j,3,1 connect=1,2,(hxend)1,(hxend)2,6;2,3,(hxend)1,(hxend)2,6;3,1,(hxend)1,(hxend)2,6 gen=3 helices=true steric=true ring-export-limit=20" + " ring-export=" + methodName + "_3");
	    System.out.println("Result of rgrow command(3): " + growProperties3);
	    // assert "0".equals(growProperties3.getProperty("ringless_collisions"));
	    assert "1".equals(growProperties3.getProperty("ring_count"));
	    System.out.println("Rgrow command successful! Tree residues: ");
	    System.out.println("tree residues");
	    String exportname = methodName + "_exp_3.pdb";
	    System.out.println("Exporting ring structure to: " + exportname);
	    app.runScriptLine("exportpdb " + exportname + " junction=true");
	    System.out.println("Rgrow command successful! Signature: ");
	    app.runScriptLine("signature root");
	    // make sure there are not collisions:
	    assert "0".equals(growProperties3.getProperty("collision_count"))
;	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testRGrowCommand: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testRGrowCommand: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test (groups={"new"})
    public void testRGrowCommandRand(){
	String methodName = "testRGrowCommandRand";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/triangle.names ${NANOTILER_HOME}/test/fixtures 2");
	    app.runScriptLine("status");
	    Properties growProperties = app.runScriptLine("rgrow blocks=j,3,1 connect=1,1,(hxend)1,(hxend)2,6 gen=2 helices=true steric=true ring-export-limit=5 rand=true"
							  + " ring-export=" + methodName + "_1");
	    // assert "1".equals(growProperties.getProperty("ringless_collisions"));
	    System.out.println("Result of rgrow command(1): " + growProperties);
	    app.runScriptLine("tree strands");
	    app.runScriptLine("exportpdb " + methodName + "_exp_1.pdb junction=true");
	    app.runScriptLine("status");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testRGrowCommand: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testRGrowCommand: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Creates random construct consisting of the 11nt motif */
    @Test (groups={"new"})
    public void testRGrowCommandRand2(){
	String methodName = "testRGrowCommandRand2";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    System.out.println("Issuing command: loadjunctions ${NANOTILER_HOME}/test/fixtures/nt11motif.names ${NANOTILER_HOME}/test/fixtures 2 mode=g");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/nt11motif.names ${NANOTILER_HOME}/test/fixtures 2 mode=g");
	    app.runScriptLine("status");
	    Properties growProperties = app.runScriptLine("rgrow blocks=m,2,1 gen=2 helices=true steric=true ring-export-limit=5 rand=true"
							  + " ring-export=" + methodName + "_1");
	    // assert "1".equals(growProperties.getProperty("ringless_collisions"));
	    System.out.println("Result of rgrow command(1): " + growProperties);
	    app.runScriptLine("tree strands");
	    app.runScriptLine("exportpdb " + methodName + "_exp_1.pdb junction=true");
	    app.runScriptLine("status");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testRGrowCommand: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testRGrowCommand: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Creates random construct consisting of 11nt motif and the right-angle motif */
    @Test (groups={"new"})
    public void testRGrowCommandRand3(){
	String methodName = "testRGrowCommandRand3";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/rant11motifs.names ${NANOTILER_HOME}/test/fixtures 2 mode=g");
	    app.runScriptLine("status");
	    Properties growProperties = app.runScriptLine("rgrow blocks=m,2,1;m,3,1 gen=2 helices=true steric=true ring-export-limit=5 rand=true"
							  + " ring-export=" + methodName + "_1");
	    // assert "1".equals(growProperties.getProperty("ringless_collisions"));
	    System.out.println("Result of rgrow command(1): " + growProperties);
	    app.runScriptLine("tree strands");
	    app.runScriptLine("exportpdb " + methodName + "_exp_1.pdb junction=true");
	    app.runScriptLine("status");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testRGrowCommand: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testRGrowCommand: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


    /** Creates random construct consisting of 11nt motif and the right-angle motif */
    @Test (groups={"new"})
    public void testRGrowCommandNewDir(){
	String methodName = "testRGrowCommandRand3";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/rant11motifs.names ${NANOTILER_HOME}/test/fixtures 2 mode=g");
	    app.runScriptLine("status");
	    String path = "growCommandTestDir";
	    // detect path here
	    String ringExportName = path + "/" + "ring_export_default"; // ringExportName;
	     
	    Properties growProperties = app.runScriptLine("rgrow blocks=m,2,1;m,3,1 gen=2 helices=true steric=true ring-export-limit=5 rand=true"
							  + " ring-export=" + ringExportName + "_1");
	    // assert "1".equals(growProperties.getProperty("ringless_collisions"));
	    System.out.println("Result of rgrow command(1): " + growProperties);
	    app.runScriptLine("tree strands");
	    app.runScriptLine("exportpdb " + methodName + "_exp_1.pdb junction=true");
	    app.runScriptLine("status");
	    File dir = new File(path);
	    assert dir.exists(); // test that directory was created
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testRGrowCommand: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testRGrowCommand: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


    /** tests building a triangular structure out of one building block */
    @Test (groups={"new"})
    public void testRGrowCommand4Way(){
	String methodName = "testRGrowCommand4Way";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/fourway.names ${NANOTILER_HOME}/test/fixtures 2");
	    app.runScriptLine("status");
	    Properties growProperties = app.runScriptLine("rgrow blocks=j,2,1 connect=1,1,(hxend)1,(hxend)2,6 gen=2 helices=true steric=true ring-export-limit=5"
							  + " ring-export=" + methodName + "_1");
	    // assert "1".equals(growProperties.getProperty("ringless_collisions"));
	    System.out.println("Result of rgrow command(1): " + growProperties);
	    app.runScriptLine("tree strands");
	    app.runScriptLine("exportpdb " + methodName + "_exp_1.pdb junction=true");
	    app.runScriptLine("history file=" + methodName + "_initial.tmp.script");
	    assert "0".equals(growProperties.getProperty("ring_count"));
	    app.runScriptLine("status");
	    app.runScriptLine("clear all");
	    Properties growProperties2 = app.runScriptLine("rgrow blocks=j,2,1 connect=1,1,(hxend)1,(hxend)2,6 gen=2 helices=true steric=true ring-export-limit=20"
							   + " ring-export=" + methodName + "_2");
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of rgrow command(2): " + growProperties2);
	    assert "1".equals(growProperties2.getProperty("ring_count"));
	    System.out.println("Rgrow command successful! Tree residues: ");
	    System.out.println("tree residues");
	    String exportname = methodName + "_exp_2.pdb";
	    System.out.println("Exporting ring structure to: " + exportname);
	    app.runScriptLine("exportpdb " + exportname + " junction=true");
	    System.out.println("Rgrow command successful! Signature: ");
	    app.runScriptLine("signature root");
	    // make sure there are not collisions:
	    assert "0".equals(growProperties2.getProperty("collision_count"));
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testRGrowCommand: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testRGrowCommand: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a triangular structure out of one building block using randomized connectivity mode */
    @Test (groups={"new"})
    public void testRGrowCommand4WayRand(){
	String methodName = "testRGrowCommand4WayRand";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/fourway.names ${NANOTILER_HOME}/test/fixtures 2");
	    app.runScriptLine("status");
	    Properties growProperties = app.runScriptLine("rgrow blocks=j,2,1 connect=1,1,(hxend)1,(hxend)2,6 gen=2 helices=true steric=true ring-export-limit=5"
							  + " ring-export=" + methodName + "_1");
	    // assert "1".equals(growProperties.getProperty("ringless_collisions"));
	    System.out.println("Result of rgrow command(1): " + growProperties);
	    app.runScriptLine("tree strands");
	    app.runScriptLine("exportpdb " + methodName + "_exp_1.pdb junction=true");
	    app.runScriptLine("history file=" + methodName + "_initial.tmp.script");
	    assert "0".equals(growProperties.getProperty("ring_count"));
	    app.runScriptLine("status");
	    app.runScriptLine("clear all");
	    Properties growProperties2 = app.runScriptLine("rgrow blocks=j,2,1 connect=1,1,(hxend)1,(hxend)2,6 gen=2 helices=true steric=true ring-export-limit=20"
							   + " ring-export=" + methodName + "_2");
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of rgrow command(2): " + growProperties2);
	    assert "1".equals(growProperties2.getProperty("ring_count"));
	    System.out.println("Rgrow command successful! Tree residues: ");
	    System.out.println("tree residues");
	    String exportname = methodName + "_exp_2.pdb";
	    System.out.println("Exporting ring structure to: " + exportname);
	    app.runScriptLine("exportpdb " + exportname + " junction=true");
	    System.out.println("Rgrow command successful! Signature: ");
	    app.runScriptLine("signature root");
	    // make sure there are not collisions:
	    assert "0".equals(growProperties2.getProperty("collision_count"));
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testRGrowCommand: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testRGrowCommand: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }



    Object3DSet getJunctions(NanoTilerScripter app) {
	return Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), "StrandJunction3D");
    }

    Object3DSet getKissingLoops(NanoTilerScripter app) {
	return Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), "KissingLoop3D");
    }

    Object3DSet getJunctionsAndKissingLoops(NanoTilerScripter app) {
	Object3DSet result = getJunctions(app);
	result.merge(getKissingLoops(app));
	return result;
    }

    /** Prints all StrandJunction3D objects placed in controller */
    private void printJunctions(PrintStream ps, NanoTilerScripter app) {
	Object3DSet junctions = getJunctions(app);
	ps.println("Placed junctions: " + junctions.size());
	for (int i = 0; i < junctions.size(); ++i) {
	    ps.println(junctions.get(i).getFullName() + " " + junctions.get(i).getProperties());
	}
    }

    /** tests building a triangular structure out of one building block (using 3 non-equivalent copies, graph mode switched off ) */
    @Test (groups={"new"})
    public void testRGrowCommandAsGraph1(){
	String methodName = "testRGrowCommandAsGraph1";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/triangle.names ${NANOTILER_HOME}/test/fixtures 2");
	    app.runScriptLine("status");
	    Properties growProperties1 = app.runScriptLine("rgrow blocks=j,3,1;j,3,1;j,3,1 connect=1,2,(hxend)1,(hxend)2,6;2,3,(hxend)1,(hxend)2,6;3,1,(hxend)1,(hxend)2,6 gen=3 helices=true steric=true ring-export-limit=20 graph=false" + " ring-export=" + methodName + "_1");
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of rgrow command: " + growProperties1);
	    String exportname = methodName + ".pdb";
	    app.runScriptLine("exportpdb " + exportname + " junction=true");
	    assert "1".equals(growProperties1.getProperty("ring_count"));
	    app.runScriptLine("tree strands");
	    printJunctions(System.out, app);
	    assert getJunctions(app).size() == 3;
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testRGrowCommandAsGraph: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testRGrowCommandAsGraph: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }
	    

    /** tests building a triangular structure out of one building block */
    @Test (groups={"new"})
    public void testRGrowCommandAsGraph2(){
	String methodName = "testRGrowCommandAsGraph2";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/triangle.names ${NANOTILER_HOME}/test/fixtures 2");
	    app.runScriptLine("status");
	    Properties growProperties2 = app.runScriptLine("rgrow blocks=j,3,1;j,3,1;j,3,1 connect=1,2,(hxend)1,(hxend)2,9;2,3,(hxend)1,(hxend)2,9 gen=3 helices=true steric=true ring-export-limit=20 graph=true ring-export=" + methodName + "_1");
	    System.out.println("Rgrow command successful! Tree strands: ");
	    app.runScriptLine("tree strands");
	    String exportname = methodName + ".pdb";
	    System.out.println("Exporting structure to: " + exportname);
	    app.runScriptLine("exportpdb " + exportname + " junction=true");
	    System.out.println("Rgrow command successful! Signature: ");
	    app.runScriptLine("signature root");
	    printJunctions(System.out, app);
// 	    Object3DSet junctions = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), "StrandJunction3D");
// 	    System.out.println("Placed junctions: " + junctions.size());
// 	    for (int i = 0; i < junctions.size(); ++i) {
// 		System.out.println(junctions.get(i).getFullName() + " " + junctions.get(i).getProperties());
// 	    }
	    System.out.println("Properties generated: " + growProperties2);
	    assert "0".equals(growProperties2.getProperty("ring_count"));
	    assert getJunctions(app).size() == 3;
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testRGrowCommandAsGraph: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testRGrowCommandAsGraph: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a triangular structure out of one building block using non-ring-closing parameters */
    @Test (groups={"new"})
    public void testRGrowCommandAsGraph2b(){
	String methodName = "testRGrowCommandAsGraph2b";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/triangle.names ${NANOTILER_HOME}/test/fixtures 2");
	    app.runScriptLine("status");
	    Properties growProperties2 = app.runScriptLine("rgrow blocks=j,3,1 connect=1,1,(hxend)1,(hxend)2,9 gen=2 helices=true steric=true ring-export-limit=20 graph=false ring-export=" + methodName + "_1");
	    System.out.println("Rgrow command successful! Tree strands: ");
	    app.runScriptLine("tree strands");
	    String exportname = methodName + ".pdb";
 	    System.out.println("Exporting structure to: " + exportname);
 	    app.runScriptLine("exportpdb " + exportname + " junction=true");
	    System.out.println("Rgrow command successful! Signature: ");
	    app.runScriptLine("signature root");
	    printJunctions(System.out, app);
// 	    Object3DSet junctions = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), "StrandJunction3D");
// 	    System.out.println("Placed junctions: " + junctions.size());
// 	    for (int i = 0; i < junctions.size(); ++i) {
// 		System.out.println(junctions.get(i).getFullName() + " " + junctions.get(i).getProperties());
// 	    }
	    System.out.println("Properties generated: " + growProperties2);
	    assert "0".equals(growProperties2.getProperty("ring_count"));
	    assert getJunctions(app).size() == 5;
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testRGrowCommandAsGraph: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testRGrowCommandAsGraph: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a triangular structure out of one building block using non-ring-closing parameters */
    @Test (groups={"new"})
    public void testRGrowCommandAsGraph2b8(){
	String methodName = "testRGrowCommandAsGraph2b8";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/triangle.names ${NANOTILER_HOME}/test/fixtures 2");
	    app.runScriptLine("status");
	    Properties growProperties2 = app.runScriptLine("rgrow blocks=j,3,1 connect=1,1,(hxend)1,(hxend)2,8 gen=2 helices=true steric=true ring-export-limit=20 graph=false ring-export=" + methodName + "_1");
	    System.out.println("Rgrow command successful! Tree strands: ");
	    app.runScriptLine("tree strands");
	    String exportname = methodName + ".pdb";
 	    System.out.println("Exporting structure to: " + exportname);
 	    app.runScriptLine("exportpdb " + exportname + " junction=true");
	    System.out.println("Rgrow command successful! Signature: ");
	    app.runScriptLine("signature root");
	    printJunctions(System.out, app);
// 	    Object3DSet junctions = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), "StrandJunction3D");
// 	    System.out.println("Placed junctions: " + junctions.size());
// 	    for (int i = 0; i < junctions.size(); ++i) {
// 		System.out.println(junctions.get(i).getFullName() + " " + junctions.get(i).getProperties());
// 	    }
	    System.out.println("Properties generated: " + growProperties2);
	    assert "0".equals(growProperties2.getProperty("ring_count"));
	    assert getJunctions(app).size() == 5;
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testRGrowCommandAsGraph: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testRGrowCommandAsGraph: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a triangular structure out of one building block using non-ring-closing parameters */
    @Test (groups={"new"})
    public void testRGrowCommandAsGraph2b7(){
	String methodName = "testRGrowCommandAsGraph2b7";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/triangle.names ${NANOTILER_HOME}/test/fixtures 2");
	    app.runScriptLine("status");
	    Properties growProperties2 = app.runScriptLine("rgrow blocks=j,3,1 connect=1,1,(hxend)1,(hxend)2,7 gen=2 helices=true steric=true ring-export-limit=20 graph=false ring-export=" + methodName + "_1");
	    System.out.println("Rgrow command successful! Tree strands: ");
	    app.runScriptLine("tree strands");
	    String exportname = methodName + ".pdb";
 	    System.out.println("Exporting structure to: " + exportname);
 	    app.runScriptLine("exportpdb " + exportname + " junction=true");
	    System.out.println("Rgrow command successful! Signature: ");
	    app.runScriptLine("signature root");
	    printJunctions(System.out, app);
// 	    Object3DSet junctions = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), "StrandJunction3D");
// 	    System.out.println("Placed junctions: " + junctions.size());
// 	    for (int i = 0; i < junctions.size(); ++i) {
// 		System.out.println(junctions.get(i).getFullName() + " " + junctions.get(i).getProperties());
// 	    }
	    System.out.println("Properties generated: " + growProperties2);
	    assert "0".equals(growProperties2.getProperty("ring_count"));
	    assert getJunctions(app).size() == 5;
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testRGrowCommandAsGraph: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testRGrowCommandAsGraph: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a triangular structure out of one building block */
    @Test (groups={"new"})
    public void testRGrowCommandAsGraph2c(){
	String methodName = "testRGrowCommandAsGraph2c";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/triangle.names ${NANOTILER_HOME}/test/fixtures 2");
	    app.runScriptLine("status");
	    Properties growProperties2 = app.runScriptLine("rgrow blocks=j,3,1;j,3,1 connect=1,2,(hxend)1,(hxend)2,9;2,1,(hxend)1,(hxend)2,9 gen=2 helices=true steric=true ring-export-limit=20 graph=false ring-export=" + methodName + "_1");
	    System.out.println("Rgrow command successful! Tree strands: ");
	    app.runScriptLine("tree strands");
// 	    String exportname = methodName + ".pdb";
// 	    System.out.println("Exporting structure to: " + exportname);
// 	    app.runScriptLine("exportpdb " + exportname + " junction=true");
	    System.out.println("Rgrow command successful! Signature: ");
	    app.runScriptLine("signature root");
	    printJunctions(System.out, app);
// 	    Object3DSet junctions = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), "StrandJunction3D");
// 	    System.out.println("Placed junctions: " + junctions.size());
// 	    for (int i = 0; i < junctions.size(); ++i) {
// 		System.out.println(junctions.get(i).getFullName() + " " + junctions.get(i).getProperties());
// 	    }
	    System.out.println("Properties generated: " + growProperties2);
	    assert "0".equals(growProperties2.getProperty("ring_count"));
	    assert getJunctions(app).size() == 5;
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testRGrowCommandAsGraph: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testRGrowCommandAsGraph: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a triangular structure out of one building block */
    @Test (groups={"new"})
    public void testRGrowCommandAsGraph2e(){
	String methodName = "testRGrowCommandAsGraph2e";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/triangle.names ${NANOTILER_HOME}/test/fixtures 2");
	    app.runScriptLine("status");
	    Properties growProperties2 = app.runScriptLine("rgrow blocks=j,3,1;j,3,1;j,3,1 connect=1,2,(hxend)1,(hxend)2,6;1,3,(hxend)2,(hxend)1,6;2,3,(hxend)1,(hxend)2,6 gen=2 helices=true steric=true ring-export-limit=20 graph=false ring-export=" + methodName + "_1");
	    System.out.println("Rgrow command successful! Tree strands: ");
	    app.runScriptLine("tree strands");
// 	    String exportname = methodName + ".pdb";
// 	    System.out.println("Exporting structure to: " + exportname);
// 	    app.runScriptLine("exportpdb " + exportname + " junction=true");
	    System.out.println("Rgrow command successful! Signature: ");
	    app.runScriptLine("signature root");
	    printJunctions(System.out, app);
// 	    Object3DSet junctions = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), "StrandJunction3D");
// 	    System.out.println("Placed junctions: " + junctions.size());
// 	    for (int i = 0; i < junctions.size(); ++i) {
// 		System.out.println(junctions.get(i).getFullName() + " " + junctions.get(i).getProperties());
// 	    }
	    System.out.println("Properties generated: " + growProperties2);
	    assert "1".equals(growProperties2.getProperty("ring_count"));
	    assert getJunctions(app).size() == 3;
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testRGrowCommandAsGraph: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testRGrowCommandAsGraph: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a triangular structure out of one building block */
    @Test (groups={"new"})
    public void testRGrowCommandAsGraph2f(){
	String methodName = "testRGrowCommandAsGraph2f";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/triangle.names ${NANOTILER_HOME}/test/fixtures 2");
	    app.runScriptLine("status");
	    Properties growProperties2 = app.runScriptLine("rgrow blocks=j,3,1;j,3,1;j,3,1 connect=1,2,(hxend)1,(hxend)2,6;1,3,(hxend)2,(hxend)1,6;2,3,(hxend)1,(hxend)2,6 gen=2 helices=true steric=true ring-export-limit=20 graph=true stem-rms=10 ring-export=" + methodName + "_1");
	    System.out.println("Rgrow command successful! Tree strands: ");
	    app.runScriptLine("tree strands");
 	    String exportname = methodName + ".pdb";
 	    System.out.println("Exporting structure to: " + exportname);
 	    app.runScriptLine("exportpdb " + exportname + " junction=true");
	    System.out.println("Rgrow command successful! Signature: ");
	    app.runScriptLine("signature root");
	    printJunctions(System.out, app);
// 	    Object3DSet junctions = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), "StrandJunction3D");
// 	    System.out.println("Placed junctions: " + junctions.size());
// 	    for (int i = 0; i < junctions.size(); ++i) {
// 		System.out.println(junctions.get(i).getFullName() + " " + junctions.get(i).getProperties());
// 	    }
	    System.out.println("Properties generated: " + growProperties2);
	    assert "0".equals(growProperties2.getProperty("ring_count"));
	    assert getJunctions(app).size() == 3;
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testRGrowCommandAsGraph: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testRGrowCommandAsGraph: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a triangular structure out of one building block (4 copies, provoking a collising on graph mode) */
    /*
    @Test (groups={"new"})
    public void testRGrowCommandAsGraph2g(){
	String methodName = "testRGrowCommandAsGraph2g";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/triangle.names ${NANOTILER_HOME}/test/fixtures 2");
	    app.runScriptLine("status");
	    Properties growProperties2 = app.runScriptLine("rgrow blocks=j,3,1;j,3,1;j,3,1;j,3,1 connect=1,2,(hxend)1,(hxend)2,6;2,3,(hxend)1,(hxend)2,6;3,4,(hxend)1,(hxend)2,6 gen=2 helices=true steric=true ring-export-limit=20 graph=true stem-rms=10 ring-export=" + methodName + "_1");
	    System.out.println("Rgrow command successful! Tree strands: ");
	    app.runScriptLine("tree strands");
 	    String exportname = methodName + ".pdb";
 	    System.out.println("Exporting structure to: " + exportname);
 	    app.runScriptLine("exportpdb " + exportname + " junction=true");
	    System.out.println("Rgrow command successful! Signature: ");
	    app.runScriptLine("signature root");
	    printJunctions(System.out, app);
// 	    Object3DSet junctions = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), "StrandJunction3D");
// 	    System.out.println("Placed junctions: " + junctions.size());
// 	    for (int i = 0; i < junctions.size(); ++i) {
// 		System.out.println(junctions.get(i).getFullName() + " " + junctions.get(i).getProperties());
// 	    }
	    System.out.println("Properties generated: " + growProperties2);
	    assert "0".equals(growProperties2.getProperty("ring_count"));
	    printJunctions(System.out, app);
	    assert getJunctions(app).size() == 4; // this currently does not work, TODO
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testRGrowCommandAsGraph: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testRGrowCommandAsGraph: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }
    */

    /** tests building a triangular structure out of one building block (3 copies, this version should NOT close a ring */
    @Test (groups={"new"})
    public void testRGrowCommandAsGraph2h(){
	String methodName = "testRGrowCommandAsGraph2h";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/triangle.names ${NANOTILER_HOME}/test/fixtures 2");
	    app.runScriptLine("status");
	    Properties growProperties2 = app.runScriptLine("rgrow blocks=j,3,1;j,3,1;j,3,1 connect=1,2,(hxend)1,(hxend)1,36;2,3,(hxend)2,(hxend)1,34;1,3,(hxend)2,(hxend)2,33 gen=4 graph=true helices=true steric=true ring-export-limit=20 graph=true stem-rms=10 ring-export=" + methodName + "_1");
	    System.out.println("Rgrow command successful! Tree strands: ");
	    app.runScriptLine("tree strands");
 	    String exportname = methodName + ".pdb";
 	    System.out.println("Exporting structure to: " + exportname);
 	    app.runScriptLine("exportpdb " + exportname + " junction=true");
	    System.out.println("Rgrow command successful! Signature: ");
	    app.runScriptLine("signature root");
	    printJunctions(System.out, app);
// 	    Object3DSet junctions = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), "StrandJunction3D");
// 	    System.out.println("Placed junctions: " + junctions.size());
// 	    for (int i = 0; i < junctions.size(); ++i) {
// 		System.out.println(junctions.get(i).getFullName() + " " + junctions.get(i).getProperties());
// 	    }
	    System.out.println("Properties generated: " + growProperties2);
	    assert "0".equals(growProperties2.getProperty("ring_count"));
	    assert getJunctions(app).size() == 3;
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Expected CommandExecutionException in testRGrowCommandAsGraph2h: " + e.getMessage());
	}
	catch(CommandException e) {
	    System.out.println("Expected CommandException in testRGrowCommandAsGraph2h: " + e.getMessage());
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a triangular structure out of one building block, giving a topology.
     * @deprecated topologies option not supported anymore, use graph=true mode */
    /*
    @Test (groups={"new"})
    public void testRGrowCommandWithTopologies(){
	System.out.println(TestTools.generateMethodHeader("testRGrowCommandWithTopologies"));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/triangle.names ${NANOTILER_HOME}/test/fixtures 2");
	    app.runScriptLine("status");
	    app.runScriptLine("echo rgrow command using 3 explicit building blocks...");
	    Properties growProperties3 = app.runScriptLine("rgrow blocks=j,3,1;j,3,1;j,3,1 connect=1,2,(hxend)1,(hxend)2,6;2,3,(hxend)1,(hxend)2,6;3,1,(hxend)1,(hxend)2,6 gen=3 helices=true steric=true ring-export-limit=20 topologies=baddummy");
	    // assert "0".equals(growProperties3.getProperty("ringless_collisions"));
	    assert "1".equals(growProperties3.getProperty("ring_count"));
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testRGrowCommand: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testRGrowCommand: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter("testRGrowCommand"));
    }
    */

    /** tests building a dimer structure, clash check switched off */
    /*
    @Test (groups={"new"})
    public void testGrowDimer1(){
	String methodName = "testGrowDimer1";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
		app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
		app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/dimer.names ${NANOTILER_HOME}/test/fixtures");
		app.runScriptLine("status");
	    for (int len = 3; len <= 3; ++len) {
		System.out.println("# Working on length " + len);
		app.runScriptLine("echo rgrow command using 1 building block:");
		app.runScriptLine("clear all");
		Properties growProperties = app.runScriptLine("rgrow blocks=j,2,1 connect=1,1,(hxend)1,(hxend)2," + len + " gen=2 helices=true steric=false ring-export-limit=15 steric=false ring-export=" + methodName + "_1");
		if ("1".equals(growProperties.getProperty("ring_count"))) {
		    System.out.println("# * Ring found as expected! *");
		}
		else {
		    assert false; // there should be a ring
		}
		System.out.println(growProperties);
		assert "0".equals(growProperties.getProperty("ringless_collisions"));
		app.runScriptLine("tree strands");
		printJunctions(System.out, app);
		// app.runScriptLine("exportpdb " + methodName + ".pdb");
		Object3DSet junctions = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), "StrandJunction3D");
		assert getJunctions(app).size() == 2; // nothing should be placed because of collisions
	    }
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testRGrowCommand: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testRGrowCommand: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }
    */

    /** tests building a dimer structure, this time steric clash check on */
    /*
    @Test (groups={"new"})
    public void testGrowDimer2(){
	String methodName = "testGrowDimer2";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
		app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
		app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/dimer.names ${NANOTILER_HOME}/test/fixtures");
		app.runScriptLine("status");
	    for (int len = 3; len <= 3; ++len) {
		System.out.println("***************** Working on length " + len + " *******************");
		app.runScriptLine("echo rgrow command using 1 building block:");
		app.runScriptLine("clear all");
		Properties growProperties = app.runScriptLine("rgrow blocks=j,2,1 connect=1,1,(hxend)1,(hxend)2," + len + " gen=2 helices=true steric=true ring-export-limit=15 steric=false ring-export=" + methodName + "_1");
		if ("1".equals(growProperties.getProperty("ring_count"))) {
		    System.out.println("**************** UNEXPECTED RING FOUND!!!!!!!!!!!!!!!!! *****************");
		    assert false;
		}
		else {
		    System.out.println("No ring formed, probably due to successful collision check.");
		}
		System.out.println(growProperties);
		assert "0".equals(growProperties.getProperty("ringless_collisions"));
		app.runScriptLine("tree strands");
		printJunctions(System.out, app);
		app.runScriptLine("exportpdb " + methodName + ".pdb");
		assert ! "0".equals(growProperties.getProperty("collision_count"));
		// assert getJunctions(app).size() == 1; // nothing except first junction should be placed because of collisions
	    }
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testRGrowCommand: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testRGrowCommand: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }
    */
    /** tests building a dimer structure, clash check switched off */
    /*
    @Test (groups={"new"})
    public void testGrowDimer3(){
	String methodName = "testGrowDimer3";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
		app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
		app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/dimer.names ${NANOTILER_HOME}/test/fixtures");
		app.runScriptLine("status");
	    for (int len = 5; len <= 5; ++len) {
		System.out.println("* Working on length " + len + " *");
		app.runScriptLine("echo rgrow command using 1 building block:");
		app.runScriptLine("clear all");
		Properties growProperties = app.runScriptLine("rgrow blocks=j,2,2 connect=1,1,(hxend)1,(hxend)2," + len + " gen=2 helices=true steric=false ring-export-limit=15 steric=false ring-export=" + methodName + "_1");
		if ("1".equals(growProperties.getProperty("ring_count"))) {
		    System.out.println("* EXPTECTED RING FOUND! *");
		}
		else {
		    assert false; // there should be a ring
		}
		System.out.println(growProperties);
		assert "0".equals(growProperties.getProperty("ringless_collisions"));
		app.runScriptLine("tree strands");
		printJunctions(System.out, app);
		// app.runScriptLine("exportpdb " + methodName + ".pdb");
		Object3DSet junctions = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), "StrandJunction3D");
		assert getJunctions(app).size() == 2; // nothing should be placed because of collisions
	    }
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testRGrowCommand: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testRGrowCommand: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }
    */

    /** tests building a dimer structure, this time steric clash check on */
    /*
    @Test (groups={"new"})
    public void testGrowDimer4(){
	String methodName = "testGrowDimer4";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
		app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
		app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/dimer.names ${NANOTILER_HOME}/test/fixtures");
		app.runScriptLine("status");
	    for (int len = 5; len <= 5; ++len) {
		System.out.println("***************** Working on length " + len + " *******************");
		app.runScriptLine("echo rgrow command using 1 building block:");
		app.runScriptLine("clear all");
		Properties growProperties = app.runScriptLine("rgrow blocks=j,2,2 connect=1,1,(hxend)1,(hxend)2," + len + " gen=2 helices=true steric=true ring-export-limit=15 steric=false ring-export=" + methodName + "_1");
		System.out.println(growProperties);
		app.runScriptLine("tree strands");
		printJunctions(System.out, app);
		app.runScriptLine("exportpdb " + methodName + ".pdb");
// 		if ("1".equals(growProperties.getProperty("ring_count"))) {
// 		    System.out.println("**************** UNEXPECTED RING FOUND!!!!!!!!!!!!!!!!! *****************");
// 		    assert false;
// 		}
// 		else {
// 		    System.out.println("No ring formed, probably due to successful collision check.");
// 		}
		assert "0".equals(growProperties.getProperty("ringless_collisions"));
		assert ! "0".equals(growProperties.getProperty("collision_count"));
		// 		Object3DSet junctions = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), "StrandJunction3D");
		// 		assert getJunctions(app).size() == 1; // nothing except first junction should be placed because of collisions
	    }
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testRGrowCommand: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testRGrowCommand: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }
    */

    /** tests building a dimer structure, clash check switched off. Also tests new convinience for not having to write (hxend) in connection descriptor */
    /*
    @Test (groups={"new"})
    public void testGrowDimer5(){
	String methodName = "testGrowDimer5";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
		app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
		app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/dimer.names ${NANOTILER_HOME}/test/fixtures");
		app.runScriptLine("status");
	    for (int len = 9; len <= 9; ++len) {
		System.out.println("* Working on length " + len + " *");
		app.runScriptLine("echo rgrow command using 1 building block:");
		app.runScriptLine("clear all");
		Properties growProperties = app.runScriptLine("rgrow blocks=k,2,1 connect=1,1,1,1," + len + ";" + "1,1,2,2," + len + " gen=2 helices=true steric=false ring-export-limit=20 steric=false ring-export=" + methodName + "_1");
		System.out.println(growProperties);
		assert "0".equals(growProperties.getProperty("ringless_collisions"));
		app.runScriptLine("tree strands");
		printJunctions(System.out, app);
		// app.runScriptLine("exportpdb " + methodName + ".pdb");
		assert getKissingLoops(app).size() == 2; // nothing should be placed because of collisions
		if ("1".equals(growProperties.getProperty("ring_count"))) {
		    System.out.println("* EXPTECTED RING FOUND! *");
		}
		else {
		    assert false; // there should be a ring
		}
		assert "0".equals(growProperties.getProperty("collision_count"));
	    }
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testRGrowCommand: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testRGrowCommand: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }
    */

    /** tests building a dimer structure, clash check switched off. Also tests new convinience for not having to write (hxend) in connection descriptor */
    /*
    @Test (groups={"new"})
    public void testGrowDimer6(){
	String methodName = "testGrowDimer6";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
		app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
		app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/dimer.names ${NANOTILER_HOME}/test/fixtures");
		app.runScriptLine("status");
	    for (int len = 9; len <= 9; ++len) {
		System.out.println("* Working on length " + len + " *");
		app.runScriptLine("echo rgrow command using 1 building block:");
		app.runScriptLine("clear all");
		Properties growProperties = app.runScriptLine("rgrow blocks=k,2,1 connect=1,1,1,1," + len + ";" + "1,1,2,2," + len + " gen=2 helices=true steric=true ring-export-limit=20 steric=true ring-export=" + methodName + "_1");
// 		if ("1".equals(growProperties.getProperty("ring_count"))) {
// 		    System.out.println("* EXPTECTED RING FOUND! *");
// 		    assert false;
// 		}
// 		else {
// 		    System.out.println("* As expected no ring found! *");
// 		}
		System.out.println(growProperties);
		assert "0".equals(growProperties.getProperty("ringless_collisions"));
		app.runScriptLine("tree strands");
		printJunctions(System.out, app);
		// app.runScriptLine("exportpdb " + methodName + ".pdb");
		if ("0".equals(growProperties.getProperty("collision_count"))) {
		    System.out.println("* UNEXPTECTED RING FOUND! *");
		    assert false;
		}
		else {
		    System.out.println("* As expected no ring found! *");
		}
		// currently: structure is built even though there are collisions
// 		Object3DSet junctions = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), "StrandJunction3D");
// 		assert getJunctions(app).size() == 1; // nothing should be placed because of collisions
	    }
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testRGrowCommand: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testRGrowCommand: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }
    */

    /** tests building a triangular structure out of one building block. Also tests not using "(hxend)" keyword. */
    @Test (groups={"newest_later", "slow"})
    public void testRGrowCommand7(){
	String methodName = "testRGrowCommand7";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/growdemo.names ${NANOTILER_HOME}/test/fixtures 2");
	    app.runScriptLine("status");
	    for (int bp = 0; bp <= 10; ++bp) {
		app.runScriptLine("clear all");
		Properties growProperties = app.runScriptLine("rgrow blocks=j,2,1 connect=1,1,1,2," + bp + " gen=3 helices=true steric=true ring-export-limit=15"
					   + " ring-export=" + methodName + "_exp_b1_bp" + bp);
		app.runScriptLine("exportpdb " + methodName + "_b1_bp" + bp + ".pdb junction=true");
		app.runScriptLine("clear all");
		growProperties = app.runScriptLine("rgrow blocks=j,2,2 connect=1,1,1,2," + bp + " gen=3 helices=true steric=true ring-export-limit=15"
					   + " ring-export=" + methodName + "_exp_b2_bp" + bp);
		app.runScriptLine("exportpdb " + methodName + "_b2_bp" + bp + ".pdb junction=true");
	    // assert "1".equals(growProperties.getProperty("ringless_collisions"));
	    }
	    app.runScriptLine("status");
	    app.runScriptLine("clear all");
	    app.runScriptLine("signature root");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testRGrowCommand: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testRGrowCommand: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a triangular structure out of one building block */
    @Test (groups={"new"})
    public void testGrowHexagon() {
	String methodName = "testGrowHexagon";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("status");
	    //	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/1PNY.pdb_j2_0-G867_0-G933.names ${NANOTILER_HOME}/test/fixtures 2");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/grow_hexagon.names ${NANOTILER_HOME}/test/fixtures 2");
	    app.runScriptLine("status");
	    for (int len = 6; len <= 6; ++len) {
		app.runScriptLine("clear all");
		Properties growProperties = app.runScriptLine("rgrow blocks=j,2,1 connect=1,1,(hxend)1,(hxend)2," +len + " gen=3 helices=false steric=true ring-export-limit=5"
							      + " ring-export=" + methodName + "_1");
		// assert "1".equals(growProperties.getProperty("ringless_collisions"));
		System.out.println("Result of rgrow command(1): " + growProperties);
		app.runScriptLine("tree strands");
		
		app.runScriptLine("status");
		String exportname = methodName + "_L" + len + ".pdb";
		System.out.println("Exporting ring structure to: " + exportname);
		System.out.println("Rgrow command successful! Signature: ");
		app.runScriptLine("signature root");
		app.runScriptLine("exportpdb " + exportname + "_initial.pdb junction=true");
		app.runScriptLine("ringfixconstraints root");
		app.runScriptLine("opthelices helices=true steps=1000000");
		app.runScriptLine("echo exporting unfused structure with these strands:");
		app.runScriptLine("exportpdb " + exportname + " junction=true");
                app.runScriptLine("ringfuse root");
		app.runScriptLine("echo exporting fused structure with these strands:");
		app.runScriptLine("tree strand");
		app.runScriptLine("exportpdb " + exportname + "_fused.pdb " + " junction=true name=root.fused");
	    }
	    // check for found rings
	    // assert "0".equals(growProperties.getProperty("ring_count"));
	    // make sure there are not collisions:
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testRGrowCommand: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testRGrowCommand: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


}
