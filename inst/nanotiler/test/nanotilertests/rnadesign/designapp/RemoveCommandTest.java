package nanotilertests.rnadesign.designapp;

import commandtools.CommandException;
import commandtools.CommandExecutionException;
import generaltools.TestTools;
import org.testng.annotations.*; // for testing
import rnadesign.designapp.*;

/** Test class for import command */
public class RemoveCommandTest {

    public RemoveCommandTest() { }

    @Test (groups={"newest", "y2016"})
    public void testRemoveCommand(){
	String methodName = "testRemoveCommand";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("genhelix");
	    app.runScriptLine("status");
	    app.runScriptLine("remove 1.1.1.1");
	    app.runScriptLine("secondary");
	    app.runScriptLine("status");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	} catch(CommandException e) {
	    System.out.println("Command exception in import command test: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }



}
