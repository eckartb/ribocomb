package nanotilertests.rnadesign.designapp;

import java.util.Properties;
import generaltools.TestTools;
import commandtools.CommandException;
import rnadesign.designapp.*;

import org.testng.annotations.*;


public class ResiduePropertyCommandTest {

    /** Production run for reading cube structure and optimizing its sequences. */
    @Test(groups={"new", "production", "slow"})
    public void testResidueProperty() {
	String methodName = "testResidueProperty";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("loadbasepairs ${NANOTILER_HOME}/resources/2J00_A_rnaview.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/testMutateCorners_mutated_rnaview.pdb findstems=false");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("status");
	    app.runScriptLine("secondary");
	    app.runScriptLine("echo Setting status property:");
	    //  Example: mutate A:C5,G15,A2 B:U1,G2,A15
	    app.runScriptLine("resproperty all adhoc");
	    app.runScriptLine("resproperty A:1-6,12,23,34,45 fragment");
	    app.runScriptLine("resproperty B:1-6,12,23,34,45 fragment");
	    app.runScriptLine("resproperty C:1-6,12,23,34,45 fragment");
	    app.runScriptLine("resproperty D:1-6,12,23,34,45 fragment");
	    app.runScriptLine("resproperty E:1-6,12,23,34,45 fragment");
	    app.runScriptLine("resproperty F:1-6,12,23,34,45 fragment");
	    app.runScriptLine("tree strand");
	    // app.runScriptLine("secondary");
	    // app.runScriptLine("echo Adding base pair constraint:");
	    // app.runScriptLine("genbpconstraint root.import.B.48 root.import.C.31");
	    app.runScriptLine("links type=bp"); // show base pairs
            app.runScriptLine("secondary");
	    app.runScriptLine("properties root.import.A.1");
	    app.runScriptLine("properties root.import.A.2");
	    app.runScriptLine("properties root.import.A.12");
	    app.runScriptLine("properties root.import.A.45");
	    app.runScriptLine("properties root.import.F.45");
	    app.runScriptLine("optsequences iter=20000 iter2=10000 error=4");
	    app.runScriptLine("exportpdb " + methodName + "_opt.pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Production run for reading cube structure and optimizing its sequences, using uneven start sequences. */
    @Test(groups={"new", "production", "slow"})
    public void testResiduePropertyUnevenLeader() {
	String methodName = "testResiduePropertyUnevenLeader";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("loadbasepairs ${NANOTILER_HOME}/resources/2J00_A_rnaview.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/testMutateUnevenCorners_mutated_rnaview.pdb findstems=false");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("status");
	    app.runScriptLine("secondary");
	    app.runScriptLine("echo Setting status property:");
	    //  Example: mutate A:C5,G15,A2 B:U1,G2,A15
	    app.runScriptLine("resproperty all adhoc");
	    app.runScriptLine("resproperty A:1-6,12,23,34,45 fragment");
	    app.runScriptLine("resproperty B:1-6,12,23,34,45 fragment");
	    app.runScriptLine("resproperty C:1-6,12,23,34,45 fragment");
	    app.runScriptLine("resproperty D:1-6,12,23,34,45 fragment");
	    app.runScriptLine("resproperty E:1-6,12,23,34,45 fragment");
	    app.runScriptLine("resproperty F:1-6,12,23,34,45 fragment");
	    app.runScriptLine("tree strand");
	    // app.runScriptLine("secondary");
	    // app.runScriptLine("echo Adding base pair constraint:");
	    // app.runScriptLine("genbpconstraint root.import.B.48 root.import.C.31");
	    app.runScriptLine("links type=bp"); // show base pairs
            app.runScriptLine("secondary");
	    app.runScriptLine("properties root.import.A.1");
	    app.runScriptLine("properties root.import.A.2");
	    app.runScriptLine("properties root.import.A.12");
	    app.runScriptLine("properties root.import.A.45");
	    app.runScriptLine("properties root.import.F.45");
	    app.runScriptLine("optsequences iter=20000 iter2=10000 error=4");
	    app.runScriptLine("exportpdb " + methodName + "_opt.pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


    /** Production run for reading cube structure and optimizing its sequences, using uneven start sequences. */
    @Test(groups={"new", "production", "slow"})
    public void testResiduePropertyUnevenLeader2() {
	String methodName = "testResiduePropertyUnevenLeader2";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("loadbasepairs ${NANOTILER_HOME}/resources/2J00_A_rnaview.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/testExtendAtStart2_mutated_ABCDEF.pdb findstems=false");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("status");
	    app.runScriptLine("secondary");
	    app.runScriptLine("echo Setting status property:");
	    //  Example: mutate A:C5,G15,A2 B:U1,G2,A15
	    app.runScriptLine("resproperty all adhoc");
	    app.runScriptLine("resproperty A:1-6,12,23,34,45 fragment");
	    app.runScriptLine("resproperty B:1-6,12,23,34,45 fragment");
	    app.runScriptLine("resproperty C:1-6,12,23,34,45 fragment");
	    app.runScriptLine("resproperty D:1-6,12,23,34,45 fragment");
	    app.runScriptLine("resproperty E:1-6,12,23,34,45 fragment");
	    app.runScriptLine("resproperty F:1-6,12,23,34,45 fragment");
	    app.runScriptLine("tree strand");
	    // app.runScriptLine("secondary");
	    // app.runScriptLine("echo Adding base pair constraint:");
	    // app.runScriptLine("genbpconstraint root.import.B.48 root.import.C.31");
	    app.runScriptLine("links type=bp"); // show base pairs
            app.runScriptLine("secondary");
	    app.runScriptLine("properties root.import.A.1");
	    app.runScriptLine("properties root.import.A.2");
	    app.runScriptLine("properties root.import.A.12");
	    app.runScriptLine("properties root.import.A.45");
	    app.runScriptLine("properties root.import.F.45");
	    app.runScriptLine("optsequences iter=20000 iter2=20000 error=4 s1=4 s2=1");
	    app.runScriptLine("exportpdb " + methodName + "_opt.pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Production run for reading cube structure and optimizing its sequences, using uneven start sequences. */
    @Test(groups={"new", "production", "slow"})
    public void testResiduePropertyNupack() {
	String methodName = "testResiduePropertyNupack";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("loadbasepairs ${NANOTILER_HOME}/resources/2J00_A_rnaview.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/testMutateUnevenCorners_mutated_rnaview.pdb findstems=false");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("status");
	    app.runScriptLine("secondary");
	    app.runScriptLine("echo Setting status property:");
	    //  Example: mutate A:C5,G15,A2 B:U1,G2,A15
	    app.runScriptLine("resproperty all adhoc");
	    app.runScriptLine("resproperty A:1-6,12,23,34,45 fragment");
	    app.runScriptLine("resproperty B:1-6,12,23,34,45 fragment");
	    app.runScriptLine("resproperty C:1-6,12,23,34,45 fragment");
	    app.runScriptLine("resproperty D:1-6,12,23,34,45 fragment");
	    app.runScriptLine("resproperty E:1-6,12,23,34,45 fragment");
	    app.runScriptLine("resproperty F:1-6,12,23,34,45 fragment");
	    app.runScriptLine("tree strand");
	    // app.runScriptLine("secondary");
	    // app.runScriptLine("echo Adding base pair constraint:");
	    // app.runScriptLine("genbpconstraint root.import.B.48 root.import.C.31");
	    app.runScriptLine("links type=bp"); // show base pairs
            app.runScriptLine("secondary");
	    app.runScriptLine("properties root.import.A.1");
	    app.runScriptLine("properties root.import.A.2");
	    app.runScriptLine("properties root.import.A.12");
	    app.runScriptLine("properties root.import.A.45");
	    app.runScriptLine("properties root.import.F.45");
	    app.runScriptLine("optsequences iter=20000 iter2=10000 error=4 s2=3");
	    app.runScriptLine("exportpdb " + methodName + "_opt.pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Production run for reading cube structure and optimizing its sequences, using even start sequences,
     * Criton-Scorer and RNAcofold scorer or dummy scorer. */
    @Test(groups={"new", "production", "slow"})
    public void testResiduePropertyCriton() {
	String methodName = "testResiduePropertyCriton";
	System.out.println(TestTools.generateMethodHeader(methodName));
	boolean uCornerMode = false;
	boolean fragmentMode = false;
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("loadbasepairs ${NANOTILER_HOME}/resources/2J00_A_rnaview.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/testMutateLeader2_mutated_rnaview.pdb findstems=false");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("status");
	    app.runScriptLine("secondary");
	    app.runScriptLine("echo Setting status property:");
	    // Example: mutate A:C5,G15,A2 B:U1,G2,A15
	    app.runScriptLine("resproperty all adhoc");
	    if (fragmentMode) {
		app.runScriptLine("resproperty A:1-8,49,50 fragment");
		app.runScriptLine("resproperty B:1-8,31,32,33,34,49,50 fragment");
		app.runScriptLine("resproperty C:1-8,31,32,33,34,49,50 fragment");
		app.runScriptLine("resproperty D:1-8,49,50 fragment");
		app.runScriptLine("resproperty E:1-8,31,32,33,34,49,50 fragment");
		app.runScriptLine("resproperty F:1-8,31,32,33,34,49,50 fragment");
	    }
	    else {
		app.runScriptLine("resproperty A:1-6 fragment");
		app.runScriptLine("resproperty B:1-6 fragment");
		app.runScriptLine("resproperty C:1-6 fragment");
		app.runScriptLine("resproperty D:1-6 fragment");
		app.runScriptLine("resproperty E:1-6 fragment");
		app.runScriptLine("resproperty F:1-6 fragment");
	    }
	    if (uCornerMode) { // keep corners constant at "U":
		app.runScriptLine("resproperty A:14,25,36,47 fragment");
		app.runScriptLine("resproperty B:14,25,36,47 fragment");
		app.runScriptLine("resproperty C:14,25,36,47 fragment");
		app.runScriptLine("resproperty D:14,25,36,47 fragment");
		app.runScriptLine("resproperty E:14,25,36,47 fragment");
		app.runScriptLine("resproperty F:14,25,36,47 fragment");
	    }

	    app.runScriptLine("tree strand");
	    // app.runScriptLine("secondary");
	    // app.runScriptLine("echo Adding base pair constraint:");
	    // app.runScriptLine("genbpconstraint root.import.B.48 root.import.C.31");
	    app.runScriptLine("links type=bp"); // show base pairs
            app.runScriptLine("secondary");
	    app.runScriptLine("properties root.import.A.1");
	    app.runScriptLine("properties root.import.A.2");
	    app.runScriptLine("properties root.import.A.14");
	    app.runScriptLine("properties root.import.A.47");
	    app.runScriptLine("properties root.import.F.47");
	    app.runScriptLine("optsequences iter=10000 iter2=20000 error=30.0 s1=4 s2=5 rerun=5 prop=critonlen:5"); // stage1: criton scorer, stage2: dummy scorer
	    app.runScriptLine("exportpdb " + methodName + "_opt.pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


    /** Production run for reading cube structure and optimizing its sequences, using even start sequences,
     * Criton-Scorer and NUPACK/complexes scorer. */
    @Test(groups={"new", "production", "slow"})
    public void testResiduePropertyCritonNupack() {
	String methodName = "testResiduePropertyCritonNupack";
	System.out.println(TestTools.generateMethodHeader(methodName));
	boolean uCornerMode = false;
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("loadbasepairs ${NANOTILER_HOME}/resources/2J00_A_rnaview.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/testMutateLeader2_mutated_rnaview.pdb findstems=false");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("status");
	    app.runScriptLine("secondary");
	    app.runScriptLine("echo Setting status property:");
	    // Example: mutate A:C5,G15,A2 B:U1,G2,A15
	    app.runScriptLine("resproperty all adhoc");
	    app.runScriptLine("resproperty A:1-8,49,50 fragment");
	    app.runScriptLine("resproperty B:1-8,31,32,33,34,49,50 fragment");
	    app.runScriptLine("resproperty C:1-8,31,32,33,34,49,50 fragment");
	    app.runScriptLine("resproperty D:1-8,49,50 fragment");
	    app.runScriptLine("resproperty E:1-8,31,32,33,34,49,50 fragment");
	    app.runScriptLine("resproperty F:1-8,31,32,33,34,49,50 fragment");

	    if (uCornerMode) { // keep corners constant at "U":
		app.runScriptLine("resproperty A:14,25,36,47 fragment");
		app.runScriptLine("resproperty B:14,25,36,47 fragment");
		app.runScriptLine("resproperty C:14,25,36,47 fragment");
		app.runScriptLine("resproperty D:14,25,36,47 fragment");
		app.runScriptLine("resproperty E:14,25,36,47 fragment");
		app.runScriptLine("resproperty F:14,25,36,47 fragment");
	    }

	    app.runScriptLine("tree strand");
	    // app.runScriptLine("secondary");
	    // app.runScriptLine("echo Adding base pair constraint:");
	    // app.runScriptLine("genbpconstraint root.import.B.48 root.import.C.31");
	    app.runScriptLine("links type=bp"); // show base pairs
            app.runScriptLine("secondary");
	    app.runScriptLine("properties root.import.A.1");
	    app.runScriptLine("properties root.import.A.2");
	    app.runScriptLine("properties root.import.A.14");
	    app.runScriptLine("properties root.import.A.47");
	    app.runScriptLine("properties root.import.F.47");
	    app.runScriptLine("optsequences iter=10000 iter2=1000 error=18.0 s1=4 s2=3 rerun=10"); // stage1: criton scorer, stage2: dummy scorer
	    app.runScriptLine("exportpdb " + methodName + "_opt.pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Production run for reading cube structure and optimizing its sequences, using even start sequences,
     * Criton-Scorer and RNAcofold scorer.
     * Same as testResiduePropertyCriton(), but using non-constant zipper regions at ends and in middle. */
    @Test(groups={"new", "production", "slow"})
    public void testResiduePropertyCriton2() {
	String methodName = "testResiduePropertyCriton2";
	System.out.println(TestTools.generateMethodHeader(methodName));
	boolean uCornerMode = false;
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("loadbasepairs ${NANOTILER_HOME}/resources/2J00_A_rnaview.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/testMutateLeader2_mutated_rnaview.pdb findstems=false");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("status");
	    app.runScriptLine("secondary");
	    app.runScriptLine("echo Setting status property:");
	    // Example: mutate A:C5,G15,A2 B:U1,G2,A15
	    app.runScriptLine("resproperty all adhoc");
	    app.runScriptLine("resproperty A:1-6 fragment");
	    app.runScriptLine("resproperty B:1-6 fragment");
	    app.runScriptLine("resproperty C:1-6 fragment");
	    app.runScriptLine("resproperty D:1-6 fragment");
	    app.runScriptLine("resproperty E:1-6 fragment");
	    app.runScriptLine("resproperty F:1-6 fragment");

	    if (uCornerMode) { // keep corners constant at "U":
		app.runScriptLine("resproperty A:14,25,36,47 fragment");
		app.runScriptLine("resproperty B:14,25,36,47 fragment");
		app.runScriptLine("resproperty C:14,25,36,47 fragment");
		app.runScriptLine("resproperty D:14,25,36,47 fragment");
		app.runScriptLine("resproperty E:14,25,36,47 fragment");
		app.runScriptLine("resproperty F:14,25,36,47 fragment");
	    }

	    app.runScriptLine("tree strand");
	    // app.runScriptLine("secondary");
	    // app.runScriptLine("echo Adding base pair constraint:");
	    // app.runScriptLine("genbpconstraint root.import.B.48 root.import.C.31");
	    app.runScriptLine("links type=bp"); // show base pairs
            app.runScriptLine("secondary");
	    app.runScriptLine("properties root.import.A.1");
	    app.runScriptLine("properties root.import.A.2");
	    app.runScriptLine("properties root.import.A.14");
	    app.runScriptLine("properties root.import.A.47");
	    app.runScriptLine("properties root.import.F.47");
	    app.runScriptLine("optsequences iter=10000 iter2=1000 error=18.0 s1=4 s2=1 rerun=10"); // stage1: criton scorer, stage2: dummy scorer
	    app.runScriptLine("exportpdb " + methodName + "_opt.pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Production run for reading cube structure and optimizing its sequences, using even start sequences,
     * Criton-Scorer and complexes scorer.
     * Same as testResiduePropertyCriton(), but using non-constant zipper regions at ends and in middle. */
    @Test(groups={"new", "production", "slow"})
    public void testResiduePropertyCritonNupack2() {
	String methodName = "testResiduePropertyCritonNupack2";
	System.out.println(TestTools.generateMethodHeader(methodName));
	boolean uCornerMode = false;
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("loadbasepairs ${NANOTILER_HOME}/resources/2J00_A_rnaview.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/testMutateLeader2_mutated_rnaview.pdb findstems=false");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("status");
	    app.runScriptLine("secondary");
	    app.runScriptLine("echo Setting status property:");
	    // Example: mutate A:C5,G15,A2 B:U1,G2,A15
	    app.runScriptLine("resproperty all adhoc");
	    app.runScriptLine("resproperty A:1-6 fragment");
	    app.runScriptLine("resproperty B:1-6 fragment");
	    app.runScriptLine("resproperty C:1-6 fragment");
	    app.runScriptLine("resproperty D:1-6 fragment");
	    app.runScriptLine("resproperty E:1-6 fragment");
	    app.runScriptLine("resproperty F:1-6 fragment");

	    if (uCornerMode) { // keep corners constant at "U":
		app.runScriptLine("resproperty A:14,25,36,47 fragment");
		app.runScriptLine("resproperty B:14,25,36,47 fragment");
		app.runScriptLine("resproperty C:14,25,36,47 fragment");
		app.runScriptLine("resproperty D:14,25,36,47 fragment");
		app.runScriptLine("resproperty E:14,25,36,47 fragment");
		app.runScriptLine("resproperty F:14,25,36,47 fragment");
	    }

	    app.runScriptLine("tree strand");
	    // app.runScriptLine("secondary");
	    // app.runScriptLine("echo Adding base pair constraint:");
	    // app.runScriptLine("genbpconstraint root.import.B.48 root.import.C.31");
	    app.runScriptLine("links type=bp"); // show base pairs
            app.runScriptLine("secondary");
	    app.runScriptLine("properties root.import.A.1");
	    app.runScriptLine("properties root.import.A.2");
	    app.runScriptLine("properties root.import.A.14");
	    app.runScriptLine("properties root.import.A.47");
	    app.runScriptLine("properties root.import.F.47");
	    app.runScriptLine("optsequences iter=10000 iter2=1000 error=18.0 s1=4 s2=3 rerun=5"); // stage1: criton scorer, stage2: dummy scorer
	    app.runScriptLine("exportpdb " + methodName + "_opt.pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Used for writing of secondary structure output */
    @Test(groups={"new", "slow"})
    public void testResiduePropertySecondary() {
	String methodName = "testResiduePropertySecondary";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("loadbasepairs ${NANOTILER_HOME}/resources/2J00_A_rnaview.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/testMutateUnevenCorners_mutated_rnaview.pdb findstems=false");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("status");
	    app.runScriptLine("secondary");
	    app.runScriptLine("echo Setting status property:");
	    // Example: mutate A:C5,G15,A2 B:U1,G2,A15
	    app.runScriptLine("resproperty all adhoc");
	    app.runScriptLine("resproperty A:1-6,12,23,34,45 fragment");
	    app.runScriptLine("resproperty B:1-6,12,23,34,45 fragment");
	    app.runScriptLine("resproperty C:1-6,12,23,34,45 fragment");
	    app.runScriptLine("resproperty D:1-6,12,23,34,45 fragment");
	    app.runScriptLine("resproperty E:1-6,12,23,34,45 fragment");
	    app.runScriptLine("resproperty F:1-6,12,23,34,45 fragment");
	    app.runScriptLine("tree strand");
	    // app.runScriptLine("secondary");
	    // app.runScriptLine("echo Adding base pair constraint:");
	    // app.runScriptLine("genbpconstraint root.import.B.48 root.import.C.31");
	    // app.runScriptLine("links type=bp"); // show base pairs

	    app.runScriptLine("properties root.import.A.1");
	    // app.runScriptLine("properties root.import.A.2");
	    // app.runScriptLine("properties root.import.A.12");
	    // app.runScriptLine("properties root.import.A.45");
	    // app.runScriptLine("properties root.import.F.45");
            app.runScriptLine("secondary");
	    // app.runScriptLine("optsequences iter=1000 iter2=100 error=0.0 s1=4 s2=5"); // stage1: criton scorer, stage2: dummy scorer
	    // app.runScriptLine("exportpdb " + methodName + "_opt.pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }



}
