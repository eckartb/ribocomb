package nanotilertests.rnadesign.designapp;

import commandtools.CommandException;
import commandtools.CommandExecutionException;
import tools3d.objects3d.Object3DTools;
import org.testng.annotations.*; // for testing
import generaltools.TestTools;
import rnadesign.designapp.*;

/** Test class for import command */
public class RingFixConstraintsCommandTest {

    public RingFixConstraintsCommandTest() { }

    /** Reads imperfect, generates helix constraints corresponding to perfectly symmetric ring */
    @Test (groups={"new", "slow"})
    public void testRingFixConstraintsCommand1(){
	String methodName = "testRingFixConstraintsCommand";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    // hard test: RNAview does not recognize one three-way junction:
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/2J00.rnaview.pdb_j3_A-G1064_A-C1107_A-G1187_grow_ring_rnaview.pdb");
	    // again, junctions are not properly recognized
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1S1Icleanfusedreidede25i25i225_renumbered.pdb");
	    // make sure stems of length 2 can be read:
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/2I2T.pdb_j2_B-C274_B-G361_chain_ring_1221_L7_rnaview.pdb");
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("controller import-stem-length-min=3");
	    assert app.getGraphController().getImportStemLengthMin() == 3;
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1NKW.pdb_j2_0-A1043_0-C1132_chain_ring_1221_L5_rnaview.pdb");
	    app.runScriptLine("tree allowed=StrandJunction3D");
	    app.runScriptLine("tree strands");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("exportpdb " + methodName + "_tmp1.pdb");
	    app.runScriptLine("links");
	    app.runScriptLine("ringfixconstraints root");
	    app.runScriptLine("echo Generated ring constraints:");
	    app.runScriptLine("links");
	    app.runScriptLine("opthelices helices=true");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("exportpdb " + methodName + "_fixed_junctions.pdb name=root.import_cov_jnc");
	    app.runScriptLine("remove root.stems");
	    app.runScriptLine("remove root.import"); // remove all original data
	    app.runScriptLine("exportpdb " + methodName + "_fixed_all.pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	} catch(CommandException e) {
	    System.out.println("Command exception in import command test: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Reads imperfect, generates helix constraints corresponding to perfectly symmetric ring */
    @Test (groups={"new", "slow"})
    public void testRingFixConstraintsCommand2(){
	String methodName = "testRingFixConstraintsCommand2";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    // hard test: RNAview does not recognize one three-way junction:
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/2J00.rnaview.pdb_j3_A-G1064_A-C1107_A-G1187_grow_ring_rnaview.pdb");
	    // again, junctions are not properly recognized
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1S1Icleanfusedreidede25i25i225_renumbered.pdb");
	    // make sure stems of length 2 can be read:
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/2I2T.pdb_j2_B-C274_B-G361_chain_ring_1221_L7_rnaview.pdb");
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("controller import-stem-length-min=3");
	    assert app.getGraphController().getImportStemLengthMin() == 3;
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/2HGI.pdb_j2_A-C150_A-G168_chain_ring.1Y0Q.pdb_k2_A-C146_A-A225_1221_L75.pdb");
	    app.runScriptLine("tree allowed=StrandJunction3D");
	    app.runScriptLine("tree strands");
	    app.runScriptLine("tree forbidden=Atom3D,Nucleotide3D,CoordinateSystem3D,Object3D");
	    app.runScriptLine("exportpdb " + methodName + "_tmp1.pdb");
	    app.runScriptLine("exportpdb " + methodName + "_tmp1_junctions.pdb name=root.import_cov_jnc");
	    app.runScriptLine("links");
	    // make sure there are 8 structural elements:
	    int numJunctions = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), "StrandJunction3D").size();
	    System.out.println("Number of detected junctions (expecting 8): " + numJunctions);
	    assert numJunctions == 8;
	    app.runScriptLine("ringfixconstraints root");
	    app.runScriptLine("echo Generated ring constraints:");
	    app.runScriptLine("links");
	    app.runScriptLine("opthelices helices=true initial-error=1000");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("exportpdb " + methodName + "_fixed_junctions.pdb name=root.import_cov_jnc");
	    app.runScriptLine("remove root.stems");
	    app.runScriptLine("remove root.import"); // remove all original data
	    app.runScriptLine("exportpdb " + methodName + "_fixed_all.pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	} catch(CommandException e) {
	    System.out.println("Command exception in import command test: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Reads imperfect, generates helix constraints corresponding to perfectly symmetric ring */
    @Test (groups={"new", "slow"})
    public void testRingFixConstraintsCommand3(){
	String methodName = "testRingFixConstraintsCommand3";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    // hard test: RNAview does not recognize one three-way junction:
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/2J00.rnaview.pdb_j3_A-G1064_A-C1107_A-G1187_grow_ring_rnaview.pdb");
	    // again, junctions are not properly recognized
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1S1Icleanfusedreidede25i25i225_renumbered.pdb");
	    // make sure stems of length 2 can be read:
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/2I2T.pdb_j2_B-C274_B-G361_chain_ring_1221_L7_rnaview.pdb");
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("controller import-stem-length-min=3");
	    assert app.getGraphController().getImportStemLengthMin() == 3;
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/2GYA.pdb_j2_0-G539_0-G553_chain_ring.1FFK.pdb_k2_0-U55_0-C83_1221_L75.pdb_fixed.pdb");
	    app.runScriptLine("tree allowed=StrandJunction3D");
	    app.runScriptLine("tree strands");
	    app.runScriptLine("tree forbidden=Atom3D,Nucleotide3D,CoordinateSystem3D,Object3D");
	    app.runScriptLine("exportpdb " + methodName + "_tmp1.pdb");
	    app.runScriptLine("exportpdb " + methodName + "_tmp1_junctions.pdb name=root.import_cov_jnc");
	    app.runScriptLine("links");
	    // make sure there are 8 structural elements:
	    int numJunctions = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), "StrandJunction3D").size();
	    System.out.println("Number of detected junctions (expecting 3): " + numJunctions);
	    assert numJunctions == 3;
	    app.runScriptLine("ringfixconstraints root");
	    app.runScriptLine("echo Generated ring constraints:");
	    app.runScriptLine("links");
	    app.runScriptLine("opthelices helices=true initial-error=1000");
	    app.runScriptLine("tree forbidden=Atom3D");
	    app.runScriptLine("exportpdb " + methodName + "_fixed_junctions.pdb name=root.import_cov_jnc");
	    app.runScriptLine("remove root.stems");
	    app.runScriptLine("remove root.import"); // remove all original data
	    app.runScriptLine("exportpdb " + methodName + "_fixed_all.pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	} catch(CommandException e) {
	    System.out.println("Command exception in import command test: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Reads kissing loop structure composed of rings, generates helix constraints corresponding to perfectly symmetric ring */
    @Test (groups={"new", "slow"})
    public void testRingFixConstraintsCommand4(){
	String methodName = "testRingFixConstraintsCommand4";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    // hard test: RNAview does not recognize one three-way junction:
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/2J00.rnaview.pdb_j3_A-G1064_A-C1107_A-G1187_grow_ring_rnaview.pdb");
	    // again, junctions are not properly recognized
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1S1Icleanfusedreidede25i25i225_renumbered.pdb");
	    // make sure stems of length 2 can be read:
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/2I2T.pdb_j2_B-C274_B-G361_chain_ring_1221_L7_rnaview.pdb");
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("controller import-stem-length-min=3");
	    assert app.getGraphController().getImportStemLengthMin() == 3;
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/2I2T.pdb_k2_B-G1450_B-U2701_chain_ring_1221_L10_nohelices.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/2I2T.pdb_k2_B-G1450_B-U2701_chain_ring_1221_L10_nohelices_rnaview.pdb");
	    app.runScriptLine("tree strands");
	    app.runScriptLine("tree forbidden=Atom3D,Nucleotide3D,CoordinateSystem3D,Object3D");
	    app.runScriptLine("exportpdb " + methodName + "_tmp1.pdb");
	    app.runScriptLine("exportpdb " + methodName + "_tmp1_junctions.pdb name=root.import_cov_jnc");
	    app.runScriptLine("exportpdb " + methodName + "_tmp2_junctions.pdb name=root.import_cov_kl");
	    app.runScriptLine("links");
	    // make sure there are 8 structural elements:
	    int numJunctions = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), "StrandJunction3D").size();
	    System.out.println("Number of detected junctions (expecting 0): " + numJunctions);
	    app.runScriptLine("tree forbidden=Atom3D,Nucleotide3D,CoordinateSystem3D,Object3D");
	    assert numJunctions == 0;
	    int numKl = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), "KissingLoop3D").size();
	    System.out.println("Number of detected kissing loops (expecting 3): " + numKl);
	    assert numKl == 3;
	    app.runScriptLine("ringfixconstraints root");
	    app.runScriptLine("echo Generated ring constraints:");
	    app.runScriptLine("links");
	    app.runScriptLine("opthelices helices=true initial-error=1000 steps=11000"); // very short run
	    app.runScriptLine("tree forbidden=Atom3D,Nucleotide3D,CoordinateSystem3D,Object3D");
	    app.runScriptLine("exportpdb " + methodName + "_fixed_junctions.pdb name=root.import_cov_jnc");
	    app.runScriptLine("remove root.stems");
	    app.runScriptLine("remove root.import"); // remove all original data
	    app.runScriptLine("exportpdb " + methodName + "_fixed_all.pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	} catch(CommandException e) {
	    System.out.println("Command exception in import command test: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


    /** Reads kissing loop + junction structure. Really short optimization. The challenge is that the result is so bad that not all helices can be placed. */
    @Test (groups={"new", "slow"})
    public void testRingFixConstraintsCommand5(){
	String methodName = "testRingFixConstraintsCommand5";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    // hard test: RNAview does not recognize one three-way junction:
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/2J00.rnaview.pdb_j3_A-G1064_A-C1107_A-G1187_grow_ring_rnaview.pdb");
	    // again, junctions are not properly recognized
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1S1Icleanfusedreidede25i25i225_renumbered.pdb");
	    // make sure stems of length 2 can be read:
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/2I2T.pdb_j2_B-C274_B-G361_chain_ring_1221_L7_rnaview.pdb");
 	    app.runScriptLine("seed 10"); // make it reproducible
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("controller import-stem-length-min=3");
	    assert app.getGraphController().getImportStemLengthMin() == 3;
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/2I2T.pdb_k2_B-G1450_B-U2701_chain_ring_1221_L10_nohelices.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/2HGU.pdb_j2_A-U1019_A-C1121_chain_ring.1FFK.pdb_k2_0-U55_0-C83_1122_L106_nohelices.pdb");
	    app.runScriptLine("tree strands");
	    app.runScriptLine("tree forbidden=Atom3D,Nucleotide3D,CoordinateSystem3D,Object3D");
	    app.runScriptLine("exportpdb " + methodName + "_tmp1.pdb");
	    app.runScriptLine("exportpdb " + methodName + "_tmp1_junctions.pdb name=root.import_cov_jnc");
	    app.runScriptLine("exportpdb " + methodName + "_tmp2_junctions.pdb name=root.import_cov_kl");
	    app.runScriptLine("links");
	    // make sure there are 8 structural elements:
	    int numJunctions = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), "StrandJunction3D").size();
	    System.out.println("Number of detected junctions (expecting 2): " + numJunctions);
	    app.runScriptLine("tree forbidden=Atom3D,Nucleotide3D,CoordinateSystem3D,Object3D");
	    assert numJunctions == 2;
	    int numKl = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), "KissingLoop3D").size();
	    System.out.println("Number of detected kissing loops (expecting 2): " + numKl);
	    assert numKl == 2;
	    app.runScriptLine("ringfixconstraints root");
	    app.runScriptLine("echo Generated ring constraints:");
	    app.runScriptLine("links");
	    app.runScriptLine("opthelices helices=true initial-error=1000 steps=11000 rms=25"); // very short run
	    app.runScriptLine("tree forbidden=Atom3D,Nucleotide3D,CoordinateSystem3D,Object3D");
	    app.runScriptLine("exportpdb " + methodName + "_fixed_junctions.pdb name=root.import_cov_jnc");
	    app.runScriptLine("remove root.stems");
	    app.runScriptLine("remove root.import"); // remove all original data
	    app.runScriptLine("exportpdb " + methodName + "_fixed_final.pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	} catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + ": " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


    /** Reads kissing loop + junction structure. Really short optimization. The challenge is that the result is so bad that not all helices can be placed. */
    @Test (groups={"new", "slow"})
    public void testRingFixConstraintsCommand6(){
	String methodName = "testRingFixConstraintsCommand6";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    // hard test: RNAview does not recognize one three-way junction:
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/2J00.rnaview.pdb_j3_A-G1064_A-C1107_A-G1187_grow_ring_rnaview.pdb");
	    // again, junctions are not properly recognized
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1S1Icleanfusedreidede25i25i225_renumbered.pdb");
	    // make sure stems of length 2 can be read:
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/2I2T.pdb_j2_B-C274_B-G361_chain_ring_1221_L7_rnaview.pdb");
 	    app.runScriptLine("seed 10"); // make it reproducible
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("controller import-stem-length-min=3");
	    assert app.getGraphController().getImportStemLengthMin() == 3;
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/2I2T.pdb_k2_B-G1450_B-U2701_chain_ring_1221_L10_nohelices.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/2HGU.pdb_j2_A-U1019_A-C1121_chain_ring.1FFK.pdb_k2_0-U55_0-C83_1122_L106_nohelices.pdb");
	    app.runScriptLine("tree strands");
	    app.runScriptLine("tree forbidden=Atom3D,Nucleotide3D,CoordinateSystem3D,Object3D");
	    app.runScriptLine("exportpdb " + methodName + "_tmp1.pdb");
	    app.runScriptLine("exportpdb " + methodName + "_tmp1_junctions.pdb name=root.import_cov_jnc");
	    app.runScriptLine("exportpdb " + methodName + "_tmp2_junctions.pdb name=root.import_cov_kl");
	    app.runScriptLine("links");
	    // make sure there are 8 structural elements:
	    int numJunctions = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), "StrandJunction3D").size();
	    System.out.println("Number of detected junctions (expecting 2): " + numJunctions);
	    app.runScriptLine("tree forbidden=Atom3D,Nucleotide3D,CoordinateSystem3D,Object3D");
	    assert numJunctions == 2;
	    int numKl = Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), "KissingLoop3D").size();
	    System.out.println("Number of detected kissing loops (expecting 2): " + numKl);
	    assert numKl == 2;
	    app.runScriptLine("ringfixconstraints root");
	    app.runScriptLine("echo Generated ring constraints:");
	    app.runScriptLine("links");
	    app.runScriptLine("opthelices helices=true initial-error=1000 steps=500000 rms=25"); // very short run
	    app.runScriptLine("tree forbidden=Atom3D,Nucleotide3D,CoordinateSystem3D,Object3D");
	    app.runScriptLine("exportpdb " + methodName + "_fixed_junctions.pdb name=root.import_cov_jnc");
	    app.runScriptLine("remove root.stems");
	    app.runScriptLine("remove root.import"); // remove all original data
	    app.runScriptLine("exportpdb " + methodName + "_fixed_final.pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	} catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + ": " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Reads kissing loop + junction structure. Really short optimization. The challenge is that the result is so bad that not all helices can be placed. */
    @Test (groups={"new", "slow"})
    public void testRingFixConstraintsCommand7(){
	String methodName = "testRingFixConstraintsCommand7";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    // hard test: RNAview does not recognize one three-way junction:
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/2J00.rnaview.pdb_j3_A-G1064_A-C1107_A-G1187_grow_ring_rnaview.pdb");
	    // again, junctions are not properly recognized
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1S1Icleanfusedreidede25i25i225_renumbered.pdb");
	    // make sure stems of length 2 can be read:
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/2I2T.pdb_j2_B-C274_B-G361_chain_ring_1221_L7_rnaview.pdb");
 	    app.runScriptLine("seed 10"); // make it reproducible
	    app.runScriptLine("source ${NANOTILER_HOME}/scripts/ringfix.script ${NANOTILER_HOME}/test/fixtures/2HGU.pdb_j2_A-U1019_A-C1121_chain_ring.1FFK.pdb_k2_0-U55_0-C83_1122_L106_nohelices.pdb " + methodName + "_final.pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	} catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + ": " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Reads kissing loop + junction structure. Really short optimization. The challenge is that the result is so bad that not all helices can be placed. */
    @Test (groups={"new", "slow"})
    public void testRingFixConstraintsCommand8(){
	String methodName = "testRingFixConstraintsCommand8";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    // hard test: RNAview does not recognize one three-way junction:
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/2J00.rnaview.pdb_j3_A-G1064_A-C1107_A-G1187_grow_ring_rnaview.pdb");
	    // again, junctions are not properly recognized
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1S1Icleanfusedreidede25i25i225_renumbered.pdb");
	    // make sure stems of length 2 can be read:
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/2I2T.pdb_j2_B-C274_B-G361_chain_ring_1221_L7_rnaview.pdb");
 	    app.runScriptLine("seed 10"); // make it reproducible
	    String command = "source ${NANOTILER_HOME}/scripts/ringfix.script ${NANOTILER_HOME}/test/fixtures/1BIV-1122-L1617-rv.pdb " + methodName + "_final.pdb";
	    System.out.println("Command: " + command);
	    app.runScriptLine(command);
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	} catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + ": " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

}
