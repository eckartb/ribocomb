package nanotilertests.rnadesign.designapp;

import commandtools.CommandException;
import commandtools.CommandExecutionException;
import generaltools.TestTools;
import tools3d.objects3d.Object3DTools;
import org.testng.annotations.*; // for testing
import rnadesign.designapp.*;

/** Test class for import command */
public class RingFuseCommandTest {

    public RingFuseCommandTest() { }

    /** Reads imperfect, generates helix constraints corresponding to perfectly symmetric ring */
    @Test (groups={"new"})
    public void testRingFuseCommand(){
	String methodName = "testRingFuseCommand";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("controller import-stem-length-min=3");
	    assert app.getGraphController().getImportStemLengthMin() == 3;
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1NKW.pdb_j2_0-A1043_0-C1132_chain_ring_1221_L5_rnaview.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/ringFixConstraintsCommandTest_fixed_all.pdb");
	    app.runScriptLine("links"); // show all found links like base pairs
	    app.runScriptLine("echo Found junctions:");
	    app.runScriptLine("tree allowed=StrandJunction3D");
	    int seqNum1 = app.getGraphController().getSequences().getSequenceCount();
	    System.out.println("Number of sequences before fusing: " + seqNum1);
	    app.runScriptLine("tree strands");
	    app.runScriptLine("ringfuse root");
	    app.runScriptLine("echo Fused strands:");
	    app.runScriptLine("tree strands");
	    app.runScriptLine("exportpdb ringFuseCommandTest.pdb junction=false name=root.fused renumber=count");
	    int seqNum2 = app.getGraphController().getSequences().getSequenceCount();
	    System.out.println("Number of sequences after fusing: " + seqNum2);
	    if (seqNum2 <= seqNum1) {
		System.err.println("Number of sequences did not increase after fusing!");
	    }
	    assert seqNum2 > seqNum1;
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	} catch(CommandException e) {
	    System.out.println("Command exception in import command test: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Reads imperfect, generates helix constraints corresponding to perfectly symmetric ring */
    @Test (groups={"slow", "new"})
    public void testRingFuseCommand2(){
	String methodName = "testRingFuseCommand2";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("controller import-stem-length-min=3");
	    assert app.getGraphController().getImportStemLengthMin() == 3;
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1NKW.pdb_j2_0-A1043_0-C1132_chain_ring_1221_L5_rnaview.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/2GYA.pdb_j2_0-G539_0-G553_chain_ring.1FFK.pdb_k2_0-U55_0-C83_1221_L75.pdb_fixed.pdb");
	    app.runScriptLine("links"); // show all found links like base pairs
	    app.runScriptLine("echo Found junctions:");
	    app.runScriptLine("tree allowed=StrandJunction3D");
	    int seqNum1 = app.getGraphController().getSequences().getSequenceCount();
	    System.out.println("Number of sequences before fusing: " + seqNum1);
	    app.runScriptLine("tree strands");
	    app.runScriptLine("ringfuse root");
	    app.runScriptLine("renumber all");
	    app.runScriptLine("echo Fused strands:");
	    app.runScriptLine("tree strands");
	    app.runScriptLine("exportpdb ringFuseCommandTest2.pdb junction=false name=root.fused renumber=count");
	    int seqNum2 = app.getGraphController().getSequences().getSequenceCount();
	    System.out.println("Number of sequences after fusing: " + seqNum2);
	    if (seqNum2 <= seqNum1) {
		System.err.println("Number of sequences did not increase after fusing!");
	    }
	    assert seqNum2 > seqNum1;
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	} catch(CommandException e) {
	    System.out.println("Command exception in import command test: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Reads imperfect, generates helix constraints corresponding to perfectly symmetric ring
     * @deprecated Test taken out because ring structure is not valid*/
    
    /*
    @Test (groups={"new"})
    public void testRingFuseCommand3(){
	String methodName = "testRingFuseCommand3";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("controller import-stem-length-min=3");
	    assert app.getGraphController().getImportStemLengthMin() == 3;
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1NKW.pdb_j2_0-A1043_0-C1132_chain_ring_1221_L5_rnaview.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/2GYA-nooverlap-nov20_edit_filt.pdb");
	    app.runScriptLine("links"); // show all found links like base pairs
	    app.runScriptLine("echo Found junctions:");
	    app.runScriptLine("tree allowed=StrandJunction3D");
	    // there must be at least one junction!
	    assert Object3DTools.collectByClassName(app.getGraphController().getGraph().getGraph(), 
						    "StrandJunction3D").size() > 0;
	    int seqNum1 = app.getGraphController().getSequences().getSequenceCount();
	    System.out.println("Number of sequences before fusing: " + seqNum1);
	    app.runScriptLine("tree strands");
	    app.runScriptLine("ringfuse root");
	    app.runScriptLine("renumber all");
	    app.runScriptLine("echo Fused strands:");
	    app.runScriptLine("tree strands");
	    app.runScriptLine("exportpdb " + methodName + ".pdb junction=false name=root.fused renumber=count");
	    int seqNum2 = app.getGraphController().getSequences().getSequenceCount();
	    System.out.println("Number of sequences after fusing: " + seqNum2);
	    if (seqNum2 <= seqNum1) {
		System.err.println("Number of sequences did not increase after fusing!");
	    }
	    assert seqNum2 > seqNum1;
	} catch(CommandException e) {
	    System.out.println("Command exception in import command test: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }
    */
}
