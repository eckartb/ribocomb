package nanotilertests.rnadesign.designapp;

import commandtools.CommandException;
import java.util.Properties;
import generaltools.TestTools;
import org.testng.annotations.*;
import rnadesign.designapp.*;

public class ScoreFitCommandTest {

    public ScoreFitCommandTest() { }

    @Test(groups={"incomplete"})
    public void testScoreFit1() {
	String methodName = "testScoreFit1";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/2GYA.pdb_j2_0-G539_0-G553_chain_ring.1FFK.pdb_k2_0-U55_0-C83_1221_L75.pdb_fixed_ABKL.pdb");
	    app.runScriptLine("tree");
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("loadjunctions ${NANOTILER_HOME}/test/fixtures/helix_bridge.names ${NANOTILER_HOME}/test/fixtures 2"); // only one entry, but that one should fit
	    app.runScriptLine("status");
// 	    String name1 = "root.import.stems.stemA_B_1.B_7_A_1";
// 	    String name2 = "root.import.stems.stemK_L_2.K_7_L_1";
	    String name1 = "root.import.import_cov_jnc.j1.A_1_B_7";
	    String name2 = "root.import.import_cov_jnc.j2.D_1_C_7";
	    Properties resultProperties = app.runScriptLine("scorefit " + name1 + " " + name2);
	    System.out.println("Result of " + methodName + " : " + resultProperties);
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    // app.runScriptLine("bridgeit root.pdb.B.C1 root.pdb.A.G21");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_hist.txt");
	}
	catch (CommandException ce) {
	    System.out.println("Error in " + methodName + " : " + ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }
    
}
