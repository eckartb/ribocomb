package nanotilertests.rnadesign.designapp;

import java.io.PrintStream;

import tools3d.objects3d.Object3D;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandException;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import generaltools.TestTools;
import rnadesign.designapp.*;

import org.testng.annotations.*; // for testing

public class SelectCommandTest {

    @Test (groups={"new"})
    /** Tests signature command. Not completely implemented! TODO */
    public void testSelect(){
	String methodName = "testSelect";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("echo Running " + methodName);
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/testOptimizeHelicesCommand.pdb name=pdb");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    // testing for BranchDescriptor3D under nameroot.import_cov_jnc.j1.C_1_A_9 1.3.1.4
	    String fullName = "root.pdb.1"; // pdb_cov_jnc.j1.C_1_A_9";
	    Object3D obj1 = app.getGraphController().getGraph().findByFullName(fullName);
	    //	    Object3D obj2 = app.getGraphController().getGraph().findByFullName("1.1.3.1.4");
	    assert obj1 != null;
	    //  assert obj1 == obj2;
	    assert obj1.getFullName().equals(fullName);
	    System.out.println("Found object with full name: " + obj1.getFullName());
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandException e) {
	    System.out.println(e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test (groups={"new"})
    /** Tests signature command. Not completely implemented! */
    public void testSelectNoRoot(){
	String methodName = "testSelectNoRoot";
	// System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("source ${NANOTILER_HOME}/resources/defaults.script");
	    app.runScriptLine("genhelix"); // import ${NANOTILER_HOME}/test/fixtures/testOptimizeHelicesCommand.pdb name=pdb");
	    // app.runScriptLine("tree"); // allowed=BranchDescriptor3D");
	    // testing for BranchDescriptor3D under nameroot.import_cov_jnc.j1.C_1_A_9 1.3.1.4
	    String fullName = ".helix_root.helix_back"; // .pdb.pdb_cov_jnc.j1.C_1_A_9";
	    String fullName2 = "root.helix_root.helix_back"; // root.pdb.pdb_cov_jnc.j1.C_1_A_9";
	    Object3D obj1 = app.getGraphController().getGraph().findByFullName(fullName);
	    //	    Object3D obj2 = app.getGraphController().getGraph().findByFullName("1.1.3.1.4");
	    assert obj1 != null;
	    //  assert obj1 == obj2;
	    // System.out.println("Found object with full name: " + obj1.getFullName() + " expecting: " + fullName2);
	    assert obj1.getFullName().equals(fullName2);

	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandException e) {
	    System.out.println(e.getMessage());
	    assert false;
	}
	// System.out.println(TestTools.generateMethodFooter(methodName));
    }

}
