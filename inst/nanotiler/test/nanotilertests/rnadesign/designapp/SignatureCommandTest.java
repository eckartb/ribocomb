package nanotilertests.rnadesign.designapp;

import java.io.PrintStream;

import generaltools.TestTools;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandException;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import rnadesign.designapp.*;

import org.testng.annotations.*; // for testing

public class SignatureCommandTest {

    @Test (groups={"new"})
    /** Tests signature command. Not completely implemented! TODO */
    public void testSignature(){
	String methodName = "testSignature";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/triangle.points");
	    app.runScriptLine("signature root.import");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandException e) {
	    System.out.println(e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test (groups={"new"})
    /** Tests signature command. Not completely implemented! TODO */
    public void testSignature2(){
	String methodName = "testSignature2";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1P9X.pdb_j2_0-C2631_0-G2643_chain_ring_1221_L6_nohelices.pdb.fixed.fused.pdb");
	    app.runScriptLine("tree strands");
	    app.runScriptLine("signature root.import");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandException e) {
	    System.out.println(e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

}
