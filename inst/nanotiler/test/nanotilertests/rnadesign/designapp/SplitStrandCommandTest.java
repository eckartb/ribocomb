package nanotilertests.rnadesign.designapp;

import org.testng.*;
import org.testng.annotations.*;
import generaltools.*;

import commandtools.*;

import rnadesign.designapp.*;

public class SplitStrandCommandTest {

    /** reads a tRNA strand,splits it and writes out the result */
    @Test(groups={"new", "new"})
    public void testSplitStrandCommand1() {
	String methodName = "testSplitStrandsCommand1";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	int split = 12;
	try{
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1LDZ_model1.pdb findstems=false");
	    app.runScriptLine("echo Original strand(s):");
	    app.runScriptLine("tree strands");
	    int origLength = app.getGraphController().getSequences().getSequence("A").size();
	    assert origLength > 0;
	    app.runScriptLine("splitstrand root.import.A " + split);
	    app.runScriptLine("echo Strand(s) after split:");
	    app.runScriptLine("tree strands");
	    app.runScriptLine("exportpdb testSplitStrandCommand1.pdb");
	    assert app.getGraphController().getSequences().getSequenceCount() == 2;
	    int len1 = app.getGraphController().getSequences().getSequence("A").size();
	    int len2 = app.getGraphController().getSequences().getSequence("A_split").size();
	    assert len1 + 1 == split;
	    assert  len1 + len2 == origLength;
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	} catch(CommandException e) {
	    System.out.println("Command exception in split strand command test: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }
    
}
