package nanotilertests.rnadesign.designapp;

import generaltools.TestTools;
import commandtools.*;
import org.testng.annotations.*;
import rnadesign.designapp.*;

public class ToeholdCommandTest {

    /** Imoports one cube corner and attempts to fuse its strands */
    @Test(groups={"new"})
    public void nanoTriangle() {
	String methodName = "nanoTriangle";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/4phy_triangle%1,1,1,2,2_fuse.pdb");
	    assert app.getGraphController().getSequences().getSequenceCount() == 2; // 2 strands before fusing
	    app.runScriptLine("secondary");
	    app.runScriptLine("toehold cutoff=5");
	    app.runScriptLine("exportpdb " + methodName + "_toehold.pdb");
	    System.out.println("The secondary structure and sequences should be different compared to before the start of the algorithm:");
	    app.runScriptLine("secondary");
	    assert app.getGraphController().getSequences().getSequenceCount() == 2; // 3 strands after fusing
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	} catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Imoports one cube corner and attempts to fuse its strands */
    @Test(groups={"new"})
    public void nanoTriangle10BPPipeline() {
	String methodName = "nanoTriangle10BPPipeline";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {

	    /** search for ring */

	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("loadjunctions  ${NANOTILER_HOME}/test/fixtures/4phy.names");
	    app.runScriptLine("status");
	    app.runScriptLine("growscan ring-export-limit=30 gen=3 bp=25");
	    app.runScriptLine("clear all");
	    String foundRingFile = "grow_ring%1,1,1,2,2.pdb";

	    /** fix ring closure */

	    // like script "ringfix.script":
	    app.runScriptLine("controller import-stem-length-min=3");
	    app.runScriptLine("import " + foundRingFile + " findstems=true");
	    // # generate constraints that correspond to a ring formed by junctions
	    app.runScriptLine("ringfixconstraints root");
	    // # echo Generated ring constraints:
	    // # links
	    app.runScriptLine("opthelices helices=true steps=100000");
	    // # move junctions to new subtree
	    app.runScriptLine("move root.import.import_cov_jnc root");
	    // # remove original data
	    app.runScriptLine("remove root.import");
	    // # only export the junctions and the newly generated helices
	    String fixedFileName = methodName + "_fixed.pdb";
	    String fusedFileName = methodName + "_fused.pdb";
	    app.runScriptLine("exportpdb " + fixedFileName);

	    app.runScriptLine("clear all");

	    /** fusing */
	    app.runScriptLine("import " + fixedFileName);
	    app.runScriptLine("fuseallstrands");
	    app.runScriptLine("exportpdb " + fusedFileName);

	    app.runScriptLine("clear all");
	    
	    /** toehold */
 	    app.runScriptLine("import " + fusedFileName);
	    assert app.getGraphController().getSequences().getSequenceCount() == 2;
 	    app.runScriptLine("secondary");
 	    app.runScriptLine("toehold cutoff=5");
 	    app.runScriptLine("exportpdb " + methodName + "_toehold.pdb");
 	    System.out.println("The secondary structure and sequences should be different compared to before the start of the algorithm:");
 	    app.runScriptLine("secondary");
 	    assert app.getGraphController().getSequences().getSequenceCount() == 2;
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	} catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Imoports one cube corner and attempts to fuse its strands */
    @Test(groups={"new"})
    public void nanoTriangle11BPPipeline() {
	String methodName = "nanoTriangle11BPPipeline";
	System.out.println(TestTools.generateMethodHeader(methodName));
	NanoTilerScripter app = new NanoTilerScripter();
	try {

	    /** search for ring */

	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("loadjunctions  ${NANOTILER_HOME}/test/fixtures/4phy.names");
	    app.runScriptLine("status");
	    app.runScriptLine("growscan ring-export-limit=50 gen=3 bp=25");

	    app.runScriptLine("clear all");

	    /** fix ring closure */

	    String foundRingFile = "grow_ring%1,1,1,2,3_nohelices.pdb";
	    // like script "ringfix.script":
	    app.runScriptLine("controller import-stem-length-min=3");
	    app.runScriptLine("import " + foundRingFile + " findstems=true");
	    // # generate constraints that correspond to a ring formed by junctions
	    app.runScriptLine("ringfixconstraints root");
	    // # echo Generated ring constraints:
	    // # links
	    app.runScriptLine("opthelices helices=true steps=3000000");
	    // # move junctions to new subtree
	    app.runScriptLine("move root.import.import_cov_jnc root");
	    // # remove original data
	    app.runScriptLine("remove root.import");
	    // # only export the junctions and the newly generated helices
	    String fixedFileName = methodName + "_fixed.pdb";
	    String fusedFileName = methodName + "_fused.pdb";
	    app.runScriptLine("exportpdb " + fixedFileName);
	    app.runScriptLine("clear all");

	    /** fusing */
	    app.runScriptLine("import " + fixedFileName);
	    app.runScriptLine("fuseallstrands cutoff=9");
	    app.runScriptLine("exportpdb " + fusedFileName);

	    app.runScriptLine("clear all");
	    
	    /** toehold */
 	    app.runScriptLine("import " + fusedFileName);
	    // assert app.getGraphController().getSequences().getSequenceCount() == 2;
 	    app.runScriptLine("secondary");
 	    app.runScriptLine("toehold cutoff=5");
	    app.runScriptLine("removephosphates"); // removes phosphate groups from all 5' ends of sequences
 	    app.runScriptLine("exportpdb " + methodName + "_toehold.pdb");
 	    System.out.println("The secondary structure and sequences should be different compared to before the start of the algorithm:");
 	    app.runScriptLine("secondary");
 	    assert app.getGraphController().getSequences().getSequenceCount() == 2;
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	} catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


}
