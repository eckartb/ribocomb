package nanotilertests.rnadesign.designapp;

import tools3d.objects3d.*;
import generaltools.TestTools;
import java.io.PrintStream;
import java.math.BigInteger;
import java.util.List;
import java.util.ArrayList;
import java.util.Properties;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import rnadesign.rnamodel.DBElementDescriptor;
import rnadesign.rnamodel.DBElementConnectionDescriptor;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandException;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import tools3d.Vector3D;
import rnadesign.designapp.*;

import org.testng.annotations.*; // for testing

public class TraceRnaGraphCommandTest {

    public TraceRnaGraphCommandTest() { }

    /** tests building a triangular structure out of one building block */
    @Test (groups={"new","slow"})
    public void testTraceRnaGraphCommand1(){
	String methodName = "testTraceRnaGraphCommand1";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/triangle.points scale=3 name=newgraph");
	    app.runScriptLine("tree");
	    Properties optProperties = app.runScriptLine("tracernagraph root=root.newgraph error=5 verbose=3");
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of Trace RNA graph command: " + optProperties);
	    app.runScriptLine("tree strands");
	    // make sure that some RNA has been generated:
	    assert app.getGraphController().getSequences().getSequenceCount() > 0;
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a triangular structure out of one building block */
    @Test (groups={"new", "slow"})
    public void testTraceRnaGraphCommandTriangle(){
	String methodName = "testTraceRnaGraphCommandTriangle";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/triangle.points scale=1 name=newgraph");
	    app.runScriptLine("tree");
	    app.runScriptLine("optrnagraph root=root.newgraph");
	    app.runScriptLine("tree");
	    Properties optProperties = app.runScriptLine("tracernagraph root=root.newgraph verbose=1 error=30 offset=12");
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of Trace RNA graph command: " + optProperties);
	    app.runScriptLine("tree strands");
	    // make sure that some RNA has been generated:
	    assert app.getGraphController().getSequences().getSequenceCount() > 0;
	    app.runScriptLine("exportpdb " + methodName + "_traced.pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


    /** tests building a triangular structure out of one building block */
    @Test (groups={"new", "slow"})
    public void testTraceRnaGraphCommandSquare(){
	String methodName = "testTraceRnaGraphCommandSquare";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("genshape ring4 length=40");
	    app.runScriptLine("tree");
	    Properties properties = app.runScriptLine("tracernagraph root=root.ring4 bpmax=6");
	    app.runScriptLine("tree");
	    // Properties properties = app.runScriptLine("tracernagraph root=root.newgraph verbose=1 error=30 offset=12");
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of Trace RNA graph command: " + properties);
	    app.runScriptLine("tree strands");
	    app.runScriptLine("tree allowed=BranchDescriptor3D");
	    // make sure that some RNA has been generated:
	    assert app.getGraphController().getSequences().getSequenceCount() > 0;
	    app.runScriptLine("exportpdb " + methodName + "_traced.pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a triangular structure out of one building block */
    @Test (groups={"new", "slow"})
    public void testTraceRnaGraphCommandOctahedron(){
	String methodName = "testTraceRnaGraphCommandOctahedron";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("genshape octahedron length=100 name=opt");
	    app.runScriptLine("tree");
	    app.runScriptLine("optrnagraph root=root.opt error=20 stems=200000");
	    app.runScriptLine("tree");
	    Properties optProperties = app.runScriptLine("tracernagraph root=root.opt steps=200000 verbose=1 error=60");
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of Trace RNA graph command: " + optProperties);
	    // app.runScriptLine("tree strands");
	    // make sure that some RNA has been generated:
	    assert app.getGraphController().getSequences().getSequenceCount() > 0;
	    app.runScriptLine("exportpdb " + methodName + "_traced.pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in testTraceRnaGraphCommand2: " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }
    

    /** tests building a triangular structure out of one building block */
    @Test (groups={"new","slow"})
    public void testTraceRnaGraphCommandTetrahedron(){
	String methodName = "testTraceRnaGraphCommandTetrahedron";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("genshape tetrahedron length=100 name=opt");
	    app.runScriptLine("tree");
	    app.runScriptLine("optrnagraph root=root.opt error=20 stems=200000");
	    app.runScriptLine("tree");
	    Properties optProperties = app.runScriptLine("tracernagraph root=root.opt steps=200000 verbose=1 error=60 offset=12");
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of Trace RNA graph command: " + optProperties);
	    // app.runScriptLine("tree strands");
	    // make sure that some RNA has been generated:
	    assert app.getGraphController().getSequences().getSequenceCount() > 0;
	    app.runScriptLine("exportpdb " + methodName + "_traced.pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testTraceRnaGraphCommand2: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a triangular structure out of one building block */
    @Test (groups={"new", "slow"})
    public void testTraceRnaGraphCommandCube(){
	String methodName = "testTraceRnaGraphCommandCube";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("genshape cube length=100 name=opt");
	    app.runScriptLine("tree");
	    app.runScriptLine("echo Optimizing graph edges...");
	    app.runScriptLine("optrnagraph root=root.opt steps=300000 verbose=2 error=5");
	    app.runScriptLine("tree");
	    Properties optProperties = app.runScriptLine("tracernagraph root=root.opt verbose=1 error=40 steps=2000000");
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of Trace RNA graph command: " + optProperties);
	    // app.runScriptLine("tree strands");
	    // make sure that some RNA has been generated:
	    assert app.getGraphController().getSequences().getSequenceCount() > 0;
	    app.runScriptLine("exportpdb " + methodName + "_traced.pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testTraceRnaGraphCommand2: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Test using small cube structure */
    @Test (groups={"new", "slow"})
    public void testTraceRnaGraphCommandSmallCube(){
	String methodName = "testTraceRnaGraphCommandSmallCube";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("genshape cube length=50 name=opt");
	    app.runScriptLine("tree");
	    app.runScriptLine("echo Optimizing graph edges...");
	    app.runScriptLine("optrnagraph root=root.opt steps=300000 verbose=2 error=2.5 offset=11.5");
	    app.runScriptLine("tree");
	    Properties optProperties = app.runScriptLine("tracernagraph root=root.opt verbose=1 error=40 steps=500000 offset=11.5");
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of Trace RNA graph command: " + optProperties);
	    // app.runScriptLine("tree strands");
	    // make sure that some RNA has been generated:
	    assert app.getGraphController().getSequences().getSequenceCount() > 0;
	    app.runScriptLine("exportpdb " + methodName + "_traced.pdb");
	    app.runScriptLine("ringfuse");
	    String fusedName = methodName + "_traced_fused.pdb";
	    app.runScriptLine("echo Exporting fused structure to file " + fusedName);
	    app.runScriptLine("exportpdb " + fusedName + " name=root.fused");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testTraceRnaGraphCommand2: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a triangular structure out of one building block */
    @Test (groups={"new", "slow"})
    public void testTraceRnaGraphCommandSmallCubeLong(){
	String methodName = "testTraceRnaGraphCommandSmallCubeLong";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("genshape cube length=50 name=opt");
	    app.runScriptLine("tree");
	    app.runScriptLine("echo Optimizing graph edges...");
	    app.runScriptLine("optrnagraph root=root.opt steps=300000 verbose=2 error=5");
	    app.runScriptLine("tree");
	    Properties optProperties = app.runScriptLine("tracernagraph root=root.opt verbose=1 error=40 steps=400000");
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of Trace RNA graph command: " + optProperties);
	    // app.runScriptLine("tree strands");
	    // make sure that some RNA has been generated:
	    assert app.getGraphController().getSequences().getSequenceCount() > 0;
	    app.runScriptLine("exportpdb " + methodName + "_traced.pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in testTraceRnaGraphCommand2: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a triangular structure out of one building block */
    @Test (groups={"new", "slow"})
    public void testTraceRnaGraphCommandDodecahedron(){
	String methodName = "testTraceRnaGraphCommandDodecahedron";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("genshape dodecahedron length=100 name=opt");
	    app.runScriptLine("tree");
	    app.runScriptLine("optrnagraph root=root.opt error=20 stems=200000");
	    app.runScriptLine("tree");
	    Properties optProperties = app.runScriptLine("tracernagraph root=root.opt steps=200000 verbose=1 error=60");
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of Trace RNA graph command: " + optProperties);
	    // app.runScriptLine("tree strands");
	    // make sure that some RNA has been generated:
	    assert app.getGraphController().getSequences().getSequenceCount() > 0;
	    app.runScriptLine("exportpdb " + methodName + "_traced.pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a triangular structure out of one building block */
    @Test (groups={"new","slow"})
    public void testTraceRnaGraphCommandIcosahedron(){
	String methodName = "testTraceRnaGraphCommandIcosahedron";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("genshape icosahedron length=100 name=opt");
	    app.runScriptLine("tree");
	    app.runScriptLine("optrnagraph root=root.opt error=20 stems=200000");
	    app.runScriptLine("tree");
	    Properties optProperties = app.runScriptLine("tracernagraph root=root.opt steps=200000 verbose=1 error=60");
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of Trace RNA graph command: " + optProperties);
	    // app.runScriptLine("tree strands");
	    // make sure that some RNA has been generated:
	    assert app.getGraphController().getSequences().getSequenceCount() > 0;
	    app.runScriptLine("exportpdb " + methodName + "_traced.pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


}
