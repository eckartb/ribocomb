package nanotilertests.rnadesign.designapp;

import tools3d.objects3d.*;
import generaltools.TestTools;
import java.io.PrintStream;
import java.io.File;
import java.math.BigInteger;
import java.util.List;
import java.util.ArrayList;
import java.util.Properties;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import rnadesign.rnamodel.DBElementDescriptor;
import rnadesign.rnamodel.DBElementConnectionDescriptor;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandException;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import tools3d.Vector3D;
import rnadesign.designapp.*;

import org.testng.annotations.*; // for testing

public class TraceRnaGraphScriptCommandTest {

    public TraceRnaGraphScriptCommandTest() { }

    /** tests building a simple corner out of two helices */
    @Test (groups={"new","slow"})
    public void testTraceRnaGraphScriptCommandCorner() {
	String methodName = "testTraceRnaGraphScriptCommandCorner";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    //   app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/triangle.points scale=1 name=newgraph");
	    app.runScriptLine("rmlink 3");
	    app.runScriptLine("tree");
	    app.runScriptLine("links");
	    String fileName = methodName + ".script";
	    Properties optProperties = app.runScriptLine("tracegraphscript graph=root.newgraph.p0;root.newgraph.p1;root.newgraph.p2 name=corner file=" + fileName);
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of Trace RNA graph command: ");
	    String output = optProperties.getProperty("output");
	    assert(output != null);
	    System.out.println(output);
	    File file = new File(fileName);
	    assert( file.exists());
	    app.runScriptLine("source " + fileName);
	    app.runScriptLine("tree strand");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a simple corner out of two helices */
    @Test (groups={"new","slow"})
    public void testTraceRnaGraphScriptCommandCorner90() {
	String methodName = "testTraceRnaGraphScriptCommandCorner";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    //   app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/corner90.points scale=1 name=newgraph");
	    app.runScriptLine("tree");
	    app.runScriptLine("links");
	    String fileName = methodName + ".script";
	    Properties optProperties = app.runScriptLine("tracegraphscript graph=root.newgraph.p0;root.newgraph.p1;root.newgraph.p2 name=corner file=" + fileName);
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of Trace RNA graph command: ");
	    String output = optProperties.getProperty("output");
	    assert(output != null);
	    System.out.println(output);
	    File file = new File(fileName);
	    assert( file.exists());
	    app.runScriptLine("source " + fileName);
	    app.runScriptLine("tree strand");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a simple corner out of two helices using 90 degree rotational symmetry */
    @Test (groups={"new","slow"})
    public void testTraceRnaGraphScriptCommandCorner90Sym() {
	String methodName = "testTraceRnaGraphScriptCommandCorner90Sym";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    //   app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/corner90.points scale=5 name=newgraph");
	    app.runScriptLine("synth cs cs1 root 0 0 0");
	    app.runScriptLine("select root.cs1");
	    app.runScriptLine("rotate 90 0 0 1");
	    app.runScriptLine("symadd root.cs1");
	    app.runScriptLine("syminfo");
	    app.runScriptLine("tree");
	    app.runScriptLine("links");
	    String fileName = methodName + ".script";
	    Properties optProperties = app.runScriptLine("tracegraphscript graph=root.newgraph.p0;root.newgraph.p1;root.newgraph.p2 name=corner90s sym=1,3,1,2,1 max=20 file=" + fileName);
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of Trace RNA graph command: ");
	    String output = optProperties.getProperty("output");
	    assert(output != null);
	    System.out.println(output);
	    File file = new File(fileName);
	    assert( file.exists());
	    app.runScriptLine("source " + fileName);
	    app.runScriptLine("tree strand");
	    app.runScriptLine("clone root.corner90s.helix0_root root.corner90s helix1_root");
 	    app.runScriptLine("symapply root.corner90s.helix1_root 1");
	    app.runScriptLine("deselect");
 	    app.runScriptLine("rename root.corner90s.helix1_root.helix0_forw helix1_forw");
	    app.runScriptLine("deselect");
 	    app.runScriptLine("rename root.corner90s.helix1_root.helix0_back helix1_back");
	    app.runScriptLine("deselect");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a simple corner out of two helices using 90 degree rotational symmetry */
    @Test (groups={"new","slow"})
    public void testTraceRnaGraphScriptCommandDualCorner90Sym() {
	String methodName = "testTraceRnaGraphScriptCommandDualCorner90Sym";
	String shortName = "dualcorner90s";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    //   app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/dualcorner90.points scale=5 name=newgraph");
	    app.runScriptLine("synth cs cs1 root 0 0 0");
	    app.runScriptLine("select root.cs1");
	    app.runScriptLine("rotate 90 0 0 1");
	    app.runScriptLine("symadd root.cs1");
	    app.runScriptLine("synth cs cs2 root 0 0 0");
	    app.runScriptLine("select root.cs2");
	    app.runScriptLine("rotate 180 0 0 1");
	    app.runScriptLine("symadd root.cs2");
	    app.runScriptLine("syminfo");
	    app.runScriptLine("tree");
	    app.runScriptLine("links");
	    app.runScriptLine("deselect");
	    String fileName = methodName + ".script";
	    Properties optProperties = app.runScriptLine("tracegraphscript graph=root.newgraph.p0;root.newgraph.p1;root.newgraph.p2;root.newgraph.p3 name="
							 + shortName + " sym=2,3,1,2,1;3,4,1,2,2 max=20 file=" + fileName);
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of Trace RNA graph command: ");
	    String output = optProperties.getProperty("output");
	    assert(output != null);
	    System.out.println(output);
	    File file = new File(fileName);
	    assert( file.exists());
	    app.runScriptLine("source " + fileName);
	    app.runScriptLine("tree strand");
	    app.runScriptLine("deselect");
	    app.runScriptLine("clone root." + shortName + ".helix0_root root." + shortName + " helix1_root");
	    app.runScriptLine("clone root." + shortName + ".helix0_root root." + shortName + " helix2_root");
 	    app.runScriptLine("symapply root." + shortName + ".helix1_root 1");
 	    app.runScriptLine("symapply root." + shortName + ".helix2_root 2");
	    app.runScriptLine("deselect");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a simple corner out of two helices using 90 degree rotational symmetry */
    @Test (groups={"new","slow"})
    public void testTraceRnaGraphScriptCommandSquareSym() {
	String methodName = "testTraceRnaGraphScriptCommandSquareSym";
	String shortName = "squareSym";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    //   app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/square.points scale=2 name=newgraph");
	    app.runScriptLine("synth cs cs1 root 0 0 0");
	    app.runScriptLine("select root.cs1");
	    app.runScriptLine("rotate 90 0 0 1");
	    app.runScriptLine("symadd root.cs1");
	    app.runScriptLine("synth cs cs2 root 0 0 0");
	    app.runScriptLine("select root.cs2");
	    app.runScriptLine("rotate 180 0 0 1");
	    app.runScriptLine("symadd root.cs2");
	    app.runScriptLine("synth cs cs3 root 0 0 0");
	    app.runScriptLine("select root.cs3");
	    app.runScriptLine("rotate 270 0 0 1");
	    app.runScriptLine("symadd root.cs3");
	    app.runScriptLine("syminfo");
	    app.runScriptLine("tree");
	    app.runScriptLine("links");
	    app.runScriptLine("deselect");
	    String fileName = methodName + ".script";
	    Properties optProperties = app.runScriptLine("tracegraphscript graph=root.newgraph.p0;root.newgraph.p1;root.newgraph.p2;root.newgraph.p3 name="
							 + shortName + " sym=2,3,1,2,1;3,4,1,2,2;4,1,1,2,3 max=20 file=" + fileName);
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of Trace RNA graph command: ");
	    String output = optProperties.getProperty("output");
	    assert(output != null);
	    System.out.println(output);
	    File file = new File(fileName);
	    assert( file.exists());
	    app.runScriptLine("source " + fileName);
	    app.runScriptLine("tree strand");
	    app.runScriptLine("deselect");
	    app.runScriptLine("clone root." + shortName + ".helix0_root root." + shortName + " helix1_root");
	    app.runScriptLine("clone root." + shortName + ".helix0_root root." + shortName + " helix2_root");
	    app.runScriptLine("clone root." + shortName + ".helix0_root root." + shortName + " helix3_root");
 	    app.runScriptLine("symapply root." + shortName + ".helix1_root 1");
 	    app.runScriptLine("symapply root." + shortName + ".helix2_root 2");
 	    app.runScriptLine("symapply root." + shortName + ".helix3_root 3");
	    app.runScriptLine("deselect");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a star shaped 3-way junction */
    @Test (groups={"new","slow"})
    public void testTraceRnaGraphScriptCommandStar3() {
	String methodName = "testTraceRnaGraphScriptCommandStar3";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    //   app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/star3.points scale=1 name=newgraph");
	    app.runScriptLine("tree");
	    app.runScriptLine("links");
	    String fileName = methodName + ".script";
	    Properties optProperties = app.runScriptLine("tracegraphscript graph=root.newgraph.p0;root.newgraph.p1;root.newgraph.p2;root.newgraph.p3 name=star3 file=" + fileName);
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of Trace RNA graph command: ");
	    String output = optProperties.getProperty("output");
	    assert(output != null);
	    System.out.println(output);
	    File file = new File(fileName);
	    assert( file.exists());
	    app.runScriptLine("source " + fileName);
	    app.runScriptLine("tree strand");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a star shaped 3-way junction */
    @Test (groups={"new","slow"})
    public void testTraceRnaGraphScriptCommandStar3Sym() {
	String methodName = "testTraceRnaGraphScriptCommandStar3Sym";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    //   app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/star3.points scale=1 name=newgraph");
	    app.runScriptLine("tree");
	    app.runScriptLine("links");
	    String fileName = methodName + ".script";
	    app.runScriptLine("synth cs cs1 root 0 0 0");
	    app.runScriptLine("synth cs cs2 root 0 0 0");
	    app.runScriptLine("select root.cs1");
	    app.runScriptLine("rotate 120 0 0 1");
	    app.runScriptLine("select root.cs2");
	    app.runScriptLine("rotate 240 0 0 1");
	    app.runScriptLine("symadd root.cs1");
	    app.runScriptLine("symadd root.cs2");
	    Properties optProperties = app.runScriptLine("tracegraphscript graph=root.newgraph.p0;root.newgraph.p1;root.newgraph.p2;root.newgraph.p3 name=star3 sym=1,3,1,2,1;1,4,1,2,2 file=" + fileName);
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of Trace RNA graph command: ");
	    String output = optProperties.getProperty("output");
	    assert(output != null);
	    System.out.println(output);
	    File file = new File(fileName);
	    assert( file.exists());
	    app.runScriptLine("source " + fileName);
	    app.runScriptLine("tree strand");
	    app.runScriptLine("clone root.star3.helix0_root root.star3 helix1_root");
	    app.runScriptLine("clone root.star3.helix0_root root.star3 helix2_root");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("symapply root.star3.helix1_root 1");
	    app.runScriptLine("symapply root.star3.helix2_root 2");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a triangular structure out of 3 helices */
    @Test (groups={"new","slow"})
    public void testTraceRnaGraphScriptCommandTriangle() {
	String methodName = "testTraceRnaGraphScriptCommandTriangle";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    //   app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/triangle.points scale=1 name=newgraph");
	    app.runScriptLine("tree");
	    app.runScriptLine("links");
	    String fileName = methodName + ".script";
	    Properties optProperties = app.runScriptLine("tracegraphscript graph=root.newgraph name=triangle file=" + fileName);
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of Trace RNA graph command: ");
	    String output = optProperties.getProperty("output");
	    assert(output != null);
	    System.out.println(output);
	    File file = new File(fileName);
	    assert( file.exists());
	    app.runScriptLine("source " + fileName);
	    app.runScriptLine("tree strand");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a triangular structure out of 3 helices */
    @Test (groups={"new","slow"})
    public void testTraceRnaGraphScriptCommandTriangleSym() {
	String methodName = "testTraceRnaGraphScriptCommandTriangle";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    //   app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/triangle.points scale=0.55 name=newgraph");
	    app.runScriptLine("synth cs cs1 root 0 0 0");
	    app.runScriptLine("synth cs cs2 root 0 0 0");
	    app.runScriptLine("select root.cs1");
	    app.runScriptLine("rotate 120 0 0 1");
	    app.runScriptLine("select root.cs2");
	    app.runScriptLine("rotate 240 0 0 1");
	    app.runScriptLine("symadd root.cs1");
	    app.runScriptLine("symadd root.cs2");
	    app.runScriptLine("tree");
	    app.runScriptLine("links");
	    String fileName = methodName + ".script";
	    Properties optProperties = app.runScriptLine("tracegraphscript graph=root.newgraph.p0;root.newgraph.p1;root.newgraph.p2 name=triangle sym=2,3,1,2,1;3,1,1,2,2 file=" + fileName);
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of Trace RNA graph command: ");
	    String output = optProperties.getProperty("output");
	    assert(output != null);
	    System.out.println(output);
	    File file = new File(fileName);
	    assert( file.exists());
	    app.runScriptLine("source " + fileName);
	    app.runScriptLine("tree strand");
	    app.runScriptLine("clone root.triangle.helix0_root root.triangle helix1_root");
	    app.runScriptLine("clone root.triangle.helix0_root root.triangle helix2_root");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("symapply root.triangle.helix1_root 1");
	    app.runScriptLine("symapply root.triangle.helix2_root 2");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");
	    app.runScriptLine("tree strand");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a square structure out of four helices */
    @Test (groups={"new","slow"})
    public void testTraceRnaGraphScriptCommandSquare(){
	String methodName = "testTraceRnaGraphScriptCommandSquare";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    // app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/square.points scale=1 name=newgraph");
	    app.runScriptLine("tree");
	    app.runScriptLine("links");
	    String fileName = methodName + ".script";
	    Properties optProperties = app.runScriptLine("tracegraphscript graph=root.newgraph alg=helix name=square file=" + fileName);
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of Trace RNA graph command: ");
	    String output = optProperties.getProperty("output");
	    assert(output != null);
	    System.out.println(output);
	    File file = new File(fileName);
	    assert( file.exists());
	    app.runScriptLine("source " + fileName);
	    app.runScriptLine("tree strand");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a square structure out of four helices */
    @Test (groups={"new","slow"})
    public void testTraceRnaGraphScriptCommandDBSquare(){
	String methodName = "testTraceRnaGraphScriptCommandDBSquare";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    // app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/square.points scale=1 name=newgraph");
	    app.runScriptLine("tree");
	    app.runScriptLine("links");
	    String fileName = methodName + ".script";
	    Properties optProperties = app.runScriptLine("tracegraphscript graph=root.newgraph alg=db name=square file=" + fileName);
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of Trace RNA graph command: ");
	    String output = optProperties.getProperty("output");
	    assert(output != null);
	    System.out.println(output);
	    File file = new File(fileName);
	    assert( file.exists());
	    app.runScriptLine("source " + fileName);
	    app.runScriptLine("tree strand");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a square structure out of four helices */
    @Test (groups={"new","slow"})
    public void testTraceRnaGraphScriptCommandDBPolygon(){
	String methodName = "testTraceRnaGraphScriptCommandDBPolygon";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	int numSides = 3;
	try{
	    // app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/square.points scale=1 name=newgraph");
	    app.runScriptLine("genshape ring" + numSides +" length=50 name=polygon");
	    app.runScriptLine("tree");
	    app.runScriptLine("links");
	    String fileName = methodName + ".script";
	    // app.runScriptLine("loadjunctions $NANOTILER_HOME/test/fixtures/dimer.names $NANOTILER_HOME/test/fixtures");
	    app.runScriptLine("loadjunctions ../test/fixtures/2BJ2.names");
	    app.runScriptLine("status");
	    Properties optProperties = app.runScriptLine("tracegraphscript graph=root.polygon kl=true alg=db name=polygon file=" + fileName);
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of Trace RNA graph command: ");
	    String output = optProperties.getProperty("output");
	    assert(output != null);
	    System.out.println(output);
	    File file = new File(fileName);
	    assert( file.exists());
	    app.runScriptLine("source " + fileName);
	    app.runScriptLine("tree strand");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a square structure out of four helices */
    @Test (groups={"new","slow"})
    public void testTraceRnaGraphScriptCommandDBPolygonHelix(){
	String methodName = "testTraceRnaGraphScriptCommandDBPolygonHelix";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	int numSides = 4;
	double minDist = 12.0;
	try{
	    // app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    // app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/square.points scale=1 name=newgraph");
	    app.runScriptLine("genshape ring" + numSides +" length=40 name=polygon");
	    app.runScriptLine("tree");
	    app.runScriptLine("links");
	    String fileName = methodName + ".script";
	    // app.runScriptLine("loadjunctions $NANOTILER_HOME/test/fixtures/dimer.names $NANOTILER_HOME/test/fixtures");
	    app.runScriptLine("loadjunctions ../test/fixtures/2BJ2.names");
	    app.runScriptLine("status");
	    Properties optProperties = app.runScriptLine("tracegraphscript graph=root.polygon kl=true alg=helix min=" 
							 + minDist + " name=" + methodName + " file=" + fileName);
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of Trace RNA graph command: ");
	    String output = optProperties.getProperty("output");
	    assert(output != null);
	    System.out.println(output);
	    File file = new File(fileName);
	    assert( file.exists());
	    app.runScriptLine("source " + fileName);
	    app.runScriptLine("tree strand");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a cube structure out of 12 helices */
    @Test (groups={"new","slow"})
    public void testTraceRnaGraphScriptCommandCube(){
	String methodName = "testTraceRnaGraphScriptCommandCube";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    // app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/cube.points scale=1.8 name=newgraph");
	    app.runScriptLine("tree");
	    app.runScriptLine("links");
	    String fileName = methodName + ".script";
	    Properties optProperties = app.runScriptLine("tracegraphscript graph=root.newgraph.p0;root.newgraph.p1;root.newgraph.p2;root.newgraph.p3;root.newgraph.p4;root.newgraph.p5;root.newgraph.p6;root.newgraph.p7 name=cube file=" + fileName);
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of Trace RNA graph command: ");
	    String output = optProperties.getProperty("output");
	    assert(output != null);
	    System.out.println(output);
	    File file = new File(fileName);
	    assert( file.exists());
	    app.runScriptLine("source " + fileName);
	    app.runScriptLine("tree strand");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a cube structure out of 12 helices */
    @Test (groups={"new","slow"})
    public void testTraceRnaGraphScriptCommandCubeSym(){
	String methodName = "testTraceRnaGraphScriptCommandCubeSym";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    // app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/cube.points scale=1.8 name=newgraph");
	    app.runScriptLine("tree");
	    app.runScriptLine("links");
	    String fileName = methodName + ".script";
	    app.runScriptLine("synth cs cs1 root 0 0 0");
	    app.runScriptLine("synth cs cs2 root 0 0 0");
	    app.runScriptLine("synth cs cs3 root 0 0 0");
	    app.runScriptLine("select root.cs1");
	    app.runScriptLine("rotate 90 0 0 1");
	    app.runScriptLine("select root.cs2");
	    app.runScriptLine("rotate 180 0 0 1");
	    app.runScriptLine("select root.cs3");
	    app.runScriptLine("rotate 270 0 0 1");
	    app.runScriptLine("symadd root.cs1");
	    app.runScriptLine("symadd root.cs2");
	    app.runScriptLine("symadd root.cs3");
	    Properties optProperties = app.runScriptLine("tracegraphscript graph=root.newgraph.p0;root.newgraph.p1;root.newgraph.p2;root.newgraph.p3;root.newgraph.p4;root.newgraph.p5;root.newgraph.p6;root.newgraph.p7 name=cube sym=2,3,1,2,1;3,4,1,2,2;4,1,1,2,3;6,7,5,6,1;7,8,5,6,2;8,5,5,6,3;2,6,1,5,1;3,7,1,5,2;4,8,1,5,3 min=6 max=16 file=" + fileName);
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of Trace RNA graph command: ");
	    String output = optProperties.getProperty("output");
	    assert(output != null);
	    System.out.println(output);
	    File file = new File(fileName);
	    assert( file.exists());
	    app.runScriptLine("source " + fileName);
	    app.runScriptLine("tree strand");

 	    app.runScriptLine("clone root.cube.helix0_root root.cube helix0a_root");
 	    app.runScriptLine("clone root.cube.helix0_root root.cube helix0b_root");
 	    app.runScriptLine("clone root.cube.helix0_root root.cube helix0c_root");

 	    app.runScriptLine("clone root.cube.helix4_root root.cube helix4a_root");
 	    app.runScriptLine("clone root.cube.helix4_root root.cube helix4b_root");
 	    app.runScriptLine("clone root.cube.helix4_root root.cube helix4c_root");

 	    app.runScriptLine("clone root.cube.helix8_root root.cube helix8a_root");
 	    app.runScriptLine("clone root.cube.helix8_root root.cube helix8b_root");
 	    app.runScriptLine("clone root.cube.helix8_root root.cube helix8c_root");
 	    app.runScriptLine("tree strand");
 	    app.runScriptLine("symapply root.cube.helix0a_root 1");
 	    app.runScriptLine("symapply root.cube.helix0b_root 2");
 	    app.runScriptLine("symapply root.cube.helix0c_root 3");

 	    app.runScriptLine("symapply root.cube.helix4a_root 1");
 	    app.runScriptLine("symapply root.cube.helix4b_root 2");
 	    app.runScriptLine("symapply root.cube.helix4c_root 3");

 	    app.runScriptLine("symapply root.cube.helix8a_root 1");
 	    app.runScriptLine("symapply root.cube.helix8b_root 2");
 	    app.runScriptLine("symapply root.cube.helix8c_root 3");

 	    app.runScriptLine("tree strand");
	    app.runScriptLine("exportpdb " + methodName + ".pdb");

	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** tests building a tetrahedron out of 6 helices */
    @Test (groups={"new","slow"})
    public void testTraceRnaGraphScriptCommandTetrahedron(){
	String methodName = "testTraceRnaGraphScriptCommandTetrahedron";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    // app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/tetrahedron.points scale=0.9 name=newgraph");
	    app.runScriptLine("tree");
	    app.runScriptLine("links");
	    String fileName = methodName + ".script";
	    // Properties optProperties = app.runScriptLine("tracegraphscript graph=root.newgraph.p0;root.newgraph.p1;root.newgraph.p2;root.newgraph.p3;root.newgraph.p4;root.newgraph.p5 name=tetra file=" + fileName);
	    Properties optProperties = app.runScriptLine("tracegraphscript graph=root.newgraph name=tetra file=" + fileName);
	    // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	    System.out.println("Result of Trace RNA graph command: ");
	    String output = optProperties.getProperty("output");
	    assert(output != null);
	    System.out.println(output);
	    File file = new File(fileName);
	    assert( file.exists());
	    app.runScriptLine("source " + fileName);
	    app.runScriptLine("tree strand");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch(CommandExecutionException e) {
	    System.out.println("Error in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	catch(CommandException e) {
	    System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

}
