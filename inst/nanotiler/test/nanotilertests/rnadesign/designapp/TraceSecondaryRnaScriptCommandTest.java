package nanotilertests.rnadesign.designapp;

import tools3d.objects3d.*;
import java.io.PrintStream;
import java.io.File;
import java.math.BigInteger;
import java.util.List;
import java.util.ArrayList;
import java.util.Properties;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import rnadesign.rnamodel.DBElementDescriptor;
import rnadesign.rnamodel.DBElementConnectionDescriptor;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandException;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import tools3d.Vector3D;
import rnadesign.designapp.*;
import generaltools.*;
import rnasecondary.*;
import generaltools.*;
import org.testng.annotations.*;
import sequence.*;
import java.util.*;
import java.text.ParseException;
import java.io.*;
import secondarystructuredesign.*;
import generaltools.ResultWorker;
import generaltools.StringTools;
import org.testng.annotations.*; // for testing
import rnadesign.rnamodel.*;

public class TraceSecondaryRnaScriptCommandTest {

    public TraceSecondaryRnaScriptCommandTest() { }

    //TODO:update for new class
    /** tests building a simple corner out of two helices */
  //   @Test (groups={"dev1","slow"})
  //   public void testTraceSecondaryRnaScriptCommand1() {
	// String methodName = "testTraceSecondaryRnaScriptCommand1";
	// System.out.println(TestTools.generateMethodHeader(methodName));
 // 	NanoTilerScripter app = new NanoTilerScripter();
	// try{
	//     //   app.runScriptLine("loadnucleotides ${NANOTILER_HOME}/resources/nucleotidesDB.pdb");
	//     String scriptOutputFile = "${NANOTILER_HOME}/test/generated_scripts/" + methodName + "_sec.script";
	//     String outputPdbFile =  methodName + "_generated.pdb";
	//     
	//     Properties props = app.runScriptLine("tracesecondary i=${NANOTILER_HOME}/test/fixtures/hairpin.sec file=" + scriptOutputFile);
	//     // Properties optProperties = app.runScriptLine("tracegraphscript graph=root.newgraph.p0;root.newgraph.p1;root.newgraph.p2 name=corner file=" + fileName);
	//     // assert "0".equals(growProperties2.getProperty("ringless_collisions"));
	//     System.out.println("Result of Trace RNA Secondary Structure command: ");
	//     PropertyTools.printProperties(System.out, props);
	//     //app.runScriptLine("echo Executing generated script!");
	//     //app.runScriptLine("source " + scriptOutputFile);
	//     //app.runScriptLine("tree strand");
	//     // assert ... check if strand were generated
	//     //app.runScriptLine("exportpdb " + outputPdbFile);
	//     //app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+"_history.script");
	// }
	// catch(CommandExecutionException e) {
	//     System.out.println("Error in " + methodName + " : " + e.getMessage());
	//     assert false;
	// }
	// catch(CommandException e) {
	//     System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	//     assert false;
	// }
	// System.out.println(TestTools.generateMethodFooter(methodName));
  //   }
  //   
  // 
  //  @Test(groups={"new"})
  //   public void testStemGenerator() {
	// String methodName = "testStemFinder";
	// System.out.println(TestTools.generateMethodHeader(methodName));
	// String fileName = "../test/fixtures/tiny.sec";
	// SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
	// SecondaryStructure structure = null;
	// NanoTilerScripter app = new NanoTilerScripter();
	// try {
	// 	app.runScriptLine("source ${NANOTILER_HOME}/resources/defaults.script");
	//     structure = parser.parse(fileName);
	//     List<Stem> stems = SecondaryStructureTools.findHelices(structure);
	//     System.out.println(methodName + " : Found stems:");  
	//     for (Stem stem : stems) {
	//       System.out.println(stem);
	//       Properties props = app.runScriptLine("genhelix bp="+stem.size() );
	//     }
	//     assert stems.size() > 0;
	//     String outputPdbFile =  methodName + "_generated.pdb";
	//     app.runScriptLine("exportpdb " + outputPdbFile);
	// }
	// catch (IOException ioe) {
	//     System.out.println(ioe.getMessage());
	//     assert false;
	// }
	// catch (ParseException pe) {
	//     System.out.println(pe.getMessage());
	//     assert false;
	// }
	// catch(CommandExecutionException e) {
	//     System.out.println("Error in " + methodName + " : " + e.getMessage());
	//     assert false;
	// }
	// catch(CommandException e) {
	//     System.out.println("Command exception in " + methodName + " : " + e.getMessage());
	//     assert false;
	// }
	// assert structure != null;
	// assert structure.getSequenceCount() == 2;
	// 
	// }
	// 
	//    @Test(groups={"new"})
  //   public void testScriptGenerator() {
	// String methodName = "testScriptGenerator";
	// System.out.println(TestTools.generateMethodHeader(methodName));
	// String fileName = "../test/fixtures/tiny.sec";
	// SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
	// SecondaryStructure structure = null;
	// NanoTilerScripter app = new NanoTilerScripter();
	// Properties params = new Properties();
	// try{
	// 	structure = parser.parse(fileName);
	// }catch(Exception e){
	// }
	// 
	// 
	// TraceSecondaryScriptGenerator tracesecondary = new TraceSecondaryScriptGenerator(structure, "tracesecondarytestcase", methodName, params );
	// String script = tracesecondary.generate();
	// 
	// try{
	// 	PrintWriter writer = new PrintWriter("./"+methodName+".script", "UTF-8");
	// 	writer.println(script);
	// 	writer.close();
	// 	System.out.println("FILE: ./"+methodName+".script");
	// }catch(Exception e){
	// 	System.out.println("file write failed in testScriptGenerator");
	// }
  // 
	// assert structure != null;
	// assert structure.getSequenceCount() == 2;
	// 
	// }
 

}
