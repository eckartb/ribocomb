package nanotilertests.rnadesign.designapp;

import commandtools.CommandException;
import generaltools.TestTools;
import org.testng.annotations.*;
import rnadesign.designapp.*;

public class TreeCommandTest {

    public TreeCommandTest() { }

    @Test(groups={"new"})
    public void testTreeStrands() {
	String methodName = "testTreeStrands";
	System.out.println(TestTools.generateMethodHeader(methodName));
	try {
	    NanoTilerScripter app = new NanoTilerScripter();
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/1OOA_missing3n.pdb");
	    app.runScriptLine("tree");
	    app.runScriptLine("echo The following tree strand command should list 4 RNA strands:");
	    app.runScriptLine("tree strand");
	    System.out.println(TestTools.generateMethodFooter(methodName));
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	}
	catch (CommandException ce) {
	    System.out.println(ce.getMessage());
	    assert false;
	}
    }

}
