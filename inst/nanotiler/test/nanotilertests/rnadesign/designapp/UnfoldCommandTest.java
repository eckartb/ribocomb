package nanotilertests.rnadesign.designapp;

import commandtools.CommandException;
import commandtools.CommandExecutionException;
import generaltools.TestTools;
import org.testng.annotations.*; // for testing
import rnadesign.designapp.*;

/** Test class for import command */
public class UnfoldCommandTest {

    public UnfoldCommandTest() { }

    @Test (groups={"outdated"})
    public void testUnfold(){
	String methodName = "testUnfold";
	System.out.println(TestTools.generateMethodHeader(methodName));
 	NanoTilerScripter app = new NanoTilerScripter();
	try{
	    app.runScriptLine("import ${NANOTILER_HOME}/test/fixtures/6TNA_rnaview.pdb");
	    app.runScriptLine("unfold");
	    app.runScriptLine("exportpdb unfoldtest_tmp.pdb");
	    app.runScriptLine("history file=${NANOTILER_HOME}/test/generated_scripts/"+methodName+".script");
	} catch(CommandException e) {
	    System.out.println("Command exception in import command test: " + e.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }



}
