package nanotilertests.rnadesign.rnacontrol;

import java.io.*;
import org.testng.annotations.*;
import tools3d.Vector3D;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DTools;
import tools3d.objects3d.Object3DIOException;
import generaltools.TestTools;

import rnadesign.rnacontrol.*;

public class Object3DGraphControllerTest {

    @Test(groups={"new"})
    public void testFindByFullName() {
	String methodName = "testFindByFullName";
	System.out.println(TestTools.generateMethodHeader(methodName));
	boolean addStemsFlag = false; // true;
	boolean onlyFirstPathMode = false;
	boolean findStemsFlag = true;
	boolean addJunctionsFlag = true;
	String importName = "import";
	int format = Object3DGraphControllerConstants.PDB_RNAVIEW_FORMAT;
	char sequenceChar = 'N';
	String inputFileName = "../test/fixtures/testOptimizeHelicesCommand.pdb";
	Object3DGraphController controller = new Object3DGraphController();
	try {
	    FileInputStream fis = new FileInputStream(inputFileName);
	    controller.readAndAdd(fis, "import", new Vector3D(1,1,1), new Vector3D(0,0,0),
				  format, addStemsFlag, sequenceChar, onlyFirstPathMode,
				  findStemsFlag, addJunctionsFlag);
	    String[] allowedNames = null;
	    String[] forbiddenNames = new String[1];
	    forbiddenNames[0] = "Atom3D";
	    String name = "root";
	    // controller.getGraph().printTree(System.out, name, allowedNames, forbiddenNames);
	    // root.import_cov_jnc.j3.H_1_I_11 1.3.3.6
	    String fullName1= "1.1.17.3.5.1"; // "1.1.3.3.6"; 
	    String fullName2 = "root.import.import_cov_jnc.j3.I_1-11.A1"; // "root.import.import_cov_jnc.j3.H_1_I_11";
	    Object3D root = controller.getGraph().getGraph();
	    assert root != null;
	    Object3D obj1a = Object3DTools.findByFullName(root,fullName1);
	    Object3D obj2a = Object3DTools.findByFullName(root,fullName2);
	    assert obj1a != null;
	    assert obj2a != null;
	    assert obj1a == obj2a;
	    System.out.println("First stage of test passed, objects could be find with Object3DTools.");
	    Object3D obj1 = controller.getGraph().findByFullName(fullName1);
	    Object3D obj2 = controller.getGraph().findByFullName(fullName2);
	    assert obj1 != null;
	    assert obj2 != null;
	    assert obj1 == obj2;
	}
	catch (IOException ioe) {
	    System.out.println(ioe.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

}
