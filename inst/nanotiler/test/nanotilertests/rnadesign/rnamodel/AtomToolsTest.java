package nanotilertests.rnadesign.rnamodel;

import java.io.*;
import generaltools.TestTools;
import org.testng.annotations.*;
import tools3d.objects3d.*;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.designapp.NanoTilerScripter;
import rnadesign.rnamodel.*;

import static rnadesign.rnamodel.PackageConstants.*;

public class AtomToolsTest {

    @Test(groups={"scoring"})
    public void testAtomFinder() {
	String methodName = "testAtomFinder";
	
		Object3DGraphController controller = new Object3DGraphController();
		NanoTilerScripter app = new NanoTilerScripter(controller);
		
		try{
		app.runScriptLine("import fixtures/1BJ2_scoring.pdb");
		Object3D obj = controller.getGraph().findByFullName(".import");
		Object3DSet objSet = AtomTools.findAtoms(obj);
		System.out.println( "# of atoms: "+objSet.size() );
		assert (objSet.size() == 1284);
		} catch(Exception e){
		System.out.println(e);
		}
    }
    
    @Test(groups={"scoring","rms"})
    public void testRms() {
	String methodName = "rms";
	
		Object3DGraphController controller = new Object3DGraphController();
		NanoTilerScripter app = new NanoTilerScripter(controller);
		
		try{
		app.runScriptLine("import fixtures/decoy0001_amb.pdb");
		app.runScriptLine("import fixtures/decoy0002_amb.pdb");
		Object3D obj1 = controller.getGraph().findByFullName(".import");
		Object3D obj2 = controller.getGraph().findByFullName(".import1");
		double rms = controller.findRms(obj1, obj2);
		System.out.println("rms: "+rms);
		assert rms < 0.5;
		assert rms > 0.1;
		} catch(Exception e){
		System.out.println(e);
		}
    }


}
