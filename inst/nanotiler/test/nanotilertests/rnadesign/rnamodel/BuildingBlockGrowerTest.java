package nanotilertests.rnadesign.rnamodel;

import org.testng.*;
import org.testng.annotations.*;
import rnadesign.rnamodel.*;

public class BuildingBlockGrowerTest {

    public BuildingBlockGrowerTest() { }

    /** Tests topology extracting */
    @Test(groups = {"new"})
	public void testExtractTopologySize() {
	String topology = "(0%1);(4);(2%3);(5%6);[s_]([s_]([o_]([s_]([o_]([s_]([s_]))))));(%0->6%1->0%4->3%2->5%3->1%5->4%6->2%)";
	assert BuildingBlockGrower.extractTopologySize(topology) == 7;
    }

}
