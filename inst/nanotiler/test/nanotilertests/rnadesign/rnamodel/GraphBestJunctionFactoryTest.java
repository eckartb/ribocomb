package nanotilertests.rnadesign.rnamodel;

import java.io.*;
import org.testng.annotations.*;
import tools3d.objects3d.*;
import generaltools.TestTools;
import rnadesign.rnamodel.*;

public class GraphBestJunctionFactoryTest {

    @Test(groups={"new"})
    public void testGenerateCubeCorner() {
	RnaPdbRnaviewReader reader = new RnaPdbRnaviewReader();
	String ndbFileName = "../resources/nucleotidesDB.pdb";
	String outputFileName = "testGenerateCubeCorner.pdb";
	try {	    
	    FileInputStream fis = new FileInputStream(ndbFileName);
	    GeneralPdbWriter writer = new GeneralPdbWriter();
	    Object3D nucleotidesDB = NucleotideDBTools.readNucleotideDB(fis);
	    System.out.println(nucleotidesDB.infoString());
	    // writer.write(System.out, nucleotidesDB);
	    GraphBestJunctionFactory factory = new GraphBestJunctionFactory(3, Math.toRadians(45.0), nucleotidesDB);
	    factory.setCollisionDistance(2.0);
	    Object3DLinkSetBundle result = factory.generate();
	    FileOutputStream fos = new FileOutputStream(outputFileName);
	    writer.write(fos, result.getObject3D());
	}
	catch (IOException ioe) {
	    System.out.println("Error generating junction: " + ioe);
	    assert false;
	}
    }

    @Test(groups={"new"})
    public void testGenerateDodecahedronCorner() {
	String methodName = "testGenerateDodecahedronCorner";
	System.out.println(TestTools.generateMethodHeader(methodName));
	RnaPdbRnaviewReader reader = new RnaPdbRnaviewReader();
	String ndbFileName = "../resources/nucleotidesDB.pdb";
	String outputFileName = "testGenerateDodecahedronCorner.pdb";
	double offPlaneAng = Math.toRadians((180-116.57)/2); // using dihedral angle of dodecahedron
	try {	    
	    FileInputStream fis = new FileInputStream(ndbFileName);
	    GeneralPdbWriter writer = new GeneralPdbWriter();
	    Object3D nucleotidesDB = NucleotideDBTools.readNucleotideDB(fis);
	    System.out.println(nucleotidesDB.infoString());
	    // writer.write(System.out, nucleotidesDB);
	    GraphBestJunctionFactory factory = new GraphBestJunctionFactory(3, offPlaneAng, nucleotidesDB);
	    factory.setCollisionDistance(2.0);
	    factory.setPropagateMode(true);
	    Object3DLinkSetBundle result = factory.generate();
	    FileOutputStream fos = new FileOutputStream(outputFileName);
	    writer.write(fos, result.getObject3D());
	}
	catch (IOException ioe) {
	    System.out.println("Error generating junction: " + ioe);
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

}
