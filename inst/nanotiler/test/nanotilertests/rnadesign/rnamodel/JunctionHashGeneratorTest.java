package nanotilertests.rnadesign.rnamodel;

import tools3d.Matrix4D;
import tools3d.objects3d.CoordinateSystem3D;
import java.util.*;
import java.io.*;
import org.testng.*;
import org.testng.annotations.*;
import java.util.zip.DataFormatException;
import rnadesign.rnamodel.*;

public class JunctionHashGeneratorTest {

    /** Tests topology extracting */

    @Test(groups = {"new"})
    public void testParseOrientations() {
	String inputFileName = "../test/fixtures/j4508.properties";
	String inputFileName2 = "../test/fixtures/j4656.properties";
	try {
	    FileInputStream fis = new FileInputStream(inputFileName);
	    JunctionScanOutputParser parser = new JunctionScanOutputParser(fis);
	    parser.parse();
	    List<CoordinateSystem3D> orientations1 = parser.getOrientations();
	    FileInputStream fis2 = new FileInputStream(inputFileName2);
	    JunctionScanOutputParser parser2 = new JunctionScanOutputParser(fis2);
	    parser2.parse();
	    List<CoordinateSystem3D> orientations2 = parser2.getOrientations();
	    for (int i = 0; i < orientations1.size(); ++i) {
		System.out.println("Helix " + (i+1) + ":");
		System.out.println(orientations1.get(i));
	    }
	    for (int i = 0; i < orientations2.size(); ++i) {
		System.out.println("Helix " + (i+1) + ":");
		System.out.println(orientations2.get(i));
	    }
	    JunctionHashGenerator generator = new JunctionHashGenerator();
	    List<Matrix4D> trafos1 = generator.generateNormalizedTransformations(orientations1);
	    List<Matrix4D> trafos2 = generator.generateNormalizedTransformations(orientations2);
	    for (int i = 0; i < trafos1.size(); ++i) {
		System.out.println("Transformation " + (i+1) + ":");
		System.out.println(trafos1.get(i));
	    }
	    for (int i = 0; i < trafos2.size(); ++i) {
		System.out.println("Transformation " + (i+1) + ":");
		System.out.println(trafos2.get(i));
	    }
	    double score = generator.scoreNormalizedTransformations(trafos1, trafos2);
	    System.out.println("Score: " + score);
	} catch (IOException ioe) {
	    System.err.println(ioe.getMessage());
	    assert false;
	} catch (DataFormatException dfe) {
	    System.err.println(dfe.getMessage());
	    assert false;
	}
    }

}
