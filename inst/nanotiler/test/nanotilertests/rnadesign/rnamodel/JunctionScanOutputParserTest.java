package rnadesign.rnamodel;

import tools3d.objects3d.CoordinateSystem3D;
import java.util.*;
import java.io.*;
import org.testng.*;
import org.testng.annotations.*;
import java.util.zip.DataFormatException;

public class JunctionScanOutputParserTest {

    /** Tests topology extracting */
    @Test(groups = {"new"})
    public void testParseOrientations() {
	String inputFileName = "../test/fixtures/j4508.properties";
	try {
	    FileInputStream fis = new FileInputStream(inputFileName);
	    JunctionScanOutputParser parser = new JunctionScanOutputParser(fis);
	    parser.parse();
	    List<CoordinateSystem3D> orientations = parser.getOrientations();
	    for (int i = 0; i < orientations.size(); ++i) {
		System.out.println("Helix " + (i+1) + ":");
		System.out.println(orientations.get(i));
	    }
	} catch (IOException ioe) {
	    System.err.println(ioe.getMessage());
	    assert false;
	} catch (DataFormatException dfe) {
	    System.err.println(dfe.getMessage());
	    assert false;
	}
    }

}
