package nanotilertests.rnadesign.rnamodel;

import java.io.*;
import generaltools.TestTools;
import org.testng.annotations.*;
import tools3d.objects3d.*;
import rnadesign.rnamodel.*;
import rnasecondary.BasePairInteractionType;

import static rnadesign.rnamodel.PackageConstants.*;

public class LWBasePairClassifierTest {

    @Test(groups={"y2015", "tight"})
    public void testLWBasePairClassifierAU() {
	String methodName = "testLWBasePairClassifierAU";
	System.out.println(TestTools.generateMethodHeader(methodName));
	RnaPdbRnaviewReader reader = new RnaPdbRnaviewReader();
	String inFileName = NANOTILER_HOME + SLASH + "test" + SLASH + "fixtures" + SLASH
	    + "NA1_U_edit2.pdb";
	LWBasePairClassifier bpScorer = new LWBasePairClassifier();
	try {
	    FileInputStream fis = new FileInputStream(inFileName);
	    Object3D obj = reader.readAnyObject3D(fis);
	    assert obj != null;
	    assert obj.size() > 0;
	    int nStrands = obj.getChildCount("RnaStrand");
	    System.out.println("Object with " + nStrands + " strands read.");
	    for (int i = 0; i < nStrands; ++i) {
		System.out.println("Strand " + (i+1) + " : " + obj.getChild(obj.getIndexOfChild(i, "RnaStrand")).size());
	    }
	    RnaStrand strand1 = (RnaStrand)(obj.getChild(obj.getIndexOfChild(0, "RnaStrand")));
	    RnaStrand strand2 = (RnaStrand)(obj.getChild(obj.getIndexOfChild(1, "RnaStrand")));
	    Residue3D n1 = strand1.getResidue3D(0);
	    Residue3D n2 = strand2.getResidue3D(0);
	    assert n1 instanceof Nucleotide3D;
	    assert n2 instanceof Nucleotide3D;
	    BasePairInteractionType interaction = bpScorer.classifyBasePair((Nucleotide3D)n1, (Nucleotide3D)n2);
	    System.out.println("Found interaction between " + n1.getFullName() + " and " + n2.getFullName() + " : " + interaction);
	    System.out.println("Detected type of base pair: " + interaction.getSubTypeName());
	    assert "cWW".equals(interaction.getSubTypeName()); // regular cis-Watson-Crick/Watson-Crick interaction
	}
	catch(FileNotFoundException fnfe) {
	    System.out.println(fnfe.getMessage());
	    assert false;
	}
	catch(Object3DIOException ioe) {
	    System.out.println(ioe.getMessage());
	}
	catch(RnaModelException ioe) {
	    System.out.println(ioe.getMessage());
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new", "y2015"})
    public void testLWBasePairClassifier1KOD() {
	String methodName = "testLWBasePairClassifier1KOD";
	System.out.println(TestTools.generateMethodHeader(methodName));
	RnaPdbRnaviewReader reader = new RnaPdbRnaviewReader();
	String inFileName = NANOTILER_HOME + SLASH + "test" + SLASH + "fixtures" + SLASH + "1KOD.rnaview.pdb";
	LWBasePairClassifier bpScorer = new LWBasePairClassifier();
	try {
	    FileInputStream fis = new FileInputStream(inFileName);
	    Object3D obj = reader.readAnyObject3D(fis);
	    Object3DSet nucs = Object3DTools.collectByClassName(obj, "Nucleotide3D");
	    assert obj != null;
	    assert obj.size() > 0;
	    int nStrands = obj.getChildCount("RnaStrand");
	    System.out.println("Object with " + nStrands + " strands and " + nucs.size() + " nucleotides read.");
	    for (int i = 0; i < nStrands; ++i) {
		System.out.println("Strand " + (i+1) + " : " + obj.getChild(obj.getIndexOfChild(i, "RnaStrand")).size());
	    }
	    LinkSet links = bpScorer.classifyBasePairs(nucs);
	    for (int i = 0; i < links.size(); ++i) {
		Link link = links.get(i);
		assert (link.getObj1() != link.getObj2());
		if (link instanceof InteractionLink) {
		    System.out.println("" + (i+1) + " " + BasePairTools.toString((InteractionLink)link));
		}
	    }
	}
	catch(FileNotFoundException fnfe) {
	    System.out.println(fnfe.getMessage());
	    assert false;
	}
	catch(Object3DIOException ioe) {
	    System.out.println(ioe.getMessage());
	}
	catch(RnaModelException ioe) {
	    System.out.println(ioe.getMessage());
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"newest", "y2015"})
    public void testLWBasePairClassifier1F1T() {
	String methodName = "testLWBasePairClassifier1F1T";
	System.out.println(TestTools.generateMethodHeader(methodName));
	RnaPdbRnaviewReader reader = new RnaPdbRnaviewReader();
	String inFileName = NANOTILER_HOME + SLASH + "test" + SLASH + "fixtures" + SLASH + "1F1T.pdb";
	LWBasePairClassifier bpScorer = new LWBasePairClassifier();
	try {
	    FileInputStream fis = new FileInputStream(inFileName);
	    Object3D obj = reader.readAnyObject3D(fis);
	    Object3DSet nucs = Object3DTools.collectByClassName(obj, "Nucleotide3D");
	    assert obj != null;
	    assert obj.size() > 0;
	    int nStrands = obj.getChildCount("RnaStrand");
	    System.out.println("Object with " + nStrands + " strands and " + nucs.size() + " nucleotides read.");
	    for (int i = 0; i < nStrands; ++i) {
		System.out.println("Strand " + (i+1) + " : " + obj.getChild(obj.getIndexOfChild(i, "RnaStrand")).size());
	    }
	    LinkSet links = bpScorer.classifyBasePairs(nucs);
	    for (int i = 0; i < links.size(); ++i) {
		Link link = links.get(i);
		assert (link.getObj1() != link.getObj2());
		if (link instanceof InteractionLink) {
		    System.out.println("" + (i+1) + " " + BasePairTools.toString((InteractionLink)link));
		}
	    }
	}
	catch(FileNotFoundException fnfe) {
	    System.out.println(fnfe.getMessage());
	    assert false;
	}
	catch(Object3DIOException ioe) {
	    System.out.println(ioe.getMessage());
	}
	catch(RnaModelException ioe) {
	    System.out.println(ioe.getMessage());
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


    /** Read structure that containts trans WatsonCrick-Hoogsteen interaction according to NDB:
Model Number,Pair Number,Pair Name,Leontis / Westhof Classification,Saenger Classification
1,1,A_G2648:U2672_A,1,28
1,2,A_C2649:G2671_A,1,19
1,3,A_U2650:A2670_A,1,20
1,4,A_C2651:G2669_A,1,19
1,5,A_C2652:G2668_A,1,19
1,6,A_U2656:A2665_A,4,24  # tWH THIS IS THE ONLY BASE PAIR PROVIDED IN THE STRUCTURE
1,7,A_A2657:G2664_A,9,11  # cHS
1,8,A_C2658:G2663_A,1,19
1,9,A_G2659:A2662_A,0,0
    */
    @Test(groups={"new", "y2015", "tight"})
    public void testLWBasePairClassifierTWW() {
	String methodName = "testLWBasePairClassifierTWW";
	System.out.println(TestTools.generateMethodHeader(methodName));
	RnaPdbRnaviewReader reader = new RnaPdbRnaviewReader();
	String inFileName = NANOTILER_HOME + SLASH + "test" + SLASH + "fixtures" + SLASH + "4NLF_tWH.pdb";
	LWBasePairClassifier bpScorer = new LWBasePairClassifier();
	try {
	    FileInputStream fis = new FileInputStream(inFileName);
	    Object3D obj = reader.readAnyObject3D(fis);
	    Object3DSet nucs = Object3DTools.collectByClassName(obj, "Nucleotide3D");
	    assert obj != null;
	    assert obj.size() > 0;
	    int nStrands = obj.getChildCount("RnaStrand");
	    System.out.println("Object with " + nStrands + " strands and " + nucs.size() + " nucleotides read.");
	    for (int i = 0; i < nStrands; ++i) {
		System.out.println("Strand " + (i+1) + " : " + obj.getChild(obj.getIndexOfChild(i, "RnaStrand")).size());
	    }
	    LinkSet links = bpScorer.classifyBasePairs(nucs);
	    System.out.println("Found " + links.size() + " base pairs.");
	    for (int i = 0; i < links.size(); ++i) {
		Link link = links.get(i);
		assert (link.getObj1() != link.getObj2());
		if (link instanceof InteractionLink) {
		    System.out.println("" + (i+1) + " " + BasePairTools.toString((InteractionLink)link));
		}
	    }
	    assert(links.size() == 1); // should be one base pair (twH)
	}
	catch(FileNotFoundException fnfe) {
	    System.out.println(fnfe.getMessage());
	    assert false;
	}
	catch(Object3DIOException ioe) {
	    System.out.println(ioe.getMessage());
	}
	catch(RnaModelException ioe) {
	    System.out.println(ioe.getMessage());
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Read structure that containts trans WatsonCrick-Hoogsteen interaction according to NDB:
Model Number,Pair Number,Pair Name,Leontis / Westhof Classification,Saenger Classification
1,1,A_G2648:U2672_A,1,28
1,2,A_C2649:G2671_A,1,19
1,3,A_U2650:A2670_A,1,20
1,4,A_C2651:G2669_A,1,19
1,5,A_C2652:G2668_A,1,19
1,6,A_U2656:A2665_A,4,24  # tWH THIS IS THE ONLY BASE PAIR PROVIDED IN THE STRUCTURE
1,7,A_A2657:G2664_A,9,11  # cHS
1,8,A_C2658:G2663_A,1,19
1,9,A_G2659:A2662_A,0,0
    */
    @Test(groups={"new", "y2015", "tight"})
    public void testLWBasePairClassifierCHS() {
	String methodName = "testLWBasePairClassifierCHS";
	System.out.println(TestTools.generateMethodHeader(methodName));
	RnaPdbRnaviewReader reader = new RnaPdbRnaviewReader();
	String inFileName = NANOTILER_HOME + SLASH + "test" + SLASH + "fixtures" + SLASH + "4NLF_cHS.pdb";
	LWBasePairClassifier bpScorer = new LWBasePairClassifier();
	try {
	    FileInputStream fis = new FileInputStream(inFileName);
	    Object3D obj = reader.readAnyObject3D(fis);
	    Object3DSet nucs = Object3DTools.collectByClassName(obj, "Nucleotide3D");
	    assert obj != null;
	    assert obj.size() > 0;
	    int nStrands = obj.getChildCount("RnaStrand");
	    System.out.println("Object with " + nStrands + " strands and " + nucs.size() + " nucleotides read.");
	    for (int i = 0; i < nStrands; ++i) {
		System.out.println("Strand " + (i+1) + " : " + obj.getChild(obj.getIndexOfChild(i, "RnaStrand")).size());
	    }
	    LinkSet links = bpScorer.classifyBasePairs(nucs);
	    System.out.println("Found " + links.size() + " base pairs.");
	    for (int i = 0; i < links.size(); ++i) {
		Link link = links.get(i);
		assert (link.getObj1() != link.getObj2());
		if (link instanceof InteractionLink) {
		    InteractionLink iLink = (InteractionLink)link;		    
		    String leontisWesthof =iLink.getInteraction().getInteractionType().getSubTypeName();
		    System.out.println("" + (i+1) + " " + BasePairTools.toString(iLink) + " recognized type: " + leontisWesthof);
		    assert leontisWesthof.equals("cHS"); // should be cis-Hoogsteen/Sugar Interaction
		}
	    }
	    assert(links.size() == 1); // should be one base pair (twH)
	}
	catch(FileNotFoundException fnfe) {
	    System.out.println(fnfe.getMessage());
	    assert false;
	}
	catch(Object3DIOException ioe) {
	    System.out.println(ioe.getMessage());
	}
	catch(RnaModelException ioe) {
	    System.out.println(ioe.getMessage());
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Read structure that containts trans WatsonCrick-Hoogsteen interaction according to NDB entry for 4NLF:
Model Number,Pair Number,Pair Name,Leontis / Westhof Classification,Saenger Classification
1,1,A_G2648:U2672_A,1,28
1,2,A_C2649:G2671_A,1,19
1,3,A_U2650:A2670_A,1,20
1,4,A_C2651:G2669_A,1,19
1,5,A_C2652:G2668_A,1,19
1,6,A_U2656:A2665_A,4,24  # tWH
1,7,A_A2657:G2664_A,9,11  # cHS
1,8,A_C2658:G2663_A,1,19
1,9,A_G2659:A2662_A,0,0 # THIS DOES NOT FIT Leontis-Westhof classes, does not have to be recognized

Correct analysis by this test found:
1 A G2648 2 A U2672 25 cWW
2 A C2649 3 A G2671 24 cWW
3 A U2650 4 A A2670 23 cWW
4 A C2651 5 A G2669 22 cWW
5 A C2652 6 A G2668 21 cWW
6 A U2656 10 A A2665 19 tWH
7 A A2657 11 A G2664 18 cHS
8 A C2658 12 A G2663 17 cWW
    */
    @Test(groups={"new", "y2015", "tight"})
    public void testLWBasePairClassifierTWWFull() {
	String methodName = "testLWBasePairClassifierTWWFull";
	System.out.println(TestTools.generateMethodHeader(methodName));
	RnaPdbRnaviewReader reader = new RnaPdbRnaviewReader();
	String inFileName = NANOTILER_HOME + SLASH + "test" + SLASH + "fixtures" + SLASH + "4NLF.pdb";
	LWBasePairClassifier bpScorer = new LWBasePairClassifier();
	try {
	    FileInputStream fis = new FileInputStream(inFileName);
	    Object3D obj = reader.readAnyObject3D(fis);
	    Object3DSet nucs = Object3DTools.collectByClassName(obj, "Nucleotide3D");
	    assert obj != null;
	    assert obj.size() > 0;
	    int nStrands = obj.getChildCount("RnaStrand");
	    System.out.println("Object with " + nStrands + " strands and " + nucs.size() + " nucleotides read.");
	    for (int i = 0; i < nStrands; ++i) {
		System.out.println("Strand " + (i+1) + " : " + obj.getChild(obj.getIndexOfChild(i, "RnaStrand")).size());
	    }
	    LinkSet links = bpScorer.classifyBasePairs(nucs);
	    System.out.println("Found " + links.size() + " base pairs.");
	    for (int i = 0; i < links.size(); ++i) {
		Link link = links.get(i);
		assert (link.getObj1() != link.getObj2());
		if (link instanceof InteractionLink) {
		    System.out.println("" + (i+1) + " " + BasePairTools.toString((InteractionLink)link));
		}
	    }
	    assert(links.size() == 8); // there should be 8 pairs recognized
	}
	catch(FileNotFoundException fnfe) {
	    System.out.println(fnfe.getMessage());
	    assert false;
	}
	catch(Object3DIOException ioe) {
	    System.out.println(ioe.getMessage());
	}
	catch(RnaModelException ioe) {
	    System.out.println(ioe.getMessage());
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

}
