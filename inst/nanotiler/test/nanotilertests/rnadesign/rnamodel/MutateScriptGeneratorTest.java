package nanotilertests.rnadesign.rnamodel;

import tools3d.objects3d.*;
import java.io.PrintStream;
import java.io.File;
import java.math.BigInteger;
import java.util.List;
import java.util.ArrayList;
import java.util.Properties;
import rnadesign.rnacontrol.Object3DGraphController;
import rnadesign.rnacontrol.Object3DGraphControllerException;
import rnadesign.rnamodel.DBElementDescriptor;
import rnadesign.rnamodel.DBElementConnectionDescriptor;
import commandtools.AbstractCommand;
import commandtools.Command;
import commandtools.CommandException;
import commandtools.CommandExecutionException;
import commandtools.StringParameter;
import tools3d.Vector3D;
import rnadesign.designapp.*;
import generaltools.*;
import rnasecondary.*;
import generaltools.*;
import org.testng.annotations.*;
import sequence.*;
import java.util.*;
import java.text.ParseException;
import java.io.*;
import secondarystructuredesign.*;
import generaltools.ResultWorker;
import generaltools.StringTools;
import org.testng.annotations.*; // for testing
import rnadesign.rnamodel.*;

public class MutateScriptGeneratorTest {

    public void MutateScriptGeneratorTest() { }
    
    @Test (groups={"newer"})
    public void testMutateScriptGeneratorCase1(){
    
		String methodName = "testMutateScriptGeneratorCase1";
		System.out.println(TestTools.generateMethodHeader(methodName));
    
    	String script = createScript("../test/fixtures/tiny2.sec", "../test/fixtures/tinyNoSequence.sec");
    	
	    try{
			PrintWriter writer = new PrintWriter("./"+methodName+".script", "UTF-8");
			writer.println(script);
			writer.close();
			System.out.println("FILE: ./"+methodName+".script");
		}catch(Exception e){
			System.out.println("file write failed in testScriptGenerator");
		}
		
    	//assert script.contains("mutate std1:G0,G1,A2,A3,G4,G5,G6,G7,G8,G9,G10,G11,G12,G13,G14,C15,C16,C17,C18,C19,C20,C21,C22,C23,C24,C25,C26,C27,C28,C29,C30,C31,");
		//assert script.contains("std2:G0,G1,A2,A3,G4,G5,G6,G7,G8,G9,G10,G11,G12,G13,G14,C15,C16,C17,C18,C19,C20,C21,C22,C23,C24,C25,C26,C27,C28,C29,C30,C31,");
		
    	System.out.println(TestTools.generateMethodFooter(methodName));
    }
    

    /** tests generating dist constraints */
    public String createScript(String filepath, String filepath2) {
    String methodName = "createScript";
 	NanoTilerScripter app = new NanoTilerScripter();
 	String fileName = filepath;
 	String fileName2 = filepath2;
 	SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
 	SecondaryStructure structure = null;
 	SecondaryStructure structure2 = null;
 	String script = "";
 	
	try{
	
		structure = parser.parse(fileName);
		assert structure != null;
		
		structure2 = parser.parse(fileName2);
		assert structure2 != null;
		
		app.runScriptLine("tree strand");
		
		String[] names = {"std1","std2","std3","std4"};
		
		System.out.println("Creating mockup script. Not to be executed");
		
		MutateScriptGenerator scriptGenerator = new MutateScriptGenerator(structure2, structure, names, "MutateScriptGeneratorTestName", new Properties() );
		script = scriptGenerator.generate();
		app.runScriptLine(script);
		app.runScriptLine("tree residues");
		
		
		

	    
	}
	catch(Exception e) {
	    assert false;
	}
	assert script != "";
	
	return script;
    }
    
    }
