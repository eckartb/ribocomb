package nanotilertests.rnadesign.rnamodel;

import java.io.*;
import generaltools.TestTools;
import org.testng.annotations.*;
import tools3d.objects3d.*;
import rnadesign.rnamodel.*;
import static rnadesign.rnamodel.PackageConstants.*;

public class RnaPdbRnaviewReaderTest {

    @Test(groups={"new"})
    public void testReadAnyObject3D() {
	String methodName = "testReadAnyObject3D";
	System.out.println(TestTools.generateMethodHeader(methodName));
	RnaPdbRnaviewReader reader = new RnaPdbRnaviewReader();
	String inFileName = NANOTILER_HOME + SLASH + "test" + SLASH + "fixtures" + SLASH
	    + "2NVS.rnaview.pdb_j2_N-T6_T-A7.pdb_ren_rnaview.pdb";
	boolean errorFlag = false;
	try {
	    FileInputStream fis = new FileInputStream(inFileName);
	    Object3D obj = reader.readAnyObject3D(fis);
	    assert obj != null;
	    assert obj.size() > 0;
	    int nStrands = obj.getChildCount("RnaStrand");
	    System.out.println("Object with " + nStrands + " strands read.");
	    for (int i = 0; i < nStrands; ++i) {
		System.out.println("Strand " + (i+1) + " : " + obj.getChild(obj.getIndexOfChild(i, "RnaStrand")).size());
	    }
	}
	catch(FileNotFoundException fnfe) {
	    System.out.println(fnfe.getMessage());
	    assert false;
	}
	catch(Object3DIOException ioe) {
	    System.out.println("We WANT the following error message:");
	    System.out.println(ioe.getMessage());
	    errorFlag = true; // exception thrown
	}
	if (!errorFlag) {
	    System.out.println("This test case has the theme of reading a DNA molecule even though RNA was expected. An Object3DIOException was  not thrown even though it should have. This means the test failed.");
	    assert false;
	} else {
	    System.out.println("Test succeeded!");
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /*
    @Test(groups={"new"})
    public void testWriteCT() {
	String methodName = "testReadAnyObject3D";
	System.out.println(TestTools.generateMethodHeader(methodName));
	RnaPdbRnaviewReader reader = new RnaPdbRnaviewReader();
	String inFileName = NANOTILER_HOME + SLASH + "test" + SLASH + "fixtures" + SLASH + "2GOZ.pdb";
	boolean errorFlag = false;
	try {
	    FileInputStream fis = new FileInputStream(inFileName);
	    Object3D obj = reader.readAnyObject3D(fis);
	    assert obj != null;
	    assert obj.size() > 0;
	    int nStrands = obj.getChildCount("RnaStrand");
	    System.out.println("Object with " + nStrands + " strands read.");
	    SecondaryStructureWriter writer = new SecondaryStructureCTFormatWriter();
	    String ctOutput = writer.writeString(structure); // FIXIT
	}
	catch(FileNotFoundException fnfe) {
	    System.out.println(fnfe.getMessage());
	    assert false;
	}
	catch(Object3DIOException ioe) {
	    System.out.println("We WANT the following error message:");
	    System.out.println(ioe.getMessage());
	    errorFlag = true; // exception thrown
	}
	if (!errorFlag) {
	    System.out.println("This test case has the theme of reading a DNA molecule even though RNA was expected. An Object3DIOException was  not thrown even though it should have. This means the test failed.");
	    assert false;
	} else {
	    System.out.println("Test succeeded!");
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }
    */

}
