package nanotilertests.rnadesign.rnamodel;

import java.io.*;
import generaltools.TestTools;
import org.testng.annotations.*;
import tools3d.objects3d.*;
import rnadesign.rnamodel.*;
import rnasecondary.RnaInteractionType;

import static rnadesign.rnamodel.PackageConstants.*;

public class SimpleHydrogenBondFinderTest {

    /** This test should detect 2 hydrogen bonds in an A-U base pair structure. */
    @Test(groups={"new", "y2015", "tight"})
    public void testSimpleHydrogenBondFinderAU() {
	String methodName = "testSimpleHydrogenBondFinderAU";
	System.out.println(TestTools.generateMethodHeader(methodName));
	RnaPdbRnaviewReader reader = new RnaPdbRnaviewReader();
	String inFileName = NANOTILER_HOME + SLASH + "test" + SLASH + "fixtures" + SLASH
	    + "NA1_U_edit2.pdb";
	HydrogenBondFinder finder = new SimpleHydrogenBondFinder();
	try {
	    FileInputStream fis = new FileInputStream(inFileName);
	    Object3D obj = reader.readAnyObject3D(fis);
	    assert obj != null;
	    assert obj.size() > 0;
	    int nStrands = obj.getChildCount("RnaStrand");
	    System.out.println("Object with " + nStrands + " strands read.");
	    for (int i = 0; i < nStrands; ++i) {
		System.out.println("Strand " + (i+1) + " : " + obj.getChild(obj.getIndexOfChild(i, "RnaStrand")).size());
	    }
	    RnaStrand strand1 = (RnaStrand)(obj.getChild(obj.getIndexOfChild(0, "RnaStrand")));
	    RnaStrand strand2 = (RnaStrand)(obj.getChild(obj.getIndexOfChild(1, "RnaStrand")));
	    Residue3D n1 = strand1.getResidue3D(0);
	    Residue3D n2 = strand2.getResidue3D(0);
	    assert n1 instanceof Nucleotide3D;
	    assert n2 instanceof Nucleotide3D;
	    LinkSet bonds = finder.find((Nucleotide3D)n1, (Nucleotide3D)n2);
	    System.out.println("Found interaction between " + n1.getFullName() + " and " + n2.getFullName() + " : ");
	    for (int i = 0; i < bonds.size(); ++i) {
		System.out.println(bonds.get(i).toString());
	    }
	    assert bonds.size() == 2;
	}
	catch(FileNotFoundException fnfe) {
	    System.out.println(fnfe.getMessage());
	    assert false;
	}
	catch(Object3DIOException ioe) {
	    System.out.println(ioe.getMessage());
	}
	catch(RnaModelException rme) {
	    System.out.println(rme.getMessage());
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


}
