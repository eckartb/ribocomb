package nanotilertests.rnasecondary;

import java.io.*;
import java.text.ParseException;
import generaltools.*;
import org.testng.annotations.*;
import rnasecondary.*;

public class CTFileParserTest {


    @Test(groups={"newCT"})
    public void testParse() {
	String fileName = "../test/fixtures/CT/scube10242008_reorder.ct";
	SecondaryStructureParser parser = new CTFileParser();
	MutableSecondaryStructure structure = null;
	try {
	    structure = parser.parse(fileName);
	    System.out.println("Sequences (6): "+structure.getSequenceCount() );
	    System.out.println("Residues (288): "+structure.getSequences().getTotalResidueCount() );
      System.out.println("Residue sequence: ggcaacggaugguucguugcc gcaccgaaccauccggugc " + structure.getSequences ());
	    System.out.println("Interactions (120): "+structure.getInteractionCount() );
	}
	catch (IOException ioe) {
	    System.out.println(ioe.getMessage());
	    assert false;
	}
	catch (ParseException pe) {
	    System.out.println(pe.getMessage());
	    assert false;
	}
	assert structure != null;
	assert structure.getSequenceCount() == 6;
	assert structure.getSequences().getTotalResidueCount() == 288;
	assert structure.getInteractionCount() == 120;
    }


}
