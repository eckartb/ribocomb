package nanotilertests.rnasecondary;

import java.io.*;
import java.text.ParseException;
import generaltools.*;
import org.testng.annotations.*;
import rnasecondary.*;

public class ImprovedSecondaryStructureParserTest {


    @Test(groups={"y2011"})
    public void testParse() {
	String fileName = "../test/fixtures/hairpin.sec";
	SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
	MutableSecondaryStructure structure = null;
	try {
	    structure = parser.parse(fileName);
	    System.out.println("Sequences (1): "+structure.getSequenceCount() );
	    System.out.println("Resiudes (15): "+structure.getSequences().getTotalResidueCount() );
	    System.out.println("Interactions (4): "+structure.getInteractionCount() );
	}
	catch (IOException ioe) {
	    System.out.println(ioe.getMessage());
	    assert false;
	}
	catch (ParseException pe) {
	    System.out.println(pe.getMessage());
	    assert false;
	}
	assert structure != null;
	assert structure.getSequenceCount() == 1;
	assert structure.getSequences().getTotalResidueCount() == 15;
	assert structure.getInteractionCount() == 4;
    }

    @Test(groups={"y2016"})
    public void testPuzzle() {
	String[] lines = new String[3];
	lines[0] = "~STRAND A";
	lines[1] = "SEQUENCE  = CGUGGUUAGGGCCACGUUAAAUAGUUGCUUAAGCCCUAAGCGUUGAUAAAUAUCAGGUGCAA";
	lines[2] = "STRUCTURE = BBBB.(((((((BBBB........AAAA....)))))))...(((((....)))))..AAAA";
	// lines[2] = "STRUCTURE = BBBB-(((((((BBBB--------AAAA----)))))))---(((((----)))))--AAAA";
	SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
	MutableSecondaryStructure structure = null;
	try {
	    structure = parser.parse(lines);
	    System.out.println("Sequences (1): "+structure.getSequenceCount() );
	    System.out.println("Resiudes (62): "+structure.getSequences().getTotalResidueCount() );
	    System.out.println("Interactions (20): "+structure.getInteractionCount() );
	    assert(structure.getSequenceCount() == 1);
	    assert(structure.getSequences().getTotalResidueCount() == 62);
	    assert(structure.getInteractionCount() == 20);
	}
	catch (ParseException pe) {
	    System.out.println(pe.getMessage());
	    assert false;
	}
	
    }
    
    @Test(groups={"y2016"})
    public void testInternalLoop() {
  String[] lines = new String[6];
  lines[0] = "~STRAND A";
  lines[1] = "SEQUENCE  = cggaccgagccag";
  lines[2] = "STRUCTURE = .AAAA....BBBB";
  lines[3] = "~STRAND B";
  lines[4] = "SEQUENCE  = gcugggagucc";
  lines[5] = "STRUCTURE = .BBBB..AAAA";
  SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
  MutableSecondaryStructure structure = null;
  try {
      structure = parser.parse(lines);
      SecondaryStructureWriter writer = new SecondaryStructureScriptFormatWriter();
      String out = writer.writeString(structure);
      System.out.println("Read:");
      System.out.println(out);
      System.out.println("Should be:");
      System.out.println("~STRAND A\nSEQUENCE  = cggaccgagccag\nSTRUCTURE = .AAAA....BBBB\n\n~STRAND B\nSEQUENCE  = gcugggagucc\nSTRUCTURE = .BBBB..(())");

      System.out.println("Sequences (2): "+structure.getSequenceCount() );
      System.out.println("Resiudes (24): "+structure.getSequences().getTotalResidueCount() );
      System.out.println("Interactions (8): "+structure.getInteractionCount() );
      assert(structure.getSequenceCount() == 2);
      assert(structure.getSequences().getTotalResidueCount() == 24);
      assert(structure.getInteractionCount() == 8);
  }
  catch (ParseException pe) {
      System.out.println(pe.getMessage());
      assert false;
  }
  
    }


}
