package nanotilertests.rnasecondary;

import java.io.*;
import org.testng.annotations.*;
import sequence.*;
import static rnasecondary.PackageConstants.*;
import generaltools.StringTools;
import generaltools.TestTools;
import rnasecondary.*;

public class MatchFoldSecondaryStructurePredictorTest {

    static Alphabet alphabet = DnaTools.AMBIGUOUS_RNA_ALPHABET;

    /** From 2004 Chemical Communications paper by Russell P. Goodman */
    static UnevenAlignment generateRnaTetrahedronAlignment() {
	SimpleUnevenAlignment ali = new SimpleUnevenAlignment();
	try {
	    ali.addSequence(new SimpleSequence(
	       "ACAUUCCUAAGUCUGAAACAUUACAGCUUGCUACACGAGAAGAGCCGCCAUAGUA", "A", alphabet));
	    ali.addSequence(new SimpleSequence(
               "UAUCACCAGGCAGUUGACAGUGUAGCAAGCUGUAAUAGAUGCGAGGGUCCAAUAC", "B", alphabet));
	    ali.addSequence(new SimpleSequence(
               "UCAACUGCCUGGUGAUAAAACGACACUACGUGGGAAUCUACUAUGGCGGCUCUUC", "C", alphabet));
	    ali.addSequence(new SimpleSequence(
               "UUCAGACUUAGGAAUGUGCUUCCCACGUAGUGUCGUUUGUAUUGGACCCUCGCAU", "D", alphabet));

	}
	catch (UnknownSymbolException nse) {
	    System.out.println("Unknown sequence symbol: " + nse.getMessage());
	    assert false;
	}
	catch (DuplicateNameException dne) {
	    System.out.println("Duplicate name exception: " + dne.getMessage());
	    assert false;
	}
	return ali;
    }

    /*
    @Test(groups={"new"})
    public void testMatchFoldTetrahedron() {
	String methodName = "testHXMatchTetrahedron";
	System.out.println(TestTools.generateMethodHeader(methodName));
	UnevenAlignment ali = generateRnaTetrahedronAlignment();
	HXMatchStructurePredictor predictor = new HXMatchStructurePredictor(ali);	
	predictor.run();
	SecondaryStructure tetrahedronStructure = (SecondaryStructure)(predictor.getResult());
	assert tetrahedronStructure != null;
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Predicted the following structure with " + tetrahedronStructure.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(tetrahedronStructure));
	assert tetrahedronStructure.getSequenceCount() == 6; // must be this many sequences
// 	System.out.println("Starting to optimize tetrahedron sequences:");
// 	String[] result = optimizer.optimize(tetrahedronStructure);
// 	System.out.println("Finished optimizing tetrahedron sequences:");
// 	assert result != null;
// 	for (int i = 0; i < result.length; ++i) {
// 	    System.out.println(result[i]);
// 	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }
    */

    @Test(groups={"new"})
    public void testFolding() {
        String methodName = "testHXMatchTetrahedron";
	System.out.println(TestTools.generateMethodHeader(methodName));
	String inFileName = NANOTILER_HOME + "/test/fixtures/kl1.fa"; // two sequences corresponding to HIV kissing loop
	SimpleUnevenAlignment ali;
	try {
	    FileInputStream fis = new FileInputStream(inFileName);
	    String[] lines = StringTools.readAllLines(fis);
	    ali = new SimpleUnevenAlignment(lines, DnaTools.AMBIGUOUS_RNA_ALPHABET);
	    System.out.println("Read alignment: " );
	    System.out.println(ali.toString());
	    MatchFoldSecondaryStructurePredictor predictor = new MatchFoldSecondaryStructurePredictor(ali);
	    predictor.run();
	    if (!predictor.validate()) {
		assert(predictor.getException() != null);
		System.out.println(predictor.getException().getMessage());
	    }
	    assert (predictor.validate()); // make sure not errors occurred
	    SecondaryStructure result = (SecondaryStructure)(predictor.getResult());
	    assert result != null;
	    SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	    System.out.println(writer.writeString(result));
	    assert(result.getInteractionCount() == 16); // number of base pairs in HIV kissing loop
	} catch (IOException ioe) {
	    System.out.println("IO Error : " + ioe.getMessage());
	    assert false;
	} catch (DuplicateNameException dne) {
	    System.out.println(dne.getMessage());
	    assert false;
	} catch (UnknownSymbolException use) {
	    System.out.println(use.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

}
