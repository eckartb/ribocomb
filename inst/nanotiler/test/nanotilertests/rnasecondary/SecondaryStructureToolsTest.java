package nanotilertests.rnasecondary;

import rnasecondary.*;
import generaltools.*;
import org.testng.annotations.*;
import sequence.*;
import java.util.*;
import java.text.ParseException;
import java.io.*;
import secondarystructuredesign.*;
import generaltools.ResultWorker;
import generaltools.StringTools;

public class SecondaryStructureToolsTest {

	public SecondaryStructureToolsTest(){
	}
	
	@Test(groups={"new"})
    public void testPermutationFinder() {
	String methodName = "testPermutationFinder";
	System.out.println(TestTools.generateMethodHeader(methodName));
	SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
	SecondaryStructure structure1 = null;
	SecondaryStructure structure2 = null;
	try {
	    structure1 = parser.parse("../test/fixtures/twohelices.sec");
	    structure2 = parser.parse("../test/fixtures/twohelicesNoSequence.sec");
	    int[] perm = SecondaryStructureTools.findSequencePermutation(structure1, structure2);
	    System.out.println(methodName + " : Found perm:" + Arrays.toString(perm));  
	    assert perm[0] == 1;
	    assert perm[1] == 0;
	    
	}
	catch (IOException ioe) {
	    System.out.println(ioe.getMessage());
	    assert false;
	}
	catch (ParseException pe) {
	    System.out.println(pe.getMessage());
	    assert false;
	}
	assert structure1 != null;
	assert structure2 != null;
	
	}
 
   @Test(groups={"new"})
    public void testStemFinder() {
	String methodName = "testStemFinder";
	System.out.println(TestTools.generateMethodHeader(methodName));
	String fileName = "../test/fixtures/hairpin.sec";
	SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
	SecondaryStructure structure = null;
	try {
	    structure = parser.parse(fileName);
	    List<Stem> stems = SecondaryStructureTools.findHelices(structure);
	    System.out.println(methodName + " : Found stems:");  
	    for (Stem stem : stems) {
	      System.out.println(stem);
	    }
	    assert stems.size() > 0;
	    
	}
	catch (IOException ioe) {
	    System.out.println(ioe.getMessage());
	    assert false;
	}
	catch (ParseException pe) {
	    System.out.println(pe.getMessage());
	    assert false;
	}
	assert structure != null;
	assert structure.getSequenceCount() == 2;
	
	}
 
 	   @Test(groups={"new"})
    public void testSingleStrandsFinder() {
	String methodName = "testSingleStrandsFinder";
	System.out.println(TestTools.generateMethodHeader(methodName));
	String fileName = "../test/fixtures/twohelices.sec";
	SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
	SecondaryStructure structure = null;
	try {
	    structure = parser.parse(fileName);
	    List<Interaction> sequenceSubs = SecondaryStructureTools.findSingleStrands(structure);
// 	    System.out.println(methodName + " : Found single strands:");  
// 	    for (SequenceSubset sequenceSub : sequenceSubs) {
// 	      System.out.println(sequenceSub);
// 	    }

		System.out.println("Strands found: " + sequenceSubs.size());
	    assert sequenceSubs.size() > 0;
	    
	}
	catch (IOException ioe) {
	    System.out.println(ioe.getMessage());
	    assert false;
	}
	catch (ParseException pe) {
	    System.out.println(pe.getMessage());
	    assert false;
	}
	assert structure != null;
	assert structure.getSequenceCount() == 2;
	
	}
	
// 	@Test(groups={"current"})
//  public void testInternalLoopFinder() {
// String methodName = "testInternalLoopFinder";
// System.out.println(TestTools.generateMethodHeader(methodName));
// String fileName = "../test/fixtures/429D.sec";
// SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
// SecondaryStructure structure = null;
// try {
// 	 structure = parser.parse(fileName);
// 	 System.out.println(structure);
// 	 List sequenceSubs = SecondaryStructureTools.findInternalLoops(structure);
// // 	    System.out.println(methodName + " : Found single strands:");  
// // 	    for (SequenceSubset sequenceSub : sequenceSubs) {
// // 	      System.out.println(sequenceSub);
// // 	    }
// 
//  System.out.println("Loops found: " + sequenceSubs.size());
// 	 assert sequenceSubs.size() > 0;
// 	 
// }
// catch (IOException ioe) {
// 	 System.out.println(ioe.getMessage());
// 	 assert false;
// }
// catch (ParseException pe) {
// 	 System.out.println(pe.getMessage());
// 	 assert false;
// }
// assert structure != null;
// assert structure.getSequenceCount() == 2;
// 
// }

}
