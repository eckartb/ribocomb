package nanotilertests.rnasecondary; 

import generaltools.*;
import org.testng.annotations.*;
import sequence.*;
import java.util.*;
import java.text.ParseException;
import java.io.*;
import secondarystructuredesign.*;
import generaltools.ResultWorker;
import generaltools.StringTools;
import rnasecondary.*;

public class SimpleSecondaryStructurePredictorTest {

    public static final int SIMPLE_MODE = 1;

    public static final int HXMATCH_MODE = 2;

    private static String fixturesDir = "../test/fixtures";

    private static Alphabet alphabet = DnaTools.AMBIGUOUS_RNA_ALPHABET;

    /** From 1994 Nature paper */
    static UnevenAlignment generateDnaCubeAlignment() {
	SimpleUnevenAlignment ali = new SimpleUnevenAlignment();
	try {
	    ali.addSequence(new SimpleSequence("CUUGCUCAUACUGCAUUCGGCCAGCCUGACAUCACCGUGUACGCCCAAACCUUUCAACUUAGAUGGUAGAAGGAGGGCAG", "A", alphabet));
	    ali.addSequence(new SimpleSequence("CGCUGUGGGUCAGGCUGGCCGAAUGCAGAGCCAAUCCUUGG", "B", alphabet));
	    ali.addSequence(new SimpleSequence("GAUUGGCUUAUGAGCAAGCUGCCCUCCUCGUUAGUU", "C", alphabet));
	    ali.addSequence(new SimpleSequence("CUGGAACUAACGUCUACCAUCUAAGUUGAAAGUCUCUUG", "D", alphabet));
	    ali.addSequence(new SimpleSequence("GUGACCAAGAGAGUUUGGGCGUACACGGUGAUCCACAGCGACUC", "E", alphabet));
	    ali.addSequence(new SimpleSequence("CGUGCUAACAGGUAGAGUUCGACGAAUUACACAAAUCGGCGCAAUACUAUCCCGACUUGGACCAGCCUUUCGCCAUCUCG", "F", alphabet));
	    ali.addSequence(new SimpleSequence("GUGAUUGGUAAUUCGUCGAACUCUACCCUGAAUGCGAGU", "G", alphabet));
	    ali.addSequence(new SimpleSequence("GCAUUCAGUGUUAGCACGCGAGAUGGCGUUCUGACG", "H", alphabet));
	    ali.addSequence(new SimpleSequence("GUCACCGUCAGAAAAAGGCUGGUCCAAGUCGGGGCAGCGUC", "I", alphabet));
	    ali.addSequence(new SimpleSequence("CCAGGACGCUGCAUAGUAUUGCGCCGAUUUGUCAAUCACCCAAG", "J", alphabet));
	}
	catch (UnknownSymbolException nse) {
	    System.out.println("Unknown sequence symbol: " + nse.getMessage());
	    assert false;
	}
	catch (DuplicateNameException dne) {
	    System.out.println("Duplicate name exception: " + dne.getMessage());
	    assert false;
	}
	return ali;
    }

    /** From 2004 Chemical Communications paper by Russell P. Goodman */
    static UnevenAlignment generateDnaTetrahedronAlignment() {
	SimpleUnevenAlignment ali = new SimpleUnevenAlignment();
	try {
	    ali.addSequence(new SimpleSequence(
	       "ACAUUCCUAAGUCUGAAACAUUACAGCUUGCUACACGAGAAGAGCCGCCAUAGUA", "A", alphabet));
	    ali.addSequence(new SimpleSequence(
               "UAUCACCAGGCAGUUGACAGUGUAGCAAGCUGUAAUAGAUGCGAGGGUCCAAUAC", "B", alphabet));
	    ali.addSequence(new SimpleSequence(
               "UCAACUGCCUGGUGAUAAAACGACACUACGUGGGAAUCUACUAUGGCGGCUCUUC", "C", alphabet));
	    ali.addSequence(new SimpleSequence(
               "UUCAGACUUAGGAAUGUGCUUCCCACGUAGUGUCGUUUGUAUUGGACCCUCGCAU", "D", alphabet));

	}
	catch (UnknownSymbolException nse) {
	    System.out.println("Unknown sequence symbol: " + nse.getMessage());
	    assert false;
	}
	catch (DuplicateNameException dne) {
	    System.out.println("Duplicate name exception: " + dne.getMessage());
	    assert false;
	}
	return ali;
    }

    @Test(groups={"new"})
    public void testDnaCubePrediction() {
	String methodName = "testDnaCubePrediction";
	System.out.println(TestTools.generateMethodHeader(methodName));
	UnevenAlignment ali = generateDnaCubeAlignment();
	SimpleSecondaryStructurePredictor predictor = new SimpleSecondaryStructurePredictor(ali);	
	predictor.run();
	SecondaryStructure cubeStructure = (SecondaryStructure)(predictor.getResult());
	assert cubeStructure != null;
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Predicted the following structure with " + cubeStructure.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(cubeStructure));
	assert cubeStructure.getSequenceCount() == 10; // must be this many sequences
// 	System.out.println("Starting to optimize cube sequences:");
// 	String[] result = optimizer.optimize(cubeStructure);
// 	System.out.println("Finished optimizing cube sequences:");
// 	assert result != null;
// 	for (int i = 0; i < result.length; ++i) {
// 	    System.out.println(result[i]);
// 	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new"})
    public void testDnaTetrahedronPrediction() {
	String methodName = "testDnaTetrahedronPrediction";
	System.out.println(TestTools.generateMethodHeader(methodName));
	UnevenAlignment ali = generateDnaTetrahedronAlignment();
	SimpleSecondaryStructurePredictor predictor = new SimpleSecondaryStructurePredictor(ali);	
	predictor.run();
	SecondaryStructure cubeStructure = (SecondaryStructure)(predictor.getResult());
	assert cubeStructure != null;
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Predicted the following structure with " + cubeStructure.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(cubeStructure));
	assert cubeStructure.getSequenceCount() == 10; // must be this many sequences
// 	System.out.println("Starting to optimize cube sequences:");
// 	String[] result = optimizer.optimize(cubeStructure);
// 	System.out.println("Finished optimizing cube sequences:");
// 	assert result != null;
// 	for (int i = 0; i < result.length; ++i) {
// 	    System.out.println(result[i]);
// 	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    public static void main(String[] args) {
	System.out.println("Starting simplefold!");
	if (args.length != 1) {
	    System.out.println("Usage: simplefold sequencefile");
	    System.exit(0);
	}
	String filename = args[0];
	FileInputStream fis = null;
	String[] sequences = null;
	UnevenAlignment ali = null;
	int predictionMode = SIMPLE_MODE;
	Alphabet alphabet = DnaTools.AMBIGUOUS_RNA_ALPHABET;
	try {
	    fis = new FileInputStream(filename);
	    sequences = StringTools.readAllLines(fis);
	    for (int i = 0; i < sequences.length; ++i) {
		sequences[i] = sequences[i].toUpperCase();
	    }
	    if (sequences[0].charAt(0) == '~') {
		SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
		SecondaryStructure sec = parser.parse(sequences);
		ali = sec.getSequences();
	    }
	    else {
		if (!DnaTools.checkSequencesCompatible(sequences, alphabet)) {
		    sequences = DnaTools.translateTU(sequences);
		}
		if (!DnaTools.checkSequencesCompatible(sequences, alphabet)) {
		    System.out.println("Alignment cannot be made compatible with RNA alphabet (A,C,G,U,N)");
		    System.exit(1);
		}
		ali = new SimpleUnevenAlignment(sequences, alphabet);
	    }
	}
	catch (IOException ioe) {
	    System.out.println("IO error reading " + filename + " : " + ioe.getMessage());
	    System.exit(1);
	}
	catch (ParseException pe) {
	    System.out.println("Parse error reading " + filename + " : " + pe.getMessage());
	    System.exit(1);
	}
	catch (DuplicateNameException dne) {
	    System.out.println(dne.getMessage());
	    System.exit(1);
	}
	catch (UnknownSymbolException use) {
	    System.out.println(use.getMessage());
	    System.exit(1);
	}
	assert ali != null;
	ResultWorker predictor = null;
	switch (predictionMode) {
	case SIMPLE_MODE: 
	    predictor = new SimpleSecondaryStructurePredictor(ali);	
	    break;
	case HXMATCH_MODE:
	    predictor = new HXMatchStructurePredictor(ali);	
	    break;
	default:
	    System.out.println("Unknown prediction mode encountered!");
	    System.exit(1);
	}
	predictor.run();
	SecondaryStructure cubeStructure = (SecondaryStructure)(predictor.getResult());
	assert cubeStructure != null;
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Predicted the following structure with " + cubeStructure.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(cubeStructure));
// 	System.out.println("Starting to optimize cube sequences:");
// 	String[] result = optimizer.optimize(cubeStructure);
// 	System.out.println("Finished optimizing cube sequences:");
// 	assert result != null;
// 	for (int i = 0; i < result.length; ++i) {
// 	    System.out.println(result[i]);
// 	}
	System.out.println("Good bye!");
    }


}
