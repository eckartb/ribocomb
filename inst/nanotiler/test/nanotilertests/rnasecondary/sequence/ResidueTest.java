package nanotilertests.rnasecondary.sequence;

import java.io.*;
import java.util.*;
import java.text.ParseException;
import generaltools.*;
import org.testng.annotations.*;
import rnasecondary.*;
import rnasecondary.substructures.*;
import sequence.Residue;
import sequence.Sequence;

public class ResidueTest {
  
    @Test(groups={"getPos"})
    public void testParse() {
    	String fileName = "../test/fixtures/5DI4.sec";
    	SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
    	SecondaryStructure structure;;
    	try {
    	    structure = parser.parse(fileName);
    	    System.out.println("Sequences: " + structure.getSequenceCount());
    	    System.out.println("Residues: "+structure.getSequences().getTotalResidueCount());
	    System.out.println("Length of first strand: " + structure.getSequence(0).size());
	    System.out.println("Length of second strand: " + structure.getSequence(1).size());
	    System.out.println("Getting position of residues in substructures, but Positions list only contains indexes in the first strand.");
	    
	    SubstructureMapper mapper = new SubstructureMapper(); //will find substructures in input structure
	    List<Substructure> subs = mapper.getSubstructures(structure); //Non-redundant list of largest possible substructures
	    int highest = 0;
	    int b = 0;
	    for(int i=0; i<subs.size(); i++){
		List<Residue> res = subs.get(i).getResidues();
		for (int k = 0; k < res.size(); k++){
		    Residue residue = res.get(k);
		    b = residue.getPos();
		    System.out.println ("Positions: " + b);
 		    System.out.println ("Parent structure: " + ((Sequence)(residue.getParentObject())).getName()); // toString());
		    if (b > highest) {
			highest =b;
		    }
		}
	    }
	    assert(highest>=structure.getSequences().getTotalResidueCount()-2);
	    System.out.println("Current highest: " + highest);
        }
    	catch (IOException ioe) {
    	    System.out.println(ioe.getMessage());
    	    assert false;
    	}
    	catch (ParseException pe) {
    	    System.out.println(pe.getMessage());
    	    assert false;
    	}
    }
}
