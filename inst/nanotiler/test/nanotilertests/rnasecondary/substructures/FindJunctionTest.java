package nanotilertests.rnasecondary.substructures;

import java.io.*;
import java.text.ParseException;
import generaltools.*;
import org.testng.annotations.*;
import rnasecondary.*;
import rnasecondary.substructures.*;

public class FindJunctionTest {

    @Test(groups={"y2016"})
    public void test3way() {
	// String[] lines = new String[12];
	// lines[0] = "~STRAND rT1";
	// lines[1] = "SEQUENCE  = GGAUGCUGGUACUUUUGAAACAUUUCGAGUCGCGAGGGUUUUCCCAUCGUUGGCCCGUAUCGCGUUUUCUUAUGAAGA";
	// lines[2] = "STRUCTURE = AAAAAAAAAAAA....BBBBBBBBBBBBBBBBBBBBBB....CCCCCCCCCCCCCCCCCCCCCC....DDDDDDDDDD";
  // lines[3] = "~STRAND rT2";
  // lines[4] = "SEQUENCE  = GGUCGCGACCUUCUUUUCCCUCGCGACUCGAAAUGUUUCUUUUCGAGGUCGCCC";
  // lines[5] = "STRUCTURE = .EEEEEEEEEEEE....BBBBBBBBBBBBBBBBBBBBBB....FFFFFFFFFFF";
  // lines[6] = "~STRAND rT3";
  // lines[7] = "SEQUENCE  = GGAUCUUUCGCCUUUUCGCGAUACGGGCCAACGAUGGGUUUUGAAGGUCGCGAC";
  // lines[8] = "STRUCTURE = GGGGGGGGGGGG....CCCCCCCCCCCCCCCCCCCCCC....EEEEEEEEEEEE";
  // lines[9] = "~STRAND rT4";
  // lines[10] = "SEQUENCE  = GGGCGACCUCGUUUUGUACCAGCAUCCUCUUCAUAAGUUUUGGCGAAAGAUCC";
  // lines[11] = "STRUCTURE = FFFFFFFFFFF....AAAAAAAAAAAADDDDDDDDDD....GGGGGGGGGGGG";

  // String[] lines = new String[6];
  // lines[0] = "~STRAND A";
  // lines[1] = "SEQUENCE  = cggaccgagccag";
  // lines[2] = "STRUCTURE = .AAAA..B..C..";
  // lines[3] = "~STRAND B";
  // lines[4] = "SEQUENCE  = gcugggagucc";
  // lines[5] = "STRUCTURE = .C..B..AAAA";
  
  // String[] lines = new String[12];
  // lines[0] = "~STRAND rT1";
  // lines[1] = "SEQUENCE  = CCCCCCCCCCC";
  // lines[2] = "STRUCTURE = ...A...B...";
  // lines[3] = "~STRAND rT2";
  // lines[4] = "SEQUENCE  = GGGGGGGGGGG";
  // lines[5] = "STRUCTURE = ...B...C...";
  // lines[6] = "~STRAND rT3";
  // lines[7] = "SEQUENCE  = AAAAAAAAAAA";
  // lines[8] = "STRUCTURE = ...C...D...";
  // lines[9] = "~STRAND rT4";
  // lines[10] = "SEQUENCE  = UUUUUUUUUUU";
  // lines[11] = "STRUCTURE = ...D...A...";
  
  String[] lines = new String[3];
  lines[0] = "~STRAND A";
  lines[1] = "SEQUENCE  = gggaugcguaggauaggugggagccugugaacccccgccuccgggugggggggaggcgccggugaaauaccacccuuccc";
  lines[2] = "STRUCTURE = ((((((...(..(.)((((((.(((...((((((((((((..))))))))))))))).....(...))))))))))))))";



	SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
	MutableSecondaryStructure structure = null;
	try {
	    structure = parser.parse(lines);
	    // System.out.println("Sequences (1): "+structure.getSequenceCount() );
	    // System.out.println("Resiudes (62): "+structure.getSequences().getTotalResidueCount() );
	    // System.out.println("Interactions (20): "+structure.getInteractionCount() );
	    // assert(structure.getSequenceCount() == 1);
	    // assert(structure.getSequences().getTotalResidueCount() == 62);
	    // assert(structure.getInteractionCount() == 20);
	}
	catch (ParseException pe) {
	    System.out.println(pe.getMessage());
	    assert false;
	}
  
  FindJunction fj = new FindJunction();
  fj.run(structure);
	
  SecondaryStructure junction = fj.getStructures().get(0);
  
  SecondaryStructureWriter writer = new SecondaryStructureScriptFormatWriter();
  String out = writer.writeString(junction);
  System.out.println(out);
  
  
    }
    
    @Test(groups={"y2016"})
    public void testCube(){
      SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
    	MutableSecondaryStructure structure = null;
    	try {
    	    structure = parser.parse("fixtures/cube_even.sec");
    	    // System.out.println("Sequences (1): "+structure.getSequenceCount() );
    	    // System.out.println("Resiudes (62): "+structure.getSequences().getTotalResidueCount() );
    	    // System.out.println("Interactions (20): "+structure.getInteractionCount() );
    	    // assert(structure.getSequenceCount() == 1);
    	    // assert(structure.getSequences().getTotalResidueCount() == 62);
    	    // assert(structure.getInteractionCount() == 20);
    	}
    	catch (Exception pe) {
    	    System.out.println(pe.getMessage());
    	    assert false;
    	}
      
      FindJunction fj = new FindJunction();
      fj.run(structure);
    }


}
