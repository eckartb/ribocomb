package nanotilertests.secondarystructuredesign;

import generaltools.StringTools;
import java.io.*;
import java.util.Properties;
import java.util.logging.*;
import rnasecondary.*;
import numerictools.AccuracyTools;
import sequence.*;
import launchtools.*;

import secondarystructuredesign.*;

import static rnasecondary.RnaInteractionType.*;

/** Assigns a score to a secondary structure and a set of potential sequences */
public class MatchFoldSwitchProbScorer implements SwitchScorer {
    
    private Level debugLevel = Level.INFO;
    private static Logger log = Logger.getLogger(secondarystructuredesign.PackageConstants.LOGFILE_DEFAULT);
    private boolean debugMode = false;
    private SecondaryStructureScorer scorer = new MatchFoldProbScorer();
    private double negativeWeight = 1.0; // weight of negative-design structures
    private double scoreOffset = 1000.0;

    /** returns error score for complete secondary structure and trial sequences */
    public Properties generateReport(StringBuffer[] bseqs,
				     SecondaryStructure structure1,
				     SecondaryStructure antiStructure1,
				     SecondaryStructure structure2,
				     SecondaryStructure antiStructure2,
				     int[] structure1SeqIds,
				     int[] structure2SeqIds) {
	assert(structure1SeqIds.length == structure1.getSequenceCount());
	assert(structure2SeqIds.length == structure2.getSequenceCount());
	// System.out.println("Starting MatchfoldSecondaryStructureScorer.scoreStructure...");
	Properties resultProperties = new Properties();
        int n = bseqs.length;
        int countNonZero1 = structure1SeqIds.length;
        int countNonZero2 = structure2SeqIds.length;
	StringBuffer[] bseqs1 = new StringBuffer[countNonZero1];
	StringBuffer[] bseqs2 = new StringBuffer[countNonZero2];
	int count = 0;
	for (int i = 0; i < structure1SeqIds.length; ++i) {
	    bseqs1[i] = bseqs[structure1SeqIds[i]];
	}
	for (int i = 0; i < structure2SeqIds.length; ++i) {
	    bseqs2[i] = bseqs[structure2SeqIds[i]];
	}
        int[][][][] matrices1 = structure1.generateInteractionMatrices();
        int[][][][] matrices2 = structure2.generateInteractionMatrices();
	double result1 = scorer.scoreStructure(bseqs1, structure1, matrices1);
	double result2 = scorer.scoreStructure(bseqs2, structure2, matrices2);
	double result = result1 + result2;
	double aResult1 = 0.0;
	double aResult2 = 0.0;
	if (antiStructure1 != null) {
	    int[][][][] aMatrices1 = antiStructure1.generateInteractionMatrices();
	    aResult1 = scorer.scoreStructure(bseqs1, antiStructure1, aMatrices1); // not great: requires another folding simulation
	    result = result - (negativeWeight * aResult1);
	}
	if (antiStructure2 != null) {
	    int[][][][] aMatrices2 = antiStructure2.generateInteractionMatrices();
	    aResult2 = scorer.scoreStructure(bseqs2, antiStructure2, aMatrices2); // not great: requires another folding simulation
	    result = result - (negativeWeight * aResult2);
	}
	result += scoreOffset;
	resultProperties.setProperty("score", "" + result);
	resultProperties.setProperty("score_1", "" + result1);
	resultProperties.setProperty("score_2", "" + result2);
	resultProperties.setProperty("neg_score_1", "" + aResult1);
	resultProperties.setProperty("neg_score_2", "" + aResult2);
	resultProperties.setProperty("score_offset", "" + scoreOffset);
	assert result >= 0.0;
	return resultProperties;
    }

    /** returns error score for complete secondary structure and trial sequences */
    public Properties generateReport(StringBuffer[] bseqs,
				     SecondaryStructure structure1,
				     SecondaryStructure structure2,
				     int[] structure1SeqIds,
				     int[] structure2SeqIds) {
	return generateReport(bseqs, structure1, null, structure2, null, structure1SeqIds, structure2SeqIds);
    }

    /** Sets new secondary structure scorer */
    public void setScorer(SecondaryStructureScorer _scorer) {
	this.scorer = _scorer;
    }

}
