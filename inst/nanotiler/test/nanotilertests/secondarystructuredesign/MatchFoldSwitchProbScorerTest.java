package nanotilertests.secondarystructuredesign;

import java.io.*;
import java.text.*;
import java.util.Date;
import java.util.Properties;
import generaltools.*;
import rnasecondary.*;
import secondarystructuredesign.*;
import static secondarystructuredesign.PackageConstants.*;

import org.testng.annotations.*;

/** Tests MonteCarloSequenceOptimizer */
public class MatchFoldSwitchProbScorerTest {

    private static String fixturesDir = NANOTILER_HOME + "/test/fixtures";

    private SecondaryStructure generateCubeSecondaryStructure(String name) {
	SecondaryStructure cubeStructure = null;
	SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
	String inputFileName = fixturesDir + "/" + name;
	String[] inputLines = null;
	try {
	    cubeStructure = parser.parse(inputFileName);
	}
	catch (IOException ioe) {
	    System.out.println("IOException while reading " + inputFileName + " : " + ioe.getMessage());
	    assert false;
	}
	catch (ParseException pe) {
	    System.out.println("ParseException parsing " + inputFileName + " : " + pe.getMessage());
	    assert false;	    
	}	
	assert cubeStructure != null;
	return cubeStructure;
    }

    private SecondaryStructure generateSecondaryStructureWithWeights(String name) {
	SecondaryStructure cubeStructure = null;
	SecondaryStructureParser parser = new SecondaryStructureParserWithWeights();
	String inputFileName = fixturesDir + "/" + name;
	String[] inputLines = null;
	try {
	    cubeStructure = parser.parse(inputFileName);
	}
	catch (IOException ioe) {
	    System.out.println("IOException while reading " + inputFileName + " : " + ioe.getMessage());
	    assert false;
	}
	catch (ParseException pe) {
	    System.out.println("ParseException parsing " + inputFileName + " : " + pe.getMessage());
	    assert false;	    
	}	
	assert cubeStructure != null;
	return cubeStructure;
    }

    /** Use cube sequences from Ned Seeman */
    @Test(groups={"newest_later", "fast"})
    public void testGenomeSwitch() {
	String methodName = "testGenomeSwitch";
	System.out.println(TestTools.generateMethodHeader(methodName));
	SecondaryStructure structure1 = generateSecondaryStructureWithWeights("polGenomeSwitchBound5.sec");
	SecondaryStructure structure2 = generateSecondaryStructureWithWeights("polGenomeSwitchUnbound5.sec");
	assert structure1 != null;
	assert structure2 != null;
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	writer.setWriteWeightsMode(true);
	System.out.println("Read the following structure 1 with " + structure1.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(structure1));
	System.out.println("Read the following structure 2 with " + structure2.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(structure2));
	assert structure1.getSequenceCount() == 2; // must be this many sequences
	assert structure2.getSequenceCount() == 1; // must be this many sequences
	System.out.println("Starting to optimize cube sequences:");
	int[] seqIds1 = new int[2];
	int[] seqIds2 = new int[1];
	seqIds1[0] = 0;
	seqIds1[1] = 1;
	seqIds1[0] = 0;
	StringBuffer[] bseqs = new StringBuffer[structure1.getSequenceCount()];
	for (int i = 0; i < structure1.getSequenceCount(); ++i) {
	    bseqs[i] = new StringBuffer(structure1.getSequence(i).sequenceString());
	}
	MatchFoldSwitchProbScorer scorer = new MatchFoldSwitchProbScorer();
	Properties report = scorer.generateReport(bseqs, structure1, structure2, seqIds1, seqIds2);
	System.out.println("Finished optimizing tetrahedron sequences:");
        PropertyTools.printProperties(System.out, report);
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Use cube sequences from Ned Seeman */
    @Test(groups={"new", "fast"})
    public void testGenomeSwitchWithWeights() {
	String methodName = "testGenomeSwitchWithWeights";
	System.out.println(TestTools.generateMethodHeader(methodName));
	SecondaryStructure structure1 = generateSecondaryStructureWithWeights("polGenomeSwitchBound5.sec");
	SecondaryStructure structure2 = generateSecondaryStructureWithWeights("polGenomeSwitchUnbound5.sec");
	assert structure1 != null;
	assert structure2 != null;
	assert structure1.weightsSanityCheck();
	assert structure2.weightsSanityCheck();
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	writer.setWriteWeightsMode(true);
	System.out.println("Read the following structure 1 with " + structure1.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(structure1));
	System.out.println("Read the following structure 2 with " + structure2.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(structure2));
	assert structure1.getSequenceCount() == 2; // must be this many sequences
	assert structure2.getSequenceCount() == 1; // must be this many sequences
	System.out.println("Starting to optimize cube sequences:");
	int[] seqIds1 = new int[2];
	int[] seqIds2 = new int[1];
	seqIds1[0] = 0;
	seqIds1[1] = 1;
	seqIds1[0] = 0;
	StringBuffer[] bseqs = new StringBuffer[structure1.getSequenceCount()];
	for (int i = 0; i < structure1.getSequenceCount(); ++i) {
	    bseqs[i] = new StringBuffer(structure1.getSequence(i).sequenceString());
	}
	SwitchScorer switchScorer = new MatchFoldSwitchProbScorer();
	MatchFoldWeightedProbScorer indScorer = new MatchFoldWeightedProbScorer();
	switchScorer.setScorer(indScorer);
	Properties report = switchScorer.generateReport(bseqs, structure1, structure2, seqIds1, seqIds2);
	System.out.println("Finished optimizing tetrahedron sequences:");
        PropertyTools.printProperties(System.out, report);
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

}
