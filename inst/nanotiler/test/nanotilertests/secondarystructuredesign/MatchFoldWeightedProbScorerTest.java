package nanotilertests.secondarystructuredesign;

import java.io.*;
import java.text.*;
import java.util.Date;
import java.util.Properties;
import generaltools.*;
import rnasecondary.*;
import secondarystructuredesign.*;

import static secondarystructuredesign.PackageConstants.*;

import org.testng.annotations.*;

/** Tests MonteCarloSequenceOptimizer */
public class MatchFoldWeightedProbScorerTest {

    private static String fixturesDir = NANOTILER_HOME + "/test/fixtures";

    private SecondaryStructure generateCubeSecondaryStructure(String name) {
	SecondaryStructure cubeStructure = null;
	SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
	String inputFileName = fixturesDir + "/" + name;
	String[] inputLines = null;
	try {
	    cubeStructure = parser.parse(inputFileName);
	}
	catch (IOException ioe) {
	    System.out.println("IOException while reading " + inputFileName + " : " + ioe.getMessage());
	    assert false;
	}
	catch (ParseException pe) {
	    System.out.println("ParseException parsing " + inputFileName + " : " + pe.getMessage());
	    assert false;	    
	}	
	assert cubeStructure != null;
	return cubeStructure;
    }

    private SecondaryStructure generateSecondaryStructureWithWeights(String name) {
	SecondaryStructure cubeStructure = null;
	SecondaryStructureParser parser = new SecondaryStructureParserWithWeights();
	String inputFileName = fixturesDir + "/" + name;
	String[] inputLines = null;
	try {
	    cubeStructure = parser.parse(inputFileName);
	}
	catch (IOException ioe) {
	    System.out.println("IOException while reading " + inputFileName + " : " + ioe.getMessage());
	    assert false;
	}
	catch (ParseException pe) {
	    System.out.println("ParseException parsing " + inputFileName + " : " + pe.getMessage());
	    assert false;	    
	}	
	assert cubeStructure != null;
	return cubeStructure;
    }

    /** Use cube sequences from Ned Seeman */
    @Test(groups={"new", "fast"})
    public void testBoundConstructWithWeights() {
	String methodName = "testBoundConstructWithWeights";
	System.out.println(TestTools.generateMethodHeader(methodName));
	SecondaryStructure structure1 = generateSecondaryStructureWithWeights("polGenomeSwitchBound5.sec");
	assert structure1 != null;
	assert structure1.weightsSanityCheck();
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	writer.setWriteWeightsMode(true);
	System.out.println("Read the following structure 1 with " + structure1.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(structure1));
	assert structure1.getSequenceCount() == 2; // must be this many sequences
	System.out.println("Starting to optimize cube sequences:");
	int[] seqIds1 = new int[2];
	seqIds1[0] = 0;
	seqIds1[1] = 1;
	seqIds1[0] = 0;
	StringBuffer[] bseqs1 = new StringBuffer[structure1.getSequenceCount()];
	for (int i = 0; i < structure1.getSequenceCount(); ++i) {
	    bseqs1[i] = new StringBuffer(structure1.getSequence(i).sequenceString());
	}
	MatchFoldWeightedProbScorer indScorer = new MatchFoldWeightedProbScorer();
	int[][][][] interactionMatrices = structure1.generateInteractionMatrices();
	Properties report = indScorer.generateReport(bseqs1, structure1, interactionMatrices);
	System.out.println("Finished optimizing tetrahedron sequences:");
        PropertyTools.printProperties(System.out, report);
	assert report.getProperty("score") != null;
	double score = Double.parseDouble(report.getProperty("score"));
	assert (score > 0.0);
	assert (score < 20.0);
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

}
