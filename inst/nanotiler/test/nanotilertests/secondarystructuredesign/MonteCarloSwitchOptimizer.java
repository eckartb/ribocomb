package nanotilertests.secondarystructuredesign;

import rnasecondary.*;
import java.util.logging.*;
import sequence.*;
import java.util.*;
import generaltools.PropertyCarrier;
import generaltools.PropertyTools;
import generaltools.Randomizer;
import generaltools.SimplePropertyCarrier;
import numerictools.IntegerArrayTools;
import secondarystructuredesign.*;

import static secondarystructuredesign.PackageConstants.*;

/** generates a set of sequences optimized to fullfill secondary structure */
public class MonteCarloSwitchOptimizer extends SimplePropertyCarrier implements SwitchOptimizer {
    private Logger log = Logger.getLogger("NanoTiler_debug");
    private Alphabet alphabet = DnaTools.RNA_ALPHABET;
    private double errorScoreLimit = 0.0; // limit for acceptable solution
    private int iterMax = 500;
    private double gcContent = 0.6;
    private int rerun = 1;
    private double kt = 0.5;
    private boolean ignoreNotConstantMode = false; // if true, set all non-constant residue interactions as UNKNOWN_SUBTYPE, effectively ignoring them
    // private double finalScoreWeight = 0.1;
    private Random rand = Randomizer.getInstance();
    private boolean randomizeFlag = true; // true;
    private int[] nucleotideProbabilities;
    private static final int CHAR_HELP_SIZE = 100;
    private char[] nucleotideHelperChars;
    // private SecondaryStructureScorer defaultScorer = new CritonScorer(); // new DefaultSecondaryStructureScorer();
    // private SecondaryStructureScorer finalScorer = new RnacofoldSecondaryStructureScorer();
    private SwitchScorer switchScorer = new MatchFoldSwitchProbScorer();
    private int stride1 = 100;
    private boolean doSanityChecks = false; // true;
    private Level debugLogLevel = Level.INFO;
    private SequenceConstraintScorer constraintScorer = null;
    private String algorithm = "twostage";

    public MonteCarloSwitchOptimizer() {
	nucleotideProbabilities = new int[alphabet.size()];
	initProbabilities();
    }

    public int getIterMax() { return iterMax; }

    private void initProbabilities() {
	nucleotideProbabilities[0] = (int)(CHAR_HELP_SIZE * (1-gcContent)/2); // A content in percent
	nucleotideProbabilities[1] = (int)(CHAR_HELP_SIZE * (gcContent)/2); // C
	nucleotideProbabilities[2] = (int)(CHAR_HELP_SIZE * (gcContent)/2); // G
	nucleotideProbabilities[3] = (int)(CHAR_HELP_SIZE * (1-gcContent)/2); // U
	int sum = 0;
	for (int i = 0; i < nucleotideProbabilities.length; ++i) {
	    sum += nucleotideProbabilities[i];
	}
	nucleotideHelperChars = new char[sum];
	int pc = 0;
	for (int i = 0; i < nucleotideProbabilities.length; ++i) {
	    int numChar = nucleotideProbabilities[i];
	    for (int j = 0; j < numChar; ++j) {
		if (pc >= nucleotideHelperChars.length) {
		    assert false; // should never happen
		}
		nucleotideHelperChars[pc++] = alphabet.getSymbol(i).getCharacter();
	    } 
	}
	if (pc != CHAR_HELP_SIZE) {
	    log.severe("Bad helper index: " + pc + " " + CHAR_HELP_SIZE);
	}
	StringBuffer buf = new StringBuffer();
	for (int i = 0; i <  nucleotideHelperChars.length; ++i) {
	    buf.append("" + nucleotideHelperChars[i]);
	}
	log.finest("Helper character array: " + buf.toString());
    }

    // internal class describing mutation of n'th sequence at position p with character c 
    private class Mutation {
	private int seqId; // sequence id
	private int pos; // position in sequence
	private char character; // character
	public Mutation(int seqId, int pos, char character) {
	    this.seqId = seqId;
	    this.pos = pos;
	    this.character = character;
	}
	public int getPos() { return pos; }
	public char getCharacter() { return character; }
	public int getSeqId() { return seqId; }
	public void setCharacter(char c) { this.character = c; }
        public String toString() { return "" + (seqId+1) + " " + (pos+1) + " " + character; }
    }

    public double getErrorScoreLimit() { return this.errorScoreLimit; }

    public SwitchScorer getSwitchScorer() { return this.switchScorer; }

    /** Returns ignoreNonConstantMode. See setIgnoreNonConstantMode for more details. */
    public boolean getIgnoreNotConstantMode() { return this.ignoreNotConstantMode; }

    // public double getFinalScoreWeight() { return this.finalScoreWeight; }

    public double getGcContent() { return gcContent; }

    /** returns kt value that determines acceptance probability of unfavourable mutations */
    public double getKt() { return kt; }

    public void setGcContent(double x) { this.gcContent = x; initProbabilities(); }

    public void setErrorScoreLimit(double x) { this.errorScoreLimit = x; }

    /** if true, set all non-constant residue interactions as UNKNOWN_SUBTYPE, effectively ignoring them. */
    public void setIgnoreNotConstantMode(boolean mode) { this.ignoreNotConstantMode = mode; }

    /** number of steps of first-stage optimazation */
    public void setIterMax(int n) { this.iterMax = n; }

    /** sets how many times the algorithm starts over */
    public void setRerun(int n) { this.rerun = n; }

    /** sets kt value that determines acceptance probability of unfavourable mutations */
    public void setKt(double kt) {
	assert kt > 0.0;
	this.kt = kt;
    }

    /** mutates a character (using nucleotide alphabet) if upper case */
    private char mutateCharacter(char c) {
	if (Character.isLowerCase(c)) {
	    return c;
	}
	char result = c;
	do {
	    result = nucleotideHelperChars[rand.nextInt(nucleotideHelperChars.length)];
	}
	while (result == c);
	return result;
    }

    /** randomizes sequence */
    private void randomizeSequence(StringBuffer buf) {
	int origLen = buf.length();
	for (int i = 0; i < buf.length(); ++i) {
	    if (Character.isUpperCase(buf.charAt(i))) {
		buf.setCharAt(i, mutateCharacter(buf.charAt(i)));
// 		if ((i>0) && (buf.charAt(i-1) == buf.charAt(i))) {
// 		    // avoid same nucleotides in a row, try again one more time:
// 		    buf.setCharAt(i, mutateCharacter(buf.charAt(i)));
// 		}
	    }
	}
	assert buf.length() == origLen; // no change in length
    }

    /** returns random watson crick pair (GC or AU) */
    private String generateRandomWatsonCrick() {
	String result = "";
	if (rand.nextDouble() > gcContent) {
	    result = "AU";
	}
	else {
	    result = "CG";
	}
	if (rand.nextDouble() > 0.5) {
	    return "" + result.charAt(1) + result.charAt(0);
	}
	assert result.length() == 2;
	return result;
    }

    /** returns true, if character is in alphabet */
    private boolean isInAlphabet(char c, Alphabet alphabet) {
	return alphabet.contains(c);
    }

    /** returns true, if characters are in alphabet */
    private boolean isInAlphabet(char c1, char c2, Alphabet alphabet1, Alphabet alphabet2) {
	return isInAlphabet(c1, alphabet1) && isInAlphabet(c2, alphabet2);
    }

    /** Returns: 0: AU, 1: CG, 2: GC, 3: UA */
    private String generateWatsonCrick(int id) {
	assert ((id >= 0) && (id < 4));
	switch (id) {
	case 0: return "AU";
	case 1: return "CG";
	case 2: return "GC";
	case 3: return "UA";
	}
	return null;
    }

    /** returns random watson crick pair (GC or AU) */
    private String generateRandomWatsonCrick(Alphabet alphabet1, Alphabet alphabet2) {
	List<Integer> ids = new ArrayList<Integer>();
	for (int i = 0; i < 4; ++i) {
	    String pair = generateWatsonCrick(i);
	    if (isInAlphabet(pair.charAt(0), pair.charAt(1), alphabet1, alphabet2)) {
		ids.add(i);
	    }
	}
	if (ids.size() == 0) {
	    System.out.println("Could not generate watson crick pairs for alphabets: " + alphabet1 + " + " + alphabet2);
	    return null;
	}
	int rid = rand.nextInt(ids.size());
	int id = ids.get(rid);
	String result = generateWatsonCrick(id);
	assert (isInAlphabet(result.charAt(0), result.charAt(1), alphabet1, alphabet2));
	return result;
    }

    private void randomizeSequences2(StringBuffer[] buf,
				     int[][][][] interactionMatrices,
				     List<Alphabet> alphabets) {
	assert ((buf != null) && (buf.length > 0));
	assert ((interactionMatrices != null) && (interactionMatrices.length == buf.length));
	assert((alphabets == null) || (alphabets.size() == buf.length));
	for (int i = 0; i < buf.length; ++i) {
	    randomizeSequence(buf[i]);
	}
	for (int i = 0; i < interactionMatrices.length; ++i) {
	    for (int j = i; j < interactionMatrices[i].length; ++j) {
		if (interactionMatrices[i][j].length != buf[i].length()) {
		    log.warning("" + i + " " + j + " " + interactionMatrices[i][j].length
				+ " " + buf[i].length());
		}
		assert interactionMatrices[i][j].length == buf[i].length();
		for (int k = 0; k < interactionMatrices[i][j].length; ++k) {
		    assert interactionMatrices[i][j][k].length == buf[j].length();
		    for (int m = 0; m < interactionMatrices[i][j][k].length; ++m) {
			if ((interactionMatrices[i][j][k][m] == RnaInteractionType.WATSON_CRICK)
			    && (!RnaSecondaryTools.isWatsonCrick(buf[i].charAt(k), buf[j].charAt(m)) ) ) {
			    if (Character.isUpperCase(buf[i].charAt(k)) && Character.isUpperCase(buf[j].charAt(m) ) ) {
				String randWC = generateRandomWatsonCrick();
				assert randWC != null;
				if (alphabets != null) {
				    randWC = generateRandomWatsonCrick(alphabets.get(i), alphabets.get(j));
				    assert((randWC != null) && (randWC.length() == 2) && isInAlphabet(randWC.charAt(0), alphabets.get(i))
					   && isInAlphabet(randWC.charAt(1), alphabets.get(j)));
				}
				assert randWC != null;
				assert randWC.length() == 2;
				assert buf != null;
				assert buf[i] != null;
				assert buf[j] != null;
				if (randWC == null) {
				    System.out.println("Randwc is null! Internal error!");
				    assert false;
				}
				char c1 = randWC.charAt(0);
				char c2 = randWC.charAt(1);
				StringBuffer buf1 = buf[i];
				StringBuffer buf2 = buf[j];
				buf1.setCharAt(k, c1);
				buf2.setCharAt(m, c2);
// 				if ((k > 0) && (m > 0)) {
// 				    if ((buf[i].charAt(k-1) == buf[i].charAt(k))
// 					|| (buf[j].charAt(m-1) == buf[j].charAt(m))) {
// 					// avoid duplication, try one more time:
// 					randWC = generateRandomWatsonCrick();
// 					buf[i].setCharAt(k, randWC.charAt(0));
// 					buf[j].setCharAt(m, randWC.charAt(1));
// 				    }
//				}
			    } else if (Character.isUpperCase(buf[i].charAt(k)) ) { // only first nt is mutable
				buf[i].setCharAt(k, generateWatsonCrickPartner(buf[j].charAt(m)));
			    } else if (Character.isUpperCase(buf[j].charAt(m) ) ) { // only second nt is mutable
				buf[j].setCharAt(m, generateWatsonCrickPartner(buf[i].charAt(k)));
			    }
			}
			
		    }
		}
	    }
	}
    }

    private void randomizeSequences2Orig(StringBuffer[] buf,
				     int[][][][] interactionMatrices,
				     List<Alphabet> alphabets) {
	assert ((buf != null) && (buf.length > 0));
	assert ((interactionMatrices != null) && (interactionMatrices.length == buf.length));
	assert((alphabets == null) || (alphabets.size() == buf.length));
	for (int i = 0; i < buf.length; ++i) {
	    randomizeSequence(buf[i]);
	}
	for (int i = 0; i < interactionMatrices.length; ++i) {
	    for (int j = i; j < interactionMatrices[i].length; ++j) {
		if (interactionMatrices[i][j].length != buf[i].length()) {
		    log.warning("" + i + " " + j + " " + interactionMatrices[i][j].length
				+ " " + buf[i].length());
		}
		assert interactionMatrices[i][j].length == buf[i].length();
		for (int k = 0; k < interactionMatrices[i][j].length; ++k) {
		    assert interactionMatrices[i][j][k].length == buf[j].length();
		    for (int m = 0; m < interactionMatrices[i][j][k].length; ++m) {
			if (Character.isUpperCase(buf[i].charAt(k)) && Character.isUpperCase(buf[j].charAt(m) ) ) {
			    if (interactionMatrices[i][j][k][m] == RnaInteractionType.WATSON_CRICK) {
				String randWC = generateRandomWatsonCrick();
				assert randWC != null;
				if (alphabets != null) {
				    randWC = generateRandomWatsonCrick(alphabets.get(i), alphabets.get(j));
				    assert((randWC != null) && (randWC.length() == 2) && isInAlphabet(randWC.charAt(0), alphabets.get(i))
					   && isInAlphabet(randWC.charAt(1), alphabets.get(j)));
				}
				assert randWC != null;
				assert randWC.length() == 2;
				assert buf != null;
				assert buf[i] != null;
				assert buf[j] != null;
				if (randWC == null) {
				    System.out.println("Randwc is null! Internal error!");
				    assert false;
				}
				char c1 = randWC.charAt(0);
				char c2 = randWC.charAt(1);
				StringBuffer buf1 = buf[i];
				StringBuffer buf2 = buf[j];
				buf1.setCharAt(k, c1);
				buf2.setCharAt(m, c2);
// 				if ((k > 0) && (m > 0)) {
// 				    if ((buf[i].charAt(k-1) == buf[i].charAt(k))
// 					|| (buf[j].charAt(m-1) == buf[j].charAt(m))) {
// 					// avoid duplication, try one more time:
// 					randWC = generateRandomWatsonCrick();
// 					buf[i].setCharAt(k, randWC.charAt(0));
// 					buf[j].setCharAt(m, randWC.charAt(1));
// 				    }
//				}
			    }
			}
			
		    }
		}
	    }
	}
    }

    private String generateSequencesOutputString(StringBuffer[] bufs) {
	StringBuffer result = new StringBuffer();
	for (int i = 0; i < bufs.length; ++i) {
	    result.append(bufs[i]);
	    result.append(NEWLINE);
	}
	return result.toString();
    }
    
    /** Initializes string buffers. If residue status is unknown, perform no optimization. */
    /*
    private StringBuffer[] initStringBuffers(SecondaryStructure structure, 
					     boolean randomizeFlag) {
	StringBuffer[] bseqs = new StringBuffer[structure.getSequenceCount()];
	for (int i = 0; i < bseqs.length; ++i) {
	    Sequence sequence = structure.getSequence(i);
	    bseqs[i] = new StringBuffer(sequence.sequenceString());
	    for (int j = 0; j < bseqs[i].length(); ++j) {
		Residue res = sequence.getResidue(j);
		if (SequenceStatus.fragment.equals(res.getProperty(SequenceStatus.name))) {
		    bseqs[i].setCharAt(j, Character.toLowerCase(bseqs[i].charAt(j)));
		}
		else if (SequenceStatus.adhoc.equals(res.getProperty(SequenceStatus.name))) {
		    bseqs[i].setCharAt(j, Character.toUpperCase(bseqs[i].charAt(j)));
		}
		else {
		    log.warning("Residue status unknown: " + bseqs[i] + " " + res.getSymbol() + " " 
				+ (i+1) + " " + (j+1));
		    // bseqs[i].setCharAt(j, Character.toUpperCase(bseqs[i].charAt(j)));
		    bseqs[i].setCharAt(j, Character.toLowerCase(bseqs[i].charAt(j))); // do not modify if unknown
		}
	    }
	    if (randomizeFlag) {
		randomizeSequence(bseqs[i]);
	    }
	}
	return bseqs;
    }
    */

    private Mutation generateMutation(StringBuffer[] bseqs, List<Alphabet> alphabets) {
	int seqId = 0;
	int pos = 0;
	char newChar = 'N';
	char oldChar = 'N';
	do {
	    seqId = rand.nextInt(bseqs.length);
	    pos = rand.nextInt(bseqs[seqId].length());
	    oldChar = bseqs[seqId].charAt(pos);
	}
	while (Character.isLowerCase(oldChar)); // do not mutate lower case characters. Careful: infinite loop if all lower char
	do {
	    Alphabet currAlph = alphabet;
	    if (alphabets != null) {
		currAlph = alphabets.get(seqId);
	    }
	    newChar = currAlph.getSymbol(rand.nextInt(currAlph.size())).getCharacter();
	}
	while (newChar == oldChar); // must be different
	assert Character.isUpperCase(bseqs[seqId].charAt(pos));
	return new Mutation(seqId, pos, newChar);
    }

    private char generateWatsonCrickPartner(char c) {
	c = Character.toUpperCase(c);
	switch (c) {
	case 'A': return 'U';
	case 'C': return 'G';
	case 'G': return 'C';
	case 'U': return 'A';
	}
	assert false; // should never be here
	return 'N';
    }

    /** If a given mutation is part of a base pair, find compensatory matching mutation.
     * TO DO: improve slow implementation!
     */
    Mutation findMatchingMutation(Mutation mutation,
				  int[][][][] interactionMatrices,
				  StringBuffer[] bseqs,
				  List<Alphabet> alphabets) {
	assert(alphabets != null);
	int seqId = mutation.getSeqId();
	int pos = mutation.getPos();
	char c = mutation.getCharacter();
	char cPart = generateWatsonCrickPartner(c); 
	for (int i = 0; i <= seqId; ++i) {
	    for (int j = 0; j < interactionMatrices[i][seqId].length; ++j) {
		if (interactionMatrices[i][seqId][j][pos] == RnaInteractionType.WATSON_CRICK) {
		    assert(alphabets != null);
		    // if (alphabets != null) {
		    if ((!alphabets.get(i).contains(cPart)) || Character.isLowerCase(bseqs[i].charAt(j))) {
			// log.info("Found bad matching mutation (1): " + new Mutation(i,j, cPart) + " for sequence: " + bseqs[i]);
			return new Mutation(-i-1, j, cPart); // mutation necessary but not acceptable
		    }
		    // }
                    assert(Character.isUpperCase(bseqs[i].charAt(j)));
		    // log.info("Found matching mutation (1): " + new Mutation(i,j, cPart) + " for sequence: " + bseqs[i]);
		    return new Mutation(i, j, cPart);
		}
	    }
	}
	for (int i = seqId+1; i < interactionMatrices[seqId].length; ++i) {
	    for (int j = 0; j < interactionMatrices[seqId][i][pos].length; ++j) {
		if (interactionMatrices[seqId][i][pos][j] == RnaInteractionType.WATSON_CRICK) {
		    assert(alphabets != null);
		    // if (alphabets != null) {
		    if ((!alphabets.get(i).contains(cPart)) || Character.isLowerCase(bseqs[i].charAt(j))) {
			// return null;
			// log.info("Found bad matching mutation (2): " + new Mutation(i,j, cPart) + " for sequence: " + bseqs[i]);
			return new Mutation(-i-1, j, cPart); // mutation necessary but not acceptable
		    }
			// }
                    assert(Character.isUpperCase(bseqs[i].charAt(j)));
		    // log.info("Found matching mutation (2): " + new Mutation(i,j, cPart) + " for sequence: " + bseqs[i]);
		    return new Mutation(i, j, cPart);
		}
	    }
	}
	return null; // compensatory mutation not necessary because original mutation is not part of a base pair
    }

//     private Mutation generateMatchingMutation(StringBuffer[] bseqs, Mutation prevMutation,
// 					      int[][][][] interactionMatrices, alphabets) {
// 	return findMatchingMutation(prevMutation, interactionMatrices, alphabets);
//     }

    /** Initializes string buffers. If residue status is unknown, perform optimization. */
    private StringBuffer[] initStringBuffers2(SecondaryStructure structure, 
					      boolean randomizeFlag,
					      int[][][][] interactionMatrices,
					      List<Alphabet> alphabets) {
	StringBuffer[] bseqs = new StringBuffer[structure.getSequenceCount()];
	for (int i = 0; i < bseqs.length; ++i) {
	    Sequence sequence = structure.getSequence(i);
	    bseqs[i] = new StringBuffer(sequence.sequenceString());
	    for (int j = 0; j < bseqs[i].length(); ++j) {
		Residue res = sequence.getResidue(j);
		if (SequenceStatus.fragment.equals(res.getProperty(SequenceStatus.name))) {
		    bseqs[i].setCharAt(j, Character.toLowerCase(bseqs[i].charAt(j)));
		}
		else if (SequenceStatus.adhoc.equals(res.getProperty(SequenceStatus.name))) {
		    bseqs[i].setCharAt(j, Character.toUpperCase(bseqs[i].charAt(j)));
		}
		else {
		    log.fine("Residue status unknown: " + bseqs[i] + " " + res.getSymbol() + " " 
				+ (i+1) + " " + (j+1) + " : setting for optimizable (adhoc)");
		    bseqs[i].setCharAt(j, Character.toUpperCase(bseqs[i].charAt(j))); // if residue status unknown, perform optimization
		    // bseqs[i].setCharAt(j, Character.toLowerCase(bseqs[i].charAt(j))); // if residue status unknown, perform NO optimization
		}
	    }
	}
	if (randomizeFlag) {
	    log.info("Randomizing sequences (2)!");
	    randomizeSequences2(bseqs, interactionMatrices, structure.getAlphabets() );
	}
	return bseqs;
    }

    private void applyMutation(Mutation mutation,
			       StringBuffer[] bseqs) {
	if (Character.isUpperCase(bseqs[mutation.getSeqId()].charAt(mutation.getPos()))) {
	    bseqs[mutation.getSeqId()].setCharAt(mutation.getPos(), mutation.getCharacter());
	}
	else {
	    // ignore mutation because residue was set to constant indicated by a lower case residue character
	}
    }

    /** Returns base pair matrix for two given sequence ids id1 and id2 */
    int[][] generateInteractionMatrix(SecondaryStructure structure, int id1, int id2) {
	Sequence seq1 = structure.getSequence(id1);
	Sequence seq2 = structure.getSequence(id2);
	int[][] result = new int[seq1.size()][seq2.size()];
	for (int i = 0; i < result.length; ++i) {
	    for (int j = 0; j < result[i].length; ++j) {
		result[i][j] = RnaInteractionType.NO_INTERACTION;
	    }
	}
	for (int i = 0; i < structure.getInteractionCount(); ++i) {
	    Interaction interaction = structure.getInteraction(i);
	    int pos1 = interaction.getResidue1().getPos();
	    int pos2 = interaction.getResidue2().getPos();
	    if ((interaction.getSequence1() == seq1) && (interaction.getSequence2() == seq2)) {
		result[pos1][pos2] = interaction.getInteractionType().getSubTypeId();
		if (id1 == id2) {
		    result[pos2][pos1] = result[pos1][pos2]; // if same sequence: symmetric
		}
	    }
	    else if ((interaction.getSequence2() == seq1) && (interaction.getSequence1() == seq2)) {
		result[pos2][pos1] = interaction.getInteractionType().getSubTypeId();		
		assert id1 != id2;
	    }
	}
	// sets rows and columns to UNKNOWN_INTERACTION, if property seqstatus is "ignore"
	for (int i = 0; i < result.length; ++i) {
	    if (("ignore".equals(seq1.getResidue(i).getProperty("seqstatus")))
		|| (ignoreNotConstantMode && SequenceStatus.fragment.equals(seq1.getResidue(i).getProperty(SequenceStatus.name)))) {
		for (int j = 0; j < result[i].length; ++j) {
		    result[i][j] = RnaInteractionType.UNKNOWN_SUBTYPE;
		}
	    }
	}
	for (int i = 0; i < result[0].length; ++i) {
	    if (("ignore".equals(seq2.getResidue(i).getProperty("seqstatus"))) 
		|| (ignoreNotConstantMode && SequenceStatus.fragment.equals(seq2.getResidue(i).getProperty(SequenceStatus.name)))) {
		for (int j = 0; j < result.length; ++j) {
		    result[j][i] = RnaInteractionType.UNKNOWN_SUBTYPE;
		}
	    }
	}
	return result;
    }

    /** Generates set of base pair matrices that depend on the sequence ids and the residue ids */
    int[][][][] generateInteractionMatrices(SecondaryStructure structure) {
	int[][][][] matrices = new int[structure.getSequenceCount()][structure.getSequenceCount()][0][0];
	for (int i = 0; i < structure.getSequenceCount(); ++i) {
	    for (int j = i; j < structure.getSequenceCount(); ++j) {
		matrices[i][j] = generateInteractionMatrix(structure, i, j);
	    }
	}
	return matrices;
    }

    /** Return true if all wanted base pairs correspond to GC and AU */
    private boolean sanityCheck(StringBuffer sb1,
				StringBuffer sb2,
				SecondaryStructure structure,
				int[][] interactionMatrices,
				int m, 
				int n ) {
	assert interactionMatrices.length == sb1.length();
	assert interactionMatrices[0].length == sb2.length();
	for (int i = 0; i < interactionMatrices.length; ++i) {
	    for (int j = 0; j < interactionMatrices[0].length; ++j) {
		if (interactionMatrices[i][j] == RnaInteractionType.WATSON_CRICK) {
		    if (!RnaSecondaryTools.isWatsonCrick(sb1.charAt(i), sb2.charAt(j))) {
			return false;
		    }
		}
	    }
	}
	return true;
    }

    /** Return true if all wanted base pairs correspond to GC and AU */
    private boolean sanityCheck(StringBuffer[] bseqs,
				SecondaryStructure structure,
				int[][][][] interactionMatrices) {
	for (int i = 0; i < bseqs.length; ++i) {
	    for (int j = i; j < bseqs.length; ++j) {
		if (!sanityCheck(bseqs[i], bseqs[j], structure, interactionMatrices[i][j], i, j)) {
		    return false;
		}
	    }
	}
	return true;
    }

    /** Return true if all wanted base pairs correspond to GC and AU */
    public boolean sanityCheckSlow(String[] seqs,
				SecondaryStructure structure) {
        System.out.println("sanityCheckSlow");
	int[][][][] interactionMatrices = generateInteractionMatrices(structure);
        StringBuffer[] bseqs = new StringBuffer[seqs.length];
	for (int i = 0; i < bseqs.length; ++i) {
            System.out.println("seqs[i]");
	    bseqs[i] = new StringBuffer(seqs[i]);
	}
        System.out.println("Structure: " + structure.toString());
	return sanityCheck(bseqs, structure, interactionMatrices);
    }

    /** Using only simple scorer */
    private double optimizationTrial(StringBuffer[] bseqs,
				     StringBuffer[] bseqs1, // must point to same buffers as bseqs
				     StringBuffer[] bseqs2, // must point to same buffers as bseqs
				     SecondaryStructure structure1,
				     SecondaryStructure antiStructure1,
				     SecondaryStructure structure2,
				     SecondaryStructure antiStructure2,
				     double oldScore,
				     int[][][][] interactionMatrices1,
				     int[][][][] interactionMatrices2,
				     int[] structure1SeqIds,
				     int[] structure2SeqIds,
				     List<Alphabet> alphabets) {
	log.finest("Starting optimizationTrial: " + oldScore);
	assert(alphabets != null);
 // 	if (doSanityChecks) {
// 	    assert(sanityCheck(bseqs, structure, interactionMatrices));
// 	}
	Mutation mutation = null; 
	Mutation mutation2 = null; 
	boolean first = rand.nextBoolean();
	do {
	    if (first) {
		mutation = generateMutation(bseqs1, alphabets);
		mutation2 = findMatchingMutation(mutation, interactionMatrices1, bseqs1, alphabets);
	    } else {
		mutation = generateMutation(bseqs2, alphabets);
		mutation2 = findMatchingMutation(mutation, interactionMatrices2, bseqs2, alphabets);
	    }
// 	    if (debugLogLevel > Level.INFO) {
// 		log.info("trying mutation: " + mutation);
// 		if (mutation2 != null) {
// 		    log.info("compensatory mutation: " + mutation2);
// 		} else {
// 		    log.info("no compensatory mutation found or necessary.");
// 		}
// 	    }
	} while ( (first && ((mutation2 != null) && (mutation2.getSeqId() < 0)) || (!structure1.isActive(mutation.getSeqId())))
		  || ( (!first) &&  ((mutation2 != null) && (mutation2.getSeqId() < 0)) || (!structure2.isActive(mutation.getSeqId() ) ) ) );
	int count = 0;
	char oldCharacter = ' ';
	if (first) {
	    oldCharacter = bseqs1[mutation.getSeqId()].charAt(mutation.getPos());
	    applyMutation(mutation, bseqs1); // mutate
	} else {
	    oldCharacter = bseqs2[mutation.getSeqId()].charAt(mutation.getPos());
	    applyMutation(mutation, bseqs2); // mutate
	}
	char oldCharacter2 = 'X';
	if (mutation2 != null) {
	    oldCharacter2 = ' ';
	    if (first) {
		oldCharacter2 = bseqs1[mutation2.getSeqId()].charAt(mutation2.getPos());
		applyMutation(mutation2, bseqs1); // mutate
	    } else {
		oldCharacter2 = bseqs2[mutation2.getSeqId()].charAt(mutation2.getPos());
		applyMutation(mutation2, bseqs1); // mutate
	    }
	} else {
// 	    if (debugLogLevel > Level.INFO) {
// 		log.info("No matching mutation found");
// 	    }
	}
	boolean undo = false;
	double newScore = oldScore;
// 	if (doSanityChecks) {
// 	    if(!sanityCheck(bseqs, structure, interactionMatrices)) {
// 		log.severe("Sanity check after sequence mutation failed!: " + NEWLINE + generateSequencesOutputString(bseqs));
		
// 		assert(false);
// 	    }
// 	}
	// newScore = // defaultScorer.scoreStructure(bseqs, structure, interactionMatrices);
	newScore = Double.parseDouble(switchScorer.generateReport(bseqs, structure1, antiStructure1, structure2, antiStructure2, 
								  structure1SeqIds, structure2SeqIds).getProperty("score"));
	log.fine("new score: " + newScore);
	if (Math.exp(-(newScore - oldScore)/kt) < rand.nextDouble()){ // worse, do not accept step
	    undo = true;
	}
	else {
	    if (newScore < oldScore) {
		log.finest("New score accepted: " + newScore + " " + oldScore);
	    }
	}
	if (undo) {
	    mutation.setCharacter(oldCharacter); // set to former character
	    if (first) {
		applyMutation(mutation, bseqs1);
	    } else {
		applyMutation(mutation, bseqs2);
	    }
	    if (mutation2 != null) {
		mutation2.setCharacter(oldCharacter2); // set to former character
		if (first) {
		    applyMutation(mutation2, bseqs1);
		} else {
		    applyMutation(mutation2, bseqs2);
		}
	    }
	    newScore = oldScore;
	}
// 	if (doSanityChecks) {
// 	    assert(sanityCheck(bseqs, structure, interactionMatrices));
// 	}
	log.finest("Finished optimizationTrial!");
	return newScore;
    }


    /** returns true if sequence has upper case characters */
    boolean hasUpperCaseCharacters(String s) {
	for (int i = 0; i < s.length(); ++i) {
	    if (Character.isUpperCase(s.charAt(i))) {
		return true;
	    }
	}
	return false;
    }
    
    /** returns true if sequence has upper case characters */
    boolean hasUpperCaseCharacters(StringBuffer[] bseqs) {
	for (int i = 0; i < bseqs.length; ++i) {
	    if (hasUpperCaseCharacters(bseqs[i].toString())) {
		return true;
	    }
	}
	return false;
    }

    private StringBuffer[] cloneBuffers(StringBuffer[] bufs) {
	assert bufs != null;
	StringBuffer[] result = new StringBuffer[bufs.length];
	for (int i = 0; i < bufs.length; ++i) {
	    result[i] = new StringBuffer(bufs[i].toString());
	}
	return result;
    }

    /** Optimize sequences for two given secondary structures. */
    Properties optimize1Stage(String[] sequences,
			      SecondaryStructure structure1,
			      SecondaryStructure antiStructure1,
			      SecondaryStructure structure2,
			      SecondaryStructure antiStructure2,
			      int[] structure1SeqIds,
			      int[] structure2SeqIds ) {
	assert(structure1.getAlphabets() != null);
	assert(structure2.getAlphabets() != null);
	assert switchScorer != null;
	log.info("Starting MonteCarloSequenceOptimizer.optimize !");
	int rerunCount = 0;
	StringBuffer[] bseqs = new StringBuffer[sequences.length];
	for (int i = 0; i < sequences.length; ++i) {
	    bseqs[i] = new StringBuffer(sequences[i]); // convert to StringBuffer
	}
	int[][][][] interactionMatrices1 = generateInteractionMatrices(structure1);
	int[][][][] interactionMatrices2 = generateInteractionMatrices(structure2);
	// StringBuffer[] bseqsBest = null;
	double errorScoreBest = 1e10;
	StringBuffer[] bseqsFinal = null;
	RERUN_LOOP: do {
	    StringBuffer[] bseqs1 = initStringBuffers2(structure1, randomizeFlag, interactionMatrices1, structure1.getAlphabets()); // default: optimization
	    StringBuffer[] bseqs2 = initStringBuffers2(structure2, randomizeFlag, interactionMatrices2, structure2.getAlphabets()); // default: optimization
	    for (int i = 0; i < structure1SeqIds.length; ++i) {
		bseqs[structure1SeqIds[i]] = bseqs1[i];
	    }
	    for (int i = 0; i < structure2SeqIds.length; ++i) {
		bseqs[structure2SeqIds[i]] = bseqs2[i];
	    }
	    double errorScore = Double.parseDouble(switchScorer.generateReport(bseqs, structure1, antiStructure1, structure2, antiStructure2, structure1SeqIds, structure2SeqIds).getProperty("score"));
	    if (errorScore < errorScoreBest) {
		errorScoreBest = errorScore;
	    }
	    log.info("Starting optimization run " + (rerunCount + 1) + " with score : " 
		     + errorScore + " and sequences: " + NEWLINE + generateSequencesOutputString(bseqs));
	    if (!hasUpperCaseCharacters(bseqs)) {
		log.warning("No residue can be optimized because they are all in lower case characters!");
		break;
	    }
	    int stage1Steps = 0;
	    for (int i = 0; (i < iterMax) && (errorScore > 0.0); ++i, ++stage1Steps) {
		// apply default scorer:
// 		if (!sanityCheck(bseqs, structure, interactionMatrices)) {
// 		    log.log(debugLogLevel, "Not all base pairs are yet compatible!");
// 		} else {
// 		    // log.fine("Sanity checks passed!");
// 		}
		assert(structure1.getAlphabets() != null);
		errorScore = optimizationTrial(bseqs, bseqs1, bseqs2, structure1, antiStructure1, structure2, antiStructure2, errorScore, interactionMatrices1, interactionMatrices2, structure1SeqIds, structure2SeqIds, structure1.getAlphabets());
		// 		if (doSanityChecks) {
// 		    sanityCheck(bseqs, structure, interactionMatrices);
// 		}
		// log.info("New error Score: " + errorScore + " " + i);
		if (((i % stride1) == 0) || (errorScore < errorScoreBest)) {
		    System.out.println("Round " + (rerunCount+1) + " : Sequences at iteration " + (i+1) + " and score: " + errorScore
				       + " so far best: " + errorScoreBest);
		    System.out.println(generateSequencesOutputString(bseqs));
		    // PropertyTools.printProperties(System.out,defaultScorer.generateReport(bseqs, structure, interactionMatrices));
		}

			// check if basic rules are fullfilled:
			// errorScore = defaultScorer.scoreStructure(bseqs, structure, interactionMatrices);		    
// 			if (doSanityChecks) {
// 			    assert sanityCheck(bseqs, structure, interactionMatrices);
// 			}
		// errorScoreTot = optimizationTrial(bseqs, structure, errorScoreTot, interactionMatrices, true, structure.getAlphabets());
// 		if (doSanityChecks) {
// 		    assert sanityCheck(bseqs, structure, interactionMatrices);
// 		}
		if (errorScore < errorScoreBest) {
		    log.info("Found new best sequences with score: " 
			     + errorScore + " at iteration " + (i+1));
		    errorScoreBest = errorScore;
		    bseqsFinal = cloneBuffers(bseqs);
		}
		
	    }
	    log.info("Ended optimization run after round " + (rerunCount + 1) + " steps " + stage1Steps 
		     + " with strings: " + NEWLINE + generateSequencesOutputString(bseqs));
	}
	while ((++rerunCount < rerun)); // restart if not optimal score was found
	// AFTER_OPT: // label to allow quitting of optimization
	if (bseqsFinal == null) {
	    log.warning("Sorry, no sequences that fullfill all optimization criteria found!");
	    return null;
	}
	String[] result = new String[bseqsFinal.length]; // translate back to strings
	log.info("Best error score of all runs: " + errorScoreBest);
	setProperty("best_score", "" + errorScoreBest);
        
	log.info("Final strings: " + NEWLINE + generateSequencesOutputString(bseqsFinal));
	for (int i = 0; i < bseqsFinal.length; ++i) {
	    result[i] = bseqsFinal[i].toString();
	}
	Properties resultReport = switchScorer.generateReport(bseqsFinal, structure1, antiStructure1, structure2, antiStructure2, structure1SeqIds, structure2SeqIds);
	return resultReport; 
    }

    /* public String[] optimize(SecondaryStructure structure) {
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Starting sequence optimization with algorithm " + algorithm + " and structure:");
	System.out.println(writer.writeString(structure));
	if (algorithm.equals("twostage")) {
	    return optimize2Stage(structure);
	} else if (algorithm.equals("systematic")) {
	    return optimizeSystematic(structure);
	} else {
	    log.warning("Unknown optimization algorithm! Defined: twostage");
	}
	return null;
    }
    */

    /** Optimize sequences for two given secondary structures. */
    public Properties optimize(String[] sequences,
                        SecondaryStructure structure1,
	  	        SecondaryStructure structure2,
		        int[] structure1SeqIds,
		        int[] structure2SeqIds ) {
	return optimize(sequences, structure1, null, structure2, null, structure1SeqIds, structure2SeqIds);
    }

    /** Optimize sequences for two given secondary structures. */
    public Properties optimize(String[] sequences,
			       SecondaryStructure structure1,
			       SecondaryStructure antiStructure1,
			       SecondaryStructure structure2,
			       SecondaryStructure antiStructure2,
			       int[] structure1SeqIds,
			       int[] structure2SeqIds ) {
	StringBuffer[] bseqs = new StringBuffer[sequences.length];
	for (int i = 0; i < sequences.length; ++i) {
	    bseqs[i] = new StringBuffer(sequences[i]); // convert to StringBuffer
	}
	return optimize1Stage(sequences, structure1, antiStructure1, structure2, antiStructure2, structure1SeqIds, structure2SeqIds);
    }


    /** Optimize sequences for two given secondary structures. */
    public Properties eval(String[] sequences,
		    SecondaryStructure structure1,
		    SecondaryStructure structure2,
		    int[] structure1SeqIds,
		    int[] structure2SeqIds ) {
	StringBuffer[] bseqs = new StringBuffer[sequences.length];
	for (int i = 0; i < sequences.length; ++i) {
	    bseqs[i] = new StringBuffer(sequences[i]); // convert to StringBuffer
	}
	return switchScorer.generateReport(bseqs, structure1, structure2, structure1SeqIds, structure2SeqIds);
    }


    /** If true, randomize sequences upon initialization */
    public void setRandomizeFlag(boolean flag) { this.randomizeFlag = flag; }

    public void setSwitchScorer(SwitchScorer scorer) { this.switchScorer = scorer; }

}
