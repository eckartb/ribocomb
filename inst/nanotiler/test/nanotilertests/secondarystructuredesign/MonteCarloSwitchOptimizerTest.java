package nanotilertests.secondarystructuredesign;

import java.io.*;
import java.text.*;
import java.util.*;
import generaltools.*;
import rnasecondary.*;
import secondarystructuredesign.*;

import static secondarystructuredesign.PackageConstants.*;

import org.testng.annotations.*;

/** Tests MonteCarloSequenceOptimizer */
public class MonteCarloSwitchOptimizerTest {

    private static String fixturesDir = NANOTILER_HOME + "/test/fixtures";

    private SecondaryStructure generateCubeSecondaryStructure(String name) {
	SecondaryStructure cubeStructure = null;
	SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
	String inputFileName = fixturesDir + "/" + name;
	String[] inputLines = null;
	try {
	    cubeStructure = parser.parse(inputFileName);
	}
	catch (IOException ioe) {
	    System.out.println("IOException while reading " + inputFileName + " : " + ioe.getMessage());
	    assert false;
	}
	catch (ParseException pe) {
	    System.out.println("ParseException parsing " + inputFileName + " : " + pe.getMessage());
	    assert false;	    
	}	
	assert cubeStructure != null;
	return cubeStructure;
    }

    private SecondaryStructure generateWeightedSecondaryStructure(String name) {
	SecondaryStructure cubeStructure = null;
	SecondaryStructureParser parser = new SecondaryStructureParserWithWeights();
	String inputFileName = fixturesDir + "/" + name;
	String[] inputLines = null;
	try {
	    cubeStructure = parser.parse(inputFileName);
	}
	catch (IOException ioe) {
	    System.out.println("IOException while reading " + inputFileName + " : " + ioe.getMessage());
	    assert false;
	}
	catch (ParseException pe) {
	    System.out.println("ParseException parsing " + inputFileName + " : " + pe.getMessage());
	    assert false;	    
	}	
	assert cubeStructure != null;
	return cubeStructure;
    }

    /** Test of DNA tetrahedron */
    @Test(groups={"new"})
    public void testDnaTetrahedronSwitchOptimize() {
	String methodName = "testDnaTetrahedronSwitchOptimize";
	System.out.println(TestTools.generateMethodHeader(methodName));
	SecondaryStructure structure = generateCubeSecondaryStructure("tetrahedron.sec");
	assert structure != null;
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure with " + structure.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(structure));
	assert structure.getSequenceCount() == 4; // must be this many sequences
	System.out.println("Starting to optimize cube sequences:");
	int[] seqIds = new int[4];
	seqIds[0] = 0;
	seqIds[1] = 1;
	seqIds[2] = 2;
	seqIds[3] = 3;
	String[] sequences = new String[structure.getSequenceCount()];
	StringBuffer[] bseqs = new StringBuffer[structure.getSequenceCount()];
	for (int i = 0; i < structure.getSequenceCount(); ++i) {
	    bseqs[i] = new StringBuffer(structure.getSequence(i).sequenceString());
	    sequences[i] = structure.getSequence(i).sequenceString();
	}
	MatchFoldSwitchProbScorer scorer = new MatchFoldSwitchProbScorer();
	SwitchOptimizer optimizer = new MonteCarloSwitchOptimizer();
	Properties report = scorer.generateReport(bseqs, structure, structure, seqIds, seqIds);
	System.out.println("Report of tetrahedron sequences:");
        PropertyTools.printProperties(System.out, report);
	Properties result = optimizer.optimize(sequences, structure, structure, seqIds, seqIds);
	System.out.println("Result of sequence optimization:");
        PropertyTools.printProperties(System.out, result);
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** First attempt at switch structure */
    @Test(groups={"new"})
    public void testGenomeSwitchOptimize() {
	String methodName = "testGenomeSwitchOptimize";
	System.out.println(TestTools.generateMethodHeader(methodName));
	SecondaryStructure structure1 = generateCubeSecondaryStructure("polGenomeSwitchBound.sec");
	SecondaryStructure structure2 = generateCubeSecondaryStructure("polGenomeSwitchUnbound.sec");
	assert structure1 != null;
	assert structure2 != null;
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure with " + structure1.getSequenceCount() + " sequences:");
	System.out.println("Read the following structure with " + structure2.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(structure1));
	System.out.println(writer.writeString(structure2));
	assert structure1.getSequenceCount() == 2; // must be this many sequences
	assert structure2.getSequenceCount() == 1; // must be this many sequences
	System.out.println("Starting to optimize genome switch sequences:");
	int[] seqIds1 = new int[2];
	seqIds1[0] = 0;
	seqIds1[1] = 1;
	int[] seqIds2 = new int[1];
	seqIds2[0] = 0;
	String[] sequences = new String[structure1.getSequenceCount()];
	StringBuffer[] bseqs = new StringBuffer[structure1.getSequenceCount()];
	for (int i = 0; i < structure1.getSequenceCount(); ++i) {
	    bseqs[i] = new StringBuffer(structure1.getSequence(i).sequenceString());
	    sequences[i] = structure1.getSequence(i).sequenceString();
	}
	MatchFoldSwitchProbScorer scorer = new MatchFoldSwitchProbScorer();
	SwitchOptimizer optimizer = new MonteCarloSwitchOptimizer();
	optimizer.setIterMax(5000);
	optimizer.setRerun(5);
	Properties report = scorer.generateReport(bseqs, structure1, structure2, seqIds1, seqIds2);
	System.out.println("Report of genome switch sequences:");
        PropertyTools.printProperties(System.out, report);
	Properties result = optimizer.optimize(sequences, structure1, structure2, seqIds1, seqIds2);
	System.out.println("Result of sequence optimization:");
        PropertyTools.printProperties(System.out, result);
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Second, slightly modified attempt at switch structure */
    @Test(groups={"new"})
    public void testGenomeSwitchOptimize2() {
	String methodName = "testGenomeSwitchOptimize2";
	System.out.println(TestTools.generateMethodHeader(methodName));
	SecondaryStructure structure1 = generateCubeSecondaryStructure("polGenomeSwitchBound2.sec");
	SecondaryStructure structure2 = generateCubeSecondaryStructure("polGenomeSwitchUnbound2.sec");
	assert structure1 != null;
	assert structure2 != null;
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure with " + structure1.getSequenceCount() + " sequences:");
	System.out.println("Read the following structure with " + structure2.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(structure1));
	System.out.println(writer.writeString(structure2));
	assert structure1.getSequenceCount() == 2; // must be this many sequences
	assert structure2.getSequenceCount() == 1; // must be this many sequences
	System.out.println("Starting to optimize genome switch sequences:");
	int[] seqIds1 = new int[2];
	seqIds1[0] = 0;
	seqIds1[1] = 1;
	int[] seqIds2 = new int[1];
	seqIds2[0] = 0;
	String[] sequences = new String[structure1.getSequenceCount()];
	StringBuffer[] bseqs = new StringBuffer[structure1.getSequenceCount()];
	for (int i = 0; i < structure1.getSequenceCount(); ++i) {
	    bseqs[i] = new StringBuffer(structure1.getSequence(i).sequenceString());
	    sequences[i] = structure1.getSequence(i).sequenceString();
	}
	MatchFoldSwitchProbScorer scorer = new MatchFoldSwitchProbScorer();
	SwitchOptimizer optimizer = new MonteCarloSwitchOptimizer();
	optimizer.setIterMax(5000);
	optimizer.setRerun(5);
	Properties report = scorer.generateReport(bseqs, structure1, structure2, seqIds1, seqIds2);
	System.out.println("Report of genome switch sequences:");
        PropertyTools.printProperties(System.out, report);
	Properties result = optimizer.optimize(sequences, structure1, structure2, seqIds1, seqIds2);
	System.out.println("Result of sequence optimization:");
        PropertyTools.printProperties(System.out, result);
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Third attempt at switch structure: much shorter construct. */
    @Test(groups={"newest_later"})
    public void testGenomeSwitchOptimize3() {
	String methodName = "testGenomeSwitchOptimize3";
	System.out.println(TestTools.generateMethodHeader(methodName));
	SecondaryStructure structure1 = generateCubeSecondaryStructure("polGenomeSwitchBound3.sec");
	SecondaryStructure structure2 = generateCubeSecondaryStructure("polGenomeSwitchUnbound3.sec");
	structure1.generateDefaultWeights();
	structure2.generateDefaultWeights();
	System.out.println("Weights of structure 1:\n");
	for (int i = 0; i < structure1.getSequenceCount(); ++i) {
	    System.out.println("Weights of sequence" + (i+1) + ":");
	    List<Double> weights = structure1.getWeights(i);
	    for (int j = 0; j < weights.size(); ++j) {
		System.out.println("" + j + " " + weights.get(j));
	    }
	}
	System.out.println("Weights of structure 2:\n");
	for (int i = 0; i < structure2.getSequenceCount(); ++i) {
	    System.out.println("Weights of sequence" + (i+1) + ":");
	    List<Double> weights = structure2.getWeights(i);
	    for (int j = 0; j < weights.size(); ++j) {
		System.out.println("" + j + " " + weights.get(j));
	    }
	}
	assert structure1 != null;
	assert structure2 != null;
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure with " + structure1.getSequenceCount() + " sequences:");
	System.out.println("Read the following structure with " + structure2.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(structure1));
	System.out.println(writer.writeString(structure2));
	assert structure1.getSequenceCount() == 2; // must be this many sequences
	assert structure2.getSequenceCount() == 1; // must be this many sequences
	System.out.println("Starting to optimize genome switch sequences:");
	int[] seqIds1 = new int[2];
	seqIds1[0] = 0;
	seqIds1[1] = 1;
	int[] seqIds2 = new int[1];
	seqIds2[0] = 0;
	String[] sequences = new String[structure1.getSequenceCount()];
	StringBuffer[] bseqs = new StringBuffer[structure1.getSequenceCount()];
	for (int i = 0; i < structure1.getSequenceCount(); ++i) {
	    bseqs[i] = new StringBuffer(structure1.getSequence(i).sequenceString());
	    sequences[i] = structure1.getSequence(i).sequenceString();
	}
	MatchFoldSwitchProbScorer scorer = new MatchFoldSwitchProbScorer();
	SwitchOptimizer optimizer = new MonteCarloSwitchOptimizer();
	optimizer.setIterMax(5000);
	optimizer.setRerun(5);
	Properties report = scorer.generateReport(bseqs, structure1, structure2, seqIds1, seqIds2);
	System.out.println("Report of genome switch sequences:");
        PropertyTools.printProperties(System.out, report);
	Properties result = optimizer.optimize(sequences, structure1, structure2, seqIds1, seqIds2);
	System.out.println("Result of sequence optimization:");
        PropertyTools.printProperties(System.out, result);
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Fourth attempt at switch structure: using residue-weighted target structures. */
    @Test(groups={"new"})
    public void testGenomeWeightedSwitchOptimize() {
	String methodName = "testGenomeWeightedSwitchOptimize";
	System.out.println(TestTools.generateMethodHeader(methodName));
	SecondaryStructure structure1 = generateWeightedSecondaryStructure("polGenomeSwitchBound6.sec");
	SecondaryStructure structure2 = generateWeightedSecondaryStructure("polGenomeSwitchUnbound6.sec");
	assert (structure1.weightsSanityCheck());
	assert (structure2.weightsSanityCheck());
	// structure1.generateDefaultWeights();
	// structure2.generateDefaultWeights();
	System.out.println("Weights of structure 1:\n");
	boolean foundZero = false;
	for (int i = 0; i < structure1.getSequenceCount(); ++i) {
	    System.out.println("Weights of sequence" + (i+1) + ":");
	    List<Double> weights = structure1.getWeights(i);
	    for (int j = 0; j < weights.size(); ++j) {
		System.out.println("" + j + " " + weights.get(j));
		if (weights.get(j) == 0.0) {
		    foundZero = true;
		}
	    }
	}
	System.out.println("Weights of structure 2:\n");
	for (int i = 0; i < structure2.getSequenceCount(); ++i) {
	    System.out.println("Weights of sequence" + (i+1) + ":");
	    List<Double> weights = structure2.getWeights(i);
	    for (int j = 0; j < weights.size(); ++j) {
		System.out.println("" + j + " " + weights.get(j));
		if (weights.get(j) == 0.0) {
		    foundZero = true;
		}
	    }
	}
	assert structure1 != null;
	assert structure2 != null;
	if (!foundZero) {
	    System.out.println("Could not find residues with zero weights. Exiting.");
	    assert false;
	}
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure with " + structure1.getSequenceCount() + " sequences:");
	System.out.println("Read the following structure with " + structure2.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(structure1));
	System.out.println(writer.writeString(structure2));
	assert structure1.getSequenceCount() == 2; // must be this many sequences
	assert structure2.getSequenceCount() == 1; // must be this many sequences
	System.out.println("Starting to optimize genome switch sequences:");
	int[] seqIds1 = new int[2];
	seqIds1[0] = 0;
	seqIds1[1] = 1;
	int[] seqIds2 = new int[1];
	seqIds2[0] = 0;
	String[] sequences = new String[structure1.getSequenceCount()];
	StringBuffer[] bseqs = new StringBuffer[structure1.getSequenceCount()];
	for (int i = 0; i < structure1.getSequenceCount(); ++i) {
	    bseqs[i] = new StringBuffer(structure1.getSequence(i).sequenceString());
	    sequences[i] = structure1.getSequence(i).sequenceString();
	}
	MatchFoldSwitchProbScorer switchScorer = new MatchFoldSwitchProbScorer();
	MatchFoldWeightedProbScorer weightedIndScorer = new MatchFoldWeightedProbScorer();
	switchScorer.setScorer(weightedIndScorer);
	MonteCarloSwitchOptimizer optimizer = new MonteCarloSwitchOptimizer();
	optimizer.setSwitchScorer(switchScorer); // replace by weighted scorer
	optimizer.setIterMax(5000);
	optimizer.setRerun(5);
	optimizer.setRandomizeFlag(false);
	Properties report = switchScorer.generateReport(bseqs, structure1, structure2, seqIds1, seqIds2);
	System.out.println("Report of genome switch sequences:");
        PropertyTools.printProperties(System.out, report);
	Properties result = optimizer.optimize(sequences, structure1, structure2, seqIds1, seqIds2);
	System.out.println("Result of sequence optimization:");
        PropertyTools.printProperties(System.out, result);
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Fourth attempt at switch structure: using residue-weighted target structures. */
    @Test(groups={"new"})
    public void testGenomeWeightedSwitchOptimize20() {
	String methodName = "testGenomeWeightedSwitchOptimize20";
	System.out.println(TestTools.generateMethodHeader(methodName));
	SecondaryStructure structure1 = generateWeightedSecondaryStructure("polGenomeSwitchBound20.sec");
	SecondaryStructure structure2 = generateWeightedSecondaryStructure("polGenomeSwitchUnbound20.sec");
	assert (structure1.weightsSanityCheck());
	assert (structure2.weightsSanityCheck());
	// structure1.generateDefaultWeights();
	// structure2.generateDefaultWeights();
	System.out.println("Weights of structure 1:\n");
	boolean foundZero = false;
	for (int i = 0; i < structure1.getSequenceCount(); ++i) {
	    System.out.println("Weights of sequence" + (i+1) + ":");
	    List<Double> weights = structure1.getWeights(i);
	    for (int j = 0; j < weights.size(); ++j) {
		System.out.println("" + j + " " + weights.get(j));
		if (weights.get(j) == 0.0) {
		    foundZero = true;
		}
	    }
	}
	System.out.println("Weights of structure 2:\n");
	for (int i = 0; i < structure2.getSequenceCount(); ++i) {
	    System.out.println("Weights of sequence" + (i+1) + ":");
	    List<Double> weights = structure2.getWeights(i);
	    for (int j = 0; j < weights.size(); ++j) {
		System.out.println("" + j + " " + weights.get(j));
		if (weights.get(j) == 0.0) {
		    foundZero = true;
		}
	    }
	}
	assert structure1 != null;
	assert structure2 != null;
	if (!foundZero) {
	    System.out.println("Could not find residues with zero weights. Exiting.");
	    assert false;
	}
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure with " + structure1.getSequenceCount() + " sequences:");
	System.out.println("Read the following structure with " + structure2.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(structure1));
	System.out.println(writer.writeString(structure2));
	assert structure1.getSequenceCount() == 2; // must be this many sequences
	assert structure2.getSequenceCount() == 1; // must be this many sequences
	System.out.println("Starting to optimize genome switch sequences:");
	int[] seqIds1 = new int[2];
	seqIds1[0] = 0;
	seqIds1[1] = 1;
	int[] seqIds2 = new int[1];
	seqIds2[0] = 0;
	String[] sequences = new String[structure1.getSequenceCount()];
	StringBuffer[] bseqs = new StringBuffer[structure1.getSequenceCount()];
	for (int i = 0; i < structure1.getSequenceCount(); ++i) {
	    bseqs[i] = new StringBuffer(structure1.getSequence(i).sequenceString());
	    sequences[i] = structure1.getSequence(i).sequenceString();
	}
	MatchFoldSwitchProbScorer switchScorer = new MatchFoldSwitchProbScorer();
	MatchFoldWeightedProbScorer weightedIndScorer = new MatchFoldWeightedProbScorer();
	switchScorer.setScorer(weightedIndScorer);
	MonteCarloSwitchOptimizer optimizer = new MonteCarloSwitchOptimizer();
	optimizer.setSwitchScorer(switchScorer); // replace by weighted scorer
	optimizer.setIterMax(5000);
	optimizer.setRerun(5);
	optimizer.setRandomizeFlag(false);
	Properties report = switchScorer.generateReport(bseqs, structure1, structure2, seqIds1, seqIds2);
	System.out.println("Report of genome switch sequences:");
        PropertyTools.printProperties(System.out, report);
	Properties result = optimizer.optimize(sequences, structure1, structure2, seqIds1, seqIds2);
	System.out.println("Result of sequence optimization:");
        PropertyTools.printProperties(System.out, result);
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Fourth attempt at switch structure: using residue-weighted target structures. */
    @Test(groups={"new"})
    public void testTwistTSurvivinOptimize() {
	String methodName = "testTwistTSurvivinOptimize";
	System.out.println(TestTools.generateMethodHeader(methodName));
	SecondaryStructure structure1 = generateWeightedSecondaryStructure("TWIST_SurvivinBound_849.sec");
	SecondaryStructure structure2 = generateWeightedSecondaryStructure("TWIST_SurvivinUnbound_849.sec");
	assert (structure1.weightsSanityCheck());
	assert (structure2.weightsSanityCheck());
	// structure1.generateDefaultWeights();
	// structure2.generateDefaultWeights();
	System.out.println("Weights of structure 1:\n");
	boolean foundZero = false;
	for (int i = 0; i < structure1.getSequenceCount(); ++i) {
	    System.out.println("Weights of sequence" + (i+1) + ":");
	    List<Double> weights = structure1.getWeights(i);
	    for (int j = 0; j < weights.size(); ++j) {
		System.out.println("" + j + " " + weights.get(j));
		if (weights.get(j) == 0.0) {
		    foundZero = true;
		}
	    }
	}
	System.out.println("Weights of structure 2:\n");
	for (int i = 0; i < structure2.getSequenceCount(); ++i) {
	    System.out.println("Weights of sequence" + (i+1) + ":");
	    List<Double> weights = structure2.getWeights(i);
	    for (int j = 0; j < weights.size(); ++j) {
		System.out.println("" + j + " " + weights.get(j));
		if (weights.get(j) == 0.0) {
		    foundZero = true;
		}
	    }
	}
	assert structure1 != null;
	assert structure2 != null;
	if (!foundZero) {
	    System.out.println("Could not find residues with zero weights. Exiting.");
	    assert false;
	}
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure with " + structure1.getSequenceCount() + " sequences:");
	System.out.println("Read the following structure with " + structure2.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(structure1));
	System.out.println(writer.writeString(structure2));
	assert structure1.getSequenceCount() == 2; // must be this many sequences
	assert structure2.getSequenceCount() == 1; // must be this many sequences
	System.out.println("Starting to optimize genome switch sequences:");
	int[] seqIds1 = new int[2];
	seqIds1[0] = 0;
	seqIds1[1] = 1;
	int[] seqIds2 = new int[1];
	seqIds2[0] = 0;
	String[] sequences = new String[structure1.getSequenceCount()];
	StringBuffer[] bseqs = new StringBuffer[structure1.getSequenceCount()];
	for (int i = 0; i < structure1.getSequenceCount(); ++i) {
	    bseqs[i] = new StringBuffer(structure1.getSequence(i).sequenceString());
	    sequences[i] = structure1.getSequence(i).sequenceString();
	}
	MatchFoldSwitchProbScorer switchScorer = new MatchFoldSwitchProbScorer();
	MatchFoldWeightedProbScorer weightedIndScorer = new MatchFoldWeightedProbScorer();
	switchScorer.setScorer(weightedIndScorer);
	MonteCarloSwitchOptimizer optimizer = new MonteCarloSwitchOptimizer();
	optimizer.setSwitchScorer(switchScorer); // replace by weighted scorer
	optimizer.setIterMax(1000);
	optimizer.setRerun(1);
	optimizer.setRandomizeFlag(false);
	Properties report = switchScorer.generateReport(bseqs, structure1, structure2, seqIds1, seqIds2);
	System.out.println("Report of genome switch sequences:");
        PropertyTools.printProperties(System.out, report);
	Properties result = optimizer.optimize(sequences, structure1, structure2, seqIds1, seqIds2);
	System.out.println("Result of sequence optimization:");
        PropertyTools.printProperties(System.out, result);
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Fourth attempt at switch structure: using residue-weighted target structures. */
    @Test(groups={"newest_later"})
    public void testTwistTSurvivinAntiOptimize() {
	String methodName = "testTwistTSurvivinAntiOptimize";
	System.out.println(TestTools.generateMethodHeader(methodName));
	SecondaryStructure structure1 = generateWeightedSecondaryStructure("TWIST_SurvivinBound_849.sec");
	SecondaryStructure antiStructure1 = null; // generateWeightedSecondaryStructure("TWIST_SurvivinAntiBound_849.sec");
	SecondaryStructure structure2 = generateWeightedSecondaryStructure("TWIST_SurvivinUnbound_849.sec");
	SecondaryStructure antiStructure2 = generateWeightedSecondaryStructure("TWIST_SurvivinAntiUnbound_849.sec");
	assert (structure1.weightsSanityCheck());
	assert (structure2.weightsSanityCheck());
	// structure1.generateDefaultWeights();
	// structure2.generateDefaultWeights();
	System.out.println("Weights of structure 1:\n");
	boolean foundZero = false;
	for (int i = 0; i < structure1.getSequenceCount(); ++i) {
	    System.out.println("Weights of sequence" + (i+1) + ":");
	    List<Double> weights = structure1.getWeights(i);
	    for (int j = 0; j < weights.size(); ++j) {
		System.out.println("" + j + " " + weights.get(j));
		if (weights.get(j) == 0.0) {
		    foundZero = true;
		}
	    }
	}
	System.out.println("Weights of structure 2:\n");
	for (int i = 0; i < structure2.getSequenceCount(); ++i) {
	    System.out.println("Weights of sequence" + (i+1) + ":");
	    List<Double> weights = structure2.getWeights(i);
	    for (int j = 0; j < weights.size(); ++j) {
		System.out.println("" + j + " " + weights.get(j));
		if (weights.get(j) == 0.0) {
		    foundZero = true;
		}
	    }
	}
	assert structure1 != null;
	assert structure2 != null;
	if (!foundZero) {
	    System.out.println("Could not find residues with zero weights. Exiting.");
	    assert false;
	}
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure 1 with " + structure1.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(structure1));
// 	System.out.println("Read the following anti-structure 1 with " + antiStructure1.getSequenceCount() + " sequences:");
// 	System.out.println(writer.writeString(antiStructure1));
	System.out.println("Read the following structure 2 with " + structure2.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(structure2));
	System.out.println("Read the following anti-structure 2 with " + structure2.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(antiStructure2));
	assert structure1.getSequenceCount() == 2; // must be this many sequences
	assert structure2.getSequenceCount() == 1; // must be this many sequences
	System.out.println("Starting to optimize genome switch sequences:");
	int[] seqIds1 = new int[2];
	seqIds1[0] = 0;
	seqIds1[1] = 1;
	int[] seqIds2 = new int[1];
	seqIds2[0] = 0;
	String[] sequences = new String[structure1.getSequenceCount()];
	StringBuffer[] bseqs = new StringBuffer[structure1.getSequenceCount()];
	for (int i = 0; i < structure1.getSequenceCount(); ++i) {
	    bseqs[i] = new StringBuffer(structure1.getSequence(i).sequenceString());
	    sequences[i] = structure1.getSequence(i).sequenceString();
	}
	MatchFoldSwitchProbScorer switchScorer = new MatchFoldSwitchProbScorer();
	MatchFoldWeightedProbScorer weightedIndScorer = new MatchFoldWeightedProbScorer();
	switchScorer.setScorer(weightedIndScorer);
	MonteCarloSwitchOptimizer optimizer = new MonteCarloSwitchOptimizer();
	optimizer.setSwitchScorer(switchScorer); // replace by weighted scorer
	optimizer.setIterMax(1000);
	optimizer.setRerun(1);
	optimizer.setRandomizeFlag(false);
	Properties report = switchScorer.generateReport(bseqs, structure1, antiStructure1, structure2, antiStructure2, seqIds1, seqIds2);
	System.out.println("Report of genome switch sequences:");
        PropertyTools.printProperties(System.out, report);
	Properties result = optimizer.optimize(sequences, structure1, antiStructure1, structure2, antiStructure2, seqIds1, seqIds2);
	System.out.println("Result of sequence optimization:");
        PropertyTools.printProperties(System.out, result);
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Fourth attempt at switch structure: using residue-weighted target structures.
	TWIST_SurvivinBound_329.sec
	TWIST_SurvivinBound_414.sec
	TWIST_SurvivinBound_543.sec
	TWIST_SurvivinBound_849.sec
	TWIST_SurvivinBound_1165.sec
    */
    @Test(groups={"newest_later"})
    public void testTwistTSurvivinOptimize2() {
	String methodName = "testTwistTSurvivinOptimize2";
	System.out.println(TestTools.generateMethodHeader(methodName));
	int[] ids = new int[5];
	ids[0] = 329;
	ids[1] = 414;
	ids[2] = 543;
	ids[3] = 849;
	ids[4] = 1165;
	for (int id : ids) {
	    System.out.println("Working on id " + id);
	SecondaryStructure structure1 = generateWeightedSecondaryStructure("TWIST_SurvivinBound_" + id + ".sec");
	SecondaryStructure structure2 = generateWeightedSecondaryStructure("TWIST_SurvivinUnbound_" + id + ".sec");
	assert (structure1.weightsSanityCheck());
	assert (structure2.weightsSanityCheck());
	// structure1.generateDefaultWeights();
	// structure2.generateDefaultWeights();
	System.out.println("Weights of structure 1:\n");
	boolean foundZero = false;
// 	for (int i = 0; i < structure1.getSequenceCount(); ++i) {
// 	    System.out.println("Weights of sequence" + (i+1) + ":");
// 	    List<Double> weights = structure1.getWeights(i);
// 	    for (int j = 0; j < weights.size(); ++j) {
// 		System.out.println("" + j + " " + weights.get(j));
// 		if (weights.get(j) == 0.0) {
// 		    foundZero = true;
// 		}
// 	    }
// 	}
// 	System.out.println("Weights of structure 2:\n");
// 	for (int i = 0; i < structure2.getSequenceCount(); ++i) {
// 	    System.out.println("Weights of sequence" + (i+1) + ":");
// 	    List<Double> weights = structure2.getWeights(i);
// 	    for (int j = 0; j < weights.size(); ++j) {
// 		System.out.println("" + j + " " + weights.get(j));
// 		if (weights.get(j) == 0.0) {
// 		    foundZero = true;
// 		}
// 	    }
// 	}
// 	assert structure1 != null;
// 	assert structure2 != null;
// 	if (!foundZero) {
// 	    System.out.println("Could not find residues with zero weights. Exiting.");
// 	    assert false;
// 	}
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure with " + structure1.getSequenceCount() + " sequences:");
	System.out.println("Read the following structure with " + structure2.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(structure1));
	System.out.println(writer.writeString(structure2));
	assert structure1.getSequenceCount() == 2; // must be this many sequences
	assert structure2.getSequenceCount() == 1; // must be this many sequences
	System.out.println("Starting to optimize genome switch sequences:");
	int[] seqIds1 = new int[2];
	seqIds1[0] = 0;
	seqIds1[1] = 1;
	int[] seqIds2 = new int[1];
	seqIds2[0] = 0;
	String[] sequences = new String[structure1.getSequenceCount()];
	StringBuffer[] bseqs = new StringBuffer[structure1.getSequenceCount()];
	for (int i = 0; i < structure1.getSequenceCount(); ++i) {
	    bseqs[i] = new StringBuffer(structure1.getSequence(i).sequenceString());
	    sequences[i] = structure1.getSequence(i).sequenceString();
	}
	MatchFoldSwitchProbScorer switchScorer = new MatchFoldSwitchProbScorer();
	MatchFoldWeightedProbScorer weightedIndScorer = new MatchFoldWeightedProbScorer();
	switchScorer.setScorer(weightedIndScorer);
	MonteCarloSwitchOptimizer optimizer = new MonteCarloSwitchOptimizer();
	optimizer.setSwitchScorer(switchScorer); // replace by weighted scorer
	optimizer.setIterMax(5000);
	optimizer.setRerun(5);
	optimizer.setRandomizeFlag(false);
	Properties report = switchScorer.generateReport(bseqs, structure1, structure2, seqIds1, seqIds2);
	System.out.println("Report of genome switch sequences for id :" + id);
        PropertyTools.printProperties(System.out, report);
// 	Properties result = optimizer.optimize(sequences, structure1, structure2, seqIds1, seqIds2);
// 	System.out.println("Result of sequence optimization:");
//         PropertyTools.printProperties(System.out, result);
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** General method for running switch structure: using residue-weighted target structures. */
    public static Properties runSwitchOptimize(SecondaryStructure structure1, SecondaryStructure structure2, int[] seqIds2)  {
	// String methodName = "runSwitchOptimize";
	// System.out.println(TestTools.generateMethodHeader(methodName));
	// SecondaryStructureParser parser = new SecondaryStructureParserWithWeights();
	// SecondaryStructure structure1 = parser.parse(name1);
	// SecondaryStructure structure2 = parser.parse(name2);
	assert structure1 != null;
	assert structure2 != null;
	assert (structure1.weightsSanityCheck());
	assert (structure2.weightsSanityCheck());
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure with " + structure1.getSequenceCount() + " sequences:");
	System.out.println("Read the following structure with " + structure2.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(structure1));
	System.out.println(writer.writeString(structure2));
	int[] seqIds1 = new int[structure1.getSequenceCount()];
	String[] sequences = new String[structure1.getSequenceCount()];
	StringBuffer[] bseqs = new StringBuffer[structure1.getSequenceCount()];
	for (int i = 0; i < structure1.getSequenceCount(); ++i) {
	    seqIds1[i] = i;
	    bseqs[i] = new StringBuffer(structure1.getSequence(i).sequenceString());
	    sequences[i] = structure1.getSequence(i).sequenceString();
	}
	MatchFoldSwitchProbScorer switchScorer = new MatchFoldSwitchProbScorer();
	MatchFoldWeightedProbScorer weightedIndScorer = new MatchFoldWeightedProbScorer();
	switchScorer.setScorer(weightedIndScorer);
	MonteCarloSwitchOptimizer optimizer = new MonteCarloSwitchOptimizer();
	optimizer.setSwitchScorer(switchScorer); // replace by weighted scorer
	optimizer.setIterMax(5000);
	optimizer.setRerun(5);
	optimizer.setRandomizeFlag(false);
	Properties report = switchScorer.generateReport(bseqs, structure1, structure2, seqIds1, seqIds2);
	System.out.println("Report of genome switch sequences:");
        PropertyTools.printProperties(System.out, report);
	Properties result = optimizer.optimize(sequences, structure1, structure2, seqIds1, seqIds2);
	// System.out.println("Result of sequence optimization:");
        // PropertyTools.printProperties(System.out, result);
	// System.out.println(TestTools.generateMethodFooter(methodName));
	return result;
    }

}
