package nanotilertests.secondarystructuredesign;

import java.text.ParseException;
import java.util.logging.*;
import rnasecondary.*;
import java.io.*;
import static secondarystructuredesign.PackageConstants.*;
import generaltools.StringTools;
import generaltools.TestTools;
import secondarystructuredesign.*;

import org.testng.annotations.*;

public class RiboswitchSecondaryStructureYESScorerTest {

    private static Logger log = Logger.getLogger("mcsopt");
    private int debugLevel = 1;

    private static void helpOutput(PrintStream ps) {
	ps.println("Usage: mcsopt filename\n");
    }

    public static void exitError(String message) {
	System.out.println(message);
	System.exit(0);
    }

    private static void outputStrings(PrintStream ps,
		       String[] lines) {
	for (int i = 0; i < lines.length; ++i) {
	    ps.println(lines[i]);
	}
    }

    private static char prettyPrintChar(PrintStream ps, 
				 char c1, char c2) {
	if (RnaSecondaryTools.isRnaComplement(c1, c2)) {
	    return '1';
	}
	return ' ';
    }

    private static char prettyPrintChar(PrintStream ps, 
					String s1,
					String s2,
					int i, int j) {
	char c1 = s1.charAt(i);
	char c2 = s2.charAt(j);
	if ( (i <= 0) || (j <= 0) || ((i+1) >= s1.length())
	     || ((j+1) >= s2.length()))  {
	    return prettyPrintChar(ps, c1, c2);
	}
	else {
	    char c1a = s1.charAt(i-1);
	    char c2a = s2.charAt(j+1);
	    char c1b = s1.charAt(i+1);
	    char c2b = s2.charAt(j-1);
	    if (RnaSecondaryTools.isRnaComplement(c1, c2)
		&& (RnaSecondaryTools.isRnaComplement(c1a, c2a)
		    || RnaSecondaryTools.isRnaComplement(c1b, c2b) ) ) {
		return '1';
	    }
	}
	return ' ';
    }
    
    private static void prettyPrintLine(PrintStream ps, 
			     String seq1, String sec1,
			     String seq2, String sec2,
			     int i) {
	for (int j = 0; j < seq1.length(); ++j) {
	    ps.print("" + prettyPrintChar(ps, seq1, seq2, j, i));
	}
	ps.println(" " + seq2.charAt(i) + " " + sec2.charAt(i));
    }

    private static void prettyPrint(PrintStream ps, 
			     String seq1, String sec1,
			     String seq2, String sec2) {
	ps.println(seq1);
	ps.println(sec1);
	for (int i = 0; i < seq2.length(); ++i) {
	    prettyPrintLine(ps, seq1, sec1, seq2, sec2, i);
	}
    }

    private static void prettyPrint(PrintStream ps, 
				    String seq1, String seq2) {
	prettyPrint(ps, seq1, seq1, seq2, seq2);
    }

    private static void prettyPrint(PrintStream ps, 
				    SecondaryStructure struct,
				    int i, int j) {
	prettyPrint(ps, struct.getSequence(i).sequenceString(),
		    struct.getSequence(j).sequenceString());

    }

    public static void prettyPrint(PrintStream ps, SecondaryStructure struct) {
	for (int i = 0; i < struct.getSequenceCount(); ++i) {
	    for (int j = i; j < struct.getSequenceCount(); ++j) {
		ps.println("Sequence " + (i+1) + " versus " + (j+1));
		prettyPrint(ps, struct, i, j);
	    }
	}
    }

    public static void prettyPrint(PrintStream ps, String[] sequences) {
	for (int i = 0; i < sequences.length; ++i) {
	    for (int j = i; j < sequences.length; ++j) {
		ps.println("Sequence " + (i+1) + " versus " + (j+1));
		prettyPrint(ps, sequences[i], sequences[j]);
		ps.println();
	    }
	}
    }

    // yeslogic_rnafoldc_inp.sec
    @Test(groups={"new"})
    public void testRNAfoldConstraints1() {
	String methodName = "testRNAfoldConstraints1";
	System.out.println(TestTools.generateMethodHeader(methodName));
	// String fileName = NANOTILER_HOME + "/test/fixtures/yeslogic_rnafoldc_inp.sec";
	RiboswitchSecondaryStructureYESScorer scorer = new RiboswitchSecondaryStructureYESScorer();
	String sequence = "GGGCGACCCUGAUGAGCUUGAGUUUAGCUCGUCACUGUCCAGGUUCAAUCAGGCGAAACGGUGAAAGCCGUAGGUUGCCC";
	String constraints = "-------------------------xxxxxxxxxxxxxxxxxxxxxx---------------------------------";
	try {
	    String[] result = scorer.launchRnafold(sequence, constraints);
	    System.out.println(StringTools.paste(result, "\n"));
	}
	catch (IOException ioe) {
	    System.out.println("IOException in " + methodName + " : " + ioe.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Reads Breaker "YES-logic" sequences */
    @Test(groups={"new", "slow"})
    public void testBistableYESOptimization1() {
	String methodName = "testBistableYESOptimization1";
	System.out.println(TestTools.generateMethodHeader(methodName));
      	String fileName = NANOTILER_HOME + "/test/fixtures/yeslogic_ON.sec";
	SecondaryStructure secStruct = null;
	log.info("Importing secondary structure from " + fileName);
	try {
	    SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
	    secStruct = parser.parse(fileName);
	    assert secStruct != null;
	}
	catch (IOException ioe) {
	    log.warning("IO error when scraping result file from: " + fileName);
	    exitError(ioe.getMessage());
	}
	catch (ParseException pe) {
	    log.warning("Parsing error when scraping result file from: " + fileName);
	    exitError(pe.getMessage());
	}
	if (secStruct == null) {
	    exitError("Error parsing secondary structure!");
	}
	else {
	    log.info("Secondary structure correctly read!");
	}
	SecondaryStructureWriter writer = new SecondaryStructureScriptFormatWriter();
	String resultString = writer.writeString(secStruct);
	// System.out.println("Initial structure before optimization: " + NEWLINE + resultString);
	// prettyPrint(System.out, secStruct);
	String[] newSequences;
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setIterMax(4000);
	optimizer.setIter2Max(200);
	optimizer.setErrorScoreLimit(2.0);
	RiboswitchSecondaryStructureYESScorer scorer = null;
	scorer = new RiboswitchSecondaryStructureYESScorer();
	optimizer.setFinalScorer(scorer);
	newSequences = optimizer.optimize(secStruct);
	if ((newSequences != null) && (newSequences.length == 2)) {
	    System.out.println("Optimization finished!");
	    // prettyPrint(System.out, newSequences);
	    System.out.println("New sequence(s): ");
	    outputStrings(System.out, newSequences);
	    System.out.println("Concatenated sequences: ");
	    System.out.println(newSequences[0] + "&" + newSequences[1]);
	}
	else {
	    System.out.println("Warning: could not generate optimized sequences in method " + methodName);
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Reads Breaker "YES-logic" sequences, using ? place holders */
    @Test(groups={"new", "slow"})
    public void testBistableYESOptimization1b() {
	String methodName = "testBistableYESOptimization1b";
	System.out.println(TestTools.generateMethodHeader(methodName));
      	String fileName = NANOTILER_HOME + "/test/fixtures/yeslogic_ON_B.sec";
	SecondaryStructure secStruct = null;
	log.info("Importing secondary structure from " + fileName);
	try {
	    SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
	    secStruct = parser.parse(fileName);
	    assert secStruct != null;
	}
	catch (IOException ioe) {
	    log.warning("IO error when scraping result file from: " + fileName);
	    exitError(ioe.getMessage());
	}
	catch (ParseException pe) {
	    log.warning("Parsing error when scraping result file from: " + fileName);
	    exitError(pe.getMessage());
	}
	if (secStruct == null) {
	    exitError("Error parsing secondary structure!");
	}
	else {
	    log.info("Secondary structure correctly read!");
	}
	SecondaryStructureWriter writer = new SecondaryStructureScriptFormatWriter();
	String resultString = writer.writeString(secStruct);
	System.out.println("Initial structure before optimization: " + NEWLINE + resultString);
	// prettyPrint(System.out, secStruct);
	String[] newSequences;
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setIterMax(10000);
	optimizer.setIter2Max(200);
	optimizer.setErrorScoreLimit(0.5);
	optimizer.setFinalScorer(new RiboswitchSecondaryStructureYESScorer());
	newSequences = optimizer.optimize(secStruct);
	if ((newSequences != null) && (newSequences.length == 2)) {
	    System.out.println("Optimization finished!");
	    // prettyPrint(System.out, newSequences);
	    System.out.println("New sequence(s): ");
	    outputStrings(System.out, newSequences);
	    System.out.println("Concatenated sequences: ");
	    System.out.println(newSequences[0] + "&" + newSequences[1]);
	}
	else {
	    System.out.println("Warning: could not generate optimized sequences in method " + methodName);
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Reads Breaker "YES-logic" sequences, using ? place holders */
    @Test(groups={"new", "slow"})
    public void testBistableYESOptimization1c() {
	String methodName = "testBistableYESOptimization1b";
	System.out.println(TestTools.generateMethodHeader(methodName));
      	String fileName = NANOTILER_HOME + "/test/fixtures/yeslogic_ON_C.sec";
	SecondaryStructure secStruct = null;
	log.info("Importing secondary structure from " + fileName);
	try {
	    SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
	    secStruct = parser.parse(fileName);
	    assert secStruct != null;
	}
	catch (IOException ioe) {
	    log.warning("IO error when scraping result file from: " + fileName);
	    exitError(ioe.getMessage());
	}
	catch (ParseException pe) {
	    log.warning("Parsing error when scraping result file from: " + fileName);
	    exitError(pe.getMessage());
	}
	if (secStruct == null) {
	    exitError("Error parsing secondary structure!");
	}
	else {
	    log.info("Secondary structure correctly read!");
	}
	SecondaryStructureWriter writer = new SecondaryStructureScriptFormatWriter();
	String resultString = writer.writeString(secStruct);
	System.out.println("Initial structure before optimization: " + NEWLINE + resultString);
	// prettyPrint(System.out, secStruct);
	String[] newSequences;
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setIterMax(10000);
	optimizer.setIter2Max(200);
	optimizer.setErrorScoreLimit(0.5);
	optimizer.setFinalScorer(new RiboswitchSecondaryStructureYESScorer());
	newSequences = optimizer.optimize(secStruct);
	if ((newSequences != null) && (newSequences.length == 2)) {
	    System.out.println("Optimization finished!");
	    // prettyPrint(System.out, newSequences);
	    System.out.println("New sequence(s): ");
	    outputStrings(System.out, newSequences);
	    System.out.println("Concatenated sequences: ");
	    System.out.println(newSequences[0] + "&" + newSequences[1]);
	}
	else {
	    System.out.println("Warning: could not generate optimized sequences in method " + methodName);
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Reads Breaker "YES-logic" sequences, using ? place holders */
    @Test(groups={"new", "slow"})
    public void testBistableYESOptimization1cMir17() {
	String methodName = "testBistableYESOptimization1cMir17";
	System.out.println(TestTools.generateMethodHeader(methodName));
      	String fileName = NANOTILER_HOME + "/test/fixtures/yeslogic_ON_C_mir17.sec";
	SecondaryStructure secStruct = null;
	log.info("Importing secondary structure from " + fileName);
	try {
	    SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
	    secStruct = parser.parse(fileName);
	    assert secStruct != null;
	}
	catch (IOException ioe) {
	    log.warning("IO error when scraping result file from: " + fileName);
	    exitError(ioe.getMessage());
	}
	catch (ParseException pe) {
	    log.warning("Parsing error when scraping result file from: " + fileName);
	    exitError(pe.getMessage());
	}
	if (secStruct == null) {
	    exitError("Error parsing secondary structure!");
	}
	else {
	    log.info("Secondary structure correctly read!");
	}
	SecondaryStructureWriter writer = new SecondaryStructureScriptFormatWriter();
	String resultString = writer.writeString(secStruct);
	System.out.println("Initial structure before optimization: " + NEWLINE + resultString);
	// prettyPrint(System.out, secStruct);
	String[] newSequences;
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setIterMax(10000);
	optimizer.setIter2Max(200);
	optimizer.setErrorScoreLimit(2.5);
	optimizer.setFinalScorer(new RiboswitchSecondaryStructureYESScorer());
	newSequences = optimizer.optimize(secStruct);
	if ((newSequences != null) && (newSequences.length == 2)) {
	    System.out.println("Optimization finished!");
	    // prettyPrint(System.out, newSequences);
	    System.out.println("New sequence(s): ");
	    outputStrings(System.out, newSequences);
	    System.out.println("Concatenated sequences: ");
	    System.out.println(newSequences[0] + "&" + newSequences[1]);
	}
	else {
	    System.out.println("Warning: could not generate optimized sequences in method " + methodName);
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Reads Breaker "YES-logic" sequences, using ? place holders */
    @Test(groups={"new", "slow"})
    public void testBistableYESOptimization1cMir17Let7b() {
	String methodName = "testBistableYESOptimization1cMir17Let7b";
	System.out.println(TestTools.generateMethodHeader(methodName));
      	String fileName = NANOTILER_HOME + "/test/fixtures/yeslogic_ON_C_mir17_let7b_B.sec";
	SecondaryStructure secStruct = null;
	log.info("Importing secondary structure from " + fileName);
	try {
	    SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
	    secStruct = parser.parse(fileName);
	    assert secStruct != null;
	}
	catch (IOException ioe) {
	    log.warning("IO error when scraping result file from: " + fileName);
	    exitError(ioe.getMessage());
	}
	catch (ParseException pe) {
	    log.warning("Parsing error when scraping result file from: " + fileName);
	    exitError(pe.getMessage());
	}
	if (secStruct == null) {
	    exitError("Error parsing secondary structure!");
	}
	else {
	    log.info("Secondary structure correctly read!");
	}
	SecondaryStructureWriter writer = new SecondaryStructureScriptFormatWriter();
	String resultString = writer.writeString(secStruct);
	System.out.println("Initial structure before optimization: " + NEWLINE + resultString);
	// prettyPrint(System.out, secStruct);
	String[] newSequences;
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setIterMax(10000);
	optimizer.setIter2Max(200);
	optimizer.setErrorScoreLimit(2.5);
	optimizer.setFinalScorer(new RiboswitchSecondaryStructureYESScorer());
	newSequences = optimizer.optimize(secStruct);
	if ((newSequences != null) && (newSequences.length == 2)) {
	    System.out.println("Optimization finished!");
	    // prettyPrint(System.out, newSequences);
	    System.out.println("New sequence(s): ");
	    outputStrings(System.out, newSequences);
	    System.out.println("Concatenated sequences: ");
	    System.out.println(newSequences[0] + "&" + newSequences[1]);
	}
	else {
	    System.out.println("Warning: could not generate optimized sequences in method " + methodName);
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Reads Breaker "YES-logic" sequences in the off state. */
    @Test(groups={"new", "slow"})
    public void testBistableYESOptimization2() {
	String methodName = "testBistableYESOptimization2";
	System.out.println(TestTools.generateMethodHeader(methodName));
      	String fileName = NANOTILER_HOME + "/test/fixtures/yeslogic_OFF.sec";
	SecondaryStructure secStruct = null;
	log.info("Importing secondary structure from " + fileName);
	try {
	    SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
	    secStruct = parser.parse(fileName);
	    assert secStruct != null;
	}
	catch (IOException ioe) {
	    log.warning("IO error when scraping result file from: " + fileName);
	    exitError(ioe.getMessage());
	}
	catch (ParseException pe) {
	    log.warning("Parsing error when scraping result file from: " + fileName);
	    exitError(pe.getMessage());
	}
	if (secStruct == null) {
	    exitError("Error parsing secondary structure!");
	}
	else {
	    log.info("Secondary structure correctly read!");
	}
	SecondaryStructureWriter writer = new SecondaryStructureScriptFormatWriter();
	String resultString = writer.writeString(secStruct);
	// System.out.println("Initial structure before optimization: " + NEWLINE + resultString);
	// prettyPrint(System.out, secStruct);
	String[] newSequences;
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setIterMax(4000);
	optimizer.setIter2Max(200);
	optimizer.setErrorScoreLimit(2.0);
	optimizer.setFinalScorer(new RiboswitchSecondaryStructureYESScorer());
	newSequences = optimizer.optimize(secStruct);
	if ((newSequences != null) && (newSequences.length == 2)) {
	    System.out.println("Optimization finished!");
	    // prettyPrint(System.out, newSequences);
	    System.out.println("New sequence(s): ");
	    outputStrings(System.out, newSequences);
	    System.out.println("Concatenated sequences: ");
	    System.out.println(newSequences[0] + "&" + newSequences[1]);
	}
	else {
	    System.out.println("Warning: could not generate optimized sequences in method " + methodName);
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


    /** Reads Breaker "YES-logic" sequences, starting from non-sensical sequences  */
    @Test(groups={"new", "slow"})
    public void testBistableYESOptimization3() {
	String methodName = "testBistableYESOptimization3";
	System.out.println(TestTools.generateMethodHeader(methodName));
      	String fileName = NANOTILER_HOME + "/test/fixtures/yeslogic_ON_RAND.sec";
	SecondaryStructure secStruct = null;
	log.info("Importing secondary structure from " + fileName);
	try {
	    SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
	    secStruct = parser.parse(fileName);
	    assert secStruct != null;
	}
	catch (IOException ioe) {
	    log.warning("IO error when scraping result file from: " + fileName);
	    exitError(ioe.getMessage());
	}
	catch (ParseException pe) {
	    log.warning("Parsing error when scraping result file from: " + fileName);
	    exitError(pe.getMessage());
	}
	if (secStruct == null) {
	    exitError("Error parsing secondary structure!");
	}
	else {
	    log.info("Secondary structure correctly read!");
	}
	SecondaryStructureWriter writer = new SecondaryStructureScriptFormatWriter();
	String resultString = writer.writeString(secStruct);
	// System.out.println("Initial structure before optimization: " + NEWLINE + resultString);
	// prettyPrint(System.out, secStruct);
	String[] newSequences;
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setIterMax(4000);
	optimizer.setIter2Max(200);
	optimizer.setErrorScoreLimit(2.0);
	optimizer.setFinalScorer(new RiboswitchSecondaryStructureYESScorer());
	newSequences = optimizer.optimize(secStruct);
	if ((newSequences != null) && (newSequences.length == 2)) {
	    System.out.println("Optimization finished!");
	    // prettyPrint(System.out, newSequences);
	    System.out.println("New sequence(s): ");
	    outputStrings(System.out, newSequences);
	    System.out.println("Concatenated sequences: ");
	    System.out.println(newSequences[0] + "&" + newSequences[1]);
	}
	else {
	    System.out.println("Warning: could not generate optimized sequences in method " + methodName);
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Reads Breaker "YES-logic" sequences, starting from non-sensical sequences not using any info from Breaker paper.  */
    @Test(groups={"new", "slow"})
    public void testBistableYESOptimization4() {
	String methodName = "testBistableYESOptimization4";
	System.out.println(TestTools.generateMethodHeader(methodName));
      	String fileName = NANOTILER_HOME + "/test/fixtures/yeslogic_ON_RAND2.sec";
	SecondaryStructure secStruct = null;
	log.info("Importing secondary structure from " + fileName);
	try {
	    SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
	    secStruct = parser.parse(fileName);
	    assert secStruct != null;
	}
	catch (IOException ioe) {
	    log.warning("IO error when scraping result file from: " + fileName);
	    exitError(ioe.getMessage());
	}
	catch (ParseException pe) {
	    log.warning("Parsing error when scraping result file from: " + fileName);
	    exitError(pe.getMessage());
	}
	if (secStruct == null) {
	    exitError("Error parsing secondary structure!");
	}
	else {
	    log.info("Secondary structure correctly read!");
	}
	SecondaryStructureWriter writer = new SecondaryStructureScriptFormatWriter();
	String resultString = writer.writeString(secStruct);
	// System.out.println("Initial structure before optimization: " + NEWLINE + resultString);
	// prettyPrint(System.out, secStruct);
	String[] newSequences;
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setIterMax(40000);
	optimizer.setIter2Max(200);
	optimizer.setErrorScoreLimit(25.0);
	optimizer.setFinalScorer(new RiboswitchSecondaryStructureYESScorer());
	newSequences = optimizer.optimize(secStruct);
	if ((newSequences != null) && (newSequences.length == 2)) {
	    System.out.println("Optimization finished!");
	    // prettyPrint(System.out, newSequences);
	    System.out.println("New sequence(s): ");
	    outputStrings(System.out, newSequences);
	    System.out.println("Concatenated sequences: ");
	    System.out.println(newSequences[0] + "&" + newSequences[1]);
	}
	else {
	    System.out.println("Warning: could not generate optimized sequences in method " + methodName);
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Reads Breaker "YES-logic" sequences, starting from non-sensical sequences not using any info from Breaker paper.  */
    @Test(groups={"new", "slow"})
    public void testBistableYESOptimization5() {
	String methodName = "testBistableYESOptimization5";
	System.out.println(TestTools.generateMethodHeader(methodName));
      	String fileName = NANOTILER_HOME + "/test/fixtures/yeslogic_ON_RAND3_miR17-miR15a.sec";
	SecondaryStructure secStruct = null;
	log.info("Importing secondary structure from " + fileName);
	try {
	    SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
	    secStruct = parser.parse(fileName);
	    assert secStruct != null;
	}
	catch (IOException ioe) {
	    log.warning("IO error when scraping result file from: " + fileName);
	    exitError(ioe.getMessage());
	}
	catch (ParseException pe) {
	    log.warning("Parsing error when scraping result file from: " + fileName);
	    exitError(pe.getMessage());
	}
	if (secStruct == null) {
	    exitError("Error parsing secondary structure!");
	}
	else {
	    log.info("Secondary structure correctly read!");
	}
	SecondaryStructureWriter writer = new SecondaryStructureScriptFormatWriter();
	String resultString = writer.writeString(secStruct);
	// System.out.println("Initial structure before optimization: " + NEWLINE + resultString);
	// prettyPrint(System.out, secStruct);
	String[] newSequences;
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setIterMax(40000);
	optimizer.setIter2Max(200);
	optimizer.setErrorScoreLimit(25.0);
	optimizer.setFinalScorer(new RiboswitchSecondaryStructureYESScorer());
	newSequences = optimizer.optimize(secStruct);
	if ((newSequences != null) && (newSequences.length == 2)) {
	    System.out.println("Optimization finished!");
	    // prettyPrint(System.out, newSequences);
	    System.out.println("New sequence(s): ");
	    outputStrings(System.out, newSequences);
	    System.out.println("Concatenated sequences: ");
	    System.out.println(newSequences[0] + "&" + newSequences[1]);
	}
	else {
	    System.out.println("Warning: could not generate optimized sequences in method " + methodName);
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Reads Breaker "YES-logic" sequences, starting from non-sensical sequences not using any info from Breaker paper.  */
    @Test(groups={"new", "slow"})
    public void testBistableYESOptimization6() {
	String methodName = "testBistableYESOptimization6";
	System.out.println(TestTools.generateMethodHeader(methodName));
      	String fileName = NANOTILER_HOME + "/test/fixtures/yeslogic_ON_RAND4_miR17-let7b.sec";
	SecondaryStructure secStruct = null;
	log.info("Importing secondary structure from " + fileName);
	try {
	    SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
	    secStruct = parser.parse(fileName);
	    assert secStruct != null;
	}
	catch (IOException ioe) {
	    log.warning("IO error when scraping result file from: " + fileName);
	    exitError(ioe.getMessage());
	}
	catch (ParseException pe) {
	    log.warning("Parsing error when scraping result file from: " + fileName);
	    exitError(pe.getMessage());
	}
	if (secStruct == null) {
	    exitError("Error parsing secondary structure!");
	}
	else {
	    log.info("Secondary structure correctly read!");
	}
	SecondaryStructureWriter writer = new SecondaryStructureScriptFormatWriter();
	String resultString = writer.writeString(secStruct);
	// System.out.println("Initial structure before optimization: " + NEWLINE + resultString);
	// prettyPrint(System.out, secStruct);
	String[] newSequences;
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setIterMax(40000);
	optimizer.setIter2Max(200);
	optimizer.setErrorScoreLimit(25.0);
	optimizer.setFinalScorer(new RiboswitchSecondaryStructureYESScorer());
	newSequences = optimizer.optimize(secStruct);
	if ((newSequences != null) && (newSequences.length == 2)) {
	    System.out.println("Optimization finished!");
	    // prettyPrint(System.out, newSequences);
	    System.out.println("New sequence(s): ");
	    outputStrings(System.out, newSequences);
	    System.out.println("Concatenated sequences: ");
	    System.out.println(newSequences[0] + "&" + newSequences[1]);
	}
	else {
	    System.out.println("Warning: could not generate optimized sequences in method " + methodName);
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Reads Breaker "YES-logic" sequences, starting from non-sensical sequences not using any info from Breaker paper.  */
    @Test(groups={"new", "slow"})
    public void testBistableYESOptimization7() {
	String methodName = "testBistableYESOptimization7";
	System.out.println(TestTools.generateMethodHeader(methodName));
      	String fileName = NANOTILER_HOME + "/test/fixtures/yeslogic_ON_RAND3_miR17-miR16-1_B.sec";
	SecondaryStructure secStruct = null;
	log.info("Importing secondary structure from " + fileName);
	try {
	    SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
	    secStruct = parser.parse(fileName);
	    assert secStruct != null;
	}
	catch (IOException ioe) {
	    log.warning("IO error when scraping result file from: " + fileName);
	    exitError(ioe.getMessage());
	}
	catch (ParseException pe) {
	    log.warning("Parsing error when scraping result file from: " + fileName);
	    exitError(pe.getMessage());
	}
	if (secStruct == null) {
	    exitError("Error parsing secondary structure!");
	}
	else {
	    log.info("Secondary structure correctly read!");
	}
	SecondaryStructureWriter writer = new SecondaryStructureScriptFormatWriter();
	String resultString = writer.writeString(secStruct);
	// System.out.println("Initial structure before optimization: " + NEWLINE + resultString);
	// prettyPrint(System.out, secStruct);
	String[] newSequences;
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setIterMax(40000);
	optimizer.setIter2Max(200);
	optimizer.setErrorScoreLimit(28.0);
	optimizer.setFinalScorer(new RiboswitchSecondaryStructureYESScorer());
	newSequences = optimizer.optimize(secStruct);
	if ((newSequences != null) && (newSequences.length == 2)) {
	    System.out.println("Optimization finished!");
	    // prettyPrint(System.out, newSequences);
	    System.out.println("New sequence(s): ");
	    outputStrings(System.out, newSequences);
	    System.out.println("Concatenated sequences: ");
	    System.out.println(newSequences[0] + "&" + newSequences[1]);
	}
	else {
	    System.out.println("Warning: could not generate optimized sequences in method " + methodName);
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Reads Breaker "YES-logic" sequences, starting from non-sensical sequences not using any info from Breaker paper.  */
    @Test(groups={"new", "slow"})
    public void testBistableYESOptimization8() {
	String methodName = "testBistableYESOptimization8";
	System.out.println(TestTools.generateMethodHeader(methodName));
      	String fileName = NANOTILER_HOME + "/test/fixtures/yeslogic_ON_RAND3_miR17-miR16-1_C.sec";
	SecondaryStructure secStruct = null;
	log.info("Importing secondary structure from " + fileName);
	try {
	    SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
	    secStruct = parser.parse(fileName);
	    assert secStruct != null;
	}
	catch (IOException ioe) {
	    log.warning("IO error when scraping result file from: " + fileName);
	    exitError(ioe.getMessage());
	}
	catch (ParseException pe) {
	    log.warning("Parsing error when scraping result file from: " + fileName);
	    exitError(pe.getMessage());
	}
	if (secStruct == null) {
	    exitError("Error parsing secondary structure!");
	}
	else {
	    log.info("Secondary structure correctly read!");
	}
	SecondaryStructureWriter writer = new SecondaryStructureScriptFormatWriter();
	String resultString = writer.writeString(secStruct);
	// System.out.println("Initial structure before optimization: " + NEWLINE + resultString);
	// prettyPrint(System.out, secStruct);
	String[] newSequences;
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setIterMax(40000);
	optimizer.setIter2Max(200);
	optimizer.setErrorScoreLimit(28.0);
	optimizer.setFinalScorer(new RiboswitchSecondaryStructureYESScorer());
	newSequences = optimizer.optimize(secStruct);
	if ((newSequences != null) && (newSequences.length == 2)) {
	    System.out.println("Optimization finished!");
	    // prettyPrint(System.out, newSequences);
	    System.out.println("New sequence(s): ");
	    outputStrings(System.out, newSequences);
	    System.out.println("Concatenated sequences: ");
	    System.out.println(newSequences[0] + "&" + newSequences[1]);
	}
	else {
	    System.out.println("Warning: could not generate optimized sequences in method " + methodName);
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Reads and optimizes "XOR-logic" sequences (derived from Breakers NOT-1) */
    @Test(groups={"new", "slow"})
    public void testBistableYESOptimization9() {
	String methodName = "testBistableYESOptimization9";
	System.out.println(TestTools.generateMethodHeader(methodName));
      	String fileName = NANOTILER_HOME + "/test/fixtures/xorlogic_B.sec";
	SecondaryStructure secStruct = null;
	log.info("Importing secondary structure from " + fileName);
	try {
	    SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
	    secStruct = parser.parse(fileName);
	    assert secStruct != null;
	}
	catch (IOException ioe) {
	    log.warning("IO error when scraping result file from: " + fileName);
	    exitError(ioe.getMessage());
	}
	catch (ParseException pe) {
	    log.warning("Parsing error when scraping result file from: " + fileName);
	    exitError(pe.getMessage());
	}
	if (secStruct == null) {
	    exitError("Error parsing secondary structure!");
	}
	else {
	    log.info("Secondary structure correctly read!");
	}
	SecondaryStructureWriter writer = new SecondaryStructureScriptFormatWriter();
	String resultString = writer.writeString(secStruct);
	// System.out.println("Initial structure before optimization: " + NEWLINE + resultString);
	// prettyPrint(System.out, secStruct);
	String[] newSequences;
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setIterMax(40000);
	optimizer.setIter2Max(200);
	optimizer.setErrorScoreLimit(2.0);
	optimizer.setFinalScorer(new RiboswitchSecondaryStructureYESScorer());
	newSequences = optimizer.optimize(secStruct);
	if ((newSequences != null) && (newSequences.length == 2)) {
	    System.out.println("Optimization finished!");
	    // prettyPrint(System.out, newSequences);
	    System.out.println("New sequence(s): ");
	    outputStrings(System.out, newSequences);
	    System.out.println("Concatenated sequences: ");
	    System.out.println(newSequences[0] + "&" + newSequences[1]);
	}
	else {
	    System.out.println("Warning: could not generate optimized sequences in method " + methodName);
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Reads and optimizes "XOR-logic" sequences (derived from Breakers NOT-1) */
    @Test(groups={"new", "outdated", "slow"})
    public void testBistableYESOptimization10() {
	String methodName = "testBistableYESOptimization10";
	System.out.println(TestTools.generateMethodHeader(methodName));
      	String fileName = NANOTILER_HOME + "/test/fixtures/notlogic_B.sec";
	SecondaryStructure secStruct = null;
	log.info("Importing secondary structure from " + fileName);
	try {
	    SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
	    secStruct = parser.parse(fileName);
	    assert secStruct != null;
	}
	catch (IOException ioe) {
	    log.warning("IO error when scraping result file from: " + fileName);
	    exitError(ioe.getMessage());
	}
	catch (ParseException pe) {
	    log.warning("Parsing error when scraping result file from: " + fileName);
	    exitError(pe.getMessage());
	}
	if (secStruct == null) {
	    exitError("Error parsing secondary structure!");
	}
	else {
	    log.info("Secondary structure correctly read!");
	}
	SecondaryStructureWriter writer = new SecondaryStructureScriptFormatWriter();
	String resultString = writer.writeString(secStruct);
	// System.out.println("Initial structure before optimization: " + NEWLINE + resultString);
	// prettyPrint(System.out, secStruct);
	String[] newSequences;
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setIterMax(40000);
	optimizer.setIter2Max(200);
	optimizer.setErrorScoreLimit(2.0);
	optimizer.setFinalScorer(new RiboswitchSecondaryStructureYESScorer());
	newSequences = optimizer.optimize(secStruct);
	if ((newSequences != null) && (newSequences.length == 2)) {
	    // System.out.println("Optimization finished!");
	    // prettyPrint(System.out, newSequences);
	    System.out.println("New sequence(s): ");
	    outputStrings(System.out, newSequences);
	    System.out.println("Concatenated sequences: ");
	    System.out.println(newSequences[0] + "&" + newSequences[1]);
	}
	else {
	    System.out.println("Warning: could not generate optimized sequences in method " + methodName);
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Reads Breaker "YES-logic" sequences, starting from non-sensical sequences not using any info from Breaker paper.  */
    @Test(groups={"new", "slow"})
    public void testBistableYESOptimization11() {
	String methodName = "testBistableYESOptimization11";
	System.out.println(TestTools.generateMethodHeader(methodName));
      	String fileName = NANOTILER_HOME + "/test/fixtures/yeslogic_ON_RAND3_miR17-miR16-1_C.sec";
	SecondaryStructure secStruct = null;
	log.info("Importing secondary structure from " + fileName);
	try {
	    SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
	    secStruct = parser.parse(fileName);
	    assert secStruct != null;
	}
	catch (IOException ioe) {
	    log.warning("IO error when scraping result file from: " + fileName);
	    exitError(ioe.getMessage());
	}
	catch (ParseException pe) {
	    log.warning("Parsing error when scraping result file from: " + fileName);
	    exitError(pe.getMessage());
	}
	if (secStruct == null) {
	    exitError("Error parsing secondary structure!");
	}
	else {
	    log.info("Secondary structure correctly read!");
	}
	SecondaryStructureWriter writer = new SecondaryStructureScriptFormatWriter();
	String resultString = writer.writeString(secStruct);
	// System.out.println("Initial structure before optimization: " + NEWLINE + resultString);
	// prettyPrint(System.out, secStruct);
	String[] newSequences;
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setIterMax(40000);
	optimizer.setIter2Max(200);
	optimizer.setErrorScoreLimit(28.0);
	optimizer.setFinalScorer(new RiboswitchSecondaryStructureYESScorer());
	newSequences = optimizer.optimize(secStruct);
	if ((newSequences != null) && (newSequences.length == 2)) {
	    System.out.println("Optimization finished!");
	    // prettyPrint(System.out, newSequences);
	    System.out.println("New sequence(s): ");
	    outputStrings(System.out, newSequences);
	    System.out.println("Concatenated sequences: ");
	    System.out.println(newSequences[0] + "&" + newSequences[1]);
	}
	else {
	    System.out.println("Warning: could not generate optimized sequences in method " + methodName);
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Reads Breaker "YES-logic" sequences. Probability mode currently not supported. */
    @Test(groups={"new", "outdated"})
    public void testBistableYESProbOptimization1() {
	String methodName = "testBistableYESProbOptimization1";
	System.out.println(TestTools.generateMethodHeader(methodName));
      	String fileName = NANOTILER_HOME + "/test/fixtures/yeslogic_ON.sec";
	SecondaryStructure secStruct = null;
	log.info("Importing secondary structure from " + fileName);
	try {
	    SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
	    secStruct = parser.parse(fileName);
	    assert secStruct != null;
	}
	catch (IOException ioe) {
	    log.warning("IO error when scraping result file from: " + fileName);
	    exitError(ioe.getMessage());
	}
	catch (ParseException pe) {
	    log.warning("Parsing error when scraping result file from: " + fileName);
	    exitError(pe.getMessage());
	}
	if (secStruct == null) {
	    exitError("Error parsing secondary structure!");
	}
	else {
	    log.info("Secondary structure correctly read!");
	}
	SecondaryStructureWriter writer = new SecondaryStructureScriptFormatWriter();
	String resultString = writer.writeString(secStruct);
	// System.out.println("Initial structure before optimization: " + NEWLINE + resultString);
	// prettyPrint(System.out, secStruct);
	String[] newSequences;
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setIterMax(4000);
	optimizer.setIter2Max(200);
	optimizer.setErrorScoreLimit(2.0);
	RiboswitchSecondaryStructureYESScorer scorer = new RiboswitchSecondaryStructureYESScorer();
	scorer.setProbabilityMode(true); // run in "probability" mode
	optimizer.setFinalScorer(scorer);
	newSequences = optimizer.optimize(secStruct);
	if ((newSequences != null) && (newSequences.length == 2)) {
	    System.out.println("Optimization finished!");
	    // prettyPrint(System.out, newSequences);
	    System.out.println("New sequence(s): ");
	    outputStrings(System.out, newSequences);
	    System.out.println("Concatenated sequences: ");
	    System.out.println(newSequences[0] + "&" + newSequences[1]);
	}
	else {
	    System.out.println("Warning: could not generate optimized sequences in method " + methodName);
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Reads Breaker "YES-logic" sequences, starting from non-sensical sequences not using any info from Breaker paper.
     * Similar to testBistableOptimization11
     * */
    @Test(groups={"new", "outdated"})
    public void testBistableYESProbOptimization2() {
	String methodName = "testBistableYESProbOptimization2";
	System.out.println(TestTools.generateMethodHeader(methodName));
      	String fileName = NANOTILER_HOME + "/test/fixtures/yeslogic_ON_RAND3_miR17-miR16-1_C.sec";
	SecondaryStructure secStruct = null;
	log.info("Importing secondary structure from " + fileName);
	try {
	    SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
	    secStruct = parser.parse(fileName);
	    assert secStruct != null;
	}
	catch (IOException ioe) {
	    log.warning("IO error when scraping result file from: " + fileName);
	    exitError(ioe.getMessage());
	}
	catch (ParseException pe) {
	    log.warning("Parsing error when scraping result file from: " + fileName);
	    exitError(pe.getMessage());
	}
	if (secStruct == null) {
	    exitError("Error parsing secondary structure!");
	}
	else {
	    log.info("Secondary structure correctly read!");
	}
	SecondaryStructureWriter writer = new SecondaryStructureScriptFormatWriter();
	String resultString = writer.writeString(secStruct);
	// System.out.println("Initial structure before optimization: " + NEWLINE + resultString);
	// prettyPrint(System.out, secStruct);
	String[] newSequences;
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setIterMax(40000);
	optimizer.setIter2Max(200);
	optimizer.setErrorScoreLimit(2.0);
	RiboswitchSecondaryStructureYESScorer scorer = new RiboswitchSecondaryStructureYESScorer();
	scorer.setProbabilityMode(true);
	optimizer.setFinalScorer(scorer);
	newSequences = optimizer.optimize(secStruct);
	if ((newSequences != null) && (newSequences.length == 2)) {
	    System.out.println("Optimization finished!");
	    // prettyPrint(System.out, newSequences);
	    System.out.println("New sequence(s): ");
	    outputStrings(System.out, newSequences);
	    System.out.println("Concatenated sequences: ");
	    System.out.println(newSequences[0] + "&" + newSequences[1]);
	}
	else {
	    System.out.println("Warning: could not generate optimized sequences in method " + methodName);
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


}
