package nanotilertests.secondarystructuredesign;

import java.io.*;
import java.text.*;
import java.util.Date;
import generaltools.*;
import rnasecondary.*;
import secondarystructuredesign.*;

import static secondarystructuredesign.PackageConstants.*;

import org.testng.annotations.*;

/** Tests MonteCarloSequenceOptimizer */
public class RnacofoldSecondaryStructureScorerTest {

    private static String fixturesDir = NANOTILER_HOME + "/test/fixtures";

    private SecondaryStructure generateCubeSecondaryStructure(String name) {
	SecondaryStructure cubeStructure = null;
	SecondaryStructureParser parser = new ImprovedSecondaryStructureParser();
	String inputFileName = fixturesDir + "/" + name;
	String[] inputLines = null;
	try {
	    cubeStructure = parser.parse(inputFileName);
	}
	catch (IOException ioe) {
	    System.out.println("IOException while reading " + inputFileName + " : " + ioe.getMessage());
	    assert false;
	}
	catch (ParseException pe) {
	    System.out.println("ParseException parsing " + inputFileName + " : " + pe.getMessage());
	    assert false;	    
	}	
	assert cubeStructure != null;
	return cubeStructure;
    }

    @Test(groups={"newest_later", "slow"})
    public void testTiny() {
	String methodName = "testTiny";
	System.out.println(TestTools.generateMethodHeader(methodName));
	// MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	// optimizer.setErrorScoreLimit(30);
	// optimizer.setIterMax(10000);
	// optimizer.setIter2Max(20);
	RnacofoldSecondaryStructureScorer scorer = new RnacofoldSecondaryStructureScorer();
	SecondaryStructure cubeStructure = generateCubeSecondaryStructure("tiny.sec");
	assert cubeStructure != null;
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure with " + cubeStructure.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(cubeStructure));
	assert cubeStructure.getSequenceCount() == 2; // must be this many sequences
	System.out.println("Starting to evaluate cube sequences:");
	StringBuffer[] bseqs = new StringBuffer[cubeStructure.getSequenceCount()];
	for (int i = 0; i < cubeStructure.getSequenceCount(); ++i) {
	    bseqs[i] = new StringBuffer(cubeStructure.getSequence(i).sequenceString());
	}
	int[][][][] interactionMatrices = cubeStructure.generateInteractionMatrices();
	double result = scorer.scoreStructure(bseqs,cubeStructure, interactionMatrices);
	System.out.println("Finished evaluating sequences: " + result);
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"newest_later", "slow"})
    public void testCube() {
	String methodName = "testCube";
	System.out.println(TestTools.generateMethodHeader(methodName));
	RnacofoldSecondaryStructureScorer scorer = new RnacofoldSecondaryStructureScorer();
	SecondaryStructure cubeStructure = generateCubeSecondaryStructure("cube_uneven.sec");
	assert cubeStructure != null;
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure with " + cubeStructure.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(cubeStructure));
	assert cubeStructure.getSequenceCount() == 6; // must be this many sequences
	StringBuffer[] bseqs = new StringBuffer[cubeStructure.getSequenceCount()];
	for (int i = 0; i < cubeStructure.getSequenceCount(); ++i) {
	    bseqs[i] = new StringBuffer(cubeStructure.getSequence(i).sequenceString());
	}
	int[][][][] interactionMatrices = cubeStructure.generateInteractionMatrices();
	System.out.println("Starting to evaluate cube sequences:");
	Date timeInD = new Date();
	double result = scorer.scoreStructure(bseqs,cubeStructure, interactionMatrices);
	Date timeOutD = new Date();
	long msec = timeOutD.getTime() - timeInD.getTime();
	System.out.println("Finished evaluating sequences: " + result + " execution time: " + (msec/1000.0));
        assert(Math.abs(606.573381-result) < 0.01);

	// second evaluation should give same result!
	timeInD = new Date();
	double result2 = scorer.scoreStructure(bseqs,cubeStructure, interactionMatrices);
	timeOutD = new Date();
	long msec2 = timeOutD.getTime() - timeInD.getTime();
	System.out.println("Finished evaluating sequences (2nd round): " + result + " execution time: " + (msec2/1000.0));
        assert(Math.abs(result2-result) < 0.01);

	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new", "slow"})
    public void testOptimizeCubeConstraints() {
	String methodName = "testOptimizeCubeConstraints";
	System.out.println(TestTools.generateMethodHeader(methodName));
	MonteCarloSequenceOptimizerVersionsFactory factory 
	    = new MonteCarloSequenceOptimizerVersionsFactory(MonteCarloSequenceOptimizerVersionsFactory.VERSION_OCTOBER_2008);
	MonteCarloSequenceOptimizer optimizer = (MonteCarloSequenceOptimizer)(factory.generate());
	optimizer.setIterMax(10000);
	optimizer.setIter2Max(100);
	SecondaryStructure cubeStructure = generateCubeSecondaryStructure("cube6_fifthTry_constraints.sec");
	assert cubeStructure != null;
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure with " + cubeStructure.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(cubeStructure));
	assert cubeStructure.getSequenceCount() == 6; // must be this many sequences
	System.out.println("Starting to optimize cube sequences:");
	String[] result = optimizer.optimize(cubeStructure);
	System.out.println("Finished optimizing cube sequences:");
	assert result != null;
	for (int i = 0; i < result.length; ++i) {
	    System.out.println(result[i]);
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new", "slow"})
    public void testOptimizeCubeCriton() {
	String methodName = "testOptimizeCubeCriton";
	System.out.println(TestTools.generateMethodHeader(methodName));
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setErrorScoreLimit(30);
	optimizer.setIterMax(10000);
	optimizer.setIter2Max(20000);
	optimizer.setKt(1.0);
	optimizer.setKt2(0.4);
	DummyEnergySecondaryStructureScorer scorer2 = new DummyEnergySecondaryStructureScorer();
	scorer2.setZeroMode(true);
	optimizer.setFinalScorer(scorer2);
	SecondaryStructure cubeStructure = generateCubeSecondaryStructure("cube_uneven.sec");
	assert cubeStructure != null;
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure with " + cubeStructure.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(cubeStructure));
	assert cubeStructure.getSequenceCount() == 6; // must be this many sequences
	System.out.println("Starting to optimize cube sequences:");
	String[] result = optimizer.optimize(cubeStructure);
	System.out.println("Finished optimizing cube sequences:");
	assert result != null;
	for (int i = 0; i < result.length; ++i) {
	    System.out.println(result[i]);
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new", "slow"})
    public void testOptimizeCubeCritonLoop2() {
	String methodName = "testOptimizeCubeCritonLoop2";
	System.out.println(TestTools.generateMethodHeader(methodName));
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setErrorScoreLimit(23.0);
	optimizer.setIterMax(50000);
	optimizer.setIter2Max(40000);
	optimizer.setKt(1.0);
	optimizer.setKt2(0.4);
	optimizer.setRerun(5);
	// DummyEnergySecondaryStructureScorer scorer2 = new DummyEnergySecondaryStructureScorer();
	// scorer2.setZeroMode(true);
	// optimizer.setFinalScorer(scorer2);
	SecondaryStructure cubeStructure = generateCubeSecondaryStructure("cube_even2.sec");
	assert cubeStructure != null;
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure with " + cubeStructure.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(cubeStructure));
	assert cubeStructure.getSequenceCount() == 6; // must be this many sequences
	System.out.println("Starting to optimize cube sequences:");
	String[] result = optimizer.optimize(cubeStructure);
	System.out.println("Finished optimizing cube sequences:");
	assert result != null;
	for (int i = 0; i < result.length; ++i) {
	    System.out.println(result[i]);
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    @Test(groups={"new", "slow"})
    public void testOptimizeCubeCritonLoop2Simple() {
	String methodName = "testOptimizeCubeCritonLoop2Simple";
	System.out.println(TestTools.generateMethodHeader(methodName));
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setErrorScoreLimit(45.0);
	optimizer.setIterMax(20000);
	optimizer.setIter2Max(10000);
	optimizer.setKt(1.0);
	optimizer.setKt2(0.4);
	optimizer.setRerun(5);
	SecondaryStructureScorer scorer2 = new SimpleSecondaryStructurePredictorScorer();
	optimizer.setFinalScorer(scorer2);
	SecondaryStructure cubeStructure = generateCubeSecondaryStructure("cube_even2.sec");
	assert cubeStructure != null;
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure with " + cubeStructure.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(cubeStructure));
	assert cubeStructure.getSequenceCount() == 6; // must be this many sequences
	System.out.println("Starting to optimize cube sequences:");
	String[] result = optimizer.optimize(cubeStructure);
	System.out.println("Finished optimizing cube sequences:");
	assert result != null;
	for (int i = 0; i < result.length; ++i) {
	    System.out.println(result[i]);
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Production test case, using for the first time sequences that have minimum helix lengths 4 */
    @Test(groups={"new", "production", "slow"})
    public void testOptimizeCubeCritonLoop24HSimple() {
	String methodName = "testOptimizeCubeCritonLoop2H4Simple";
	System.out.println(TestTools.generateMethodHeader(methodName));
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setErrorScoreLimit(45.0);
	optimizer.setIterMax(20000);
	optimizer.setIter2Max(40000);
	optimizer.setKt(1.0);
	optimizer.setKt2(0.4);
	optimizer.setRerun(5);
	optimizer.setFinalScoreWeight(0.1); // higher than default weight of 0.1
	RnacofoldSecondaryStructureScorer scorer2 = new RnacofoldSecondaryStructureScorer();
	scorer2.setInterStrandMode(false); // compute only intra-strand energies
	scorer2.setSelfEnergyCutoff(-8.0); // only penalize strands if very stable energies
	SecondaryStructureScorer scorer3 = new SimpleSecondaryStructurePredictorScorer();
	scorer2.setSubScorer(scorer3);
	scorer2.setSubScorerWeight(5.0);
	optimizer.setFinalScorer(scorer2);
	SecondaryStructure cubeStructure = generateCubeSecondaryStructure("cube_even4.sec");
	assert cubeStructure != null;
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure with " + cubeStructure.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(cubeStructure));
	assert cubeStructure.getSequenceCount() == 6; // must be this many sequences
	System.out.println("Starting to optimize cube sequences:");
	String[] result = optimizer.optimize(cubeStructure);
	System.out.println("Finished optimizing cube sequences:");
	assert result != null;
	for (int i = 0; i < result.length; ++i) {
	    System.out.println(result[i]);
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Production test case, using for the first time sequences that have minimum helix lengths 4. Using as loops 2 Us */
    @Test(groups={"new", "production", "slow"})
    public void testOptimizeCubeCritonLoop24HOpt42U() {
	String methodName = "testOptimizeCubeCritonLoop2H4Opt42U";
	System.out.println(TestTools.generateMethodHeader(methodName));
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setErrorScoreLimit(45.0);
	optimizer.setIterMax(20000);
	optimizer.setIter2Max(40000);
	optimizer.setKt(1.0);
	optimizer.setKt2(0.4);
	optimizer.setRerun(5);
	optimizer.setRandomizeFlag(false); // no initial sequence randomization
	optimizer.setFinalScoreWeight(0.05); // higher than default weight of 0.1
	RnacofoldSecondaryStructureScorer scorer2 = new RnacofoldSecondaryStructureScorer();
	// scorer2.setInterStrandMode(false); // deactivate compute only intra-strand energies
	scorer2.setSelfEnergyCutoff(-10.0); // only penalize strands if very stable energies
	SecondaryStructureScorer scorer3 = new SimpleSecondaryStructurePredictorScorer();
	scorer2.setSubScorer(scorer3);
	scorer2.setSubScorerWeight(5.0);
	optimizer.setFinalScorer(scorer2);
	SecondaryStructure cubeStructure = generateCubeSecondaryStructure("cube_even4_opt42U.sec");
	assert cubeStructure != null;
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure with " + cubeStructure.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(cubeStructure));
	assert cubeStructure.getSequenceCount() == 6; // must be this many sequences
	System.out.println("Starting to optimize cube sequences:");
	String[] result = optimizer.optimize(cubeStructure);
	System.out.println("Finished optimizing cube sequences:");
	assert result != null;
	for (int i = 0; i < result.length; ++i) {
	    System.out.println(result[i]);
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Production test case, using for the first time sequences that have minimum helix lengths 4. Using as loops 2 Us */
    @Test(groups={"new", "production", "slow"})
    public void testOptimizeCubeCritonLoop24HOpt42UunevenNoWave() {
	String methodName = "testOptimizeCubeCritonLoop2H4Opt42UunevenNoWave";
	System.out.println(TestTools.generateMethodHeader(methodName));
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setErrorScoreLimit(15.0);
	optimizer.setIterMax(40000);
	optimizer.setIter2Max(20000);
	optimizer.setKt(1.0);
	optimizer.setKt2(0.4);
	optimizer.setRerun(5);
	optimizer.setRandomizeFlag(false); // no initial sequence randomization
	optimizer.setFinalScoreWeight(0.05); // higher than default weight of 0.1
	RnacofoldSecondaryStructureScorer scorer2 = new RnacofoldSecondaryStructureScorer();
	// scorer2.setInterStrandMode(false); // deactivate compute only intra-strand energies
	scorer2.setSelfEnergyCutoff(-10.0); // only penalize strands if very stable energies
	SecondaryStructureScorer scorer3 = new SimpleSecondaryStructurePredictorScorer();
	scorer2.setSubScorer(scorer3);
	scorer2.setSubScorerWeight(5.0);
	optimizer.setFinalScorer(scorer2);
	SecondaryStructure cubeStructure = generateCubeSecondaryStructure("cube_uneven4_opt42U_nowave.sec");
	assert cubeStructure != null;
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure with " + cubeStructure.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(cubeStructure));
	assert cubeStructure.getSequenceCount() == 6; // must be this many sequences
	System.out.println("Starting to optimize cube sequences:");
	String[] result = optimizer.optimize(cubeStructure);
	System.out.println("Finished optimizing cube sequences:");
	assert result != null;
	for (int i = 0; i < result.length; ++i) {
	    System.out.println(result[i]);
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Production test case, using for the first time sequences that have minimum helix lengths 4. Using as loops 2 Us.
     * No use of RNAcofold scoring.
     */
    @Test(groups={"new", "production", "slow"})
    public void testOptimizeCubeCritonLoop24HOpt42UunevenNoWave2() {
	String methodName = "testOptimizeCubeCritonLoop2H4Opt42UunevenNoWave2";
	System.out.println(TestTools.generateMethodHeader(methodName));
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setErrorScoreLimit(51.0);
	optimizer.setIterMax(40000);
	optimizer.setIter2Max(20000);
	optimizer.setKt(1.0);
	optimizer.setKt2(0.4);
	optimizer.setRerun(5);
	optimizer.setRandomizeFlag(true); // no initial sequence randomization
	optimizer.setFinalScoreWeight(0.05); // higher than default weight of 0.1
	DummyEnergySecondaryStructureScorer scorer2 = new DummyEnergySecondaryStructureScorer();
	scorer2.setZeroMode(true);
	optimizer.setFinalScorer(scorer2);
	SecondaryStructure cubeStructure = generateCubeSecondaryStructure("cube_uneven4_opt42U_nowave_b.sec");
	assert cubeStructure != null;
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure with " + cubeStructure.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(cubeStructure));
	assert cubeStructure.getSequenceCount() == 6; // must be this many sequences
	System.out.println("Starting to optimize cube sequences:");
	String[] result = optimizer.optimize(cubeStructure);
	System.out.println("Finished optimizing cube sequences:");
	assert result != null;
	for (int i = 0; i < result.length; ++i) {
	    System.out.println(result[i]);
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Production test case, using for the first time sequences that have minimum helix lengths 4. Using as loops 2 Us.
     * No use of RNAcofold scoring.
     * New modified rule not allowing more than on G/C in a helix
     * 2nt length differences between sequences
     */
    @Test(groups={"new", "production", "slow"})
    public void testOptimizeCubeCritonLoop24HOpt42UunevenNoWave3() {
	String methodName = "testOptimizeCubeCritonLoop2H4Opt42UunevenNoWave2"; // bad name, fix it in next submit
	System.out.println(TestTools.generateMethodHeader(methodName));
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setErrorScoreLimit(51.0);

	optimizer.setIterMax(40000);
	optimizer.setIter2Max(20000);
	optimizer.setKt(1.0);
	optimizer.setKt2(0.4);
	optimizer.setRerun(5);
	optimizer.setRandomizeFlag(true); // no initial sequence randomization
	optimizer.setFinalScoreWeight(0.05); // higher than default weight of 0.1
	CritonScorer scorer = new CritonScorer();

	scorer.setConsecutiveLimit(3);
	scorer.setConsecutiveLimitG(2);
	scorer.setCritonLen(6);
	scorer.setAuViolationWeight(10.0);
	scorer.setGcViolationWeight(10);
	scorer.setBranchMigrationWeight(10.0);
	scorer.setConsecutiveWeight(10.0);
	scorer.setGCMinFracWeight(0.0); // no minimum value for GC content
	scorer.setGuComplementMode(false);
	scorer.setZipperWeight(10.0);

	optimizer.setDefaultScorer(scorer);
	DummyEnergySecondaryStructureScorer scorer2 = new DummyEnergySecondaryStructureScorer();
	scorer2.setZeroMode(true);
	optimizer.setFinalScorer(scorer2);
	SecondaryStructure cubeStructure = generateCubeSecondaryStructure("cube_uneven4_opt42U_nowave_d.sec");
	assert cubeStructure != null;
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure with " + cubeStructure.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(cubeStructure));
	assert cubeStructure.getSequenceCount() == 6; // must be this many sequences
	System.out.println("Starting to optimize cube sequences:");
	String[] result = optimizer.optimize(cubeStructure);
	System.out.println("Finished optimizing cube sequences:");
	assert result != null;
	for (int i = 0; i < result.length; ++i) {
	    System.out.println(result[i]);
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Production test case, using for the first time sequences that have minimum helix lengths 4. Using as loops 2 Us.
     * No use of RNAcofold scoring.
     * New modified rule not allowing more than on G/C in a helix
     * 2nt length differences between sequences
     */
    @Test(groups={"new", "production", "slow"})
    public void testOptimizeCubeCritonLoop24HOpt42UunevenNoWave4() {
	String methodName = "testOptimizeCubeCritonLoop2H4Opt42UunevenNoWave4";
	System.out.println(TestTools.generateMethodHeader(methodName));
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setErrorScoreLimit(51.0);

	optimizer.setIterMax(40000);
	optimizer.setIter2Max(10);
	optimizer.setKt(1.0);
	optimizer.setKt2(0.4);
	optimizer.setRerun(50);
	optimizer.setRandomizeFlag(true); // no initial sequence randomization
	optimizer.setFinalScoreWeight(0.05); 
	CritonScorer scorer = new CritonScorer();

	scorer.setConsecutiveLimit(3);
	scorer.setConsecutiveLimitG(2);
	scorer.setCritonLen(6);
	scorer.setAuViolationWeight(10.0);
	scorer.setGcViolationWeight(10);
	scorer.setBranchMigrationWeight(10.0);
	scorer.setConsecutiveWeight(10.0);
	scorer.setGCMinFracWeight(0.0); // no minimum value for GC content
	scorer.setGuComplementMode(false);
	scorer.setZipperWeight(10.0);

	optimizer.setDefaultScorer(scorer);
	RnacofoldSecondaryStructureScorer scorer2 = new RnacofoldSecondaryStructureScorer();
	optimizer.setFinalScorer(scorer2);
	SecondaryStructure cubeStructure = generateCubeSecondaryStructure("cube_uneven4_opt42U_nowave_d.sec");
	scorer2.setSelfEnergyCutoff(-10.0); // only penalize strands if very stable energies
	SecondaryStructureScorer scorer3 = new SimpleSecondaryStructurePredictorScorer();
	scorer2.setSubScorer(scorer3);
	scorer2.setSubScorerWeight(5.0);
	optimizer.setFinalScorer(scorer2);
	assert cubeStructure != null;
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure with " + cubeStructure.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(cubeStructure));
	assert cubeStructure.getSequenceCount() == 6; // must be this many sequences
	System.out.println("Starting to optimize cube sequences:");
	String[] result = optimizer.optimize(cubeStructure);
	System.out.println("Finished optimizing cube sequences:");
	assert result != null;
	for (int i = 0; i < result.length; ++i) {
	    System.out.println(result[i]);
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Production test case, using for the first time sequences that have minimum helix lengths 4. Using as loops 2 Us.
     * Use of matchfold!
     * New modified rule not allowing more than on G/C in a helix
     * 2nt length differences between sequences
     * Careful: the input sequence has a "bug" with interaction "M". Use ...noved_e.sec instead in next test.
     */
    @Test(groups={"new", "production", "slow"})
    public void testOptimizeCubeCritonLoop24HOpt42UunevenNoWave5() {
	String methodName = "testOptimizeCubeCritonLoop2H4Opt42UunevenNoWave5";
	System.out.println(TestTools.generateMethodHeader(methodName));
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setErrorScoreLimit(1000.0);
	optimizer.setIterMax(10000);
	optimizer.setIter2Max(1000);
	optimizer.setKt(1.0);
	optimizer.setKt2(0.4);
	optimizer.setRerun(5);
	optimizer.setRandomizeFlag(true); // no initial sequence randomization
	// optimizer.setFinalScoreWeight(0.05); 
	optimizer.setFinalScoreWeight(1.0); 
	CritonScorer scorer = new CritonScorer();

	scorer.setConsecutiveLimit(3);
	scorer.setConsecutiveLimitG(2);
	scorer.setCritonLen(6);
	scorer.setAuViolationWeight(10.0);
	scorer.setGcViolationWeight(10);
	scorer.setBranchMigrationWeight(10.0);
	scorer.setConsecutiveWeight(10.0);
	scorer.setGCMinFracWeight(0.0); // no minimum value for GC content
	scorer.setGuComplementMode(false);
	scorer.setZipperWeight(10.0);

	optimizer.setDefaultScorer(scorer);
	SecondaryStructureScorer scorer2 = new MatchFoldSecondaryStructureScorer();
	optimizer.setFinalScorer(scorer2);
	SecondaryStructure cubeStructure = generateCubeSecondaryStructure("cube_uneven4_opt42U_nowave_d.sec");
	// scorer2.setSelfEnergyCutoff(-10.0); // only penalize strands if very stable energies
	assert cubeStructure != null;
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure with " + cubeStructure.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(cubeStructure));
	assert cubeStructure.getSequenceCount() == 6; // must be this many sequences
	System.out.println("Starting to optimize cube sequences:");
	String[] result = optimizer.optimize(cubeStructure);
	System.out.println("Finished optimizing cube sequences:");
	assert result != null;
	for (int i = 0; i < result.length; ++i) {
	    System.out.println(result[i]);
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Cube optimization. Same as testOptimizeCubeCritonLoop24HOpt42UunevenNoWave5, however fixed "bug" in interaction "M"
     * Use of matchfold!
     * New modified rule not allowing more than on G/C in a helix
     * 2nt length differences between sequences
     */
    @Test(groups={"new", "production", "slow"})
    public void testOptimizeCubeCritonLoop24HOpt42UunevenNoWave6MatchFold() {
	String methodName = "testOptimizeCubeCritonLoop2H4Opt42UunevenNoWave6MatchFold";
	System.out.println(TestTools.generateMethodHeader(methodName));
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setErrorScoreLimit(55.0);
	optimizer.setIterMax(10000);
	optimizer.setIter2Max(1000);
	optimizer.setKt(1.0);
	optimizer.setKt2(0.4);
	optimizer.setRerun(5);
	optimizer.setRandomizeFlag(true); // initial sequence randomization
	// optimizer.setFinalScoreWeight(0.05); 
	optimizer.setFinalScoreWeight(1.0); 
	CritonScorer scorer = new CritonScorer();

	scorer.setConsecutiveLimit(3);
	scorer.setConsecutiveLimitG(2);
	scorer.setCritonLen(6);
	scorer.setAuViolationWeight(10.0);
	scorer.setGcViolationWeight(1.0); // notice: difference compared to testOptimizeCubeCritonLoop24HOpt42UunevenNoWave5
	scorer.setBranchMigrationWeight(10.0);
	scorer.setConsecutiveWeight(10.0);
	scorer.setGCMinFracWeight(0.0); // no minimum value for GC content
	scorer.setGuComplementMode(false);
	scorer.setZipperWeight(10.0);

	optimizer.setDefaultScorer(scorer);
	SecondaryStructureScorer scorer2 = new MatchFoldSecondaryStructureScorer();
	optimizer.setFinalScorer(scorer2);
	SecondaryStructure cubeStructure = generateCubeSecondaryStructure("cube_uneven4_opt42U_nowave_e.sec");
	// scorer2.setSelfEnergyCutoff(-10.0); // only penalize strands if very stable energies
	assert cubeStructure != null;
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure with " + cubeStructure.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(cubeStructure));
	assert cubeStructure.getSequenceCount() == 6; // must be this many sequences
	System.out.println("Starting to optimize cube sequences:");
	String[] result = optimizer.optimize(cubeStructure);
	System.out.println("Finished optimizing cube sequences:");
	assert result != null;
	for (int i = 0; i < result.length; ++i) {
	    System.out.println(result[i]);
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Cube optimization. Same as testOptimizeCubeCritonLoop24HOpt42UunevenNoWave5, however fixed "bug" in interaction "M"
     * Use of matchfold!
     * New modified rule not allowing more than on G/C in a helix
     * 2nt length differences between sequences
     */
    @Test(groups={"new", "production", "slow"})
    public void testOptimizeCubeCritonLoop24HOpt42UunevenNoWave6NoMatchFold() {
	String methodName = "testOptimizeCubeCritonLoop2H4Opt42UunevenNoWave6NoMatchFold";
	System.out.println(TestTools.generateMethodHeader(methodName));
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setErrorScoreLimit(55.0);
	optimizer.setIterMax(10000);
	optimizer.setIter2Max(1000);
	optimizer.setKt(1.0);
	optimizer.setKt2(0.4);
	optimizer.setRerun(5);
	optimizer.setRandomizeFlag(true); // initial sequence randomization
	// optimizer.setFinalScoreWeight(0.05); 
	optimizer.setFinalScoreWeight(1.0); 
	CritonScorer scorer = new CritonScorer();

	scorer.setConsecutiveLimit(3);
	scorer.setConsecutiveLimitG(2);
	scorer.setCritonLen(6);
	scorer.setAuViolationWeight(10.0);
	scorer.setGcViolationWeight(1.0); // notice: difference compared to testOptimizeCubeCritonLoop24HOpt42UunevenNoWave5
	scorer.setBranchMigrationWeight(10.0);
	scorer.setConsecutiveWeight(10.0);
	scorer.setGCMinFracWeight(0.0); // no minimum value for GC content
	scorer.setGuComplementMode(false);
	scorer.setZipperWeight(10.0);

	optimizer.setDefaultScorer(scorer);
	SecondaryStructureScorer scorer2 = new DummyEnergySecondaryStructureScorer();
	optimizer.setFinalScorer(scorer2);
	SecondaryStructure cubeStructure = generateCubeSecondaryStructure("cube_uneven4_opt42U_nowave_e.sec");
	// scorer2.setSelfEnergyCutoff(-10.0); // only penalize strands if very stable energies
	assert cubeStructure != null;
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure with " + cubeStructure.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(cubeStructure));
	assert cubeStructure.getSequenceCount() == 6; // must be this many sequences
	System.out.println("Starting to optimize cube sequences:");
	String[] result = optimizer.optimize(cubeStructure);
	System.out.println("Finished optimizing cube sequences:");
	assert result != null;
	for (int i = 0; i < result.length; ++i) {
	    System.out.println(result[i]);
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Cube optimization. Same as testOptimizeCubeCritonLoop24HOpt42UunevenNoWave5, however fixed "bug" in interaction "M"
     * Use of matchfold!
     * New modified rule not allowing more than on G/C in a helix
     * 2nt length differences between sequences
     */
    @Test(groups={"new", "production", "slow"})
    public void testOptimizeCube10CritonLoop24HOpt42MatchFold() {
	String methodName = "testOptimizeCube10CritonLoop2H4Opt42MatchFold";
	System.out.println(TestTools.generateMethodHeader(methodName));
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setErrorScoreLimit(82.0);
	optimizer.setIterMax(20000);
	optimizer.setIter2Max(1000);
	optimizer.setKt(1.0);
	optimizer.setKt2(0.4);
	optimizer.setRerun(5);
	optimizer.setRandomizeFlag(true); // initial sequence randomization
	// optimizer.setFinalScoreWeight(0.05); 
	optimizer.setFinalScoreWeight(1.0); 
	CritonScorer scorer = new CritonScorer();

	scorer.setConsecutiveLimit(3);
	scorer.setConsecutiveLimitG(2);
	scorer.setCritonLen(6);
	scorer.setAuViolationWeight(10.0);
	scorer.setGcViolationWeight(1.0); // notice: difference compared to testOptimizeCubeCritonLoop24HOpt42UunevenNoWave5
	scorer.setBranchMigrationWeight(10.0);
	scorer.setConsecutiveWeight(10.0);
	scorer.setGCMinFracWeight(0.0); // no minimum value for GC content
	scorer.setGuComplementMode(false);
	scorer.setZipperWeight(10.0);

	optimizer.setDefaultScorer(scorer);
	SecondaryStructureScorer scorer2 = new MatchFoldSecondaryStructureScorer();
	optimizer.setFinalScorer(scorer2);
	SecondaryStructure cubeStructure = generateCubeSecondaryStructure("daisy3_0_rmAPcaps_helix_bridge2_fused3_unit_edit.sec");
	// scorer2.setSelfEnergyCutoff(-10.0); // only penalize strands if very stable energies
	assert cubeStructure != null;
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure with " + cubeStructure.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(cubeStructure));
	assert cubeStructure.getSequenceCount() == 10; // must be this many sequences
	System.out.println("Starting to optimize cube sequences:");
	String[] result = optimizer.optimize(cubeStructure);
	System.out.println("Finished optimizing cube sequences:");
	assert result != null;
	for (int i = 0; i < result.length; ++i) {
	    System.out.println(result[i]);
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Cube optimization. Same as testOptimizeCubeCritonLoop24HOpt42UunevenNoWave5, however fixed "bug" in interaction "M"
     * Use of matchfold!
     * New modified rule not allowing more than on G/C in a helix
     * 2nt length differences between sequences
     */
    @Test(groups={"new", "production", "slow"})
    public void testOptimizeTriangleMatchFold() {
	String methodName = "testOptimizeTriangleMatchFold";
	System.out.println(TestTools.generateMethodHeader(methodName));
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setErrorScoreLimit(118.0);
	optimizer.setIterMax(20000);
	optimizer.setIter2Max(1000);
	optimizer.setKt(1.0);
	optimizer.setKt2(0.4);
	optimizer.setRerun(5);
	optimizer.setRandomizeFlag(true); // initial sequence randomization
	// optimizer.setFinalScoreWeight(0.05); 
	optimizer.setFinalScoreWeight(1.0); 
	CritonScorer scorer = new CritonScorer();

	scorer.setConsecutiveLimit(3);
	scorer.setConsecutiveLimitG(2);
	scorer.setCritonLen(6);
	scorer.setAuViolationWeight(10.0);
	scorer.setGcViolationWeight(1.0); // notice: difference compared to testOptimizeCubeCritonLoop24HOpt42UunevenNoWave5
	scorer.setBranchMigrationWeight(10.0);
	scorer.setConsecutiveWeight(10.0);
	scorer.setGCMinFracWeight(0.0); // no minimum value for GC content
	scorer.setGuComplementMode(false);
	scorer.setZipperWeight(10.0);

	optimizer.setDefaultScorer(scorer);
	SecondaryStructureScorer scorer2 = new MatchFoldSecondaryStructureScorer();
	optimizer.setFinalScorer(scorer2);
	SecondaryStructure cubeStructure = generateCubeSecondaryStructure("triangle.sec");
	// scorer2.setSelfEnergyCutoff(-10.0); // only penalize strands if very stable energies
	assert cubeStructure != null;
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure with " + cubeStructure.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(cubeStructure));
	assert cubeStructure.getSequenceCount() == 1; // must be this many sequences
	System.out.println("Starting to optimize cube sequences:");
	String[] result = optimizer.optimize(cubeStructure);
	System.out.println("Finished optimizing cube sequences:");
	assert result != null;
	for (int i = 0; i < result.length; ++i) {
	    System.out.println(result[i]);
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Cube optimization. Same as testOptimizeCubeCritonLoop24HOpt42UunevenNoWave5, however fixed "bug" in interaction "M"
     * Use of matchfold!
     * New modified rule not allowing more than on G/C in a helix
     * 2nt length differences between sequences
     */
    @Test(groups={"new", "production", "slow"})
    public void testOptimizeTriangle3MatchFold() {
	String methodName = "testOptimizeTriangle3MatchFold";
	System.out.println(TestTools.generateMethodHeader(methodName));
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setErrorScoreLimit(120.0);
	optimizer.setIterMax(20000);
	optimizer.setIter2Max(10000);
	optimizer.setKt(1.0);
	optimizer.setKt2(0.4);
	optimizer.setRerun(5);
	optimizer.setRandomizeFlag(true); // initial sequence randomization
	// optimizer.setFinalScoreWeight(0.05); 
	optimizer.setFinalScoreWeight(1.0); 
	CritonScorer scorer = new CritonScorer();

	scorer.setConsecutiveLimit(3);
	scorer.setConsecutiveLimitG(2);
	scorer.setCritonLen(6);
	scorer.setAuViolationWeight(10.0);
	scorer.setGcViolationWeight(1.0); // notice: difference compared to testOptimizeCubeCritonLoop24HOpt42UunevenNoWave5
	scorer.setBranchMigrationWeight(10.0);
	scorer.setConsecutiveWeight(10.0);
	scorer.setGCMinFracWeight(0.0); // no minimum value for GC content
	scorer.setGuComplementMode(false);
	scorer.setZipperWeight(10.0);

	optimizer.setDefaultScorer(scorer);
	SecondaryStructureScorer scorer2 = new MatchFoldSecondaryStructureScorer();
	optimizer.setFinalScorer(scorer2);
	SecondaryStructure cubeStructure = generateCubeSecondaryStructure("triangle3.sec");
	// scorer2.setSelfEnergyCutoff(-10.0); // only penalize strands if very stable energies
	assert cubeStructure != null;
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure with " + cubeStructure.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(cubeStructure));
	assert cubeStructure.getSequenceCount() == 3; // must be this many sequences
	System.out.println("Starting to optimize cube sequences:");
	String[] result = optimizer.optimize(cubeStructure);
	System.out.println("Finished optimizing cube sequences:");
	assert result != null;
	for (int i = 0; i < result.length; ++i) {
	    System.out.println(result[i]);
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Cube optimization: Using modified 10-strand cube design with 4 MG binding aptamer groups.
     * Use of matchfold!
     */
    @Test(groups={"new", "production", "slow"})
    public void testOptimizeCube10MG4_1MatchFold() {
	String methodName = "testOptimizeCube10MG4_1MatchFold";
	System.out.println(TestTools.generateMethodHeader(methodName));
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setErrorScoreLimit(146.0);
	optimizer.setIterMax(20000);
	optimizer.setIter2Max(1000);
	optimizer.setKt(1.0);
	optimizer.setKt2(0.4);
	optimizer.setRerun(5);
	optimizer.setRandomizeFlag(true); // initial sequence randomization
	// optimizer.setFinalScoreWeight(0.05); 
	optimizer.setFinalScoreWeight(1.0); 
	CritonScorer scorer = new CritonScorer();
	scorer.setConsecutiveLimit(3);
	scorer.setConsecutiveLimitG(2);
	scorer.setCritonLen(6);
	scorer.setAuViolationWeight(10.0);
	scorer.setGcViolationWeight(1.0); // notice: difference compared to testOptimizeCubeCritonLoop24HOpt42UunevenNoWave5
	scorer.setBranchMigrationWeight(10.0);
	scorer.setConsecutiveWeight(10.0);
	scorer.setGCMinFracWeight(0.0); // no minimum value for GC content
	scorer.setGuComplementMode(false);
	scorer.setZipperWeight(10.0);
	optimizer.setDefaultScorer(scorer);
	SecondaryStructureScorer scorer2 = new MatchFoldSecondaryStructureScorer();
	optimizer.setFinalScorer(scorer2);
	SecondaryStructure cubeStructure = generateCubeSecondaryStructure("cube_even_10_mg4-1.sec");
	// scorer2.setSelfEnergyCutoff(-10.0); // only penalize strands if very stable energies
	assert cubeStructure != null;
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure with " + cubeStructure.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(cubeStructure));
	assert cubeStructure.getSequenceCount() == 10; // must be this many sequences
	System.out.println("Starting to optimize cube sequences:");
	String[] result = optimizer.optimize(cubeStructure);
	System.out.println("Finished optimizing cube sequences:");
	assert result != null;
	for (int i = 0; i < result.length; ++i) {
	    System.out.println(result[i]);
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Production test case of set of two cubes bridged at corner */
    @Test(groups={"new", "production", "slow"})
    public void testOptimizeCubeMG_BF() {
	String methodName = "testOptimizeCubeMG_BF";
	System.out.println(TestTools.generateMethodHeader(methodName));
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setErrorScoreLimit(25.0);
	optimizer.setIterMax(40000);
	optimizer.setIter2Max(20000);
	optimizer.setKt(1.0);
	optimizer.setKt2(0.4);
	optimizer.setRerun(5);
	optimizer.setRandomizeFlag(false); // no initial sequence randomization
	optimizer.setFinalScoreWeight(0.05); // higher than default weight of 0.1
	RnacofoldSecondaryStructureScorer scorer2 = new RnacofoldSecondaryStructureScorer();
	// scorer2.setInterStrandMode(false); // deactivate compute only intra-strand energies
	scorer2.setSelfEnergyCutoff(-10.0); // only penalize strands if very stable energies
	SecondaryStructureScorer scorer3 = new SimpleSecondaryStructurePredictorScorer();
	scorer2.setSubScorer(scorer3);
	scorer2.setSubScorerWeight(5.0);
	optimizer.setFinalScorer(scorer2);
	SecondaryStructure cubeStructure = generateCubeSecondaryStructure("testCubeFragmentPlacementLoop5_27_bridged_fused.sec");
	assert cubeStructure != null;
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure with " + cubeStructure.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(cubeStructure));
	assert cubeStructure.getSequenceCount() == 6; // must be this many sequences
	System.out.println("Starting to optimize cube sequences:");
	String[] result = optimizer.optimize(cubeStructure);
	System.out.println("Finished optimizing cube sequences:");
	assert result != null;
	for (int i = 0; i < result.length; ++i) {
	    System.out.println(result[i]);
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Production test case, using for the first time small triangle sequences. */
    @Test(groups={"new", "production", "slow"})
    public void testOptimizeSmallTriangle() {
	String methodName = "testOptimizeSmallTriangle";
	System.out.println(TestTools.generateMethodHeader(methodName));
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setErrorScoreLimit(45.0);
	optimizer.setIterMax(40000);
	optimizer.setIter2Max(20000);
	optimizer.setKt(1.0);
	optimizer.setKt2(0.4);
	optimizer.setRerun(5);
	optimizer.setRandomizeFlag(false); // no initial sequence randomization
	optimizer.setFinalScoreWeight(0.05); // higher than default weight of 0.1
	RnacofoldSecondaryStructureScorer scorer2 = new RnacofoldSecondaryStructureScorer();
	// scorer2.setInterStrandMode(false); // deactivate compute only intra-strand energies
	scorer2.setSelfEnergyCutoff(-10.0); // only penalize strands if very stable energies
	SecondaryStructureScorer scorer3 = new SimpleSecondaryStructurePredictorScorer();
	scorer2.setSubScorer(scorer3);
	scorer2.setSubScorerWeight(5.0);
	optimizer.setFinalScorer(scorer2);
	SecondaryStructure cubeStructure = generateCubeSecondaryStructure("testFuseStrandsCommandTest_final2.sec");
	assert cubeStructure != null;
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure with " + cubeStructure.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(cubeStructure));
	assert cubeStructure.getSequenceCount() == 4; // must be this many sequences
	System.out.println("Starting to optimize cube sequences:");
	String[] result = optimizer.optimize(cubeStructure);
	System.out.println("Finished optimizing cube sequences:");
	assert result != null;
	for (int i = 0; i < result.length; ++i) {
	    System.out.println(result[i]);
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }


    /** Production test case, using for the first time small triangle sequences. */
    @Test(groups={"new", "production", "slow"})
    public void testOptimizeMir192() {
	String methodName = "testOptimizeMir192";
	System.out.println(TestTools.generateMethodHeader(methodName));
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setErrorScoreLimit(25.0);
	optimizer.setIterMax(40000);
	optimizer.setIter2Max(1);
	optimizer.setKt(1.0);
	optimizer.setKt2(0.4);
	optimizer.setRerun(5);
	optimizer.setRandomizeFlag(true); // no initial sequence randomization
	optimizer.setFinalScoreWeight(0.05); // higher than default weight of 0.1
	RnacofoldSecondaryStructureScorer scorer2 = new RnacofoldSecondaryStructureScorer();
	// scorer2.setInterStrandMode(false); // deactivate compute only intra-strand energies
	scorer2.setSelfEnergyCutoff(-10.0); // only penalize strands if very stable energies
	SecondaryStructureScorer scorer3 = new SimpleSecondaryStructurePredictorScorer();
	scorer2.setSubScorer(scorer3);
	scorer2.setSubScorerWeight(5.0);
	optimizer.setFinalScorer(scorer2);
	SecondaryStructure cubeStructure = generateCubeSecondaryStructure("mir192_let7d_mir21.sec");
	assert cubeStructure != null;
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure with " + cubeStructure.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(cubeStructure));
	assert cubeStructure.getSequenceCount() == 2; // must be this many sequences
	System.out.println("Starting to optimize mir-192 sequences:");
	String[] result = optimizer.optimize(cubeStructure);
	System.out.println("Finished optimizing cube sequences:");
	assert result != null;
	for (int i = 0; i < result.length; ++i) {
	    System.out.println(result[i]);
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Production test case, using for the first time sequences that have minimum helix lengths 4, using half sized sequences. */
    @Test(groups={"new", "production", "slow"})
    public void testOptimizeCubeCritonHalfSeq() {
	String methodName = "testOptimizeCubeCritonHalfSeq";
	System.out.println(TestTools.generateMethodHeader(methodName));
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setErrorScoreLimit(60.0);
	optimizer.setIterMax(20000);
	optimizer.setIter2Max(40000);
	optimizer.setKt(1.0);
	optimizer.setKt2(0.4);
	optimizer.setRerun(5);
	optimizer.setRandomizeFlag(false); // no initial sequence randomization
	optimizer.setFinalScoreWeight(0.05); // higher than default weight of 0.1
	RnacofoldSecondaryStructureScorer scorer2 = new RnacofoldSecondaryStructureScorer();
	// scorer2.setInterStrandMode(false); // deactivate compute only intra-strand energies
	scorer2.setSelfEnergyCutoff(-10.0); // only penalize strands if very stable energies
	SecondaryStructureScorer scorer3 = new SimpleSecondaryStructurePredictorScorer();
	scorer2.setSubScorer(scorer3);
	scorer2.setSubScorerWeight(5.0);
	optimizer.setFinalScorer(scorer2);
	SecondaryStructure cubeStructure = generateCubeSecondaryStructure("cube_even_s10.sec");
	assert cubeStructure != null;
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure with " + cubeStructure.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(cubeStructure));
	assert cubeStructure.getSequenceCount() == 10; // must be this many sequences
	System.out.println("Starting to optimize cube sequences:");
	String[] result = optimizer.optimize(cubeStructure);
	System.out.println("Finished optimizing cube sequences:");
	assert result != null;
	for (int i = 0; i < result.length; ++i) {
	    System.out.println(result[i]);
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Production test case, using for the first time sequences that have minimum helix lengths 4 */
    @Test(groups={"new", "production", "slow"})
    public void testOptimizeLargeCubeCritonLoop2() {
	String methodName = "testOptimizeLargeCubeCritonLoop2";
	System.out.println(TestTools.generateMethodHeader(methodName));
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setErrorScoreLimit(21.5);
	optimizer.setIterMax(20000);
	optimizer.setIter2Max(40000);
	optimizer.setKt(1.0);
	optimizer.setKt2(0.4);
	optimizer.setRerun(5);
	optimizer.setRandomizeFlag(true); // no initial sequence randomization
	optimizer.setFinalScoreWeight(0.05); // higher than default weight of 0.1
	CritonScorer scorer1 = new CritonScorer();
	scorer1.setCritonLen(6); // higher than default 5
	scorer1.setDebugLevel(2);
	optimizer.setDefaultScorer(scorer1);
	RnacofoldSecondaryStructureScorer scorer2 = new RnacofoldSecondaryStructureScorer();
	// scorer2.setInterStrandMode(false); // deactivate compute only intra-strand energies
	scorer2.setSelfEnergyCutoff(-120.0); // only penalize strands if very stable energies
	SecondaryStructureScorer scorer3 = new SimpleSecondaryStructurePredictorScorer();
	scorer2.setEnergyGap(17.5); // require minimum free energy of binding
	scorer2.setStructureWeight(0.01);
	scorer2.setSubScorer(scorer3);
	scorer2.setSubScorerWeight(5.0);
	optimizer.setFinalScorer(scorer2);
	SecondaryStructure cubeStructure = generateCubeSecondaryStructure("testExtendAtStartLargeCubeLoop2.sec");
	assert cubeStructure != null;
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure with " + cubeStructure.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(cubeStructure));
	assert cubeStructure.getSequenceCount() == 6; // must be this many sequences
	System.out.println("Starting to optimize cube sequences:");
	String[] result = optimizer.optimize(cubeStructure);
	System.out.println("Finished optimizing cube sequences:");
	assert result != null;
	for (int i = 0; i < result.length; ++i) {
	    System.out.println(result[i]);
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Uses previously optimized cube as input */
    @Test(groups={"new", "slow"})
    public void testOptimizeCubeOpt() {
	String methodName = "testOptimizeCubeOpt";
	System.out.println(TestTools.generateMethodHeader(methodName));
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setErrorScoreLimit(2);
	optimizer.setIterMax(10000);
	optimizer.setIter2Max(100);
	SecondaryStructure cubeStructure = generateCubeSecondaryStructure("cube_uneven_opt.sec");
	assert cubeStructure != null;
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure with " + cubeStructure.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(cubeStructure));
	assert cubeStructure.getSequenceCount() == 6; // must be this many sequences
	System.out.println("Starting to optimize cube sequences:");
	String[] result = optimizer.optimize(cubeStructure);
	System.out.println("Finished optimizing cube sequences:");
	assert result != null;
	for (int i = 0; i < result.length; ++i) {
	    System.out.println(result[i]);
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Use cube sequences from Ned Seeman */
    @Test(groups={"new", "slow"})
    public void testOptimizeDnaCube() {
	String methodName = "testOptimizeDnaCube";
	System.out.println(TestTools.generateMethodHeader(methodName));
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setErrorScoreLimit(0.0);
	optimizer.setIterMax(10000);
	optimizer.setIter2Max(100);
	optimizer.setRandomizeFlag(false); // no sequence randomization
	SecondaryStructure cubeStructure = generateCubeSecondaryStructure("dna_cube.sec");
	assert cubeStructure != null;
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure with " + cubeStructure.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(cubeStructure));
	assert cubeStructure.getSequenceCount() == 10; // must be this many sequences
	System.out.println("Starting to optimize cube sequences:");
	String[] result = optimizer.optimize(cubeStructure);
	System.out.println("Finished optimizing cube sequences:");
	assert result != null;
	for (int i = 0; i < result.length; ++i) {
	    System.out.println(result[i]);
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

    /** Use cube sequences from Ned Seeman */
    @Test(groups={"new", "slow"})
    public void testOptimizeDnaTetrahedron() {
	String methodName = "testOptimizeDnaTetrahedron";
	System.out.println(TestTools.generateMethodHeader(methodName));
	MonteCarloSequenceOptimizer optimizer = new MonteCarloSequenceOptimizer();
	optimizer.setErrorScoreLimit(0.0);
	optimizer.setIterMax(10000);
	optimizer.setIter2Max(100);
	optimizer.setRandomizeFlag(true); // no sequence randomization
	SecondaryStructure cubeStructure = generateCubeSecondaryStructure("tetrahedron.sec");
	assert cubeStructure != null;
	SecondaryStructureScriptFormatWriter writer = new SecondaryStructureScriptFormatWriter();
	System.out.println("Read the following structure with " + cubeStructure.getSequenceCount() + " sequences:");
	System.out.println(writer.writeString(cubeStructure));
	assert cubeStructure.getSequenceCount() == 4; // must be this many sequences
	System.out.println("Starting to optimize cube sequences:");
	String[] result = optimizer.optimize(cubeStructure);
	System.out.println("Finished optimizing cube sequences:");
	assert result != null;
	for (int i = 0; i < result.length; ++i) {
	    System.out.println(result[i]);
	}
	System.out.println(TestTools.generateMethodFooter(methodName));
    }

}
