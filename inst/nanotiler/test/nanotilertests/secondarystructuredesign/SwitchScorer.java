package nanotilertests.secondarystructuredesign;

import java.util.Properties;
import rnasecondary.*;
import secondarystructuredesign.*;

/** Assigns a score to a secondary structure and a set of potential sequences */
public interface SwitchScorer {
    
    /** returns error score for complete secondary structure and trial sequences */
    Properties generateReport(StringBuffer[] bseqs,
			      SecondaryStructure structure1,
			      SecondaryStructure structure2,
			      int[] structure1SeqIds,
			      int[] structure2SeqIds);


    /** returns error score for complete secondary structure and trial sequences */
    Properties generateReport(StringBuffer[] bseqs,
			      SecondaryStructure structure1,
			      SecondaryStructure antiStructure1,
			      SecondaryStructure structure2,
			      SecondaryStructure antiStructure2,
			      int[] structure1SeqIds,
			      int[] structure2SeqIds);


    /** Sets new secondary structure scorer */
    void setScorer(SecondaryStructureScorer _scorer);

}
