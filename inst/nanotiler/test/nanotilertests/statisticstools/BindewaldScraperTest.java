package nanotilertests.statisticstools;

import generaltools.StringTools;
import java.io.*;
import java.util.List;
import java.util.Properties;
import statisticstools.*;

import org.testng.*;
import org.testng.annotations.*;

import static statisticstools.StatisticsVocabulary.*;

public class BindewaldScraperTest {

    /** @depracated : uses several external scripts
     * and is almost never used.
    /*
    @Test(groups={"new"})
    public void testParseBindewaldFile() {
	System.out.println("Starting testParseBindewaldFile");
	String filename = "${NANOTILER_HOME}/test/fixtures/valuepairs_spearman.out";
	try {
	    FileInputStream fis = new FileInputStream(filename);
	    String[] lines = StringTools.readAllLines(fis);
	    BindewaldScraper bindewald = new BindewaldScraper(lines);
	    System.out.println("Generated statistics object from file " + filename 
			       + " " + bindewald);
	    assert bindewald.validate();
	    assert bindewald.getPValue() == 0.01667; // compare with file
	}
	catch (IOException ioe) {
	    System.out.println("Error reading file " + filename + " : " + ioe.getMessage());
	    assert false;
	}
	System.out.println("Passed test testParseBindewaldFile !");
    }
    */
}
