package nanotilertests.statisticstools;

import launchtools.SimpleQueueManager;
import generaltools.ParsingException;
import generaltools.TestTools;
import generaltools.Randomizer;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.io.*;
import statisticstools.*;

import org.testng.*;
import org.testng.annotations.*;

import static statisticstools.StatisticsVocabulary.*;

public class CorrelationTestsTest {

    /** Computes Spearman correlation coefficient, p-values etc using external call to R. */
    @Test(groups={"new"})
    public void testComputeCorrelation() {
	System.out.println(TestTools.generateMethodHeader("testComputeCorrelation"));
	String datafileName = "../test/fixtures/valuepairs.dat";
	try {
	    FileInputStream fis = new FileInputStream(datafileName);
	    List<List<Double> > sampleData = CorrelationTests.readPairedData(fis);
	    StatisticsResult result = CorrelationTests.computeCorrelation(sampleData, SPEARMAN);
	    System.out.println("The result of the Spearman correlation is: " + result);
	    StatisticsResult bindewaldResult = CorrelationTests.computeCorrelation(sampleData, BINDEWALD);
	    System.out.println("The result of the Bindewald correlation is: " + bindewaldResult);
	    assert bindewaldResult.getPValue() >= result.getPValue(); // Bindewald correlation is more conservative
	}
	catch (IOException ioe) {
	    System.out.println("Error reading file: " + datafileName + " " + ioe.getMessage());
	    assert false;
	}
	catch (ParsingException pe) {
	    System.out.println("Error parsing file: " + datafileName + " " + pe.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter("testComputeCorrelation"));
    }

}
