package nanotilertests.statisticstools;

import generaltools.StringTools;
import generaltools.TestTools;
import java.io.*;
import java.util.List;
import java.util.Properties;

import org.testng.*;
import org.testng.annotations.*;

import statisticstools.*;

import static statisticstools.StatisticsVocabulary.*;

public class RSpearmanScraperTest {


    @Test(groups={"new"})
    public void testParseSpearmanFile() {
	System.out.println(TestTools.generateMethodHeader("testParseSpearmanFile"));
	String filename = "../test/fixtures/valuepairs_spearman.out";
	try {
	    FileInputStream fis = new FileInputStream(filename);
	    String[] lines = StringTools.readAllLines(fis);
	    RSpearmanScraper spearman = new RSpearmanScraper(lines);
	    System.out.println("Generated statistics object from file " + filename 
			       + " " + spearman);
	    assert spearman.validate();
	    assert spearman.getPValue() == 0.01667; // compare with file
	    assert 1.0 == Double.parseDouble(spearman.getProperty(StatisticsVocabulary.ESTIMATE));
	}
	catch (IOException ioe) {
	    System.out.println("Error reading file " + filename + " : " + ioe.getMessage());
	    assert false;
	}
	catch (NumberFormatException nfe) {
	    System.out.println("Error parsing correlation coefficient: " + nfe.getMessage());
	    assert false;
	}
	System.out.println(TestTools.generateMethodFooter("testParseSpearmanFile"));
    }

}
