package nanotilertests.tools3d;

import org.testng.annotations.*;
import generaltools.TestTools;
import generaltools.Randomizer;
import java.util.Random;
import java.io.*;
import tools3d.*;

/**
 * centrol superposition method, given by Peter Rotkiewitc
 */
public class Vector3DToolsTest {

    private static Random rnd = Randomizer.getInstance();

    public Vector3DToolsTest() { }

    private static void printPoints(Vector3D[] points, PrintStream ps) {
	for (int i = 0; i < points.length; ++i) {
	    ps.println("" + (i + 1) + " : " + points[i]);
	}
    }

    @Test(groups={"newest"})
public void testComputeSphereIntersectionVolume() {
	String methodName = "testCapsuleIntersectionbySphere";
	System.out.println(TestTools.generateMethodHeader(methodName));

  //end points of first helix
  Vector3D h11 = new Vector3D(0.0,0.0,0.0);
//  Vector3D h12 = new Vector3D(10.0,0.0,0.0);
  
  //end points of second helix
  Vector3D h21 = new Vector3D(1000.0,1000.0,1000.0);
//  Vector3D h22 = new Vector3D(1010.0,1000.0,1000.0);
  
  // Vector3D h11 = new Vector3D(-689.1926607703246, 536.2127426023994, -301.5023236660967);
  // Vector3D h12 = new Vector3D(-668.0417199973901, 549.7859776545455, -297.7500441854528 );
  // Vector3D h21 = new Vector3D(267.668097401389, -227.11185010380373, 462.4082121684878 );
  // Vector3D h22 = new Vector3D(290.1654654721174, -215.30516032876926 ,462.7891493463009 );
  
  //intersection between helices with radius 10.0
  double score = Vector3DTools.computeSphereIntersectionVolume(h11, h21, 10.0, 10.0); //
  System.out.println("Overlap: "+score);
  
  assert score == 0.0: "Helices are far away, should be 0 overlap."; //far away helices

	System.out.println(TestTools.generateMethodFooter(methodName));
  }


  @Test(groups={"newest"})
public void testComputeSphereIntersectionVolumeWithOverlap() {
String methodName = "testCapsuleIntersectionbySphere";
System.out.println(TestTools.generateMethodHeader(methodName));

//end points of first helix
Vector3D h11 = new Vector3D(0.0,0.0,0.0);
//  Vector3D h12 = new Vector3D(10.0,0.0,0.0);

//end points of second helix
Vector3D h21 = new Vector3D(10000.0, 10000.0, 10000.0);
//  Vector3D h22 = new Vector3D(1010.0,1000.0,1000.0);

// Vector3D h11 = new Vector3D(-689.1926607703246, 536.2127426023994, -301.5023236660967);
// Vector3D h12 = new Vector3D(-668.0417199973901, 549.7859776545455, -297.7500441854528 );
// Vector3D h21 = new Vector3D(267.668097401389, -227.11185010380373, 462.4082121684878 );
// Vector3D h22 = new Vector3D(290.1654654721174, -215.30516032876926 ,462.7891493463009 );
double radius = 1.0;
//intersection between helices with radius 10.0
double score = Vector3DTools.computeSphereIntersectionVolume(h11, h21, radius, radius); //
assert (score >= 0.0);
double volume = (4*Math.PI/3.0) * Math.pow(radius, 3);
assert (Math.abs(score-volume) < 0.001);
System.out.println("Overlap: "+score);

assert score <= 0.001: "Helices are far away, should be 0 overlap."; //far away helices

System.out.println(TestTools.generateMethodFooter(methodName));
}

  @Test(groups={"newest"})
  public void testComputeSphereIntersectionVolume4() {
String methodName = "testIntersectionbySphere_4";
System.out.println(TestTools.generateMethodHeader(methodName));

//end points of first helix
Vector3D h11 = new Vector3D(0.0,0.0,0.0);
Vector3D h12 = new Vector3D(10.0,0.0,0.0);

//end points of second helix
Vector3D h21 = new Vector3D(1000.0,1000.0,1000.0);
Vector3D h22 = new Vector3D(1000.0,1000.0,1000.0);

// Vector3D h11 = new Vector3D(-689.1926607703246, 536.2127426023994, -301.5023236660967);
// Vector3D h12 = new Vector3D(-668.0417199973901, 549.7859776545455, -297.7500441854528 );
// Vector3D h21 = new Vector3D(267.668097401389, -227.11185010380373, 462.4082121684878 );
// Vector3D h22 = new Vector3D(290.1654654721174, -215.30516032876926 ,462.7891493463009 );

//intersection between helices with radius 10.0
double score = Vector3DTools.computeSphereIntersectionVolume(h11, h12, 10.0, h21, h22, 10.0); //
System.out.println("Overlap: "+score);

assert score == 0.0: "Helices are far away, should be 0 overlap."; //far away helices

System.out.println(TestTools.generateMethodFooter(methodName));
}

    @Test(groups={"newest"})
    public void testCapsuleIntersection_4() {
	String methodName = "testCapsuleIntersection_4";
	System.out.println(TestTools.generateMethodHeader(methodName));

  //end points of first helix
  Vector3D h11 = new Vector3D(0.0,0.0,0.0);
  Vector3D h12 = new Vector3D(10.0,0.0,0.0);
  
  //end points of second helix
  Vector3D h21 = new Vector3D(1000.0,1000.0,1000.0);
  Vector3D h22 = new Vector3D(1010.0,1000.0,1000.0);
  
  // Vector3D h11 = new Vector3D(-689.1926607703246, 536.2127426023994, -301.5023236660967);
  // Vector3D h12 = new Vector3D(-668.0417199973901, 549.7859776545455, -297.7500441854528 );
  // Vector3D h21 = new Vector3D(267.668097401389, -227.11185010380373, 462.4082121684878 );
  // Vector3D h22 = new Vector3D(290.1654654721174, -215.30516032876926 ,462.7891493463009 );
  
  //intersection between helices with radius 10.0
  double score = Vector3DTools.capsuleIntersectionVolume(h11, h12, 10.0, h21, h22, 10.0); //
  System.out.println("Overlap: "+score);
  
  assert score == 0.0: "Helices are far away, should be 0 overlap."; //far away helices

	System.out.println(TestTools.generateMethodFooter(methodName));
    }

}
