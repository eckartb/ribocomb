package nanotilertests.tools3d.objects3d;

import java.util.logging.Logger;

import tools3d.CoordinateSystem;
import tools3d.Matrix3D;
import tools3d.Vector3D;
import tools3d.Matrix4D;
import tools3d.Vector4D;
import tools3d.Vector3DTools;
import tools3d.Orientable;
import tools3d.objects3d.*;

import org.testng.*;
import org.testng.annotations.*;

import static tools3d.PackageConstants.*;

public class CoordinateSystem3DTest {

    public CoordinateSystem3DTest() { }

    @Test(groups={"fast"})
    public void testInverse() {
	CoordinateSystem3D cs = new CoordinateSystem3D(Vector3D.ZVEC);
	Vector3D rotAxis = new Vector3D(0.2, 0.3, 0.4);
	Vector3D newPos = new Vector3D(10.0, 15, -11.0);
	double angle = 30.0 * DEG2RAD;
	cs.rotate(rotAxis, angle);
	cs.translate(newPos);
	Matrix4D m1 = cs.generateMatrix4D();
	System.out.println("Raw coordinate system: " + cs.toString());
	CoordinateSystem3D cs2 = new CoordinateSystem3D(cs);
	CoordinateSystem3D cs3 = new CoordinateSystem3D(cs);
	CoordinateSystem3D cs4 = new CoordinateSystem3D(cs);
	CoordinateSystem3D invertedCs = (CoordinateSystem3D)(cs.inverse());
	Matrix4D m2 = invertedCs.generateMatrix4D();
	Matrix4D m3 = m1.multiply(m2);
	System.out.println("Inverted coordinate system: " + invertedCs.toString());
	cs.activeTransform(invertedCs); // should cancel each other out!
	cs2.activeTransform2(invertedCs); // should cancel each other out!
	cs3.activeTransform3(invertedCs); // should cancel each other out!
	System.out.println("Should be unit coordinate system: " + cs.toString());
	System.out.println("Should again2  be unit coordinate system: " + cs3.toString());
	System.out.println("Should again3  be unit coordinate system: " + m3.toString());
	assert cs.getPosition().length() < 0.01; // should be zero
	Vector3D newX = cs.getX();
	Vector3D newY = cs.getY();
	Vector3D newZ = cs.getZ();
	assert newX.minus(Vector3D.EX).length() < 0.01;
	assert newY.minus(Vector3D.EY).length() < 0.01;
	assert newZ.minus(Vector3D.EZ).length() < 0.01;
	assert cs.isUnitTransform(0.01);
    }
}
