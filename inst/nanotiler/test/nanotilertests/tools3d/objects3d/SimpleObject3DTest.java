package nanotilertests.tools3d.objects3d;

import org.testng.annotations.*;

import tools3d.objects3d.*;

public class SimpleObject3DTest {

    /** Tests construction of object tree, retrieval by name */
    @Test(groups={"new"})
    public void testFindInTree() {
	SimpleObject3D obj1 = new SimpleObject3D("p1");
	SimpleObject3D obj1_1 = new SimpleObject3D("p1_1");
	SimpleObject3D obj1_2 = new SimpleObject3D("p1_2");
	SimpleObject3D obj1_2_1 = new SimpleObject3D("p1_2_1");
	SimpleObject3D obj1_2_2 = new SimpleObject3D("p1_2_2");
	obj1.insertChild(obj1_1);
	obj1.insertChild(obj1_2);
	obj1_2.insertChild(obj1_2_1);
	obj1_2.insertChild(obj1_2_2);
	assert obj1.size() == 2;
	assert obj1.getChild(0) == obj1_1;
	assert obj1.getChild("p1_1") == obj1_1;
	assert obj1_2.getChild("p1_2_2") == obj1_2_2;
	assert Object3DTools.findByFullName(obj1,"p1.p1_2.p1_2_2") == obj1_2_2;
	assert Object3DTools.findByFullName(obj1,"1.2.2") == obj1_2_2; // must be findable using number scheme
	assert Object3DTools.findByFullName(obj1,"1.2.(Object3D)2") == obj1_2_2; // must be findable using number scheme
	assert Object3DTools.findByFullName(obj1,"1.2.(Object3D)3") == null; // does not exist, should return null
	obj1_2.insertChildSave(obj1_2_2);
	assert Object3DTools.findByFullName(obj1,"p1.p1_2.p1_2_21") != null; // should exist
    }

}
