#!/bin/csh
if ($NANOTILER_HOME == "") then
    echo "NANOTILER_HOME variable not defined!"
    exit
endif
mkdir -p ${NANOTILER_HOME}/test/generated_scripts
set assertions="-ea"
if ($1 == "da") then
    set assertions="-da"
endif
java -Dpid=$$ $assertions -Xmx1500m -cp ${NANOTILER_HOME}/jar/nanotiler.jar:${NANOTILER_HOME}/jar/nanotilertests.jar:testng-5.2-jdk15.jar:${NANOTILER_HOME}/jar/Jama-1.0.3.jar:/home/bas/bindewae/project/ENM/src:${NANOTILER_HOME}/jar/printf.jar:${NANOTILER_HOME}/jar/spline.jar:. org.testng.TestNG -d ${NANOTILER_HOME}/test/testng-output ${NANOTILER_HOME}/test/testng-newest.xml
