package nanotilertests.tools3d.objects3d.elasticnet;

import java.io.InputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.Logger;

import Jama.EigenvalueDecomposition;

import rnadesign.rnamodel.GeneralPdbWriter;
import rnadesign.rnamodel.RnaPdbReader;
import tools3d.objects3d.Object3DFactory;
import tools3d.objects3d.Object3DLinkSetBundle;
import tools3d.objects3d.Object3DTools;
import tools3d.objects3d.Object3D;
import tools3d.objects3d.Object3DSet;
import tools3d.objects3d.SimpleObject3DSet;
import tools3d.objects3d.LinkSet;
import tools3d.objects3d.SimpleLinkSet;
import tools3d.objects3d.Object3DSetTools;

import tools3d.objects3d.elasticnet.*;

/** This class evaluates a PDB with the RnaSpot potential generated
 * with the program RnaSpotGen.
 */
public class ElasticNetTest {

    private static Logger log = Logger.getLogger("NanoTiler_debug");

    private static int gap = 4;
    
    private static double slack = 15.0;

    private static void writeDecomposition(Logger l, EigenvalueDecomposition decomp) {
	double[] eigenValues = decomp. getRealEigenvalues();
	double[] imagEigenValues = decomp. getImagEigenvalues();
	for (int i = 0; i < eigenValues.length; ++i) {
	    l.fine("" + (i+1) + " " + eigenValues[i]); //  + " + i " + imagEigenValues[i]);
	}
    }

    public static void main(String[] args) {
	boolean deleteAtomsMode = true;
	log.fine("ElasticNetTest called");
	if (args.length < 1) {
	    log.fine("ElasticNetTest usage: ElasticNetTest pdbfilename");
	    System.exit(0);
	}
	EnmObjectAlgorithm eniAlgorithm = new SimpleEnmObjectAlgorithm();
	GeneralPdbWriter writer = new GeneralPdbWriter();
	for (int i = 0; i < args.length; ++i) {
	    String fileName = args[i];
	    // open file:
	    try {
		log.fine("Processing file " + (i+1) + " : " + fileName);
		InputStream is = new FileInputStream(fileName);
		Object3DFactory reader = new RnaPdbReader();
		Object3DLinkSetBundle bundle = reader.readBundle(is);
		log.fine(writer.writeString(bundle.getObject3D()));
		if (deleteAtomsMode) {
		    log.fine("Removing all atoms!");
		    Object3DTools.removeByClassName(bundle.getObject3D(), "Atom3D");
		}
		is.close();
		Object3D root = bundle.getObject3D();
		// keep only nucleotides or amino acids:
		Object3DSet objectSet = new SimpleObject3DSet();
		LinkSet links = new SimpleLinkSet();
		String[] objectNames = {"Nucleotide3D", "AminoAcid3D"};
		// generate object set such that it contains only amino acids ant nucleotides
		Object3DSetTools.addToSet(objectSet, root, objectNames);

		// run elastic network algorithm:
		EigenvalueDecomposition decomp = eniAlgorithm.computeNormalModes(objectSet, links);
		// output:
		log.fine("Results:");
		writeDecomposition(log, decomp);
	    }
	    catch (IOException exp) {
		log.severe("Error opening input file: " + fileName);
	    }
	}

    }

}
