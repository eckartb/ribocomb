package nanotilertests.rnadesign.rnaspot;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.IOException;
import java.util.logging.Logger;

import rnadesign.rnamodel.RnaPdbReader;
import tools3d.objects3d.Object3DFactory;
import tools3d.objects3d.Object3DLinkSetBundle;

import rnadesign.rnaspot.*;

/** This class evaluates a PDB with the RnaSpot potential generated
 * with the program RnaSpotGen.
 */
public class RnaSpotTest {

    private static Logger log = Logger.getLogger("NanoTiler_debug"); /** The generated debug log. */

    private static int gap = 4;
    
    private static double slack = 15.0;


    public static void main(String[] args) {
	System.out.print("RnaSpotGen call with: ");
	for (int i = 0; i < args.length; ++i) {
	    System.out.print(args[i] + " ");
	}
	if (args.length < 2) {
	    log.severe("RnaSpotTest usage: RnaSpotTest paramterfilename pdbfilename");
	    System.exit(0);
	}
	System.out.println("");
	// this datastructure collects all the interaction data:
	SimpleInteractionHistogramSet interactions = new SimpleInteractionHistogramSet();
	String parameterFileName = args[0];
	try {
	    FileInputStream fis = new FileInputStream(parameterFileName);
	    interactions.read(fis);
	}
	catch (FileNotFoundException e1) {
	    log.severe("File not found: " + parameterFileName);
	    System.exit(0);
	}
	catch (IOException e2) {
	    log.severe("Error reading parameter file: " 
			       + parameterFileName);
	    System.exit(0);
	}
	// create evaluator from set of raw histograms:
	// InteractionEvaluatorFactory evaluatorFactory = new SipplEvaluatorFactory(interactions);
	// InteractionEvaluator evaluator = evaluatorFactor.create(); // can compute every atom pair interaction
	RnaStructureEvaluator evaluator = new RnaStructureEvaluator(interactions, gap, slack);
	for (int i = 1; i < args.length; ++i) {
	    String fileName = args[i];
	    // open file:
	    try {
		System.out.print("Processing file " + (i+1) + " : " + fileName);
		InputStream is = new FileInputStream(fileName);
		Object3DFactory reader = new RnaPdbReader();
		Object3DLinkSetBundle bundle = reader.readBundle(is);
		is.close();
		// evalutate score:
		log.info(" Score: " + evaluator.getValue(bundle));
	    }
	    catch (IOException exp) {
		log.severe("Error opening input file: " + fileName);
	    }
	}

    }

}
