This tutorial demonstrates how to generated RNA structures using a graph and not "motifs". Instead the graph is traced with helices; helix positions are optimized (see script tutorial6a.script and reference/angle.script).
The resulting structure contains gaps; single-stranded fragments that "bridge" those gaps can be found with the bridgeit command (see script tutorial6b.script).
Lastly, the strands of the found bridging fragments can be fused with the fusestrands command (see script tutorial6c.scipt).
The resulting structure tutorial6_fused.pdb can then be used for molecular refinement or sequence optimization.
