This tutorial shows how a draft structure can be used for sequence optimization using the web server:
http://matchfold.abcc.ncifcrf.gov
The optimized sequence can then be incorporated into the structure using the mutate command.
