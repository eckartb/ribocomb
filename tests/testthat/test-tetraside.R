library(ribocomb)
library(testthat)

proot <- system.file(package="ribocomb")
NANO = file.path(proot,"nanotiler","bin","nanoscript")

indir = file.path(proot,"extdata","structures","hex87")

namesfile = file.path(indir,"hshape.names")
testthat::expect_true(file.exists(namesfile))
prefix = paste("loadjunctions", namesfile, indir, 1)
hnames <- c("k2.1.1.1","k2.1.1.2","k2.1.2.1","k2.1.2.2","k2.1.3.1","k2.1.3.2","k2.1.4.1","k2.1.4.2","k2.1.5.1","k2.1.5.2","k2.1.6.1","k2.1.6.2","j3.1.7.1","j3.1.7.2")
hconn <- matrix(0,nrow=length(hnames),ncol=length(hnames)) # integer ids of each connection

colnames(hconn) <- hnames
rownames(hconn) <- colnames(hconn)

hconn[2,3] <- 1
hconn[4,5] <- 1
hconn[6,7] <- 1
hconn[8,9] <- 1
hconn[10,11] <- 1
hconn[1,13] <- 2
hconn[12,14] <- 3

result <- bbhelixassemble(hconn=hconn,hmin=c(10,2,2),hmax=c(10,5,5),prefix=prefix,opt_steps=100000)

nms <- names(result)
for (nm in nms) {
 v <- as.numeric(strsplit(nm,"-")[[1]])
# cat("# working on case", nm,":\n")
 outfile = paste0("test-tetraside_",nm,".script")
 writeLines(result[[nm]],outfile)
 nanocommand = paste(NANO, outfile)
 cat("#",nanocommand, "\n") 	      
 nano_out <- system(nanocommand, intern=TRUE,ignore.stderr=TRUE)
 nano_result = nano_out[length(nano_out)-1]
 parsed <- parse_nanoscript(nano_result)
# print(parsed)
 cat("@result",nm, v, parsed[["start_score"]],parsed[["final_score"]], "\n")
}

# print(result)
